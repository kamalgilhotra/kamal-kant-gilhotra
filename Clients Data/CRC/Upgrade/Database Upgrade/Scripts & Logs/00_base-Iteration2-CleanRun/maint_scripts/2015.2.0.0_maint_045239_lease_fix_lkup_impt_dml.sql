/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045239_lease_fix_lkup_impt_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 11/30/2015 A Scott, W Davis Fix Lease import lookups
||============================================================================
*/

update pp_import_template_fields
set import_lookup_id = 1042
where column_name = 'lease_id'
and import_type_id = (
   select import_type_id 
   from pp_import_type
   where description = 'Add: Interim Interest Rates'
);

update pp_import_lookup
set lookup_sql =' ( select b.lease_id from ls_lease b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) ) '
where import_lookup_id = 1042;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2978, 0, 2015, 2, 0, 0, 45239, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045239_lease_fix_lkup_impt_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
