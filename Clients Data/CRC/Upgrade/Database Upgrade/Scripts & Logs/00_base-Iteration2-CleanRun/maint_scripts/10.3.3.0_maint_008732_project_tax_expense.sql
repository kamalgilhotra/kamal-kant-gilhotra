/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008732_project_tax_expense.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/03/2011 Zack Spratling Point Release
||============================================================================
*/

create table TE_AGGREGATION
(
 TE_AGGREGATION_ID number(22,0) not null,
 DESCRIPTION       varchar2(35),
 LONG_DESCRIPTION  varchar2(255)
);

alter table TE_AGGREGATION
   add constraint TE_AGGREGATION_PK
       primary key (TE_AGGREGATION_ID)
       using index tablespace PWRPLANT_IDX;

insert into TE_AGGREGATION
   (TE_AGGREGATION_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (1, 'Work Order Test', 'Tax status determination will be made at the work order level.');

insert into TE_AGGREGATION
   (TE_AGGREGATION_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (2, 'Non-Qualifying', 'No work orders under this funding project will qualify for tax repairs.');

insert into TE_AGGREGATION
   (TE_AGGREGATION_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (3, 'Fully Qualifying',
    'All work orders under this funding project will qualify for tax repairs.');

insert into TE_AGGREGATION
   (TE_AGGREGATION_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (4, 'Aggregate',
    'Work orders under this funding project should be aggregated to determine their tax repair status.');
commit;

alter table WORK_ORDER_ACCOUNT add TE_AGGREGATION_ID number(22,0) default 1;
alter table WORK_ORDER_ACCOUNT
   add constraint R_WORK_ORDER_ACCOUNT22
       foreign key (TE_AGGREGATION_ID)
       references TE_AGGREGATION(TE_AGGREGATION_ID);

alter table WO_TAX_EXPENSE_TEST add TAX_STATUS_FP_FULLY_QUALIFYING number(22,0);
alter table WO_TAX_EXPENSE_TEST
   add constraint FK_FULLY_QUAL
       foreign key (TAX_STATUS_FP_FULLY_QUALIFYING)
       references WO_TAX_STATUS(TAX_STATUS_ID);

alter table WO_TAX_EXPENSE_TEST add TAX_STATUS_FP_NON_QUALIFYING number(22,0);
alter table WO_TAX_EXPENSE_TEST
   add constraint FK_NON_QUAL
       foreign key (TAX_STATUS_FP_NON_QUALIFYING)
       references WO_TAX_STATUS(TAX_STATUS_ID);

alter table WO_TAX_EXPENSE_TEST
   add constraint FK_NO_REPL
       foreign key (TAX_STATUS_NO_REPLACEMENT)
       references WO_TAX_STATUS(TAX_STATUS_ID);

alter table WO_TAX_EXPENSE_TEST
   add constraint FK_OVER_MAX_AMT
       foreign key (TAX_STATUS_OVER_MAX_AMOUNT)
       references WO_TAX_STATUS(TAX_STATUS_ID);

alter table RELATED_WOS add AGGREGATED number(22,0) default 0;

alter table WORK_ORDER_ACCOUNT add TAX_EXPENSE_TEST_ID number(22,0);
alter table WORK_ORDER_ACCOUNT
   add constraint R_WORK_ORDER_ACCOUNT21
       foreign key (TAX_EXPENSE_TEST_ID)
       references WO_TAX_EXPENSE_TEST(TAX_EXPENSE_TEST_ID);

update WORK_ORDER_ACCOUNT A
   set (TAX_EXPENSE_TEST_ID, REPAIR_EXCLUDE) =
        (select nvl(a.tax_expense_test_id, B.TAX_EXPENSE_TEST_ID), nvl(a.repair_exclude, B.REPAIR_EXCLUDE)
           from WORK_ORDER_TYPE B, WORK_ORDER_CONTROL C
          where A.WORK_ORDER_ID = C.WORK_ORDER_ID
            and B.WORK_ORDER_TYPE_ID = C.WORK_ORDER_TYPE_ID);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (9, 0, 10, 3, 3, 0, 8732, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008732_project_tax_expense.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
