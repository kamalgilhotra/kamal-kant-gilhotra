/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045507_lease_add_fasb_dynamic_inputs_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 02/29/2016 Will Davis       Make FASB lease test dynamic
|| 2015.2.2.0 04/14/2016 David Haupt	  Backporting to 2015.2.2
||============================================================================
*/

create table ls_fasb_test_criteria
(company_id number (22,0) not null,
 set_of_books_id number(22,0) not null,
 time_stamp date,
 user_id varchar2(24),
 econ_life_percent number (22,8) not null,
 npv_percent number (22,8) not null);

ALTER TABLE ls_fasb_test_criteria
  ADD CONSTRAINT pk_ls_fasb_test_criteria PRIMARY KEY (
    company_id,
    set_of_books_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
;

comment on table ls_fasb_test_criteria is 'A dynamic table allowing customers to input values by company for FASB or IASB capitalization tests';
comment on column ls_fasb_test_criteria.company_id is 'The internal company identifier with PowerPlant';
comment on column ls_fasb_test_criteria.company_id is 'System-assigned identifier to a set of books.';
comment on column ls_fasb_test_criteria.econ_life_percent is 'The percentage of the economic life that a lease term must exceed in order to be considered capital';
comment on column ls_fasb_test_criteria.npv_percent is 'The percentage of the fair market value of underlying assets that the net present value must exceed in order for an ILR to be considered capital';





--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3082, 0, 2015, 2, 2, 0, 045507, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045507_lease_add_fasb_dynamic_inputs_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;