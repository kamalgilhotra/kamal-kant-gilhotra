/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052835_lessor_02_partial_month_import_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 03/26/2019  Sarah Byers      Add Partial Month Payment Term Type functionality to Lessor
||============================================================================
*/

insert into pp_import_column (
  import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, is_on_table)
select import_type_id, 'payment_term_type_id', 'Payment Term Type', 'payment_term_type_id_xlate', 0, 1, 'number(22,0)', 1
  from pp_import_type
 where upper(import_table_name) = 'LSR_IMPORT_ILR';
 
update pp_import_column
   set description = 'Payment Term Date (yyyymmdd)'
 where import_type_id = (select import_type_id from pp_import_type where upper(import_table_name) = 'LSR_IMPORT_ILR')
   and column_name = 'payment_term_date'; 

insert into pp_import_lookup (
  import_lookup_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name)
values (
  2038, 'Payment Term Type.Description', 'Payment Term Type.Description', 'payment_term_type_id', 
  '(select pt.payment_term_type_id from ls_payment_term_type pt where upper( trim( <importfield> ) ) = upper( trim( pt.description ) ) and pt.payment_term_type_id in (2,4) )',
  0, 'ls_payment_term_type', 'description');
  
insert into pp_import_column_lookup (
  import_type_id, column_name, import_lookup_id)
select import_type_id, 'payment_term_type_id', 2038
  from pp_import_type
 where upper(import_table_name) = 'LSR_IMPORT_ILR';
 
insert into pp_import_template_fields (
  import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select t.import_template_id, nvl(max(f.field_id),0) + 1, t.import_type_id, 'payment_term_type_id', 2038
  from pp_import_template t
  join pp_import_template_fields f on f.import_template_id = t.import_template_id 
                                  and f.import_type_id = t.import_type_id
 where t.import_type_id = (select import_type_id from pp_import_type where upper(import_table_name) = 'LSR_IMPORT_ILR')
   and lower(t.long_description) like 'default template for lessor ilr%'
 group by t.import_template_id, t.import_type_id;  


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16245, 0, 2018, 2, 0, 0, 52835, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052835_lessor_02_partial_month_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;