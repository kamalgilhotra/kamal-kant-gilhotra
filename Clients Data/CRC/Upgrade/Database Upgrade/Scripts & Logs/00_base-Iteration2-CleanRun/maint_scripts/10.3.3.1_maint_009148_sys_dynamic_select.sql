/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009148_sys_dynamic_select.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.1   01/11/2012 David Liss     Point Release
||============================================================================
*/

create or replace package body PP_MISC_PKG as
   function DYNAMIC_SELECT(SQLS varchar2) return varchar2 is
      /*
      ||============================================================================
      || Application: PowerPlant
      || Object Name: DYNAMIC_SELECT
      || Description: Executes a select statement and only succeeds if returns exactly one record.
      || Used in cases when creating a datastore in powerscript to return one record could
      || affect performance. Make sure you check sqlca.sqlcode when calling this function.
      ||============================================================================
      || Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
      ||============================================================================
      || Version Date       Revised By     Reason for Change
      || ------- ---------- -------------- -----------------------------------------
      || 1.0     03/23/2011 David Liss   Create
      || 1.1     01/11/2012 David Liss   Need to Raise Application Error to catch all errors
      ||============================================================================
      */

      RECORD_VAL varchar2(4000);

   begin
      -- If this doesn't return 1 record this will fail.
      execute immediate SQLS
         into RECORD_VAL;

      return RECORD_VAL;

   exception
      when others then
         -- this catches all SQL errors, including no_data_found
         RAISE_APPLICATION_ERROR(-20001, 'ERROR executing SQL Block: ', true);

   end;
end PP_MISC_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (78, 0, 10, 3, 3, 1, 9148, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.1_maint_009148_sys_dynamic_select.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
