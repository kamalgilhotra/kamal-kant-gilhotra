/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038593_lease_component_import.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 06/17/2014 Shane Ward          Adding Component and II Rate Import
||============================================================================
*/

--Component Sequence
create sequence LS_COMPONENT_SEQ cache 20;

--Component Import
create table LS_IMPORT_COMPONENT
(
 IMPORT_RUN_ID               number(22,0) not null,
 LINE_ID                     number(22,0) not null,
 ERROR_MESSAGE               varchar2(4000),
 TIME_STAMP                  date,
 USER_ID                     varchar2(18),
 COMPONENT_ID                number(22,0),
 LS_COMP_STATUS_ID           number(22,0),
 COMPANY_ID                  number(22,0),
 COMPANY_XLATE               varchar2(254),
 DATE_RECEIVED               varchar2(254),
 DESCRIPTION                 varchar2(35),
 LONG_DESCRIPTION            varchar2(254),
 SERIAL_NUMBER               varchar2(35),
 PO_NUMBER                   varchar2(35),
 AMOUNT                      number(22,2),
 LS_ASSET_ID                 number(22,0),
 LS_ASSET_XLATE              varchar2(254),
 INTERIM_INTEREST_START_DATE varchar2(254),
 INTERIM_INTEREST            number(22,2)
);

comment on table LS_IMPORT_COMPONENT is 'Import table used by IMPORT TOOL to mass import components to ls_component';
comment on column LS_IMPORT_COMPONENT.IMPORT_RUN_ID is 'System assigned identifier of import run';
comment on column LS_IMPORT_COMPONENT.LINE_ID is 'Line in imported spreadsheet';
comment on column LS_IMPORT_COMPONENT.ERROR_MESSAGE is 'Holds validation error message for failed validations; if none, error_message is null';
comment on column LS_IMPORT_COMPONENT.COMPONENT_ID is 'System assigned identifier of component';
comment on column LS_IMPORT_COMPONENT.LS_COMP_STATUS_ID is 'Status of component, set list located in ls_component_status';
comment on column LS_IMPORT_COMPONENT.COMPANY_ID is 'System assigned identifier of company';
comment on column LS_IMPORT_COMPONENT.COMPANY_XLATE is 'Translate column for company.';
comment on column LS_IMPORT_COMPONENT.DATE_RECEIVED is 'Date component was received by lessor';
comment on column LS_IMPORT_COMPONENT.DESCRIPTION is 'Description of component.';
comment on column LS_IMPORT_COMPONENT.LONG_DESCRIPTION is 'Description of component.';
comment on column LS_IMPORT_COMPONENT.SERIAL_NUMBER is 'Serial Number for component/piece.';
comment on column LS_IMPORT_COMPONENT.PO_NUMBER is 'Powerplan Item Number.';
comment on column LS_IMPORT_COMPONENT.AMOUNT is 'Total amount assigned to component.';
comment on column LS_IMPORT_COMPONENT.LS_ASSET_ID is 'Asset the component is linked to.';
comment on column LS_IMPORT_COMPONENT.LS_ASSET_XLATE is 'Translate column for asset (asset number, description).';
comment on column LS_IMPORT_COMPONENT.INTERIM_INTEREST_START_DATE is 'Date interest begins to be owed.';
comment on column LS_IMPORT_COMPONENT.INTERIM_INTEREST is 'Calculated interest for component.';

create table LS_IMPORT_COMPONENT_ARCHIVE
(
 IMPORT_RUN_ID               number(22,0) not null,
 LINE_ID                     number(22,0) not null,
 ERROR_MESSAGE               varchar2(4000),
 TIME_STAMP                  date,
 USER_ID                     varchar2(18),
 COMPONENT_ID                number(22,0),
 LS_COMP_STATUS_ID           number(22,0),
 COMPANY_ID                  number(22,0),
 COMPANY_XLATE               varchar2(254),
 DATE_RECEIVED               varchar2(254),
 DESCRIPTION                 varchar2(35),
 LONG_DESCRIPTION            varchar2(254),
 SERIAL_NUMBER               varchar2(35),
 PO_NUMBER                   varchar2(35),
 AMOUNT                      number(22,2),
 LS_ASSET_ID                 number(22,0),
 LS_ASSET_XLATE              varchar2(254),
 INTERIM_INTEREST_START_DATE varchar2(254),
 INTERIM_INTEREST            number(22,2)
);

comment on table LS_IMPORT_COMPONENT_ARCHIVE is 'Import table used by IMPORT TOOL to archive mass import components from ls_import_component';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.IMPORT_RUN_ID is 'System assigned identifier of import run';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.LINE_ID is 'Line in imported spreadsheet';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.ERROR_MESSAGE is 'Holds validation error message for failed validations; if none, error_message is null';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.COMPONENT_ID is 'System assigned identifier of component';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.LS_COMP_STATUS_ID is 'Status of component, set list located in ls_component_status';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.COMPANY_ID is 'System assigned identifier of company';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.COMPANY_XLATE is 'Translate column for company.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.DATE_RECEIVED is 'Date component was received by lessor';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.DESCRIPTION is 'Description of component.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.LONG_DESCRIPTION is 'Description of component.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.SERIAL_NUMBER is 'Serial Number for component/piece.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.PO_NUMBER is 'Powerplan Item Number.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.AMOUNT is 'Total amount assigned to component.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.LS_ASSET_ID is 'Asset the component is linked to.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.LS_ASSET_XLATE is 'Translate column for asset (asset number, description).';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.INTERIM_INTEREST_START_DATE is 'Date interest begins to be owed.';
comment on column LS_IMPORT_COMPONENT_ARCHIVE.INTERIM_INTEREST is 'Calculated interest for component.';

--IMPORT Tool
insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, ALLOW_UPDATES_ON_ADD,
    DELEGATE_OBJECT_NAME)
values
   (255, 'Add: Lease Components', 'Lessee Components', 'ls_import_component', 'ls_import_component_archive', 1,
    'nvo_ls_logic_import');

insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID) values (255, 8);

--COLUMNS
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'component_id', 'Component ID', null, 1, 1, 'number(22,0)', null, 1, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'company_id', 'Company ID', 'company_xlate', 1, 1, 'varchar2(254)', 'company_setup', 1, 'company_id');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'date_received', 'Date Received', null, 1, 1, 'varchar(254)', null, 1, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'description', 'Description', null, 1, 1, 'varchar2(35)', null, 1, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'long_description', 'Long Description', null, 1, 1, 'varchar2(254)', null, 1, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'serial_number', 'Serial Number', null, 1, 1, 'varchar2(35)', null, 1, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'po_number', 'Purchase Order Number', null, 1, 1, 'varchar2(35)', null, 1, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'amount', 'Amount', null, 1, 1, 'number(22,2)', null, 0, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'ls_asset_id', 'Asset ID', 'ls_asset_xlate', 1, 1, 'varchar2(35)', 'ls_asset', 0, 'ls_asset_id');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (255, 'interim_interest_start_date', 'Interim Interest Start Date', null, 1, 1, 'varchar2(254)', null, 1, null);

--XLATES
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 255, 'ls_asset_id', IMPORT_LOOKUP_ID from PP_IMPORT_LOOKUP where COLUMN_NAME = 'ls_asset_id';

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 255, 'company_id', IMPORT_LOOKUP_ID from PP_IMPORT_LOOKUP where COLUMN_NAME = 'company_id';

--INTERIM INTEREST RATES

create table LS_IMPORT_LEASE_INTERIM_RATES
(
 IMPORT_RUN_ID number(22,0) not null,
 LINE_ID       number(22,0) not null,
 ERROR_MESSAGE varchar2(4000),
 LEASE_ID      number(22,0),
 LEASE_XLATE   varchar2(254),
 MONTH         varchar2(254),
 RATE          number(22,8),
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

comment on table LS_IMPORT_LEASE_INTERIM_RATES is 'Import table used by IMPORT TOOL to mass import lease rates to ls_lease_interim_rates';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.IMPORT_RUN_ID is 'System assigned identifier of import run';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.LINE_ID is 'Line in imported spreadsheet';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.ERROR_MESSAGE is 'Holds validation error message for failed validations; if none, error_message is null';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.LEASE_ID is 'Lease Identifier';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.LEASE_XLATE is 'Translate for lease (number, description)';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.MONTH is 'Month interim interest rate goes into service';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.RATE is 'Monthly rate';

create table LS_IMPORT_LEASE_INT_RATES_ARC
(
 IMPORT_RUN_ID number(22,0) not null,
 LINE_ID       number(22,0) not null,
 ERROR_MESSAGE varchar2(4000),
 LEASE_ID      number(22,0),
 LEASE_XLATE   varchar2(254),
 MONTH         varchar2(254),
 RATE          number(22,8),
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

comment on table LS_IMPORT_LEASE_INTERIM_RATES is 'Import table used by IMPORT TOOL to archive imported lease interim interest rates';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.IMPORT_RUN_ID is 'System assigned identifier of import run';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.LINE_ID is 'Line in imported spreadsheet';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.ERROR_MESSAGE is 'Holds validation error message for failed validations; if none, error_message is null';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.LEASE_ID is 'Lease Identifier';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.LEASE_XLATE is 'Translate for lease (number, description)';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.MONTH is 'Month interim interest rate goes into service';
comment on column LS_IMPORT_LEASE_INTERIM_RATES.RATE is 'Monthly rate';

--Import
insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, ALLOW_UPDATES_ON_ADD,
    DELEGATE_OBJECT_NAME)
values
   (256, 'Add: Lease Interim Interest Rates', 'Lessee Interim Interest Rates', 'ls_import_lease_interim_rates',
    'ls_import_lease_int_rates_arc', 1, 'nvo_ls_logic_import');

insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID) values (256, 8);

--Columns
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (256, 'lease_id', 'Lease ID', 'lease_xlate', 1, 1, 'number(22,0)', 'ls_lease', 1, 'lease_id');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (256, 'month', 'Month', null, 1, 1, 'varchar(254)', null, 1, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (256, 'rate', 'Interim Interest Rate', null, 1, 1, 'number(22,8)', null, 1, null);

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 256, 'lease_id', IMPORT_LOOKUP_ID from PP_IMPORT_LOOKUP where COLUMN_NAME = 'lease_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1215, 0, 10, 4, 3, 0, 38593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038593_lease_component_import.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
