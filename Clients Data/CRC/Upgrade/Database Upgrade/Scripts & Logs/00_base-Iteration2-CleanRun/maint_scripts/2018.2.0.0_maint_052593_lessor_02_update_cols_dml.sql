/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052593_lessor_02_update_cols_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 01/04/2019  Anand R          update new coloumn estimated residual columns in lsr_asset
||============================================================================
*/

update lsr_asset
set estimated_residual_amount = fair_market_value * estimated_residual_pct,
    estimated_res_amt_comp_curr = fair_market_value_comp_curr * estimated_residual_pct;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13463, 0, 2018, 2, 0, 0, 52593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052593_lessor_02_update_cols_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;