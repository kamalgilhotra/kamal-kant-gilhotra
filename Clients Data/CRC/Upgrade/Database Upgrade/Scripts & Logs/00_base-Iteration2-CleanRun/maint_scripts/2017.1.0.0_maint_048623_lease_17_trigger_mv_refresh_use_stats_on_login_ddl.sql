/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048623_lease_17_trigger_mv_refresh_use_stats_on_login_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/23/2017 Andrew Hill      Add a trigger to set the _mv_refresh_use_stats
||                                        parameter when logged in using PowerPlan, Post,
||                                        or the Lease SSP
||============================================================================
*/

CREATE OR REPLACE TRIGGER pwrplan_mv_refresh_use_stats
AFTER LOGON ON DATABASE
BEGIN
  IF LOWER(sys_context('USERENV', 'MODULE')) IN ('powerpla.exe', 'ssp_lease_control.exe')
      OR LOWER(sys_context('USERENV', 'MODULE')) LIKE 'post%.exe'
      OR LOWER(sys_context('USERENV', 'SESSION_USER')) = 'pwrplant'
  THEN
    EXECUTE IMMEDIATE 'alter session set "_mv_refresh_use_stats" = TRUE';
  END IF;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3672, 0, 2017, 1, 0, 0, 48623, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048623_lease_17_trigger_mv_refresh_use_stats_on_login_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
