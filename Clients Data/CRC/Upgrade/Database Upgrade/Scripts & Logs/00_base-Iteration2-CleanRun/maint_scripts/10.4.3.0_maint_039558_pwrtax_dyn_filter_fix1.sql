/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039558_pwrtax_dyn_filter_fix1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 09/09/2014 Andrew Scott        clean up of dynamic filters for powertax
||                                         base gui.  1 or 2 may still have tax year as
||                                         whole numbers, not decimals.
||============================================================================
*/

update PP_DYNAMIC_FILTER
   set DW_ID_DATATYPE = 'D'
 where LABEL = 'Tax Year'
   and DW_ID = 'tax_year'
   and DW_ID_DATATYPE = 'N';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1380, 0, 10, 4, 3, 0, 39558, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039558_pwrtax_dyn_filter_fix1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
