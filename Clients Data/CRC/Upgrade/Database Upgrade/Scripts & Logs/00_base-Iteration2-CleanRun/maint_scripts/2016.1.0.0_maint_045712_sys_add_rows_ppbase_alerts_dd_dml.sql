/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045712_sys_add_rows_ppbase_alerts_dd_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2016.1.0  06/14/2016 Anand R       Add rows for actions drop down for Alerts
||============================================================================
*/

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (1, 'ADMIN', 'work_order_id', 'drill_to_actuals', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (2, 'ADMIN', 'work_order_id', 'drill_to_estimate', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (3, 'ADMIN', 'work_order_id', 'drill_to_forecast', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (4, 'ADMIN', 'work_order_id', 'drill_to_commitments', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (5, 'ADMIN', 'work_order_id', 'drill_to_fp_details', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (6, 'ADMIN', 'work_order_id', 'drill_to_fp_dashboard', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (7, 'ADMIN', 'work_order_id', 'drill_to_completion_window', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (8, 'ADMIN', 'work_order_id', 'drill_to_cr', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (9, 'LESSEE', 'ilr_id', 'drill_to_ilr', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (10, 'LESSEE', 'lease_id', 'drill_to_mla', null, null);

insert into ppbase_alerts_drilldown_list (DRILLDOWN_ID, MODULE_NAME, KEY_FIELD, DRILL_TO_WINDOW, USER_ID, TIME_STAMP)
values (11, 'LESSEE', 'ls_asset_id', 'drill_to_lease_asset', null, null);

--=======================================================================

insert into ppbase_alerts_drilldown_config (drilldown_id, MODULE_NAME, OBJECT_TO_OPEN, WORKSPACE_IDENTIFIER, REQUEST_IDENTIFIER, REQUEST, OBJECT_STRUCTURE, USER_ID, TIME_STAMP)
values (9, 'LESSEE', 'w_ls_ilrcntr_details', 'search_ilr', null, null, 's_calling_structure_2', null, null);

insert into ppbase_alerts_drilldown_config (drilldown_id, MODULE_NAME, OBJECT_TO_OPEN, WORKSPACE_IDENTIFIER, REQUEST_IDENTIFIER, REQUEST, OBJECT_STRUCTURE, USER_ID, TIME_STAMP)
values (11, 'LESSEE', 'w_ls_lscntr_details', 'search_asset', null, null, 's_calling_structure_2', null, null);

insert into ppbase_alerts_drilldown_config (drilldown_id, MODULE_NAME, OBJECT_TO_OPEN, WORKSPACE_IDENTIFIER, REQUEST_IDENTIFIER, REQUEST, OBJECT_STRUCTURE, USER_ID, TIME_STAMP)
values (10, 'LESSEE', 'w_ls_assetcntr_details', 'search_mla', null, null, 's_calling_structure_2', null, null);

--=======================================================================

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values (40, 'ADMIN', 'drill_to_ilr', 'ILR', 1, 'ue_drilldown', 'PWRPLANT', to_date('06-06-2016 16:01:59', 'dd-mm-yyyy hh24:mi:ss'));

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values (41, 'ADMIN', 'drill_to_mla', 'MLA', 1, 'ue_drilldown', 'PWRPLANT', to_date('13-06-2016 22:23:51', 'dd-mm-yyyy hh24:mi:ss'));

insert into ppbase_actions_windows (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values (42, 'ADMIN', 'drill_to_lease_asset', 'Lease Asset', 1, 'ue_drilldown', 'PWRPLANT', to_date('10-06-2016 13:25:20', 'dd-mm-yyyy hh24:mi:ss'));



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3218, 0, 2016, 1, 0, 0, 045712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045712_sys_add_rows_ppbase_alerts_dd_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;