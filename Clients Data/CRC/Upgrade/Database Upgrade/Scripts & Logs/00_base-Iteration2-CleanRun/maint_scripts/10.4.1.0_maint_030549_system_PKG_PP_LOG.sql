/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030549_system_PKG_PP_LOG.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 B. Beck        Point release
||============================================================================
*/

create or replace package PKG_PP_LOG as
   subtype G_RESULT_TYPE is pls_integer range 1 .. 3 not null;
   G_SUCCESS constant G_RESULT_TYPE := 1;
   G_FAIL    constant G_RESULT_TYPE := 2;
   G_WARNING constant G_RESULT_TYPE := 3;

   procedure P_START_LOG(P_PROCESS_ID in pls_integer);

   procedure P_END_LOG;

   procedure P_WRITE_MESSAGE(P_MSG in varchar2);

   procedure P_WRITE_MESSAGE(P_MSG            in varchar2,
                             P_PP_ERROR_CODE  in varchar2,
                             P_SQL_ERROR_CODE in varchar2);
end PKG_PP_LOG;
/

create or replace package body PKG_PP_LOG as
   /*
   ||============================================================================
   || Subsystem: PowerPlant
   || Object Name: maint_030549_system_PKG_PP_LOG.sql
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.1.0 05/09/2013 B.Beck         Original Version
   ||============================================================================
   */

   G_PROCESS_ID    pls_integer;
   G_OCCURRENCE_ID pls_integer;
   G_MSG_ORDER     pls_integer;

   --**************************************************************************
   --                            P_START_LOG
   --**************************************************************************
   procedure P_START_LOG(P_PROCESS_ID in pls_integer) is
      pragma autonomous_transaction;

   begin
      G_PROCESS_ID := P_PROCESS_ID;
      G_MSG_ORDER  := 0;

      select NVL(max(OCCURRENCE_ID), 0) + 1
        into G_OCCURRENCE_ID
        from PP_PROCESSES_OCCURRENCES
       where PROCESS_ID = G_PROCESS_ID;

      insert into PP_PROCESSES_OCCURRENCES
         (PROCESS_ID, OCCURRENCE_ID, START_TIME)
      values
         (G_PROCESS_ID, G_OCCURRENCE_ID, sysdate);

      commit;
   exception
      when others then
         rollback;
   end P_START_LOG;

   --**************************************************************************
   --                            P_END_LOG
   --**************************************************************************
   procedure P_END_LOG is
      pragma autonomous_transaction;

   begin
      update PP_PROCESSES_OCCURRENCES
         set END_TIME = sysdate
       where PROCESS_ID = G_PROCESS_ID
         and OCCURRENCE_ID = G_OCCURRENCE_ID;

      commit;
   exception
      when others then
         rollback;
   end P_END_LOG;

   --**************************************************************************
   --                            P_WRITE_MESSAGE
   --**************************************************************************
   procedure P_WRITE_MESSAGE(P_MSG in varchar2) is
      pragma autonomous_transaction;

   begin
      for I in 0 .. FLOOR(LENGTH(P_MSG) / 2000)
      loop
         G_MSG_ORDER := G_MSG_ORDER + 1;

         insert into PP_PROCESSES_MESSAGES
            (PROCESS_ID, OCCURRENCE_ID, MSG, MSG_ORDER, TIME_STAMP, USER_ID)
         values
            (G_PROCESS_ID, G_OCCURRENCE_ID, SUBSTR(P_MSG, 1 + I * 2000, 2000), G_MSG_ORDER, sysdate,
             user);
      end loop;

      commit;
   exception
      when others then
         rollback;
         raise;
   end P_WRITE_MESSAGE;

   --**************************************************************************
   --                            P_WRITE_MESSAGE
   --**************************************************************************
   procedure P_WRITE_MESSAGE(P_MSG            in varchar2,
                             P_PP_ERROR_CODE  in varchar2,
                             P_SQL_ERROR_CODE in varchar2) is
      pragma autonomous_transaction;

   begin
      for I in 0 .. FLOOR(LENGTH(P_MSG) / 2000)
      loop
         G_MSG_ORDER := G_MSG_ORDER + 1;

         insert into PP_PROCESSES_MESSAGES
            (PROCESS_ID, OCCURRENCE_ID, MSG, MSG_ORDER, PP_ERROR_CODE, SQL_ERROR_CODE, TIME_STAMP,
             USER_ID)
         values
            (G_PROCESS_ID, G_OCCURRENCE_ID, SUBSTR(P_MSG, 1 + I * 2000, 2000), G_MSG_ORDER,
             P_PP_ERROR_CODE, P_SQL_ERROR_CODE, sysdate, user);
      end loop;

      commit;
   exception
      when others then
         rollback;
         raise;
   end P_WRITE_MESSAGE;
end PKG_PP_LOG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (447, 0, 10, 4, 1, 0, 30549, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030549_system_PKG_PP_LOG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
