/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044447_aro_liability_span_report_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2 07/21/2015   Andrew Hill    Initial commit
||============================================================================
*/

UPDATE pp_reports
SET input_window = 'dw_company_select,dw_set_of_books_select'
where report_number = 'ARO - 1000';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2706, 0, 2015, 2, 0, 0, 044447, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044447_aro_liability_span_report_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;