/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040140_depr_indexes_for_depr_calc.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.2.4 09/30/2014 Brandon Beck
||========================================================================================
*/

create index IX_DEPR_CALC_STG_1 on DEPR_CALC_STG (DEPR_METHOD_ID,SET_OF_BOOKS_ID);
create index IX_DEPR_CALC_STG_2 on DEPR_CALC_STG (DEPR_GROUP_ID,SET_OF_BOOKS_ID,GL_POST_MO_YR, FUNDING_WO_ID, REVISION);
create index IX_DEPR_LDG_BLND_STG_2 on DEPR_LEDGER_BLENDING_STG (DEPR_GROUP_ID,SOURCE_SOB_ID,GL_POST_MO_YR, FUNDING_WO_ID, REVISION);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1476, 0, 10, 4, 2, 4, 40140, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.4_maint_040140_depr_indexes_for_depr_calc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;