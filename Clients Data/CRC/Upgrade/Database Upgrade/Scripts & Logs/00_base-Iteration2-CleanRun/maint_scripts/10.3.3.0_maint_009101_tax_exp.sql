/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009101_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   12/12/2011 Elhadj Bah     Point Release
||============================================================================
*/

drop table REPAIR_WORK_ORDER_TEMP;
drop table REPAIR_WORK_ORDER_TEMP_ORIG;
drop table REPAIR_AMOUNT_ALLOCATE;
drop table REPAIR_BLANKET_PROCESSING;

create global temporary table REPAIR_WORK_ORDER_TEMP
(
 COMPANY_ID                number(22,0) not null,
 WORK_ORDER_NUMBER         varchar2(35) not null,
 REPAIR_UNIT_CODE_ID       number(22,0) not null,
 REPAIR_LOCATION_ID        number(22,0) not null,
 VINTAGE                   number(22,0) not null,
 FERC_ACTIVITY_CODE        number(22,0) not null,
 REPAIR_METHOD_ID          number(22,0) not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 QUANTITY                  number(22,0),
 COST                      number(22,2),
 REPAIR_QTY_INCLUDE        number(22,2) default 1,
 TOTAL_PERIOD_ADDS_EXPENSE number(22,2),
 TOTAL_PERIOD_RETS_EXPENSE number(22,2)
) on commit preserve rows;

alter table REPAIR_WORK_ORDER_TEMP
   add CONSTRAINT PK_REPAIR_WORK_ORDER_TEMP
       primary key (COMPANY_ID, WORK_ORDER_NUMBER, REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID,
                    VINTAGE, FERC_ACTIVITY_CODE, REPAIR_METHOD_ID, REPAIR_QTY_INCLUDE);

create global temporary table REPAIR_WORK_ORDER_TEMP_ORIG
(
 COMPANY_ID                number(22,0) not null,
 WORK_ORDER_NUMBER         varchar2(35) not null,
 REPAIR_UNIT_CODE_ID       number(22,0) not null,
 REPAIR_LOCATION_ID        number(22,0) not null,
 VINTAGE                   number(22,0) not null,
 FERC_ACTIVITY_CODE        number(22,0) not null,
 REPAIR_METHOD_ID          number(22,0) not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 QUANTITY                  number(22,0),
 COST                      number(22,2),
 UTILITY_ACCOUNT_ID        number(22,0) not null,
 BUS_SEGMENT_ID            number(22,0) not null,
 RETIREMENT_UNIT_ID        number(22,0) not null,
 OVH_TO_UDG                number(22,0) default 0,
 REPAIR_QTY_INCLUDE        number(22,2) default 1,
 TOTAL_PERIOD_ADDS_EXPENSE number(22,2),
 TOTAL_PERIOD_RETS_EXPENSE number(22,2)
) on commit preserve rows;

alter table REPAIR_WORK_ORDER_TEMP_ORIG
   add constraint PK_REPAIR_WORK_ORDER_TEMP_ORIG
       primary key (COMPANY_ID, WORK_ORDER_NUMBER, REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID, VINTAGE,
                    FERC_ACTIVITY_CODE, REPAIR_METHOD_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID,
                    RETIREMENT_UNIT_ID);

create global temporary table REPAIR_AMOUNT_ALLOCATE
(
 ASSET_ID                      number(22,0) not null,
 ASSET_ACTIVITY_ID             number(22,0) not null,
 COMPANY_ID                    number(22,0),
 WORK_ORDER_NUMBER             varchar2(35),
 REPAIR_UNIT_CODE_ID           number(22,0),
 REPAIR_LOCATION_ID            number(22,0),
 REPAIR_METHOD_ID              number(22,0),
 BOOK_SUMMARY_ID               number(22,0),
 ACTIVITY_COST                 number(22,2),
 ACTIVITY_COST_RATIO           number(22,8),
 MY_REPAIR_ELIGIBLE_AMOUNT     number(22,2),
 REPAIR_AMOUNT_SUB             number(22,2),
 USER_ID                       varchar2(18),
 TIME_STAMP                    date,
 WORK_REQUEST                  varchar2(35) not null,
 MY_RETIREMENT_ELIGIBLE_AMOUNT number(22,2),
 FERC_ACTIVITY_CODE            number(22,0)
) on commit preserve rows;

alter table REPAIR_AMOUNT_ALLOCATE
   add constraint PK_REPAIR_AMOUNT_ALLOCATE
       primary key (ASSET_ID, ASSET_ACTIVITY_ID, WORK_REQUEST);

create global temporary table REPAIR_BLANKET_PROCESSING
(
 WORK_ORDER_ID              number(22,0) not null,
 WORK_ORDER_NUMBER          varchar2(35),
 COMPANY_ID                 number(22,0),
 BLANKET_METHOD             varchar2(35) not null,
 REPAIR_LOCATION_ID         number(22,0) not null,
 REPAIR_UNIT_CODE_ID        number(22,0) not null,
 CLOSING_OPTION_ID          number(22,0),
 REPLACEMENT_QTY            number(22,2),
 WO_UOP_QTY                 number(22,0),
 WO_UOP_RETIRE_QTY          number(22,0),
 WO_UOP_COST                number(22,2),
 WO_UOP_RETIRE_COST         number(22,2),
 WO_TOTAL_PERIOD_COST       number(22,2),
 EXPENSE_PERCENTAGE         number(22,2),
 NUMBER_OF_CIRCUITS         number(22,0),
 PRORATA_TEST_QTY           number(22,2),
 PRORATA_THRESHOLD          number(22,2),
 PRORATA_PASS_FAIL          varchar2(35),
 PROP_TOTAL_UOP_QTY         number(22,2),
 PROP_TOTAL_CIRCUIT_PERCENT number(22,2),
 PROP_QTY_PER_CIRCUIT       number(22,2),
 PROP_THRESHOLD             number(22,2),
 PROP_PASS_FAIL             varchar2(35),
 COST_UOP_REPLACEMENT_COST  number(22,2),
 COST_UOP_TEST_QTY          number(22,0),
 COST_THRESHOLD             number(22,2),
 COST_PASS_FAIL             varchar2(35),
 ALL_COST_REPLACEMENT_COST  number(22,2),
 ALL_COST_THRESHOLD         number(22,2),
 ALL_COST_PASS_FAIL         varchar2(35)
) on commit preserve rows;

alter table REPAIR_BLANKET_PROCESSING
   add constraint PK_REPAIR_BLANKET_PROCESSING
       primary key (WORK_ORDER_ID, BLANKET_METHOD, REPAIR_LOCATION_ID, REPAIR_UNIT_CODE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (70, 0, 10, 3, 3, 0, 9101, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_009101_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
