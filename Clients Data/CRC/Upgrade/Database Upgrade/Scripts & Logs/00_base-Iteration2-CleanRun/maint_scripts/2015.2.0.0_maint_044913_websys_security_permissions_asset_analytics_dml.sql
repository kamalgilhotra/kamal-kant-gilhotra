/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 	Security
|| File Name:   maint_044913_websys_security_permissions_asset_analytics_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------  -----------------------------------------------
|| 2015.2     09/11/2015 Khamchanh Sisouphanh Updating data for web security Asset Analytics
||==========================================================================================
*/

--Asset Analytics Permissions
DELETE FROM PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL
 WHERE MODULE_ID = 3;

INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Homepage.Config',3, null,'All','Analytics Homepage','Config');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Case.Action',3, null,'All','Case Management','Action Assign');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Case.Edit',3, null,'All','Case Management','Edit');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Case.Export',3, null,'All','Case Management','Export');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Case.NameDrill',3, null,'All','Case Management','Case Name - Drill Hyperlink');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Case.ShowChart',3, null,'All','Case Management','Show Chart');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Case.ShowComments',3, null,'All','Case Management','Show Comments');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Case.Inactivate',3, null,'All','Case Management','Inactivate');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.ProjectedRetirementsByCost',3, null,'All','Dashboards','Projected Retirements by Cost');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.ProjectedRetirementsByQty',3, null,'All','Dashboards','Projected Retirements by Quantity');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.SurvivorDistribution',3, null,'All','Dashboards','Survivor Distribution');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.LifeAnalysisByCost',3, null,'All','Dashboards','Life Analysis by Cost');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.LifeAnalysisByQty',3, null,'All','Dashboards','Life Analysis by Quantity');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.LifeAnalysisByEscalatedCost',3, null,'All','Dashboards','Life Analysis by Escalated Cost');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.CPRLedgerAnomalies',3, null,'All','Dashboards','CPR Ledger Anomalies');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.RetirementPatternsByCost',3, null,'All','Dashboards','Retirement Patterns by Cost');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.RetirementPatternsByQty',3, null,'All','Dashboards','Retirement Patterns by Quantity');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.AdditionPatternsByCost',3, null,'All','Dashboards','Addition Patterns by Cost');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.AdditionPatternsByQty',3, null,'All','Dashboards','Addition Patterns by Quantity');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.ReserveAdequacy',3, null,'All','Dashboards','Reserve Adequacy');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.CostOfRemovalPerUnit',3, null,'All','Dashboards','Cost of Removal per Unit');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.AverageUnitCosts',3, null,'All','Dashboards','Average Unit Costs');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.DepreciationLedgerTrends',3, null,'All','Dashboards','Depreciation Ledger Trends');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.CORReserveTrends',3, null,'All','Dashboards','COR Reserve Trends');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.LifeReserveTrends',3, null,'All','Dashboards','Life Reserve Trends');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.AverageAgeTrends',3, null,'All','Dashboards','Average Age Trends');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.RetirementActivityAnomalies',3, null,'All','Dashboards','Retirement Activity Anomalies');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.Dashboard.SalvageperUnit',3, null,'All','Dashboards','Salvage per Unit');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.SumDashboardBtns.ExportChart',3, null,'All','Summary Dashboard Buttons','Export Chart');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.SumDashboardBtns.DataView',3, null,'All','Summary Dashboard Buttons','Data View');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.SumDashboardBtns.SQL',3, null,'All','Summary Dashboard Buttons','SQL');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.SumDashboardBtns.Cases',3, null,'All','Summary Dashboard Buttons','Cases');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.SumDashboardBtns.Performance',3, null,'All','Summary Dashboard Buttons','Performance');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.SumDashboardBtns.PopoutFilters',3, null,'All','Summary Dashboard Buttons','Popout Filters');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.CreateCase',3, null,'All','Exploration Dashboard Buttons','Create Case');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.SQL',3, null,'All','Exploration Dashboard Buttons','SQL');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.ExportChart',3, null,'All','Exploration Dashboard Buttons','Export Chart');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.Details',3, null,'All','Exploration Dashboard Buttons','Details');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.SaveAsTemplate',3, null,'All','Exploration Dashboard Buttons','Save as Template');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.PopoutFilter',3, null,'All','Exploration Dashboard Buttons','Popout Filter');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.UpdateChartAndPivotOptions',3, null,'All','Exploration Dashboard Buttons','Update Chart And Pivot Options');
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.Back',3, null,'All','Exploration Dashboard Buttons','Back');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2871, 0, 2015, 2, 0, 0, 044913, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044913_websys_security_permissions_asset_analytics_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;