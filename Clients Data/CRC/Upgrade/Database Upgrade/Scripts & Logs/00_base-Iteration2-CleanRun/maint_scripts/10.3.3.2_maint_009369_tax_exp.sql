/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009369_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.2   02/09/2012 Sunjin Cone    Point Release
||============================================================================
*/

create global temporary TABLE REPAIR_CALC_PRA_REVERSE
(
 BATCH_ID            number(22, 0),
 REPAIR_SCHEMA_ID    number(22, 0),
 COMPANY_ID          number(22, 0) not null,
 FERC_ACTIVITY_CODE  number(22, 0),
 WORK_ORDER_NUMBER   varchar2(35),
 ASSET_ID            number(22, 0) not null,
 ASSET_ACTIVITY_ID   number(22, 0) not null,
 ADD_QUALIFIED       number(22,2),
 REVERSE             number(22,2),
 RETIREMENT_REVERSAL number(22,2),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
) on commit preserve rows;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (88, 0, 10, 3, 3, 2, 9369, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.2_maint_009369_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
