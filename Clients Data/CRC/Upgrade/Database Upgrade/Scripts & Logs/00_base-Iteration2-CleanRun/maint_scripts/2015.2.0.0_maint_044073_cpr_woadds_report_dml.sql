/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044073_cpr_woadds_report_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2.0.0 8/25/2015  Sunjin COne		New CPR report
||============================================================================
*/ 

insert into pp_reports (REPORT_ID,DESCRIPTION, 
LONG_DESCRIPTION,DATAWINDOW,SPECIAL_NOTE,
REPORT_NUMBER,INPUT_WINDOW,
PP_REPORT_SUBSYSTEM_ID,REPORT_TYPE_ID,PP_REPORT_TIME_OPTION_ID,PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_NUMBER,OLD_REPORT_NUMBER,
PP_REPORT_ENVIR_ID, DYNAMIC_DW)
 select  max(REPORT_ID)+1, 'Closed WO CPR Activity', 
'Closed Status Work Orders with Unitized Additions in More Than Three Separate Months for a selected company.','dw_cpr_adds_stnd_wos_months_report','ALREADY HAS REPORT TIME, ALREADY HAS LEDGER STATUS, SHOW ZERO ASSETS',
'Asset - 7000','dw_company_select', 
9,5,2,3, 1, 'Asset - 7000','',
1, 0
from PP_REPORTS ;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2834, 0, 2015, 2, 0, 0, 044073, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044073_cpr_woadds_report_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;