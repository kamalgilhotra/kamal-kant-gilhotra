/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045367_lease_add_fcst_charting_to_menu_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 02/19/2016 Charlie Shilling Add a link to the forecast charting comparison workspace in the menu
|| 2015.2.2.0 04/14/2016 David Haupt	  Backporting to 2015.2.2
||============================================================================
*/
INSERT INTO ppbase_workspace (MODULE, workspace_identifier, label, workspace_uo_name, object_type_id)
VALUES('LESSEE', 'financial_reporting', 'Financial Impact Reporting', 'uo_ls_forecastcntr_fin_reporting', 1);

INSERT INTO ppbase_menu_items (MODULE, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
VALUES ('LESSEE', 'financial_reporting', 2, 3, 'Review Financial Impact', 'forecast', 'financial_reporting', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3067, 0, 2015, 2, 2, 0, 045367, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045367_lease_add_fcst_charting_to_menu_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;