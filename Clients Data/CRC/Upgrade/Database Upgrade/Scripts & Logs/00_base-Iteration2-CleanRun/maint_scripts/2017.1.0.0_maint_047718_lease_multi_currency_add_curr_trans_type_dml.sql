/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047718_lease_multi_currency_add_curr_trans_type_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/11/2017 Anand R          add currency gain loss trans types
||============================================================================
*/

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3051, 'Lease Currency Gain/Loss Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3052, 'Lease Currency Gain/Loss Credit');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3566, 0, 2017, 1, 0, 0, 47718, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047718_lease_multi_currency_add_curr_trans_type_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;