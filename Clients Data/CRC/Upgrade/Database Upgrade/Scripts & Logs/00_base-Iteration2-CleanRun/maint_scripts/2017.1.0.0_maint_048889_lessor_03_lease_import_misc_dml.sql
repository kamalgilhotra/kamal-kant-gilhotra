/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048889_lessor_03_lease_import_misc_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/09/2017 Shane "C" Ward		Miscellaneous items for Lessor MLA import
||============================================================================
*/

DELETE FROM PP_IMPORT_COLUMN
WHERE  import_type_id = 501 AND
       column_name = 'partial_retire_sw';

UPDATE PP_IMPORT_COLUMN
SET    description = 'Sublease'
WHERE  column_name = 'sublease' AND
       import_type_id = 501;

UPDATE PP_IMPORT_COLUMN
SET    description = 'Sublease - Lessee Lease Number'
WHERE  column_name = 'sublease_lease_id' AND
       import_type_id = 501;

	   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3784, 0, 2017, 1, 0, 0, 48889, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048889_lessor_03_lease_import_misc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	   
