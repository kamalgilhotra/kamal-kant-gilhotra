/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_045616_5_reg_stat_ledger_sv_ddl.sql
||========================================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 2016.1.0.0 04/05/2016 Sarah Byers	  Create Stat Ledger View for query purposes
||========================================================================================
*/

create or replace view reg_statistic_ledger_sv (
	historic_ledger,
	forecast_ledger,
	reg_company,
	statistic,
	month_number,
	stat_value,
	unit_of_measure) as (
select h.long_description historic_ledger,
		 f.description forecast_ledger,
		 c.description reg_company,
	    s.description statistic,
		 l.month_number month_number,
		 l.stat_value stat_value,
		 s.unit_of_measure unit_of_measure
  from reg_statistic_ledger l,
		 reg_statistic s,
		 reg_historic_version h,
		 reg_forecast_version f,
		 (select distinct reg_company_id, description from reg_company_sv) c
 where l.historic_version_id = h.historic_version_id
	and l.forecast_version_id = f.forecast_version_id
	and l.reg_company_id = c.reg_company_id
	and l.reg_stat_id = s.reg_stat_id);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3159, 0, 2016, 1, 0, 0, 045616, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045616_5_reg_stat_ledger_sv_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;