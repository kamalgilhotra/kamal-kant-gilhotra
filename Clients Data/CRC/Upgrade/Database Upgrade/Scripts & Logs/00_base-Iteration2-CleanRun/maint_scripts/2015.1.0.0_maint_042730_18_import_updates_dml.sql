/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_18_import_updates_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select 
   it.import_template_id,
   9 AS field_id,
   type.import_type_id,
   'ilr_id' AS column_name,
   il.import_lookup_id
from pp_import_template it, pp_import_type type, pp_import_lookup il
where it.description = 'Lease Invoice Add'
and type.import_table_name = 'ls_import_invoice'
and il.description = 'LS ILR.ILR Number'
and not exists(
   select 1
   from pp_import_template_fields
   where import_template_id = it.import_template_id
   and import_type_id = type.import_type_id
   and column_name = 'ilr_id')
;

update pp_import_lookup set lookup_sql=replace(lookup_sql,' ) ) )',' ) ) and <importtable>.company_id = b.company_id)'),
   lookup_constraining_columns = 'company_id'
where column_name in ('ls_asset_id')
and lookup_sql not like '%company_id%';

update pp_import_lookup set lookup_sql=replace(lookup_sql,' ) ) )',' ) ) and <importtable>.company_id = b.company_id and <importtable>.lease_id = b.lease_id)'),
   lookup_constraining_columns = 'company_id, lease_id'
where column_name in ('ilr_id')
and lookup_sql not like '%company_id%';

update pp_import_lookup set lookup_sql=replace(replace(lookup_sql,'ls_lease b','ls_lease b, ls_lease_company c'),' ) ) )',' ) ) and b.lease_id = c.lease_id and <importtable>.company_id = c.company_id )'),
   lookup_constraining_columns = 'company_id'
where column_name in ('lease_id')
and lookup_sql not like '%company_id%';

update pp_import_column set processing_order = 2 where column_name = 'lease_id';
update pp_import_column set processing_order = 3 where column_name in ('ilr_id', 'ls_asset_id');

insert into pp_update_flex
(col_id, type_name, flexvchar1)
select 
nvl(max(col_id),0) + 1,
'Lease Upgrade',
'LS-1130_import_updates.sql'
from pp_update_flex;

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2400, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_18_import_updates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;