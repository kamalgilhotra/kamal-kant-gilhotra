/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044159_reg_allocator_dyn_change_2_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 08/07/2015 Sarah Byers 		 	Populate REG_CASE_ALLOCATOR_DYN and REG_JUR_ALLOCATOR_DYN
||========================================================================================
*/

-- Copy records into REG_JUR_ALLOCATOR_DYN from REG_ALLOCATOR_DYN for all 
-- Jur Templates that have a dynamic allocator assigned
insert into reg_jur_allocator_dyn (
	reg_jur_template_id, reg_allocator_id, row_number, reg_acct_type_id, sub_acct_type_id, 
	reg_acct_id, reg_alloc_target_id, sign)
select c.reg_jur_template_id, a.reg_allocator_id, a.row_number, a.reg_acct_type_id, a.sub_acct_type_id, 
		 a.reg_acct_id, a.reg_alloc_target_id, a.sign
  from reg_allocator_dyn a, reg_jur_category_allocator c
 where a.reg_allocator_id = c.reg_allocator_id
	and a.reg_acct_id is null
union
select c.reg_jur_template_id, a.reg_allocator_id, a.row_number, j.reg_acct_type_id, j.sub_acct_type_id, 
		 a.reg_acct_id, a.reg_alloc_target_id, a.sign
  from reg_allocator_dyn a, reg_jur_category_allocator c, reg_jur_acct_default j
 where a.reg_allocator_id = c.reg_allocator_id
	and a.reg_acct_id is not null
	and c.reg_jur_template_id = j.reg_jur_template_id
	and a.reg_acct_id = j.reg_acct_id;

-- Copy records into REG_CASE_ALLOCATOR_DYN from REG_ALLOCATOR_DYN for all 
-- Cases that have a dynamic allocator assigned
insert into reg_case_allocator_dyn (
	reg_case_id, reg_allocator_id, row_number, reg_acct_type_id, sub_acct_type_id, 
	reg_acct_id, reg_alloc_target_id, sign)
select c.reg_case_id, a.reg_allocator_id, a.row_number, a.reg_acct_type_id, a.sub_acct_type_id, 
		 a.reg_acct_id, a.reg_alloc_target_id, a.sign
  from reg_allocator_dyn a, reg_case_category_allocator c
 where a.reg_allocator_id = c.reg_allocator_id
	and a.reg_acct_id is null
union
select c.reg_case_id, a.reg_allocator_id, a.row_number, j.reg_acct_type_id, j.sub_acct_type_id, 
		 a.reg_acct_id, a.reg_alloc_target_id, a.sign
  from reg_allocator_dyn a, reg_case_category_allocator c, reg_case_acct j
 where a.reg_allocator_id = c.reg_allocator_id
	and a.reg_acct_id is not null
	and c.reg_case_id = j.reg_case_id
	and a.reg_acct_id = j.reg_acct_id;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2793, 0, 2015, 2, 0, 0, 044159, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044159_reg_allocator_dyn_change_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;