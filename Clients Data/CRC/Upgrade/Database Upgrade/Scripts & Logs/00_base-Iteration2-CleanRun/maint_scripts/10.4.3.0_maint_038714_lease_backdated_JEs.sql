/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038714_lease_backdated_JEs.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 07/07/2014 Kyle Peterson
||========================================================================================
*/

merge into JE_TRANS_TYPE JTT
using (select '3042' as TRANS_TYPE, 'Lease Backdated Obligation Credit' as DESCRIPTION
         from DUAL) A
on (JTT.TRANS_TYPE = A.TRANS_TYPE)
when matched then
   update set JTT.DESCRIPTION = A.DESCRIPTION
when not matched then
   insert (TRANS_TYPE, DESCRIPTION) values (A.TRANS_TYPE, A.DESCRIPTION);

insert into JE_METHOD_TRANS_TYPE (JE_METHOD_ID, TRANS_TYPE) (select JE_METHOD_ID, 3042 from JE_METHOD A where not exists (select 1 from JE_METHOD_TRANS_TYPE B where A.JE_METHOD_ID = B.JE_METHOD_ID AND B.TRANS_TYPE = 3042));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1240, 0, 10, 4, 3, 0, 38714, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038714_lease_backdated_JEs.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
