/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051039_lessee_03_reset_pks_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/09/2018 Charlie Shilling need to populate SOB column on LS_ILR_PAYMENT_TERM_VAR_PAYMNT before we add it to the PK (which makes it NOT NULL)
||============================================================================
*/
ALTER TABLE ls_ilr_payment_term_var_paymnt
  ADD CONSTRAINT ls_ilr_pay_term_var_paymnt_pk PRIMARY KEY (
    ilr_id,
    revision,
    payment_term_id,
    bucket_number,
	set_of_books_id
  )
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5286, 0, 2017, 4, 0, 0, 51039, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051039_lessee_03_reset_pks_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;