/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042668_pcm_fcst_options_version_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/02/2015 Sarah Byers      New table to support sorting comparison BVs
||============================================================================
*/
-- New table
create table wo_est_fcst_options_version (
users varchar2(18) not null,
template_name varchar2(254) not null,
template_level varchar2(35) not null,
description varchar2(35) not null,
sort_order number(22,0) not null,
budget_version_id number(22,0),
user_id varchar2(18),
time_stamp date);

-- PK
alter table wo_est_fcst_options_version add (
constraint pk_wo_est_fcst_options_version primary key (users, template_name, template_level, description)
using index tablespace pwrplant_idx);

-- FKs
alter table wo_est_fcst_options_version
add constraint r_wo_est_fcst_options_version1
foreign key (users, template_name, template_level)
references wo_est_forecast_options (users, template_name, template_level);

alter table wo_est_fcst_options_version
add constraint r_wo_est_fcst_options_version2
foreign key (budget_version_id)
references budget_version (budget_version_id);

-- Comments
comment on table wo_est_fcst_options_version is '(O)  [13]The WO Est Forecast Options Versions table stores the comparison versions and sort order for the Funding Project forecasting window for a specific user.';
comment on column wo_est_fcst_options_version.users is 'System assigned identifier for the user assigned to the record.';
comment on column wo_est_fcst_options_version.template_name is 'The user-assigned name of the template used to identify which set of forecast options the user would like to use while updating project forecasts';
comment on column wo_est_fcst_options_version.template_level is 'Indicates the level to which this forecast template applies (FP/WO/BI/BV; that is, Funding Project, Work Order, Budget Item, Budget Version)';
comment on column wo_est_fcst_options_version.description is 'Description of the budget version, the First Approved Amount, or the Current Approved Amount.';
comment on column wo_est_fcst_options_version.sort_order is 'Sort order for the comparison budget versions that will be displayed on the estimate workspace.';
comment on column wo_est_fcst_options_version.budget_version_id is 'System assigned identifier for the budget version.';
comment on column wo_est_fcst_options_version.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column wo_est_fcst_options_version.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


COMMIT;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2245, 0, 2015, 1, 0, 0, 042668, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042668_pcm_fcst_options_version_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;