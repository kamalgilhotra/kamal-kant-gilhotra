/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030359_lease_init_wizards.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/28/2013 Kyle Peterson  Point release
||============================================================================
*/

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_lscntr_wksp_init'
 where WORKSPACE_IDENTIFIER = 'initiate_mla'
 and MODULE = 'LESSEE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (435, 0, 10, 4, 1, 0, 30359, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030359_lease_init_wizards.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

