/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044453_depr_add_prospective_1_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/30/2015 Daniel Motter  Creation
||============================================================================
*/

declare
    col_exists number;
begin
    select count(*) into col_exists from all_tab_cols where lower(table_name) = 'depreciation_method' and lower(column_name) = 'prospective';
    if col_exists = 0 then
        execute immediate 'alter table depreciation_method add prospective number(1) null';
        dbms_output.put_line('Added prospective column to depreciation_method table.');
    else
        dbms_output.put_line('Prospective column already exists on depreciation_method table.');
    end if;

    select count(*) into col_exists from all_tab_cols where lower(table_name) = 'fcst_depreciation_method' and lower(column_name) = 'prospective';
    if col_exists = 0 then
        execute immediate 'alter table fcst_depreciation_method add prospective number(1) null';
        dbms_output.put_line('Added prospective column to fcst_depreciation_method table.');
    else
        dbms_output.put_line('Prospective column already exists on fcst_depreciation_method table.');
    end if;
    
    select count(*) into col_exists from all_tab_cols where lower(table_name) = 'depr_ledger_blending_stg' and lower(column_name) = 'prospective';
    if col_exists = 0 then
        execute immediate 'alter table depr_ledger_blending_stg add prospective number(1) null';
        dbms_output.put_line('Added prospective column to depr_ledger_blending_stg table.');
    else
        dbms_output.put_line('Prospective column already exists on depr_ledger_blending_stg table.');
    end if;
    
    select count(*) into col_exists from all_tab_cols where lower(table_name) = 'depr_ledger_blending_stg_arc' and lower(column_name) = 'prospective';
    if col_exists = 0 then
        execute immediate 'alter table depr_ledger_blending_stg_arc add prospective number(1) null';
        dbms_output.put_line('Added prospective column to depr_ledger_blending_stg_arc table.');
    else
        dbms_output.put_line('Prospective column already exists on depr_ledger_blending_stg_arc table.');
    end if;
end;
/

comment on column depreciation_method.prospective is 'Indicates whether or not this depr method will be used for prospective analysis.';
comment on column fcst_depreciation_method.prospective is 'Indicates whether or not this fcst depr method will be used for prospective analysis.';
comment on column depr_ledger_blending_stg.prospective is 'Indicates whether or not this depr method will be used for prospective analysis.';
comment on column depr_ledger_blending_stg_arc.prospective is 'Indicates whether or not this depr method will be used for prospective analysis.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2734, 0, 2015, 2, 0, 0, 044453, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044453_depr_add_prospective_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;