/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030265_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/23/2013 Andrew Scott   Point Release
||============================================================================
*/

----  increase flex fields from 23 to 50
alter table PT_PARCEL_FLEX_FIELDS
   add (FLEX_24 number(22,8),
        FLEX_25 number(22,8),
        FLEX_26 number(22,8),
        FLEX_27 number(22,8),
        FLEX_28 number(22,8),
        FLEX_29 number(22,8),
        FLEX_30 number(22,8),
        FLEX_31 number(22,8),
        FLEX_32 number(22,8),
        FLEX_33 number(22,8),
        FLEX_34 date,
        FLEX_35 date,
        FLEX_36 date,
        FLEX_37 date,
        FLEX_38 date,
        FLEX_39 varchar2(254),
        FLEX_40 varchar2(254),
        FLEX_41 varchar2(254),
        FLEX_42 varchar2(254),
        FLEX_43 varchar2(254),
        FLEX_44 varchar2(254),
        FLEX_45 varchar2(254),
        FLEX_46 varchar2(254),
        FLEX_47 varchar2(254),
        FLEX_48 varchar2(254),
        FLEX_49 varchar2(254),
        FLEX_50 varchar2(254));

----  increase flex fields from 23 to 50
alter table PT_IMPORT_PARCEL_ARCHIVE
   add (FLEX_24 varchar2(35),
        FLEX_25 varchar2(35),
        FLEX_26 varchar2(35),
        FLEX_27 varchar2(35),
        FLEX_28 varchar2(35),
        FLEX_29 varchar2(35),
        FLEX_30 varchar2(35),
        FLEX_31 varchar2(35),
        FLEX_32 varchar2(35),
        FLEX_33 varchar2(35),
        FLEX_34 varchar2(35),
        FLEX_35 varchar2(35),
        FLEX_36 varchar2(35),
        FLEX_37 varchar2(35),
        FLEX_38 varchar2(35),
        FLEX_39 varchar2(254),
        FLEX_40 varchar2(254),
        FLEX_41 varchar2(254),
        FLEX_42 varchar2(254),
        FLEX_43 varchar2(254),
        FLEX_44 varchar2(254),
        FLEX_45 varchar2(254),
        FLEX_46 varchar2(254),
        FLEX_47 varchar2(254),
        FLEX_48 varchar2(254),
        FLEX_49 varchar2(254),
        FLEX_50 varchar2(254));

----  increase flex fields from 23 to 50
alter table PT_IMPORT_PARCEL
   add (FLEX_24 varchar2(35),
        FLEX_25 varchar2(35),
        FLEX_26 varchar2(35),
        FLEX_27 varchar2(35),
        FLEX_28 varchar2(35),
        FLEX_29 varchar2(35),
        FLEX_30 varchar2(35),
        FLEX_31 varchar2(35),
        FLEX_32 varchar2(35),
        FLEX_33 varchar2(35),
        FLEX_34 varchar2(35),
        FLEX_35 varchar2(35),
        FLEX_36 varchar2(35),
        FLEX_37 varchar2(35),
        FLEX_38 varchar2(35),
        FLEX_39 varchar2(254),
        FLEX_40 varchar2(254),
        FLEX_41 varchar2(254),
        FLEX_42 varchar2(254),
        FLEX_43 varchar2(254),
        FLEX_44 varchar2(254),
        FLEX_45 varchar2(254),
        FLEX_46 varchar2(254),
        FLEX_47 varchar2(254),
        FLEX_48 varchar2(254),
        FLEX_49 varchar2(254),
        FLEX_50 varchar2(254));

insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_24', 'Flex Field 24', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_25', 'Flex Field 25', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_26', 'Flex Field 26', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_27', 'Flex Field 27', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_28', 'Flex Field 28', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_29', 'Flex Field 29', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_30', 'Flex Field 30', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_31', 'Flex Field 31', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_32', 'Flex Field 32', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_33', 'Flex Field 33', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_34', 'Flex Field 34', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_35', 'Flex Field 35', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_36', 'Flex Field 36', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_37', 'Flex Field 37', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_38', 'Flex Field 38', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_39', 'Flex Field 39', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_40', 'Flex Field 40', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_41', 'Flex Field 41', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_42', 'Flex Field 42', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_43', 'Flex Field 43', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_44', 'Flex Field 44', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_45', 'Flex Field 45', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_46', 'Flex Field 46', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_47', 'Flex Field 47', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_48', 'Flex Field 48', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_49', 'Flex Field 49', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (6, 'flex_50', 'Flex Field 50', 0, 1, 'varchar2(254)', 1);


insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_24', 'Flex Field 24', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_25', 'Flex Field 25', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_26', 'Flex Field 26', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_27', 'Flex Field 27', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_28', 'Flex Field 28', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_29', 'Flex Field 29', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_30', 'Flex Field 30', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_31', 'Flex Field 31', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_32', 'Flex Field 32', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_33', 'Flex Field 33', 0, 1, 'number(22,8)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_34', 'Flex Field 34', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_35', 'Flex Field 35', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_36', 'Flex Field 36', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_37', 'Flex Field 37', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_38', 'Flex Field 38', 0, 1, 'date', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_39', 'Flex Field 39', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_40', 'Flex Field 40', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_41', 'Flex Field 41', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_42', 'Flex Field 42', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_43', 'Flex Field 43', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_44', 'Flex Field 44', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_45', 'Flex Field 45', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_46', 'Flex Field 46', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_47', 'Flex Field 47', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_48', 'Flex Field 48', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_49', 'Flex Field 49', 0, 1, 'varchar2(254)', 1);
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE) values (7, 'flex_50', 'Flex Field 50', 0, 1, 'varchar2(254)', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (488, 0, 10, 4, 1, 0, 30265, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030265_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
