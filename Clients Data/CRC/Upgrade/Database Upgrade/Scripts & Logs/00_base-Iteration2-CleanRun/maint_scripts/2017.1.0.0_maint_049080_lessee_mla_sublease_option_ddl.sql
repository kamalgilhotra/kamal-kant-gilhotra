/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049080_lessee_mla_sublease_option_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date     Revised By      Reason for Change
|| ---------- -------- --------------  ------------------------------------
|| 2017.1.0.0 9/25/2017 Shane "C" Ward   Add Sublease to Lessee Lease Options
||============================================================================
*/

ALTER TABLE ls_lease_options ADD sublease NUMBER(1,0);
ALTER TABLE ls_lease_options ADD sublease_lease_id NUMBER(22,0);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3740, 0, 2017, 1, 0, 0, 49080, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049080_lessee_mla_sublease_option_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
