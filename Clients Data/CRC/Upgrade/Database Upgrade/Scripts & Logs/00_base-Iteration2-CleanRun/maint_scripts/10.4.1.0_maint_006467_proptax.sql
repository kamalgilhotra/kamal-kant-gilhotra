/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_006467_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- -------------------------------------
|| 10.4.1.0    09/10/2012 Julia Breuer   Point Release
||============================================================================
*/

insert into PWRPLANT.PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Admin Center - Locations - Display Tax District on Asset Location', sysdate, user, 'Whether to display the tax district stored on the asset location in the Locations workspace.  This tax district is not used in Property Tax but it may be displayed for reference.  This system option is also used in the Returns Center Location Audit.', 0, 'No', null, 1 );

insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Display Tax District on Asset Location', 'No', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Display Tax District on Asset Location', 'Yes', sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (219, 0, 10, 4, 1, 0, 6467, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_006467_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
