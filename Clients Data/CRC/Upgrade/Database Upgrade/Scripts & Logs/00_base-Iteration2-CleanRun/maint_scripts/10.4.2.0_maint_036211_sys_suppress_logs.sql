/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036211_sys_suppress_logs.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/12/2014 Stephen Motter
||============================================================================
*/

alter table PP_PROCESSES_OCCURRENCES add SUPPRESS_INDICATOR number;

comment on column PP_PROCESSES_OCCURRENCES.SUPPRESS_INDICATOR is 'This column indicates whether the occurrence is displayed in the online logs window by default. 1 = do not show.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (961, 0, 10, 4, 2, 0, 36211, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036211_sys_suppress_logs.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;