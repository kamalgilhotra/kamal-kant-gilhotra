SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037610_lease_je_info.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 09/05/2014 Charlie Shilling need LEASE AUTO RETIRE in gl_je_control
||============================================================================
*/

declare
   V_JE_ID STANDARD_JOURNAL_ENTRIES.JE_ID%type;
begin
   begin
      select JE_ID
        into V_JE_ID
        from GL_JE_CONTROL
       where UPPER(trim(PROCESS_ID)) = 'LEASE AUTO RETIRE';

      DBMS_OUTPUT.PUT_LINE('Record already exists.');
   exception
      when NO_DATA_FOUND then
         select NVL(max(JE_ID), 0) + 1 into V_JE_ID from STANDARD_JOURNAL_ENTRIES;

         insert into STANDARD_JOURNAL_ENTRIES
            (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
         values
            (V_JE_ID, 'LEASEAUTORET', 'LEASEAUTORET', 'LAM Auto Retirement',
             'Leased Asset Automated Retirement');
         DBMS_OUTPUT.PUT_LINE('Row inserted into STANDARD_JOURNAL_ENTRIES.');

         insert into GL_JE_CONTROL
            (PROCESS_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN,
             CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
         values
            ('LEASE AUTO RETIRE', V_JE_ID, null, null, 'NONE', 'NONE', 'NONE', 'NONE', null, null,
             null);
         DBMS_OUTPUT.PUT_LINE('Row inserted into GL_JE_CONTROL.');
   end;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1100, 0, 10, 4, 2, 0, 37610, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037610_lease_je_info.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
