/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052459_lessee_02_impair_wksp_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/23/2018 K Powers       Impairment Workspace
||============================================================================
*/

-- Set up Lease Impairment Workspace
INSERT INTO PPBASE_WORKSPACE
(module,workspace_identifier,label,workspace_uo_name,minihelp,object_type_id)
values
('LESSEE','control_impairments','Lease Impairments','uo_ls_impair_wksp','Lease Impairment Workspace',1);

insert into ppbase_menu_items
(module,menu_identifier,menu_level,item_order,label,minihelp,parent_menu_identifier,workspace_identifier,enable_yn)
values
('LESSEE','control_impairments',2,6,'Lease Impairments','Lease Impairment Dashboard','menu_wksp_control','control_impairments',1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11086, 0, 2018, 1, 0, 0, 52459, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052459_lessee_02_impair_wksp_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;