/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050180_lessor_04_cleanup_lsr_ilr_rate_types_dml.sql
|| Description:	Add new rate types to rate type table
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/20/2018 Andrew Hill    Clean up ILR Rate Types
||============================================================================
*/

DELETE FROM
lsr_ilr_rates
WHERE rate_type_id NOT IN(SELECT rate_type_id
                          FROM lsr_ilr_rate_types
                          WHERE DESCRIPTION IN ('Sales Type Discount Rate Override',
                                                'Direct Finance Discount Rate Override',
                                                'Direct Finance Interest on Net Inv Rate Override',
                                                'Direct Finance Discount Rate',
                                                'Direct Finance FMV Comparison Rate Override',
                                                'Sales Type Discount Rate',
                                                'Direct Finance FMV Comparison Rate',
                                                'Direct Finance Interest on Net Inv Rate'));
                                                
DELETE FROM
lsr_ilr_rate_types
WHERE DESCRIPTION NOT IN ('Sales Type Discount Rate Override',
                          'Direct Finance Discount Rate Override',
                          'Direct Finance Interest on Net Inv Rate Override',
                          'Direct Finance Discount Rate',
                          'Direct Finance FMV Comparison Rate Override',
                          'Sales Type Discount Rate',
                          'Direct Finance FMV Comparison Rate',
                          'Direct Finance Interest on Net Inv Rate');
						  


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4222, 0, 2017, 3, 0, 0, 50180, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050180_lessor_04_cleanup_lsr_ilr_rate_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 