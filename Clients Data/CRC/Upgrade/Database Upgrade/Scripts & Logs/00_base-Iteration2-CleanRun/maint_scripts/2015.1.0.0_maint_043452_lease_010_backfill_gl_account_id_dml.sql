/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_043452_lease_010_backfill_gl_account_id_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1.0.0 04/08/2015 Charlie Shilling    backfill gl_account_id so we can make it NOT NULL
||==========================================================================================
*/

UPDATE ls_asset a 
SET a.gl_account_id = (
	SELECT ilra.cap_asset_account_id 
	FROM ls_ilr_account ilra 
	WHERE ilra.ilr_id = a.ilr_id)
WHERE a.gl_account_id IS NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2495, 0, 2015, 1, 0, 0, 043452, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043452_lease_010_backfill_gl_account_id_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;