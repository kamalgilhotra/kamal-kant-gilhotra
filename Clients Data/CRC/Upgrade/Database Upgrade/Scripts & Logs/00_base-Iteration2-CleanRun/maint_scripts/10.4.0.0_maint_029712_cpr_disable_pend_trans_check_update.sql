/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029712_cpr_disable_pend_trans_check_update.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   04/16/2013 Charlie Shilling
||============================================================================
*/

alter trigger PEND_TRANS_CHECK_UPDATE disable;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (347, 0, 10, 4, 0, 0, 29712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029712_cpr_disable_pend_trans_check_update.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;