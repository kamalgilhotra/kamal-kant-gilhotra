/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030056_sys_remove_edit_mask.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/07/2013 Stephen Motter Patch Release
||============================================================================
*/

update POWERPLANT_COLUMNS
   set EDIT_MASK = null
 where COLUMN_NAME = 'gl_je_code'
   and TABLE_NAME = 'standard_journal_entries';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (391, 0, 10, 4, 1, 0, 30056, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030056_sys_remove_edit_mask.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
