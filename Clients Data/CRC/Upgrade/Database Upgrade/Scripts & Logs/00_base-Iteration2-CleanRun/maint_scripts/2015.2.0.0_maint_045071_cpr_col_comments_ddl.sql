/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045071_cpr_col_comments_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 11/03/2015 Andrew Scott     modify column comments
||============================================================================
*/

comment on column PEND_TRANSACTION.ADJUSTED_COST_OF_REMOVAL is 'Records the user modified dollar amount of removal cost associated with a retirement transaction that is actually used when posting the transaction.';
comment on column PEND_TRANSACTION.ADJUSTED_SALVAGE_CASH is 'Records the user modified dollar amount of salvage cash associated with a retirement transaction that is actually used when posting the transaction.';
comment on column PEND_TRANSACTION.ADJUSTED_SALVAGE_RETURNS is 'Records the user modified dollar amount of salvage returns associated with a retirement transaction that is actually used when posting the transaction.';
comment on column PEND_TRANSACTION.ADJUSTED_RESERVE_CREDITS is 'Records the user modified dollar amount of reserve credits associated with a retirement transaction that is actually used when posting the transaction.';
comment on column PEND_TRANSACTION.ADJUSTED_RESERVE is 'Records the user modified dollar amount of depreciation reserve associated with a transaction that is actually used when posting the transaction.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2959, 0, 2015, 2, 0, 0, 45071, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045071_cpr_col_comments_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
   SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

