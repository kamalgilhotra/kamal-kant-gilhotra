/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050379_lessor_01_create_termination_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.4.0.0 05/25/2018 Jared Watkins     Create the new Transaction types we need for Lessor Terminations
||============================================================================
*/

/* Add new trans type 4040 - Deferred Initial Direct Costs Clearing (Loss) Debit */
insert into je_trans_type (trans_type, description)
values (4040, '4040 - Lessor Deferred Initial Direct Costs Clearing (Loss) Debit');

/* Add new trans type 4041 - Deferred Initial Direct Costs Credit */
insert into je_trans_type (trans_type, description)
values (4041, '4041 - Lessor Deferred Initial Direct Costs Credit');

/* Add new trans type 4042 - Deferred Rent Debit */
insert into je_trans_type (trans_type, description)
values (4042, '4042 - Lessor Deferred Rent Debit');

/* Add new trans type 4043 - Deferred Rent Clearing (Gain) Credit */
insert into je_trans_type (trans_type, description)
values (4043, '4043 - Lessor Deferred Rent Clearing (Gain) Credit');

/* Add new trans type 4044 - Accrued Rent Clearing (Loss) Debit */
insert into je_trans_type (trans_type, description)
values (4044, '4044 - Lessor Accrued Rent Clearing (Loss) Debit');

/* Add new trans type 4045 - Accrued Rent Credit */
insert into je_trans_type (trans_type, description)
values (4045, '4045 - Lessor Accrued Rent Credit');

/* Add new trans type 4046 - Termination Penalty Costs Debit */
insert into je_trans_type (trans_type, description)
values (4046, '4046 - Lessor Termination Penalty Costs Debit');

/* Add new trans type 4047 - Termination Penalty Clearing (Gain) Credit */
insert into je_trans_type (trans_type, description)
values (4047, '4047 - Lessor Termination Penalty Clearing (Gain) Credit');

/* Relate the new trans types to the delivered JE Method */
insert into je_method_trans_type(je_method_id, trans_type)
select 1, trans_type from je_trans_type 
where trans_type between 4040 and 4047;

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6142, 0, 2017, 4, 0, 0, 50379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050379_lessor_01_create_termination_trans_types_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;