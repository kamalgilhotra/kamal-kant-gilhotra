/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009600_sys_system_controls.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/26/2012 Sunjin Cone    Point Release
||============================================================================
*/

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'Yes'
 where UPPER(CONTROL_NAME) in ('POST BALANCE CWIP',
                               'POST BALANCE CWIP 106',
                               'POST BALANCE CWIP 107/106',
                               'POST BALANCE CWIP 106/101');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (123, 0, 10, 3, 4, 0, 9600, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009600_sys_system_controls.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
