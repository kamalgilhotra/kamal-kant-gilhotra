/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009818_quantity.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.1   05/08/2012 Sunjin Cone    Point Release
||============================================================================
*/

alter table REPAIR_WORK_ORDER_SEGMENTS  add EXPENSE_PERCENT number(22,2);
alter table REPAIR_WO_SEG_REPORTING     add EXPENSE_PERCENT number(22,2);

alter table REPAIR_WORK_ORDER_SEGMENTS  add REPLACEMENT_QUANTITY number(22,2);
alter table REPAIR_WO_SEG_REPORTING     add REPLACEMENT_QUANTITY number(22,2);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (131, 0, 10, 3, 4, 1, 9818, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.1_maint_009818_quantity.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
