SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032563_taxrpr_mypp_shortcuts.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/02/2013 Alex P.        Point Release
||============================================================================
*/

declare

   MENU_LEVELS integer;
   IDX         integer;

   -- Set the variables below for your module
   MODULE_NAME        varchar2(35) := 'REPAIRS'; -- Same as MODULE in ppbase_menu_items
   MODULE_DESCRIPTION varchar2(35) := 'Tax Repairs';
   WINDOW_NAME        varchar2(35) := 'w_rpr_center_main';
   WINDOW_DESCRIPTION varchar2(254) := 'Main Tax Repairs window';
   WINDOW_PATH        varchar2(254) := 'm_top_frame.m_application.m_taxrepairs';
   -- If more than one reporting window is used create 3 more variables like the ones below and create another insert into pp_mypp_report_type_task
   REPORT_WKSP_IDENTIFIER     varchar2(35) := 'menu_wksp_reports';
   LOWER_REPORT_TYPE_ID number(22, 0) := 100; -- Lower bound of the report type id range for the specified module
   UPPER_REPORT_TYPE_ID number(22, 0) := 104; -- Upper bound of the report type id range for the specified module

begin
   --------------------- Use to clean up data, if needed --------------------------
   delete from PP_MYPP_REPORT_TYPE_TASK
    where TASK_ID in
          (select TASK_ID
             from PP_MYPP_TASK
            where LONG_DESCRIPTION like
                  LOWER('%( ' || MODULE_NAME || ' > ' || REPORT_WKSP_IDENTIFIER || ' )%'));
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows deleted from PP_MYPP_REPORT_TYPE_TASK.');

   delete from PP_MYPP_TASK_STEP
    where TASK_ID in
          (select TASK_ID
             from PP_MYPP_TASK
            where LONG_DESCRIPTION like LOWER('%( ' || MODULE_NAME || ' > %'));
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows deleted from PP_MYPP_TASK_STEP.');

   delete from PP_MYPP_USER_TASK
    where TASK_ID in
          (select TASK_ID
             from PP_MYPP_TASK
            where LONG_DESCRIPTION like LOWER('%( ' || MODULE_NAME || ' > %'));
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows deleted from PP_MYPP_USER_TASK.');

   delete from PP_MYPP_TASK where LONG_DESCRIPTION like LOWER('%( ' || MODULE_NAME || ' > %');
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows deleted from PP_MYPP_TASK.');

   delete from PP_MYPP_TASK_STEP
    where TASK_ID in (select TASK_ID from PP_MYPP_TASK where LONG_DESCRIPTION = WINDOW_DESCRIPTION);
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows deleted from PP_MYPP_TASK_STEP.');

   delete from PP_MYPP_USER_TASK
    where TASK_ID in (select TASK_ID from PP_MYPP_TASK where LONG_DESCRIPTION = WINDOW_DESCRIPTION);
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows deleted from PP_MYPP_USER_TASK.');

   delete from PP_MYPP_TASK where LONG_DESCRIPTION = WINDOW_DESCRIPTION;
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows deleted from PP_MYPP_TASK.');

   -- Set menu level to the number of levels in the menu tree you'd like to loop through
   select max(MENU_LEVEL) into MENU_LEVELS from PPBASE_MENU_ITEMS where MODULE = MODULE_NAME;

   ------------------------- Parent Item -------------------------

   insert into PP_MYPP_TASK
      (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
       ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
      (select max(TASK_ID) + 1,
              null,
              null,
              MODULE_DESCRIPTION,
              WINDOW_DESCRIPTION,
              null,
              0,
              null,
              null
         from PP_MYPP_TASK);
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows inserted into PP_MYPP_TASK.');

   insert into PP_MYPP_TASK_STEP
      (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
      (select max(TASK_ID), 1, null, null, 'w_top_frame', WINDOW_PATH, 'clicked', null
         from PP_MYPP_TASK);
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows inserted into PP_MYPP_TASK_STEP.');

   ------------------------- Level 1 of the menu -------------------------

   insert into PP_MYPP_TASK
      (TASK_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID, ALLOW_AS_SHORTCUT_YN)
      (select MYPP_TASK.MAX_ID + ROWNUM,
              LABEL,
              MINIHELP || LOWER(' ( ' || MODULE || ' > ' || MENU_IDENTIFIER || ' ) '),
              MYPP_TASK.MAX_ID,
              NVL2(WORKSPACE_IDENTIFIER, 1, 0)
         from PPBASE_MENU_ITEMS, (select max(TASK_ID) MAX_ID from PP_MYPP_TASK) MYPP_TASK
        where MENU_LEVEL = 1
          and MODULE = MODULE_NAME);
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows inserted into PP_MYPP_TASK.');

   insert into PP_MYPP_TASK_STEP
      (TASK_ID, STEP_ID, WINDOW, PATH, EVENT)
      (select TASK_ID, 1, WINDOW_NAME, WORKSPACE_IDENTIFIER, 'ppbaseuo'
         from PPBASE_MENU_ITEMS, PP_MYPP_TASK
        where WORKSPACE_IDENTIFIER is not null
          and MODULE = MODULE_NAME
          and MENU_LEVEL = 1
          and MINIHELP || LOWER(' ( ' || MODULE || ' > ' || MENU_IDENTIFIER || ' ) ') =
              PP_MYPP_TASK.LONG_DESCRIPTION);
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows inserted into PP_MYPP_TASK_STEP.');

   ------------------------- Level 2+ of the menu -------------------------

   for IDX in 2 .. MENU_LEVELS
   loop

      insert into PP_MYPP_TASK
         (TASK_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID, ALLOW_AS_SHORTCUT_YN)
         (select MYPP_TASK.MAX_ID + ROWNUM,
                 CHILD_MENU_ITEMS.LABEL,
                 CHILD_MENU_ITEMS.MINIHELP ||
                 LOWER(' ( ' || CHILD_MENU_ITEMS.MODULE || ' > ' ||
                       CHILD_MENU_ITEMS.MENU_IDENTIFIER || ' ) '),
                 PP_MYPP_TASK.TASK_ID,
                 NVL2(CHILD_MENU_ITEMS.WORKSPACE_IDENTIFIER, 1, 0)
            from PPBASE_MENU_ITEMS CHILD_MENU_ITEMS,
                 PPBASE_MENU_ITEMS PARENT_MENU_ITEMS,
                 PP_MYPP_TASK,
                 (select max(TASK_ID) MAX_ID from PP_MYPP_TASK) MYPP_TASK
           where CHILD_MENU_ITEMS.MENU_LEVEL = IDX
             and CHILD_MENU_ITEMS.PARENT_MENU_IDENTIFIER = PARENT_MENU_ITEMS.MENU_IDENTIFIER
             and PP_MYPP_TASK.LONG_DESCRIPTION =
                 PARENT_MENU_ITEMS.MINIHELP ||
                 LOWER(' ( ' || PARENT_MENU_ITEMS.MODULE || ' > ' ||
                       PARENT_MENU_ITEMS.MENU_IDENTIFIER || ' ) ')
             and CHILD_MENU_ITEMS.MODULE = MODULE_NAME
             and CHILD_MENU_ITEMS.MODULE = PARENT_MENU_ITEMS.MODULE);
      DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows inserted into PP_MYPP_TASK for index: ' || IDX);

      insert into PP_MYPP_TASK_STEP
         (TASK_ID, STEP_ID, WINDOW, PATH, EVENT)
         (select TASK_ID, 1, WINDOW_NAME, WORKSPACE_IDENTIFIER, 'ppbaseuo'
            from PPBASE_MENU_ITEMS, PP_MYPP_TASK
           where WORKSPACE_IDENTIFIER is not null
             and MODULE = MODULE_NAME
             and MENU_LEVEL = IDX
             and MINIHELP || LOWER(' ( ' || MODULE || ' > ' || MENU_IDENTIFIER || ' ) ') =
                 PP_MYPP_TASK.LONG_DESCRIPTION);
      DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows inserted into PP_MYPP_TASK_STEP for index: ' || IDX);

   end loop;

   insert into PP_MYPP_REPORT_TYPE_TASK
      (REPORT_TYPE_ID, TASK_ID)
      (select PP_REPORT_TYPE.REPORT_TYPE_ID, PP_MYPP_TASK.TASK_ID
         from PP_REPORT_TYPE, PP_MYPP_TASK
        where PP_REPORT_TYPE.REPORT_TYPE_ID between LOWER_REPORT_TYPE_ID and UPPER_REPORT_TYPE_ID
          and PP_MYPP_TASK.LONG_DESCRIPTION like
              LOWER('%( ' || MODULE_NAME || ' > ' || REPORT_WKSP_IDENTIFIER || ' )%'));
   DBMS_OUTPUT.PUT_LINE(sql%rowcount || ' rows inserted into PP_MYPP_REPORT_TYPE_TASK.');
   commit;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (671, 0, 10, 4, 2, 0, 32563, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032563_taxrpr_mypp_shortcuts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
