/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049206_lessor_03_cpr_asset_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/01/2017 Shane "C" Ward		Set up Lessor CPR Asset Default Import Template
||============================================================================
*/

--Update import type description
UPDATE pp_import_type SET description = 'Add: PowerPlan Lessor Asset to ILR'
WHERE import_type_id = 503;

--Default Template
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
VALUES      (( SELECT Max(import_template_id) + 1
               FROM   PP_IMPORT_TEMPLATE ),
             503,
             'Add: PowerPlan Lessor Assets to ILR',
             'Default Template for Adding CPR Assets to ILR',
             1,
             0);
			 
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             1,
             503,
             'ilr_id',
             1090);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             2,
             503,
             'revision',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             3,
             503,
             'cpr_asset_id',
             1093);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             4,
             503,
             'fair_market_value',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             5,
             503,
             'guaranteed_residual',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             6,
             503,
             'estimated_residual',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             7,
             503,
             'estimated_residual_pct',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             8,
             503,
             'expected_life',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 503 AND
                      description = 'Add: PowerPlan Lessor Assets to ILR' ),
             9,
             503,
             'economic_life',
             NULL); 
			 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3853, 0, 2017, 1, 0, 0, 49206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049206_lessor_03_cpr_asset_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;