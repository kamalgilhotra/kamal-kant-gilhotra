/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041510_sys_multi_thread_fix.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1   12/01/2014 Andrew Scott   multi threading issue found and fixed for maint 41510.
||                                    additional change was to add a check box to report
||                                    maintenance to turn off multi-threading entirely.
||============================================================================
*/

alter table pp_reports
add turn_off_multi_thread number(22,0);

comment on column pp_reports.turn_off_multi_thread is 'Check box in report maint to turn off muli-threading, which is how reports are run by default for modules built on the base gui framework.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2076, 0, 2015, 1, 0, 0, 41510, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041510_sys_multi_thread_fix.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;