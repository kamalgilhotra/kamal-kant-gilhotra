/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044883_pwrtax_fix_arc_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2   09/03/2015 Betsy Calender Needed to Fix Restore of Arc cases
 ||============================================================================
 */ 

Update arc_tax_Record_control 
set asset_id = 0
where asset_id is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2846, 0, 2015, 2, 0, 0, 044883, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044883_pwrtax_fix_arc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;