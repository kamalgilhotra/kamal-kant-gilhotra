/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046793_depr_reg_layer_indexes_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 11/28/2016 Sarah Byers    Add indexes on FK columns to prevent deadlock in reg layer process
 ||============================================================================
 */ 

create index fcst_dli_fk_ix1 on fcst_depr_ledger_inc (fcst_depr_group_id, fcst_depr_version_id) tablespace pwrplant_idx;
create index fcst_dli_fk_ix2 on fcst_depr_ledger_inc (fcst_depr_version_id, set_of_books_id) tablespace pwrplant_idx;
create index fcst_cdi_fk_ix1 on fcst_cpr_depr_inc (fcst_depr_version_id, set_of_books_id) tablespace pwrplant_idx;
create index fcst_cdi_fk_ix2 on fcst_cpr_depr_inc (fcst_depr_group_id, fcst_depr_version_id) tablespace pwrplant_idx;
create index fcst_dmr_fk_ix2 on fcst_depr_method_rates (fcst_depr_version_id, set_of_books_id) tablespace pwrplant_idx;
create index fcst_uop_fk_ix1 on fcst_unit_of_production (fcst_depr_group_id, fcst_depr_version_id) tablespace pwrplant_idx;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3348, 0, 2016, 1, 0, 0, 046793, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046793_depr_reg_layer_indexes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;