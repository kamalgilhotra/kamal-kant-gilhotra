/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045587_reg_1_ta_family_member_map_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.2.0 04/13/2016 Sarah Byers  	 Added GL Account
||============================================================================
*/

alter table reg_ta_family_member_map add (gl_account varchar2(35));

comment on column reg_ta_family_member_map.gl_account is 'Records the general ledger account values associated with the Reg Family Member.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3147, 0, 2015, 2, 2, 0, 45587, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045587_reg_1_ta_family_member_map_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;