/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052901_lessee_audits_je_method_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.1.0.2 12/21/2018 Daniel Mendel    Fix JE Method Audits
||============================================================================
*/

UPDATE powerplant_dddw SET dropdown_name = 'je_method', table_name = 'je_method' WHERE dropdown_name = 'ls_je_method';

UPDATE pp_table_audits SET dropdown_name = 'je_method' WHERE dropdown_name = 'ls_je_method';

UPDATE pp_table_audits_pk_lookup SET display_table_name = 'je_method' WHERE display_table_name = 'ls_je_method';

--**************************
-- Log the run of the script
--**************************
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (14585, 0, 2018, 1, 0, 2, 52901, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_052901_lessee_audits_je_method_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;