/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009944_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/18/2012 Julia Breuer   Point Release
||============================================================================
*/

insert into pwrplant.ptc_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option ) values ( 'Bills Center - Bill Entry - Installment for Positive Rounding Differences', sysdate, user, 'This option indicates where any positive rounding difference should be placed when spreading an entered amount to installments.', 0, 'Earliest Unpaid Installment', null, 1 );
insert into pwrplant.ptc_system_options ( system_option_id, time_stamp, user_id, long_description, system_only, pp_default_value, option_value, is_base_option ) values ( 'Bills Center - Bill Entry - Installment for Negative Rounding Differences', sysdate, user, 'This option indicates where any negative rounding difference should be placed when spreading an entered amount to installments.', 0, 'Latest Unpaid Installment', null, 1 );

insert into pwrplant.ptc_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Bills Center - Bill Entry - Installment for Positive Rounding Differences', 'Earliest Unpaid Installment', sysdate, user );
insert into pwrplant.ptc_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Bills Center - Bill Entry - Installment for Positive Rounding Differences', 'Latest Unpaid Installment', sysdate, user );
insert into pwrplant.ptc_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Bills Center - Bill Entry - Installment for Negative Rounding Differences', 'Earliest Unpaid Installment', sysdate, user );
insert into pwrplant.ptc_system_options_values ( system_option_id, option_value, time_stamp, user_id ) values ( 'Bills Center - Bill Entry - Installment for Negative Rounding Differences', 'Latest Unpaid Installment', sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (223, 0, 10, 4, 1, 0, 9944, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009944_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
