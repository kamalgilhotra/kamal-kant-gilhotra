/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045363_lease_add_npv_start_date_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 02/10/2016 Sarah Byers	 Add NPV_START_DATE to LS_ILR_STG
|| 2015.2.2.0 04/14/2016 David Haupt	 Backporting to 2015.2.2
||============================================================================
*/

alter table ls_ilr_stg add (npv_start_date date);

comment on column ls_ilr_stg.npv_start_date is 'The month in which net present value calculations begin for lease schedules';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3053, 0, 2015, 2, 2, 0, 045363, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045363_lease_add_npv_start_date_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;