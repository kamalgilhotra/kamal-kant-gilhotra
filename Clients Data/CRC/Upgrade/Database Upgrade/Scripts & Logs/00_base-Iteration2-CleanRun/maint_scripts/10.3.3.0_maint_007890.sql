/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007890.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/25/2011 Lee Quinn      Point Release
||============================================================================
*/

-- Maint 7890
insert into REIMB_BILLING_TYPE (BILLING_TYPE_ID, DESCRIPTION) values (5, 'Scheduled Amounts');

create table REIMB_CUSTOMER_FIX as
   select CUSTOMER_ID, TAX_ID from REIMB_CUSTOMER;

update REIMB_CUSTOMER set TAX_ID = null;

alter table REIMB_CUSTOMER
   modify TAX_ID varchar2(35);

update REIMB_CUSTOMER RC
   set TAX_ID = (select B.TAX_ID from REIMB_CUSTOMER_FIX B where RC.CUSTOMER_ID = B.CUSTOMER_ID);

drop table REIMB_CUSTOMER_FIX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (18, 0, 10, 3, 3, 0, 7890, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007890.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
