/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010506_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   10/09/2012 Alex Pivoshenko PPBASE objects
||============================================================================
*/

-- PPBASE_SYSTEM_OPTIONS
create table PPBASE_SYSTEM_OPTIONS
(
 SYSTEM_OPTION_ID varchar2(254) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 LONG_DESCRIPTION varchar2(4000) not null,
 SYSTEM_ONLY      number(22,0) not null,
 PP_DEFAULT_VALUE varchar2(254) not null,
 OPTION_VALUE     varchar2(254),
 IS_BASE_OPTION   number(22,0) not null
);

alter table PPBASE_SYSTEM_OPTIONS
   add constraint PK_PPBASE_SYSTEM_OPTIONS
       primary key (SYSTEM_OPTION_ID, SYSTEM_ONLY)
       using index tablespace PWRPLANT_IDX;

-- PPBASE_USER_OPTIONS
create table PPBASE_USER_OPTIONS
(
 OBJECT            varchar2(1000) not null,
 OPTION_IDENTIFIER varchar2(254) not null,
 USERS             varchar2(18) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 USER_OPTION       varchar2(4000),
 USER_OPTION_B     varchar2(4000),
 USER_OPTION_C     varchar2(4000),
 USER_OPTION_D     varchar2(4000)
);

alter table PPBASE_USER_OPTIONS
   add constraint PK_PPBASE_USER_OPTIONS
       primary key (OBJECT, OPTION_IDENTIFIER, USERS)
       using index tablespace PWRPLANT_IDX;

drop index PP_REQ_TAB_COL_NDX;

-- ADD COLUMNS TO PP_REQUIRED_TABLE_COLUMN to enhance required field functionality
alter table PP_REQUIRED_TABLE_COLUMN
   add (OBJECTPATH  varchar2(512),
        DESCRIPTION varchar2(35));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (299, 0, 10, 4, 0, 0, 10506, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010506_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
