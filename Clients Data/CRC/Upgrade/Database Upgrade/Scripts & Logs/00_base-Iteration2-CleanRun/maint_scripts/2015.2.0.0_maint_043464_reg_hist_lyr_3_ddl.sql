/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043464_reg_hist_lyr_3_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/03/2015 Andrew Scott   rename 2 fp tables and rebuild their pkeys.
||                                      drop the old log table.
||============================================================================
*/

SET SERVEROUTPUT ON

ALTER TABLE FCST_DEPR_LEDGER_FP
RENAME TO FCST_DEPR_LEDGER_INC;

ALTER TABLE FCST_CPR_DEPR_FP
RENAME TO FCST_CPR_DEPR_INC;

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from FCST_DEPR_LEDGER_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'FCST_DEPR_LEDGER_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table FCST_DEPR_LEDGER_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from FCST_CPR_DEPR_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'FCST_CPR_DEPR_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table FCST_CPR_DEPR_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/

alter table FCST_DEPR_LEDGER_INC
  add constraint FCST_DEPR_LEDGER_INC_PK primary key (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, FCST_DEPR_VERSION_ID, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table FCST_CPR_DEPR_INC
  add constraint FCST_CPR_DEPR_INC_PK primary key (FCST_DEPR_VERSION_ID, ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;

drop table INCREMENTAL_FP_RUN_LOG;

alter table FCST_CPR_DEPR_INC drop column FUNDING_WO_ID;
alter table FCST_CPR_DEPR_INC drop column REVISION;
alter table FCST_DEPR_LEDGER_INC drop column FUNDING_WO_ID;
alter table FCST_DEPR_LEDGER_INC drop column REVISION;




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2584, 0, 2015, 2, 0, 0, 043464, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043464_reg_hist_lyr_3_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;