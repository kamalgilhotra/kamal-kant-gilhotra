/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051061_lessor_01_create_accrued_deferred_rent_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.4.0.0 05/24/2018 Jared Watkins     Create the new Transaction/Accrual types we need for Accrued/Deferred Rent JEs
||============================================================================
*/

/* Add new trans type 4034 - Monthly Accrued Rent Debit */
insert into je_trans_type (trans_type, description)
values (4034, '4034 - Lessor Monthly Accrued Rent Debit');

/* Add new trans type 4035 - Monthly Accrued Rent Credit */
insert into je_trans_type (trans_type, description)
values (4035, '4035 - Lessor Monthly Accrued Rent Credit');

/* Add new trans type 4036 - Monthly Deferred Rent Debit */
insert into je_trans_type (trans_type, description)
values (4036, '4036 - Lessor Monthly Deferred Rent Debit');

/* Add new trans type 4037 - Monthly Deferred Rent Credit */
insert into je_trans_type (trans_type, description)
values (4037, '4037 - Lessor Monthly Deferred Rent Credit');

/* Relate the new trans types to the delivered JE Method */
insert into je_method_trans_type(je_method_id, trans_type)
select 1, trans_type from je_trans_type 
where trans_type between 4034 and 4037;

/* Add new accrual types for Deferred Rent and Accrued Rent */
/* For some dumb reason the accrual types for Lessor are mapped off of the Payment Type table */
insert into ls_payment_type(payment_type_id, description)
select 29, 'Deferred Rent' from dual
union
select 30, 'Accrued Rent' from dual;

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5903, 0, 2017, 4, 0, 0, 51061, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051061_lessor_01_create_accrued_deferred_rent_trans_types_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;