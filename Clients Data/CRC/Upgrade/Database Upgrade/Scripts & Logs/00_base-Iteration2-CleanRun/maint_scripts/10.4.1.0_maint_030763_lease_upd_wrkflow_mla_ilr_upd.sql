/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030763_lease_upd_wrkflow_mla_ilr_upd.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/15/2013 Matthew Mikulka Point Release
||============================================================================
*/

alter table LS_ILR_SCHEDULE rename column LS_ILR_ID to ILR_ID;

--Update MLA Approval workflow type
update WORKFLOW_TYPE
  set SQL_APPROVAL_AMOUNT = 'select nvl(sum(max_lease_line),0) from LS_LEASE_COMPANY where lease_id = <<id_field1>>'
 where SUBSYSTEM = 'mla_approval';

--Update ILR Approval sql_approval_amount SQL
update WORKFLOW_TYPE
   set SQL_APPROVAL_AMOUNT = 'select nvl(sum(beg_obligation),0) from ls_ilr_schedule where ilr_id = <<id_field1>> and revision = <<id_field2>> and month = (select min(month) from ls_ilr_schedule where ilr_id = <<id_field1>> and revision = <<id_field2>>)'
 where SUBSYSTEM = 'ilr_approval';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (477, 0, 10, 4, 1, 0, 30763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030763_lease_upd_wrkflow_mla_ilr_upd.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
