/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_034896_taxrpr_tax_reconcile_rpt.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 06/10/2014 Alex P.
||========================================================================================
*/

insert into PP_REPORTS_TIME_OPTION
   (PP_REPORT_TIME_OPTION_ID, DESCRIPTION, PARAMETER_UO_NAME, DWNAME1, LABEL1)
values
   (44, 'RPR - Tax Year Add Expense', 'uo_ppbase_report_parms_dddw_one', 'dddw_rpr_tax_year_add_expense', 'Tax Year');

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION,
    REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID,
    PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Tax Repairs - Tax Reconciliation',
          'Reconciles additions and repair amounts from Tax Repirs module to additions and repair amounts from Powertax (Report 30). Runs for the selected Tax/Fiscal Year using powertax look-back and look-forward periods.',
          null,
          'dw_rpr_rpt_asst_tax_reconcile',
          'REPAIRS',
          null,
          null,
          'RPR - 0072',
          null,
          null,
          null,
          13,
          100,
          44,
          37,
          1,
          3,
          0
     from PP_REPORTS;

alter table REPAIR_PROCESSING_PERIOD add SHORT_DESCRIPTION varchar2(35);
comment on column REPAIR_PROCESSING_PERIOD.SHORT_DESCRIPTION is 'Short description of a given processing period. Can be used where space is limited.';

update REPAIR_PROCESSING_PERIOD set SHORT_DESCRIPTION = 'Range' where PROCESSING_PERIOD_ID = 1;

update REPAIR_PROCESSING_PERIOD set SHORT_DESCRIPTION = 'LTD' where PROCESSING_PERIOD_ID = 2;

alter table REPAIR_PROCESSING_PERIOD modify SHORT_DESCRIPTION not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1180, 0, 10, 4, 3, 0, 34896, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_034896_taxrpr_tax_reconcile_rpt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
