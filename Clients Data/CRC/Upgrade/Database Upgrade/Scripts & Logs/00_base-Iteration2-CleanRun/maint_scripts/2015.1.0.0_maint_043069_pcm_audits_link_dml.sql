/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043069_pcm_audits_link_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/10/2015 Ryan Oliveria    Link to Audits window from search
||============================================================================
*/

insert into PPBASE_WORKSPACE_LINKS
	(MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WINDOW, LINKED_WINDOW_LABEL, LINKED_WINDOW_OPENSHEET)
values
	('pcm', 'fp_search', 5, 'w_trig_history_display', 'Audits', 1);

insert into PPBASE_WORKSPACE_LINKS
	(MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER, LINKED_WINDOW, LINKED_WINDOW_LABEL, LINKED_WINDOW_OPENSHEET)
values
	('pcm', 'wo_search', 5, 'w_trig_history_display', 'Audits', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2369, 0, 2015, 1, 0, 0, 043069, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043069_pcm_audits_link_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;