/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_04_insert_ls_accrual_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/30/2018 Josh Sandler 	  Add Lessee Accrual types
||============================================================================
*/

INSERT INTO ls_accrual_type (accrual_type_id, description)
SELECT 2, 'Interest/Rent Accrual' FROM dual
UNION ALL
SELECT 3, 'Executory 1 Accrual' FROM dual
UNION ALL
SELECT 4, 'Executory 2 Accrual' FROM dual
UNION ALL
SELECT 5, 'Executory 3 Accrual' FROM dual
UNION ALL
SELECT 6, 'Executory 4 Accrual' FROM dual
UNION ALL
SELECT 7, 'Executory 5 Accrual' FROM dual
UNION ALL
SELECT 8, 'Executory 6 Accrual' FROM dual
UNION ALL
SELECT 9, 'Executory 7 Accrual' FROM dual
UNION ALL
SELECT 10, 'Executory 8 Accrual' FROM dual
UNION ALL
SELECT 11, 'Executory 9 Accrual' FROM dual
UNION ALL
SELECT 12, 'Executory 10 Accrual' FROM dual
UNION ALL
SELECT 13, 'Contingent 1 Accrual' FROM dual
UNION ALL
SELECT 14, 'Contingent 2 Accrual' FROM dual
UNION ALL
SELECT 15, 'Contingent 3 Accrual' FROM dual
UNION ALL
SELECT 16, 'Contingent 4 Accrual' FROM dual
UNION ALL
SELECT 17, 'Contingent 5 Accrual' FROM dual
UNION ALL
SELECT 18, 'Contingent 6 Accrual' FROM dual
UNION ALL
SELECT 19, 'Contingent 7 Accrual' FROM dual
UNION ALL
SELECT 20, 'Contingent 8 Accrual' FROM dual
UNION ALL
SELECT 21, 'Contingent 9 Accrual' FROM dual
UNION ALL
SELECT 22, 'Contingent 10 Accrual' FROM dual
UNION ALL
SELECT 23, 'ST Deferred Rent Accrual' FROM dual
UNION ALL
SELECT 24, 'LT Deferred Rent Accrual' FROM dual
UNION ALL
SELECT 25, 'Prepaid Rent Accrual' FROM dual
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(4300, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_04_insert_ls_accrual_type_dml.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
