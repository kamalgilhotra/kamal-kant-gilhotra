/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046763_taxrpr_02_companies_dml.sql.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 10/10/2017 Eric Berger    DML multi-batch reporting.
||============================================================================
*/

--if there are batches for the company, turn on the flag
UPDATE company_setup o
   SET is_tax_repairs_company = 1
 WHERE EXISTS (SELECT company_id
          FROM repair_batch_control i
         WHERE o.company_id = i.company_id);

--update dynamic filters to only show the in-scope companies in tax repairs
INSERT INTO pp_dynamic_filter
  (filter_id, label, input_type, sqls_column_expression, dw, dw_id,
   dw_description, dw_id_datatype)
  SELECT 286, 'Company', 'dw',
         'company.company_id', 'dw_rpr_company_filter', 'company_id',
         'description', 'N'
    FROM dual
   WHERE NOT EXISTS
   (SELECT 1 FROM pp_dynamic_filter WHERE dw = 'dw_rpr_company_filter');

UPDATE pp_dynamic_filter_mapping
   SET filter_id =
       (SELECT Max(filter_id)
          FROM pp_dynamic_filter
         WHERE dw = 'dw_rpr_company_filter')
 WHERE pp_report_filter_id BETWEEN 30 AND 38
   AND filter_id IN (3, 24, 49, 51);

--create the menu item for the company configuration window
INSERT INTO ppbase_workspace
  (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP,
   OBJECT_TYPE_ID)
  SELECT 'REPAIRS', 'menu_wksp_tax_yr', 'Tax Year Configuration',
         'uo_rpr_config_wksp_tax_yr', 'Tax Year Configuration', 1
    FROM dual
   WHERE NOT EXISTS
   (SELECT 1
            FROM ppbase_workspace
           WHERE Lower(MODULE) = 'repairs'
             AND Lower(workspace_identifier) = 'menu_wksp_tax_yr');

--create the menu item for the company configuration window
INSERT INTO ppbase_workspace
  (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP,
   OBJECT_TYPE_ID)
  SELECT 'REPAIRS', 'menu_wksp_tax_rpr_companies', 'Company Configuration',
         'uo_rpr_config_wksp_company', 'Repair Company Configuration', 1
    FROM dual
   WHERE NOT EXISTS
   (SELECT 1
            FROM ppbase_workspace
           WHERE LOWER(MODULE) = 'repairs'
             AND LOWER(workspace_identifier) = 'menu_wksp_tax_rpr_companies');

--fix the tr menu structure
MERGE INTO ppbase_menu_items a
USING (SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_tax_exp_tests' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
              4 AS ITEM_ORDER, 'Repair Tests' AS LABEL,
              'Repair Test Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_exp_tests' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE, 'menu_wksp_import' AS MENU_IDENTIFIER,
              2 AS MENU_LEVEL, 12 AS ITEM_ORDER, 'Import' AS LABEL,
              'Import Center' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_import' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_book_summary' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
              2 AS ITEM_ORDER, 'Book Summary' AS LABEL,
              'Repair Book Summary Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_book_summary' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_repair_schema' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
              3 AS ITEM_ORDER, 'Repair Schema' AS LABEL,
              'Repair Schema Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_repair_schema' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_tax_locations' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
              5 AS ITEM_ORDER, 'Repair Locations' AS LABEL,
              'Repair Location Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_locations' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE, 'menu_wksp_tax_status' AS MENU_IDENTIFIER,
              2 AS MENU_LEVEL, 8 AS ITEM_ORDER, 'Tax Status (Gen)' AS LABEL,
              'Tax Status Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_status' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE, 'menu_wksp_range_test' AS MENU_IDENTIFIER,
              2 AS MENU_LEVEL, 9 AS ITEM_ORDER, 'Range Test (Gen)' AS LABEL,
              'Range Test Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_range_test' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_tax_loc_rollups' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
              10 AS ITEM_ORDER, 'Repair Loc Rollups (Alloc)' AS LABEL,
              'Repair Location Rollup Configuration for Blanket Allocations' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_loc_rollups' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE, 'menu_wksp_tax_units' AS MENU_IDENTIFIER,
              2 AS MENU_LEVEL, 6 AS ITEM_ORDER,
              'Tax Units of Property' AS LABEL,
              'Tax Units of Property Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_units' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_tax_thresholds' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
              7 AS ITEM_ORDER, 'Tax Thresholds' AS LABEL,
              'Tax Threshold Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_thresholds' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_system_options' AS MENU_IDENTIFIER, 2 AS MENU_LEVEL,
              13 AS ITEM_ORDER, 'System Options' AS LABEL,
              'System Options' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_system_options' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE, 'menu_wksp_tax_yr' AS MENU_IDENTIFIER,
              2 AS MENU_LEVEL, 11 AS ITEM_ORDER,
              'Tax Year Configuration' AS LABEL,
              'Tax Year Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_yr' AS WORKSPACE_IDENTIFIER, 1 AS ENABLE_YN
         FROM DUAL
       UNION ALL
       SELECT 'REPAIRS' AS MODULE,
              'menu_wksp_tax_rpr_companies' AS MENU_IDENTIFIER,
              2 AS MENU_LEVEL, 1 AS ITEM_ORDER,
              'Company Configuration' AS LABEL,
              'Repair Company Configuration' AS MINIHELP,
              'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
              'menu_wksp_tax_rpr_companies' AS WORKSPACE_IDENTIFIER,
              1 AS ENABLE_YN
         FROM DUAL) b
ON (a.MODULE = b.MODULE AND a.MENU_IDENTIFIER = b.MENU_IDENTIFIER)
WHEN NOT MATCHED THEN
  INSERT
    (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
     PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
  VALUES
    (b.MODULE, b.MENU_IDENTIFIER, b.MENU_LEVEL, b.ITEM_ORDER, b.LABEL,
     b.MINIHELP, b.PARENT_MENU_IDENTIFIER, b.WORKSPACE_IDENTIFIER,
     b.ENABLE_YN)
WHEN MATCHED THEN
  UPDATE
     SET a.MENU_LEVEL             = b.MENU_LEVEL,
         a.ITEM_ORDER             = b.ITEM_ORDER,
         a.LABEL                  = b.LABEL,
         a.MINIHELP               = b.MINIHELP,
         a.PARENT_MENU_IDENTIFIER = b.PARENT_MENU_IDENTIFIER,
         a.WORKSPACE_IDENTIFIER   = b.WORKSPACE_IDENTIFIER,
         a.ENABLE_YN              = b.ENABLE_YN;

--updating the tax repairs import templates so only in scope companies appear
INSERT INTO pp_import_lookup
  (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL,
   IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME,
   LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL,
   DERIVED_AUTOCREATE_YN)
  SELECT 1122,
         'Company.Description',
         'The passed in value corresponds to the Company: Description field.  Translate to the Company ID using the Description column on the Company table.',
         'company_id',
         '( select co.company_id from company co where upper( trim( <importfield> ) ) = upper( trim( co.description ) ) )',
         0, 'company', 'description', '',
         'select co.description from company_setup cs, company co where cs.company_id = co.company_id and cs.is_tax_repairs_company = 1 union select ''All Companies'' from dual',
         NULL
    FROM dual
   WHERE NOT EXISTS
   (SELECT 1
            FROM pp_import_lookup
           WHERE LOOKUP_VALUES_ALTERNATE_SQL =
                 'select co.description from company_setup cs, company co where cs.company_id = co.company_id and cs.is_tax_repairs_company = 1 union select ''All Companies'' from dual');

UPDATE pp_import_template_fields
   SET import_lookup_id =
       1122
 WHERE (import_template_id, column_name) IN
       (SELECT pf.import_template_id, pf.column_name
          FROM pp_import_type_subsystem ts, pp_import_type it,
               pp_import_subsystem ps, pp_import_template pt,
               pp_import_template_fields pf
         WHERE it.import_type_id = ts.import_type_id
           AND ps.import_subsystem_id = ts.import_subsystem_id
           AND pt.import_type_id = it.import_type_id
           AND pf.import_template_id = pt.import_template_id
           AND Lower(pf.column_name) = 'company_id'
           AND Lower(ps.description) = 'tax repairs');

update pp_import_lookup x
   set x.lookup_sql = '( select co.company_id from (select company.company_id, company.description from company union select-1, ''ALL COMPANIES'' FROM DUAL) co where upper( trim( <importfield> ) ) = upper( trim( co.description ) ) )'
 where x.import_lookup_id =
       1122;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13723, 0, 2018, 2, 0, 0, 46763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_046763_taxrpr_02_companies_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
