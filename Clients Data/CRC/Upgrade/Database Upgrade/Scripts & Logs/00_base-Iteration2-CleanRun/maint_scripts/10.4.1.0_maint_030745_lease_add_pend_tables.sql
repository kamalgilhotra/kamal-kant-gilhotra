/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030745_lease_add_pend_tables.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   07/10/2013 Matthew Mikulka Point release
||============================================================================
*/

--Add Quantity Column to LS_ASSET

alter table LS_ASSET add QUANTITY number(22,2);

--ADD LS_PEND_TRANSACTION, LS_PEND_BASIS, LS_PEND_CLASS_CODE

--drop table LS_PEND_CLASS_CODE;
--drop table LS_PEND_BASIS;
--drop table LS_PEND_TRANSACTION;

create table LS_PEND_TRANSACTION
(
 LS_PEND_TRANS_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 LS_ASSET_ID      number(22,0),
 POSTING_AMOUNT   number(22,2),
 POSTING_QUANTITY number(22,2),
 ACTIVITY_CODE    char(5),
 GL_JE_CODE       char(18)
);

alter table LS_PEND_TRANSACTION
   add constraint PK_LS_PEND_TRANSACTION
       primary key (LS_PEND_TRANS_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PEND_TRANSACTION
   add constraint FK1_LS_PEND_TRANSACTION
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

create table LS_PEND_BASIS
(
 LS_PEND_TRANS_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 BASIS_1          number(22,2) not null,
 BASIS_2          number(22,2) not null,
 BASIS_3          number(22,2) not null,
 BASIS_4          number(22,2) not null,
 BASIS_5          number(22,2) not null,
 BASIS_6          number(22,2) not null,
 BASIS_7          number(22,2) not null,
 BASIS_8          number(22,2) not null,
 BASIS_9          number(22,2) not null,
 BASIS_10         number(22,2) not null,
 BASIS_11         number(22,2) not null,
 BASIS_12         number(22,2) not null,
 BASIS_13         number(22,2) not null,
 BASIS_14         number(22,2) not null,
 BASIS_15         number(22,2) not null,
 BASIS_16         number(22,2) not null,
 BASIS_17         number(22,2) not null,
 BASIS_18         number(22,2) not null,
 BASIS_19         number(22,2) not null,
 BASIS_20         number(22,2) not null,
 BASIS_21         number(22,2) not null,
 BASIS_22         number(22,2) not null,
 BASIS_23         number(22,2) not null,
 BASIS_24         number(22,2) not null,
 BASIS_25         number(22,2) not null,
 BASIS_26         number(22,2) not null,
 BASIS_27         number(22,2) not null,
 BASIS_28         number(22,2) not null,
 BASIS_29         number(22,2) not null,
 BASIS_30         number(22,2) not null,
 BASIS_31         number(22,2) not null,
 BASIS_32         number(22,2) not null,
 BASIS_33         number(22,2) not null,
 BASIS_34         number(22,2) not null,
 BASIS_35         number(22,2) not null,
 BASIS_36         number(22,2) not null,
 BASIS_37         number(22,2) not null,
 BASIS_38         number(22,2) not null,
 BASIS_39         number(22,2) not null,
 BASIS_40         number(22,2) not null,
 BASIS_41         number(22,2) not null,
 BASIS_42         number(22,2) not null,
 BASIS_43         number(22,2) not null,
 BASIS_44         number(22,2) not null,
 BASIS_45         number(22,2) not null,
 BASIS_46         number(22,2) not null,
 BASIS_47         number(22,2) not null,
 BASIS_48         number(22,2) not null,
 BASIS_49         number(22,2) not null,
 BASIS_50         number(22,2) not null,
 BASIS_51         number(22,2) not null,
 BASIS_52         number(22,2) not null,
 BASIS_53         number(22,2) not null,
 BASIS_54         number(22,2) not null,
 BASIS_55         number(22,2) not null,
 BASIS_56         number(22,2) not null,
 BASIS_57         number(22,2) not null,
 BASIS_58         number(22,2) not null,
 BASIS_59         number(22,2) not null,
 BASIS_60         number(22,2) not null,
 BASIS_61         number(22,2) not null,
 BASIS_62         number(22,2) not null,
 BASIS_63         number(22,2) not null,
 BASIS_64         number(22,2) not null,
 BASIS_65         number(22,2) not null,
 BASIS_66         number(22,2) not null,
 BASIS_67         number(22,2) not null,
 BASIS_68         number(22,2) not null,
 BASIS_69         number(22,2) not null,
 BASIS_70         number(22,2) not null
);

alter table LS_PEND_BASIS
   add constraint PK_LS_PEND_BASIS
       primary key (LS_PEND_TRANS_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PEND_BASIS
   add constraint FK1_LS_PEND_BASIS
       foreign key (LS_PEND_TRANS_ID)
       references LS_PEND_TRANSACTION (LS_PEND_TRANS_ID);

create table LS_PEND_CLASS_CODE
(
 CLASS_CODE_ID    number(22,0) not null,
 LS_PEND_TRANS_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 VALUE            varchar2(254) not null
);

alter table LS_PEND_CLASS_CODE
   add constraint PK_LS_PEND_CLASS_CODE
   primary key (CLASS_CODE_ID, LS_PEND_TRANS_ID)
   using index tablespace PWRPLANT_IDX;

alter table LS_PEND_CLASS_CODE
   add constraint FK1_LS_PEND_CLASS_CODE
       foreign key (CLASS_CODE_ID)
       references CLASS_CODE (CLASS_CODE_ID);

alter table LS_PEND_CLASS_CODE
   add constraint FK2_LS_PEND_CLASS_CODE
       foreign key (LS_PEND_TRANS_ID)
       references LS_PEND_TRANSACTION (LS_PEND_TRANS_ID);

--Create sequence for LS_PEND_TRANSACTION

create sequence LS_PEND_TRANSACTION_SEQ
   start with   1
   increment by 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (473, 0, 10, 4, 1, 0, 30745, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030745_lease_add_pend_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
