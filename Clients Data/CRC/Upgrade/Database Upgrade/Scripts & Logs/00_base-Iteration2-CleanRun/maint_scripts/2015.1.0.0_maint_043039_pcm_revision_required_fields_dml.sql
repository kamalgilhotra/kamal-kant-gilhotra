/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043039_pcm_revision_required_fields_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/02/2015 Ryan Oliveria    Require Date fields once in revisions
||============================================================================
*/

delete from PP_REQUIRED_TABLE_COLUMN
where id between 1016 and 1018;





--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2332, 0, 2015, 1, 0, 0, 043039, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043039_pcm_revision_required_fields_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;