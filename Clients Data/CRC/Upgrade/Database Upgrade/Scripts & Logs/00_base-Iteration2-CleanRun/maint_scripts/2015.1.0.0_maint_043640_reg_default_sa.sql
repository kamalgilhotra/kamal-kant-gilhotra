/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043640_reg_default_sa.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1   04/15/2015    Shane Ward       delivery default sub accounts
||============================================================================
*/


CREATE OR REPLACE PROCEDURE p_create_default_reg_sa AS
	sa_count NUMBER;
BEGIN

	SELECT Count(1)
		INTO sa_count
		FROM reg_sub_acct_type
	 WHERE reg_acct_type_id <> 11
		 AND sub_acct_type_id > 4;

	IF sa_count < 1 THEN
		--Insert
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 1, 'Plant in Service', 'Plant in Service', 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1,
			 2,
			 'Accumlated Reserve for Depr',
			 'Accumlated Reserve for Depr',
			 2);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 3, 'CWIP', 'CWIP', 3);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 4, 'Nuclear Fuel', 'Nuclear Fuel', 4);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 5, 'Fuel Stock', 'Fuel Stock', 5);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 6, 'Gas Stored Underground', 'Gas Stored Underground', 6);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 7, 'Other Prop and Invest', 'Other Prop and Invest', 7);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 8, 'Materials and Supplies', 'Materials and Supplies', 8);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 9, 'Working Capital', 'Working Capital', 9);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 10, 'Other Current Assets', 'Other Current Assets', 10);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 11, 'Regulatory Assets', 'Regulatory Assets', 11);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 12, 'Deferred Debits', 'Deferred Debits', 12);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 13, 'ADIT - Ratebase', 'ADIT - Ratebase', 13);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 14, 'Current Liabilities', 'Current Liabilities', 14);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(1, 15, 'Noncurrent Liabilities', 'Noncurrent Liabilities', 15);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(2,
			 1,
			 'Utility Assets Not in Ratebase',
			 'Utility Assets Not in Ratebase',
			 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(2,
			 2,
			 'Utility Liabilities Not in Ratebase',
			 'Utility Liabilities Not in Ratebase',
			 2);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(7, 1, 'Non Utility Property', 'Non Utility Property', 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(7,
			 2,
			 'Accum Reserve for Non-Util Prop',
			 'Accum Reserve for Non-Util Prop',
			 2);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(7, 3, 'Non Utility Liabilities', 'Non Utility Liabilities', 3);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(8, 1, 'Common Stock', 'Common Stock', 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(8, 2, 'Retained Earnings', 'Retained Earnings', 2);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(8, 3, 'Preferred Stock', 'Preferred Stock', 3);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(8, 4, 'Long Term Debt', 'Long Term Debt', 4);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(8, 5, 'ADIT - Capital Structure', 'ADIT - Capital Structure', 5);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(5, 1, 'Operating Revenues', 'Operating Revenues', 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3, 1, 'Fuel', 'Fuel', 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3, 2, 'Purchased Power', 'Purchased Power', 2);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3, 3, 'Other Operations', 'Other Operations', 3);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3,
			 4,
			 'Depreciation and Amortization',
			 'Depreciation and Amortization',
			 4);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3,
			 5,
			 'Net Regulatory Debits (Credits)',
			 'Net Regulatory Debits (Credits)',
			 5);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3, 6, 'Taxes other than income', 'Taxes other than income', 6);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3, 7, 'Income taxes', 'Income taxes', 7);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3,
			 8,
			 'Losses from disp of utility plant',
			 'Losses from disp of utility plant',
			 8);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(3,
			 9,
			 'Losses from disp of allowances',
			 'Losses from disp of allowances',
			 9);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(4, 1, 'AFUDC Equity', 'AFUDC Equity', 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(4, 2, 'Interest Income', 'Interest Income', 2);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(4, 3, 'Other income', 'Other income', 3);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(4, 4, 'Other deductions', 'Other deductions', 4);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(4,
			 5,
			 'Taxes other than income (BTL)',
			 'Taxes other than income (BTL)',
			 5);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(4, 6, 'Income taxes (BTL)', 'Income taxes (BTL)', 6);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(6, 1, 'Interest on long term debt', 'Interest on long term debt', 1);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(6, 2, 'AFUDC Debt', 'AFUDC Debt', 2);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(6,
			 3,
			 'Interest on short term debt',
			 'Interest on short term debt',
			 3);
		INSERT INTO reg_sub_acct_type
			(reg_acct_type_id,
			 sub_acct_type_id,
			 description,
			 long_description,
			 sort_order)
		Values
			(6, 4, 'Other interest expense', 'Other interest expense', 4);

	END IF;

END;
/

EXECUTE p_create_default_reg_sa;

DROP PROCEDURE p_create_default_reg_sa;

UPDATE reg_acct_master SET reg_acct_type_default = 1 , sub_acct_type_id = 1 WHERE reg_acct_id = 0;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2501, 0, 2015, 1, 0, 0, 43640, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043640_reg_default_sa.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;