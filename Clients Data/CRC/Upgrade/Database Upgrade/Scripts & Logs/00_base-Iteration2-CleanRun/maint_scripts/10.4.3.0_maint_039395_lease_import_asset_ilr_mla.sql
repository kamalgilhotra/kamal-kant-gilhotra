/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_039395_lease_import_asset_ilr_mla.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 08/13/2014 Daniel Motter  Creation
||============================================================================
*/

--Assets - need to add columns for estimated residual percent and tax summary

--add columns to asset import tables for new fields
alter table LS_IMPORT_ASSET
   add (ESTIMATED_RESIDUAL number(22,2) null,
        TAX_SUMMARY_ID     number(22,0) null,
        TAX_SUMMARY_XLATE  varchar2(35) null);

alter table LS_IMPORT_ASSET_ARCHIVE
   add (ESTIMATED_RESIDUAL number(22,2) null,
        TAX_SUMMARY_ID     number(22,0) null,
        TAX_SUMMARY_XLATE  varchar2(35) null);

--estimated_residual
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE)
values
   (253, 'estimated_residual', 'Estimated Residual Percent', 0, 1, 'number(22,8)');

--tax_summary_id
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (253, 'tax_summary_id', 'Tax Summary ID', 'tax_summary_xlate', 0, 1, 'number(22,0)', 'ls_tax_summary', 1,
    'tax_summary_id');

--set available translation options
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 253, 'tax_summary_id', P.IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP P
    where P.DESCRIPTION = 'LS Tax Summary.Description';

--MLAs - need to add columns for reconciliation level, days in year, and cutoff day

--add columns to mla import tables for new fields
alter table LS_IMPORT_LEASE
   add (LS_RECONCILE_TYPE_ID    number(22,0) null,
        LS_RECONCILE_TYPE_XLATE varchar2(35) null,
        DAYS_IN_YEAR            number(3,0)  null,
        CUT_OFF_DAY             number(2,0)  null);

alter table LS_IMPORT_LEASE_ARCHIVE
   add (LS_RECONCILE_TYPE_ID    number(22,0) null,
        LS_RECONCILE_TYPE_XLATE varchar2(35) null,
        DAYS_IN_YEAR            number(3,0)  null,
        CUT_OFF_DAY             number(2,0)  null);

--ls_reconcile_type_id
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (251, 'ls_reconcile_type_id', 'Reconciliation Level', 'ls_reconcile_type_xlate', 0, 1, 'number(22,0)',
    'ls_reconcile_type', 1, 'ls_reconcile_type_id');

--add translation options
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME,
    LOOKUP_COLUMN_NAME)
   select max(IMPORT_LOOKUP_ID) + 1,
          'LS Reconcile Type.Description',
          'The passed in value corresponds to the LS Tax Reconcile Type: Description field.  Translate to the Reconcile Type ID using the Description column on the LS Reconcile Type table.',
          'ls_reconcile_type_id',
          '( select rt.ls_reconcile_type_id from ls_reconcile_type rt where upper( trim( <importfield> ) ) = upper( trim( rt.description ) ) )',
          0,
          'ls_reconcile_type',
          'description'
     from PP_IMPORT_LOOKUP;

--set available translation options
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 251, 'ls_reconcile_type_id', P.IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP P
    where P.DESCRIPTION = 'LS Reconcile Type.Description';

--days_in_year
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE)
values
   (251, 'days_in_year', 'Days In Year', 0, 1, 'number(3,0)');

--cut_off_day
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE)
values
   (251, 'cut_off_day', 'Cut Off Day', 0, 1, 'number(2,0)');


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1356, 0, 10, 4, 3, 0, 39395, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039395_lease_import_asset_ilr_mla.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;