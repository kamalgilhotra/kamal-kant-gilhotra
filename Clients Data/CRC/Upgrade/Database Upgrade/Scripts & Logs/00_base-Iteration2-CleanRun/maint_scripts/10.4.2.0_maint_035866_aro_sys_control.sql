/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035866_aro_sys_control.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Ryan Oliveria  System control was uneditable
||============================================================================
*/

update PP_SYSTEM_CONTROL
   set DESCRIPTION = 'dw_yes_no;1'
 where CONTROL_NAME = 'ARO Settlmnt Credits by Ext GL Acct';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (903, 0, 10, 4, 2, 0, 35866, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035866_aro_sys_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;