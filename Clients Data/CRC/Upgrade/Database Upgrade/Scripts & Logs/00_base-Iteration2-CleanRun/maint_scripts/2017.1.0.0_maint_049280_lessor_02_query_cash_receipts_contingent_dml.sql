/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049280_lessor_02_query_cash_receipts_contingent_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/03/2017 Josh Sandler     Update contingent amount
||============================================================================
*/

UPDATE pp_any_query_criteria
SET SQL = 'SELECT  (SELECT filter_value FROM pp_any_required_filter WHERE  Upper(column_name)= ''START MONTHNUM'') AS start_monthnum,  (SELECT filter_value FROM pp_any_required_filter WHERE  Upper(column_name)= ''END MONTHNUM'') AS end_monthnum,  lct.description currency_type,  c.description company,  ls.lease_number lease_number,  ilr.ilr_number,  st.description ilr_status,  lg.description lease_group,  ig.description ilr_group,  lse.description lessee,  ct.description capitalization_type,  sob.description set_of_books,  To_Char(schedule.MONTH, ''yyyymm'') month_number,  schedule.iso_code currency,  Decode(fasb_ct.fasb_cap_type_id,     1,     schedule.interest_income_received+     schedule.executory_paid1+schedule.executory_paid2+schedule.executory_paid3+schedule.executory_paid4+schedule.executory_paid5+     schedule.executory_paid6+schedule.executory_paid7+schedule.executory_paid8+schedule.executory_paid9+schedule.executory_paid10+     schedule.contingent_paid1+schedule.contingent_paid2+schedule.contingent_paid3+schedule.contingent_paid4+schedule.contingent_paid5+     schedule.contingent_paid6+schedule.contingent_paid7+schedule.contingent_paid8+schedule.contingent_paid9+schedule.contingent_paid10,     0  ) operating_cash_receivable,  Decode(fasb_ct.fasb_cap_type_id,     2,     schedule.interest_income_received+Nvl(schedule.principal_received,0)+     schedule.executory_paid1+schedule.executory_paid2+schedule.executory_paid3+schedule.executory_paid4+schedule.executory_paid5+     schedule.executory_paid6+schedule.executory_paid7+schedule.executory_paid8+' ||
'schedule.executory_paid9+schedule.executory_paid10+     schedule.contingent_paid1+schedule.contingent_paid2+schedule.contingent_paid3+schedule.contingent_paid4+schedule.contingent_paid5+     schedule.contingent_paid6+schedule.contingent_paid7+schedule.contingent_paid8+schedule.contingent_paid9+schedule.contingent_paid10,     0  ) capital_cash_receivable,  currency_display_symbol AS currency_symbol  FROM v_lsr_ilr_mc_schedule schedule   JOIN company c ON (schedule.company_id = c.company_id)   JOIN lsr_ilr ilr ON (schedule.ilr_id = ilr.ilr_id AND schedule.revision = ilr.current_revision)   JOIN ls_lease_currency_type lct ON (schedule.ls_cur_type = lct.ls_currency_type_id)   JOIN lsr_lease ls ON (ilr.lease_id = ls.lease_id)   JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id)   JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id)   JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id)   JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)   JOIN lsr_cap_type ct ON (o.lease_cap_type_id = ct.cap_type_id)   JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id)   JOIN set_of_books sob ON (schedule.set_of_books_id = sob.set_of_books_id)   JOIN lsr_fasb_type_sob fasb_sob ON (fasb_sob.set_of_books_id = schedule.set_of_books_id AND fasb_sob.cap_type_id = ct.cap_type_id)   JOIN lsr_fasb_cap_type fasb_ct ON (fasb_ct.fasb_cap_type_id = fasb_sob.fasb_cap_type_id)  where to_char(schedule.month, ''yyyymm'') >=          (                SELECT filter_value                FROM   pp_any_required_filter                WHERE  upper(column_name)= ''START MONTHNUM'')  and to_char(schedule.month, ''yyyymm'') <=          (                SELECT filter_value                FROM   pp_any_required_filter                WHERE  upper(column_name)= ''END MONTHNUM'')  and c.description in (SELECT filter_value                FROM   pp_any_required_filter                WHERE  upper(column_name)= ''COMPANY'')'
WHERE description = 'Disclosure: Cash Receipts'
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3873, 0, 2017, 1, 0, 0, 49280, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049280_lessor_02_query_cash_receipts_contingent_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
