/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041281_cpr_ssp_depr_approval_dyn_val.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0  12/2/2014 Charlie Shilling Dynamic Validation Type for CPR - Depreciation Approval
||============================================================================
*/

INSERT INTO wo_validation_type
	(wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2,
	 col3, hard_edit)
VALUES
	(1005, 'CPR - Depreciation Approval', 'CPR Control - Depreciation Approval Validations',
	 'nvo_depr_approval.uf_depr_approval', 'select <arg2> from dual', 'month', 'company_id',
	 'call_location', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2080, 0, 2015, 1, 0, 0, 041281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041281_cpr_ssp_depr_approval_dyn_val.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;