/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049318_lease_01_field_capitalize_est_res_sys_ctrl_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

insert into pp_system_control(control_id, control_name, control_value, description, long_description, company_id)
select pwrplant1.nextval, 'Lease Capitalize Estimated Residual', 'No', 'dw_yes_no;1',
   'Determine if the estimated residual amount should be capitalized or if it will just be used in the depreciation calculation. "Yes" will capitalize, "No" will not.',
-1 from dual
where not exists(
   select 1
   from pp_system_control
   where control_name = 'Lease Capitalize Estimated Residual')
   ;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4016, 0, 2017, 1, 0, 0, 49318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049318_lease_01_field_capitalize_est_res_sys_ctrl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;