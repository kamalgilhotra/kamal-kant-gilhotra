/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_005391_cpr.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/09/2012 AYP            Omitted from 10.3.3.0
||============================================================================
*/

alter table PP_CONV_BATCHES add ASSIGN_DEPR_GROUP_FLAG number(22);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (196, 0, 10, 3, 5, 0, 5391, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_005391_cpr.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'),
    SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;


