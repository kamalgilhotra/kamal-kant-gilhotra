/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044507_reg_layer_group_queries_2_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 08/18/2015 Sarah Byers 		 	Add layer group to layer queries
||========================================================================================
*/

-- reg_forecast_ledger_layer_view
delete from cr_dd_sources_criteria_fields
 where id in (
	select id from cr_dd_sources_criteria
	 where table_name = 'reg_forecast_ledger_layer_view');

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'ADJ_AMOUNT', 15, 1, 1, null, 'Adj Amount', 313, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'ADJ_MONTH', 16, 0, 1, null, 'Adj Month', 281, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'ANNUALIZED_AMT', 14, 1, 1, null, 'Annualized Amt', 418, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'ANNUALIZED_TOTAL', 22, 0, 1, null, 'Annualized Total', 454, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'ANNUALZIED_LAYER_AMOUNT', 18, 1, 1, null, 'Annualzied Layer Amount', 674, 0, 'Any', null, null, 0, null, null,
 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'FCST_AMOUNT', 13, 1, 1, null, 'Fcst Amount', 340, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'FORECAST_LEDGER', 2, 0, 1, null, 'Forecast Ledger', 441, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'FORECAST_LEDGER_ID', 25, 0, 1, null, 'Forecast Ledger ID', 509, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'GL_MONTH', 12, 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'INCREMENTAL_ADJ_ID', 29, 0, 1, null, 'Incremental Adj ID', 491, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'INCREMENTAL_ADJ_TYPE_ID', 30, 0, 1, null, 'Incremental Adj Type ID', 637, 0, 'Any', null, null, 0, null, null,
 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'ITEM', 11, 0, 1, null, 'Item', 793, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'LABEL', 10, 0, 1, null, 'Label', 1003, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'LAYER_AMOUNT', 17, 1, 1, null, 'Layer Amount', 377, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'LAYER_DESCRIPTION', 8, 0, 1, null, 'Layer Description', 491, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'LAYER_GROUP', 9, 0, 1, null, 'Layer Group', 550, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'LAYER_TYPE', 7, 0, 1, null, 'Layer Type', 710, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'RECON_ADJ_AMOUNT', 19, 1, 1, null, 'Recon Adj Amount', 500, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'RECON_ADJ_COMMENT', 20, 0, 1, null, 'Recon Adj Comment', 541, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_ACCOUNT', 3, 0, 1, null, 'Reg Account', 1369, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_ACCOUNT_TYPE', 4, 0, 1, null, 'Reg Account Type', 715, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_ACCT_ID', 24, 0, 1, null, 'Reg Acct ID', 331, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_ACCT_TYPE_ID', 27, 0, 1, null, 'Reg Acct Type ID', 477, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_COMPANY', 1, 0, 1, null, 'Reg Company', 505, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_COMPANY_ID', 23, 0, 1, null, 'Reg Company ID', 445, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_SOURCE', 6, 0, 1, null, 'Reg Source', 820, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'REG_SOURCE_ID', 26, 0, 1, null, 'Reg Source ID', 395, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'SUB_ACCOUNT_TYPE', 5, 0, 1, null, 'Sub Account Type', 994, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'SUB_ACCT_TYPE_ID', 28, 0, 1, null, 'Sub Acct Type ID', 473, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_forecast_ledger_layer_view'), 
'TOTAL', 21, 0, 1, null, 'Total', 377, 0, 'Any', null, null, 0, null, null, 0) ; 

-- reg_layer_ea_details_view
delete from cr_dd_sources_criteria_fields
 where id in (
	select id from cr_dd_sources_criteria
	 where table_name = 'reg_layer_ea_details_view');

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'AMOUNT', 12, 1, 1, null, 'Amount', 212, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'AS_OF_DATE', 6, 0, 1, null, 'As Of Date', 303, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'END_MONTH', 8, 0, 1, null, 'End Month', 299, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'ITEM', 11, 0, 1, null, 'Item', 980, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'ITEM_SORT', 14, 0, 1, null, 'Item Sort', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'LABEL', 10, 0, 1, null, 'Label', 1003, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'LABEL_SORT', 13, 0, 1, null, 'Label Sort', 285, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'LAYER', 1, 0, 1, null, 'Layer', 276, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'LAYER_GROUP', 3, 0, 1, null, 'Layer Group', 335, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'LONG_DESCRIPTION', 2, 0, 1, null, 'Long Description', 459, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'MONTH_YEAR', 9, 0, 1, null, 'Month Year', 317, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'SELECTED_FIELD', 4, 0, 1, null, 'Selected Field', 427, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'SELECTED_VALUE', 5, 0, 1, null, 'Selected Value', 710, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_ea_details_view'), 
'START_MONTH', 7, 0, 1, null, 'Start Month', 322, 0, 'Any', null, null, 0, null, null, 0) ; 


-- reg_layer_fp_details_view
delete from cr_dd_sources_criteria_fields
 where id in (
	select id from cr_dd_sources_criteria
	 where table_name = 'reg_layer_fp_details_view');

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'DIFFERENCE', 12, 0, 1, null, 'Difference', 294, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'FUNDING_PROJECT_NUMBER', 4, 0, 1, null, 'Funding Project Number', 642, 0, 'Any', null, null, 0, null, null, 
0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'ITEM', 7, 0, 1, null, 'Item', 870, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'ITEM_SORT', 15, 0, 1, null, 'Item Sort', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'LABEL', 6, 0, 1, null, 'Label', 1003, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'LABEL_SORT', 14, 0, 1, null, 'Label Sort', 285, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'LAYER', 1, 0, 1, null, 'Layer', 308, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'LAYER_GROUP', 3, 0, 1, null, 'Layer Group', 335, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'LONG_DESCRIPTION', 2, 0, 1, null, 'Long Description', 459, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'MONTH_YEAR', 5, 0, 1, null, 'Month Year', 317, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'ORIGINAL_AMOUNT', 9, 1, 1, null, 'Original Amount', 427, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'ORIGINAL_REVISION', 8, 0, 1, null, 'Original Revision', 459, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'UPDATED_AMOUNT', 11, 1, 1, null, 'Updated Amount', 450, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'UPDATED_REVISION', 10, 0, 1, null, 'Updated Revision', 482, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where table_name = 'reg_layer_fp_details_view'), 
'WO_DESCRIPTION', 13, 0, 1, null, 'Wo Description', 418, 0, 'Any', null, null, 0, null, null, 0) ; 



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2798, 0, 2015, 2, 0, 0, 044507, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044507_reg_layer_group_queries_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;