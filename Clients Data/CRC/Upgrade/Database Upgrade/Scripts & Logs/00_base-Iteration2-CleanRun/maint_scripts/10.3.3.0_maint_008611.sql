/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008611.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/24/2011 Sunjin Cone    Point Release
||============================================================================
*/

--Maint 8611
-- Sunjin

SET SERVEROUTPUT ON

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';
   PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

   LL_COUNT number;
begin
   select count(*)
     into LL_COUNT
     from UNITIZED_WORK_ORDER X, WORK_ORDER_CONTROL Y
    where X.WORK_ORDER_ID = Y.WORK_ORDER_ID
      and X.ACTIVITY_CODE not like '%ADD%'
      and X.STATUS is null
      and X.LDG_ASSET_ID in (select B.LDG_ASSET_ID
                               from PEND_TRANSACTION_ARCHIVE A, RETIRE_TRANSACTION B
                              where A.PEND_TRANS_ID = B.PEND_TRANS_ID
                                and A.LDG_DEPR_GROUP_ID < 0
                                and ABS(A.COST_OF_REMOVAL) + ABS(A.SALVAGE_CASH) +
                                    ABS(A.SALVAGE_RETURNS) + ABS(A.RESERVE_CREDITS) <> 0
                                and TO_CHAR(A.GL_POSTING_MO_YR, 'yyyy') >= 2010
                                and A.FERC_ACTIVITY_CODE = 2
                                and A.LDG_ASSET_ID is null
                                and B.LDG_ASSET_ID is not null)
      and not exists (select 1
             from WO_UNIT_ITEM_PEND_TRANS Z
            where Z.UNIT_ITEM_ID = X.UNIT_ITEM_ID
              and Z.WORK_ORDER_ID = X.WORK_ORDER_ID);

   if LL_COUNT > 0 then
      DBMS_OUTPUT.PUT_LINE(PPCORA ||
                           '20000 - Data issues exist and need to be addressed.  Read comment in update script.');
   else
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'No data issues exist.');
   end if;
end;
/

/*
If the above code returns Count > 0 then review data using the following queries:
Reset Unitization needs to be done on each work order number that returns in this query:

select distinct Y.WORK_ORDER_NUMBER, Y.COMPANY_ID
  from UNITIZED_WORK_ORDER X, WORK_ORDER_CONTROL Y
 where X.WORK_ORDER_ID = Y.WORK_ORDER_ID
   and X.ACTIVITY_CODE not like '%ADD%'
   and X.STATUS is null
   and X.LDG_ASSET_ID in (select B.LDG_ASSET_ID
                            from PEND_TRANSACTION_ARCHIVE A, RETIRE_TRANSACTION B
                           where A.PEND_TRANS_ID = B.PEND_TRANS_ID
                             and A.LDG_DEPR_GROUP_ID < 0
                             and ABS(A.COST_OF_REMOVAL) + ABS(A.SALVAGE_CASH) +
                                 ABS(A.SALVAGE_RETURNS) + ABS(A.RESERVE_CREDITS) <> 0
                             and TO_CHAR(A.GL_POSTING_MO_YR, 'yyyy') >= 2010
                             and A.FERC_ACTIVITY_CODE = 2
                             and A.LDG_ASSET_ID is null
                             and B.LDG_ASSET_ID is not null)
   and not exists (select 1
          from WO_UNIT_ITEM_PEND_TRANS Z
         where Z.UNIT_ITEM_ID = X.UNIT_ITEM_ID
           and Z.WORK_ORDER_ID = X.WORK_ORDER_ID);
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (17, 0, 10, 3, 3, 0, 8611, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008611.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
