/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051928_lessor_01_add_set_of_books_termination_tables_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/26/2018  Alex Healey    adding set of books for lsr_ilr_termination_op and lsr_ilr_termination_st_df tables
||============================================================================
*/
ALTER TABLE lsr_ilr_termination_op
ADD set_of_books_id NUMBER(22,0)  NULL;

ALTER TABLE lsr_ilr_termination_st_df
ADD set_of_books_id NUMBER(22,0) NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11022, 0, 2018, 1, 0, 0, 51928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051928_lessor_01_add_set_of_books_termination_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;