/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048943_2_lessor_02_create_additional_sales_schedule_types_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/06/2017 A. Hill          Add types for lessor sales type schedule calc
||============================================================================
*/

DECLARE
  cnt NUMBER;
BEGIN
  
  SELECT COUNT(1) INTO cnt
  FROM all_types
  WHERE LOWER(OWNER) = 'pwrplant'
  AND LOWER(type_name) = 'lsr_ilr_op_sch_pay_term_tab';
  
  IF cnt <> 0 THEN
    EXECUTE IMMEDIATE 'drop type lsr_ilr_op_sch_pay_term_tab';
  END IF;
  
  execute immediate 'create or replace TYPE lsr_ilr_op_sch_pay_term AS OBJECT(payment_month_frequency NUMBER(22,0),
                                                                              payment_term_start_date date,
                                                                              number_of_terms NUMBER(22,0),
                                                                              payment_amount NUMBER(22,2),
                                                                              executory_buckets lsr_bucket_amount_tab,
                                                                              contingent_buckets lsr_bucket_amount_tab,
                                                                              is_prepay number(1,0))';
  
  execute immediate 'create or replace type lsr_ilr_op_sch_pay_term_tab as table of lsr_ilr_op_sch_pay_term';

  SELECT COUNT(1) INTO cnt
  FROM all_types
  WHERE LOWER(OWNER) = 'pwrplant'
  AND LOWER(type_name) = 'lsr_ilr_op_sch_result_full_tab';
  
  IF cnt <> 0 THEN
    execute immediate 'DROP TYPE lsr_ilr_op_sch_result_full_tab';
  END IF;
  
  execute immediate 'create or replace TYPE lsr_ilr_op_sch_result_full IS OBJECT( ilr_id NUMBER,
                                                                                  revision NUMBER,
                                                                                  set_of_books_id number, --need to implement multiple sets of books
                                                                                  MONTH DATE,
                                                                                  interest_income_received number,
                                                                                  interest_income_accrued NUMBER,
                                                                                  interest_rental_recvd_spread NUMBER,
                                                                                  begin_deferred_rev number,
                                                                                  deferred_rev NUMBER,
                                                                                  end_deferred_rev NUMBER,
                                                                                  begin_receivable NUMBER,
                                                                                  end_receivable NUMBER,
                                                                                  executory_accrual1 NUMBER,
                                                                                  executory_accrual2 NUMBER,
                                                                                  executory_accrual3 NUMBER,
                                                                                  executory_accrual4 number,
                                                                                  executory_accrual5 NUMBER,
                                                                                  executory_accrual6 NUMBER,
                                                                                  executory_accrual7 number,
                                                                                  executory_accrual8 NUMBER,
                                                                                  executory_accrual9 number,
                                                                                  executory_accrual10 NUMBER,
                                                                                  executory_paid1 NUMBER,
                                                                                  executory_paid2 NUMBER,
                                                                                  executory_paid3 number,
                                                                                  executory_paid4 NUMBER,
                                                                                  executory_paid5 NUMBER,
                                                                                  executory_paid6 NUMBER,
                                                                                  executory_paid7 NUMBER,
                                                                                  executory_paid8 NUMBER,
                                                                                  executory_paid9 NUMBER,
                                                                                  executory_paid10 NUMBER,
                                                                                  contingent_accrual1 number,
                                                                                  contingent_accrual2 NUMBER,
                                                                                  contingent_accrual3 NUMBER,
                                                                                  contingent_accrual4 number,
                                                                                  contingent_accrual5 NUMBER,
                                                                                  contingent_accrual6 NUMBER,
                                                                                  contingent_accrual7 number,
                                                                                  contingent_accrual8 NUMBER,
                                                                                  contingent_accrual9 number,
                                                                                  contingent_accrual10 NUMBER,
                                                                                  contingent_paid1 NUMBER,
                                                                                  contingent_paid2 NUMBER,
                                                                                  contingent_paid3 NUMBER,
                                                                                  contingent_paid4 NUMBER,
                                                                                  contingent_paid5 NUMBER,
                                                                                  contingent_paid6 NUMBER,
                                                                                  contingent_paid7 number,
                                                                                  contingent_paid8 NUMBER,
                                                                                  contingent_paid9 NUMBER,
                                                                                  contingent_paid10 NUMBER,
                                                                                  current_lease_cost NUMBER)';
                                                        
  execute immediate 'CREATE TYPE lsr_ilr_op_sch_result_full_tab is table of lsr_ilr_op_sch_result_full';
  
  SELECT COUNT(1) INTO cnt
  FROM all_types
  WHERE LOWER(OWNER) = 'pwrplant'
  and lower(type_name) = 'lsr_ilr_op_sch_pay_info_tab';
  
  IF cnt <> 0 THEN
    EXECUTE IMMEDIATE 'drop type lsr_ilr_op_sch_pay_info_tab';
  END IF;
  
  execute immediate 'CREATE OR REPLACE TYPE lsr_ilr_op_sch_pay_info AS OBJECT  (payment_month_frequency NUMBER,
                                                                                payment_term_start_date date,
                                                                                MONTH DATE,
                                                                                number_of_terms NUMBER,
                                                                                payment_amount NUMBER,
                                                                                contingent_buckets lsr_bucket_amount_tab,
                                                                                executory_buckets lsr_bucket_amount_tab,
                                                                                is_prepay NUMBER(1,0),
                                                                                iter NUMBER)';

  execute immediate 'CREATE TYPE lsr_ilr_op_sch_pay_info_tab as table of lsr_ilr_op_sch_pay_info';
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3765, 0, 2017, 1, 0, 0, 48943, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048943_2_lessor_02_create_additional_sales_schedule_types_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
