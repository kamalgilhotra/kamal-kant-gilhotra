/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046361_proj_extend_pp_calendar_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2016.1.0 09/06/2016 Daniel Mendel  Extend PP Calendar to 2099
 ||============================================================================
 */

INSERT INTO pp_calendar (MONTH, YEAR, month_number, fiscal_month, fiscal_year, fiscal_quarter)  
  SELECT To_Date(SubStr(month_number,5,2)||'01'||(YEAR+rw), 'MM/DD/YYYY'), YEAR+rw, YEAR+rw||SubStr(month_number,5,2), fiscal_month, fiscal_year+rw, fiscal_quarter 
  FROM pp_calendar, (SELECT ROWNUM rw FROM pp_calendar WHERE ROWNUM <= (SELECT 2099 - Max(YEAR) FROM pp_calendar)) yr
  WHERE YEAR = (SELECT Max(YEAR) FROM pp_calendar)
  AND rw > 0;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3277, 0, 2016, 1, 0, 0, 046361, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046361_proj_extend_pp_calendar_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;