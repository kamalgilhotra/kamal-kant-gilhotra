/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050387_lessor_01_operating_jes_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/21/2018 Anand R        Add JE trans-types and payment type for Lessor Operating ILR
||============================================================================
*/

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4032, 'Initial Direct Cost Amortization Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4033, 'Initial Direct Cost Amortization Credit');

insert into ls_payment_type (payment_type_id, description)
values (28, 'Initial Direct Cost Amortization' );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4227, 0, 2017, 3, 0, 0, 50387, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050387_lessor_01_operating_jes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 