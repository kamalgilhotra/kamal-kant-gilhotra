/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048037_lease_update_invoice_view_historic_rates_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version   Date      Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 07/26/2017 Jared Watkins  Update the v_ls_invoice_fx_vw to follow the same
||                    logic as the payments views since we only want to use a calculated rate and not an effective dated rate
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_INVOICE_FX_VW AS
WITH
cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
 invoice_id,
 vendor_id,
 invoice_status,
 invoice_number,
 inv.ls_asset_Id,
 inv.ilr_id,
 inv.ilr_Id tax_local_id,
 gl_posting_Mo_yr,
 invoice_amount * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_amount,
 invoice_interest * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_interest,
 invoice_executory * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_executory,
 invoice_contingent * decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) invoice_contingent,
 lease.lease_id,
 lease.lease_number,
 inv.company_id,
 cur.ls_cur_type ls_cur_type,
 cr.exchange_date,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 decode(ls_cur_type, 2, nvl(rate, historic_rate), 1) rate,
 cur.iso_code,
 cur.currency_display_symbol
 FROM ls_invoice inv
 INNER JOIN currency_schema cs
    ON inv.company_id = cs.company_id
 INNER JOIN ls_lease lease
    ON inv.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 LEFT OUTER JOIN ls_lease_calculated_date_rates cr
    ON lease.contract_currency_id = cr.contract_currency_id
   AND cur.currency_id = cr.company_currency_id
   AND inv.company_id = cr.company_id
   AND inv.gl_posting_mo_yr = cr.accounting_month
 WHERE Nvl(cr.exchange_rate_type_id, 1) = 1;

 
 --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3611, 0, 2017, 1, 0, 0, 48037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048037_lease_update_invoice_view_historic_rates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
