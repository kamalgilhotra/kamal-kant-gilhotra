/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051835_taxrpr_blanket_allocation_method_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 06/25/2018 Eric Berger    Misc changes to the Blanket Allocation
||                                      method.
||============================================================================
*/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_processing ADD quantity_vs_cost NUMBER(2,0)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_proc_debug ADD quantity_vs_cost NUMBER(2,0)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_processing ADD repair_qty_include NUMBER(22,2) DEFAULT 1 NOT NULL';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_proc_debug ADD repair_qty_include NUMBER(22,2) DEFAULT 1 NOT NULL';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_process_report ADD repair_qty_include NUMBER(22,2) DEFAULT 1 NOT NULL';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_results ADD repair_qty_include NUMBER(22,2) DEFAULT 1 NOT NULL';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blanket_results_debug ADD repair_qty_include NUMBER(22,2) DEFAULT 1 NOT NULL';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE repair_blkt_results_reporting ADD repair_qty_include NUMBER(22,2)  DEFAULT 1 NOT NULL';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column already exists in table.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_BLANKET_PROCESSING DROP CONSTRAINT PK_REPAIR_BLANKET_PROCESSING';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already dropped.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'DROP index PK_REPAIR_BLANKET_PROCESSING';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already dropped.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_BLANKET_PROCESSING ADD CONSTRAINT PK_REPAIR_BLANKET_PROCESSING PRIMARY KEY (WORK_ORDER_ID, BLANKET_METHOD, REPAIR_LOCATION_ID, REPAIR_UNIT_CODE_ID, REPAIR_QTY_INCLUDE)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_BLKT_RESULTS_REPORTING  DROP CONSTRAINT PK_REPAIR_BLKT_RESULTS_REPORT';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already dropped.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'DROP index PK_REPAIR_BLKT_RESULTS_REPORT';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already dropped.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_BLKT_RESULTS_REPORTING ADD CONSTRAINT PK_REPAIR_BLKT_RESULTS_REPORT PRIMARY KEY (BATCH_ID, WORK_ORDER_ID, REPAIR_UNIT_CODE_ID, BLANKET_METHOD, REPAIR_QTY_INCLUDE)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_BLANKET_PROCESS_REPORT DROP CONSTRAINT PK_REPAIR_BLANKET_REPORT';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already dropped.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'DROP index PK_REPAIR_BLANKET_REPORT';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Index already dropped.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_BLANKET_PROCESS_REPORT ADD CONSTRAINT PK_REPAIR_BLANKET_REPORT PRIMARY KEY (BATCH_ID, REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER, REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID, BLANKET_METHOD, REPAIR_QTY_INCLUDE)';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Constraint already exists.');
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13742, 0, 2018, 2, 0, 0, 51835, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_051835_taxrpr_blanket_allocation_method_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;