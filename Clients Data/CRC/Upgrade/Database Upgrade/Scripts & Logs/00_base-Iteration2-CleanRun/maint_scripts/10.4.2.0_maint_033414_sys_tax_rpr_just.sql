/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033414_sys_tax_rpr_just.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/24/2013 Stephen Motter
||============================================================================
*/

update WO_DOC_JUSTIFICATION_RULES
   set USEROBJECT_NAME = 'uo_dw_wo_doc_just_tax'
 where LOWER(DATAWINDOW_NAME) = 'dw_wo_doc_just_tax_stat'
   and TAB_INDICATOR = 10;

update WO_DOC_JUSTIFICATION_RULES
   set USEROBJECT_NAME = 'uo_dw_wo_doc_just_tax_wf'
 where LOWER(nvl(DATAWINDOW_NAME, ' ')) <> 'dw_wo_doc_just_tax_stat'
   and TAB_INDICATOR = 10
   and LOWER(TYPE_OF_DATA) = 'workflow';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (708, 0, 10, 4, 2, 0, 33414, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033414_sys_tax_rpr_just.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
