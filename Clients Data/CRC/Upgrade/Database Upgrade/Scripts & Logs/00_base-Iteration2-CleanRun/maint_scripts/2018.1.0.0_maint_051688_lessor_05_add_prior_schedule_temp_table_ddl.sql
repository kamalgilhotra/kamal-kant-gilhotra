/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051688_lessor_05_add_prior_schedule_temp_table_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  09/18/2018 Jared Watkins    Add temp table to hold the schedule from the prior approved revision for use in a remeasurement
||============================================================================
*/

create global temporary table lsr_ilr_prior_schedule_temp(
    ilr_id                          number(22,0),
	revision                        number(22,0),
	set_of_books_id                 number(22,0),
	month                           date,
	interest_income_received        number(22,2),
	interest_income_accrued         number(22,2),
	interest_rental_recvd_spread    number(22,2),
	beg_deferred_rev                number(22,2),
	deferred_rev_activity           number(22,2),
	end_deferred_rev                number(22,2),
	beg_receivable                  number(22,2),
	end_receivable                  number(22,2),
	beg_lt_receivable               number(22,2),
	end_lt_receivable               number(22,2),
	initial_direct_cost             number(22,2),
	executory_accrual1              number(22,2),
	executory_accrual2              number(22,2),
	executory_accrual3              number(22,2),
	executory_accrual4              number(22,2),
	executory_accrual5              number(22,2),
	executory_accrual6              number(22,2),
	executory_accrual7              number(22,2),
	executory_accrual8              number(22,2),
	executory_accrual9              number(22,2),
	executory_accrual10             number(22,2),
	executory_paid1                 number(22,2),
	executory_paid2                 number(22,2),
	executory_paid3                 number(22,2),
	executory_paid4                 number(22,2),
	executory_paid5                 number(22,2),
	executory_paid6                 number(22,2),
	executory_paid7                 number(22,2),
	executory_paid8                 number(22,2),
	executory_paid9                 number(22,2),
	executory_paid10                number(22,2),
	contingent_accrual1             number(22,2),
	contingent_accrual2             number(22,2),
	contingent_accrual3             number(22,2),
	contingent_accrual4             number(22,2),
	contingent_accrual5             number(22,2),
	contingent_accrual6             number(22,2),
	contingent_accrual7             number(22,2),
	contingent_accrual8             number(22,2),
	contingent_accrual9             number(22,2),
	contingent_accrual10            number(22,2),
	contingent_paid1                number(22,2),
	contingent_paid2                number(22,2),
	contingent_paid3                number(22,2),
	contingent_paid4                number(22,2),
	contingent_paid5                number(22,2),
	contingent_paid6                number(22,2),
	contingent_paid7                number(22,2),
	contingent_paid8                number(22,2),
	contingent_paid9                number(22,2),
	contingent_paid10               number(22,2),
	beg_deferred_rent               number(22,2),
	deferred_rent                   number(22,2),
	end_deferred_rent               number(22,2),
	beg_accrued_rent                number(22,2),
	accrued_rent                    number(22,2),
	end_accrued_rent                number(22,2),
	principal_received              number(22,2),
	principal_accrued               number(22,2),
	beg_unguaranteed_residual       number(22,2),
	interest_unguaranteed_residual  number(22,2),
	ending_unguaranteed_residual    number(22,2),
	beg_net_investment              number(22,2),
	interest_net_investment         number(22,2),
	ending_net_investment           number(22,2),
	begin_deferred_profit           number(22,2),
	recognized_profit               number(22,2),
	end_deferred_profit             number(22,2)
);

alter table lsr_ilr_prior_schedule_temp add (
constraint pk_lsr_ilr_prior_schedule_temp primary key(ilr_id, revision, set_of_books_id, month));

comment on table lsr_ilr_prior_schedule_temp is '(C)  [06] The ILR Prior Schedule table holds the prior approved schedule from the revision being remeasured.';

comment on column lsr_ilr_prior_schedule_temp.ilr_id is 'System-assigned identifier of a particular ILR';
comment on column lsr_ilr_prior_schedule_temp.revision is 'The current approved revision of the ILR; in a remeasurement this is used to pull the prior schedule as well as any relevant amounts';
comment on column lsr_ilr_prior_schedule_temp.set_of_books_id is 'The internal set of books ID within PowerPlant';
comment on column lsr_ilr_prior_schedule_temp.month is 'The month being processed';
comment on column lsr_ilr_prior_schedule_temp.interest_income_received is 'Interest/Income Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.interest_income_accrued is 'Interest/Income Accrued on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.interest_rental_recvd_spread is 'Interest/Rental Received Spread on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.beg_deferred_rev is 'Beginning Deferred Revenue on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.deferred_rev_activity is 'Deferred Revenue Activity on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.end_deferred_rev is 'Ending Deferred Revenue on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.beg_receivable is 'Beginning Receivable on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.end_receivable is 'Ending Receivable on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.beg_lt_receivable is 'Beginning Long-Term Receivable on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.end_lt_receivable is 'Ending Long-Term Receivable on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.initial_direct_cost is 'Initial Direct Cost on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual1 is 'Executory Bucket 1 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual2 is 'Executory Bucket 2 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual3 is 'Executory Bucket 3 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual4 is 'Executory Bucket 4 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual5 is 'Executory Bucket 5 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual6 is 'Executory Bucket 6 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual7 is 'Executory Bucket 7 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual8 is 'Executory Bucket 8 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual9 is 'Executory Bucket 9 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_accrual10 is 'Executory Bucket 10 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid1 is 'Executory Bucket 1 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid2 is 'Executory Bucket 2 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid3 is 'Executory Bucket 3 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid4 is 'Executory Bucket 4 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid5 is 'Executory Bucket 5 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid6 is 'Executory Bucket 6 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid7 is 'Executory Bucket 7 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid8 is 'Executory Bucket 8 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid9 is 'Executory Bucket 9 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.executory_paid10 is 'Executory Bucket 10 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual1 is 'Contingent Bucket 1 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual2 is 'Contingent Bucket 2 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual3 is 'Contingent Bucket 3 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual4 is 'Contingent Bucket 4 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual5 is 'Contingent Bucket 5 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual6 is 'Contingent Bucket 6 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual7 is 'Contingent Bucket 7 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual8 is 'Contingent Bucket 8 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual9 is 'Contingent Bucket 9 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_accrual10 is 'Contingent Bucket 10 Accrual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid1 is 'Contingent Bucket 1 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid2 is 'Contingent Bucket 2 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid3 is 'Contingent Bucket 3 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid4 is 'Contingent Bucket 4 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid5 is 'Contingent Bucket 5 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid6 is 'Contingent Bucket 6 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid7 is 'Contingent Bucket 7 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid8 is 'Contingent Bucket 8 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid9 is 'Contingent Bucket 9 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.contingent_paid10 is 'Contingent Bucket 10 Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.beg_deferred_rent is 'Beginning Deferred Rent on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.deferred_rent is 'Deferred Rent Activity on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.end_deferred_rent is 'Ending Deferred Rent on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.beg_accrued_rent is 'Beginning Accrued Rent on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.accrued_rent is 'Accrued Rent Activity on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.end_accrued_rent is 'Ending Accrued Rent on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.principal_received is 'Principal Received on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.principal_accrued is 'Principal Accrued on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.beg_unguaranteed_residual is 'Beginning Unguaranteed Residual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.interest_unguaranteed_residual is 'Interest on Unguaranteed Residual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.ending_unguaranteed_residual is 'Ending Unguaranteed Residual on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.beg_net_investment is 'Beginning Net Investment on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.interest_net_investment is 'Interest on Net Investment on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.ending_net_investment is 'Ending Net Investmenton current approved revision';
comment on column lsr_ilr_prior_schedule_temp.begin_deferred_profit is 'Beginning Deferred Profit on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.recognized_profit is 'Recognized Profit on current approved revision';
comment on column lsr_ilr_prior_schedule_temp.end_deferred_profit is 'Ending Deferred Profit on current approved revision';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(9903, 0, 2018, 1, 0, 0, 51688, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051688_lessor_05_add_prior_schedule_temp_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;