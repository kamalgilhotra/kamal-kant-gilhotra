/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038432_reg_cwip_cap_budget_components.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 06/20/2014 Shane Ward       Cap Budget Components now link
||                                      to CWIP Components
||============================================================================
*/

alter table REG_BUDGET_COMPONENT_VALUES DROP CONSTRAINT R_REG_BUDG_COMPONENT_VALUES1;
alter table REG_BUDGET_COMP_WHERE_CLAUSE drop constraint FK1_REG_BUDGET_COMP_WHERE_CLAU;

drop table REG_BUDGET_FAMILY_COMPONENT;

alter table REG_BUDGET_COMPONENT_VALUES
   add constraint R_REG_BUDG_COMPONENT_VALUES1
       foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
       references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

alter table REG_BUDGET_COMP_WHERE_CLAUSE
   add constraint FK1_REG_BUDGET_COMP_WHERE_CLAU
       foreign key (REG_COMPONENT_ID, REG_FAMILY_ID)
       references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);


create or replace view REG_BUDGET_FAMILY_COMPONENT
as
select REG_COMPONENT_ID,
       REG_FAMILY_ID,
       DESCRIPTION,
       REG_ACCT_TYPE_DEFAULT,
       SUB_ACCT_TYPE_ID,
       REG_ANNUALIZATION_ID,
       ACCT_GOOD_FOR,
       LONG_DESCRIPTION,
       USER_ID,
       TIME_STAMP,
       USED_BY_CLIENT,
       REG_CWIP_SOURCE_ID,
       HIST_OR_FCST
  from REG_CWIP_FAMILY_COMPONENT;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1166, 0, 10, 4, 2, 7, 38432, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038432_reg_cwip_cap_budget_components.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;