/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008850_proptax_script2.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   11/29/2011 Julia Breuer   Point Release
||============================================================================
*/

begin
   DBMS_STATS.SET_TABLE_STATS('PWRPLANT', 'PT_TEMP_LEDGER_ADJS', '', null, null, 1, 1, 36, null);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (62, 0, 10, 3, 4, 0, 8850, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_008850_proptax_script2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
