/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037099_lease_PKG_LEASE_IMPORT.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.1.1 10/09/2013 Kyle Peterson  Patch Release
|| 10.4.1.2 10/08/2013 Ryan Oliveria  Fixed hard coded workflow type
|| 10.4.2.0 01/07/2014 Kyle Peterson  Point Release
|| 10.4.2.0 03/17/2014 Brandon Beck
||============================================================================
*/

create or replace package PKG_LEASE_IMPORT as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_IMPORT
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 08/19/2013 K.Peterson     Original Version
   ||============================================================================
   */
   type NUM_ARRAY is table of number(22) index by binary_integer;

   procedure P_INVOICE_ROLLUP;

   procedure P_BUILDILRS(A_ILR_IDS   NUM_ARRAY,
                         A_REVISIONS NUM_ARRAY,
                         A_SEND_JES  number);

   function F_IMPORT_INVOICES(A_RUN_ID number) return varchar2;

   function F_IMPORT_LESSORS(A_RUN_ID number) return varchar2;

   function F_VALIDATE_INVOICES(A_RUN_ID number) return varchar2;

   function F_VALIDATE_LESSORS(A_RUN_ID number) return varchar2;

   function F_IMPORT_ILRS(A_RUN_ID number) return varchar2;

   function F_VALIDATE_ILRS(A_RUN_ID number) return varchar2;

   function F_IMPORT_MLAS(A_RUN_ID number) return varchar2;

   function F_VALIDATE_MLAS(A_RUN_ID number) return varchar2;

   function F_IMPORT_ASSETS(A_RUN_ID number) return varchar2;

   function F_VALIDATE_ASSETS(A_RUN_ID number) return varchar2;

end PKG_LEASE_IMPORT;
/

create or replace package body PKG_LEASE_IMPORT as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_IMPORT
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 08/19/2013 K.Peterson    Original Version
   ||============================================================================
   */

   --DECLARE VARIABLES HERE
   type INVOICE_IMPORT_TYPE is table of LS_IMPORT_INVOICE%rowtype index by pls_integer;
   type INVOICE_IMPORT_HDR is record(
      VENDOR_ID        LS_IMPORT_INVOICE.VENDOR_ID%type,
      COMPANY_ID       LS_IMPORT_INVOICE.COMPANY_ID%type,
      LEASE_ID         LS_IMPORT_INVOICE.LEASE_ID%type,
      GL_POSTING_MO_YR LS_INVOICE.GL_POSTING_MO_YR%type,
      INVOICE_NUMBER   LS_INVOICE.INVOICE_NUMBER%type);
   type INVOICE_IMPORT_LINE is record(
      INVOICE_ID          LS_INVOICE.INVOICE_ID%type,
      INVOICE_LINE_NUMBER LS_INVOICE_LINE.INVOICE_LINE_NUMBER%type,
      PAYMENT_TYPE_ID     LS_IMPORT_INVOICE.PAYMENT_TYPE_ID%type,
      GL_POSTING_MO_YR    LS_INVOICE_LINE.GL_POSTING_MO_YR%type,
      LS_ASSET_ID         LS_IMPORT_INVOICE.LS_ASSET_ID%type,
      AMOUNT              LS_IMPORT_INVOICE.AMOUNT%type);
   type INVOICE_IMPORT_HDR_TABLE is table of INVOICE_IMPORT_HDR index by pls_integer;
   type INVOICE_LINE_TABLE is table of INVOICE_IMPORT_LINE index by pls_integer;
   type ASSET_IMPORT_TABLE is table of LS_IMPORT_ASSET%rowtype index by pls_integer;
   type MLA_COMPANY_LINE is record(
      LEASE_ID       LS_LEASE_COMPANY.LEASE_ID%type,
      COMPANY_ID     LS_LEASE_COMPANY.COMPANY_ID%type,
      MAX_LEASE_LINE LS_LEASE_COMPANY.MAX_LEASE_LINE%type,
      VENDOR_ID      LS_LEASE_VENDOR.VENDOR_ID%type,
      PAYMENT_PCT    LS_LEASE_VENDOR.PAYMENT_PCT%type);
   type MLA_COMPANY_TABLE is table of MLA_COMPANY_LINE index by pls_integer;

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   procedure P_INVOICE_ROLLUP is
      L_INDEX number;
      type INVOICE_LINES_TABLE is table of LS_INVOICE_LINE%rowtype index by pls_integer;
      INVOICE_LINES INVOICE_LINES_TABLE;
   begin
      update LS_INVOICE
         set INVOICE_AMOUNT = 0, INVOICE_INTEREST = 0, INVOICE_EXECUTORY = 0, INVOICE_CONTINGENT = 0;

      select * bulk collect into INVOICE_LINES from LS_INVOICE_LINE;

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_AMOUNT = INVOICE_AMOUNT + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID;

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_INTEREST = INVOICE_INTEREST + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID
            and INVOICE_LINES(L_INDEX).PAYMENT_TYPE_ID in (2, 5);

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_EXECUTORY = INVOICE_EXECUTORY + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID
            and INVOICE_LINES(L_INDEX).PAYMENT_TYPE_ID in (3, 6);

      forall L_INDEX in indices of INVOICE_LINES
         update LS_INVOICE
            set INVOICE_CONTINGENT = INVOICE_CONTINGENT + INVOICE_LINES(L_INDEX).AMOUNT
          where INVOICE_ID = INVOICE_LINES(L_INDEX).INVOICE_ID
            and INVOICE_LINES(L_INDEX).PAYMENT_TYPE_ID in (4, 7);

   end P_INVOICE_ROLLUP;

   --**************************************************************************
   --                            P_BUILDILRS
   --**************************************************************************
   --
   procedure P_BUILDILRS(A_ILR_IDS   NUM_ARRAY,
                         A_REVISIONS NUM_ARRAY,
                         A_SEND_JES  number) is
      RTN      varchar2(4000);
      RTN_NUM  number(22, 0);
      L_STATUS varchar2(2000);

   begin

      --Turn off the JEs if we don't want them
      if A_SEND_JES = 0 then
        PKG_LEASE_ASSET_POST.G_SEND_JES := false;
      end if;

      RTN := PKG_LEASE_SCHEDULE.F_MASS_PROCESS_ILRS(A_ILR_IDS, A_REVISIONS);
      if RTN <> 'OK' then
         return;
      end if;

      RTN := PKG_LEASE_SCHEDULE.F_PROCESS_ASSETS;
      if RTN <> 'OK' then
         return;
      end if;

      RTN := PKG_LEASE_SCHEDULE.F_SAVE_SCHEDULES;
      if RTN <> 'OK' then
         return;
      end if;

      --APPROVE HERE
      for I in 1 .. A_ILR_IDS.COUNT
      loop

         L_STATUS := 'Sending ILR for approval';
         RTN_NUM  := PKG_LEASE_CALC.F_SEND_ILR_NO_COMMIT(A_ILR_IDS(I), A_REVISIONS(I), L_STATUS);
         if RTN_NUM <> 1 then
            rollback;
            RAISE_APPLICATION_ERROR(-20000, 'Error Sending: ' || L_STATUS);
            return;
         end if;

         L_STATUS := 'Auto Approving ILR';
         RTN_NUM  := PKG_LEASE_CALC.F_APPROVE_ILR_NO_COMMIT(A_ILR_IDS(I), A_REVISIONS(I), L_STATUS, FALSE);
         if RTN_NUM <> 1 then
            rollback;
            RAISE_APPLICATION_ERROR(-20000, 'Error Approving: ' || L_STATUS);
            return;
         end if;


         update WORKFLOW
            set DATE_SENT = sysdate,
                DATE_APPROVED = sysdate
          where ID_FIELD1 = A_ILR_IDS(I)
            and ID_FIELD2 = A_REVISIONS(I)
            and SUBSYSTEM = 'ilr_approval';

      end loop;

      commit;

      --Turn the JEs back on
      PKG_LEASE_ASSET_POST.G_SEND_JES := TRUE;

   end P_BUILDILRS;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   --##########################################################################
   --                            INVOICES
   --##########################################################################
   function F_IMPORT_INVOICES(A_RUN_ID in number) return varchar2 is
      L_INVOICE_LINE             INVOICE_LINE_TABLE;
      L_INDEX                    number;
      L_INVOICE_IMPORT_HDR_TABLE INVOICE_IMPORT_HDR_TABLE;
      L_MSG                      varchar2(2000);
   begin
      L_MSG := 'Collecting for Invoice headers';
      select distinct VENDOR_ID,
                      COMPANY_ID,
                      LEASE_ID,
                      TO_DATE(GL_POSTING_MO_YR, 'yyyymm'),
                      INVOICE_NUMBER bulk collect
        into L_INVOICE_IMPORT_HDR_TABLE
        from LS_IMPORT_INVOICE
       where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'Inserting dummy invoices into LS_INVOICE';
      forall L_INDEX in indices of L_INVOICE_IMPORT_HDR_TABLE
         insert into LS_INVOICE
            (INVOICE_ID, COMPANY_ID, LEASE_ID, GL_POSTING_MO_YR, VENDOR_ID, INVOICE_NUMBER)
            select LS_INVOICE_SEQ.NEXTVAL,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).COMPANY_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).GL_POSTING_MO_YR,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).VENDOR_ID,
                   L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).INVOICE_NUMBER
              from DUAL;

      L_MSG := 'Collecting for Invoice lines';
      select LI.INVOICE_ID,
             ROW_NUMBER() OVER(partition by TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm'), LII.VENDOR_ID, LII.COMPANY_ID, LII.LEASE_ID order by TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm')),
             LII.PAYMENT_TYPE_ID,
             TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm'),
             LII.LS_ASSET_ID,
             LII.AMOUNT bulk collect
        into L_INVOICE_LINE
        from LS_IMPORT_INVOICE LII, LS_INVOICE LI
       where LII.IMPORT_RUN_ID = A_RUN_ID
         and LII.VENDOR_ID = LI.VENDOR_ID
         and LII.COMPANY_ID = LI.COMPANY_ID
         and TO_DATE(LII.GL_POSTING_MO_YR, 'yyyymm') = LI.GL_POSTING_MO_YR
         and LII.LEASE_ID = LI.LEASE_ID;

      L_MSG := 'Inserting into LS_INVOICE_LINE';
      forall L_INDEX in indices of L_INVOICE_LINE
         insert into LS_INVOICE_LINE
            (INVOICE_ID, INVOICE_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID, GL_POSTING_MO_YR,
             AMOUNT)
            select L_INVOICE_LINE(L_INDEX).INVOICE_ID,
                   L_INVOICE_LINE(L_INDEX).INVOICE_LINE_NUMBER,
                   L_INVOICE_LINE(L_INDEX).PAYMENT_TYPE_ID,
                   L_INVOICE_LINE(L_INDEX).LS_ASSET_ID,
                   L_INVOICE_LINE(L_INDEX).GL_POSTING_MO_YR,
                   L_INVOICE_LINE(L_INDEX).AMOUNT
              from DUAL;

      P_INVOICE_ROLLUP;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_INVOICES;

   function F_VALIDATE_INVOICES(A_RUN_ID in number) return varchar2 is
      L_INDEX                    number;
      L_INVOICE_IMPORT_HDR_TABLE INVOICE_IMPORT_HDR_TABLE;
      L_MSG                      varchar2(2000);
      NUM_UPDATED                number;
   begin
      L_MSG := 'Collecting unique vendor/company/lease/month combinations';
      select distinct VENDOR_ID,
                      COMPANY_ID,
                      LEASE_ID,
                      TO_DATE(GL_POSTING_MO_YR, 'yyyymm'),
                      INVOICE_NUMBER bulk collect
        into L_INVOICE_IMPORT_HDR_TABLE
        from LS_IMPORT_INVOICE
       where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'Updating ls_import_invoices with errors';
      forall L_INDEX in indices of L_INVOICE_IMPORT_HDR_TABLE
         update LS_IMPORT_INVOICE
            set ERROR_MESSAGE = 'Invoice already exists for this company/month/vendor/lease combination.'
          where exists
          (select 1
                   from LS_INVOICE
                  where GL_POSTING_MO_YR = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).GL_POSTING_MO_YR
                    and COMPANY_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).COMPANY_ID
                    and LEASE_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID
                    and VENDOR_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).VENDOR_ID)
            and TO_DATE(GL_POSTING_MO_YR, 'yyyymm') = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX)
               .GL_POSTING_MO_YR
            and COMPANY_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).COMPANY_ID
            and LEASE_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).LEASE_ID
            and VENDOR_ID = L_INVOICE_IMPORT_HDR_TABLE(L_INDEX).VENDOR_ID;
      --num_updated := sql%rowcount;

      update LS_IMPORT_INVOICE A
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Invoice Number must be the same for this company/month/vendor/lease combination.'
       where IMPORT_RUN_ID = A_RUN_ID
         and exists (select 1
                from LS_IMPORT_INVOICE B
               where B.GL_POSTING_MO_YR = A.GL_POSTING_MO_YR
                 and B.COMPANY_ID = A.COMPANY_ID
                 and B.LEASE_ID = A.LEASE_ID
                 and B.VENDOR_ID = A.VENDOR_ID
                 and NVL(B.INVOICE_NUMBER, '*~*~INVALIDNUMBER~*~*') <>
                     NVL(A.INVOICE_NUMBER, '*~*~INVALIDNUMBER~*~*'));

      update LS_IMPORT_INVOICE A
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' There is already an invoice with this invoice number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and exists (select 1 from LS_INVOICE B where B.INVOICE_NUMBER = A.INVOICE_NUMBER);

      update LS_IMPORT_INVOICE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' All invoice amounts must be positive.'
       where IMPORT_RUN_ID = A_RUN_ID
         and AMOUNT < 0;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_INVOICES;
   --##########################################################################
   --                            LESSORS
   --##########################################################################
   function F_IMPORT_LESSORS(A_RUN_ID in number) return varchar2 is
      type LESSOR_IMPORT_TABLE is table of LS_IMPORT_LESSOR%rowtype index by pls_integer;
      L_LESSOR_IMPORT LESSOR_IMPORT_TABLE;
      L_INDEX         number;
      L_MSG           varchar2(2000);
   begin

      --here we're populating our PK for the lessors. If the previous row has a different unique identifier than the current
      --row we give it a new id, else we give it the previous row's lessor_id but we make it negative so we don't attempt to insert
      --on that row
      update LS_IMPORT_LESSOR
         set LESSOR_ID = LS_LESSOR_SEQ.NEXTVAL
       where LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_LESSOR_IDENTIFIER order by UNIQUE_LESSOR_IDENTIFIER, LINE_ID) NUM
                                   from LS_IMPORT_LESSOR
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where NUM = 1)
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_LESSOR A
         set A.LESSOR_ID =
              (select -B.LESSOR_ID
                 from LS_IMPORT_LESSOR B
                where A.UNIQUE_LESSOR_IDENTIFIER = B.UNIQUE_LESSOR_IDENTIFIER
                  and B.LESSOR_ID is not null
                  and IMPORT_RUN_ID = A_RUN_ID)
       where A.LESSOR_ID is null
         and IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'Collecting for lessor insert';
      select * bulk collect
        into L_LESSOR_IMPORT
        from LS_IMPORT_LESSOR
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(LESSOR_ID, -1) > 0;

      L_MSG := 'Inserting Lessors';
      forall L_INDEX in indices of L_LESSOR_IMPORT
         insert into LS_LESSOR
            (LESSOR_ID, DESCRIPTION, LONG_DESCRIPTION, ADDRESS1, ADDRESS2, ADDRESS3, ADDRESS4, ZIP,
             POSTAL, CITY, COUNTY_ID, STATE_ID, COUNTRY_ID, PHONE_NUMBER, EXTENSION, FAX_NUMBER,
             EXTERNAL_LESSOR_NUMBER, SITE_CODE, STATUS_CODE_ID, MULTI_VENDOR_YN_SW, LEASE_GROUP_ID)
            (select L_LESSOR_IMPORT(L_INDEX).LESSOR_ID,
                    L_LESSOR_IMPORT(L_INDEX).DESCRIPTION,
                    L_LESSOR_IMPORT(L_INDEX).LONG_DESCRIPTION,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS1,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS2,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS3,
                    L_LESSOR_IMPORT(L_INDEX).ADDRESS4,
                    L_LESSOR_IMPORT(L_INDEX).ZIP,
                    L_LESSOR_IMPORT(L_INDEX).POSTAL,
                    L_LESSOR_IMPORT(L_INDEX).CITY,
                    L_LESSOR_IMPORT(L_INDEX).COUNTY_ID,
                    L_LESSOR_IMPORT(L_INDEX).STATE_ID,
                    L_LESSOR_IMPORT(L_INDEX).COUNTRY_ID,
                    L_LESSOR_IMPORT(L_INDEX).PHONE_NUMBER,
                    L_LESSOR_IMPORT(L_INDEX).EXTENSION,
                    L_LESSOR_IMPORT(L_INDEX).FAX_NUMBER,
                    L_LESSOR_IMPORT(L_INDEX).EXTERNAL_LESSOR_NUMBER,
                    L_LESSOR_IMPORT(L_INDEX).SITE_CODE,
                    1,
                    L_LESSOR_IMPORT(L_INDEX).MULTI_VENDOR_YN_SW,
                    L_LESSOR_IMPORT(L_INDEX).LEASE_GROUP_ID
               from DUAL);

      update LS_IMPORT_LESSOR set VENDOR_ID = LS_VENDOR_SEQ.NEXTVAL where IMPORT_RUN_ID = A_RUN_ID;

      select * bulk collect
        into L_LESSOR_IMPORT
        from LS_IMPORT_LESSOR
       where IMPORT_RUN_ID = A_RUN_ID;

      forall L_INDEX in indices of L_LESSOR_IMPORT
         insert into LS_VENDOR
            (VENDOR_ID, LESSOR_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_VENDOR_NUMBER,
             REMIT_ADDR, STATUS_CODE_ID, PRIMARY)
            select L_LESSOR_IMPORT(L_INDEX).VENDOR_ID,
                   ABS(L_LESSOR_IMPORT(L_INDEX).LESSOR_ID),
                   L_LESSOR_IMPORT(L_INDEX).VENDOR_DESCRIPTION,
                   L_LESSOR_IMPORT(L_INDEX).VENDOR_LONG_DESCRIPTION,
                   L_LESSOR_IMPORT(L_INDEX).EXTERNAL_VENDOR_NUMBER,
                   L_LESSOR_IMPORT(L_INDEX).REMIT_ADDR,
                   1,
                   L_LESSOR_IMPORT(L_INDEX).PRIMARY
              from DUAL;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_LESSORS;

   function F_VALIDATE_LESSORS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(2000);
   begin
      --check for duplicate lessor descriptions
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate lessor description.'
       where IMPORT_RUN_ID = A_RUN_ID
         and DESCRIPTION in
             (select DESCRIPTION
                from (select DESCRIPTION, count(*) as MY_COUNT
                        from (select DESCRIPTION
                                from LS_LESSOR
                              union all
                              select DESCRIPTION
                                from (select distinct UNIQUE_LESSOR_IDENTIFIER, DESCRIPTION
                                        from LS_IMPORT_LESSOR
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by DESCRIPTION)
               where MY_COUNT > 1);

      --check for duplicate external_lessor_number
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate external lessor number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and EXTERNAL_LESSOR_NUMBER in
             (select EXTERNAL_LESSOR_NUMBER
                from (select EXTERNAL_LESSOR_NUMBER, count(*) as MY_COUNT
                        from (select EXTERNAL_LESSOR_NUMBER
                                from LS_LESSOR
                              union all
                              select EXTERNAL_LESSOR_NUMBER
                                from (select distinct UNIQUE_LESSOR_IDENTIFIER, EXTERNAL_LESSOR_NUMBER
                                        from LS_IMPORT_LESSOR
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by EXTERNAL_LESSOR_NUMBER)
               where MY_COUNT > 1);

      --check for duplicate vendor_descriptions
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate vendor description.'
       where IMPORT_RUN_ID = A_RUN_ID
         and VENDOR_DESCRIPTION in (select DESCRIPTION as VENDOR_DESCRIPTION
                                      from (select DESCRIPTION, count(*) as MY_COUNT
                                              from (select DESCRIPTION
                                                      from LS_VENDOR
                                                    union all
                                                    select VENDOR_DESCRIPTION as DESCRIPTION
                                                      from LS_IMPORT_LESSOR
                                                     where IMPORT_RUN_ID = A_RUN_ID)
                                             group by DESCRIPTION)
                                     where MY_COUNT > 1);

      --check that only one vendor is primary
      update LS_IMPORT_LESSOR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Exactly one vendor must be primary for each lessor.'
       where IMPORT_RUN_ID = A_RUN_ID
         and UNIQUE_LESSOR_IDENTIFIER in (select UNIQUE_LESSOR_IDENTIFIER
                                            from (select UNIQUE_LESSOR_IDENTIFIER, sum(PRIMARY) SUMS
                                                    from LS_IMPORT_LESSOR
                                                   where IMPORT_RUN_ID = A_RUN_ID
                                                   group by UNIQUE_LESSOR_IDENTIFIER)
                                           where SUMS <> 1);

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_LESSORS;

   --##########################################################################
   --                            MLAS
   --##########################################################################
   function F_IMPORT_MLAS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(2000);
      type MLA_IMPORT_TABLE is table of LS_IMPORT_LEASE%rowtype index by pls_integer;
      L_INDEX            number;
      MLA_COMPANY        MLA_COMPANY_TABLE;
      L_MLA_IMPORT_TABLE MLA_IMPORT_TABLE;
      L_CC_SQL           clob;
      CC_NEEDED          boolean;
      RTN                number;
   begin

      update LS_IMPORT_LEASE
         set LEASE_ID = LS_LEASE_SEQ.NEXTVAL
       where LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_LEASE_IDENTIFIER order by UNIQUE_LEASE_IDENTIFIER, LINE_ID) NUM
                                   from LS_IMPORT_LEASE
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where NUM = 1)
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_LEASE A
         set A.LEASE_ID =
              (select -B.LEASE_ID
                 from LS_IMPORT_LEASE B
                where A.UNIQUE_LEASE_IDENTIFIER = B.UNIQUE_LEASE_IDENTIFIER
                  and B.LEASE_ID is not null)
       where A.LEASE_ID is null
         and IMPORT_RUN_ID = A_RUN_ID;

      select * bulk collect
        into L_MLA_IMPORT_TABLE
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(LEASE_ID, -1) > 0;

      L_MSG := 'LS_LEASE';
      forall L_INDEX in indices of L_MLA_IMPORT_TABLE
         insert into LS_LEASE
            (LEASE_ID, LEASE_STATUS_ID, LEASE_NUMBER, DESCRIPTION, LONG_DESCRIPTION, LESSOR_ID,
             LEASE_TYPE_ID, PAYMENT_DUE_DAY, PRE_PAYMENT_SW, LEASE_GROUP_ID, LEASE_CAP_TYPE_ID,
             WORKFLOW_TYPE_ID, MASTER_AGREEMENT_DATE, NOTES, CURRENT_REVISION, INITIATION_DATE)
            select L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID,
                   1,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_NUMBER,
                   L_MLA_IMPORT_TABLE(L_INDEX).DESCRIPTION,
                   L_MLA_IMPORT_TABLE(L_INDEX).LONG_DESCRIPTION,
                   L_MLA_IMPORT_TABLE(L_INDEX).LESSOR_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_TYPE_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).PAYMENT_DUE_DAY,
                   L_MLA_IMPORT_TABLE(L_INDEX).PRE_PAYMENT_SW,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_GROUP_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).LEASE_CAP_TYPE_ID,
                   L_MLA_IMPORT_TABLE(L_INDEX).WORKFLOW_TYPE_ID,
                   TO_DATE(L_MLA_IMPORT_TABLE(L_INDEX).MASTER_AGREEMENT_DATE, 'yyyymm'),
                   L_MLA_IMPORT_TABLE(L_INDEX).NOTES,
                   1,
                   sysdate
              from DUAL;

      L_MSG := 'LS_LEASE_APPROVAL';
      forall L_INDEX in indices of L_MLA_IMPORT_TABLE
         insert into LS_LEASE_APPROVAL
            (LEASE_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID, 1, a.workflow_type_id, 1
      from workflow_type a
      where a.external_workflow_type = 'AUTO'
      ;

      L_MSG := 'LS_LEASE_OPTIONS';
      forall L_INDEX in indices of L_MLA_IMPORT_TABLE
         insert into LS_LEASE_OPTIONS
            (LEASE_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT,
             RENEWAL_OPTION_TYPE_ID, CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW,
          TAX_SUMMARY_ID, TAX_RATE_OPTION_ID, AUTO_GENERATE_INVOICES)
         values
            (L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID, 1,
             L_MLA_IMPORT_TABLE(L_INDEX).PURCHASE_OPTION_TYPE_ID,
             L_MLA_IMPORT_TABLE(L_INDEX).PURCHASE_OPTION_AMT,
             L_MLA_IMPORT_TABLE(L_INDEX).RENEWAL_OPTION_TYPE_ID,
             L_MLA_IMPORT_TABLE(L_INDEX).CANCELABLE_TYPE_ID, L_MLA_IMPORT_TABLE(L_INDEX).ITC_SW,
             L_MLA_IMPORT_TABLE(L_INDEX).PARTIAL_RETIRE_SW, L_MLA_IMPORT_TABLE(L_INDEX).SUBLET_SW,
          nvl(L_MLA_IMPORT_TABLE(L_INDEX).TAX_SUMMARY_ID, 0),
          nvl(L_MLA_IMPORT_TABLE(L_INDEX).TAX_RATE_OPTION_ID, 0),
          nvl(L_MLA_IMPORT_TABLE(L_INDEX).AUTO_GENERATE_INVOICES, 0));

      L_CC_SQL := 'BEGIN ';
      for L_INDEX in 1 .. L_MLA_IMPORT_TABLE.COUNT
      loop
         if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
            .CLASS_CODE_VALUE1 is not null then
            CC_NEEDED := true;
            L_CC_SQL  := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                 values (' ||
                         TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID1) || ', ' ||
                         TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                         TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE1) || ''');';
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE2 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID2) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE2) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE3 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID3) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE3) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE4 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID4) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE4) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE5 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID5) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE5) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE6 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID6) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE6) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE7 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID7) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE7) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE8 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID8) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE8) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE9 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID9) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE9) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE10 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID10) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE10) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE11 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID11) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE11) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE12 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID12) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE12) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE13 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID13) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE13) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE14 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID14) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE14) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE15 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID15) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE15) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE16 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID16) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE16) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE17 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID17) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE17) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE18 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID18) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE18) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE19 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID19) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE19) || ''');';
            end if;
            if L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20 is not null and L_MLA_IMPORT_TABLE(L_INDEX)
               .CLASS_CODE_VALUE20 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_MLA_CLASS_CODE (class_code_id, lease_id, value)
                                    values (' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_ID20) || ', ' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID) || ', ''' ||
                           TO_CHAR(L_MLA_IMPORT_TABLE(L_INDEX).CLASS_CODE_VALUE20) || ''');';
            end if;
         end if;
      end loop;
      L_CC_SQL := L_CC_SQL || ' END;';
      if CC_NEEDED then
         L_MSG := L_CC_SQL;
         execute immediate L_CC_SQL;
      end if;

      select distinct ABS(LEASE_ID), COMPANY_ID, MAX_LEASE_LINE, 0, 0 bulk collect
        into MLA_COMPANY
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'LS_LEASE_COMPANY';
      forall L_INDEX in indices of MLA_COMPANY
         insert into LS_LEASE_COMPANY
            (LEASE_ID, COMPANY_ID, MAX_LEASE_LINE)
         values
            (MLA_COMPANY(L_INDEX).LEASE_ID, MLA_COMPANY(L_INDEX).COMPANY_ID,
             MLA_COMPANY(L_INDEX).MAX_LEASE_LINE);

      select distinct LEASE_ID, COMPANY_ID, MAX_LEASE_LINE, VENDOR_ID, PAYMENT_PCT bulk collect
        into MLA_COMPANY
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID;

      L_MSG := 'LS_LEASE_VENDOR';
      forall L_INDEX in indices of MLA_COMPANY
         insert into LS_LEASE_VENDOR
            (LEASE_ID, COMPANY_ID, VENDOR_ID, PAYMENT_PCT)
         values
            (ABS(MLA_COMPANY(L_INDEX).LEASE_ID), MLA_COMPANY(L_INDEX).COMPANY_ID,
             MLA_COMPANY(L_INDEX).VENDOR_ID, MLA_COMPANY(L_INDEX).PAYMENT_PCT);

      --Auto approve it
      select * bulk collect
        into L_MLA_IMPORT_TABLE
        from LS_IMPORT_LEASE
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(LEASE_ID, -1) > 0
         and (AUTO_APPROVE = 1 or AUTO_APPROVE is null)
         and (WORKFLOW_TYPE_ID in (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where upper(EXTERNAL_WORKFLOW_TYPE) = 'AUTO') or WORKFLOW_TYPE_ID is null);

      L_MSG := 'Auto Approving';
      for L_INDEX in 1 .. L_MLA_IMPORT_TABLE.COUNT
      loop
         RTN := PKG_LEASE_CALC.F_APPROVE_MLA(L_MLA_IMPORT_TABLE(L_INDEX).LEASE_ID, 1);
      end loop;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_MLAS;

   function F_VALIDATE_MLAS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(2000);
   begin
      --check for duplicate lease number
      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate Lease Number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LEASE_NUMBER in
             (select LEASE_NUMBER
                from (select LEASE_NUMBER, count(*) as MY_COUNT
                        from (select LEASE_NUMBER
                                from LS_LEASE
                              union all
                              select LEASE_NUMBER
                                from (select distinct LEASE_NUMBER, UNIQUE_LEASE_IDENTIFIER
                                        from LS_IMPORT_LEASE
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by LEASE_NUMBER)
               where MY_COUNT > 1);

      --check that payment_pct makes sense
      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Payment Percentage must be between 0 and 1.'
       where IMPORT_RUN_ID = A_RUN_ID
         and PAYMENT_PCT not between 0.000000001 and 1.000000001; --we dont want a 0 but 1 is ok

      --check that the payment_pct adds up to 1 across the companies
      update LS_IMPORT_LEASE
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Total payment percentage must be 100% per company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and (UNIQUE_LEASE_IDENTIFIER, COMPANY_ID) in
             (select UNIQUE_LEASE_IDENTIFIER, COMPANY_ID
                from (select UNIQUE_LEASE_IDENTIFIER, COMPANY_ID, sum(PAYMENT_PCT) TOTAL_PCT
                        from LS_IMPORT_LEASE
                       where IMPORT_RUN_ID = A_RUN_ID
                       group by UNIQUE_LEASE_IDENTIFIER, COMPANY_ID)
               where TOTAL_PCT <> 1);

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_MLAS;

   --##########################################################################
   --                            ILRS
   --##########################################################################
   function F_IMPORT_ILRS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(2000);
      type ILR_IMPORT_TABLE is table of LS_IMPORT_ILR%rowtype;
      L_ILR_IMPORT ILR_IMPORT_TABLE;
      L_INDEX      number;
      L_CC_SQL     clob;
      CC_NEEDED    boolean;
   begin

      update LS_IMPORT_ILR
         set ILR_ID = LS_ILR_SEQ.NEXTVAL
       where LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_ILR_IDENTIFIER order by UNIQUE_ILR_IDENTIFIER, LINE_ID) NUM
                                   from LS_IMPORT_ILR
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where NUM = 1)
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_ILR A
         set A.ILR_ID =
              (select -B.ILR_ID
                 from LS_IMPORT_ILR B
                where A.UNIQUE_ILR_IDENTIFIER = B.UNIQUE_ILR_IDENTIFIER
                  and B.ILR_ID is not null)
       where A.ILR_ID is null
         and IMPORT_RUN_ID = A_RUN_ID;

      update LS_IMPORT_ILR A
         set A.WORKFLOW_TYPE_ID =
              (select WORKFLOW_TYPE_ID from LS_ILR_GROUP where ILR_GROUP_ID = A.ILR_GROUP_ID)
       where A.WORKFLOW_TYPE_ID is null
         and A.IMPORT_RUN_ID = A_RUN_ID;

      select * bulk collect
        into L_ILR_IMPORT
        from LS_IMPORT_ILR
       where IMPORT_RUN_ID = A_RUN_ID
         and NVL(ILR_ID, -1) > 0;

      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR
            (ILR_ID, ILR_STATUS_ID, ILR_NUMBER, LEASE_ID, COMPANY_ID, EST_IN_SVC_DATE, EXTERNAL_ILR,
             ILR_GROUP_ID, NOTES, CURRENT_REVISION, WORKFLOW_TYPE_ID)
         values
            (L_ILR_IMPORT(L_INDEX).ILR_ID, 1, L_ILR_IMPORT(L_INDEX).ILR_NUMBER,
             L_ILR_IMPORT(L_INDEX).LEASE_ID, L_ILR_IMPORT(L_INDEX).COMPANY_ID,
             TO_DATE(L_ILR_IMPORT(L_INDEX).EST_IN_SVC_DATE, 'yyyymm'),
             L_ILR_IMPORT(L_INDEX).EXTERNAL_ILR, L_ILR_IMPORT(L_INDEX).ILR_GROUP_ID,
             L_ILR_IMPORT(L_INDEX).NOTES, 1, L_ILR_IMPORT(L_INDEX).WORKFLOW_TYPE_ID);

      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_APPROVAL
            (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         values
            (L_ILR_IMPORT(L_INDEX).ILR_ID, 1, 4, 1);

      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_OPTIONS
            (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
             CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, INCEPTION_AIR,
             LEASE_CAP_TYPE_ID, TERMINATION_AMT, IMPORT_RUN_ID)
         values
            (L_ILR_IMPORT(L_INDEX).ILR_ID, 1, L_ILR_IMPORT(L_INDEX).PURCHASE_OPTION_TYPE_ID,
             L_ILR_IMPORT(L_INDEX).PURCHASE_OPTION_AMT, L_ILR_IMPORT(L_INDEX).RENEWAL_OPTION_TYPE_ID,
             L_ILR_IMPORT(L_INDEX).CANCELABLE_TYPE_ID, L_ILR_IMPORT(L_INDEX).ITC_SW,
             L_ILR_IMPORT(L_INDEX).PARTIAL_RETIRE_SW, L_ILR_IMPORT(L_INDEX).SUBLET_SW,
             L_ILR_IMPORT(L_INDEX).INCEPTION_AIR, L_ILR_IMPORT(L_INDEX).LEASE_CAP_TYPE_ID,
             L_ILR_IMPORT(L_INDEX).TERMINATION_AMT, A_RUN_ID);

      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_ACCOUNT
            (ILR_ID, INT_ACCRUAL_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID,
            EXEC_ACCRUAL_ACCOUNT_ID, EXEC_EXPENSE_ACCOUNT_ID,
            CONT_ACCRUAL_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID,
         CAP_ASSET_ACCOUNT_ID, ST_OBLIG_ACCOUNT_ID,
         LT_OBLIG_ACCOUNT_ID, AP_ACCOUNT_ID,
         RES_DEBIT_ACCOUNT_ID, RES_CREDIT_ACCOUNT_ID)
         select L_ILR_IMPORT(L_INDEX).ILR_ID,
         nvl(L_ILR_IMPORT(L_INDEX).INT_ACCRUAL_ACCOUNT_ID, INT_ACCRUAL_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).INT_EXPENSE_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID),
            nvl(L_ILR_IMPORT(L_INDEX).EXEC_ACCRUAL_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).EXEC_EXPENSE_ACCOUNT_ID, EXEC_EXPENSE_ACCOUNT_ID),
            nvl(L_ILR_IMPORT(L_INDEX).CONT_ACCRUAL_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).CONT_EXPENSE_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).CAP_ASSET_ACCOUNT_ID, CAP_ASSET_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).ST_OBLIG_ACCOUNT_ID, ST_OBLIG_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).LT_OBLIG_ACCOUNT_ID, LT_OBLIG_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).AP_ACCOUNT_ID, AP_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).RES_DEBIT_ACCOUNT_ID, RES_DEBIT_ACCOUNT_ID),
         nvl(L_ILR_IMPORT(L_INDEX).RES_CREDIT_ACCOUNT_ID, RES_CREDIT_ACCOUNT_ID)
         from LS_ILR_GROUP
         where ILR_GROUP_ID = L_ILR_IMPORT(L_INDEX).ILR_GROUP_ID;

      L_CC_SQL := 'BEGIN ';
      for L_INDEX in 1 .. L_ILR_IMPORT.COUNT
      loop
         if L_ILR_IMPORT(L_INDEX)
          .CLASS_CODE_ID1 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE1 is not null then
            CC_NEEDED := true;
            L_CC_SQL  := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                 values (' ||
                         TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID1) || ', ' ||
                         TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                         TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE1) || ''');';
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID2 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE2 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID2) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE2) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID3 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE3 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID3) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE3) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID4 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE4 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID4) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE4) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID5 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE5 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID5) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE5) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID6 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE6 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID6) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE6) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID7 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE7 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID7) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE7) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID8 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE8 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID8) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE8) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID9 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE9 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID9) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE9) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID10 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE10 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID10) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE10) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID11 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE11 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID11) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE11) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID12 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE12 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID12) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE12) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID13 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE13 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID13) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE13) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID14 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE14 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID14) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE14) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID15 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE15 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID15) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE15) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID16 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE16 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID16) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE16) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID17 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE17 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID17) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE17) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID18 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE18 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID18) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE18) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID19 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE19 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID19) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE19) || ''');';
            end if;
            if L_ILR_IMPORT(L_INDEX)
             .CLASS_CODE_ID20 is not null and L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE20 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ILR_CLASS_CODE (class_code_id, ilr_id, value)
                                    values (' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_ID20) || ', ' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).ILR_ID) || ', ''' ||
                           TO_CHAR(L_ILR_IMPORT(L_INDEX).CLASS_CODE_VALUE20) || ''');';
            end if;
         end if;
      end loop;
      L_CC_SQL := L_CC_SQL || ' END;';
      if CC_NEEDED then
         L_MSG := L_CC_SQL;
         execute immediate L_CC_SQL;
      end if;

     select * bulk collect into L_ILR_IMPORT from LS_IMPORT_ILR where IMPORT_RUN_ID = A_RUN_ID;

      forall L_INDEX in indices of L_ILR_IMPORT
         insert into LS_ILR_PAYMENT_TERM
            (ILR_ID, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE, PAYMENT_FREQ_ID,
             NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT, REVISION,
             C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6, C_BUCKET_7,
             C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3, E_BUCKET_4,
             E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         values
            (ABS(L_ILR_IMPORT(L_INDEX).ILR_ID), L_ILR_IMPORT(L_INDEX).PAYMENT_TERM_ID, 2,
             TO_DATE(L_ILR_IMPORT(L_INDEX).PAYMENT_TERM_DATE, 'yyyymm'),
             L_ILR_IMPORT(L_INDEX).PAYMENT_FREQ_ID, L_ILR_IMPORT(L_INDEX).NUMBER_OF_TERMS,
             L_ILR_IMPORT(L_INDEX).EST_EXECUTORY_COST, L_ILR_IMPORT(L_INDEX).PAID_AMOUNT,
             L_ILR_IMPORT(L_INDEX).CONTINGENT_AMOUNT, 1, L_ILR_IMPORT(L_INDEX).C_BUCKET_1,
             L_ILR_IMPORT(L_INDEX).C_BUCKET_2, L_ILR_IMPORT(L_INDEX).C_BUCKET_3,
             L_ILR_IMPORT(L_INDEX).C_BUCKET_4, L_ILR_IMPORT(L_INDEX).C_BUCKET_5,
             L_ILR_IMPORT(L_INDEX).C_BUCKET_6, L_ILR_IMPORT(L_INDEX).C_BUCKET_7,
             L_ILR_IMPORT(L_INDEX).C_BUCKET_8, L_ILR_IMPORT(L_INDEX).C_BUCKET_9,
             L_ILR_IMPORT(L_INDEX).C_BUCKET_10, L_ILR_IMPORT(L_INDEX).E_BUCKET_1,
             L_ILR_IMPORT(L_INDEX).E_BUCKET_2, L_ILR_IMPORT(L_INDEX).E_BUCKET_3,
             L_ILR_IMPORT(L_INDEX).E_BUCKET_4, L_ILR_IMPORT(L_INDEX).E_BUCKET_5,
             L_ILR_IMPORT(L_INDEX).E_BUCKET_6, L_ILR_IMPORT(L_INDEX).E_BUCKET_7,
             L_ILR_IMPORT(L_INDEX).E_BUCKET_8, L_ILR_IMPORT(L_INDEX).E_BUCKET_9,
             L_ILR_IMPORT(L_INDEX).E_BUCKET_10);

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_ILRS;

   function F_VALIDATE_ILRS(A_RUN_ID in number) return varchar2 is
      L_MSG varchar2(2000);
   begin
      --check we don't have duplicate ILR numbers
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate ilr number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and ILR_NUMBER in (select ILR_NUMBER
                              from (select ILR_NUMBER, count(*) as MY_COUNT
                                      from (select ILR_NUMBER
                                              from LS_ILR
                                            union all
                                            select ILR_NUMBER
                                              from (select distinct ILR_NUMBER, UNIQUE_ILR_IDENTIFIER
                                                      from LS_IMPORT_ILR
                                                     where IMPORT_RUN_ID = A_RUN_ID))
                                     group by ILR_NUMBER)
                             where MY_COUNT > 1);

      --check we don't have duplicate external ilrs
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate external ilr.'
       where IMPORT_RUN_ID = A_RUN_ID
         and EXTERNAL_ILR in (select EXTERNAL_ILR
                                from (select EXTERNAL_ILR, count(*) as MY_COUNT
                                        from (select EXTERNAL_ILR
                                                from LS_ILR
                                              union all
                                              select EXTERNAL_ILR
                                                from (select distinct EXTERNAL_ILR, UNIQUE_ILR_IDENTIFIER
                                          from LS_IMPORT_ILR
                                               where IMPORT_RUN_ID = A_RUN_ID))
                                       group by EXTERNAL_ILR)
                               where MY_COUNT > 1
                                 and EXTERNAL_ILR is not null);

      --check the payment terms have sequential payment term ids
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non sequential payment term ids.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LINE_ID in (select LINE_ID
                           from (select LINE_ID,
                                        PAYMENT_TERM_ID,
                                        ROW_NUMBER() OVER(partition by UNIQUE_ILR_IDENTIFIER order by UNIQUE_ILR_IDENTIFIER, PAYMENT_TERM_ID) MY_TERMS
                                   from LS_IMPORT_ILR
                                  where IMPORT_RUN_ID = A_RUN_ID)
                          where PAYMENT_TERM_ID <> MY_TERMS);

      --check that one term ends where the next begins
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Start of ILR term does not match with end of previous term.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LINE_ID in
             (select LINE_ID
                from (select LINE_ID,
                             TERM_START,
                             LAG(TERM_END, 1, TO_DATE('07/04/1776', 'mm/dd/yyyy')) OVER(partition by UNIQUE_ILR_IDENTIFIER order by UNIQUE_ILR_IDENTIFIER, TERM_START) as TERM_END
                        from (select LINE_ID,
                                     UNIQUE_ILR_IDENTIFIER,
                                     TO_DATE(PAYMENT_TERM_DATE, 'yyyymm') as TERM_START,
                                     ADD_MONTHS(TO_DATE(PAYMENT_TERM_DATE, 'yyyymm'),
                                                NUMBER_OF_TERMS *
                                                DECODE(PAYMENT_FREQ_ID, 1, 12, 2, 6, 3, 3, 4, 1)) as TERM_END
                                from LS_IMPORT_ILR
                               where IMPORT_RUN_ID = A_RUN_ID))
               where TERM_START <> TERM_END
                 and TERM_END <> TO_DATE('07/04/1776', 'mm/dd/yyyy'));

      --make sure the company is valid for the lease
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Company not valid for this MLA.'
       where IMPORT_RUN_ID = A_RUN_ID
         and COMPANY_ID not in
             (select COMPANY_ID from LS_LEASE_COMPANY where LEASE_ID = LS_IMPORT_ILR.LEASE_ID);

      --check that any buckets being imported are enabled.
      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 1 is not enabled.'
       where not (E_BUCKET_1 is null or E_BUCKET_1 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 1
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 1));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 2 is not enabled.'
       where not (E_BUCKET_2 is null or E_BUCKET_2 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 2
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 2));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 3 is not enabled.'
       where not (E_BUCKET_3 is null or E_BUCKET_3 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 3
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 3));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 4 is not enabled.'
       where not (E_BUCKET_4 is null or E_BUCKET_4 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 4
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 4));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 5 is not enabled.'
       where not (E_BUCKET_5 is null or E_BUCKET_5 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 5
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 5));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 6 is not enabled.'
       where not (E_BUCKET_6 is null or E_BUCKET_6 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 6
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 6));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 7 is not enabled.'
       where not (E_BUCKET_7 is null or E_BUCKET_7 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 7
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 7));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 8 is not enabled.'
       where not (E_BUCKET_8 is null or E_BUCKET_8 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 8
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 8));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 9 is not enabled.'
       where not (E_BUCKET_9 is null or E_BUCKET_9 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 9
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 9));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Executory bucket 10 is not enabled.'
       where not (E_BUCKET_10 is null or E_BUCKET_10 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Executory'
                         and BUCKET_NUMBER = 10
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Executory'
                  and BUCKET_NUMBER = 10));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 1 is not enabled.'
       where not (C_BUCKET_1 is null or C_BUCKET_1 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 1
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 1));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 2 is not enabled.'
       where not (C_BUCKET_2 is null or C_BUCKET_2 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 2
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 2));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 3 is not enabled.'
       where not (C_BUCKET_3 is null or C_BUCKET_3 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 3
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 3));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 4 is not enabled.'
       where not (C_BUCKET_4 is null or C_BUCKET_4 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 4
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 4));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 5 is not enabled.'
       where not (C_BUCKET_5 is null or C_BUCKET_5 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 5
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 5));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 6 is not enabled.'
       where not (C_BUCKET_6 is null or C_BUCKET_6 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 6
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 6));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 7 is not enabled.'
       where not (C_BUCKET_7 is null or C_BUCKET_7 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 7
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 7));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 8 is not enabled.'
       where not (C_BUCKET_8 is null or C_BUCKET_8 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 8
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 8));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 9 is not enabled.'
       where not (C_BUCKET_9 is null or C_BUCKET_9 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 9
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 9));

      update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Contingent bucket 10 is not enabled.'
       where not (C_BUCKET_10 is null or C_BUCKET_10 = 0)
         and IMPORT_RUN_ID = A_RUN_ID
         and (exists (select *
                        from LS_RENT_BUCKET_ADMIN
                       where RENT_TYPE = 'Contingent'
                         and BUCKET_NUMBER = 10
                         and STATUS_CODE_ID = 0) or not exists
              (select *
                 from LS_RENT_BUCKET_ADMIN
                where RENT_TYPE = 'Contingent'
                  and BUCKET_NUMBER = 10));

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Interest Accrual Account.'
       where INT_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = INT_ACCRUAL_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Interest Expense Account.'
       where INT_EXPENSE_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = INT_EXPENSE_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Executory Accrual Account.'
       where EXEC_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = EXEC_ACCRUAL_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Executory Expense Account.'
       where EXEC_EXPENSE_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = EXEC_EXPENSE_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Contingent Accrual Account.'
       where CONT_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = CONT_ACCRUAL_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Contingent Expense Account.'
       where CONT_ACCRUAL_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = CONT_ACCRUAL_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Capital Asset Account.'
       where CAP_ASSET_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = CAP_ASSET_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Short-term Obligation Account.'
       where ST_OBLIG_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = ST_OBLIG_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Long-term Obligation Account.'
       where LT_OBLIG_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = LT_OBLIG_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease AP Account.'
       where AP_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = AP_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Residual Debit Account.'
       where RES_DEBIT_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = RES_DEBIT_ACCOUNT_ID) <> 11;

     update LS_IMPORT_ILR
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Non-lease Residual Credit Account.'
       where RES_CREDIT_ACCOUNT_ID is not null
         and IMPORT_RUN_ID = A_RUN_ID
         and (select GL_ACCOUNT.ACCOUNT_TYPE_ID
            from GL_ACCOUNT
            where GL_ACCOUNT.GL_ACCOUNT_ID = RES_CREDIT_ACCOUNT_ID) <> 11;

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_ILRS;
   --##########################################################################
   --                            ASSETS
   --##########################################################################
   function F_IMPORT_ASSETS(A_RUN_ID in number) return varchar2 is
      L_MSG          varchar2(2000);
      L_ASSET_IMPORT ASSET_IMPORT_TABLE;
      L_INDEX        number;
      L_CC_SQL       clob;
      CC_NEEDED      boolean;
   begin

      update LS_IMPORT_ASSET set LS_ASSET_ID = LS_ASSET_SEQ.NEXTVAL;

      select * bulk collect
        into L_ASSET_IMPORT
        from LS_IMPORT_ASSET
       where IMPORT_RUN_ID = A_RUN_ID;

      forall L_INDEX in indices of L_ASSET_IMPORT
         insert into LS_ASSET
            (LS_ASSET_ID, LS_ASSET_STATUS_ID, LEASED_ASSET_NUMBER, DESCRIPTION, LONG_DESCRIPTION,
             QUANTITY, FMV, COMPANY_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_ID,
             RETIREMENT_UNIT_ID, PROPERTY_GROUP_ID, WORK_ORDER_ID, ASSET_LOCATION_ID,
             GUARANTEED_RESIDUAL_AMOUNT, EXPECTED_LIFE, ECONOMIC_LIFE, NOTES, ILR_ID, SERIAL_NUMBER,
             IMPORT_RUN_ID, TAX_ASSET_LOCATION_ID, DEPARTMENT_ID)
            select L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID,
                   1,
                   L_ASSET_IMPORT(L_INDEX).LEASED_ASSET_NUMBER,
                   L_ASSET_IMPORT(L_INDEX).DESCRIPTION,
                   L_ASSET_IMPORT(L_INDEX).LONG_DESCRIPTION,
                   L_ASSET_IMPORT(L_INDEX).QUANTITY,
                   L_ASSET_IMPORT(L_INDEX).FMV,
                   L_ASSET_IMPORT(L_INDEX).COMPANY_ID,
                   L_ASSET_IMPORT(L_INDEX).BUS_SEGMENT_ID,
                   L_ASSET_IMPORT(L_INDEX).UTILITY_ACCOUNT_ID,
                   L_ASSET_IMPORT(L_INDEX).SUB_ACCOUNT_ID,
                   L_ASSET_IMPORT(L_INDEX).RETIREMENT_UNIT_ID,
                   L_ASSET_IMPORT(L_INDEX).PROPERTY_GROUP_ID,
                   L_ASSET_IMPORT(L_INDEX).WORK_ORDER_ID,
                   L_ASSET_IMPORT(L_INDEX).ASSET_LOCATION_ID,
                   L_ASSET_IMPORT(L_INDEX).GUARANTEED_RESIDUAL_AMOUNT,
                   L_ASSET_IMPORT(L_INDEX).EXPECTED_LIFE,
                   L_ASSET_IMPORT(L_INDEX).ECONOMIC_LIFE,
                   L_ASSET_IMPORT(L_INDEX).NOTES,
                   L_ASSET_IMPORT(L_INDEX).ILR_ID,
                   L_ASSET_IMPORT(L_INDEX).SERIAL_NUMBER,
                   A_RUN_ID,
                   nvl(L_ASSET_IMPORT(L_INDEX).TAX_ASSET_LOCATION_ID, L_ASSET_IMPORT(L_INDEX).ASSET_LOCATION_ID),
                   L_ASSET_IMPORT(L_INDEX).DEPARTMENT_ID
              from DUAL;

      --Do class code inserts here
      L_CC_SQL := 'BEGIN ';
      for L_INDEX in 1 .. L_ASSET_IMPORT.COUNT
      loop
         if L_ASSET_IMPORT(L_INDEX)
          .CLASS_CODE_ID1 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE1 is not null then
            CC_NEEDED := true;
            L_CC_SQL  := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                 values (' ||
                         TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID1) || ', ' ||
                         TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                         TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE1) || ''');';
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID2 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE2 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID2) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE2) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID3 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE3 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID3) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE3) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID4 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE4 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID4) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE4) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID5 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE5 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID5) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE5) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID6 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE6 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID6) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE6) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID7 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE7 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID7) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE7) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID8 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE8 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID8) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE8) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX)
             .CLASS_CODE_ID9 is not null and L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE9 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID9) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE9) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID10 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE10 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID10) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE10) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID11 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE11 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID11) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE11) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID12 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE12 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID12) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE12) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID13 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE13 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID13) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE13) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID14 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE14 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID14) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE14) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID15 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE15 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID15) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE15) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID16 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE16 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID16) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE16) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID17 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE17 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID17) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE17) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID18 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE18 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID18) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE18) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID19 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE19 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID19) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE19) || ''');';
            end if;
            if L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID20 is not null and L_ASSET_IMPORT(L_INDEX)
               .CLASS_CODE_VALUE20 is not null then
               L_CC_SQL := L_CC_SQL || ' INSERT INTO LS_ASSET_CLASS_CODE (class_code_id, ls_asset_id, value)
                                    values (' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_ID20) || ', ' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).LS_ASSET_ID) || ', ''' ||
                           TO_CHAR(L_ASSET_IMPORT(L_INDEX).CLASS_CODE_VALUE20) || ''');';
            end if;
         end if;
      end loop;
      L_CC_SQL := L_CC_SQL || ' END;';
      if CC_NEEDED then
         L_MSG := L_CC_SQL;
         execute immediate L_CC_SQL;
      end if;

      --Insert into LS_ILR_ASSET_MAP
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, LS_ASSET_ID, REVISION)
         select IA.ILR_ID, IA.LS_ASSET_ID, 1
           from LS_IMPORT_ASSET IA
          where IMPORT_RUN_ID = A_RUN_ID
            and IA.ILR_ID is not null;

      insert into LS_ASSET_TAX_MAP
      (LS_ASSET_ID, TAX_LOCAL_ID, STATUS_CODE_ID)
      (select LA.LS_ASSET_ID LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 1 STATUS_CODE_ID
        from LS_ASSET           LA,
            LS_TAX_LOCAL       TL,
            LS_ILR,
            LS_LEASE,
            LS_LEASE_OPTIONS,
            LS_TAX_STATE_RATES,
            ASSET_LOCATION,
            LS_IMPORT_ASSET
       where LA.ILR_ID = LS_ILR.ILR_ID
         and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID
         and LS_LEASE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
         and LS_LEASE.CURRENT_REVISION = LS_LEASE_OPTIONS.REVISION
         and LS_LEASE_OPTIONS.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID
         and TL.TAX_LOCAL_ID = LS_TAX_STATE_RATES.TAX_LOCAL_ID
         and LS_TAX_STATE_RATES.STATE_ID = ASSET_LOCATION.STATE_ID
         and LA.TAX_ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID
         and LA.LS_ASSET_ID = LS_IMPORT_ASSET.LS_ASSET_ID
         and LS_IMPORT_ASSET.IMPORT_RUN_ID = A_RUN_ID
         and LS_IMPORT_ASSET.ILR_ID is not null
      union
      select LA.LS_ASSET_ID LS_ASSET_ID, TL.TAX_LOCAL_ID TAX_LOCAL_ID, 1 STATUS_CODE_ID
        from LS_ASSET              LA,
            LS_TAX_LOCAL          TL,
            LS_ILR,
            LS_LEASE,
            LS_LEASE_OPTIONS,
            LS_TAX_DISTRICT_RATES,
            ASSET_LOCATION,
            LS_IMPORT_ASSET
       where LA.ILR_ID = LS_ILR.ILR_ID
         and LS_ILR.LEASE_ID = LS_LEASE.LEASE_ID
         and LS_LEASE.LEASE_ID = LS_LEASE_OPTIONS.LEASE_ID
         and LS_LEASE.CURRENT_REVISION = LS_LEASE_OPTIONS.REVISION
         and LS_LEASE_OPTIONS.TAX_SUMMARY_ID = TL.TAX_SUMMARY_ID
         and TL.TAX_LOCAL_ID = LS_TAX_DISTRICT_RATES.TAX_LOCAL_ID
         and LS_TAX_DISTRICT_RATES.LS_TAX_DISTRICT_ID = ASSET_LOCATION.LS_TAX_DISTRICT_ID
         and LA.TAX_ASSET_LOCATION_ID = ASSET_LOCATION.ASSET_LOCATION_ID
         and LA.LS_ASSET_ID = LS_IMPORT_ASSET.LS_ASSET_ID
         and LS_IMPORT_ASSET.IMPORT_RUN_ID = A_RUN_ID
         and LS_IMPORT_ASSET.ILR_ID is not null);



      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_IMPORT_ASSETS;

   function F_VALIDATE_ASSETS(A_RUN_ID in number) return varchar2 is
      L_MSG          varchar2(2000);
      L_ASSET_IMPORT ASSET_IMPORT_TABLE;
      L_INDEX        number;
   begin
      --check we don't have duplicate LEASED_ASSET_NUMBERs
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Duplicate leased asset number.'
       where IMPORT_RUN_ID = A_RUN_ID
         and LEASED_ASSET_NUMBER in
             (select LEASED_ASSET_NUMBER
                from (select LEASED_ASSET_NUMBER, count(*) as MY_COUNT
                        from (select LEASED_ASSET_NUMBER
                                from LS_ASSET
                              union all
                              select LEASED_ASSET_NUMBER
                                from (select distinct LEASED_ASSET_NUMBER, UNIQUE_ASSET_IDENTIFIER
                                        from LS_IMPORT_ASSET
                                       where IMPORT_RUN_ID = A_RUN_ID))
                       group by LEASED_ASSET_NUMBER)
               where MY_COUNT > 1);

      --check our CPR validations.
      --valid lease depr group
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Retirement unit not in sub_ledger -100 property unit.'
       where IMPORT_RUN_ID = A_RUN_ID
         and RETIREMENT_UNIT_ID not in
             (select RETIREMENT_UNIT_ID
                from RETIREMENT_UNIT
               where PROPERTY_UNIT_ID in
                     (select PROPERTY_UNIT_ID from PROPERTY_UNIT where SUBLEDGER_TYPE_ID = -100));

      --valid bus_segment
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This business segment is not valid for this company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and BUS_SEGMENT_ID not in
             (select BUS_SEGMENT_ID
                from COMPANY_BUS_SEGMENT_CONTROL
               where COMPANY_ID = LS_IMPORT_ASSET.COMPANY_ID);

      --valid WO for this bus_Segment/company
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' Invalid work order for this company/business segment combination.'
       where IMPORT_RUN_ID = A_RUN_ID
         and WORK_ORDER_ID not in
             (select WORK_ORDER_ID
                from WORK_ORDER_CONTROL
               where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID
                 and COMPANY_ID = LS_IMPORT_ASSET.COMPANY_ID);
      --valid asset locations
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE || ' This asset location is not valid for this company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and ASSET_LOCATION_ID not in
             (select ASSET_LOCATION_ID
                from ASSET_LOCATION
               where MAJOR_LOCATION_ID in
                     (select MAJOR_LOCATION_ID
                        from COMPANY_MAJOR_LOCATION
                       where LS_IMPORT_ASSET.COMPANY_ID = COMPANY_ID));
      --valid utility_account
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This utility account is not valid for this business segment.'
       where IMPORT_RUN_ID = A_RUN_ID
         and UTILITY_ACCOUNT_ID not in
             (select UTILITY_ACCOUNT_ID
                from UTILITY_ACCOUNT
               where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID);

      --valid retirement_unit
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This retirement unit is not valid for this utility account/business segment.'
       where IMPORT_RUN_ID = A_RUN_ID
         and RETIREMENT_UNIT_ID not in
             (select RETIREMENT_UNIT_ID
                from RETIREMENT_UNIT
               where PROPERTY_UNIT_ID in
                     (select PROPERTY_UNIT_ID
                        from UTIL_ACCT_PROP_UNIT
                       where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID
                         and UTILITY_ACCOUNT_ID = LS_IMPORT_ASSET.UTILITY_ACCOUNT_ID));

      --valid sub account...
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This sub account is not valid for this utility account/business segment.'
       where IMPORT_RUN_ID = A_RUN_ID
         and SUB_ACCOUNT_ID not in
             (select SUB_ACCOUNT_ID
                from SUB_ACCOUNT
               where BUS_SEGMENT_ID = LS_IMPORT_ASSET.BUS_SEGMENT_ID
                 and UTILITY_ACCOUNT_ID = LS_IMPORT_ASSET.UTILITY_ACCOUNT_ID);

      --valid property group
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE ||
                              ' This property group is not valid for this retirement unit.'
       where IMPORT_RUN_ID = A_RUN_ID
         and PROPERTY_GROUP_ID not in
             (select PROPERTY_GROUP_ID
                from PROP_GROUP_PROP_UNIT
               where PROPERTY_UNIT_ID =
                     (select PROPERTY_UNIT_ID
                        from RETIREMENT_UNIT
                       where RETIREMENT_UNIT_ID = LS_IMPORT_ASSET.RETIREMENT_UNIT_ID));

      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Quantities must be positive.'
       where IMPORT_RUN_ID = A_RUN_ID
         and QUANTITY < 0;

      --If an ILR is defined then make sure the company is valid
      update LS_IMPORT_ASSET
         set ERROR_MESSAGE = ERROR_MESSAGE || ' Not a valid ILR for this company.'
       where IMPORT_RUN_ID = A_RUN_ID
         and ILR_ID is not null
         and COMPANY_ID not in
             (select COMPANY_ID from LS_ILR where ILR_ID = LS_IMPORT_ASSET.ILR_ID);

      return 'OK';
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         return L_MSG;
   end F_VALIDATE_ASSETS;

end PKG_LEASE_IMPORT;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1058, 0, 10, 4, 2, 0, 37099, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037099_lease_PKG_LEASE_IMPORT.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;