/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049462_lessee_01_ilr_import_update_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/12/2017 Shane "C" Ward    Updates to Lessee ILR Import
||============================================================================
*/
ALTER TABLE ls_import_ilr add     sublease_flag              NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr add  sublease_flag_xlate        VARCHAR2(35) ;
ALTER TABLE ls_import_ilr add  sublease_id                NUMBER(22, 0) ;
ALTER TABLE ls_import_ilr add  sublease_id_xlate          VARCHAR2(254) ;
ALTER TABLE ls_import_ilr add       intent_to_purch            NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr add  intent_to_purch_xlate      VARCHAR2(35);
ALTER TABLE ls_import_ilr add  specialized_asset          NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr add  specialized_asset_xlate    VARCHAR2(35) ;
ALTER TABLE ls_import_ilr add  intercompany_lease         NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr add  intercompany_lease_xlate   VARCHAR2(35) ;
ALTER TABLE ls_import_ilr add  intercompany_company       NUMBER(22, 0) ;
ALTER TABLE ls_import_ilr add  intercompany_company_xlate VARCHAR2(254) ;

COMMENT ON COLUMN ls_import_ilr.sublease_flag IS 'ID for Yes/No on whether ILR has sublease. Is the lessor subleasing the asset?';
COMMENT ON COLUMN ls_import_ilr.sublease_flag_xlate IS 'Translate Column for ID for Yes/No on whether ILR has sublease';
COMMENT ON COLUMN ls_import_ilr.sublease_id IS 'ID of Sublease for ILR (lsr_lease)';
COMMENT ON COLUMN ls_import_ilr.sublease_id_xlate IS 'ID of Sublease for ILR (lsr_lease)';
COMMENT ON COLUMN ls_import_ilr.intent_to_purch IS 'ID for Yes/No for Intent to Purchase. Is the business planning on the lessee purchasing the asset.';
COMMENT ON COLUMN ls_import_ilr.intent_to_purch_xlate IS 'Translate Column ID for Yes/No for Intent to Purchase';
COMMENT ON COLUMN ls_import_ilr.specialized_asset IS 'ID for Yes/No for Specialized Asset. Is the asset so specialized in nature, there would be no other use for it after the term of the lease?';
COMMENT ON COLUMN ls_import_ilr.specialized_asset_xlate IS 'Translate Column ID for Yes/No for Specialized Asset';
COMMENT ON COLUMN ls_import_ilr.intercompany_lease IS 'ID for Yes/No for whether ILR is an Intercompany Lease. Is the lease with another company in the business?';
COMMENT ON COLUMN ls_import_ilr.intercompany_lease_xlate IS 'Translate Column ID for Yes/No for whether ILR is an Intercompany Lease';
COMMENT ON COLUMN ls_import_ilr.intercompany_company IS 'ID of Company ILR is associated with for Intercompany Lease. If the lease is with another company within the business, enter the company description here.';
COMMENT ON COLUMN ls_import_ilr.intercompany_company_xlate IS 'Translate Column for ID of Company ILR is associated with for Intercompany Lease';

ALTER TABLE ls_import_ilr_archive add     sublease_flag              NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr_archive add  sublease_flag_xlate        VARCHAR2(35) ;
ALTER TABLE ls_import_ilr_archive add  sublease_id                NUMBER(22, 0) ;
ALTER TABLE ls_import_ilr_archive add  sublease_id_xlate          VARCHAR2(254) ;
ALTER TABLE ls_import_ilr_archive add       intent_to_purch            NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr_archive add  intent_to_purch_xlate      VARCHAR2(35);
ALTER TABLE ls_import_ilr_archive add  specialized_asset          NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr_archive add  specialized_asset_xlate    VARCHAR2(35) ;
ALTER TABLE ls_import_ilr_archive add  intercompany_lease         NUMBER(1, 0) ;
ALTER TABLE ls_import_ilr_archive add  intercompany_lease_xlate   VARCHAR2(35) ;
ALTER TABLE ls_import_ilr_archive add  intercompany_company       NUMBER(22, 0) ;
ALTER TABLE ls_import_ilr_archive add  intercompany_company_xlate VARCHAR2(254) ;

COMMENT ON COLUMN ls_import_ilr_archive.sublease_flag IS 'ID for Yes/No on whether ILR has sublease. Is the lessor subleasing the asset?';
COMMENT ON COLUMN ls_import_ilr_archive.sublease_flag_xlate IS 'Translate Column for ID for Yes/No on whether ILR has sublease';
COMMENT ON COLUMN ls_import_ilr_archive.sublease_id IS 'ID of Sublease for ILR (lsr_lease)';
COMMENT ON COLUMN ls_import_ilr_archive.sublease_id_xlate IS 'ID of Sublease for ILR (lsr_lease)';
COMMENT ON COLUMN ls_import_ilr_archive.intent_to_purch IS 'ID for Yes/No for Intent to Purchase. Is the business planning on the lessee purchasing the asset.';
COMMENT ON COLUMN ls_import_ilr_archive.intent_to_purch_xlate IS 'Translate Column ID for Yes/No for Intent to Purchase';
COMMENT ON COLUMN ls_import_ilr_archive.specialized_asset IS 'ID for Yes/No for Specialized Asset. Is the asset so specialized in nature, there would be no other use for it after the term of the lease?';
COMMENT ON COLUMN ls_import_ilr_archive.specialized_asset_xlate IS 'Translate Column ID for Yes/No for Specialized Asset';
COMMENT ON COLUMN ls_import_ilr_archive.intercompany_lease IS 'ID for Yes/No for whether ILR is an Intercompany Lease. Is the lease with another company in the business?';
COMMENT ON COLUMN ls_import_ilr_archive.intercompany_lease_xlate IS 'Translate Column ID for Yes/No for whether ILR is an Intercompany Lease';
COMMENT ON COLUMN ls_import_ilr_archive.intercompany_company IS 'ID of Company ILR is associated with for Intercompany Lease. If the lease is with another company within the business, enter the company description here.';
COMMENT ON COLUMN ls_import_ilr_archive.intercompany_company_xlate IS 'Translate Column for ID of Company ILR is associated with for Intercompany Lease';

ALTER TABLE ls_ilr_options DROP CONSTRAINT fk_ls_ilr_sublease;

ALTER TABLE ls_ilr_options
  ADD CONSTRAINT fk_ls_ilr_sublease FOREIGN KEY (
    sublease_id
  ) REFERENCES lsr_lease (
    lease_id
  );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3938, 0, 2017, 1, 0, 0, 49462, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049462_lessee_01_ilr_import_update_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;