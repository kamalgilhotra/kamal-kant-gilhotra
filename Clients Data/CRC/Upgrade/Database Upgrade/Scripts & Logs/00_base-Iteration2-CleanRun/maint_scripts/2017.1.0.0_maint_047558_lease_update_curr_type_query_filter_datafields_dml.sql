/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047558_lease_update_curr_type_query_filter_datafields_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/18/2017 Jared Watkins    update the filter field for the updated MC Lessee 
||                                        queries to use the description, since the filter value is shown in the UI
||============================================================================
*/

update pp_any_query_criteria_fields 
set data_field = 'description' 
where id in (132,133,135) 
  and data_field = 'ls_currency_type_id'
  and column_header = 'Currency Type' 
  and display_table = 'ls_lease_currency_type';

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3584, 0, 2017, 1, 0, 0, 47588, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047558_lease_update_curr_type_query_filter_datafields_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  
  