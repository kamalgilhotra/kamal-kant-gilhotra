/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051039_lessee_01_add_sob_columns_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/09/2018 Charlie Shilling need to add SOB column to LS_ILR_PAYMENT_TERM_VAR_PAYMNT and LS_ILR_PAYMENT_TERM_STG
||============================================================================
*/
ALTER TABLE ls_ilr_payment_term_var_paymnt
ADD set_of_books_id NUMBER(22,0);

ALTER TABLE ls_ilr_payment_term_var_paymnt
ADD CONSTRAINT ls_ilr_pay_term_var_paymnt_fk4
	FOREIGN KEY (set_of_books_id)
	REFERENCES set_of_books (set_of_books_id);

--need to drop existing PK so that we can populate the set_of_books_id column
ALTER TABLE ls_ilr_payment_term_var_paymnt
DROP CONSTRAINT ls_ilr_pay_term_var_paymnt_pk drop index;

--DROP INDEX ls_ilr_pay_term_var_paymnt_pk;


ALTER TABLE ls_ilr_payment_term_stg
ADD set_of_books_id NUMBER(22,0);

--we can go ahead and change this PK because it's a temp table so nothing should be in it.
ALTER TABLE ls_ilr_payment_term_stg
DROP CONSTRAINT pk_ls_ilr_payment_term_stg drop index;

--DROP INDEX pk_ls_ilr_payment_term_stg;

ALTER TABLE ls_ilr_payment_term_stg
  ADD CONSTRAINT pk_ls_ilr_payment_term_stg PRIMARY KEY (
    ilr_id,
    revision,
    payment_term_id,
	set_of_books_id
  )
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5284, 0, 2017, 4, 0, 0, 51039, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051039_lessee_01_add_sob_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;