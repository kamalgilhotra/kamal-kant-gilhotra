/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_039395_lease_import_comments.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 08/15/2014 Daniel Motter  Adding column comments
||============================================================================
*/

comment on column LS_IMPORT_ASSET.ESTIMATED_RESIDUAL is 'The estimated residual percent.';
comment on column LS_IMPORT_ASSET.TAX_SUMMARY_ID is 'The tax summary.';
comment on column LS_IMPORT_ASSET.TAX_SUMMARY_XLATE is 'Translation field used for determining the tax summary.';

comment on column LS_IMPORT_ASSET_ARCHIVE.ESTIMATED_RESIDUAL is 'The estimated residual percent.';
comment on column LS_IMPORT_ASSET_ARCHIVE.TAX_SUMMARY_ID is 'The tax summary.';
comment on column LS_IMPORT_ASSET_ARCHIVE.TAX_SUMMARY_XLATE is 'Translation field used for determining the tax summary.';

comment on column LS_IMPORT_LEASE.LS_RECONCILE_TYPE_ID is 'The reconciliation level.';
comment on column LS_IMPORT_LEASE.LS_RECONCILE_TYPE_XLATE is 'Translation field used to determine the reconciliation level.';
comment on column LS_IMPORT_LEASE.DAYS_IN_YEAR is 'The number of days in year.';
comment on column LS_IMPORT_LEASE.CUT_OFF_DAY is 'The cut-off day.';

comment on column LS_IMPORT_LEASE_ARCHIVE.LS_RECONCILE_TYPE_ID is 'The reconciliation level.';
comment on column LS_IMPORT_LEASE_ARCHIVE.LS_RECONCILE_TYPE_XLATE is 'Translation field used to determine the reconciliation level.';
comment on column LS_IMPORT_LEASE_ARCHIVE.DAYS_IN_YEAR is 'The number of days in year.';
comment on column LS_IMPORT_LEASE_ARCHIVE.CUT_OFF_DAY is 'The cut-off day.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1359, 0, 10, 4, 3, 0, 39395, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039395_lease_import_comments.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;