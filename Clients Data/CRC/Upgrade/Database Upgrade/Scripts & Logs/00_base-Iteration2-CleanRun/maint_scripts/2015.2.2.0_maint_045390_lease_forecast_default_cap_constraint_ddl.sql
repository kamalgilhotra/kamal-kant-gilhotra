/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045390_lease_forecast_default_cap_constraint_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- --------------------  ---------------------------------
|| 2016.1.0 02/05/2016 Anand R			     Drop constraint and recreate
|| 2015.2.2.0 04/14/2016 David Haupt		 Backporting to 2015.2.2
||============================================================================
*/ 

ALTER TABLE ls_forecast_default_cap_types
DROP CONSTRAINT ls_fcst_default_cap_types_fk3;

ALTER TABLE ls_forecast_default_cap_types
  ADD CONSTRAINT ls_fcst_default_cap_types_fk3 
  FOREIGN KEY (to_cap_type_id) 
  REFERENCES ls_lease_cap_type (ls_lease_cap_type_id) ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3043, 0, 2015, 2, 2, 0, 045390, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045390_lease_forecast_default_cap_constraint_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;