/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036872_cwip_fast101.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/19/2014 Sunjin Cone
||============================================================================
*/

alter table UNITIZE_WO_LIST_TEMP add HAS_GAIN_LOSS_OCR      number(22,0);
alter table UNITIZE_WO_LIST_TEMP add HAS_ELIG_ADDS          number(22,0);
alter table UNITIZE_WO_LIST_TEMP add HAS_ELIG_RWIP          number(22,0);
alter table UNITIZE_WO_LIST_TEMP add MIN_RWIP_BATCH_UNIT_ID number(22,0);

alter table UNITIZE_ALLOC_UNITS_STG_TEMP add STATUS number(22,0);

comment on column UNITIZE_WO_LIST_TEMP.HAS_GAIN_LOSS_OCR is 'This field has a 1 value if the work order has Original Cost Retirement with URGL or SAGL needing to be processed for WO Retirements during Unitization.';
comment on column UNITIZE_WO_LIST_TEMP.HAS_ELIG_ADDS is 'This field has a 1 value if the work order has eligible Addition charges for Unitization.';
comment on column UNITIZE_WO_LIST_TEMP.HAS_ELIG_RWIP is 'This field has a 1 value if the work order has eligible RWIP charges for Unitization.';
comment on column UNITIZE_WO_LIST_TEMP.MIN_RWIP_BATCH_UNIT_ID is 'This field has the min(BATCH_UNIT_ITEM_ID) from CHARGE_GROUP_CONTROL for the first time RWIP charges unitized for the work order.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.STATUS is 'Internally set processing value used in unitization allocation: Null =  From charges; 10 = Minor addition;  11 = From wo estimates; other = original Unit_Item_Id for Late RWIP';

comment on column UNITIZE_WO_UNITS_STG.UNIT_ITEM_SOURCE is 'This field indicates if the source of the unit item creation is Actuals, Estimates, or LATE.';
comment on column UNITIZE_WO_UNITS_STG.STATUS is 'Internally set processing value used in unitization: Null =  From charges; 10 = Minor addition;  11 = From wo estimates; other = original Unit_Item_Id for Late RWIP';

create table UNITIZE_ALLOC_LATE_ADD_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 BOOK_SUMMARY_ID        number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 COST_SOURCE            varchar2(35),
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_UT      number(22,8),
 ALLOC_RATIO_BY_SUB     number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_LATE_ADD_STATS
   add constraint PK_UNITIZE_ALLOC_LATE_A
       primary key (COMPANY_ID, WORK_ORDER_ID, EXPENDITURE_TYPE_ID, BOOK_SUMMARY_ID, UNIT_ITEM_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_LATE_ADD_STATS is '(C) [04] Allocation information used to unitize late Additions charges for Late Close using the CPR dollars.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.BOOK_SUMMARY_ID is 'System-assigned identifier of the book summary of the late charge.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.COST_SOURCE is 'This field will indicate the ALLOC_STATISTIC being sourced from one of the following:  CPR_LEDGER, CPR_ACTIVITY, CPR_LDG_BASIS, or CPR_ACT_BASIS.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_LATE_ADD_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_ALLOC_LATE_RWIP_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 PROCESSING_TYPE_ID     number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 COST_SOURCE            varchar2(35),
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_UT      number(22,8),
 ALLOC_RATIO_BY_SUB     number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_LATE_RWIP_STATS
   add constraint PK_UNITIZE_ALLOC_LATE_R
       primary key (COMPANY_ID, WORK_ORDER_ID, EXPENDITURE_TYPE_ID, PROCESSING_TYPE_ID, UNIT_ITEM_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_LATE_RWIP_STATS is '(C) [04] Allocation information used to unitize late RWIP charges for Late Close using the first time unitized RWIP dollars.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.PROCESSING_TYPE_ID is 'System-assigned identifier of the processing type. The processing type of the late charge determines if the late charge is Cost of Removal, Salvage Cash, etc.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.COST_SOURCE is 'This field will indicate the ALLOC_STATISTIC being sourced from one of the following, MATCHED, ALL, where MATCHED indicates same processing type from prior unitization used. ALL indicates total RWIP dollars used.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_LATE_RWIP_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create global temporary table UNITIZE_ALLOC_LATE_BP_TEMP
(
 COMPANY_ID         number(22,0) not null,
 WORK_ORDER_ID      number(22,0) not null ,
 BOOK_SUMMARY_ID    number(22,0),
 PROCESSING_TYPE_ID number(22,0)
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_LATE_BP_TEMP is '(C) [04] A global temp table that stores the list of distinct Book Summaries of the Addition late charges or distinct Processing Types of the RWIP late charges for the work order(s) being unitized.';
comment on column UNITIZE_ALLOC_LATE_BP_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LATE_BP_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_LATE_BP_TEMP.BOOK_SUMMARY_ID is 'System-assigned identifier of the book summary of the Addition late charge.';
comment on column UNITIZE_ALLOC_LATE_BP_TEMP.PROCESSING_TYPE_ID is 'System-assigned identifier of the processing type of the RWIP late charge.';

delete from PP_SYSTEM_CONTROL_COMPANY where UPPER(CONTROL_NAME) = 'WOCLOSE - LATE UNITIZE CHILD WO';

update PP_SYSTEM_CONTROL_COMPANY
   set LONG_DESCRIPTION = '"Yes" will indicate late charge unitization will obey Unitize by Account, which is a work order attribute on Work Order Account.  "No" will override the Unitize by Account = Yes to force the Unitize by Account = No for late charge unitization.  This control will only override the Unitize by Account = Yes, and it will NOT override the Unitize by Account = No.  Default is "No."'
 where UPPER(CONTROL_NAME) = 'AUTO101 - LATE CHARGE OBEY ACCOUNT';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1369, 0, 10, 4, 3, 0, 36872, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036872_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;