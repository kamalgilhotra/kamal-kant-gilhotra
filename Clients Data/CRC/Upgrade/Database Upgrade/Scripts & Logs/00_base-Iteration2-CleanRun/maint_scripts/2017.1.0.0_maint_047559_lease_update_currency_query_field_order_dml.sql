/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047559_lease_update_currency_query_field_order_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/18/2017 Jared Watkins    update the column order for the updated MC Lessee 
||                                        queries to move the currency field before any of the amounts
||============================================================================
*/

update pp_any_query_criteria_fields
set column_order = column_order + 1
where id = (select id from pp_any_query_criteria where description = 'Lease Payment Header and Invoice Detail')
and column_order >= 5;

update pp_any_query_criteria_fields
set column_order = 5
where column_header = 'Currency'
and detail_field = 'currency'
and id = (select id from pp_any_query_criteria where description = 'Lease Payment Header and Invoice Detail');

update pp_any_query_criteria_fields
set column_order = column_order + 1
where id = (select id from pp_any_query_criteria where description = 'Lease Payment Detail by Asset')
and column_order >= 12;

update pp_any_query_criteria_fields
set column_order = 12
where column_header = 'Currency'
and detail_field = 'currency'
and id = (select id from pp_any_query_criteria where description = 'Lease Payment Detail by Asset');

update pp_any_query_criteria_fields
set column_order = column_order + 1
where id = (select id from pp_any_query_criteria where description = 'Lease Payment Detail by Company')
and column_order >= 5;

update pp_any_query_criteria_fields
set column_order = 5
where column_header = 'Currency'
and detail_field = 'currency'
and id = (select id from pp_any_query_criteria where description = 'Lease Payment Detail by Company');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3588, 0, 2017, 1, 0, 0, 47559, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047559_lease_update_currency_query_field_order_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
