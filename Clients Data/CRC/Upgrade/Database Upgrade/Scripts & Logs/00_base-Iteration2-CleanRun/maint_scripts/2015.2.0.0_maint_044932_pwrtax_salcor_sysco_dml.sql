/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044932_pwrtax_salcor_sysco_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date          Revised By         Reason for Change
 || -------- ------------- ------------------ ----------------------------------------
 || 2015.2   09/10/2015    Michael Bradley    Sets the default to 1 for all companies except if the database is FPL
 ||                                           FPL is the only customer that uses this control variable as 0 for now
 ||============================================================================
 */ 
 
SET DEFINE OFF

update pp_system_control_company
set control_value = '1'
where control_name = 'Treat Salvage_Cash as Salvage' and not exists (select 1 from company_Setup where description='0001 -Florida Power & Light Company');

update pp_system_control_company
set control_value = '1'
where control_name = 'Treat Salvage_Returns as Salvage' and not exists (select 1 from company_Setup where description='0001 -Florida Power & Light Company');

update pp_system_control_company
set control_value = '1'
where control_name = 'Treat Reserve_Credits as Salvage' and not exists (select 1 from company_Setup where description='0001 -Florida Power & Light Company');

SET DEFINE ON

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2860, 0, 2015, 2, 0, 0, 044932, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044932_pwrtax_salcor_sysco_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;