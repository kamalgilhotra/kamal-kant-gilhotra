/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033267_proptax_any_query_pt2.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------
|| 10.4.2.0 11/18/2013 Andrew Scott         Replace old query tool with base Any Query window
||                                          QA discovered some additional config to be done for this maint, so placing in pt2.sql
||============================================================================
*/

----any queries uses these to check company security
----these tables are out of date for property tax
delete from PP_REPORTS_COMPANY_SECURITY
 where LOWER(TABLE_NAME) in ('prop_tax_preallo_pull',
                             'property_tax_ledger_control',
                             'property_tax_ledger_new',
                             'pt_accrual_allowed_amt',
                             'pt_accrual_merge',
                             'pt_accrual_merge_save',
                             'pt_adjustment_ledger',
                             'pt_adjustment_transfer_history',
                             'pt_assessed_value_state',
                             'pt_cpr_pull_temp',
                             'pt_preallo_adj_trans_history',
                             'pt_preallo_adjustment_ledger',
                             'pt_type_cwip_pseudo_assign',
                             'pt_type_cwip_pseudo_levels',
                             'pt_temp_allo_incr_total',
                             'pt_temp_cprpull_basis_adjs',
                             'pt_report_county_field',
                             'pt_report_location_field',
                             'pt_report_state_field',
                             'pt_report_tax_district_field',
                             'pt_report_type_code_field');

----try to add newer tables that were missing.  put in plsql in case they already exist (keep errors from popping up).
SET SERVEROUTPUT ON

begin
   execute immediate 'INSERT INTO PP_REPORTS_COMPANY_SECURITY (TABLE_NAME) VALUES (''PT_LEDGER'')';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('PT_LEDGER ALREADY EXISTS IN PP_REPORTS_COMPANY_SECURITY.');
end;
/

begin
   execute immediate 'INSERT INTO PP_REPORTS_COMPANY_SECURITY (TABLE_NAME) VALUES (''PT_LEDGER_TAX_YEAR'')';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('PT_LEDGER_TAX_YEAR ALREADY EXISTS IN PP_REPORTS_COMPANY_SECURITY.');
end;
/

begin
   execute immediate 'INSERT INTO PP_REPORTS_COMPANY_SECURITY (TABLE_NAME) VALUES (''PT_LEDGER_ADJUSTMENT'')';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('PT_LEDGER_ADJUSTMENT ALREADY EXISTS IN PP_REPORTS_COMPANY_SECURITY.');
end;
/

begin
   execute immediate 'INSERT INTO PP_REPORTS_COMPANY_SECURITY (TABLE_NAME) VALUES (''PT_LEDGER_TRANSFER'')';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('PT_LEDGER_TRANSFER ALREADY EXISTS IN PP_REPORTS_COMPANY_SECURITY.');
end;
/

begin
   execute immediate 'INSERT INTO PP_REPORTS_COMPANY_SECURITY (TABLE_NAME) VALUES (''PT_PREALLO_ADJUSTMENT'')';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('PT_PREALLO_ADJUSTMENT ALREADY EXISTS IN PP_REPORTS_COMPANY_SECURITY.');
end;
/

begin
   execute immediate 'INSERT INTO PP_REPORTS_COMPANY_SECURITY (TABLE_NAME) VALUES (''PT_PREALLO_TRANSFER'')';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('PT_PREALLO_TRANSFER ALREADY EXISTS IN PP_REPORTS_COMPANY_SECURITY.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (763, 0, 10, 4, 2, 0, 33267, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033267_proptax_any_query_pt2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
