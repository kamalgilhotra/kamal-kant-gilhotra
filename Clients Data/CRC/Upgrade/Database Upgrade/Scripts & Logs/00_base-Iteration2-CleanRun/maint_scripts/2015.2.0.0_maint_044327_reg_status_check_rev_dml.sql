/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044327_reg_status_check_rev_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2 08/10/2015   Shane "C" Ward Updated Case Status Checks
||============================================================================
*/

UPDATE reg_status_check SET check_sql =  'SELECT ''Account '' || nvl(aa.long_description, aa.reg_acct_id) || '' in case '' || c.case_name || '' does not have an allocator assigned.'', aa.reg_alloc_category_id, aa.reg_alloc_acct_id, aa.reg_allocator_id, c.case_name FROM reg_case c, reg_case_alloc_account aa, (select step_order, category_id, category_descr, parent_category_id, parent_category_descr, reg_case_id from (select c.step_order    step_order, c.reg_alloc_category_id      category_id, ac.description               category_descr, p.reg_alloc_category_id      parent_category_id, ap.description               parent_category_descr, c.reg_case_id from reg_case_alloc_steps c, reg_case_alloc_steps p, reg_alloc_category   ac, reg_alloc_category   ap where c.reg_case_id = p.reg_case_id and c.step_order = p.step_order + 1 and c.step_order >= 2 AND c.step_order <= (SELECT z.step_order FROM reg_case_alloc_steps z WHERE z.reg_case_id = c.reg_case_id AND z.jur_result_flag = 1) and c.reg_alloc_category_id = ac.reg_alloc_category_id and p.reg_alloc_category_id = ap.reg_alloc_category_id)) steps WHERE c.reg_case_id = <case_id> AND c.reg_case_id = steps.reg_case_id AND steps.reg_case_id = aa.reg_case_id AND steps.parent_category_id = aa.reg_alloc_category_id AND aa.reg_calc_status_id not in (-9, -2, -1) AND aa.reg_allocator_id IS NULL and aa.reg_acct_id <> 0 AND (aa.reg_alloc_target_id) IN (SELECT tf.reg_alloc_target_id FROM reg_case_target_forward tf WHERE tf.reg_case_id = <case_id> AND tf.carry_forward_dollars = 1 UNION SELECT 0 FROM dual) AND step_order < (SELECT Max(step_order) FROM reg_case_alloc_steps WHERE reg_case_id = <case_id>)'
                            WHERE reg_status_check_id = 4;
                            
UPDATE reg_status_check SET check_sql =  'SELECT ''Account '' || nvl(aa.long_description, aa.reg_acct_id) || '' in case '' || c.case_name || '' does not have an allocator assigned.'', aa.reg_alloc_category_id, aa.reg_alloc_acct_id, aa.reg_allocator_id, c.case_name FROM reg_case c, reg_case_alloc_account aa, (select step_order, category_id, category_descr, parent_category_id, parent_category_descr, reg_case_id from (select c.step_order                 step_order, c.reg_alloc_category_id      category_id, ac.description               category_descr, p.reg_alloc_category_id      parent_category_id, ap.description               parent_category_descr, c.reg_case_id from reg_case_alloc_steps c, reg_case_alloc_steps p, reg_alloc_category   ac, reg_alloc_category   ap where c.reg_case_id = p.reg_case_id and c.step_order = p.step_order + 1 AND c.step_order > (SELECT z.step_order FROM reg_case_alloc_steps z WHERE z.reg_case_id = c.reg_case_id AND z.jur_result_flag = 1) AND c.step_order <= (SELECT z.step_order FROM reg_case_alloc_steps z WHERE z.reg_case_id = c.reg_case_id AND z.recovery_class_flag = 1) and c.reg_alloc_category_id = ac.reg_alloc_category_id and p.reg_alloc_category_id = ap.reg_alloc_category_id)) steps WHERE c.reg_case_id = <case_id> AND c.reg_case_id = steps.reg_case_id AND steps.reg_case_id = aa.reg_case_id AND steps.parent_category_id = aa.reg_alloc_category_id and aa.reg_acct_id <> 0 AND aa.reg_calc_status_id not in (-9, -2, -1) AND aa.reg_allocator_id IS NULL AND (aa.reg_alloc_target_id) IN (SELECT tf.reg_alloc_target_id FROM reg_case_target_forward tf WHERE tf.reg_case_id = <case_id> AND tf.carry_forward_dollars = 1 UNION SELECT 0 FROM dual) AND step_order < (SELECT Max(step_order) FROM reg_case_alloc_steps WHERE reg_case_id = <case_id>)'
                            WHERE reg_status_check_id = 5;
                            
UPDATE reg_status_check SET check_sql =  'SELECT '' Account '' || nvl(aa.long_description, aa.reg_acct_id) || '' in case '' || c.case_name || '' does not have an allocator assigned. '', aa.reg_alloc_category_id, aa.reg_alloc_acct_id, aa.reg_allocator_id, c.case_name FROM reg_case c, reg_case_alloc_account aa, (select step_order, category_id, category_descr, parent_category_id, parent_category_descr, reg_case_id, jur_result_flag, class_cost_of_service_flag, recovery_class_flag from (select c.step_order                 step_order, c.reg_alloc_category_id      category_id, ac.description               category_descr, p.reg_alloc_category_id      parent_category_id, ap.description               parent_category_descr, c.reg_case_id, c.jur_result_flag, c.class_cost_of_service_flag, c.recovery_class_flag from reg_case_alloc_steps c, reg_case_alloc_steps p, reg_alloc_category   ac, reg_alloc_category   ap where c.reg_case_id = p.reg_case_id and c.step_order = p.step_order + 1 AND c.step_order > (SELECT z.step_order FROM reg_case_alloc_steps z WHERE z.reg_case_id = c.reg_case_id AND z.jur_result_flag = 1) AND c.step_order <= (SELECT z.step_order FROM reg_case_alloc_steps z WHERE z.reg_case_id = c.reg_case_id AND z.recovery_class_flag = 1) and c.reg_alloc_category_id = ac.reg_alloc_category_id and p.reg_alloc_category_id = ap.reg_alloc_category_id)) steps WHERE c.reg_case_id = <case_id> AND c.reg_case_id = steps.reg_case_id AND steps.reg_case_id = aa.reg_case_id AND steps.parent_category_id = aa.reg_alloc_category_id AND aa.reg_calc_status_id not in (-9, -2, -1) and aa.reg_acct_id <> 0 AND aa.reg_allocator_id IS NULL AND (aa.reg_alloc_target_id) IN (SELECT tf.reg_alloc_target_id FROM reg_case_target_forward tf WHERE tf.reg_case_id = <case_id> AND tf.carry_forward_dollars = 1 UNION SELECT 0 FROM dual) AND step_order < (SELECT Max(step_order) FROM reg_case_alloc_steps WHERE reg_case_id = <case_id>)'
                            WHERE reg_status_check_id = 13;                                                        

UPDATE reg_status_check SET check_sql = 'SELECT distinct ''Adjustment '' || a.description || '' parameters are not complete in case '' || c.case_name || ''
        and test year '' || ac.test_yr_ended || ''.'', ac.adjustment_id
    FROM reg_case c,
                reg_case_adjustment a,
                reg_case_adjust_acct ac
            WHERE c.reg_case_id = a.reg_case_id
                        and a.reg_case_id = ac.reg_case_id
                        and a.adjustment_id = ac.adjustment_id
                        and a.adjustment_id <> 0
                        and a.adjustment_type_id <> 5
                        and ac.input_percentage is null
                        and ac.input_amount is null
                        and c.reg_case_id = <case_id>
                        and ac.test_yr_ended = <case_year>'
                        WHERE reg_status_check_id = 16;
						
						
						
UPDATE reg_status_check SET check_sql = 'SELECT distinct ''Adjustment '' || a.description || '' monthly parameters are not complete in case '' || c.case_name || '' 
        and test year '' || ac.test_yr_ended || ''.'', ac.adjustment_id 
    FROM reg_case c,
                reg_case_adjustment a,
                reg_case_adjust_acct_monthly ac 
            WHERE c.reg_case_id = a.reg_case_id 
                        and a.reg_case_id = ac.reg_case_id 
                        and a.adjustment_id = ac.adjustment_id
                        AND a.adjustment_timing = 1 
                        and a.adjustment_id <> 0 
                        and ac.input_amount is null 
                        and c.reg_case_id = <case_id>'
                        WHERE reg_status_check_id =17;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2764, 0, 2015, 2, 0, 0, 044327, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044327_reg_status_check_rev_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;