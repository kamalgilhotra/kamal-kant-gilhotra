/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050378_lessor_01_add_je_codes_for_rerecognition_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/03/2018 Jared Watkins    Create Lessor JE codes and Disposition Codes for Asset Rerecognition
||============================================================================
*/

--STANDARD_JOURNAL_ENTRIES
insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1,
          'LSR Rerecognition',
          'Lessor Rerecognition',
          'Lessor Rerecognition',
          'Lessor Rerecognition'
     from STANDARD_JOURNAL_ENTRIES A;

insert into GL_JE_CONTROL
   (PROCESS_ID, JE_ID, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN)
   select S.GL_JE_CODE, S.JE_ID, 'NONE', 'NONE', 'NONE', 'NONE'
     from STANDARD_JOURNAL_ENTRIES S
    where trim(S.GL_JE_CODE) in ('LSR Rerecognition');
	
	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7502, 0, 2017, 4, 0, 0, 50378, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050378_lessor_01_add_je_codes_for_rerecognition_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;