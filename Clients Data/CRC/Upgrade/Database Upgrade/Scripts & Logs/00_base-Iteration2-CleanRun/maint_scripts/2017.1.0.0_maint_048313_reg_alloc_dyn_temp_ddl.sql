 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048313_reg_alloc_dyn_temp_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 06/20/2017 Sarah Byers    New temp tables for RMS dynamic allocation performance rewrite
 ||============================================================================
 */ 

CREATE GLOBAL TEMPORARY TABLE reg_case_alloc_dyn_factor_temp (
reg_case_id NUMBER(22,0) NOT NULL,
case_year NUMBER(22,0) NOT NULL,
reg_allocator_id NUMBER(22,0) NOT NULL,
reg_alloc_category_id NUMBER(22,0) NOT NULL,
reg_alloc_target_id NUMBER(22,0) NOT NULL,
row_number NUMBER(22,0) NOT NULL,
amount NUMBER(22,2) NOT NULL
) ON COMMIT PRESERVE rows;

ALTER TABLE reg_case_alloc_dyn_factor_temp ADD (
CONSTRAINT pk_reg_case_adyn_factor_temp PRIMARY KEY (reg_case_id, case_year, reg_allocator_id, reg_alloc_category_id, reg_alloc_target_id, row_number));

comment on table reg_case_alloc_dyn_factor_temp is 'Stores the allocated basis amounts for each dynamic allocator basis row for use in calculating allocation factors.';
comment on column reg_case_alloc_dyn_factor_temp.reg_case_id is 'System assigned identifier of a regulatory case.';
comment on column reg_case_alloc_dyn_factor_temp.case_year is 'Regulatory case year in YYYYMM format.';
comment on column reg_case_alloc_dyn_factor_temp.reg_allocator_id is 'System assigned identifier of a regulatory allocator.';
comment on column reg_case_alloc_dyn_factor_temp.reg_alloc_category_id is 'System assigned identifier of a regulatory allocation category.';
comment on column reg_case_alloc_dyn_factor_temp.reg_alloc_target_id is 'System assigned identifier of a regulatory allocation target.';
comment on column reg_case_alloc_dyn_factor_temp.row_number is 'System assigned dynamic allocator basis row number';
comment on column reg_case_alloc_dyn_factor_temp.amount is 'The allocated amount for the dynamic allocator basis row.';

CREATE GLOBAL TEMPORARY TABLE reg_case_alloc_dyn_list_temp (
reg_case_id NUMBER(22,0) NOT NULL,
case_year NUMBER(22,0) NOT NULL,
reg_alloc_category_id NUMBER(22,0) NOT NULL,
reg_allocator_id NUMBER(22,0) NOT NULL,
processed NUMBER(1,0)
) ON COMMIT PRESERVE ROWS;

ALTER TABLE reg_case_alloc_dyn_list_temp ADD (
CONSTRAINT pk_reg_case_adyn_list_temp PRIMARY KEY (reg_case_id, case_year, reg_alloc_category_id, reg_allocator_id));

comment on table reg_case_alloc_dyn_list_temp is 'Stores the dynamic allocators to be processed by case, year, and allocation category.';
comment on column reg_case_alloc_dyn_list_temp.reg_case_id is 'System assigned identifier of a regulatory case.';
comment on column reg_case_alloc_dyn_list_temp.case_year is 'Regulatory case year in YYYYMM format.';
comment on column reg_case_alloc_dyn_list_temp.reg_alloc_category_id is 'System assigned identifier of a regulatory allocation category.';
comment on column reg_case_alloc_dyn_list_temp.reg_allocator_id is 'System assigned identifier of a regulatory allocator.';
comment on column reg_case_alloc_dyn_list_temp.processed is '0 = needs to be processed; 1 = all basis amounts have been calculated; 2 = factors have been calculated; 3 = allocated';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3528, 0, 2017, 1, 0, 0, 48313, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048313_reg_alloc_dyn_temp_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
