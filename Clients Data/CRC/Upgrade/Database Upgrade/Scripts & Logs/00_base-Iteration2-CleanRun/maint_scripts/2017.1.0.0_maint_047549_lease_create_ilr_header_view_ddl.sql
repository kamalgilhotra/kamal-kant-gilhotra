/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047549_lease_create_ilr_header_view_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/24/2017 Jared Watkins    create a multicurrency view for ILR header fields
||                                        that are needed for reports/queries
||============================================================================
*/


CREATE OR REPLACE VIEW v_ls_ilr_header_fx_vw AS
WITH
cur AS (
  SELECT /*+ materialize */ ls_cur_type, currency_id, currency_display_symbol, iso_code
  FROM (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id, contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id, company_cur.currency_display_symbol, company_cur.iso_code
    FROM currency company_cur
  )
),
open_month AS (
  SELECT /*+ materialize */ company_id, Min(gl_posting_mo_yr) open_month
  FROM ls_process_control
  WHERE open_next IS NULL
  GROUP BY company_id
),
cr AS (
  SELECT /*+ materialize */ exchange_date, currency_from, currency_to, rate
  FROM currency_rate_default
)
SELECT /*+ no_merge */ 
  ilr.ilr_id,
  ilr.ilr_number,
  liasob.revision,
  liasob.set_of_books_id,
  cur.ls_cur_type, 
  lio.in_service_exchange_rate,
  cr.rate,
  cur.currency_display_symbol, 
  cur.iso_code,
  lease.contract_currency_id, 
  cur.currency_id,
  cr.exchange_date,
  open_month.open_month,
  ilr.lease_id,
  ilr.company_id,
  liasob.internal_rate_return,
  liasob.is_om,
  lio.purchase_option_amt * nvl(lio.in_service_exchange_rate, cr.rate) purchase_option_amt,
  lio.termination_amt * nvl(lio.in_service_exchange_rate, cr.rate) termination_amt,
  liasob.net_present_value * nvl(lio.in_service_exchange_rate, cr.rate) net_present_value,
  liasob.capital_cost * nvl(lio.in_service_exchange_rate, cr.rate) capital_cost,
  liasob.current_lease_cost * nvl(lio.in_service_exchange_rate, cr.rate) current_lease_cost
FROM ls_ilr ilr
INNER JOIN ls_ilr_amounts_set_of_books liasob
ON ilr.ilr_id = liasob.ilr_id
INNER JOIN ls_ilr_options lio
ON lio.ilr_id = liasob.ilr_id
AND lio.revision = liasob.revision
INNER JOIN ls_lease lease
ON ilr.lease_id = lease.lease_id
INNER JOIN currency_schema cs
  ON ilr.company_id = cs.company_id
INNER JOIN cur
  ON (cur.ls_cur_type = 1 AND cur.currency_id = lease.contract_currency_id)
  OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
INNER JOIN open_month
  ON ilr.company_id = open_month.company_id
INNER JOIN cr
  ON cur.currency_id = cr.currency_to
  AND lease.contract_currency_id = cr.currency_from
WHERE cr.exchange_date = (
  SELECT Max(exchange_date)
  FROM cr cr2
  WHERE cr.currency_from = cr2.currency_from
  AND cr.currency_to = cr2.currency_to
  AND cr2.exchange_date < Add_Months(open_month.open_month, 1)
)
AND cs.currency_type_id = 1
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3454, 0, 2017, 1, 0, 0, 47549, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047549_lease_create_ilr_header_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
