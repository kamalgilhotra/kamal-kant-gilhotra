/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050573_taxrpr_group_by_tax_uop_sys_ctrl_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/01/2017 Eric Berger    Create the wo_status processing tables
||										and pl/sql package.
||============================================================================
*/

INSERT INTO PPBASE_SYSTEM_OPTIONS
            (system_option_id,
             long_description,
             system_only,
             pp_default_value,
             option_value,
             is_base_option,
             allow_company_override)
SELECT 'Repairs - CWIP Method - Estimate Total Qty by UOP',
             '"Yes" will test the replacement threshold against the quantity or cost on that Work Order for the chosen Tax Unit of Property. "No" tests the replacement threshold against the total cost or quantity on that Work Order.',
             0,
             'Yes',
             'Yes',
             1,
             0
FROM dual
WHERE NOT EXISTS ( 
    SELECT 1
    FROM PPBASE_SYSTEM_OPTIONS
    WHERE Upper(system_option_id) = 'REPAIRS - CWIP METHOD - ESTIMATE TOTAL QTY BY UOP'
);

INSERT INTO PPBASE_SYSTEM_OPTIONS_MODULE
            (system_option_id,
             MODULE)
SELECT 'Repairs - CWIP Method - Estimate Total Qty by UOP',
             'REPAIRS'
FROM dual
WHERE NOT EXISTS (
    SELECT 1
    FROM PPBASE_SYSTEM_OPTIONS_MODULE
    WHERE Upper(system_option_id) = 'REPAIRS - CWIP METHOD - ESTIMATE TOTAL QTY BY UOP'
    AND Upper(MODULE) = 'REPAIRS'
);

INSERT INTO ppbase_system_options_values (SYSTEM_OPTION_ID, OPTION_VALUE)
SELECT 'Repairs - CWIP Method - Estimate Total Qty by UOP','Yes'
FROM dual
WHERE NOT EXISTS (
    SELECT 1
    FROM ppbase_system_options_values
    WHERE Upper(system_option_id) = 'REPAIRS - CWIP METHOD - ESTIMATE TOTAL QTY BY UOP'
    AND Upper(option_value) = 'YES'
)
;

INSERT INTO ppbase_system_options_values (SYSTEM_OPTION_ID, OPTION_VALUE)
SELECT 'Repairs - CWIP Method - Estimate Total Qty by UOP','No'
FROM dual
WHERE NOT EXISTS (
    SELECT 1
    FROM ppbase_system_options_values
    WHERE Upper(system_option_id) = 'REPAIRS - CWIP METHOD - ESTIMATE TOTAL QTY BY UOP'
    AND Upper(option_value) = 'NO'
)
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13741, 0, 2018, 2, 0, 0, 50573, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_050573_taxrpr_group_by_tax_uop_sys_ctrl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;