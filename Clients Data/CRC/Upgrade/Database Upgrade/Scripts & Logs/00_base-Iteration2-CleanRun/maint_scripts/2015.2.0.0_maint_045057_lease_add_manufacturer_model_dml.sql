/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045057_lease_add_manufacturer_model_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/07/2015 Will Davis	 	  Add manufacturer and model to component table
||============================================================================
*/

insert into pp_import_column
(import_type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
values (
(select import_type_id from pp_import_type where description = 'Add: Lease Components'), 
'Manufacturer',
'Manufacturer', 
0, 
2,
'varchar2(36)',
1);

insert into pp_import_column
(import_type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
values (
(select import_type_id from pp_import_type where description = 'Add: Lease Components'), 
'Model',
'Model', 
0, 
2,
'varchar2(36)',
1);

insert into pp_import_template_fields
(import_template_id, field_id, import_type_id, column_name)
values (
(select import_template_id from pp_import_template where description = 'Lease Component Add'),
(select max(field_id) + 1 from pp_import_template_fields 
    where import_template_id = (select import_template_id from pp_import_template where description = 'Lease Component Add')),
 (select import_type_id from pp_import_type where description = 'Add: Lease Components'), 
'Manufacturer');

insert into pp_import_template_fields
(import_template_id, field_id, import_type_id, column_name)
values (
(select import_template_id from pp_import_template where description = 'Lease Component Add'),
(select max(field_id) + 1 from pp_import_template_fields 
    where import_template_id = (select import_template_id from pp_import_template where description = 'Lease Component Add')),
 (select import_type_id from pp_import_type where description = 'Add: Lease Components'), 
'Model');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2900, 0, 2015, 2, 0, 0, 45057, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045057_lease_add_manufacturer_model_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
