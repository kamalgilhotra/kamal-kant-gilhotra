/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029381_sys_pp_system_errors.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.0.0 02/25/2013 Charlie Shilling Point Release
||============================================================================
*/

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1021',
    'POST 1021: ERROR: This transaction has no row in pend_basis. All non-retirement transactions must have a pend_basis row.',
    'POST 1021: ERROR: This transaction has no row in pend_basis. All non-retirement transactions must have a pend_basis row.',
    'POST 1021: Call PPC.');

insert into PP_SYSTEM_ERRORS
   (ERROR_ID, PP_REPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION, ERROR_REPORTED, ACTION)
values
   ((select NVL(max(ERROR_ID), 0) + 1 from PP_SYSTEM_ERRORS), 14, 'POST 1022',
    'POST 1022: ERROR: Valid retire methods for mass retirements are FIFO, Retire Curve, HW - FIFO and HW - Curve. When no retire_method_id is given on PEND_TRANSACTION, Post looks up the retire_method_id on the ' ||
    'PROPERTY_UNIT_DEFAULT_LIFE table for the given property_unit, company, utility_account, and business segment. If the retire method is not found there, Post tries the same criteria with company_id -1 (all companies). ' ||
    'Finally, if the retire method is still not found, Post uses the retire method found on PROPERTY_UNIT.',
    'POST 1022: ERROR: Invalid retire_method_id found for mass retirement: ###. If this retire_method_id was not given on pend_transaction, Post looked it up on either the property_unit_default_life or property_unit tables.',
    'POST 1022: Either update the appropriate table, update the retire_method on this transaction, or delete and re-create the transaction from the start.');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (307, 0, 10, 4, 0, 0, 29381, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029381_sys_pp_system_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;