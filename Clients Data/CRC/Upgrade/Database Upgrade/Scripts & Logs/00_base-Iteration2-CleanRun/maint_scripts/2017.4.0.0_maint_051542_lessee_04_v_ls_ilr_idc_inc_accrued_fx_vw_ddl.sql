/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051542_lessee_04_v_ls_ilr_idc_inc_accrued_fx_vw_DDL.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/15/2018 David Conway     Off Balance Sheet Expense by Set of Books
||============================================================================
*/

CREATE OR replace VIEW v_ls_ilr_idc_inc_accrued_fx_vw
AS
  SELECT stg.ilr_id                                                                   ilr_id,
         stg.revision                                                                 revision,
         stg.set_of_books_id                                                          set_of_books_id,
         SUM( initial_direct_cost )                                                   accrued_idc,
         SUM( incentive_amount - Nvl( Decode( s.month, i.est_in_svc_date, 0,
                                                       incentive_math_amount ), 0 ) ) accrued_incentive,
         curr.remeasurement_date,
         stg.is_om                                                                    curr_is_om,
         s.is_om                                                                      approved_is_om,
         Decode( s.is_om, 1, Decode( stg.is_om, 0, 1 ),
                                 0 )                                                  switch_to_cap --whether or not we are converting cap types on the schedule being calculated
  FROM   LS_ILR_SCHEDULE s,
         LS_ILR_STG stg,
         LS_ILR i,
         LS_ILR_OPTIONS curr,
         LS_ILR_OPTIONS appr
  WHERE  stg.ilr_id = i.ilr_id AND
         s.ilr_id = i.ilr_id AND
         s.revision = i.current_revision AND
         i.ilr_id = appr.ilr_id AND
         i.current_revision = appr.revision AND
         stg.ilr_id = curr.ilr_id AND
         stg.revision = curr.revision AND
         s.set_of_books_id = stg.set_of_books_id AND
         s.month < stg.npv_start_date
  GROUP  BY stg.ilr_id,stg.revision,stg.set_of_books_id,curr.remeasurement_date,stg.is_om,s.is_om
  UNION
  SELECT DISTINCT ilr_id,
                  revision,
                  set_of_books_id,
                  0    accrued_idc,
                  0    accrued_incentive,
                  NULL remeasurement_date,
                  0 curr_is_om,
                  0 approved_is_om,
                  0 switch_to_cap
  FROM   LS_ILR_STG
  WHERE npv_start_date IS NULL;
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7465, 0, 2017, 4, 0, 0, 51542, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051542_lessee_04_v_ls_ilr_idc_inc_accrued_fx_vw_DDL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
