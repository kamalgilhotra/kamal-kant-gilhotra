/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030347_taxrpr_hide_rpt250.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version   Date       Revised By          Reason for Change
|| --------  ---------- ------------------- ----------------------------------
|| 10.4.1.0  08/16/2013 Andrew Scott        Hide a tax repair report
||============================================================================
*/

update PP_REPORTS set PP_REPORT_ENVIR_ID = 7 where REPORT_NUMBER = 'RPR - 0250';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (516, 0, 10, 4, 1, 0, 30347, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030347_taxrpr_hide_rpt250.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;