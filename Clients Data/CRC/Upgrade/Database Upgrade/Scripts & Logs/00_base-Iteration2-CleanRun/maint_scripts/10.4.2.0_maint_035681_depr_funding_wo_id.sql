/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035681_depr_funding_wo_id.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/03/2014  Kyle Peterson
||============================================================================
*/

alter table DEPR_CALC_STG
   add (FUNDING_WO_ID number(22,0) default -1,
        REVISION      number(22,0) default -1);

comment on column DEPR_CALC_STG.FUNDING_WO_ID is 'A system-generated identifier of a particular funding project.';
comment on column DEPR_CALC_STG.REVISION is 'The revision of the funding project. Only used for incremental forecasting.';

alter table DEPR_CALC_STG_ARC
   add (FUNDING_WO_ID number(22,0) default -1,
        REVISION      number(22,0) default -1);

comment on column DEPR_CALC_STG_ARC.FUNDING_WO_ID is 'A system-generated identifier of a particular funding project.';
comment on column DEPR_CALC_STG_ARC.REVISION is 'The revision of the funding project. Only used for incremental forecasting.';

alter table FCST_DEPR_CALC_STG_ARC
   add (FUNDING_WO_ID number(22,0) default -1,
        REVISION      number(22,0) default -1);

comment on column FCST_DEPR_CALC_STG_ARC.FUNDING_WO_ID is 'A system-generated identifier of a particular funding project.';
comment on column FCST_DEPR_CALC_STG_ARC.REVISION is 'The revision of the funding project. Only used for incremental forecasting.';

alter table CPR_DEPR_CALC_STG
   add (FUNDING_WO_ID number(22,0) default -1,
        REVISION      number(22,0) default -1);

comment on column CPR_DEPR_CALC_STG.FUNDING_WO_ID is 'A system-generated identifier of a particular funding project.';
comment on column CPR_DEPR_CALC_STG.REVISION is 'The revision of the funding project. Only used for incremental forecasting.';

alter table CPR_DEPR_CALC_STG_ARC
   add (FUNDING_WO_ID number(22,0) default -1,
        REVISION      number(22,0) default -1);

comment on column CPR_DEPR_CALC_STG_ARC.FUNDING_WO_ID is 'A system-generated identifier of a particular funding project.';
comment on column CPR_DEPR_CALC_STG_ARC.REVISION is 'The revision of the funding project. Only used for incremental forecasting.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (936, 0, 10, 4, 2, 0, 35681, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035681_depr_funding_wo_id.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;