/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053087_lessee_03_ilr_rev_order_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.1.0.2 02/11/2019 C. Yura        backfill new ls_ilr_approval fields
||============================================================================
*/

update ls_ilr_approval set revision_app_order = null;

merge into ls_ilr_approval LIA
using (
        SELECT ilr.ilr_id ilr_id, lia.revision revision,
					ROW_NUMBER() OVER(PARTITION BY ilr.ilr_id ORDER BY lia.acct_month_approved, lia.approval_date, lia.revision) rev_rnk
				FROM ls_ilr ilr
				JOIN ls_ilr_approval lia
				ON lia.ilr_id = ilr.ilr_id
				WHERE lia.revision > 0
					AND (lia.approval_date IS NOT NULL or lia.approval_status_id = 3)
          and (lia.ilr_id, lia.revision) not in (select ilr_id, current_revision from ls_ilr where ilr_status_id = 6)) ls_rev_rnk
          on (ls_rev_rnk.ilr_id = lia.ilr_id and ls_rev_rnk.revision = lia.revision)
    when matched then
      update set lia.revision_app_order = ls_rev_rnk.rev_rnk;

declare
   L_COUNT number;
begin
   /*See if there are any records left unpopulated*/  
	select count(*) into L_COUNT From ls_ilr_approval lia, ls_ilr ilr
	where ilr.ilr_id = lia.ilr_id 
	and (lia.approval_date IS NOT NULL or lia.approval_status_id = 3)
          and (lia.ilr_id, lia.revision) not in (select ilr_id, current_revision from ls_ilr where ilr_status_id = 6)
          and lia.revision_app_order is null;
   
   if L_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'Could not populate ls_ilr_approval.revision_app_order for all records with status = 3 or approval_date is not null where revision is not Pending In Service status.  
                               Will have to manually determine how to backfill outliers');
   end if;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15006, 0, 2018, 2, 0, 0, 53087, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053087_lessee_03_ilr_rev_order_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
	  