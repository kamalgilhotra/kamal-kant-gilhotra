/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050161_prov_get_sequence_fn_ddl.sql
|| Description:	Function to return the nextval of a sequence for use in places
||  		    that would otherwise give error
||		        ORA-02287: sequence number not allowed here
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1   03/24/2017 Andrew Hill    Create function
||============================================================================
*/

CREATE OR REPLACE FUNCTION f_get_sequence_nextval(sequence_owner VARCHAR2, sequence_name VARCHAR2) RETURN NUMBER AS
  val NUMBER;
BEGIN
  EXECUTE IMMEDIATE 'SELECT ' || sequence_owner  || '.' || sequence_name || '.nextval from dual' INTO val;
  return val;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4073, 0, 2017, 1, 0, 0, 50161, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_050161_prov_get_sequence_fn_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;