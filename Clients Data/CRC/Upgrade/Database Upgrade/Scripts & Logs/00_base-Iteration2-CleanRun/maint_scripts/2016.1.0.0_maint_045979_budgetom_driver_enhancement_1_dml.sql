 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045979_budgetom_driver_enhancement_1_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- -------------------------------------
 || 2016.1.0.0  08/17/2016 Anand R       PP-45979. Budget O&M driver enhancements
 ||============================================================================
 */ 

insert into CR_BUDGET_LABOR_TEMPLATES 
(LABOR_TEMPLATE_ID, DESCRIPTION)
values ('-1', 'Default Template');

insert into CR_BUDGET_LABOR_DRIVERS 
(TAB_INDICATOR, DESCRIPTION)
values (-1, 'Labor');

update CR_BUDGET_DATA_LABOR2_DEFINE 
set LABOR_TEMPLATE_ID=-1, TAB_INDICATOR = -1;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3266, 0, 2016, 1, 0, 0, 045979, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045979_budgetom_driver_enhancement_1_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;