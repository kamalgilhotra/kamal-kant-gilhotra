/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033533_taxrpr_anyquery.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/08/2014 Stephen Motter Point Release
||============================================================================
*/

update PP_ANY_QUERY_CRITERIA
   set DESCRIPTION = 'Repl Cost/Qty by Comp and Tax UOP'
 where DESCRIPTION = 'Tax Repairs CWIP/InService Analysis'
   and SUBSYSTEM = 'Tax Repairs';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (984, 0, 10, 4, 2, 0, 33533, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033533_taxrpr_anyquery.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;