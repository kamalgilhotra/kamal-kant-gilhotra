/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031823_depr_import_stg.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/10/2014 Sunjin Cone    Point Release
||============================================================================
*/

alter table DEPR_LEDGER_IMPORT_STG     modify DEPR_LEDGER_STATUS varchar2(10);
alter table DEPR_LEDGER_IMPORT_STG_ARC modify DEPR_LEDGER_STATUS varchar2(10);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1018, 0, 10, 4, 2, 0, 31823, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031823_depr_import_stg.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;