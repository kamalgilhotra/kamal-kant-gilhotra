/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042418_sys_stats_log_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1     02/12/2015 Andrew Scott     Usage statistics logging as windows and base gui objects
||                                        are opened, closed, activated, deactivated.
||============================================================================
*/

create table pp_stats_logging
(
   users varchar2(18),
   sessionid number(22,0),
   seq number(22,0),
   object_name varchar2(100),
   logged_event varchar2(100),
   logged_time date,
   user_id varchar2(18),
   time_stamp date
);

alter table pp_stats_logging
add constraint pp_stats_logging_pk
primary key (users, sessionid, seq)
using index tablespace PWRPLANT_IDX;

comment on table pp_stats_logging is '(C) [10] The PP Stats Logging table stores the windows and base gui objects that are opened, closed, activated, and deactivated by user and session.  The data is saved during the applications useage and saved to this table when exiting from the application.';
comment on column pp_stats_logging.users is 'The id of the person using the application.';
comment on column pp_stats_logging.sessionid is 'The Oracle session id.';
comment on column pp_stats_logging.seq is 'The sequence in which an event is logged specific to the user and session id.';
comment on column pp_stats_logging.object_name is 'The window or object associated with the logged event.';
comment on column pp_stats_logging.logged_event is 'The event being logged.  For windows, this will be either ''activate'', ''deactivate'', ''close'', ''open'', or ''closequery''.  For base gui workspaces and main windows, this will be either ''pp_initializeforpopout'', ''pp_initialize'', ''pp_activate'', ''pp_close'', ''of_closeworkspace'', or ''of_displayWorkspace''.';
comment on column pp_stats_logging.logged_time is 'The time the object event occurred.';
comment on column pp_stats_logging.USER_ID    is 'Standard system-assigned user id used for audit purposes.';
comment on column pp_stats_logging.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2305, 0, 2015, 1, 0, 0, 42418, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042418_sys_stats_log_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;