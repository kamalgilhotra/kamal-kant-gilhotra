/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047037_lease_import_vp_var_formula_comp_amount_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/16/2017 David H          create new import for VP variable component amounts
||============================================================================
*/

--Create new import type
insert into pp_import_type(import_type_id, description, long_description,
  import_table_name, archive_table_name, allow_updates_on_add, delegate_object_name)
values(264, 'Add: Var Payment Variable Amounts', 'VP Variable Components Amounts Add',
  'ls_import_vp_var_comp_amt', 'ls_import_vp_var_comp_amt_arch', 1, 'nvo_ls_logic_import')
;

--relate the import type to the lease subsystem
insert into pp_import_type_subsystem(import_type_id, import_subsystem_id)
values(264, 8)
;

--insert the columns we will need to use in the import
insert into pp_import_column(import_type_id, column_name, description, import_column_name,
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(264, 'formula_component_id', 'Formula Component Identifier', 'formula_component_xlate',
  1, 1, 'number(22,0)', 'ls_formula_component', 'formula_component_id')
;

--insert the columns we will need to use in the import
insert into pp_import_column(import_type_id, column_name, description, import_column_name,
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(264, 'ilr_id', 'ILR Identifier', 'ilr_xlate',
  1, 1, 'number(22,0)', 'ls_asset', 'ilr_id')
;

--insert the columns we will need to use in the import
insert into pp_import_column(import_type_id, column_name, description, import_column_name,
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(264, 'ls_asset_id', 'LS Asset Identifier', 'ls_asset_xlate',
  1, 1, 'number(22,0)', 'ls_asset', 'ls_asset_id')
;

insert into pp_import_column(import_type_id, column_name, description, import_column_name,
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(264, 'incurred_month_number', 'Month', NULL,
  1, 1, 'number(6,0)', NULL, NULL)
;

insert into pp_import_column(import_type_id, column_name, description, import_column_name,
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(264, 'amount', 'Amount', NULL,
  0, 1, 'number(28,8)', NULL, NULL)
;

--add the new lookup for ILR ID into the database
insert into pp_import_lookup(import_lookup_id, description, long_description,
  column_name, lookup_sql,
  is_derived, lookup_table_name, lookup_column_name)
values(2505, 'LS_ILR.ILR_Number', 'The passed in value corresponds to ILR Number field',
  'ilr_id', '( select b.ilr_id from ls_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.ilr_number ) ) )',
  0, 'ls_ilr', 'ilr_number')
;

--add the new lookup for LS Asset ID into the database
insert into pp_import_lookup(import_lookup_id, description, long_description,
  column_name, lookup_sql,
  is_derived, lookup_table_name, lookup_column_name)
values(2506, 'LS_Asset.Leased_Asset_Number', 'The passed in value corresponds to Leased Asset Number field',
  'ls_asset_id', '( select b.ls_asset_id from ls_asset b where upper( trim( <importfield> ) ) = upper( trim( b.leased_asset_number ) ) )',
  0, 'ls_asset', 'leased_asset_number')
;

--associate the column in the import with the lookup translation
insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
values(264, 'formula_component_id', 2504);

insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
values(264, 'ilr_id', 2505);

insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
values(264, 'ls_asset_id', 2506);

--add the import template (based on max ID + 1) and the fields for the template
declare
  templateID number ;
begin
  templateID := pp_import_template_seq.nextval;
  --add the specific template into the DB
  insert into pp_import_template(import_template_id, import_type_id, description,
    long_description, created_by, created_date, do_update_with_add)
   values( templateID, 264, 'VP Var Component Amounts Add',
    'VP Var Component Amounts Add', 'PWRPLANT', SYSDATE, 1
  );

  --add the fields for the template into the DB
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 1, 264, 'formula_component_id', 2504)
  ;

  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 2, 264, 'ilr_id', 2505)
  ;

  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 3, 264, 'ls_asset_id', 2506)
  ;

  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 4, 264, 'incurred_month_number', NULL)
  ;

  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 5, 264, 'amount', NULL)
  ;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3503, 0, 2017, 1, 0, 0, 47037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047037_lease_import_vp_var_formula_comp_amount_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
