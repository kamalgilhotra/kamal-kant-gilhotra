/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041879_prov_report_cancel_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/30/2014 Blake Andrews    Add system control for delay before "Report Cancel" window is displayed
||============================================================================
*/

insert into tax_accrual_system_control
(	control_id,
	control_name,
	control_value,
	description,
	long_description,
	company_id
)
select	300,
			'Report Wait Time',
			20,
			null,
			'Number of seconds before the user is presented with the option to cancel a currently-running report. Default is 20 seconds.',
			-1
from dual
where 300 not in (select control_id from tax_accrual_system_control);

--The 54513 report does not need the Calc Rates logic to run before the report runs like the 54516 does.  This update statement will keep that from occurring.  This Will help with performance.
update tax_accrual_rep_special_note 
set	calc_rates = 0,
		je_temp_table_ind = 0
where description = 'ETR_FOOTNOTE_CONS';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2148, 0, 2015, 1, 0, 0, 41879, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041879_prov_report_cancel_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;