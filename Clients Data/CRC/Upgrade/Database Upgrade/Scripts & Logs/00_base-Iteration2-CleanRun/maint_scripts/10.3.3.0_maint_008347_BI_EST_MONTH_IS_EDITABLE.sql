/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008347_BI_EST_MONTH_IS_EDITABLE.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/17/2011 Chris Mardis   Point Release
||============================================================================
*/

create or replace view bi_est_month_is_editable as
select A.BUDGET_NUMBER WORK_ORDER_NUMBER,
       A.BUDGET_ID WORK_ORDER_ID,
       A.BUDGET_VERSION_ID REVISION,
       TO_NUMBER(A.YEAR) year,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'01')),1,0,decode(sign(to_number(a.year||'01') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'01') - actuals_month_number),1,1,0)))))) january,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'02')),1,0,decode(sign(to_number(a.year||'02') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'02') - actuals_month_number),1,1,0)))))) february,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'03')),1,0,decode(sign(to_number(a.year||'03') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'03') - actuals_month_number),1,1,0)))))) march,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'04')),1,0,decode(sign(to_number(a.year||'04') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'04') - actuals_month_number),1,1,0)))))) april,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'05')),1,0,decode(sign(to_number(a.year||'05') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'05') - actuals_month_number),1,1,0)))))) may,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'06')),1,0,decode(sign(to_number(a.year||'06') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'06') - actuals_month_number),1,1,0)))))) june,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'07')),1,0,decode(sign(to_number(a.year||'07') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'07') - actuals_month_number),1,1,0)))))) july,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'08')),1,0,decode(sign(to_number(a.year||'08') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'08') - actuals_month_number),1,1,0)))))) august,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'09')),1,0,decode(sign(to_number(a.year||'09') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'09') - actuals_month_number),1,1,0)))))) september,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'10')),1,0,decode(sign(to_number(a.year||'10') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'10') - actuals_month_number),1,1,0)))))) october,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'11')),1,0,decode(sign(to_number(a.year||'11') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'11') - actuals_month_number),1,1,0)))))) november,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'12')),1,0,decode(sign(to_number(a.year||'12') - end_month_number),1,0,decode(approval_status_id,1,0,decode(sign(to_number(a.year||'12') - actuals_month_number),1,1,0)))))) december,
       decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(approval_status_id,1,0,1))) revision_edit
  from (select 2 FUNDING_WO_INDICATOR,
               WOC.BUDGET_NUMBER,
               WOA.BUDGET_ID,
               WOA.BUDGET_VERSION_ID,
               PP.YEAR,
               (select DECODE(count(*), 0, 0, 1)
                  from BUDGET_VERSION BV
                 where BV.BUDGET_VERSION_ID = WOA.BUDGET_VERSION_ID
                   and OPEN_FOR_ENTRY = 0) NOT_OPEN_FOR_ENTRY,
               (select DECODE(count(*), 0, 0, 1)
                  from BUDGET_VERSION BV
                 where BV.BUDGET_VERSION_ID = WOA.BUDGET_VERSION_ID
                   and BRING_IN_SUBS = 1) SUBS_ONLY,
               TO_NUMBER(TO_CHAR(WOA.START_DATE, 'yyyymm')) START_MONTH_NUMBER,
               TO_NUMBER(TO_CHAR(WOA.CLOSE_DATE, 'yyyymm')) END_MONTH_NUMBER,
               (select NVL(max(NVL(BV.ACTUALS_MONTH, 0)), 0)
                  from BUDGET_VERSION BV
                 where BV.BUDGET_VERSION_ID = WOA.BUDGET_VERSION_ID) ACTUALS_MONTH_NUMBER,
               (select DECODE(count(*), 0, 0, 1)
                  from BUDGET_APPROVAL A
                 where A.BUDGET_ID = WOA.BUDGET_ID
                   and A.BUDGET_VERSION_ID = WOA.BUDGET_VERSION_ID
                   and A.APPROVAL_NUMBER = (select max(B.APPROVAL_NUMBER)
                                              from BUDGET_APPROVAL B
                                             where B.BUDGET_ID = A.BUDGET_ID)
                   and A.APPROVAL_STATUS_ID = 2) APPROVAL_STATUS_ID
          from BUDGET WOC, BUDGET_AMOUNTS WOA, PP_TABLE_YEARS PP, (select distinct budget_id from TEMP_BUDGET) TWO
         where WOC.BUDGET_ID = WOA.BUDGET_ID
           and WOA.BUDGET_ID = TWO.BUDGET_ID) A;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (52, 0, 10, 3, 3, 0, 8347, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008347_BI_EST_MONTH_IS_EDITABLE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
