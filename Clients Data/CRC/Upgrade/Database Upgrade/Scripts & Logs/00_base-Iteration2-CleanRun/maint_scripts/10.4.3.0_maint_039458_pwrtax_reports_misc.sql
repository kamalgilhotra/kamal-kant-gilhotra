/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_039458_pwrtax_reports_misc.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/11/2014 Anand Rajashekar    Report updates
||========================================================================================
*/

update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 006'  where REPORT_ID =  400001;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 007', PP_REPORT_TIME_OPTION_ID = 52  where REPORT_ID =  400002;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 012'  where REPORT_ID =  400003;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 035', LONG_DESCRIPTION = concat(LONG_DESCRIPTION, ' (excludes capitalized depreciation)') where REPORT_ID =  400004;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 036', LONG_DESCRIPTION = concat(LONG_DESCRIPTION, ' (excludes capitalized depreciation)') where REPORT_ID =  400005;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 037'  where REPORT_ID =  400006;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 054'  where REPORT_ID =  400008;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 055'  where REPORT_ID =  400009;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 072'  where REPORT_ID =  400012;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 073'  where REPORT_ID =  400013;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 001'  where REPORT_ID =  403001;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 008'  where REPORT_ID =  403003;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 016'  where REPORT_ID =  403004;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 017'  where REPORT_ID =  403005;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 022'  where REPORT_ID =  403006;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 026'  where REPORT_ID =  403007;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 030'  where REPORT_ID =  403008;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 050'  where REPORT_ID =  403010;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 051'  where REPORT_ID =  403011;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 052'  where REPORT_ID =  403012;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 053'  where REPORT_ID =  403013;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 064'  where REPORT_ID =  403014;
update PP_REPORTS set REPORT_NUMBER = 'PwrTax - 065'  where REPORT_ID =  403015;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1417, 0, 10, 4, 3, 0, 39458, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039458_pwrtax_reports_misc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
