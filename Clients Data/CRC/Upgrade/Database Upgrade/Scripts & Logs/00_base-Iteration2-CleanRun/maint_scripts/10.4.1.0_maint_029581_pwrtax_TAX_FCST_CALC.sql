/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029581_pwrtax_TAX_FCST_CALC.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/14/2013 Roger Roach      Changed the Fast Tax to use Oracle Packages
||============================================================================
*/

create or replace package TAX_FCST_CALC is
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: TAX_FCST_CALC
   --|| Description: Calculate Tax Forecast
   --||============================================================================
   --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 5.0      02/20/2013 Roger Roach    Original Version
   --|| 5.1      02/22/2013 Roger Roach    Fixed problem Tax Limitation
   --||============================================================================

   G_LIMIT constant pls_integer := 10000;
   subtype HASH_T is varchar2(200);
   G_SEP constant varchar2(1) := '-';
   G_START_TIME timestamp;

   type DEF_TAX_UTIL_REC is record(
      ID      number(22, 0),
      ROW_NUM number(22, 2));

   type DEF_TAX_UTIL_REC_TYPE is table of DEF_TAX_UTIL_REC index by pls_integer;

   type DEF_TAX_UTIL_TABLE_TYPE is table of DEF_TAX_UTIL_REC_TYPE index by HASH_T;

   G_DEF_TAX_UTIL_HASH         DEF_TAX_UTIL_TABLE_TYPE;
   G_TAX_FCST_CALC_INPUTS_HASH DEF_TAX_UTIL_TABLE_TYPE;

   type TABLE_INTEGERS_TYPE is table of pls_integer index by pls_integer;

   type TAX_FCST_CALC_OUTPUT_HASH_TYPE is table of TABLE_INTEGERS_TYPE index by HASH_T;

   G_TAX_FCST_CALC_OUTPUT_HASH TAX_FCST_CALC_OUTPUT_HASH_TYPE;

   G_TAX_GRID_LAST_RECORD_ID pls_integer;
   G_TAX_GRID_LAST_BOOK_ID   pls_integer;

   G_START_RECORD_ID pls_integer;
   G_END_RECORD_ID   pls_integer;

   G_VERSION    number(22, 0);
   G_START_YEAR number(22, 2);
   G_END_YEAR   number(22, 2);

   G_JOB_NO  number(22, 0);
   G_LINE_NO number(22, 0);

   G_TABLE_VINTAGE_IDS TABLE_LIST_ID_TYPE;
   G_TABLE_CLASS_IDS   TABLE_LIST_ID_TYPE;
   G_TABLE_COMPANY_IDS TABLE_LIST_ID_TYPE;
   G_TABLE_BOOK_IDS    TABLE_LIST_ID_TYPE;

   G_TAX_YEAR_COUNT      number(22, 2);
   G_TABLE_BOOK_COUNT    number(22, 0);
   G_TABLE_VINTAGE_COUNT number(22, 0);
   G_TABLE_CLASS_COUNT   number(22, 0);
   G_TABLE_COMPANY_COUNT number(22, 0);

   type TAX_GRID_HASH_TYPE is table of number(22, 0) index by HASH_T;

   type TAX_GRID_REC is record(
      ID       number(22, 0),
      TAX_YEAR number(22, 2));

   type TAX_GRID_REC_TYPE is table of TAX_GRID_REC index by pls_integer;

   type TAX_GRID_REC_TABLE_TYPE is table of TAX_GRID_REC_TYPE index by HASH_T;

   G_TAX_GRID_HASH TAX_GRID_REC_TABLE_TYPE;

   type TAX_FCST_CALC_OUTPUT_TYPE is table of TAX_FORECAST_OUTPUT%rowtype;

   type DFIT_FORECAST_OUTPUT_TYPE is table of DFIT_FORECAST_OUTPUT%rowtype;

   type TAX_CONVENTION_TABLE_TYPE is table of pls_integer index by binary_integer;

   G_TAX_CONVENTION_HASH TAX_CONVENTION_TABLE_TYPE;

   type TAX_RATES_REC is record(
      ID      number(22, 0),
      RATE_ID number(22, 0));

   type TAX_RATES_REC_TYPE is table of TAX_RATES_REC index by pls_integer;

   type TAX_RATES_TABLE_TYPE is table of TAX_RATES_REC_TYPE index by HASH_T;

   G_TAX_RATES_HASH  TAX_RATES_TABLE_TYPE;
   G_DFIT_RATES_HASH TAX_RATES_TABLE_TYPE;

   cursor TAX_GRID_CUR is
      select distinct TD.TAX_RECORD_ID,
                      TD.TAX_BOOK_ID,
                      TD.TAX_YEAR,
                      BOOK_BALANCE_END,
                      TAX_BALANCE_END,
                      ACCUM_RESERVE_END,
                      SL_RESERVE_END,
                      FIXED_DEPRECIABLE_BASE,
                      ESTIMATED_SALVAGE,
                      ACCUM_SALVAGE_END,
                      ACCUM_ORDIN_RETIRES_END,
                      EST_SALVAGE_PCT,
                      RESERVE_AT_SWITCH_END,
                      QUANTITY,
                      CONVENTION_ID,
                      EXTRAORDINARY_CONVENTION,
                      TAX_LIMIT_ID,
                      TAX_RATE_ID,
                      V.YEAR VINTAGE_YEAR,
                      TB.BOOK_DEPR_ALLOC_IND,
                      TD.DEPRECIATION
        from VINTAGE V, TAX_CONTROL TC, TAX_RECORD_CONTROL TRC, TAX_DEPRECIATION TD, TAX_BOOK TB
       where (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TD.TAX_BOOK_ID in (select * from table(G_TABLE_BOOK_IDS)) or G_TABLE_BOOK_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and TD.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and TC.TAX_BOOK_ID = TD.TAX_BOOK_ID
         and TRC.TAX_RECORD_ID = TC.TAX_RECORD_ID
         and TRC.TAX_RECORD_ID = TD.TAX_RECORD_ID
         and V.VINTAGE_ID = TRC.VINTAGE_ID
         and TC.TAX_BOOK_ID = TB.TAX_BOOK_ID
         and TRC.VERSION_ID = G_VERSION
       order by TD.TAX_RECORD_ID, TD.TAX_YEAR;

   type TYPE_TAX_GRID_REC is varray(10040) of TAX_GRID_CUR%rowtype;

   type TYPE_TAX_GRID_SAVE_REC is varray(40) of TAX_GRID_CUR%rowtype;

   G_TAX_GRID_REC       TYPE_TAX_GRID_REC;
   G_TAX_GRID_SAVE1_REC TYPE_TAX_GRID_SAVE_REC;
   G_TAX_GRID_SAVE2_REC TYPE_TAX_GRID_SAVE_REC;

   G_TAX_DEPR_LAST_RECORD_ID number(22, 0);
   G_TAX_DEPR_LAST_BOOK_ID   number(22, 0);

   cursor TAX_CONVENTION_CUR is
      select CONVENTION_ID,
             RETIRE_DEPR_ID,
             RETIRE_BAL_ID,
             RETIRE_RESERVE_ID,
             GAIN_LOSS_ID,
             SALVAGE_ID,
             EST_SALVAGE_ID
        from TAX_CONVENTION;

   type TYPE_TAX_CONVENTION_REC is table of TAX_CONVENTION_CUR%rowtype;

   G_TAX_CONVENTION_REC TYPE_TAX_CONVENTION_REC;

   cursor TAX_RATES_CUR is
      select TR.TAX_RATE_ID,
             RATE,
             RATE1,
             TR.YEAR,
             NET_GROSS,
             LIFE,
             REMAINING_LIFE_PLAN,
             START_METHOD,
             ROUNDING_CONVENTION,
             SWITCHED_YEAR
        from TAX_RATES TR, TAX_RATE_CONTROL TRC
       where TR.TAX_RATE_ID = TRC.TAX_RATE_ID
       order by TR.YEAR;

   type TYPE_TAX_RATES_REC is table of TAX_RATES_CUR%rowtype;

   G_TAX_RATES_REC TYPE_TAX_RATES_REC;

   cursor TAX_LIMITATION_CUR is
      select TAX_LIMIT_ID, LIMITATION, year from TAX_LIMITATION order by year;

   type TYPE_TAX_LIMITATION_REC is table of TAX_LIMITATION_CUR%rowtype;

   G_TAX_LIMITATION_REC TYPE_TAX_LIMITATION_REC;

   cursor TAX_FCST_CALC_INPUTS_CUR is
      select TAX_RECORD_ID,
             TAX_YEAR,
             BOOK_ADDITIONS,
             BOOK_RETIREMENTS,
             ACTUAL_SALVAGE,
             TAX_FORECAST_VERSION_ID
        from TAX_FORECAST_INPUT
       where TAX_FORECAST_VERSION_ID = G_VERSION;

   type TYPE_TAX_FCST_CALC_INPUTS_REC is table of TAX_FCST_CALC_INPUTS_CUR%rowtype;

   G_TAX_FCST_CALC_INPUTS_REC TYPE_TAX_FCST_CALC_INPUTS_REC;

   cursor DEF_TAX_UTIL_CUR is
      select DIT.TAX_RECORD_ID,
             DIT.TAX_YEAR,
             DIT.TAX_MONTH,
             DIT.NORMALIZATION_ID,
             DIT.TIME_SLICE_ID,
             DIT.DEF_INCOME_TAX_BALANCE_BEG,
             DIT.DEF_INCOME_TAX_BALANCE_END,
             DIT.NORM_DIFF_BALANCE_BEG,
             DIT.NORM_DIFF_BALANCE_END,
             DIT.DEF_INCOME_TAX_PROVISION,
             DIT.DEF_INCOME_TAX_REVERSAL,
             DIT.ARAM_RATE,
             DIT.LIFE,
             DIT.DEF_INCOME_TAX_ADJUST,
             DIT.DEF_INCOME_TAX_RETIRE,
             DIT.DEF_INCOME_TAX_GAIN_LOSS,
             DIT.ARAM_RATE_END,
             DIT.GAIN_LOSS_DEF_TAX_BALANCE,
             DIT.GAIN_LOSS_DEF_TAX_BALANCE_END,
             NS.NORM_FROM_TAX,
             NS.NORM_TO_TAX,
             NS.DEF_INCOME_TAX_RATE_ID,
             NS.AMORTIZATION_TYPE_ID,
             V.YEAR,
             NS.BOOK_DEPR_ALLOC_IND,
             DIT.INPUT_AMORTIZATION,
             BASIS_DIFF_ADD_RET
        from DEFERRED_INCOME_TAX DIT, TAX_RECORD_CONTROL TRC, NORMALIZATION_SCHEMA NS, VINTAGE V
       where (DIT.TAX_RECORD_ID = TRC.TAX_RECORD_ID)
         and (DIT.NORMALIZATION_ID = NS.NORMALIZATION_ID)
         and (TRC.VINTAGE_ID = V.VINTAGE_ID)
         and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and DIT.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and DIT.TAX_RECORD_ID between G_START_RECORD_ID and G_END_RECORD_ID
         and TRC.VERSION_ID = G_VERSION
       order by DIT.TAX_YEAR;

   type TYPE_DEF_TAX_UTIL_REC is table of DEF_TAX_UTIL_CUR%rowtype;

   G_DEF_TAX_UTIL_REC TYPE_DEF_TAX_UTIL_REC;

   cursor DFIT_RATES_CUR is
      select DEF_INCOME_TAX_RATE_ID,
             TO_NUMBER(TO_CHAR(EFFECTIVE_DATE, 'YYYYMMDD')) EFFECTIVE_DATE,
             MONTHLY_RATE,
             ANNUAL_RATE
        from DEFERRED_INCOME_TAX_RATES
       order by EFFECTIVE_DATE asc;

   type TYPE_DFIT_RATES_REC is table of DFIT_RATES_CUR%rowtype;

   G_DFIT_RATES_REC TYPE_DFIT_RATES_REC;

   procedure SET_SESSION_PARAMETER;

   function GET_VERSION return varchar2;

   function CALC_FCST(A_JOB_NO number) return integer;

   function CALC(A_VERSION                     pls_integer,
                 A_TAX_GRID_INDEX              pls_integer,
                 A_TAX_RATES_SUB_REC           TYPE_TAX_RATES_REC,
                 A_TAX_RATES_ROWS              pls_integer,
                 A_TAX_CONVENTION_SUB_REC      TAX_CONVENTION_CUR%rowtype,
                 A_EXTRAORD_CONVENTION_SUB_REC TAX_CONVENTION_CUR%rowtype,
                 A_TAX_LIMITATION_SUB_REC      TYPE_TAX_LIMITATION_REC,
                 A_TAX_LIMITATION_ROWS         pls_integer,
                 A_TAX_FCST_CALC_INPUTS        TYPE_TAX_FCST_CALC_INPUTS_REC,
                 A_TAX_FCST_CALC_INPUTS_ROWS   pls_integer,
                 A_TAX_FCST_CALC_OUTPUTS       in out nocopy TAX_FCST_CALC_OUTPUT_TYPE,
                 A_TAX_FCST_CALC_OUTPUTS_ROWS  in out nocopy pls_integer,
                 A_END_YR                      pls_integer) return pls_integer;

   function DEF_CALC(A_VERSION                    pls_integer,
                     A_DEF_TAX_UTIL_INDEX         pls_integer,
                     A_DEF_TAX_UTIL_SUB_REC       in out nocopy TYPE_DEF_TAX_UTIL_REC,
                     A_DEF_TAX_RATES_SUB_REC      TYPE_DFIT_RATES_REC,
                     A_DEF_TAX_RATES_ROWS         pls_integer,
                     A_TAX_FCST_FROM              TAX_FCST_CALC_OUTPUT_TYPE,
                     A_TAX_FCST_FROM_ROWS         pls_integer,
                     A_TAX_FCST_TO                TAX_FCST_CALC_OUTPUT_TYPE,
                     A_TAX_FCST_TO_ROWS           pls_integer,
                     A_TAX_DFIT_FCST_OUTPUTS      in out nocopy DFIT_FORECAST_OUTPUT_TYPE,
                     A_TAX_DFIT_FCST_OUTPUTS_ROWS in out nocopy pls_integer,
                     A_TAX_RATES_SUB_REC          TYPE_TAX_RATES_REC,
                     A_TAX_RATES_ROWS             pls_integer,
                     A_END_YR                     pls_integer) return pls_integer;

   procedure WRITE_LOG(A_JOB_NO     number,
                       A_ERROR_TYPE number,
                       A_CODE       number,
                       A_MSG        varchar2);
end TAX_FCST_CALC;
/


create or replace package body TAX_FCST_CALC is

   -- =============================================================================
   --  Function GET_VERSION
   -- =============================================================================
   function GET_VERSION return varchar2 is
   begin
      return '5.11';
   end GET_VERSION;

   -- =============================================================================
   --  Function GET_TAX_GRID
   -- =============================================================================
   function GET_TAX_GRID(A_START_ROW out pls_integer) return integer is
      L_COUNT             integer;
      L_SAVE_COUNT        pls_integer;
      I                   pls_integer;
      J                   pls_integer;
      K                   pls_integer;
      CODE                pls_integer;
      L_CURRENT_RECORD_ID pls_integer;
      L_CURRENT_BOOK_ID   pls_integer;
      L_YEAR_COUNT        pls_integer;
      type ARRAY_OF_INTGERS is table of pls_integer;
      L_RECORDS_NOT_USERS            ARRAY_OF_INTGERS;
      L_TAX_GRID_LAST_SAVE_RECORD_ID pls_integer;
      L_TAX_GRID_LAST_SAVE_BOOK_ID   pls_integer;
      L_MOVE_ROW_COUNT               pls_integer;

   begin
      if not TAX_GRID_CUR%isopen then
         open TAX_GRID_CUR;
      end if;
      A_START_ROW := 1;
      fetch TAX_GRID_CUR bulk collect
         into G_TAX_GRID_REC limit G_LIMIT;

      I       := G_TAX_GRID_REC.FIRST;
      L_COUNT := G_TAX_GRID_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_GRID_CUR;
         return 0;
      else
         G_TAX_GRID_LAST_RECORD_ID := G_TAX_GRID_REC(L_COUNT).TAX_RECORD_ID;
         G_TAX_GRID_LAST_BOOK_ID   := G_TAX_GRID_REC(L_COUNT).TAX_BOOK_ID;
      end if;

      --* For the def_tax_util_rec retieve
      G_START_RECORD_ID := G_TAX_GRID_REC(1).TAX_RECORD_ID;
      if L_COUNT = G_LIMIT then
         G_END_RECORD_ID := G_TAX_GRID_REC(L_COUNT).TAX_RECORD_ID;
      else
         G_END_RECORD_ID := G_TAX_GRID_REC(L_COUNT).TAX_RECORD_ID - 1;
      end if;
      -- save the last records that are not complete
      K := 0;
      G_TAX_GRID_SAVE2_REC.DELETE;
      if L_COUNT = G_LIMIT then
         J := 1;
         for J in reverse 1 .. L_COUNT
         loop
            if G_TAX_GRID_LAST_RECORD_ID = G_TAX_GRID_REC(J).TAX_RECORD_ID and
               G_TAX_GRID_LAST_BOOK_ID = G_TAX_GRID_REC(J).TAX_BOOK_ID then
               G_TAX_GRID_SAVE2_REC.EXTEND;
               K := K + 1;
               G_TAX_GRID_SAVE2_REC(K) := G_TAX_GRID_REC(J);
            else
               exit;
            end if;
         end loop;
         L_COUNT := L_COUNT - K;
      else
         G_TAX_GRID_LAST_RECORD_ID := 0;
         G_TAX_GRID_LAST_BOOK_ID   := 0;
      end if;

      -- restore the save records
      J := 0;

      if G_TAX_GRID_SAVE1_REC.COUNT > 0 then
         L_SAVE_COUNT := G_TAX_GRID_SAVE1_REC.COUNT;
         K            := 0;
         for J in 1 .. L_SAVE_COUNT
         loop
            G_TAX_GRID_REC.EXTEND;
            G_TAX_GRID_REC(L_COUNT + J) := G_TAX_GRID_SAVE1_REC(J);
            K := K + 1;
         end loop;

         -- we must reorganize the collection
         L_TAX_GRID_LAST_SAVE_RECORD_ID := G_TAX_GRID_SAVE1_REC(1).TAX_RECORD_ID;
         L_TAX_GRID_LAST_SAVE_BOOK_ID   := G_TAX_GRID_SAVE1_REC(1).TAX_BOOK_ID;
         L_MOVE_ROW_COUNT               := 0;
         for J in 1 .. L_COUNT
         loop
            if L_TAX_GRID_LAST_SAVE_RECORD_ID = G_TAX_GRID_REC(J).TAX_RECORD_ID and
               L_TAX_GRID_LAST_SAVE_BOOK_ID = G_TAX_GRID_REC(J).TAX_BOOK_ID then
               L_MOVE_ROW_COUNT := L_MOVE_ROW_COUNT + 1;
            else
               exit;
            end if;
         end loop;
         L_COUNT := L_COUNT + L_SAVE_COUNT;
         for J in 1 .. L_MOVE_ROW_COUNT
         loop
            G_TAX_GRID_REC.EXTEND;
            G_TAX_GRID_REC(L_COUNT + J) := G_TAX_GRID_REC(J);
         end loop;
         L_COUNT     := L_COUNT + L_MOVE_ROW_COUNT;
         A_START_ROW := L_MOVE_ROW_COUNT + 1;
      end if;

      G_TAX_GRID_SAVE1_REC := G_TAX_GRID_SAVE2_REC;

      -- create an index to the data

      I                   := 1;
      L_CURRENT_RECORD_ID := 0;
      L_CURRENT_BOOK_ID   := 0;
      G_TAX_GRID_HASH.DELETE;
      while I <= L_COUNT
      loop
         if L_CURRENT_RECORD_ID = G_TAX_GRID_REC(I).TAX_RECORD_ID and
            L_CURRENT_BOOK_ID = G_TAX_GRID_REC(I).TAX_BOOK_ID then
            L_YEAR_COUNT := L_YEAR_COUNT + 1;
            G_TAX_GRID_HASH(TO_CHAR(G_TAX_GRID_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_GRID_REC(I).TAX_BOOK_ID))(L_YEAR_COUNT).TAX_YEAR := G_TAX_GRID_REC(I).TAX_YEAR;
            G_TAX_GRID_HASH(TO_CHAR(G_TAX_GRID_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_GRID_REC(I).TAX_BOOK_ID))(L_YEAR_COUNT).ID := I;

         else
            L_YEAR_COUNT        := 1;
            L_CURRENT_RECORD_ID := G_TAX_GRID_REC(I).TAX_RECORD_ID;
            L_CURRENT_BOOK_ID   := G_TAX_GRID_REC(I).TAX_BOOK_ID;
            if L_CURRENT_RECORD_ID = 117152 then
               J := 0;
            end if;
            G_TAX_GRID_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_BOOK_ID))(L_YEAR_COUNT).TAX_YEAR := G_TAX_GRID_REC(I).TAX_YEAR;
            G_TAX_GRID_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_BOOK_ID))(L_YEAR_COUNT).ID := I;

         end if;
         I := I + 1;
      end loop;
      -- g_tax_grid_rec.delete;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Grid Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;

   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   ' Tax Grid ' || sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_GRID;

   -- =============================================================================
   --  Function GET_TAX_RATES
   -- =============================================================================
   function GET_TAX_RATES return integer is
      L_COUNT        integer;
      I              pls_integer;
      CODE           pls_integer;
      L_LAST_RATE_ID pls_integer;
      L_ROWS         pls_integer;

   begin
      if not TAX_RATES_CUR%isopen then
         open TAX_RATES_CUR;
      end if;

      fetch TAX_RATES_CUR bulk collect
         into G_TAX_RATES_REC;

      I       := G_TAX_RATES_REC.FIRST;
      L_COUNT := G_TAX_RATES_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_RATES_CUR;
      end if;

      -- create an index to the data
      L_COUNT        := G_TAX_RATES_REC.COUNT;
      I              := 1;
      L_LAST_RATE_ID := 0;
      while I <= L_COUNT
      loop
         if G_TAX_RATES_REC(I).TAX_RATE_ID = L_LAST_RATE_ID then
            L_ROWS := L_ROWS + 1;
            G_TAX_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         else
            L_ROWS := 1;
            L_LAST_RATE_ID := G_TAX_RATES_REC(I).TAX_RATE_ID;
            G_TAX_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         end if;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Rates Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_RATES;

   -- =============================================================================
   --  Function GET_TAX_CONVENTION
   -- =============================================================================
   function GET_TAX_CONVENTION return integer is
      L_COUNT integer;
      I       pls_integer;
      CODE    pls_integer;

   begin
      if not TAX_CONVENTION_CUR%isopen then
         open TAX_CONVENTION_CUR;
      end if;

      fetch TAX_CONVENTION_CUR bulk collect
         into G_TAX_CONVENTION_REC;

      I       := G_TAX_CONVENTION_REC.FIRST;
      L_COUNT := G_TAX_CONVENTION_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_CONVENTION_CUR;
      end if;

      I := 1;
      while I <= L_COUNT
      loop
         G_TAX_CONVENTION_HASH(G_TAX_CONVENTION_REC(I).CONVENTION_ID) := I;
         I := I + 1;
      end loop;

      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Convention Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_CONVENTION;

   -- =============================================================================
   --  Function GET_TAX_FCST_CALC_INPUTS
   -- =============================================================================
   function GET_TAX_FCST_CALC_INPUTS return integer is
      L_COUNT             integer;
      I                   pls_integer;
      CODE                pls_integer;
      L_REC_COUNT         pls_integer;
      L_CURRENT_RECORD_ID pls_integer;

   begin
      if not TAX_FCST_CALC_INPUTS_CUR%isopen then
         open TAX_FCST_CALC_INPUTS_CUR;
      end if;

      fetch TAX_FCST_CALC_INPUTS_CUR bulk collect
         into G_TAX_FCST_CALC_INPUTS_REC;

      I       := G_TAX_FCST_CALC_INPUTS_REC.FIRST;
      L_COUNT := G_TAX_FCST_CALC_INPUTS_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_FCST_CALC_INPUTS_CUR;
      end if;

      I := 1;

      L_CURRENT_RECORD_ID := 0;

      while I <= L_COUNT
      loop

         if L_CURRENT_RECORD_ID = G_TAX_FCST_CALC_INPUTS_REC(I).TAX_RECORD_ID then
            L_REC_COUNT := L_REC_COUNT + 1;
            G_TAX_FCST_CALC_INPUTS_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ROW_NUM := L_REC_COUNT;
            G_TAX_FCST_CALC_INPUTS_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ID := I;

         else
            L_REC_COUNT := 1;
            L_CURRENT_RECORD_ID := G_DEF_TAX_UTIL_REC(I).TAX_RECORD_ID;
            G_TAX_FCST_CALC_INPUTS_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ROW_NUM := L_REC_COUNT;
            G_TAX_FCST_CALC_INPUTS_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ID := I;

         end if;
         I := I + 1;
      end loop;

      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Fsct Calc Inputs Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_FCST_CALC_INPUTS;

   -- =============================================================================
   --  Function GET_TAX_LIMITATION
   -- =============================================================================
   function GET_TAX_LIMITATION return integer is
      L_COUNT integer;
      I       pls_integer;
      CODE    pls_integer;
   begin
      if not TAX_LIMITATION_CUR%isopen then
         open TAX_LIMITATION_CUR;
      end if;

      fetch TAX_LIMITATION_CUR bulk collect
         into G_TAX_LIMITATION_REC;

      I       := G_TAX_LIMITATION_REC.FIRST;
      L_COUNT := G_TAX_LIMITATION_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_LIMITATION_CUR;
      end if;

      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax limitation Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_LIMITATION;

   -- =============================================================================
   --  Function GET_DEF_TAX_UTIL
   -- =============================================================================
   function GET_DEF_TAX_UTIL return integer is
      L_COUNT             integer;
      I                   pls_integer;
      CODE                pls_integer;
      L_REC_COUNT         pls_integer;
      L_CURRENT_RECORD_ID pls_integer;

   begin

      open DEF_TAX_UTIL_CUR;

      G_DEF_TAX_UTIL_REC.DELETE;
      G_DEF_TAX_UTIL_HASH.DELETE;

      fetch DEF_TAX_UTIL_CUR bulk collect
         into G_DEF_TAX_UTIL_REC;

      I       := G_DEF_TAX_UTIL_REC.FIRST;
      L_COUNT := G_DEF_TAX_UTIL_REC.COUNT;

      close DEF_TAX_UTIL_CUR;

      -- create an index to the data

      -- create an index to the data

      L_CURRENT_RECORD_ID := -11111111;

      while I <= L_COUNT
      loop

         if L_CURRENT_RECORD_ID = G_DEF_TAX_UTIL_REC(I).TAX_RECORD_ID then
            L_REC_COUNT := L_REC_COUNT + 1;
            G_DEF_TAX_UTIL_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ROW_NUM := L_REC_COUNT;
            G_DEF_TAX_UTIL_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ID := I;

         else
            L_REC_COUNT := 1;
            L_CURRENT_RECORD_ID := G_DEF_TAX_UTIL_REC(I).TAX_RECORD_ID;
            G_DEF_TAX_UTIL_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ROW_NUM := L_REC_COUNT;
            G_DEF_TAX_UTIL_HASH(TO_CHAR(L_CURRENT_RECORD_ID))(L_REC_COUNT).ID := I;

         end if;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Def Tax Util Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   'Tax Def Tax Util ' || sqlerrm(CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_DEF_TAX_UTIL;

   -- =============================================================================
   --  Function GET_DFIT_RATES
   -- =============================================================================
   function GET_DFIT_RATES return integer is
      L_COUNT        integer;
      I              pls_integer;
      CODE           pls_integer;
      L_LAST_RATE_ID pls_integer;
      L_ROWS         pls_integer;
   begin
      if not DFIT_RATES_CUR%isopen then
         open DFIT_RATES_CUR;
      end if;

      fetch DFIT_RATES_CUR bulk collect
         into G_DFIT_RATES_REC;

      I       := G_DFIT_RATES_REC.FIRST;
      L_COUNT := G_DFIT_RATES_REC.COUNT;

      if L_COUNT = 0 then
         close DFIT_RATES_CUR;
      end if;

      -- create an index to the data
      L_COUNT        := G_DFIT_RATES_REC.COUNT;
      I              := 1;
      L_LAST_RATE_ID := -11111;
      while I <= L_COUNT
      loop
         if G_DFIT_RATES_REC(I).DEF_INCOME_TAX_RATE_ID = L_LAST_RATE_ID then
            L_ROWS := L_ROWS + 1;
            G_DFIT_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         else
            L_ROWS := 1;
            L_LAST_RATE_ID := G_DFIT_RATES_REC(I).DEF_INCOME_TAX_RATE_ID;
            G_DFIT_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         end if;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Dfit Rates Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   'Dfit Rates  ' || sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_DFIT_RATES;

   -- =============================================================================
   --  Function FILTER_TAX_RATES
   -- =============================================================================
   function FILTER_TAX_RATES(A_TAX_RATES_SUB_REC in out nocopy TYPE_TAX_RATES_REC,
                             A_RATE_ID           pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;
   begin

      L_COUNT := G_TAX_RATES_REC.COUNT;
      if A_TAX_RATES_SUB_REC.EXISTS(1) then
         A_TAX_RATES_SUB_REC.DELETE;
      end if;
      L_ROW := 0;

      L_COUNT := G_TAX_RATES_HASH(A_RATE_ID).COUNT;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_TAX_RATES_HASH(A_RATE_ID)(I).ID;
         A_TAX_RATES_SUB_REC.EXTEND(1);
         A_TAX_RATES_SUB_REC(I) := G_TAX_RATES_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_rates ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_RATES;

   -- =============================================================================
   --  Function FILTER_DFIT_RATES
   -- =============================================================================
   function FILTER_DFIT_RATES(A_DFIT_RATES_SUB_REC in out nocopy TYPE_DFIT_RATES_REC,
                              A_RATE_ID            pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;

   begin

      L_COUNT := G_DFIT_RATES_REC.COUNT;
      if A_DFIT_RATES_SUB_REC.EXISTS(1) then
         A_DFIT_RATES_SUB_REC.DELETE;
      end if;
      L_ROW := 0;

      L_COUNT := G_DFIT_RATES_HASH(A_RATE_ID).COUNT;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_DFIT_RATES_HASH(A_RATE_ID)(I).ID;
         A_DFIT_RATES_SUB_REC.EXTEND(1);
         A_DFIT_RATES_SUB_REC(I) := G_DFIT_RATES_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Filter Dfit Rates ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_DFIT_RATES;

   -- =============================================================================
   --  Function FILTER_TAX_LIMITATION
   -- =============================================================================
   function FILTER_TAX_LIMITATION(A_TAX_LIMITATION_SUB_REC in out nocopy TYPE_TAX_LIMITATION_REC,
                                  A_LIMIT_ID               pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_CODE  pls_integer;

   begin

      L_COUNT := G_TAX_LIMITATION_REC.COUNT;
      A_TAX_LIMITATION_SUB_REC.DELETE;
      L_ROW := 0;
      for I in 1 .. L_COUNT
      loop
         if G_TAX_LIMITATION_REC(I).TAX_LIMIT_ID = A_LIMIT_ID then
            L_ROW := 1 + L_ROW;
            A_TAX_LIMITATION_SUB_REC.EXTEND;
            A_TAX_LIMITATION_SUB_REC(L_ROW) := G_TAX_LIMITATION_REC(I);
         end if;
      end loop;
      return L_ROW;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_limitation ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_LIMITATION;

   -- =============================================================================
   --  Function FILTER_TAX_FCST_CALC_INPUTS
   -- =============================================================================
   function FILTER_TAX_FCST_CALC_INPUTS(A_TAX_FCST_CALC_INPUTS_REC in out nocopy TYPE_TAX_FCST_CALC_INPUTS_REC,
                                        A_RECORD_ID                pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;

   begin

      L_COUNT := G_TAX_FCST_CALC_INPUTS_REC.COUNT;
      if A_TAX_FCST_CALC_INPUTS_REC.EXISTS(1) then
         A_TAX_FCST_CALC_INPUTS_REC.DELETE;
      end if;
      L_ROW := 0;

      L_COUNT := G_TAX_FCST_CALC_INPUTS_HASH(A_RECORD_ID).COUNT;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_TAX_FCST_CALC_INPUTS_HASH(A_RECORD_ID)(I).ID;
         A_TAX_FCST_CALC_INPUTS_REC.EXTEND(1);
         A_TAX_FCST_CALC_INPUTS_REC(I) := G_TAX_FCST_CALC_INPUTS_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_fcst_calc_inputs' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_FCST_CALC_INPUTS;

   -- =============================================================================
   --  Function FILTER_DEF_TAX_UTIL
   -- =============================================================================
   function FILTER_DEF_TAX_UTIL(A_DEF_TAX_UTIL_REC in out nocopy TYPE_DEF_TAX_UTIL_REC,
                                A_RECORD_ID        pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;

   begin

      L_COUNT := G_DEF_TAX_UTIL_REC.COUNT;
      if A_DEF_TAX_UTIL_REC.EXISTS(1) then
         A_DEF_TAX_UTIL_REC.DELETE;
      end if;
      L_ROW := 0;
      begin
         L_COUNT := G_DEF_TAX_UTIL_HASH(A_RECORD_ID).COUNT;
      exception
         when NO_DATA_FOUND then
            return 0;
      end;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_DEF_TAX_UTIL_HASH(TO_CHAR(A_RECORD_ID))(I).ID;
         A_DEF_TAX_UTIL_REC.EXTEND(1);
         A_DEF_TAX_UTIL_REC(I) := G_DEF_TAX_UTIL_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_def_tax_util ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_DEF_TAX_UTIL;

   -- =============================================================================
   --  Function FILTER_TAX_FCST_CALC_OUTPUTS
   -- =============================================================================
   function FILTER_TAX_FCST_CALC_OUTPUTS(A_TAX_FCST_CALC_OUTPUT_REC     in out nocopy TAX_FCST_CALC_OUTPUT_TYPE,
                                         A_TAX_FCST_CALC_OUTPUT_SUB_REC in out nocopy TAX_FCST_CALC_OUTPUT_TYPE,
                                         A_RECORD_ID                    pls_integer,
                                         A_BOOK_ID                      pls_integer)
      return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;

   begin

      L_COUNT := A_TAX_FCST_CALC_OUTPUT_REC.COUNT;
      if A_TAX_FCST_CALC_OUTPUT_SUB_REC.EXISTS(1) then
         A_TAX_FCST_CALC_OUTPUT_SUB_REC.DELETE;
      end if;
      L_ROW := 0;
      begin
         L_COUNT := G_TAX_FCST_CALC_OUTPUT_HASH(TO_CHAR(A_RECORD_ID) || G_SEP || TO_CHAR(A_BOOK_ID)).COUNT;
      exception
         when NO_DATA_FOUND then
            return 0;
      end;

      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_TAX_FCST_CALC_OUTPUT_HASH(TO_CHAR(A_RECORD_ID) || G_SEP || TO_CHAR(A_BOOK_ID)) (I);
         A_TAX_FCST_CALC_OUTPUT_SUB_REC.EXTEND(1);
         A_TAX_FCST_CALC_OUTPUT_SUB_REC(I) := A_TAX_FCST_CALC_OUTPUT_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_fcst_calc_outputs ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_FCST_CALC_OUTPUTS;

   -- =============================================================================
   --  Function ADD_TAX_FCST_CALC_OUTPUTS
   -- =============================================================================
   function ADD_TAX_FCST_CALC_OUTPUTS(A_INDEX                    pls_integer,
                                      A_TAX_YEAR                 number,
                                      A_TAX_RECORD_ID            pls_integer,
                                      A_TAX_BOOK_ID              pls_integer,
                                      A_TAX_FCST_CALC_OUTPUT_REC in out nocopy TAX_FORECAST_OUTPUT%rowtype)
      return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;

   begin

      begin
         L_COUNT := G_TAX_FCST_CALC_OUTPUT_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_BOOK_ID))
                    .COUNT;
      exception
         when NO_DATA_FOUND then
            G_TAX_FCST_CALC_OUTPUT_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_BOOK_ID))(1) := A_INDEX;
            return 1;
      end;
      G_TAX_FCST_CALC_OUTPUT_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_BOOK_ID))(L_COUNT + 1) := A_INDEX;
      return 1;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_fcst_calc_outputs ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;
   end ADD_TAX_FCST_CALC_OUTPUTS;

   -- =============================================================================
   --  Function INSERT_TAX_FCST_CALC_OUTPUT
   -- =============================================================================
   function INSERT_TAX_FCST_CALC_OUTPUT(A_NUM_REC                  pls_integer,
                                        A_TAX_FCST_CALC_OUTPUT_REC in out nocopy TAX_FCST_CALC_OUTPUT_TYPE)
      return number as
      I      pls_integer;
      L_CODE pls_integer;

   begin
      forall I in 1 .. A_NUM_REC
         insert into TAX_FORECAST_OUTPUT
            (TAX_RECORD_ID, TAX_BOOK_ID, TAX_YEAR, TAX_FORECAST_VERSION_ID, TAX_BALANCE,
             ACCUM_RESERVE, DEPRECIATION, GAIN_LOSS, TAX_ADDITIONS, TAX_RETIREMENTS)
         values
            (A_TAX_FCST_CALC_OUTPUT_REC(I).TAX_RECORD_ID, A_TAX_FCST_CALC_OUTPUT_REC(I).TAX_BOOK_ID,
             A_TAX_FCST_CALC_OUTPUT_REC(I).TAX_YEAR,
             A_TAX_FCST_CALC_OUTPUT_REC(I).TAX_FORECAST_VERSION_ID,
             A_TAX_FCST_CALC_OUTPUT_REC(I).TAX_BALANCE, A_TAX_FCST_CALC_OUTPUT_REC(I).ACCUM_RESERVE,
             A_TAX_FCST_CALC_OUTPUT_REC(I).DEPRECIATION, A_TAX_FCST_CALC_OUTPUT_REC(I).GAIN_LOSS,
             A_TAX_FCST_CALC_OUTPUT_REC(I).TAX_ADDITIONS,
             A_TAX_FCST_CALC_OUTPUT_REC(I).TAX_RETIREMENTS);

      WRITE_LOG(G_JOB_NO, 6, 0, 'Insert Tax Fcst Calc Output Rows=' || TO_CHAR(A_NUM_REC));
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Insert Tax Fcst Calc Output ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end INSERT_TAX_FCST_CALC_OUTPUT;

   -- =============================================================================
   --  Function INSERT_DFIT_FORECAST_OUTPUT
   -- =============================================================================
   function INSERT_DFIT_FORECAST_OUTPUT(A_NUM_REC                  pls_integer,
                                        A_DFIT_FORECAST_OUTPUT_REC in out nocopy DFIT_FORECAST_OUTPUT_TYPE)
      return number as
      I      pls_integer;
      L_CODE pls_integer;

   begin
      forall I in 1 .. A_NUM_REC
         insert into DFIT_FORECAST_OUTPUT
            (TAX_RECORD_ID, TAX_YEAR, NORMALIZATION_ID, DEF_INCOME_TAX_BALANCE, NORM_DIFF_BALANCE,
             DEF_INCOME_TAX_PROVISION, DEF_INCOME_TAX_REVERSAL, ARAM_RATE, LIFE,
             DEF_INCOME_TAX_GAIN_LOSS, TAX_FORECAST_VERSION_ID, FROM_DEPRECIATION, TO_DEPRECIATION)
         values
            (A_DFIT_FORECAST_OUTPUT_REC(I).TAX_RECORD_ID, A_DFIT_FORECAST_OUTPUT_REC(I).TAX_YEAR,
             A_DFIT_FORECAST_OUTPUT_REC(I).NORMALIZATION_ID,
             A_DFIT_FORECAST_OUTPUT_REC(I).DEF_INCOME_TAX_BALANCE,
             A_DFIT_FORECAST_OUTPUT_REC(I).NORM_DIFF_BALANCE,
             A_DFIT_FORECAST_OUTPUT_REC(I).DEF_INCOME_TAX_PROVISION,
             A_DFIT_FORECAST_OUTPUT_REC(I).DEF_INCOME_TAX_REVERSAL,
             A_DFIT_FORECAST_OUTPUT_REC(I).ARAM_RATE, A_DFIT_FORECAST_OUTPUT_REC(I).LIFE,
             A_DFIT_FORECAST_OUTPUT_REC(I).DEF_INCOME_TAX_GAIN_LOSS,
             A_DFIT_FORECAST_OUTPUT_REC(I).TAX_FORECAST_VERSION_ID,
             A_DFIT_FORECAST_OUTPUT_REC(I).FROM_DEPRECIATION,
             A_DFIT_FORECAST_OUTPUT_REC(I).TO_DEPRECIATION);

      WRITE_LOG(G_JOB_NO, 6, 0, 'Insert Dfit Forecast Output Rows=' || TO_CHAR(A_NUM_REC));
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Insert Dfit Forecast Output ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end INSERT_DFIT_FORECAST_OUTPUT;

   -- =============================================================================
   --  Function CALC_FCST
   -- =============================================================================
   function CALC_FCST(A_JOB_NO number) return integer is
      cursor TAX_JOB_PARAMS_CUR is
         select VERSION,
                FCST_VERSION,
                TAX_YEAR,
                VINTAGE,
                TAX_BOOK_ID,
                TAX_CLASS_ID,
                COMPANY_ID,
                END_YEAR
           from TAX_JOB_PARAMS
          where JOB_NO = A_JOB_NO;
      L_VERSION             number(22, 0);
      L_TAX_YEAR            number(22, 2);
      L_VINTAGE             number(22, 0);
      L_TAX_BOOK_ID         number(22, 0);
      L_TAX_RECORD_ID       number(22, 0);
      L_TAX_CLASS_ID        number(22, 0);
      L_COMPANY_ID          number(22, 0);
      L_SELECT_FCST_VERSION pls_integer;
      L_FCST_VERSION        pls_integer;
      L_SELECT_END_YR       pls_integer;
      L_END_YR              pls_integer;
      type TAX_YEARS_TYPE is table of number(22, 2) index by pls_integer;
      TAX_YEARS                      TAX_YEARS_TYPE;
      TAX_YEAR_COUNT                 number(22, 2);
      L_CURRENT_YEAR                 number(22, 0);
      I                              pls_integer;
      L_START_ROW                    pls_integer;
      CODE                           integer;
      ROWS                           pls_integer;
      L_SHORT_MONTHS                 pls_integer;
      L_NEXT_YEAR                    number(22, 2);
      L_YEAR                         number(22, 2);
      L_YEAR_COUNT                   pls_integer;
      K                              pls_integer;
      L_COUNT                        pls_integer;
      L_TAX_FCST_CALC_INPUTS_ROWS    pls_integer;
      L_TAX_GRID_INDEX               pls_integer;
      L_TAX_GRID_ROWS                pls_integer;
      L_TAX_LIMITATION_SUB_REC       TYPE_TAX_LIMITATION_REC;
      L_TAX_LIMITATION_ROWS          pls_integer;
      L_TAX_LIMITATION_SUB_ROWS      pls_integer;
      L_TAX_RATES_ROWS               pls_integer;
      L_TAX_CONVENTION_ROWS          pls_integer;
      L_TAX_RATES_SUB_REC            TYPE_TAX_RATES_REC;
      L_TAX_RATES_SUB_ROWS           pls_integer;
      L_DEF_TAX_UTIL_SUB_ROWS        pls_integer;
      L_DEF_TAX_UTIL_ROWS            pls_integer;
      L_DFIT_RATES_ROWS              pls_integer;
      L_TAX_CONVENTION_INDEX         pls_integer;
      L_EXTRAORDINARY_CONV_INDEX     pls_integer;
      L_TAX_FCST_CALC_INPUTS_SUB_REC TYPE_TAX_FCST_CALC_INPUTS_REC;
      L_TAX_FCST_CALC_IN_SUB_ROWS    pls_integer;
      L_DEF_TAX_UTIL_SUB_REC         TYPE_DEF_TAX_UTIL_REC;
      L_CODE                         pls_integer;
      L_STATUS                       pls_integer;
      L_TAX_FCST_CALC_OUTPUT_REC     TAX_FCST_CALC_OUTPUT_TYPE;
      L_TAX_FCST_CALC_OUTPUTS_ROWS   pls_integer;
      L_TAX_FCST_FROM                TAX_FCST_CALC_OUTPUT_TYPE;
      L_TAX_FCST_FROM_ROWS           pls_integer;
      L_TAX_FCST_TO                  TAX_FCST_CALC_OUTPUT_TYPE;
      L_TAX_FCST_TO_ROWS             pls_integer;
      L_DEF_TAX_UTIL_INDEX           pls_integer;
      L_DEF_TAX_RATES_SUB_REC        TYPE_DFIT_RATES_REC;
      L_DEF_TAX_RATES_ROWS           pls_integer;
      L_TAX_DFIT_FCST_OUTPUTS        DFIT_FORECAST_OUTPUT_TYPE;
      L_TAX_DFIT_FCST_OUTPUTS_ROWS   pls_integer;
      L_DFIT_RATES_SUB_ROWS          pls_integer;
      L_DFIT_RATES_SUB_REC           TYPE_DFIT_RATES_REC;
      L_INDEX                        pls_integer;
      L_TAX_RATE_ID                  pls_integer;
      L_TOTAL_ROWS                   pls_integer;

   begin
      G_START_TIME := CURRENT_TIMESTAMP;
      --pp_plsql_debug.debug_procedure_on('fast_tax');
      G_JOB_NO  := A_JOB_NO;
      G_LINE_NO := 1;
      DBMS_OUTPUT.ENABLE(10000);
      WRITE_LOG(G_JOB_NO, 0, 0, 'Forecast Started Version=' || GET_VERSION());
      SET_SESSION_PARAMETER;

      G_TABLE_BOOK_IDS    := TABLE_LIST_ID_TYPE(-1);
      G_TABLE_VINTAGE_IDS := TABLE_LIST_ID_TYPE();
      G_TABLE_CLASS_IDS   := TABLE_LIST_ID_TYPE(-1);
      G_TABLE_COMPANY_IDS := TABLE_LIST_ID_TYPE(-1);

      L_TAX_DFIT_FCST_OUTPUTS      := DFIT_FORECAST_OUTPUT_TYPE();
      L_TAX_DFIT_FCST_OUTPUTS_ROWS := 0;
      L_TAX_FCST_CALC_OUTPUT_REC   := TAX_FCST_CALC_OUTPUT_TYPE();
      L_TAX_FCST_CALC_OUTPUTS_ROWS := 0;
      L_TAX_FCST_FROM              := TAX_FCST_CALC_OUTPUT_TYPE();
      L_TAX_FCST_TO                := TAX_FCST_CALC_OUTPUT_TYPE();

      L_DFIT_RATES_SUB_REC     := TYPE_DFIT_RATES_REC();
      L_DEF_TAX_UTIL_SUB_REC   := TYPE_DEF_TAX_UTIL_REC();
      L_TAX_LIMITATION_SUB_REC := TYPE_TAX_LIMITATION_REC();
      L_DEF_TAX_RATES_SUB_REC  := TYPE_DFIT_RATES_REC();

      G_TAX_GRID_SAVE1_REC := TYPE_TAX_GRID_SAVE_REC();
      G_TAX_GRID_SAVE2_REC := TYPE_TAX_GRID_SAVE_REC();
      G_DEF_TAX_UTIL_REC   := TYPE_DEF_TAX_UTIL_REC();

      TAX_YEAR_COUNT        := 0;
      G_TABLE_BOOK_COUNT    := 0;
      G_TABLE_VINTAGE_COUNT := 0;
      G_TABLE_CLASS_COUNT   := 0;
      G_TABLE_COMPANY_COUNT := 0;

      L_TAX_LIMITATION_SUB_REC := TYPE_TAX_LIMITATION_REC();
      L_TAX_RATES_SUB_REC      := TYPE_TAX_RATES_REC();

      open TAX_JOB_PARAMS_CUR;
      loop
         fetch TAX_JOB_PARAMS_CUR
            into L_VERSION,
                 L_SELECT_FCST_VERSION,
                 L_TAX_YEAR,
                 L_VINTAGE,
                 L_TAX_BOOK_ID,
                 L_TAX_CLASS_ID,
                 L_COMPANY_ID,
                 L_SELECT_END_YR;
         if not L_VERSION is null then
            G_VERSION := L_VERSION;
         end if;
         if not L_TAX_YEAR is null then
            TAX_YEAR_COUNT := TAX_YEAR_COUNT + 1;
            TAX_YEARS(TAX_YEAR_COUNT) := L_TAX_YEAR;
         end if;
         if not L_SELECT_FCST_VERSION is null then
            L_FCST_VERSION := L_SELECT_FCST_VERSION;
         end if;
         if not L_SELECT_END_YR is null then
            L_END_YR := L_SELECT_END_YR;
         end if;
         if not L_VINTAGE is null then
            G_TABLE_VINTAGE_COUNT := 1 + G_TABLE_VINTAGE_COUNT;
            G_TABLE_VINTAGE_IDS.EXTEND;
            G_TABLE_VINTAGE_IDS(G_TABLE_VINTAGE_COUNT) := L_VINTAGE;
         end if;
         if not L_TAX_BOOK_ID is null then
            G_TABLE_BOOK_COUNT := 1 + G_TABLE_BOOK_COUNT;
            G_TABLE_BOOK_IDS.EXTEND;
            G_TABLE_BOOK_IDS(G_TABLE_BOOK_COUNT) := L_TAX_BOOK_ID;
         end if;
         if not L_TAX_CLASS_ID is null then
            G_TABLE_CLASS_COUNT := 1 + G_TABLE_CLASS_COUNT;
            G_TABLE_CLASS_IDS.EXTEND;
            G_TABLE_CLASS_IDS(G_TABLE_CLASS_COUNT) := L_TAX_CLASS_ID;
         end if;
         if not L_COMPANY_ID is null then
            G_TABLE_COMPANY_COUNT := 1 + G_TABLE_COMPANY_COUNT;
            G_TABLE_COMPANY_IDS.EXTEND;
            G_TABLE_COMPANY_IDS(G_TABLE_COMPANY_COUNT) := L_COMPANY_ID;
         end if;
         exit when TAX_JOB_PARAMS_CUR%notfound;
      end loop;

      G_START_YEAR := TAX_YEARS(TAX_YEARS.FIRST);
      G_END_YEAR   := TAX_YEARS(TAX_YEARS.LAST);

      L_TAX_CONVENTION_ROWS := GET_TAX_CONVENTION();

      L_TAX_RATES_ROWS := GET_TAX_RATES();

      L_TAX_LIMITATION_ROWS := GET_TAX_LIMITATION();

      L_DFIT_RATES_ROWS := GET_DFIT_RATES();

      L_TAX_FCST_CALC_INPUTS_ROWS := GET_TAX_FCST_CALC_INPUTS();
      L_TOTAL_ROWS                := 0;
      loop
         L_TAX_FCST_CALC_OUTPUTS_ROWS := 0;
         L_TAX_DFIT_FCST_OUTPUTS_ROWS := 0;

         L_TAX_GRID_ROWS := GET_TAX_GRID(L_START_ROW);
         exit when L_TAX_GRID_ROWS = 0;
         L_DEF_TAX_UTIL_ROWS := GET_DEF_TAX_UTIL();

         L_TOTAL_ROWS := L_TOTAL_ROWS + L_TAX_GRID_ROWS;
         for L_TAX_GRID_INDEX in L_START_ROW .. L_TAX_GRID_ROWS
         loop
            I               := 0;
            L_TAX_RECORD_ID := G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_RECORD_ID;
            L_TAX_BOOK_ID   := G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_BOOK_ID;

            if L_TAX_RECORD_ID = 141458 then
               null;
            end if;

            L_TAX_CONVENTION_INDEX := G_TAX_CONVENTION_HASH(G_TAX_GRID_REC(L_TAX_GRID_INDEX).CONVENTION_ID);

            L_EXTRAORDINARY_CONV_INDEX := G_TAX_CONVENTION_HASH(G_TAX_GRID_REC(L_TAX_GRID_INDEX).EXTRAORDINARY_CONVENTION);

            L_TAX_LIMITATION_SUB_ROWS := FILTER_TAX_LIMITATION(L_TAX_LIMITATION_SUB_REC,
                                                               G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_LIMIT_ID);

            L_TAX_RATES_SUB_ROWS := FILTER_TAX_RATES(L_TAX_RATES_SUB_REC,
                                                     G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_RATE_ID);

            if L_TAX_FCST_CALC_INPUTS_ROWS = 0 then
               L_TAX_FCST_CALC_IN_SUB_ROWS := 0;
            else
               L_TAX_FCST_CALC_IN_SUB_ROWS := FILTER_TAX_FCST_CALC_INPUTS(L_TAX_FCST_CALC_INPUTS_SUB_REC,
                                                                          L_TAX_RECORD_ID);
            end if;
            if L_TAX_RATES_SUB_ROWS != 0 then
               L_STATUS := CALC(L_FCST_VERSION,
                                L_TAX_GRID_INDEX,
                                L_TAX_RATES_SUB_REC,
                                L_TAX_RATES_SUB_ROWS,
                                G_TAX_CONVENTION_REC(L_TAX_CONVENTION_INDEX),
                                G_TAX_CONVENTION_REC(L_EXTRAORDINARY_CONV_INDEX),
                                L_TAX_LIMITATION_SUB_REC,
                                L_TAX_LIMITATION_SUB_ROWS,
                                L_TAX_FCST_CALC_INPUTS_SUB_REC,
                                L_TAX_FCST_CALC_IN_SUB_ROWS,
                                L_TAX_FCST_CALC_OUTPUT_REC,
                                L_TAX_FCST_CALC_OUTPUTS_ROWS,
                                L_END_YR);
               if L_STATUS = -1 then
                  goto FINISH;
               end if;
               if L_TAX_GRID_INDEX <> L_TAX_GRID_ROWS then
                  if G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_RECORD_ID = G_TAX_GRID_REC(L_TAX_GRID_INDEX + 1).TAX_RECORD_ID then
                     CONTINUE;
                  end if;
               end if;

               L_DEF_TAX_UTIL_SUB_ROWS := FILTER_DEF_TAX_UTIL(L_DEF_TAX_UTIL_SUB_REC,
                                                              G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_RECORD_ID);

               for L_DEF_TAX_UTIL_INDEX in 1 .. L_DEF_TAX_UTIL_SUB_ROWS
               loop
                  L_TAX_FCST_FROM_ROWS := FILTER_TAX_FCST_CALC_OUTPUTS(L_TAX_FCST_CALC_OUTPUT_REC,
                                                                       L_TAX_FCST_FROM,
                                                                       L_DEF_TAX_UTIL_SUB_REC(L_DEF_TAX_UTIL_INDEX).TAX_RECORD_ID,
                                                                       L_DEF_TAX_UTIL_SUB_REC(L_DEF_TAX_UTIL_INDEX).NORM_FROM_TAX);

                  L_TAX_FCST_TO_ROWS := FILTER_TAX_FCST_CALC_OUTPUTS(L_TAX_FCST_CALC_OUTPUT_REC,
                                                                     L_TAX_FCST_TO,
                                                                     L_DEF_TAX_UTIL_SUB_REC(L_DEF_TAX_UTIL_INDEX).TAX_RECORD_ID,
                                                                     L_DEF_TAX_UTIL_SUB_REC(L_DEF_TAX_UTIL_INDEX).NORM_TO_TAX);

                  if L_TAX_FCST_FROM_ROWS = 0 then
                     exit;
                  end if;
                  if L_TAX_FCST_TO_ROWS = 0 then
                     exit;
                  end if;

                  L_DFIT_RATES_SUB_ROWS := FILTER_DFIT_RATES(L_DFIT_RATES_SUB_REC,
                                                             L_DEF_TAX_UTIL_SUB_REC(L_DEF_TAX_UTIL_INDEX).DEF_INCOME_TAX_RATE_ID);

                  --l_tax_rate_id = find_tax_rate_id( to_char(g_tax_grid_rec(l_tax_grid_index).tax_record_id) || g_sep  ||
                  --                      to_char(l_def_tax_util_sub(l_def_tax_util_index).norm_from_tax))

                  if G_TAX_GRID_HASH(TO_CHAR(G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_RECORD_ID) || G_SEP || TO_CHAR(L_DEF_TAX_UTIL_SUB_REC(L_DEF_TAX_UTIL_INDEX).NORM_FROM_TAX))
                   .EXISTS(1) then
                     L_INDEX       := G_TAX_GRID_HASH(TO_CHAR(G_TAX_GRID_REC(L_TAX_GRID_INDEX).TAX_RECORD_ID) || G_SEP ||
                                                      TO_CHAR(L_DEF_TAX_UTIL_SUB_REC(L_DEF_TAX_UTIL_INDEX).NORM_FROM_TAX))(1).ID;
                     L_TAX_RATE_ID := G_TAX_GRID_REC(L_INDEX).TAX_RATE_ID;
                  end if;

                  L_TAX_RATES_SUB_ROWS := FILTER_TAX_RATES(L_TAX_RATES_SUB_REC, L_TAX_RATE_ID);
                  if G_DEF_TAX_UTIL_REC(L_DEF_TAX_UTIL_INDEX).TAX_RECORD_ID = 130606 then
                     null;
                  end if;
                  L_STATUS := DEF_CALC(L_FCST_VERSION,
                                       L_DEF_TAX_UTIL_INDEX,
                                       L_DEF_TAX_UTIL_SUB_REC,
                                       L_DFIT_RATES_SUB_REC,
                                       L_DFIT_RATES_SUB_ROWS,
                                       L_TAX_FCST_FROM,
                                       L_TAX_FCST_FROM_ROWS,
                                       L_TAX_FCST_TO,
                                       L_TAX_FCST_TO_ROWS,
                                       L_TAX_DFIT_FCST_OUTPUTS,
                                       L_TAX_DFIT_FCST_OUTPUTS_ROWS,
                                       L_TAX_RATES_SUB_REC,
                                       L_TAX_RATES_SUB_ROWS,
                                       L_END_YR);
                  if L_STATUS = -1 then
                     goto FINISH;
                  end if;

               end loop;

            end if;
         end loop;
         <<FINISH>>

         L_STATUS := INSERT_TAX_FCST_CALC_OUTPUT(L_TAX_FCST_CALC_OUTPUTS_ROWS,
                                                 L_TAX_FCST_CALC_OUTPUT_REC);
         L_TAX_FCST_CALC_OUTPUT_REC.DELETE;
         L_TAX_FCST_CALC_OUTPUTS_ROWS := 0;

         L_STATUS := INSERT_DFIT_FORECAST_OUTPUT(L_TAX_DFIT_FCST_OUTPUTS_ROWS,
                                                 L_TAX_DFIT_FCST_OUTPUTS);
         L_TAX_DFIT_FCST_OUTPUTS.DELETE;
         L_TAX_DFIT_FCST_OUTPUTS_ROWS := 0;
      end loop;
      commit;
      WRITE_LOG(G_JOB_NO, 0, 0, 'Total Calculation Rows=' || TO_CHAR(L_TOTAL_ROWS));
      WRITE_LOG(G_JOB_NO, 0, 0, 'Finished');
      return 0;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   'Fsct Calc ' || sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end CALC_FCST;

   -- =============================================================================
   --  Function SET_SESSION_PARAMETER
   -- =============================================================================
   procedure SET_SESSION_PARAMETER is
      cursor SESSION_PARAM_CUR is
         select PARAMETER, value, type
           from PP_SESSION_PARAMETERS
          where LOWER(USERS) = 'all'
             or LOWER(USERS) = 'fcst';
      L_COUNT pls_integer;
      I       pls_integer;
      type TYPE_SESSION_PARAM_REC is table of SESSION_PARAM_CUR%rowtype;
      L_SESSION_PARAM_REC TYPE_SESSION_PARAM_REC;
      L_CODE              pls_integer;
      L_SQL               varchar2(200);

   begin

      open SESSION_PARAM_CUR;
      fetch SESSION_PARAM_CUR bulk collect
         into L_SESSION_PARAM_REC;
      close SESSION_PARAM_CUR;

      L_COUNT := L_SESSION_PARAM_REC.COUNT;
      for I in 1 .. L_COUNT
      loop
         if L_SESSION_PARAM_REC(I).TYPE = 'number' or L_SESSION_PARAM_REC(I).TYPE = 'boolean' then
            L_SQL := 'alter session set ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ' || L_SESSION_PARAM_REC(I).VALUE;
         elsif L_SESSION_PARAM_REC(I).TYPE = 'string' then
            L_SQL := 'alter session set ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ''' || L_SESSION_PARAM_REC(I).VALUE || '''';
         end if;
         execute immediate L_SQL;
         WRITE_LOG(G_JOB_NO,
                   0,
                   0,
                   'Set Session Parameter ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ' || L_SESSION_PARAM_REC(I).VALUE);
      end loop;
      return;
   exception
      when NO_DATA_FOUND then
         return;
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   5,
                   L_CODE,
                   'Set Session Parameter ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return;
   end SET_SESSION_PARAMETER;

   -- =============================================================================
   --  Procedure WRITE_LOG
   -- =============================================================================
   procedure WRITE_LOG(A_JOB_NO     number,
                       A_ERROR_TYPE number,
                       A_CODE       number,
                       A_MSG        varchar2) as
      pragma autonomous_transaction;
      L_CODE   pls_integer;
      L_MSG    varchar(2000);
      L_CUR_TS timestamp;

   begin
      L_CUR_TS := CURRENT_TIMESTAMP;
      L_MSG    := A_MSG;
      DBMS_OUTPUT.PUT_LINE(L_MSG);
      insert into TAX_JOB_LOG
         (JOB_NO, LINE_NO, LOG_DATE, ERROR_TYPE, error_code, MSG)
      values
         (A_JOB_NO, G_LINE_NO, sysdate, A_ERROR_TYPE, A_CODE, L_MSG);
      commit;
      G_LINE_NO := G_LINE_NO + 1;
      return;
   exception
      when others then
         L_CODE := sqlcode;
         DBMS_OUTPUT.PUT_LINE('Write Log ' || sqlerrm(L_CODE) || ' ' ||
                              DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return;
   end WRITE_LOG;

   -- =============================================================================
   --  Function CALC
   -- =============================================================================
   function CALC(A_VERSION                pls_integer,
                 A_TAX_GRID_INDEX         pls_integer,
                 A_TAX_RATES_SUB_REC      TYPE_TAX_RATES_REC,
                 A_TAX_RATES_ROWS         pls_integer,
                 A_TAX_CONVENTION_SUB_REC TAX_CONVENTION_CUR%rowtype,

                 A_EXTRAORD_CONVENTION_SUB_REC TAX_CONVENTION_CUR%rowtype,

                 A_TAX_LIMITATION_SUB_REC     TYPE_TAX_LIMITATION_REC,
                 A_TAX_LIMITATION_ROWS        pls_integer,
                 A_TAX_FCST_CALC_INPUTS       TYPE_TAX_FCST_CALC_INPUTS_REC,
                 A_TAX_FCST_CALC_INPUTS_ROWS  pls_integer,
                 A_TAX_FCST_CALC_OUTPUTS      in out nocopy TAX_FCST_CALC_OUTPUT_TYPE,
                 A_TAX_FCST_CALC_OUTPUTS_ROWS in out nocopy pls_integer,
                 A_END_YR                     pls_integer) return pls_integer is

      L_CODE                    pls_integer;
      L_TAX_RECORD_ID           pls_integer;
      L_TAX_BOOK_ID             pls_integer;
      L_TAX_FORECAST_VERSION_ID pls_integer;
      L_NN                      pls_integer;
      L_NUM_INPUTS              pls_integer;

      L_NET      varchar2(10);
      L_ORIG_NET varchar2(10);

      L_METHOD   pls_integer;
      L_ROUND    pls_integer;
      L_RLIFE    pls_integer;
      L_WLIFE    pls_integer;
      L_SWITCHED pls_integer;
      L_LIFE     number;
      L_REM_LIFE number;
      L_FLIFE    BINARY_DOUBLE;

      L_SCOOP      boolean;
      L_DONE       boolean;
      L_ALLOCATION boolean;

      L_TAX_LIMITATION BINARY_DOUBLE;

      I         pls_integer;
      J         pls_integer;
      K         pls_integer;
      L_VINTAGE pls_integer;

      L_RATIO  BINARY_DOUBLE;
      L_RATIO1 BINARY_DOUBLE;
      L_CHANGE BINARY_DOUBLE;
      L_AMOUNT BINARY_DOUBLE;

      type NUM_ARRAY is table of BINARY_DOUBLE index by binary_integer;
      L_TAX_RATES NUM_ARRAY;
      L_DEPR_RATE BINARY_DOUBLE := 0;

      L_RETIRE_DEPR_CONV     BINARY_DOUBLE := 0;
      L_RETIRE_BAL_CONV      BINARY_DOUBLE := 0;
      L_RETIRE_RES_CONV      BINARY_DOUBLE := 0;
      L_GAIN_LOSS_CONV       BINARY_DOUBLE := 0;
      L_CAP_GAIN_CONV        BINARY_DOUBLE := 0;
      L_SALVAGE_CONV         BINARY_DOUBLE := 0;
      L_EST_SALVAGE_CONV     BINARY_DOUBLE := 0;
      L_RETIRE_DEPR_RATIO    BINARY_DOUBLE := 0;
      L_RETIRE_RES_RATIO     BINARY_DOUBLE := 0;
      L_RETIRE_BAL_RATIO     BINARY_DOUBLE := 0;
      L_SALVAGE_RATIO        BINARY_DOUBLE := 0;
      L_BEG_SALVAGE_RATIO    BINARY_DOUBLE := 0;
      L_ADR_SALVAGE_RATIO    BINARY_DOUBLE := 0;
      L_SV_RETIRE_DEPR_RATIO BINARY_DOUBLE := 0;
      L_DEPR_SALVAGE_RATIO   BINARY_DOUBLE := 0;

      L_EX_RETIRE_DEPR_CONV     BINARY_DOUBLE := 0;
      L_EX_RETIRE_BAL_CONV      BINARY_DOUBLE := 0;
      L_EX_RETIRE_RES_CONV      BINARY_DOUBLE := 0;
      L_EX_GAIN_LOSS_CONV       BINARY_DOUBLE := 0;
      L_EX_CAP_GAIN_CONV        BINARY_DOUBLE := 0;
      L_EX_SALVAGE_CONV         BINARY_DOUBLE := 0;
      L_EX_EST_SALVAGE_CONV     BINARY_DOUBLE := 0;
      L_EX_RETIRE_DEPR_RATIO    BINARY_DOUBLE := 0;
      L_EX_RETIRE_RES_RATIO     BINARY_DOUBLE := 0;
      L_EX_RETIRE_BAL_RATIO     BINARY_DOUBLE := 0;
      L_EX_SALVAGE_RATIO        BINARY_DOUBLE := 0;
      L_EX_BEG_SALVAGE_RATIO    BINARY_DOUBLE := 0;
      L_SV_EX_RETIRE_DEPR_RATIO BINARY_DOUBLE := 0;
      L_EX_DEPR_SALVAGE_RATIO   BINARY_DOUBLE := 0;

      L_BEGIN_RES_IMPACT        BINARY_DOUBLE := 0;
      L_RESERVE                 BINARY_DOUBLE := 0;
      L_RETIREMENT_DEPR         BINARY_DOUBLE := 0;
      L_EX_RETIREMENT_DEPR      BINARY_DOUBLE := 0;
      L_EX_RETIRE_RES_IMPACT    BINARY_DOUBLE := 0;
      L_TAX_YEAR                BINARY_DOUBLE := 0;
      L_BOOK_BALANCE            BINARY_DOUBLE := 0;
      L_TAX_BALANCE             BINARY_DOUBLE := 0;
      L_REMAINING_LIFE          BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE           BINARY_DOUBLE := 0;
      L_SL_RESERVE              BINARY_DOUBLE := 0;
      L_DEPRECIABLE_BASE        BINARY_DOUBLE := 0;
      L_FIXED_DEPRECIABLE_BASE  BINARY_DOUBLE := 0;
      L_ACTUAL_SALVAGE          BINARY_DOUBLE := 0;
      L_ESTIMATED_SALVAGE       BINARY_DOUBLE := 0;
      L_ACCUM_SALVAGE           BINARY_DOUBLE := 0;
      L_ADDITIONS               BINARY_DOUBLE := 0;
      L_TRANSFERS               BINARY_DOUBLE := 0;
      L_ADJUSTMENTS             BINARY_DOUBLE := 0;
      L_RETIREMENTS             BINARY_DOUBLE := 0;
      L_EXTRAORDINARY_RETIRES   BINARY_DOUBLE := 0;
      L_ACCUM_ORDINARY_RETIRES  BINARY_DOUBLE := 0;
      L_DEPRECIATION            BINARY_DOUBLE := 0;
      L_COST_OF_REMOVAL         BINARY_DOUBLE := 0;
      L_GAIN_LOSS               BINARY_DOUBLE := 0;
      L_CAPITAL_GAIN_LOSS       BINARY_DOUBLE := 0;
      L_EST_SALVAGE_PCT         BINARY_DOUBLE := 0;
      L_BOOK_BALANCE_END        BINARY_DOUBLE := 0;
      L_TAX_BALANCE_END         BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE_END       BINARY_DOUBLE := 0;
      L_SL_RESERVE_END          BINARY_DOUBLE := 0;
      L_ACCUM_SALVAGE_END       BINARY_DOUBLE := 0;
      L_ACCUM_ORDIN_RETIRES_END BINARY_DOUBLE := 0;
      L_RETIRE_INVOL_CONV       BINARY_DOUBLE := 0;
      L_SALVAGE_INVOL_CONV      BINARY_DOUBLE := 0;
      L_SALVAGE_EXTRAORD        BINARY_DOUBLE := 0;
      L_CALC_DEPRECIATION       BINARY_DOUBLE := 0;
      L_OVER_ADJ_DEPRECIATION   BINARY_DOUBLE := 0;
      L_RETIRE_RES_IMPACT       BINARY_DOUBLE := 0;
      L_TRANSFER_RES_IMPACT     BINARY_DOUBLE := 0;
      L_SALVAGE_RES_IMPACT      BINARY_DOUBLE := 0;
      L_ADJUSTED_RETIRE_BASIS   BINARY_DOUBLE := 0;
      L_RESERVE_AT_SWITCH       BINARY_DOUBLE := 0;
      L_RESERVE_AT_SWITCH_END   BINARY_DOUBLE := 0;
      L_QUANTITY                BINARY_DOUBLE := 0;

      -- Activity Variables

      L_BOOK_RETIREMENT          BINARY_DOUBLE;
      L_BOOK_ADJUSTMENT          BINARY_DOUBLE;
      L_BOOK_ADDITION            BINARY_DOUBLE;
      L_BOOK_EXTRAORD_RETIREMENT BINARY_DOUBLE;
      L_INPUT_DEPRECIATION       BINARY_DOUBLE;
      L_EX_BEGIN_RES_IMPACT      BINARY_DOUBLE;

      L_NUM       pls_integer;
      L_ROW_COUNT pls_integer;
      L_ROW       pls_integer;
      L_STATUS    pls_integer;

   begin

      -- Move the items in the Tax_depreciation table to variables
      -- Get Rid of The Statements we don't end up needing

      L_BOOK_BALANCE           := G_TAX_GRID_REC(A_TAX_GRID_INDEX).BOOK_BALANCE_END;
      L_TAX_BALANCE            := G_TAX_GRID_REC(A_TAX_GRID_INDEX).TAX_BALANCE_END;
      L_ACCUM_RESERVE          := G_TAX_GRID_REC(A_TAX_GRID_INDEX).ACCUM_RESERVE_END;
      L_SL_RESERVE             := G_TAX_GRID_REC(A_TAX_GRID_INDEX).SL_RESERVE_END;
      L_ACCUM_ORDINARY_RETIRES := G_TAX_GRID_REC(A_TAX_GRID_INDEX).ACCUM_ORDIN_RETIRES_END;
      L_ACCUM_SALVAGE          := G_TAX_GRID_REC(A_TAX_GRID_INDEX).ACCUM_SALVAGE_END;
      L_FIXED_DEPRECIABLE_BASE := G_TAX_GRID_REC(A_TAX_GRID_INDEX).FIXED_DEPRECIABLE_BASE;
      L_ESTIMATED_SALVAGE      := G_TAX_GRID_REC(A_TAX_GRID_INDEX).ESTIMATED_SALVAGE;
      L_EST_SALVAGE_PCT        := G_TAX_GRID_REC(A_TAX_GRID_INDEX).EST_SALVAGE_PCT;
      L_RESERVE_AT_SWITCH      := G_TAX_GRID_REC(A_TAX_GRID_INDEX).RESERVE_AT_SWITCH_END;
      L_QUANTITY               := G_TAX_GRID_REC(A_TAX_GRID_INDEX).QUANTITY;

      L_TAX_YEAR      := G_TAX_GRID_REC(A_TAX_GRID_INDEX).TAX_YEAR + 1;
      L_TAX_RECORD_ID := G_TAX_GRID_REC(A_TAX_GRID_INDEX).TAX_RECORD_ID;
      L_TAX_BOOK_ID   := G_TAX_GRID_REC(A_TAX_GRID_INDEX).TAX_BOOK_ID;

      if G_TAX_GRID_REC(A_TAX_GRID_INDEX).BOOK_DEPR_ALLOC_IND = 1 then
         L_ALLOCATION         := true;
         L_INPUT_DEPRECIATION := G_TAX_GRID_REC(A_TAX_GRID_INDEX).DEPRECIATION;
      else
         L_ALLOCATION := false;
      end if;

      L_NUM_INPUTS := A_TAX_FCST_CALC_INPUTS_ROWS;

      --dw_tax_fcst_calc_inputs.setsort('tax_year a')
      --dw_tax_fcst_calc_inputs.sort()

      -- Get The Tax Conventions

      L_RETIRE_DEPR_CONV := A_TAX_CONVENTION_SUB_REC.RETIRE_DEPR_ID;
      L_RETIRE_BAL_CONV  := A_TAX_CONVENTION_SUB_REC.RETIRE_BAL_ID;
      L_RETIRE_RES_CONV  := A_TAX_CONVENTION_SUB_REC.RETIRE_RESERVE_ID;
      L_GAIN_LOSS_CONV   := A_TAX_CONVENTION_SUB_REC.GAIN_LOSS_ID;
      --cap_gain_conv    := a_tax_convention_sub_rec.cap_gain_loss_id;
      L_SALVAGE_CONV     := A_TAX_CONVENTION_SUB_REC.SALVAGE_ID;
      L_EST_SALVAGE_CONV := A_TAX_CONVENTION_SUB_REC.EST_SALVAGE_ID;

      L_EX_RETIRE_DEPR_CONV := A_EXTRAORD_CONVENTION_SUB_REC.RETIRE_DEPR_ID;
      L_EX_RETIRE_BAL_CONV  := A_EXTRAORD_CONVENTION_SUB_REC.RETIRE_BAL_ID;
      L_EX_RETIRE_RES_CONV  := A_EXTRAORD_CONVENTION_SUB_REC.RETIRE_RESERVE_ID;
      L_EX_GAIN_LOSS_CONV   := A_EXTRAORD_CONVENTION_SUB_REC.GAIN_LOSS_ID;
      --cap_gain_conv    := a_extraord_convention_rec.cap_gain_loss_id;
      L_EX_SALVAGE_CONV     := A_EXTRAORD_CONVENTION_SUB_REC.SALVAGE_ID;
      L_EX_EST_SALVAGE_CONV := A_EXTRAORD_CONVENTION_SUB_REC.EST_SALVAGE_ID;

      --g_perform.put("time","after gets")

      -- Get the Tax Rates, and lives and conventions for remaining life rates.

      --dw_tax_rates.setsort('tax_rates_year a')
      --dw_tax_rates.sort()

      for I in 1 .. A_TAX_RATES_ROWS
      loop
         L_TAX_RATES(I) := A_TAX_RATES_SUB_REC(I).RATE;
      end loop;

      L_ORIG_NET := A_TAX_RATES_SUB_REC(1).NET_GROSS;
      L_ROUND    := A_TAX_RATES_SUB_REC(1).ROUNDING_CONVENTION;
      L_LIFE     := A_TAX_RATES_SUB_REC(1).LIFE;
      L_RLIFE    := A_TAX_RATES_SUB_REC(1).REMAINING_LIFE_PLAN;
      L_METHOD   := A_TAX_RATES_SUB_REC(1).START_METHOD;
      L_VINTAGE  := G_TAX_GRID_REC(A_TAX_GRID_INDEX).VINTAGE_YEAR;
      L_SWITCHED := A_TAX_RATES_SUB_REC(1).SWITCHED_YEAR;

      -- Set The Retirement Depreciation Convention

      case
         when ROUND(L_RETIRE_DEPR_CONV) = 1 then
            L_RETIRE_DEPR_RATIO := 0;
         when ROUND(L_RETIRE_DEPR_CONV) = 2 then
            L_RETIRE_DEPR_RATIO := .5;
         when ROUND(L_RETIRE_DEPR_CONV) = 3 then
            L_RETIRE_DEPR_RATIO := .5;
         when ROUND(L_RETIRE_DEPR_CONV) = 4 then
            L_RETIRE_DEPR_RATIO := 1;
      end case;

      L_SV_RETIRE_DEPR_RATIO := L_RETIRE_DEPR_RATIO;

      -- Set The Retirement balance Convention

      case
         when ROUND(L_RETIRE_BAL_CONV) = 1 or ROUND(L_RETIRE_BAL_CONV) = 3 or
              ROUND(L_RETIRE_BAL_CONV) = 4 then
            L_RETIRE_BAL_RATIO := 0;
         when ROUND(L_RETIRE_BAL_CONV) = 2 then
            L_RETIRE_BAL_RATIO := 1;
      end case;

      -- Set The Salvage Convention

      case
         when ROUND(L_SALVAGE_CONV) = 1 then
            L_SALVAGE_RATIO      := 0;
            L_BEG_SALVAGE_RATIO  := 0;
            L_DEPR_SALVAGE_RATIO := 0;
         when ROUND(L_SALVAGE_CONV) = 2 then
            L_SALVAGE_RATIO      := 1;
            L_BEG_SALVAGE_RATIO  := 1;
            L_DEPR_SALVAGE_RATIO := 0;
         when ROUND(L_SALVAGE_CONV) = 3 then
            L_SALVAGE_RATIO      := 1;
            L_BEG_SALVAGE_RATIO  := 0;
            L_DEPR_SALVAGE_RATIO := 0;
         when ROUND(L_SALVAGE_CONV) = 4 then
            L_SALVAGE_RATIO      := 1;
            L_BEG_SALVAGE_RATIO  := 1;
            L_DEPR_SALVAGE_RATIO := 1;
      end case;

      -- Set The Retirement Depreciation Convention

      case
         when ROUND(L_EX_RETIRE_DEPR_CONV) = 1 then
            L_EX_RETIRE_DEPR_RATIO := 0;
         when ROUND(L_EX_RETIRE_DEPR_CONV) = 2 then
            L_EX_RETIRE_DEPR_RATIO := .5;
         when ROUND(L_EX_RETIRE_DEPR_CONV) = 3 then
            L_EX_RETIRE_DEPR_RATIO := .5;
         when ROUND(L_EX_RETIRE_DEPR_CONV) = 4 then
            L_EX_RETIRE_DEPR_RATIO := 1;
      end case;

      L_SV_EX_RETIRE_DEPR_RATIO := L_EX_RETIRE_DEPR_RATIO;

      -- Set The Retirement balance Convention

      case
         when ROUND(L_EX_RETIRE_BAL_CONV) = 1 or ROUND(L_EX_RETIRE_BAL_CONV) = 3 or
              ROUND(L_EX_RETIRE_BAL_CONV) = 4 then
            L_EX_RETIRE_BAL_RATIO := 0;
         when ROUND(L_EX_RETIRE_BAL_CONV) = 2 then
            L_EX_RETIRE_BAL_RATIO := 1;
      end case;

      -- Set The Salvage Convention

      case
         when ROUND(L_EX_RETIRE_BAL_CONV) = 1 then
            L_EX_SALVAGE_RATIO      := 0;
            L_EX_BEG_SALVAGE_RATIO  := 0;
            L_EX_DEPR_SALVAGE_RATIO := 0;
         when ROUND(L_EX_RETIRE_BAL_CONV) = 2 then
            L_EX_SALVAGE_RATIO      := 1;
            L_EX_BEG_SALVAGE_RATIO  := 1;
            L_EX_DEPR_SALVAGE_RATIO := 0;
         when ROUND(L_EX_RETIRE_BAL_CONV) = 3 then
            L_EX_SALVAGE_RATIO      := 1;
            L_EX_BEG_SALVAGE_RATIO  := 0;
            L_EX_DEPR_SALVAGE_RATIO := 0;
         when ROUND(L_EX_RETIRE_BAL_CONV) = 4 then
            L_EX_SALVAGE_RATIO      := 1;
            L_EX_BEG_SALVAGE_RATIO  := 1;
            L_EX_DEPR_SALVAGE_RATIO := 1;
      end case;

      -- Set the ADR Salvage Convention

      case
         when ROUND(L_EST_SALVAGE_CONV) = 4 then
            L_ADR_SALVAGE_RATIO := 0;
         else
            L_ADR_SALVAGE_RATIO := 1;
      end case;

      --
      -- Put out an Initial Tax Record
      --
      --dw_tax_fcst_calc_outputs.insertrow(0)
      --nn = dw_tax_fcst_calc_outputs.rowcount()

      A_TAX_FCST_CALC_OUTPUTS.EXTEND;

      L_ROW_COUNT     := A_TAX_FCST_CALC_OUTPUTS.COUNT;
      L_TAX_BALANCE   := ROUND(L_TAX_BALANCE, 2);
      L_ACCUM_RESERVE := ROUND(L_ACCUM_RESERVE, 2);

      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).TAX_BALANCE := L_TAX_BALANCE;
      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).ACCUM_RESERVE := L_ACCUM_RESERVE;

      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).TAX_RECORD_ID := L_TAX_RECORD_ID;
      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).TAX_BOOK_ID := L_TAX_BOOK_ID;
      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).TAX_YEAR := L_TAX_YEAR - 1;
      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).TAX_FORECAST_VERSION_ID := A_VERSION;

      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).DEPRECIATION := 0;
      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).GAIN_LOSS := 0;
      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).TAX_ADDITIONS := 0;
      A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT).TAX_RETIREMENTS := 0;

      A_TAX_FCST_CALC_OUTPUTS_ROWS := 1 + A_TAX_FCST_CALC_OUTPUTS_ROWS;
      L_STATUS                     := ADD_TAX_FCST_CALC_OUTPUTS(A_TAX_FCST_CALC_OUTPUTS_ROWS,
                                                                L_TAX_YEAR - 1,
                                                                L_TAX_RECORD_ID,
                                                                L_TAX_BOOK_ID,
                                                                A_TAX_FCST_CALC_OUTPUTS(L_ROW_COUNT));
      /*add_tax_fcst_calc_outputs(*tax_fcst_calc_outputs);
      if((*tax_fcst_calc_outputs)->next == NULL)
           {
           memory_error("Forecast Calculation",i);
           return -1;
           }
      (*tax_fcst_calc_outputs) := (*tax_fcst_calc_outputs)->next;
      (*tax_fcst_calc_outputs)->next := NULL;
      (*tax_fcst_calc_outputs_rows)++; */
      --
      --
      -- LOOP over time to calculate the tax depreciation forecast
      --

      L_DONE := false;

      while L_DONE = false
      loop

         --* UPPER is important for good match

         L_ORIG_NET := UPPER(L_ORIG_NET);
         L_NET      := L_ORIG_NET;

         --* which depreciation rates should we use for this tax year, if not on remaining life
         --* plan.

         I := L_TAX_YEAR - L_VINTAGE + 1;

         L_SCOOP := false; /* Test to see if in last depreciation year */

         if I != 1 then
            if I >= A_TAX_RATES_ROWS and L_ACCUM_RESERVE != 0 then
               L_SCOOP := true;
            end if;
         else
            if L_TAX_RATES(I) = 0 and L_ACCUM_RESERVE != 0 then
               L_SCOOP := true;
            end if;
         end if;

         -- If a life rate, then are only 2 rates, the first year and
         -- all subsequent years. There is NEVER a scoop.

         if L_METHOD = 6 then

            if (I > 1) then
               I := 2;
            end if;
            L_SCOOP := false;
         end if;

         --* If a vintage rate, match the tax_year with the designated year

         if L_METHOD = 7 then
            L_NUM := A_TAX_RATES_ROWS;
            for I in 1 .. L_NUM
            loop
               if L_TAX_YEAR = G_TAX_GRID_REC(A_TAX_GRID_INDEX).VINTAGE_YEAR then
                  exit;
               end if;
            end loop;
            if I > L_NUM then
               I := A_TAX_RATES_ROWS + 1;
            else
               --* IF this is the first year, then replace the first year rates.
               if L_TAX_YEAR = L_VINTAGE then
                  L_TAX_RATES(I) := A_TAX_RATES_SUB_REC(I).RATE1;
               end if;

               L_SCOOP := false;
               if I = A_TAX_RATES_ROWS and L_ACCUM_RESERVE != 0 then
                  L_SCOOP := true;
               end if;
            end if;
         end if;

         if L_RLIFE != 1 then
            if I > A_TAX_RATES_ROWS or I < 1 then
               L_DEPR_RATE := 0;
            else
               L_DEPR_RATE := L_TAX_RATES(I);
               if L_NET = 'GRNET' then
                  if I < L_SWITCHED then
                     L_NET := 'GROSS';
                  else
                     L_NET := 'NET';
                  end if;
               end if;
               if L_NET = 'NETGR' then
                  if I = L_SWITCHED then
                     L_RESERVE_AT_SWITCH := L_ACCUM_RESERVE;
                  end if;
                  if I < L_SWITCHED then
                     L_NET := 'NET';
                  else
                     L_NET := 'GROSS';
                  end if;
               end if;

            end if; --* else rates
         end if; --* rlife

         -- Calculate the depreciation rate to use this tax year, if on remaining life
         -- plan.

         if L_RLIFE = 1 then
            L_NET := 'NET';
            if I < 1 then
               L_DEPR_RATE := 0;
            else
               if (L_TAX_BALANCE = 0) then
                  L_REM_LIFE := 0;
               else
                  L_REM_LIFE := L_LIFE * (L_SL_RESERVE / L_TAX_BALANCE);
               end if;
               if L_REM_LIFE > L_LIFE then
                  L_REM_LIFE := L_LIFE;
               end if;
               if L_REM_LIFE < 1 then
                  L_REM_LIFE := 1;
               end if;
               -- sl case is easy

               if L_METHOD != 2 then
                  -- sl case
                  if L_REM_LIFE != 0 then
                     -- div by 0 protect
                     L_DEPR_RATE := 1 / L_REM_LIFE;
                  else
                     L_DEPR_RATE := 0;
                  end if;
                  -- SYD case requires SYD calc

               else
                  -- syd case
                  L_WLIFE := ROUND(L_REM_LIFE); -- whole number part
                  L_FLIFE := L_REM_LIFE - L_WLIFE; -- fractional part

                  if (L_WLIFE = 0 and L_FLIFE = 0) or L_WLIFE = -1 then
                     -- div by 0 protect
                     L_DEPR_RATE := 0;
                  else
                     L_DEPR_RATE := L_REM_LIFE /
                                    (((L_WLIFE + 1) * L_WLIFE / 2) + ((L_WLIFE + 1) * L_FLIFE));
                  end if;

               end if; -- syd case

            end if;

            if ABS(L_DEPR_RATE) > 1 then
               L_DEPR_RATE := 1;
            end if;

            L_DEPR_RATE := ROUND(L_DEPR_RATE, 5);

            if L_DEPR_RATE != 0 then
               L_SCOOP := false;
            end if;
         end if; --* rlife

         --
         -- Establish Tax Limitations (e.g. Luxury Auto if applicable)
         --

         L_TAX_LIMITATION := -1;

         if A_TAX_LIMITATION_ROWS != 0 then
            -- dw_tax_limitation.setsort(year a')
            -- dw_tax_limitation.sort()

            I := L_TAX_YEAR - L_VINTAGE + 1;

            if I > A_TAX_LIMITATION_ROWS or I < 1 then
               L_TAX_LIMITATION := -1;
            else
               L_TAX_LIMITATION := A_TAX_LIMITATION_SUB_REC(I).LIMITATION;
            end if;
         end if;

         L_OVER_ADJ_DEPRECIATION := 0;
         L_BOOK_RETIREMENT       := 0;
         L_ACTUAL_SALVAGE        := 0;
         L_BOOK_ADDITION         := 0;
         L_GAIN_LOSS             := 0;
         L_SALVAGE_EXTRAORD      := 0;
         L_RETIRE_RES_IMPACT     := 0;
         L_EX_RETIRE_RES_IMPACT  := 0;

         -- Determine Current Year Activity:

         if L_NUM_INPUTS = 0 then
            goto OUT1;

            for K in 1 .. L_NUM_INPUTS
            loop
               if A_TAX_FCST_CALC_INPUTS(K).TAX_YEAR = L_TAX_YEAR then
                  L_BOOK_RETIREMENT := A_TAX_FCST_CALC_INPUTS(K).BOOK_RETIREMENTS;
                  L_ACTUAL_SALVAGE  := A_TAX_FCST_CALC_INPUTS(K).ACTUAL_SALVAGE;
                  L_BOOK_ADDITION   := A_TAX_FCST_CALC_INPUTS(K).BOOK_ADDITIONS;
                  -- Handle in the retrieve function for C version
                  --if isnull(book_retirement) then book_retirement = 0
                  --if isnull(book_addition) then book_addition = 0
                  --if isnull(actual_salvage) then actual_salvage = 0

                  goto OUT1; -- get one hit and were out of here.
               end if;
            end loop;
         end if;

         <<OUT1>>

         --
         -- Determine Tax Basis Additions. Assume that Book Additions = Tax Additions
         --

         L_ADDITIONS := L_BOOK_ADDITION;

         -- Process the Book Retirements by Ratioing Through the Tax to Book Differences,
         -- and Determine Tax Basis Retirement.

         L_RATIO := (L_BOOK_BALANCE + L_BOOK_ADDITION);

         if L_RATIO != 0 then
            L_RATIO := (L_BOOK_RETIREMENT) / (L_BOOK_BALANCE + L_BOOK_ADDITION);
         else
            if ROUND(L_TAX_BALANCE, 2) != 0 then
               L_RATIO           := L_BOOK_RETIREMENT / L_TAX_BALANCE;
               L_BOOK_RETIREMENT := 0;
            else
               L_RATIO := 0;
            end if;
         end if;

         L_RETIREMENTS := L_RATIO * (L_TAX_BALANCE + L_ADDITIONS);

         L_BOOK_BALANCE_END := L_BOOK_BALANCE + L_BOOK_ADDITION - L_BOOK_RETIREMENT;

         -- Set The Retirement Depreciation Convention

         if L_TAX_YEAR = L_VINTAGE and L_RETIRE_DEPR_RATIO != 1 then
            L_RETIRE_DEPR_RATIO := 0;
         else
            L_RETIRE_DEPR_RATIO := L_SV_RETIRE_DEPR_RATIO;
         end if;

         -- If an ordinary or extraordinary retirement finishes off the tax balance, and
         -- this is an ADR-type convention for retirements, then scoop

         if ((L_RETIRE_BAL_CONV = 3 or L_RETIRE_BAL_CONV = 4 or L_ACCUM_ORDINARY_RETIRES != 0) and
            ROUND(L_TAX_BALANCE - L_RETIREMENTS - L_EXTRAORDINARY_RETIRES -
                   L_ACCUM_ORDINARY_RETIRES,
                   0) <= 0 and L_TAX_BALANCE > 0) or
            ((L_RETIRE_BAL_CONV = 3 or L_RETIRE_BAL_CONV = 4 or L_ACCUM_ORDINARY_RETIRES != 0) and
            ROUND(L_TAX_BALANCE - L_RETIREMENTS - L_EXTRAORDINARY_RETIRES -
                   L_ACCUM_ORDINARY_RETIRES,
                   0) >= 0 and L_TAX_BALANCE < 0) then

            L_SCOOP := true;
         end if;

         -- Set The Retirement balance Convention

         case
            when ROUND(L_RETIRE_RES_CONV) = 1 then
               L_RETIRE_RES_RATIO := 0;
            when ROUND(L_RETIRE_RES_CONV) = 2 or ROUND(L_RETIRE_RES_CONV) = 4 then
               if (L_TAX_BALANCE + L_ADDITIONS) != 0 then
                  L_RETIRE_RES_RATIO := (L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO)) /
                                        (L_TAX_BALANCE + L_ADDITIONS);
               else
                  L_RETIRE_RES_RATIO := 0;

               end if;
            when ROUND(L_RETIRE_RES_CONV) = 3 then
               L_RETIRE_RES_RATIO := 1;
         end case;

         -- Set The Salvage Convention

         -- Set The Retirement Depreciation Convention
         -- First year retirements have
         -- 0 depr.

         if L_TAX_YEAR = L_VINTAGE and L_EX_RETIRE_DEPR_RATIO != 1 then
            L_EX_RETIRE_DEPR_RATIO := 0;
         else
            L_EX_RETIRE_DEPR_RATIO := L_SV_EX_RETIRE_DEPR_RATIO;
         end if;

         -- Set The Retirement balance Convention

         -- Set The Retirement Reserve Convention

         case
            when ROUND(L_EX_RETIRE_RES_CONV) = 1 then
               L_EX_RETIRE_RES_RATIO := 0;
            when ROUND(L_EX_RETIRE_RES_CONV) = 2 or ROUND(L_EX_RETIRE_RES_CONV) = 4 then
               if (L_TAX_BALANCE + L_ADDITIONS) != 0 then
                  L_EX_RETIRE_RES_RATIO := (L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO)) /
                                           (L_TAX_BALANCE + L_ADDITIONS);
               else
                  L_EX_RETIRE_RES_RATIO := 0;
               end if;
            when ROUND(L_EX_RETIRE_RES_CONV) = 3 then
               L_EX_RETIRE_RES_RATIO := 1;

         end case;

         -- If constant dollar-amount estimated salvage, then calculate an estimated salvage percent

         if (L_EST_SALVAGE_CONV = 5 or L_EST_SALVAGE_CONV = 6) and L_ESTIMATED_SALVAGE != 0 then

            if (L_TAX_BALANCE + L_ADDITIONS + L_ADJUSTMENTS - L_RETIREMENTS -
               L_EXTRAORDINARY_RETIRES - L_ACCUM_ORDINARY_RETIRES != 0) then
               L_EST_SALVAGE_PCT := (L_ESTIMATED_SALVAGE -
                                    (L_ACCUM_SALVAGE + L_ACTUAL_SALVAGE + L_SALVAGE_EXTRAORD)) /
                                    (L_TAX_BALANCE + L_ADDITIONS + L_ADJUSTMENTS - L_RETIREMENTS -
                                    L_EXTRAORDINARY_RETIRES - L_ACCUM_ORDINARY_RETIRES);
            else
               L_EST_SALVAGE_PCT := 0;
               if (L_EST_SALVAGE_PCT * L_ESTIMATED_SALVAGE) <= 0 then
                  L_EST_SALVAGE_PCT := 0;
               end if;
            end if;
         end if;

         L_BEGIN_RES_IMPACT := L_RETIREMENTS * (L_RETIRE_RES_RATIO);

         L_EX_BEGIN_RES_IMPACT := L_EXTRAORDINARY_RETIRES * (L_EX_RETIRE_RES_RATIO);

         if L_NET = 'GROSS' then
            L_RETIREMENT_DEPR    := L_RETIREMENTS * L_RETIRE_DEPR_RATIO * L_DEPR_RATE;
            L_EX_RETIREMENT_DEPR := L_EXTRAORDINARY_RETIRES * L_EX_RETIRE_DEPR_RATIO * L_DEPR_RATE;
         else
            L_RETIREMENT_DEPR    := (L_RETIREMENTS - L_BEGIN_RES_IMPACT) * L_RETIRE_DEPR_RATIO *
                                    L_DEPR_RATE;
            L_EX_RETIREMENT_DEPR := (L_EXTRAORDINARY_RETIRES - L_EX_BEGIN_RES_IMPACT) *
                                    L_EX_RETIRE_DEPR_RATIO * L_DEPR_RATE;
         end if;

         --retire_res_impact = begin_res_impact + ex_begin_res_impact;
         --
         --if ( (accum_reserve - (accum_salvage * salvage_ratio) ) == 0)  -- div by 0
         --   reserve_at_switch = 0;
         --else
         --   reserve_at_switch = (reserve_at_switch
         --                  / (accum_reserve - (accum_salvage * salvage_ratio) ))
         --                  * retire_res_impact;
         --
         --
         --
         --if(retire_res_ratio != 0 )
         --   retire_res_impact = retire_res_impact + retirement_depr;
         --
         --
         --if(ex_retire_res_ratio != 0 )
         --   retire_res_impact = retire_res_impact + ex_retirement_depr;
         --
         --
         --
         --
         --depreciable_base = tax_balance
         --                     + additions
         --                     - retirements
         --                     - extraordinary_retires
         --                     + (retirements * retire_depr_ratio)
         --                     + (extraordinary_retires * ex_retire_depr_ratio)
         --                     -  reserve_at_switch;
         if L_RETIRE_RES_RATIO != 0 then
            if ABS(L_BEGIN_RES_IMPACT + L_RETIREMENT_DEPR) > ABS(L_RETIREMENTS) and
               L_RETIRE_RES_CONV != 4 then
               L_RETIRE_RES_IMPACT := L_RETIREMENTS;
            else
               L_RETIRE_RES_IMPACT := L_BEGIN_RES_IMPACT + L_RETIREMENT_DEPR;
            end if;
         end if;

         if L_EX_RETIRE_RES_RATIO != 0 then
            if ABS(L_EX_BEGIN_RES_IMPACT + L_EX_RETIREMENT_DEPR) > ABS(L_EXTRAORDINARY_RETIRES) and
               L_EX_RETIRE_RES_CONV != 4 then
               L_EX_RETIRE_RES_IMPACT := L_EXTRAORDINARY_RETIRES;
            else
               L_EX_RETIRE_RES_IMPACT := L_EX_BEGIN_RES_IMPACT + L_EX_RETIREMENT_DEPR;
            end if;
         end if;

         L_RETIRE_RES_IMPACT := L_RETIRE_RES_IMPACT + L_EX_RETIRE_RES_IMPACT;

         L_DEPRECIABLE_BASE := L_TAX_BALANCE
                              --  + adjustments * book_balance_adjust_method
                               + L_ADDITIONS - L_RETIREMENTS - L_EXTRAORDINARY_RETIRES +
                               (L_RETIREMENTS * L_RETIRE_DEPR_RATIO) +
                               (L_EXTRAORDINARY_RETIRES * L_EX_RETIRE_DEPR_RATIO);
         -- + (depreciable_base_adjust * depreciable_base_adjust_method);

         if L_TAX_BALANCE != 0 then
            L_DEPRECIABLE_BASE := L_DEPRECIABLE_BASE -
                                  (L_RESERVE_AT_SWITCH * L_DEPRECIABLE_BASE / L_TAX_BALANCE);
         end if;

         if (L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO)) = 0.0 then
            L_RESERVE_AT_SWITCH_END := 0;
         else
            L_RESERVE_AT_SWITCH_END := L_RESERVE_AT_SWITCH -
                                       ((L_RESERVE_AT_SWITCH /
                                       (L_ACCUM_RESERVE - (L_ACCUM_SALVAGE * L_SALVAGE_RATIO))) *
                                       (L_BEGIN_RES_IMPACT + L_EX_BEGIN_RES_IMPACT));
         end if;

         -- Estimated Salvage Reduces Depreciable Base Election

         if L_EST_SALVAGE_CONV = 2 or L_EST_SALVAGE_CONV = 6 then
            L_DEPRECIABLE_BASE := L_DEPRECIABLE_BASE * (1 - L_EST_SALVAGE_PCT);
         end if;

         -- Calculate the salvage impact on the beginning reserve

         L_SALVAGE_RES_IMPACT := +L_ACTUAL_SALVAGE * L_SALVAGE_RATIO * L_BEG_SALVAGE_RATIO +
                                 L_SALVAGE_EXTRAORD * L_EX_SALVAGE_RATIO * L_EX_BEG_SALVAGE_RATIO;

         if L_NET = 'NET' then
            L_RESERVE := L_ACCUM_RESERVE + L_SALVAGE_RES_IMPACT -
                         (L_BEGIN_RES_IMPACT * (1 - L_RETIRE_DEPR_RATIO)) -
                         (L_EX_BEGIN_RES_IMPACT * (1 - L_EX_RETIRE_DEPR_RATIO));
         end if;

         -- Re-Calculate the salvage impact on the ending reserve

         L_SALVAGE_RES_IMPACT := +L_ACTUAL_SALVAGE * L_SALVAGE_RATIO +
                                 L_SALVAGE_EXTRAORD * L_EX_SALVAGE_RATIO;

         if (L_RESERVE * L_DEPRECIABLE_BASE >= 0 and ABS(L_RESERVE) >= ABS(L_DEPRECIABLE_BASE)) then
            L_DEPRECIABLE_BASE := 0;
         else
            if L_NET = 'NET' then
               L_DEPRECIABLE_BASE := L_DEPRECIABLE_BASE - L_RESERVE +
                                     (L_ACCUM_SALVAGE + L_SALVAGE_RES_IMPACT) *
                                     (L_DEPR_SALVAGE_RATIO * L_SALVAGE_RATIO);
            end if;
         end if;
         if L_FIXED_DEPRECIABLE_BASE != 0 then
            L_DEPRECIABLE_BASE := L_FIXED_DEPRECIABLE_BASE;
         end if;

         L_CALC_DEPRECIATION := L_DEPRECIABLE_BASE * L_DEPR_RATE;

         --
         --  Check tax limitation e.g. luxury autos
         --
         if L_TAX_LIMITATION != -1 and L_QUANTITY > 0 then
            --if( (quantity * tax_limitation) < calc_depreciation)
            L_CALC_DEPRECIATION := L_QUANTITY * L_TAX_LIMITATION;
         end if;

         if L_ALLOCATION = true then
            L_CALC_DEPRECIATION := L_INPUT_DEPRECIATION;
         end if;

         --
         -- Determine Ending Tax Basis Balance and Other Balances
         --

         L_TAX_BALANCE_END := L_TAX_BALANCE + L_ADDITIONS + L_ADJUSTMENTS -
                              (L_RETIREMENTS * L_RETIRE_BAL_RATIO) -
                              (L_EXTRAORDINARY_RETIRES * L_EX_RETIRE_BAL_RATIO);

         L_ACCUM_ORDIN_RETIRES_END := L_ACCUM_ORDINARY_RETIRES +
                                      (L_RETIREMENTS * (1 - L_RETIRE_BAL_RATIO)) +
                                      (L_EXTRAORDINARY_RETIRES * (1 - L_EX_RETIRE_BAL_RATIO));

         L_ACCUM_RESERVE_END := L_ACCUM_RESERVE + L_CALC_DEPRECIATION - L_RETIRE_RES_IMPACT +
                                L_SALVAGE_RES_IMPACT;

         L_TAX_BALANCE_END         := ROUND(L_TAX_BALANCE_END, 2);
         L_ACCUM_ORDIN_RETIRES_END := ROUND(L_ACCUM_ORDIN_RETIRES_END, 2);
         L_ACCUM_RESERVE_END       := ROUND(L_ACCUM_RESERVE_END, 2);

         if L_ACCUM_RESERVE_END * (L_TAX_BALANCE_END - (L_TAX_BALANCE_END - (L_ACCUM_ORDIN_RETIRES_END *
            L_ADR_SALVAGE_RATIO)) * L_EST_SALVAGE_PCT) >= 0 and
            ABS(L_ACCUM_RESERVE_END) >=
            ABS(L_TAX_BALANCE_END -
                (L_TAX_BALANCE_END - (L_ACCUM_ORDIN_RETIRES_END * L_ADR_SALVAGE_RATIO)) *
                L_EST_SALVAGE_PCT) then

            L_OVER_ADJ_DEPRECIATION := L_ACCUM_RESERVE_END - (L_TAX_BALANCE_END - (L_TAX_BALANCE_END -
                                       (L_ACCUM_ORDIN_RETIRES_END *
                                       L_ADR_SALVAGE_RATIO)) *
                                       L_EST_SALVAGE_PCT);

            if ABS(L_OVER_ADJ_DEPRECIATION) > ABS(L_CALC_DEPRECIATION) then
               L_OVER_ADJ_DEPRECIATION := L_CALC_DEPRECIATION;
            end if;
            if L_EST_SALVAGE_PCT >= 0 then
               if ABS(L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION) > ABS(L_TAX_BALANCE_END) then
                  L_GAIN_LOSS := L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION - L_TAX_BALANCE_END;
                  if L_GAIN_LOSS > L_SALVAGE_RES_IMPACT then
                     L_OVER_ADJ_DEPRECIATION := L_OVER_ADJ_DEPRECIATION -
                                                (L_SALVAGE_RES_IMPACT - L_GAIN_LOSS);
                     L_GAIN_LOSS             := L_SALVAGE_RES_IMPACT;
                     L_SALVAGE_RES_IMPACT    := 0;
                  else
                     L_SALVAGE_RES_IMPACT := L_SALVAGE_RES_IMPACT - L_GAIN_LOSS;
                  end if;
               end if;
            else
               --* est_salvage_pct < 0
               if ABS(L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION) >
                  ABS(L_TAX_BALANCE_END * (1 - L_EST_SALVAGE_PCT)) then

                  L_GAIN_LOSS := L_ACCUM_RESERVE_END - L_OVER_ADJ_DEPRECIATION -
                                 (L_TAX_BALANCE_END * (1 - L_EST_SALVAGE_PCT));
                  if L_GAIN_LOSS > L_SALVAGE_RES_IMPACT then
                     L_OVER_ADJ_DEPRECIATION := L_OVER_ADJ_DEPRECIATION -
                                                (L_SALVAGE_RES_IMPACT - L_GAIN_LOSS);
                     L_GAIN_LOSS             := L_SALVAGE_RES_IMPACT;
                     L_SALVAGE_RES_IMPACT    := 0;
                  else
                     L_SALVAGE_RES_IMPACT := L_SALVAGE_RES_IMPACT - L_GAIN_LOSS;
                  end if;

               end if;
            end if; --* est_salvage_pct >= 0

            L_ACCUM_RESERVE_END := L_ACCUM_RESERVE_END - L_GAIN_LOSS - L_OVER_ADJ_DEPRECIATION;
            L_SCOOP             := false;
         end if;

         if (L_SCOOP) then
            -- scoop rest of depreciation in the last year of depreciation
            L_AMOUNT := (L_TAX_BALANCE_END -
                        (L_TAX_BALANCE_END - (L_ACCUM_ORDIN_RETIRES_END * L_ADR_SALVAGE_RATIO)) *
                        L_EST_SALVAGE_PCT) - L_ACCUM_RESERVE_END;

            L_ACCUM_RESERVE_END := L_ACCUM_RESERVE_END + L_AMOUNT;

            L_OVER_ADJ_DEPRECIATION := L_OVER_ADJ_DEPRECIATION - L_AMOUNT;

         end if; -- scoop

         if L_RETIRE_BAL_CONV = 4 then
            if ABS(L_TAX_BALANCE_END - L_ACCUM_RESERVE_END) < .1 and
               ABS(L_TAX_BALANCE_END - L_ACCUM_ORDIN_RETIRES_END) < .1 then
               L_RETIRE_RES_IMPACT       := L_RETIRE_RES_IMPACT + L_ACCUM_RESERVE_END;
               L_TAX_BALANCE_END         := 0;
               L_ACCUM_RESERVE_END       := 0;
               L_ACCUM_ORDIN_RETIRES_END := 0;
            end if;
         end if;
         L_DEPRECIATION := L_CALC_DEPRECIATION - L_OVER_ADJ_DEPRECIATION;

         L_ACCUM_SALVAGE_END := (L_ACCUM_SALVAGE * L_SALVAGE_RATIO) + L_SALVAGE_RES_IMPACT;
         if L_RLIFE = 1 and L_LIFE > 0 then
            L_SL_RESERVE_END := L_SL_RESERVE + ((L_TAX_BALANCE + L_TAX_BALANCE_END) / (2 * L_LIFE));
         else
            L_SL_RESERVE_END := 0;
         end if;

         --if( tax_balance + additions != 0)  -- div by 0 check
         --   {
         if L_GAIN_LOSS_CONV = 2 then
            --On Asset
            L_GAIN_LOSS := L_GAIN_LOSS + L_ACTUAL_SALVAGE -
                           (L_RETIREMENTS - (L_RETIRE_RES_IMPACT - L_EX_RETIRE_RES_IMPACT));
         end if;
         --     11-20-98 line above replaces the two below
         --           ((accum_reserve - (accum_salvage * salvage_ratio) ) / (tax_balance + additions)
         --            * retirements + retirement_depr));
         --

         if (L_EX_GAIN_LOSS_CONV = 2) then
            --On Asset
            L_GAIN_LOSS := L_GAIN_LOSS + L_SALVAGE_EXTRAORD -
                           (L_EXTRAORDINARY_RETIRES - (L_EX_RETIRE_RES_IMPACT));
         end if;
         --     11-20-98 line above replaces the two below
         --              ((accum_reserve - (accum_salvage * salvage_ratio) ) / (tax_balance + additions)
         --             * extraordinary_retires + ex_retirement_depr));

         -- }     -- div by 0 check

         if L_GAIN_LOSS_CONV = 3 then
            -- Salvage = Gain
            L_GAIN_LOSS := L_GAIN_LOSS + L_ACTUAL_SALVAGE;
         end if;

         if (L_EX_GAIN_LOSS_CONV = 3) then
            -- Salvage = Gain
            L_GAIN_LOSS := L_GAIN_LOSS + L_SALVAGE_EXTRAORD;
         end if;

         -- Save the balances

         L_BOOK_BALANCE           := L_BOOK_BALANCE_END;
         L_TAX_BALANCE            := L_TAX_BALANCE_END;
         L_ACCUM_RESERVE          := L_ACCUM_RESERVE_END;
         L_SL_RESERVE             := L_SL_RESERVE_END;
         L_ACCUM_ORDINARY_RETIRES := L_ACCUM_ORDIN_RETIRES_END;
         L_ACCUM_SALVAGE          := L_ACCUM_SALVAGE_END;
         L_FIXED_DEPRECIABLE_BASE := L_FIXED_DEPRECIABLE_BASE;
         L_RESERVE_AT_SWITCH      := L_RESERVE_AT_SWITCH_END;

         -- Get Everything Back Into The DataWindows

         --dw_tax_fcst_calc_outputs.insertrow(0)
         --nn = dw_tax_fcst_calc_outputs.rowcount()
         --(*tax_fcst_calc_outputs)->next := (struct s_tax_fcst_calc_outputs FAR *)
         --                   GetMem(sizeof( struct s_tax_fcst_calc_outputs));
         A_TAX_FCST_CALC_OUTPUTS.EXTEND;
         L_ROW := A_TAX_FCST_CALC_OUTPUTS.COUNT;

         L_TAX_BALANCE_END   := ROUND(L_TAX_BALANCE_END, 2);
         L_ACCUM_RESERVE_END := ROUND(L_ACCUM_RESERVE_END, 2);
         L_ADDITIONS         := ROUND(L_ADDITIONS, 2);
         L_RETIREMENTS       := ROUND(L_RETIREMENTS, 2);
         L_DEPRECIATION      := ROUND(L_DEPRECIATION, 2);
         L_GAIN_LOSS         := ROUND(L_GAIN_LOSS, 2);

         A_TAX_FCST_CALC_OUTPUTS(L_ROW).TAX_BALANCE := L_TAX_BALANCE_END;
         A_TAX_FCST_CALC_OUTPUTS(L_ROW).ACCUM_RESERVE := L_ACCUM_RESERVE_END;
         A_TAX_FCST_CALC_OUTPUTS(L_ROW).TAX_ADDITIONS := L_ADDITIONS;
         A_TAX_FCST_CALC_OUTPUTS(L_ROW).TAX_RETIREMENTS := L_RETIREMENTS;

         A_TAX_FCST_CALC_OUTPUTS(L_ROW).DEPRECIATION := L_DEPRECIATION;
         A_TAX_FCST_CALC_OUTPUTS(L_ROW).GAIN_LOSS := L_GAIN_LOSS;

         A_TAX_FCST_CALC_OUTPUTS(L_ROW).TAX_RECORD_ID := L_TAX_RECORD_ID;
         A_TAX_FCST_CALC_OUTPUTS(L_ROW).TAX_BOOK_ID := L_TAX_BOOK_ID;
         A_TAX_FCST_CALC_OUTPUTS(L_ROW).TAX_YEAR := L_TAX_YEAR;
         A_TAX_FCST_CALC_OUTPUTS(L_ROW).TAX_FORECAST_VERSION_ID := A_VERSION;

         A_TAX_FCST_CALC_OUTPUTS_ROWS := A_TAX_FCST_CALC_OUTPUTS_ROWS + 1;

         L_TAX_YEAR := L_TAX_YEAR + 1;

         if L_SCOOP = true and L_EST_SALVAGE_PCT = 0 then
            if L_NUM_INPUTS = 0 then
               L_DONE := true;
            else
               if (A_TAX_FCST_CALC_INPUTS(L_NUM_INPUTS).TAX_YEAR < L_TAX_YEAR) then
                  L_DONE := true;
               end if;
            end if;
         end if;

         if A_END_YR < L_TAX_YEAR then
            L_DONE := true;
         end if;

      -- end of time loop
      end loop;
      --* end of do while

      --* --g_perform.put("time","bot")

      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'FCST Calc Record ID=' ||
                   TO_CHAR(G_TAX_GRID_REC(A_TAX_GRID_INDEX).TAX_RECORD_ID) || '  ' ||
                   sqlerrm(L_CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;

   end CALC;

   -- =============================================================================
   --  Function DEF_CALC
   -- =============================================================================
   function DEF_CALC(A_VERSION                    pls_integer,
                     A_DEF_TAX_UTIL_INDEX         pls_integer,
                     A_DEF_TAX_UTIL_SUB_REC       in out nocopy TYPE_DEF_TAX_UTIL_REC,
                     A_DEF_TAX_RATES_SUB_REC      TYPE_DFIT_RATES_REC,
                     A_DEF_TAX_RATES_ROWS         pls_integer,
                     A_TAX_FCST_FROM              TAX_FCST_CALC_OUTPUT_TYPE,
                     A_TAX_FCST_FROM_ROWS         pls_integer,
                     A_TAX_FCST_TO                TAX_FCST_CALC_OUTPUT_TYPE,
                     A_TAX_FCST_TO_ROWS           pls_integer,
                     A_TAX_DFIT_FCST_OUTPUTS      in out nocopy DFIT_FORECAST_OUTPUT_TYPE,
                     A_TAX_DFIT_FCST_OUTPUTS_ROWS in out nocopy pls_integer,
                     A_TAX_RATES_SUB_REC          TYPE_TAX_RATES_REC,
                     A_TAX_RATES_ROWS             pls_integer,
                     A_END_YR                     pls_integer) return pls_integer is

      L_TAX_YEAR     BINARY_DOUBLE;
      I              pls_integer;
      L_VINTAGE_YEAR pls_integer;
      L_RATE_INDEX   pls_integer;
      L_NYEARS       pls_integer;
      J              pls_integer;
      K              pls_integer;
      L_CODE         pls_integer;
      type DEF_TAX_RATES_TYPE is table of BINARY_DOUBLE index by binary_integer;
      L_DEF_TAX_RATES DEF_TAX_RATES_TYPE;

      L_DIFF_CURRENT       BINARY_DOUBLE;
      L_REM_RATE           BINARY_DOUBLE;
      L_TAX_BALANCE_FROM   BINARY_DOUBLE;
      L_ACCUM_RESERVE_FROM BINARY_DOUBLE;

      L_DONE         boolean;
      L_ALLOCATION   boolean;
      L_AMORTIZATION boolean;

      type INT_ARRAY_TYPE is table of pls_integer index by binary_integer;
      L_DEF_TAX_DATES INT_ARRAY_TYPE;

      L_TAX_DATE pls_integer;

      L_DFIT_RATE_ORDER INT_ARRAY_TYPE;
      L_COUNT           pls_integer;
      L_DEF_TAX_DATE    BINARY_DOUBLE;

      L_DEF_INCOME_TAX_BALANCE_BEG BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_BALANCE_END BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_BALANCE_ADJ BINARY_DOUBLE := 0;
      L_NORM_DIFF_BALANCE_BEG      BINARY_DOUBLE := 0;
      L_NORM_DIFF_BALANCE_END      BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_PROVISION   BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_REVERSAL    BINARY_DOUBLE := 0;
      L_ARAM_RATE                  BINARY_DOUBLE := 0;
      L_ARAM_RATE_END              BINARY_DOUBLE := 0;
      L_LIFE                       BINARY_DOUBLE := 0;
      L_DEPRECIATION               BINARY_DOUBLE := 0;
      L_DEPRECIATION_2             BINARY_DOUBLE := 0;
      L_GAIN_LOSS                  BINARY_DOUBLE := 0;
      L_GAIN_LOSS_2                BINARY_DOUBLE := 0;
      L_RETIREMENTS                BINARY_DOUBLE := 0;
      L_REM_LIFE                   BINARY_DOUBLE := 0;
      L_AMORT_AMOUNT               BINARY_DOUBLE := 0;

      L_SCOOP          boolean := false;
      L_MORE_FROM_DEPR boolean := false;

      L_MSG varchar2(200);
      --
      --   Move the items in the deferred tax table to variables
      --   Get rid of the gets we don't need
      --
      L_TAX_RECORD pls_integer;
      L_NUM        BINARY_DOUBLE;

   begin

      L_TAX_RECORD                 := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).TAX_RECORD_ID;
      L_TAX_YEAR                   := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).TAX_YEAR;
      L_DEF_INCOME_TAX_BALANCE_BEG := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).DEF_INCOME_TAX_BALANCE_END;
      L_NORM_DIFF_BALANCE_BEG      := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORM_DIFF_BALANCE_END;
      L_ARAM_RATE                  := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).ARAM_RATE_END;
      L_LIFE                       := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).LIFE;
      L_VINTAGE_YEAR               := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).YEAR;

      --Basis difference deferred tax set-up

      if (A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).AMORTIZATION_TYPE_ID <= 0) then
         L_AMORTIZATION := true;
      else
         L_AMORTIZATION := false;
      end if;

      if (A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).BOOK_DEPR_ALLOC_IND >= 1) then
         L_ALLOCATION := true;
      else
         L_ALLOCATION := false;
      end if;

      if (L_AMORTIZATION = true and L_ALLOCATION = true) then

         if (A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORM_DIFF_BALANCE_BEG != 0) then
            L_AMORT_AMOUNT := - (A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).INPUT_AMORTIZATION + A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).BASIS_DIFF_ADD_RET);
         else
            L_AMORT_AMOUNT := - (A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).INPUT_AMORTIZATION * 2);
         end if;
      end if;

      -- PSEG CONVERSION EQUATIONs:

      --decimal gain_loss_dfit_bal
      --
      --
      --if isnull(def_income_tax_adjust) then def_income_tax_adjust := 0
      --
      --gain_loss_dfit_bal             := def_tax->gain_loss_def_Tax_balance')
      --
      --if isnull(gain_loss_dfit_bal) then gain_loss_dfit_bal := 0
      --
      --def_income_tax_balance_beg     := def_income_tax_balance_beg + gain_loss_dfit_bal
      --
      --norm_diff_balance_beg          := def_tax->pseg_beg_norm')

      --
      --  We may need Deferred Tax Rates
      --  Get the most recent effective deferred tax rate
      --  The datawindow comes in ordered by ascending effective date
      --

      --dw_def_tax_rates.setsort('effective_date a')
      --dw_def_tax_rates.sort()
      for I in 1 .. A_DEF_TAX_RATES_ROWS
      loop
         L_COUNT := 1;
         for J in 1 .. A_DEF_TAX_RATES_ROWS
         loop
            if A_DEF_TAX_RATES_SUB_REC(I).EFFECTIVE_DATE > A_DEF_TAX_RATES_SUB_REC(J).EFFECTIVE_DATE then
               L_COUNT := L_COUNT + 1;
            elsif A_DEF_TAX_RATES_SUB_REC(I).EFFECTIVE_DATE = A_DEF_TAX_RATES_SUB_REC(J).EFFECTIVE_DATE and I > J then
               L_COUNT := L_COUNT + 1;
            end if;
         end loop;
         L_DFIT_RATE_ORDER(L_COUNT) := I;
      end loop;

      for I in 1 .. A_DEF_TAX_RATES_ROWS
      loop
         J := L_DFIT_RATE_ORDER(I);
         L_DEF_TAX_RATES(I) := A_DEF_TAX_RATES_SUB_REC(J).ANNUAL_RATE;
         L_DEF_TAX_DATES(I) := A_DEF_TAX_RATES_SUB_REC(J).EFFECTIVE_DATE;
      end loop;

      --for(i = 1;i <= def_tax_rates_rows;++i)
      --    {
      --    def_tax_rates[i - 1] = def_tax_rates_s[i - 1]->annual_rate;
      --    def_tax_dates[i - 1] = def_tax_rates_s[i - 1]->effective_date;
      --    }

      --
      --  LOOP over TIME
      --
      --

      -- PUT out an initial record
      /*
      dw_dfit_fcst_output.insertrow(0)
      i = dw_dfit_fcst_output.rowcount()

      dfit_fcst_output.setitem(i,'tax_record_id',def_tax->tax_record_id'))
      dw_dfit_fcst_output.setitem(i,'normalization_id',def_tax->normalization_id'))
      dw_dfit_fcst_output.setitem(i,'tax_year', tax_year)
      dw_dfit_fcst_output.setitem(i,'tax_FORECAST_VERSION_ID', G_VERSION_FCST_ID)
      dw_dfit_fcst_output.setitem(i,'def_income_tax_balance', def_income_tax_balance_beg)
      dw_dfit_fcst_output.setitem(i,'norm_diff_balance',      norm_diff_balance_beg)
      dw_dfit_fcst_output.setitem(i,'aram_rate',              aram_rate)
      dw_dfit_fcst_output.setitem(i,'from_depreciation',      0)
      dw_dfit_fcst_output.setitem(i,'to_depreciation',       0)
      */

      A_TAX_DFIT_FCST_OUTPUTS.EXTEND;
      L_COUNT := A_TAX_DFIT_FCST_OUTPUTS.COUNT;

      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TAX_RECORD_ID := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).TAX_RECORD_ID;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).NORMALIZATION_ID := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TAX_YEAR := L_TAX_YEAR;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TAX_FORECAST_VERSION_ID := A_VERSION;

      if (ABS(L_DEF_INCOME_TAX_BALANCE_BEG) < .0000001) then
         L_DEF_INCOME_TAX_BALANCE_BEG := 0;
      end if;

      if (ABS(L_DEF_INCOME_TAX_BALANCE_BEG) > 1000000000000000) then
         WRITE_LOG(G_JOB_NO,
                   4,
                   0,
                   'Is too Large def_income_tax_balance_beg Record=' || TO_CHAR(L_TAX_RECORD) ||
                   ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                   TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
      end if;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_BALANCE := L_DEF_INCOME_TAX_BALANCE_BEG;

      if (ABS(L_NORM_DIFF_BALANCE_BEG) < .0000001) then
         L_NORM_DIFF_BALANCE_BEG := 0;
      end if;
      if (ABS(L_NORM_DIFF_BALANCE_BEG) > 1000000000000000) then
         WRITE_LOG(G_JOB_NO,
                   4,
                   0,
                   'Is too Large norm_diff_balance_beg Record=' || TO_CHAR(L_TAX_RECORD) ||
                   ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                   TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
      end if;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).NORM_DIFF_BALANCE := L_NORM_DIFF_BALANCE_BEG;

      if (ABS(L_ARAM_RATE) < .0000001) then
         L_ARAM_RATE := 0;
      end if;
      if (ABS(L_ARAM_RATE) > 1000000000000000) then
         WRITE_LOG(G_JOB_NO,
                   4,
                   0,
                   'Is too  Large aram_rate Record=' || TO_CHAR(L_TAX_RECORD) || ' Year=' ||
                   TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                   TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
      end if;

      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).ARAM_RATE := L_ARAM_RATE;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_PROVISION := 0;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).LIFE := 0;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_REVERSAL := 0;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_GAIN_LOSS := 0;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).FROM_DEPRECIATION := 0;
      A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TO_DEPRECIATION := 0;

      A_TAX_DFIT_FCST_OUTPUTS_ROWS := A_TAX_DFIT_FCST_OUTPUTS_ROWS + 1;

      -- Start the loop

      K      := 1; -- DO NOT WANT THE BEGINNING BALANCE YEAR
      L_DONE := false;

      while L_DONE = false
      loop
         K := K + 1;

         if (L_AMORTIZATION != true) then
            if (K <= A_TAX_FCST_FROM_ROWS) then
               L_DEPRECIATION       := A_TAX_FCST_FROM(K).DEPRECIATION;
               L_GAIN_LOSS          := A_TAX_FCST_FROM(K).GAIN_LOSS;
               L_RETIREMENTS        := A_TAX_FCST_FROM(K).TAX_RETIREMENTS;
               L_TAX_BALANCE_FROM   := A_TAX_FCST_FROM(K).TAX_BALANCE;
               L_ACCUM_RESERVE_FROM := A_TAX_FCST_FROM(K).ACCUM_RESERVE;
            else
               L_DEPRECIATION       := 0;
               L_GAIN_LOSS          := 0;
               L_RETIREMENTS        := 0;
               L_TAX_BALANCE_FROM   := 0;
               L_ACCUM_RESERVE_FROM := 0;
            end if;

            if (K <= A_TAX_FCST_TO_ROWS) then
               L_DEPRECIATION_2 := A_TAX_FCST_FROM(K).DEPRECIATION;
               L_GAIN_LOSS_2    := A_TAX_FCST_FROM(K).GAIN_LOSS;
            else
               L_DEPRECIATION_2 := 0;
               L_GAIN_LOSS_2    := 0;
            end if;
         else
            -- if amortization
            --if(k  <= tax_fcst_to_rows)
            -- {
            -- retirements := tax_fcst_from[k - 1]->tax_retirements;
            -- tax_balance_from := tax_fcst_from[k - 1]->tax_balance;
            -- }
            --else
            -- {
            -- retirements := 0;
            -- tax_balance_from := 0;
            -- }
            if (L_ALLOCATION = false) then
               L_REM_LIFE := L_VINTAGE_YEAR + L_LIFE - L_TAX_YEAR + .5;
               if (L_REM_LIFE < 1) then
                  L_REM_LIFE := 1;
               end if;
               L_DEPRECIATION_2 := L_NORM_DIFF_BALANCE_BEG * 1 / L_REM_LIFE;
            else
               L_DEPRECIATION_2 := L_AMORT_AMOUNT;
            end if;
         end if;

         L_DEF_INCOME_TAX_PROVISION   := 0;
         L_DEF_INCOME_TAX_REVERSAL    := 0;
         L_NORM_DIFF_BALANCE_END      := 0;
         L_DEF_INCOME_TAX_BALANCE_END := 0;
         L_DIFF_CURRENT               := 0;
         L_ARAM_RATE_END              := 0;

         L_TAX_DATE := (ROUND(L_TAX_YEAR, 0) * 10000) + 100 + 1;
         --tax_date := (tax_year * 10000) + 100 + 1;

         L_RATE_INDEX := 0;

         for I in 1 .. A_DEF_TAX_RATES_ROWS
         loop
            if (L_TAX_DATE >= L_DEF_TAX_DATES(I)) then
               L_RATE_INDEX := I;
            end if;
         end loop;

         if (L_RATE_INDEX = 0) then
            WRITE_LOG(5, 1, 0, 'Warning No Good Def Tax Rates');
            L_RATE_INDEX := 1;
         end if;

         --
         --
         --
         --
         --

         -- the 'ending aram rate' is the one we use this year

         L_ARAM_RATE_END := 0; --this is a div by 0 check

         if (L_NORM_DIFF_BALANCE_BEG != 0) then
            L_ARAM_RATE_END := L_DEF_INCOME_TAX_BALANCE_BEG / L_NORM_DIFF_BALANCE_BEG;
         else
            L_ARAM_RATE_END := L_DEF_TAX_RATES(L_RATE_INDEX);
         end if;

         if (L_NORM_DIFF_BALANCE_BEG = 0 and L_DEPRECIATION = 0) then
            L_DEPRECIATION_2 := 0;
            L_DONE           := true;
            goto HAVE_CALCED;
         end if;

         L_DIFF_CURRENT := (L_DEPRECIATION - L_GAIN_LOSS) - (L_DEPRECIATION_2 - L_GAIN_LOSS_2);

         -- if both the current difference and the balance have the same sign
         -- then deferred taxes are building-up

         if ((L_DIFF_CURRENT * L_NORM_DIFF_BALANCE_BEG) >= 0) then
            L_DEF_INCOME_TAX_PROVISION := L_DIFF_CURRENT * L_DEF_TAX_RATES(L_RATE_INDEX);
            goto HAVE_CALCED;
         end if;

         -- We are in a reversal, because the current difference and the balance
         -- do not have the same sign

         -- if if there is no zero-check necessity, then the calc is straightforward.

         if (ABS(L_DIFF_CURRENT) <= ABS(L_NORM_DIFF_BALANCE_BEG)) then
            L_DEF_INCOME_TAX_REVERSAL := L_DIFF_CURRENT * L_ARAM_RATE_END;

            goto HAVE_CALCED;
         end if;

         -- Need to do a zero check because the current turnaround difference is greater than the
         -- accumulated difference.
         --

         L_DEF_INCOME_TAX_REVERSAL := (-L_NORM_DIFF_BALANCE_BEG) * L_ARAM_RATE_END;

         if (L_AMORTIZATION = true) then
            L_DIFF_CURRENT := (-L_NORM_DIFF_BALANCE_BEG);
            goto HAVE_CALCED;
         end if;

         -- Will need to know if there is more 'from' difference coming in the future to do
         -- the following check for finished with def tax, or just another cross-over point
         -- Determine if there is more depreciation out there

         L_MORE_FROM_DEPR := false;

         L_NYEARS := L_TAX_YEAR - L_VINTAGE_YEAR + 2;

         if (L_NYEARS <= A_TAX_RATES_ROWS) then
            L_REM_RATE := 0;

            -- these are sorted in asc year order
            for J in L_NYEARS .. A_TAX_RATES_ROWS
            loop
               L_REM_RATE := L_REM_RATE + A_TAX_RATES_SUB_REC(J).RATE;
            end loop;

            if (L_REM_RATE != 0 and ROUND(L_TAX_BALANCE_FROM, 2) != 0) then
               L_MORE_FROM_DEPR := true;
            end if;

         end if; -- nyears <= ...

         -- Now, if there are more future 'from' tax rates then this is
         -- a temporary cross-over point. Apply the remainder of the current difference as a build-up
         -- at the statutory rate

         if (L_MORE_FROM_DEPR) then
            L_DEF_INCOME_TAX_PROVISION := (L_DIFF_CURRENT + L_NORM_DIFF_BALANCE_BEG) *
                                          L_DEF_TAX_RATES(L_RATE_INDEX);
            goto HAVE_CALCED;
         else
            L_DIFF_CURRENT := (-L_NORM_DIFF_BALANCE_BEG);
            L_DONE         := true;
            goto HAVE_CALCED;
         end if;

         <<HAVE_CALCED>>

         L_DEF_INCOME_TAX_BALANCE_END := L_DEF_INCOME_TAX_BALANCE_BEG + L_DEF_INCOME_TAX_PROVISION +
                                         L_DEF_INCOME_TAX_REVERSAL;

         L_NORM_DIFF_BALANCE_END := L_NORM_DIFF_BALANCE_BEG + L_DIFF_CURRENT;

         L_DEF_INCOME_TAX_BALANCE_END := ROUND(L_DEF_INCOME_TAX_BALANCE_END, 1);
         L_NORM_DIFF_BALANCE_END      := ROUND(L_NORM_DIFF_BALANCE_END, 1);

         -- save the balances

         L_DEF_INCOME_TAX_BALANCE_BEG := L_DEF_INCOME_TAX_BALANCE_END;
         L_NORM_DIFF_BALANCE_BEG      := L_NORM_DIFF_BALANCE_END;

         --
         -- Make a record, Put Everything back into the datawindows
         --

         --dw_dfit_fcst_output.insertrow(0)
         --i := tax_dfit_fcst_outputs_rows;

         --tax_year := tax_year + 1;
         /*
         dw_dfit_fcst_output.setitem(i,'tax_record_id',def_tax->tax_record_id'))
         dw_dfit_fcst_output.setitem(i,'normalization_id',def_tax->normalization_id'))
         dw_dfit_fcst_output.setitem(i,'tax_year', tax_year)
         dw_dfit_fcst_output.setitem(i,'tax_FORECAST_VERSION_ID', G_VERSION_FCST_ID)
         dw_dfit_fcst_output.setitem(i,'def_income_tax_balance',  def_income_tax_balance_end)
         dw_dfit_fcst_output.setitem(i,'norm_diff_balance',       norm_diff_balance_end)
         dw_dfit_fcst_output.setitem(i,'def_income_tax_provision',def_income_tax_provision)
         dw_dfit_fcst_output.setitem(i,'def_income_tax_reversal', def_income_tax_reversal)
         dw_dfit_fcst_output.setitem(i,'aram_rate',               aram_rate_end)
         dw_dfit_fcst_output.setitem(i,'from_depreciation', depreciation + Gain_loss )
         dw_dfit_fcst_output.setitem(i,'to_depreciation',   depreciation_2 + gain_loss_2)
         */

         L_TAX_YEAR := L_TAX_YEAR + 1;
         A_TAX_DFIT_FCST_OUTPUTS.EXTEND;
         L_COUNT := A_TAX_DFIT_FCST_OUTPUTS.COUNT;

         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TAX_RECORD_ID := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).TAX_RECORD_ID;
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).NORMALIZATION_ID := A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID;
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TAX_YEAR := L_TAX_YEAR;
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TAX_FORECAST_VERSION_ID := A_VERSION;

         /*if(fabs(def_income_tax_balance_end) < .0000001)
         norm_diff_balance_end := 0; */

         if (ABS(L_DEF_INCOME_TAX_BALANCE_END) < .0000001) then
            L_DEF_INCOME_TAX_BALANCE_END := 0;
         end if;

         if (ABS(L_DEF_INCOME_TAX_BALANCE_END) > 1000000000000000) then
            WRITE_LOG(G_JOB_NO,
                      4,
                      0,
                      'Is too  Large def_income_tax_balance_end Record=' || TO_CHAR(L_TAX_RECORD) ||
                      ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                      TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
         end if;
         L_DEF_INCOME_TAX_BALANCE_END := ROUND(L_DEF_INCOME_TAX_BALANCE_END, 2);
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_BALANCE := L_DEF_INCOME_TAX_BALANCE_END;

         if (ABS(L_NORM_DIFF_BALANCE_END) < .0000001) then
            L_NORM_DIFF_BALANCE_END := 0;
         end if;

         if (ABS(L_NORM_DIFF_BALANCE_END) > 1000000000000000) then
            WRITE_LOG(G_JOB_NO,
                      4,
                      0,
                      'Is too  Large norm_diff_balance_end Record=' || TO_CHAR(L_TAX_RECORD) ||
                      ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                      TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
         end if;
         L_NORM_DIFF_BALANCE_END := ROUND(L_NORM_DIFF_BALANCE_END, 2);
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).NORM_DIFF_BALANCE := L_NORM_DIFF_BALANCE_END;

         if (ABS(L_DEF_INCOME_TAX_PROVISION) < .0000001) then
            L_DEF_INCOME_TAX_PROVISION := 0;
         end if;

         if (ABS(L_DEF_INCOME_TAX_PROVISION) > 1000000000000000) then
            WRITE_LOG(G_JOB_NO,
                      4,
                      0,
                      'Is too  Large def_income_tax_provision Record=' || TO_CHAR(L_TAX_RECORD) ||
                      ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                      TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
         end if;
         L_DEF_INCOME_TAX_PROVISION := ROUND(L_DEF_INCOME_TAX_PROVISION, 2);
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_PROVISION := L_DEF_INCOME_TAX_PROVISION;

         if (ABS(L_DEF_INCOME_TAX_REVERSAL) < .0000001) then
            L_DEF_INCOME_TAX_REVERSAL := 0;
         end if;
         if (ABS(L_DEF_INCOME_TAX_REVERSAL) > 1000000000000000) then
            WRITE_LOG(G_JOB_NO,
                      4,
                      0,
                      'Is too  Large def_income_tax_reversal Record=' || TO_CHAR(L_TAX_RECORD) ||
                      ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                      TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
         end if;
         L_DEF_INCOME_TAX_REVERSAL := ROUND(L_DEF_INCOME_TAX_REVERSAL, 2);
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_REVERSAL := L_DEF_INCOME_TAX_REVERSAL;

         if (ABS(L_ARAM_RATE_END) < .0000001) then
            L_ARAM_RATE_END := 0;
         end if;
         if (ABS(L_ARAM_RATE_END) > 1000000000000000) then
            WRITE_LOG(G_JOB_NO,
                      4,
                      0,
                      'Is too  Large aram_rate Record=' || TO_CHAR(L_TAX_RECORD) || ' Year=' ||
                      TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                      TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
         end if;
         L_ARAM_RATE_END := ROUND(L_ARAM_RATE_END, 8);
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).ARAM_RATE := L_ARAM_RATE_END;

         L_NUM := L_DEPRECIATION - L_GAIN_LOSS;
         if (ABS(L_NUM) < .0000001) then
            L_NUM := 0;
         end if;
         if (ABS(L_NUM) > 1000000000000000) then
            WRITE_LOG(G_JOB_NO,
                      4,
                      0,
                      'Is too  Largedepreciation - gain_loss  Record=' || TO_CHAR(L_TAX_RECORD) ||
                      ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                      TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
         end if;
         L_NUM := ROUND(L_NUM, 2);
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).FROM_DEPRECIATION := L_NUM;

         L_NUM := L_DEPRECIATION_2 - L_GAIN_LOSS_2;
         if (ABS(L_NUM) < .0000001) then
            L_NUM := 0;
         end if;
         if (ABS(L_NUM) > 1000000000000000) then
            WRITE_LOG(G_JOB_NO,
                      4,
                      0,
                      'Is too  Large depreciation_2 - gain_loss_2 Record=' || TO_CHAR(L_TAX_RECORD) ||
                      ' Year=' || TO_CHAR(L_TAX_YEAR) || ' norm id = ' ||
                      TO_CHAR(A_DEF_TAX_UTIL_SUB_REC(A_DEF_TAX_UTIL_INDEX).NORMALIZATION_ID));
         end if;
         L_NUM := ROUND(L_NUM, 2);

         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).TO_DEPRECIATION := L_NUM;
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).LIFE := 0;
         A_TAX_DFIT_FCST_OUTPUTS(L_COUNT).DEF_INCOME_TAX_GAIN_LOSS := 0;

         A_TAX_DFIT_FCST_OUTPUTS_ROWS := A_TAX_DFIT_FCST_OUTPUTS_ROWS + 1;

         if (A_END_YR < L_TAX_YEAR) then
            L_DONE := true;
         end if;

      end loop; -- End of Loop

      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Deferred Calc Record ID=' || TO_CHAR(L_TAX_RECORD) || '  ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return -1;

   end DEF_CALC;

end TAX_FCST_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (410, 0, 10, 4, 1, 0, 29581, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029581_pwrtax_TAX_FCST_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
