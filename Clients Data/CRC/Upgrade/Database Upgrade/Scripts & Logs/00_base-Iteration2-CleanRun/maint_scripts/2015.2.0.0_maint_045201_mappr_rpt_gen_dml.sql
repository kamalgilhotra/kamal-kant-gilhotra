/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045201_mappr_rpt_gen_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 11/12/2015 Andrew Scott     mobile approvals report generation ssp
||============================================================================
*/

insert into pp_processes
(process_id, description, long_description, executable_file, version, allow_concurrent)
select max_id + 1, 'MA Report Gen', 'Mobile Approvals Report Generation', 'ssp_report_gen.exe', '2015.2.0.0', 1
from 
( select max(process_id) max_id from pp_processes );

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
select process_id, pbd_name, '2015.2.0.0'
FROM pp_processes,
(
select 'ssp_report_gen_custom.pbd' pbd_name from dual union
select 'ppprojct_custom.pbd' from dual union
select 'ppsystem_interface_custom.pbd' from dual
)
WHERE lower(trim(executable_file)) = 'ssp_report_gen.exe';

INSERT INTO pp_job_executable
(job_type, job_name, executable, category, arguments, optional_file_path)
SELECT 
'SSP', 'MA Report Gen', 'ssp_report_gen.exe', 'CR/System Processes',
TO_CLOB('{"parms": [{"type": "string","value": "","label": "Datawindow Name","required": "true"},{"type": "long","value": "","label": "Work Order ID","required": "true"},{"type": "long","value": "","label": "Revision","required": "false"}]}'),
null
FROM DUAL;

insert into PP_VERSION_EXES
(PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
select '2015.2.0.0', 'ssp_report_gen.exe', '2015.2.0.0' from dual;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2975, 0, 2015, 2, 0, 0, 45201, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045201_mappr_rpt_gen_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
   SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
