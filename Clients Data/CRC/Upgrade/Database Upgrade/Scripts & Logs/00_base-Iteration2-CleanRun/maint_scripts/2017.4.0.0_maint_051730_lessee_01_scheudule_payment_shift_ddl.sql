/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051730_lessee_01_scheudule_payment_shift_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.4.0.0 07/05/2018 Sarah Byers      New field to support negative payment shift
||============================================================================
*/

alter table ls_ilr_options add (schedule_payment_shift number(22,0) null);

comment on column ls_ilr_options.schedule_payment_shift is 'Indicates the number of months to delay a payment within the schedule calculation.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7802, 0, 2017, 4, 0, 0, 51730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051730_lessee_01_scheudule_payment_shift_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;