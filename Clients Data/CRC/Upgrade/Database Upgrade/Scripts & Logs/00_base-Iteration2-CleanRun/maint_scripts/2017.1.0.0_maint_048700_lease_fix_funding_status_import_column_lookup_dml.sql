/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048700_lease_fix_funding_status_import_column_lookup_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ------------------------------------
|| 2017.1.0.0 08/21/2017 Johnny Sisouphanh Add funding status column to import column lookup
||============================================================================
*/

UPDATE pp_import_column
   SET parent_table = NULL
 WHERE import_type_id IN
       (SELECT import_type_id
          FROM pp_import_type
         WHERE description = 'Add: ILR''s ')
   AND column_name = 'funding_status_id';

--Funding Status Description
INSERT INTO pp_import_column_lookup
  (import_type_id, column_name, import_lookup_id)
  SELECT 252, 'funding_status_id', import_lookup_id
    FROM pp_import_lookup
   WHERE upper(description) = 'ILR FUNDING STATUS.DESCRIPTION';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3669, 0, 2017, 1, 0, 0, 48700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048700_lease_fix_funding_status_import_column_lookup_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;