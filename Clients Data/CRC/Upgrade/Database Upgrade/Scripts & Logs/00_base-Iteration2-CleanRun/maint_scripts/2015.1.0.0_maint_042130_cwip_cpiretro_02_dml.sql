/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_042130_cwip_cpiretro_02_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   1/29/2015   Sunjin Cone     Add new option for Idle Check for CPI Retro Calc
||============================================================================
*/ 

insert into POWERPLANT_COLUMNS
(COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, DROPDOWN_NAME, COLUMN_RANK)
select 'cpi_retro_idle', TABLE_NAME, 'p', 'CPI Retro Idle', DROPDOWN_NAME, 25
from POWERPLANT_COLUMNS
where TABLE_NAME = 'afudc_control'
and COLUMN_NAME = 'allow_neg_cpi';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2234, 0, 2015, 1, 0, 0, 42130, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042130_cwip_cpiretro_02_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;