/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053008_lessee_01_sys_control_description_updates_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.2 02/14/2019 Sarah Byers    Clean up system control description values
||============================================================================
*/
 
update pp_system_control
   set description = 'dw_yes_no;1'
 where lower(trim(control_name)) in ('lease fast payment approvals', 'lease catch up principal', 
                                     'hide lease department in cpr panel', 'lease debug execute immediate',
                                     'disable lease validation', 'hide lease department in cpr panel',
                                     'lease catch up principal', 'lease fast payment approvals',
                                     'purge unviewed batch reports');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15082, 0, 2018, 1, 0, 2, 53008, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053008_lessee_01_sys_control_description_updates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
