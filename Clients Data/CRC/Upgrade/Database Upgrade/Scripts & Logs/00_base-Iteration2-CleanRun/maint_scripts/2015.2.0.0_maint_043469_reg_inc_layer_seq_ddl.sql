/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043469_reg_inc_layer_seq_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/05/2015 Sarah Byers    Sequence for populating INCREMENTAL_HIST_LAYER.HIST_LAYER_ID
||============================================================================
*/

create sequence inc_layer_seq;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2577, 0, 2015, 2, 0, 0, 043469, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043469_reg_inc_layer_seq_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;