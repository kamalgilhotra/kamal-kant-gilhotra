/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010928_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/18/2012 Julia Breuer   Point Release
||============================================================================
*/

update PWRPLANT.PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select woc.work_order_id from work_order_control woc where upper( trim( <importfield> ) ) = upper( trim( woc.work_order_number ) ) and funding_wo_indicator = 0 )'
 where IMPORT_LOOKUP_ID = 110;

update PWRPLANT.PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select woc.work_order_id from work_order_control woc where upper( trim( <importfield> ) ) = upper( trim( woc.work_order_number ) ) and <importtable>.company_id = woc.company_id and funding_wo_indicator = 0 )'
 where IMPORT_LOOKUP_ID = 111;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (225, 0, 10, 4, 1, 0, 10928, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010928_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
