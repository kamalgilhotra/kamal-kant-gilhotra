 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046188_lease_fix_wo_validations_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0  08/18/2016 Will Davis      Lease WO validations did not allow for entry by revision
 ||============================================================================
 */ 

update wo_validation_type 
set col2 = 'revision'
where description in ('Leased MLAs', 'Leased ILRs');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3264, 0, 2016, 1, 0, 0, 046188, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046188_lease_fix_wo_validations_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;