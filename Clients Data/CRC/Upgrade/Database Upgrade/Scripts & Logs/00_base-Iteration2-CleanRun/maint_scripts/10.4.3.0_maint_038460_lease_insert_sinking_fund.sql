/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_038460_lease_insert_sinking_fund.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 07/18/2014 Daniel Motter       Creation
||========================================================================================
*/

/* Insert row for sinking fund leases into LS_LEASE_TYPE */
insert into LS_LEASE_TYPE
   (LEASE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION)
   (select 3, 'Sinking Fund', 'Sinking Fund'
      from DUAL
     where not exists (select * from LS_LEASE_TYPE where LEASE_TYPE_ID = 3));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1277, 0, 10, 4, 3, 0, 38460, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038460_lease_insert_sinking_fund.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
