/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037023_lease_mec_auto_ret.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 03/11/2014 Kyle Peterson
||============================================================================
*/

alter table LS_MEC_CONFIG add PROCESS_4 number(1,0) default 1;

alter table LS_MEC_CONFIG
   add constraint FK_LS_MEC_CONFIG5
       foreign key (PROCESS_4)
       references YES_NO(YES_NO_ID);

comment on column LS_MEC_CONFIG.PROCESS_1 is 'Controls whether or not Depreciation shows up in Month End Close. 1 is yes, 0 is no.';
comment on column LS_MEC_CONFIG.PROCESS_2 is 'Controls whether or not Auto Retirements show up in Month End Close. 1 is yes, 0 is no.';
comment on column LS_MEC_CONFIG.PROCESS_3 is 'Controls whether or not Accruals show up in Month End Close. 1 is yes, 0 is no.';
comment on column LS_MEC_CONFIG.PROCESS_4 is 'Controls whether or not Payments show up in Month End Close. 1 is yes, 0 is no.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1022, 0, 10, 4, 2, 0, 37023, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037023_lease_mec_auto_ret.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;