/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010522_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/05/2012 Chris Mardis   Point Release
||============================================================================
*/

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'bs-only'
 where LOWER(trim(CONTROL_NAME)) = LOWER('FPINT-Validate WO TYPE/Bud Item Co')
   and LOWER(DESCRIPTION) like '%dw_yes_no%'
   and LOWER(trim(CONTROL_VALUE)) = 'no';

update PP_SYSTEM_CONTROL_COMPANY
   set DESCRIPTION = 'yes;bs-only;no',
       LONG_DESCRIPTION = '"YES" = the funding project initiation window will validate that the work order type company and the budget item company are the same; "BS-ONLY" = will validate only that the company/bus segment relationship is valid; "NO" = the validation will be skipped'
 where LOWER(trim(CONTROL_NAME)) = LOWER('FPINT-Validate WO TYPE/Bud Item Co');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (169, 0, 10, 3, 5, 0, 10522, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010522_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
