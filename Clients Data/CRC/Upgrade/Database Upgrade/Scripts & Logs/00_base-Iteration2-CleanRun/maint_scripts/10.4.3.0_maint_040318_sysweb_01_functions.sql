/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_sysweb_01_functions.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/24/2014 Chad Theilman    Web Framework - Functions
||============================================================================
*/

CREATE OR REPLACE FUNCTION PWRPLANT.SYS_DOES_OBJECT_EXIST(objectName  VARCHAR2,
                                                          objectOwner VARCHAR2)
  RETURN NUMBER IS
  object_exists NUMBER := 0;
BEGIN
  --This Does Not Confirm Object Type.  Just that the name exists.
  SELECT COUNT(*)
    INTO object_exists
    FROM ALL_OBJECTS
   WHERE OBJECT_NAME = upper(objectName)
     AND OWNER = upper(objectOwner);

  RETURN object_exists;
  --Valid Objects are; CLUSTER, CONSUMER GROUP, CONTEXT, DESTINATION, DIRECTORY, EDITION, EVALUATION CONTEXT, FUNCTION, INDEX, INDEX PARTITION, INDEXTYPE, JAVA CLASS, JAVA DATA, JAVA RESOURCE, JAVA SOURCE, JOB, JOB CLASS, LIBRARY, LOB, MATERIALIZED VIEW, OPERATOR, PACKAGE, PACKAGE BODY, PROCEDURE, PROGRAM, RULE, RULE SET, SCHEDULE, SCHEDULER GROUP, SEQUENCE, SYNONYM, TABLE, TABLE PARTITION, TRIGGER, TYPE, TYPE BODY, VIEW, WINDOW, XML SCHEMA
END;
/

CREATE OR REPLACE FUNCTION PWRPLANT.SYS_DOES_TABLE_EXIST(tableName  VARCHAR2,
                                                         tableOwner VARCHAR2)
  RETURN NUMBER IS
  table_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
    INTO table_exists
    FROM ALL_TABLES
   WHERE TABLE_NAME = upper(tableName)
     AND OWNER = upper(tableOwner);

  RETURN table_exists;
END;
/

CREATE OR REPLACE FUNCTION PWRPLANT.SYS_DOES_TABLE_COLUMN_EXIST(tableColumn VARCHAR2,
                                                                tableName   VARCHAR2,
                                                                tableOwner  VARCHAR2)
  RETURN NUMBER IS
  table_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
    INTO table_column_exists
    FROM ALL_TAB_COLUMNS
   WHERE COLUMN_NAME = upper(tableColumn)
     AND TABLE_NAME = upper(tableName)
     AND OWNER = upper(tableOwner);

  RETURN table_column_exists;
END;
/

CREATE OR REPLACE FUNCTION PWRPLANT.SYS_DOES_TABLE_COLUMN_EXIST_AS(columnDataType VARCHAR2,
                                                                   tableColumn    VARCHAR2,
                                                                   tableName      VARCHAR2,
                                                                   tableOwner     VARCHAR2)
  RETURN NUMBER IS
  table_column_exists NUMBER := 0;
BEGIN
  SELECT COUNT(*)
    INTO table_column_exists
    FROM ALL_TAB_COLUMNS
   WHERE DATA_TYPE = upper(columnDataType)
     AND COLUMN_NAME = upper(tableColumn)
     AND TABLE_NAME = upper(tableName)
     AND OWNER = upper(tableOwner);

  RETURN table_column_exists;
END;
/

create or replace Function PWRPLANT.SYS_DOES_INDEX_EXIST(indexName  varchar2,
                                                         indexOwner varchar2)
  RETURN NUMBER is
  index_exists number := 0;

begin

  select count(*)
    into index_exists
    from ALL_INDEXES
   where INDEX_NAME = upper(indexName)
     and owner = upper(indexOwner);

  return index_exists;

end;
/

create or replace Function PWRPLANT.SYS_DOES_SEQUENCE_EXIST(sequenceName  varchar2,
                                                            sequenceOwner varchar2)
  RETURN NUMBER is
  seq_count number := 0;

begin
	select count(*) into seq_count
	from all_objects where object_type = 'SEQUENCE' 
	and Owner = upper(sequenceOwner) 
	and Object_Name = upper(sequenceName);
	
	return seq_count;

end;
/

CREATE OR REPLACE Function SYS_DOES_USER_EXIST(aUserName varchar2)
  RETURN NUMBER is
  user_exists number := 0;

begin
  select count(*)
    into user_exists
    from ALL_USERS
   where UserName = upper(aUserName);

  return user_exists;

end;
/

create or replace Function PWRPLANT.SYS_DOES_ROLE_EXIST(roleName varchar2)
  RETURN NUMBER is
  role_exists number := 0;

begin
  select count(*)
    into role_exists
    from DBA_ROLES
   where Role = upper(roleName);

  return role_exists;

end;
/

create or replace Function PWRPLANT.SYS_DOES_VIEW_EXIST(viewName  varchar2,
                                                        viewOwner varchar2)
  RETURN NUMBER is
  view_exists number := 0;

begin
  select count(*)
    into view_exists
    from ALL_VIEWS
   where VIEW_NAME = upper(viewName)
     and OWNER = upper(viewOwner);

  return view_exists;
end;
/

create or replace Function PWRPLANT.SYS_DOES_FUNCTION_EXIST(funcName varchar2)
  RETURN NUMBER is
  proc_exists number := 0;

begin
  select count(*)
    into proc_exists
    from ALL_PROCEDURES
   where Object_Name = upper(funcName);

  return proc_exists;

end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1566, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_sysweb_01_functions.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;