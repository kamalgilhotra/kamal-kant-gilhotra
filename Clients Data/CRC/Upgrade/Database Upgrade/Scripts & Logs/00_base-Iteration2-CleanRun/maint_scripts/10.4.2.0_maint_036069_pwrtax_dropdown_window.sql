SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036069_pwrtax_dropdown_window.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/10/2014 Andrew Scott        maint-36069  MLP drop down window.
||============================================================================
*/

------These columns were added to the transfer table in script for maint 35534.  We also need to make sure that
------they exist on the archive table.

begin
   execute immediate 'alter table ARC_TAX_TRANSFER_CONTROL add DEFERRED_TAX_SCHEMA_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_TRANSFER_CONTROL already has DEFERRED_TAX_SCHEMA_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table ARC_TAX_TRANSFER_CONTROL add NOTES varchar2(2000)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_TRANSFER_CONTROL already has NOTES or table alter not granted.');
end;
/

begin
   execute immediate 'alter table ARC_TAX_TRANSFER_CONTROL add DEF_GAIN_VINTAGE_ID number(22, 0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_TRANSFER_CONTROL already has DEF_GAIN_VINTAGE_ID or table alter not granted.');
end;
/

comment on column ARC_TAX_TRANSFER_CONTROL.DEFERRED_TAX_SCHEMA_ID is 'deferred tax schema the activity is being transferred to, in the system.';
comment on column ARC_TAX_TRANSFER_CONTROL.NOTES is 'User input note describing the nature of the transfer.';
comment on column ARC_TAX_TRANSFER_CONTROL.DEF_GAIN_VINTAGE_ID is 'vintage used for the calculated deferred gain.';

----create a new sequence for the package_id.  Make re-runnable.
declare
   cur_max_ttc_id number;
   cur_max_tpc_id number;
   seq_start_no number;
begin

   -- grab the max package id on tax_transfer_control
   select max(package_id)
   into cur_max_ttc_id
   from tax_transfer_control;

   if cur_max_ttc_id is null or cur_max_ttc_id < 0 then
      cur_max_ttc_id := 0;
   end if;

   dbms_output.put_line('Max package_id on tax_transfer_control : '||cur_max_ttc_id);

   -- grab the max package id on tax_package_control
   select max(package_id)
   into cur_max_tpc_id
   from tax_package_control;

   if cur_max_tpc_id is null or cur_max_tpc_id < 0 then
      cur_max_tpc_id := 0;
   end if;

   dbms_output.put_line('Max package_id on tax_package_control : '||cur_max_tpc_id);

   ----which one is greater?  Use that number + 1 as the starting number for the new sequence
   if cur_max_ttc_id > cur_max_tpc_id then
      seq_start_no := cur_max_ttc_id + 1;
   else
      seq_start_no := cur_max_tpc_id + 1;
   end if;

   dbms_output.put_line('New sequence to start with : '||seq_start_no);

   ---- drop the existing sequence if it exists
   begin
      execute immediate 'drop public synonym tax_package_seq';
      DBMS_OUTPUT.PUT_LINE('Synonym tax_package_seq dropped.');
   exception
      when others then
         null; --Ignore exception if public synonym can not be dropped.
   end;

   begin
      execute immediate 'drop sequence tax_package_seq';
      DBMS_OUTPUT.PUT_LINE('Sequence tax_package_seq dropped.');
   exception
      when others then
         null; --Ignore exception if sequence can not be dropped.
   end;

   ---- create the new sequence
   execute immediate 'create sequence tax_package_seq start with '||seq_start_no;
   DBMS_OUTPUT.PUT_LINE('Sequence tax_package_seq created, starting with '||seq_start_no);

end;
/


----new columns needed for :
----tax_transfer_control
----tax_temp_transfer_control
----arc_tax_tax_transfer_control

----tax_layer_id
----asset_id
----in_service_month

begin
   execute immediate 'alter table TAX_TRANSFER_CONTROL add TAX_LAYER_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TRANSFER_CONTROL already has TAX_LAYER_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table TAX_TRANSFER_CONTROL add ASSET_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TRANSFER_CONTROL already has ASSET_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table TAX_TRANSFER_CONTROL add IN_SERVICE_MONTH date';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TRANSFER_CONTROL already has IN_SERVICE_MONTH or table alter not granted.');
end;
/

begin
   execute immediate 'alter table TAX_TEMP_TRANSFER_CONTROL add TAX_LAYER_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TEMP_TRANSFER_CONTROL already has TAX_LAYER_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table TAX_TEMP_TRANSFER_CONTROL add ASSET_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TEMP_TRANSFER_CONTROL already has ASSET_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table TAX_TEMP_TRANSFER_CONTROL add IN_SERVICE_MONTH date';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table TAX_TEMP_TRANSFER_CONTROL already has IN_SERVICE_MONTH or table alter not granted.');
end;
/

begin
   execute immediate 'alter table ARC_TAX_TRANSFER_CONTROL add TAX_LAYER_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_TRANSFER_CONTROL already has TAX_LAYER_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table ARC_TAX_TRANSFER_CONTROL add ASSET_ID number(22,0)';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_TRANSFER_CONTROL already has ASSET_ID or table alter not granted.');
end;
/

begin
   execute immediate 'alter table ARC_TAX_TRANSFER_CONTROL add IN_SERVICE_MONTH date';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_TRANSFER_CONTROL already has IN_SERVICE_MONTH or table alter not granted.');
end;
/

comment on column TAX_TRANSFER_CONTROL.TAX_LAYER_ID is 'Tax layer selected for transfers.  Only available if the system is configured for individual asset depreciation.';
comment on column TAX_TRANSFER_CONTROL.ASSET_ID is 'Asset ID of the tax records used for transfer froms.  Only available if the system is configured for individual asset depreciation.';
comment on column TAX_TRANSFER_CONTROL.IN_SERVICE_MONTH is 'Optional In Service Month selected for transfers.  Only available if the system is configured for individual asset depreciation.';

comment on column TAX_TEMP_TRANSFER_CONTROL.TAX_LAYER_ID is 'Tax layer selected for transfers.  Only available if the system is configured for individual asset depreciation.';
comment on column TAX_TEMP_TRANSFER_CONTROL.ASSET_ID is 'Asset ID of the tax records used for transfer froms.  Only available if the system is configured for individual asset depreciation.';
comment on column TAX_TEMP_TRANSFER_CONTROL.IN_SERVICE_MONTH is 'Optional In Service Month selected for transfers.  Only available if the system is configured for individual asset depreciation.';

comment on column ARC_TAX_TRANSFER_CONTROL.TAX_LAYER_ID is 'Tax layer selected for transfers.  Only available if the system is configured for individual asset depreciation.';
comment on column ARC_TAX_TRANSFER_CONTROL.ASSET_ID is 'Asset ID of the tax records used for transfer froms.  Only available if the system is configured for individual asset depreciation.';
comment on column ARC_TAX_TRANSFER_CONTROL.IN_SERVICE_MONTH is 'Optional In Service Month selected for transfers.  Only available if the system is configured for individual asset depreciation.';


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (960, 0, 10, 4, 2, 0, 36069, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036069_pwrtax_dropdown_window.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;