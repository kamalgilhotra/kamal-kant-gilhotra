/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_006943_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/03/2011 Blake Andrews  Point Release
||============================================================================
*/

--maint-006943

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'pre_apportion_taxable_y'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 15
   and ROW_VALUE = 'pre_apportion_taxable_y_tot';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'state_tax_deductible'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 16
   and ROW_VALUE = 'state_tax_deductible_tot';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'taxable_y'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 17
   and ROW_VALUE = 'total_taxable_y';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'calc_stat_rate'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 18
   and ROW_VALUE = 'calc_stat_rate_tot';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'calc_current_tax'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 19
   and ROW_VALUE = 'calc_current_tax_tot';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'compute_28'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 20
   and ROW_VALUE = 'curr_month_trueup_tot';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'tax_before_credits'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 21
   and ROW_VALUE = 'total_tax_b4_credits';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'credits'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 22
   and ROW_VALUE = 'credits_tot';

update TAX_ACCRUAL_REP_CONS_ROWS
   set ROW_VALUE = 'current_tax'
 where REP_CONS_TYPE_ID = 2
   and ROW_ID = 23
   and ROW_VALUE = 'current_tax_tot';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (2, 0, 10, 3, 3, 0, 6943, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_006943_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
