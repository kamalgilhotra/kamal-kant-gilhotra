/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009179_prov_table_maint.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 12/18/2013 Ron Ferentini       Change approvals workspace and
||                                         menu to be "approvers".
||============================================================================
*/

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK,
    READ_ONLY)
   select 'm_type_treatment_id',
          'tax_accrual_m_type',
          sysdate,
          'PWRPLANT',
          'e',
          'Type Treatment(0,1,2,3,4,5)',
          9,
          0
     from DUAL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (820, 0, 10, 4, 2, 0, 9179, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_009179_prov_table_maint.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;