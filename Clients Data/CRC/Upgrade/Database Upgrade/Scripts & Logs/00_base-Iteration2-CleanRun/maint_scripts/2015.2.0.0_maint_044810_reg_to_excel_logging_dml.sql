/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044810_reg_to_excel_logging_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 09/10/2015 Sarah Byers    Logging for RMS Export
||============================================================================
*/

insert into pp_processes (
	process_id, description, long_description, executable_file, version, allow_concurrent, 
	system_lock_enabled, system_lock, async_email_on_complete)
select nvl(max(process_id),0) + 1, 'RMS to Excel', 'Export RMS Case Working Model to Excel', 'rms_to_excel.exe', null, 1,
		 0, 0, 0
  from pp_processes;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2858, 0, 2015, 2, 0, 0, 044810, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044810_reg_to_excel_logging_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;