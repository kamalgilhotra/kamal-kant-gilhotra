/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008734_repair_work_order_tables.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/13/2011 Zack Spratling Point Release
||============================================================================
*/

--###PATCH(8734)
alter table REPAIR_WORK_ORDER_SEGMENTS add TE_AGGREGATION_ID number(22,0);

alter table REPAIR_WORK_ORDER_SEGMENTS
   add constraint FK_TEAGGREGATE
       foreign key (TE_AGGREGATION_ID)
       REFERENCES TE_AGGREGATION(TE_AGGREGATION_ID);

alter table REPAIR_WO_SEG_REPORTING drop column BATCH_ID;

alter table REPAIR_WO_SEG_REPORTING add TE_AGGREGATION_ID number(22,0);

alter table REPAIR_WO_SEG_REPORTING
   add constraint FK_TEAGGREGATE1
       foreign key (TE_AGGREGATION_ID)
       references TE_AGGREGATION(TE_AGGREGATION_ID);

alter table REPAIR_WO_SEG_REPORTING add BATCH_ID number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (15, 0, 10, 3, 3, 0, 8734, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008734_repair_work_order_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
