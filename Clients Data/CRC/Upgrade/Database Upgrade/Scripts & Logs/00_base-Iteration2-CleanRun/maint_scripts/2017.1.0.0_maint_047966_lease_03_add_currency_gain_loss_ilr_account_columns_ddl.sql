/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047966_lease_03_add_currency_gain_loss_ilr_account_columns.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 07/17/2017 Andrew Hill    add process_5 column to month end close config table 
||                                          for currency gain/loss process
||============================================================================
*/

ALTER TABLE ls_ilr_account ADD(currency_gain_loss_dr_acct_id NUMBER(22,0),
                               currency_gain_loss_cr_acct_id NUMBER(22,0));

ALTER TABLE ls_ilr_account ADD(CONSTRAINT ls_ilr_account_fk16 FOREIGN KEY (currency_gain_loss_dr_acct_id) REFERENCES gl_account(gl_account_id));

ALTER TABLE ls_ilr_account ADD(CONSTRAINT ls_ilr_account_fk17 FOREIGN KEY (currency_gain_loss_cr_acct_id) REFERENCES gl_account(gl_account_id));

COMMENT ON COLUMN ls_ilr_account.currency_gain_loss_dr_acct_id IS 'The currency gain/loss debit account';

COMMENT ON COLUMN ls_ilr_account.currency_gain_loss_cr_acct_id IS 'The currency gain/loss credit account';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3583, 0, 2017, 1, 0, 0, 47966, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047966_lease_03_add_currency_gain_loss_ilr_account_columns_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
