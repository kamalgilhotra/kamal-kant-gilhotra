/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050386_lessor_01_add_accrual_jes_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/06/2018 Anand R        Add JE trans type for Lessor Accruals
||============================================================================
*/

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4010, 'Lessor Monthly Interest Accrual Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4011, 'Lessor Monthly Interest Accrual Credit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4012, 'Lessor Monthly Executory Accrual Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4013, 'Lessor Monthly Executory Accrual Credit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4014, 'Lessor Monthly Contingent Accrual Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4015, 'Lessor Monthly Contingent Accrual Credit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4016, 'Lessor Unguaranteed Residual Accretion Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4017, 'Lessor Unguaranteed Residual Accretion Credit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4056, 'Lessor Deferred Selling Profit Debit');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4057, 'Lessor Recognize Selling Profit Credit');

/* Add GL_JE_CODE for Lessor Accruals  */

insert into standard_journal_entries (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
values ( (select nvl((max(je_id) + 1),1) from standard_journal_entries), 'Lessor Accruals', 'Lessor Accruals', 'Lessor Accruals', 'Lessor Accruals');

insert into gl_je_control (PROCESS_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
values ('Lessor Accruals', (select max(je_id) from standard_journal_entries ), null, null, 'NONE', 'NONE', 'NONE', 'NONE', null, null, null);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4168, 0, 2017, 3, 0, 0, 50386, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050386_lessor_01_add_accrual_jes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

