
/*
 ||============================================================================
 || Application: acqaider_app
 || File Name: maint_043663_sys_acqa_02_triggers_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.1.0.0 03/20/2015 Sherri Ramson  <New Sequence and Triggers for stand alone acqaider app>
 ||============================================================================
 */

CREATE SEQUENCE pp_acqa_table_audit_seq
  MINVALUE 1
  INCREMENT BY 1
/


CREATE OR REPLACE TRIGGER trig_acqa_afudc_da_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON afudc_data FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
     IF (
          --(:old.afudc_type_id <> :new.afudc_type_id OR
          (:old.afudc_type_id is null AND :new.afudc_type_id is not null) --AKM20150107 OR
          --(:new.afudc_type_id is null AND :old.afudc_type_id is not null)
          --)
       AND (--(:old.effective_date <> :new.effective_date OR
          (:old.effective_date is null AND :new.effective_date is not null)-- AKM20150107 OR
          --(:new.effective_date is null AND :old.effective_date is not null)
          )
       )THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
         (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'afudc_data', 'afudc_type_id,effective_date', 'insert', :old.afudc_type_id||','||:old.effective_date,:new.afudc_type_id||','||:new.effective_date, USER, SYSDATE,  window, windowtitle, comments);

      END IF;



        IF  ((   (:old.afudc_type_id <> :new.afudc_type_id) OR
                (:new.afudc_type_id is null AND :old.afudc_type_id is not null))
          AND  ((:old.effective_date <> :new.effective_date) OR
              (:new.effective_date is null AND :old.effective_date is not null) )  )

        THEN

     select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
           (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'afudc_data', 'afudc_type_id,effective_date', 'delete', :old.afudc_type_id||','||:old.effective_date,:new.afudc_type_id||','||:new.effective_date, USER, SYSDATE,  window, windowtitle, comments);


         END IF;
   --AKM20150107 ugly fix
    UPDATE acqa_batch_audit_trail   z
      SET action='delete', new_value=',f'
      WHERE new_value = ','
      AND    z.batch_id=batch_id
      AND table_name = 'afudc_data'  ;

    --AKM20150107 end
  END IF;


  IF DELETING THEN
     IF (
          (:old.afudc_type_id <> :new.afudc_type_id OR
          (:old.afudc_type_id is null AND :new.afudc_type_id is not null) OR
          (:new.afudc_type_id is null AND :old.afudc_type_id is not null))
       AND(:old.effective_date <> :new.effective_date OR
          (:old.effective_date is null AND :new.effective_date is not null) OR
          (:new.effective_date is null AND :old.effective_date is not null))
       )THEN


    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
           (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'afudc_data', 'afudc_type_id,effective_date', 'insert', :old.afudc_type_id||','||:old.effective_date,:new.afudc_type_id||','||:new.effective_date, USER, SYSDATE,  window, windowtitle, comments);

      END IF;
       --AKM20150107 ugly fix

      DELETE FROM acqa_batch_audit_trail z
      where action='delete'
      AND z.batch_id= batch_id
      and new_value = ','
      AND table_name = 'afudc_data'
         ;
      UPDATE acqa_batch_audit_trail   z
      SET action='delete', new_value=',f'
      WHERE new_value = ','
      AND    z.batch_id=batch_id
      AND table_name = 'afudc_data'  ;
      --AKM20150107 end


  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_afudc_ty_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON afudc_control FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.afudc_type_id <> :new.afudc_type_id OR
      (:old.afudc_type_id is null AND :new.afudc_type_id is not null) OR
      (:new.afudc_type_id is null AND :old.afudc_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'afudc_control', 'afudc_type_id', 'insert', :old.afudc_type_id,:new.afudc_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


  IF DELETING THEN
    IF :old.afudc_type_id <> :new.afudc_type_id OR
      (:old.afudc_type_id is null AND :new.afudc_type_id is not null) OR
      (:new.afudc_type_id is null AND :old.afudc_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'afudc_control', 'afudc_type_id', 'delete', :old.afudc_type_id,:new.afudc_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_al_audit_trail AFTER
 INSERT  /*OR  UPDATE  */ OR  DELETE  /*OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON asset_location FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.asset_location_id<> :new.asset_location_id OR
      (:old.asset_location_id is null AND :new.asset_location_id is not null) OR
      (:new.asset_location_id is null AND :old.asset_location_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'asset_location', 'asset_location_id', 'insert', :old.asset_location_id,:new.asset_location_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


   IF DELETING THEN
    IF :old.asset_location_id<> :new.asset_location_id OR
      (:old.asset_location_id is null AND :new.asset_location_id is not null) OR
      (:new.asset_location_id is null AND :old.asset_location_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'asset_location', 'asset_location_id', 'delete', :old.asset_location_id,:new.asset_location_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;



END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_bs_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON business_segment FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.bus_segment_id <> :new.bus_segment_id OR
      (:old.bus_segment_id is null AND :new.bus_segment_id is not null) OR
      (:new.bus_segment_id is null AND :old.bus_segment_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'business_segment', 'bus_segment_id', 'insert', :old.bus_segment_id,:new.bus_segment_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


  IF DELETING THEN
    IF :old.bus_segment_id <> :new.bus_segment_id OR
      (:old.bus_segment_id is null AND :new.bus_segment_id is not null) OR
      (:new.bus_segment_id is null AND :old.bus_segment_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'business_segment', 'bus_segment_id', 'delete', :old.bus_segment_id,:new.bus_segment_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_bs_gl_audit_trail AFTER
 INSERT  OR  DELETE  --OF company_id, major_location_id */
 ON gl_acct_bus_segment FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.gl_account_id <> :new.gl_account_id OR
      (:old.gl_account_id is null AND :new.gl_account_id is not null) OR
      (:new.gl_account_id is null AND :old.gl_account_id is not null) OR
      (:old.bus_segment_id <> :new.bus_segment_id ) OR
      (:old.bus_segment_id is null AND :new.bus_segment_id is not null) OR
      (:new.bus_segment_id is null AND :old.bus_segment_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'gl_acct_bus_segment', 'bus_segment_id,gl_account_id', 'insert', :old.bus_segment_id||','||:old.gl_account_id,:new.bus_segment_id||','||:new.gl_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.gl_account_id <> :new.gl_account_id OR
      (:old.gl_account_id is null AND :new.gl_account_id is not null) OR
      (:new.gl_account_id is null AND :old.gl_account_id is not null) OR
      (:old.bus_segment_id <> :new.bus_segment_id ) OR
      (:old.bus_segment_id is null AND :new.bus_segment_id is not null) OR
      (:new.bus_segment_id is null AND :old.bus_segment_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'gl_acct_bus_segment', 'bus_segment_id,gl_account_id', 'delete', :old.bus_segment_id||','||:old.gl_account_id,:new.bus_segment_id||','||:new.gl_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_cc_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON class_code FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.class_code_id <> :new.class_code_id OR
      (:old.class_code_id is null AND :new.class_code_id is not null) OR
      (:new.class_code_id is null AND :old.class_code_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'class_code', 'class_code_id', 'insert', :old.class_code_id,:new.class_code_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


  IF DELETING THEN
    IF :old.class_code_id <> :new.class_code_id OR
      (:old.class_code_id is null AND :new.class_code_id is not null) OR
      (:new.class_code_id is null AND :old.class_code_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'class_code', 'class_code_id', 'delete', :old.class_code_id,:new.class_code_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_co_bs_ct_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON company_bus_segment_control FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
--IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
 --   IF --:old.bus_segment_id <> :new.bus_segment_id OR
 --     (:old.bus_segment_id is null AND :new.bus_segment_id is not null) OR
 --     (:new.bus_segment_id is null AND :old.bus_segment_id is not null) OR
 --     (:old.company_id <> :new.company_id ) OR
 --     (:old.company_id is null AND :new.company_id is not null and :old.bus_segment_id is null AND :new.bus_segment_id is not null) OR
 --     (:new.company_id is null AND :old.company_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_bus_segment_control', 'company_id,bus_segment_id', 'insert', :old.company_id||','||:old.bus_segment_id,:new.company_id||','||:new.bus_segment_id, USER, SYSDATE,  window, windowtitle, comments);
    --END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    --IF :old.bus_segment_id <> :new.bus_segment_id OR
    --  (:old.bus_segment_id is null AND :new.bus_segment_id is not null) OR
    --  (:new.bus_segment_id is null AND :old.bus_segment_id is not null) OR
    --  (:old.company_id <> :new.company_id )
      --(:old.company_id is null AND :new.company_id is not null) OR
      --(:new.company_id is null AND :old.company_id is not null)
      --THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_bus_segment_control', 'company_id,bus_segment_id', 'delete', :old.company_id||','||:old.bus_segment_id,:new.company_id||','||:new.bus_segment_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  --END IF;
--$$$AKM
--END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_co_gl_audit_trail AFTER
 INSERT  OR  DELETE  --OF company_id, major_location_id */
 ON company_gl_account FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.gl_account_id <> :new.gl_account_id OR
      (:old.gl_account_id is null AND :new.gl_account_id is not null) OR
      (:new.gl_account_id is null AND :old.gl_account_id is not null) OR
      (:old.company_id <> :new.company_id ) OR
      (:old.company_id is null AND :new.company_id is not null) OR
      (:new.company_id is null AND :old.company_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_gl_account', 'company_id,gl_account_id', 'insert', :old.company_id||','||:old.gl_account_id,:new.company_id||','||:new.gl_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.gl_account_id <> :new.gl_account_id OR
      (:old.gl_account_id is null AND :new.gl_account_id is not null) OR
      (:new.gl_account_id is null AND :old.gl_account_id is not null) OR
      (:old.company_id <> :new.company_id ) OR
      (:old.company_id is null AND :new.company_id is not null) OR
      (:new.company_id is null AND :old.company_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_gl_account', 'company_id,gl_account_id', 'delete', :old.company_id||','||:old.gl_account_id,:new.company_id||','||:new.gl_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_co_loc_audit_trail AFTER
 INSERT OR  DELETE  --OF company_id, major_location_id */
 ON company_major_location FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.major_location_id <> :new.major_location_id OR
      (:old.major_location_id is null AND :new.major_location_id is not null) OR
      (:new.major_location_id is null AND :old.major_location_id is not null) OR
      (:old.company_id <> :new.company_id ) OR
      (:old.company_id is null AND :new.company_id is not null) OR
      (:new.company_id is null AND :old.company_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_major_location', 'company_id,major_location_id', 'insert', :old.company_id||','||:old.major_location_id,:new.company_id||','||:new.major_location_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.major_location_id <> :new.major_location_id OR
      (:old.major_location_id is null AND :new.major_location_id is not null) OR
      (:new.major_location_id is null AND :old.major_location_id is not null) OR
      (:old.company_id <> :new.company_id ) OR
      (:old.company_id is null AND :new.company_id is not null) OR
      (:new.company_id is null AND :old.company_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_major_location', 'company_id,major_location_id', 'delete', :old.company_id||','||:old.major_location_id,:new.company_id||','||:new.major_location_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_co_pu_audit_trail AFTER
 INSERT OR  DELETE  --OF company_id, major_location_id */
 ON company_property_unit FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.company_id <> :new.company_id OR
      (:old.company_id is null AND :new.company_id is not null) OR
      (:new.company_id is null AND :old.company_id is not null) OR
      (:old.property_unit_id <> :new.property_unit_id ) OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_property_unit', 'property_unit_id,company_id', 'insert', :old.property_unit_id||','||:old.company_id,:new.property_unit_id||','||:new.company_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.company_id <> :new.company_id OR
      (:old.company_id is null AND :new.company_id is not null) OR
      (:new.company_id is null AND :old.company_id is not null) OR
      (:old.property_unit_id <> :new.property_unit_id ) OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'company_property_unit', 'property_unit_id,company_id', 'delete', :old.property_unit_id||','||:old.company_id,:new.property_unit_id||','||:new.company_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_dept_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON department FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.department_id <> :new.department_id OR
      (:old.department_id is null AND :new.department_id is not null) OR
      (:new.department_id is null AND :old.department_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'department', 'department_id', 'insert', :old.department_id,:new.department_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
  --$$$AKM
  IF DELETING THEN
    IF :old.department_id <> :new.department_id OR
      (:old.department_id is null AND :new.department_id is not null) OR
      (:new.department_id is null AND :old.department_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'department', 'department_id', 'delete', :old.department_id,:new.department_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_div_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON division FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.division_id <> :new.division_id OR
      (:old.division_id is null AND :new.division_id is not null) OR
      (:new.division_id is null AND :old.division_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'division', 'division_id', 'delete', :old.division_id,:new.division_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.division_id <> :new.division_id OR
      (:old.division_id is null AND :new.division_id is not null) OR
      (:new.division_id is null AND :old.division_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'division', 'division_id', 'insert', :old.division_id,:new.division_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_fc_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON func_class FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.func_class_id <> :new.func_class_id OR
      (:old.func_class_id is null AND :new.func_class_id is not null) OR
      (:new.func_class_id is null AND :old.func_class_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'func_class', 'func_class_id', 'insert', :old.func_class_id,:new.func_class_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

  --$$$AKM
  IF DELETING THEN
    IF :old.func_class_id <> :new.func_class_id OR
      (:old.func_class_id is null AND :new.func_class_id is not null) OR
      (:new.func_class_id is null AND :old.func_class_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'func_class', 'func_class_id', 'delete', :old.func_class_id,:new.func_class_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_fc_loc_audit_trail AFTER
 INSERT  OR  DELETE  --OF company_id, major_location_id */
 ON func_class_loc_type FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.location_type_id <> :new.location_type_id OR
      (:old.location_type_id is null AND :new.location_type_id is not null) OR
      (:new.location_type_id is null AND :old.location_type_id is not null) OR
      (:old.func_class_id <> :new.func_class_id ) OR
      (:old.func_class_id is null AND :new.func_class_id is not null) OR
      (:new.func_class_id is null AND :old.func_class_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'func_class_loc_type', 'func_class_id,location_type_id', 'insert', :old.func_class_id||','||:old.location_type_id,:new.func_class_id||','||:new.location_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.location_type_id <> :new.location_type_id OR
      (:old.location_type_id is null AND :new.location_type_id is not null) OR
      (:new.location_type_id is null AND :old.location_type_id is not null) OR
      (:old.func_class_id <> :new.func_class_id ) OR
      (:old.func_class_id is null AND :new.func_class_id is not null) OR
      (:new.func_class_id is null AND :old.func_class_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'func_class_loc_type', 'func_class_id,location_type_id', 'delete', :old.func_class_id||','||:old.location_type_id,:new.func_class_id||','||:new.location_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_fc_pg_audit_trail AFTER
 INSERT OR  DELETE  --OF company_id, major_location_id */
 ON func_class_prop_grp FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.property_group_id <> :new.property_group_id OR
      (:old.property_group_id is null AND :new.property_group_id is not null) OR
      (:new.property_group_id is null AND :old.property_group_id is not null) OR
      (:old.func_class_id <> :new.func_class_id ) OR
      (:old.func_class_id is null AND :new.func_class_id is not null) OR
      (:new.func_class_id is null AND :old.func_class_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'func_class_prop_grp', 'func_class_id,property_group_id', 'insert', :old.func_class_id||','||:old.property_group_id,:new.func_class_id||','||:new.property_group_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.property_group_id <> :new.property_group_id OR
      (:old.property_group_id is null AND :new.property_group_id is not null) OR
      (:new.property_group_id is null AND :old.property_group_id is not null) OR
      (:old.func_class_id <> :new.func_class_id ) OR
      (:old.func_class_id is null AND :new.func_class_id is not null) OR
      (:new.func_class_id is null AND :old.func_class_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'func_class_prop_grp', 'func_class_id,property_group_id', 'delete', :old.func_class_id||','||:old.property_group_id,:new.func_class_id||','||:new.property_group_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_fp_type_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON work_order_type FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
  IF :NEW.funding_wo=1 then
    IF :old.work_order_type_id <> :new.work_order_type_id OR
      (:old.work_order_type_id is null AND :new.work_order_type_id is not null) OR
      (:new.work_order_type_id is null AND :old.work_order_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'work_order_type_fp', 'work_order_type_id', 'insert', :old.work_order_type_id,:new.work_order_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
  END IF;


  IF DELETING THEN
  IF :old.funding_wo=1 then
    IF :old.work_order_type_id <> :new.work_order_type_id OR
      (:old.work_order_type_id is null AND :new.work_order_type_id is not null) OR
      (:new.work_order_type_id is null AND :old.work_order_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'work_order_type_fp', 'work_order_type_id', 'delete', :old.work_order_type_id,:new.work_order_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_gl_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON gl_account FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.gl_account_id <> :new.gl_account_id OR
      (:old.gl_account_id is null AND :new.gl_account_id is not null) OR
      (:new.gl_account_id is null AND :old.gl_account_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'gl_account', 'gl_account_id', 'insert', :old.gl_account_id,:new.gl_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

  --$$$AKM

  IF DELETING THEN
    IF :old.gl_account_id <> :new.gl_account_id OR
      (:old.gl_account_id is null AND :new.gl_account_id is not null) OR
      (:new.gl_account_id is null AND :old.gl_account_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'gl_account', 'gl_account_id', 'delete', :old.gl_account_id,:new.gl_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_loc_type_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON location_type FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.location_type_id <> :new.location_type_id OR
      (:old.location_type_id is null AND :new.location_type_id is not null) OR
      (:new.location_type_id is null AND :old.location_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'location_type', 'location_type_id', 'insert', :old.location_type_id,:new.location_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


  IF DELETING THEN
    IF :old.location_type_id <> :new.location_type_id OR
      (:old.location_type_id is null AND :new.location_type_id is not null) OR
      (:new.location_type_id is null AND :old.location_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'location_type', 'location_type_id', 'delete', :old.location_type_id,:new.location_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_ml_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON major_location FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.major_location_id <> :new.major_location_id OR
      (:old.major_location_id is null AND :new.major_location_id is not null) OR
      (:new.major_location_id is null AND :old.major_location_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'major_location', 'major_location_id', 'insert', :old.major_location_id,:new.major_location_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


   IF DELETING THEN
    IF :old.major_location_id <> :new.major_location_id OR
      (:old.major_location_id is null AND :new.major_location_id is not null) OR
      (:new.major_location_id is null AND :old.major_location_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'major_location', 'major_location_id', 'delete', :old.major_location_id,:new.major_location_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_pg_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON property_group FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.property_group_id <> :new.property_group_id OR
      (:old.property_group_id is null AND :new.property_group_id is not null) OR
      (:new.property_group_id is null AND :old.property_group_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'property_group', 'property_group_id', 'insert', :old.property_group_id,:new.property_group_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


  IF DELETING THEN
    IF :old.property_group_id <> :new.property_group_id OR
      (:old.property_group_id is null AND :new.property_group_id is not null) OR
      (:new.property_group_id is null AND :old.property_group_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'property_group', 'property_group_id', 'delete', :old.property_group_id,:new.property_group_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_pg_pu_audit_trail AFTER
 INSERT  OR  DELETE  --OF company_id, major_location_id */
 ON prop_group_prop_unit FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.property_group_id <> :new.property_group_id OR
      (:old.property_group_id is null AND :new.property_group_id is not null) OR
      (:new.property_group_id is null AND :old.property_group_id is not null) OR
      (:old.property_unit_id <> :new.property_unit_id ) OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'prop_group_prop_unit', 'property_unit_id,property_group_id', 'insert', :old.property_unit_id||','||:old.property_group_id,:new.property_unit_id||','||:new.property_group_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.property_group_id <> :new.property_group_id OR
      (:old.property_group_id is null AND :new.property_group_id is not null) OR
      (:new.property_group_id is null AND :old.property_group_id is not null) OR
      (:old.property_unit_id <> :new.property_unit_id ) OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'prop_group_prop_unit', 'property_unit_id,property_group_id', 'delete', :old.property_unit_id||','||:old.property_group_id,:new.property_unit_id||','||:new.property_group_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_pu_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON property_unit FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.property_unit_id <> :new.property_unit_id OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'property_unit', 'property_unit_id', 'insert', :old.property_unit_id,:new.property_unit_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


  IF DELETING THEN
    IF :old.property_unit_id <> :new.property_unit_id OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'property_unit', 'property_unit_id', 'delete', :old.property_unit_id,:new.property_unit_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_ru_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON retirement_unit FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.retirement_unit_id <> :new.retirement_unit_id OR
      (:old.retirement_unit_id is null AND :new.retirement_unit_id is not null) OR
      (:new.retirement_unit_id is null AND :old.retirement_unit_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'retirement_unit', 'retirement_unit_id', 'insert', :old.retirement_unit_id,:new.retirement_unit_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


  IF DELETING THEN
    IF :old.retirement_unit_id <> :new.retirement_unit_id OR
      (:old.retirement_unit_id is null AND :new.retirement_unit_id is not null) OR
      (:new.retirement_unit_id is null AND :old.retirement_unit_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'retirement_unit', 'retirement_unit_id', 'delete', :old.retirement_unit_id,:new.retirement_unit_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_sa_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON sub_account FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.sub_account_id <> :new.sub_account_id OR
      (:old.sub_account_id is null AND :new.sub_account_id is not null) OR
      (:new.sub_account_id is null AND :old.sub_account_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'sub_account', 'sub_account_id', 'insert', :old.sub_account_id,:new.sub_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

  --$$$AKM

   IF DELETING THEN
    IF :old.sub_account_id <> :new.sub_account_id OR
      (:old.sub_account_id is null AND :new.sub_account_id is not null) OR
      (:new.sub_account_id is null AND :old.sub_account_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'sub_account', 'sub_account_id', 'delete', :old.sub_account_id,:new.sub_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_ua_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON utility_account FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.utility_account_id <> :new.utility_account_id OR
      (:old.utility_account_id is null AND :new.utility_account_id is not null) OR
      (:new.utility_account_id is null AND :old.utility_account_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'utility_account', 'utility_account_id', 'insert', :old.utility_account_id,:new.utility_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

  --$$$AKM

   IF DELETING THEN
    IF :old.utility_account_id <> :new.utility_account_id OR
      (:old.utility_account_id is null AND :new.utility_account_id is not null) OR
      (:new.utility_account_id is null AND :old.utility_account_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'utility_account', 'utility_account_id', 'delete', :old.utility_account_id,:new.utility_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_ua_pu_audit_trail AFTER
 INSERT  OR  DELETE  --OF company_id, major_location_id */
 ON util_acct_prop_unit FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.utility_account_id <> :new.utility_account_id OR
      (:old.utility_account_id is null AND :new.utility_account_id is not null) OR
      (:new.utility_account_id is null AND :old.utility_account_id is not null) OR
      (:old.property_unit_id <> :new.property_unit_id ) OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'util_acct_prop_unit', 'property_unit_id,utility_account_id', 'insert', :old.property_unit_id||','||:old.utility_account_id,:new.property_unit_id||','||:new.utility_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.utility_account_id <> :new.utility_account_id OR
      (:old.utility_account_id is null AND :new.utility_account_id is not null) OR
      (:new.utility_account_id is null AND :old.utility_account_id is not null) OR
      (:old.property_unit_id <> :new.property_unit_id ) OR
      (:old.property_unit_id is null AND :new.property_unit_id is not null) OR
      (:new.property_unit_id is null AND :old.property_unit_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'util_acct_prop_unit', 'property_unit_id,utility_account_id', 'delete', :old.property_unit_id||','||:old.utility_account_id,:new.property_unit_id||','||:new.utility_account_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_wf_level_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON workflow_rule FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.workflow_rule_id <> :new.workflow_rule_id OR
      (:old.workflow_rule_id is null AND :new.workflow_rule_id is not null) OR
      (:new.workflow_rule_id is null AND :old.workflow_rule_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'workflow_rule', 'workflow_type_id', 'insert', :old.workflow_rule_id,:new.workflow_rule_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;



  IF DELETING THEN
    IF :old.workflow_rule_id <> :new.workflow_rule_id OR
      (:old.workflow_rule_id is null AND :new.workflow_rule_id is not null) OR
      (:new.workflow_rule_id is null AND :old.workflow_rule_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'workflow_rule', 'workflow_rule_id', 'delete', :old.workflow_rule_id,:new.workflow_rule_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_wf_ty_ru_audit_trail AFTER
 INSERT OR  DELETE  --OF company_id, major_location_id */
 ON workflow_type_rule FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.workflow_type_id <> :new.workflow_type_id OR
      (:old.workflow_type_id is null AND :new.workflow_type_id is not null) OR
      (:new.workflow_type_id is null AND :old.workflow_type_id is not null) OR
      (:old.workflow_rule_id <> :new.workflow_rule_id ) OR
      (:old.workflow_rule_id is null AND :new.workflow_rule_id is not null) OR
      (:new.workflow_rule_id is null AND :old.workflow_rule_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'workflow_type_rule', 'workflow_rule_id,workflow_type_id', 'insert', :old.workflow_rule_id||','||:old.workflow_type_id,:new.workflow_rule_id||','||:new.workflow_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
    IF :old.workflow_type_id <> :new.workflow_type_id OR
      (:old.workflow_type_id is null AND :new.workflow_type_id is not null) OR
      (:new.workflow_type_id is null AND :old.workflow_type_id is not null) OR
      (:old.workflow_rule_id <> :new.workflow_rule_id ) OR
      (:old.workflow_rule_id is null AND :new.workflow_rule_id is not null) OR
      (:new.workflow_rule_id is null AND :old.workflow_rule_id is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'workflow_type_rule', 'workflow_rule_id,workflow_type_id', 'delete', :old.workflow_rule_id||','||:old.workflow_type_id,:new.workflow_rule_id||','||:new.workflow_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_wf_type_audit_trail AFTER
 INSERT OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON workflow_type FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.workflow_type_id <> :new.workflow_type_id OR
      (:old.workflow_type_id is null AND :new.workflow_type_id is not null) OR
      (:new.workflow_type_id is null AND :old.workflow_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'workflow_type', 'workflow_type_id', 'insert', :old.workflow_type_id,:new.workflow_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;



  IF DELETING THEN
    IF :old.workflow_type_id <> :new.workflow_type_id OR
      (:old.workflow_type_id is null AND :new.workflow_type_id is not null) OR
      (:new.workflow_type_id is null AND :old.workflow_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'workflow_type', 'workflow_type_id', 'delete', :old.workflow_type_id,:new.workflow_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;


END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_wf_us_audit_trail AFTER
 INSERT OR  DELETE  --OF company_id, major_location_id */
 ON approval_auth_level FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
    IF :old.approval_type_id <> :new.approval_type_id OR
      (:old.approval_type_id is null AND :new.approval_type_id is not null) OR
      (:new.approval_type_id is null AND :old.approval_type_id is not null) OR
      (:old.auth_level <> :new.auth_level ) OR
      (:old.auth_level is null AND :new.auth_level is not null) OR
      (:new.auth_level is null AND :old.auth_level is not null)    OR
      (:old.users <> :new.users ) OR
      (:old.users is null AND :new.users is not null) OR
      (:new.users is null AND :old.users is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail_stg    stg
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (1, batch_id, 'approval_auth_level', 'approval_type_id,auth_level,users', 'insert', :old.approval_type_id||','||:old.auth_level||','||:old.users,:new.approval_type_id||','||:new.auth_level||','||:new.users, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
 --$$$AKM
  IF DELETING THEN
      IF :old.approval_type_id <> :new.approval_type_id OR
      (:old.approval_type_id is null AND :new.approval_type_id is not null) OR
      (:new.approval_type_id is null AND :old.approval_type_id is not null) OR
      (:old.auth_level <> :new.auth_level ) OR
      (:old.auth_level is null AND :new.auth_level is not null) OR
      (:new.auth_level is null AND :old.auth_level is not null) OR
      (:old.users <> :new.users ) OR
      (:old.users is null AND :new.users is not null) OR
      (:new.users is null AND :old.users is not null)THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail_stg   stg
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (1, batch_id, 'approval_auth_level', 'approval_type_id,auth_level,users', 'delete', :old.approval_type_id||','||:old.auth_level||','||:old.users,:new.approval_type_id||','||:new.auth_level||','||:new.users, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;

--$$$AKM
END IF;
END;
/

CREATE OR REPLACE TRIGGER trig_acqa_wo_type_audit_trail AFTER
 INSERT  OR  DELETE  --OF bus_segment_id, description, status_code_id, external_bus_segment */
 ON work_order_type FOR EACH ROW
DECLARE
  batch_id number(22,0);
  osuser varchar2(60);
  machine varchar2(60);
  terminal varchar2(60);
  old_lookup varchar2(250);
  new_lookup varchar2(250);
  pk_lookup varchar2(1500);
  pk_temp varchar2(250);
  window varchar2(60);
  windowtitle varchar2(250);
  comments varchar2(250);
  trans varchar2(35);
  ret number(22,0);
BEGIN
IF nvl(sys_context('powerplant_ctx','audit'),'yes') = 'no' then
   return;
end if;
  window       := nvl(sys_context('powerplant_ctx','window'      ),'unknown window');
  windowtitle  := nvl(sys_context('powerplant_ctx','windowtitle' ),'');
  trans        := nvl(sys_context('powerplant_ctx','process'     ),'');
  comments     := nvl(sys_context('powerplant_ctx','comments'    ),'');
--  prog         := nvl(sys_context('powerplant_ctx','program'     ),'');
  osuser       := nvl(sys_context('powerplant_ctx','osuser'      ),'');
  machine      := nvl(sys_context('powerplant_ctx','machine'     ),'');
  terminal     := nvl(sys_context('powerplant_ctx','terminal'    ),'');
  comments     := 'OSUSER='||trim(osuser)||'; MACHINE='||trim(machine)||'; TERMINAL='||trim(terminal);
--IF prog = '' THEN SELECT PROGRAM INTO prog FROM V$SESSION WHERE AUDSID = USERENV('sessionid');  END IF;
IF WINDOW = 'w_acqaider_main' then
  IF INSERTING THEN
  IF :NEW.funding_wo=0 then
    IF :old.work_order_type_id <> :new.work_order_type_id OR
      (:old.work_order_type_id is null AND :new.work_order_type_id is not null) OR
      (:new.work_order_type_id is null AND :old.work_order_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'work_order_type', 'work_order_type_id', 'insert', :old.work_order_type_id,:new.work_order_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
  END IF;


  IF DELETING THEN
  IF :old.funding_wo=0 then
    IF :old.work_order_type_id <> :new.work_order_type_id OR
      (:old.work_order_type_id is null AND :new.work_order_type_id is not null) OR
      (:new.work_order_type_id is null AND :old.work_order_type_id is not null) THEN

    select batch_id into batch_id
    from acqa_current_user_batch
    where user_id = USER
    and session_id = USERENV('sessionid')
    ;

        INSERT INTO acqa_batch_audit_trail
          (id, batch_id, table_name, column_name, action, old_value, new_value, user_id, time_stamp, ACTIVE_WINDOW, WINDOW_TITLE, comments)
          VALUES
          (pp_acqa_table_audit_seq.NEXTVAL, batch_id, 'work_order_type', 'work_order_type_id', 'delete', :old.work_order_type_id,:new.work_order_type_id, USER, SYSDATE,  window, windowtitle, comments);
    END IF;
  END IF;
  END IF;

END IF;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2509, 0, 2015, 1, 0, 0, 43663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043663_sys_acqa_02_triggers_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
