/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043858_reg_jur_coc_wksp_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 05/19/2015 Sarah Byers    Add theo cap struct field to reg_jur_template
||============================================================================
*/

insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values (
	'REG', 'uo_reg_jur_coc_ws', 'Jur Cost of Capital', 'uo_reg_jur_coc_ws', 'Jur Cost of Capital', 1);

update ppbase_menu_items
	set item_order = item_order + 1
 where parent_menu_identifier = 'CASE_JUR_MAINT'
	and item_order >= 7;

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values (
	'REG', 'JUR_COST_OF_CAP', 2, 7, 'Jur Cost of Capital', 'Jurisdictional Cost of Capital', 'CASE_JUR_MAINT', 'uo_reg_jur_coc_ws', 1);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2562, 0, 2015, 2, 0, 0, 043858, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043858_reg_jur_coc_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;