/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029155_system_basegui_tables.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/18/2013 Stephen Wicks  Point Release
||============================================================================
*/

-- change the primary key on ppbase_system_options
alter table PPBASE_SYSTEM_OPTIONS drop primary key drop index;

alter table PPBASE_SYSTEM_OPTIONS
   add constraint PPBASE_SYSTEM_OPTIONS_PK
       primary key (SYSTEM_OPTION_ID)
       using index tablespace PWRPLANT_IDX;

-- create ppbase_system_options_wksp
create table PPBASE_SYSTEM_OPTIONS_WKSP
(
 SYSTEM_OPTION_ID     varchar2(254) not null,
 WORKSPACE_IDENTIFIER varchar2(35)  not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table PPBASE_SYSTEM_OPTIONS_WKSP
   add constraint PPBASE_SYSTEM_OPTIONS_WKSP_PK
       primary key (SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER)
       using index tablespace PWRPLANT_IDX;

alter table PPBASE_SYSTEM_OPTIONS_WKSP
   add constraint PPBASE_SYSOPTCNTRS_SYSOPT_FK
       foreign key (SYSTEM_OPTION_ID)
       references PPBASE_SYSTEM_OPTIONS;

-- create ppbase_system_options_values
create table PPBASE_SYSTEM_OPTIONS_VALUES
(
 SYSTEM_OPTION_ID varchar2(254) not null,
 OPTION_VALUE     varchar2(254) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table PPBASE_SYSTEM_OPTIONS_VALUES
   add constraint PPBASE_SYSTEM_OPTIONS_VAL_PK
       primary key (SYSTEM_OPTION_ID, OPTION_VALUE)
       using index tablespace PWRPLANT_IDX;

alter table PPBASE_SYSTEM_OPTIONS_VALUES
   add constraint PPBASE_SYSOPTVALS_SYSOPT_FK
       foreign key (SYSTEM_OPTION_ID)
       references PPBASE_SYSTEM_OPTIONS;

-- ppbase_report_package
create table PPBASE_REPORT_PACKAGE
(
 PACKAGE_ID       number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 PACKAGE_NUMBER   varchar2(35) not null,
 DESCRIPTION      varchar2(35) not null,
 LONG_DESCRIPTION varchar2(254),
 SEPARATE_JOBS_ON varchar2(254) not null,
 NAME_JOBS_ON     varchar2(254) not null
);

alter table PPBASE_REPORT_PACKAGE
   add constraint PPBASE_REPORT_PACKAGE_PK
       primary key (PACKAGE_ID)
       using index tablespace PWRPLANT_IDX;

-- ppbase_report_package_reports
create table PPBASE_REPORT_PACKAGE_REPORTS
(
 PACKAGE_ID   number(22,0) not null,
 REPORT_ID    number(22,0) not null,
 TIME_STAMP   date,
 USER_ID      varchar2(18),
 REPORT_ORDER number(22,0) not null,
 WHERE_CLAUSE varchar2(2000)
);

alter table PPBASE_REPORT_PACKAGE_REPORTS
   add constraint PPBASE_RPT_PACKAGE_REPORTS_PK
       primary key (PACKAGE_ID, REPORT_ORDER)
       using index tablespace PWRPLANT_IDX;

alter table PPBASE_REPORT_PACKAGE_REPORTS
   add constraint PPBASE_RPTPACKRTP_RPTPACK_FK
       foreign key (PACKAGE_ID)
       references PPBASE_REPORT_PACKAGE;

alter table PPBASE_REPORT_PACKAGE_REPORTS
   add constraint PPBASE_RPTPACKRTP_RPT_FK
       foreign key (REPORT_ID)
       references PP_REPORTS;

create unique index PPBASE_RPTPCKGRPT_PACK_RPT_NDX
   on PPBASE_REPORT_PACKAGE_REPORTS (REPORT_ID, PACKAGE_ID)
      tablespace PWRPLANT_IDX;

-- add filter user object name column to pp_reports_filter
alter table PP_REPORTS_FILTER add FILTER_UO_NAME varchar2(35);

-- add parameter user object name column and corresponding parameter options to pp_reports_time_option
alter table PP_REPORTS_TIME_OPTION add PARAMETER_UO_NAME varchar2(40);
alter table PP_REPORTS_TIME_OPTION add DWNAME1           varchar2(35);
alter table PP_REPORTS_TIME_OPTION add LABEL1            varchar2(35);
alter table PP_REPORTS_TIME_OPTION add KEYCOLUMN1        varchar2(35);
alter table PP_REPORTS_TIME_OPTION add DWNAME2           varchar2(35);
alter table PP_REPORTS_TIME_OPTION add LABEL2            varchar2(35);
alter table PP_REPORTS_TIME_OPTION add KEYCOLUMN2        varchar2(35);
alter table PP_REPORTS_TIME_OPTION add DWNAME3           varchar2(35);
alter table PP_REPORTS_TIME_OPTION add LABEL3            varchar2(35);
alter table PP_REPORTS_TIME_OPTION add KEYCOLUMN3        varchar2(35);

-- add sort order to pp_report_type
alter table PP_REPORT_TYPE add SORT_ORDER number(22,0);

-- remove split workspace columns from ppbase_menu_items
alter table PPBASE_MENU_ITEMS drop column SPLIT_YN;
alter table PPBASE_MENU_ITEMS drop column IDENTIFIER_2;
alter table PPBASE_MENU_ITEMS drop column UO_WKSP_NAME_2;
alter table PPBASE_MENU_ITEMS drop column SPLIT_DIRECTION;

-- remove picture_name from ppbase_menu_items
alter table PPBASE_MENU_ITEMS drop column PICTURE_NAME;

-- make the parent_identifier nullable and update any rows with menu_level = 1 to have a null parent_identifier
alter table PPBASE_MENU_ITEMS drop primary key drop index;
alter table PPBASE_MENU_ITEMS modify PARENT_IDENTIFIER null;
update PPBASE_MENU_ITEMS set PARENT_IDENTIFIER = null where NVL(MENU_LEVEL, 0) = 1;


-- create ppbase_workspace
create table PPBASE_WORKSPACE
(
 MODULE               varchar2(35) not null,
 WORKSPACE_IDENTIFIER varchar2(35) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 LABEL                varchar2(35) not null,
 WORKSPACE_UO_NAME    varchar2(40) not null,
 MINIHELP             varchar2(100)
);

alter table PPBASE_WORKSPACE
   add constraint PPBASE_WORKSPACE_PK
       primary key (MODULE, WORKSPACE_IDENTIFIER)
       using index tablespace PWRPLANT_IDX;

-- recreate ppbase_menu_items
create table D1_PPBASE_MENU_ITEMS as select * from PPBASE_MENU_ITEMS;

drop table PPBASE_MENU_ITEMS;

create table PPBASE_MENU_ITEMS
(
 MODULE                 varchar2(35) not null,
 MENU_IDENTIFIER        varchar2(35) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 MENU_LEVEL             number(22,0) not null,
 ITEM_ORDER             number(22,0) not null,
 LABEL                  varchar2(35),
 MINIHELP               varchar2(100),
 PARENT_MENU_IDENTIFIER varchar2(35),
 WORKSPACE_IDENTIFIER   varchar2(35),
 ENABLE_YN              number(22,0)
);

alter table PPBASE_MENU_ITEMS
   add constraint PPBASE_MENU_ITEMS_PK
       primary key (MODULE, MENU_IDENTIFIER)
       using index tablespace PWRPLANT_IDX;

alter table PPBASE_MENU_ITEMS
   add constraint PPBASE_MENU_WKSP_FK
       foreign key (MODULE, WORKSPACE_IDENTIFIER)
       references PPBASE_WORKSPACE (MODULE, WORKSPACE_IDENTIFIER);

alter table PPBASE_MENU_ITEMS
   add constraint PPBASE_MENU_PARENT_FK
       foreign key (MODULE, PARENT_MENU_IDENTIFIER)
       references PPBASE_MENU_ITEMS (MODULE, MENU_IDENTIFIER);

create unique index PPBASE_MENU_UNQ_ITEM
   on PPBASE_MENU_ITEMS (MODULE, MENU_LEVEL, PARENT_MENU_IDENTIFIER, ITEM_ORDER)
      tablespace PWRPLANT_IDX;

-- recreate ppbase_workspace_links
create table D1_PPBASE_WORKSPACE_LINKS as select * from PPBASE_WORKSPACE_LINKS;

drop table PPBASE_WORKSPACE_LINKS;

create table PPBASE_WORKSPACE_LINKS
(
 MODULE                      varchar2(35) not null,
 WORKSPACE_IDENTIFIER        varchar2(35) not null,
 LINK_ORDER                  number(22,0) not null,
 TIME_STAMP                  date,
 USER_ID                     varchar2(18),
 LINKED_WORKSPACE_IDENTIFIER varchar2(35) not null
);

alter table PPBASE_WORKSPACE_LINKS
   add constraint PPBASE_WORKSPACE_LINKS_PK
       primary key (MODULE, WORKSPACE_IDENTIFIER, LINK_ORDER)
       using index tablespace PWRPLANT_IDX;

alter table PPBASE_WORKSPACE_LINKS
   add constraint PPBASE_WKSP_LINK_WKSP_FK
       foreign key (MODULE, WORKSPACE_IDENTIFIER)
       references PPBASE_WORKSPACE (MODULE, WORKSPACE_IDENTIFIER);

alter table PPBASE_WORKSPACE_LINKS
   add constraint PPBASE_WKSP_LINK_LINKWKSP_FK
       foreign key (MODULE, LINKED_WORKSPACE_IDENTIFIER)
       references PPBASE_WORKSPACE (MODULE, WORKSPACE_IDENTIFIER);

-- Create a table to relate system options to modules.
create table PPBASE_SYSTEM_OPTIONS_MODULE
(
 SYSTEM_OPTION_ID varchar2(254) not null,
 MODULE           varchar2(35)  not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table PPBASE_SYSTEM_OPTIONS_MODULE
   add constraint PPBASE_SYSOPT_MODULE
       primary key (SYSTEM_OPTION_ID, MODULE)
       using index tablespace PWRPLANT_IDX;

alter table PPBASE_SYSTEM_OPTIONS_MODULE
   add constraint PPBASE_SYSOPT_MODULE_OPT_FK
       foreign key (SYSTEM_OPTION_ID)
       references PPBASE_SYSTEM_OPTIONS;

-- Convert the following property tax system options to base system options:
--    Reporting Center - Packages - Include Blank Reports Default
--    Reporting Center - Packages - Save as Separate PDF Files Default
--    Reporting Center - Packages - Send as Separate Print Jobs Default
--    Reports - Emailing - Default Subject
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
values
   ('Reports - Packages - Include Blank Reports Default', sysdate, user,
    'The default value for the ''Include Blank Reports in Package'' option when printing a package.  This option controls whether reports for a given package with no data are included (''Yes'') or not included (''No'') in the package.',
    0, 'No', null, 1);
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
values
   ('Reports - Packages - Save as Separate PDF Files Default', sysdate, user,
    'The default value for the ''Save Each Package/Report as Separate PDF File'' option when PDF''ing a package.  Saving each package/report as a separate PDF file results in one PDF file per package break (or per report if not printing a package); a ''No'' results in one PDF file for all packages/reports being printed.',
    0, 'Yes', null, 1);
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
values
   ('Reports - Packages - Send as Separate Print Jobs Default', sysdate, user,
    'The default value for the ''Send Each Package/Report as Separate Print Job'' option when printing a package.  This option controls management of print jobs.  Sending each package/report as a separate print job results in one print job per package break (or per report if not printing a package); a ''No'' results in one print job for all packages/reports being printed.',
    0, 'No', null, 1);
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
values
   ('Reports - Emailing - Default Subject', sysdate, user,
    'The default email subject to use when emailing a report and an email subject has not been specified.  This value will also be used in the message text as the name of the report, though that can be edited before sending the email.',
    0, 'PowerPlant Report', null, 1);

-- modify the workspace links to allow opening of windows and datawindows/reports
alter table PPBASE_WORKSPACE_LINKS modify LINKED_WORKSPACE_IDENTIFIER null;
alter table PPBASE_WORKSPACE_LINKS add LINKED_WINDOW               varchar2(40);
alter table PPBASE_WORKSPACE_LINKS add LINKED_DATAWINDOW           varchar2(40);
alter table PPBASE_WORKSPACE_LINKS add LINKED_WINDOW_LABEL         varchar2(35);
alter table PPBASE_WORKSPACE_LINKS add LINKED_WINDOW_MICROHELP     varchar2(254);
alter table PPBASE_WORKSPACE_LINKS add LINKED_DATAWINDOW_LABEL     varchar2(35);
alter table PPBASE_WORKSPACE_LINKS add LINKED_DATAWINDOW_MICROHELP varchar2(254);

-- modify the workspace uo name on ppbase_workspace to be 60 characters
alter table PPBASE_WORKSPACE modify WORKSPACE_UO_NAME varchar2(60);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (414, 0, 10, 4, 1, 0, 29155, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029155_system_basegui_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
