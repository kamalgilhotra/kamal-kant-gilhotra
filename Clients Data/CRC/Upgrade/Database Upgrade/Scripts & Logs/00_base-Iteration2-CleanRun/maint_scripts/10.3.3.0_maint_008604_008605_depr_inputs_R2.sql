/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008604_008605_depr_inputs_R2.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   12/09/2011 Sunjin Cone    Point Release
||============================================================================
*/

alter table DEPR_ACTIVITY
   add constraint FK_DA_DEPR_TRANS_SET_ID
       foreign key (DEPR_TRANS_SET_ID)
       references DEPR_TRANS_SET;

alter table PEND_DEPR_ACTIVITY
   add constraint FK_PDA_DEPR_TRANS_SET_ID
       foreign key (DEPR_TRANS_SET_ID)
       references DEPR_TRANS_SET;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (67, 0, 10, 3, 3, 0, 8604, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008604_008605_depr_inputs_R2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
