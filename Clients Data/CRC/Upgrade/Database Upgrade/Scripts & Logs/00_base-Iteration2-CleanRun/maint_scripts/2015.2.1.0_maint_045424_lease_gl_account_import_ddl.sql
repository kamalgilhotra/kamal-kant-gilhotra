/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045424_lease_gl_account_import_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.1.0 02/12/2016 Will Davis  Remove GL account from asset import
||============================================================================
*/

declare
v_count number;
begin
select count(1)
into v_count
from user_tab_columns
where table_name = 'LS_IMPORT_ASSET'
  AND COLUMN_NAME = 'GL_ACCOUNT_ID';
IF V_COUNT = 1 THEN
  EXECUTE IMMEDIATE 'ALTER TABLE LS_IMPORT_ASSET DROP COLUMN GL_ACCOUNT_ID';
END IF;
select count(1)
into v_count
from user_tab_columns
where table_name = 'LS_IMPORT_ASSET'
  AND COLUMN_NAME = 'GL_ACCOUNT_XLATE';
IF V_COUNT = 1 THEN
  EXECUTE IMMEDIATE 'ALTER TABLE LS_IMPORT_ASSET DROP COLUMN GL_ACCOUNT_XLATE';
END IF;
select count(1)
into v_count
from user_tab_columns
where table_name = 'LS_IMPORT_ASSET_ARCHIVE'
  AND COLUMN_NAME = 'GL_ACCOUNT_ID';
IF V_COUNT = 1 THEN
  EXECUTE IMMEDIATE 'ALTER TABLE LS_IMPORT_ASSET_ARCHIVE DROP COLUMN GL_ACCOUNT_ID';
END IF;
select count(1)
into v_count
from user_tab_columns
where table_name = 'LS_IMPORT_ASSET_ARCHIVE'
  AND COLUMN_NAME = 'GL_ACCOUNT_XLATE';
IF V_COUNT = 1 THEN
  EXECUTE IMMEDIATE 'ALTER TABLE LS_IMPORT_ASSET_ARCHIVE DROP COLUMN GL_ACCOUNT_XLATE';
END IF;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3056, 0, 2015, 2, 1, 0, 045424, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045424_lease_gl_account_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;