/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029365_cpr_loader_syscontrols.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   03/11/2013 Cliff Robinson   Point Release
||============================================================================
*/

-- add new system controls for the CPR Loader.
insert into PWRPLANT.PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
   select CONTROL_ID,
          'CPR Loader: Acct Summ Mths to Summ',
          '36',
          'The number of months to summarize to account summary when running from CPR Loader. Default to 3 years',
          -1
     from (select UPPER(trim('CPR Loader: Acct Summ Mths to Summ'))
             from DUAL
           minus
           select UPPER(trim(CONTROL_NAME))
             from PP_SYSTEM_CONTROL
            where UPPER(trim(CONTROL_NAME)) = UPPER(trim('CPR Loader: Acct Summ Mths to Summ'))),
          (select max(CONTROL_ID) + 1 CONTROL_ID from PP_SYSTEM_CONTROL);

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID,
    CONTROL_TYPE)
   select CONTROL_ID,
          'Is System Live?',
          'Yes',
          'dw_yes_no;1',
          'This is a system wide control that define if this system is live. This control is used in CPR Activity Loader.',
          -1,
          'system only'
     from (select UPPER(trim('Is System Live?'))
             from DUAL
           minus
           select UPPER(trim(CONTROL_NAME))
             from PP_SYSTEM_CONTROL
            where UPPER(trim(CONTROL_NAME)) = UPPER(trim('Is System Live?'))),
          (select max(CONTROL_ID) + 1 CONTROL_ID from PP_SYSTEM_CONTROL);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (316, 0, 10, 4, 0, 0, 29365, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029365_cpr_loader_syscontrols.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;