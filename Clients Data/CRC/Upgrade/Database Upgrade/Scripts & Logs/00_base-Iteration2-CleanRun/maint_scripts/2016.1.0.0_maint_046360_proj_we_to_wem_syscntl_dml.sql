/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046360_proj_we_to_wem_syscntl_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2016.1.0 09/06/2016 Daniel Mendel  Breakout sys control for WO and FP
 ||============================================================================
 */

insert into pp_system_control_company (control_id, control_name, control_value, company_id, long_description, description)
select (SELECT Max(control_id) FROM pp_system_control_company) + 1, 'FPEST - Copy FP wo_estimate to WEM', control_value, company_id,
'Yes means that FP wo_estimate will be copied to wo_est_monthly upon update. The default is no.', description
from pp_system_control_company 
where lower(trim(control_name)) = 'woest - copy wo_estimate to wem'
and not exists (select 1 from pp_system_control_company where lower(trim(control_name)) = lower('FPEST - Copy FP wo_estimate to WEM'));

UPDATE pp_system_control_company
SET control_name = 'WOEST - Copy WO wo_estimate to WEM', 
	long_description = 'Yes means that WO wo_estimate will be copied to wo_est_monthly upon update. The default is no.'
WHERE Lower(trim(control_name)) = 'woest - copy wo_estimate to wem';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3278, 0, 2016, 1, 0, 0, 046360, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046360_proj_we_to_wem_syscntl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;