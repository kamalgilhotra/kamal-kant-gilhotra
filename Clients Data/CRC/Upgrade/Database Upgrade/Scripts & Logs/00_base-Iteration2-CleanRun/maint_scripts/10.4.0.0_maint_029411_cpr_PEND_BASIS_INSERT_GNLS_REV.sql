/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029411_cpr_PEND_BASIS_INSERT_GNLS_REV.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   04/16/2013 Charlie Shilling
||============================================================================
*/

create or replace trigger PEND_BASIS_INSERT_GNLS_REV
   after insert on PEND_BASIS
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_BASIS_INSERT_GNLS_REV
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 03/27/2013 Brandon Beck     INITIAL Version to spread gain-loss-reversal across pend transactions
   || 10.4.0.0 04/16/2013 Brandon Beck     Spread gain-loss-reversal across pend transactions
   ||============================================================================
   */

begin

   --
   -- Allocate the gain_loss_reversal based on the COR and SALVAGE allocation of charges
   --
   update PEND_TRANSACTION PT2
      set GAIN_LOSS_REVERSAL =
           (with PT_VIEW as (select SP.PEND_TRANS_ID,
                                    ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2) +
                                    DECODE(ROW_NUMBER() OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID order by SP.RATE desc,
                                                SP.PEND_TRANS_ID),
                                           1,
                                           SP.GAIN_LOSS_REVERSAL -
                                           sum(ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2))
                                           OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID),
                                           0) WITH_PLUG
                               from (select PT.PEND_TRANS_ID,
                                            PT.WORK_ORDER_NUMBER,
                                            PT.COMPANY_ID,
                                            PT.LDG_ASSET_ID,
                                            GET_PEND_SOB_GL_REVERSAL(PT.LDG_ASSET_ID,
                                                                     1,
                                                                     PT.ACTIVITY_CODE,
                                                                     PT.WORK_ORDER_NUMBER,
                                                                     PT.COMPANY_ID) as GAIN_LOSS_REVERSAL,
                                            RATIO_TO_REPORT(NVL(ABS(PT.COST_OF_REMOVAL), 0) +
                                                            NVL(ABS(PT.SALVAGE_CASH), 0) +
                                                            NVL(ABS(PT.SALVAGE_RETURNS), 0)) OVER(partition by PT.WORK_ORDER_NUMBER, PT.COMPANY_ID, PT.LDG_ASSET_ID) as RATE
                                       from PEND_TRANSACTION PT
                                      where PT.POSTING_STATUS in (1, 3)
                                        and PT.LDG_DEPR_GROUP_ID <= -1
                                        and PT.LDG_DEPR_GROUP_ID >= -20
                                        and UPPER(trim(PT.ACTIVITY_CODE)) in ('URGL', 'SAGL')) SP)
              select PV.WITH_PLUG
                from PT_VIEW PV
               where PV.PEND_TRANS_ID = PT2.PEND_TRANS_ID)
               where PT2.POSTING_STATUS in (1, 3)
                 and PT2.LDG_DEPR_GROUP_ID <= -1
                 and PT2.LDG_DEPR_GROUP_ID >= -20
                 and UPPER(trim(PT2.ACTIVITY_CODE)) in ('URGL', 'SAGL');

   -- now update gain_loss
   update PEND_TRANSACTION
      set GAIN_LOSS = -1 * ((NVL(RESERVE, 0) + POSTING_AMOUNT + NVL(SALVAGE_CASH, 0) +
                       NVL(SALVAGE_RETURNS, 0) + NVL(RESERVE_CREDITS, 0) +
                       NVL(COST_OF_REMOVAL, 0) + NVL(GAIN_LOSS_REVERSAL, 0)))
    where UPPER(trim(ACTIVITY_CODE)) in ('URGL', 'SAGL')
      and POSTING_STATUS = 1;

   -- update pend_trans_set_of_books
   update PEND_TRANSACTION_SET_OF_BOOKS PT2
      set GAIN_LOSS_REVERSAL =
           (with PT_VIEW as (select SP.PEND_TRANS_ID,
                                    SP.SET_OF_BOOKS_ID,
                                    ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2) +
                                    DECODE(ROW_NUMBER() OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID,
                                                SP.SET_OF_BOOKS_ID order by SP.RATE desc,
                                                SP.PEND_TRANS_ID),
                                           1,
                                           SP.GAIN_LOSS_REVERSAL -
                                           sum(ROUND(SP.GAIN_LOSS_REVERSAL * SP.RATE, 2))
                                           OVER(partition by SP.WORK_ORDER_NUMBER,
                                                SP.COMPANY_ID,
                                                SP.LDG_ASSET_ID,
                                                SP.SET_OF_BOOKS_ID),
                                           0) WITH_PLUG
                               from (select PT.PEND_TRANS_ID,
                                            PT3.WORK_ORDER_NUMBER,
                                            PT3.COMPANY_ID,
                                            PT3.LDG_ASSET_ID,
                                            PT.SET_OF_BOOKS_ID,
                                            GET_PEND_SOB_GL_REVERSAL(PT3.LDG_ASSET_ID,
                                                                     PT.SET_OF_BOOKS_ID,
                                                                     PT.ACTIVITY_CODE,
                                                                     PT3.WORK_ORDER_NUMBER,
                                                                     PT3.COMPANY_ID) as GAIN_LOSS_REVERSAL,
                                            RATIO_TO_REPORT(NVL(ABS(PT.ADJUSTED_COST_OF_REMOVAL), 0) +
                                                            NVL(ABS(PT.ADJUSTED_SALVAGE_CASH), 0) +
                                                            NVL(ABS(PT.ADJUSTED_SALVAGE_RETURNS), 0)) OVER(partition by PT3.WORK_ORDER_NUMBER, PT3.COMPANY_ID, PT3.LDG_ASSET_ID, PT.SET_OF_BOOKS_ID) as RATE
                                       from PEND_TRANSACTION_SET_OF_BOOKS PT, PEND_TRANSACTION PT3
                                      where PT3.POSTING_STATUS in (1, 3)
                                        and PT3.LDG_DEPR_GROUP_ID <= -1
                                        and PT3.LDG_DEPR_GROUP_ID >= -20
                                        and PT.PEND_TRANS_ID = PT3.PEND_TRANS_ID
                                        and UPPER(trim(PT.ACTIVITY_CODE)) in ('URGL', 'SAGL')) SP)
              select PV.WITH_PLUG
                from PT_VIEW PV
               where PV.PEND_TRANS_ID = PT2.PEND_TRANS_ID
                 and PV.SET_OF_BOOKS_ID = PT2.SET_OF_BOOKS_ID)
               where UPPER(trim(PT2.ACTIVITY_CODE)) in ('URGL', 'SAGL')
                 and exists (select 1
                        from PEND_TRANSACTION PT3
                       where PT3.POSTING_STATUS in (1, 3)
                         and PT3.LDG_DEPR_GROUP_ID <= -1
                         and PT3.LDG_DEPR_GROUP_ID >= -20
                         and PT2.PEND_TRANS_ID = PT3.PEND_TRANS_ID);

   -- update gain_loss
   update PEND_TRANSACTION_SET_OF_BOOKS PT_SOB
      set GAIN_LOSS = -1 * ((NVL(RESERVE, 0) + POSTING_AMOUNT + NVL(ADJUSTED_SALVAGE_CASH, 0) +
                       NVL(ADJUSTED_SALVAGE_RETURNS, 0) + NVL(ADJUSTED_RESERVE_CREDITS, 0) +
                       NVL(ADJUSTED_COST_OF_REMOVAL, 0) + NVL(GAIN_LOSS_REVERSAL, 0)))
    where UPPER(trim(ACTIVITY_CODE)) in ('URGL', 'SAGL')
      and exists (select 1
             from PEND_TRANSACTION PT
            where PT.POSTING_STATUS in (1, 3)
              and PT_SOB.PEND_TRANS_ID = PT.PEND_TRANS_ID);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (346, 0, 10, 4, 0, 0, 29411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029411_cpr_PEND_BASIS_INSERT_GNLS_REV.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;