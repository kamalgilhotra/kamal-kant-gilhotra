/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi8.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/03/2013 Brandon Beck   Point release
||============================================================================
*/

alter table LS_MONTHLY_ACCRUAL_STG add SET_OF_BOOKS_ID number(22,0);
alter table LS_PAYMENT_LINE        add SET_OF_BOOKS_ID number(22,0);

alter table LS_PAYMENT_LINE drop primary key drop index;

alter table LS_PAYMENT_LINE
   add constraint PK_LS_PAYMENT_LINE
       primary key (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID, SET_OF_BOOKS_ID)
       using index tablespace PWRPLANT_IDX;


--Set work order lookup
update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'work_order_id'
            and LONG_DESCRIPTION = 'Lessee Work Order Id')
 where COLUMN_NAME = 'work_order_id'
   and IMPORT_TEMPLATE_ID =
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where DESCRIPTION = 'Leased Asset Add');

--Map on property group description instead of long description
update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'property_group_id'
            and LOOKUP_COLUMN_NAME = 'description')
 where COLUMN_NAME = 'property_group_id'
   and IMPORT_TEMPLATE_ID =
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where DESCRIPTION = 'Leased Asset Add');

--Limit valid values on work_order lookup to prevent Excel errors
update PP_IMPORT_LOOKUP
   set LOOKUP_VALUES_ALTERNATE_SQL = 'where funding_wo_indicator = 0 and rownum < 65000'
 where COLUMN_NAME = 'work_order_id'
   and LONG_DESCRIPTION = 'Lessee Work Order Id';

--Use Lease Number instead of Lease Description
update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'lease_id'
            and LOOKUP_COLUMN_NAME = 'lease_number')
 where COLUMN_NAME = 'lease_id';

--Delete error message template fields
delete from PP_IMPORT_TEMPLATE_FIELDS where COLUMN_NAME = 'error_message';

--Make expected and economic life required
update PP_IMPORT_COLUMN
   set IS_REQUIRED = 1
 where COLUMN_NAME in ('economic_life', 'expected_life')
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');


--Foreign key from LS_LEASE_VENDOR to LS_VENDOR
alter table LS_LEASE_VENDOR
   add constraint FK_LS_LEASE_VENDOR_2
       foreign key (VENDOR_ID)
       references LS_VENDOR (VENDOR_ID);

--Foreign key from lS_LEASE_COMPANY to LS_LEASE
alter table LS_LEASE_COMPANY
   add constraint FK_LEASECO_LEASE
       foreign key (LEASE_ID)
       references LS_LEASE (LEASE_ID);

--Map on Country ID, not description (there is no description)
update PP_IMPORT_LOOKUP
   set DESCRIPTION = 'Country.Country Id', LOOKUP_COLUMN_NAME = 'country_id'
 where COLUMN_NAME = 'country_id';

--Make field ID's sequential
update PP_IMPORT_TEMPLATE_FIELDS A
   set A.FIELD_ID =
        (select count(1)
           from PP_IMPORT_TEMPLATE_FIELDS B
          where B.IMPORT_TEMPLATE_ID = A.IMPORT_TEMPLATE_ID
            and B.FIELD_ID <= A.FIELD_ID)
 where IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Use class code description, instead of long description
update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'class_code_id'
            and LOOKUP_COLUMN_NAME = 'description'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME like 'class_code_id%'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Use Leased Asset Number, not description, on Invoices
update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'ls_asset_id'
            and LOOKUP_COLUMN_NAME = 'leased_asset_number'
            and LOOKUP_VALUES_ALTERNATE_SQL is null)
 where COLUMN_NAME = 'ls_asset_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

/* PP IMPORT COLUMN LOOKUPS */
update PP_IMPORT_COLUMN_LOOKUP
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'lease_id'
            and LOOKUP_COLUMN_NAME = 'lease_number')
 where COLUMN_NAME = 'lease_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID
          from PP_IMPORT_TYPE
         where IMPORT_TABLE_NAME in ('ls_import_ilr', 'ls_import_invoice'));

update PP_IMPORT_COLUMN_LOOKUP
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where COLUMN_NAME = 'ls_asset_id'
            and LOOKUP_COLUMN_NAME = 'leased_asset_number')
 where COLUMN_NAME = 'ls_asset_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Remove "loaded" and "is modified" columns
delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME in ('loaded', 'is_modified')
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Set class_code xlates on all tables
update PP_IMPORT_COLUMN
   set IMPORT_COLUMN_NAME = replace(COLUMN_NAME, 'id', 'xlate')
 where COLUMN_NAME like 'class_code_id%'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Disable updates on default lease templates
update PP_IMPORT_TEMPLATE
   set DO_UPDATE_WITH_ADD = 0
 where IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values
   (13, 'LESSEE', 'delete_lessor', 'Delete Lessor', 1, 'ue_delete');

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values
   (14, 'LESSEE', 'init_mla', 'Initiate MLA', 1, 'ue_initMLA');

alter table LS_IMPORT_LEASE ADD(AUTO_APPROVE number(22, 0), AUTO_APPROVE_XLATE varchar2(35));

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (122, 'auto_approve', 'Auto Approve MLA?', 'auto_approve_xlate', 0, 1, 'number(22,0)', 'yes_no',
    1, 'yes_no_id');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (122, 'auto_approve', 77);
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (122, 'auto_approve', 78);


insert into PP_ANY_QUERY_CRITERIA
   (ID, SOURCE_ID, CRITERIA_FIELD, TABLE_NAME, DESCRIPTION, TIME_STAMP, USER_ID, FEEDER_FIELD,
    SUBSYSTEM, QUERY_TYPE, sql, SQL2, SQL3, SQL4, SQL5, SQL6)
   select max(ID) + 1,
          1001,
          'none',
          'Dynamic View',
          'Future Minimum Lease Payments',
          TO_DATE('2013-08-27 23:10:30', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          'none',
          'lessee',
          'user',
          'select   (select s.description from set_of_books s where s.set_of_books_id = sch2.set_of_books_id) as set_of_books,   c.description,
          (select lg.description from ls_lease_group lg where mla.lease_group_id = lg.lease_group_id) as lease_group,   (select ls.description from
          ls_lessor ls where ls.lessor_id = mla.lessor_id) as lessor,   mla.lease_number as lease_number, mla.description as lease_description,
          case when mla.pre_payment_sw = 1 then ''Prepay'' else ''Arrears'' end as prepay,   (select ig.description from ls_ilr_group ig where
          ilr.ilr_group_id = ig.ilr_group_id) as ilr_group,   ilr.ilr_number as ilr_number,    (select lct.description from ls_lease_cap_type lct
          where lct.ls_lease_cap_type_id = ilro.lease_cap_type_id) as capitalization_type,   la.description as asset_description, la.serial_number
          as serial_number,   al.ext_asset_location as ext_asset_location, al.long_description as asset_loc_description,   (select ua.description
          from utility_account ua where ua.utility_account_id = la.utility_account_id and ua.bus_segment_id = la.bus_segment_id) as utility_account,
          (select ru.description from retirement_unit ru where ru.retirement_unit_id = la.retirement_unit_id) as retirement_unit,
          nvl(td.county_id, al.county_id) as county,   nvl(td.state_id, al.state_id) as state,   td.tax_district_code as tax_district_code,
          case when sch2.the_row = 1 then la.fmv else 0 end as fair_market_value,   sch2.month_number as month_number,   case when sch2.the_row = 1
          then sch2.beg_capital_cost else 0 end as beg_capital_cost,   nvl(sch2.residual_amount, 0) as residual_amount, nvl(sch2.termination_penalty_amount, 0)
          as termination_penalty_amount,   nvl(sch2.bargain_purchase_amount, 0) as bargain_purchase_amount,   nvl(sch2.beg_obligation, 0) as beg_obligation,
          nvl(sch2.beg_lt_obligation, 0) as beg_lt_obligation,   nvl(sch2.interest_accrual, 0) as interest_accrual, nvl(sch2.principal_accrual, 0)
          as principal_accrual,   nvl(sch2.interest_paid, 0) as interest_paid, nvl(sch2.principal_paid, 0) as principal_paid  from    ls_lease mla,
          ls_lease_company lsc,   ls_ilr ilr, ls_ilr_options ilro,   ls_asset la,   (    select row_number() over( partition by sch.ls_asset_id,
          sch.revision, sch.set_of_books_id order by sch.month ) as the_row,     sch.set_of_books_id, sch.ls_asset_id, sch.revision,
          to_number(to_char(sch.month, ''yyyymm'')) as month_number,     sch.residual_amount as residual_amount, sch.term_penalty as termination_penalty_amount,
          sch.bpo_price as bargain_purchase_amount, sch.beg_capital_cost as beg_capital_cost,     sch.beg_obligation as beg_obligation, sch.beg_lt_obligation
          as beg_lt_obligation,     sch.interest_accrual as interest_accrual, sch.principal_accrual as principal_accrual,     sch.interest_paid as interest_paid,
          sch.principal_paid as principal_paid     from ls_asset_schedule sch   ) sch2,   company c,   asset_location al, prop_tax_district td  where mla.lease_id =
          lsc.lease_id  and lsc.company_id = c.company_id  and lsc.lease_id = ilr.lease_id (+)  and lsc.company_id = ilr.company_id (+)  and ilr.ilr_id = la.ilr_id (+)
          and ilr.ilr_id = ilro.ilr_id (+)  and ilr.current_revision = ilro.revision (+)  and la.ls_asset_id = sch2.ls_asset_id (+)
          and la.approved_revision = sch2.revision (+)  and la.asset_location_id = al.asset_location_id (+)  and al.tax_district_id = td.tax_district_id (+)
          order by 1, 2, 3, 5, 8, 9, 11, 21',
          null,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA;

insert into PP_ANY_QUERY_CRITERIA
   (ID, SOURCE_ID, CRITERIA_FIELD, TABLE_NAME, DESCRIPTION, TIME_STAMP, USER_ID, FEEDER_FIELD,
    SUBSYSTEM, QUERY_TYPE, sql, SQL2, SQL3, SQL4, SQL5, SQL6)
   select max(ID) + 1,
          1001,
          'none',
          'Dynamic View',
          'ILR by Set of Books (ALL Revisions)',
          TO_DATE('2013-08-27 21:53:16', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          'none',
          'lessee',
          'user',
          'select l.ilr_id as ilr_id,    ll.ilr_number as ilr_number,   l.revision as revision,    (select a.description from approval_status a, ls_ilr_approval la where la.approval_status_id = a.approval_status_id and l.ilr_id = la.ilr_id and l.revision = la.revision) as revision_status,   (select s.description from set_of_books s where l.set_of_books_id = s.set_of_books_id) as set_of_books,   c.description as company,   l.net_present_value as net_present_value, l.capital_cost as capital_cost,    l.current_lease_cost as fair_market_value,   (select count(1) from ls_asset la where la.ilr_id = l.ilr_id) as number_of_assets   from ls_ilr_amounts_set_of_books l, ls_ilr ll, company c  where l.ilr_id = ll.ilr_id  and ll.company_id = c.company_id  order by 2, 3',
          null,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA;

insert into PP_ANY_QUERY_CRITERIA
   (ID, SOURCE_ID, CRITERIA_FIELD, TABLE_NAME, DESCRIPTION, TIME_STAMP, USER_ID, FEEDER_FIELD,
    SUBSYSTEM, QUERY_TYPE, sql, SQL2, SQL3, SQL4, SQL5, SQL6)
   select max(ID) + 1,
          1001,
          'none',
          'Dynamic View',
          'ILR by Set of Books (CURRENT Revision)',
          TO_DATE('2013-08-27 21:53:55', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          'none',
          'lessee',
          'user',
          'select l.ilr_id as ilr_id,    ll.ilr_number as ilr_number,   l.revision as revision,    (select a.description from approval_status a, ls_ilr_approval la where la.approval_status_id = a.approval_status_id and l.ilr_id = la.ilr_id and l.revision = la.revision) as revision_status,   (select s.description from set_of_books s where l.set_of_books_id = s.set_of_books_id) as set_of_books,   c.description as company,   l.net_present_value as net_present_value, l.capital_cost as capital_cost,    l.current_lease_cost as fair_market_value,   (select count(1) from ls_asset la where la.ilr_id = l.ilr_id) as number_of_assets   from ls_ilr_amounts_set_of_books l, ls_ilr ll, company c  where l.ilr_id = ll.ilr_id  and ll.company_id = c.company_id  and ll.current_revision = l.revision  order by 2, 3',
          null,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA;

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'capital_cost',
          8,
          TO_DATE('2013-08-27 21:43:26', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Capital Cost',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'company',
          6,
          TO_DATE('2013-08-27 21:42:03', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Company',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'fair_market_value',
          9,
          TO_DATE('2013-08-27 21:43:26', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Fair Market Value',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'ilr_id',
          1,
          TO_DATE('2013-08-27 21:43:25', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'ILR Id',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'ilr_number',
          2,
          TO_DATE('2013-08-27 21:43:26', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'ILR Number',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'net_present_value',
          7,
          TO_DATE('2013-08-27 21:43:26', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Net Present Value',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'number_of_assets',
          10,
          TO_DATE('2013-08-27 21:43:26', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Number Of Assets',
          300,
          null,
          null,
          'NUMBER',
          1,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'revision',
          3,
          TO_DATE('2013-08-27 21:43:26', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Revision',
          150,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'revision_status',
          4,
          TO_DATE('2013-08-27 21:42:02', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Revision Status',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'set_of_books',
          5,
          TO_DATE('2013-08-27 21:53:17', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Set Of Books',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'Future Minimum Lease Payments';


insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'capital_cost',
          8,
          TO_DATE('2013-08-27 21:45:21', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Capital Cost',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'company',
          6,
          TO_DATE('2013-08-27 21:45:01', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Company',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'fair_market_value',
          9,
          TO_DATE('2013-08-27 21:45:21', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Fair Market Value',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'ilr_id',
          1,
          TO_DATE('2013-08-27 21:45:21', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'ILR Id',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'ilr_number',
          2,
          TO_DATE('2013-08-27 21:45:21', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'ILR Number',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'net_present_value',
          7,
          TO_DATE('2013-08-27 21:45:21', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Net Present Value',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'number_of_assets',
          10,
          TO_DATE('2013-08-27 21:45:22', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Number Of Assets',
          300,
          null,
          null,
          'NUMBER',
          1,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'revision',
          3,
          TO_DATE('2013-08-27 21:44:59', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Revision',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'revision_status',
          4,
          TO_DATE('2013-08-27 21:45:00', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Revision Status',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'set_of_books',
          5,
          TO_DATE('2013-08-27 21:53:56', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Set Of Books',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (ALL Revisions)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'asset_description',
          11,
          TO_DATE('2013-08-27 23:06:40', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Asset Description',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'asset_loc_description',
          14,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Asset Location',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'bargain_purchase_amount',
          25,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Bargain Purchase Amount',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'beg_capital_cost',
          22,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Beg Capital Cost',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'beg_lt_obligation',
          27,
          TO_DATE('2013-08-27 23:08:53', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Beg Lt Obligation',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'beg_obligation',
          26,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Beg Obligation',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'capitalization_type',
          10,
          TO_DATE('2013-08-27 23:06:39', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Capitalization Type',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'county',
          17,
          TO_DATE('2013-08-27 23:06:43', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'County',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'description',
          2,
          TO_DATE('2013-08-27 23:08:51', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Company',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'ext_asset_location',
          13,
          TO_DATE('2013-08-27 23:06:41', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Ext Asset Location',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'fair_market_value',
          20,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Fair Market Value',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'ilr_group',
          8,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'ILR Group',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'ilr_number',
          9,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'ILR Number',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'interest_accrual',
          28,
          TO_DATE('2013-08-27 23:08:53', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Interest Accrual',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'interest_paid',
          30,
          TO_DATE('2013-08-27 23:08:53', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Interest Paid',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'lease_description',
          6,
          TO_DATE('2013-08-27 23:06:37', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Lease Description',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'lease_group',
          3,
          TO_DATE('2013-08-27 23:06:36', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Lease Group',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'lease_number',
          5,
          TO_DATE('2013-08-27 23:06:37', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Lease Number',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'lessor',
          4,
          TO_DATE('2013-08-27 23:06:37', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Lessor',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'month_number',
          21,
          TO_DATE('2013-08-27 23:06:45', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Month Number',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'prepay',
          7,
          TO_DATE('2013-08-27 23:06:38', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Prepay',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'principal_accrual',
          29,
          TO_DATE('2013-08-27 23:08:53', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Principal Accrual',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'principal_paid',
          31,
          TO_DATE('2013-08-27 23:08:53', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Principal Paid',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'residual_amount',
          23,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Residual Amount',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'retirement_unit',
          16,
          TO_DATE('2013-08-27 23:06:42', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Retirement Unit',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'serial_number',
          12,
          TO_DATE('2013-08-27 23:06:40', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Serial Number',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'set_of_books',
          1,
          TO_DATE('2013-08-27 23:06:35', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Set Of Books',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'state',
          18,
          TO_DATE('2013-08-27 23:06:44', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'State',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'tax_district_code',
          19,
          TO_DATE('2013-08-27 23:06:44', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Tax District Code',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'termination_penalty_amount',
          24,
          TO_DATE('2013-08-27 23:08:52', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          1,
          1,
          null,
          'Termination Penalty Amount',
          300,
          null,
          null,
          'NUMBER',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

insert into PP_ANY_QUERY_CRITERIA_FIELDS
   (ID, DETAIL_FIELD, COLUMN_ORDER, TIME_STAMP, USER_ID, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
    DEFAULT_VALUE, COLUMN_HEADER, COLUMN_WIDTH, DISPLAY_FIELD, DISPLAY_TABLE, COLUMN_TYPE,
    QUANTITY_FIELD, DATA_FIELD, REQUIRED_FILTER, REQUIRED_ONE_MULT, SORT_COL)
   select ID,
          'utility_account',
          15,
          TO_DATE('2013-08-27 23:06:42', 'yyyy-mm-dd hh24:mi:ss'),
          'PWRPLANT',
          0,
          1,
          null,
          'Utility Account',
          300,
          null,
          null,
          'VARCHAR2',
          0,
          null,
          null,
          null,
          null
     from PP_ANY_QUERY_CRITERIA
    where DESCRIPTION = 'ILR by Set of Books (CURRENT Revision)';

delete from PPBASE_ACTIONS_WINDOWS
 where MODULE = 'LESSEE'
   and ACTION_IDENTIFIER = 'new_asset';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (568, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi8.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
