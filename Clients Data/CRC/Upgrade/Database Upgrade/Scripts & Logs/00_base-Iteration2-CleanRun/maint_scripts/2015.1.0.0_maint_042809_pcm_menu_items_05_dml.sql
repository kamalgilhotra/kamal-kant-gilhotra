/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042809_pcm_menu_items_05_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 	B.Beck			Modify Left Hand Navigation
||==========================================================================================
*/

delete
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier like 'jt%'
and menu_level = 4
;

delete
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier like 'jt%'
and menu_level = 3
;

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier in
(
	'jt_est_cashflows',
	'jt_maint_details',
	'jt_maint_contacts',
	'jt_maint_class_codes',
	'jt_maint_other',
	'jt_mon_dashboards',
	'jt_mon_charges',
	'jt_mon_commitments',
	'jt_maint_info',
	'jt_forecast',
	'jt_close'
);

delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier = 'rpt_bud_vs_act';

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier = 'rpt_bud_vs_act';

update ppbase_menu_items
set workspace_identifier = ''
where module = 'pcm'
and menu_identifier = 'rpt_queries';

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier = 'rpt_queries';

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'rpt_fp_queries', 3, 1, 'FP Queries', 
	'Funding Project Queries', 'rpt_queries', '', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'rpt_wo_queries', 3, 2, 'WO Queries', 
	'Work Order Queries', 'rpt_queries', '', 1
);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
select module, menu_identifier, label, 'uo_pcm_cntr_wksp_stub', minihelp, 1
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier is not null
and workspace_identifier is null
and menu_identifier not in
(
	select a.parent_menu_identifier
	from ppbase_menu_items a
	where a.module = 'pcm'
	and a.parent_menu_identifier is not null
);

update ppbase_menu_items
set workspace_identifier = menu_identifier
where module = 'pcm'
and parent_menu_identifier is not null
and workspace_identifier is null
and menu_identifier not in
(
	select a.parent_menu_identifier
	from ppbase_menu_items a
	where a.module = 'pcm'
	and a.parent_menu_identifier is not null
);

update ppbase_workspace
set label = label || ' (' || upper( substr(workspace_identifier, 1, 2) ) || ')'
where module = 'pcm'
and substr(workspace_identifier, 1, 2) in ('fp', 'wo')
and label not like '%)%'
;


delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier in ('config_close', 'config_forecast')
;

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier in ('config_close', 'config_forecast')
;

update ppbase_menu_items
set item_order = item_order + 10
where module = 'pcm'
and parent_menu_identifier = 'configuration';


update ppbase_menu_items
set item_order = 1, label = 'Dashboards', minihelp = 'Configure Dashboards'
where module = 'pcm'
and menu_identifier = 'config_monitor';

update ppbase_menu_items
set item_order = 2, label = 'Forecasting Options', minihelp = 'Configure Forecasting Options'
where module = 'pcm'
and menu_identifier = 'config_estimate';

update ppbase_menu_items
set item_order = 4, label = 'Justification', minihelp = 'Configure Justifications'
where module = 'pcm'
and menu_identifier = 'config_justify';

update ppbase_menu_items
set item_order = 5, label = 'Material Reconciliation', minihelp = 'Configure Material Reconciliation'
where module = 'pcm'
and menu_identifier = 'config_matl_rec';

update ppbase_menu_items
set item_order = 6, label = 'Metrics', minihelp = 'Configure Metrics'
where module = 'pcm'
and menu_identifier = 'config_metrics';

update ppbase_menu_items
set item_order = 7, label = 'Notifications', minihelp = 'Configure Notifications'
where module = 'pcm'
and menu_identifier = 'config_authorize';

update ppbase_menu_items
set item_order = 9, label = 'Validations', minihelp = 'Configure Dynamic Validations'
where module = 'pcm'
and menu_identifier = 'config_dyn_valid';

update ppbase_menu_items
set item_order = 11, label = 'Work Order Types', minihelp = 'Configure Work Order Types'
where module = 'pcm'
and menu_identifier = 'config_maintain';

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
(
	'pcm', 'config_fp_worksheets', 'FP Worksheets', 'uo_pcm_cntr_wksp_stub', 'Configure FP Worksheets', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'config_fp_worksheets', 2, 3, 'FP Worksheets', 
	'Configure FP Worksheets', 'configuration', 'config_fp_worksheets', 1
);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
(
	'pcm', 'config_rates', 'Rates', 'uo_pcm_cntr_wksp_stub', 'Configure Rates', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'config_rates', 2, 8, 'Rates', 
	'Configure Rates', 'configuration', 'config_rates', 1
);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
(
	'pcm', 'config_wo_worksheets', 'WO Worksheets', 'uo_pcm_cntr_wksp_stub', 'Configure WO Worksheets', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'config_wo_worksheets', 2, 10, 'WO Worksheets', 
	'Configure WO Worksheets', 'configuration', 'config_wo_worksheets', 1
);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
(
	'pcm', 'config_workflow', 'Workflows', 'uo_pcm_cntr_wksp_stub', 'Configure Workflows', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'config_workflow', 2, 12, 'Workflows', 
	'Configure Workflows', 'configuration', 'config_workflow', 1
);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2283, 0, 2015, 1, 0, 0, 042809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042809_pcm_menu_items_05_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;