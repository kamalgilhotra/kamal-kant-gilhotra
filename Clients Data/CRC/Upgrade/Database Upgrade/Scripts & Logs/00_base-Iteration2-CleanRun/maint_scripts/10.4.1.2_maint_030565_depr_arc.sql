/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030565_depr_arc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.2   09/05/2013 Charlie Shilling create table
||            09/01/2015 Sam Leach        Added code to drop public synonym
||============================================================================
*/

/* ** DKB 01/28/2015 ADDED TO DROP SYNONYM EXISTING ON DEPR_CALC_OVER_STG_ARC TABLE */
declare
   TABLE_DNE exception;
   pragma exception_init(TABLE_DNE, -942);

   V_COUNT number(22, 0) := 0;
   V_SQL   varchar2(2000) := 'select count(1) from all_synonyms where upper(synonym_name) = ''DEPR_CALC_OVER_STG_ARC''';

   C_CHECK_ROWS sys_refcursor;
begin
   open C_CHECK_ROWS for V_SQL;
   fetch C_CHECK_ROWS
      into V_COUNT;
   close C_CHECK_ROWS;


   if V_COUNT > 0 then
      execute immediate 'DROP PUBLIC SYNONYM DEPR_CALC_OVER_STG_ARC';
   else
      DBMS_OUTPUT.PUT_LINE('Public Synonym DEPR_CALC_OVER_STG_ARC does not exist. Moving forward with recreating table DEPR_CALC_OVER_STG_ARC...');
   end if;

exception
   when TABLE_DNE then
      DBMS_OUTPUT.PUT_LINE('Error dropping Public Synonym DEPR_CALC_OVER_STG_ARC');
end;
/


declare
   TABLE_DNE exception;
   pragma exception_init(TABLE_DNE, -942);

   V_COUNT number(22, 0) := 0;
   V_SQL   varchar2(2000) := 'select count(1) from DEPR_CALC_OVER_STG_ARC';

   C_CHECK_ROWS sys_refcursor;
begin
   open C_CHECK_ROWS for V_SQL;
   fetch C_CHECK_ROWS
      into V_COUNT;
   close C_CHECK_ROWS;

   if V_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'Trying to drop and recreate DEPR_CALC_OVER_STG_ARC. There are records currently in this table. Please make sure that the records are archived or are no longer needed, clear the table, and re-run the script.');
   end if;

   execute immediate 'DROP TABLE DEPR_CALC_OVER_STG_ARC';
exception
   when TABLE_DNE then
      DBMS_OUTPUT.PUT_LINE('Table DEPR_CALC_OVER_STG_ARC does not exist. Creating table now...');
end;
/

create table DEPR_CALC_OVER_STG_ARC
(
 DEPR_GROUP_ID          number(22,0) not null,
 GL_POST_MO_YR          date         not null,
 SET_OF_BOOKS_ID        number(22,0) not null,
 TYPE                   varchar2(35) null,
 END_BALANCE            number(22,2) default 0 null,
 END_RESERVE            number(22,2) default 0 null,
 EXPENSE                number(22,2) default 0 null,
 RETRO_EXPENSE          number(22,2) default 0 null,
 NET_SALVAGE_PCT        number(22,8) default 0 null,
 SIGN_BALANCE           number(22,0) null,
 SIGN_RESERVE           number(22,0) null,
 SIGN_EXPENSE           number(22,0) null,
 OVER_DEPR_AMOUNT       number(22,2) default 0 null,
 OVER_DEPR_CHECK_AMOUNT number(22,2) default 0 null
);

create index NDX_DEPR_CALC_OVER_STG_ARC
   on DEPR_CALC_OVER_STG_ARC (DEPR_GROUP_ID, GL_POST_MO_YR, SET_OF_BOOKS_ID);

comment on table DEPR_CALC_OVER_STG_ARC is '(T) [01] A global temporary table used to hold over depreciation checks during Depreciation Calculation';

comment on column DEPR_CALC_OVER_STG_ARC.DEPR_GROUP_ID is 'The internal powerplant id for the depr group going through over depreciation check';
comment on column DEPR_CALC_OVER_STG_ARC.GL_POST_MO_YR is 'The month going through over depreciation check';
comment on column DEPR_CALC_OVER_STG_ARC.SET_OF_BOOKS_ID is 'The set of books going through over depreciation check';
comment on column DEPR_CALC_OVER_STG_ARC.TYPE is 'The type of over depreciation check.  Values are LIFE, COR, COMB';
comment on column DEPR_CALC_OVER_STG_ARC.END_BALANCE is 'The ending balance to use for over depreciation check';
comment on column DEPR_CALC_OVER_STG_ARC.END_RESERVE is 'The ending reserve to use for the over depreciation check';
comment on column DEPR_CALC_OVER_STG_ARC.EXPENSE is 'The expense calculated.  This is used to determine maximum depreciation adjustment allowed if over depreciated';
comment on column DEPR_CALC_OVER_STG_ARC.RETRO_EXPENSE is 'The expense adjustment calculated as a result of retro rate change.  This is used to determine maximum depreciation adjustment allowed if over depreciated';
comment on column DEPR_CALC_OVER_STG_ARC.NET_SALVAGE_PCT is 'The net salvage percent (or cor percent if COR)';
comment on column DEPR_CALC_OVER_STG_ARC.SIGN_BALANCE is '1 if end balance is positive, -1 if negative, 0 if zero';
comment on column DEPR_CALC_OVER_STG_ARC.SIGN_RESERVE is '1 if end reserve is positive, -1 if negative, 0 if zero';
comment on column DEPR_CALC_OVER_STG_ARC.SIGN_EXPENSE is '1 if expense is positive, -1 if negative, 0 if zero';
comment on column DEPR_CALC_OVER_STG_ARC.OVER_DEPR_AMOUNT is 'The amount of the adjustment due to over depreciation';
comment on column DEPR_CALC_OVER_STG_ARC.OVER_DEPR_CHECK_AMOUNT is 'Used to store the minimum amount of expense or expense + retro adj.  Used to compare total over deprecation to the maximum allowed';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (756, 0, 10, 4, 1, 2, 30565, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_030565_depr_arc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
