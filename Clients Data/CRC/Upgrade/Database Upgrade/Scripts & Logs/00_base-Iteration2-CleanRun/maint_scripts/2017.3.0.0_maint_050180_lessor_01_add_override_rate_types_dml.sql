/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050180_lessor_01_add_override_rate_types_dml.sql
|| Description:	Add new rate types to rate type table
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/2/2018 Jared Watkins  Add the override rate types and the DF discount rate that was incorrectly removed
||============================================================================
*/

UPDATE lsr_ilr_rate_types
SET description = 'Sales Type Discount Rate'
WHERE description = 'Annual Discount Rate';

MERGE INTO lsr_ilr_rate_types a
USING ( select 8 rate_type_id, 'Sales Type Discount Rate Override' description from dual
        union all
        select 9, 'Direct Finance Discount Rate Override' from dual
        union all
        select 10, 'Direct Finance FMV Comparison Rate Override' from dual
        union all
        select 11, 'Direct Finance Interest on Net Inv Rate Override' from dual
        union all
        select 12, 'Direct Finance Discount Rate' from dual) b 
ON (LOWER(TRIM(a.description)) = LOWER(TRIM(b.description)) and a.rate_type_id = b.rate_type_id)
WHEN NOT MATCHED THEN
  INSERT(a.rate_type_id, a.description)
  VALUES(b.rate_type_id, b.description);
  
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4166, 0, 2017, 3, 0, 0, 50180, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050180_lessor_01_add_override_rate_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
