/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041461_reg_cwip_activity.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1   11/24/2014 Shane Ward        Activity Rollforward CWIP
||============================================================================
*/

/*************************
** Activity Temp Table
*************************/
CREATE GLOBAL TEMPORARY TABLE reg_hist_cwip_load_act_temp (
  reg_company_id      NUMBER(22,0) NOT NULL,
  reg_component_id    NUMBER(22,0) NOT NULL,
  reg_activity_id     NUMBER(22,0) NOT NULL,
  reg_member_id       NUMBER(22,0) NOT NULL,
  reg_member_rule_id  NUMBER(22,0) NOT NULL,
  historic_version_id NUMBER(22,0) NOT NULL,
  gl_month            NUMBER(22,1) NOT NULL,
  amount              NUMBER(22,2) NULL
)
  ON COMMIT delete ROWS;

ALTER TABLE reg_hist_cwip_load_act_temp
  ADD CONSTRAINT pk_reg_hist_cwip_load_act_temp PRIMARY KEY (
    reg_company_id,
    reg_component_id,
    reg_activity_id,
    reg_member_id,
    reg_member_rule_id,
    historic_version_id,
    gl_month
  );

COMMENT ON table reg_hist_cwip_load_act_temp IS 'Loading Temp table for CWIP Activity to Reg Activity';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.reg_company_id IS 'Standard system-assigned identifier for the regulatory company';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.reg_component_id IS 'Standard system-assigned identifier for the regulatory component';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.reg_activity_id IS 'Standard system-assigned identifier for the regulatory activity';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.reg_member_id IS 'Standard system-assigned identifier for the regulatory family member';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.reg_member_rule_id IS 'System assigned identifier of the regulatory member rule';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.historic_version_id IS 'Standard system-assigned identifier for the historic ledger';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.gl_month IS 'General Ledger Month';
COMMENT ON COLUMN reg_hist_cwip_load_act_temp.amount IS 'Amount pulled from CWIP';

/***********************************
** Component Activity Table
***********************************/
 CREATE TABLE reg_component_activity
  (reg_family_id NUMBER(22,0),
  reg_component_id NUMBER(22,0),
  reg_activity_id NUMBER(22,0),
  description     VARCHAR2(35),
  reg_status_id   NUMBER(22,0),
  reg_source_id   NUMBER(22,0),
  user_id         VARCHAR2(18),
  time_stamp      DATE);

ALTER TABLE reg_component_activity
  ADD CONSTRAINT pk_reg_component_activity PRIMARY KEY (
    reg_component_id,
    reg_activity_id,
    reg_source_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

ALTER TABLE reg_component_activity
  ADD CONSTRAINT r_reg_component_activity1 FOREIGN KEY (
    reg_family_id
  ) REFERENCES reg_family (
    reg_family_id
  );
  
comment on table reg_component_activity is 'Table to hold all component activities for each regulatory Family/Source';
comment on column reg_component_activity.reg_family_id is 'System assigned identifier of Regulatory Family';
comment on column reg_component_activity.reg_component_id is 'System assigned identifier of Reg Component for Family/Source';
comment on column reg_component_activity.reg_activity_id is 'System assigned identifier of a Reg Components Activity';
comment on column reg_component_activity.description is 'Activity Description for Component-Activity';
comment on column reg_component_activity.reg_status_id is 'Flag to show whether Activity is used (1-used, 0-not used)';
comment on column reg_component_activity.reg_source_id is 'System assigned identifier of Regulatory Source';

/***********************************
** Add Activity Flag for Component
***********************************/
ALTER TABLE reg_cwip_family_component ADD load_activity NUMBER(22,0);
comment on column reg_cwip_family_component.load_activity is 'Flag for whether component loads activity.';

/***********************************
** All things necessary to get rid
** of reg_depr_component_activity
***********************************/
--Save already existing activities from depr
INSERT INTO reg_component_activity
	(reg_family_id,
	 reg_component_id,
	 reg_activity_id,
	 description,
	 reg_status_id,
	 reg_source_id)
	SELECT 	 1,
			 reg_component_id,
			 reg_activity_id,
			 description,
			 reg_status_id,
			 1
		FROM reg_depr_component_activity;

--Drop References
ALTER TABLE reg_fcst_depr_ledger_map_act DROP CONSTRAINT r_reg_fcst_depr_ledg_map_act3;
ALTER TABLE reg_depr_ledger_map_act DROP CONSTRAINT r_reg_depr_ledger_map_act3;
ALTER TABLE reg_history_activity DROP CONSTRAINT r_reg_history_activity2;
ALTER TABLE reg_forecast_activity DROP CONSTRAINT r_reg_forecast_activity2;

--Rebuild References
ALTER TABLE reg_history_activity
  ADD CONSTRAINT r_reg_history_activity2 FOREIGN KEY (
    reg_component_id,
    reg_activity_id,
    reg_source_id
  ) REFERENCES reg_component_activity (
    reg_component_id,
    reg_activity_id,
    reg_source_id
  );

ALTER TABLE reg_forecast_activity
  ADD CONSTRAINT r_reg_forecast_activity2 FOREIGN KEY (
    reg_component_id,
    reg_activity_id,
    reg_source_id
  ) REFERENCES reg_component_activity (
    reg_component_id,
    reg_activity_id,
    reg_source_id
  );

/**************************
** Drop Depr Comp Activity
**************************/
DROP TABLE reg_depr_component_activity;

/**************************
** Index for Cwip Charge
**************************/
CREATE INDEX ix_cc_closed_monum
  ON cwip_charge (
    WORK_ORDER_ID,
    Nvl(CLOSED_MONTH_NUMBER, 999999),
    CHARGE_TYPE_ID,
    EXPENDITURE_TYPE_ID
  )
  tablespace pwrplant_idx;

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2055, 0, 2015, 1, 0, 0, 41461, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041461_reg_cwip_activity.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;