/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044287_pwrtax_3_k1_export_templates_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/08/2015 Sarah Byers  	 Add template and formula concept to K1 export
||============================================================================
*/

-- Modify new columns on TAX_K1_EXPORT_CONFIG_SAVE to be not nullable
alter table tax_k1_export_config_save modify (
k1_exp_config_save_id number(22,0) not null,
k1_exp_config_temp_id number(22,0) not null);

-- Create new primary key
alter table tax_k1_export_config_save add (
constraint tax_k1_export_config_save_pk primary key (k1_exp_config_save_id) using index tablespace pwrplant_idx);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2688, 0, 2015, 2, 0, 0, 044287, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044287_pwrtax_3_k1_export_templates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;