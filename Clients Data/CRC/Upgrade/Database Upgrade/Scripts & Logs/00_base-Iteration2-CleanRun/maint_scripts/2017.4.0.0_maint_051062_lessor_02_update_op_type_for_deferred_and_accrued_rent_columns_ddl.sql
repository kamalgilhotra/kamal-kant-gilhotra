/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051062_lessor_02_update_op_type_for_deferred_and_accrued_rent_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/29/2018 Andrew Hill      Add deferred and accrued rent columns to schedule result type
||============================================================================
*/

drop type LSR_ILR_OP_SCH_RESULT_TAB;

CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_RESULT AUTHID current_user IS OBJECT( MONTH DATE,
                                                                            interest_income_received 		 NUMBER,
                                                                            interest_income_accrued 		 NUMBER,
                                                                            interest_rental_recvd_spread NUMBER,
                                                                            begin_deferred_rev 				   NUMBER,
                                                                            deferred_rev 					       NUMBER,
                                                                            end_deferred_rev 				     NUMBER,
                                                                            begin_receivable 				     NUMBER,
                                                                            end_receivable 					     NUMBER,
                                                                            begin_lt_receivable          NUMBER,
                                                                            end_lt_receivable            NUMBER,
                                                                            initial_direct_cost          NUMBER,
                                                                            executory_accrual1           NUMBER,
                                                                            executory_accrual2           NUMBER,
                                                                            executory_accrual3           NUMBER,
                                                                            executory_accrual4           NUMBER,
                                                                            executory_accrual5           NUMBER,
                                                                            executory_accrual6           NUMBER,
                                                                            executory_accrual7           NUMBER,
                                                                            executory_accrual8           NUMBER,
                                                                            executory_accrual9           NUMBER,
                                                                            executory_accrual10          NUMBER,
                                                                            executory_paid1              NUMBER,
                                                                            executory_paid2              NUMBER,
                                                                            executory_paid3              NUMBER,
                                                                            executory_paid4              NUMBER,
                                                                            executory_paid5              NUMBER,
                                                                            executory_paid6              NUMBER,
                                                                            executory_paid7              NUMBER,
                                                                            executory_paid8              NUMBER,
                                                                            executory_paid9              NUMBER,
                                                                            executory_paid10             NUMBER,
                                                                            contingent_accrual1          NUMBER,
                                                                            contingent_accrual2          NUMBER,
                                                                            contingent_accrual3          NUMBER,
                                                                            contingent_accrual4          NUMBER,
                                                                            contingent_accrual5          NUMBER,
                                                                            contingent_accrual6          NUMBER,
                                                                            contingent_accrual7          NUMBER,
                                                                            contingent_accrual8          NUMBER,
                                                                            contingent_accrual9          NUMBER,
                                                                            contingent_accrual10         NUMBER,
                                                                            contingent_paid1             NUMBER,
                                                                            contingent_paid2             NUMBER,
                                                                            contingent_paid3             NUMBER,
                                                                            contingent_paid4             NUMBER,
                                                                            contingent_paid5             NUMBER,
                                                                            contingent_paid6             NUMBER,
                                                                            contingent_paid7             NUMBER,
                                                                            contingent_paid8             NUMBER,
                                                                            contingent_paid9             NUMBER,
                                                                            contingent_paid10            NUMBER,
                                                                            begin_deferred_rent          NUMBER,
                                                                            deferred_rent                NUMBER,
                                                                            end_deferred_rent            NUMBER,
                                                                            begin_accrued_rent           NUMBER,
                                                                            accrued_rent                 NUMBER,
                                                                            end_accrued_rent             NUMBER);
/

CREATE TYPE LSR_ILR_OP_SCH_RESULT_TAB AS TABLE OF LSR_ILR_OP_SCH_RESULT;
/

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5983, 0, 2017, 4, 0, 0, 51062, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051062_lessor_02_update_op_type_for_deferred_and_accrued_rent_columns_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;