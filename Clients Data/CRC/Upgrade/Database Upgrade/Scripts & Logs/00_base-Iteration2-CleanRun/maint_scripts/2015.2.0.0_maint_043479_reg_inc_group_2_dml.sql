/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043479_reg_inc_group_2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/17/2015 Sarah Byers    Add Inc Adj Type to the Group Table
||============================================================================
*/

-- Default reg_incremental_group.incremental_adj_type_id to 1
update reg_incremental_group
	set incremental_adj_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2604, 0, 2015, 2, 0, 0, 043479, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043479_reg_inc_group_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;