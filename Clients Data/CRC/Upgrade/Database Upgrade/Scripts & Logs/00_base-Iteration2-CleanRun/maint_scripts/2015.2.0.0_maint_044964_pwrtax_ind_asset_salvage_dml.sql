/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044964_pwrtax_ind_asset_salvage_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/21/2015 Sarah Byers    Populate subledger_type_id
||============================================================================
*/

update book_alloc_group b
	set subledger_type_id = (
		select x.subledger_type_id from (
			select a.book_alloc_group_id, min(d.subledger_type_id) subledger_type_id
			  from depr_group d, tax_utility_account a
			 where d.depr_group_id = a.depr_group_id
			 group by a.book_alloc_group_id) x
		 where x.book_alloc_group_id = b.book_alloc_group_id)
 where exists (
		select 1 from (
			select a.book_alloc_group_id, min(d.subledger_type_id) subledger_type_id
			  from depr_group d, tax_utility_account a
			 where d.depr_group_id = a.depr_group_id
			 group by a.book_alloc_group_id) x
		 where x.book_alloc_group_id = b.book_alloc_group_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2935, 0, 2015, 2, 0, 0, 44964, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044964_pwrtax_ind_asset_salvage_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;


