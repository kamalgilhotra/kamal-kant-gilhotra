/*
||============================================================================
|| Application: PowerPlant
|| File Name: maint_048002_ls_01_fcst_multicurrency_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2017.1.0.0 2017-07-06 Andrew Hill       Add columns to lease forecast for multicurrency
||============================================================================
*/

ALTER TABLE ls_forecast_version 
ADD(ls_currency_type_id NUMBER(22,0),
    currency_id         NUMBER(22,0));

ALTER TABLE ls_forecast_version 
ADD CONSTRAINT ls_forecast_version_cur_typ_fk 
FOREIGN KEY (ls_currency_type_id) 
REFERENCES ls_lease_currency_type(ls_currency_type_id);

ALTER TABLE ls_forecast_version
ADD CONSTRAINT ls_forecast_version_curncy_fk
FOREIGN KEY (currency_id) 
REFERENCES currency(currency_id);
                                               
COMMENT ON COLUMN ls_forecast_version.ls_currency_type_id IS 'Currency Type associated with this forecast version.';
COMMENT ON COLUMN ls_forecast_version.currency_id IS 'Currency associated with this forecast version.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3560, 0, 2017, 1, 0, 0, 48002, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048002_ls_01_fcst_multicurrency_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
