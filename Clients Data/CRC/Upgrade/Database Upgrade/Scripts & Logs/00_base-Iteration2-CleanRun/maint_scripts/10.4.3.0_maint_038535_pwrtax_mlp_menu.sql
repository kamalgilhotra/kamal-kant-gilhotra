/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038535_pwrtax_mlp_menu.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 08/12/2014 Anand Rajashekar    Asset Technical Termination
||========================================================================================
*/

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where MODULE = 'powertax'
   and MENU_LEVEL = 1
   and ITEM_ORDER >= 6;

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, ENABLE_YN)
values
   ('powertax', 'mlp', 1, 6, 'MLP', 1);

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'mlp_review_asset_grid', 'Asset Grid (MLP)', 'uo_tax_depr_asset_wksp_review', 'Asset Grid (MLP)');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('powertax', 'mlp_review_asset_grid', 2, 1, 'Asset Grid', 'mlp', 'mlp_review_asset_grid', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1324, 0, 10, 4, 3, 0, 38535, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038535_pwrtax_mlp_menu.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;