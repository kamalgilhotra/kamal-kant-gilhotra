/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038095_pwrtax_gui_process_salvage.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/02/2014 Andrew Scott        Base GUI workspaces for the Processing
||                                         of Salvage and COR.
||============================================================================
*/

----
----  set up the new workspace to be accessible in powertax base gui
----

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'depr_input_salvage_process', 'Process Salvage/COR', 'uo_tax_depr_act_wksp_prcs_salvage_cor',
    'Process Salvage/COR');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'depr_input_salvage_process'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'depr_input_salvage_process';

----
----  set up the dynamic filters.
----
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (67, 'PowerTax - COR/Salvage Process', 'uo_ppbase_tab_filter_dynamic');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (165, 'Book Alloc Group', 'dw', 'book_alloc_assign.book_alloc_group_id', '', 'dw_tax_depr_act_book_depr_filter',
    'book_alloc_group_id', 'description', 'N', 0, 0, 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (166, 'Tax Year (Unlocked)', 'dw', 'tax_depreciation.tax_year', '', 'dw_tax_year_unlock_by_version_filter',
    'tax_year', 'tax_year', 'D', 1, 1, 0, 0, 0);

----filter id 13 is a company id select, required, that joins into company
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (67, 13);

----filter id 165 is the newly created book alloc group id filter
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (67, 165);

----filter id 166 is the newly created tax year (unlocked) filter
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (67, 166);


----we want to make the book alloc groups displayed in the filter to
----be limited to those for the selected tax year and selected companies...
insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE)
values
   (53, 165, 166, 'utility_account_depreciation.tax_year', '');

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE)
values
   (54, 165, 13, 'tax_utility_account.company_id', '');

----
----  set up the new system option
----

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values
   ('Process Salvage/COR Spread Option Default',
    'Default spread option for the process salvage and COR tax workspace.  Option values are to either use "Real" or "Nominal" dollars for the allocation.',
    0, 'Real', 1, 0);

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Process Salvage/COR Spread Option Default', 'Real');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Process Salvage/COR Spread Option Default', 'Nominal');

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Process Salvage/COR Spread Option Default', 'powertax');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1257, 0, 10, 4, 3, 0, 38095, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038095_pwrtax_gui_process_salvage.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;