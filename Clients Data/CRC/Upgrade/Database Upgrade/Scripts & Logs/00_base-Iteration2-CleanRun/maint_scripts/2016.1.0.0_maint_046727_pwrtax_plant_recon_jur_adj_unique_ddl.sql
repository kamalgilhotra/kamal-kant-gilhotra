/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046727_pwrtax_plant_recon_jur_adj_unique_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 10/20/2016 Jared Watkins  Remove and readd the jurisdiction/adjustments
||                                      unique index so it can be seen by all users
||============================================================================
*/
begin
  execute immediate 'ALTER TABLE tax_plant_recon_form_adj ' ||
    'DROP CONSTRAINT tax_plant_recon_form_adj_ui';

  DBMS_OUTPUT.PUT_LINE('Successfully dropped tax_plant_recon_form_adj_ui constraint.');
exception
  when others then
      if sqlcode = -2443 then
        --1418 is 'specified index does not exist', which means we have already removed the index, so do nothing
        DBMS_OUTPUT.PUT_LINE('tax_plant_recon_form_adj_ui constraint was already removed. No action necessary.');
      else
        RAISE_APPLICATION_ERROR(-20000,
                                'Could not drop index tax_plant_recon_form_adj_ui. SQL Error: ' || sqlerrm);
      end if;
end;
/

begin
  execute immediate 'DROP INDEX tax_plant_recon_form_adj_ui';
  DBMS_OUTPUT.PUT_LINE('Successfully dropped index tax_plant_recon_form_adj_ui.');
exception
  when others then
      if sqlcode = -1418 then
        --1418 is 'specified index does not exist', which means we have already removed the index, so do nothing
        DBMS_OUTPUT.PUT_LINE('Index tax_plant_recon_form_adj_ui was already removed. No action necessary.');
      else
        RAISE_APPLICATION_ERROR(-20000,
                                'Could not drop index tax_plant_recon_form_adj_ui. SQL Error: ' || sqlerrm);
      end if;
end;
/

begin
  execute immediate 'ALTER TABLE tax_plant_recon_form_adj ' ||
    'ADD CONSTRAINT tax_plant_recon_form_adj_ui UNIQUE ( ' ||
    'tax_pr_form_id, ' ||
    'jurisdiction_id, ' ||
    'tax_pr_adj_id) ' ||
  'USING INDEX TABLESPACE pwrplant_idx';

  DBMS_OUTPUT.PUT_LINE('Successfully added constraint tax_plant_recon_form_adj_ui.');
exception
when others then
    if sqlcode = -2261 then
      --2261 is 'such unique or primary key already exists', which means we have already removed the index, so do nothing
      DBMS_OUTPUT.PUT_LINE('Index tax_plant_recon_form_adj_ui was already removed. No action necessary.');
    else
      RAISE_APPLICATION_ERROR(-20000,
                              'Could not drop index tax_plant_recon_form_adj_ui. SQL Error: ' || sqlerrm);
    end if;
end;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3326, 0, 2016, 1, 0, 0, 046727, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046727_pwrtax_plant_recon_jur_adj_unique_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;