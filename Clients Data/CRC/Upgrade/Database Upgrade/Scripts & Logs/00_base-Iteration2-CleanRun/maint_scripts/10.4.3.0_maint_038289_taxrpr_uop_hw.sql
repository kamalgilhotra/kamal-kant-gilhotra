/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038289_taxrpr_uop_hw.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 09/15/2014 Alex P.
||============================================================================
*/

alter table REPAIR_WORK_ORDER_SEGMENTS add HW_BASE_YEAR number(22,0);
alter table REPAIR_WORK_ORDER_SEGMENTS add HW_BASE_RATE number(22,8);
alter table REPAIR_WORK_ORDER_SEGMENTS add HW_NEW_RATE  number(22,8);

comment on column REPAIR_WORK_ORDER_SEGMENTS.HW_BASE_YEAR is 'Base year corresponding to the threshold amount when testing on cost.';
comment on column REPAIR_WORK_ORDER_SEGMENTS.HW_BASE_RATE is 'Base Handy Whitman rate for the base year.';
comment on column REPAIR_WORK_ORDER_SEGMENTS.HW_NEW_RATE is 'Handy Whitman rate for the year being processed. The ratio of hw_new_rate to hw_base_rate is used to calculate present value of the threshold.';

insert into REPAIR_PRETEST_EXPLAIN
   (ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (13, 'Handy Wtmn',
    'Repairs - Allow Non-Indexed Replace is set to NO and Handy Whitman rates could not be determined.');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1423, 0, 10, 4, 3, 0, 38289, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038289_taxrpr_uop_hw.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
