SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035688_projects.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/14/2014 Lee Quinn
||============================================================================
*/

-- was in 1148_10.4.0.0_maint_007861_projects_workflow_conversion_budg_review.sql
--**********************************************
-- ADD FIX 01082014
-- This is a copy of what is in the 1172_10.4.0.0_maint_011688_budget.sql script
--**********************************************
-- Drop the foreign key on WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'WORK_ORDER_TYPE'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;

   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/

-- Drop the foreign key on WORK_ORDER_APPROVAL > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME in ('WORK_ORDER_APPROVAL', 'WORK_ORDER_APPROVAL_ARCH')
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;
   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/

--**********************************************
-- END ADD
--**********************************************

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select *
     from (select max(CONTROL_ID) + 1 CONTROL_ID,
                  'Workflow User Object-Budg Review' CONTROL_NAME,
                  'yes' CONTROL_VALUE,
                  'dw_yes_no;1',
                  'Yes - turns on the workflow functionality for the budget review process; No - uses the old budget review process',
                  -1
             from PP_SYSTEM_CONTROL_COMPANY)
    where not exists
    (select 1
             from PP_SYSTEM_CONTROL_COMPANY
            where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object-Budg Review'));

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'yes'
 where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object-Budg Review');

update PP_MYPP_APPROVAL_CONTROL
   set DW = 'dw_budget_review_workflow_grid'
 where DW = 'dw_budget_review_grid';

-- **********************************************************************************
-- Was in 1149_10.4.0.0_maint_007861_projects_workflow_conversion_wo_fp_approvals.sql
insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select *
     from (select max(CONTROL_ID) + 1 CONTROL_ID,
                  'Workflow User Object' CONTROL_NAME,
                  'yes' CONTROL_VALUE,
                  'dw_yes_no;1',
                  '"Yes" will activate the Approval Workflow User Object; "No" forces the existing Approvals to be used',
                  -1
             from PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PP_SYSTEM_CONTROL_COMPANY
            where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object'));

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'yes'
 where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object');

update PP_MYPP_APPROVAL_CONTROL
   set DW = 'dw_fp_approval_workflow_count_mypp'
 where DW = 'dw_fp_approval_count_mypp';

update PP_MYPP_APPROVAL_CONTROL
   set DW = 'dw_wo_approval_workflow_count_mypp'
 where DW = 'dw_wo_approval_count_mypp';

-- 01242014
insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select *
     from (select max(CONTROL_ID) + 1 CONTROL_ID,
                  'Workflow User Object - Bud Item' CONTROL_NAME,
                  'yes' CONTROL_VALUE,
                  'dw_yes_no;1',
                  '"Yes" will activate the Approval Workflow User Object; "No" forces the existing Approvals to be used',
                  -1
             from PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PP_SYSTEM_CONTROL_COMPANY
            where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object - Bud Item'));

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'yes'
 where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object - Bud Item');

-- Drop the foreign key on BUDGET_APPROVAL > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME in ('BUDGET_APPROVAL', 'BUDGET_APPROVAL_ARCH')
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;
   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/

-- Drop the foreign key on WORK_ORDER_APPROVAL > APPROVAL_TYPE_ID referencing APPROVAL
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME in ('WORK_ORDER_APPROVAL', 'WORK_ORDER_APPROVAL_ARCH')
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'APPROVAL'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;
   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > APPROVAL_TYPE_ID referencing APPROVAL doesn''t exist.');
   end if;
end;
/

-- Drop the foreign key on WORK_ORDER_TYPE > APPROVAL_TYPE_ID referencing APPROVAL
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'WORK_ORDER_TYPE'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'APPROVAL'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;

   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > APPROVAL_TYPE_ID referencing APPROVAL doesn''t exist.');
   end if;
end;
/

-- Drop the foreign key on BUDGET_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'BUDGET_TYPE'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;

   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, BUDGET_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/

update PP_MYPP_APPROVAL_CONTROL
   set DW = 'dw_budget_approval_workflow_grid'
 where LOWER(trim(DW)) = 'dw_budget_approval_grid';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (856, 0, 10, 4, 2, 0, 35688, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035688_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;