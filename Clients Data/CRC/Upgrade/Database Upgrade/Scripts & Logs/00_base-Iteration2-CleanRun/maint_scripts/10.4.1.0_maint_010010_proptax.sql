/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010010_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.4.1.0    06/20/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Add Property Tax workspaces.
--
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_approval', sysdate, user, 'Approvals', 'uo_ptc_accrcntr_wksp_approve', 'Approvals' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_control', sysdate, user, 'Accrual Control', 'uo_ptc_accrcntr_wksp_control', 'View Accrual Control' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_custom_release', sysdate, user, 'Custom JE Release', 'uo_ptc_accrcntr_wksp_custom_release_top', 'Custom Journal Entry Release' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_liability', sysdate, user, 'Calc Liability', 'uo_ptc_accrcntr_wksp_est_liab', 'Calculate Liabilities' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_review', sysdate, user, 'Journal Entry Review', 'uo_ptc_accrcntr_wksp_review', 'Review Journal Entries and Calculations' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_search_accruals', sysdate, user, 'Accruals', 'uo_ptc_accrcntr_wksp_search_accruals', 'View Accrual Calculations' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_search_actuals', sysdate, user, 'Actuals', 'uo_ptc_accrcntr_wksp_search_actuals', 'View Actuals' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_search_estimates', sysdate, user, 'Estimates', 'uo_ptc_accrcntr_wksp_search_estimates', 'View Process Estimates' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'accrual_tree', sysdate, user, 'Accrual Type Mapping', 'uo_ptc_accrcntr_wksp_tree', 'View / Edit Accounting Mapping' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_allocations', sysdate, user, 'Allocation Statistics', 'uo_ptc_admincntr_wksp_allocations', 'Statistics Maintenance' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_approvals', sysdate, user, 'Approvers', 'uo_ptc_admincntr_wksp_approval', 'Update Approvers' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_auth_dist', sysdate, user, 'Auth Dist Maint', 'uo_ptc_admincntr_wksp_authdist', 'Authority District Maintenance' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_case_setup', sysdate, user, 'Case Setup', 'uo_ptc_admincntr_wksp_case_setup', 'Case Setup' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_escalation_indices', sysdate, user, 'Escalation Indices', 'uo_ptc_admincntr_wksp_esc_values', 'Update Escalation Indices' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_import', sysdate, user, 'Import', 'uo_ptc_admincntr_wksp_import', 'Import Data' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_interfaces', sysdate, user, 'Interfaces', 'uo_ptc_admincntr_wksp_interfaces', 'Run Interfaces' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_locations', sysdate, user, 'Locations', 'uo_ptc_admincntr_wksp_loc_maint', 'Update Location Mappings' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_processes', sysdate, user, 'Process Steps', 'uo_ptc_admincntr_wksp_process_setup', 'Process Steps for Returns Center' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_ptco_setup', sysdate, user, 'PT Company Setup', 'uo_ptc_admincntr_wksp_ptco_setup', 'Prop Tax Company Setup' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_rollups', sysdate, user, 'Tax Type Rollups', 'uo_ptc_admincntr_wksp_rollups', 'Type Rollups Maintenance' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_rpt_fields', sysdate, user, 'Report Fields', 'uo_ptc_admincntr_wksp_rpt_fields', 'Report Field Maintenance' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_rsv_factors', sysdate, user, 'Reserve Factors', 'uo_ptc_admincntr_wksp_rsv_fctrs', 'Update Reserve Factors' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_systemoptions', sysdate, user, 'System Options', 'uo_ptc_admincntr_wksp_system_options', 'Access System Options' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_table_maint', sysdate, user, 'Table Maintenance', 'uo_ptc_admincntr_wksp_table_maint', 'Update Table Data' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_tax_types', sysdate, user, 'Tax Types', 'uo_ptc_admincntr_wksp_tax_types', 'Update Tax Types' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_tax_year_setup', sysdate, user, 'Tax Year Setup', 'uo_ptc_admincntr_wksp_tax_year_setup', 'Tax Year Setup' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'admin_unit_cost', sysdate, user, 'Unit Cost Amounts', 'uo_ptc_admincntr_wksp_unit_cost', 'Update Unit Cost Amounts' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'bills_groups', sysdate, user, 'Groups', 'uo_ptc_billcntr_wksp_groups', 'Add / Update Statement Groups' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'bills_levyrates', sysdate, user, 'Levy Rates', 'uo_ptc_billcntr_wksp_levyrates', 'Add / Update Levy Rates' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'bills_schedules', sysdate, user, 'Schedules', 'uo_ptc_billcntr_wksp_schedules', 'Add / Update Schedules' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'bills_search', sysdate, user, 'Search', 'uo_ptc_billcntr_wksp_search', 'Search for Bills' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'data_run', sysdate, user, 'Run', 'uo_ptc_valcntr_wksp_scenario_run', 'Run Scenarios' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'data_scenario', sysdate, user, 'Scenarios', 'uo_ptc_valcntr_wksp_scenario_search', 'View Scenario Results' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'data_templates', sysdate, user, 'Templates', 'uo_ptc_valcntr_wksp_template', 'Update Templates' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'data_vault', sysdate, user, 'Vault', 'uo_ptc_valcntr_wksp_vault_search', 'View / Edit Vault Values' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'ledger_postallo', sysdate, user, 'Search Ledger', 'uo_ptc_ledgcntr_wksp_postallo_search', 'View / Edit Ledger' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'ledger_postallo_copy', sysdate, user, 'Copy Ledger Activity', 'uo_ptc_ledgcntr_wksp_copy_activity', 'Copy Ledger Activity to Current Year' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'ledger_preallo', sysdate, user, 'Search Preallo Ledger', 'uo_ptc_ledgcntr_wksp_preallo_search', 'View / Edit Preallo Ledger' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'ledger_preallo_copy', sysdate, user, 'Copy Preallo Ledger Activity', 'uo_ptc_ledgcntr_wksp_copy_activity', 'Copy Preallo Activity to Current Year' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'ledger_recalc', sysdate, user, 'Recalculate', 'uo_ptc_ledgcntr_wksp_calc_processes', 'Recalculate Ledger Item Values' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'parcel_assessment_groups', sysdate, user, 'Assessment Groups', 'uo_ptc_prclcntr_wksp_assessments', 'Assessment Group Setup' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'parcel_geography_factors', sysdate, user, 'Geography Factors', 'uo_ptc_prclcntr_wksp_geo_fctrs', 'Geography Type Factors' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'parcel_search', sysdate, user, 'Search', 'uo_ptc_prclcntr_wksp_search', 'Search for Parcels' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'payment_home', sysdate, user, 'Home', 'uo_ptc_pymtcntr_wksp_home', 'View Payment Center Home' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'payment_search', sysdate, user, 'Search', 'uo_ptc_pymtcntr_wksp_search', 'Search for Payments' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'reporting_electronic', sysdate, user, 'Electronic Filings', 'uo_ptc_rptcntr_wksp_report', 'Generate Electronic Filings' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'reporting_query', sysdate, user, 'Query', 'uo_ptc_rptcntr_wksp_query', 'Launch the Query Tool' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'reporting_rendition', sysdate, user, 'Returns', 'uo_ptc_rptcntr_wksp_report', 'Generate Returns' );
insert into PPBASE_WORKSPACE ( MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP ) values ( 'proptax', 'reporting_report', sysdate, user, 'Reports', 'uo_ptc_rptcntr_wksp_report', 'Run Standard Reports' );

--
-- Add Property Tax menu items.
--
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_center', sysdate, user, 1, 1, 'Accrual Center', 'Generate and Book Accruals', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_center', sysdate, user, 1, 2, 'Admin Center', 'Tables and Admin Functionality', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'bills_center', sysdate, user, 1, 3, 'Bills Center', 'Process and Pay Bills', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'data_center', sysdate, user, 1, 4, 'Data Center', 'Run Data Analysis', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_center', sysdate, user, 1, 5, 'Ledger Center', 'View / Modify Ledger Items', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'parcel_center', sysdate, user, 1, 6, 'Parcel Center', 'View / Modify Parcels', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'payment_center', sysdate, user, 1, 7, 'Payments Center', 'Approve, Release, and View Payments', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'reporting_center', sysdate, user, 1, 8, 'Reporting Center', 'Run Reports, Returns, and Queries', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'returns_center', sysdate, user, 1, 9, 'Returns Center', 'Process and Print Returns', '', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_tree', sysdate, user, 2, 1, 'Accrual Type Mapping', 'View / Edit Accounting Mapping', 'accrual_center', 'accrual_tree', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_process', sysdate, user, 2, 2, 'Processing', 'Process Estimates and Accruals', 'accrual_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_search', sysdate, user, 2, 3, 'Search', 'Search', 'accrual_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_table_maint', sysdate, user, 2, 1, 'Table Maintenance', 'Update Table Data', 'admin_center', 'admin_table_maint', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_setup', sysdate, user, 2, 2, 'Setup', 'Setup Companies and Years', 'admin_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_types', sysdate, user, 2, 3, 'Property Tax Types', 'Maintain Property Tax Types', 'admin_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_geography', sysdate, user, 2, 4, 'Geography', 'Maintain Locations, Districts, and Authorities', 'admin_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_factors', sysdate, user, 2, 5, 'Factors', 'Maintain Calculation Factors', 'admin_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_import', sysdate, user, 2, 6, 'Import', 'Import Data', 'admin_center', 'admin_import', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_approvals', sysdate, user, 2, 7, 'Approvers', 'Update Approvers', 'admin_center', 'admin_approvals', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_rpt_fields', sysdate, user, 2, 8, 'Report Fields', 'Report Field Maintenance', 'admin_center', 'admin_rpt_fields', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_interfaces', sysdate, user, 2, 9, 'Interfaces', 'Run Interfaces', 'admin_center', 'admin_interfaces', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_systemoptions', sysdate, user, 2, 10, 'System Options', 'Access System Options', 'admin_center', 'admin_systemoptions', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'bills_config', sysdate, user, 2, 1, 'Configuration', 'Maintain Rates, Schedules, and Groups', 'bills_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'bills_search', sysdate, user, 2, 2, 'Search', 'Search for Bills', 'bills_center', 'bills_search', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'data_search', sysdate, user, 2, 1, 'Search', 'Search for Data', 'data_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'data_templates', sysdate, user, 2, 2, 'Templates', 'Update Templates', 'data_center', 'data_templates', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'data_run', sysdate, user, 2, 3, 'Run', 'Run Scenarios', 'data_center', 'data_run', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_search', sysdate, user, 2, 1, 'Search', 'Search for Ledger Items', 'ledger_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_copy', sysdate, user, 2, 2, 'Copy Activity', 'Copy Activity to Current Year', 'ledger_center', '', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_recalc', sysdate, user, 2, 3, 'Recalculate', 'Recalculate Ledger Item Values', 'ledger_center', 'ledger_recalc', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'parcel_search', sysdate, user, 2, 1, 'Search', 'Search for Parcels', 'parcel_center', 'parcel_search', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'parcel_assessment_groups', sysdate, user, 2, 2, 'Assessment Groups', 'Assessment Group Setup', 'parcel_center', 'parcel_assessment_groups', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'parcel_geography_factors', sysdate, user, 2, 3, 'Geography Factors', 'Geography Type Factors', 'parcel_center', 'parcel_geography_factors', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'payment_home', sysdate, user, 2, 1, 'Home', 'View Payment Center Home', 'payment_center', 'payment_home', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'payment_search', sysdate, user, 2, 2, 'Search', 'Search for Payments', 'payment_center', 'payment_search', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'reporting_report', sysdate, user, 2, 1, 'Reports', 'Run Standard Reports', 'reporting_center', 'reporting_report', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'reporting_rendition', sysdate, user, 2, 2, 'Returns', 'Generate Returns', 'reporting_center', 'reporting_rendition', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'reporting_electronic', sysdate, user, 2, 3, 'Electronic Filings', 'Generate Electronic Filings', 'reporting_center', 'reporting_electronic', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'reporting_query', sysdate, user, 2, 4, 'Query', 'Launch the Query Tool', 'reporting_center', 'reporting_query', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_liability', sysdate, user, 3, 1, 'Calc Liability', 'Calculate Liabilities', 'accrual_process', 'accrual_liability', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_control', sysdate, user, 3, 2, 'Accrual Control', 'View Accrual Control', 'accrual_process', 'accrual_control', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_review', sysdate, user, 3, 3, 'Calc/Journal Review', 'Review Journals and Calculations', 'accrual_process', 'accrual_review', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_approval', sysdate, user, 3, 4, 'Approvals', 'Approvals', 'accrual_process', 'accrual_approval', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_search_estimates', sysdate, user, 3, 1, 'Estimates', 'View / Process Estimates', 'accrual_search', 'accrual_search_estimates', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_search_actuals', sysdate, user, 3, 2, 'Actuals', 'View Actuals', 'accrual_search', 'accrual_search_actuals', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'accrual_search_accruals', sysdate, user, 3, 3, 'Accruals', 'View Accrual Calculations', 'accrual_search', 'accrual_search_accruals', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_rsv_factors', sysdate, user, 3, 1, 'Reserve Factors', 'Update Reserve Factors', 'admin_factors', 'admin_rsv_factors', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_escalation_indices', sysdate, user, 3, 2, 'Escalation Indices', 'Update Escalation Indices', 'admin_factors', 'admin_escalation_indices', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_unit_cost', sysdate, user, 3, 3, 'Unit Cost Amounts', 'Update Unit Cost Amounts', 'admin_factors', 'admin_unit_cost', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_locations', sysdate, user, 3, 1, 'Locations', 'Update Location Mappings', 'admin_geography', 'admin_locations', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_auth_dist', sysdate, user, 3, 2, 'Auth Dist Maint', 'Authority District Maintenance', 'admin_geography', 'admin_auth_dist', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_ptco_setup', sysdate, user, 3, 1, 'PT Company Setup', 'Prop Tax Company Setup', 'admin_setup', 'admin_ptco_setup', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_tax_year_setup', sysdate, user, 3, 2, 'Tax Year Setup', 'Tax Year Setup', 'admin_setup', 'admin_tax_year_setup', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_case_setup', sysdate, user, 3, 3, 'Case Setup', 'Case Setup', 'admin_setup', 'admin_case_setup', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_processes', sysdate, user, 3, 4, 'Process Steps', 'Process Steps for Returns Center', 'admin_setup', 'admin_processes', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_tax_types', sysdate, user, 3, 1, 'Tax Types', 'Update Tax Types', 'admin_types', 'admin_tax_types', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_rollups', sysdate, user, 3, 2, 'Tax Type Rollups', 'Type Rollups Maintenance', 'admin_types', 'admin_rollups', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'admin_allocations', sysdate, user, 3, 3, 'Allocation Statistics', 'Statistics Maintenance', 'admin_types', 'admin_allocations', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'bills_levyrates', sysdate, user, 3, 1, 'Levy Rates', 'Add / Update Levy Rates', 'bills_config', 'bills_levyrates', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'bills_schedules', sysdate, user, 3, 2, 'Schedules', 'Add / Update Schedules', 'bills_config', 'bills_schedules', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'bills_groups', sysdate, user, 3, 3, 'Groups', 'Add / Update Statement Groups', 'bills_config', 'bills_groups', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'data_vault', sysdate, user, 3, 1, 'Vault', 'View / Edit Vault Values', 'data_search', 'data_vault', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'data_scenario', sysdate, user, 3, 2, 'Scenarios', 'View Scenario Results', 'data_search', 'data_scenario', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_preallo_copy', sysdate, user, 3, 1, 'Copy Preallo Ledger Activity', 'Copy Preallo Activity to Current Year', 'ledger_copy', 'ledger_preallo_copy', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_postallo_copy', sysdate, user, 3, 2, 'Copy Ledger Activity', 'Copy Ledger Activity to Current Year', 'ledger_copy', 'ledger_postallo_copy', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_preallo', sysdate, user, 3, 1, 'Search Preallo Ledger', 'View / Edit Preallo Ledger', 'ledger_search', 'ledger_preallo', 1 );
insert into PPBASE_MENU_ITEMS ( MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN ) values ( 'proptax', 'ledger_postallo', sysdate, user, 3, 2, 'Search Ledger', 'View / Edit Ledger', 'ledger_search', 'ledger_postallo', 1 );

--
-- Delete system options no longer needed or that have become base.
--
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Accrual Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Admin Center - Import - Templates - Allow Modifications to Used Templates';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Admin Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Bills Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Data Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Ledger Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Parcel Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Payments Center - Default Workspace';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Payments Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Reporting Center - Default Workspace';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Reporting Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Returns Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Special - Happy Assessor - Picture';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Special - Play Games - User(s)';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Special - Slapped Assessor - Picture';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Include Blank Reports Default';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Save as Separate PDF Files Default';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Send as Separate Print Jobs Default';
delete from PTC_SYSTEM_OPTIONS_CENTERS where SYSTEM_OPTION_ID = 'Reports - Emailing - Default Subject';

delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Accrual Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Admin Center - Import - Templates - Allow Modifications to Used Templates';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Admin Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Bills Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Data Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Ledger Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Parcel Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Payments Center - Default Workspace';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Payments Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Reporting Center - Default Workspace';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Reporting Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Returns Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Special - Happy Assessor - Picture';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Special - Play Games - User(s)';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Special - Slapped Assessor - Picture';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Include Blank Reports Default';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Save as Separate PDF Files Default';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Send as Separate Print Jobs Default';
delete from PTC_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID = 'Reports - Emailing - Default Subject';

delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Accrual Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Admin Center - Import - Templates - Allow Modifications to Used Templates';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Admin Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Bills Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Data Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Ledger Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Parcel Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Payments Center - Default Workspace';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Payments Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Reporting Center - Default Workspace';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Reporting Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Returns Center - Initial Menu State';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Special - Happy Assessor - Picture';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Special - Play Games - User(s)';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Special - Slapped Assessor - Picture';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Include Blank Reports Default';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Save as Separate PDF Files Default';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Reporting Center - Packages - Send as Separate Print Jobs Default';
delete from PTC_SYSTEM_OPTIONS where SYSTEM_OPTION_ID = 'Reports - Emailing - Default Subject';

--
-- Convert PTC system options to PPBASE system options.
--
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
   select SYSTEM_OPTION_ID,
          sysdate,
          user,
          LONG_DESCRIPTION,
          SYSTEM_ONLY,
          PP_DEFAULT_VALUE,
          OPTION_VALUE,
          IS_BASE_OPTION
     from PTC_SYSTEM_OPTIONS;

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID)
   select SYSTEM_OPTION_ID, OPTION_VALUE, sysdate, user from PTC_SYSTEM_OPTIONS_VALUES;

insert into PPBASE_USER_OPTIONS
   (object, OPTION_IDENTIFIER, USERS, TIME_STAMP, USER_ID, USER_OPTION, USER_OPTION_B,
    USER_OPTION_C, USER_OPTION_D)
   select object,
          OPTION_IDENTIFIER,
          USERS,
          sysdate,
          user,
          USER_OPTION,
          USER_OPTION_B,
          USER_OPTION_C,
          USER_OPTION_D
     from PTC_USER_OPTIONS
    where object in ('Bills Center.Search.Details', 'Bills Center.Search', 'Returns Center');

insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Release JE - Custom Release Workspace', 'accrual_control', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accruals - Spread True-up Over Remaining Months', 'accrual_control', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Delimiter', 'accrual_search_accruals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Fields', 'accrual_search_accruals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Default Grid View', 'accrual_search_accruals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Initial Row Selection Status', 'accrual_search_accruals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Delimiter', 'accrual_search_actuals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Fields', 'accrual_search_actuals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Actuals - Default Grid View', 'accrual_search_actuals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Actuals - Default Payment Status Selections', 'accrual_search_actuals', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Estimates - Default Estimates Source', 'accrual_search_estimates', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Estimates - Initial Row Selection Status', 'accrual_search_estimates', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Copy Full - Overwrite Default', 'admin_allocations', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Copy Incremental - Overwrite Default', 'admin_allocations', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Default Statistic Option', 'admin_allocations', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Initial Row Selection Status', 'admin_allocations', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Case Maintenance - Allow Case Deletion', 'admin_case_setup', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Allow Asset System Data Imports', 'admin_import', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Apply Edits Option', 'admin_import', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Review Option', 'admin_import', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Row Display', 'admin_import', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Display Tax District on Asset Location', 'admin_locations', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Restrict Parcels by County', 'admin_locations', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Tax Type Rollups - Default Show Active Only Option', 'admin_rollups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Tax Type Rollups - Require State Parameter', 'admin_rollups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Tax Type Rollups - Require Type Rollup Parameter', 'admin_rollups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Types - Default Show Active Only Option', 'admin_tax_types', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Types - Initial Row Selection Status', 'admin_tax_types', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Unspecified State Definition', 'admin_tax_types', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Allow Payee Changes to Vouchered Statement Groups', 'bills_groups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Statement Group Uniqueness Columns', 'bills_groups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Groups - Number of Statement Years to Display Schedules', 'bills_groups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Levy Rates - Copy - Overwrite Default', 'bills_levyrates', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Levy Rates - Default Display Format', 'bills_levyrates', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Database - Levy Rates Copy - Use Temp ID', 'bills_levyrates', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Allow Changes to Assessments for Paid Bills', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Automatically Calculate Assessed Values from Input Value', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Assessment in Grid', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Equalized Value in Grid', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Taxable Value in Grid', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Value Column Order', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Allow Deletion of Received Bills', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Allow Parcel / Authority / Levy Class Combination on Multiple Bills', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Authority Can Only Belong to One Vendor', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Authority Can Only Belong to One Vendor - Exceptions', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Automatically Generate Authority / District Relationships', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills - Warn Before Deleting Bills', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Default Verified Status', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Installment for Negative Rounding Differences', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Installment for Positive Rounding Differences', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Restrict Authorities by County Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Restrict Parcel by County Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Bill Lines Assessment by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Bill Lines Taxable Value by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Due Date Overrides by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy First Installment Indicator by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy No Longer Valid Bills Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Notes by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Refund Installments by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Reimbursement Installments by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Statement Numbers by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Supplemental Installments by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Tax Amounts by Default', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Weekend Due Date Default Option', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Levy Rates - Default Display Format', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Allow Manual Updating to Fully Paid', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Allow Statement Groups Changes On No Longer Valid Bills', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Allow Statement Groups Changes On Received Bills', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Automatically Save On Opening Voucher Window', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Display Missing Assessments Warning on Save', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - F2 Functionality', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Initial Row Selection Status', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Percent Threshold for Row Coloring Notification', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Place to Refresh Calculated Amount', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Bills Center - Voucher - Apply Sort From Bill Entry Grid to Voucher Window', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Database - Bills Center - Bill Entry - Separate Session to Add Composite', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - AP Interface - Send to AP Amount Verification', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Allow Payment Dates Prior to Current Date', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Allow Vouchering of Bills With No Active Ledger Items', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Default Scheduled Pay Date', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Default Scheduled Pay Date - Backup Days', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Default Special Instructions', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Preview Voucher Before Saving Dataobject', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Require Assessments for All Parcels Being Vouchered', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Vouchering Completed Dataobject', 'bills_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Scenario - Default Results Display', 'data_scenario', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Vault - Default Results Display', 'data_vault', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Adjust - Allow Adjustments for $0 Balances', 'ledger_postallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Adjust/Transfer - Default Auto-Calc Reserve Option', 'ledger_postallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Transfer - Allow Zero Dollar Transfers', 'ledger_postallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Class Code Value1', 'ledger_postallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Class Code Value2', 'ledger_postallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Location', 'ledger_postallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Parcel', 'ledger_postallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Adjust - Allow Adjustments for $0 Balances', 'ledger_preallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Transfer - Allow Zero Dollar Transfers', 'ledger_preallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Class Code Value1', 'ledger_preallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Class Code Value2', 'ledger_preallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Location', 'ledger_preallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Parcel', 'ledger_preallo', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Database - Temp Parcels Asmt Groups - Minimum Number of Parcel-Assessment Group Combos in List for Temp Parcels Asmt Groups Use', 'parcel_assessment_groups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Assessment Groups - Setup - Copy Equalization Factors Default', 'parcel_assessment_groups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Assessment Groups - Setup - Copy Taxable Value Rates Default', 'parcel_assessment_groups', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Allow Changes to Assessments for Paid Bills', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Automatically Calculate Assessed Values from Input Value', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Assessment in Grid', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Cost Approach Value in Grid', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Equalized Value in Grid', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Market Value in Grid', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Prop Tax Balance in Grid', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Quantity Factored in Grid', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Taxable Value in Grid', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Restrict Assessment Entry by Ledger Items', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Value Column Order', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Database - Temp Parcels - Minimum Number of Parcels in List for Temp Parcels Use', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Copy Authority-District Relationships Default', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Copy Equalization Factors Default', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Copy Taxable Value Rates Default', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Overwrite Default', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Details - Assessments - Show Only Cases with Values', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Details - Auto-Save Changes When Changing Records', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Assessments - Default Case Compare Type', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Assessments - Default Case Compare Year', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Autosize Row Height', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Flex Field Labels - Append Flex Field Number to Label', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Initial Row Selection Status', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcels - Allow Deletes', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Parcels - Parcel Uniqueness Columns', 'parcel_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - AP Interface - Send to AP Amount Verification', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Approvals - Payment Approver Can Also Release', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Approvals - Payment Enterer Can Also Approve', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Warn Before Rejecting Payments', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Warn Before Voiding Payments', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Current Payments - Default Begin Date', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Current Payments - Default End Date', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Current Payments - Retrieve Automatically On First Open', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default View', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Allow Any User to Select Rows', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Approval Report Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Auto-Refresh on Entry', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Default Enterer', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Only Show My Approvals', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Auto-Refresh on Entry', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Default Due By Date', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Default PT Company', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Default State', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Don''t Send to AP - Release Completed Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Don''t Send to AP - Release Completed Dataobject 2', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Send to AP - Release Completed Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Send to AP - Release Completed Dataobject 2', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Warn On Release (Don''t Send to AP)', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Default Due By Date', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Default PT Company', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Default State', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Retrieve Automatically On First Open', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Delimiter', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Fields', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Voiding - Allow Voiding of Paid Payments', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Voiding - Allow Voiding of Sent to AP Payments', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments - Warn Before Voiding Payments', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Search - Allow Manual Status Change from Sent to AP to Paid', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Search - Allow Multiple Voids At Once', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Display Tax District on Asset Location', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Restrict Parcels by County', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Database - Allocation - Location - Use With Clause for Temp Allo Percents', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger - Class Code Value 1', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Ledger - Class Code Value 2', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Assign - Delete by State Table', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Creation - Assets to Use', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Default Refresh Off Option', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Vintage Range Option', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns - CWIP Pseudo Tree - Default Refresh Off Option', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns - CWIP Tree - Creation - Include RWIP', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns - CWIP Tree - Default Refresh Off Option', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Ignore Prop Tax Locations for State Allocations (CPR)', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Ignore Prop Tax Locations for State Allocations (CWIP)', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Treat Missing Depr Factors As Zeros', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Unspecified State Definition', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation Audit - Filter to Show Errors', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Copy Stats - Copy Option Selected by Default', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - CPR Tree - Print - Report Type', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - CWIP Pseudo Tree - Print - Report Type', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - CWIP Tree - Print - Report Type', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - PT Loc/County Audits - Auto-Create Prop Tax Locations - Location Rollup Creation Option', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - PT Loc/County Audits - Enable Property Tax Location Dropdown', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Check Request for Un-Approved Payments', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Check Request for Un-Released Payments', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Validation Report for Sent To AP/Paid Payments', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Analysis Report Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Attachment Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Check Request Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Labels Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - USPS Forms Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Validation Dataobject', 'payment_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Check Request for Un-Approved Payments', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Check Request for Un-Released Payments', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Validation Report for Sent To AP/Paid Payments', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Analysis Report Dataobject', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Attachment Dataobject', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Check Request Dataobject', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Labels Dataobject', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - USPS Forms Dataobject', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Validation Dataobject', 'payment_search', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Auto-Retrieve Steps if All Paramaters Populated on Open', 'returns_home', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_WKSP ( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID ) values ( 'Returns Center - Remember Last Used Prop Tax Company / State / Tax Year', 'returns_home', sysdate, user );

--
-- Relate the Property Tax system options to the module.
--
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Delimiter', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accounting - GL Account - External Account Code Fields', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Default Grid View', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Accrual Search - Initial Row Selection Status', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Actuals - Default Grid View', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Actuals - Default Payment Status Selections', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Estimates - Default Estimates Source', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Estimates - Initial Row Selection Status', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accrual Center - Release JE - Custom Release Workspace', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Accruals - Spread True-up Over Remaining Months', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Copy Full - Overwrite Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Copy Incremental - Overwrite Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Default Statistic Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Allocation Stats - Initial Row Selection Status', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Case Maintenance - Allow Case Deletion', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Allow Asset System Data Imports', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Apply Edits Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Review Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Import - Default Row Display', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Display Tax District on Asset Location', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Locations - Restrict Parcels by County', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Tax Type Rollups - Default Show Active Only Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Tax Type Rollups - Require State Parameter', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Tax Type Rollups - Require Type Rollup Parameter', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Types - Default Show Active Only Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Admin Center - Types - Initial Row Selection Status', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Allow Changes to Assessments for Paid Bills', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Automatically Calculate Assessed Values from Input Value', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Assessment in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Cost Approach Value in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Equalized Value in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Market Value in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Prop Tax Balance in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Quantity Factored in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Display Taxable Value in Grid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Restrict Assessment Entry by Ledger Items', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Assessments - Entry - Value Column Order', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Allow Deletion of Received Bills', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Allow Parcel / Authority / Levy Class Combination on Multiple Bills', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Allow Payee Changes to Vouchered Statement Groups', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Authority Can Only Belong to One Vendor', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Authority Can Only Belong to One Vendor - Exceptions', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Automatically Generate Authority / District Relationships', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Statement Group Uniqueness Columns', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills - Warn Before Deleting Bills', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Default Verified Status', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Installment for Negative Rounding Differences', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Installment for Positive Rounding Differences', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Restrict Authorities by County Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Bill Entry - Restrict Parcel by County Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Bill Lines Assessment by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Bill Lines Taxable Value by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Due Date Overrides by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy First Installment Indicator by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy No Longer Valid Bills Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Notes by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Refund Installments by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Reimbursement Installments by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Statement Numbers by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Supplemental Installments by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Copy Tax Amounts by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Copy - Weekend Due Date Default Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Groups - Number of Statement Years to Display Schedules', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Levy Rates - Copy - Overwrite Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Levy Rates - Default Display Format', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Allow Manual Updating to Fully Paid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Allow Statement Groups Changes On No Longer Valid Bills', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Allow Statement Groups Changes On Received Bills', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Automatically Save On Opening Voucher Window', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Display Missing Assessments Warning on Save', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - F2 Functionality', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Initial Row Selection Status', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Percent Threshold for Row Coloring Notification', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Search - Place to Refresh Calculated Amount', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Bills Center - Voucher - Apply Sort From Bill Entry Grid to Voucher Window', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Scenario - Default Results Display', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Data Center - Search Vault - Default Results Display', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Database - Allocation - Location - Use With Clause for Temp Allo Percents', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Database - Bills Center - Bill Entry - Separate Session to Add Composite', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Database - Levy Rates Copy - Use Temp ID', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Database - Temp Parcels - Minimum Number of Parcels in List for Temp Parcels Use', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Database - Temp Parcels Asmt Groups - Minimum Number of Parcel-Assessment Group Combos in List for Temp Parcels Asmt Groups Use', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Images - Network Storage Location', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Images - Storage Type', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger - Class Code Value 1', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger - Class Code Value 2', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Adjust - Allow Adjustments for $0 Balances', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Adjust/Transfer - Default Auto-Calc Reserve Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Transfer - Allow Zero Dollar Transfers', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Class Code Value1', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Class Code Value2', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Location', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Ledger Center - Wildcard to Search Parcel', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Assessment Groups - Setup - Copy Equalization Factors Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Assessment Groups - Setup - Copy Taxable Value Rates Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Copy Authority-District Relationships Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Copy Equalization Factors Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Copy Taxable Value Rates Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Copy Assessments - Overwrite Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Details - Assessments - Show Only Cases with Values', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Details - Auto-Save Changes When Changing Records', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Assessments - Default Case Compare Type', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Assessments - Default Case Compare Year', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Autosize Row Height', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Flex Field Labels - Append Flex Field Number to Label', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Search - Initial Row Selection Status', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcels - Allow Deletes', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Parcels - Parcel Uniqueness Columns', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - AP Interface - Send to AP Amount Verification', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Approvals - Payment Approver Can Also Release', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Approvals - Payment Enterer Can Also Approve', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Voiding - Allow Voiding of Paid Payments', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Voiding - Allow Voiding of Sent to AP Payments', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Allow Payment Dates Prior to Current Date', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Allow Vouchering of Bills With No Active Ledger Items', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Default Scheduled Pay Date', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Default Scheduled Pay Date - Backup Days', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Default Special Instructions', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Preview Voucher Before Saving Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Require Assessments for All Parcels Being Vouchered', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Vouchering - Vouchering Completed Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Warn Before Rejecting Payments', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments - Warn Before Voiding Payments', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Current Payments - Default Begin Date', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Current Payments - Default End Date', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Current Payments - Retrieve Automatically On First Open', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Default View', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Allow Any User to Select Rows', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Approval Report Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Auto-Refresh on Entry', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Default Enterer', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Approved - Only Show My Approvals', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Auto-Refresh on Entry', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Default Due By Date', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Default PT Company', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Default State', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Don''t Send to AP - Release Completed Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Don''t Send to AP - Release Completed Dataobject 2', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Send to AP - Release Completed Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Send to AP - Release Completed Dataobject 2', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Released - Warn On Release (Don''t Send to AP)', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Default Due By Date', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Default PT Company', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Default State', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Home - Un-Vouchered - Retrieve Automatically On First Open', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Analysis Report Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Attachment Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Check Request Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Check Request for Un-Approved Payments', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Check Request for Un-Released Payments', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Enable Validation Report for Sent To AP/Paid Payments', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Labels Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - USPS Forms Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Reports - Validation Dataobject', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Search - Allow Manual Status Change from Sent to AP to Paid', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Payments Center - Search - Allow Multiple Voids At Once', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Assign - Delete by State Table', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Creation - Assets to Use', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Default Refresh Off Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns - CPR Tree - Vintage Range Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns - CWIP Pseudo Tree - Default Refresh Off Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns - CWIP Tree - Creation - Include RWIP', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns - CWIP Tree - Default Refresh Off Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Ignore Prop Tax Locations for State Allocations (CPR)', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Ignore Prop Tax Locations for State Allocations (CWIP)', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Treat Missing Depr Factors As Zeros', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation - Unspecified State Definition', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Allocation Audit - Filter to Show Errors', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Auto-Retrieve Steps if All Paramaters Populated on Open', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - CPR Tree - Print - Report Type', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - CWIP Pseudo Tree - Print - Report Type', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - CWIP Tree - Print - Report Type', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Copy Stats - Copy Option Selected by Default', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - PT Loc/County Audits - Auto-Create Prop Tax Locations - Location Rollup Creation Option', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - PT Loc/County Audits - Enable Property Tax Location Dropdown', 'proptax', sysdate, user );
insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID ) values ( 'Returns Center - Remember Last Used Prop Tax Company / State / Tax Year', 'proptax', sysdate, user );

--
-- There are now separate workspaces for each of the report types, so reset those.
--
update PPBASE_WORKSPACE set WORKSPACE_UO_NAME = 'uo_ptc_rptcntr_wksp_electronic' where MODULE = 'proptax' and WORKSPACE_IDENTIFIER = 'reporting_electronic';
update PPBASE_WORKSPACE set WORKSPACE_UO_NAME = 'uo_ptc_rptcntr_wksp_rendition'  where MODULE = 'proptax' and WORKSPACE_IDENTIFIER = 'reporting_rendition';

--
-- We've renamed all of the logic objects, so update the pp_datawindow_hints table accordingly.
--
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_accruals', 'nvo_ptc_logic_accruals' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_accruals%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_asset_shell', 'nvo_ptc_logic_asset_shell' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_asset_shell%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_bills', 'nvo_ptc_logic_bills' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_bills%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_calendar', 'nvo_ptc_logic_calendar' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_calendar%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_case', 'nvo_ptc_logic_case' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_case%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_control_allocation', 'nvo_ptc_logic_control_allocation' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_control_allocation%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_control_balances', 'nvo_ptc_logic_control_balances' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_control_balances%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_control_extraction', 'nvo_ptc_logic_control_extraction' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_control_extraction%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_control_nettax', 'nvo_ptc_logic_control_nettax' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_control_nettax%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_import', 'nvo_ptc_logic_import' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_import%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_ledger', 'nvo_ptc_logic_ledger' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_ledger%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_location', 'nvo_ptc_logic_location' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_location%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_log', 'nvo_ptc_logic_log' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_log%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_parcel', 'nvo_ptc_logic_parcel' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_parcel%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_payments', 'nvo_ptc_logic_payments' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_payments%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_preallo_ledger', 'nvo_ptc_logic_preallo_ledger' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_preallo_ledger%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_tables', 'nvo_ptc_logic_tables' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_tables%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_taxyear', 'nvo_ptc_logic_taxyear' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_taxyear%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_types', 'nvo_ptc_logic_types' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_types%';
update PP_DATAWINDOW_HINTS set DATAWINDOW = replace( DATAWINDOW, 'uo_ptc_logic_valuation', 'nvo_ptc_logic_valuation' ) where lower( DATAWINDOW ) like '%uo_ptc_logic_valuation%';

--
-- Update the tables that use dw_pt_xxx_filter to use the new dw_pp_xxx_filter.
--
update PPBASE_SYSTEM_OPTIONS_VALUES set OPTION_VALUE = replace( OPTION_VALUE, 'dw_pt_class_code_filter', 'dw_pp_class_code_filter' ) where OPTION_VALUE like '%dw_pt_class_code_filter%';
update PPBASE_SYSTEM_OPTIONS_VALUES set OPTION_VALUE = replace( OPTION_VALUE, 'dw_pt_state_filter', 'dw_pp_state_filter' ) where OPTION_VALUE like '%dw_pt_state_filter%';
update PPBASE_SYSTEM_OPTIONS_VALUES set OPTION_VALUE = replace( OPTION_VALUE, 'dw_pt_user_filter', 'dw_pp_user_filter' ) where OPTION_VALUE like '%dw_pt_user_filter%';

update POWERPLANT_COLUMNS
   set DROPDOWN_NAME = replace(DROPDOWN_NAME, 'pt_', 'pp_')
 where PP_EDIT_TYPE_ID = 'p'
   and DROPDOWN_NAME in
       ('pt_company_filter', 'pt_spread_factor_filter', 'pt_state_filter', 'pt_yes_no_filter');

update POWERPLANT_COLUMNS set DROPDOWN_NAME = 'pp_county_filter' where TABLE_NAME in ( 'prop_tax_district', 'pt_market_value_rate_county', 'property_tax_authority' ) and COLUMN_NAME = 'county_id';
update POWERPLANT_COLUMNS set DROPDOWN_NAME = 'pp_state_filter' where TABLE_NAME in ( 'prop_tax_district' ) and COLUMN_NAME = 'state_id';
update POWERPLANT_COLUMNS set DROPDOWN_NAME = 'pp_yes_no_filter' where TABLE_NAME in ( 'prop_tax_district' ) and COLUMN_NAME = 'assignment_indicator';
update POWERPLANT_COLUMNS set DROPDOWN_NAME = 'pp_status_code_filter' where TABLE_NAME in ( 'pt_type_rollup', 'pt_reserve_factors', 'pt_escalated_value_type' ) and COLUMN_NAME = 'status_code_id';

--
-- Create additional Property Tax report types for standard reports so that the reports will be segregated in the Reporting Center.
--
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 202, 'PTC - Client Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 203, 'PTC - Ledger Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 204, 'PTC - Preallo Ledger Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 205, 'PTC - Ledger Comparison Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 206, 'PTC - Assessment Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 207, 'PTC - Assessment Comparison Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 208, 'PTC - Parcel Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 209, 'PTC - Parcel Comparison Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 210, 'PTC - Bill Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 211, 'PTC - Bill Comparison Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 212, 'PTC - Payment Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 213, 'PTC - Payment Comparison Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 214, 'PTC - Actuals Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 215, 'PTC - Actuals Comparison Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 216, 'PTC - Accruals Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 217, 'PTC - Audit Reports' );
insert into PP_REPORT_TYPE ( REPORT_TYPE_ID, DESCRIPTION ) values ( 218, 'PTC - Other Reports' );

update PP_REPORTS set REPORT_TYPE_ID = 202 where REPORT_TYPE_ID = 9 and REPORT_ID between 500000 and 500999;
update PP_REPORTS set REPORT_TYPE_ID = 203 where REPORT_TYPE_ID = 9 and REPORT_ID between 501000 and 501899;
update PP_REPORTS set REPORT_TYPE_ID = 204 where REPORT_TYPE_ID = 9 and REPORT_ID between 501900 and 501999;
update PP_REPORTS set REPORT_TYPE_ID = 205 where REPORT_TYPE_ID = 9 and REPORT_ID between 502000 and 502999;
update PP_REPORTS set REPORT_TYPE_ID = 206 where REPORT_TYPE_ID = 9 and REPORT_ID between 503000 and 503999;
update PP_REPORTS set REPORT_TYPE_ID = 207 where REPORT_TYPE_ID = 9 and REPORT_ID between 504000 and 504999;
update PP_REPORTS set REPORT_TYPE_ID = 208 where REPORT_TYPE_ID = 9 and REPORT_ID between 505000 and 505499;
update PP_REPORTS set REPORT_TYPE_ID = 209 where REPORT_TYPE_ID = 9 and REPORT_ID between 505500 and 505999;
update PP_REPORTS set REPORT_TYPE_ID = 210 where REPORT_TYPE_ID = 9 and REPORT_ID between 506000 and 506499;
update PP_REPORTS set REPORT_TYPE_ID = 211 where REPORT_TYPE_ID = 9 and REPORT_ID between 506500 and 506999;
update PP_REPORTS set REPORT_TYPE_ID = 212 where REPORT_TYPE_ID = 9 and REPORT_ID between 507000 and 507499;
update PP_REPORTS set REPORT_TYPE_ID = 213 where REPORT_TYPE_ID = 9 and REPORT_ID between 507500 and 507999;
update PP_REPORTS set REPORT_TYPE_ID = 214 where REPORT_TYPE_ID = 9 and REPORT_ID between 508000 and 508499;
update PP_REPORTS set REPORT_TYPE_ID = 215 where REPORT_TYPE_ID = 9 and REPORT_ID between 508500 and 508999;
update PP_REPORTS set REPORT_TYPE_ID = 216 where REPORT_TYPE_ID = 9 and REPORT_ID between 509000 and 509499;
update PP_REPORTS set REPORT_TYPE_ID = 217 where REPORT_TYPE_ID = 9 and REPORT_ID between 509500 and 509699;
update PP_REPORTS set REPORT_TYPE_ID = 218 where REPORT_TYPE_ID = 9 and REPORT_ID between 509700 and 510000;

update PP_REPORT_TYPE set DESCRIPTION = 'PTC - Standard Reports' where REPORT_TYPE_ID = 9;
update PP_REPORT_TYPE set DESCRIPTION = 'PTC - Returns' where REPORT_TYPE_ID = 200;
update PP_REPORT_TYPE set DESCRIPTION = 'PTC - Electronic Filings' where REPORT_TYPE_ID = 201;

--
-- Make the Ledger Copy Activity workspace just one menu item instead of one for preallo and one for postallo.
--
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('proptax', 'ledger_copy', sysdate, user, 'Copy Activity', 'uo_ptc_ledgcntr_wksp_copy_activity',
    'Copy Activity to Current Year');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'ledger_copy'
 where MODULE = 'proptax'
   and MENU_IDENTIFIER = 'ledger_copy';

delete from PPBASE_MENU_ITEMS where MODULE = 'proptax' and MENU_IDENTIFIER in ( 'ledger_preallo_copy', 'ledger_postallo_copy' );
delete from PPBASE_WORKSPACE  where MODULE = 'proptax' and WORKSPACE_IDENTIFIER in ( 'ledger_preallo_copy', 'ledger_postallo_copy' );

--
-- Set up the Returns Center.
--
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('proptax', 'returns_home', sysdate, user, 'Returns Center', 'uo_ptc_rtncntr_wksp_home',
    'Returns Center');

update PPBASE_MENU_ITEMS set WORKSPACE_IDENTIFIER = 'returns_home' where MODULE = 'proptax' and MENU_IDENTIFIER = 'returns_center';
update PT_PROCESS set OBJECT_NAME = 'uo_ptc_rptcntr_wksp_rendition' where PT_PROCESS_ID = 12;

--
-- Create a table to store process statuses.
--
create table PT_PROCESS_STATUS
(
  PT_PROCESS_STATUS_ID number(22,0) not null,
  TIME_STAMP           date,
  USER_ID              varchar2(18),
  DESCRIPTION          varchar2(35),
  LONG_DESCRIPTION     varchar2(254)
);

alter table PT_PROCESS_STATUS
   add constraint PT_PROCESS_STATUS_PK
       primary key (PT_PROCESS_STATUS_ID)
       using index tablespace PWRPLANT_IDX;

-- Insert the fixed values.
insert into PT_PROCESS_STATUS ( PT_PROCESS_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 1, 'Unprocessed', 'This process has not yet been run.' );
insert into PT_PROCESS_STATUS ( PT_PROCESS_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 2, 'Failed', 'This process has been run, but the run failed.' );
insert into PT_PROCESS_STATUS ( PT_PROCESS_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 3, 'Re-Run Needed', 'This process has been successfully run, but a process on which it is dependent has since been run.  Thus, this process needs to be re-run.' );
insert into PT_PROCESS_STATUS ( PT_PROCESS_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 4, 'Succeeded', 'This process has been successfully run.' );

-- Add status to the PT Process Company State Tax Year table.
alter table PT_PROCESS_COMPANY_STATE_TY add PT_PROCESS_STATUS_ID number(22,0);

alter table PT_PROCESS_COMPANY_STATE_TY
   add constraint PT_PROC_CO_ST_TY_PROC_STAT_FK
       foreign key (PT_PROCESS_STATUS_ID)
       references PT_PROCESS_STATUS;

-- Succeeded - is_completed_successful = 1
update PT_PROCESS_COMPANY_STATE_TY set PT_PROCESS_STATUS_ID = 4 where IS_COMPLETED_SUCCESSFUL = 1;

-- Failed - is_completed_successful = 0 but run time is filled in
update PT_PROCESS_COMPANY_STATE_TY set PT_PROCESS_STATUS_ID = 2 where IS_COMPLETED_SUCCESSFUL = 0 and LAST_RUN_TIME is not null;

-- Re-Run Needed - is_completed_successful = 1 but a process on which it depends has a more recent run time
update PT_PROCESS_COMPANY_STATE_TY PCSTY
   set PCSTY.PT_PROCESS_STATUS_ID = 3
 where PCSTY.IS_COMPLETED_SUCCESSFUL = 1
   and exists (select 'x'
          from PT_PROCESS_DEPENDENCY DEP, PT_PROCESS_COMPANY_STATE_TY INPCSTY
         where DEP.DEPENDENT_PROCESS_ID = PCSTY.PT_PROCESS_ID
           and DEP.TAX_YEAR = INPCSTY.TAX_YEAR
           and DEP.PROP_TAX_COMPANY_ID = INPCSTY.PROP_TAX_COMPANY_ID
           and DEP.STATE_ID = INPCSTY.STATE_ID
           and DEP.PT_PROCESS_ID = INPCSTY.PT_PROCESS_ID
           and INPCSTY.LAST_RUN_TIME > PCSTY.LAST_RUN_TIME);

-- Unprocessed - Any remaining processes
update PT_PROCESS_COMPANY_STATE_TY set pt_PROCESS_STATUS_ID = 1 where PT_PROCESS_STATUS_ID is null;

alter table PT_PROCESS_COMPANY_STATE_TY modify PT_PROCESS_STATUS_ID not null;
alter table PT_PROCESS_COMPANY_STATE_TY drop column IS_COMPLETED_SUCCESSFUL;

--
-- Update the short descriptions on some processes so that they fit on the screen in the Returns Center.
--
update PT_PROCESS set SHORT_DESCRIPTION = 'Loc/County Audit' where PT_PROCESS_ID = 21;
update PT_PROCESS set SHORT_DESCRIPTION = 'Import Preallo Items' where PT_PROCESS_ID = 41;
update PT_PROCESS set SHORT_DESCRIPTION = 'CPR Preallo Audit' where PT_PROCESS_ID = 4;
update PT_PROCESS set SHORT_DESCRIPTION = 'CWIP Preallo Audit' where PT_PROCESS_ID = 16;
update PT_PROCESS set SHORT_DESCRIPTION = 'Copy Preallo Activity' where PT_PROCESS_ID = 5;
update PT_PROCESS set SHORT_DESCRIPTION = 'Import Preallo Adjs' where PT_PROCESS_ID = 33;
update PT_PROCESS set SHORT_DESCRIPTION = 'Input Preallo Activity' where PT_PROCESS_ID = 25;
update PT_PROCESS set SHORT_DESCRIPTION = 'CPR Ledger Audit' where PT_PROCESS_ID = 9;
update PT_PROCESS set SHORT_DESCRIPTION = 'CWIP Ledger Audit' where PT_PROCESS_ID = 17;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (417, 0, 10, 4, 1, 0, 10010, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010010_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
