/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_052042_lessee_06_forecast_asset_query_new_cols_DML.sqll
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0   07/23/2018   AJSmith      Add many new columns to Asset Schedule queries 
 ||============================================================================
 */ 

DECLARE

  query_id number;
  
BEGIN
-- Forecast Lease Asset Schedule by Leased Asset
  select id into query_id from pp_any_query_criteria 
  WHERE description = 'Forecast Lease Asset Schedule by Leased Asset';
  
  --BEG_DEFERRED_RENT, 
    INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_deferred_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Deferred Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_deferred_rent')
	;
     
-- DEFERRED_RENT, 
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'deferred_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Deferred Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'deferred_rent')
	;
     
--  END_DEFERRED_RENT, 
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_deferred_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Deferred Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_deferred_rent')
	;
     
--  BEG_LIABILITY, 
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_liability',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Liability',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_liability')
	;
     
--  END_LIABILITY, 
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_liability',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Liability',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_liability')
	;
     
--  BEG_LT_LIABILITY, 
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_lt_liability',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg LT Liability',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_lt_liability')
	;
     
--  END_LT_LIABILITY,
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_lt_liability',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End LT Liability',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_lt_liability')
	;
     
-- Principal Remeasurement
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'principal_remeasurement',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Principal Remeasurement',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'principal_remeasurement')
	;
     

  -- Principal Remeasurement
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'principal_remeasurement',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Principal Remeasurement',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'principal_remeasurement')
	;
     
-- Liability Remeasurement
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'liability_remeasurement',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Liability Remeasurement',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'liability_remeasurement')
	;
     
--Beg Net ROU Asset
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_net_rou_asset',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Net ROU Asset',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_net_rou_asset')
	;

-- ROU Asset Remeasurement
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'rou_asset_remeasurement',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'ROU Asset Remeasurement',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'rou_asset_remeasurement')
	;

-- End Net ROU Asset
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_net_rou_asset',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Net ROU Asset',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_net_rou_asset')
	;

-- Initial Direct Cost
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'initial_direct_cost',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Initial Direct Cost',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'initial_direct_cost')
	;

-- Incentive Amount 
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'incentive_amount',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Incentive Amount',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'incentive_amount')
	;

-- Beg Prepaid Rent
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_prepaid_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Prepaid Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_prepaid_rent')
	;

-- Prepay Amortization
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'prepay_amortization',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Prepay Amortization',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'prepay_amortization')
	;

-- Prepaid Rent
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'prepaid_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Prepaid Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'prepaid_rent')
	;

-- End Prepay Rent
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_prepaid_rent',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Prepaid Rent',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_prepaid_rent')
	;

-- Beg Arrears Accrual
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'beg_arrears_accrual',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Beg Arrears Accrual',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'beg_arrears_accrual')
	;

-- Arrears Accrual
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'arrears_accrual',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Arrears Accrual',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'arrears_accrual')
	;

-- End Arrears Accrual
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_arrears_accrual',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Arrears Accrual',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_arrears_accrual')
	;

-- Executory Adjust
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'executory_adjust',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Executory Adjust',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'executory_adjust')
	;

-- Contingent Adjust
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'contingent_adjust',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Contingent Adjust',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'contingent_adjust')
	;

-- Begin Reserve
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'begin_reserve',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Begin Reserve',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'begin_reserve')
	;

-- Depr Expense
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'depr_expense',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Depr Expense',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'depr_expense')
	;

-- Depr Expense Adjust
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'depr_exp_alloc_adjust',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'Depr Expense Adjust',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'depr_exp_alloc_adjust')
	;

-- End Reserve
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID, DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA,
     COLUMN_HEADER,
     COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS)
   SELECT query_id, 'end_reserve',
     (select max(column_order) + 1 from pp_any_query_criteria_fields where id = query_id),
     1, 1,
     'End Reserve',
     300, 'NUMBER', 0, 0, 0
	 from dual
	 where not exists
		(select 1 from pp_any_query_criteria_fields x
		where x.id = query_id
		and x.detail_field = 'end_reserve')
	;

END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8506, 0, 2017, 4, 0, 0, 52042, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052042_lessee_06_forecast_asset_query_new_cols_DML.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  