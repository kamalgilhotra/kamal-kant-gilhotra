/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035999_depr_vintage_input_table.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/13/2014 Kyle Peterson
||============================================================================
*/

CREATE TABLE FCST_DEPR_VINTAGE_INPUT
(
 FCST_DEPR_GROUP_ID          number(22,0) not null,
 SET_OF_BOOKS_ID             number(22,0) not null,
 VINTAGE                     number(22,0) not null,
 ACCOUNTING_MONTH            date         not null,
 FCST_DEPR_VERSION_ID        number(22,0) not null,
 TIME_STAMP                  date         null,
 USER_ID                     varchar2(18) null,
 ACCUM_COST                  number(22,2) null,
 ADDITIONS                   number(22,2) null,
 RETIREMENTS                 number(22,2) null,
 TRANSFERS_IN                number(22,2) null,
 TRANSFERS_OUT               number(22,2) null,
 ADJUSTMENTS                 number(22,2) null,
 FCST_COMBINED_DEPR_GROUP_ID number(22,0) null
);

alter table FCST_DEPR_VINTAGE_INPUT
   add constraint PK_FCST_DEPR_VINTAGE_INPUT
       PRIMARY KEY (FCST_DEPR_GROUP_ID, SET_OF_BOOKS_ID, VINTAGE, ACCOUNTING_MONTH, FCST_DEPR_VERSION_ID);

alter table FCST_DEPR_VINTAGE_INPUT
   add constraint R_FCST_DEPR_VINTAGE_INPUT1
       foreign key (FCST_DEPR_GROUP_ID, FCST_DEPR_VERSION_ID)
       references FCST_DEPR_GROUP_VERSION (FCST_DEPR_GROUP_ID, FCST_DEPR_VERSION_ID);

alter table FCST_DEPR_VINTAGE_INPUT
   add constraint R_FCST_DEPR_VINTAGE_INPUT2
       foreign key (FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID)
       references FCST_VERSION_SET_OF_BOOKS (FCST_DEPR_VERSION_ID, SET_OF_BOOKS_ID);

comment on table FCST_DEPR_VINTAGE_INPUT is '(C)  [01]
The Fcst Depr Vintage Input table provides an area to store user input for forecast depreciation vintages.';

comment on column FCST_DEPR_VINTAGE_INPUT.fcst_depr_group_id is 'System-assigned identifier to a forecast depreciation group.';
comment on column FCST_DEPR_VINTAGE_INPUT.set_of_books_id is 'System-assigned identifier to a set of books.';
comment on column FCST_DEPR_VINTAGE_INPUT.vintage is 'Book vintage (engineering in-service date).';
comment on column FCST_DEPR_VINTAGE_INPUT.accounting_month is 'Accounting month and year.';
comment on column FCST_DEPR_VINTAGE_INPUT.fcst_depr_version_id is 'System-assigned identifier of a forecast depreciation version.';
comment on column FCST_DEPR_VINTAGE_INPUT.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column FCST_DEPR_VINTAGE_INPUT.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column FCST_DEPR_VINTAGE_INPUT.accum_cost is 'Accumulated book cost at the end of the period in dollars.';
comment on column FCST_DEPR_VINTAGE_INPUT.additions is 'Records the asset additions activity that occurred during the month for the depreciation group in dollars.  The interpretation is the FERC activity, e.g., including adjustment transactions reclassified as a FERC add.  Normally positive.';
comment on column FCST_DEPR_VINTAGE_INPUT.retirements is 'Records the asset retirement activity that occurred during the month in dollars.  These are just the retirement transactions.  Normally negative.  These are also used to reduce the reserve.';
comment on column FCST_DEPR_VINTAGE_INPUT.transfers_in is 'Records the asset transfers into the depreciation group for the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally positive.';
comment on column FCST_DEPR_VINTAGE_INPUT.transfers_out is 'Records the asset transfers out of the depreciation group that occurred during the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally negative.';
comment on column FCST_DEPR_VINTAGE_INPUT.adjustments is 'Records the asset adjustments that occurred to the depreciation group during the month in dollars.  The interpretation is the FERC activity, e.g. it excludes adjustment transactions that have been reclassified as FERC additions or retirements.  (Increases in the depreciable balance are positive.)';
comment on column FCST_DEPR_VINTAGE_INPUT.fcst_combined_depr_group_id is 'System-assigned identifier of a particular combined depr group.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (962, 0, 10, 4, 2, 0, 35999, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035999_depr_vintage_input_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;