SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007861_projects_workflow_conversion_budg_review.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   09/24/2012 Chris Mardis   Point Release
||============================================================================
*/

/*
--
-- Only run this script if the system control "Workflow User Object-Budg Review" does not exist or lower(trim(control_value)) = "no"
-- WARNING: this script may take a LONG time to run
--
*/

/*
-- If the following PL/SQL block raises an Error then the script may need to be altered before running.  Check with Chris Mardis for
-- further information on customization of this script.
--
-- After customization set "L_SKIP  boolean := TRUE;" in the PL/SQL block below.
*/

declare
   L_SKIP          boolean := false;
   L_COUNT         number := 0;
   L_CONTROL_VALUE varchar2(30);
begin
   if not L_SKIP then
      select LOWER(CONTROL_VALUE)
        into L_CONTROL_VALUE
        from PP_SYSTEM_CONTROL
       where LOWER(CONTROL_NAME) = 'workflow user object-budg review';

      if L_CONTROL_VALUE = 'no' then
         select count(*) into L_COUNT from BUDGET_REVIEW;
         if L_COUNT > 0 then
            RAISE_APPLICATION_ERROR(-20000,
                                    'This script may need to be altered before running.  Please read comments in script.');
         else
            update PP_SYSTEM_CONTROL
               set CONTROL_VALUE = 'yes'
             where LOWER(CONTROL_NAME) = 'workflow user object-budg review';
            commit;
            DBMS_OUTPUT.PUT_LINE('System control updated to yes.');
         end if;
      else
         DBMS_OUTPUT.PUT_LINE('System control was already set to yes.');
      end if;
   end if;
end;
/


-- 01142014 - Moved comment block down to Conversion for Budget Review Types

--**********************************************
-- ADD FIX 01082014
-- This is a copy of what is in the 1172_10.4.0.0_maint_011688_budget.sql script
--**********************************************
-- Drop the foreign key on WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'WORK_ORDER_TYPE'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;

   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/

-- Drop the foreign key on WORK_ORDER_APPROVAL > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE
declare
   LV_EXISTS boolean := false;

begin
   for CSR_ALTER_SQL in (select 'alter table ' || TABLE_NAME || ' drop constraint ' ||
                                CONSTRAINT_NAME ALTER_SQL
                           from ALL_CONSTRAINTS
                          where OWNER = 'PWRPLANT'
                            and TABLE_NAME = 'WORK_ORDER_APPROVAL'
                            and R_CONSTRAINT_NAME in
                                (select CONSTRAINT_NAME
                                   from ALL_CONSTRAINTS
                                  where OWNER = 'PWRPLANT'
                                    and TABLE_NAME = 'BUDGET_REVIEW_TYPE'
                                    and CONSTRAINT_TYPE = 'P'))
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
      execute immediate CSR_ALTER_SQL.ALTER_SQL;
      LV_EXISTS := true;
   end loop;
   if not LV_EXISTS then
      DBMS_OUTPUT.PUT_LINE('Foreign key to be dropped, WORK_ORDER_TYPE > BUDGET_REVIEW_TYPE_ID referencing BUDGET_REVIEW_TYPE doesn''t exist.');
   end if;
end;
/
--**********************************************
-- END ADD
--**********************************************

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select *
     from (select max(CONTROL_ID) + 1 CONTROL_ID,
                  'Workflow User Object-Budg Review' CONTROL_NAME,
                  'yes' CONTROL_VALUE,
                  'dw_yes_no;1',
                  'Yes - turns on the workflow functionality for the budget review process; No - uses the old budget review process',
                  -1
             from PP_SYSTEM_CONTROL_COMPANY)
    where not exists
    (select 1
             from PP_SYSTEM_CONTROL_COMPANY
            where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object-Budg Review'));

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'yes'
 where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object-Budg Review');

update PP_MYPP_APPROVAL_CONTROL
   set DW = 'dw_budget_review_workflow_grid'
 where DW = 'dw_budget_review_grid';

/*
--
-- Conversion for Budget Review Types
--
-- Modified 01082014
create table BUDG_REVIEW_WORKFLOW_TYPE_CONV as
select BUDGET_REVIEW_TYPE_ID,
       (NVL((select max(WORKFLOW_TYPE_ID) from WORKFLOW_TYPE), 0) + ROWNUM) * 100 WORK_TYPE_ID
  from (select BUDGET_REVIEW_TYPE_ID
          from BUDGET_REVIEW_TYPE
         where LOWER(USAGE_FLAGS) like '%review%'
         order by BUDGET_REVIEW_TYPE_ID);

insert into WORKFLOW_TYPE
   (WORKFLOW_TYPE_ID, DESCRIPTION, SUBSYSTEM, ACTIVE, USE_LIMITS, SQL_APPROVAL_AMOUNT, BASE_SQL)
   select B.WORKFLOW_TYPE_ID BUDGET_REVIEW_TYPE_ID,
          DESCRIPTION,
          'budg_review',
          1,
          0,
          'select nvl(sum(appr_amount),0) appr_amount from (' || CHR(13) || CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_viewapp a, wo_est_expense_viewapp b, wo_est_removal_viewapp c, wo_est_jobbing_viewapp d, wo_est_credits_viewapp e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''budg_review''' || CHR(13) || CHR(10) || ')' SQL_APPROVAL_AMOUNT,
          1 BASE_SQL
     from BUDGET_REVIEW_TYPE A, BUDG_REVIEW_WORKFLOW_TYPE_CONV B
    where A.BUDGET_REVIEW_TYPE_ID = B.BUDGET_REVIEW_TYPE_ID;

insert into WORKFLOW_RULE
   (WORKFLOW_RULE_ID, DESCRIPTION, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED)
   select B.WORKFLOW_TYPE_ID + A.ROWN WORKFLOW_RULE_ID,
          A.DESCRIPTION,
          0 AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED
     from (select BUDGET_REVIEW_TYPE_ID,
                  DESCRIPTION,
                  RANK() OVER(partition by BUDGET_REVIEW_TYPE_ID order by LEVEL_ID desc) ROWN
             from (select BUDGET_REVIEW_TYPE_ID, LEVEL_ID, DESCRIPTION
                     from BUDGET_REVIEW_LEVEL
                    order by BUDGET_REVIEW_TYPE_ID, LEVEL_ID desc)) A,
          BUDG_REVIEW_WORKFLOW_TYPE_CONV B
    where A.BUDGET_REVIEW_TYPE_ID = B.BUDGET_REVIEW_TYPE_ID;

insert into WORKFLOW_TYPE_RULE
   (WORKFLOW_TYPE_ID, WORKFLOW_RULE_ID, RULE_ORDER)
   select B.WORKFLOW_TYPE_ID WORKFLOW_TYPE_ID,
          B.WORKFLOW_TYPE_ID + A.ROWN WORKFLOW_RULE_ID,
          RANK() OVER(partition by A.BUDGET_REVIEW_TYPE_ID order by A.BUDGET_REVIEW_TYPE_ID, A.ROWN) RULE_ORDER
     from (select BUDGET_REVIEW_TYPE_ID,
                  DESCRIPTION,
                  RANK() OVER(partition by BUDGET_REVIEW_TYPE_ID order by LEVEL_ID desc) ROWN
             from (select BUDGET_REVIEW_TYPE_ID, LEVEL_ID, DESCRIPTION
                     from BUDGET_REVIEW_LEVEL
                    order by BUDGET_REVIEW_TYPE_ID, LEVEL_ID desc)) A,
          BUDG_REVIEW_WORKFLOW_TYPE_CONV B
    where A.BUDGET_REVIEW_TYPE_ID = B.BUDGET_REVIEW_TYPE_ID;
commit;

insert into APPROVAL_AUTH_LEVEL
   (APPROVAL_TYPE_ID, AUTH_LEVEL, USERS)
   select C.WORKFLOW_TYPE_ID APPROVAL_TYPE_ID, C.WORKFLOW_TYPE_ID + A.ROWN AUTH_LEVEL, B.USERS
     from (select BUDGET_REVIEW_TYPE_ID,
                  LEVEL_ID,
                  RANK() OVER(partition by BUDGET_REVIEW_TYPE_ID order by LEVEL_ID desc) ROWN
             from (select BUDGET_REVIEW_TYPE_ID, LEVEL_ID, DESCRIPTION
                     from BUDGET_REVIEW_LEVEL
                    order by BUDGET_REVIEW_TYPE_ID, LEVEL_ID desc)) A,
          BUDGET_REVIEW_USERS B,
          BUDG_REVIEW_WORKFLOW_TYPE_CONV C
    where A.BUDGET_REVIEW_TYPE_ID = B.BUDGET_REVIEW_TYPE_ID
      and A.LEVEL_ID = B.LEVEL_ID
      and A.BUDGET_REVIEW_TYPE_ID = C.BUDGET_REVIEW_TYPE_ID;

--
-- Conversion for existing budget reviews
--
update WORK_ORDER_APPROVAL A
   set BUDGET_REVIEW_TYPE_ID =
        (select CNV.WORKFLOW_TYPE_ID
           from BUDG_REVIEW_WORKFLOW_TYPE_CONV CNV
          where CNV.BUDGET_REVIEW_TYPE_ID = A.BUDGET_REVIEW_TYPE_ID)
 where A.BUDGET_REVIEW_TYPE_ID is not null;

update WORK_ORDER_TYPE A
   set A.BUDGET_REVIEW_TYPE_ID =
        (select CNV.WORKFLOW_TYPE_ID
           from BUDG_REVIEW_WORKFLOW_TYPE_CONV CNV
          where CNV.BUDGET_REVIEW_TYPE_ID = A.BUDGET_REVIEW_TYPE_ID)
 where A.BUDGET_REVIEW_TYPE_ID is not null;

commit;

delete from WORKFLOW_DETAIL
 where WORKFLOW_ID in (select WORKFLOW_ID from WORKFLOW where SUBSYSTEM = 'budg_review');

delete from WORKFLOW where SUBSYSTEM = 'budg_review';

commit;

insert into WORKFLOW
   (WORKFLOW_ID, WORKFLOW_TYPE_ID, WORKFLOW_TYPE_DESC, SUBSYSTEM, ID_FIELD1, ID_FIELD2,
    APPROVAL_AMOUNT, APPROVAL_STATUS_ID, DATE_SENT, DATE_APPROVED, USE_LIMITS, SQL_APPROVAL_AMOUNT)
   select PWRPLANT1.NEXTVAL WORKFLOW_ID,
          WT.WORKFLOW_TYPE_ID,
          WT.DESCRIPTION WORKFLOW_TYPE_DESC,
          'budg_review' SUBSYSTEM,
          WOA.WORK_ORDER_ID ID_FIELD1,
          WOA.REVISION ID_FIELD2,
          (select CAPITAL + EXPENSE + REMOVAL + JOBBING + CREDITS
             from WO_EST_CAPITAL_VIEWAPP A,
                  WO_EST_EXPENSE_VIEWAPP B,
                  WO_EST_REMOVAL_VIEWAPP C,
                  WO_EST_JOBBING_VIEWAPP D,
                  WO_EST_CREDITS_VIEWAPP E
            where A.WORK_ORDER_ID = B.WORK_ORDER_ID
              and A.REVISION = B.REVISION
              and A.WORK_ORDER_ID = C.WORK_ORDER_ID
              and A.REVISION = C.REVISION
              and A.WORK_ORDER_ID = D.WORK_ORDER_ID
              and A.REVISION = D.REVISION
              and A.WORK_ORDER_ID = E.WORK_ORDER_ID
              and A.REVISION = E.REVISION
              and A.WORK_ORDER_ID = WOA.WORK_ORDER_ID
              and A.REVISION = WOA.REVISION) APPROVAL_AMOUNT,
          REVIEW_STATUS APPROVAL_STATUS_ID,
          DECODE(REVIEW_STATUS,
                 1,
                 TO_DATE(null),
                 (select NVL(min(REVIEW_DATE), sysdate)
                    from BUDGET_REVIEW A
                   where A.WORK_ORDER_ID = WOA.WORK_ORDER_ID
                     and A.REVISION = WOA.REVISION)) DATE_SENT,
          DECODE(DECODE(REVIEW_STATUS, 3, 3, 4, 3, 1),
                 1,
                 TO_DATE(null),
                 (select NVL(max(REVIEW_DATE), sysdate)
                    from BUDGET_REVIEW A
                   where A.WORK_ORDER_ID = WOA.WORK_ORDER_ID
                     and A.REVISION = WOA.REVISION)) DATE_APPROVED,
          0 USE_LIMITS,
          WT.SQL_APPROVAL_AMOUNT
     from WORK_ORDER_APPROVAL WOA, WORK_ORDER_CONTROL WOC, WORKFLOW_TYPE WT
    where WOA.WORK_ORDER_ID = WOC.WORK_ORDER_ID
      and WOC.FUNDING_WO_INDICATOR = 1
      and WOA.BUDGET_REVIEW_TYPE_ID = WT.WORKFLOW_TYPE_ID;

commit;

insert into WORKFLOW_DETAIL
   (WORKFLOW_ID, ID, WORKFLOW_RULE_ID, WORKFLOW_RULE_DESC, RULE_ORDER, USERS, USER_APPROVED,
    DATE_APPROVED, APPROVAL_STATUS_ID, REQUIRED, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED)
   select (select W.WORKFLOW_ID
             from WORKFLOW W
            where W.ID_FIELD1 = TO_CHAR(BRD.WORK_ORDER_ID)
              and W.ID_FIELD2 = TO_CHAR(BRD.REVISION)
              and W.SUBSYSTEM = 'budg_review') WORKFLOW_ID,
          RANK() OVER(partition by BRD.WORK_ORDER_ID, BRD.REVISION order by BRD.WORK_ORDER_ID, BRD.REVISION, BRD.LEVEL_ID desc, BRD.USERS) ID,
          CNV.WORKFLOW_TYPE_ID + A.ROWN WORKFLOW_RULE_ID,
          A.DESCRIPTION WORKFLOW_RULE_DESC,
          WTR.RULE_ORDER,
          BRD.USERS,
          DECODE(BRD.REVIEW_DATE, null, null, BRD.USERS) USER_APPROVED,
          BRD.REVIEW_DATE DATE_APPROVED,
          DECODE(BRD.STATUS,
                 0,
                 3, --approved
                 1,
                 4, --rejected
                 2,
                 1, --initiated
                 3,
                 DECODE(BR.STATUS, 3, 2, 5), --pending (if the entire level is marked as approved make this pending record inactive)
                 5 --inactive catch-all
                 ) APPROVAL_STATUS_ID,
          REQUIRE_FLAG REQUIRED,
          0 AUTHORITY_LIMIT,
          null NUM_APPROVERS,
          1 NUM_REQUIRED
     from (select BUDGET_REVIEW_TYPE_ID,
                  LEVEL_ID,
                  DESCRIPTION,
                  RANK() OVER(partition by BUDGET_REVIEW_TYPE_ID order by LEVEL_ID desc) ROWN
             from (select BUDGET_REVIEW_TYPE_ID, LEVEL_ID, DESCRIPTION
                     from BUDGET_REVIEW_LEVEL
                    order by BUDGET_REVIEW_TYPE_ID, LEVEL_ID desc)) A,
          BUDGET_REVIEW_DETAIL BRD,
          BUDGET_REVIEW BR,
          WORK_ORDER_APPROVAL WOA,
          BUDG_REVIEW_WORKFLOW_TYPE_CONV CNV,
          WORKFLOW_TYPE_RULE WTR
    where WOA.WORK_ORDER_ID = BR.WORK_ORDER_ID
      and WOA.REVISION = BR.REVISION
      and BR.WORK_ORDER_ID = BRD.WORK_ORDER_ID
      and BR.REVISION = BRD.REVISION
      and BR.LEVEL_ID = BRD.LEVEL_ID
      and WOA.BUDGET_REVIEW_TYPE_ID = CNV.WORKFLOW_TYPE_ID
      and CNV.BUDGET_REVIEW_TYPE_ID = A.BUDGET_REVIEW_TYPE_ID
      and BRD.LEVEL_ID = A.LEVEL_ID
      and WOA.BUDGET_REVIEW_TYPE_ID = WTR.WORKFLOW_TYPE_ID
      and WOA.BUDGET_REVIEW_TYPE_ID + A.ROWN = WTR.WORKFLOW_RULE_ID;

commit;
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (234, 0, 10, 4, 0, 0, 7861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_007861_projects_workflow_conversion_budg_review.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

