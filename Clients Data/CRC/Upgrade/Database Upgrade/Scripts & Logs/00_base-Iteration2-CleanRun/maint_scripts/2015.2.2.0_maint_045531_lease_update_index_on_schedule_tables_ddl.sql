/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045531_lease_update_index_on_schedule_tables_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 03/29/2016 Charlie Shilling improve performance during build schedule process
|| 2015.2.2.0 04/14/2016 David Haupt	  Backport to 2015.2.2
||============================================================================
*/
DECLARE
	index_not_exists 	EXCEPTION;
	PRAGMA EXCEPTION_INIT(index_not_exists, -1418);
BEGIN
	BEGIN EXECUTE IMMEDIATE 'DROP INDEX ls_ilr_schedule_idx'; EXCEPTION WHEN index_not_exists THEN NULL; END;
	EXECUTE IMMEDIATE 'CREATE INDEX ls_ilr_schedule_idx ON ls_ilr_schedule (revision, ilr_id, set_of_books_id) TABLESPACE pwrplant_idx';
	BEGIN EXECUTE IMMEDIATE 'DROP INDEX ls_asset_schedule_idx'; EXCEPTION WHEN index_not_exists THEN NULL; END;
	EXECUTE IMMEDIATE 'CREATE INDEX ls_asset_schedule_idx ON ls_asset_schedule (revision, ls_asset_id, set_of_books_id) TABLESPACE pwrplant_idx';
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3122, 0, 2015, 2, 2, 0, 045531, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045531_lease_update_index_on_schedule_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;