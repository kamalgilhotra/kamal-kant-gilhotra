/*
||==============================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_041909_Contact_Edit_Initiator_Flag.sql
||==============================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==============================================================================================
|| Version    Date       Revised By          	Reason for Change
|| ---------- ---------- ------------------- 	-----------------------------------------------
|| 2015.1.0.0 1/13/2015  LAlston    	     	Converting WO Detail's Edit Initiator Flag from 
||												PP_SYSTEM_Control to PPBase System Tables.
||==============================================================================================
*/

-- WODETAIL - Edit WO Initiator
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select control_name, long_description, 0, 'Yes', control_value, 1, 0
  from pp_system_control
 where control_name = 'WODETAIL - Edit WO Initiator';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODETAIL - Edit WO Initiator', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODETAIL - Edit WO Initiator', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODETAIL - Edit WO Initiator', 'pcm');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2159, 0, 2015, 1, 0, 0, 041909, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041909_Contact_Edit_Initiator_Flag.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;