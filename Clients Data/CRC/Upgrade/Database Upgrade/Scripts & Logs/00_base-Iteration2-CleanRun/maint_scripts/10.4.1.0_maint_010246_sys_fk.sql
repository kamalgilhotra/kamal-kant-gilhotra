/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010246_sys_fk.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/21/2013 Lee Quinn      Point Release
||============================================================================
*/

alter table PP_VERIFY_USER_COMMENTS
   add constraint PP_VER_COMMENTS_CAT_ID_FK
       foreign key (COMMENT_CATEGORY_ID)
       references PP_VERIFY_COMMENTS_CATEGORY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (527, 0, 10, 4, 1, 0, 10246, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010246_sys_fk.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
