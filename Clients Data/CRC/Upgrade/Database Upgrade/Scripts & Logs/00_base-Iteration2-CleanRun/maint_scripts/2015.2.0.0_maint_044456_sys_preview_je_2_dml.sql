
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044456_sys_preview_je_2_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 08/05/2015 Andrew Scott   New preview JE feature
||============================================================================
*/

insert into pp_processes
(process_id, description, long_description, executable_file, version, allow_concurrent)
select max_id + 1, 'Preview Journal Entries', 'Preview Journal Entries', 'ssp_preview_je.exe', '2015.2.0.0', 1
from 
( select max(process_id) max_id from pp_processes );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2749, 0, 2015, 2, 0, 0, 044456, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044456_sys_preview_je_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;