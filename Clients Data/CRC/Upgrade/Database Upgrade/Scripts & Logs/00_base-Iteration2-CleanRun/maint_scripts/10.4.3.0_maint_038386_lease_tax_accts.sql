/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_038386_lease_tax_accts.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 06/10/2014 Kyle Peterson  Point Release
||============================================================================
*/

--Make staging table into "real table"
alter table LS_MONTHLY_TAX_STG
   add (TIME_STAMP date,
        USER_ID    varchar2(18));

alter table LS_MONTHLY_TAX_STG rename to LS_MONTHLY_TAX;

--Add accounts to taxes and FK to GL accounts
alter table LS_TAX_LOCAL
   add (ACCRUAL_ACCT_ID number(22,0),
        EXPENSE_ACCT_ID number(22,0));

alter table LS_TAX_LOCAL
   add constraint FK_LS_TAX_LOCAL2
       foreign key (ACCRUAL_ACCT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_TAX_LOCAL
   add constraint FK_LS_TAX_LOCAL3
       foreign key (EXPENSE_ACCT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

comment on column LS_TAX_LOCAL.ACCRUAL_ACCT_ID is 'The GL Account for accruals for this particular tax.';
comment on column LS_TAX_LOCAL.EXPENSE_ACCT_ID is 'The GL Account for expense for this particular tax.';

--Get rid of Accrual type limitations
alter table LS_MONTHLY_ACCRUAL_STG drop constraint FK_LS_MONTHLY_ACCRUAL_STG2;
drop table LS_ACCRUAL_TYPE;

--Add accounts to exec/cont buckets and fk to gl transaction
alter table LS_RENT_BUCKET_ADMIN
   add (ACCRUAL_ACCT_ID number(22,0),
        EXPENSE_ACCT_ID number(22,0));

comment on column LS_RENT_BUCKET_ADMIN.ACCRUAL_ACCT_ID is 'The GL Account for accruals for this particular bucket.';
comment on column LS_RENT_BUCKET_ADMIN.EXPENSE_ACCT_ID is 'The GL Account for expense for this particular bucket.';

alter table LS_RENT_BUCKET_ADMIN
   add constraint FK_LS_RENT_BUCKET_ADMIN1
       foreign key (ACCRUAL_ACCT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_RENT_BUCKET_ADMIN
   add constraint FK_LS_RENT_BUCKET_ADMIN2
       foreign key (EXPENSE_ACCT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

--Eliminate the payment types table and its FKs
alter table LS_PAYMENT_LINE drop constraint FK_LS_PAYMENT_LINE3;
alter table LS_INVOICE_LINE drop constraint FK_LS_INVOICE_LINE2;
drop table LS_PAYMENT_TYPE;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1195, 0, 10, 4, 3, 0, 38386, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038386_lease_tax_accts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
