/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051330_lessor_04_delete_add_keywords_dml
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/06/2018 Anand R        Delete keywords for LT receivable and ST receivable. 
||                                      Add two new ones for deferred rent and accrued rent
||============================================================================
*/

delete from pp_journal_keywords where lower(keyword) = 'p_lsr_ltd' and keyword_type = 1;

delete from pp_journal_keywords where lower(keyword) = 'p_lsr_std' and keyword_type = 1;

INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_dfr' AS keyword,
         'Lessor Deferred Rent' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.deferred_rent_acct_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_dfr');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_acr' AS keyword,
         'Lessor Accrued Rent' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.accrued_rent_acct_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_acr');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6442, 0, 2017, 4, 0, 0, 51330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051330_lessor_04_delete_add_keywords_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;