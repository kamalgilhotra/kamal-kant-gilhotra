/*
||===========================================================================================
|| Application: PowerPlan
|| File Name:   maint_043029_cwip_wo_extend_option_value_length_ddl.sql
||===========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||===========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ------------------------------------------------
|| 2015.1.0.0 02/24/2015 Luke Warren         Extend Option Value Length for Month End Options
||===========================================================================================
*/

-- Update option_value to hold 254 characters
ALTER TABLE pp_month_end_options MODIFY option_value VARCHAR2(254);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2323, 0, 2015, 1, 0, 0, 043029, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043029_cwip_wo_extend_option_value_length_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;