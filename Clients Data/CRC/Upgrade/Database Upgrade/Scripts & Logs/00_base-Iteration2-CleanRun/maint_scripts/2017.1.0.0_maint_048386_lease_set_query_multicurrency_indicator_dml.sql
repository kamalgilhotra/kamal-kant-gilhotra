/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048386_lease_set_query_multicurrency_indicator_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/02/2017 Josh Sandler     Set multicurrency indicator for lease queries
||============================================================================
*/

UPDATE pp_any_query_criteria
SET is_multicurrency = 1
WHERE description IN (
'ILR by Set of Books (CURRENT Revision)',
'Lease Balance Sheet Detail by Asset (Company Currency)',
'Lease Balance Sheet Span by Asset (Company Currency)',
'Lease GL Account Balances by Asset (Company Currency)',
'Lease Payment Detail by Asset',
'Lease Payment Detail by Company',
'Lease Payment Header and Invoice Detail'
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3620, 0, 2017, 1, 0, 0, 48386, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048386_lease_set_query_multicurrency_indicator_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;