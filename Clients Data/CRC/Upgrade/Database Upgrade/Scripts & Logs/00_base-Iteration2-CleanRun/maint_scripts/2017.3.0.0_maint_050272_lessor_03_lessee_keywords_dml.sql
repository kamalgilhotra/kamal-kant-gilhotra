/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050272_lessor_03_lessee_keywords_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 01/02/2018 Anand R        Add Lessee keywords
||============================================================================
*/

-- COMPANY
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_co' AS keyword,
         'Lease Company' AS description,
         'select co.gl_company_no  from ls_asset a, company_setup co  where a.ls_asset_id=<arg1>  and a.company_id = co.company_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_co');

-- COMPANY (POST)
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_co2' AS keyword,
         'Lease Company (POST)' AS description,
         'select co.gl_company_no  from ls_asset a, company_setup co  where a.ls_asset_id= (select ls_asset_id from ls_cpr_asset_map where asset_id = <arg1>)  and a.company_id = co.company_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_co2');

-- COMPANY (TRF)
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_trfco' AS keyword,
         'Lease Company (Transfer From)' AS description,
         'select c.gl_company_no from pend_transaction a, company_setup c where a.company_id = c.company_id and a.pend_trans_id = <arg1>' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         8 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_trfco');

-- COMPANY (TRT)
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_trtco' AS keyword,
         'Lease Company (Transfer To)' AS description,
         'select co.gl_company_no from pend_transaction trt, company_setup co where trt.company_id = co.company_id and trt.ldg_asset_id = <arg1>' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         8 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_trtco');

-- CAPITAL ACCOUNT
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct1' AS keyword,
         'Lease Capital Property Account' AS description,
         'select gl.external_account_code   from ls_asset la, ls_ilr_account ilr, gl_account gl   where ls_asset_id = <arg1>   and la.ilr_id = ilr.ilr_id   and gl.gl_account_id = ilr.cap_asset_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct1');

-- ST OBLIG
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct2' AS keyword,
         'Lease ST Obligation Account' AS description,
         'select gl.external_account_code  from ls_ilr_account ilra, ls_asset a, gl_account gl   where a.ls_asset_id=<arg1>          and a.ilr_id=ilra.ilr_id     and ilra.st_oblig_account_id = gl.gl_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct2');

-- LT OBLIG
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct3' AS keyword,
         'Lease LT Obligation Account' AS description,
         'select gl.external_account_code    from ls_ilr_account ilra, ls_asset a, gl_Account gl  where a.ls_asset_id=<arg1>          and a.ilr_id=ilra.ilr_id     and ilra.lt_oblig_account_id = gl.gl_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct3');

-- AP ON ILR ACCT
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct4' AS keyword,
         'Lease Payable Account' AS description,
         'select gl.external_account_code    from ls_ilr_account ilra, ls_asset a, gl_Account gl  where a.ls_asset_id=<arg1>          and a.ilr_id=ilra.ilr_id     and ilra.ap_account_id = gl.gl_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct4');

-- DEPR RESERVE
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct5' AS keyword,
         'Lease Reserve Account' AS description,
         'select gl.external_account_code from depr_group, gl_account gl where depr_group.reserve_acct_id = gl.gl_account_id and depr_group_id = (select depr_group_id from ls_asset where ls_asset_id = <arg1>)' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct5');

-- DEPR EXPENSE
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct6' AS keyword,
         'Lease Depr Expense Account' AS description,
         'select gl.external_account_code from depr_group, gl_account gl where depr_group.expense_acct_id = gl.gl_account_id and depr_group_id = (select depr_group_id from ls_asset where ls_asset_id = <arg1>)' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct6');

-- INTEREST EXPENSE
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct7' AS keyword,
         'Expense Account' AS description,
         'select gl.external_account_code    from ls_ilr_account ilra, ls_asset a, gl_Account gl  where a.ls_asset_id=<arg1>          and a.ilr_id=ilra.ilr_id     and ilra.int_expense_account_id = gl.gl_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct7');

-- INTEREST ACCRUAL ON ILR
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct8' AS keyword, 
		 'Lease Interest Accrual Account' AS description, 
		 'select nvl(gl.external_account_code,'' '')      from ls_ilr_account ilra, ls_asset a, gl_Account gl    where a.ls_asset_id=<arg1>            and a.ilr_id=ilra.ilr_id       and ilra.int_accrual_account_id = gl.gl_account_id' AS sqls,
		 1 AS keyword_type,
		 'all' AS valid_elements,
		 2 AS bind_arg_code_id1,
		 SYSDATE AS time_stamp,
		 'PWRPLANT' AS user_id
  FROM dual 
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct8');

-- DEPR RESERVE (POST)
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acct9' AS keyword,
		 'Lease Reserve Acct (POST)' AS description,
		 'select gl.external_account_code from depr_group, gl_account gl where depr_group.reserve_acct_id = gl.gl_account_id and depr_group_id = (select depr_group_id from ls_asset where ls_asset_id = (SELECT ls_asset_id FROM ls_cpr_asset_map WHERE asset_id = <arg1>))' AS sqls,
		 1 AS keyword_type,
		 'all' AS valid_elements,
		 2 AS bind_arg_code_id1,
		 SYSDATE AS time_stamp,
		 'PWRPLANT' AS user_id
  FROM dual 
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acct9');

-- CAPITAL ACCOUNT (POST)
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acc10' AS keyword,
         'Lease Capital Acct (POST)' AS description,
         'select nvl(gl.external_account_code, '' '') from ls_asset la, ls_ilr_account ilr, gl_account gl   where la.ls_asset_id = (SELECT ls_asset_id FROM ls_cpr_asset_map WHERE asset_id = <arg1>) and la.ilr_id = ilr.ilr_id   and gl.gl_account_id = ilr.cap_asset_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acc10');

-- RESIDUAL DEBIT ILR ACCT
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acc14' AS keyword,
		 'Lease Residual Debit Account' AS description,
		 'select nvl(gl.external_account_code, '' '')     from ls_asset la, ls_ilr_account ilr, gl_account gl     where ls_asset_id = <arg1>     and la.ilr_id = ilr.ilr_id     and gl.gl_account_id = ilr.res_debit_account_id' AS sqls,
		 1 AS keyword_type,
		 'all' AS valid_elements,
		 2 AS bind_arg_code_id1,
		 SYSDATE AS time_stamp,
		 'PWRPLANT' AS user_id
  FROM dual 
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acc14');

-- RESIDUAL CREDIT ILR ACCT
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acc15' AS keyword,
		 'Lease Residual Credit Account' AS description,
		 'select nvl(gl.external_account_code, '' '')     from ls_asset la, ls_ilr_account ilr, gl_account gl     where ls_asset_id = <arg1>     and la.ilr_id = ilr.ilr_id     and gl.gl_account_id = ilr.res_credit_account_id' AS sqls,
		 1 AS keyword_type,
		 'all' AS valid_elements,
		 2 AS bind_arg_code_id1,
		 SYSDATE AS time_stamp,
		 'PWRPLANT' AS user_id
  FROM dual 
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acc15');

-- GAIN ACCT FROM DG
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acc16' AS keyword,
         'Lease Gain Account' AS description,
         'select gl.external_account_code from depr_group, gl_account gl where depr_group.gain_acct_id = gl.gl_account_id and depr_group_id = (select depr_group_id from ls_asset where ls_asset_id = <arg1>)' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acc16');

-- LOSS ACCT FROM DG
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acc17' AS keyword,
         'Lease Loss Account' AS description,
         'select gl.external_account_code from depr_group, gl_account gl where depr_group.loss_acct_id = gl.gl_account_id and depr_group_id = (select depr_group_id from ls_asset where ls_asset_id = <arg1>)' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acc17');

-- GAIN ACCT FROM DG (POST)
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
  SELECT 'p_ls_acc18' AS keyword,
         'Lease Gain Account' AS description,
         'select gl.external_account_code from depr_group, gl_account gl where depr_group.gain_acct_id = gl.gl_account_id and depr_group_id = (select depr_group_id from ls_asset where ls_asset_id = (select ls_asset_id from ls_cpr_asset_map where asset_id = <arg1>))' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         2 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         'PWRPLANT' AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_ls_acc18');

  --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4101, 0, 2017, 3, 0, 0, 50272, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050272_lessor_03_lessee_keywords_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 