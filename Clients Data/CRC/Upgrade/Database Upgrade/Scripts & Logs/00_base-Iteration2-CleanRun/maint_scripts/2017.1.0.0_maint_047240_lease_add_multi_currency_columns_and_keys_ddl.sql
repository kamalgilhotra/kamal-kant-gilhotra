/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047240_lease_add_multi_currency_columns_and_keys_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/10/2017 Charlie Shilling add columns to support multi-currency functionality
||============================================================================
*/

ALTER TABLE ls_lease
ADD contract_currency_id NUMBER(22,0);

COMMENT ON COLUMN ls_lease.contract_currency_id IS 'The contract currency used by this MLA and its underlying ILR and Assets.';

ALTER TABLE ls_lease
  ADD CONSTRAINT fk_mla_contract_currency FOREIGN KEY (
    contract_currency_id
  ) REFERENCES currency (
    currency_id
  )
/

ALTER TABLE ls_ilr_options
ADD in_service_exchange_rate NUMBER(22,8);

COMMENT ON COLUMN ls_ilr_options.in_service_exchange_rate IS 'The exchange rate used to convert from the lease contract currency to the company''s currency at the time that the ILR revision was put in-service. This is used to calculate a gain/loss as the exchange rate changes over time.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3429, 0, 2017, 1, 0, 0, 47240, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047240_lease_add_multi_currency_columns_and_keys_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	