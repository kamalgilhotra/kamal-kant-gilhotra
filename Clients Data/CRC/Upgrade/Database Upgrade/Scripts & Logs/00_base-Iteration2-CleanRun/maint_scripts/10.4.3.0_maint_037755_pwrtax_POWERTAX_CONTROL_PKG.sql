/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_037755_pwrtax_POWERTAX_CONTROL_PKG.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| PP Version Version   Date          Revised By    Reason for Change
|| ---------- --------- ------------- ------------- --------------------------
|| 10.4.3.0   1.01      05/29/2014    Andrew Scott  Added function for case management
||                                                  wksp to find the earliest range of
||                                                  trids to use for the new tax records
||============================================================================
*/


create or replace package POWERTAX_CONTROL_PKG as
--||============================================================================
--|| Application: PowerPlant
--|| Object Name: POWERTAX_CONTROL_PKG
--|| Description: Modifies and retrieves PowerTax information for the current database
--||              Also used to hold general tax utilities.
--||============================================================================
--|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| PP Version  Version    Date           Revised By     Reason for Change
--|| ----------  ---------  -------------  -------------  -------------------------
--|| 10.4.2.0    1.00       01/17/2014     Julia Breuer   Initial Creation
--|| 10.4.3.0    1.01       05/29/2014     Andrew Scott   Added function for case management
--||                                                      wksp to find the earliest range of
--||                                                      trids to use for the new tax records
--||============================================================================

   -- Sets the global version_id variable
   procedure P_SET_VERSION_ID(A_VERSION_ID in VERSION.VERSION_ID%type);

   -- Returns the current version_id
   function F_GET_VERSION_ID return number;

   -- For Copied Cases and Adding Vintages, finds a range of tax record ids to use
   -- and applies it to the renumbering table.
   function F_RENUMBER_TRIDS return number;

end POWERTAX_CONTROL_PKG;
/

create or replace package body POWERTAX_CONTROL_PKG as

   G_VERSION_ID VERSION.VERSION_ID%type;

   --**************************************************************************************
   -- p_set_version_id
   --**************************************************************************************
   procedure P_SET_VERSION_ID(A_VERSION_ID in VERSION.VERSION_ID%type) is

   begin
      G_VERSION_ID := A_VERSION_ID;
   end P_SET_VERSION_ID;

   --**************************************************************************************
   -- f_get_version_id
   --**************************************************************************************
   function F_GET_VERSION_ID return number is

   begin
      return NVL(G_VERSION_ID, 0);
   end F_GET_VERSION_ID;

   --**************************************************************************************
   -- f_renumber_trids
   --**************************************************************************************
   function F_RENUMBER_TRIDS return number is

      l_version_count PLS_INTEGER := 0;
      l_version_key PLS_INTEGER := 0;
      cur_iter PLS_INTEGER := 0;
      cur_trid PLS_INTEGER;
      prior_trid PLS_INTEGER;

      cursor l_trid_cur is
         select tax_record_id
         from tax_record_control
         order by tax_record_id ;

      l_trid_rec l_trid_cur%rowtype;

   begin

      select count(*)
      into l_version_count
      from temp_renumber_trids;

      if l_version_count is null or l_version_count = 0 then
         ----this function should only be called immediately after the refreshing of contents of
         RAISE_APPLICATION_ERROR(-20001, 'Unable to retrieve the number of rows in temp_renumber_trids. ');
         return - 1;
      end if;

      open l_trid_cur;

      loop
         fetch l_trid_cur
            into l_trid_rec;
         exit when l_trid_cur%notfound;

         cur_iter := cur_iter + 1;

         if cur_iter is null or cur_iter = 0 then
            ----this function should only be called immediately after the refreshing of contents of
            RAISE_APPLICATION_ERROR(-20002, 'Cursor iteration variable not correctly initiated');
            return - 1;
         elsif cur_iter = 1 then
            ----grab the trid for loop 2 to use as the prior trid
            cur_trid := l_trid_rec.tax_record_id;
            ----skip the first row
            continue;
         else
            ----save off the prior loop iteration trid
            prior_trid := cur_trid;
         end if;
         
         cur_trid := l_trid_rec.tax_record_id;

         if cur_trid - prior_trid > l_version_count then
            ----this means that a range has been found to renumber
            ----the trids.  So set the version key and exit the loop.
            l_version_key := prior_trid;
            exit;
         end if;

      end loop;

      close l_trid_cur;

      if l_version_key = 0 then
         ----this means that there was no available range found.  so just
         ----use the max iteration as the version key (update statement will do
         ----rownum + version key, so this should not error.
         l_version_key := cur_trid;
      end if;

      ----one last check to make sure a version key was found.
      if l_version_key is null or l_version_key = 0 then 
            RAISE_APPLICATION_ERROR(-20003, 'Failed to find the version key to use for the update.');
            return - 1;
      end if;

      execute immediate 'update temp_renumber_trids set rnum = rnum + '||l_version_key;

      return 0;

   exception
      when others then
         RAISE_APPLICATION_ERROR(-20000,'Failed Renumber Tax Record IDs. Error: '||sqlerrm);
         return -1;

   end F_RENUMBER_TRIDS;

end POWERTAX_CONTROL_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1163, 0, 10, 4, 3, 0, 37755, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037755_pwrtax_POWERTAX_CONTROL_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;