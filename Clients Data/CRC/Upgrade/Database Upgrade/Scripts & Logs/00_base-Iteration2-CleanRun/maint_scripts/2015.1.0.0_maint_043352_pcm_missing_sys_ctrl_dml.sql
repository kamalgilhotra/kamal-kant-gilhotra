/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_043352_pcm_missing_sys_ctrl_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 	  03/24/2015 Ryan Oliveria		 PCM backout
||==========================================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense User Object',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" activates the Funding Project Tax Expense tab using the justification user object. "No" makes the tab invisible and disabled.',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense User Object')));

insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense User Object - WO',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" activates the Work Order Tax Expense tab using the justification user object. "No" makes the tab invisible and disabled.',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense User Object - WO')));



insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test FPs - Always',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" always runs the tax expense test on the funding project. "No" only runs it when the current answer might change',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FPs - Always')));

insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test WOs - Always Run',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" always runs the tax expense test on the work order. "No" only runs it when the current answer might change',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WOs - Always Run')));


insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Display Related Work Order tab',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" will display the "Related Work Orders" tab on the work order details',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Display Related Work Order tab')));


/* From maint_010342_010343*/
insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test WO - New Rev',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" run tax expense test upon creating a new revision on a Work Order. Default is "No".',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WO - New Rev')));

insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test FP - New Rev',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" run tax expense test upon creating a new revision on a Funding Project. Default is "No".',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - New Rev')));



insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test WO - Monthly Ests',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" run tax expense test upon updating Work Order Monthly Estimates. Default is "No".',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WO - Monthly Ests')));

insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test FP - Monthly Ests',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" run tax expense test upon updating Funding Project Monthly Estimates. Default is "No".',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - Monthly Ests')));



insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test FP - Monthly Fcst',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" run tax expense test upon updating Funding Project Monthly Estimates in the Forecast window. Default is "No".',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - Monthly Fcst')));



insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test WO - Unit Ests',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" run tax expense test upon updating Work Order Unit Estimates. Default is "No".',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test WO - Unit Ests')));

insert into PP_SYSTEM_CONTROL_COMPANY (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1,
       'Tax Expense Test FP - Unit Ests',
	   'no',
	   'dw_yes_no;1',
	   '"Yes" run tax expense test upon updating Funding Project Unit Estimates. Default is "No".',
	   -1
  from dual
 where not exists (
       select 1
         from PP_SYSTEM_CONTROL_COMPANY
	    where lower(trim(CONTROL_NAME)) = lower(trim('Tax Expense Test FP - Unit Ests')));


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2440, 0, 2015, 1, 0, 0, 043352, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043352_pcm_missing_sys_ctrl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;