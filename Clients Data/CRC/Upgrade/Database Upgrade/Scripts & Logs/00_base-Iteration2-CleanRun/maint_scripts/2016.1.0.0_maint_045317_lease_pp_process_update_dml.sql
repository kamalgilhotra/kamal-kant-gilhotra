/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045317_lease_pp_process_update_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 02/08/2016 Will Davis			 Added a lease SSP executable - 
||											 update pp processes
||============================================================================
*/ 

update pp_processes
set version = '2016.1.0.0'
where upper(description) = 'LESSEE CALCULATIONS';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3046, 0, 2016, 1, 0, 0, 045317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045317_lease_pp_process_update_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;