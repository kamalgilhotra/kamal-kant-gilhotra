/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038215_pwrtax_rpts_conv4.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/24/2014 Anand Rajashekar    script changes to for report conversion
||============================================================================
*/

--Insert new dynamic filter

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (175, 'FCST-Tax Book', 'dw', 'tax_forecast_output.tax_book_id', null, 'dw_tax_book_filter', 'tax_book_id',
    'description', 'N', 0, 0, null, null, 'PWRPLANT', TO_DATE('24-JUL-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (176, 'FCST-Vintage', 'dw', 'tax_record_control.vintage_id', null, 'dw_tax_vintage_by_version_filter', 'vintage_id',
    'description', 'N', 0, 0, null, null, 'PWRPLANT', TO_DATE('24-JUL-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (177, 'FCST-Tax Class', 'dw', 'tax_record_control.tax_class_id', null, 'dw_tax_class_by_version_filter',
    'tax_class_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT', TO_DATE('24-JUL-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (178, 'FCST-Forecast Version', 'dw', 'tax_forecast_version.tax_forecast_version_id', null,
    'dw_tax_fcst_version_by_version_filter', 'tax_forecast_version_id', 'description', 'N', 1, 0, null, null, 'PWRPLANT',
    TO_DATE('24-JUL-14', 'DD-MON-RR'), 0, 0, 0);


--Updates for report filter 71

delete from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 71 ;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,105,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,157,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,176,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,177,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,178,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (71,170,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));


--Updates for report filter 73

delete from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 73 ;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,101,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,105,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,176,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,177,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,178,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (73,170,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));


--Updates for report filter 62

delete from PP_DYNAMIC_FILTER_RESTRICTIONS where RESTRICTION_ID = 55;

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (55, 170, 178, 'tax_forecast_output.tax_forecast_version_id', null, 'PWRPLANT', TO_DATE('21-JUL-14', 'DD-MON-RR'));


update PP_DYNAMIC_FILTER set LABEL = 'FCST-Company' where FILTER_ID = 142;

update PP_REPORTS_FILTER set FILTER_UO_NAME = 'uo_ppbase_tab_filter_dynamic' where PP_REPORT_FILTER_ID = 62;

delete from pp_dynamic_filter_mapping where pp_report_filter_id = 62 and filter_id = 170 ;

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (77, TO_DATE('22-JUL-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - Forecasting - TaxBook', null,
    'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (77,157,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (77,175,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (77,176,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (77,177,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (77,178,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (77,170,'PWRPLANT',to_date('22-JUL-14','DD-MON-RR'));

update PP_REPORTS set PP_REPORT_FILTER_ID = 77 where REPORT_ID = 401001;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1276, 0, 10, 4, 3, 0, 38215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038215_pwrtax_rpts_conv4.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
