/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051996_lessee_01_retire_dynamic_validation_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2018.1.0.0 07/16/2018 Crystal Yura     Expand column to allow up to 256 (max allowed by PB)
||============================================================================
*/

ALTER TABLE wo_validation_type MODIFY FUNCTION  VARCHAR2(256);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8182, 0, 2018, 1, 0, 0, 51996, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051996_lessee_01_retire_dynamic_validation_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
