/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051355_lessee_01_ls_ilr_schedule_fx_tmp_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.2  09/13/2018 Sarah Byers      Disclosure Reporting Temp Tables
||============================================================================
*/

-- ls_ilr_schedule_fx_temp 
create GLOBAL TEMPORARY TABLE ls_ilr_schedule_fx_temp (
  LS_CUR_TYPE               number(22,0) not null,
  ILR_ID                    number(22,0) not null,
  REVISION                  number(22,0) not null,
  SET_OF_BOOKS_ID           number(22,0) not null,
  MONTH                     date not null,
  COMPANY_ID                number(22,0),
  LEASE_ID                  number(22,0),
  IS_OM                     number(22,0),
  ISO_CODE                  varchar2(3),
  CURRENCY_DISPLAY_SYMBOL   varchar2(35),
  RESIDUAL_AMOUNT           number(22,2), 
  TERM_PENALTY              number(22,2), 
  BPO_PRICE                 number(22,2), 
  BEG_CAPITAL_COST          number(22,2),
  END_CAPITAL_COST          number(22,2),
  BEG_OBLIGATION            number(22,2), 
  END_OBLIGATION            number(22,2), 
  BEG_LT_OBLIGATION         number(22,2), 
  END_LT_OBLIGATION         number(22,2), 
  BEG_LIABILITY             number(22,2),
  END_LIABILITY             number(22,2),
  BEG_LT_LIABILITY          number(22,2),
  END_LT_LIABILITY          number(22,2),
  BEG_NET_ROU_ASSET         number(22,2),
  END_NET_ROU_ASSET         number(22,2),
  INTEREST_ACCRUAL          number(22,2), 
  PRINCIPAL_ACCRUAL         number(22,2), 
  INTEREST_PAID             number(22,2),
  PRINCIPAL_PAID            number(22,2),
  EXECUTORY_PAID1           number(22,2),
  EXECUTORY_PAID2           number(22,2),
  EXECUTORY_PAID3           number(22,2),
  EXECUTORY_PAID4           number(22,2),
  EXECUTORY_PAID5           number(22,2),
  EXECUTORY_PAID6           number(22,2),
  EXECUTORY_PAID7           number(22,2),
  EXECUTORY_PAID8           number(22,2),
  EXECUTORY_PAID9           number(22,2),
  EXECUTORY_PAID10          number(22,2),
  CONTINGENT_PAID1          number(22,2),
  CONTINGENT_PAID2          number(22,2),
  CONTINGENT_PAID3          number(22,2),
  CONTINGENT_PAID4          number(22,2),
  CONTINGENT_PAID5          number(22,2),
  CONTINGENT_PAID6          number(22,2),
  CONTINGENT_PAID7          number(22,2),
  CONTINGENT_PAID8          number(22,2),
  CONTINGENT_PAID9          number(22,2),
  CONTINGENT_PAID10         number(22,2),
  EXECUTORY_ACCRUAL1        number(22,2), 
  EXECUTORY_ACCRUAL2        number(22,2), 
  EXECUTORY_ACCRUAL3        number(22,2), 
  EXECUTORY_ACCRUAL4        number(22,2), 
  EXECUTORY_ACCRUAL5        number(22,2), 
  EXECUTORY_ACCRUAL6        number(22,2), 
  EXECUTORY_ACCRUAL7        number(22,2), 
  EXECUTORY_ACCRUAL8        number(22,2), 
  EXECUTORY_ACCRUAL9        number(22,2), 
  EXECUTORY_ACCRUAL10       number(22,2),
  CONTINGENT_ACCRUAL1       number(22,2), 
  CONTINGENT_ACCRUAL2       number(22,2), 
  CONTINGENT_ACCRUAL3       number(22,2), 
  CONTINGENT_ACCRUAL4       number(22,2), 
  CONTINGENT_ACCRUAL5       number(22,2), 
  CONTINGENT_ACCRUAL6       number(22,2), 
  CONTINGENT_ACCRUAL7       number(22,2), 
  CONTINGENT_ACCRUAL8       number(22,2), 
  CONTINGENT_ACCRUAL9       number(22,2), 
  CONTINGENT_ACCRUAL10      number(22,2), 
  DEPR_EXPENSE              number(22,2),
  BEGIN_RESERVE             number(22,2),
  END_RESERVE               number(22,2),
  CURRENT_LEASE_COST        number(22,2), 
  BEG_DEFERRED_RENT         number(22,2), 
  DEFERRED_RENT             number(22,2), 
  END_DEFERRED_RENT         number(22,2), 
  BEG_ST_DEFERRED_RENT      number(22,2), 
  END_ST_DEFERRED_RENT      number(22,2), 
  PRINCIPAL_REMEASUREMENT   number(22,2), 
  LIABILITY_REMEASUREMENT   number(22,2), 
  INITIAL_DIRECT_COST       number(22,2), 
  INCENTIVE_AMOUNT          number(22,2), 
  BEG_PREPAID_RENT          number(22,2), 
  PREPAY_AMORTIZATION       number(22,2), 
  PREPAID_RENT              number(22,2), 
  END_PREPAID_RENT          number(22,2), 
  IDC_MATH_AMOUNT           number(22,2), 
  INCENTIVE_MATH_AMOUNT     number(22,2), 
  ROU_ASSET_REMEASUREMENT   number(22,2), 
  PARTIAL_TERM_GAIN_LOSS    number(22,2), 
  ADDITIONAL_ROU_ASSET      number(22,2), 
  EXECUTORY_ADJUST          number(22,2), 
  CONTINGENT_ADJUST         number(22,2), 
  BEG_ARREARS_ACCRUAL       number(22,2), 
  ARREARS_ACCRUAL           number(22,2), 
  END_ARREARS_ACCRUAL       number(22,2), 
  REMAINING_PRINCIPAL       number(22,2),
  BEG_NET_WAVG_RATE         number(22,8),
  CURR_NET_WAVG_RATE        number(22,8),
  REMEASUREMENT_DATE        date,
  CURRENCY_ID        number(22,0)
)
on commit preserve rows;

alter table ls_ilr_schedule_fx_temp add (
constraint pk_ls_ilr_schedule_fx_temp primary key (ls_cur_type, ilr_id, revision, set_of_books_id, month) );

comment on table ls_ilr_schedule_fx_temp is 'LS ILR Schedule FX Temp stores contract and company ILR Schedule results for Lessee Disclosure Reports.';
comment on column ls_ilr_schedule_fx_temp.ls_cur_type is 'System assigned identifier for a lease currency type: 1 = Contract, 2 = Company.';
comment on column ls_ilr_schedule_fx_temp.ilr_id is 'System assigned identifier for an ILR.';
comment on column ls_ilr_schedule_fx_temp.revision is 'System assigned identifier of a revision.'; 
comment on column ls_ilr_schedule_fx_temp.set_of_books_id is 'System assigned identifier of a set of books';
comment on column ls_ilr_schedule_fx_temp.month is  'The accounting month for the ILR.';
comment on column ls_ilr_schedule_fx_temp.company_id is 'System assigned identifier of a company.';
comment on column ls_ilr_schedule_fx_temp.lease_id is 'System assigned identifier of a lease.';
comment on column ls_ilr_schedule_fx_temp.is_om is 'Identifies whether the ILR is on or off balance sheet; 1 = O and M; 2 = Capital.';
comment on column ls_ilr_schedule_fx_temp.iso_code is  'The 3-character ISO standard code used to denote this currency';
comment on column ls_ilr_schedule_fx_temp.currency_display_symbol is 'The display symbol for the currency on the record.';
comment on column ls_ilr_schedule_fx_temp.residual_amount is 'The guaranteed residual amount.';
comment on column ls_ilr_schedule_fx_temp.term_penalty is 'The penalty amount for terminating the lease.';
comment on column ls_ilr_schedule_fx_temp.bpo_price is 'The bargain purchase amount.';
comment on column ls_ilr_schedule_fx_temp.beg_capital_cost is 'The beginning capital amount for the period.';
comment on column ls_ilr_schedule_fx_temp.end_capital_cost is 'The ending capital amount for the period.';
comment on column ls_ilr_schedule_fx_temp.beg_obligation is 'The beginning obligation for the period.';
comment on column ls_ilr_schedule_fx_temp.end_obligation is 'The ending obligation for the period.';
comment on column ls_ilr_schedule_fx_temp.beg_lt_obligation is 'The beginning long term obligation for the period.';
comment on column ls_ilr_schedule_fx_temp.end_lt_obligation is 'The ending long term obligation for the period.';
comment on column ls_ilr_schedule_fx_temp.beg_liability is 'The beginning liability for the period.';
comment on column ls_ilr_schedule_fx_temp.end_liability is 'The ending liability for the period.';
comment on column ls_ilr_schedule_fx_temp.beg_lt_liability is 'The beginning long term liability for the period.';
comment on column ls_ilr_schedule_fx_temp.end_lt_liability is 'The ending long term liability for the period.';
comment on column ls_ilr_schedule_fx_temp.beg_net_rou_asset is 'Begin Capital Cost less Begin Reserve for all Assets on ILR.';
comment on column ls_ilr_schedule_fx_temp.end_net_rou_asset is 'End Capital Cost less End Reserve for all Assets on ILR';
comment on column ls_ilr_schedule_fx_temp.interest_accrual is 'Records the amount of interest accrued.  Accruals occur every month regardless of payment schedule.';
comment on column ls_ilr_schedule_fx_temp.principal_accrual is 'Records the amount of principal accrued.  Accruals occur every month regardless of payment schedule.';
comment on column ls_ilr_schedule_fx_temp.interest_paid is 'The amount of interest paid in this period .';
comment on column ls_ilr_schedule_fx_temp.principal_paid is 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
comment on column ls_ilr_schedule_fx_temp.executory_paid1 is 'The executory paid amount associated with the executory bucket number 1.';
comment on column ls_ilr_schedule_fx_temp.executory_paid2 is 'The executory paid amount associated with the executory bucket number 2.';
comment on column ls_ilr_schedule_fx_temp.executory_paid3 is 'The executory paid amount associated with the executory bucket number 3.';
comment on column ls_ilr_schedule_fx_temp.executory_paid4 is 'The executory paid amount associated with the executory bucket number 4.';
comment on column ls_ilr_schedule_fx_temp.executory_paid5 is 'The executory paid amount associated with the executory bucket number 5.';
comment on column ls_ilr_schedule_fx_temp.executory_paid6 is 'The executory paid amount associated with the executory bucket number 6.';
comment on column ls_ilr_schedule_fx_temp.executory_paid7 is 'The executory paid amount associated with the executory bucket number 7.';
comment on column ls_ilr_schedule_fx_temp.executory_paid8 is 'The executory paid amount associated with the executory bucket number 8.';
comment on column ls_ilr_schedule_fx_temp.executory_paid9 is 'The executory paid amount associated with the executory bucket number 9.';
comment on column ls_ilr_schedule_fx_temp.executory_paid10 is 'The executory paid amount associated with the executory bucket number 10.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid1 is 'The contingent paid amount associated with the contingent bucket number 1.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid2 is 'The contingent paid amount associated with the contingent bucket number 2.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid3 is 'The contingent paid amount associated with the contingent bucket number 3.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid4 is 'The contingent paid amount associated with the contingent bucket number 4.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid5 is 'The contingent paid amount associated with the contingent bucket number 5.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid6 is 'The contingent paid amount associated with the contingent bucket number 6.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid7 is 'The contingent paid amount associated with the contingent bucket number 7.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid8 is 'The contingent paid amount associated with the contingent bucket number 8.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid9 is 'The contingent paid amount associated with the contingent bucket number 9.';
comment on column ls_ilr_schedule_fx_temp.contingent_paid10 is 'The contingent paid amount associated with the contingent bucket number 10.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual1 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual2 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual3 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual4 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual5 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual6 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual7 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual8 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual9 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.executory_accrual10 is 'The amount of executory costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual1 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual2 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual3 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual4 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual5 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual6 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual7 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual8 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual9 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.contingent_accrual10 is 'The amount of contingent costs accrued in this period.';
comment on column ls_ilr_schedule_fx_temp.depr_expense is 'The depreciation expense for the month.';
comment on column ls_ilr_schedule_fx_temp.begin_reserve is 'The beginning reserve for the ILR.';
comment on column ls_ilr_schedule_fx_temp.end_reserve is 'The ending reserve for this ILR.';
comment on column ls_ilr_schedule_fx_temp.current_lease_cost is 'The total amount funded on the ILR in the given month.';
comment on column ls_ilr_schedule_fx_temp.beg_deferred_rent is 'The beginning deferred rent amount for the period.';
comment on column ls_ilr_schedule_fx_temp.deferred_rent is 'The deferred rent amount for the period.';
comment on column ls_ilr_schedule_fx_temp.end_deferred_rent is 'The ending deferred rent amount for the period.';
comment on column ls_ilr_schedule_fx_temp.beg_st_deferred_rent is 'The beginning short term deferred rent amount for the period.';
comment on column ls_ilr_schedule_fx_temp.end_st_deferred_rent is 'The ending short term deferred rent amount for the period.';
comment on column ls_ilr_schedule_fx_temp.principal_remeasurement is 'The principal/obligation adjustment in the first period of a remeasurement.';
comment on column ls_ilr_schedule_fx_temp.liability_remeasurement is 'The liability adjustment in the first period of a remeasurement.';
comment on column ls_ilr_schedule_fx_temp.initial_direct_cost is 'Holds Initial Direct Cost Amount for ILR Schedule.';
comment on column ls_ilr_schedule_fx_temp.incentive_amount is 'Holds Incentive Amount for ILR Schedule.';
comment on column ls_ilr_schedule_fx_temp.beg_prepaid_rent is 'The beginning prepaid rent for the period.';
comment on column ls_ilr_schedule_fx_temp.prepay_amortization is 'The prepayment amortization amount for the period.';
comment on column ls_ilr_schedule_fx_temp.prepaid_rent is 'The prepaid rent amount for the period.';
comment on column ls_ilr_schedule_fx_temp.end_prepaid_rent is 'The ending prepaid rent for the period.'; 
comment on column ls_ilr_schedule_fx_temp.idc_math_amount is 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user.';
comment on column ls_ilr_schedule_fx_temp.incentive_math_amount is 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user.';
comment on column ls_ilr_schedule_fx_temp.rou_asset_remeasurement is 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
comment on column ls_ilr_schedule_fx_temp.partial_term_gain_loss is 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
comment on column ls_ilr_schedule_fx_temp.additional_rou_asset is 'Calculation of ROU Asset based on Quantity Retirement Method.';
comment on column ls_ilr_schedule_fx_temp.executory_adjust is 'Summated value of all Executory Adjusted values, by ILR.';
comment on column ls_ilr_schedule_fx_temp.contingent_adjust is 'Summated value of all Contingent Adjusted values, by ILR.';
comment on column ls_ilr_schedule_fx_temp.beg_arrears_accrual is 'The beginning arrears accrual balance for the period.';
comment on column ls_ilr_schedule_fx_temp.arrears_accrual is 'The arrears accrual amount for the period.';
comment on column ls_ilr_schedule_fx_temp.end_arrears_accrual is 'The ending arrears accrual balance for the period.';
comment on column ls_ilr_schedule_fx_temp.remaining_principal is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc';
comment on column ls_ilr_schedule_fx_temp.beg_net_wavg_rate is 'The net weighted average rate for the ILR for the prior period.';
comment on column ls_ilr_schedule_fx_temp.curr_net_wavg_rate is 'The net weighted average rate for the ILR for the current period';
comment on column ls_ilr_schedule_fx_temp.remeasurement_date is 'The remeasurement date of the ILR/revision.';
comment on column ls_ilr_schedule_fx_temp.currency_id is 'System assigned identifier of a currency.';

-- ls_curr_fx_temp 
create GLOBAL TEMPORARY TABLE ls_curr_fx_temp (
  CURRENCY_FROM             number(22,0) not null,
  CURRENCY_TO               number(22,0) not null,
  MONTH                     date,
  ACT_MONTH_RATE            number(22,8),
  AVG_MONTH_RATE            number(22,8),
  ACT_NOW_RATE              number(22,8)
)
on commit preserve rows;

alter table ls_curr_fx_temp add (
constraint pk_ls_curr_fx_temp primary key (currency_from, currency_to, month));

comment on table ls_curr_fx_temp is 'LS Curr FX Temp stores the currency exchange rates for Lessee Disclosure Reports.';
comment on column ls_curr_fx_temp.currency_from is 'System assigned identifier for the starting currency.';
comment on column ls_curr_fx_temp.currency_to is 'System assigned identifier for the ending currency.';
comment on column ls_curr_fx_temp.month is 'The effective month for the exchange rate.';
comment on column ls_curr_fx_temp.act_month_rate is 'The actual exchange rate for the month (exchange rate type 1).';
comment on column ls_curr_fx_temp.avg_month_rate is 'The average exchange rate for the month (exchange rate type 4).';
comment on column ls_curr_fx_temp.act_now_rate is 'The most recent actual exchnage rate as of runtime (exchange rate type 1).';

-- ls_ilr_war_fx_temp 
create GLOBAL TEMPORARY TABLE ls_ilr_war_fx_temp (
  ILR_ID                   number(22,0) not null,
  REVISION                 number(22,0) not null,
  SET_OF_BOOKS_ID          number(22,0) not null,
  GROSS_WEIGHTED_AVG_RATE  number(22,8),
  NET_WEIGHTED_AVG_RATE    number(22,8),
  IN_SERVICE_EXCHANGE_RATE number(22,8),
  EFFECTIVE_MONTH          date
)
on commit preserve rows;

alter table ls_ilr_war_fx_temp add (
constraint pk_ls_ilr_war_fx_temp primary key (ilr_id, revision, set_of_books_id));

comment on table ls_ilr_war_fx_temp is 'LS WAR FX Temp stores the weighted average currency exchange rates for Lessee Disclosure Reports.';
comment on column ls_ilr_war_fx_temp.ilr_id is 'System-assigned identifier of an ILR.';
comment on column ls_ilr_war_fx_temp.revision is 'System-assigned identifier of a revision.';
comment on column ls_ilr_war_fx_temp.set_of_books_id is 'System-assigned identifier for the set of books.';
comment on column ls_ilr_war_fx_temp.gross_weighted_avg_rate is 'The weighted average exchange rate calculated based on the Gross ROU Amount corresponding to each In Service Date Rate for an ILR.';
comment on column ls_ilr_war_fx_temp.net_weighted_avg_rate is 'The weighted average exchange rate calculated based on the Net ROU Amount corresponding to each In Service Date Rate for an ILR.';
comment on column ls_ilr_war_fx_temp.in_service_exchange_rate is 'The exchange rate for the In Service Date or Remeasurement Date on the ILR/revision.';
comment on column ls_ilr_war_fx_temp.effective_month is 'The effective month for the weighted average rates.';

-- ls_ilr_depr_fx_temp;
create GLOBAL TEMPORARY TABLE ls_ilr_depr_fx_temp (
  ILR_ID                    number(22,0) not null,
  REVISION                  number(22,0) not null,
  SET_OF_BOOKS_ID           number(22,0) not null,
  MONTH                     date not null,
  DEPR_EXPENSE              number(22,2),
  BEGIN_RESERVE             number(22,2),
  END_RESERVE               number(22,2)
)
on commit preserve rows;

alter table ls_ilr_depr_fx_temp add (
constraint pk_ls_ilr_depr_fx_temp primary key (ilr_id, revision, set_of_books_id, month) );

comment on table ls_ilr_depr_fx_temp is 'LS ILR Depr Temp stores the depreciation amounts for ILRs for Lessee Disclosure Reports.';
comment on column ls_ilr_depr_fx_temp.ilr_id is 'System-assigned identifier of an ILR.';
comment on column ls_ilr_depr_fx_temp.revision is 'System-assigned identifier of a revision.';
comment on column ls_ilr_depr_fx_temp.set_of_books_id is 'System-assigned identifier of a revision.';
comment on column ls_ilr_depr_fx_temp.month is 'The accounting month.';
comment on column ls_ilr_depr_fx_temp.depr_expense is 'The depreciation expense for the month.';
comment on column ls_ilr_depr_fx_temp.begin_reserve is 'The beginning reserve for the ILR.';
comment on column ls_ilr_depr_fx_temp.end_reserve is 'The ending reserve for the ILR.';


-- ls_ilr_future_payments_fx_temp 
create GLOBAL TEMPORARY TABLE ls_ilr_future_payments_fx_temp (
  LS_CUR_TYPE               number(22,0) not null,
  ILR_ID                    number(22,0) not null,
  REVISION                  number(22,0) not null,
  SET_OF_BOOKS_ID           number(22,0) not null,
  COMPANY_ID                number(22,0),
  LEASE_ID                  number(22,0), 
  ISO_CODE                  varchar2(3),
  CURRENCY_DISPLAY_SYMBOL   varchar2(35),
  CURRENCY_ID        number(22,0),
  TOTAL_PAYMENT             number(22,2)
)
on commit preserve rows;

alter table ls_ilr_future_payments_fx_temp add (
constraint pk_ls_ilr_future_pay_fx_temp primary key (ls_cur_type, ilr_id, revision, set_of_books_id) );

comment on table ls_ilr_future_payments_fx_temp is 'LS ILR Future Payments FX Temp the total remaining payments for ILRs for Lessee Disclosure Reports.';
comment on column ls_ilr_future_payments_fx_temp.ls_cur_type is 'System assigned identifier for a lease currency type: 1 = Contract, 2 = Company.';
comment on column ls_ilr_future_payments_fx_temp.ilr_id is 'System assigned identifier for an ILR.';
comment on column ls_ilr_future_payments_fx_temp.revision is 'System assigned identifier of a revision.'; 
comment on column ls_ilr_future_payments_fx_temp.set_of_books_id is 'System assigned identifier of a set of books';
comment on column ls_ilr_future_payments_fx_temp.company_id is 'System assigned identifier of a company.';
comment on column ls_ilr_future_payments_fx_temp.lease_id is 'System assigned identifier of a lease.';
comment on column ls_ilr_future_payments_fx_temp.iso_code is  'The 3-character ISO standard code used to denote this currency';
comment on column ls_ilr_future_payments_fx_temp.currency_display_symbol is 'The display symbol for the currency on the record.';
comment on column ls_ilr_future_payments_fx_temp.currency_id is 'System assigned identifier of a currency.';
comment on column ls_ilr_future_payments_fx_temp.total_payment is 'The total remaining payments for an ILR.';

-- ls_ilr_future_pay_mon_fx_temp 
create GLOBAL TEMPORARY TABLE ls_ilr_future_pay_mon_fx_temp (
  LS_CUR_TYPE               number(22,0) not null,
  ILR_ID                    number(22,0) not null,
  REVISION                  number(22,0) not null,
  SET_OF_BOOKS_ID           number(22,0) not null,
  month                     date not null,
  COMPANY_ID                number(22,0),
  LEASE_ID                  number(22,0),
  CURRENCY_ID               number(22,0), 
  ISO_CODE                  varchar2(3),
  CURRENCY_DISPLAY_SYMBOL   varchar2(35),
  TOTAL_PAYMENT             number(22,2)
)
on commit preserve rows;

alter table ls_ilr_future_pay_mon_fx_temp add (
constraint pk_ls_ilr_fpay_mon_fx_temp primary key (ls_cur_type, ilr_id, revision, set_of_books_id, month) );

comment on table ls_ilr_future_pay_mon_fx_temp is 'LS ILR Future Paym Mon FX Temp the total payments for an ILR for a period for Lessee Disclosure Reports.';
comment on column ls_ilr_future_pay_mon_fx_temp.ls_cur_type is 'System assigned identifier for a lease currency type: 1 = Contract, 2 = Company.';
comment on column ls_ilr_future_pay_mon_fx_temp.ilr_id is 'System assigned identifier for an ILR.';
comment on column ls_ilr_future_pay_mon_fx_temp.revision is 'System assigned identifier of a revision.'; 
comment on column ls_ilr_future_pay_mon_fx_temp.set_of_books_id is 'System assigned identifier of a set of books';
comment on column ls_ilr_future_pay_mon_fx_temp.month is 'The accounting month.';
comment on column ls_ilr_future_pay_mon_fx_temp.company_id is 'System assigned identifier of a company.';
comment on column ls_ilr_future_pay_mon_fx_temp.lease_id is 'System assigned identifier of a lease.';
comment on column ls_ilr_future_pay_mon_fx_temp.currency_id is 'System assigned identifier of a currency.';
comment on column ls_ilr_future_pay_mon_fx_temp.iso_code is  'The 3-character ISO standard code used to denote this currency';
comment on column ls_ilr_future_pay_mon_fx_temp.currency_display_symbol is 'The display symbol for the currency on the record.';
comment on column ls_ilr_future_pay_mon_fx_temp.total_payment is 'The total payments for an ILR for the period.';

-- ls_ilr_npv_rate 
create GLOBAL TEMPORARY TABLE ls_ilr_npv_rate (
  ILR_ID                    number(22,0) not null,
  REVISION                  number(22,0) not null,
  SET_OF_BOOKS_ID           number(22,0) not null,
  COMPANY_ID                number(22,0),
  LEASE_ID                  number(22,0), 
  PRE_PAYMENT_SW            number(22,0),
  CONTRACT_CURRENCY_ID      number(22,0),
  START_MONTH               date,
  MONTHS_REMAINING          number(22,0),
  YEAR_MOD                  number(22,0),
  IN_YEAR                   number(22,0),
  EFF_RATE                  number(22,8)
)
on commit preserve rows;

alter table ls_ilr_npv_rate add (
constraint pk_ls_ilr_npv_rate primary key (ilr_id, revision, set_of_books_id) );

comment on table ls_ilr_npv_rate is 'LS ILR NPV Rate stores the rates used to calculate the NPV an ILR for a period for Lessee Disclosure Reports.';
comment on column ls_ilr_npv_rate.ilr_id is 'System assigned identifier for an ILR.';
comment on column ls_ilr_npv_rate.revision is 'System assigned identifier of a revision.'; 
comment on column ls_ilr_npv_rate.set_of_books_id is 'System assigned identifier of a set of books';
comment on column ls_ilr_npv_rate.company_id is 'System assigned identifier of a company.';
comment on column ls_ilr_npv_rate.lease_id is 'System assigned identifier of a lease.';
comment on column ls_ilr_npv_rate.pre_payment_sw is 'Indicates whether payments are made in arrears or pre-paid.';
comment on column ls_ilr_npv_rate.contract_currency_id is 'System assigned identifier of a currency.';
comment on column ls_ilr_npv_rate.start_month is 'The starting month for calculating the NPV.';
comment on column ls_ilr_npv_rate.months_remaining is 'The total remaining months for an ILR.';
comment on column ls_ilr_npv_rate.year_mod is 'The number of months between the start month and the report month.';
comment on column ls_ilr_npv_rate.in_year is 'Indicates in which year the payment occurs.';
comment on column ls_ilr_npv_rate.eff_rate is 'The effective rate used for calculating the NPV for an ILR as of the report month.';

-- ls_ilr_npv_fx_temp 
create GLOBAL TEMPORARY TABLE ls_ilr_npv_fx_temp (
  LS_CUR_TYPE               number(22,0) not null,
  ILR_ID                    number(22,0) not null,
  REVISION                  number(22,0) not null,
  SET_OF_BOOKS_ID           number(22,0) not null,
  MONTH                     date not null,
  COMPANY_ID                number(22,0),
  LEASE_ID                  number(22,0), 
  ISO_CODE                  varchar2(3),
  CURRENCY_DISPLAY_SYMBOL   varchar2(35),
  CURRENCY_ID               number(22,0),
  START_MONTH               date,
  MONTHS_REMAINING          number(22,0),
  CALC_MONTH                number(22,0),
  YEAR_MOD                  number(22,0),
  IN_YEAR                   number(22,0),
  FUTURE_PAYMENT            number(22,2),
  DISC_PAYMENT              number(22,2),
  RESIDUAL_AMOUNT           number(22,2),
  TERM_PENALTY              number(22,2),
  BPO_PRICE                 number(22,2)
)
on commit preserve rows;

alter table ls_ilr_npv_fx_temp add (
constraint pk_ls_ilr_npv_fx_temp primary key (ls_cur_type, ilr_id, revision, set_of_books_id, month) );

comment on table ls_ilr_npv_fx_temp is 'LS ILR NPV FX Temp stores components for calculating the NPV for an ILR for a period for Lessee Disclosure Reports.';
comment on column ls_ilr_npv_fx_temp.ls_cur_type is 'System assigned identifier for a lease currency type: 1 = Contract, 2 = Company.';
comment on column ls_ilr_npv_fx_temp.ilr_id is 'System assigned identifier for an ILR.';
comment on column ls_ilr_npv_fx_temp.revision is 'System assigned identifier of a revision.'; 
comment on column ls_ilr_npv_fx_temp.set_of_books_id is 'System assigned identifier of a set of books';
comment on column ls_ilr_npv_fx_temp.month is 'The accounting month.';
comment on column ls_ilr_npv_fx_temp.company_id is 'System assigned identifier of a company.';
comment on column ls_ilr_npv_fx_temp.lease_id is 'System assigned identifier of a lease.';
comment on column ls_ilr_npv_fx_temp.iso_code is  'The 3-character ISO standard code used to denote this currency';
comment on column ls_ilr_npv_fx_temp.currency_display_symbol is 'The display symbol for the currency on the record.';
comment on column ls_ilr_npv_fx_temp.currency_id is 'System assigned identifier of a currency.';
comment on column ls_ilr_npv_fx_temp.start_month is 'The starting month for calculating the NPV.';
comment on column ls_ilr_npv_fx_temp.months_remaining is 'The total remaining months for an ILR.';
comment on column ls_ilr_npv_fx_temp.calc_month is 'The current month for calculating the NPV.';
comment on column ls_ilr_npv_fx_temp.year_mod is 'The number of months between the start month and the report month.';
comment on column ls_ilr_npv_fx_temp.in_year is 'Indicates in which year the payment occurs.';
comment on column ls_ilr_npv_fx_temp.future_payment is 'The total payments for an ILR for the period.';
comment on column ls_ilr_npv_fx_temp.disc_payment is 'The discounted payments for an ILR for the period.';
comment on column ls_ilr_npv_fx_temp.residual_amount is 'The guaranteed residual amount.';
comment on column ls_ilr_npv_fx_temp.term_penalty is 'The penalty amount for terminating the lease.';
comment on column ls_ilr_npv_fx_temp.bpo_price is 'The bargain purchase amount.';

-- ls_ilr_future_liab_fx_temp 
create GLOBAL TEMPORARY TABLE ls_ilr_future_liab_fx_temp (
  LS_CUR_TYPE               number(22,0) not null,
  ILR_ID                    number(22,0) not null,
  REVISION                  number(22,0) not null,
  SET_OF_BOOKS_ID           number(22,0) not null,
  COMPANY_ID                number(22,0),
  LEASE_ID                  number(22,0),
  CURRENCY_ID               number(22,0), 
  ISO_CODE                  varchar2(3),
  CURRENCY_DISPLAY_SYMBOL   varchar2(35),
  LEASE_LIABILITY           number(22,2),
  MONTHS_REMAINING          number(22,0)
)
on commit preserve rows;

alter table ls_ilr_future_liab_fx_temp add (
constraint pk_ls_ilr_fut_liab_fx_temp primary key (ls_cur_type, ilr_id, revision, set_of_books_id) );

comment on table ls_ilr_future_liab_fx_temp is 'LS ILR Future Payments FX Temp the total remaining payments for ILRs for Lessee Disclosure Reports.';
comment on column ls_ilr_future_liab_fx_temp.ls_cur_type is 'System assigned identifier for a lease currency type: 1 = Contract, 2 = Company.';
comment on column ls_ilr_future_liab_fx_temp.ilr_id is 'System assigned identifier for an ILR.';
comment on column ls_ilr_future_liab_fx_temp.revision is 'System assigned identifier of a revision.'; 
comment on column ls_ilr_future_liab_fx_temp.set_of_books_id is 'System assigned identifier of a set of books';
comment on column ls_ilr_future_liab_fx_temp.company_id is 'System assigned identifier of a company.';
comment on column ls_ilr_future_liab_fx_temp.lease_id is 'System assigned identifier of a lease.';
comment on column ls_ilr_future_liab_fx_temp.currency_id is 'System assigned identifier of a currency.';
comment on column ls_ilr_future_liab_fx_temp.iso_code is  'The 3-character ISO standard code used to denote this currency';
comment on column ls_ilr_future_liab_fx_temp.currency_display_symbol is 'The display symbol for the currency on the record.';
comment on column ls_ilr_future_liab_fx_temp.lease_liability is 'The total remaining lease liability for an ILR.';
comment on column ls_ilr_future_liab_fx_temp.months_remaining is 'The total remaining months for an ILR.';

-- ls_lease_calc_rates_temp 
create GLOBAL TEMPORARY TABLE ls_lease_calc_rates_temp (
  company_id number(22,0) not null,
  contract_currency_id number(22,0) not null,
  company_currency_id number(22,0) not null,
  accounting_month date not null,
  act_rate number(22,8),
  avg_rate number(22,8)
) on commit preserve rows;

alter table ls_lease_calc_rates_temp add (
constraint pk_ls_lease_calc_rates_temp primary key (company_id, contract_currency_id, company_currency_id, accounting_month));

comment on table ls_lease_calc_rates_temp is 'LS Lease Calc Rates Temp stores the locked exchange rates for Lessee Disclosure Reports.';
comment on column ls_lease_calc_rates_temp.company_id is 'System assigned identifier of a company.';
comment on column ls_lease_calc_rates_temp.contract_currency_id is 'System assigned identifier for the contract currency.';
comment on column ls_lease_calc_rates_temp.company_currency_id is 'System assigned identifier for the company currency.';
comment on column ls_lease_calc_rates_temp.accounting_month is 'The effective accounting month for the exchange rate.';
comment on column ls_lease_calc_rates_temp.act_rate is 'The actual exchange rate for the month (exchange rate type 1).';
comment on column ls_lease_calc_rates_temp.avg_rate is 'The average exchange rate for the month (exchange rate type 4).';

-- ls_avg_rate_cv_temp 
create GLOBAL TEMPORARY TABLE ls_avg_rate_cv_temp (
  company_id number(22,0) not null,
  control_value varchar2(3),
  currency_id number(22,0)
) on commit preserve rows;

alter table ls_avg_rate_cv_temp add (
constraint pk_ls_avg_rate_cv_temp primary key (company_id));

-- Add new indexes
create index ls_ilr_id_curr_rev_idx on ls_ilr (ilr_id, current_revision);
create index ls_lease_calc_date_rt_fmth_idx on ls_lease_calculated_date_rates (trunc(accounting_month,'fmmonth'));

comment on table ls_avg_rate_cv_temp is 'LS Lease Calc Rates Temp stores the locked exchange rates for Lessee Disclosure Reports.';
comment on column ls_avg_rate_cv_temp.company_id is 'System assigned identifier of a company.';
comment on column ls_avg_rate_cv_temp.control_value is 'Indicates whether average exchange rates are used for the company.';
comment on column ls_avg_rate_cv_temp.currency_id is 'System assigned identifier for the company currency.';

-- Updated view
CREATE OR REPLACE VIEW V_LS_COMPANY_FX (
  company_id, company_description, currency_id, iso_code, currency_display_symbol,
  month, currency_from, currency_to, now_rate, bal_rate, act_rate) AS
with lease_co as (
       select /*+ MATERIALIZE */ c.company_id, c.description, cs.currency_id
         from company c
         join currency_schema cs on c.company_id = cs.company_id
        where cs.currency_type_id = 1
     ),
     sc_bs as (
        select /*+ MATERIALIZE */ * 
          from pp_system_control_company
         where trim(lower(control_name)) = 'lease consolidated bs curr type'
     ),
     sc_is as (
        select /*+ MATERIALIZE */ * 
          from pp_system_control_company
         where trim(lower(control_name)) = 'lease consolidated is curr type'
     )
select c.company_id, c.description company_description, cur.currency_id, cur.iso_code, cur.currency_display_symbol, 
       fx.month, fx.currency_from, fx.currency_to, fx.act_now_rate now_rate,
       decode(to_number(bs_cv.control_value), 4, nvl(calc_rate.avg_rate, fx.avg_month_rate),  nvl(calc_rate.act_rate, fx.act_month_rate)) bal_rate,
       decode(to_number(is_cv.control_value), 4, nvl(calc_rate.avg_rate, fx.avg_month_rate),  nvl(calc_rate.act_rate, fx.act_month_rate)) act_rate
  from lease_co c
  join currency cur on cur.currency_id = c.currency_id
  join ls_curr_fx_temp fx on fx.currency_to = cur.currency_id
  join (
          select sc_bs.company_id, lower(trim(sc_bs.control_value)) control_value
             from sc_bs
            where sc_bs.company_id <> -1
          union all
          select c.company_id, lower(trim(sc_bs.control_value)) control_value
            from sc_bs
           cross join (select company_id from lease_co
                       minus
                       select company_id from sc_bs b) c
           where sc_bs.company_id = -1
       ) bs_cv on bs_cv.company_id = c.company_id
  join (
          select sc_is.company_id, lower(trim(sc_is.control_value)) control_value
            from sc_is
           where sc_is.company_id <> -1
          union all
          select c.company_id, lower(trim(sc_is.control_value)) control_value
            from sc_is
           cross join (select company_id from lease_co
                       minus
                       select company_id from sc_is b) c
           where sc_is.company_id = -1
       ) is_cv on is_cv.company_id = c.company_id 
  left outer join (
          select cr1.company_id, cr1.contract_currency_id, cr1.company_currency_id, 
                 cr1.accounting_month, cr1.rate act_rate, nvl(cr4.rate, cr1.rate) avg_rate
           from ls_lease_calculated_date_rates cr1
           left outer join (
               select company_id, contract_currency_id, company_currency_id, accounting_month, rate
                 from ls_lease_calculated_date_rates
                where exchange_rate_type_id = 4
                          ) cr4 on cr1.company_id = cr4.company_id
                               and cr1.contract_currency_id = cr4.contract_currency_id
                               and cr1.company_currency_id = cr4.company_currency_id
                               and cr1.accounting_month = cr4.accounting_month
           where cr1.exchange_rate_type_id = 1   
                  ) calc_rate on calc_rate.company_id = c.company_id
                             and calc_rate.contract_currency_id = fx.currency_from
                             and calc_rate.company_currency_id = cur.currency_id
                             and trunc(calc_rate.accounting_month,'fmmonth') = fx.month ;
							 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(9662, 0, 2017, 4, 0, 2, 51355, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.2_maint_051355_lessee_01_ls_ilr_schedule_fx_tmp_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;