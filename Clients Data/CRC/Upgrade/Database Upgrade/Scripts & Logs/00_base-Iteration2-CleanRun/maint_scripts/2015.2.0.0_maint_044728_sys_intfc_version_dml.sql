/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044728_sys_intfc_version_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2015.2.0.0  09/30/2015 Sarah Byers    Add the rest of the interfaces to pp_custom_pbd_versions
||============================================================================
*/

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'exe_budget_allocations_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'budget_allocations.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'exe_budallo_custom_dw.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'budget_allocations.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'exe_cr_allocations_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_allocations.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'exe_crallo_custom_dw,pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_allocations.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_balances_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_balances.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_balances_bdg_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_balances_bdg.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_batch_derivation_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_batch_derivation.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_batch_derivation_bdg_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_batch_derivation_bdg.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_budget_entry_calc_all_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_budget_entry_calc_all.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_build_deriver_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_build_deriver.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_delete_budget_allocations_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_delete_budget_allocations.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_financial_reports_build_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_financial_reports_build.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_flatten_structures_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_flatten_structures.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_man_jrnl_reversals_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_man_jrnl_reversals.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_posting_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_posting.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_posting_custom_bdg.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_posting_bdg.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_sap_trueup_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_sap_trueup.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_sum_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_sum.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_sum_ab_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_sum_ab.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cr_to_commitments_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cr_to_commitments.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'cwip_charge_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'cwip_charge.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'populate_matlrec_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'populate_matlrec.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'pp_integration_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'pp_integration.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'ppprojct_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'pp_integration.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'pptocr_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'pptocr.exe';

insert into pp_custom_pbd_versions (
	process_id, pbd_name, version)
select process_id,
		 'ppprojct_custom.pbd',
		 '2015.2.0.0'
  from pp_processes
 where lower(trim(executable_file)) = 'ssp_budget.exe';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2885, 0, 2015, 2, 0, 0, 044728, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044728_sys_intfc_version_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;