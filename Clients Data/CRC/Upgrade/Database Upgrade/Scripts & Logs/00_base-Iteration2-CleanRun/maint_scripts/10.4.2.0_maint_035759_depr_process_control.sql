/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035759_depr_process_control.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/03/2014 Kyle Peterson
||============================================================================
*/

create table DEPR_CALC_PROCESS_CONTROL
(
 GROUP_ID     number(22,0) not null,
 TYPE         varchar2(254) not null,
 IS_PROCESSED number(1,0) default 0
);

alter table DEPR_CALC_PROCESS_CONTROL
   add constraint PK_DEPR_CALC_PROCESS_CONTROL
       primary key (GROUP_ID, TYPE)
       using index tablespace PWRPLANT_IDX;

create sequence DEPR_CALC_PROCESS_CONTROL_SEQ;

comment on table DEPR_CALC_PROCESS_CONTROL is '(P)  [01]
The Depr Calc Process Control table holds records indicating what types of depreciation have been staged and their completion status.';

comment on column DEPR_CALC_PROCESS_CONTROL.GROUP_ID is 'The system assigned identifier of a group of depreciation calculations.';
comment on column DEPR_CALC_PROCESS_CONTROL.TYPE is 'The type of depreciation. A group can have multiple types.';
comment on column DEPR_CALC_PROCESS_CONTROL.IS_PROCESSED is 'Whether or not the group has been processed.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1005, 0, 10, 4, 2, 0, 35759, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035759_depr_process_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;