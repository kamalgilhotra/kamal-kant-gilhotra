/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051665_lessee_02_add_payment_shift_import_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.1 06/07/2018 Crystal Yura      Add payment shift to ILR import
||============================================================================
*/


INSERT INTO pp_import_column 
(IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,IS_ON_TABLE)
SELECT   import_type_id,
         'payment_shift',
         'Payment Shift',
         0,
         1,
         'number(12,0)',
         1
from pp_import_type a where lower(import_table_name) = 'ls_import_ilr'
and not exists (select 1 from pp_import_column b where a.import_type_id = b.import_type_id and lower(b.column_name) = 'payment_shift');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(6583, 0, 2017, 3, 0, 1, 51665, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051665_lessee_02_add_payment_shift_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;