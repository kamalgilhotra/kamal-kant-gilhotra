/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047966_lease_01_add_currency_gain_loss_mec_proc_column_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 07/11/2017 Andrew Hill    add process_5 column to month end close config table 
||                                          for currency gain/loss process
||============================================================================
*/

ALTER TABLE ls_mec_config ADD(process_5 NUMBER(1,0) DEFAULT 0 NOT NULL);

ALTER TABLE ls_mec_config ADD(CONSTRAINT ls_mec_config_proc_5_tf_chk CHECK (process_5 IN (0,1)));

COMMENT ON COLUMN ls_mec_config.process_5 IS 'Controls whether or not Currency Gain/Loss shows up in Month End Close. 1 is yes, 0 is no.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3581, 0, 2017, 1, 0, 0, 47966, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047966_lease_01_add_currency_gain_loss_mec_proc_column_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;