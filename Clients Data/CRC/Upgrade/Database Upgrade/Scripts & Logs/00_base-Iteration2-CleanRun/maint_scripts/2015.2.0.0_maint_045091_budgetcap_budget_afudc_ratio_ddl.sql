 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045091_budgetcap_budget_afudc_ratio_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2   10/15/2015 Christine Bell add ratios to budget afudc
 ||============================================================================
 */ 

set define OFF

ALTER TABLE budget_afudc_calc 
  ADD ( input_afudc_ratio NUMBER(22, 8) NULL, input_cpi_ratio NUMBER(22, 8) NULL 
  ); 

ALTER TABLE budget_afudc_calc_temp 
  ADD ( input_afudc_ratio NUMBER(22, 8) NULL, input_cpi_ratio NUMBER(22, 8) NULL 
  ); 

CREATE TABLE budget_afudc_input_ratio 
  ( 
     work_order_id     NUMBER(22, 0) NOT NULL, 
     effective_date    DATE NOT NULL, 
     time_stamp        DATE NULL, 
     user_id           VARCHAR2(18) NULL, 
     input_afudc_ratio NUMBER(22, 12) DEFAULT 1 NULL, 
     input_cpi_ratio   NUMBER(22, 12) DEFAULT 1 NULL 
  ) ;


ALTER TABLE budget_afudc_input_ratio 
  ADD CONSTRAINT budget_afudc_input_ratio_pk PRIMARY KEY ( work_order_id, 
  effective_date ) 
  using index tablespace PWRPLANT_IDX;


ALTER TABLE budget_afudc_input_ratio 
  ADD CONSTRAINT r_budget_afudc_input_ratio1 FOREIGN KEY ( work_order_id ) 
  REFERENCES work_order_control ( work_order_id );

 
COMMENT ON TABLE budget_afudc_input_ratio IS '(O) [04] The Budget AFUDC Input Ratio table contains decimal factors that reduce the Budget AFUDC (interest) or Budget CPI tax interest base.  IT can be input through a special window or calculated using a user function, either as an interface, or the standard-called function (f_wo_control_pre_afudc), which is called by company just before the AFUDC calculation.'; 

COMMENT ON COLUMN budget_afudc_input_ratio.work_order_id IS 'System-assigned identifier of the work order.'; 

COMMENT ON COLUMN budget_afudc_input_ratio.effective_date IS 'The effective dated month and year of the budget input ratio adjustment.'; 

COMMENT ON COLUMN budget_afudc_input_ratio.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.'; 

COMMENT ON COLUMN budget_afudc_input_ratio.user_id IS 'Standard system-assigned user id used for audit purposes.'; 

COMMENT ON COLUMN budget_afudc_input_ratio.input_afudc_ratio IS 'Decimal ratio for adjusting the AFUDC base (excluding compounding) for the month.  (Default is ''1.0''.)'; 

COMMENT ON COLUMN budget_afudc_input_ratio.input_cpi_ratio IS 'Decimal ratio for adjusting the CPI base (excluding compounding) for the month.  (Default is ''1.0''.)'; 

COMMENT ON COLUMN budget_afudc_calc.input_cpi_ratio IS 'The budget input CPI ratio used in the budget CPI calculation.  (See Budget AFUDC Input Ratio table.)';

COMMENT ON COLUMN budget_afudc_calc.input_afudc_ratio IS 'The budget input AFUDC ratio used in the budget AFUDC or interest calculation.  (See Budget AFUDC Input Ratio table.)';

COMMENT ON COLUMN budget_afudc_calc_temp.input_cpi_ratio IS 'The budget input CPI ratio used in the budget CPI calculation.  (See Budget AFUDC Input Ratio table.)';

COMMENT ON COLUMN budget_afudc_calc_temp.input_afudc_ratio IS 'The budget input AFUDC ratio used in the budget AFUDC or interest calculation.  (See Budget AFUDC Input Ratio table.)';

set define ON 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2927, 0, 2015, 2, 0, 0, 45091, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045091_budgetcap_budget_afudc_ratio_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;