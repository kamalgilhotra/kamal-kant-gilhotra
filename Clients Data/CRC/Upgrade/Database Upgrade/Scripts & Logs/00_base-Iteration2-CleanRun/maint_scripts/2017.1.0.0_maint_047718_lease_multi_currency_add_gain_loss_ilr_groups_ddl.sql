/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047718_lease_multi_currency_add_gain_loss_ilr_groups_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/10/2017 Anand R          add gain loss columns to ILR group
||============================================================================
*/

alter table ls_ilr_group
add 
(
  use_multi_currency number(1,0) null,
  curr_gain_loss_acct_id number(22,0) null,
  curr_gain_loss_offset_acct_id number(22,0) null
);

COMMENT ON COLUMN ls_ilr_group.use_multi_currency IS 'Indicates whether the ILR Group uses multi currency.';
COMMENT ON COLUMN ls_ilr_group.curr_gain_loss_acct_id IS 'The currency gain loss account.';
COMMENT ON COLUMN ls_ilr_group.curr_gain_loss_offset_acct_id IS 'The currency gain loss offset account.';

ALTER TABLE ls_ilr_group
  ADD CONSTRAINT ls_ilr_group_fk16 FOREIGN KEY (
    curr_gain_loss_acct_id
  ) REFERENCES gl_account (
    gl_account_id
  );
  
ALTER TABLE ls_ilr_group
  ADD CONSTRAINT ls_ilr_group_fk17 FOREIGN KEY (
    curr_gain_loss_offset_acct_id
  ) REFERENCES gl_account (
    gl_account_id
  );
  

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3563, 0, 2017, 1, 0, 0, 47718, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047718_lease_multi_currency_add_gain_loss_ilr_groups_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  