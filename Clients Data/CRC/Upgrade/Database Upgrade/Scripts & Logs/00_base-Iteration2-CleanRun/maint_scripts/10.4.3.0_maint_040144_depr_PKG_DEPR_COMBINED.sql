/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040144_depr_PKG_DEPR_COMBINED.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/03/2014 Charlie Shilling
||============================================================================
*/

create or replace package PKG_DEPR_COMBINED
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PKG_DEPR_COMBINED
|| Description: Functions and procedures to handle combined depr group functionality
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- -----------------------------------------
|| 10.4.2.0 02/13/2014 C. Shilling    Create package
||============================================================================
*/
 as
   procedure P_COMBINED_DEPR_CALC(A_FCST_VERSION_ID number, A_MONTH date);
end PKG_DEPR_COMBINED;
/

create or replace package body PKG_DEPR_COMBINED as

	function F_STG_COMBINED_DEPR(A_FCST_VERSION_ID number, A_MONTH date) return number is
		V_RECORD_COUNT integer;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.F_STG_COMBINED_DEPR');

		if nvl(A_FCST_VERSION_ID, 0) = 0 then
			/*Stage actuals*/
			PKG_PP_LOG.P_WRITE_MESSAGE('Staging combined depreciation records for actuals...');

			/*Do not join to combined_depr_group's depr_method_rates yet in case one is not assigned.
			We will do an update after this insert that will populate combined method's values.*/
			insert into DEPR_CALC_COMBINED_STG
				(DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, COMBINED_CALC_OPTION, SKIP_DEPR_ALLOC,
				 SKIP_DEPR_REASON, SKIP_COR_ALLOC, SKIP_COR_REASON, DEPR_METHOD_ID, MID_PERIOD_METHOD,
				 MID_PERIOD_CONV, OVER_DEPR_CHECK, NET_GROSS, NET_SALVAGE_PCT, COST_OF_REMOVAL_PCT,
				 DEPRECIATION_BASE, COST_OF_REMOVAL_BASE, CDG_DEPR_GROUP_ID, CDG_DEPR_METHOD_ID,
				 CDG_DEPR_BASE, CDG_COR_BASE, BEGIN_BALANCE, COR_BEG_RESERVE, BEGIN_RESERVE,
				 PLANT_ACTIVITY, RESERVE_ACTIVITY, COR_ACTIVITY,
				 IMPAIRMENT_ASSET_BEGIN_BALANCE, IMPAIRMENT_ASSET_ACTIVITY_SALV, RWIP_SALVAGE, RWIP_COR,
				 END_BALANCE, END_RESERVE,
				 COR_END_RESERVE, DEPR_EXP_AND_ADJS, SALV_EXP_AND_ADJS, COR_EXP_AND_ADJS,
				 DEPR_AMT_TO_ALLOC, COR_AMT_TO_ALLOC, AVG_REM_LIFE, NET_PLANT, NET_COR, OVER_DEPR_ADJ,
				 OVER_DEPR_ADJ_SALV, OVER_DEPR_ADJ_COR, DEPR_FACTOR, COR_FACTOR, TARGET_DEPR_AMT,
				 TARGET_COR_AMT, COMBINED_DEPR_ADJ, COMBINED_SALV_ADJ, COMBINED_COR_ADJ,
				 DEPR_ROUNDING_PLUG, COR_ROUNDING_PLUG,
				 RWIP_IN_OVER_DEPR, RWIP_ALLOCATION, RWIP_COST_OF_REMOVAL)
				select STG.DEPR_GROUP_ID,
					   STG.SET_OF_BOOKS_ID,
					   STG.GL_POST_MO_YR,
					   NVL2(CDG.DEPR_METHOD_ID, 2, 1) as COMBINED_CALC_OPTION, /*if cdg.depr_method_id is NOT null, 2, else 1*/
					   0 as SKIP_DEPR_ALLOC,
					   '' as SKIP_DEPR_REASON,
					   0 as SKIP_COR_ALLOC,
					   '' as SKIP_COR_REASON,
					   STG.DEPR_METHOD_ID,
					   STG.MID_PERIOD_METHOD,
					   STG.MID_PERIOD_CONV,
					   STG.OVER_DEPR_CHECK,
					   STG.NET_GROSS,
					   STG.NET_SALVAGE_PCT,
					   STG.COST_OF_REMOVAL_PCT,
					   STG.DEPRECIATION_BASE,
					   STG.COST_OF_REMOVAL_BASE,
					   CDG.COMBINED_DEPR_GROUP_ID,
					   CDG.DEPR_METHOD_ID,
					   0 as CDG_DEPR_BASE,
					   0 as CDG_COR_BASE,
					   STG.BEGIN_BALANCE,
					   STG.COR_BEG_RESERVE,
					   STG.BEGIN_RESERVE,
					   STG.PLANT_ACTIVITY,
					   STG.RESERVE_ACTIVITY + STG.RESERVE_BLENDING_TRANSFER,
					   STG.COR_ACTIVITY + STG.COR_BLENDING_TRANSFER,
					   STG.IMPAIRMENT_ASSET_BEGIN_BALANCE,
					   STG.IMPAIRMENT_ASSET_ACTIVITY_SALV,
					   STG.RWIP_SALVAGE,
					   STG.RWIP_COR,
					   STG.END_BALANCE,
					   STG.END_RESERVE,
					   STG.COR_END_RESERVE,
					   (DEPRECIATION_EXPENSE + RETRO_DEPR_ADJ + CURVE_TRUEUP_ADJ + UOP_EXP_ADJ + RESERVE_BLENDING_ADJUSTMENT),
					   (SALVAGE_EXPENSE + RETRO_SALV_ADJ + CURVE_TRUEUP_ADJ_SALV ), /*CBS - in the old code, there was no salvage adjustment for uop, so omit.*/
					   (COR_EXPENSE + RETRO_COR_ADJ + CURVE_TRUEUP_ADJ_COR + COR_BLENDING_ADJUSTMENT),
					   0 as DEPR_AMT_TO_ALLOC,
					   0 as COR_AMT_TO_ALLOC,
					   0 as AVG_REM_LIFE,
					   0 as NET_PLANT,
					   0 as NET_COR,
					   STG.OVER_DEPR_ADJ,
					   STG.OVER_DEPR_ADJ_SALV,
					   STG.OVER_DEPR_ADJ_COR,
					   0 as DEPR_FACTOR,
					   0 as COR_FACTOR,
					   0 as TARGET_DEPR_AMT,
					   0 as TARGET_COR_AMT,
					   0 as COMBINED_DEPR_ADJ,
					   0 as COMBINED_SALV_ADJ,
					   0 as COMBINED_COR_ADJ,
					   0 as DEPR_ROUNDING_PLUG,
					   0 as COR_ROUNDING_PLUG,
					   STG.RWIP_IN_OVER_DEPR,
					   STG.RWIP_ALLOCATION,
					   STG.RWIP_COST_OF_REMOVAL
				  from DEPR_CALC_STG STG, COMBINED_DEPR_GROUP CDG, DEPR_COMBINED_GROUP_BOOKS DCGB
				 where STG.COMBINED_DEPR_GROUP_ID = CDG.COMBINED_DEPR_GROUP_ID
				   and STG.COMBINED_DEPR_GROUP_ID = DCGB.COMBINED_DEPR_GROUP_ID
				   and STG.SET_OF_BOOKS_ID = DCGB.SET_OF_BOOKS_ID
				   and DCGB.INCLUDE_IN_CALC = 1
				   and STG.COMBINED_DEPR_GROUP_ID is not null
				   and STG.GL_POST_MO_YR = A_MONTH;

			PKG_PP_LOG.P_WRITE_MESSAGE('Updating Option 2 records with the combined depr_group''s method information...');

			/*Now we update the CDG's method values.*/
			/*If the CDG doesn't have a method, the joins will exclude it,
			so we don't have to specify option 2.*/
			update DEPR_CALC_COMBINED_STG STG
			   set (CDG_OVER_DEPR_CHECK,
					 CDG_NET_GROSS,
					 CDG_NET_SALVAGE_PCT,
					 CDG_RATE,
					 CDG_COST_OF_REMOVAL_PCT,
					 CDG_COR_RATE) =
					(select DMR.OVER_DEPR_CHECK,
							DMR.NET_GROSS,
							DMR.NET_SALVAGE_PCT,
							DMR.RATE,
							DMR.COST_OF_REMOVAL_PCT,
							DMR.COST_OF_REMOVAL_RATE
					   from DEPR_METHOD_RATES DMR
					  where DMR.DEPR_METHOD_ID = STG.CDG_DEPR_METHOD_ID
						and DMR.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
						and DMR.EFFECTIVE_DATE =
							(select max(EFFECTIVE_DATE)
							   from DEPR_METHOD_RATES DMR2
							  where DMR2.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
								and DMR2.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
								and DMR2.EFFECTIVE_DATE <= STG.GL_POST_MO_YR))
			 where exists (select 1
					  from DEPR_METHOD_RATES DMR
					 where DMR.DEPR_METHOD_ID = STG.CDG_DEPR_METHOD_ID
					   and DMR.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					   and DMR.EFFECTIVE_DATE =
						   (select max(EFFECTIVE_DATE)
							  from DEPR_METHOD_RATES DMR2
							 where DMR2.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
							   and DMR2.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
							   and DMR2.EFFECTIVE_DATE <= STG.GL_POST_MO_YR))
			   and STG.GL_POST_MO_YR = A_MONTH;
		else
			--stage fcst
			PKG_PP_LOG.P_WRITE_MESSAGE('Staging combined depreciation records for forecast...');

			/*Do not join to combined_depr_group's depr_method_rates yet in case one is not assigned.
			We will do an update after this insert that will populate combined method's values.*/
			insert into DEPR_CALC_COMBINED_STG
				(DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, COMBINED_CALC_OPTION, SKIP_DEPR_ALLOC,
				 SKIP_DEPR_REASON, SKIP_COR_ALLOC, SKIP_COR_REASON, DEPR_METHOD_ID, MID_PERIOD_METHOD,
				 MID_PERIOD_CONV, OVER_DEPR_CHECK, NET_GROSS, NET_SALVAGE_PCT, COST_OF_REMOVAL_PCT,
				 DEPRECIATION_BASE, COST_OF_REMOVAL_BASE, CDG_DEPR_GROUP_ID, CDG_DEPR_METHOD_ID,
				 CDG_DEPR_BASE, CDG_COR_BASE, BEGIN_BALANCE, COR_BEG_RESERVE, BEGIN_RESERVE,
				 PLANT_ACTIVITY, RESERVE_ACTIVITY, COR_ACTIVITY,
				 IMPAIRMENT_ASSET_BEGIN_BALANCE, IMPAIRMENT_ASSET_ACTIVITY_SALV, RWIP_SALVAGE, RWIP_COR,
				 END_BALANCE, END_RESERVE,
				 COR_END_RESERVE, DEPR_EXP_AND_ADJS, SALV_EXP_AND_ADJS, COR_EXP_AND_ADJS,
				 DEPR_AMT_TO_ALLOC, COR_AMT_TO_ALLOC, AVG_REM_LIFE, NET_PLANT, NET_COR, OVER_DEPR_ADJ,
				 OVER_DEPR_ADJ_SALV, OVER_DEPR_ADJ_COR, DEPR_FACTOR, COR_FACTOR, TARGET_DEPR_AMT,
				 TARGET_COR_AMT, COMBINED_DEPR_ADJ, COMBINED_SALV_ADJ, COMBINED_COR_ADJ,
				 RWIP_IN_OVER_DEPR, RWIP_ALLOCATION, RWIP_COST_OF_REMOVAL)
				select STG.DEPR_GROUP_ID,
					   STG.SET_OF_BOOKS_ID,
					   STG.GL_POST_MO_YR,
					   NVL2(CDG.FCST_DEPR_METHOD_ID, 2, 1) as COMBINED_CALC_OPTION, /*if cdg.depr_method_id is NOT null, 2, else 1*/
					   0 as SKIP_DEPR_ALLOC,
					   '' as SKIP_DEPR_REASON,
					   0 as SKIP_COR_ALLOC,
					   '' as SKIP_COR_REASON,
					   STG.DEPR_METHOD_ID,
					   STG.MID_PERIOD_METHOD,
					   STG.MID_PERIOD_CONV,
					   STG.OVER_DEPR_CHECK,
					   STG.NET_GROSS,
					   STG.NET_SALVAGE_PCT,
					   STG.COST_OF_REMOVAL_PCT,
					   STG.DEPRECIATION_BASE,
					   STG.COST_OF_REMOVAL_BASE,
					   CDG.FCST_COMBINED_DEPR_GROUP_ID,
					   CDG.FCST_DEPR_METHOD_ID,
					   0 as CDG_DEPR_BASE,
					   0 as CDG_COR_BASE,
					   STG.BEGIN_BALANCE,
					   STG.COR_BEG_RESERVE,
					   STG.BEGIN_RESERVE,
					   STG.PLANT_ACTIVITY,
					   STG.RESERVE_ACTIVITY + STG.RESERVE_BLENDING_TRANSFER,
					   STG.COR_ACTIVITY + STG.COR_BLENDING_TRANSFER,
					   STG.IMPAIRMENT_ASSET_BEGIN_BALANCE,
					   STG.IMPAIRMENT_ASSET_ACTIVITY_SALV,
					   STG.RWIP_SALVAGE,
					   STG.RWIP_COR,
					   STG.END_BALANCE,
					   STG.END_RESERVE,
					   STG.COR_END_RESERVE,
					   (DEPRECIATION_EXPENSE + RETRO_DEPR_ADJ + CURVE_TRUEUP_ADJ + UOP_EXP_ADJ + RESERVE_BLENDING_ADJUSTMENT),
					   (SALVAGE_EXPENSE + RETRO_SALV_ADJ + CURVE_TRUEUP_ADJ_SALV), /*CBS - in the old code, there was no salvage adjustment for uop, so omit.*/
					   (COR_EXPENSE + RETRO_COR_ADJ + CURVE_TRUEUP_ADJ_COR + COR_BLENDING_ADJUSTMENT),
					   0 as DEPR_AMT_TO_ALLOC,
					   0 as COR_AMT_TO_ALLOC,
					   0 as AVG_REM_LIFE,
					   0 as NET_PLANT,
					   0 as NET_COR,
					   STG.OVER_DEPR_ADJ,
					   STG.OVER_DEPR_ADJ_SALV,
					   STG.OVER_DEPR_ADJ_COR,
					   0 as DEPR_FACTOR,
					   0 as COR_FACTOR,
					   0 as TARGET_DEPR_AMT,
					   0 as TARGET_COR_AMT,
					   0 as COMBINED_DEPR_ADJ,
					   0 as COMBINED_SALV_ADJ,
					   0 as COMBINED_COR_ADJ,
					   STG.RWIP_IN_OVER_DEPR,
					   STG.RWIP_ALLOCATION,
					   STG.RWIP_COST_OF_REMOVAL
				  from DEPR_CALC_STG STG, FCST_COMBINED_DEPR_GROUP CDG
				 where STG.COMBINED_DEPR_GROUP_ID = CDG.FCST_COMBINED_DEPR_GROUP_ID
				   and STG.COMBINED_DEPR_GROUP_ID is not null
				   and STG.GL_POST_MO_YR = A_MONTH;

			PKG_PP_LOG.P_WRITE_MESSAGE('Updating Option 2 records with the forecast combined depr_group''s method information...');

			/*Now we update the CDG's method values.*/
			/*If the CDG doesn't have a method, the joins will exclude it,
			so we don't have to specify option 2.*/
			update DEPR_CALC_COMBINED_STG STG
			   set (CDG_OVER_DEPR_CHECK,
					 CDG_NET_GROSS,
					 CDG_NET_SALVAGE_PCT,
					 CDG_RATE,
					 CDG_COST_OF_REMOVAL_PCT) =
					(select DMR.OVER_DEPR_CHECK,
							DMR.NET_GROSS,
							DMR.NET_SALVAGE_PCT,
							DMR.RATE,
							DMR.COST_OF_REMOVAL_PCT
					   from FCST_DEPR_METHOD_RATES DMR
					  where DMR.FCST_DEPR_METHOD_ID = STG.CDG_DEPR_METHOD_ID
						and DMR.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
						and DMR.EFFECTIVE_DATE =
							(select max(EFFECTIVE_DATE)
							   from FCST_DEPR_METHOD_RATES DMR2
							  where DMR2.FCST_DEPR_METHOD_ID = DMR.FCST_DEPR_METHOD_ID
								and DMR2.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
								and DMR2.EFFECTIVE_DATE <= STG.GL_POST_MO_YR))
			 where exists (select 1
					  from FCST_DEPR_METHOD_RATES DMR
					 where DMR.FCST_DEPR_METHOD_ID = STG.CDG_DEPR_METHOD_ID
					   and DMR.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
					   and DMR.EFFECTIVE_DATE =
						   (select max(EFFECTIVE_DATE)
							  from FCST_DEPR_METHOD_RATES DMR2
							 where DMR2.FCST_DEPR_METHOD_ID = DMR.FCST_DEPR_METHOD_ID
							   and DMR2.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
							   and DMR2.EFFECTIVE_DATE <= STG.GL_POST_MO_YR))
			   and STG.GL_POST_MO_YR = A_MONTH;
		end if;

		select count(1) into V_RECORD_COUNT from DEPR_CALC_COMBINED_STG;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		return V_RECORD_COUNT;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
	end F_STG_COMBINED_DEPR;

	procedure P_VALIDATE_COMBINED_DEPR(A_MONTH date) is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_VALIDATE_COMBINED_DEPR');
		null; /* TODO Place holder for code so that package can compile.*/

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
	end P_VALIDATE_COMBINED_DEPR;

	procedure P_GET_RESERVE_RATIOS(A_MONTH date) is
		TAB_RESERVE_RATIOS PKG_DEPR_FCST.DMR_RR_TAB;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_GET_RESERVE_RATIOS');
		/*Build table of depr methods to pass to the reserve ratio function*/
		select DMR.DEPR_METHOD_ID,
			   DMR.SET_OF_BOOKS_ID,
			   DMR.MORTALITY_CURVE_ID,
			   DMR.EXPECTED_AVERAGE_LIFE,
			   DMR.END_OF_LIFE,
			   DMR.RESERVE_RATIO_ID,
			   DMR.ALLOCATION_PROCEDURE,
			   DMR.EFFECTIVE_DATE bulk collect
		  into TAB_RESERVE_RATIOS
		  from DEPR_CALC_COMBINED_STG STG, DEPR_METHOD_RATES DMR
		 where DMR.DEPR_METHOD_ID = STG.DEPR_METHOD_ID
		   and DMR.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
		   and DMR.EFFECTIVE_DATE =
			   (select max(EFFECTIVE_DATE)
				  from DEPR_METHOD_RATES DMR2
				 where DMR2.DEPR_METHOD_ID = DMR.DEPR_METHOD_ID
				   and DMR2.SET_OF_BOOKS_ID = DMR.SET_OF_BOOKS_ID
				   and DMR2.EFFECTIVE_DATE <= STG.GL_POST_MO_YR)
		   and STG.GL_POST_MO_YR = A_MONTH
		   and STG.COMBINED_CALC_OPTION = 1; /*only theoretical needs a reserve ratio.*/

		/*Call try to find reserve ratio - build if needed*/
		for i in 1 .. TAB_RESERVE_RATIOS.count loop
			if PKG_DEPR_FCST.F_GET_RESERVE_RATIO_ID(TAB_RESERVE_RATIOS(i)) = -1 then
				if upper(TAB_RESERVE_RATIOS(i).ALLOCATION_PROCEDURE) = 'ELG' then
					PKG_DEPR_FCST.P_LOAD_RESERVE_RATIOS_ELG(TAB_RESERVE_RATIOS(i));
				else
					PKG_DEPR_FCST.P_LOAD_RESERVE_RATIOS_BROAD(TAB_RESERVE_RATIOS(i));
				end if;
			end if;
		end loop;

		/*Update depr_method_rates*/
		forall i in indices of TAB_RESERVE_RATIOS
			update DEPR_METHOD_RATES
			set RESERVE_RATIO_ID = TAB_RESERVE_RATIOS(i).RESERVE_RATIO_ID
			where DEPR_METHOD_ID = TAB_RESERVE_RATIOS(i).DMR_ID
			and SET_OF_BOOKS_ID = TAB_RESERVE_RATIOS(i).SOB_ID
			and EFFECTIVE_DATE = TAB_RESERVE_RATIOS(i).MONTH;

		/*Backfill Staging table with reserve ratio id. The PKG_DEPR_FCST procedure updated
        depr_method_rates directly, so we can backfill from there.*/
		forall i in indices of TAB_RESERVE_RATIOS
			update DEPR_CALC_COMBINED_STG
			set RESERVE_RATIO_ID = TAB_RESERVE_RATIOS(i).RESERVE_RATIO_ID
			where DEPR_METHOD_ID = TAB_RESERVE_RATIOS(i).DMR_ID
			and SET_OF_BOOKS_ID = TAB_RESERVE_RATIOS(i).SOB_ID
			and GL_POST_MO_YR = A_MONTH
			and COMBINED_CALC_OPTION = 1;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
	end P_GET_RESERVE_RATIOS;

	procedure P_BUILD_DEPR_VINTAGE_SUMMARY(A_MONTH date) is
		V_DEPR_GROUPS PKG_PP_COMMON.NUM_TABTYPE;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_BUILD_DEPR_VINTAGE_SUMMARY');
		/*Get list of depr groups to build depr_vintage_summary for*/
		select distinct STG.DEPR_GROUP_ID bulk collect
		  into V_DEPR_GROUPS
		  from DEPR_CALC_COMBINED_STG STG
		 where STG.COMBINED_CALC_OPTION = 1 /*Only build depr vintage summary for option 1*/
		   and STG.GL_POST_MO_YR = A_MONTH;

		/*Call build function*/
		PP_DEPR_PKG.P_BUILD_VINTAGE_SUMMARY(V_DEPR_GROUPS, A_MONTH);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
	end P_BUILD_DEPR_VINTAGE_SUMMARY;

  procedure P_CALC_AVG_LIFE(A_MONTH date) is
  begin
	  PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_CALC_AVG_LIFE');

	  /*Update vintages that exist in depr_vintage_summary*/
	  update DEPR_CALC_COMBINED_STG STG
		 set STG.AVG_REM_LIFE =
			   (select DECODE(sum(DECODE(RR_DVS.REMAINING_LIFE,
										 0,
										 0,
										 (RR_DVS.ACCUM_COST * (1 - RR_DVS.RESERVE_RATIO)) /
										 RR_DVS.REMAINING_LIFE)),
							  0,
							  0,
							  sum(RR_DVS.ACCUM_COST * (1 - RR_DVS.RESERVE_RATIO)) /
							  sum(DECODE(RR_DVS.REMAINING_LIFE,
										 0,
										 0,
										 (RR_DVS.ACCUM_COST * (1 - RR_DVS.RESERVE_RATIO)) /
										 RR_DVS.REMAINING_LIFE)))
				  from (
						 /*Top of union is to join depr_vintage_summary for vintages that have a reserve ratio row to join to (based on integer_age).
                         Bottom of union is to join on depr_vintage_summary records that are too old for reserve ratio*/
						 select DVS.DEPR_GROUP_ID,
								 DVS.SET_OF_BOOKS_ID,
								 DVS.ACCOUNTING_MONTH,
								 RR.REMAINING_LIFE,
								 DVS.ACCUM_COST,
								 RR.RESERVE_RATIO
						   from DEPR_VINTAGE_SUMMARY DVS, RESERVE_RATIOS RR, DEPR_CALC_COMBINED_STG STG
						  where DVS.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
							and DVS.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
							and DVS.ACCOUNTING_MONTH = STG.GL_POST_MO_YR
							and RR.RESERVE_RATIO_ID = STG.RESERVE_RATIO_ID
							and RR.INTEGER_AGE = TO_NUMBER(TO_CHAR(STG.GL_POST_MO_YR, 'YYYY')) - DVS.VINTAGE
						 union
						 select DVS.DEPR_GROUP_ID,
								 DVS.SET_OF_BOOKS_ID,
								 DVS.ACCOUNTING_MONTH,
								 0 as REMAINING_LIFE,
								 DVS.ACCUM_COST,
								 1 as RESERVE_RATIO
						   from DEPR_VINTAGE_SUMMARY DVS,
								 (select RESERVE_RATIO_ID, max(INTEGER_AGE) as MAX_AGE
									from RESERVE_RATIOS
								   group by RESERVE_RATIO_ID) RR,
								 DEPR_CALC_COMBINED_STG STG
						  where DVS.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
							and DVS.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
							and DVS.ACCOUNTING_MONTH = STG.GL_POST_MO_YR
							and RR.MAX_AGE < TO_NUMBER(TO_CHAR(STG.GL_POST_MO_YR, 'YYYY')) - DVS.VINTAGE /*If we're too old, use max_age and default 0 remaining life and 1 reserve ratio*/
						    and RR.RESERVE_RATIO_ID = STG.RESERVE_RATIO_ID) RR_DVS
				where RR_DVS.DEPR_GROUP_ID = STG.DEPR_GROUP_ID
				  and RR_DVS.SET_OF_BOOKS_ID = STG.SET_OF_BOOKS_ID
				  and RR_DVS.ACCOUNTING_MONTH = STG.GL_POST_MO_YR
				group by RR_DVS.DEPR_GROUP_ID, RR_DVS.SET_OF_BOOKS_ID, RR_DVS.ACCOUNTING_MONTH)
	   where STG.COMBINED_CALC_OPTION = 1 /*only calc avg remaining life for option 1*/
		 and STG.GL_POST_MO_YR = A_MONTH;

	  PKG_PP_ERROR.REMOVE_MODULE_NAME;
  exception
	  when others then
		  PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
  end P_CALC_AVG_LIFE;
	procedure P_DO_COMBINED_CALC(A_MONTH date) is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_DO_COMBINED_CALC - FIRST MODEL STATEMENT');
		MERGE INTO depr_calc_combined_stg A
		USING (
			SELECT DEPR_GROUP_ID, SET_OF_BOOKS_ID, COMBINED_CALC_OPTION,
				SKIP_DEPR_ALLOC, SKIP_COR_ALLOC, GL_POST_MO_YR,
				MID_PERIOD_CONV,
				NET_SALVAGE_PCT, COST_OF_REMOVAL_PCT, DEPRECIATION_BASE,
				COST_OF_REMOVAL_BASE, CDG_DEPR_GROUP_ID,
				CDG_OVER_DEPR_CHECK, CDG_NET_GROSS, CDG_NET_SALVAGE_PCT,
				CDG_COST_OF_REMOVAL_PCT, CDG_RATE, CDG_COR_RATE,
				CDG_DEPR_BASE, CDG_COR_BASE, BEGIN_BALANCE, COR_BEG_RESERVE, BEGIN_RESERVE,
				PLANT_ACTIVITY, RESERVE_ACTIVITY, COR_ACTIVITY, END_BALANCE, END_RESERVE,
				COR_END_RESERVE, DEPR_EXP_AND_ADJS, SALV_EXP_AND_ADJS, COR_EXP_AND_ADJS,
				DEPR_AMT_TO_ALLOC, COR_AMT_TO_ALLOC, COMBINED_OVER_DEPR,
				COMBINED_OVER_DEPR_COR, NET_PLANT, NET_COR, OVER_DEPR_ADJ,
				OVER_DEPR_ADJ_SALV, OVER_DEPR_ADJ_COR, DEPR_FACTOR, COR_FACTOR,
				RWIP_IN_OVER_DEPR, RWIP_ALLOCATION, RWIP_COST_OF_REMOVAL
			FROM depr_calc_combined_stg
			WHERE gl_post_mo_yr = a_month
			model PARTITION BY (cdg_depr_group_id, set_of_books_id)
			dimension BY(combined_calc_option, depr_group_id)
			measures (
				GL_POST_MO_YR,
				SKIP_DEPR_ALLOC,
				SKIP_COR_ALLOC,
				MID_PERIOD_CONV,
				NET_SALVAGE_PCT,
				COST_OF_REMOVAL_PCT,
				DEPRECIATION_BASE,
				COST_OF_REMOVAL_BASE,
				CDG_NET_GROSS,
				CDG_NET_SALVAGE_PCT,
				CDG_COST_OF_REMOVAL_PCT,
				CDG_RATE,
				CDG_OVER_DEPR_CHECK,
				CDG_COR_RATE,
				CDG_DEPR_BASE,
				CDG_COR_BASE,
				BEGIN_BALANCE,
				BEGIN_RESERVE,
				COR_BEG_RESERVE,
				PLANT_ACTIVITY,
				RESERVE_ACTIVITY,
				COR_ACTIVITY,
				END_BALANCE,
				END_RESERVE,
				COR_END_RESERVE,
				DEPR_EXP_AND_ADJS,
				SALV_EXP_AND_ADJS,
				COR_EXP_AND_ADJS,
				DEPR_AMT_TO_ALLOC,
				COR_AMT_TO_ALLOC,
				NET_PLANT,
				NET_COR,
				OVER_DEPR_ADJ,
				OVER_DEPR_ADJ_SALV,
				OVER_DEPR_ADJ_COR,
				DEPR_FACTOR,
				COR_FACTOR,
				IMPAIRMENT_ASSET_BEGIN_BALANCE,
				IMPAIRMENT_ASSET_ACTIVITY_SALV,
				RWIP_SALVAGE,
				RWIP_COR,
				COMBINED_OVER_DEPR,
				COMBINED_OVER_DEPR_COR,
				0 as DEPR_REDUCTION,
				0 as COR_REDUCTION,
				RWIP_IN_OVER_DEPR,
				RWIP_ALLOCATION,
				RWIP_COST_OF_REMOVAL
			)
			rules UPDATE (
			/*
			Prepare to calculate total allocation amount - Option 2
			*/
				/*Calculate depreciation base for each child group*/
				DEPRECIATION_BASE[2,any] = case
					when CDG_NET_GROSS[CV(),CV()] = 1 then
						(BEGIN_BALANCE [CV(),CV()] * (1 - CDG_NET_SALVAGE_PCT [CV(),CV()])
							+ (IMPAIRMENT_ASSET_BEGIN_BALANCE[CV(),CV()] * CDG_NET_SALVAGE_PCT [CV(),CV()])
							- BEGIN_RESERVE [CV(),CV()] - RWIP_SALVAGE [CV(),CV()])
						+ (MID_PERIOD_CONV [CV(),CV()] * (PLANT_ACTIVITY [CV(),CV()]
							* (1 - CDG_NET_SALVAGE_PCT [CV(),CV()])
							+ (IMPAIRMENT_ASSET_ACTIVITY_SALV[CV(),CV()] * CDG_NET_SALVAGE_PCT [CV(),CV()])
							- RESERVE_ACTIVITY[CV(),CV()]))
					else
						BEGIN_BALANCE[CV(),CV()] + (PLANT_ACTIVITY[CV(),CV()] * MID_PERIOD_CONV[CV(),CV()])
					end,
				COST_OF_REMOVAL_BASE[2,any] = case
					when CDG_NET_GROSS[CV(),CV()] = 1 then
						(BEGIN_BALANCE[CV(),CV()] + PLANT_ACTIVITY[CV(),CV()] * MID_PERIOD_CONV[CV(),CV()])
							* (CDG_COST_OF_REMOVAL_PCT[CV(),CV()])
						- (COR_BEG_RESERVE[CV(),CV()] + RWIP_COR[CV(),CV()] + COR_ACTIVITY[CV(),CV()] * MID_PERIOD_CONV[CV(),CV()])
						+ (IMPAIRMENT_ASSET_BEGIN_BALANCE[CV(),CV()] + IMPAIRMENT_ASSET_ACTIVITY_SALV[CV(),CV()] * MID_PERIOD_CONV[CV(),CV()])
							* (1 - CDG_COST_OF_REMOVAL_PCT[CV(),CV()])
					else
						DEPRECIATION_BASE[CV(),CV()]
					end,
				/*Calculate the total depreciation base for the parent group*/
				CDG_DEPR_BASE[2,any] = sum(DEPRECIATION_BASE) OVER (),
				CDG_COR_BASE[2,any] = sum(COST_OF_REMOVAL_BASE) OVER (),

			/*
			Calculate the total allocation amount - Option 1
			This is the sum of all the depreciation expense for the child depr groups, including all adjustments (except over_depr adjustment).
			*/
				DEPR_AMT_TO_ALLOC[1,any] = sum(DEPR_EXP_AND_ADJS + SALV_EXP_AND_ADJS) OVER (),
				COR_AMT_TO_ALLOC[1,any] = sum(COR_EXP_AND_ADJS) OVER (),

			/*
			Calculate the total allocation amount - Option 2
			This is calculated by multiplying the total depreciation base for the parent group by the parent group's method's rates
			*/
				DEPR_AMT_TO_ALLOC[2,any] = CDG_DEPR_BASE[CV(),CV()] * CDG_RATE[CV(),CV()] / 12,
				COR_AMT_TO_ALLOC[2,any] = CDG_COR_BASE[CV(),CV()] * CDG_COR_RATE[CV(),CV()] / 12,

			/*
			Prepare to calculate allocation factors - Option 1
			*/
				/*calculate net_plant*/
				NET_PLANT[1,any] = END_BALANCE[CV(),CV()] * (1 - NET_SALVAGE_PCT[CV(),CV()]) - END_RESERVE[CV(),CV()],
				NET_COR[1,any] = END_BALANCE[CV(),CV()] * COST_OF_REMOVAL_PCT[CV(),CV()] - COR_END_RESERVE[CV(),CV()],

			/*
			Omit over_depr gropus from the allocation - Option 1
			This is indicated by the group having a net_plant value that is zero or the opposite sign of the total net plant
			Since Option 1 is based on remaining life, we omit over depreciated child groups even if they do not have over_depr set on that depr_group
			*/
				SKIP_DEPR_ALLOC[1,any] = case
					when SKIP_DEPR_ALLOC[CV(),CV()] = 0 and (OVER_DEPR_ADJ[CV(),CV()] + OVER_DEPR_ADJ_SALV[CV(),CV()] <> 0 or NET_PLANT[CV(),CV()] = 0 or sign(NET_PLANT[CV(),CV()]) <> sign(sum(END_BALANCE) OVER ())) then
						1
					else
						SKIP_DEPR_ALLOC[CV(),CV()]
					end,
				SKIP_COR_ALLOC[1,any] = case
					when SKIP_COR_ALLOC[CV(),CV()] = 0 and (OVER_DEPR_ADJ_COR[CV(),CV()] <> 0 or NET_COR[CV(),CV()] = 0 or sign(NET_COR[CV(),CV()]) <> sign(sum(NET_COR) OVER ())) then
						1
					else
						SKIP_COR_ALLOC[CV(),CV()]
					end,

			/*
			Check for groups to omit from the allocation - Option 2
			This is indicated by the group having a non-zero over depr amount or a depr_group having a depreciation expense
			with the opposite sign of the total depreciation expense.
			*/
				SKIP_DEPR_ALLOC[2,any] = case
					when SKIP_DEPR_ALLOC[CV(),CV()] = 0 and (OVER_DEPR_ADJ[CV(),CV()] + OVER_DEPR_ADJ_SALV[CV(),CV()] <> 0
					or sign(DEPR_EXP_AND_ADJS[CV(),CV()] + SALV_EXP_AND_ADJS[CV(),CV()]) <> sign(sum(DEPR_EXP_AND_ADJS + SALV_EXP_AND_ADJS) OVER ())) then
						1
					else
						SKIP_DEPR_ALLOC[CV(),CV()]
					end,
				SKIP_COR_ALLOC[2,any] = case
					when SKIP_COR_ALLOC[CV(),CV()] = 0 and (OVER_DEPR_ADJ_COR[CV(),CV()] <> 0
					or sign(COR_EXP_AND_ADJS[CV(),CV()]) <> sign(sum(COR_EXP_AND_ADJS) OVER ())) then
						1
					else
						SKIP_COR_ALLOC[CV(),CV()]
					end,

			/*
			Remove cost of skipped, over-depr'ed depr groups from the total to allocate.
			*/
				/*total the amount that we need to remove*/
				DEPR_REDUCTION[any,any] = sum(decode(SKIP_DEPR_ALLOC, 0, 0, DEPR_EXP_AND_ADJS + SALV_EXP_AND_ADJS + OVER_DEPR_ADJ + OVER_DEPR_ADJ_SALV)) OVER (),
				COR_REDUCTION[any,any] = sum(decode(SKIP_COR_ALLOC, 0, 0, COR_EXP_AND_ADJS + OVER_DEPR_ADJ_COR)) OVER (),
				/*adjust amt to allocate*/
				DEPR_AMT_TO_ALLOC[any,any] = DEPR_AMT_TO_ALLOC[CV(),CV()] - DEPR_REDUCTION[CV(),CV()],
				COR_AMT_TO_ALLOC[any,any] = COR_AMT_TO_ALLOC[CV(),CV()] - COR_REDUCTION[CV(),CV()],

			/*
			Do combined group level over_depr check - Option 2 (Option 1 doesn't have a depr_method, so there's no way for users to set it to do over_depr_check)
			*/
				/*Life first */
				COMBINED_OVER_DEPR[2,any] = case
					when CDG_OVER_DEPR_CHECK[CV(),CV()] in (1,2) then /*1 = life and cor separate, 2 = life only*/
						PKG_DEPR_CALC.F_OVERDEPR_SINGLE_ROW(
							sum(END_BALANCE) OVER (), --A_END_BALANCE
							sum(BEGIN_RESERVE + RESERVE_ACTIVITY - (IMPAIRMENT_ASSET_BEGIN_BALANCE + IMPAIRMENT_ASSET_ACTIVITY_SALV)) OVER () + DEPR_AMT_TO_ALLOC[CV(),CV()], --A_END_RESERVE - Basically, get our begin reserve and activity from the children, then use our alloc amount as the expense
							DEPR_AMT_TO_ALLOC[CV(),CV()] + sum(RWIP_IN_OVER_DEPR * (RWIP_ALLOCATION - RWIP_COST_OF_REMOVAL)) OVER () , --A_EXPENSE
							CDG_NET_SALVAGE_PCT[CV(),CV()] --A_NET_SALVAGE_PCT
						)
					when CDG_OVER_DEPR_CHECK[CV(),CV()] = 3 then /*3 = combined life and cor*/
						PKG_DEPR_CALC.F_OVERDEPR_SINGLE_ROW(
							sum(END_BALANCE) OVER (), --A_END_BALANCE
							sum(BEGIN_RESERVE + RESERVE_ACTIVITY + COR_BEG_RESERVE + COR_ACTIVITY - (IMPAIRMENT_ASSET_BEGIN_BALANCE + IMPAIRMENT_ASSET_ACTIVITY_SALV) * CDG_NET_SALVAGE_PCT) OVER () + DEPR_AMT_TO_ALLOC[CV(),CV()] + COR_AMT_TO_ALLOC[CV(),CV()], --A_END_RESERVE - Basically, get our begin reserve and activity from the children, then use our alloc amount as the expense
							DEPR_AMT_TO_ALLOC[CV(),CV()] + COR_AMT_TO_ALLOC[CV(),CV()] + sum(RWIP_IN_OVER_DEPR * RWIP_ALLOCATION) OVER (), --A_EXPENSE
							CDG_NET_SALVAGE_PCT[CV(),CV()] + CDG_COST_OF_REMOVAL_PCT[CV(),CV()] --A_NET_SALVAGE_PCT /*CBS TODO verify that this should be net_salv - cor, not net_salv + cor*/
						)
					else
						0
					end,
				/*Then COR*/
				COMBINED_OVER_DEPR_COR[2,any] = case
					when CDG_OVER_DEPR_CHECK[CV(),CV()] = 1 then /*1 = life and cor separate*/
						PKG_DEPR_CALC.F_OVERDEPR_SINGLE_ROW(
							sum(END_BALANCE) OVER (), --A_END_BALANCE
							sum(COR_BEG_RESERVE + COR_ACTIVITY) OVER () + COR_AMT_TO_ALLOC[CV(),CV()], --A_END_RESERVE - Basically, get our begin reserve and activity from the children, then use our alloc amount as the expense
							COR_AMT_TO_ALLOC[CV(),CV()] + sum(RWIP_IN_OVER_DEPR * RWIP_COST_OF_REMOVAL) OVER (), --A_EXPENSE
							1 - CDG_COST_OF_REMOVAL_PCT[CV(),CV()] --A_NET_SALVAGE_PCT
						)
					when CDG_OVER_DEPR_CHECK[CV(),CV()] = 3 then /*3 = combined life and cor*/
						/*Pro-rate the over_depr amount*/
						decode(COR_AMT_TO_ALLOC[CV(),CV()] + DEPR_AMT_TO_ALLOC[CV(),CV()] + sum(RWIP_IN_OVER_DEPR * RWIP_ALLOCATION) OVER (),
							0,
							0,
							COMBINED_OVER_DEPR[CV(),CV()] * (COR_AMT_TO_ALLOC[CV(),CV()] + sum(RWIP_IN_OVER_DEPR * RWIP_COST_OF_REMOVAL) OVER ())
								/ (COR_AMT_TO_ALLOC[CV(),CV()] + DEPR_AMT_TO_ALLOC[CV(),CV()] + sum(RWIP_IN_OVER_DEPR * RWIP_ALLOCATION) OVER ())) /*DEPR_AMT_TO_ALLOC includes salv to alloc*/
					else /*This is either 0 (no over depr check) or 2 (life-only over depr check)*/
						0
					end,
				/*Re-foot life after prorating*/
				COMBINED_OVER_DEPR[2,any] = case
					when CDG_OVER_DEPR_CHECK[CV(),CV()] = 3 then
						COMBINED_OVER_DEPR[CV(),CV()] - COMBINED_OVER_DEPR_COR[CV(),CV()]
					else
						COMBINED_OVER_DEPR[CV(),CV()]
					end,
				/*Reduce the amount to allocate by the over depr amount*/
				DEPR_AMT_TO_ALLOC[2,any] = DEPR_AMT_TO_ALLOC[CV(),CV()] + COMBINED_OVER_DEPR[CV(),CV()],
				COR_AMT_TO_ALLOC[2,any] = COR_AMT_TO_ALLOC[CV(),CV()] + COMBINED_OVER_DEPR_COR[CV(),CV()]
			)) B
		on (A.DEPR_GROUP_ID = B.DEPR_GROUP_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POST_MO_YR = B.GL_POST_MO_YR)
		when matched then
			update
				set DEPRECIATION_BASE = B.DEPRECIATION_BASE,
				COST_OF_REMOVAL_BASE = B.COST_OF_REMOVAL_BASE,
				CDG_DEPR_BASE = B.CDG_DEPR_BASE,
				CDG_COR_BASE = B.CDG_COR_BASE,
				DEPR_AMT_TO_ALLOC = B.DEPR_AMT_TO_ALLOC,
				COR_AMT_TO_ALLOC = B.COR_AMT_TO_ALLOC,
				NET_PLANT = B.NET_PLANT,
				NET_COR = B.NET_COR,
				SKIP_DEPR_ALLOC = B.SKIP_DEPR_ALLOC,
				SKIP_COR_ALLOC = B.SKIP_COR_ALLOC,
				COMBINED_OVER_DEPR = B.COMBINED_OVER_DEPR,
				COMBINED_OVER_DEPR_COR = B.COMBINED_OVER_DEPR_COR
		;

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_DO_COMBINED_CALC - SECOND MODEL STATEMENT');

		/*I'm using a second model/merge statement so that I can include skip_depr_alloc and skip_cor_alloc as dimensions
		instead of using  bunch of case statements to check when they are set. We cannot omit them completely
		because there might be some that are allocating for depr and not cor or vice versa.*/
		MERGE INTO depr_calc_combined_stg A
		USING (
			SELECT *
			FROM depr_calc_combined_stg
			WHERE gl_post_mo_yr = a_month
			model PARTITION BY (cdg_depr_group_id, set_of_books_id)
			dimension BY(combined_calc_option, depr_group_id, skip_depr_alloc, skip_cor_alloc)
			measures (
				GL_POST_MO_YR,
				SKIP_DEPR_REASON,
				SKIP_COR_REASON,
				DEPR_FACTOR,
				COR_FACTOR,
				NET_PLANT,
				NET_COR,
				AVG_REM_LIFE,
				COMBINED_OVER_DEPR,
				COMBINED_OVER_DEPR_COR,
				DEPR_AMT_TO_ALLOC,
				DEPR_EXP_AND_ADJS,
				SALV_EXP_AND_ADJS,
				OVER_DEPR_ADJ,
				OVER_DEPR_ADJ_SALV,
				COR_AMT_TO_ALLOC,
				COR_EXP_AND_ADJS,
				OVER_DEPR_ADJ_COR,
				CDG_OVER_DEPR_CHECK,
				END_BALANCE,
				BEGIN_RESERVE,
				RESERVE_ACTIVITY,
				CDG_NET_SALVAGE_PCT,
				COR_BEG_RESERVE,
				COR_ACTIVITY,
				CDG_COST_OF_REMOVAL_PCT,
				TARGET_DEPR_AMT,
				TARGET_COR_AMT,
				0 AS COMBINED_DEPR_ADJ,
				0 AS COMBINED_SALV_ADJ,
				0 AS COMBINED_COR_ADJ,
				0 AS THEO_EXP_DEPR,
				0 AS THEO_EXP_COR,
				0 AS TOTAL_THEO_EXP_DEPR,
				0 AS TOTAL_THEO_EXP_COR
			)
			rules UPDATE (
			/*
			Set message for skipped depr groups.
			*/
				SKIP_DEPR_REASON[1,any,1,any] = 'Removed from allocation because net_plant is zero or opposite sign of total net_plant. This is an informational message, not a warning or error.',
				SKIP_COR_REASON[1,any,any,1] = 'Removed from allocation because net cost of removal is zero or opposite sign of total net cost of removal. This is an informational message, not a warning or error.',
				SKIP_DEPR_REASON[2,any,1,any] = 'Removed from allocation because the group has a life or salvage over depreciation adjustment or the depreciation expense is the opposite sign of the total depreciation expense. This is an informational message, not a warning or error.',
				SKIP_COR_REASON[2,any,any,1] = 'Removed from allocation because the group has a cost of removal over depreciation adjustment or the cost of removal expense is the opposite sign of the total cost of removal expense. This is an informational message, not a warning or error.',

			/*
			Calculate theoretical expense for each child group - Option 1
			*/
				THEO_EXP_DEPR[1,any,0,any] = decode(AVG_REM_LIFE[CV(),CV(),CV(),CV()] * 12,
					0,
					0,
					NET_PLANT[CV(),CV(),CV(),CV()] / (AVG_REM_LIFE[CV(),CV(),CV(),CV()] * 12)),
				THEO_EXP_COR[1,any,any,0] = decode(AVG_REM_LIFE[CV(),CV(),CV(),CV()] * 12,
					0,
					0,
					NET_COR[CV(),CV(),CV(),CV()] / (AVG_REM_LIFE[CV(),CV(),CV(),CV()] * 12)),

			/*
			Calculate total theoretical expense for the parent group - Option 1
			*/
				TOTAL_THEO_EXP_DEPR[1,any,0,any] = sum(decode(SKIP_DEPR_ALLOC, 0, THEO_EXP_DEPR, 0)) OVER (),
				TOTAL_THEO_EXP_COR[1,any,any,0] = sum(decode(SKIP_COR_ALLOC, 0, THEO_EXP_COR, 0)) OVER (),

			/*
			Calculate allocation factors - Option 1
			This is the theoretical expense for the specific child group over the total theoretical expense for the combined group.
			*/
				DEPR_FACTOR[1,any,0,any] = decode(TOTAL_THEO_EXP_DEPR[CV(),CV(),CV(),CV()],
					0,
					0,
					THEO_EXP_DEPR[CV(),CV(),CV(),CV()] / TOTAL_THEO_EXP_DEPR[CV(),CV(),CV(),CV()]),
				COR_FACTOR[1,any,any,0] = decode(TOTAL_THEO_EXP_COR[CV(),CV(),CV(),CV()],
					0,
					0,
					THEO_EXP_COR[CV(),CV(),CV(),CV()] / TOTAL_THEO_EXP_COR[CV(),CV(),CV(),CV()]),

			/*
			Calculate allocation factors - Option 2
			This is the actual expense incurred for the specific child group over the total expense incurred for the combined group.
			*/
				DEPR_FACTOR[2,any,0,any] = decode(sum(decode(SKIP_DEPR_ALLOC, 0, DEPR_EXP_AND_ADJS + SALV_EXP_AND_ADJS, 0)) OVER (),
					0,
					0,
					(DEPR_EXP_AND_ADJS[CV(),CV(),CV(),CV()] + SALV_EXP_AND_ADJS[CV(),CV(),CV(),CV()]) /
						sum(decode(SKIP_DEPR_ALLOC, 0, DEPR_EXP_AND_ADJS + SALV_EXP_AND_ADJS, 0)) OVER ()),
				COR_FACTOR[2,any,any,0] = decode(sum(decode(SKIP_COR_ALLOC, 0, COR_EXP_AND_ADJS,0)) OVER (),
					0,
					0,
					COR_EXP_AND_ADJS[CV(),CV(),CV(),CV()] /
						sum(decode(SKIP_COR_ALLOC, 0, COR_EXP_AND_ADJS,0)) OVER ()),

			/*
			Calculate target expense amounts
			*/
				TARGET_DEPR_AMT[any,any,0,any] = DEPR_AMT_TO_ALLOC[CV(),CV(),CV(),CV()] * DEPR_FACTOR[CV(),CV(),CV(),CV()],
				TARGET_COR_AMT[any,any,any,0] = COR_AMT_TO_ALLOC[CV(),CV(),CV(),CV()] * COR_FACTOR[CV(),CV(),CV(),CV()],

			/*
			Calculate combined expense adjustments
			*/
				COMBINED_DEPR_ADJ[any,any,0,any] = TARGET_DEPR_AMT[CV(),CV(),CV(),CV()] - DEPR_EXP_AND_ADJS[CV(),CV(),CV(),CV()] - SALV_EXP_AND_ADJS[CV(),CV(),CV(),CV()],
				COMBINED_COR_ADJ[any,any,any,0] = TARGET_COR_AMT[CV(),CV(),CV(),CV()] - COR_EXP_AND_ADJS[CV(),CV(),CV(),CV()],

			/*
			Prorate combined depr adjustment between life and salv
			*/
				/*Prorate*/
				COMBINED_SALV_ADJ[any,any,0,any] =
					round(decode(SALV_EXP_AND_ADJS[CV(),CV(),CV(),CV()] + DEPR_EXP_AND_ADJS[CV(),CV(),CV(),CV()],
						0,
						0,
						COMBINED_DEPR_ADJ[CV(),CV(),CV(),CV()] * SALV_EXP_AND_ADJS[CV(),CV(),CV(),CV()] /
							(SALV_EXP_AND_ADJS[CV(),CV(),CV(),CV()] + DEPR_EXP_AND_ADJS[CV(),CV(),CV(),CV()])), 2),
				/*Re-foot life*/
				COMBINED_DEPR_ADJ[any,any,0,any] = COMBINED_DEPR_ADJ[CV(),CV(),CV(),CV()] - COMBINED_SALV_ADJ[CV(),CV(),CV(),CV()]
		)) B
		on (A.DEPR_GROUP_ID = B.DEPR_GROUP_ID and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID and A.GL_POST_MO_YR = B.GL_POST_MO_YR)
		when matched then
			update
			set SKIP_DEPR_REASON = B.SKIP_DEPR_REASON,
				SKIP_COR_REASON = B.SKIP_COR_REASON,
				DEPR_AMT_TO_ALLOC = B.DEPR_AMT_TO_ALLOC,
				COR_AMT_TO_ALLOC = B.COR_AMT_TO_ALLOC,
				COMBINED_OVER_DEPR = B.COMBINED_OVER_DEPR,
				COMBINED_OVER_DEPR_COR = B.COMBINED_OVER_DEPR_COR,
				DEPR_FACTOR = B.DEPR_FACTOR,
				COR_FACTOR = B.COR_FACTOR,
				TARGET_DEPR_AMT = B.TARGET_DEPR_AMT,
				TARGET_COR_AMT = B.TARGET_COR_AMT,
				COMBINED_DEPR_ADJ = B.COMBINED_DEPR_ADJ,
				COMBINED_COR_ADJ = B.COMBINED_COR_ADJ,
				COMBINED_SALV_ADJ = B.COMBINED_SALV_ADJ;

		/*Take into account any rounding error. We are safe to base this on the sum of target_depr_amt
		since no rounding error can occur on the combined columns since we use addition/subtraction to re-foot anything.
		we need to do this as two statements since we might be putting the rounding error on different
		depr groups for depr and cor*/
		/*depr expense*/
		update DEPR_CALC_COMBINED_STG STG1
		set DEPR_ROUNDING_PLUG = (
			SELECT plug2.plug
			FROM (
				SELECT Min(stg.depr_group_id) DEPR_GROUP_ID, stg.set_of_books_id, stg.gl_post_mo_yr, depr_plug.plug
				FROM (
						SELECT cdg_depr_group_id, set_of_books_id, gl_post_mo_yr, Max(Abs(target_depr_amt)) target_depr_amt, Max(depr_amt_to_alloc) - Sum(target_depr_amt) plug
						FROM depr_calc_combined_stg
						WHERE skip_depr_alloc = 0
						GROUP BY cdg_depr_group_id, set_of_books_id, gl_post_mo_yr
					) depr_plug,
					depr_calc_combined_stg stg
				WHERE stg.cdg_depr_group_id = depr_plug.cdg_depr_group_id
				AND stg.set_of_books_id = depr_plug.set_of_books_id
				AND stg.gl_post_mo_yr = depr_plug.gl_post_mo_yr
				AND stg.target_depr_amt = depr_plug.target_depr_amt * Sign(stg.target_depr_amt)
				AND stg.skip_depr_alloc = 0
				GROUP BY stg.cdg_depr_group_id, stg.set_of_books_id, stg.gl_post_mo_yr, depr_plug.plug
			) plug2
			where STG1.DEPR_GROUP_ID = PLUG2.DEPR_GROUP_ID
			and STG1.SET_OF_BOOKS_ID = PLUG2.SET_OF_BOOKS_ID
			and STG1.GL_POST_MO_YR = PLUG2.GL_POST_MO_YR
		)
		WHERE EXISTS (
			SELECT 1
			FROM (
				SELECT Min(stg.depr_group_id) DEPR_GROUP_ID, stg.set_of_books_id, stg.gl_post_mo_yr, depr_plug.plug
				FROM (
						SELECT cdg_depr_group_id, set_of_books_id, gl_post_mo_yr, Max(Abs(target_depr_amt)) target_depr_amt, Max(depr_amt_to_alloc) - Sum(target_depr_amt) plug
						FROM depr_calc_combined_stg
						WHERE skip_depr_alloc = 0
						GROUP BY cdg_depr_group_id, set_of_books_id, gl_post_mo_yr
					) depr_plug,
					depr_calc_combined_stg stg
				WHERE stg.cdg_depr_group_id = depr_plug.cdg_depr_group_id
				AND stg.set_of_books_id = depr_plug.set_of_books_id
				AND stg.gl_post_mo_yr = depr_plug.gl_post_mo_yr
				AND stg.target_depr_amt = depr_plug.target_depr_amt * Sign(stg.target_depr_amt)
				AND stg.skip_depr_alloc = 0
				GROUP BY stg.cdg_depr_group_id, stg.set_of_books_id, stg.gl_post_mo_yr, depr_plug.plug
			) plug2
			where STG1.DEPR_GROUP_ID = PLUG2.DEPR_GROUP_ID
			and STG1.SET_OF_BOOKS_ID = PLUG2.SET_OF_BOOKS_ID
			and STG1.GL_POST_MO_YR = PLUG2.GL_POST_MO_YR
		);

		/*cor expense*/
		update DEPR_CALC_COMBINED_STG STG1
		set STG1.COR_ROUNDING_PLUG = (
			SELECT plug2.plug
			FROM (
				SELECT Min(stg.depr_group_id) DEPR_GROUP_ID, stg.set_of_books_id, stg.gl_post_mo_yr, cor_plug.plug
				FROM (
						SELECT cdg_depr_group_id, set_of_books_id, gl_post_mo_yr, Max(Abs(target_cor_amt)) target_cor_amt, Max(cor_amt_to_alloc) - Sum(target_cor_amt) plug
						FROM depr_calc_combined_stg
						WHERE skip_cor_alloc = 0
						GROUP BY cdg_depr_group_id, set_of_books_id, gl_post_mo_yr
					) cor_plug,
					depr_calc_combined_stg stg
				WHERE stg.cdg_depr_group_id = cor_plug.cdg_depr_group_id
				AND stg.set_of_books_id = cor_plug.set_of_books_id
				AND stg.gl_post_mo_yr = cor_plug.gl_post_mo_yr
				AND stg.target_cor_amt = cor_plug.target_cor_amt * Sign(stg.target_cor_amt)
				AND stg.skip_cor_alloc = 0
				GROUP BY stg.cdg_depr_group_id, stg.set_of_books_id, stg.gl_post_mo_yr, cor_plug.plug
			) plug2
			where STG1.DEPR_GROUP_ID = PLUG2.DEPR_GROUP_ID
			and STG1.SET_OF_BOOKS_ID = PLUG2.SET_OF_BOOKS_ID
			and STG1.GL_POST_MO_YR = PLUG2.GL_POST_MO_YR
		)
		WHERE EXISTS (
			SELECT 1
			FROM (
				SELECT Min(stg.depr_group_id) DEPR_GROUP_ID, stg.set_of_books_id, stg.gl_post_mo_yr, cor_plug.plug
				FROM (
						SELECT cdg_depr_group_id, set_of_books_id, gl_post_mo_yr, Max(Abs(target_cor_amt)) target_cor_amt, Max(cor_amt_to_alloc) - Sum(target_cor_amt) plug
						FROM depr_calc_combined_stg
						WHERE skip_cor_alloc = 0
						GROUP BY cdg_depr_group_id, set_of_books_id, gl_post_mo_yr
					) cor_plug,
					depr_calc_combined_stg stg
				WHERE stg.cdg_depr_group_id = cor_plug.cdg_depr_group_id
				AND stg.set_of_books_id = cor_plug.set_of_books_id
				AND stg.gl_post_mo_yr = cor_plug.gl_post_mo_yr
				AND stg.target_cor_amt = cor_plug.target_cor_amt * Sign(stg.target_cor_amt)
				AND stg.skip_cor_alloc = 0
				GROUP BY stg.cdg_depr_group_id, stg.set_of_books_id, stg.gl_post_mo_yr, cor_plug.plug
			) plug2
			where STG1.DEPR_GROUP_ID = PLUG2.DEPR_GROUP_ID
			and STG1.SET_OF_BOOKS_ID = PLUG2.SET_OF_BOOKS_ID
			and STG1.GL_POST_MO_YR = PLUG2.GL_POST_MO_YR
		);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
	end P_DO_COMBINED_CALC;

	procedure P_COPY_RESULTS(A_MONTH date) is
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_COPY_RESULTS');

		/*Copy our results back to the staging table and adjust our ending balances.*/
		update DEPR_CALC_STG CALC
		   set (COMBINED_DEPR_ADJ, COMBINED_SALV_ADJ, COMBINED_COR_ADJ,
				END_RESERVE, COR_END_RESERVE) =
				(select COMB.COMBINED_DEPR_ADJ + COMB.DEPR_ROUNDING_PLUG, COMB.COMBINED_SALV_ADJ, COMB.COMBINED_COR_ADJ + COMB.COR_ROUNDING_PLUG,
						CALC.END_RESERVE + COMB.COMBINED_DEPR_ADJ + COMB.DEPR_ROUNDING_PLUG + COMB.COMBINED_SALV_ADJ,
						CALC.COR_END_RESERVE + COMB.COMBINED_COR_ADJ + COMB.COR_ROUNDING_PLUG
				   from DEPR_CALC_COMBINED_STG COMB
				  where COMB.DEPR_GROUP_ID = CALC.DEPR_GROUP_ID
					and COMB.SET_OF_BOOKS_ID = CALC.SET_OF_BOOKS_ID
					and COMB.GL_POST_MO_YR = CALC.GL_POST_MO_YR
					and COMB.GL_POST_MO_YR = A_MONTH)
		 where exists (select 1
				  from DEPR_CALC_COMBINED_STG COMB
				 where COMB.DEPR_GROUP_ID = CALC.DEPR_GROUP_ID
				   and COMB.SET_OF_BOOKS_ID = CALC.SET_OF_BOOKS_ID
				   and COMB.GL_POST_MO_YR = CALC.GL_POST_MO_YR
				   and COMB.GL_POST_MO_YR = A_MONTH);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
	end P_COPY_RESULTS;

	procedure P_COMBINED_DEPR_CALC(A_FCST_VERSION_ID number, A_MONTH date) is
		NUM_RESULTS integer;
	begin
		PKG_PP_ERROR.SET_MODULE_NAME('PKG_COMBINED_DEPR.P_COMBINED_DEPR_CALC');

		NUM_RESULTS := F_STG_COMBINED_DEPR(A_FCST_VERSION_ID, A_MONTH);
		if NUM_RESULTS <= 0 then
			/*Nothing to do - return*/
			return;
		end if;

		P_VALIDATE_COMBINED_DEPR(A_MONTH);
		P_GET_RESERVE_RATIOS(A_MONTH);
		P_BUILD_DEPR_VINTAGE_SUMMARY(A_MONTH);
		P_CALC_AVG_LIFE(A_MONTH);
		P_DO_COMBINED_CALC(A_MONTH);
		P_COPY_RESULTS(A_MONTH);

		PKG_PP_ERROR.REMOVE_MODULE_NAME;
	exception
		when others then
			PKG_PP_ERROR.RAISE_ERROR('ERROR', 'Failed Operation: ' || sqlerrm);
	end P_COMBINED_DEPR_CALC;

--**************************************************************************************
-- Initialization procedure (Called the first time this package is opened in a session)
--**************************************************************************************
begin
	null; /*Placeholder - no initialization needed.*/
end PKG_DEPR_COMBINED;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1468, 0, 10, 4, 3, 0, 40144, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040144_depr_PKG_DEPR_COMBINED.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;