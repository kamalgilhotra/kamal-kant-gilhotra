/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047037_lease_add_var_comp_company_import_column_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/19/2017 Jared Watkins    add Company as a required column translating off of the Description
||============================================================================
*/

--add the new import column for this import type
insert into pp_import_column(import_type_id, column_name, description, import_column_name, 
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
select 264, 'company_id', 'Company Identifier', 'company_xlate',
  1, 1, 'number(22,0)', 'company_setup', 'company_id'
from dual
where not exists (select 1 from pp_import_column where import_type_id = 264 and column_name = 'company_id')
;

--associate the column in the import with the lookup translation
insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
select 264, 'company_id', 4512
from dual
where not exists (select 1 from pp_import_column_lookup where import_type_id = 264 and column_name = 'company_id')
;

--move the ILR and Asset translations to after the Company translation will be done
update pp_import_column set processing_order = 2 
where import_type_id = 264 and description in ('ILR Number', 'LS Asset Number')
;

--update the existing translations for the ILR/Asset ID to also use the Company ID
update pp_import_lookup set lookup_constraining_columns = 'company_id' 
where import_lookup_id in (2505,2506)
;
update pp_import_lookup set lookup_sql = '( select b.ilr_id from ls_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.ilr_number ) ) and <importtable>.company_id = b.company_id )'
where import_lookup_id = 2505
;
update pp_import_lookup set lookup_sql = '( select b.ls_asset_id from ls_asset b where upper( trim( <importfield> ) ) = upper( trim( b.leased_asset_number ) ) and <importtable>.company_id = b.company_id )'
where import_lookup_id = 2506
;

--move the non-required fields and fields depending on the company ID translation down in the template
update pp_import_template_fields set field_id = field_id + 1 
where import_template_id = (select import_template_id from pp_import_template
                            where import_type_id = 264 and description = 'VP Var Component Amounts Add') 
and field_id > 1
;

--add the fields for the template into the DB
insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select import_template_id, 2, 264, 'company_id', 4512
from pp_import_template
where import_type_id = 264 and description = 'VP Var Component Amounts Add'
and not exists (select 1 from pp_import_template_fields where import_template_id = 1174 and column_name = 'company_id')
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3507, 0, 2017, 1, 0, 0, 47037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047037_lease_add_var_comp_company_import_column_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;