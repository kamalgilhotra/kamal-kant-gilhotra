 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_042095_pcm_tax_status_wksp_dml.sql
|| Description: Add tax status workspace to PCM port
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/16/2014 Ryan Oliveria  Tax Status Workspace
||============================================================================
*/

/************* FUNDING PROJECT **************/

insert into PPBASE_WORKSPACE
	(MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
values
	('pcm', 'fp_maint_tax_status', 'Tax Status (FP)', 'uo_pcm_maint_wksp_tax_status_fp', 'Tax Status (FP)', 1);


update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where MODULE = 'pcm'
   and PARENT_MENU_IDENTIFIER = 'fp_maintain'
   and ITEM_ORDER > 7;


insert into PPBASE_MENU_ITEMS
	(MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
	('pcm', 'fp_maint_tax_status', 	4, 8, 'Tax Status', 'Funding Project Tax Status', 'fp_maintain', 'fp_maint_tax_status',  1);

/************* WORK ORDER *******************/

insert into PPBASE_WORKSPACE
	(MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
values
	('pcm', 'wo_maint_tax_status', 'Tax Status (WO)', 'uo_pcm_maint_wksp_tax_status_wo', 'Tax Status (WO)', 1);


update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where MODULE = 'pcm'
   and PARENT_MENU_IDENTIFIER = 'wo_maintain'
   and ITEM_ORDER > 6;


insert into PPBASE_MENU_ITEMS
	(MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
	('pcm', 'wo_maint_tax_status', 	4, 7, 'Tax Status', 'Work Order Tax Status', 'wo_maintain', 'wo_maint_tax_status',  1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2203, 0, 2015, 1, 0, 0, 042095, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042095_pcm_tax_status_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;