/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046771_websys_view_mobile_updates_security_key_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 11/15/2016 Jared Watkins	     Add missing security key to view Mobile Updates
||============================================================================
*/

insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Updates.Details', 2, null, 'Mobile Updates', 'Mobile Updates', 'View Updates' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Updates.Details');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3339, 0, 2016, 1, 0, 0, 046771, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046771_websys_view_mobile_updates_security_key_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;