/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033003_lease_query_field.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/08/2013 Ryan Oliveria  Make Number of Assets field display as qty
||============================================================================
*/

update PP_ANY_QUERY_CRITERIA_FIELDS
   set DETAIL_FIELD = 'qty_number_of_assets'
 where DETAIL_FIELD = 'number_of_assets'
   and ID in (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION like 'ILR%');

update PP_ANY_QUERY_CRITERIA_FIELDS
   set COLUMN_HEADER = 'Qty Number Of Assets'
 where COLUMN_HEADER = 'Number Of Assets'
   and ID in (select ID from PP_ANY_QUERY_CRITERIA where DESCRIPTION like 'ILR%');

update PP_ANY_QUERY_CRITERIA
   set sql = replace(sql, 'number_of_assets', 'qty_number_of_assets')
 where DESCRIPTION like 'ILR%';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (660, 0, 10, 4, 1, 1, 33003, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033003_lease_query_field.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
