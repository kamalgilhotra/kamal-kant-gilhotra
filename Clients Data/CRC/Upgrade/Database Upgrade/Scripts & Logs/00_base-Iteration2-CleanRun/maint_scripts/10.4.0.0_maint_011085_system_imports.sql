/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_system_imports.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/07/2012 Stephen Wicks  Point Release
||============================================================================
*/

-- pp_import_type
create table PP_IMPORT_TYPE
(
 IMPORT_TYPE_ID       number(22,0)  not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(35)  not null,
 LONG_DESCRIPTION     varchar2(254),
 IMPORT_TABLE_NAME    varchar2(35)  not null,
 ARCHIVE_TABLE_NAME   varchar2(35)  not null,
 PP_REPORT_FILTER_ID  number(22,0),
 ALLOW_UPDATES_ON_ADD number(22,0)  not null,
 DELEGATE_OBJECT_NAME varchar2(50)  not null
);

alter table pp_import_type
   add constraint PP_IMPORT_TYPE_PK
       primary key (IMPORT_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_TYPE
   add constraint PP_IMPORT_TYPE_FILTER_FK
       foreign key (PP_REPORT_FILTER_ID)
       references PP_REPORTS_FILTER;

-- pp_import_subsystem
create table PP_IMPORT_SUBSYSTEM
(
 IMPORT_SUBSYSTEM_ID number(22,0)   not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 DESCRIPTION         varchar2(35)   not null,
 LONG_DESCRIPTION    varchar2(254)
);

alter table PP_IMPORT_SUBSYSTEM
   add constraint PP_IMPORT_SUBSYSTEM_PK
       primary key (IMPORT_SUBSYSTEM_ID)
       using index tablespace PWRPLANT_IDX;

-- pp_import_type_subsystem
create table PP_IMPORT_TYPE_SUBSYSTEM
(
 IMPORT_TYPE_ID      number(22,0)   not null,
 IMPORT_SUBSYSTEM_ID number(22,0)   not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table PP_IMPORT_TYPE_SUBSYSTEM
   add constraint PP_IMPORT_TYPE_SUBSYSTEM_PK
       primary key (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_TYPE_SUBSYSTEM
   add constraint PP_IMPT_TYPE_SUBS_IMPT_TYPE_FK
       foreign key (IMPORT_TYPE_ID)
       references PP_IMPORT_TYPE;

alter table PP_IMPORT_TYPE_SUBSYSTEM
   add constraint PP_IMPT_TYPE_SUBS_IMPT_SUBS_FK
       foreign key (IMPORT_SUBSYSTEM_ID)
       references PP_IMPORT_SUBSYSTEM;

-- pp_import_column
create table PP_IMPORT_COLUMN
(
 IMPORT_TYPE_ID          number(22,0)   not null,
 COLUMN_NAME             varchar2(35)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DESCRIPTION             varchar2(35)   not null,
 IMPORT_COLUMN_NAME      varchar2(35),
 IS_REQUIRED             number(22,0)   not null,
 PROCESSING_ORDER        number(22,0)   not null,
 COLUMN_TYPE             varchar2(35)   not null,
 PARENT_TABLE            varchar2(35),
 PARENT_TABLE_PK_COLUMN2 varchar2(35),
 IS_ON_TABLE             number(22,0),
 ALLOW_AUTOCREATE        number(22,0)   not null,
 AUTOCREATE_TABLE_NAME   varchar2(35)
);

alter table PP_IMPORT_COLUMN
   add constraint PP_IMPORT_COLUMN_PK
       primary key (IMPORT_TYPE_ID, COLUMN_NAME)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_COLUMN
   add constraint PP_IMPT_COL_IMPT_TYPE_FK
       foreign key (IMPORT_TYPE_ID)
       references PP_IMPORT_TYPE;

-- pp_import_lookup
create table PP_IMPORT_LOOKUP
(
 IMPORT_LOOKUP_ID            number(22,0)   not null,
 TIME_STAMP                  date,
 USER_ID                     varchar2(18),
 DESCRIPTION                 varchar2(100)  not null,
 LONG_DESCRIPTION            varchar2(500)  not null,
 COLUMN_NAME                 varchar2(35)   not null,
 LOOKUP_SQL                  varchar2(4000) not null,
 IS_DERIVED                  number(22,0)   not null,
 LOOKUP_TABLE_NAME           varchar2(35),
 LOOKUP_COLUMN_NAME          varchar2(35),
 LOOKUP_CONSTRAINING_COLUMNS varchar2(254),
 LOOKUP_VALUES_ALTERNATE_SQL varchar2(4000)
);

alter table PP_IMPORT_LOOKUP
   add constraint PP_IMPORT_LOOKUP_PK
       primary key (IMPORT_LOOKUP_ID)
       using index tablespace PWRPLANT_IDX;

-- pp_import_column_lookup
create table PP_IMPORT_COLUMN_LOOKUP
(
 IMPORT_TYPE_ID   number(22,0)   not null,
 COLUMN_NAME      varchar2(35)   not null,
 IMPORT_LOOKUP_ID number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table PP_IMPORT_COLUMN_LOOKUP
   add constraint PP_IMPORT_COLUMN_LOOKUP_PK
       primary key (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_COLUMN_LOOKUP
   add constraint PP_IMPT_COL_LKUP_IMPT_COL_FK
       foreign key (IMPORT_TYPE_ID, COLUMN_NAME)
       references PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME);

alter table PP_IMPORT_COLUMN_LOOKUP
   add constraint PP_IMPT_COL_LKUP_IMPT_LKUP_FK
       foreign key (IMPORT_LOOKUP_ID)
       references PP_IMPORT_LOOKUP;

-- pp_import_template
create table PP_IMPORT_TEMPLATE
(
 IMPORT_TEMPLATE_ID       number(22,0)   not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 IMPORT_TYPE_ID           number(22,0)   not null,
 DESCRIPTION              varchar2(35)   not null,
 LONG_DESCRIPTION         varchar2(254),
 CREATED_BY               varchar2(18),
 CREATED_DATE             date,
 DO_UPDATE_WITH_ADD       number(22,0),
 UPDATES_IMPORT_LOOKUP_ID number(22,0),
 FILTER_CLAUSE            varchar2(4000)
);

create unique index IX_PP_IMPORT_TEMPLATE_DESC on PP_IMPORT_TEMPLATE (DESCRIPTION) tablespace PWRPLANT_IDX;

alter table PP_IMPORT_TEMPLATE
   add constraint PP_IMPORT_TEMPLATE_PK
       primary key (IMPORT_TEMPLATE_ID) using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_TEMPLATE
   add constraint PP_IMPT_TEMPLT_IMPT_TYPE_FK
       foreign key (IMPORT_TYPE_ID)
       references PP_IMPORT_TYPE;

alter table PP_IMPORT_TEMPLATE
   add constraint PP_IMPT_TEMPLT_IMPT_LOOKUP_FK
       foreign key (UPDATES_IMPORT_LOOKUP_ID)
       references PP_IMPORT_LOOKUP;

-- pp_import_template_fields
create table PP_IMPORT_TEMPLATE_FIELDS
(
 IMPORT_TEMPLATE_ID number(22,0)   not null,
 FIELD_ID           number(22,0)   not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 IMPORT_TYPE_ID     number(22,0)   not null,
 COLUMN_NAME        varchar2(35)   not null,
 IMPORT_LOOKUP_ID   number(22,0),
 AUTOCREATE_YN      number(22,0)   not null
);

alter table PP_IMPORT_TEMPLATE_FIELDS
   add constraint PP_IMPORT_TEMPLATE_FIELDS_PK
       primary key (IMPORT_TEMPLATE_ID, FIELD_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_TEMPLATE_FIELDS
   add constraint PP_IMPT_TMPLT_FLDS_TMPLT_FK
       foreign key (IMPORT_TEMPLATE_ID)
       references PP_IMPORT_TEMPLATE;

alter table PP_IMPORT_TEMPLATE_FIELDS
   add constraint PP_IMPT_TMPLT_IMPT_COL_FK
       foreign key (IMPORT_TYPE_ID, COLUMN_NAME)
       references PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME);

alter table PP_IMPORT_TEMPLATE_FIELDS
   add constraint PP_IMPT_TMPLT_IMPT_LKUP_FK
       foreign key (IMPORT_LOOKUP_ID)
       references PP_IMPORT_LOOKUP;

-- pp_import_template_edits
create table PP_IMPORT_TEMPLATE_EDITS
(
 IMPORT_TEMPLATE_ID number(22,0)   not null,
 SEQUENCE_NUMBER    number(22,0)   not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 COLUMN_NAME        varchar2(35)   not null,
 ORIGINAL_VALUE     varchar2(4000) not null,
 NEW_VALUE          varchar2(4000) not null,
 MADE_BY            varchar2(18),
 MADE_AT            date
);

alter table PP_IMPORT_TEMPLATE_EDITS
   add constraint PP_IMPORT_TEMPLATE_EDITS_PK
       primary key (IMPORT_TEMPLATE_ID, SEQUENCE_NUMBER)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_TEMPLATE_EDITS
   add constraint PP_IMPT_TEMP_EDIT_TEMP_FK
       foreign key (IMPORT_TEMPLATE_ID)
       references PP_IMPORT_TEMPLATE;

-- pp_import_type_updates_lookup
create table PP_IMPORT_TYPE_UPDATES_LOOKUP
(
 IMPORT_TYPE_ID   number(22,0)   not null,
 IMPORT_LOOKUP_ID number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table PP_IMPORT_TYPE_UPDATES_LOOKUP
   add constraint PP_IMPORT_TYPE_UPDATES_LOOK_PK
       primary key (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_TYPE_UPDATES_LOOKUP
   add constraint PP_IMPT_TY_UPD_LOOK_IMPT_TY_FK
       foreign key (IMPORT_TYPE_ID)
       references PP_IMPORT_TYPE;

alter table PP_IMPORT_TYPE_UPDATES_LOOKUP
   add constraint PP_IMPT_TY_UPD_LOOK_IMPT_LK_FK
       foreign key (IMPORT_LOOKUP_ID)
       references PP_IMPORT_LOOKUP;

-- pp_import_run
create table PP_IMPORT_RUN
(
 IMPORT_RUN_ID      number(22,0)   not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(35)   not null,
 IMPORT_TEMPLATE_ID number(22,0)   not null,
 RUN_BY             varchar2(18),
 RUN_AT             date,
 IS_COMPLETE        number(22,0)   not null,
 NOTES              varchar2(4000)
);

alter table PP_IMPORT_RUN
   add constraint PP_IMPORT_RUN_PK
       primary key (IMPORT_RUN_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_RUN
   add constraint PP_IMPT_RUN_IMPT_TEMPLT_FK
       foreign key (IMPORT_TEMPLATE_ID)
       references PP_IMPORT_TEMPLATE;

--*
--|| Dynamic SQL to create pp_import_template_seq sequence.  Then, grants and synonyms for the sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select NVL(max(IMPORT_TEMPLATE_ID), 0) from PT_IMPORT_TEMPLATE')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table PT_IMPORT_TEMPLATE doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 1000;

   SQLS := 'create SEQUENCE PWRPLANT.PP_IMPORT_TEMPLATE_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create pp_import_template_edits_seq sequence.  Then, grants and synonyms for the sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select NVL(max(SEQUENCE_NUMBER), 0) from PT_IMPORT_TEMPLATE_EDITS')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table PT_IMPORT_TEMPLATE_EDITS doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 1000;

   SQLS := 'create SEQUENCE PWRPLANT.PP_IMPORT_TEMPLATE_EDITS_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--*
--|| Dynamic SQL to create pp_import_run_seq sequence.  Then, grants and synonyms for the sequence.
--*

declare
   OBJECT_EXISTS exception;
   pragma exception_init(OBJECT_EXISTS, -00955);
   OBJECT_NOT_EXISTS exception;
   pragma exception_init(OBJECT_NOT_EXISTS, -00942);

   MAX_ID   number;
   START_ID number;
   SQLS     varchar2(4000);

begin
   begin
      execute immediate ('select NVL(max(IMPORT_RUN_ID), 0) from PT_IMPORT_RUN')
         into MAX_ID;
   exception
      when OBJECT_NOT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('Table PT_IMPORT_RUN doesn''t exist, using 0 count.');
         MAX_ID := 0;
   end;

   START_ID := MAX_ID + 1000;

   SQLS := 'create SEQUENCE PWRPLANT.PP_IMPORT_RUN_SEQ start with ' || TO_CHAR(START_ID);

   begin
      execute immediate SQLS;
      DBMS_OUTPUT.PUT_LINE(SQLS || ';');
   exception
      when OBJECT_EXISTS then
         DBMS_OUTPUT.PUT_LINE('SEQUENCE ALREADY EXISTED.');
   end;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (259, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_system_imports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
