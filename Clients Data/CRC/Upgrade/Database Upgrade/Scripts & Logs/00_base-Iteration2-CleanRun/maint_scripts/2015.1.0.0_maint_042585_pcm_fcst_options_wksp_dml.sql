/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042585_pcm_fcst_options_wksp_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 01/28/2015 Sarah Byers      Created the Forecast Options Workspace
||============================================================================
*/
-- Update the workspace identifier for the forecast template options workspace
update ppbase_workspace
	set workspace_uo_name = 'uo_pcm_fcst_template_wksp',
		 minihelp = 'Forecast Template Options'
 where workspace_identifier = 'config_estimate';

-- Update the minihelp on the menu table
update ppbase_menu_items
	set minihelp = 'Forecast Template Options'
 where workspace_identifier = 'config_estimate';

-- Set up new System Options based on System Controls
-- Grid Estimates - Hours and Quantity
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Grid Estimates - Hours and Quantity', 
		 long_description, 
		 0, 'none', control_value, 1, 0
  from pp_system_control
 where lower(trim(control_name)) = 'grid estimates-hours and quantity';

-- Values
insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Grid Estimates - Hours and Quantity', 'Hrs');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Grid Estimates - Hours and Quantity', 'Hrs and Qty');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Grid Estimates - Hours and Quantity', 'none');

-- Module
insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Grid Estimates - Hours and Quantity', 'pcm');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2235, 0, 2015, 1, 0, 0, 042585, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042585_pcm_fcst_options_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;