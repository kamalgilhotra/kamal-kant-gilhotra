/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044159_reg_allocator_dyn_change_1_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 08/07/2015 Sarah Byers 		 	Create REG_CASE_ALLOCATOR_DYN and REG_JUR_ALLOCATOR_DYN
||========================================================================================
*/
-- REG_JUR_ALLOCATOR_DYN
create table reg_jur_allocator_dyn (
reg_jur_template_id number(22,0) not null,
reg_allocator_id number(22,0) not null,
row_number number(22,0) not null,
reg_acct_type_id number(22,0) null,
sub_acct_type_id number(22,0) null,
reg_acct_id number(22,0) null,
reg_alloc_target_id number(22,0) null,
sign varchar2(1) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_jur_allocator_dyn add (
constraint pk_reg_jur_allocator_dyn primary key (reg_jur_template_id, reg_allocator_id, row_number) using index tablespace pwrplant_idx);

alter table reg_jur_allocator_dyn
add constraint fk_reg_jur_allocator_dyn1
foreign key (reg_jur_template_id)
references reg_jur_template (reg_jur_template_id);

alter table reg_jur_allocator_dyn
add constraint fk_reg_jur_allocator_dyn2
foreign key (reg_allocator_id)
references reg_allocator (reg_allocator_id);

alter table reg_jur_allocator_dyn
add constraint fk_reg_jur_allocator_dyn3
foreign key (reg_acct_type_id)
references reg_acct_type (reg_acct_type_id);

alter table reg_jur_allocator_dyn
add constraint fk_reg_jur_allocator_dyn4
foreign key (reg_acct_type_id, sub_acct_type_id)
references reg_sub_acct_type (reg_acct_type_id, sub_acct_type_id);

alter table reg_jur_allocator_dyn
add constraint fk_reg_jur_allocator_dyn5
foreign key (reg_acct_id)
references reg_acct_master (reg_acct_id);

alter table reg_jur_allocator_dyn
add constraint fk_reg_jur_allocator_dyn6
foreign key (reg_alloc_target_id)
references reg_alloc_target (reg_alloc_target_id);

comment on table reg_jur_allocator_dyn is 'The Reg Jur Allocator Dynamic table saves dynamically calculated allocators involving an arithemetic combination of one or more previous allocations for a Jurisdictional Template.';
comment on column reg_jur_allocator_dyn.reg_jur_template_id is 'System assigned identifier of a Regulatory Jurisdictional Template';
comment on column reg_jur_allocator_dyn.reg_allocator_id is 'System assigned identifier of an allocator that is dynamically calculated';
comment on column reg_jur_allocator_dyn.row_number is 'System assigned identifier for each unique selction made by the user to define the dynamic allocator.';
comment on column reg_jur_allocator_dyn.reg_acct_type_id is 'System assigned identifier for a reg account type, allowing the user to designate the entire reg account type, e.g. Rate Base, in the calculation of the allocator.';
comment on column reg_jur_allocator_dyn.sub_acct_type_id is 'System assigned identifier for a sub account type, allowing the user to designate the entire sub account type in the calculation of the allocator.';
comment on column reg_jur_allocator_dyn.reg_acct_id is 'System assigned identifier for a reg account';
comment on column reg_jur_allocator_dyn.reg_alloc_target_id is 'System assigned identifier for a reg allocation target';
comment on column reg_jur_allocator_dyn.sign is 'The sign ("+" or "-") to be applied to the current row to build the definition of the dynamic allocator.';
comment on column reg_jur_allocator_dyn.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_jur_allocator_dyn.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- REG_CASE_ALLOCATOR_DYN
create table reg_case_allocator_dyn (
reg_case_id number(22,0) not null,
reg_allocator_id number(22,0) not null,
row_number number(22,0) not null,
reg_acct_type_id number(22,0) null,
sub_acct_type_id number(22,0) null,
reg_acct_id number(22,0) null,
reg_alloc_target_id number(22,0) null,
sign varchar2(1) null,
user_id varchar2(18) null,
time_stamp date null);

alter table reg_case_allocator_dyn add (
constraint pk_reg_case_allocator_dyn primary key (reg_case_id, reg_allocator_id, row_number) using index tablespace pwrplant_idx);

alter table reg_case_allocator_dyn
add constraint fk_reg_case_allocator_dyn1
foreign key (reg_case_id)
references reg_case (reg_case_id);

alter table reg_case_allocator_dyn
add constraint fk_reg_case_allocator_dyn2
foreign key (reg_allocator_id)
references reg_allocator (reg_allocator_id);

alter table reg_case_allocator_dyn
add constraint fk_reg_case_allocator_dyn3
foreign key (reg_acct_type_id)
references reg_acct_type (reg_acct_type_id);

alter table reg_case_allocator_dyn
add constraint fk_reg_case_allocator_dyn4
foreign key (reg_acct_type_id, sub_acct_type_id)
references reg_sub_acct_type (reg_acct_type_id, sub_acct_type_id);

alter table reg_case_allocator_dyn
add constraint fk_reg_case_allocator_dyn5
foreign key (reg_acct_id)
references reg_acct_master (reg_acct_id);

alter table reg_case_allocator_dyn
add constraint fk_reg_case_allocator_dyn6
foreign key (reg_alloc_target_id)
references reg_alloc_target (reg_alloc_target_id);

comment on table reg_case_allocator_dyn is 'The Reg Case Allocator Dynamic table saves dynamically calculated allocators involving an arithemetic combination of one or more previous allocations for a Regulatory Case.';
comment on column reg_case_allocator_dyn.reg_case_id is 'System assigned identifier of a Regulatory Case';
comment on column reg_case_allocator_dyn.reg_allocator_id is 'System assigned identifier of an allocator that is dynamically calculated';
comment on column reg_case_allocator_dyn.row_number is 'System assigned identifier for each unique selction made by the user to define the dynamic allocator.';
comment on column reg_case_allocator_dyn.reg_acct_type_id is 'System assigned identifier for a reg account type, allowing the user to designate the entire reg account type, e.g. Rate Base, in the calculation of the allocator.';
comment on column reg_case_allocator_dyn.sub_acct_type_id is 'System assigned identifier for a sub account type, allowing the user to designate the entire sub account type in the calculation of the allocator.';
comment on column reg_case_allocator_dyn.reg_acct_id is 'System assigned identifier for a reg account';
comment on column reg_case_allocator_dyn.reg_alloc_target_id is 'System assigned identifier for a reg allocation target';
comment on column reg_case_allocator_dyn.sign is 'The sign ("+" or "-") to be applied to the current row to build the definition of the dynamic allocator.';
comment on column reg_case_allocator_dyn.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_case_allocator_dyn.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2792, 0, 2015, 2, 0, 0, 044159, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044159_reg_allocator_dyn_change_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;