 /*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052734_lessee_01_remeasure_gain_loss_cleanup_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 12/3/2018  Shane "C" Ward	Clean up remeasure_calc_gain_loss flag
||============================================================================
*/

UPDATE ls_ilr_options
   SET remeasure_calc_gain_loss = NULL
 WHERE remeasurement_date IS NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12882, 0, 2018, 1, 0, 0, 52734, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052734_lessee_01_remeasure_gain_loss_cleanup_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;