/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030561_depr_sch_rates.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   11/27/2013 B. Beck        Point release
||============================================================================
*/

alter table CPR_DEPR_CALC_STG add SCH_RATE_USED number(22,8);

alter table CPR_DEPR_CALC_STG_ARC add SCH_RATE_USED number(22,8);

comment on column CPR_DEPR_CALC_STG.SCH_RATE_USED is 'The total rate used to date';
comment on column CPR_DEPR_CALC_STG_ARC.SCH_RATE_USED is 'The total rate used to date';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (781, 0, 10, 4, 1, 2, 30561, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_030561_depr_sch_rates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
