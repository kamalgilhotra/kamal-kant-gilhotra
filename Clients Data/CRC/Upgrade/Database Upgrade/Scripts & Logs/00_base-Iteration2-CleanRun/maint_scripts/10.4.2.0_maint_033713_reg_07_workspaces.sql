SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033713_reg_07_workspaces.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Sarah Byers
||============================================================================
*/

-- Remove any existing entries
delete from ppbase_workspace_links where module = 'REG';
delete from ppbase_menu_items where module = 'REG';
delete from ppbase_workspace where module = 'REG';

-- PPBASE Workspace
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_ledger_adjustment_ws',
to_date('2014-01-06 14:34:14', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Reg Ledger Adjustments', 'uo_reg_ledger_adjustment_ws', 'Regulatory Ledger Adjustments') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_assign_alloc_case',
to_date('2014-01-21 16:00:24', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Allocation - Assign', 'uo_reg_assign_alloc', 'Case Allocation - Assign') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_alloc_step_target_map_case', to_date('2014-01-03 10:56:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Allocation - Steps', 'uo_reg_alloc_step_target_map',
'Case Allocation - Steps') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_alloc_allocator_factor_map_c', to_date('2014-01-03 10:56:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Allocation - Factors', 'uo_reg_alloc_allocator_factor_map',
'Case Allocation - Factors') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_tax_calc_ws_case',
to_date('2014-01-21 16:01:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Tax Details', 'uo_reg_tax_calc_ws', 'Case Tax Details') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_return_analysis',
to_date('2014-01-21 15:58:48', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Return Analysis', 'uo_reg_monitor_return_analysis', 'Case Return Analysis') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_tax_entity',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Entity Setup', 'uo_reg_tax_entity', 'Tax Entity Setup') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_adjustment_jur_default_ws', to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Jurisdictional Adjustments', 'uo_reg_adjustment_jur_default_ws',
'Jurisdictional Adjustments') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_jur_capital_struct_matrix_ws', to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Capital Structure Matrix', 'uo_reg_jur_capital_struct_matrix_ws',
'Jurisdictional Capital Structure Assignment Matrix') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_fin_monitor_summary_setup_ws', to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Analysis Key Setup', 'uo_reg_fin_monitor_summary_setup_ws',
'Analysis Key Setup') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_adjustment_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Adjustments', 'uo_reg_case_adjustment_ws', 'Case Adjustments') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_acct_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Accounts', 'uo_reg_case_acct_ws', 'Case Accounts') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_compare_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Comparison', 'uo_reg_case_compare_ws', 'Case Comparison') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_fin_monitor_control',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Financial Monitoring Control', 'uo_reg_fin_monitor_control', 'Financial Monitoring Control') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_inc_adj_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Incremental Adjustment', 'uo_reg_inc_adj_ws', 'Incremental Adjustment') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_manage_master_jur_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Manage Accounts Master', 'uo_reg_manage_master_jur_ws', 'Manage Accounts (Master to Jur Templates)') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_tax_int',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'PowerTax', 'uo_reg_hist_tax_int', 'PowerTax Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_dashboard_executive',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Exectutive Dashboard', 'uo_reg_dashboard_executive', 'Executive Dashboard') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_query_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Query Workspace', 'uo_reg_query_ws', 'Query Workspace') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_depr_int',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Assets/Depreciation', 'uo_reg_hist_depr_int', 'Assets/Depreciation Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_fcst_cr_int_grid_map',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Dept Budget (O&M)', 'uo_reg_fcst_cr_int_grid_map', 'Departmental Budget (O&M) Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_recovery_class_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Recovery Class Setup', 'uo_reg_recovery_class_ws', 'Recovery Class Setup') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_control_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Status', 'uo_reg_case_control_ws', 'Case Status') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_assign_alloc',
to_date('2014-01-03 10:56:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Jur Allocation - Assignment', 'uo_reg_assign_alloc', 'Allocation - Assignment') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_coc_rates_ws',
to_date('2014-01-21 15:59:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Cost of Capital', 'uo_reg_case_coc_rates_ws', 'Case Cost of Capital') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_manage_jur_case_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Manage Accounts Jur', 'uo_reg_manage_jur_case_ws', 'Manage Accounts (Jurisdiction Template to Case)') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_version',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Historic Ledgers', 'uo_reg_hist_version', 'Historic Ledger Maintenance') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_company_setup',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company/Jur Setup', 'uo_reg_company_setup', 'Company, Jurisdiction, and Jur Template Setup') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_recon_setup',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Recon Maintenance', 'uo_reg_hist_recon_setup', 'Historic Reconciliation Maintenance') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_cr_int_grid_map',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Charge Repository', 'uo_reg_hist_cr_int_grid_map', 'Charge Repository Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_ta_def_int',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Provision - Timing Diff/Def', 'uo_reg_hist_ta_def_int',
'Tax Provision - Timing Differences/Deferreds Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_prov_int',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Provision - Other Tax Items', 'uo_reg_hist_prov_int', 'Tax Provision - Other Tax Items Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_workspace_reports',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Report Workspace', 'uo_reg_workspace_reports', 'Report Dashboard') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_acct_company_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Account Assign', 'uo_reg_acct_company_ws', 'Assign Reg Accounts to Reg Companies') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_summary_ledger_ws',
 to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Summary Ledger', 'uo_reg_case_summary_ledger_ws', 'Case Summary Ledger') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_ext_src_design_ws',
to_date('2014-01-02 14:24:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'External Sources', 'uo_reg_ext_src_design_ws', 'External Sources') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_import_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'External Import', 'uo_reg_import_ws', 'External Source Import Tool') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_jur_acct_default',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Jurisdictional Accounts', 'uo_reg_jur_acct_default', 'Jurisdictional Accounts') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_recon_review',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Recon History Ledger', 'uo_reg_hist_recon_review', 'Reconcile History Ledger') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_case_adjust_alloc_ledger', to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Adjust Alloc Ledger', 'uo_reg_case_adjust_alloc_ledger',
'Case Adjustment Allocation Ledger') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_definition_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Definition', 'uo_reg_case_definition_ws', 'Case Definition') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_sub_acct_type_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Sub Account Type Setup', 'uo_reg_sub_acct_type_ws', 'Sub Account Type Setup') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_cwip_int',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Project CWIP', 'uo_reg_hist_cwip_int', 'Project CWIP/RWIP/WIP Comp Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_acct_rollup_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Maintain Account Trees', 'uo_reg_acct_rollup_ws', 'Maintain Account Trees') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_fcst_version',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Forecast Ledgers', 'uo_reg_fcst_version', 'Forecast Ledger Maintenance') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map',
to_date('2014-01-03 10:56:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Jur Allocation - Steps', 'uo_reg_alloc_step_target_map', 'Allocation - Steps') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_fin_monitor_report',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Financial Allocation Result', 'uo_reg_fin_monitor_report', 'Financial Monitoring Allocation Result') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_fcst_depr_int',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Fcst Assets/Depreciation', 'uo_reg_fcst_depr_int', 'Forecast Assets/Depreciation Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_charting_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Charting and Graphing Dashboard', 'uo_reg_charting_ws', 'Charting and Graphing Dashboard') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_alloc_allocator_factor_map', to_date('2014-01-03 10:56:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Jur Allocation - Factors', 'uo_reg_alloc_allocator_factor_map',
'Allocation - Factors') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_monitor_return_analysis', to_date('2014-01-21 15:57:55', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Jurisdictional Return Analysis', 'uo_reg_monitor_return_analysis',
'Jurisdictional Return Analysis') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_company_security',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Company Security', 'uo_reg_company_security', 'Company Security') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_dynamic_report_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Report Formatter', 'uo_dynamic_report_ws', 'Report Formatter') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_case_adj_hist',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Case Adjustment History', 'uo_reg_case_adj_hist', 'Case Adjustment History') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_analysis_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Regulatory Analysis', 'uo_reg_analysis_ws', 'Regulatory Analysis') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_ta_setup',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Provision - Setup', 'uo_reg_hist_ta_setup', 'Tax Provision - Integration Setup') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_hist_dashboard',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Load Ledgers', 'uo_reg_hist_dashboard', 'Load Ledgers') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_fcst_cap_bdg_int',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Capital Budget', 'uo_reg_fcst_cap_bdg_int', 'Capital Budget Integration') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_acct_master_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Account Master Setup', 'uo_reg_acct_master_ws', 'Account Master Setup') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG',
'uo_reg_capital_struct_control_ws', to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Capital Structure', 'uo_reg_capital_struct_control_ws',
'Capital Structure Type Definition') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_tax_calc_ws',
to_date('2014-01-21 16:01:04', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Jur Tax Details', 'uo_reg_tax_calc_ws', 'Jur Tax Details') ;
INSERT INTO "PPBASE_WORKSPACE" ("MODULE", "WORKSPACE_IDENTIFIER", "TIME_STAMP", "USER_ID", "LABEL", "WORKSPACE_UO_NAME", "MINIHELP") VALUES ('REG', 'uo_reg_ext_trans_ws',
to_date('2014-01-02 14:24:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'External Translate', 'uo_reg_ext_trans_ws', 'External Source Translate') ;

-- PPBASE Menu Items
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'MONITORING', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 2, 'Monitoring', 'Monitoring', null, null,
 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'REPORTS', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 7, 'Reports & Charts', 'Reports', null,
null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'SETUP', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 6, 'General Setup', 'General Setup', null,
null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'MONTHLY', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 5, 'Transaction Processing',
'Transaction Processing', null, null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_JUR_MAINT', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 4, 'Jurisdictional Maintenance',
'Jurisdictional Maintenance', null, null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'RATE_CASES', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 3, 'Case Management', 'Case Management',
null, null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CAPITAL_STRUCT_CONTROL', to_date('2014-01-02 14:39:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 5, 'Capital Structure',
'Capital Structure Type Definition', 'SETUP', 'uo_reg_capital_struct_control_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_INTEGRATION', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Historic Integration',
'Historic Integration', 'SETUP', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_INTEGRATION', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Forecast Integration',
'Forecast Integration', 'SETUP', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'EXTERNAL_INTEGRATION', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 8,
'External Source Integration', 'External Source Integration', 'SETUP', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'COMPANY_SETUP', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 4, 'Company/Jur Setup',
'Company, Jurisdiction, and Jur Template Setup', 'SETUP', 'uo_reg_company_setup', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'ACCT_SETUP', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 3, 'Account Setup', 'Account Setup',
'SETUP', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'TAX_ENTITY', to_date('2014-01-02 14:39:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 7, 'Tax Entity Setup', 'Tax Entity Setup',
 'SETUP', 'uo_reg_tax_entity', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FIN_MON_SETUP', to_date('2014-01-02 14:39:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 6, 'Analysis Key Setup',
'Analysis Key Setup', 'SETUP', 'uo_reg_fin_monitor_summary_setup_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'QUERY_DASHBOARD', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Data Interrogatory Workspace',
'Query Dashboard', 'REPORTS', 'uo_reg_query_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'ANALYSIS', to_date('2014-01-02 14:39:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 6, 'Regulatory Analysis',
'Regulatory Analysis', 'REPORTS', 'uo_reg_analysis_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CHART_DASHBOARD', to_date('2014-01-02 14:39:41', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 5, 'Charting and Graphing Dashboard',
 'Charting and Graphing Dashboard', 'REPORTS', 'uo_reg_charting_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'REPORT_DASHBOARD', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Report Workspace',
'Report Dashboard', 'REPORTS', 'uo_reg_workspace_reports', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_STATUS', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Case Status', 'Case Status',
'RATE_CASES', 'uo_reg_case_control_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'SELECT_CASE', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 14, 'Case Definition', 'Case Definition',
 'RATE_CASES', 'uo_reg_case_definition_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_COMPARE', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 13, 'Case Comparison',
'Case Comparison', 'RATE_CASES', 'uo_reg_case_compare_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_ADJ_ALLOC_LEDGER', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 12, 'Case Adjust Alloc Ledger',
 'Case Adjustment Allocation Ledger', 'RATE_CASES', 'uo_reg_case_adjust_alloc_ledger', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_SUMMARY_LEDGER', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 11, 'Case Summary Ledger',
'Case Summary Ledger', 'RATE_CASES', 'uo_reg_case_summary_ledger_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_MONITOR_RESULT', to_date('2014-01-21 15:58:51', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 10, 'Case Return Analysis',
'Case Return Analysis', 'RATE_CASES', 'uo_reg_case_return_analysis', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_TAX_CALC', to_date('2014-01-21 16:03:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 9, 'Case Tax Details',
'Case Tax Details', 'RATE_CASES', 'uo_reg_tax_calc_ws_case', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'COST_OF_CAPITAL_RATES', to_date('2014-01-21 15:59:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 8, 'Case Cost of Capital',
'Case Cost of Capital', 'RATE_CASES', 'uo_reg_case_coc_rates_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_ALLOC_ASSIGN', to_date('2014-01-21 16:00:28', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 7, 'Case Allocation - Assign',
'Case Allocation - Assignment', 'RATE_CASES', 'uo_reg_assign_alloc_case', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_ALLOC_FACTORS', to_date('2014-01-03 11:00:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 6, 'Case Allocation - Factors',
'Case Allocation - Factors', 'RATE_CASES', 'uo_reg_alloc_allocator_factor_map_c', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_ALLOC_STEPS', to_date('2014-01-03 11:00:09', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 5, 'Case Allocation - Steps',
'Case Allocation - Steps', 'RATE_CASES', 'uo_reg_alloc_step_target_map_case', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_ADJUST_HIST', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 4, 'Case Adjustment History',
'Case Adjustment History', 'RATE_CASES', 'uo_reg_case_adj_hist', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_ADJUST', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 3, 'Case Adjustments',
'Case Adjustments', 'RATE_CASES', 'uo_reg_case_adjustment_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'CASE_ACCOUNT', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Case Accounts', 'Case Accounts',
'RATE_CASES', 'uo_reg_case_acct_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'LOAD_HIST_LEDGER', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Load Ledgers', 'Load Ledgers',
'MONTHLY', 'uo_reg_hist_dashboard', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'VIEW_HIST_LEDGER', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 1, 'View Ledgers', 'View Ledgers',
'MONTHLY', 'uo_reg_query_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'RECON_HIST_LEDGER', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 3, 'Recon History Ledger',
'Reconcile History Ledger', 'MONTHLY', 'uo_reg_hist_recon_review', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'TREND_ANALYSIS', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 4, 'Trend Analysis', 'Trend Analysis',
 'MONTHLY', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FIN_MON_CONTROL', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Financial Monitoring Control',
'Financial Monitoring Control', 'MONITORING', 'uo_reg_fin_monitor_control', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_ANALYSIS', to_date('2014-01-21 15:58:51', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 3, 'Jurisdictional Return Analysis',
'Jurisdictional Return Analysis', 'MONITORING', 'uo_reg_monitor_return_analysis', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FIN_MONITOR_REPORT', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Financial Allocation Result',
'Financial Monitoring Allocation Result', 'MONITORING', 'uo_reg_fin_monitor_report', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_RECON_MAINT', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 1, 'Recon Maintenance',
'Historic Reconciliation Maintenance', 'HIST_INTEGRATION', 'uo_reg_hist_recon_setup', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_VERSION', to_date('2014-01-21 16:07:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 9, 'Historic Ledgers',
'Historic Ledger Maintenance', 'HIST_INTEGRATION', 'uo_reg_hist_version', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_POWERTAX', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 8, 'PowerTax', 'PowerTax Integration',
'HIST_INTEGRATION', 'uo_reg_hist_tax_int', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_TAX_PROV', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 7, 'Tax Provision - Other Tax Items',
'Tax Provision - Other Tax Items Integration', 'HIST_INTEGRATION', 'uo_reg_hist_prov_int', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_DEF_TAX', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 6, 'Tax Provision - Timing Diff/Def',
'Tax Provision - Timing Differences/Deferreds Integration', 'HIST_INTEGRATION', 'uo_reg_hist_ta_def_int', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_TAX_PROV_SETUP', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 5, 'Tax Provision - Setup',
'Tax Provision - Integration Setup', 'HIST_INTEGRATION', 'uo_reg_hist_ta_setup', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_CWIP', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 4, 'Project CWIP',
'Project CWIP/RWIP/WIP Comp Integration', 'HIST_INTEGRATION', 'uo_reg_hist_cwip_int', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_CR', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 3, 'Charge Repository',
'Charge Repository Integration', 'HIST_INTEGRATION', 'uo_reg_hist_cr_int_grid_map', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'HIST_DEPR', to_date('2014-01-02 14:39:40', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 2, 'Assets/Depreciation',
'Assets/Depreciation Integration', 'HIST_INTEGRATION', 'uo_reg_hist_depr_int', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_RECON_MAINT', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 1, 'Recon Maintenance',
'Reconciliation Maintenance', 'FORE_INTEGRATION', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_VERSION', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 7, 'Forecast Ledgers',
'Forecast Ledger Maintenance', 'FORE_INTEGRATION', 'uo_reg_fcst_version', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_TAX_PROV', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 6, 'Tax Provision',
'Tax Provision Integration', 'FORE_INTEGRATION', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_DEF_TAX', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 5, 'Deferred Tax',
'Deferred Tax Integration', 'FORE_INTEGRATION', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_CAPITAL', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 4, 'Capital Budget',
'Capital Budget Integration', 'FORE_INTEGRATION', 'uo_reg_fcst_cap_bdg_int', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_OM', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 3, 'Dept Budget (O&M)',
'Departmental Budget (O&M) Integration', 'FORE_INTEGRATION', 'uo_reg_fcst_cr_int_grid_map', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'FORE_DEPR', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 2, 'Fcst Assets/Depreciation',
'Forecast Assets/Depreciation Integration', 'FORE_INTEGRATION', 'uo_reg_fcst_depr_int', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'EXT_SRC', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 1, 'External Sources', 'External Sources',
'EXTERNAL_INTEGRATION', 'uo_reg_ext_src_design_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'EXT_IMPORT', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 3, 'External Import',
'External Source Import Tool', 'EXTERNAL_INTEGRATION', 'uo_reg_import_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'EXT_TRANS', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 2, 'External Translate',
'External Source Translate', 'EXTERNAL_INTEGRATION', 'uo_reg_ext_trans_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'COMPANY_SECURITY', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 1, 'Company Security',
'Company Security', 'COMPANY_SETUP', 'uo_reg_company_security', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'MONITOR_SETUP', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 2, 'Monitoring Setup',
'Monitoring Setup', 'COMPANY_SETUP', null, 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_ACCOUNT', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 1, 'Jurisdictional Accounts',
'Jurisdictional Accounts', 'CASE_JUR_MAINT', 'uo_reg_jur_acct_default', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'ACCT_MGR_JUR', to_date('2014-01-21 16:10:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 8, 'Manage Accounts Jur',
'Manage Accounts (Jurisdiction Template to Case)', 'CASE_JUR_MAINT', 'uo_reg_manage_jur_case_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_CAP_STRUCT', to_date('2014-01-21 16:10:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 7, 'Capital Structure Matrix',
'Jurisdictional Capital Structure Assignment Matrix', 'CASE_JUR_MAINT', 'uo_reg_jur_capital_struct_matrix_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_TAX_CALC', to_date('2014-01-21 16:10:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 6, 'Jur Tax Details', 'Jur Tax Details',
 'CASE_JUR_MAINT', 'uo_reg_tax_calc_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_ALLOC_ASSIGN', to_date('2014-01-21 16:10:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 5, 'Jur Allocation - Assignment',
'Jurisdictional Allocation - Assignment', 'CASE_JUR_MAINT', 'uo_reg_assign_alloc', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_ALLOC_FACTORS', to_date('2014-01-21 16:10:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 4, 'Jur Allocation - Factors',
'Jurisdictional Allocation - Factors', 'CASE_JUR_MAINT', 'uo_reg_alloc_allocator_factor_map', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_ALLOC_STEPS', to_date('2014-01-21 16:10:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 3, 'Jur Allocation - Steps',
'Jurisdictional Allocation - Steps', 'CASE_JUR_MAINT', 'uo_reg_alloc_step_target_map', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'JUR_ADJUST', to_date('2014-01-21 16:10:00', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 2, 'Jurisdictional Adjustments',
'Jurisdictional Adjustments', 'CASE_JUR_MAINT', 'uo_reg_adjustment_jur_default_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'REC_CLASS_SETUP', to_date('2014-01-21 16:10:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 2, 9, 'Recovery Class Setup',
'Recovery Class Setup', 'CASE_JUR_MAINT', 'uo_reg_recovery_class_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'ACCT_SEARCH', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 1, 'Account Master Setup',
'Account Master Setup', 'ACCT_SETUP', 'uo_reg_acct_master_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'SUB_ACCT_TYPE_SETUP', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 2, 'Sub Account Type Setup',
'Sub Account Type Setup', 'ACCT_SETUP', 'uo_reg_sub_acct_type_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'ACCT_COMPANY', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 3, 'Company Account Assign',
'Assign Reg Accounts to Reg Companies', 'ACCT_SETUP', 'uo_reg_acct_company_ws', 1) ;
INSERT INTO "PPBASE_MENU_ITEMS" ("MODULE", "MENU_IDENTIFIER", "TIME_STAMP", "USER_ID", "MENU_LEVEL", "ITEM_ORDER", "LABEL", "MINIHELP", "PARENT_MENU_IDENTIFIER",
"WORKSPACE_IDENTIFIER", "ENABLE_YN") VALUES ('REG', 'ACCT_MGR_MAST', to_date('2014-01-02 14:39:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 3, 4, 'Manage Accounts Master',
'Manage Accounts (Master to Jur Templates)', 'ACCT_SETUP', 'uo_reg_manage_master_jur_ws', 1) ;

-- PPBASE Workspace Links
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_acct_master_ws', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_sub_acct_type_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_acct_master_ws', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_jur_acct_default', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_acct_master_ws', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_acct_rollup_ws', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_acct_master_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_acct_rollup_ws', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_adjustment_jur_default_ws', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_jur_acct_default', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_adjustment_jur_default_ws', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_recovery_class_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_adjustment_jur_default_ws', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_assign_alloc', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map_c', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map_c', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_assign_alloc_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map_c', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_control_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map_c', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_allocator_factor_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_allocator_factor_map_c', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map_case', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_allocator_factor_map_c', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map_case', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_assign_alloc_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map_case', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_tax_calc_ws_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map_case', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_control_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map_case', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map_case', 6,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_allocator_factor_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_assign_alloc', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_tax_calc_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_alloc_step_target_map', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc_case', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc_case', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_allocator_factor_map_c', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc_case', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_control_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc_case', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_assign_alloc', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc_case', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc', 1, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc', 2, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_allocator_factor_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_assign_alloc', 3, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adj_hist', 1, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_adjustment_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adj_hist', 2, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_adjustment_jur_default_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adj_hist', 3, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adjustment_ws', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adjustment_ws', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_adj_hist', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adjustment_ws', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_adjustment_jur_default_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adjustment_ws', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_control_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_adjustment_ws', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_definition_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_adjustment_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_adj_hist', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_allocator_factor_map_c', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 6,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_assign_alloc_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 7,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_tax_calc_ws_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_control_ws', 8,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_control_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_adjustment_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_adj_hist', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_allocator_factor_map_c', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 6,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_assign_alloc_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 7,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_tax_calc_ws_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_case_definition_ws', 8,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_company_setup', 1, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_company_security', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_company_setup', 2, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cr_int_grid_map', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_acct_master_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cr_int_grid_map', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_sub_acct_type_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cr_int_grid_map', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_query_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cr_int_grid_map', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_dashboard', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cr_int_grid_map', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_review', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cr_int_grid_map', 6,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cwip_int', 1, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_acct_master_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cwip_int', 2, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_sub_acct_type_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cwip_int', 3, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_query_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cwip_int', 4, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_dashboard', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cwip_int', 5, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_review', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_cwip_int', 6, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_dashboard', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_query_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_dashboard', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_review', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_dashboard', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_setup', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_dashboard', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cr_int_grid_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_dashboard', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_depr_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_dashboard', 6,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cwip_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_dashboard', 7,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_depr_int', 1, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_acct_master_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_depr_int', 2, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_sub_acct_type_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_depr_int', 3, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_query_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_depr_int', 4, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_dashboard', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_depr_int', 5, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_review', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_depr_int', 6, to_date('2014-01-13 11:45:36',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_review', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_setup', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_review', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_query_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_review', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cr_int_grid_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_review', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_depr_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_review', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cwip_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_review', 6,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_dashboard', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_review', 7,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_setup', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_review', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_setup', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_query_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_setup', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cr_int_grid_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_setup', 4,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_depr_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_setup', 5,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cwip_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_setup', 6,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_dashboard', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_hist_recon_setup', 7,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_jur_acct_default', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_acct_master_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_jur_acct_default', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_query_ws', 1, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_review', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_query_ws', 2, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_recon_setup', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_query_ws', 3, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cr_int_grid_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_query_ws', 4, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_depr_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_query_ws', 5, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_cwip_int', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_query_ws', 6, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_hist_dashboard', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_query_ws', 7, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_sub_acct_type_ws', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_acct_master_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_sub_acct_type_ws', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_tax_calc_ws_case', 1,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_case_control_ws', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_tax_calc_ws_case', 2,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map_case', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_tax_calc_ws_case', 3,
to_date('2014-01-13 11:45:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_tax_calc_ws', 1, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_alloc_step_target_map', null, null, null, null, null, null) ;
INSERT INTO "PPBASE_WORKSPACE_LINKS" ("MODULE", "WORKSPACE_IDENTIFIER", "LINK_ORDER", "TIME_STAMP", "USER_ID", "LINKED_WORKSPACE_IDENTIFIER", "LINKED_WINDOW", "LINKED_DATAWINDOW",
"LINKED_WINDOW_LABEL", "LINKED_WINDOW_MICROHELP", "LINKED_DATAWINDOW_LABEL", "LINKED_DATAWINDOW_MICROHELP") VALUES ('REG', 'uo_reg_tax_calc_ws', 2, to_date('2014-01-13 11:45:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'uo_reg_workspace_reports', null, null, null, null, null, null) ;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (888, 0, 10, 4, 2, 0, 33713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033713_reg_07_workspaces.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON