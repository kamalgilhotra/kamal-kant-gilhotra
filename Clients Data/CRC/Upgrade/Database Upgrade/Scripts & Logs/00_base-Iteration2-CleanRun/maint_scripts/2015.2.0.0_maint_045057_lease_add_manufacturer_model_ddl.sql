/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045057_lease_add_manufacturer_model_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/07/2015 Will Davis	 	  Add manufacturer and model to component table
||============================================================================
*/

alter table ls_component add manufacturer varchar2(35);
alter table ls_component add model varchar2(35);

comment on column ls_component.manufacturer is 'The manufacturer of the component';
comment on column ls_component.model is 'The model of the component';

alter table ls_import_component
add manufacturer varchar2(36);

alter table ls_import_component
add model varchar2(36);


comment on column ls_import_component.manufacturer is 'The manufacturer of the component';
comment on column ls_import_component.model is 'The model of the component';

alter table ls_import_component_archive
add manufacturer varchar2(36);

alter table ls_import_component_archive
add model varchar2(36);


comment on column ls_import_component_archive.manufacturer is 'The manufacturer of the component';
comment on column ls_import_component_archive.model is 'The model of the component';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2899, 0, 2015, 2, 0, 0, 45057, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045057_lease_add_manufacturer_model_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;