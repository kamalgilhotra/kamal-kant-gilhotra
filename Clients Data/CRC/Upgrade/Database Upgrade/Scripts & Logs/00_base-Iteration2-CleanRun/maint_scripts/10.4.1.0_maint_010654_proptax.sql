/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010654_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/14/2013 Julia Breuer   Point Release
||============================================================================
*/

-- pt_import_payment
create table PT_IMPORT_PAYMENT
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 PAYMENT_XLATE          varchar2(254),
 PAYMENT_ID             number(22,0),
 PAID_DATE              varchar2(35),
 CERTIFIED_MAIL_DATE    varchar2(35),
 CERTIFIED_MAIL_NUMBER  varchar2(35),
 CHECK_NUMBER           varchar2(35),
 VOUCHER_NUMBER         varchar2(35),
 AP_REFERENCE_NUMBER    varchar2(35),
 NOTES                  varchar2(2000),
 ERROR_MESSAGE          varchar2(4000)
);

alter table PT_IMPORT_PAYMENT
   add constraint PT_IMPORT_PYMT_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PT_IMPORT_PAYMENT
   add constraint PT_IMPORT_PYMT_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

-- pt_import_payment_archive
create table PT_IMPORT_PAYMENT_ARCHIVE
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 PAYMENT_XLATE         varchar2(254),
 PAYMENT_ID            number(22,0),
 PAID_DATE             varchar2(35),
 CERTIFIED_MAIL_DATE   varchar2(35),
 CERTIFIED_MAIL_NUMBER varchar2(35),
 CHECK_NUMBER          varchar2(35),
 VOUCHER_NUMBER        varchar2(35),
 AP_REFERENCE_NUMBER   varchar2(35),
 NOTES                 varchar2(2000)
);

alter table PT_IMPORT_PAYMENT_ARCHIVE
   add constraint PT_IMPORT_PYMT_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PT_IMPORT_PAYMENT_ARCHIVE
   add constraint PT_IMPORT_PYMT_ARC_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--import type
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 41, sysdate, user, 'Update : Payment Details', 'Import Updates to Payment Data', 'pt_import_payment', 'pt_import_payment_archive', null, 0, 'uo_ptc_logic_import', '', 'Payment' );

--import type subsystem
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 41, 1, sysdate, user );

--import columns
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'payment_id', sysdate, user, 'Payment', 'payment_xlate', 1, 1, 'number(22,0)', 'pt_statement_group_payment', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'paid_date', sysdate, user, 'Paid Date', '', 0, 1, 'date', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'certified_mail_date', sysdate, user, 'Certified Mail Date', '', 0, 1, 'date', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'certified_mail_number', sysdate, user, 'Certified Mail Number', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'check_number', sysdate, user, 'Check Number', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'voucher_number', sysdate, user, 'Voucher Number', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'ap_reference_number', sysdate, user, 'AP Reference Number', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 41, 'notes', sysdate, user, 'Notes', '', 0, 1, 'varchar2(2000)', '', '', 1, null );

--lookup
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 203, sysdate, user, 'PT Payment.Voucher Number', 'The passed in value corresponds to the PT Payment: Voucher Number field.  Translate to the Payment ID using the Voucher Number column on the PT Payment table.', 'payment_id', '( select sgp.payment_id from pt_statement_group_payment sgp where upper( trim( <importfield> ) ) = upper( trim( sgp.voucher_number ) ) )', 0, 'pt_statement_group_payment', 'voucher_number', '', '', null );

--column lookup
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 41, 'payment_id', 203, sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (325, 0, 10, 4, 1, 0, 10654, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010654_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
