/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_sys_PP_GL_TRANSACTION.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.1   01/12/2012 David Liss     Point Release
|| 10.4.1.0   09/05/2013
||============================================================================
*/

create or replace function PP_GL_TRANSACTION(A_ACTIVITY           number,
                                             A_LDG_ASSET_ID       number,
                                             A_LDG_ASSET_ACT_ID   number,
                                             A_LDG_DEPR_GROUP_ID  number,
                                             A_LDG_WORK_ORDER_ID  number,
                                             A_LDG_GL_ACCOUNT_ID  number,
                                             A_GAIN_LOSS_IND      number,
                                             A_LDG_PEND_TRANS_ID  number,
                                             A_JE_METHOD_ID       number,
                                             A_JE_SET_OF_BOOKS_ID number,
                                             A_JE_REVERSAL        number,
                                             A_JE_AMOUNT_TYPE     number) return varchar2 is

/***************************************************************************************
 FUNCTION:  PP_GL_TRANSACTION
 CLIENT:    Generic Starting Function

 LAST CHANGE: 01-12-2012

 CUSTOM DESCRIPTION:

 Function Design:

 By default this function will look at PP_JOURNAL_LAYOUTS by looping over the
 CR_ELEMENTS table and create an accounting string that is fixed width and padded to match
 the cr accounting key setup and is delimited by a dash

 For each journal entry type the function will look up the activity in the PP_JOURNAL_LAYOUTS
 table for the specific company.  If no row exists for the company it will look at comapny -1
 (All Companies).  A company value of -2 will result in an "IGNORE" being used for the gl_transaction table.
 If none of those exist an error will be returned.
***************************************************************************************/

   GL_ACCOUNT_STR varchar2(2000);
   ACCT_TYPE      number(22);

   WO_ID        number(22);
   BS_ID        number(22);
   CO_ID        number(22);
   DG_ID        number(22);
   GL_ID        number(22);
   UA_ID        number(22);
   CE_ID        number(22);
   L_CHECK      number(22);
   L_CHECK2     number(22);
   I            number(22);
   J            number(22);
   K            number(22);
   PP_ERR_CODE  varchar2(1000);
   DELIMITER    varchar2(1);
   RTN          varchar2(254);
   NOTES        varchar2(254);
   LAYOUT_CO    number(22);
   SQL_STMT     varchar2(4000);
   KEYWORD_VAL  varchar2(35);
   ARG_STR      varchar2(35);
   ELEMENT_STR  varchar2(35);
   CNT_VAL      varchar2(35);
   IS_KEYWORD   varchar2(35);
   BINDING_ARG1 number;
   BINDING_ARG2 number;
   BINDING_ARG3 number;
   BINDING_ARG4 number;
   PARAM_COUNT  number;
   ARG_COUNT    number;

   type V_ARRAY is table of varchar2(51) index by binary_integer;
   type N_ARRAY is table of number(22) index by binary_integer;
   ACCOUNTING_KEY V_ARRAY;
   ELEMENTS       V_ARRAY;
   DEFAULTS       V_ARRAY;
   WIDTHS         N_ARRAY;
   PARAMS         N_ARRAY;
   BINDING_ARG    N_ARRAY;

begin

   PP_ERR_CODE := '001:' || A_LDG_PEND_TRANS_ID;

   /* Get Company from Pend Trans .
   If activity is lease specifc activity without a pend trans,
   then do not get from pend_trans
   get from lease
   */
   if a_activity <= 3000 then
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from PEND_TRANSACTION A
      where A.PEND_TRANS_ID = A_LDG_PEND_TRANS_ID;
   elsif a_activity in (3025, 3026, 3030, 3031, 3034, 3036) then
      -- leased asset retirements, transfers, gain loss have pend transactions.
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from PEND_TRANSACTION A
      where A.PEND_TRANS_ID = A_LDG_PEND_TRANS_ID;
   elsif a_activity in (3001, 3002, 3003) then
      -- leased asset additions have a different pend transaction table
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from LS_PEND_TRANSACTION A
      where A.LS_PEND_TRANS_ID = A_LDG_PEND_TRANS_ID;
   else
      -- get company / business segment from the leased asset
      select A.BUS_SEGMENT_ID, A.COMPANY_ID
      into BS_ID, CO_ID
      from LS_ASSET A
      where A.LS_ASSET_ID = A_LDG_ASSET_ID;
   end if;

   PP_ERR_CODE := '002:' || A_ACTIVITY;

   /* Determine which row in the PP_JOURNAL_LAYOUTS to Use */
   select count(*)
     into L_CHECK
     from PP_JOURNAL_LAYOUTS
    where TRANS_TYPE = A_ACTIVITY
      and COMPANY_ID = CO_ID
      and JE_METHOD_ID = A_JE_METHOD_ID;

   PP_ERR_CODE := '002a';

   if L_CHECK = 1 then
      LAYOUT_CO := CO_ID;
   else
      L_CHECK     := 0;
      PP_ERR_CODE := '002b:' || A_ACTIVITY;

      select count(*)
        into L_CHECK
        from PP_JOURNAL_LAYOUTS
       where TRANS_TYPE = TO_NUMBER(A_ACTIVITY)
         and COMPANY_ID = -1
         and JE_METHOD_ID = A_JE_METHOD_ID;

      PP_ERR_CODE := '002bb';

      if L_CHECK = 1 then
         LAYOUT_CO := -1;
      else
         PP_ERR_CODE := '002bbb';
         L_CHECK     := 0;

         select count(*)
           into L_CHECK
           from PP_JOURNAL_LAYOUTS
          where TRANS_TYPE = A_ACTIVITY
            and COMPANY_ID = -2
            and JE_METHOD_ID = A_JE_METHOD_ID;

         if L_CHECK = 1 then
            LAYOUT_CO := -2;
            /* Ignore Row Found */
            return 'IGNORE: Entry has been setup to be ignored in the PP_JOURNAL_LAYOUTS table';
         else
            /* Error Now Row Found */
            return 'ERROR: No record found in the PP_JOURNAL_LAYOUTS for trans_type=' || A_ACTIVITY || ' and company=' || CO_ID || ' or default -1 company';
         end if;
      end if;
   end if;

   /* Place parameters into an array for use in binding arguments */
   PP_ERR_CODE := 'Passing params in array';
   PARAMS(1) := A_ACTIVITY;
   PARAMS(2) := A_LDG_ASSET_ID;
   PARAMS(3) := A_LDG_ASSET_ACT_ID;
   PARAMS(4) := A_LDG_DEPR_GROUP_ID;
   PARAMS(5) := A_LDG_WORK_ORDER_ID;
   PARAMS(6) := A_LDG_GL_ACCOUNT_ID;
   PARAMS(7) := A_GAIN_LOSS_IND;
   PARAMS(8) := A_LDG_PEND_TRANS_ID;
   PARAMS(9) := A_JE_METHOD_ID;
   PARAMS(10) := A_JE_SET_OF_BOOKS_ID;
   PARAMS(11) := A_JE_REVERSAL;
   PARAMS(12) := A_JE_AMOUNT_TYPE;

   /* Determine system control for gl journal category */
   PP_ERR_CODE := 'Finding system control';
   select UPPER(RTRIM(CONTROL_VALUE))
     into CNT_VAL
     from CR_SYSTEM_CONTROL
    where UPPER(RTRIM(CONTROL_NAME)) = 'PPTOCR: GLJC IN GL ACCOUNT';

   /* Get CR Element Details */
   PP_ERR_CODE := 'elements';
   select replace(replace(replace(LOWER(DESCRIPTION), ' ', '_'), '-', '_'), '/', '_') bulk collect
     into ELEMENTS
     from CR_ELEMENTS
    order by "ORDER";

   PP_ERR_CODE := 'widths';
   select WIDTH bulk collect into WIDTHS from CR_ELEMENTS order by "ORDER";

   PP_ERR_CODE := 'defaults';
   select NVL(DEFAULT_VALUE, ' ') bulk collect into DEFAULTS from CR_ELEMENTS order by "ORDER";

   /* Add gl_journal_category to account string */
   PP_ERR_CODE := 'extending arrays';
   if CNT_VAL = 'YES' then
      ELEMENTS(ELEMENTS.LAST + 1) := 'gl_journal_category';
      WIDTHS(WIDTHS.LAST + 1) := '35';
      DEFAULTS(DEFAULTS.LAST + 1) := '0';
   end if;

   /* Get Values from PP_JOURNAL_LAYOUTS */
   for I in ELEMENTS.FIRST .. ELEMENTS.LAST
   loop

      PP_ERR_CODE := 'Convert to upper' || ELEMENTS(I);
      ELEMENTS(I) := '"' || UPPER(ELEMENTS(I)) || '"'; /* Account for reserved words */

      PP_ERR_CODE := 'pp_journal_layouts' || ELEMENTS(I);
      execute immediate 'select ' || ELEMENTS(I) || ' from PP_JOURNAL_LAYOUTS ' ||
                        ' where trans_type = ' || A_ACTIVITY || ' and company_id = ' || LAYOUT_CO ||
                        ' and je_method_id = ' || A_JE_METHOD_ID
         into ACCOUNTING_KEY(I);

      PP_ERR_CODE := 'check if null, element ' || I || ELEMENTS(I) || trim(ACCOUNTING_KEY(I));
      if trim(ACCOUNTING_KEY(I)) = '' or ACCOUNTING_KEY(I) is null then
         PP_ERR_CODE := 'Inside if statement null check';
         ACCOUNTING_KEY(I) := DEFAULTS(I);
      end if;

      /* Now do values from key words */
      PP_ERR_CODE := 'accounting_key' || ACCOUNTING_KEY(I);
      KEYWORD_VAL := ACCOUNTING_KEY(I);

      PP_ERR_CODE := 'Error in selecting whether or not a keyword exists';
      select NVL((select KEYWORD
                   from PP_JOURNAL_KEYWORDS
                  where KEYWORD = KEYWORD_VAL
                    and KEYWORD_TYPE = 1),
                 null)
        into IS_KEYWORD
        from DUAL;

      /* If I am harcoding a value */
      if IS_KEYWORD is null then

         SQL_STMT := 'select ' || ELEMENTS(I) || ' from PP_JOURNAL_LAYOUTS ' ||
                     'where trans_type = ' || A_ACTIVITY || ' and company_id = ' || LAYOUT_CO ||
                     ' and je_method_id = ' || A_JE_METHOD_ID;

         /* Begin logic for existing keywords */
      else
         PP_ERR_CODE := KEYWORD_VAL || I || 'keyword_val';
         select SQLS,
                BIND_ARG_CODE_ID1,
                BIND_ARG_CODE_ID2,
                BIND_ARG_CODE_ID3,
                BIND_ARG_CODE_ID4,
                (NVL2(BIND_ARG_CODE_ID1, 1, 0) + NVL2(BIND_ARG_CODE_ID2, 1, 0) +
                NVL2(BIND_ARG_CODE_ID3, 1, 0) + NVL2(BIND_ARG_CODE_ID4, 1, 0))
           into SQL_STMT, BINDING_ARG1, BINDING_ARG2, BINDING_ARG3, BINDING_ARG4, ARG_COUNT
           from PP_JOURNAL_KEYWORDS
          where KEYWORD = KEYWORD_VAL;

         /* Fill defaults acts like bind arg */
         SQL_STMT := replace(SQL_STMT, '<default>', DEFAULTS(I));

         PP_ERR_CODE := 'Passing binding args in array';
         BINDING_ARG(1) := BINDING_ARG1;
         BINDING_ARG(2) := BINDING_ARG2;
         BINDING_ARG(3) := BINDING_ARG3;
         BINDING_ARG(4) := BINDING_ARG4;

         PP_ERR_CODE := 'Passing binding default in array' || I;

         /* Replace arguments with actual values */
         for J in 1 .. ARG_COUNT
         loop
            ARG_STR  := '<arg' || J || '>';
            SQL_STMT := replace(SQL_STMT, ARG_STR, PARAMS(BINDING_ARG(J)));
         end loop;

         PP_ERR_CODE := 'Passing elements into array postition ' || I;

         /* Replace elements with previous calculated values */
         for K in 1 .. (I - 1)
         loop
            ELEMENT_STR := '<element' || K || '>';
            SQL_STMT    := replace(SQL_STMT, ELEMENT_STR, ACCOUNTING_KEY(K));
         end loop;

      end if; -- End logic for existing keywords

      DBMS_OUTPUT.PUT_LINE(SQL_STMT);
      DBMS_OUTPUT.PUT(CHR(10));

      /* Execute sql block */
      PP_ERR_CODE := 'Element ' || ELEMENTS(I) || 'for trans_type = ' || A_ACTIVITY ||
                     ' and company_id = ' || LAYOUT_CO || ' and je_method_id = ' || A_JE_METHOD_ID;
      execute immediate SQL_STMT
         into ACCOUNTING_KEY(I);

   end loop;

   /* Now Add Specific Logic for Implementation */

   /* Oracle requires something in each part of the if statement, just set delimiter for now */
   if A_ACTIVITY = 1000 then
      /* addition debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1001 then
      /* addition credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1002 then
      /* retirement debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1003 then
      /* retirement credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1004 then
      /* COR debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1005 then
      /* COR credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1006 then
      /* Salvage debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1007 then
      /* Salvage  credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1008 then
      /* Gain/Loss debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1009 then
      /* Gain/Loss  credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1010 then
      /* Transfer_to debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1011 then
      /* Transfer_from  credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1012 then
      /* Adjust  debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1013 then
      /* Adjust  credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1014 then
      /* Intercompany transfer debit 'From' side */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1015 then
      /* Intercompany transfer credit 'To' side */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1016 then
      /* ARO Addition debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1017 then
      /* ARO addition credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1018 then
      /* life reserve transfer debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1019 then
      /* life reserve transfer credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1020 then
      /* cor reserve transfer debit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1021 then
      /* cor reserve transfer credit */
      DELIMITER := '-';
   elsif A_ACTIVITY = 1022 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1023 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1024 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1025 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1026 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1027 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1028 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1029 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 1030 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3001 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3002 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3003 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3010 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3011 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3012 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3013 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3014 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3015 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3016 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3017 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3018 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3019 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3020 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3021 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3022 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3023 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3024 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3025 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3026 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3027 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3028 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3029 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3030 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3031 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3032 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3033 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3034 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3035 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3036 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3037 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3038 then
      DELIMITER := '-';
   elsif A_ACTIVITY = 3039 then
      DELIMITER := '-';
   end if;

   PP_ERR_CODE := 'loop_string';
   /* Loop over elements and rpad the values with spaces and build string */
   for I in ELEMENTS.FIRST .. ELEMENTS.LAST
   loop

      PP_ERR_CODE := 'element ' || ELEMENTS(I) || ' string length ' || LENGTH(GL_ACCOUNT_STR);

      if ACCOUNTING_KEY(I) is null then
         --or place defaults?
         return 'ERROR: ' || ELEMENTS(I) || ' is null ';
      end if;

      ACCOUNTING_KEY(I) := RPAD(ACCOUNTING_KEY(I), WIDTHS(I));

      if I = 1 then
         GL_ACCOUNT_STR := ACCOUNTING_KEY(I);
      else
         GL_ACCOUNT_STR := GL_ACCOUNT_STR || DELIMITER || ACCOUNTING_KEY(I);
      end if;

   end loop;

   if NOTES is not null and NOTES <> '' then
      GL_ACCOUNT_STR := GL_ACCOUNT_STR || DELIMITER || NOTES;
   end if;

   return GL_ACCOUNT_STR;

exception
   when others then
      /* this catches all SQL errors, including no_data_found */
      return('ERROR SQL: ' || PP_ERR_CODE || ':' || sqlerrm);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (583, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_sys_PP_GL_TRANSACTION.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;