/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008402_projects.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/01/2011 Chris Mardis   Point Release
||============================================================================
*/

alter table WO_EST_MONTHLY
   add WO_WORK_ORDER_ID number(22);

delete from PP_SYSTEM_CONTROL_COMPANY
 where LOWER(trim(CONTROL_NAME)) = LOWER('BDG Ests-Job Task Source Table')
   and COMPANY_ID = -1
   and CONTROL_ID <> (select min(CONTROL_ID)
                        from PP_SYSTEM_CONTROL_COMPANY
                       where LOWER(trim(CONTROL_NAME)) = LOWER('BDG Ests-Job Task Source Table')
                         and COMPANY_ID = -1);

delete from PP_SYSTEM_CONTROL_COMPANY
 where LOWER(trim(CONTROL_NAME)) = LOWER('FP Ests-Job Task Source Table')
   and COMPANY_ID = -1
   and CONTROL_ID <> (select min(CONTROL_ID)
                        from PP_SYSTEM_CONTROL_COMPANY
                       where LOWER(trim(CONTROL_NAME)) = LOWER('FP Ests-Job Task Source Table')
                         and COMPANY_ID = -1);

delete from PP_SYSTEM_CONTROL_COMPANY
 where LOWER(trim(CONTROL_NAME)) = LOWER('WO Ests-Job Task Source Table')
   and COMPANY_ID = -1
   and CONTROL_ID <> (select min(CONTROL_ID)
                        from PP_SYSTEM_CONTROL_COMPANY
                       where LOWER(trim(CONTROL_NAME)) = LOWER('WO Ests-Job Task Source Table')
                         and COMPANY_ID = -1);

update PP_SYSTEM_CONTROL_COMPANY
   set DESCRIPTION = 'job_task;job_task_list;freeform'
 where LOWER(trim(CONTROL_NAME)) = LOWER('WO Ests-Job Task Source Table');

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'job_task'
 where LOWER(trim(CONTROL_NAME)) = LOWER('WO Ests-Job Task Source Table')
   and LOWER(trim(CONTROL_VALUE)) = 'job_task_wo';

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'FPEST - Show WOS',
          'yes',
          'dw_yes_no;1',
          '"No" will make the wo_work_order_id dropdown invisible for funding project estimates.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'BDGEST - show FPS',
          'yes',
          'dw_yes_no;1',
          '"No" will make the wo_work_order_id dropdown (funding projects) invisible for budget item estimates.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

alter table WO_EST_RATE_DEFAULT         add WO_WORK_ORDER_ID number(22);

alter table WO_EST_MONTHLY_DEFAULT      add WO_WORK_ORDER_ID number(22);

alter table WO_EST_SLIDE_RESULTS_TEMP   add WO_WORK_ORDER_ID number(22);

alter table WO_EST_TRANSPOSE_TEMP       add WO_WORK_ORDER_ID number(22);

alter table WO_EST_ACTUALS_TEMP         add WO_WORK_ORDER_ID number(22);

alter table WO_EST_ACTUALS_TEMP2        add WO_WORK_ORDER_ID number(22);

alter table WO_EST_UPDATE_WITH_ACT_TEMP add INCLUDE_WO number(22);

alter table WO_EST_MONTHLY              add UWA_INCLUDE_WO number(22);

alter table WO_EST_HIERARCHY            add INCLUDE_WO number(22);

alter table WO_EST_ACTUALS_TEMP         add INCLUDE_WO number(22);

alter table WO_EST_ACTUALS_TEMP2        add INCLUDE_WO number(22);

-- This index may not exist or may have a different name.
drop index WEM_UPDATE_W_ACT_COLS;

create index WEM_UPDATE_W_ACT_COLS
    on WO_EST_MONTHLY (WORK_ORDER_ID,
                       REVISION,
                       EXPENDITURE_TYPE_ID,
                       EST_CHG_TYPE_ID,
                       DECODE("UWA_INCLUDE_WO",1,NVL("WO_WORK_ORDER_ID",0),1),
                       DECODE("UWA_INCLUDE_JT",1,NVL("JOB_TASK_ID",'*'),'1'),
                       DECODE("UWA_INCLUDE_UA",1,NVL("UTILITY_ACCOUNT_ID",0),1),
                       DECODE("UWA_INCLUDE_DEPT",1,NVL("DEPARTMENT_ID",0),1));


-- This index may not exist or may have a different name.
drop index WO_EST_WO_RE_EXP_UA_DEP_JBX;

create index WO_EST_WO_RE_EXP_UA_DEP_JBX
   on WO_EST_MONTHLY (WORK_ORDER_ID,
                      REVISION,
                      YEAR,
                      EXPENDITURE_TYPE_ID,
                      EST_CHG_TYPE_ID,
                      NVL("WO_WORK_ORDER_ID",0),
                      NVL("JOB_TASK_ID",'*'),
                      NVL("UTILITY_ACCOUNT_ID",0),
                      NVL("DEPARTMENT_ID",0));

alter table WO_EST_MONTHLY_UPLOAD      add WO_WORK_ORDER_ID varchar2(100);

alter table WO_EST_MONTHLY_UPLOAD_ARCH add WO_WORK_ORDER_ID varchar2(100);

alter table WO_INTERFACE_MONTHLY       add WO_WORK_ORDER_ID number(22);

alter table WO_INTERFACE_MONTHLY_IDS   add WO_WORK_ORDER_ID number(22);

update WO_EST_UPLOAD_TEMPLATE
   set sql = 'SELECT "WO_EST_MONTHLY_UPLOAD"."WORK_ORDER_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."YEAR",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."EXPENDITURE_TYPE_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."EST_CHG_TYPE_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."DEPARTMENT_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."UTILITY_ACCOUNT_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."WO_WORK_ORDER_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."JOB_TASK_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."DESCRIPTION",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."JANUARY",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."FEBRUARY",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."MARCH",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."APRIL",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."MAY",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."JUNE",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."JULY",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."AUGUST",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."SEPTEMBER",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."OCTOBER",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."NOVEMBER",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."DECEMBER",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."TOTAL",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_JAN",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_FEB",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_MAR",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_APR",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_MAY",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_JUN",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_JUL",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_AUG",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_SEP",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_OCT",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_NOV",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_DEC",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HRS_TOTAL",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_JAN",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_FEB",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_MAR",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_APR",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_MAY",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_JUN",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_JUL",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_AUG",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_SEP",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_OCT",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_NOV",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_DEC",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."QTY_TOTAL",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."FUTURE_DOLLARS",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."HIST_ACTUALS",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."COMPANY_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."WO_OR_FP",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."BUDGET_VERSION_ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."ID",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."PROCESS_STATUS",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."VALIDATION_ERROR",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."USERS",' || CHR(13) || CHR(10) ||
              '  "WO_EST_MONTHLY_UPLOAD"."SESSION_ID"' || CHR(13) || CHR(10) ||
              'from "WO_EST_MONTHLY_UPLOAD"'
 where DESCRIPTION = 'Default Template';

alter table WO_EST_MONTHLY_UPLOAD
   add (MAPPED_WORK_ORDER_ID       number(22),
        MAPPED_EXPENDITURE_TYPE_ID number(22),
        MAPPED_EST_CHG_TYPE_ID     number(22),
        MAPPED_DEPARTMENT_ID       number(22),
        MAPPED_UTILITY_ACCOUNT_ID  number(22),
        MAPPED_WO_WORK_ORDER_ID    number(22),
        MAPPED_JOB_TASK_ID         varchar2(35),
        MAPPED_COMPANY_ID          number(22));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (32, 0, 10, 3, 3, 0, 8402, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008402_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
