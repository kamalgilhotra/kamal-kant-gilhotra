/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034372_system_PP_SET_PROCESS.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/29/2014 Stephen Motter
||============================================================================
*/

create or replace function PP_SET_PROCESS(A_ARG varchar2, A_ARG2 varchar2) return varchar2 as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_SET_PROCESS
   || Description:
   ||============================================================================
   || Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.1                               Added a check for null argument
   || 1.1   08/29/2013 C Shilling       maint_31807 - Remove code - call package instead
   ||============================================================================
   */
begin
   return PP_PROCESS_PKG.PP_SET_PROCESS(A_ARG, A_ARG2);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (928, 0, 10, 4, 2, 0, 34372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034372_system_PP_SET_PROCESS.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;