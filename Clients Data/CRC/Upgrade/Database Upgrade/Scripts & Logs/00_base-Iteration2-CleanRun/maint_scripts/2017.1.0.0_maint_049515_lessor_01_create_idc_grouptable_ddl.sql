/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049515_lessor_01_create_idc_grouptable_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/09/2017 Anand R        Create new table for IDC group and ILR association table
||============================================================================
*/

create table lsr_initial_direct_cost_group (
   idc_group_id     NUMBER(22,0) NOT NULL,
   description      VARCHAR2(35) NOT NULL,
   long_description VARCHAR2(254),
   user_id          VARCHAR2(18),
   time_stamp       DATE );
   
alter table lsr_initial_direct_cost_group 
add constraint lsr_idc_group_pk 
primary key (idc_group_id) 
using index tablespace pwrplant_idx;

alter table lsr_initial_direct_cost_group 
add constraint lsr_idc_group_uk 
unique (description);

-- Comments
comment on table lsr_initial_direct_cost_group is '(S) [] The table stores the Indirect Cost Group Information.';
comment on column lsr_initial_direct_cost_group.idc_group_id is 'System-assigned identifier of an indirect cost group.';
comment on column lsr_initial_direct_cost_group.description is 'Description of the indirect cost group.';
comment on column lsr_initial_direct_cost_group.long_description is 'Long description of the indirect cost group..';
comment on column lsr_initial_direct_cost_group.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column lsr_initial_direct_cost_group.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


create table lsr_ilr_idc_group (
  ilr_id       NUMBER(22) not null,
  revision          NUMBER(22) not null,
  idc_group_id      NUMBER(22) not null,
  user_id           VARCHAR2(18),
  time_stamp        DATE,
  date_incurred     DATE,
  amount            NUMBER(22,2),
  description       VARCHAR2(254)
);

alter table lsr_ilr_idc_group
  add constraint lsr_ilr_idc_group_pk primary key (ilr_id, revision, idc_group_id)
  using index tablespace PWRPLANT ;
  
alter table lsr_ilr_idc_group
  add constraint lsr_ilr_idc_group_fk1 foreign key (ilr_id, revision)
  references lsr_ilr_approval(ilr_id, revision) ;
  
alter table lsr_ilr_idc_group
  add constraint lsr_ilr_idc_group_fk2 foreign key (idc_group_id)
  references lsr_initial_direct_cost_group(idc_group_id) ;
  
-- Comments
comment on table lsr_ilr_idc_group is '(S) [] The table stores the Indirect Cost groups associated to an ILR.';
comment on column lsr_ilr_idc_group.ilr_id is 'System-assigned identifier of a Lessor ILR.';
comment on column lsr_ilr_idc_group.revision is 'System-assigned identifier of a Lessor ILR revision.';
comment on column lsr_ilr_idc_group.idc_group_id is 'System-assigned identifier of an indirect cost group.';
comment on column lsr_ilr_idc_group.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column lsr_ilr_idc_group.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column lsr_ilr_idc_group.date_incurred is 'The date on which the cost incurred.';
comment on column lsr_ilr_idc_group.amount is 'Cost incurred';
comment on column lsr_ilr_idc_group.description is 'Description of the ILR indirect cost group assoiation.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3918, 0, 2017, 1, 0, 0, 49515, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049515_lessor_01_create_idc_grouptable_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
