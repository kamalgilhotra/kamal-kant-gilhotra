/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052861_lessee_02_import_default_disc_rates_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 01/21/2019 Shane "C" Ward   New Import table and mods for Default Discount Rate Type
||============================================================================
*/

--Define new Import Type
INSERT INTO pp_import_type (import_type_id, description, long_description, import_table_name, archive_table_name, allow_updates_on_add, delegate_object_name)
VALUES (271, 'Add: ILR Default Discount Rates', 'Lessee ILR Default Discount Rates', 'ls_import_default_rates', 'ls_import_default_rates_arc', '1', 'nvo_ls_logic_import');

INSERT INTO pp_import_Type_subsystem (import_Type_id, import_subsystem_id)
VALUES (271, 8);

--Define Columns in Import
INSERT INTO pp_import_column (import_type_id, column_name, column_type, description, import_column_name, is_required, processing_order, parent_table, is_on_table)
VALUES (271, 'rate_type_id', 'number(22,0)', 'Default Discount Rate Type', 'rate_type_xlate', 1, 1, 'LS_ILR_DEFAULT_RATE_TYPE', 1);

INSERT INTO pp_import_column (import_type_id, column_name, column_type, description, import_column_name, is_required, processing_order, parent_table, is_on_table)
VALUES (271, 'term_length', 'number(22,0)', 'Term Length', null, 1, 1, null, 0);

INSERT INTO pp_import_column (import_type_id, column_name, column_type, description, import_column_name, is_required, processing_order, parent_table, is_on_table)
VALUES (271, 'effective_date', 'date mmddyyyy format', 'Effective Date', null, 1, 1, null, 0);

INSERT INTO pp_import_column (import_type_id, column_name, column_type, description, import_column_name, is_required, processing_order, parent_table, is_on_table)
VALUES (271, 'borrowing_curr_id', 'number(22,0)', 'Borrowing Currency', 'borrowing_curr_xlate', 0, 1, null, 0);

INSERT INTO pp_import_column (import_type_id, column_name, column_type, description, import_column_name, is_required, processing_order, parent_table, is_on_table)
VALUES (271, 'rate', 'number(22,8)', 'Discount Rate', 'rate', 1, 1, null, 0);

--Define new lookups
insert into pp_import_lookup
  ( import_lookup_id, 
	description, 
	long_description, 
	column_name, 
	lookup_sql,
    is_derived, 
    lookup_table_name, 
    lookup_column_name)
values
  (1120, 
   'Default Disc Rate Type.Description',
   'Lessee ILR Default Discount Rate Type Description Lookup',
   'rate_type_id',
   '( select distinct b.rate_type_id from LS_ILR_DEFAULT_RATE_TYPE b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )',
   0, 
   'LS_ILR_DEFAULT_RATE_TYPE', 
   'DESCRIPTION');
   
insert into pp_import_lookup
  ( import_lookup_id, 
	description, 
	long_description, 
	column_name, 
	lookup_sql,
    is_derived, 
    lookup_table_name, 
    lookup_column_name)
values
  (1121, 
   'Default Disc Rate Type.Long Description',
   'Lessee ILR Default Discount Rate Type Long Description Lookup',
   'rate_type_id',
   '( select distinct b.rate_type_id from LS_ILR_DEFAULT_RATE_TYPE b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )',
   0, 
   'LS_ILR_DEFAULT_RATE_TYPE', 
   'LONG_DESCRIPTION');

--Assign Lookups
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
(IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
VALUES (271, 'rate_type_id', 1120);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
(IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
VALUES (271, 'rate_type_id', 1121);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
(IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
VALUES (271, 'borrowing_curr_id', 2509);

--Create default Template using sequence
DECLARE
    templateid NUMBER;
BEGIN
    templateid := pp_import_template_seq.NEXTVAL;

	Insert into pp_import_Template 
	(import_template_id, 
	 import_Type_id, 
	 description, 
	 long_description, 
	 created_by, 
	 created_date, 
	 do_update_with_add, 
	 is_autocreate_template)
	values (templateid, 
		   271, 
		   'ILR Default Discount Rates Add', 
		   'Default Template for adding ILR Default Discount Rates', 
		   'PWRPLANT', 
		   sysdate, 
		   1, 
		   0);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 1,
                 271,
                 'rate_type_id',
                 1120);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 2,
                 271,
                 'term_length',
                 null);
				 
    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 3,
                 271,
                 'effective_date',
                 null);
				 
    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 4,
                 271,
                 'borrowing_curr_id',
                 2509);
				 
    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 5,
                 271,
                 'rate',
                 null);
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14263, 0, 2018, 2, 0, 0, 52861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052861_lessee_02_import_default_disc_rates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
 