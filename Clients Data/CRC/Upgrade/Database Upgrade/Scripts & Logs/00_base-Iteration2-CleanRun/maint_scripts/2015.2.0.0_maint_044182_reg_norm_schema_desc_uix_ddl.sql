/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044182_reg_norm_schema_desc_uix_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/07/2015 Daniel Motter  Creation
||============================================================================
*/

set serveroutput on

declare
    unique_cons_exists exception;
    pragma exception_init(unique_cons_exists, -2261); 
    type ns_record is record (
        ids         varchar2(255),
        description varchar2(35)
    );
    type ns_records is table of ns_record index by pls_integer;
    l_ns_records ns_records;
begin
    select listagg(normalization_id, ', ') within group (order by normalization_id) ids, description
    bulk collect into l_ns_records
    from normalization_schema
    group by description
    having count(*) > 1;

    if l_ns_records.count > 0 then
        dbms_output.put_line('PPC-MSG> Normalization Schema descriptions are not unique. All descriptions must be unique before creating the unique constraint.');
        dbms_output.put_line('PPC-MSG> Please review the following records with the client or use the clean-up script commented out at the bottom of this maint:');
        for i in l_ns_records.first..l_ns_records.last loop
            dbms_output.put_line('PPC-MSG>     ------------------------');
            dbms_output.put_line('PPC-MSG>     Description: '||l_ns_records(i).description);
            dbms_output.put_line('PPC-MSG>     Used by ids: '||l_ns_records(i).ids);
        end loop;
        dbms_output.put_line('PPC-MSG>     ------------------------');
        raise_application_error(-20000, 'Please read the dbms log and make the necessary changes to the system.');
    else
        dbms_output.put_line('PPC-MSG> All descriptions are unique. Attempting to add unique constraint to normalization schema.');
        execute immediate 'alter table normalization_schema add constraint norm_schema_desc_uix unique (description) using index tablespace pwrplant_idx';
        dbms_output.put_line('PPC-MSG> Successfully added unique constraint to normalization schema.');
    end if;
exception
    when unique_cons_exists then
        dbms_output.put_line('PPC-MSG> Unique constraint already exists on normalization schema.');
end;
/

-- -- Clean-up script that makes all descriptions unique by prepending the normalization_id to the description:
--
-- update normalization_schema
-- set description = substr('('||normalization_id||')'||description, 1, 35)
-- where description in (
--     select z.description
--     from normalization_schema z
--     group by z.description
--     having count(*) > 1
-- );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2682, 0, 2015, 2, 0, 0, 044182, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044182_reg_norm_schema_desc_uix_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;