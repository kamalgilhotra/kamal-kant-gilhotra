/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051349_lessee_01_add_payment_month_ddl.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/24/2018 David Conway     Add payment month column to:
||                                            LS_ILR_PAYMENT_HDR
||                                            V_LS_ILR_PAYMENT_HDR_FX_VW
|| 2017.4.0.0  06/14/2018 David Conway     Added comment to payment_month,
||                                            moved rest into a separate script
||                                            to run after the DML.
||============================================================================
*/

alter table ls_payment_hdr add payment_month date;

comment on column ls_payment_hdr.payment_month
  is 'The month that the payment is to be made, will be different from the accounting month if there is a payment shift.' ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6709, 0, 2017, 4, 0, 0, 51349, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051349_lessee_01_add_payment_month_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;