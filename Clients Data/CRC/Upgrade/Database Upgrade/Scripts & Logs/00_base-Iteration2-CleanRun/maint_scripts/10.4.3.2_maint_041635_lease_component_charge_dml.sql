/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		maint_041635_lease_component_charge_dml.sql
|| File Name:   [MODULE NAME].sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 01/14/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/


--Add ppbase workspace
insert into PPBASE_WORKSPACE
	(MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID)
values
	('LESSEE', 'admin_comp_alloc', 'Component Invoice Allocation', 'uo_ls_admincntr_comp_alloc', 'Component Invoice Allocation', 1);

--Add workspace to Lease side menu
insert into PPBASE_MENU_ITEMS
	(MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
	('LESSEE', 'admin_comp_alloc', 2, 6, 'Component Invoice Allocation', '', 'menu_wksp_admin', 'admin_comp_alloc', 1);

--Register action dropdown for allocation
insert into PPBASE_ACTIONS_WINDOWS
	(ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values
	(23, 'LESSEE', 'allocate', 'Allocate', 1, 'ue_allocate');

--Register action dropdown for posting
insert into PPBASE_ACTIONS_WINDOWS
	(ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values
	(24, 'LESSEE', 'post', 'Post', 1, 'ue_post');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2167, 0, 10, 4, 3, 2, 041635, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041635_lease_component_charge_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;