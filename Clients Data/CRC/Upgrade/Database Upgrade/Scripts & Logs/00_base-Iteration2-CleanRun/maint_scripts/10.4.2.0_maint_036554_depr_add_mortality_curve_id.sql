SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036554_depr_add_mortality_curve_id.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/25/2014  Brandon Beck
||============================================================================
*/

declare
   procedure DROP_COLUMN(V_TABLE_NAME varchar2,
                         V_COL_NAME   varchar2) is
   begin
      execute immediate 'alter table ' || V_TABLE_NAME || ' drop column ' || V_COL_NAME;
      DBMS_OUTPUT.PUT_LINE('Sucessfully dropped column ' || V_COL_NAME || ' from table ' ||
                           V_TABLE_NAME || '.');
   exception
      when others then
         if sqlcode = -904 then
            --904 is invalid identifier, which means that the table already does not have this column, so do nothing
            DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' on table ' || V_TABLE_NAME ||
                                 ' was already removed. No action necessary.');
         else
            RAISE_APPLICATION_ERROR(-20000,
                                    'Could not drop column ' || V_COL_NAME || ' from ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
         end if;
   end DROP_COLUMN;

   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin
   --add mortality_curve_id
   ADD_COLUMN('depr_calc_stg',
              'mortality_curve_id',
              'number(22,0)',
              'A Key identifying the IOWA curve used for mass curve retirement processing.');
   ADD_COLUMN('depr_calc_stg_arc',
              'mortality_curve_id',
              'number(22,0)',
              'A Key identifying the IOWA curve used for mass curve retirement processing.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'mortality_curve_id',
              'number(22,0)',
              'A Key identifying the IOWA curve used for mass curve retirement processing.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (987, 0, 10, 4, 2, 0, 36554, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036554_depr_add_mortality_curve_id.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;