/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035763_lease_rental_expense_gui.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Kyle Peterson
||============================================================================
*/

update LS_ACCRUAL_TYPE set DESCRIPTION = 'Interest/Rental Accrual' where ACCRUAL_TYPE_ID = 1;

update LS_PAYMENT_TYPE set DESCRIPTION = 'Interest/Rental Payment' where PAYMENT_TYPE_ID = 2;

update LS_PAYMENT_TYPE set DESCRIPTION = 'Interest/Rental Adjustment' where PAYMENT_TYPE_ID = 5;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (906, 0, 10, 4, 2, 0, 35763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035763_lease_rental_expense_gui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;