/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038215_pwrtax_rpts_conv2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/23/2014 Alex Pivoshenko
||============================================================================
*/

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (72, 'Powertax - Tax Depr Item', 'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (174, 'Tax Depr Item', 'dw', '', 'dw_tax_depr_item_filter', 'tax_depr_item_id', 'description', 'c', 1, 1, 1, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (72, 102);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (72, 103);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (72, 105);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (72, 110);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (72, 164);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (72, 174);

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403014, sysdate, user, 'Depreciation Items by Tax Class', ' ', 'PowerTax', 'dw_tax_rpt_tax_item_xtab', '',
    'PowerTax_Rollup', '', 'PwrTax - 64', '', '', '', 1, 130, 52, 72, 1, 3, '', '', null, '', '', 0);
insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403015, sysdate, user, 'Depreciation Items by Vintage',
    'Creates a generic report of depreciation items where the depreciation items are reported by Tax Class, organized under Vintage',
    'PowerTax', 'dw_tax_rpt_tax_item_vint_xtab', '', 'PowerTax_Rollup', '', 'PwrTax - 65', '', '', '', 1, 130, 52, 72, 1,
    3, '', '', null, '', '', 0);
	
insert into PP_REPORTS
   ( REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW ) 
values
   (400012, sysdate, user, 'Book-to-Tax Reconciliation TOP',
   'Book-to-Tax Reconciliation TOP', 'PowerTax', 'dw_tax_rpt_book_tax_recon_top_xtab', '', 'PowerTax_Rollup', '', 'PwrTax - 72',
   '', 'DETAIL', '', 1, 29, 52, 69, 1, 3, '', '', null, '', '', 0 );
   
insert into pp_reports
   ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type,
    time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id,
    pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment,
    last_approved_date, pp_report_number, old_report_number, dynamic_dw ) 
values
   (400013, sysdate, user, 'Book-to-Tax Reconciliation TC', 'Book-to-Tax Reconciliation TC', 'PowerTax',
   'dw_tax_rpt_book_tax_recon_tc_xtab', '', 'PowerTax_Rollup', '', 'PwrTax - 73', '', 'DETAIL', '', 1, 29, 52, 69, 1, 3, '', '',
   null, '', '', 0 );

   
--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1268, 0, 10, 4, 3, 0, 38215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038215_pwrtax_rpts_conv2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;