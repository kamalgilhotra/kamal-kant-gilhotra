/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039712_cwip_fast101.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/16/2014 Sunjin Cone       Net $0 Unitization
||============================================================================
*/

alter table CHARGE_GROUP_CONTROL add NET_ZERO               number(22,0);
alter table UNITIZE_WO_LIST_TEMP add WOA_FUNC_CLASS_ID      number (22,0);
alter table UNITIZE_WO_LIST_TEMP add HAS_NET_ZERO_ADDS      number (22,0);
alter table UNITIZE_WO_LIST_TEMP add HAS_NET_ZERO_RWIP      number (22,0);
alter table UNITIZE_WO_LIST_TEMP add CREATE_NET_ZERO_ADD    number (22,0);
alter table UNITIZE_WO_LIST_TEMP add CREATE_NET_ZERO_RWIP   number (22,0);
alter table UNITIZE_WO_LIST_TEMP add NET_ZERO_UNIT_ASSET_ID number (22,0);

comment on column CHARGE_GROUP_CONTROL.NET_ZERO is 'System-assigns a value during Net $0 Unitization where a non-null value indicates the record combines with at least one other record as a sum(amount) equal to $0. A null value indicates the net $0 unitization tagging was not performed or the record simply does not qualify with another record.';
comment on column UNITIZE_WO_LIST_TEMP.WOA_FUNC_CLASS_ID is 'System-assigned optional identifier of a unique functional class assigned on the Work Order.';
comment on column UNITIZE_WO_LIST_TEMP.HAS_NET_ZERO_ADDS is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 Additions exist.';
comment on column UNITIZE_WO_LIST_TEMP.HAS_NET_ZERO_RWIP is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 RWIP exist.';
comment on column UNITIZE_WO_LIST_TEMP.CREATE_NET_ZERO_ADD is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 Additions exists and a $0 Unit Item needs to be created.';
comment on column UNITIZE_WO_LIST_TEMP.CREATE_NET_ZERO_RWIP is 'System-assigns a 1 value during net $0 unitization where 1 indicates net $0 RWIP exists and a $0 Unit Item needs to be created.';
comment on column UNITIZE_WO_LIST_TEMP.NET_ZERO_UNIT_ASSET_ID is 'The ASSET_ID used for the Unit Item created for net $0 unitization.';

drop table UNITIZE_ALLOC_UNITS_STG_TEMP;

create global temporary table UNITIZE_ELIG_UNITS_TEMP
(
 WORK_ORDER_ID            number(22,0) not null,
 UNIT_ITEM_ID             number(22,0) not null,
 EXPENDITURE_TYPE_ID      number(22,0) not null,
 ACTIVITY_CODE            varchar2(8),
 GL_ACCOUNT_ID            number(22,0),
 UTILITY_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_ID           number(22,0),
 SUB_ACCOUNT_ID           number(22,0),
 RETIREMENT_UNIT_ID       number(22,0),
 ASSET_LOCATION_ID        number(22,0),
 PROPERTY_GROUP_ID        number(22,0),
 SERIAL_NUMBER            varchar2(35),
 LDG_ASSET_ID             number(22,0),
 UNIT_ESTIMATE_ID         number(22,0),
 STATUS                   number(22,0),
 EXCLUDE_FROM_ALLOCATIONS number(22,0),
 QUANTITY                 number(22,2)
) on commit preserve rows;

comment on table  UNITIZE_ELIG_UNITS_TEMP is '(C) [04] A global temporary table used in Unitization processing for the eligible Unit Items where the already used Unit Items are excluded. No primary key.';
comment on column UNITIZE_ELIG_UNITS_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ELIG_UNITS_TEMP.UNIT_ITEM_ID is 'System-assigned identifier of the Unit Item.';
comment on column UNITIZE_ELIG_UNITS_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.  UADD and MADD are set to 1; all others are set to 2.';
comment on column UNITIZE_ELIG_UNITS_TEMP.ACTIVITY_CODE is 'System-assigned identifier of the Activity Code.';
comment on column UNITIZE_ELIG_UNITS_TEMP.GL_ACCOUNT_ID is  'System-assigned identifier of a gl account.';
comment on column UNITIZE_ELIG_UNITS_TEMP.RETIREMENT_UNIT_ID is  'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_ELIG_UNITS_TEMP.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.  It can be a FERC utility plant account such as 314 or the Companys own account number structure.';
comment on column UNITIZE_ELIG_UNITS_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_ELIG_UNITS_TEMP.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_ELIG_UNITS_TEMP.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_ELIG_UNITS_TEMP.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_ELIG_UNITS_TEMP.SERIAL_NUMBER is 'User input serial number.';
comment on column UNITIZE_ELIG_UNITS_TEMP.LDG_ASSET_ID is 'System-assigned identifier of an asset.';
comment on column UNITIZE_ELIG_UNITS_TEMP.UNIT_ESTIMATE_ID is 'The associated ESTIMATE_ID for Unit Items created from One for One estimates.';
comment on column UNITIZE_ELIG_UNITS_TEMP.EXCLUDE_FROM_ALLOCATIONS is '1 indicates the record is excluded from Unitization Allocation.';
comment on column UNITIZE_ELIG_UNITS_TEMP.QUANTITY is 'The summarized estimate record quantity.';


drop table UNITIZE_WO_UNITS_STG;

create global temporary table UNITIZE_WO_STG_UNITS_TEMP
(
 ID                    number(22,0),
 COMPANY_ID            number(22,0),
 WORK_ORDER_ID         number(22,0),
 NEW_UNIT_ITEM_ID      number(22,0),
 UNIT_ITEM_SOURCE      varchar2(35),
 RETIREMENT_UNIT_ID    number(22,0),
 UTILITY_ACCOUNT_ID    number(22,0),
 BUS_SEGMENT_ID        number(22,0),
 SUB_ACCOUNT_ID        number(22,0),
 PROPERTY_GROUP_ID     number(22,0),
 ASSET_LOCATION_ID     number(22,0),
 MAJOR_LOCATION_ID     number(22,0),
 WO_MAJOR_LOCATION_ID  number(22,0),
 UA_FUNC_CLASS_ID      number(22,0),
 PROPERTY_UNIT_ID      number(22,0),
 LOCATION_TYPE_ID      number(22,0),
 SERIAL_NUMBER         varchar2(35),
 GL_ACCOUNT_ID         number(22,0),
 ASSET_ACCT_METH_ID    number(22,0),
 ACTIVITY_CODE         varchar2(8),
 DESCRIPTION           varchar2(254),
 SHORT_DESCRIPTION     varchar2(35),
 AMOUNT                number(22,2),
 QUANTITY              number(22,2),
 LDG_ASSET_ID          number(22,0),
 STATUS                number(22,0),
 EST_REVISION          number(22,0),
 UNIT_ESTIMATE_ID      number(22,0),
 MINOR_UNIT_ITEM_ID    number(22,0),
 IN_SERVICE_DATE       date,
 REPLACEMENT_AMOUNT    number(22,2),
 ERROR_MSG             varchar2(2000),
 TIME_STAMP            date,
 USER_ID               varchar2(18)
) on commit preserve rows;

comment on table  UNITIZE_WO_STG_UNITS_TEMP is '(C) [04] A globabl temp table of the staged unit items that are eligible to become new unit items. No primary key.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.NEW_UNIT_ITEM_ID is 'System-assigned identifier of the new unit item.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.UNIT_ITEM_SOURCE is 'This field indicates if the source of the unit item creation is Actuals, Estimates, or CPR.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.RETIREMENT_UNIT_ID is 'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.  It can be a FERC utility plant account such as 314 or the Companys own account number structure.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.MAJOR_LOCATION_ID is 'System-assigned identifier of a major location.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.WO_MAJOR_LOCATION_ID is 'System-assigned identifier of a major location on work order control.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.UA_FUNC_CLASS_ID is 'System-assigned identifier of a particular functional class on the utility account.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.PROPERTY_UNIT_ID is 'System-assigned identifier of a particular property unit.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.LOCATION_TYPE_ID is 'System-assigned identifier of a particular location type.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.SERIAL_NUMBER is 'Records the serial number associated with the asset component by the manufacturer.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.GL_ACCOUNT_ID is 'System-assigned identifier of a particular gl account.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.ASSET_ACCT_METH_ID is 'System-assigned identifier of a particular asset accounting method.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.ACTIVITY_CODE is 'An activity code from the Activity Code table.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.DESCRIPTION is 'Description of the particular asset (retirement unit).';
comment on column UNITIZE_WO_STG_UNITS_TEMP.SHORT_DESCRIPTION is 'Short description filled in by user during unitization.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.AMOUNT is 'Amount used for Allocations.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.QUANTITY is 'Quantity used for Allocation and for the CPR entry.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.LDG_ASSET_ID is 'System-assigned identifier of the CPR asset.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.STATUS is 'Internally set processing indicated used in unitization allocation: Null =  From charges; 10 = Minor addition;  11 = From wo estimates';
comment on column UNITIZE_WO_STG_UNITS_TEMP.EST_REVISION is 'WO Estimate revision used for unit item creation';
comment on column UNITIZE_WO_STG_UNITS_TEMP.UNIT_ESTIMATE_ID is 'System-assigned identifier of a particular wo estimate record.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.MINOR_UNIT_ITEM_ID is 'Item id from this table for a previously unitized item.  Used for minor items to allow copying of additional details.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.IN_SERVICE_DATE is 'Engineering in-service date, when using multiple in-service dates for a single work order.  (See PP System Control.)';
comment on column UNITIZE_WO_STG_UNITS_TEMP.REPLACEMENT_AMOUNT is 'Dollar amount in current dollars to be (Handy Whitman) indexed to get original dollars when the retirement unit is using a retirement method of HW_Fifo or HW_Curve.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.ERROR_MSG is 'Error message';
comment on column UNITIZE_WO_STG_UNITS_TEMP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_STG_UNITS_TEMP.USER_ID is 'Standard system-assigned user id used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1425, 0, 10, 4, 3, 0, 39712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039712_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;