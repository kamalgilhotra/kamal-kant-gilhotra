 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_043631_sys_add_missing_bgd_syscon_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/13/2015 	D. Mendel   Create missing capital budget system control
 ||============================================================================
 */ 
 
insert into pp_system_control_company (control_id, control_name, control_value, description, long_description, company_id)
select (SELECT Max(control_id)+1 from pp_system_control_company), 
'BUDGET - Budget Review Any Revision',
'No',
'dw_yes_no;1',
'If this control is set to yes then when copying from another budget version and applying the obey review level then any revision on the project can have gone through the review to qualify, if no then the revision that is being copied must have been reviewed',
-1
from dual
WHERE NOT EXISTS (SELECT 1 FROM pp_system_control_company WHERE Lower(Trim(control_name)) = Lower('BUDGET - Budget Review Any Revision'));

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2498, 0, 2015, 1, 0, 0, 43631, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043631_sys_add_missing_bgd_syscon_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;