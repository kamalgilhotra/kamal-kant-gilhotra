/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030551_lease_mla_approval.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 B. Beck        Point release
||============================================================================
*/

--Create table ls_lease_approval and add keys

create table LS_LEASE_APPROVAL
(
 LEASE_ID           number(22,0),
 REVISION           number(22,0),
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 APPROVAL_TYPE_ID   number(22,0),
 APPROVAL_STATUS_ID number(22,0),
 APPROVER           varchar2(18),
 APPROVAL_DATE      date,
 REJECTED           number(1,0),
 REVISION_DESC      varchar2(35),
 REVISION_LONG_DESC varchar2(254)
);

alter table LS_LEASE_APPROVAL
   add constraint PK_LS_LEASE_APPROVAL
       primary key (LEASE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_APPROVAL
   add constraint FK_LS_LEASE_APPROVAL
       foreign key (LEASE_ID)
       references LS_LEASE(LEASE_ID);

--Update LS_LEASE_STATUS

update LS_LEASE_STATUS
   set TIME_STAMP = sysdate, DESCRIPTION = 'Initiated',
       LONG_DESCRIPTION = 'The lease has been created, but not submitted for approval'
 where LEASE_STATUS_ID = 1;

update LS_LEASE_STATUS
   set TIME_STAMP = sysdate, DESCRIPTION = 'Pending Approval',
       LONG_DESCRIPTION = 'The lease has been submitted, but not approved or rejected'
 where LEASE_STATUS_ID = 2;

update LS_LEASE_STATUS
   set TIME_STAMP = sysdate, DESCRIPTION = 'Open',
       LONG_DESCRIPTION = 'The lease has been approved, and ILRs can be attached to it'
 where LEASE_STATUS_ID = 3;

update LS_LEASE_STATUS
   set TIME_STAMP = sysdate, DESCRIPTION = 'Rejected',
       LONG_DESCRIPTION = 'The lease has been rejected'
 where LEASE_STATUS_ID = 4;

update LS_LEASE_STATUS
   set TIME_STAMP = sysdate, DESCRIPTION = 'Closed', LONG_DESCRIPTION = 'The lease is closed'
 where LEASE_STATUS_ID = 5;

insert into LS_LEASE_STATUS
   (LEASE_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (6, sysdate, user, 'Retired', 'The lease is retired');

--WORKFLOW_SUBSYSTEM update/insert
update WORKFLOW_SUBSYSTEM
   set FIELD1_DESC = 'MLA Number', FIELD2_DESC = 'Revision',
       FIELD1_SQL = 'select lease_number from ls_lease where lease_id = <<id_field1>>',
       PENDING_NOTIFICATION_TYPE = 'MLA APPROVAL MESSAGE',
       APPROVED_NOTIFICATION_TYPE = 'MLA APPROVED NOTICE MESSAGE',
       REJECTED_NOTIFICATION_TYPE = 'MLA REJECTION MESSAGE',
       SEND_SQL = 'select PKG_LEASE_CALC.F_SEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
       APPROVE_SQL = 'select PKG_LEASE_CALC.F_APPROVE_MLA(<<id_field1>>, <<id_field2>>) from dual',
       REJECT_SQL = 'select PKG_LEASE_CALC.F_REJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
       UNREJECT_SQL = 'select PKG_LEASE_CALC.F_UNREJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
       UNSEND_SQL = 'select PKG_LEASE_CALC.F_UNSEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
       UPDATE_WORKFLOW_TYPE_SQL = 'select PKG_LEASE_CALC.F_UPDATE_WORKFLOW_MLA(<<id_field1>>, <<id_field2>>) from dual'
 where SUBSYSTEM = 'mla_approval';

insert into WORKFLOW_SUBSYSTEM
   (SUBSYSTEM, FIELD1_DESC, FIELD2_DESC, FIELD3_DESC, FIELD4_DESC, FIELD1_SQL)
values
   ('ilr_approval', 'ILR Number', 'Revision', null, null,
    'select ilr_number from ls_ilr where ilr_id = <<id_field1>>');

--WORKFLOW_TYPE insert
insert into WORKFLOW_TYPE
   (WORKFLOW_TYPE_ID, DESCRIPTION, SUBSYSTEM, ACTIVE, USE_LIMITS, SQL_APPROVAL_AMOUNT,
    EXTERNAL_WORKFLOW_TYPE, BASE_SQL)
   select nvl(max(WORKFLOW_TYPE_ID),0)+1, 'MLA Approval', 'mla_approval', 1, 1,
    'select sum(max_lease_line) from LS_LEASE_COMPANY where lease_id = <<id_field1>>', null, 1
    from WORKFLOW_TYPE;

insert into WORKFLOW_TYPE
   (WORKFLOW_TYPE_ID, DESCRIPTION, SUBSYSTEM, ACTIVE, USE_LIMITS, SQL_APPROVAL_AMOUNT,
    EXTERNAL_WORKFLOW_TYPE, BASE_SQL)
   select nvl(max(WORKFLOW_TYPE_ID),0)+1, 'ILR Approval', 'ilr_approval', 1, 1,
    'select nvl(beg_obligation,0) from ls_ilr_schedule where ilr_id = <<id_field1>>', null, 1
    from WORKFLOW_TYPE;

--Update PPBASE_WORKSPACE
update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_lscntr_wksp_approvals'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'approval_mla';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (455, 0, 10, 4, 1, 0, 30551, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030551_lease_mla_approval.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
