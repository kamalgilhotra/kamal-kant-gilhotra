/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045442_jobserver_add_file_watcher_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 05/20/2016 Anand R        Add security keys for JobService File Watcher
||============================================================================
*/

insert into pp_web_security_perm_control (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME)
values ('JobService.FileWatcher.AddClone', 9, 8, null, 'All', 'Add/Clone');

insert into pp_web_security_perm_control (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME)
values ('JobService.FileWatcher.Edit', 9, 8, null, 'All', 'Edit');

insert into pp_web_security_perm_control (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME)
values ('JobService.FileWatcher.Delete', 9, 8, null, 'All', 'Delete');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3196, 0, 2016, 1, 0, 0, 45442, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045442_jobserver_add_file_watcher_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
