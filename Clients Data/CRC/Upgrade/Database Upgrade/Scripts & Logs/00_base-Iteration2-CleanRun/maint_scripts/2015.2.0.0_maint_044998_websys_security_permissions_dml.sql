/*
||================================================================================================
|| Application: PowerPlan
|| Module: 		Security
|| File Name:   maint_044998_websys_security_permissions_dml.sql
||================================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||================================================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- -------------------  ----------------------------------------------------
|| 2015.2     09/21/2015 Ryan Oliveria 		  Updating data for web security
||================================================================================================
*/


----Queue > General
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('IntegrationManager.Queue.MoveInQueue', 9, null,'Queue','General','Move in Queue');

----Asset Analytics > All > Summary Dashboard
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.SumDashboardBtns.Explore',3, null,'All','Summary Dashboard Buttons','Explore');

----Asset Analytics > All > Exploration Dashboard
INSERT INTO PWRPLANT.PP_WEB_SECURITY_PERM_CONTROL (OBJECTS, MODULE_ID, COMPONENT_ID, PSEUDO_COMPONENT, SECTION, NAME) VALUES ('AssetAnalytics.ExpDashboardBtns.ResetFilters',3, null,'All','Exploration Dashboard Buttons','Reset Filters');
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2878, 0, 2015, 2, 0, 0, 044998, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044998_websys_security_permissions_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 