/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050833_sys_01_set_types_authid_current_user_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/10/2018 Andrew Hill       Set AUTHID for Object Types to current_user
||============================================================================
*/

BEGIN
  FOR stmt IN ( SELECT  regexp_replace(LOWER(dbms_metadata.get_ddl('TYPE_SPEC', type_name, OWNER)), 
                                       'create or replace( editionable)? type ("pwrplant"\.")(.+)"( force)? (as|is)', 'ALTER TYPE \3 REPLACE AUTHID current_user \5') as text
                FROM all_types
                WHERE OWNER IN ('PWRPLANT')
                AND TYPECODE = 'OBJECT'
                AND type_name NOT LIKE 'BIN$%'
                AND type_name NOT LIKE 'SYS_PLSQL%')
  LOOP
    EXECUTE IMMEDIATE stmt.text;
  END LOOP;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4295, 0, 2017, 3, 0, 0, 50833, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050833_sys_01_set_types_authid_current_user_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
