/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041431_pcm_wo_dyn_filters_dml.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	12/04/2014 Ryan Oliveria		Adding dynamic filters for WO Search
||========================================================================================
*/
 
--* Dynamic Filters
insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(254, 'Funding Project Number', 'dw', 'work_order_control.funding_wo_id', 'dw_pp_funding_project_filter', 'work_order_id', 'work_order_number', 'N');

insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE)
values (255, 'Funding Project Description', 'sle', 'work_order_control.description', 'work_order_control.funding_wo_id in (select work_order_id from work_order_control where funding_wo_indicator = 1 and [selected_values])');



--* Dynamic Filter Mappings
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 4) /*WO Number*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 53) /*WO Description*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 3) /*Company (Description)*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 254) /*FP Number*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 255) /*FP Description*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 29) /*Budget (Description)*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 30) /*Budget (Budget Number)*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 31) /*Budget Version (Description)*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 215) /*Major Location*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 216) /*Asset Location*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 34) /*WO Type*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 33) /*WO Status*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 219) /*Responsible Dept*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 220) /*Charging Dept*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 32) /*Work Order Group*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 221) /*Completion Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 222) /*Est Complete Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 223) /*Est In-Service Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 224) /*Est Start Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 225) /*In-Service Date*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 226) /*External WO Number*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 227) /*Notes*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 228) /*Business Segment*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 229) /*AFUDC Type*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 230) /*Agreement*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 231) /*Allocation Method*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 232) /*ARO Work Order*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 233) /*Budget Organization*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 234) /*Budget Summary*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 235) /*Close Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 236) /*Date Initiated*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 237) /*Closing Option*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 238) /*Contract Admin*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 239) /*Division*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 240) /*Eligible for AFUDC*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 241) /*Eligible for CPI*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 242) /*Engineer*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 243) /*Initiator*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 244) /*Plant Accountant*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 245) /*Project Manager*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 246) /*Functional Class*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 247) /*Other*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 248) /*Reason Code*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 249) /*Reimbursable*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 250) /*Tax Status*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 251) /*Town*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 252) /*WO ID from Excel*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 253) /*WO Number from Excel*/;




--* Prop Tax District
insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION,
	DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(256, 'State', 'dw', 'asset_location.state_id',
	'dw_pp_state_filter', 'state_id', 'long_description', 'C');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION,
	DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(257, 'County', 'dw', 'asset_location.county_id',
	'dw_pp_county_filter', 'county_id', 'description', 'C');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION,
	DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(258, 'Property Tax District', 'dw', 'asset_location.tax_district_id',
	'dw_pp_prop_tax_district_filter', 'tax_district_id', 'description', 'N');



insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 256) /*State*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 257) /*County*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 258) /*Prop Tax District*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 256) /*State*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 257) /*County*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 258) /*Prop Tax District*/;



insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (71, 257, 256, 'county.state_id');

insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (72, 258, 256, 'prop_tax_district.state_id');
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (73, 258, 257, 'prop_tax_district.county_id');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2097, 0, 2015, 1, 0, 0, 041431, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041431_pcm_wo_dyn_filters_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;