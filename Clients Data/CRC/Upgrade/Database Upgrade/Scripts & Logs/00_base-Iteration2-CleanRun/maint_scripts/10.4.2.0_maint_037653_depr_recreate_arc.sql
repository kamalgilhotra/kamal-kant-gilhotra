SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_037653_depr_recreate_arc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/09/2014 Charlie Shilling
||============================================================================
*/

declare
   TABLE_DNE exception;
   pragma exception_init(TABLE_DNE, -942);

   V_COUNT number(22, 0);
   V_SQL   varchar2(2000) := 'select count(1) from DEPR_CALC_STG_ARC';

   C_CHECK_ROWS sys_refcursor;
begin
   open C_CHECK_ROWS for V_SQL;
   fetch C_CHECK_ROWS
      into V_COUNT;
   close C_CHECK_ROWS;

   if V_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'Trying to drop and recreate DEPR_CALC_STG_ARC. There are records currently in this table. Please make sure that the records are archived or are no longer needed, clear the table, and re-run the script.');
   end if;

   execute immediate 'DROP TABLE depr_calc_stg_arc';
exception
   when TABLE_DNE then
      DBMS_OUTPUT.PUT_LINE('Table DEPR_CALC_STG_ARC does not exist. Creating table now...');
end;
/

create table DEPR_CALC_STG_ARC
(
 DEPR_GROUP_ID                  number(22,0)   null,
 SET_OF_BOOKS_ID                number(22,0)   null,
 GL_POST_MO_YR                  date           null,
 CALC_MONTH                     date           null,
 DEPR_CALC_STATUS               number(2,0)    null,
 DEPR_CALC_MESSAGE              varchar2(2000) null,
 TRF_IN_EST_ADDS                varchar2(35)   null,
 INCLUDE_RWIP_IN_NET            varchar2(35)   null,
 DNSA_COR_BAL                   number(22,2)   default 0 null,
 DNSA_SALV_BAL                  number(22,2)   default 0 null,
 COR_YTD                        number(22,2)   default 0 null,
 SALV_YTD                       number(22,2)   default 0 null,
 FISCALYEARSTART                date           null,
 COMPANY_ID                     number(22,0)   null,
 SUBLEDGER_TYPE_ID              number(22,0)   null,
 DESCRIPTION                    varchar2(254)  null,
 DEPR_METHOD_ID                 number(22,0)   null,
 MID_PERIOD_CONV                number(22,2)   null,
 MID_PERIOD_METHOD              varchar2(35)   null,
 RATE                           number(22,8)   null,
 NET_GROSS                      number(22,0)   null,
 OVER_DEPR_CHECK                number(22,0)   null,
 NET_SALVAGE_PCT                number(22,8)   null,
 RATE_USED_CODE                 number(22,0)   null,
 END_OF_LIFE                    number(22,0)   null,
 EXPECTED_AVERAGE_LIFE          number(22,0)   null,
 RESERVE_RATIO_ID               number(22,0)   null,
 DMR_COST_OF_REMOVAL_RATE       number(22,8)   null,
 COST_OF_REMOVAL_PCT            number(22,8)   null,
 DMR_SALVAGE_RATE               number(22,8)   null,
 INTEREST_RATE                  number(22,8)   null,
 AMORTIZABLE_LIFE               number(22,0)   null,
 COR_TREATMENT                  number(1,0)    null,
 SALVAGE_TREATMENT              number(1,0)    null,
 NET_SALVAGE_AMORT_LIFE         number(22,0)   null,
 ALLOCATION_PROCEDURE           varchar2(5)    null,
 DEPR_LEDGER_STATUS             number(22,0)   null,
 BEGIN_RESERVE                  number(22,2)   default 0 null,
 END_RESERVE                    number(22,2)   default 0 null,
 RESERVE_BAL_PROVISION          number(22,2)   default 0 null,
 RESERVE_BAL_COR                number(22,2)   default 0 null,
 SALVAGE_BALANCE                number(22,2)   default 0 null,
 RESERVE_BAL_ADJUST             number(22,2)   default 0 null,
 RESERVE_BAL_RETIREMENTS        number(22,2)   default 0 null,
 RESERVE_BAL_TRAN_IN            number(22,2)   default 0 null,
 RESERVE_BAL_TRAN_OUT           number(22,2)   default 0 null,
 RESERVE_BAL_OTHER_CREDITS      number(22,2)   default 0 null,
 RESERVE_BAL_GAIN_LOSS          number(22,2)   default 0 null,
 RESERVE_ALLOC_FACTOR           number(22,2)   default 0 null,
 BEGIN_BALANCE                  number(22,2)   default 0 null,
 ADDITIONS                      number(22,2)   default 0 null,
 RETIREMENTS                    number(22,2)   default 0 null,
 TRANSFERS_IN                   number(22,2)   default 0 null,
 TRANSFERS_OUT                  number(22,2)   default 0 null,
 ADJUSTMENTS                    number(22,2)   default 0 null,
 DEPRECIATION_BASE              number(22,2)   default 0 null,
 END_BALANCE                    number(22,2)   default 0 null,
 DEPRECIATION_RATE              number(22,8)   null,
 DEPRECIATION_EXPENSE           number(22,2)   default 0 null,
 DEPR_EXP_ADJUST                number(22,2)   default 0 null,
 DEPR_EXP_ALLOC_ADJUST          number(22,2)   default 0 null,
 COST_OF_REMOVAL                number(22,2)   default 0 null,
 RESERVE_RETIREMENTS            number(22,2)   default 0 null,
 SALVAGE_RETURNS                number(22,2)   default 0 null,
 SALVAGE_CASH                   number(22,2)   default 0 null,
 RESERVE_CREDITS                number(22,2)   default 0 null,
 RESERVE_ADJUSTMENTS            number(22,2)   default 0 null,
 RESERVE_TRAN_IN                number(22,2)   default 0 null,
 RESERVE_TRAN_OUT               number(22,2)   default 0 null,
 GAIN_LOSS                      number(22,2)   default 0 null,
 VINTAGE_NET_SALVAGE_AMORT      number(22,2)   default 0 null,
 VINTAGE_NET_SALVAGE_RESERVE    number(22,2)   default 0 null,
 CURRENT_NET_SALVAGE_AMORT      number(22,2)   default 0 null,
 CURRENT_NET_SALVAGE_RESERVE    number(22,2)   default 0 null,
 IMPAIRMENT_RESERVE_BEG         number(22,2)   default 0 null,
 IMPAIRMENT_RESERVE_ACT         number(22,2)   default 0 null,
 IMPAIRMENT_RESERVE_END         number(22,2)   default 0 null,
 EST_ANN_NET_ADDS               number(22,2)   default 0 null,
 RWIP_ALLOCATION                number(22,2)   default 0 null,
 COR_BEG_RESERVE                number(22,2)   default 0 null,
 COR_EXPENSE                    number(22,2)   default 0 null,
 COR_EXP_ADJUST                 number(22,2)   default 0 null,
 COR_EXP_ALLOC_ADJUST           number(22,2)   default 0 null,
 COR_RES_TRAN_IN                number(22,2)   default 0 null,
 COR_RES_TRAN_OUT               number(22,2)   default 0 null,
 COR_RES_ADJUST                 number(22,2)   default 0 null,
 COR_END_RESERVE                number(22,2)   default 0 null,
 COST_OF_REMOVAL_RATE           number(22,8)   null,
 COST_OF_REMOVAL_BASE           number(22,2)   default 0 null,
 RWIP_COST_OF_REMOVAL           number(22,2)   default 0 null,
 RWIP_SALVAGE_CASH              number(22,2)   default 0 null,
 RWIP_SALVAGE_RETURNS           number(22,2)   default 0 null,
 RWIP_RESERVE_CREDITS           number(22,2)   default 0 null,
 SALVAGE_RATE                   number(22,8)   null,
 SALVAGE_BASE                   number(22,2)   default 0 null,
 SALVAGE_EXPENSE                number(22,2)   default 0 null,
 SALVAGE_EXP_ADJUST             number(22,2)   default 0 null,
 SALVAGE_EXP_ALLOC_ADJUST       number(22,2)   default 0 null,
 RESERVE_BAL_SALVAGE_EXP        number(22,2)   default 0 null,
 RESERVE_BLENDING_ADJUSTMENT    number(22,2)   default 0 null,
 RESERVE_BLENDING_TRANSFER      number(22,2)   default 0 null,
 COR_BLENDING_ADJUSTMENT        number(22,2)   default 0 null,
 COR_BLENDING_TRANSFER          number(22,2)   default 0 null,
 IMPAIRMENT_ASSET_AMOUNT        number(22,2)   default 0 null,
 IMPAIRMENT_EXPENSE_AMOUNT      number(22,2)   default 0 null,
 RESERVE_BAL_IMPAIRMENT         number(22,2)   default 0 null,
 BEGIN_BALANCE_YEAR             number(22,2)   default 0 null,
 BEGIN_RESERVE_YEAR             number(22,2)   default 0 null,
 BEGIN_COR_YEAR                 number(22,2)   default 0 null,
 BEGIN_BALANCE_QTR              number(22,2)   default 0 null,
 BEGIN_RESERVE_QTR              number(22,2)   default 0 null,
 BEGIN_COR_QTR                  number(22,2)   default 0 null,
 PLANT_ACTIVITY                 number(22,2)   default 0 null,
 RESERVE_ACTIVITY               number(22,2)   default 0 null,
 COR_ACTIVITY                   number(22,2)   default 0 null,
 PLANT_ACTIVITY_2               number(22,2)   default 0 null,
 RESERVE_ACTIVITY_2             number(22,2)   default 0 null,
 COR_ACTIVITY_2                 number(22,2)   default 0 null,
 CUMULATIVE_TRANSFERS           number(22,2)   default 0 null,
 RESERVE_DIFF                   number(22,2)   default 0 null,
 FISCAL_MONTH                   number(2,0)    null,
 FISCALQTRSTART                 date           null,
 TYPE_2_EXIST                   number(1,0)    null,
 CUM_UOP_DEPR                   number(22,2)   default 0 null,
 CUM_UOP_DEPR_2                 number(22,2)   default 0 null,
 PRODUCTION                     number(22,2)   default 0 null,
 ESTIMATED_PRODUCTION           number(22,2)   default 0 null,
 PRODUCTION_2                   number(22,2)   default 0 null,
 ESTIMATED_PRODUCTION_2         number(22,2)   default 0 null,
 SMOOTH_CURVE                   varchar2(5)    null,
 MIN_CALC                       number(22,2)   default 0 null,
 MIN_UOP_EXP                    number(22,2)   default 0 null,
 YTD_UOP_DEPR                   number(22,2)   default 0 null,
 YTD_UOP_DEPR_2                 number(22,2)   default 0 null,
 CURR_UOP_EXP                   number(22,2)   default 0 null,
 CURR_UOP_EXP_2                 number(22,2)   default 0 null,
 DEPRECIATION_UOP_RATE          number(22,8)   default 0 null,
 DEPRECIATION_UOP_RATE_2        number(22,8)   default 0 null,
 DEPRECIATION_BASE_UOP          number(22,2)   default 0 null,
 DEPRECIATION_BASE_UOP_2        number(22,2)   default 0 null,
 OVER_DEPR_ADJ                  number(22,2)   default 0 null,
 OVER_DEPR_ADJ_COR              number(22,2)   default 0 null,
 OVER_DEPR_ADJ_SALV             number(22,2)   default 0 null,
 ORIG_BEGIN_RESERVE             number(22,2)   default 0 null,
 ORIG_COR_BEG_RESERVE           number(22,2)   default 0 null,
 ORIG_DEPR_EXPENSE              number(22,2)   default 0 null,
 ORIG_DEPR_EXP_ALLOC_ADJUST     number(22,2)   default 0 null,
 ORIG_SALV_EXPENSE              number(22,2)   default 0 null,
 ORIG_SALV_EXP_ALLOC_ADJUST     number(22,2)   default 0 null,
 ORIG_COR_EXPENSE               number(22,2)   default 0 null,
 ORIG_COR_EXP_ALLOC_ADJUST      number(22,2)   default 0 null,
 RETRO_DEPR_ADJ                 number(22,2)   default 0 null,
 RETRO_COR_ADJ                  number(22,2)   default 0 null,
 RETRO_SALV_ADJ                 number(22,2)   default 0 null,
 IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2)   null,
 IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2)   null,
 CURVE_TRUEUP_ADJ               number(22,2)   null,
 CURVE_TRUEUP_ADJ_SALV          number(22,2)   null,
 CURVE_TRUEUP_ADJ_COR           number(22,2)   null,
 SPREAD_FACTOR_ID               number(22,0)   null,
 UOP_EXP_ADJ                    number(22,2)   null,
 CUM_RATE_DEPR                  number(22,2)   null,
 YTD_RATE_DEPR                  number(22,2)   null,
 CURR_RATE_EXP                  number(22,2)   null,
 MIN_ANNUAL_EXPENSE             number(22,2)   null,
 MAX_ANNUAL_EXPENSE             number(22,2)   null,
 YTD_LEDGER_DEPR_EXPENSE        number(22,2)   null,
 CUM_LEDGER_DEPR_EXPENSE        number(22,2)   null,
 MIN_CUM_EXP                    number(22,2)   null,
 EFFECTIVE_CUM_UOP_EXPENSE      number(22,2)   null,
 FUNDING_WO_ID                  number(22,0)   default -1 null,
 REVISION                       number(22,0)   default -1 null,
 COMBINED_DEPR_GROUP_ID         number(22,0)   null,
 COMBINED_DEPR_ADJ              number(22,2)   null,
 COMBINED_SALV_ADJ              number(22,2)   null,
 COMBINED_COR_ADJ               number(22,2)   null,
 RWIP_SALVAGE                   number(22,2)   null,
 RWIP_COR                       number(22,2)   null,
 MORTALITY_CURVE_ID             number(22,0)   null,
 TIME_STAMP                     date           null,
 USER_ID                        varchar2(18)   null,
 RWIP_IN_OVER_DEPR              number(1,0)    default 0 null
);

COMMENT ON TABLE depr_calc_stg_arc IS '(T)  [01]
Table to perform group depreciation calculation against.  It is the callers repsonsibility to stage the data and handle the results.';

COMMENT ON COLUMN depr_calc_stg_arc.depr_group_id IS 'Internal ID linking to the depreciation group';
COMMENT ON COLUMN depr_calc_stg_arc.set_of_books_id IS 'The set of books';
COMMENT ON COLUMN depr_calc_stg_arc.gl_post_mo_yr IS 'The accounting month';
COMMENT ON COLUMN depr_calc_stg_arc.calc_month IS 'The month being calculated';
COMMENT ON COLUMN depr_calc_stg_arc.depr_calc_status IS 'Status column';
COMMENT ON COLUMN depr_calc_stg_arc.depr_calc_message IS 'An error mesaging';
COMMENT ON COLUMN depr_calc_stg_arc.trf_in_est_adds IS 'Are transfers included int eh net adds';
COMMENT ON COLUMN depr_calc_stg_arc.include_rwip_in_net IS 'Is rwip included in the net reserve';
COMMENT ON COLUMN depr_calc_stg_arc.dnsa_cor_bal IS 'Net Salvage Amortization Balance for Cost of Removal';
COMMENT ON COLUMN depr_calc_stg_arc.dnsa_salv_bal IS 'Net Salvage Amortization Balance for Salvage';
COMMENT ON COLUMN depr_calc_stg_arc.cor_ytd IS 'The year to date cost of removal';
COMMENT ON COLUMN depr_calc_stg_arc.salv_ytd IS 'The year to date salvage';
COMMENT ON COLUMN depr_calc_stg_arc.fiscalyearstart IS 'The start of the fiscal year';
COMMENT ON COLUMN depr_calc_stg_arc.company_id IS 'Internal ID linking to company';
COMMENT ON COLUMN depr_calc_stg_arc.subledger_type_id IS 'The subledger indicator';
COMMENT ON COLUMN depr_calc_stg_arc.description IS 'The description';
COMMENT ON COLUMN depr_calc_stg_arc.depr_method_id IS 'Internal ID linking to the depreciation method';
COMMENT ON COLUMN depr_calc_stg_arc.mid_period_conv IS 'The percent of current activity to include in depreciation base';
COMMENT ON COLUMN depr_calc_stg_arc.mid_period_method IS 'The mid period method';
COMMENT ON COLUMN depr_calc_stg_arc.rate IS 'The rate';
COMMENT ON COLUMN depr_calc_stg_arc.net_gross IS '1 means calculate depreciation based on the net balance.';
COMMENT ON COLUMN depr_calc_stg_arc.over_depr_check IS 'Perform overdepreciation checks';
COMMENT ON COLUMN depr_calc_stg_arc.net_salvage_pct IS 'Percent of beginning balance that should be salvage';
COMMENT ON COLUMN depr_calc_stg_arc.rate_used_code IS 'Has the rate been used in a calculation before';
COMMENT ON COLUMN depr_calc_stg_arc.end_of_life IS 'End of life for end of life methods';
COMMENT ON COLUMN depr_calc_stg_arc.expected_average_life IS 'The expected average life';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_ratio_id IS 'The reserve ration';
COMMENT ON COLUMN depr_calc_stg_arc.dmr_cost_of_removal_rate IS 'The cost of removal rate';
COMMENT ON COLUMN depr_calc_stg_arc.cost_of_removal_pct IS 'The percentage of the plant balance that is estimated cost of removal';
COMMENT ON COLUMN depr_calc_stg_arc.dmr_salvage_rate IS 'The salvage rate';
COMMENT ON COLUMN depr_calc_stg_arc.interest_rate IS 'The interest rate';
COMMENT ON COLUMN depr_calc_stg_arc.amortizable_life IS 'The amortizable life of the depreciation group';
COMMENT ON COLUMN depr_calc_stg_arc.cor_treatment IS 'How cost of removal should be treated';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_treatment IS 'How is salvage treated';
COMMENT ON COLUMN depr_calc_stg_arc.net_salvage_amort_life IS 'Net Salvage Amortization life';
COMMENT ON COLUMN depr_calc_stg_arc.allocation_procedure IS 'The depreciation allocation procedure';
COMMENT ON COLUMN depr_calc_stg_arc.depr_ledger_status IS 'The depr ledger status';
COMMENT ON COLUMN depr_calc_stg_arc.begin_reserve IS 'The Beginning reserve for the period';
COMMENT ON COLUMN depr_calc_stg_arc.end_reserve IS 'The end reserve for the period';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_provision IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_cor IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_balance IS 'The salvage balance';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_adjust IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_retirements IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_tran_in IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_tran_out IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_other_credits IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_gain_loss IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_alloc_factor IS 'The reserve allocation factor';
COMMENT ON COLUMN depr_calc_stg_arc.begin_balance IS 'The beginning plant balance for the period';
COMMENT ON COLUMN depr_calc_stg_arc.additions IS 'The plant addidtions';
COMMENT ON COLUMN depr_calc_stg_arc.retirements IS 'The retirement amount';
COMMENT ON COLUMN depr_calc_stg_arc.transfers_in IS 'Transfer In Amount';
COMMENT ON COLUMN depr_calc_stg_arc.transfers_out IS 'Transfer Out Amount';
COMMENT ON COLUMN depr_calc_stg_arc.adjustments IS 'Plant Adjustments';
COMMENT ON COLUMN depr_calc_stg_arc.depreciation_base IS 'The deprecation base';
COMMENT ON COLUMN depr_calc_stg_arc.end_balance IS 'The end plant balance';
COMMENT ON COLUMN depr_calc_stg_arc.depreciation_rate IS 'The depreciation rate';
COMMENT ON COLUMN depr_calc_stg_arc.depreciation_expense IS 'The current period calculated depreciation expense';
COMMENT ON COLUMN depr_calc_stg_arc.depr_exp_adjust IS 'Depreciation Expense Adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.depr_exp_alloc_adjust IS 'Depreciation Expense Adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.cost_of_removal IS 'Cost of Removal for the period';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_retirements IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_returns IS 'Salvage returns';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_cash IS 'Salvage Cash';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_credits IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_adjustments IS 'Reserve adjustment amount';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_tran_in IS 'Reserve Transfer Into this group';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_tran_out IS 'Reserve Transfer out of this group';
COMMENT ON COLUMN depr_calc_stg_arc.gain_loss IS 'Gain loss';
COMMENT ON COLUMN depr_calc_stg_arc.vintage_net_salvage_amort IS 'The vintaged net salvage amortization';
COMMENT ON COLUMN depr_calc_stg_arc.vintage_net_salvage_reserve IS 'The vintaged net salvage reserve';
COMMENT ON COLUMN depr_calc_stg_arc.current_net_salvage_amort IS 'Current Net Salvage Amortization';
COMMENT ON COLUMN depr_calc_stg_arc.current_net_salvage_reserve IS 'Current Net Salvage Reserve';
COMMENT ON COLUMN depr_calc_stg_arc.impairment_reserve_beg IS 'The beginning balance for the impairment basis bucket';
COMMENT ON COLUMN depr_calc_stg_arc.impairment_reserve_act IS 'The activity for the impairment basis bucket';
COMMENT ON COLUMN depr_calc_stg_arc.impairment_reserve_end IS 'The ending reserve for the impairment';
COMMENT ON COLUMN depr_calc_stg_arc.est_ann_net_adds IS 'Estimate adds and adjustments';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_allocation IS 'The amount of rwip allocation to this group';
COMMENT ON COLUMN depr_calc_stg_arc.cor_beg_reserve IS 'The COR beginning Reserve for the perios';
COMMENT ON COLUMN depr_calc_stg_arc.cor_expense IS 'Current month calculated cost of removal expense';
COMMENT ON COLUMN depr_calc_stg_arc.cor_exp_adjust IS 'Current month cost of removal expense adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.cor_exp_alloc_adjust IS 'Current month cost of removal expense adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.cor_res_tran_in IS 'Cost of removal transferred in';
COMMENT ON COLUMN depr_calc_stg_arc.cor_res_tran_out IS 'Cost of removal transferred out';
COMMENT ON COLUMN depr_calc_stg_arc.cor_res_adjust IS 'Cost of removal reserve adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.cor_end_reserve IS 'Cost of removal ending reserve';
COMMENT ON COLUMN depr_calc_stg_arc.cost_of_removal_rate IS 'The cost of removal rate';
COMMENT ON COLUMN depr_calc_stg_arc.cost_of_removal_base IS 'The basis for calculating cost of removal expense';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_cost_of_removal IS 'The amount of RWIP Allocation that is cost of removal';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_salvage_cash IS 'The amount of RWIP Allocation that is salvage cash';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_salvage_returns IS 'The amount of RWIP Allocation that is salvage returns';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_reserve_credits IS 'The amount of RWIP Allocation that is salvage credits';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_rate IS 'The salvage rate';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_base IS 'The salvage base';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_expense IS 'The salvage expense';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_exp_adjust IS 'The salvage expense adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.salvage_exp_alloc_adjust IS 'The salvage expense adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_salvage_exp IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_blending_adjustment IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_blending_transfer IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.cor_blending_adjustment IS 'The COR adjustment due to blended set of books';
COMMENT ON COLUMN depr_calc_stg_arc.cor_blending_transfer IS 'The COR Transfer due to blended set of books';
COMMENT ON COLUMN depr_calc_stg_arc.impairment_asset_amount IS 'The amount of the impairment';
COMMENT ON COLUMN depr_calc_stg_arc.impairment_expense_amount IS 'The expense amount being impaired';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_bal_impairment IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.begin_balance_year IS 'The beginning plant balance for the year';
COMMENT ON COLUMN depr_calc_stg_arc.begin_reserve_year IS 'The beginning reserve balance for the year';
COMMENT ON COLUMN depr_calc_stg_arc.begin_cor_year IS 'The beginning cost of removal reserve balance for the year';
COMMENT ON COLUMN depr_calc_stg_arc.begin_balance_qtr IS 'The beginning plant balance for the quarter';
COMMENT ON COLUMN depr_calc_stg_arc.begin_reserve_qtr IS 'The beginning reserve balance for the quarter';
COMMENT ON COLUMN depr_calc_stg_arc.begin_cor_qtr IS 'The beginning cost of removal reserve balance for the quarter.';
COMMENT ON COLUMN depr_calc_stg_arc.plant_activity IS 'The plant activity for current month.';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_activity IS 'The reserve activity for the month.';
COMMENT ON COLUMN depr_calc_stg_arc.cor_activity IS 'The cost of removal reserve activity for the month';
COMMENT ON COLUMN depr_calc_stg_arc.plant_activity_2 IS 'The plant activity for the period. This can be different from plant_activity depending on the mid-period method.';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_activity_2 IS 'The reserve activity for the period. This can be different from plant_activity depending on the mid-period method.';
COMMENT ON COLUMN depr_calc_stg_arc.cor_activity_2 IS 'The cost of removal reserve activity for the period. This can be different from plant_activity depending on the mid-period method.';
COMMENT ON COLUMN depr_calc_stg_arc.cumulative_transfers IS 'Cumulative transfers';
COMMENT ON COLUMN depr_calc_stg_arc.reserve_diff IS 'Internal field used for depreciation calculation';
COMMENT ON COLUMN depr_calc_stg_arc.fiscal_month IS 'The firscal moth';
COMMENT ON COLUMN depr_calc_stg_arc.fiscalqtrstart IS 'The starting quarter for the month';
COMMENT ON COLUMN depr_calc_stg_arc.type_2_exist IS 'Used for unit of production calcualations';
COMMENT ON COLUMN depr_calc_stg_arc.cum_uop_depr IS 'Unit of Production';
COMMENT ON COLUMN depr_calc_stg_arc.cum_uop_depr_2 IS 'Unit of Production 2';
COMMENT ON COLUMN depr_calc_stg_arc.production IS 'Used for unit of production calcualations';
COMMENT ON COLUMN depr_calc_stg_arc.estimated_production IS 'Estimated Production for unit of production';
COMMENT ON COLUMN depr_calc_stg_arc.production_2 IS 'Used for unit of production calcualations';
COMMENT ON COLUMN depr_calc_stg_arc.estimated_production_2 IS 'Estimated Production for unit of production approach 2';
COMMENT ON COLUMN depr_calc_stg_arc.smooth_curve IS 'Should the trueup methodolgy use smoothing';
COMMENT ON COLUMN depr_calc_stg_arc.min_calc IS 'Used for unit of production calcualations';
COMMENT ON COLUMN depr_calc_stg_arc.min_uop_exp IS 'Used for unit of production calcualations';
COMMENT ON COLUMN depr_calc_stg_arc.ytd_uop_depr IS 'Used for unit of production calcualations';
COMMENT ON COLUMN depr_calc_stg_arc.ytd_uop_depr_2 IS 'Used for unit of production calcualations';
COMMENT ON COLUMN depr_calc_stg_arc.curr_uop_exp IS 'Expense for unit of production 1';
COMMENT ON COLUMN depr_calc_stg_arc.curr_uop_exp_2 IS 'Expense for unit of production 2';
COMMENT ON COLUMN depr_calc_stg_arc.depreciation_uop_rate IS 'The depreciation rate for unit of production';
COMMENT ON COLUMN depr_calc_stg_arc.depreciation_uop_rate_2 IS 'The depreciation rate for unit of production approach 2';
COMMENT ON COLUMN depr_calc_stg_arc.depreciation_base_uop IS 'The depreciation base for unit of production';
COMMENT ON COLUMN depr_calc_stg_arc.depreciation_base_uop_2 IS 'The depreciation base for unit of production approach 2';
COMMENT ON COLUMN depr_calc_stg_arc.over_depr_adj IS 'The over depr adjustment';
COMMENT ON COLUMN depr_calc_stg_arc.over_depr_adj_cor IS 'The over depr adjustment from COR';
COMMENT ON COLUMN depr_calc_stg_arc.over_depr_adj_salv IS 'The over depr adjustment from Salvage';
COMMENT ON COLUMN depr_calc_stg_arc.orig_begin_reserve IS 'The original begin_reserve from depr_ledger. Used because the retroactive rate change calculation can change the begin_reserve column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.orig_cor_beg_reserve IS 'The original cor_beg_reserve from depr_ledger. Used because the retroactive rate change calculation can change the cor_beg_reserve column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.orig_depr_expense IS 'The original depreciation expense from depr_ledger. Used because the retroactive rate change calculation can change the depreciation_expense column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.orig_depr_exp_alloc_adjust IS 'The original depr_exp_alloc_adjust from depr_ledger. Used because the retroactive rate change calculation can change the depr_exp_alloc_adjust column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.orig_salv_expense IS 'The original salvage expense from depr_ledger. Used because the retroactive rate change calculation can change the salvage_expense column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.orig_salv_exp_alloc_adjust IS 'The original salv_exp_alloc_adj from depr_ledger. Used because the retroactive rate change calculation can change the salv_exp_alloc_adjust column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.orig_cor_expense IS 'The original cost of removal expense from depr_ledger. Used because the retroactive rate change calculation can change the cor_expense column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.orig_cor_exp_alloc_adjust IS 'The original cor_exp_alloc_adjust from depr_ledger. Used because the retroactive rate change calculation can change the cor_exp_alloc_adjust column on this table.';
COMMENT ON COLUMN depr_calc_stg_arc.retro_depr_adj IS 'The adjustment to depreciation expense due to the retroactive rate change calculation. This value makes up part of the depr_exp_alloc_adjust column on depr_ledger.';
COMMENT ON COLUMN depr_calc_stg_arc.retro_cor_adj IS 'The adjustment to cost of removal expense due to the retroactive rate change calculation. This value makes up part of the cor_exp_alloc_adjust column on depr_ledger.';
COMMENT ON COLUMN depr_calc_stg_arc.retro_salv_adj IS 'The adjustment to salvage expene due to the retroactive rate change calculation. This value makes up part of the salv_exp_alloc_adjust column on depr_ledger.';
COMMENT ON COLUMN depr_calc_stg_arc.impairment_asset_activity_salv IS 'This holds any activity against the impairment bucket.  It is used for correctly processing salvage amounts.  It is populated by post.';
COMMENT ON COLUMN depr_calc_stg_arc.impairment_asset_begin_balance IS 'This holds the beginning balance for the impairment bucket.  It is used for correctly processing salvage amounts.';
COMMENT ON COLUMN depr_calc_stg_arc.curve_trueup_adj IS 'The adjustment to depreciation expense from the curve method keeping in sync with rate changes, estimate changes, and year and true up.';
COMMENT ON COLUMN depr_calc_stg_arc.curve_trueup_adj_salv IS 'The adjustment to salvage depreciation expense from the curve method keeping in sync with rate changes, estimate changes, and year and true up.';
COMMENT ON COLUMN depr_calc_stg_arc.curve_trueup_adj_cor IS 'The adjustment to cost of removal depreciation expense from the curve method keeping in sync with rate changes, estimate changes, and year and true up.';
COMMENT ON COLUMN depr_calc_stg_arc.spread_factor_id IS 'The factor_id from the spread_factor table to use while allocating depreciating expense to the current month.';
COMMENT ON COLUMN depr_calc_stg_arc.uop_exp_adj IS 'The adjustment to depreciation expense from the UOP meeting maximum and minimum values.';
COMMENT ON COLUMN depr_calc_stg_arc.cum_rate_depr IS 'The total rate depreciation over the life of the depreciation group. Used as the minimum when MIN_CALC_OPTION = 3.';
COMMENT ON COLUMN depr_calc_stg_arc.ytd_rate_depr IS 'The total rate depreciation since the beginning of the fiscal year. Used as the minimum when MIN_CALC_OPTION = 2.';
COMMENT ON COLUMN depr_calc_stg_arc.curr_rate_exp IS 'The total rate depreciation calculated for this month.';
COMMENT ON COLUMN depr_calc_stg_arc.min_annual_expense IS 'The minimum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.';
COMMENT ON COLUMN depr_calc_stg_arc.max_annual_expense IS 'The maximum depreciation expense to take each year for UOP method with MIN_CALC_OPTION = 1.';
COMMENT ON COLUMN depr_calc_stg_arc.ytd_ledger_depr_expense IS 'The year-to-date LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP.';
COMMENT ON COLUMN depr_calc_stg_arc.cum_ledger_depr_expense IS 'The cumulative LEDGER_DEPR_EXPENSE + UOP_EXP_ADJ from DEPR_GROUP_UOP for the depreciation group and set of books.';
COMMENT ON COLUMN depr_calc_stg_arc.min_cum_exp IS 'The minimum amount to compare the year-to-date or cumulative expense to.';
COMMENT ON COLUMN depr_calc_stg_arc.effective_cum_uop_expense IS 'The cumulative expense used in determining the uop_exp_adj.';
COMMENT ON COLUMN depr_calc_stg_arc.funding_wo_id IS 'A system-generated identifier of a particular funding project.';
COMMENT ON COLUMN depr_calc_stg_arc.revision IS 'The revision of the funding project. Only used for incremental forecasting.';
COMMENT ON COLUMN depr_calc_stg_arc.combined_depr_group_id IS 'The combined_depr_group_id assigned to a particular depreciation group.';
COMMENT ON COLUMN depr_calc_stg_arc.combined_depr_adj IS 'The adjustment to depreciation expense as a result of combined deprecition group allocation. This amount will be summarized to depr_exp_alloc_adjust with the other adjustments.';
COMMENT ON COLUMN depr_calc_stg_arc.combined_salv_adj IS 'The adjustment to salvage expense as a result of combined deprecition group allocation. This amount will be summarized to salv_exp_alloc_adjust with the other adjustments.';
COMMENT ON COLUMN depr_calc_stg_arc.combined_cor_adj IS 'The adjustment to cost of removal expense as a result of combined deprecition group allocation. This amount will be summarized to cor_exp_alloc_adjust with the other adjustments.';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_salvage IS 'Internal calc column for salvage.';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_cor IS 'Internal calc column for cost of removal.';
COMMENT ON COLUMN depr_calc_stg_arc.mortality_curve_id IS 'A Key identifying the IOWA curve used for mass curve retirement processing.';
COMMENT ON COLUMN depr_calc_stg_arc.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN depr_calc_stg_arc.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN depr_calc_stg_arc.rwip_in_over_depr IS 'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1104, 0, 10, 4, 2, 0, 37653, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037653_depr_recreate_arc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
