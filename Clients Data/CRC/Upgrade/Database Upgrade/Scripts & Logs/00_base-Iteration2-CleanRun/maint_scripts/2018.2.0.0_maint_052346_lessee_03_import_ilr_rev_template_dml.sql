/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052436_lessee_03_import_ilr_rev_template_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 3/5/2019 	 Shane "C" Ward   Default Import Template for ILR Revisions
||============================================================================
*/

--Create default Template using sequence
DECLARE
    templateid NUMBER;
BEGIN
    templateid := pp_import_template_seq.NEXTVAL;

	Insert into pp_import_Template
	(import_template_id,
	 import_Type_id,
	 description,
	 long_description,
	 created_by,
	 created_date,
	 do_update_with_add,
	 is_autocreate_template)
	values (templateid,
		   272,
		   'ILR New Revision Add',
		   'Default Template for uploading ILR revisions',
		   'PWRPLANT',
		   sysdate,
		   1,
		   0);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 1,
                 272,
                 'ilr_id',
                 1096);

                     INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 2,
                 272,
                 'discount_rate',
                 null);

                 INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 3,
                 272,
                 'payment_term_id',
                 null);
                 INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 4,
                 272,
                 'payment_term_date',
                 null);

                 INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 5,
                 272,
                 'payment_freq_id',
                 1047);

                 INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 6,
                 272,
                 'number_of_terms',
                 null);

                 INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 7,
                 272,
                 'paid_amount',
                 null);
END;
/

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15805, 0, 2018, 2, 0, 0, 52346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052346_lessee_03_import_ilr_rev_template_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
