/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051356_lessee_01_remove_cap_type_table_maint_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/22/2018 Josh Sandler     Remove ls_lease_cap_type from table maintenance
||============================================================================
*/

DELETE
FROM pp_table_groups
WHERE table_name = 'ls_lease_cap_type';

DELETE
FROM powerplant_columns
WHERE table_name = 'ls_lease_cap_type';

DELETE
FROM powerplant_tables
WHERE table_name = 'ls_lease_cap_type';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5806, 0, 2017, 4, 0, 0, 51356, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051356_lessee_01_remove_cap_type_table_maint_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;