/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029435_projects.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   04/16/2013 Chris Mardis
||============================================================================
*/

update WO_VALIDATION_TYPE
   set FIND_COMPANY = 'select decode(<arg4>,2,(select company_id from budget where budget_id = <arg1>),(select company_id from work_order_control where work_order_id = <arg1>)) from dual'
 where WO_VALIDATION_TYPE_ID = 37;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (350, 0, 10, 4, 0, 0, 29435, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029435_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;