SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007861_projects_workflow_conversion_wo_fp_approvals.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   09/24/2012 Chris Mardis   Point Release
||============================================================================
*/

/*
--
-- Only run this script if the system control "Workflow User Object" does not exist or lower(trim(control_value)) = "no"
-- WARNING: this script may take a LONG time to run
--
*/

/*
-- If the following PL/SQL block raises an Error then the script may need to be altered before running.  Check with Chris Mardis for
-- further information on customization of this script.
--
-- After customization set "L_SKIP  boolean := TRUE;" in the PL/SQL block below.
*/

declare
   L_SKIP          boolean := false;
   L_COUNT         number := 0;
   L_CONTROL_VALUE varchar2(30);
begin
   if not L_SKIP then
      select LOWER(CONTROL_VALUE)
        into L_CONTROL_VALUE
        from PP_SYSTEM_CONTROL
       where LOWER(CONTROL_NAME) = 'workflow user object';

      if L_CONTROL_VALUE = 'no' then
       select count(*) into L_COUNT from APPROVAL where NVL(IS_PROPTAX, 0) = 0; /*CDM - this sql was wrong. Would give a false negative.*/
         if L_COUNT > 0 then
            RAISE_APPLICATION_ERROR(-20000,
                                    'This script may need to be altered before running.  Please read comments in script.');
         else
            update PP_SYSTEM_CONTROL
               set CONTROL_VALUE = 'yes'
             where LOWER(CONTROL_NAME) = 'workflow user object';
            commit;
            DBMS_OUTPUT.PUT_LINE('System control updated to yes.');
         end if;
      else
         DBMS_OUTPUT.PUT_LINE('System control was already set to yes.');
      end if;
   end if;
end;
/


-- 01142014 - Moved comment block down to Conversion for approval Types
insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select *
     from (select max(CONTROL_ID) + 1 CONTROL_ID,
                  'Workflow User Object' CONTROL_NAME,
                  'yes' CONTROL_VALUE,
                  'dw_yes_no;1',
                  '"Yes" will activate the Approval Workflow User Object; "No" forces the existing Approvals to be used',
                  -1
             from PP_SYSTEM_CONTROL_COMPANY)
    where not exists (select 1
             from PP_SYSTEM_CONTROL_COMPANY
            where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object'));

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'yes'
 where LOWER(trim(CONTROL_NAME)) = LOWER('Workflow User Object');

update PP_MYPP_APPROVAL_CONTROL
   set DW = 'dw_fp_approval_workflow_count_mypp'
 where DW = 'dw_fp_approval_count_mypp';

update PP_MYPP_APPROVAL_CONTROL
   set DW = 'dw_wo_approval_workflow_count_mypp'
 where DW = 'dw_wo_approval_count_mypp';
/*
--
-- Conversion for approval types
--
-- CLEE - Added "NVL" and " * 100"
create table APPROVAL_WORKFLOW_TYPE_CONV as
select APPROVAL_TYPE_ID,
       (nvl((select max(WORKFLOW_TYPE_ID) from WORKFLOW_TYPE),0) + ROWNUM) * 100 WORKFLOW_TYPE_ID
  from (select APPROVAL_TYPE_ID from APPROVAL where NVL(IS_PROPTAX, 0) = 0 order by APPROVAL_TYPE_ID);

insert into WORKFLOW_TYPE
   (WORKFLOW_TYPE_ID, DESCRIPTION, SUBSYSTEM, ACTIVE, USE_LIMITS, SQL_APPROVAL_AMOUNT, BASE_SQL)
   select B.WORKFLOW_TYPE_ID APPROVAL_TYPE_ID,
          DESCRIPTION,
          'fp_approval,wo_approval' SUBSYSTEM,
          1 ACTIVE,
          1 USE_LIMITS,
          'select nvl(sum(appr_amount),0) appr_amount from (' || CHR(13) || CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_viewapp a, wo_est_expense_viewapp b, wo_est_removal_viewapp c, wo_est_jobbing_viewapp d, wo_est_credits_viewapp e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''fp_approval''' || CHR(13) || CHR(10) || 'union all' || CHR(13) ||
          CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_viewapp a, wo_est_expense_viewapp b, wo_est_removal_viewapp c, wo_est_jobbing_viewapp d, wo_est_credits_viewapp e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''wo_approval''' || CHR(13) || CHR(10) ||
          'and ''yes'' <> ( select lower(trim(control_value)) from pp_system_control_companies where company_id = <<co_id>> and lower(trim(control_name)) = lower(trim(''WOEST - Header from wo_estimate'')) )' ||
          CHR(13) || CHR(10) || 'union' || CHR(13) || CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount ' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_view2app a, wo_est_expense_view2app b, wo_est_removal_view2app c, wo_est_jobbing_view2app d, wo_est_credits_view2app e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''wo_approval''' || CHR(13) || CHR(10) ||
          'and ''yes'' = ( select lower(trim(control_value)) from pp_system_control_companies where company_id = <<co_id>> and lower(trim(control_name)) = lower(trim(''WOEST - Header from wo_estimate'')) )' ||
          CHR(13) || CHR(10) || ')' SQL_APPROVAL_AMOUNT,
          1 BASE_SQL
     from APPROVAL A, APPROVAL_WORKFLOW_TYPE_CONV B
    where A.APPROVAL_TYPE_ID = B.APPROVAL_TYPE_ID;

insert into WORKFLOW_RULE
   (WORKFLOW_RULE_ID, DESCRIPTION, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED, sql)
   select WT.WORKFLOW_TYPE_ID + A.RULE_ID WORKFLOW_RULE_ID,
          A.DESCRIPTION,
          A.AUTHORITY_LIMIT,
          A.NUM_APPROVERS,
          A.NUM_REQUIRED,
          A.SQL
     from WORKFLOW_TYPE WT,
          APPROVAL_WORKFLOW_TYPE_CONV CNV,
          (select APPROVAL_TYPE_ID,
                  1                RULE_ID,
                  AUTH_LEVEL1      DESCRIPTION,
                  DOLLAR_LIMIT1    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL1 is not null
           union all
           select APPROVAL_TYPE_ID,
                  2                RULE_ID,
                  AUTH_LEVEL2      DESCRIPTION,
                  DOLLAR_LIMIT2    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL2 is not null
           union all
           select APPROVAL_TYPE_ID,
                  3                RULE_ID,
                  AUTH_LEVEL3      DESCRIPTION,
                  DOLLAR_LIMIT3    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL3 is not null
           union all
           select APPROVAL_TYPE_ID,
                  4                RULE_ID,
                  AUTH_LEVEL4      DESCRIPTION,
                  DOLLAR_LIMIT4    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL4 is not null
           union all
           select APPROVAL_TYPE_ID,
                  5                RULE_ID,
                  AUTH_LEVEL5      DESCRIPTION,
                  DOLLAR_LIMIT5    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL5 is not null
           union all
           select APPROVAL_TYPE_ID,
                  6                RULE_ID,
                  AUTH_LEVEL6      DESCRIPTION,
                  DOLLAR_LIMIT6    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL6 is not null
           union all
           select APPROVAL_TYPE_ID,
                  7                RULE_ID,
                  AUTH_LEVEL7      DESCRIPTION,
                  DOLLAR_LIMIT7    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL7 is not null
           union all
           select APPROVAL_TYPE_ID,
                  8                RULE_ID,
                  AUTH_LEVEL8      DESCRIPTION,
                  DOLLAR_LIMIT8    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL8 is not null
           union all
           select APPROVAL_TYPE_ID,
                  9                RULE_ID,
                  AUTH_LEVEL9      DESCRIPTION,
                  DOLLAR_LIMIT9    AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL9 is not null
           union all
           select APPROVAL_TYPE_ID,
                  10               RULE_ID,
                  AUTH_LEVEL10     DESCRIPTION,
                  DOLLAR_LIMIT10   AUTHORITY_LIMIT,
                  null             NUM_APPROVERS,
                  1                NUM_REQUIRED,
                  null             sql
             from APPROVAL
            where AUTH_LEVEL10 is not null) A
    where A.APPROVAL_TYPE_ID = CNV.APPROVAL_TYPE_ID
      and CNV.WORKFLOW_TYPE_ID = WT.WORKFLOW_TYPE_ID
    order by 1;

insert into WORKFLOW_TYPE_RULE
   (WORKFLOW_TYPE_ID, WORKFLOW_RULE_ID, RULE_ORDER)
   select CNV.WORKFLOW_TYPE_ID, CNV.WORKFLOW_TYPE_ID + A.RULE_ORDER WORKFLOW_RULE_ID, RULE_ORDER
     from APPROVAL_WORKFLOW_TYPE_CONV CNV,
          (select APPROVAL_TYPE_ID, 1 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL1 is not null
           union all
           select APPROVAL_TYPE_ID, 2 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL2 is not null
           union all
           select APPROVAL_TYPE_ID, 3 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL3 is not null
           union all
           select APPROVAL_TYPE_ID, 4 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL4 is not null
           union all
           select APPROVAL_TYPE_ID, 5 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL5 is not null
           union all
           select APPROVAL_TYPE_ID, 6 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL6 is not null
           union all
           select APPROVAL_TYPE_ID, 7 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL7 is not null
           union all
           select APPROVAL_TYPE_ID, 8 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL8 is not null
           union all
           select APPROVAL_TYPE_ID, 9 RULE_ORDER
             from APPROVAL
            where AUTH_LEVEL9 is not null
           union all
           select APPROVAL_TYPE_ID, 10 RULE_ORDER from APPROVAL where AUTH_LEVEL10 is not null) A
    where A.APPROVAL_TYPE_ID = CNV.APPROVAL_TYPE_ID
    order by 2;
commit;

create table APPROVAL_AUTH_LEVEL_BAK as
   select * from APPROVAL_AUTH_LEVEL;

update APPROVAL_AUTH_LEVEL A
   set (A.APPROVAL_TYPE_ID, A.AUTH_LEVEL) =
        (select CNV.WORKFLOW_TYPE_ID, CNV.WORKFLOW_TYPE_ID + A.AUTH_LEVEL
           from APPROVAL_WORKFLOW_TYPE_CONV CNV
          where CNV.APPROVAL_TYPE_ID = A.APPROVAL_TYPE_ID)
where (A.APPROVAL_TYPE_ID) in (select  CNV.APPROVAL_TYPE_ID
           from APPROVAL_WORKFLOW_TYPE_CONV CNV
          where CNV.APPROVAL_TYPE_ID = A.APPROVAL_TYPE_ID);

commit;

--
-- Conversion for existing approvals
--
update WORK_ORDER_APPROVAL A
   set A.APPROVAL_TYPE_ID =
        (select CNV.WORKFLOW_TYPE_ID
           from APPROVAL_WORKFLOW_TYPE_CONV CNV
          where CNV.APPROVAL_TYPE_ID = A.APPROVAL_TYPE_ID)
 where APPROVAL_TYPE_ID in (select APPROVAL_TYPE_ID from APPROVAL);

update WORK_ORDER_TYPE A
   set A.APPROVAL_TYPE_ID =
        (select CNV.WORKFLOW_TYPE_ID
           from APPROVAL_WORKFLOW_TYPE_CONV CNV
          where CNV.APPROVAL_TYPE_ID = A.APPROVAL_TYPE_ID)
 where APPROVAL_TYPE_ID in (select APPROVAL_TYPE_ID from APPROVAL);

commit;

insert into WORKFLOW
   (WORKFLOW_ID, WORKFLOW_TYPE_ID, WORKFLOW_TYPE_DESC, SUBSYSTEM, ID_FIELD1, ID_FIELD2,
    APPROVAL_AMOUNT, APPROVAL_STATUS_ID, DATE_SENT, DATE_APPROVED, USE_LIMITS, SQL_APPROVAL_AMOUNT)
   select PWRPLANT1.NEXTVAL WORKFLOW_ID,
          WOA.APPROVAL_TYPE_ID WORKFLOW_TYPE_ID,
          A.DESCRIPTION WORKFLOW_TYPE_DESC,
          DECODE(WOC.FUNDING_WO_INDICATOR, 1, 'fp_approval', 'wo_approval') SUBSYSTEM,
          WOA.WORK_ORDER_ID I_ID_FIELD1,
          WOA.REVISION I_ID_FIELD2,
          (select CAPITAL + EXPENSE + REMOVAL + JOBBING + CREDITS
             from WO_EST_CAPITAL_VIEWAPP A,
                  WO_EST_EXPENSE_VIEWAPP B,
                  WO_EST_REMOVAL_VIEWAPP C,
                  WO_EST_JOBBING_VIEWAPP D,
                  WO_EST_CREDITS_VIEWAPP E
            where A.WORK_ORDER_ID = B.WORK_ORDER_ID
              and A.REVISION = B.REVISION
              and A.WORK_ORDER_ID = C.WORK_ORDER_ID
              and A.REVISION = C.REVISION
              and A.WORK_ORDER_ID = D.WORK_ORDER_ID
              and A.REVISION = D.REVISION
              and A.WORK_ORDER_ID = E.WORK_ORDER_ID
              and A.REVISION = E.REVISION
              and A.WORK_ORDER_ID = WOA.WORK_ORDER_ID
              and A.REVISION = WOA.REVISION) APPROVAL_AMOUNT,
          WOA.APPROVAL_STATUS_ID,
          DECODE(DECODE(WOA.APPROVAL_STATUS_ID, 1, 1, 6, 1, 7, 1, 2),
                 1,
                 TO_DATE(null),
                 LEAST(NVL(WOA.AUTHORIZED_DATE1, sysdate),
                       NVL(WOA.AUTHORIZED_DATE2, sysdate),
                       NVL(WOA.AUTHORIZED_DATE3, sysdate),
                       NVL(WOA.AUTHORIZED_DATE4, sysdate),
                       NVL(WOA.AUTHORIZED_DATE5, sysdate),
                       NVL(WOA.AUTHORIZED_DATE6, sysdate),
                       NVL(WOA.AUTHORIZED_DATE7, sysdate),
                       NVL(WOA.AUTHORIZED_DATE8, sysdate),
                       NVL(WOA.AUTHORIZED_DATE9, sysdate),
                       NVL(WOA.AUTHORIZED_DATE10, sysdate))) DATE_SENT,
          DECODE(DECODE(WOA.APPROVAL_STATUS_ID, 3, 3, 4, 3, 1),
                 1,
                 TO_DATE(null),
                 DECODE(GREATEST(NVL(WOA.AUTHORIZED_DATE1, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE2, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE3, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE4, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE5, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE6, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE7, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE8, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE9, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE10, sysdate - 100000)),
                        sysdate - 100000,
                        sysdate,
                        GREATEST(NVL(WOA.AUTHORIZED_DATE1, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE2, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE3, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE4, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE5, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE6, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE7, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE8, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE9, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE10, sysdate - 100000)))) DATE_APPROVED,
          1 USE_LIMITS,
          'select nvl(sum(appr_amount),0) appr_amount from (' || CHR(13) || CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_viewapp a, wo_est_expense_viewapp b, wo_est_removal_viewapp c, wo_est_jobbing_viewapp d, wo_est_credits_viewapp e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''fp_approval''' || CHR(13) || CHR(10) || 'union all' || CHR(13) ||
          CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_viewapp a, wo_est_expense_viewapp b, wo_est_removal_viewapp c, wo_est_jobbing_viewapp d, wo_est_credits_viewapp e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''wo_approval''' || CHR(13) || CHR(10) ||
          'and ''yes'' <> ( select lower(trim(control_value)) from pp_system_control_companies where company_id = <<co_id>> and lower(trim(control_name)) = lower(trim(''WOEST - Header from wo_estimate'')) )' ||
          CHR(13) || CHR(10) || 'union' || CHR(13) || CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount ' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_view2app a, wo_est_expense_view2app b, wo_est_removal_view2app c, wo_est_jobbing_view2app d, wo_est_credits_view2app e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''wo_approval''' || CHR(13) || CHR(10) ||
          'and ''yes'' = ( select lower(trim(control_value)) from pp_system_control_companies where company_id = <<co_id>> and lower(trim(control_name)) = lower(trim(''WOEST - Header from wo_estimate'')) )' ||
          CHR(13) || CHR(10) || ')' SQL_APPROVAL_AMOUNT
     from WORK_ORDER_CONTROL WOC, WORK_ORDER_APPROVAL WOA, WORKFLOW_TYPE A
    where WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
      and WOA.APPROVAL_TYPE_ID = A.WORKFLOW_TYPE_ID(+)
      and not exists
    (select 1
             from WORKFLOW W
            where W.ID_FIELD1 = TO_CHAR(WOA.WORK_ORDER_ID)
              and W.ID_FIELD2 = TO_CHAR(WOA.REVISION)
              and W.SUBSYSTEM in ('wo_approval', 'fp_approval'))
      and (WOC.FUNDING_WO_INDICATOR = 1 or
          (WOC.FUNDING_WO_INDICATOR = 0 and exists
           (select 1
               from PP_SYSTEM_CONTROL_COMPANIES PP
              where LOWER(trim(PP.CONTROL_NAME)) = LOWER(trim('WOEST - HEADER FROM WO_ESTIMATE'))
                and LOWER(trim(PP.CONTROL_VALUE)) <> 'yes'
                and PP.COMPANY_ID = WOC.COMPANY_ID)));

insert into WORKFLOW
   (WORKFLOW_ID, WORKFLOW_TYPE_ID, WORKFLOW_TYPE_DESC, SUBSYSTEM, ID_FIELD1, ID_FIELD2,
    APPROVAL_AMOUNT, APPROVAL_STATUS_ID, DATE_SENT, DATE_APPROVED, USE_LIMITS, SQL_APPROVAL_AMOUNT)
   select PWRPLANT1.NEXTVAL WORKFLOW_ID,
          WOA.APPROVAL_TYPE_ID WORKFLOW_TYPE_ID,
          A.DESCRIPTION WORKFLOW_TYPE_DESC,
          DECODE(WOC.FUNDING_WO_INDICATOR, 1, 'fp_approval', 'wo_approval') SUBSYSTEM,
          WOA.WORK_ORDER_ID I_ID_FIELD1,
          WOA.REVISION I_ID_FIELD2,
          (select CAPITAL + EXPENSE + REMOVAL + JOBBING + CREDITS
             from WO_EST_CAPITAL_VIEW2APP A,
                  WO_EST_EXPENSE_VIEW2APP B,
                  WO_EST_REMOVAL_VIEW2APP C,
                  WO_EST_JOBBING_VIEW2APP D,
                  WO_EST_CREDITS_VIEW2APP E
            where A.WORK_ORDER_ID = B.WORK_ORDER_ID
              and A.REVISION = B.REVISION
              and A.WORK_ORDER_ID = C.WORK_ORDER_ID
              and A.REVISION = C.REVISION
              and A.WORK_ORDER_ID = D.WORK_ORDER_ID
              and A.REVISION = D.REVISION
              and A.WORK_ORDER_ID = E.WORK_ORDER_ID
              and A.REVISION = E.REVISION
              and A.WORK_ORDER_ID = WOA.WORK_ORDER_ID
              and A.REVISION = WOA.REVISION) APPROVAL_AMOUNT,
          WOA.APPROVAL_STATUS_ID,
          DECODE(DECODE(WOA.APPROVAL_STATUS_ID, 1, 1, 6, 1, 7, 1, 2),
                 1,
                 TO_DATE(null),
                 LEAST(NVL(WOA.AUTHORIZED_DATE1, sysdate),
                       NVL(WOA.AUTHORIZED_DATE2, sysdate),
                       NVL(WOA.AUTHORIZED_DATE3, sysdate),
                       NVL(WOA.AUTHORIZED_DATE4, sysdate),
                       NVL(WOA.AUTHORIZED_DATE5, sysdate),
                       NVL(WOA.AUTHORIZED_DATE6, sysdate),
                       NVL(WOA.AUTHORIZED_DATE7, sysdate),
                       NVL(WOA.AUTHORIZED_DATE8, sysdate),
                       NVL(WOA.AUTHORIZED_DATE9, sysdate),
                       NVL(WOA.AUTHORIZED_DATE10, sysdate))) DATE_SENT,
          DECODE(DECODE(WOA.APPROVAL_STATUS_ID, 3, 3, 4, 3, 1),
                 1,
                 TO_DATE(null),
                 DECODE(GREATEST(NVL(WOA.AUTHORIZED_DATE1, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE2, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE3, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE4, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE5, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE6, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE7, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE8, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE9, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE10, sysdate - 100000)),
                        sysdate - 100000,
                        sysdate,
                        GREATEST(NVL(WOA.AUTHORIZED_DATE1, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE2, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE3, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE4, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE5, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE6, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE7, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE8, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE9, sysdate - 100000),
                                 NVL(WOA.AUTHORIZED_DATE10, sysdate - 100000)))) DATE_APPROVED,
          1 USE_LIMITS,
          'select nvl(sum(appr_amount),0) appr_amount from (' || CHR(13) || CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_viewapp a, wo_est_expense_viewapp b, wo_est_removal_viewapp c, wo_est_jobbing_viewapp d, wo_est_credits_viewapp e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''fp_approval''' || CHR(13) || CHR(10) || 'union all' || CHR(13) ||
          CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_viewapp a, wo_est_expense_viewapp b, wo_est_removal_viewapp c, wo_est_jobbing_viewapp d, wo_est_credits_viewapp e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''wo_approval''' || CHR(13) || CHR(10) ||
          'and ''yes'' <> ( select lower(trim(control_value)) from pp_system_control_companies where company_id = <<co_id>> and lower(trim(control_name)) = lower(trim(''WOEST - Header from wo_estimate'')) )' ||
          CHR(13) || CHR(10) || 'union' || CHR(13) || CHR(10) ||
          'select nvl(sum(capital + expense + removal + jobbing + credits),0) appr_amount ' ||
          CHR(13) || CHR(10) ||
          'from wo_est_capital_view2app a, wo_est_expense_view2app b, wo_est_removal_view2app c, wo_est_jobbing_view2app d, wo_est_credits_view2app e' ||
          CHR(13) || CHR(10) || 'where a.work_order_id = <<id_field1>>' || CHR(13) || CHR(10) ||
          'and a.revision = <<id_field2>>' || CHR(13) || CHR(10) ||
          'and a.work_order_id = b.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = b.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = c.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = c.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = d.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = d.revision' || CHR(13) || CHR(10) ||
          'and a.work_order_id = e.work_order_id' || CHR(13) || CHR(10) ||
          'and a.revision = e.revision' || CHR(13) || CHR(10) ||
          'and ''<<subsystem>>'' = ''wo_approval''' || CHR(13) || CHR(10) ||
          'and ''yes'' = ( select lower(trim(control_value)) from pp_system_control_companies where company_id = <<co_id>> and lower(trim(control_name)) = lower(trim(''WOEST - Header from wo_estimate'')) )' ||
          CHR(13) || CHR(10) || ')' SQL_APPROVAL_AMOUNT
     from WORK_ORDER_CONTROL WOC, WORK_ORDER_APPROVAL WOA, APPROVAL A
    where WOC.WORK_ORDER_ID = WOA.WORK_ORDER_ID
      and WOA.APPROVAL_TYPE_ID = A.APPROVAL_TYPE_ID(+)
      and not exists (select 1
             from WORKFLOW W
            where W.ID_FIELD1 = TO_CHAR(WOA.WORK_ORDER_ID)
              and W.ID_FIELD2 = TO_CHAR(WOA.REVISION)
              and W.SUBSYSTEM in ('wo_approval', 'fp_approval'))
      and WOC.FUNDING_WO_INDICATOR = 0
      and exists
    (select 1
             from PP_SYSTEM_CONTROL_COMPANIES PP
            where LOWER(trim(PP.CONTROL_NAME)) = LOWER(trim('WOEST - HEADER FROM WO_ESTIMATE'))
              and LOWER(trim(PP.CONTROL_VALUE)) = 'yes'
              and PP.COMPANY_ID = WOC.COMPANY_ID);

commit;

declare
   I number(22, 0);
begin
   I := 1;
   for I in 1 .. 10
   loop
      execute immediate 'insert into workflow_detail (workflow_id, ' ||
                        '  id, ' ||
                        '  workflow_rule_id, ' ||
                        '  workflow_rule_desc, ' ||
                        '  rule_order, ' ||
                        '  users, ' ||
                        '  user_approved, ' ||
                        '  date_approved, ' ||
                        '  approval_status_id, ' ||
                        '  required, authority_limit, num_approvers, num_required ) ' ||
                        'select w.workflow_id, ' ||
                        '  nvl((select max(id) from workflow_detail z where z.workflow_id = w.workflow_id),0) + rank() over(partition by w.workflow_id order by rownum) id, ' ||
                        '  a.workflow_rule_id, ' ||
                        '  a.description workflow_rule_desc, ' ||
                        '  '||to_char(i)||' rule_order, ' ||
                        '  woa.authorizer'||to_char(i)||' users, ' ||
                        '  decode(woa.authorized_date'||to_char(i)||',null,null,woa.authorizer'||to_char(i)||') user_approved, ' ||
                        '  woa.authorized_date'||to_char(i)||' date_approved, ' ||
                        '  null approval_status_id, ' ||
                        '  1 required, ' ||
                        '  a.authority_limit, ' ||
                        '  null num_approvers, ' ||
                        '  1 num_required ' ||
                        'from workflow w, ' ||
                        '  workflow_rule a, ' ||
                        '  work_order_approval woa ' ||
                        'where w.workflow_type_id + '||to_char(i)||' = a.workflow_rule_id ' ||
                        'and w.id_field1 = to_char(woa.work_order_id) ' ||
                        'and w.id_field2 = to_char(woa.revision) ' ||
                        'and w.subsystem in (''fp_approval'',''wo_approval'') ' ||
                        'and nvl(woa.authorizer'||to_char(i)||',''*'') <> ''Multiple'' ';
   end loop;
   commit;
end;
/

declare
   I number(22, 0);
begin
   I := 1;
   for I in 1 .. 10
   loop
      execute immediate 'insert into workflow_detail ( ' ||
                        '  workflow_id, id, workflow_rule_id, workflow_rule_desc, rule_order, ' ||
                        '  users, date_approved, user_approved, approval_status_id, required, ' ||
                        '  notes, authority_limit, num_approvers, num_required, sql ) ' ||
                        'select w.workflow_id, ' ||
                        '  nvl((select max(id) from workflow_detail z where z.workflow_id = w.workflow_id),0) + rank() over(partition by w.workflow_id order by rownum) id, ' ||
                        '  w.workflow_type_id + '||to_char(i)||' workflow_rule_id, ' ||
                        '  woam.auth_level workflow_rule_desc, ' ||
                        '  '||to_char(i)||' rule_order, ' ||
                        '  woam.users users, ' ||
                        '  woam.approval_date date_approved, ' ||
                        '  decode(woam.approval_date,null,null,woam.users) user_approved, ' ||
                        '  null approval_status_id, ' ||
                        '  woam.required required, ' ||
                        '  null notes, ' ||
                        '  a.dollar_limit'||to_char(i)||' authority_limit, ' ||
                        '  null num_approvers, ' ||
                        '  1 num_required, ' ||
                        '  null sql ' ||
                        'from workflow w, ' ||
                        '  approval a, ' ||
                        '  work_order_approval woa, ' ||
                        '  wo_approval_multiple woam, ' ||
                        '  approval_workflow_type_conv cnv ' ||
                        'where w.workflow_type_id = cnv.workflow_type_id ' ||
                        'and cnv.approval_type_id = a.approval_type_id ' ||
                        'and w.id_field1 = to_char(woa.work_order_id) ' ||
                        'and w.id_field2 = to_char(woa.revision) ' ||
                        'and w.subsystem in (''fp_approval'',''wo_approval'') ' ||
                        'and woa.authorizer'||to_char(i)||' = ''Multiple'' ' ||
                        'and a.auth_level'||to_char(i)||' is not null ' ||
                        'and woa.work_order_id = woam.work_order_id ' ||
                        'and woa.revision = woam.revision ' ||
                        'and woam.auth_level_id = '||to_char(i)||' ' ||
                        'and woam.approval_type_id = a.approval_type_id ';
   end loop;
   commit;
end;
/

--
-- INITIATED
--
update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 1
 where exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 1
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);
commit;

--
-- PENDING APPROVAL
--
update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 3
 where DATE_APPROVED is not null
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 2
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 2
 where DATE_APPROVED is null
   and RULE_ORDER = (select min(A.RULE_ORDER)
                       from WORKFLOW_DETAIL A
                      where A.WORKFLOW_ID = WD.WORKFLOW_ID
                        and A.DATE_APPROVED is null)
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 2
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 1
 where APPROVAL_STATUS_ID is null
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 2
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

commit;

--
-- APPROVED
--
update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 3
 where exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 3
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);
commit;

--
-- REJECTED
--
update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 4
 where DATE_APPROVED = (select max(A.DATE_APPROVED)
                          from WORKFLOW_DETAIL A
                         where A.WORKFLOW_ID = WD.WORKFLOW_ID
                           and A.DATE_APPROVED is not null)
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 4
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 3
 where DATE_APPROVED is not null
   and APPROVAL_STATUS_ID is null
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 4
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 5
 where APPROVAL_STATUS_ID is null
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 4
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

commit;

--
-- INACTIVE
--
update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 3
 where DATE_APPROVED is not null
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 5
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 2
 where DATE_APPROVED is null
   and RULE_ORDER = (select min(A.RULE_ORDER)
                       from WORKFLOW_DETAIL A
                      where A.WORKFLOW_ID = WD.WORKFLOW_ID
                        and A.DATE_APPROVED is null)
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 5
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 1
 where APPROVAL_STATUS_ID is null
   and exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 5
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);

commit;

--
-- AUTO-APPROVED
--

--*
--* No SQL needed as no levels exist for auto-approved revisions
--*

--
-- UN-REJECTED
--
update WORKFLOW_DETAIL WD
   set APPROVAL_STATUS_ID = 1
 where exists (select 1
          from WORKFLOW W
         where W.APPROVAL_STATUS_ID = 7
           and W.WORKFLOW_ID = WD.WORKFLOW_ID);
commit;
*/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (235, 0, 10, 4, 0, 0, 7861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_007861_projects_workflow_conversion_wo_fp_approvals.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

