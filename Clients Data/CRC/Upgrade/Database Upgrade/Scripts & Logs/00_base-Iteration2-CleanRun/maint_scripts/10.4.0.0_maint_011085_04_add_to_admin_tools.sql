/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_04_add_to_admin_tools.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/07/2012 Colin Lee      Point Release
||============================================================================
*/

insert into PP_CONVERSION_WINDOWS
   (WINDOW_ID, DESCRIPTION, LONG_DESCRIPTION, WINDOW_NAME, TIME_STAMP, USER_ID)
   select max(WINDOW_ID) + 1,
          'Depreciation History Loader',
          'Depreciation History Loader for Group and Individually depreciation methods',
          'w_depr_loader',
          sysdate,
          user
     from PP_CONVERSION_WINDOWS;

insert into PP_CONVERSION_WINDOWS
   (WINDOW_ID, DESCRIPTION, LONG_DESCRIPTION, WINDOW_NAME, TIME_STAMP, USER_ID)
   select max(WINDOW_ID) + 1,
          'Depreciation Rates Loader',
          'Depreciation Rates Loader for Group and Individually depreciation methods',
          'w_depr_rates_loader',
          sysdate,
          user
     from PP_CONVERSION_WINDOWS;

insert into PP_CONVERSION_WINDOWS
   (WINDOW_ID, DESCRIPTION, LONG_DESCRIPTION, WINDOW_NAME, TIME_STAMP, USER_ID)
   select max(WINDOW_ID) + 1,
          'CPR Activities and Class Cd Loader',
          'CPR loader for Asset, Activities and Class Code',
          'w_cpr_loader',
          sysdate,
          user
     from PP_CONVERSION_WINDOWS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (260, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_04_add_to_admin_tools.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
