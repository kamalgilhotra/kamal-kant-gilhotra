/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_044223_reg_add_layer_group_2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/23/2015 Anand R        Reg - update restrictions for layer group for reports
||============================================================================
*/

update pp_dynamic_filter_restrictions
set sqls_column_expression = 'reg_incremental_group.incremental_adj_type_id'
where restriction_id = 77;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2709, 0, 2015, 2, 0, 0, 044223, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044223_reg_add_layer_group_dml_2.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;