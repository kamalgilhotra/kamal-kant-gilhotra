SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035432_pwrtax_mlp_combine_3_arc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 11/11/2013 Andrew Scott        Combining powertax tables:
||                                         tax_control, tax_depreciation, tax_depr_adjust
||                                         into tax_depreciation.  This is needed for base
||                                         changes to accomodate MLP needs.
||============================================================================
*/


------
------SYNTAX OF THE THREE ORIGINAL TABLES, FROM MASTERDB:
------
------     CREATE TABLE ARC_TAX_CONTROL (TAX_BOOK_ID NUMBER(22,0) NOT NULL, TAX_RECORD_ID NUMBER(22,0) NOT NULL, TIME_STAMP DATE, USER_ID VARCHAR2(18), TYPE_OF_PROPERTY_ID NUMBER(22,0), TAX_LAW_ID NUMBER(22,0), CONVENTION_ID NUMBER(22,0), EXTRAORDINARY_CONVENTION NUMBER(22,0), TAX_RATE_ID NUMBER(22,0) NOT NULL, REMAINING_LIFE_INDICATOR NUMBER(22,0), TAX_LIMIT_ID NUMBER(22,0), SUMMARY_4562_ID NUMBER(22,0), LISTED_PROPERTY_IND NUMBER(22,0), RECOVERY_PERIOD_ID NUMBER(22,0), TAX_CREDIT_ID NUMBER(22,0), VERSION_ID NUMBER(22,0), DEFERRED_TAX_SCHEMA_ID NUMBER(22,0) DEFAULT 0) ;
------
------     CREATE TABLE ARC_TAX_DEPRECIATION (TAX_BOOK_ID NUMBER(22,0) NOT NULL, TAX_YEAR NUMBER(24,2) NOT NULL, TAX_RECORD_ID NUMBER(22,0) NOT NULL, TIME_STAMP DATE, USER_ID VARCHAR2(18), BOOK_BALANCE NUMBER(22,2), TAX_BALANCE NUMBER(22,2), REMAINING_LIFE NUMBER(22,8), ACCUM_RESERVE NUMBER(22,2), SL_RESERVE NUMBER(22,2), DEPRECIABLE_BASE NUMBER(22,2), FIXED_DEPRECIABLE_BASE NUMBER(22,2), ACTUAL_SALVAGE NUMBER(22,2), ESTIMATED_SALVAGE NUMBER(22,2), ACCUM_SALVAGE NUMBER(22,2), ADDITIONS NUMBER(22,2), TRANSFERS NUMBER(22,2), ADJUSTMENTS NUMBER(22,2), RETIREMENTS NUMBER(22,2), EXTRAORDINARY_RETIRES NUMBER(22,2), ACCUM_ORDINARY_RETIRES NUMBER(22,2), DEPRECIATION NUMBER(22,2), COST_OF_REMOVAL NUMBER(22,2), COR_EXPENSE NUMBER(22,2), GAIN_LOSS NUMBER(22,2), CAPITAL_GAIN_LOSS NUMBER(22,2), EST_SALVAGE_PCT NUMBER(22,8), BOOK_BALANCE_END NUMBER(22,2), TAX_BALANCE_END NUMBER(22,2), ACCUM_RESERVE_END NUMBER(22,2), ACCUM_SALVAGE_END NUMBER(22,2), ACCUM_ORDIN_RETIRES_END NUMBER(22,2), SL_RESERVE_END NUMBER(22,2), RETIRE_INVOL_CONV NUMBER(22,2), SALVAGE_INVOL_CONV NUMBER(22,2), SALVAGE_EXTRAORD NUMBER(22,2), CALC_DEPRECIATION NUMBER(22,2), OVER_ADJ_DEPRECIATION NUMBER(22,2), RETIRE_RES_IMPACT NUMBER(22,2), TRANSFER_RES_IMPACT NUMBER(22,2), SALVAGE_RES_IMPACT NUMBER(22,2), COR_RES_IMPACT NUMBER(22,2), ADJUSTED_RETIRE_BASIS NUMBER(22,2), RESERVE_AT_SWITCH NUMBER(22,2), QUANTITY NUMBER(22,0), CAPITALIZED_DEPR NUMBER(22,8), RESERVE_AT_SWITCH_END NUMBER(22,2), NUMBER_MONTHS_BEG NUMBER(22,2), NUMBER_MONTHS_END NUMBER(22,2), EX_RETIRE_RES_IMPACT NUMBER(22,2), EX_GAIN_LOSS NUMBER(22,2), QUANTITY_END NUMBER(22,2), ESTIMATED_SALVAGE_END NUMBER(22,2), JOB_CREATION_AMOUNT NUMBER(22,2), VERSION_ID NUMBER(22,0)) ;
------
------     CREATE TABLE ARC_TAX_DEPR_ADJUST (TAX_YEAR NUMBER(24,2) NOT NULL, TAX_BOOK_ID NUMBER(22,0) NOT NULL, TAX_RECORD_ID NUMBER(22,0) NOT NULL, TIME_STAMP DATE, USER_ID VARCHAR2(18), BOOK_BALANCE_ADJUST NUMBER(22,2), ACCUM_RESERVE_ADJUST NUMBER(22,2), DEPRECIABLE_BASE_ADJUST NUMBER(22,2), DEPRECIATION_ADJUST NUMBER(22,2), GAIN_LOSS_ADJUST NUMBER(22,2), CAP_GAIN_LOSS_ADJUST NUMBER(22,2), BOOK_BALANCE_ADJUST_METHOD NUMBER(22,0), ACCUM_RESERVE_ADJUST_METHOD NUMBER(22,0), DEPRECIABLE_BASE_ADJUST_METHOD NUMBER(22,0), DEPRECIATION_ADJUST_METHOD NUMBER(22,0), VERSION_ID NUMBER(22,0)) ;
------


-- Code block to throw errors and keep from dropping tables if they were not backed up correctly.
begin
   ----
   ----  Backup the existing three tables - Table prefixed with D1_ so they can be identified in the future for deletion and will be removed
   ----  from the base before releasing version so they don't show up as new base tables.
   ----
   execute immediate 'create table D1_ARC_TAX_CONTROL_V1042        as select * from ARC_TAX_CONTROL';
   DBMS_OUTPUT.PUT_LINE('Table D1_ARC_TAX_CONTROL_V1042 created.');
   execute immediate 'create table D1_ARC_TAX_DEPR_V1042   as select * from ARC_TAX_DEPRECIATION';
   DBMS_OUTPUT.PUT_LINE('Table D1_ARC_TAX_DEPR_V1042 created.');
   execute immediate 'create table D1_ARC_TAX_DEPR_ADJUST_V1042    as select * from ARC_TAX_DEPR_ADJUST';
   DBMS_OUTPUT.PUT_LINE('Table D1_ARC_TAX_DEPR_ADJUST_V1042 created.');

   ----
   ---- Add primary keys to the backup tables and analyze them (this will help when inserting data from the backup into the new table).
   ----
   execute immediate 'alter table D1_ARC_TAX_CONTROL_V1042                ' ||
                     '   add constraint PKD1_ARC_TAX_CONTROL_V1042          ' ||
                     '       primary key (TAX_BOOK_ID, TAX_RECORD_ID, VERSION_ID) ' ||
                     '       using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Primary key created on table D1_ARC_TAX_CONTROL_V1042.');

   execute immediate 'alter table D1_ARC_TAX_DEPR_V1042                     ' ||
                     '   add constraint PKD1_ARC_TAX_DEPR_V1042            ' ||
                     '       primary key (TAX_BOOK_ID, TAX_YEAR, TAX_RECORD_ID, VERSION_ID) ' ||
                     '       using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Primary key created on table D1_ARC_TAX_DEPR_V1042.');

   execute immediate 'alter table D1_ARC_TAX_DEPR_ADJUST_V1042                      ' ||
                     '   add constraint PKD1_ARC_TAX_DEPR_ADJ_V1042             ' ||
                     '       primary key (TAX_BOOK_ID, TAX_YEAR, TAX_RECORD_ID, VERSION_ID) ' ||
                     '       using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Primary key created on table D1_ARC_TAX_DEPR_ADJUST_V1042.');

   ----
   ----  Drop the original tables
   ----

   begin
      execute immediate 'drop public synonym ARC_TAX_DEPR_ADJUST';
      execute immediate 'drop public synonym ARC_TAX_DEPRECIATION';
      execute immediate 'drop public synonym ARC_TAX_CONTROL';
   exception
      when others then
         null; --Ignore exception if public synonyms can not be dropped.
   end;

   execute immediate 'drop table ARC_TAX_DEPR_ADJUST cascade constraints';
   DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_DEPR_ADJUST dropped.');
   execute immediate 'drop table ARC_TAX_DEPRECIATION cascade constraints';
   DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_DEPRECIATION dropped.');
   execute immediate 'drop table ARC_TAX_CONTROL cascade constraints';
   DBMS_OUTPUT.PUT_LINE('Table ARC_TAX_CONTROL dropped.');
end;
/

---- Analyze the new tables
declare
   CODE number(22, 0);
begin
   CODE := ANALYZE_TABLE('D1_ARC_TAX_CONTROL_V1042');
   CODE := ANALYZE_TABLE('D1_ARC_TAX_DEPR_V1042');
   CODE := ANALYZE_TABLE('D1_ARC_TAX_DEPR_ADJUST_V1042');
end;
/

----
----  Create the new Tax_Depreciation tables, with all columns from the old tables
----
create table ARC_TAX_DEPRECIATION
(
 TAX_BOOK_ID                    number(22,0) not null,
 TAX_YEAR                       number(24,2) not null,
 TAX_RECORD_ID                  number(22,0) not null,
 TIME_STAMP                     date,
 USER_ID                        varchar2(18),
 TYPE_OF_PROPERTY_ID            number(22,0),
 TAX_LAW_ID                     number(22,0),
 CONVENTION_ID                  number(22,0),
 EXTRAORDINARY_CONVENTION       number(22,0),
 TAX_RATE_ID                    number(22,0) not null,
 REMAINING_LIFE_INDICATOR       number(22,0),
 TAX_LIMIT_ID                   number(22,0),
 SUMMARY_4562_ID                number(22,0),
 LISTED_PROPERTY_IND            number(22,0),
 RECOVERY_PERIOD_ID             number(22,0),
 TAX_CREDIT_ID                  number(22,0),
 DEFERRED_TAX_SCHEMA_ID         number(22,0) default 0,
 BOOK_BALANCE                   number(22,2),
 TAX_BALANCE                    number(22,2),
 REMAINING_LIFE                 number(22,8),
 ACCUM_RESERVE                  number(22,2),
 SL_RESERVE                     number(22,2),
 DEPRECIABLE_BASE               number(22,2),
 FIXED_DEPRECIABLE_BASE         number(22,2),
 ACTUAL_SALVAGE                 number(22,2),
 ESTIMATED_SALVAGE              number(22,2),
 ACCUM_SALVAGE                  number(22,2),
 ADDITIONS                      number(22,2),
 TRANSFERS                      number(22,2),
 ADJUSTMENTS                    number(22,2),
 RETIREMENTS                    number(22,2),
 EXTRAORDINARY_RETIRES          number(22,2),
 ACCUM_ORDINARY_RETIRES         number(22,2),
 DEPRECIATION                   number(22,2),
 COST_OF_REMOVAL                number(22,2),
 COR_EXPENSE                    number(22,2),
 GAIN_LOSS                      number(22,2),
 CAPITAL_GAIN_LOSS              number(22,2),
 EST_SALVAGE_PCT                number(22,8),
 BOOK_BALANCE_END               number(22,2),
 TAX_BALANCE_END                number(22,2),
 ACCUM_RESERVE_END              number(22,2),
 ACCUM_SALVAGE_END              number(22,2),
 ACCUM_ORDIN_RETIRES_END        number(22,2),
 SL_RESERVE_END                 number(22,2),
 RETIRE_INVOL_CONV              number(22,2),
 SALVAGE_INVOL_CONV             number(22,2),
 SALVAGE_EXTRAORD               number(22,2),
 CALC_DEPRECIATION              number(22,2),
 OVER_ADJ_DEPRECIATION          number(22,2),
 RETIRE_RES_IMPACT              number(22,2),
 TRANSFER_RES_IMPACT            number(22,2),
 SALVAGE_RES_IMPACT             number(22,2),
 COR_RES_IMPACT                 number(22,2),
 ADJUSTED_RETIRE_BASIS          number(22,2),
 RESERVE_AT_SWITCH              number(22,2),
 QUANTITY                       number(22,0),
 CAPITALIZED_DEPR               number(22,8),
 RESERVE_AT_SWITCH_END          number(22,2),
 NUMBER_MONTHS_BEG              number(22,2),
 NUMBER_MONTHS_END              number(22,2),
 EX_RETIRE_RES_IMPACT           number(22,2),
 EX_GAIN_LOSS                   number(22,2),
 QUANTITY_END                   number(22,2),
 ESTIMATED_SALVAGE_END          number(22,2),
 JOB_CREATION_AMOUNT            number(22,2),
 BOOK_BALANCE_ADJUST            number(22,2),
 ACCUM_RESERVE_ADJUST           number(22,2),
 DEPRECIABLE_BASE_ADJUST        number(22,2),
 DEPRECIATION_ADJUST            number(22,2),
 GAIN_LOSS_ADJUST               number(22,2),
 CAP_GAIN_LOSS_ADJUST           number(22,2),
 BOOK_BALANCE_ADJUST_METHOD     number(22,0),
 ACCUM_RESERVE_ADJUST_METHOD    number(22,0),
 DEPRECIABLE_BASE_ADJUST_METHOD number(22,0),
 DEPRECIATION_ADJUST_METHOD     number(22,0),
 VERSION_ID                     number(22,0)
);

---- Add Table and Column Comments
comment on table ARC_TAX_DEPRECIATION is
'(C) [09] The Arc Tax Depreciation table archives the data in the Tax Depreciation table.';

comment on column ARC_TAX_DEPRECIATION.TAX_BOOK_ID is 'System-assigned identifier for each set of tax books  a utility maintains.';
comment on column ARC_TAX_DEPRECIATION.TAX_YEAR is 'Calendar tax year associated with the tax record''s  depreciation calculation and stored balances.  The fraction is a sequential number for dealing with short tax years.';
comment on column ARC_TAX_DEPRECIATION.TAX_RECORD_ID is 'System-assigned key that identifies an individual  tax asset on the tax depreciation (and related) tables. Tax record id records the unique combination of version, tax class, vintage,  in service month (optional), and tax location (also optional).  Each tax asset record on the depreciation table is fully described  by the tax record id combined with the tax year and tax book id keys on the depreciation tables.';
comment on column ARC_TAX_DEPRECIATION.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column ARC_TAX_DEPRECIATION.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column ARC_TAX_DEPRECIATION.TYPE_OF_PROPERTY_ID is 'Optional System-assigned identifier to tax type of property or external system reference, such as for ''Corp Tax''.';
comment on column ARC_TAX_DEPRECIATION.TAX_LAW_ID is 'Contains the System-assigned identifiers  for the distinctions in historic tax laws that are needed for reporting and IRS form filings.';
comment on column ARC_TAX_DEPRECIATION.CONVENTION_ID is 'System-defined identifier for a set of  rules and methodologies regarding the calculation of depreciation  and the development of a depreciable base.';
comment on column ARC_TAX_DEPRECIATION.EXTRAORDINARY_CONVENTION is 'System-defined identifier for a set of  rules and methodologies regarding the calculation of depreciation  and the development of a depreciable base.';
comment on column ARC_TAX_DEPRECIATION.TAX_RATE_ID is 'System-defined identifier for  the unique set of tax depreciation rates, for all sets of tax books, which  will be needed to calculate tax depreciation.';
comment on column ARC_TAX_DEPRECIATION.REMAINING_LIFE_INDICATOR is 'A Yes/No indicator describing whether a remaining life calculation is desired.  The PowerTax logic will automatically calculate the remaining life rate  and maintain the remaining life itself on the tax depreciation table';
comment on column ARC_TAX_DEPRECIATION.TAX_LIMIT_ID is 'System-assigned identifier to a depreciation limit, such as for luxury autos".';
comment on column ARC_TAX_DEPRECIATION.SUMMARY_4562_ID is 'System-assigned identifier';
comment on column ARC_TAX_DEPRECIATION.LISTED_PROPERTY_IND is 'Indicator for listed property to show on the 4562 report. 0 or Null=No. 1 = Yes"';
comment on column ARC_TAX_DEPRECIATION.RECOVERY_PERIOD_ID is 'System-assigned identifier to a row on the "Summary 4562" table for summarization on the 4562 report.';
comment on column ARC_TAX_DEPRECIATION.TAX_CREDIT_ID is 'System-assigned identifier of a "tax limit."  The limits table can contain limits (e.g., luxury autos), credits, or bonus depreciation (e.g., 30% JCA 2002).';
comment on column ARC_TAX_DEPRECIATION.DEFERRED_TAX_SCHEMA_ID is 'System-assigned identifier of a particular deferred tax schema, that indicates which normalization schemas apply to this tax record.  This is on the Federal books only.';
comment on column ARC_TAX_DEPRECIATION.BOOK_BALANCE is 'The beginning book basis balance of this tax asset record in dollars.';
comment on column ARC_TAX_DEPRECIATION.TAX_BALANCE is 'The beginning tax basis balance of this tax asset record in dollars, not reduced for ordinary (ADR) retirements.';
comment on column ARC_TAX_DEPRECIATION.REMAINING_LIFE is 'If this tax asset record is to utilize a remaining life technique, this  data item will hold the calculated remaining life.';
comment on column ARC_TAX_DEPRECIATION.ACCUM_RESERVE is 'The beginning accumulated tax depreciation reserve balance of this tax asset record.';
comment on column ARC_TAX_DEPRECIATION.SL_RESERVE is 'For "remaining life plan" rates this is the beginning tax basis minus the beginning straight line reserve, i.e., the un-recovered straight line reserve in dollars.';
comment on column ARC_TAX_DEPRECIATION.DEPRECIABLE_BASE is 'The calculated depreciable base to which the tax depreciation rates are  applied in order to calculate tax depreciation.';
comment on column ARC_TAX_DEPRECIATION.FIXED_DEPRECIABLE_BASE is 'The locked-in depreciable base to which the tax depreciation rates are  applied in order to calculate tax depreciation, which ignores retirements  and other adjustments.';
comment on column ARC_TAX_DEPRECIATION.ACTUAL_SALVAGE is 'Current year actual salvage proceeds associated with ordinary (normal/retirements) in dollars.';
comment on column ARC_TAX_DEPRECIATION.ESTIMATED_SALVAGE is '(Currently not used)';
comment on column ARC_TAX_DEPRECIATION.ACCUM_SALVAGE is 'Accumulated salvage proceeds included in the beginning depreciation reserve in dollars.';
comment on column ARC_TAX_DEPRECIATION.ADDITIONS is 'Current year tax basis additions in dollars.';
comment on column ARC_TAX_DEPRECIATION.TRANSFERS is 'Current year net tax basis transfers in dollars.';
comment on column ARC_TAX_DEPRECIATION.ADJUSTMENTS is 'Current year net tax basis adjustments in dollars.';
comment on column ARC_TAX_DEPRECIATION.RETIREMENTS is 'Current year tax basis, normal or ordinary, retirement or dispositions in dollars.';
comment on column ARC_TAX_DEPRECIATION.EXTRAORDINARY_RETIRES is 'Current year tax basis, extraordinary or abnormal, retirements or dispositions in dollars.';
comment on column ARC_TAX_DEPRECIATION.ACCUM_ORDINARY_RETIRES is 'Accumulated retirements not reducing the depreciable base at the beginning of the period in dollars.';
comment on column ARC_TAX_DEPRECIATION.DEPRECIATION is 'This is the sum of calc_depreciation, over_adj_depreciation, and any input depreciation adjustment (depreciation_adjust).';
comment on column ARC_TAX_DEPRECIATION.COST_OF_REMOVAL is 'Current year cost of removal dollars, whether directly expensed, in gain/loss, or applied to the reserve.';
comment on column ARC_TAX_DEPRECIATION.COR_EXPENSE is 'Current year cost of removal dollars that are directly expensed.';
comment on column ARC_TAX_DEPRECIATION.GAIN_LOSS is 'Current year (ordinary and capital) gain (+) or loss (-) in dollars including the (extraordinary) gain/loss.';
comment on column ARC_TAX_DEPRECIATION.CAPITAL_GAIN_LOSS is 'Current year capital gain (+) or loss (-) in dollars (ordinary and extraordinary).';
comment on column ARC_TAX_DEPRECIATION.EST_SALVAGE_PCT is 'Estimated salvage percent entered as a decimal.  (It is after reduction for the ADR exclusion).';
comment on column ARC_TAX_DEPRECIATION.BOOK_BALANCE_END is 'The ending book basis balance of this tax asset record in dollars.';
comment on column ARC_TAX_DEPRECIATION.TAX_BALANCE_END is 'The ending tax basis balance of this tax asset record in dollars, not reduced for ordinary (ADR) retirements.';
comment on column ARC_TAX_DEPRECIATION.ACCUM_RESERVE_END is 'The ending accumulated tax depreciation reserve balance in dollars.';
comment on column ARC_TAX_DEPRECIATION.ACCUM_SALVAGE_END is 'Accumulated salvage proceeds included in the ending depreciation reserve in dollars.  Current period allowed tax depreciation after adjustment but before reduction for the amount capitalized if any.';
comment on column ARC_TAX_DEPRECIATION.ACCUM_ORDIN_RETIRES_END is 'Accumulated retirements not reducing the depreciable base at the end of the period in dollars.';
comment on column ARC_TAX_DEPRECIATION.SL_RESERVE_END is 'For "remaining life plan" rates, this is the ending tax basis minus the ending straight line reserve, i.e., the ending un-recovered straight line reserve in dollars.';
comment on column ARC_TAX_DEPRECIATION.RETIRE_INVOL_CONV is 'Currently not used.';
comment on column ARC_TAX_DEPRECIATION.SALVAGE_INVOL_CONV is 'Currently not used.';
comment on column ARC_TAX_DEPRECIATION.SALVAGE_EXTRAORD is 'Current salvage associated with extraordinary retirement in dollars.';
comment on column ARC_TAX_DEPRECIATION.CALC_DEPRECIATION is 'Calculated current period depreciation in dollars before over depreciation and other adjustments.';
comment on column ARC_TAX_DEPRECIATION.OVER_ADJ_DEPRECIATION is 'Current depreciation adjustments in dollars calculated because of reserve limits.';
comment on column ARC_TAX_DEPRECIATION.RETIRE_RES_IMPACT is 'Current impact of ordinary retirements on the reserve for depreciation in dollars.';
comment on column ARC_TAX_DEPRECIATION.TRANSFER_RES_IMPACT is 'Current impact of all transfers on the reserve for depreciation in dollars.';
comment on column ARC_TAX_DEPRECIATION.SALVAGE_RES_IMPACT is 'Current impact of all salvage on the reserve for depreciation in dollars.';
comment on column ARC_TAX_DEPRECIATION.COR_RES_IMPACT is 'Current impact of all cost of removal on the reserve for depreciation in dollars.';
comment on column ARC_TAX_DEPRECIATION.ADJUSTED_RETIRE_BASIS is '(Not currently used)';
comment on column ARC_TAX_DEPRECIATION.RESERVE_AT_SWITCH is 'Reserve at switch is the reserve in dollars at the point of switch from a net method to a gross method.  It is subsequently adjusted for retirements impacting the depreciable base (e.g., extraordinary retirements).  This variable is the beginning of the year value.';
comment on column ARC_TAX_DEPRECIATION.QUANTITY is 'Quantity is the quantity of, for example, luxury auto.  This is only used when applying limit processing.';
comment on column ARC_TAX_DEPRECIATION.CAPITALIZED_DEPR is 'Input decimal rate for capitalizing a percentage of the depreciation calculated (for example, transportation equipment).';
comment on column ARC_TAX_DEPRECIATION.RESERVE_AT_SWITCH_END is 'End of the year reserve at switch.';
comment on column ARC_TAX_DEPRECIATION.NUMBER_MONTHS_BEG is 'This is the cumulative number of months in the tax years up to the current tax year.  (Note that no ''half year'' convention is used in the first year, e.g. it is ''12'' where the vintage equals the tax year).  This is needed to calculate the impacts of short tax years.';
comment on column ARC_TAX_DEPRECIATION.NUMBER_MONTHS_END is 'This is the cumulative number of months in the tax years including the current tax year.  )Note that no ''half year'' convention is used in the first year).  This is needed to calculate the impacts of short tax years.';
comment on column ARC_TAX_DEPRECIATION.EX_RETIRE_RES_IMPACT is 'Current impact of extraordinary retirements on the reserve for depreciation in dollars.';
comment on column ARC_TAX_DEPRECIATION.EX_GAIN_LOSS is 'Current year extraordinary gain (+) loss (-) in dollars, i.e. gain/loss related to an extraordinary retirement.';
comment on column ARC_TAX_DEPRECIATION.QUANTITY_END is 'Ending quantity of , for example, luxury autos.  This is used only when applying limit processing.';
comment on column ARC_TAX_DEPRECIATION.ESTIMATED_SALVAGE_END is 'Dollar amount of estimated salvage at the end of the period.';
comment on column ARC_TAX_DEPRECIATION.JOB_CREATION_AMOUNT is 'Amount of the bonus depreciation (under the Job Creation Act of 2002, the Job Growth Act of 2003, etc) taken during the year.';
comment on column ARC_TAX_DEPRECIATION.BOOK_BALANCE_ADJUST is 'Adjustment to the book balance in dollars.';
comment on column ARC_TAX_DEPRECIATION.ACCUM_RESERVE_ADJUST is 'User-input  dollar amount adjustment to accumulated tax depreciation reserve balance of the tax asset.';
comment on column ARC_TAX_DEPRECIATION.DEPRECIABLE_BASE_ADJUST is 'User-input dollar amount adjustment to depreciable base of the tax asset.';
comment on column ARC_TAX_DEPRECIATION.DEPRECIATION_ADJUST is 'User-input dollar amount adjustment to current year depreciation expense for the tax asset.';
comment on column ARC_TAX_DEPRECIATION.GAIN_LOSS_ADJUST is 'User-input dollar amount adjustment to ordinary gain/loss.';
comment on column ARC_TAX_DEPRECIATION.CAP_GAIN_LOSS_ADJUST is 'User-input dollar amount adjustment to capital gain/loss.';
comment on column ARC_TAX_DEPRECIATION.BOOK_BALANCE_ADJUST_METHOD is 'System-assigned identifier to an adjust convention (beginning, average, or end) for input adjustments to the book balance.';
comment on column ARC_TAX_DEPRECIATION.ACCUM_RESERVE_ADJUST_METHOD is 'System-assigned identifier to an adjust convention (beginning, average, or end) for input adjustments to the accumulated reserve depreciation reserve.';
comment on column ARC_TAX_DEPRECIATION.DEPRECIABLE_BASE_ADJUST_METHOD is 'System-assigned identifier to an adjust convention (beginning, average, or end) for input adjustments to the depreciable base.';
comment on column ARC_TAX_DEPRECIATION.DEPRECIATION_ADJUST_METHOD is 'Not used.';
comment on column ARC_TAX_DEPRECIATION.VERSION_ID is 'System-assigned identifier of a PowerTax version.';

----
----  Turn off logging to make the insert faster.
----
alter table ARC_TAX_DEPRECIATION nologging;

------
----  Insert into the new ARC_TAX_DEPRECIATION tables, with joining in the older 3 tables.
----

--* ABLE TO JOIN IN BACKUPS OF ARC_TAX_CONTROL, ARC_TAX_DEPRECIATION, ARC_TAX_DEPR_ADJUST
insert /*+ append */ into ARC_TAX_DEPRECIATION
   (TAX_BOOK_ID, TAX_RECORD_ID, TAX_YEAR, TIME_STAMP, USER_ID, TYPE_OF_PROPERTY_ID, TAX_LAW_ID,
    CONVENTION_ID, EXTRAORDINARY_CONVENTION, TAX_RATE_ID, REMAINING_LIFE_INDICATOR, TAX_LIMIT_ID,
    SUMMARY_4562_ID, LISTED_PROPERTY_IND, RECOVERY_PERIOD_ID, TAX_CREDIT_ID, DEFERRED_TAX_SCHEMA_ID,
    BOOK_BALANCE, TAX_BALANCE, REMAINING_LIFE, ACCUM_RESERVE, SL_RESERVE, DEPRECIABLE_BASE,
    FIXED_DEPRECIABLE_BASE, ACTUAL_SALVAGE, ESTIMATED_SALVAGE, ACCUM_SALVAGE, ADDITIONS, TRANSFERS,
    ADJUSTMENTS, RETIREMENTS, EXTRAORDINARY_RETIRES, ACCUM_ORDINARY_RETIRES, DEPRECIATION,
    COST_OF_REMOVAL, COR_EXPENSE, GAIN_LOSS, CAPITAL_GAIN_LOSS, EST_SALVAGE_PCT, BOOK_BALANCE_END,
    TAX_BALANCE_END, ACCUM_RESERVE_END, ACCUM_SALVAGE_END, ACCUM_ORDIN_RETIRES_END, SL_RESERVE_END,
    RETIRE_INVOL_CONV, SALVAGE_INVOL_CONV, SALVAGE_EXTRAORD, CALC_DEPRECIATION,
    OVER_ADJ_DEPRECIATION, RETIRE_RES_IMPACT, TRANSFER_RES_IMPACT, SALVAGE_RES_IMPACT,
    COR_RES_IMPACT, ADJUSTED_RETIRE_BASIS, RESERVE_AT_SWITCH, QUANTITY, CAPITALIZED_DEPR,
    RESERVE_AT_SWITCH_END, NUMBER_MONTHS_BEG, NUMBER_MONTHS_END, EX_RETIRE_RES_IMPACT, EX_GAIN_LOSS,
    QUANTITY_END, ESTIMATED_SALVAGE_END, JOB_CREATION_AMOUNT, BOOK_BALANCE_ADJUST,
    ACCUM_RESERVE_ADJUST, DEPRECIABLE_BASE_ADJUST, DEPRECIATION_ADJUST, GAIN_LOSS_ADJUST,
    CAP_GAIN_LOSS_ADJUST, BOOK_BALANCE_ADJUST_METHOD, ACCUM_RESERVE_ADJUST_METHOD,
    DEPRECIABLE_BASE_ADJUST_METHOD, DEPRECIATION_ADJUST_METHOD, VERSION_ID)
   select D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID,
          D1_ARC_TAX_DEPR_V1042.TAX_YEAR,
          D1_ARC_TAX_DEPR_V1042.TIME_STAMP,
          D1_ARC_TAX_DEPR_V1042.USER_ID,
          D1_ARC_TAX_CONTROL_V1042.TYPE_OF_PROPERTY_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_LAW_ID,
          D1_ARC_TAX_CONTROL_V1042.CONVENTION_ID,
          D1_ARC_TAX_CONTROL_V1042.EXTRAORDINARY_CONVENTION,
          D1_ARC_TAX_CONTROL_V1042.TAX_RATE_ID,
          D1_ARC_TAX_CONTROL_V1042.REMAINING_LIFE_INDICATOR,
          D1_ARC_TAX_CONTROL_V1042.TAX_LIMIT_ID,
          D1_ARC_TAX_CONTROL_V1042.SUMMARY_4562_ID,
          D1_ARC_TAX_CONTROL_V1042.LISTED_PROPERTY_IND,
          D1_ARC_TAX_CONTROL_V1042.RECOVERY_PERIOD_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_CREDIT_ID,
          D1_ARC_TAX_CONTROL_V1042.DEFERRED_TAX_SCHEMA_ID,
          D1_ARC_TAX_DEPR_V1042.BOOK_BALANCE,
          D1_ARC_TAX_DEPR_V1042.TAX_BALANCE,
          D1_ARC_TAX_DEPR_V1042.REMAINING_LIFE,
          D1_ARC_TAX_DEPR_V1042.ACCUM_RESERVE,
          D1_ARC_TAX_DEPR_V1042.SL_RESERVE,
          D1_ARC_TAX_DEPR_V1042.DEPRECIABLE_BASE,
          D1_ARC_TAX_DEPR_V1042.FIXED_DEPRECIABLE_BASE,
          D1_ARC_TAX_DEPR_V1042.ACTUAL_SALVAGE,
          D1_ARC_TAX_DEPR_V1042.ESTIMATED_SALVAGE,
          D1_ARC_TAX_DEPR_V1042.ACCUM_SALVAGE,
          D1_ARC_TAX_DEPR_V1042.ADDITIONS,
          D1_ARC_TAX_DEPR_V1042.TRANSFERS,
          D1_ARC_TAX_DEPR_V1042.ADJUSTMENTS,
          D1_ARC_TAX_DEPR_V1042.RETIREMENTS,
          D1_ARC_TAX_DEPR_V1042.EXTRAORDINARY_RETIRES,
          D1_ARC_TAX_DEPR_V1042.ACCUM_ORDINARY_RETIRES,
          D1_ARC_TAX_DEPR_V1042.DEPRECIATION,
          D1_ARC_TAX_DEPR_V1042.COST_OF_REMOVAL,
          D1_ARC_TAX_DEPR_V1042.COR_EXPENSE,
          D1_ARC_TAX_DEPR_V1042.GAIN_LOSS,
          D1_ARC_TAX_DEPR_V1042.CAPITAL_GAIN_LOSS,
          D1_ARC_TAX_DEPR_V1042.EST_SALVAGE_PCT,
          D1_ARC_TAX_DEPR_V1042.BOOK_BALANCE_END,
          D1_ARC_TAX_DEPR_V1042.TAX_BALANCE_END,
          D1_ARC_TAX_DEPR_V1042.ACCUM_RESERVE_END,
          D1_ARC_TAX_DEPR_V1042.ACCUM_SALVAGE_END,
          D1_ARC_TAX_DEPR_V1042.ACCUM_ORDIN_RETIRES_END,
          D1_ARC_TAX_DEPR_V1042.SL_RESERVE_END,
          D1_ARC_TAX_DEPR_V1042.RETIRE_INVOL_CONV,
          D1_ARC_TAX_DEPR_V1042.SALVAGE_INVOL_CONV,
          D1_ARC_TAX_DEPR_V1042.SALVAGE_EXTRAORD,
          D1_ARC_TAX_DEPR_V1042.CALC_DEPRECIATION,
          D1_ARC_TAX_DEPR_V1042.OVER_ADJ_DEPRECIATION,
          D1_ARC_TAX_DEPR_V1042.RETIRE_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.TRANSFER_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.SALVAGE_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.COR_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.ADJUSTED_RETIRE_BASIS,
          D1_ARC_TAX_DEPR_V1042.RESERVE_AT_SWITCH,
          D1_ARC_TAX_DEPR_V1042.QUANTITY,
          D1_ARC_TAX_DEPR_V1042.CAPITALIZED_DEPR,
          D1_ARC_TAX_DEPR_V1042.RESERVE_AT_SWITCH_END,
          D1_ARC_TAX_DEPR_V1042.NUMBER_MONTHS_BEG,
          D1_ARC_TAX_DEPR_V1042.NUMBER_MONTHS_END,
          D1_ARC_TAX_DEPR_V1042.EX_RETIRE_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.EX_GAIN_LOSS,
          D1_ARC_TAX_DEPR_V1042.QUANTITY_END,
          D1_ARC_TAX_DEPR_V1042.ESTIMATED_SALVAGE_END,
          D1_ARC_TAX_DEPR_V1042.JOB_CREATION_AMOUNT,
          D1_ARC_TAX_DEPR_ADJUST_V1042.BOOK_BALANCE_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.ACCUM_RESERVE_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIABLE_BASE_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIATION_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.GAIN_LOSS_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.CAP_GAIN_LOSS_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.BOOK_BALANCE_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_ADJUST_V1042.ACCUM_RESERVE_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIABLE_BASE_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIATION_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_V1042.VERSION_ID
     from D1_ARC_TAX_CONTROL_V1042, D1_ARC_TAX_DEPR_V1042, D1_ARC_TAX_DEPR_ADJUST_V1042
    where D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_V1042.TAX_BOOK_ID
      and D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_V1042.TAX_RECORD_ID
      and D1_ARC_TAX_CONTROL_V1042.VERSION_ID = D1_ARC_TAX_DEPR_V1042.VERSION_ID
      and D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_BOOK_ID
      and D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_RECORD_ID
      and D1_ARC_TAX_CONTROL_V1042.VERSION_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.VERSION_ID
      and D1_ARC_TAX_DEPR_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_BOOK_ID
      and D1_ARC_TAX_DEPR_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_RECORD_ID
      and D1_ARC_TAX_DEPR_V1042.TAX_YEAR = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_YEAR
      and D1_ARC_TAX_DEPR_V1042.VERSION_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.VERSION_ID;
commit;

--* ABLE TO JOIN IN BACKUPS OF TAX_CONTROL and TAX_DEPRECIATION, but not able to link back to TAX_DEPR_ADJUST
insert /*+ append */ into ARC_TAX_DEPRECIATION
   (TAX_BOOK_ID, TAX_RECORD_ID, TAX_YEAR, TIME_STAMP, USER_ID, TYPE_OF_PROPERTY_ID, TAX_LAW_ID,
    CONVENTION_ID, EXTRAORDINARY_CONVENTION, TAX_RATE_ID, REMAINING_LIFE_INDICATOR, TAX_LIMIT_ID,
    SUMMARY_4562_ID, LISTED_PROPERTY_IND, RECOVERY_PERIOD_ID, TAX_CREDIT_ID, DEFERRED_TAX_SCHEMA_ID,
    BOOK_BALANCE, TAX_BALANCE, REMAINING_LIFE, ACCUM_RESERVE, SL_RESERVE, DEPRECIABLE_BASE,
    FIXED_DEPRECIABLE_BASE, ACTUAL_SALVAGE, ESTIMATED_SALVAGE, ACCUM_SALVAGE, ADDITIONS, TRANSFERS,
    ADJUSTMENTS, RETIREMENTS, EXTRAORDINARY_RETIRES, ACCUM_ORDINARY_RETIRES, DEPRECIATION,
    COST_OF_REMOVAL, COR_EXPENSE, GAIN_LOSS, CAPITAL_GAIN_LOSS, EST_SALVAGE_PCT, BOOK_BALANCE_END,
    TAX_BALANCE_END, ACCUM_RESERVE_END, ACCUM_SALVAGE_END, ACCUM_ORDIN_RETIRES_END, SL_RESERVE_END,
    RETIRE_INVOL_CONV, SALVAGE_INVOL_CONV, SALVAGE_EXTRAORD, CALC_DEPRECIATION,
    OVER_ADJ_DEPRECIATION, RETIRE_RES_IMPACT, TRANSFER_RES_IMPACT, SALVAGE_RES_IMPACT,
    COR_RES_IMPACT, ADJUSTED_RETIRE_BASIS, RESERVE_AT_SWITCH, QUANTITY, CAPITALIZED_DEPR,
    RESERVE_AT_SWITCH_END, NUMBER_MONTHS_BEG, NUMBER_MONTHS_END, EX_RETIRE_RES_IMPACT, EX_GAIN_LOSS,
    QUANTITY_END, ESTIMATED_SALVAGE_END, JOB_CREATION_AMOUNT, BOOK_BALANCE_ADJUST,
    ACCUM_RESERVE_ADJUST, DEPRECIABLE_BASE_ADJUST, DEPRECIATION_ADJUST, GAIN_LOSS_ADJUST,
    CAP_GAIN_LOSS_ADJUST, BOOK_BALANCE_ADJUST_METHOD, ACCUM_RESERVE_ADJUST_METHOD,
    DEPRECIABLE_BASE_ADJUST_METHOD, DEPRECIATION_ADJUST_METHOD, VERSION_ID)
   select D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID,
          D1_ARC_TAX_DEPR_V1042.TAX_YEAR,
          D1_ARC_TAX_DEPR_V1042.TIME_STAMP,
          D1_ARC_TAX_DEPR_V1042.USER_ID,
          D1_ARC_TAX_CONTROL_V1042.TYPE_OF_PROPERTY_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_LAW_ID,
          D1_ARC_TAX_CONTROL_V1042.CONVENTION_ID,
          D1_ARC_TAX_CONTROL_V1042.EXTRAORDINARY_CONVENTION,
          D1_ARC_TAX_CONTROL_V1042.TAX_RATE_ID,
          D1_ARC_TAX_CONTROL_V1042.REMAINING_LIFE_INDICATOR,
          D1_ARC_TAX_CONTROL_V1042.TAX_LIMIT_ID,
          D1_ARC_TAX_CONTROL_V1042.SUMMARY_4562_ID,
          D1_ARC_TAX_CONTROL_V1042.LISTED_PROPERTY_IND,
          D1_ARC_TAX_CONTROL_V1042.RECOVERY_PERIOD_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_CREDIT_ID,
          D1_ARC_TAX_CONTROL_V1042.DEFERRED_TAX_SCHEMA_ID,
          D1_ARC_TAX_DEPR_V1042.BOOK_BALANCE,
          D1_ARC_TAX_DEPR_V1042.TAX_BALANCE,
          D1_ARC_TAX_DEPR_V1042.REMAINING_LIFE,
          D1_ARC_TAX_DEPR_V1042.ACCUM_RESERVE,
          D1_ARC_TAX_DEPR_V1042.SL_RESERVE,
          D1_ARC_TAX_DEPR_V1042.DEPRECIABLE_BASE,
          D1_ARC_TAX_DEPR_V1042.FIXED_DEPRECIABLE_BASE,
          D1_ARC_TAX_DEPR_V1042.ACTUAL_SALVAGE,
          D1_ARC_TAX_DEPR_V1042.ESTIMATED_SALVAGE,
          D1_ARC_TAX_DEPR_V1042.ACCUM_SALVAGE,
          D1_ARC_TAX_DEPR_V1042.ADDITIONS,
          D1_ARC_TAX_DEPR_V1042.TRANSFERS,
          D1_ARC_TAX_DEPR_V1042.ADJUSTMENTS,
          D1_ARC_TAX_DEPR_V1042.RETIREMENTS,
          D1_ARC_TAX_DEPR_V1042.EXTRAORDINARY_RETIRES,
          D1_ARC_TAX_DEPR_V1042.ACCUM_ORDINARY_RETIRES,
          D1_ARC_TAX_DEPR_V1042.DEPRECIATION,
          D1_ARC_TAX_DEPR_V1042.COST_OF_REMOVAL,
          D1_ARC_TAX_DEPR_V1042.COR_EXPENSE,
          D1_ARC_TAX_DEPR_V1042.GAIN_LOSS,
          D1_ARC_TAX_DEPR_V1042.CAPITAL_GAIN_LOSS,
          D1_ARC_TAX_DEPR_V1042.EST_SALVAGE_PCT,
          D1_ARC_TAX_DEPR_V1042.BOOK_BALANCE_END,
          D1_ARC_TAX_DEPR_V1042.TAX_BALANCE_END,
          D1_ARC_TAX_DEPR_V1042.ACCUM_RESERVE_END,
          D1_ARC_TAX_DEPR_V1042.ACCUM_SALVAGE_END,
          D1_ARC_TAX_DEPR_V1042.ACCUM_ORDIN_RETIRES_END,
          D1_ARC_TAX_DEPR_V1042.SL_RESERVE_END,
          D1_ARC_TAX_DEPR_V1042.RETIRE_INVOL_CONV,
          D1_ARC_TAX_DEPR_V1042.SALVAGE_INVOL_CONV,
          D1_ARC_TAX_DEPR_V1042.SALVAGE_EXTRAORD,
          D1_ARC_TAX_DEPR_V1042.CALC_DEPRECIATION,
          D1_ARC_TAX_DEPR_V1042.OVER_ADJ_DEPRECIATION,
          D1_ARC_TAX_DEPR_V1042.RETIRE_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.TRANSFER_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.SALVAGE_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.COR_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.ADJUSTED_RETIRE_BASIS,
          D1_ARC_TAX_DEPR_V1042.RESERVE_AT_SWITCH,
          D1_ARC_TAX_DEPR_V1042.QUANTITY,
          D1_ARC_TAX_DEPR_V1042.CAPITALIZED_DEPR,
          D1_ARC_TAX_DEPR_V1042.RESERVE_AT_SWITCH_END,
          D1_ARC_TAX_DEPR_V1042.NUMBER_MONTHS_BEG,
          D1_ARC_TAX_DEPR_V1042.NUMBER_MONTHS_END,
          D1_ARC_TAX_DEPR_V1042.EX_RETIRE_RES_IMPACT,
          D1_ARC_TAX_DEPR_V1042.EX_GAIN_LOSS,
          D1_ARC_TAX_DEPR_V1042.QUANTITY_END,
          D1_ARC_TAX_DEPR_V1042.ESTIMATED_SALVAGE_END,
          D1_ARC_TAX_DEPR_V1042.JOB_CREATION_AMOUNT,
          0 BOOK_BALANCE_ADJUST,
          0 ACCUM_RESERVE_ADJUST,
          0 DEPRECIABLE_BASE_ADJUST,
          0 DEPRECIATION_ADJUST,
          0 GAIN_LOSS_ADJUST,
          0 CAP_GAIN_LOSS_ADJUST,
          1 BOOK_BALANCE_ADJUST_METHOD,
          1 ACCUM_RESERVE_ADJUST_METHOD,
          1 DEPRECIABLE_BASE_ADJUST_METHOD,
          1 DEPRECIATION_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_V1042.VERSION_ID
     from D1_ARC_TAX_CONTROL_V1042, D1_ARC_TAX_DEPR_V1042
    where D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_V1042.TAX_BOOK_ID
      and D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_V1042.TAX_RECORD_ID
      and D1_ARC_TAX_CONTROL_V1042.VERSION_ID = D1_ARC_TAX_DEPR_V1042.VERSION_ID
      and not exists
    (select 1
             from D1_ARC_TAX_DEPR_ADJUST_V1042
            where D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_BOOK_ID
              and D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_RECORD_ID
              and D1_ARC_TAX_CONTROL_V1042.VERSION_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.VERSION_ID
              and D1_ARC_TAX_DEPR_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_BOOK_ID
              and D1_ARC_TAX_DEPR_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_RECORD_ID
              and D1_ARC_TAX_DEPR_V1042.TAX_YEAR = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_YEAR
              and D1_ARC_TAX_DEPR_V1042.VERSION_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.VERSION_ID);
commit;

--* ABLE TO JOIN IN BACKUPS OF ARC_TAX_CONTROL and ARC_TAX_DEPR_ADJUST, but not able to link back to ARC_TAX_DEPRECIATION
insert /*+ append */ into ARC_TAX_DEPRECIATION
   (TAX_BOOK_ID, TAX_RECORD_ID, TAX_YEAR, TIME_STAMP, USER_ID, TYPE_OF_PROPERTY_ID, TAX_LAW_ID,
    CONVENTION_ID, EXTRAORDINARY_CONVENTION, TAX_RATE_ID, REMAINING_LIFE_INDICATOR, TAX_LIMIT_ID,
    SUMMARY_4562_ID, LISTED_PROPERTY_IND, RECOVERY_PERIOD_ID, TAX_CREDIT_ID, DEFERRED_TAX_SCHEMA_ID,
    BOOK_BALANCE, TAX_BALANCE, REMAINING_LIFE, ACCUM_RESERVE, SL_RESERVE, DEPRECIABLE_BASE,
    FIXED_DEPRECIABLE_BASE, ACTUAL_SALVAGE, ESTIMATED_SALVAGE, ACCUM_SALVAGE, ADDITIONS, TRANSFERS,
    ADJUSTMENTS, RETIREMENTS, EXTRAORDINARY_RETIRES, ACCUM_ORDINARY_RETIRES, DEPRECIATION,
    COST_OF_REMOVAL, COR_EXPENSE, GAIN_LOSS, CAPITAL_GAIN_LOSS, EST_SALVAGE_PCT, BOOK_BALANCE_END,
    TAX_BALANCE_END, ACCUM_RESERVE_END, ACCUM_SALVAGE_END, ACCUM_ORDIN_RETIRES_END, SL_RESERVE_END,
    RETIRE_INVOL_CONV, SALVAGE_INVOL_CONV, SALVAGE_EXTRAORD, CALC_DEPRECIATION,
    OVER_ADJ_DEPRECIATION, RETIRE_RES_IMPACT, TRANSFER_RES_IMPACT, SALVAGE_RES_IMPACT,
    COR_RES_IMPACT, ADJUSTED_RETIRE_BASIS, RESERVE_AT_SWITCH, QUANTITY, CAPITALIZED_DEPR,
    RESERVE_AT_SWITCH_END, NUMBER_MONTHS_BEG, NUMBER_MONTHS_END, EX_RETIRE_RES_IMPACT, EX_GAIN_LOSS,
    QUANTITY_END, ESTIMATED_SALVAGE_END, JOB_CREATION_AMOUNT, BOOK_BALANCE_ADJUST,
    ACCUM_RESERVE_ADJUST, DEPRECIABLE_BASE_ADJUST, DEPRECIATION_ADJUST, GAIN_LOSS_ADJUST,
    CAP_GAIN_LOSS_ADJUST, BOOK_BALANCE_ADJUST_METHOD, ACCUM_RESERVE_ADJUST_METHOD,
    DEPRECIABLE_BASE_ADJUST_METHOD, DEPRECIATION_ADJUST_METHOD, VERSION_ID)
   select D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID,
          D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_YEAR,
          D1_ARC_TAX_DEPR_ADJUST_V1042.TIME_STAMP,
          D1_ARC_TAX_DEPR_ADJUST_V1042.USER_ID,
          D1_ARC_TAX_CONTROL_V1042.TYPE_OF_PROPERTY_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_LAW_ID,
          D1_ARC_TAX_CONTROL_V1042.CONVENTION_ID,
          D1_ARC_TAX_CONTROL_V1042.EXTRAORDINARY_CONVENTION,
          D1_ARC_TAX_CONTROL_V1042.TAX_RATE_ID,
          D1_ARC_TAX_CONTROL_V1042.REMAINING_LIFE_INDICATOR,
          D1_ARC_TAX_CONTROL_V1042.TAX_LIMIT_ID,
          D1_ARC_TAX_CONTROL_V1042.SUMMARY_4562_ID,
          D1_ARC_TAX_CONTROL_V1042.LISTED_PROPERTY_IND,
          D1_ARC_TAX_CONTROL_V1042.RECOVERY_PERIOD_ID,
          D1_ARC_TAX_CONTROL_V1042.TAX_CREDIT_ID,
          D1_ARC_TAX_CONTROL_V1042.DEFERRED_TAX_SCHEMA_ID,
          0 BOOK_BALANCE,
          0 TAX_BALANCE,
          0 REMAINING_LIFE,
          0 ACCUM_RESERVE,
          0 SL_RESERVE,
          0 DEPRECIABLE_BASE,
          0 FIXED_DEPRECIABLE_BASE,
          0 ACTUAL_SALVAGE,
          0 ESTIMATED_SALVAGE,
          0 ACCUM_SALVAGE,
          0 ADDITIONS,
          0 TRANSFERS,
          0 ADJUSTMENTS,
          0 RETIREMENTS,
          0 EXTRAORDINARY_RETIRES,
          0 ACCUM_ORDINARY_RETIRES,
          0 DEPRECIATION,
          0 COST_OF_REMOVAL,
          0 COR_EXPENSE,
          0 GAIN_LOSS,
          0 CAPITAL_GAIN_LOSS,
          0 EST_SALVAGE_PCT,
          0 BOOK_BALANCE_END,
          0 TAX_BALANCE_END,
          0 ACCUM_RESERVE_END,
          0 ACCUM_SALVAGE_END,
          0 ACCUM_ORDIN_RETIRES_END,
          0 SL_RESERVE_END,
          0 RETIRE_INVOL_CONV,
          0 SALVAGE_INVOL_CONV,
          0 SALVAGE_EXTRAORD,
          0 CALC_DEPRECIATION,
          0 OVER_ADJ_DEPRECIATION,
          0 RETIRE_RES_IMPACT,
          0 TRANSFER_RES_IMPACT,
          0 SALVAGE_RES_IMPACT,
          0 COR_RES_IMPACT,
          0 ADJUSTED_RETIRE_BASIS,
          0 RESERVE_AT_SWITCH,
          0 QUANTITY,
          0 CAPITALIZED_DEPR,
          0 RESERVE_AT_SWITCH_END,
          0 NUMBER_MONTHS_BEG,
          0 NUMBER_MONTHS_END,
          0 EX_RETIRE_RES_IMPACT,
          0 EX_GAIN_LOSS,
          0 QUANTITY_END,
          0 ESTIMATED_SALVAGE_END,
          0 JOB_CREATION_AMOUNT,
          D1_ARC_TAX_DEPR_ADJUST_V1042.BOOK_BALANCE_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.ACCUM_RESERVE_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIABLE_BASE_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIATION_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.GAIN_LOSS_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.CAP_GAIN_LOSS_ADJUST,
          D1_ARC_TAX_DEPR_ADJUST_V1042.BOOK_BALANCE_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_ADJUST_V1042.ACCUM_RESERVE_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIABLE_BASE_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_ADJUST_V1042.DEPRECIATION_ADJUST_METHOD,
          D1_ARC_TAX_DEPR_ADJUST_V1042.VERSION_ID
     from D1_ARC_TAX_CONTROL_V1042, D1_ARC_TAX_DEPR_ADJUST_V1042
    where D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_BOOK_ID
      and D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_RECORD_ID
      and D1_ARC_TAX_CONTROL_V1042.VERSION_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.VERSION_ID
      and not exists
    (select 1
             from D1_ARC_TAX_DEPR_V1042
            where D1_ARC_TAX_CONTROL_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_V1042.TAX_BOOK_ID
              and D1_ARC_TAX_CONTROL_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_V1042.TAX_RECORD_ID
              and D1_ARC_TAX_CONTROL_V1042.VERSION_ID = D1_ARC_TAX_DEPR_V1042.VERSION_ID
              and D1_ARC_TAX_DEPR_V1042.TAX_BOOK_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_BOOK_ID
              and D1_ARC_TAX_DEPR_V1042.TAX_RECORD_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_RECORD_ID
              and D1_ARC_TAX_DEPR_V1042.TAX_YEAR = D1_ARC_TAX_DEPR_ADJUST_V1042.TAX_YEAR
              and D1_ARC_TAX_DEPR_V1042.VERSION_ID = D1_ARC_TAX_DEPR_ADJUST_V1042.VERSION_ID);
commit;

----
----  Turn logging back on
----
alter table ARC_TAX_DEPRECIATION logging;

----not creating grants, synonyms, timestamp triggers as this is done by the upgrade
----tool at the end automatically.

----also, keep the backup tables for the delivery teams on upgrades for analysis should the original data be needed.

                --**************************
                -- Log the run of the script
                --**************************

                insert into PP_SCHEMA_CHANGE_LOG
                   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
                    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
                values
                   (874, 0, 10, 4, 2, 0, 35432, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035432_pwrtax_mlp_combine_3_arc.sql', 1,
                    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
                    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;