/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043819_sysweb_functions_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 2015.1   05/06/2015 Ryan Oliveria    Web Framework - Functions
||============================================================================
*/

CREATE OR REPLACE FUNCTION PWRPLANT.SYS_DOES_TRIGGER_EXIST(objectName  VARCHAR2,
                                                          objectOwner VARCHAR2)
  RETURN NUMBER IS
  object_exists NUMBER := 0;
BEGIN
  --This Does Not Confirm Object Type.  Just that the name exists.
  SELECT COUNT(*)
    INTO object_exists
    FROM ALL_TRIGGERS
   WHERE TRIGGER_NAME = upper(objectName)
     AND OWNER = upper(objectOwner);

  RETURN object_exists;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2553, 0, 2015, 1, 0, 0, 43819, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043819_sysweb_functions_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;