 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_042030_pcm_revision_details_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/14/2015 Ryan Oliveria  New Workspace
||============================================================================
*/

insert into PP_REQUIRED_TABLE_COLUMN
	(ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION, REQUIRED_COLUMN_EXPRESSION)
values
	(1013, 'work_order_approval', 'est_start_date', 'uo_pcm_revision.dw_detail', 'Est Start Date', null);

insert into PP_REQUIRED_TABLE_COLUMN
	(ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION, REQUIRED_COLUMN_EXPRESSION)
values
	(1014, 'work_order_approval', 'est_in_service_date', 'uo_pcm_revision.dw_detail', 'Est In Service Date', null);

insert into PP_REQUIRED_TABLE_COLUMN
	(ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION, REQUIRED_COLUMN_EXPRESSION)
values
	(1015, 'work_order_approval', 'est_complete_date', 'uo_pcm_revision.dw_detail', 'Est Complete Date', null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2177, 0, 2015, 1, 0, 0, 042030, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042030_pcm_revision_details_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;