/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043271_budget_afudc_by_wo_3_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 3/15/2015  Chris Mardis     Ability to calculate budget afudc by work order
||============================================================================
*/

alter table BUDGET_AFUDC_CALC_CLOSING add (constraint pk_BDG_AFC_CALC_CLOSING primary key (work_order_id, expenditure_type_id, month_number, wo_work_order_id));

alter table BUDGET_AFUDC_CALC_BEG_BAL add (constraint pk_BDG_AFC_CALC_BEG_BAL primary key (work_order_id, expenditure_type_id, month_number, wo_work_order_id));

alter table BUDGET_AFUDC_CALC add (constraint pk_BDG_AFC_CALC primary key (work_order_id, revision, expenditure_type_id, month_number, wo_work_order_id));


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2382, 0, 2015, 1, 0, 0, 43271, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043271_budget_afudc_by_wo_3_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;