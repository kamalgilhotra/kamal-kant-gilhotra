/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050749_lessor_02_add_ilr_id_rev_type_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/04/2018 Andrew Hill       Add ILR ID/Rev types
||============================================================================
*/

CREATE OR REPLACE TYPE t_lsr_ilr_id_revision AS OBJECT(ilr_id NUMBER(22,2), revision NUMBER(22,2));
/
CREATE OR REPLACE TYPE t_lsr_ilr_id_revision_tab AS TABLE OF t_lsr_ilr_id_revision;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4262, 0, 2017, 3, 0, 0, 50749, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050749_lessor_02_add_ilr_id_rev_type_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;