set serveroutput on;
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_anlyt_11_sequences.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
||  1.0     09/06/2013 Scott Moody      Initial
|| 10.4.3.0 10/24/2014 Chad Theilman    Asset Analytics - Sequences
||============================================================================
*/

declare 
  doesSequenceExist number := 0;
  seqCount number := 0;
  
  begin
	--doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PA_ANALYTICS_SEQ','PWRANLYT');
	
    select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRANLYT' and Object_Name = 'PA_ANALYTICS_SEQ';
	
	if seqCount = 0 then
		begin
		    execute immediate 'CREATE SEQUENCE PWRANLYT.PA_ANALYTICS_SEQ';  --NEEDED FOR APPLICATION
		end;
	end if;
		
	--doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PA_ACCOUNTS_SEQ','PWRANLYT');
	
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRANLYT' and Object_Name = 'PA_ACCOUNTS_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRANLYT.PA_ACCOUNTS_SEQ';
		end;
	end if;
		
	--doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PA_LOCATION_AGG_SEQ','PWRANLYT');
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRANLYT' and Object_Name = 'PA_LOCATION_AGG_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRANLYT.PA_LOCATION_AGG_SEQ';
		end;
	end if;
		
	--doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PA_PROPERTY_SEQ','PWRANLYT');
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRANLYT' and Object_Name = 'PA_PROPERTY_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRANLYT.PA_PROPERTY_SEQ';
		end;
	end if;
		
	--doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PA_VINTAGE_SPREADS_SEQ', 'PWRANLYT');
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRANLYT' and Object_Name = 'PA_VINTAGE_SPREADS_SEQ';

	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRANLYT.PA_VINTAGE_SPREADS_SEQ';
		end;
	end if;
	
	--doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PA_AVERAGE_AGES_SEQ','PWRANLYT');
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRANLYT' and Object_Name = 'PA_AVERAGE_AGES_SEQ';
	
	if seqCount = 0 then
		begin
			execute immediate 'CREATE SEQUENCE PWRANLYT.PA_AVERAGE_AGES_SEQ';
		end;
	end if;
		
	--doesSequenceExist := PWRPLANT.SYS_DOES_SEQUENCE_EXIST('PA_CPR_BALANCES_SEQ', 'PWRANLYT');
	select count(*) into seqCount from all_objects where object_type = 'SEQUENCE' and Owner = 'PWRANLYT' and Object_Name = 'PA_CPR_BALANCES_SEQ';
	
	if seqCount = 0 then 
		begin
			execute immediate 'CREATE SEQUENCE PWRANLYT.PA_CPR_BALANCES_SEQ';
		end;
	end if;
  end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1576, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_anlyt_11_sequences.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;