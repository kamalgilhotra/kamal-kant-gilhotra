/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035922_projects.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/23/2014 Chris Mardis   Point Release
||============================================================================
*/

alter table WO_EST_SLIDE_RESULTS_TEMP add HIST_ACTUALS number(22,2);
alter table WO_EST_SLIDE_RESULTS_TEMP add FUTURE_DOLLARS number(22,2);

comment on column WO_EST_SLIDE_RESULTS_TEMP.HIST_ACTUALS is 'No longer used.';
comment on column WO_EST_SLIDE_RESULTS_TEMP.FUTURE_DOLLARS is 'No longer used.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (905, 0, 10, 4, 2, 0, 35922, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035922_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;