/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045167_budgetcap_tbl_comments_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- --------------------  ---------------------------------
|| 2015.2   11/02/2015 Josh Sandler          update budget afudc calc comments
||============================================================================
*/ 

COMMENT ON COLUMN budget_afudc_calc.end_cwip IS 'Beg_cwip + amount - curent_month_closings + afudc_debt + afudc_equity';
COMMENT ON COLUMN budget_afudc_calc.end_cwip_afudc IS 'Beg_cwip_afudc + amount_afudc - current_month_closings_afudc + compounded_afudc + cap_interest_adjustment';
COMMENT ON COLUMN budget_afudc_calc.end_cwip_cpi IS 'Beg_cwip_cpi + amount_cpi - current_month_closings_cpi + compounded_cpi';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2954, 0, 2015, 2, 0, 0, 45167, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045167_budgetcap_tbl_comments_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
