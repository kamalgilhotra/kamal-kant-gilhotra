/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010289_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.2   07/02/2012 Sunjin Cone    Point Release
||============================================================================
*/

update REPAIR_BATCH_CONTROL
   set STATUS = 'Stranded:Rep Data Deleted'
 where REPAIR_SCHEMA_ID in
       (select REPAIR_SCHEMA_ID
          from REPAIR_SCHEMA
         where UPPER(USAGE_FLAG) in ('CPR', 'CWIP_CPR', 'WMIS'))
   and BATCH_ID in (select BATCH_ID
                      from REPAIR_BATCH_CONTROL
                     where STATUS = 'Stranded'
                    minus
                    select BATCH_ID from REPAIR_WO_SEG_REPORTING);

update PP_REPORTS
   set INPUT_WINDOW = 'dw_repair_batch_control_cpr'
 where DATAWINDOW in ('dw_repair_netwk_circuit_thres_related_report',
                      'dw_repair_netwk_circuit_thres_report',
                      'dw_repair_netwk_process_related_report',
                      'dw_repair_netwk_process_report',
                      'dw_repair_netwk_wo_thres_related_report',
                      'dw_repair_netwk_wo_thres_report');


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (155, 0, 10, 3, 4, 2, 10289, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.2_maint_010289_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
