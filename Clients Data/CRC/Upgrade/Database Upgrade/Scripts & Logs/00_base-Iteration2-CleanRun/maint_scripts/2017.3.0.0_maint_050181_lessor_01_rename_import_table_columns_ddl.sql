/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050181_lessor_01_rename_import_table_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/05/2018 Andrew Hill       Rename ILR Import Columns for Override Rates
||============================================================================
*/

ALTER TABLE lsr_import_ilr RENAME COLUMN sales_type_disc_rate TO sales_type_disc_rate_override;
ALTER TABLE lsr_import_ilr RENAME COLUMN direct_fin_disc_rate TO direct_fin_disc_rate_override;
ALTER TABLE lsr_import_ilr RENAME COLUMN direct_fin_int_rate TO direct_fin_int_rate_override;

COMMENT ON COLUMN lsr_import_ilr.sales_type_disc_rate_override IS 'Corresponds to the override rate used throughout the Sales-Type lease calculations.';
COMMENT ON COLUMN lsr_import_ilr.direct_fin_disc_rate_override IS 'Corresponds to the override rate used to calculate the Net Investment and all schedule calculations except for interest on Net Investment';
COMMENT ON COLUMN lsr_import_ilr.direct_fin_int_rate_override IS 'Corresponds to the override rate used to calculate interest on the Net Investment';

ALTER TABLE lsr_import_ilr_archive RENAME COLUMN sales_type_disc_rate TO sales_type_disc_rate_override;
ALTER TABLE lsr_import_ilr_archive RENAME COLUMN direct_fin_disc_rate TO direct_fin_disc_rate_override;
ALTER TABLE lsr_import_ilr_archive RENAME COLUMN direct_fin_int_rate TO direct_fin_int_rate_override;

COMMENT ON COLUMN lsr_import_ilr_archive.sales_type_disc_rate_override IS 'Corresponds to the override rate used throughout the Sales-Type lease calculations.';
COMMENT ON COLUMN lsr_import_ilr_archive.direct_fin_disc_rate_override IS 'Corresponds to the override rate used to calculate the Net Investment and all schedule calculations except for interest on Net Investment';
COMMENT ON COLUMN lsr_import_ilr_archive.direct_fin_int_rate_override IS 'Corresponds to the override rate used to calculate interest on the Net Investment';

ALTER TABLE lsr_import_ilr DROP COLUMN DIRECT_FIN_FMV_RATE;
alter table lsr_import_ilr_archive drop column DIRECT_FIN_FMV_RATE;

ALTER TABLE pp_import_template_fields drop constraint PP_IMPT_TMPLT_IMPT_COL_FK;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4276, 0, 2017, 3, 0, 0, 50181, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050181_lessor_01_rename_import_table_columns_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 