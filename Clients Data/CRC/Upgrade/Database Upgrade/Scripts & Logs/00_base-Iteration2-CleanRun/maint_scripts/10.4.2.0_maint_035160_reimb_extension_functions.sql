/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035160_reimb_extension_functions.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/15/2014 Brandon Beck
||============================================================================
*/

/* Uncomment if you want to reload all functions.  This could overwrite custom code.
drop function F_CLIENT_REIMB_ADJUSTMENT_OPEN;
drop function F_CLIENT_REIMB_APPR_REV_CUSTOM;
drop function F_CLIENT_REIMB_BG_DETAIL_OPEN;
drop function F_CLIENT_REIMB_AUTOGEN_REVIEW;
drop function F_CLIENT_REIMB_BILL_APPR;
drop function F_CLIENT_REIMB_WO_CHECK;
drop function F_CLIENT_REIMB_CALC_BILL;
drop function F_CLIENT_REIMB_CHARGE_AUDIT;
drop function F_CLIENT_REIMB_CALC_WO_PCT;
drop function F_CLIENT_REIMB_BILL_AUDIT;
drop function F_CLIENT_REIMB_REV_CALC_CUSTOM;
drop function F_CLIENT_REIMB_CUSTOMER_UPDATE;
drop function F_CLIENT_REIMB_DETAIL_UPDATE_2;
drop function F_CLIENT_REIMB_DETAIL_UPDATE;
drop function F_CLIENT_REIMB_GET_DEPOSITS;
drop function F_CLIENT_REIMB_INITIATE_BG;
drop function F_CLIENT_REIMB_INITIATE_OPEN;
drop function F_CLIENT_REIMB_PRE_REV_CUSTOM;
drop function F_CLIENT_REIMB_REFUND_CALC;
*/


create function F_CLIENT_REIMB_ADJUSTMENT_OPEN return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_ADJUSTMENT_OPEN
   return 1;
end F_CLIENT_REIMB_ADJUSTMENT_OPEN;
/


create function F_CLIENT_REIMB_APPR_REV_CUSTOM(A_BILLING_GROUP_ID number,
                                               A_REVIEW_ID        number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_APPR_REV_CUSTOM
   return 1;
end F_CLIENT_REIMB_APPR_REV_CUSTOM;
/

create function F_CLIENT_REIMB_BG_DETAIL_OPEN(A_BILLING_GROUP_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_BG_DETAIL_OPEN
   return 1;
end F_CLIENT_REIMB_BG_DETAIL_OPEN;
/


create function F_CLIENT_REIMB_AUTOGEN_REVIEW(A_REVIEW_ID number) return varchar2 is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_AUTOGEN_REVIEW_NUM
   return '';
end;
/


create function F_CLIENT_REIMB_BILL_APPR(A_BILL_NUMBER_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_BILL_APPR_CUSTOM
   return 1;
end;
/

create function F_CLIENT_REIMB_WO_CHECK(A_WORK_ORDER_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_WO_CHECK
   return 1;
end;
/


create function F_CLIENT_REIMB_CALC_BILL(A_BILLING_GROUP_ID number,
                                         A_BILLING_TYPE_ID  number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_CALC_BILL_CUSTOM
   return 1;
end;
/


create function F_CLIENT_REIMB_CHARGE_AUDIT(A_BILLING_GROUP_ID number,
                                            A_STATUS           number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_CHARGE_PULL_AUDIT
   return 1;
end;
/


create function F_CLIENT_REIMB_CALC_WO_PCT(A_BILLING_GROUP_ID number,
                                           A_WO_ID            number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_CALC_WO_PCT_BILLABLE
   return 1;
end;
/


create function F_CLIENT_REIMB_BILL_AUDIT(A_BILL_NUMBER_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to F_REIMB_BILL_AUDIT
   return 1;
end;
/

create function F_CLIENT_REIMB_REV_CALC_CUSTOM(A_BILLING_GROUP_ID number,
                                               A_FINAL_REVIEW     number,
                                               A_MONTH_NUMBER     number,
                                               A_BILL_SEQ_ID      number,
                                               A_MAN_REFUND       number) return varchar2 is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_REV_CALC_CUSTOM
   --Note that A_FINAL_REVIEW and A_MAN_REFUND are booleans in PB environment but are converted to longs before this function is called
   return 'OK';
end;
/


create function F_CLIENT_REIMB_CUSTOMER_UPDATE(A_CUSTOMER_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_CUSTOMER_UPDATE
   return 1;
end;
/


create function F_CLIENT_REIMB_DETAIL_UPDATE_2(A_BILLING_GROUP_ID PKG_PP_COMMON.NUM_TABTYPE,
                                               A_SOURCE_ID        PKG_PP_COMMON.NUM_TABTYPE,
                                               A_ID               PKG_PP_COMMON.NUM_TABTYPE,
                                               A_AMT_TYPE_ID      PKG_PP_COMMON.NUM_TABTYPE)
   return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_DETAIL_CUSTOM_UPDATE
   return 1;
end;
/


create function F_CLIENT_REIMB_DETAIL_UPDATE(A_BILLING_GROUP_ID number,
                                             A_PRE_UPDATE       number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_DETAIL_UPDATE
   --Note that A_PRE_UPDATE is a boolean in the PB environment but a long here
   return 1;
end;
/


create function F_CLIENT_REIMB_GET_DEPOSITS(A_BILLING_GROUP_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_GET_DEPOSITS
   return 0; --It returns 0 instead of 1 in original PB version. No idea why its different.
end;
/


create function F_CLIENT_REIMB_INITIATE_BG(A_BILL_GROUP_ID   number,
                                           A_WORK_ORDER_ID   number,
                                           A_REIMB_METHOD_ID number,
                                           A_COMPANY_ID      number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_INITIATE_BG_CUSTOM
   return 1;
end;
/


create function F_CLIENT_REIMB_INITIATE_OPEN(A_FUNCTION_PLACE varchar2) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_INITIATE_OPEN
   return 1;
end;
/


create function F_CLIENT_REIMB_PRE_REV_CUSTOM(A_BILLING_GROUP_ID number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_PRE_REV_CUSTOM
   return 1;
end;
/


create function F_CLIENT_REIMB_REFUND_CALC(A_CALLED_FROM      varchar2,
                                           A_TIME_CALLED      varchar2,
                                           A_BILLING_GROUP_ID number,
                                           A_REVIEW_ID        number) return number is
   -- *********************************************
   -- In order to utilize an extension function.
   -- The following table MUST be populated
   -- pp_client_extensions:
   --    id = A unique number
   --    function_name = the name of the oracle function being implemented
   --    is_active = 1 (Can be inactivated later by setting to 0)
   -- *********************************************
begin
   --maps to PB function F_REIMB_REFUND_CALC_CUSTOM
   return 1;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (866, 0, 10, 4, 2, 0, 35160, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035160_reimb_extension_functions.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;