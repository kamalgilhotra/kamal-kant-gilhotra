/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_system_actions_windows.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/13/2013 Brandon Beck   Point release
||============================================================================
*/

create table PPBASE_ACTIONS_WINDOWS
(
 ID                number(22,0) not null,
 MODULE            varchar2(50),
 ACTION_IDENTIFIER varchar2(50),
 ACTION_TEXT       varchar2(15),
 ACTION_ORDER      number(2, 0),
 ACTION_EVENT      varchar2(35),
 USER_ID           varchar2(18),
 TIME_STAMP        date
);

alter table PPBASE_ACTIONS_WINDOWS
   add constraint PPBASE_ACTIONS_WINDOWS_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (507, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_system_actions_windows.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
