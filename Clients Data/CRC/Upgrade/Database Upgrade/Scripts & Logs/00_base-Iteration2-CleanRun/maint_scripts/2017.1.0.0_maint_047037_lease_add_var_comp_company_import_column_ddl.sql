/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047037_lease_add_var_comp_company_import_column_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/19/2017 Jared Watkins    add Company as a required column translating off of the Description
||============================================================================
*/

--alter the VC import tables to include the company ID and xlate fields
alter table ls_import_vp_var_comp_amt
add company_id number(22,0)
;

alter table ls_import_vp_var_comp_amt
add company_xlate varchar2(254)
;

comment on column ls_import_vp_var_comp_amt.company_id is 'System assigned identifier for a Company.';
comment on column ls_import_vp_var_comp_amt.company_id is 'Translation field for determining the Company.';

alter table ls_import_vp_var_comp_amt_arch
add company_id number(22,0)
;

alter table ls_import_vp_var_comp_amt_arch
add company_xlate varchar2(254)
;

comment on column ls_import_vp_var_comp_amt_arch.company_id is 'System assigned identifier for a Company.';
comment on column ls_import_vp_var_comp_amt_arch.company_id is 'Translation field for determining the Company.';

--update the unique index on formula component description to TRIM() as well as UPPER()
drop index LS_FORMULA_COMP_DESC_IDX;

create unique index LS_FORMULA_COMP_DESC_IDX
on ls_formula_component(upper(trim(description)))
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3506, 0, 2017, 1, 0, 0, 47037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047037_lease_add_var_comp_company_import_column_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;