/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034116_lease_mid_period_methods.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   11/20/2013 Brandon Beck
||============================================================================
*/

insert into DEPR_MID_PERIOD_METHOD
   (MID_PERIOD_METHOD, DESCRIPTION, CALC_OPTION, SORT_ORDER)
values
   ('SLE', 'Straight Line Expense for Leases', 'I', 100);

insert into DEPR_MID_PERIOD_METHOD
   (MID_PERIOD_METHOD, DESCRIPTION, CALC_OPTION, SORT_ORDER)
values
   ('FERC', 'FERC Depreciation for Leases', 'I', 110);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (775, 0, 10, 4, 1, 2, 34116, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_034116_lease_mid_period_methods.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;