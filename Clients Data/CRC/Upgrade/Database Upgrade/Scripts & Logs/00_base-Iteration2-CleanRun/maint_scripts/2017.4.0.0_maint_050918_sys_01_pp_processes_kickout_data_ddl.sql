/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050918_sys_01_pp_processes_kickout_data_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/05/2018 TechProdMgmt	New Table to support Integration Hub integration error records
||============================================================================
*/
-- Create table
create table PP_PROCESSES_KICKOUT_DATA
(
  process_id    number(22) not null,
  occurrence_id number(22) not null,
  record_id     number(22) not null,
  error_msg     varchar2(2000) not null,
  time_stamp    date,
  user_id       varchar2(18),
  error_data    clob
)
;

-- Add comments to the table 
comment on table PP_PROCESSES_KICKOUT_DATA
  is '(O)  [10]
The PP Processes Kickout Data table holds the error messages and raw data (in JSON format) from programs running outside of standard PowerPlan.  
This is requirement for enabling SaaS integration hub but other processes can use it as well.';
-- Add comments to the columns 
comment on column PP_PROCESSES_KICKOUT_DATA.process_id
  is 'System-assigned identifier of a particular process.';
comment on column PP_PROCESSES_KICKOUT_DATA.occurrence_id
  is 'System-assigned identifier of a particular occurrence of a process.';
comment on column PP_PROCESSES_KICKOUT_DATA.record_id
  is 'Identifier for specific record for the data that encountered an error. Value could be derived from input data or system generated.';
comment on column PP_PROCESSES_KICKOUT_DATA.error_msg
  is 'Error or other message.';
comment on column PP_PROCESSES_KICKOUT_DATA.time_stamp
  is 'Standard System-assigned timestamp used for audit purposes.';
comment on column PP_PROCESSES_KICKOUT_DATA.user_id
  is 'Standard System-assigned user id used for audit purposes.';
comment on column PP_PROCESSES_KICKOUT_DATA.error_data
  is 'Raw Data that produced the error. Stored in JSON format.';
-- Create/Recreate primary, unique and foreign key constraints 
alter table PP_PROCESSES_KICKOUT_DATA
  add constraint PK_PP_PROCESSES_KICKOUT_DATA primary key (PROCESS_ID, OCCURRENCE_ID, RECORD_ID, ERROR_MSG);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6402, 0, 2017, 4, 0, 0, 50918, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050918_sys_01_pp_processes_kickout_data_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;