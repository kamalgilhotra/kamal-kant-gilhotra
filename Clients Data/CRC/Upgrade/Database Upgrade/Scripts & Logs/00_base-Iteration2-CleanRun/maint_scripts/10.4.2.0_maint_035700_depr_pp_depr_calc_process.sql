/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035700_depr_pp_depr_calc_process.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 01/14/2014 Charlie Shilling pp_depr_calc.exe needs to be able to run concurrently
||============================================================================
*/

update PP_PROCESSES
   set ALLOW_CONCURRENT = 1, EXECUTABLE_FILE = 'pp_depr_calc.exe'
 where UPPER(DESCRIPTION) = 'DEPRECIATION CALCULATION';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (865, 0, 10, 4, 2, 0, 35700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035700_depr_pp_depr_calc_process.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
