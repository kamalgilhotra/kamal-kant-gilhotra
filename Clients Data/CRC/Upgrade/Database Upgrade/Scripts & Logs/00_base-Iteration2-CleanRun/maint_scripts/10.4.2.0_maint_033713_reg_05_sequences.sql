/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033713_reg_05_sequences.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014  Sarah Byers
||============================================================================
*/

--Sequence for adjustment_id
create sequence PPSEQ_REG_ADJUST_ID;

--Sequence for reg_acct_ids
create sequence PPSEQ_REG_ACCT_ID;

--sequence for general ID needs
create sequence PPSEQ_REG_ANY_ID;

--sequence for alloc results
create sequence PPSEQ_REG_ALLOC_RESULT_ID;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (748, 0, 10, 4, 2, 0, 33713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033713_reg_05_sequences.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
