/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033366_lease_floating_rate.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   11/18/2013 Kyle Peterson
||============================================================================
*/

alter table LS_ILR_GROUP add USE_FLOATING_RATE number(1,0) default 0;
comment on column LS_ILR_GROUP.USE_FLOATING_RATE is 'Indicates whether the ILR Group uses additional floating rates.';

alter table LS_RENT_BUCKET_ADMIN add FLOATING_RATE_BUCKET number(1,0) default 2;
comment on column LS_RENT_BUCKET_ADMIN.FLOATING_RATE_BUCKET is 'Indicates which bucket receives floating rate amounts.';

create table LS_ILR_GROUP_FLOATING_RATES
(
 ILR_GROUP_ID   number(22,0) not null,
 RATE           number(22,8),
 EFFECTIVE_DATE date not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18)
);

alter table LS_ILR_GROUP_FLOATING_RATES
   add constraint PK_LS_ILR_GROUP_FLOATING_RATES
       primary key (ILR_GROUP_ID, EFFECTIVE_DATE)
       using index tablespace PWRPLANT_IDX;

alter table LS_ILR_GROUP_FLOATING_RATES
   add constraint FK_LS_ILR_GROUP_RATES1
       foreign key(ILR_GROUP_ID)
       references LS_ILR_GROUP(ILR_GROUP_ID);

comment on table LS_ILR_GROUP_FLOATING_RATES is '(S)  [01] A table that holds floating rates and effective dates for an ILR Group.';
comment on column LS_ILR_GROUP_FLOATING_RATES.ILR_GROUP_ID is 'Internal System ID for an ILR Group.';
comment on column LS_ILR_GROUP_FLOATING_RATES.RATE is 'The floating rate.';
comment on column LS_ILR_GROUP_FLOATING_RATES.EFFECTIVE_DATE is 'The date on which the rate takes effect.';
comment on column LS_ILR_GROUP_FLOATING_RATES.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LS_ILR_GROUP_FLOATING_RATES.USER_ID is 'Standard system-assigned user id used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (754, 0, 10, 4, 1, 2, 33366, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_033366_lease_floating_rate.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
