/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042752_sys_remove_api_tbls_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/05/2015 Daniel Mendel          API Kickouts should use PP Integration Kickouts
||============================================================================
*/

DELETE FROM pp_table_groups WHERE Lower(Trim(table_name)) = 'wo_interface_staging';

DELETE FROM powerplant_columns WHERE Lower(Trim(table_name)) = 'wo_interface_staging';

DELETE FROM powerplant_tables WHERE Lower(Trim(table_name)) = 'wo_interface_staging';

DELETE FROM pp_table_groups WHERE Lower(Trim(table_name)) = 'wo_interface_unit';

DELETE FROM powerplant_columns WHERE Lower(Trim(table_name)) = 'wo_interface_unit';

DELETE FROM powerplant_tables WHERE Lower(Trim(table_name)) = 'wo_interface_unit';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2264, 0, 2015, 1, 0, 0, 42752, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042752_sys_remove_api_tbls_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;