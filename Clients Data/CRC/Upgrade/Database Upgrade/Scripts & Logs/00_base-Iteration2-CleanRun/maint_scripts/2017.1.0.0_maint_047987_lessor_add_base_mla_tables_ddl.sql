/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047987_lessor_add_base_mla_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date     Revised By      Reason for Change
|| ---------- -------- --------------  ------------------------------------
|| 2017.1.0.0 9/5/2017 Jared Watkins   Create base lease, lease document, and lease class code tables for Lessor
||============================================================================
*/

--Add the Lessor Lease table for Lessor MLAs
create table lsr_lease(
  lease_id              number(22,0)   not null,
  time_stamp            date           null,
  user_id               varchar2(18)   null,
  lease_number          varchar2(35)   not null,
  description           varchar2(35)   not null,
  long_description      varchar2(254)  not null,
  lessee_id             number(22,0)   not null,
  lease_type_id         number(22,0)   not null,
  lease_status_id       number(22,0)   not null,
  lease_group_id        number(22,0)   null,
  lease_cap_type_id     number(22,0)   null,
  workflow_type_id      number(22,0)   null,
  contract_currency_id  number(22,0)   not null,
  payment_due_day       number(2,0)    not null,
  pre_payment_sw        number(1,0)    not null,
  master_agreement_date date           null,
  initiation_date       date           null,
  lease_end_date        date           null,
  days_in_year          number(3,0)    default 365 null,
  notes                 varchar2(4000) null
);

alter table lsr_lease
add constraint pk_lsr_lease
primary key (lease_id)
using index tablespace pwrplant_idx;

CREATE INDEX lsr_lease_contract_cur_idx
ON lsr_lease(contract_currency_id)
TABLESPACE pwrplant_idx;

CREATE UNIQUE INDEX lsr_lease_lease_num_idx
ON lsr_lease(lease_number)
TABLESPACE pwrplant_idx;

CREATE INDEX lsr_lease_upper_lease_num_idx
ON lsr_lease(UPPER(lease_number))
TABLESPACE pwrplant_idx;

ALTER TABLE lsr_lease
ADD CONSTRAINT fk_lsr_lease_lease_group
FOREIGN KEY (lease_group_id)
REFERENCES lsr_lease_group(lease_group_id);

ALTER TABLE lsr_lease
ADD CONSTRAINT fk_lsr_lease_lessee
FOREIGN KEY (lessee_id)
REFERENCES lsr_lessee(lessee_id);

ALTER TABLE lsr_lease
ADD CONSTRAINT fk_lsr_lease_status
FOREIGN KEY (lease_status_id)
REFERENCES ls_lease_status(lease_status_id);

ALTER TABLE lsr_lease
ADD CONSTRAINT fk_lsr_lease_type
FOREIGN KEY (lease_type_id)
REFERENCES ls_lease_type(lease_type_id);

ALTER TABLE lsr_lease
ADD CONSTRAINT fk_lsr_lease_contract_currency
FOREIGN KEY (contract_currency_id)
REFERENCES currency(currency_id);

COMMENT ON TABLE lsr_lease IS '(S)[06] The Lessor Lease table contains master lease agreements and riders as negotiated by the owning company and Lessee.';

COMMENT ON COLUMN lsr_lease.lease_id IS 'System-assigned identifier of a particular lease.';
COMMENT ON COLUMN lsr_lease.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_lease.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_lease.lease_number IS 'Identifier for a particular lease.  Lease number may accommodate alpha-numeric characters and embedded intelligence.';
COMMENT ON COLUMN lsr_lease.description IS 'Records a brief description of the lease.';
COMMENT ON COLUMN lsr_lease.long_description IS 'Records a more detailed description of the lease.';
COMMENT ON COLUMN lsr_lease.lessee_id IS 'System-assigned identifier of a particular Lessor.';
COMMENT ON COLUMN lsr_lease.lease_type_id IS 'System-assigned identifier of a particular lease type.';
COMMENT ON COLUMN lsr_lease.lease_status_id IS 'System-assigned identifier of a particular lease status.';
COMMENT ON COLUMN lsr_lease.lease_group_id IS 'Internal lease group id.';
COMMENT ON COLUMN lsr_lease.lease_cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';
COMMENT ON COLUMN lsr_lease.workflow_type_id IS 'An internal id used for routing records to approval.';
COMMENT ON COLUMN lsr_lease.contract_currency_id IS 'The contract currency used by this MLA and its underlying ILR and Assets.';
COMMENT ON COLUMN lsr_lease.payment_due_day IS 'Day of the month on which the lease payment is due to the Lessor.';
COMMENT ON COLUMN lsr_lease.pre_payment_sw IS 'Switch that can be set to yes or no to indicate whether or not the lease is paid early. 1 for yes and 0 for no';
COMMENT ON COLUMN lsr_lease.master_agreement_date IS 'Informational field to record the execute date of the Master Agreement.';
COMMENT ON COLUMN lsr_lease.initiation_date IS 'The date the lease is initiated.';
COMMENT ON COLUMN lsr_lease.lease_end_date IS 'End date for any associated ILR payment terms to be within';
COMMENT ON COLUMN lsr_lease.days_in_year IS 'User specified days in year for calculating interest';
COMMENT ON COLUMN lsr_lease.notes IS 'A freeform field for entering notes.';


--add the Lessor Lease Document table
create table lsr_lease_document(
  document_id   NUMBER(22,0)   NOT NULL,
  time_stamp    DATE           NULL,
  user_id       VARCHAR2(18)   NULL,
  lease_id      NUMBER(22,0)   NOT NULL,
  document_data BLOB           NULL,
  notes         VARCHAR2(2000) NULL,
  description   VARCHAR2(35)   NULL,
  file_name     VARCHAR2(100)  NOT NULL,
  filesize      NUMBER(22,0)   NULL,
  file_link     VARCHAR2(2000) NULL
);

ALTER TABLE lsr_lease_document
ADD CONSTRAINT pk_lsr_lease_document
PRIMARY KEY (
  lease_id,
  document_id)
USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE lsr_lease_document
ADD CONSTRAINT fk_lsr_lease_lease_document
FOREIGN KEY (lease_id)
REFERENCES lsr_lease(lease_id);

COMMENT ON TABLE lsr_lease_document IS '(S) [06] The Lessor Lease Document table holds documents associated with the Lessor lease (PDFs, DOC, XLS, etc).';

COMMENT ON COLUMN lsr_lease_document.document_id IS 'The internal document id within PowerPlant.';
COMMENT ON COLUMN lsr_lease_document.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_lease_document.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_lease_document.lease_id IS 'The internal lease id within PowerPlant .';
COMMENT ON COLUMN lsr_lease_document.document_data IS 'The stored document.';
COMMENT ON COLUMN lsr_lease_document.notes IS 'A freeform field for entering notes.';
COMMENT ON COLUMN lsr_lease_document.description IS 'a description field for the table.';
COMMENT ON COLUMN lsr_lease_document.file_name IS 'The file name.';
COMMENT ON COLUMN lsr_lease_document.filesize IS 'The size of the file.';
COMMENT ON COLUMN lsr_lease_document.file_link IS 'A link to an external file.';

--Add the Lessor Lease Class Code table
create table lsr_lease_class_code(
  class_code_id NUMBER(22,0)  NOT NULL,
  lease_id      NUMBER(22,0)  NOT NULL,
  time_stamp    DATE          NULL,
  user_id       VARCHAR2(18)  NULL,
  value         VARCHAR2(254) NOT NULL
);

ALTER TABLE lsr_lease_class_code
ADD CONSTRAINT pk_lsr_lease_class_code
PRIMARY KEY (
  lease_id,
  class_code_id)
USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE lsr_lease_class_code
ADD CONSTRAINT fk_lsr_lcc_lease_id
FOREIGN KEY (lease_id)
REFERENCES lsr_lease (lease_id);

ALTER TABLE lsr_lease_class_code
ADD CONSTRAINT fk_lsr_lcc_class_code_id
FOREIGN KEY (class_code_id)
REFERENCES class_code (class_code_id);

COMMENT ON TABLE lsr_lease_class_code IS '(S) [06] The Lessor Lease Class Code table allows additional information to be stored on the MLA.';

COMMENT ON COLUMN lsr_lease_class_code.class_code_id IS 'The internal class code id within PowerPlant.';
COMMENT ON COLUMN lsr_lease_class_code.lease_id IS 'The internal lease id within PowerPlant.';
COMMENT ON COLUMN lsr_lease_class_code.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_lease_class_code.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_lease_class_code.value IS 'The class code value.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3691, 0, 2017, 1, 0, 0, 47987, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047987_lessor_add_base_mla_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
