/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036324_reg_adj_alloc_result.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/17/2014 Sarah Byers
||============================================================================
*/

-- Table to store allocated adjustment amounts by target
create table REG_CASE_ADJUST_ALLOC_RESULT
(
 REG_CASE_ID             number(22,0) not null,
 ADJUSTMENT_ID           number(22,0) not null,
 CASE_YEAR               number(22,0) not null,
 REG_ACCT_ID             number(22,0) not null,
 REG_ALLOC_CATEGORY_ID   number(22,0) not null,
 REG_ALLOC_TARGET_ID     number(22,0) not null,
 PRIOR_STEP_TOTAL_AMOUNT number(22,2),
 ALLOC_PERCENT           number(22,8),
 ALLOC_AMOUNT            number(22,2),
 USER_ID                 varchar2(18),
 TIME_STAMP              date
);

alter table REG_CASE_ADJUST_ALLOC_RESULT
   add constraint PK_REG_CASE_ADJ_ALLOC_RESULT
       primary key (REG_CASE_ID, ADJUSTMENT_ID, CASE_YEAR, REG_ACCT_ID, REG_ALLOC_CATEGORY_ID, REG_ALLOC_TARGET_ID)
       using index tablespace PWRPLANT_IDX;

-- Foreign Keys
alter table REG_CASE_ADJUST_ALLOC_RESULT
   add constraint R_REG_CASE_ADJ_ALLOC_RESULT1
       foreign key (REG_CASE_ID)
       references REG_CASE (REG_CASE_ID);

alter table REG_CASE_ADJUST_ALLOC_RESULT
   add constraint R_REG_CASE_ADJ_ALLOC_RESULT2
       foreign key (REG_CASE_ID, ADJUSTMENT_ID)
       references REG_CASE_ADJUSTMENT (REG_CASE_ID, ADJUSTMENT_ID);

alter table REG_CASE_ADJUST_ALLOC_RESULT
   add constraint R_REG_CASE_ADJ_ALLOC_RESULT3
       foreign key (REG_CASE_ID, REG_ACCT_ID)
       references REG_CASE_ACCT (REG_CASE_ID, REG_ACCT_ID);

alter table REG_CASE_ADJUST_ALLOC_RESULT
   add constraint R_REG_CASE_ADJ_ALLOC_RESULT4
       foreign key (REG_ALLOC_CATEGORY_ID)
       references REG_ALLOC_CATEGORY (REG_ALLOC_CATEGORY_ID);

alter table REG_CASE_ADJUST_ALLOC_RESULT
   add constraint R_REG_CASE_ADJ_ALLOC_RESULT5
       foreign key (REG_ALLOC_TARGET_ID)
       references REG_ALLOC_TARGET (REG_ALLOC_TARGET_ID);

-- Comments
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.REG_CASE_ID is 'System-assigned identifier of a Regulatory Case.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.ADJUSTMENT_ID is 'System-assigned identifier of a Case Adjustment.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.CASE_YEAR is 'Month end of Case in YYYYMM format.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.REG_ACCT_ID is 'System-assigned identifier of a Regulatory Account.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.REG_ALLOC_CATEGORY_ID is 'System-assigned identifier of an Allocation Category.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.REG_ALLOC_TARGET_ID is 'System-assigned identifier of an Allocation Target.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.PRIOR_STEP_TOTAL_AMOUNT is 'Allocated Amount for the Regulatory Account from the prior allocation step.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.ALLOC_PERCENT is 'Allocated Percentage for the Regulatory Account equal to the ALLOC_AMOUNT divided by the PRIOR_STEP_TOTAL_AMOUNT.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.ALLOC_AMOUNT is 'Allocated Amount for the Regulatory Account for the current Category and Target.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.USER_ID is 'Standard system-assigned user id used for audit purposes.' ;
comment on column PWRPLANT.REG_CASE_ADJUST_ALLOC_RESULT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.' ;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (968, 0, 10, 4, 2, 0, 36324, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036324_reg_adj_alloc_result.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;