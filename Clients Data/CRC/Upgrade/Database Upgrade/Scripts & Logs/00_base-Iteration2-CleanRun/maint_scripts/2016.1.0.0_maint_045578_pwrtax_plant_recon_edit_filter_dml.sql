/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045578_pwrtax_plant_recon_edit_filter_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2016.1.0.0  03/31/2016 David Haupt    New filter for Plant Recon workspace
||============================================================================
*/

insert into pp_reports_filter
(pp_report_filter_id, description, filter_uo_name)
values
(99, 'PowerTax - Plant Recon Form Edit', 'uo_ppbase_tab_filter_dynamic');

--use the existing jurisdiction filter
insert into pp_dynamic_filter_mapping
(pp_report_filter_id, filter_id)
values
(99, 282);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3129, 0, 2016, 1, 0, 0, 45578, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045578_pwrtax_plant_recon_edit_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;