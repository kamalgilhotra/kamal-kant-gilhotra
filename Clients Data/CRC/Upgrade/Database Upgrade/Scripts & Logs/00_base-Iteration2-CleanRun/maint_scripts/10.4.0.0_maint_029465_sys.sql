/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029465_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   03/11/2013 Lee Quinn        Point Release
||============================================================================
*/

drop index WEM_UPDATE_W_ACT_COLS;
create index WEM_UPDATE_W_ACT_COLS
   on WO_EST_MONTHLY (WORK_ORDER_ID,
                      REVISION,
                      EXPENDITURE_TYPE_ID,
                      EST_CHG_TYPE_ID,
                      DECODE(UWA_INCLUDE_WO,1,NVL(WO_WORK_ORDER_ID,0),1),
                      DECODE(UWA_INCLUDE_JT,1,NVL(JOB_TASK_ID,'*'),'*'),
                      DECODE(UWA_INCLUDE_UA,1,NVL(UTILITY_ACCOUNT_ID,0),1),
                      DECODE(UWA_INCLUDE_DEPT,1,NVL(DEPARTMENT_ID,0),1))
      tablespace PWRPLANT_IDX;

drop index WE_ACTLS_WO_MN_ET_CT_JT_UA_DP;
create index WE_ACTLS_WO_MN_ET_CT_JT_UA_DP
   on WO_EST_ACTUALS_TEMP (WORK_ORDER_ID,
                           REVISION,
                           SUBSTR(MONTH_NUMBER,1,4),
                           EXPENDITURE_TYPE_ID,
                           EST_CHG_TYPE_ID,
                           DECODE(INCLUDE_JT,1,NVL(JOB_TASK_ID,'*'),'*'),
                           DECODE(INCLUDE_UA,1,NVL(UTILITY_ACCOUNT_ID,0),1),
                           DECODE(INCLUDE_DEPT,1,NVL(DEPARTMENT_ID,0),1),
                           DECODE(INCLUDE_WO, 1, NVL(WO_WORK_ORDER_ID, 0), 1));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (348, 0, 10, 4, 0, 0, 29465, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029465_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;