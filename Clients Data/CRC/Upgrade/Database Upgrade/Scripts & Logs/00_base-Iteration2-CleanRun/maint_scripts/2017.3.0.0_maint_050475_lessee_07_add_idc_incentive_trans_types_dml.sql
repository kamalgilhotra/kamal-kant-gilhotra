/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_07_add_idc_incentive_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/11/2018 Shane "C" Ward 	Add IDC and Incentive Trans Types for Lease
||============================================================================
*/

--New Trans Types
INSERT INTO je_trans_type (trans_type, description)
SELECT 3064, '3064 - Incentive Debit' FROM dual
UNION ALL
SELECT 3065, '3065 - Initial Direct Cost Credit' FROM dual;


--New Accruals
INSERT INTO ls_accrual_type (accrual_type_id, Description)
VALUES (26, 'Initial Direct Cost');

INSERT INTO ls_accrual_type (accrual_type_id, Description)
VALUES (27, 'Incentive Amount');

--Updates to ILR Import
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (252,
             'incentive_account_id',
             'Incentive Account',
             'incentive_account_xlate',
             0,
             2,
             'number(22,0)',
             'gl_account',
             1,
             'gl_account_id');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'incentive_account_id',
             601);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'incentive_account_id',
             602);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'incentive_account_id',
             603);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (252,
             'init_direct_cost_account_id',
             'Initial Direct Cost Account',
             'init_direct_cost_acct_xlate',
             0,
             2,
             'number(22,0)',
             'gl_account',
             1,
             'gl_account_id');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'init_direct_cost_account_id',
             601);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'init_direct_cost_account_id',
             602);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'init_direct_cost_account_id',
             603);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4624, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_07_add_idc_incentive_trans_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;