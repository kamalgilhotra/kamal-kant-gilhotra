/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049488_lessor_02_add_variable_payments_schema_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/20/2017 Jared Watkins  Add Lessor menu items/workspaces for Variable Payments
||============================================================================
*/

--create the table for Lessor Formula Components
create table lsr_formula_component (
	formula_component_id number(22,0) not null,
	user_id              varchar2(18),
	time_stamp           date,
	description          varchar2(35) not null,
	long_description     varchar2(254),
	component_type_id    number(22,0) not null,
	number_format_id     number(22,0) not null
);

create unique index lsr_formula_comp_desc_uidx
on lsr_formula_component( upper(trim(description)) )
tablespace pwrplant_idx;

alter table lsr_formula_component
add constraint lsr_formula_component_pk
primary key (formula_component_id)
using index tablespace pwrplant_idx;

alter table lsr_formula_component
add constraint lsr_formula_comp_num_fmt_fk
foreign key (number_format_id)
references number_format (number_format_id);

alter table lsr_formula_component
add constraint lsr_formula_comp_comp_type_fk
foreign key (component_type_id)
references ls_component_type (ls_component_type_id);

COMMENT ON TABLE lsr_formula_component IS '(S) [06] The Lessor Formula Component table holds the header-level information for all of the Lessor Variable Payment Formula Components created by the user';

COMMENT ON COLUMN lsr_formula_component.formula_component_id IS 'The system-assigned ID for user-created Lessor formula components.';
COMMENT ON COLUMN lsr_formula_component.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_formula_component.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_formula_component.description IS 'The name of the formula component.';
COMMENT ON COLUMN lsr_formula_component.long_description IS 'A full description of the formula component''s use/purpose.';
COMMENT ON COLUMN lsr_formula_component.component_type_id IS 'A reference to which type of Lessor formula component this item is.';
COMMENT ON COLUMN lsr_formula_component.number_format_id IS 'A reference to which type of number format the values for this formula component should take.';


--update LS_COMPONENT_TYPE table comment to indicate it is used by both modules
comment on table ls_component_type is '(F) [06] The LS Component Type table lists the types of Formula Component supported within the Lessee and Lessor Variable Payments functionality.';


--create the table for Lessor Index/Rate Component Amounts
create table lsr_index_component_values(
	formula_component_id number(22,0) not null,
	effective_date       date not null,
	user_id              varchar2(18),
	time_stamp           date,
	amount               number(28,8)
);

alter table lsr_index_component_values
add constraint lsr_index_component_values_pk
primary key (formula_component_id, effective_date)
using index tablespace pwrplant_idx;

alter table lsr_index_component_values
add constraint lsr_irc_values_component_fk
foreign key (formula_component_id)
references lsr_formula_component (formula_component_id);

COMMENT ON TABLE lsr_index_component_values IS '(S) [06] The Lessor Index Component Values table contains the effective-dated values entered for user-defined Index/Rate Formula Components.';

COMMENT ON COLUMN lsr_index_component_values.formula_component_id IS 'A reference to the formula component for which this index/rate is being entered';
COMMENT ON COLUMN lsr_index_component_values.effective_date IS 'The date from which the entered index/rate should be applied; is used for any dates from this value until another row is entered with a date that supercedes this.';
COMMENT ON COLUMN lsr_index_component_values.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_index_component_values.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_index_component_values.amount IS 'The numerical index/rate to be applied from the given date until another date supercedes it.';

--create the table for Lessor Variable Component Amounts
create table lsr_variable_component_values(
	formula_component_id  number(22,0) not null,
	ilr_id                number(22,0) not null,
	asset_id              number(22,0) not null,
  revision              number(22,0) not null,
	incurred_month_number number(6,0) not null,
	user_id               varchar2(18),
	time_stamp            date,
	amount                number(28,8)
);

alter table lsr_variable_component_values
add constraint lsr_var_component_values_pk
primary key (formula_component_id, ilr_id, asset_id, incurred_month_number)
using index tablespace pwrplant_idx;

alter table lsr_variable_component_values
add constraint lsr_vc_values_component_fk
foreign key (formula_component_id)
references lsr_formula_component (formula_component_id);

COMMENT ON TABLE lsr_variable_component_values IS '(S) [06] The Lessor Variable Component Values table stores the by-month values of a Formula Component for a given Lessor ILR/Asset combination.';

COMMENT ON COLUMN lsr_variable_component_values.formula_component_id IS 'A reference to the formula component for which the values will be stored.';
COMMENT ON COLUMN lsr_variable_component_values.ilr_id IS 'A reference to the Lessor ILR to which the Lessor Asset is related.';
COMMENT ON COLUMN lsr_variable_component_values.asset_id IS 'A reference to the Lessor Asset for which the value(s) are entered.';
COMMENT ON COLUMN lsr_variable_component_values.revision IS 'A reference to the Revision for the Asset/ILR.';
COMMENT ON COLUMN lsr_variable_component_values.incurred_month_number IS 'The month number (yyyymm format) in which the ''amount'' was incurred for the given Lessor Asset.';
COMMENT ON COLUMN lsr_variable_component_values.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_variable_component_values.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_variable_component_values.amount IS 'The specific value applied to the given lease asset for the month and formula component specified.';


--create the table for the Lessor Variable Component Calendar
create table lsr_var_component_calendar(
	formula_component_id number(22,0) not null,
	load_month_number    number(2,0) not null,
	user_id              varchar2(18),
	time_stamp           date,
	payment_month_number number(2,0) not null
);

alter table lsr_var_component_calendar
add constraint lsr_var_component_calendar_pk
primary key (formula_component_id, load_month_number)
using index tablespace pwrplant_idx;

alter table lsr_var_component_calendar
add constraint lsr_vc_calendar_component_fk
foreign key (formula_component_id)
references lsr_formula_component (formula_component_id);

COMMENT ON TABLE lsr_var_component_calendar IS '(F) [06] The Lessor Variable Component Calendar table is a relational table that associates a payment month to each calendar month for a given variable component.';

COMMENT ON COLUMN lsr_var_component_calendar.formula_component_id IS 'A reference to the variable component for which the load month and payment month will be stored.';
COMMENT ON COLUMN lsr_var_component_calendar.load_month_number IS 'The month number to store values for the load month.';
COMMENT ON COLUMN lsr_var_component_calendar.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_var_component_calendar.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_var_component_calendar.payment_month_number IS 'The month number to store values for the payment month';


--create the table for Lessor Variable Payments
create table lsr_variable_payment(
	variable_payment_id  number(22,0) not null,
	user_id              varchar2(18),
	time_stamp           date,
	description          varchar2(35) not null,
	long_description     varchar2(254) not null,
	active_for_new_ilrs  number(22,0) not null,
	payment_formula      varchar2(4000),
	payment_formula_db   varchar2(4000),
	asset_level_formula  number(1,0),
	preview_option       number(4,0) not null,
	preview_ilr          number(22,0),
	preview_revision     number(22,0),
	preview_set_of_books number(22,0)
);

create unique index lsr_var_payment_desc_uidx
on lsr_variable_payment( upper(trim(description)) )
tablespace pwrplant_idx;

alter table lsr_variable_payment
add constraint lsr_variable_payment_pk
primary key (variable_payment_id)
using index tablespace pwrplant_idx;

alter table lsr_variable_payment
add constraint lsr_vp_preview_ilr_rev_fk
foreign key (preview_ilr, preview_revision)
references lsr_ilr_approval (ilr_id, revision);

alter table lsr_variable_payment
add constraint lsr_vp_preview_sob_fk
foreign key (preview_set_of_books)
references set_of_books (set_of_books_id);

alter table lsr_variable_payment
add constraint lsr_vp_active_yes_no_fk
foreign key (active_for_new_ilrs)
references yes_no (yes_no_id);

COMMENT ON TABLE lsr_variable_payment IS '(S) [06] The Lessor Variable Payment table holds the header-information relating to a specific Variable Payment created by the user, as well as the current set of run options most recently selected and saved.';

COMMENT ON COLUMN lsr_variable_payment.variable_payment_id IS 'System assigned ID uniquely identifying this specific variable payment.';
COMMENT ON COLUMN lsr_variable_payment.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_variable_payment.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_variable_payment.description IS 'The name of the user-defined variable payment';
COMMENT ON COLUMN lsr_variable_payment.long_description IS 'A full description of the variable payment''s use/purpose.';
COMMENT ON COLUMN lsr_variable_payment.active_for_new_ilrs IS 'Field denoting whether this variable payment should appear as an option when assign variable payments to new ILRs. FKs off of the Yes/No Table';
COMMENT ON COLUMN lsr_variable_payment.payment_formula IS 'The user-friendly formula used to calculate the monthly payment values for ILRs using this variable payment';
COMMENT ON COLUMN lsr_variable_payment.payment_formula_db IS 'The actual DB-level formula used during calculations of the monthly values for this variable payment';
COMMENT ON COLUMN lsr_variable_payment.asset_level_formula IS 'A flag to denote whether this formula applies to the asset- or ILR-level. 0 = ILR Level; 1 = Asset Level; determined by the presence of any Variable Components or any Historic Components based on a VP that has an asset-level formula';
COMMENT ON COLUMN lsr_variable_payment.preview_option IS 'The number of months for which to calculate the formula preview; -1 to calculate until end of lease life.';
COMMENT ON COLUMN lsr_variable_payment.preview_ilr IS 'The ID of the ILR to use in the formula preview calculation.';
COMMENT ON COLUMN lsr_variable_payment.preview_revision IS 'The ID of the ILR Revision to use in the formula preview calculation.';
COMMENT ON COLUMN lsr_variable_payment.preview_set_of_books IS 'The ID of the Set of Books to use in the formula preview calculation.';

--because this is missing on the Lessee side
COMMENT ON COLUMN ls_variable_payment.payment_formula_db IS 'The actual DB-level formula used during calculations of the monthly values for this variable payment';


--create the relational table for Lessor Variable Payment Components
create table lsr_variable_payment_component(
	variable_payment_id 	number(22,0) not null,
	formula_component_id 	number(22,0) not null,
	user_id               varchar2(18),
	time_stamp            date
);

alter table lsr_variable_payment_component
add constraint lsr_var_payment_component_pk
primary key (variable_payment_id, formula_component_id)
using index tablespace pwrplant_idx;

alter table lsr_variable_payment_component
add constraint lsr_vp_comp_vp_id_fk
foreign key (variable_payment_id)
references lsr_variable_payment (variable_payment_id);

alter table lsr_variable_payment_component
add constraint lsr_vp_comp_component_id_fk
foreign key (formula_component_id)
references lsr_formula_component (formula_component_id);

COMMENT ON TABLE lsr_variable_payment_component IS '(S) [06] The Lessor Variable Payment Components table is a relational table showing which Formula Components have been related to a given Variable Payment.';

COMMENT ON COLUMN lsr_variable_payment_component.variable_payment_id IS 'A reference to the variable payment to which the formula component is related.';
COMMENT ON COLUMN lsr_variable_payment_component.formula_component_id IS 'A reference to the formula component being related to this variable payment';
COMMENT ON COLUMN lsr_variable_payment_component.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_variable_payment_component.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';


--add Fk that was missing since the table did not exist yet
alter table lsr_ilr_payment_term_var_pay
add constraint lsr_ilr_pay_term_vp_id_fk
foreign key (variable_payment_id)
references lsr_variable_payment (variable_payment_id);


--create the table for Lessor Historic Component Amounts
create table lsr_historic_component_values(
	formula_component_id number(22,0) not null,
	variable_payment_id  number(22,0) not null,
	user_id              varchar2(18),
	time_stamp           date
);

alter table lsr_historic_component_values
add constraint lsr_hist_component_values_pk
primary key (formula_component_id, variable_payment_id)
using index tablespace pwrplant_idx
;

alter table lsr_historic_component_values
add constraint lsr_hc_values_component_fk
foreign key (formula_component_id)
references lsr_formula_component (formula_component_id)
;

alter table lsr_historic_component_values
add constraint lsr_hc_values_vp_id_fk
foreign key (variable_payment_id)
references lsr_variable_payment (variable_payment_id)
;

COMMENT ON TABLE lsr_historic_component_values IS '(S) [06] The Lessor Historic Component Values table contains relates a Variable Payment ID to the Historical Component.';

COMMENT ON COLUMN lsr_historic_component_values.formula_component_id IS 'A reference to the formula component for which this Historic Lookback is being entered';
COMMENT ON COLUMN lsr_historic_component_values.variable_payment_id IS 'System assigned identifier for a variable payment';
COMMENT ON COLUMN lsr_historic_component_values.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_historic_component_values.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';


--create the table for the Lessor Historic Component Calendar
create table lsr_hist_component_calendar(
	formula_component_id number(22,0) not null,
	calc_month_number    number(2,0) not null,
	user_id              varchar2(18),
	time_stamp           date,
	timing               number(1,0) not null,
	payment_month_number number(2,0) not null
);

alter table lsr_hist_component_calendar
add constraint lsr_hist_component_calendar_pk
primary key (formula_component_id, calc_month_number)
using index tablespace pwrplant_idx;

alter table lsr_hist_component_calendar
add constraint lsr_hc_calendar_component_fk
foreign key (formula_component_id)
references lsr_formula_component (formula_component_id);

COMMENT ON TABLE lsr_hist_component_calendar IS '(F) [06] The Lessor Historic Component Calendar table associates a calculated month to each payment month for a given historic component.';

COMMENT ON COLUMN lsr_hist_component_calendar.formula_component_id IS 'A reference to the historic component for which the load month and payment month will be stored.';
COMMENT ON COLUMN lsr_hist_component_calendar.calc_month_number IS 'The month number to store values for the load month.';
COMMENT ON COLUMN lsr_hist_component_calendar.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_hist_component_calendar.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_hist_component_calendar.timing IS 'The timing of the calculated month number. 1 - Current Year, 2 - Previous Year, 3 - Payment Month.';
COMMENT ON COLUMN lsr_hist_component_calendar.payment_month_number IS 'The month number to store values for the payment month';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3803, 0, 2017, 1, 0, 0, 49488, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049488_lessor_02_add_variable_payments_schema_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;