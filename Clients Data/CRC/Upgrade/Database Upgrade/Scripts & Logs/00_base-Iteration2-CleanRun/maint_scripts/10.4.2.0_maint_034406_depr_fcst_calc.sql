/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034406_depr_fcst_calc.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   12/10/2013 Ryan Oliveria  Need to keep track of fcst version id
||                                      in depr calc
||============================================================================
*/

alter table DEPR_CALC_STG
   add FCST_DEPR_VERSION_ID number(22,0);

comment on column DEPR_CALC_STG.FCST_DEPR_VERSION_ID is 'The forecast depr version being calculated.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (811, 0, 10, 4, 2, 0, 34406, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034406_depr_fcst_calc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;