/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051048_lease_update_import_col_desc_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 05/04/2018 Anand R          PP-51048
||============================================================================
*/

update pp_import_column set 
    description = 'Est In Svc Date (YYYYMMDD)' 
where import_type_id = (select import_type_id from pp_import_type where description = 'Add: ILR''s ')
    and column_name = 'est_in_svc_date' ;
    
update pp_import_column set 
    description = 'Payment Term Date (YYYYMM)' 
where import_type_id = (select import_type_id from pp_import_type where description = 'Add: ILR''s ')
    and column_name = 'payment_term_date' ;
    
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5102, 0, 2017, 3, 0, 0, 51048, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051048_lease_update_import_col_desc_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;