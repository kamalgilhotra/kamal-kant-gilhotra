/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008843_retire_transaction.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   01/22/2013 Sunjin Cone    Point Release
||============================================================================
*/

--make ldg_asset_id not null
alter table RETIRE_TRANSACTION modify LDG_ASSET_ID number(22,0) null;

--add depr_group_id
alter table RETIRE_TRANSACTION add DEPR_GROUP_ID number(22,0);

--update depr_group_id with the value from cpr_ledger for the asset
update RETIRE_TRANSACTION A
   set DEPR_GROUP_ID =
        (select DEPR_GROUP_ID from CPR_LEDGER B where A.LDG_ASSET_ID = B.ASSET_ID);

update RETIRE_TRANSACTION A set DEPR_GROUP_ID = 0 where DEPR_GROUP_ID is null;

--now that depr_group_id is filled in, we can make it not null
alter table RETIRE_TRANSACTION modify DEPR_GROUP_ID number(22,0) not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (279, 0, 10, 4, 0, 0, 8843, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_008843_retire_transaction.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;