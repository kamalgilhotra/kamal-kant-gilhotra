/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011166_cr_system_control.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   03/29/2013 Marc Zawko      Point Release
||============================================================================
*/

insert into CR_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION)
values
   (268, 'CR BDG - Summarize Amt/Qty', 'Both',
    'There are three options when using the CR Budget Summarization tool to identify which type of values you want to summarize.  Both is the default: Both, Amount, Quantity.');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (386, 0, 10, 4, 1, 0, 11166, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011166_cr_system_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;