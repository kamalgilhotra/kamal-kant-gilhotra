/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042947_pcm_dynamic_filters_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/03/2015 Daniel Motter    Add PCM project manager dynamic filter
||============================================================================
*/

--Delete old mapping
delete from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 89 and filter_id = 254;

--Delete current mapping so no PK error is received if filter is already mapped
delete from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 89 and filter_id = 274;

--Delete old filter so no PK error is received if it already exists
delete from PP_DYNAMIC_FILTER where FILTER_ID = 274;

--Insert new pcm project manager dynamic filter
insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, USER_ID, TIME_STAMP) 
values
	(274, 'Project Manager', 'dw', 'upper(work_order_initiator.proj_mgr)', 
	'work_order_control.work_order_id in ( select distinct work_order_id from work_order_initiator where [selected_values] )',
	'dw_pp_proj_mgr_filter', 'proj_mgr', 'description', 'C', user, sysdate);

--Insert new mapping
insert into PP_DYNAMIC_FILTER_MAPPING 
	(PP_REPORT_FILTER_ID, FILTER_ID) 
values 
	(89, 274);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2337, 0, 2015, 1, 0, 0, 42947, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042947_pcm_dynamic_filters_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;