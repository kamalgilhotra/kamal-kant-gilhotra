/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030217_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.1   06/12/2013 Alex P.         Point Release
||============================================================================
*/

update CWIP_CHARGE A
   set STATUS =
        (select B.REPAIR_SCHEMA_ID
           from REPAIR_CALC_CWIP B
          where A.CHARGE_ID = B.CHARGE_ID
            and B.STATUS = 'Posted')
 where A.CHARGE_ID in (select CHARGE_ID from REPAIR_CALC_CWIP where STATUS = 'Posted')
   and A.STATUS is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (402, 0, 10, 4, 0, 1, 30217, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_030217_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;