/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046634_depr_add_company_id_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0  10/17/2016 Anand R        Add company_id column to two tables.
||============================================================================
*/

alter table CPR_DEPR_CALC_NURV_STG 
    add company_id number(22,0);

alter table CPR_DEPR_CALC_CFNU_STG 
    add company_id number(22,0);

	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3319, 0, 2016, 1, 0, 0, 046634, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046634_depr_add_company_id_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;