/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046825_reg_status_check_missing_allocator_2_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.1.0 12/20/2016 Sarah Byers		Add status checks
||============================================================================
*/

update reg_status_check
	set reg_case_process_id = 10
 where reg_case_process_id = 9
	and reg_process_step_id = 4
	and lower(description) like '%allocator%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3352, 0, 2016, 1, 1, 0, 046825, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.1.0_maint_046825_reg_status_check_missing_allocator_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	
