--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_043529_projects_syscntrl_desc_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     04/10/2015 Andrew Scott     Change the long descriptions of a couple of projects
--||                                        system controls.
--||============================================================================
--*/


update pp_system_control_company
set long_description = 'for funding projects only; "No" indicates that the FP number is auto generated, and it cannot be edited. '||
   '"Yes" indicates that the FP number is auto generated, but it can be edited.  The default is set to "No."'
where upper(trim(control_name)) = upper(trim('Override Edit Auto Gen FP Num'));
--original value : for funding projects only; no allows editing of the work order number 
--       in w_wo_entry and is the default value; yes prevents any edits when the value has been auto generated
--new value : for funding projects only; "No" indicates that the FP number is auto generated, and it cannot be edited.
--       "Yes" indicates that the FP number is auto generated, but it can be edited.  The default is set to "No."

update pp_system_control_company
set long_description = 'for work orders only; "No" indicates that the WO number is auto generated, and it cannot be edited. '||
   '"Yes" indicates that the WO number is auto generated, but it can be edited.  The default is set to "No."'
where upper(trim(control_name)) = upper(trim('Override Edit Auto Gen WO Num'));
--original value : for work orders only; no allows editing of the work order number in w_wo_entry and 
--       is the default value; yes prevents any edits when the value has been auto generated
--new value : for work orders only; "No" indicates that the WO number is auto generated, and it cannot be edited.
--       "Yes" indicates that the WO number is auto generated, but it can be edited.  The default is set to "No."


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2490, 0, 2015, 1, 0, 0, 43529, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043529_projects_syscntrl_desc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;