/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011106_pend_trans_set_of_books.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   01/15/2013 Sunjin Cone    Point Release
||============================================================================
*/

create table PEND_TRANSACTION_SET_OF_BOOKS
(
 PEND_TRANS_ID             number(22,0),
 SET_OF_BOOKS_ID           number(22,0),
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 ACTIVITY_CODE             char(5),
 ADJUSTED_COST_OF_REMOVAL  number(22,2),
 ADJUSTED_SALVAGE_CASH     number(22,2),
 ADJUSTED_SALVAGE_RETURNS  number(22,2),
 GAIN_LOSS                 number(22,2),
 RESERVE                   number(22,2),
 FERC_ACTIVITY_CODE        number(22,0),
 ADJUSTED_RESERVE_CREDITS  number(22,2),
 REPLACEMENT_AMOUNT        number(22,2),
 GAIN_LOSS_REVERSAL        number(22,2),
 POSTING_AMOUNT            number(22,2),
 POSTING_QUANTITY          number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT number(22,2)
);

alter table PEND_TRANSACTION_SET_OF_BOOKS
   add constraint PEND_TRANSACTION_SOB_PK
       primary key (PEND_TRANS_ID, SET_OF_BOOKS_ID)
       using index tablespace PWRPLANT_IDX;

create table PEND_TRANSACTION_SOB_ARC
(
 PEND_TRANS_ID             number(22,0),
 SET_OF_BOOKS_ID           number(22,0),
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 ACTIVITY_CODE             char(5),
 ADJUSTED_COST_OF_REMOVAL  number(22,2),
 ADJUSTED_SALVAGE_CASH     number(22,2),
 ADJUSTED_SALVAGE_RETURNS  number(22,2),
 GAIN_LOSS                 number(22,2),
 RESERVE                   number(22,2),
 FERC_ACTIVITY_CODE        number(22,0),
 ADJUSTED_RESERVE_CREDITS  number(22,2),
 REPLACEMENT_AMOUNT        number(22,2),
 GAIN_LOSS_REVERSAL        number(22,2),
 POSTING_AMOUNT            number(22,2),
 POSTING_QUANTITY          number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT number(22,2)
);

alter table PEND_TRANSACTION_SOB_ARC
   add constraint PEND_TRANSACTION_SOB_ARC_PK
       primary key (PEND_TRANS_ID, SET_OF_BOOKS_ID)
       using index tablespace PWRPLANT_IDX;

alter table PEND_TRANSACTION add ADJUSTED_COST_OF_REMOVAL number(22,2);
alter table PEND_TRANSACTION add ADJUSTED_SALVAGE_CASH    number(22,2);
alter table PEND_TRANSACTION add ADJUSTED_SALVAGE_RETURNS number(22,2);
alter table PEND_TRANSACTION add ADJUSTED_RESERVE_CREDITS number(22,2);

alter table PEND_TRANSACTION_ARCHIVE add ADJUSTED_COST_OF_REMOVAL number(22,2);
alter table PEND_TRANSACTION_ARCHIVE add ADJUSTED_SALVAGE_CASH    number(22,2);
alter table PEND_TRANSACTION_ARCHIVE add ADJUSTED_SALVAGE_RETURNS number(22,2);
alter table PEND_TRANSACTION_ARCHIVE add ADJUSTED_RESERVE_CREDITS number(22,2);

--
-- GET_CPR_SOB_AMT
--
create or replace function GET_CPR_SOB_AMT(P_ASSET_ID        number,
                                           P_SET_OF_BOOKS_ID number,
                                           P_FACTOR          number) return number is
   V_RTN      number(22, 2);
   V_NUMBASIS number;
   V_SOB_AMT  number(22, 2);
   V_SQLS     varchar2(4000);
   type CUR_TYP is ref cursor;
   C CUR_TYP;

begin
   --Check that Pend Transaction exists
   select count(1) into V_RTN from CPR_LEDGER where ASSET_ID = P_ASSET_ID;

   if V_RTN < 1 then
      RAISE_APPLICATION_ERROR(-10801,
                              'No Asset Found in Get CPR SOB AMT Function (asset_id: ' ||
                              P_ASSET_ID || ')');
   end if;

   select max(BOOK_SUMMARY_ID) into V_NUMBASIS from BOOK_SUMMARY;

   V_SOB_AMT := 0;

   for I in 1 .. V_NUMBASIS
   loop
      V_SQLS := 'select sum(nvl(basis_' || TO_CHAR(I) || ',0) * nvl(b.basis_' || TO_CHAR(I) ||
                '_indicator,0)) * ' || P_FACTOR ||
                ' basis_amount from cpr_ldg_basis a, set_of_books b where a.asset_id = ' ||
                TO_CHAR(P_ASSET_ID) || ' and b.set_of_books_id = ' || TO_CHAR(P_SET_OF_BOOKS_ID);
      DBMS_OUTPUT.PUT_LINE(V_SQLS);
      open C for V_SQLS;
      loop
         fetch C
            into V_RTN;
         exit when C%notfound;
         -- process row here
      end loop;
      close C;
      V_SOB_AMT := V_SOB_AMT + V_RTN;
   end loop;

   return V_SOB_AMT;
end;
/

--
-- GET_CPR_SOB_RESERVE
--
create or replace function GET_CPR_SOB_RESERVE(P_ASSET_ID        number,
                                               P_SET_OF_BOOKS_ID number,
                                               P_FACTOR          number,
                                               P_FOR_MONTH       date) return number is
   V_RTN                number(22, 2);
   V_FACTOR             number(22, 8);
   V_SUBLEDGER_IND      number(22, 0);
   V_FERC_ACTIVITY_CODE number(22, 0);
   V_RESERVE_AMT        number(22, 2);

begin

   select count(1) into V_RTN from CPR_LEDGER where ASSET_ID = P_ASSET_ID;

   if V_RTN < 1 then
      RAISE_APPLICATION_ERROR(-10801,
                              'No Asset Found in Get CPR SOB Resrve Function (asset_id: ' ||
                              P_ASSET_ID || ')');
   end if;

   select NVL(SUBLEDGER_INDICATOR, 0)
     into V_SUBLEDGER_IND
     from CPR_LEDGER
    where ASSET_ID = P_ASSET_ID;

   if V_SUBLEDGER_IND = 0 then
      --If group depreciation then get the allocation factors * sob amount
      select count(1)
        into V_RTN
        from CPR_LEDGER A, DEPR_RES_ALLO_FACTORS B
       where A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
         and TO_NUMBER(TO_CHAR(A.ENG_IN_SERVICE_YEAR, 'YYYY')) = B.VINTAGE
         and A.ASSET_ID = P_ASSET_ID
         and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
         and B.MONTH = P_FOR_MONTH;
      if V_RTN = 1 then
         select ROUND(B.FACTOR * GET_CPR_SOB_AMT(P_ASSET_ID, P_SET_OF_BOOKS_ID, P_FACTOR), 2)
           into V_RESERVE_AMT
           from CPR_LEDGER A, DEPR_RES_ALLO_FACTORS B
          where A.DEPR_GROUP_ID = B.DEPR_GROUP_ID
            and TO_NUMBER(TO_CHAR(A.ENG_IN_SERVICE_YEAR, 'YYYY')) = B.VINTAGE
            and A.ASSET_ID = P_ASSET_ID
            and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and B.MONTH = P_FOR_MONTH;
      else
         RAISE_APPLICATION_ERROR(-10802,
                                 'Depr Res Allo Factors not found for asset (asset_id: ' ||
                                 P_ASSET_ID || ')');
      end if;
   else
      --Individually depreciated get from cpr depr by pct.
      select count(1)
        into V_RTN
        from CPR_DEPR
       where ASSET_ID = P_ASSET_ID
         and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
         and GL_POSTING_MO_YR = P_FOR_MONTH;
      if V_RTN = 1 then
         select ROUND(BEG_RESERVE_MONTH * P_FACTOR, 2)
           into V_RESERVE_AMT
           from CPR_DEPR
          where ASSET_ID = P_ASSET_ID
            and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and GL_POSTING_MO_YR = P_FOR_MONTH;
      else
         RAISE_APPLICATION_ERROR(-10803,
                                 'No CPR Depr Found in Get CPR SOB Resrve Function (asset_id: ' ||
                                 P_ASSET_ID || ', set of book: ' || P_SET_OF_BOOKS_ID || ')');
      end if;
   end if;

   return V_RESERVE_AMT;
exception
   when others then
      rollback;
end;
/

--
-- GET_PEND_SOB_AMT
--
create or replace function GET_PEND_SOB_AMT(P_PEND_TRANS_ID   number,
                                            P_SET_OF_BOOKS_ID number) return number is
   V_RTN           number(22, 2);
   V_NUMBASIS      number;
   V_SOB_AMT       number(22, 2);
   V_SQLS          varchar2(4000);
   V_FACTOR        number(22, 8);
   V_ACTIVITY_CODE varchar2(10);
   V_FERC_ACT_CODE number(22, 0);
   V_ASSET_ID      number;
   V_SIGN          number(22, 0);
   V_MONTH         date;
   type CUR_TYP is ref cursor;
   C CUR_TYP;

begin
   --Check that Pend Transaction exists
   select count(1) into V_RTN from PEND_TRANSACTION where PEND_TRANS_ID = P_PEND_TRANS_ID;

   if V_RTN < 1 then
      return 0;
   end if;

   select UPPER(trim(ACTIVITY_CODE)), FERC_ACTIVITY_CODE
     into V_ACTIVITY_CODE, V_FERC_ACT_CODE
     from PEND_TRANSACTION
    where PEND_TRANS_ID = P_PEND_TRANS_ID;

   case V_ACTIVITY_CODE
      when 'UTRT' then
         select count(1)
           into V_RTN
           from PEND_TRANSACTION UTRT, PEND_TRANSACTION UTRF, CPR_LEDGER CPR
          where UTRT.LDG_ASSET_ID = UTRF.PEND_TRANS_ID
            and CPR.ASSET_ID = UTRF.LDG_ASSET_ID
            and UTRT.PEND_TRANS_ID = P_PEND_TRANS_ID;
         if V_RTN > 0 then
            -- Asset Found, get asset id from the FROM asset and get the factor and month
            select UTRF.LDG_ASSET_ID, (UTRF.POSTING_AMOUNT) / ACCUM_COST, UTRF.GL_POSTING_MO_YR
              into V_ASSET_ID, V_FACTOR, V_MONTH
              from PEND_TRANSACTION UTRT, PEND_TRANSACTION UTRF, CPR_LEDGER CPR
             where UTRT.LDG_ASSET_ID = UTRF.PEND_TRANS_ID
               and CPR.ASSET_ID = UTRF.LDG_ASSET_ID
               and UTRT.PEND_TRANS_ID = P_PEND_TRANS_ID;
            -- Call function to get sob amount * sign on this posting amount
            select GET_CPR_SOB_AMT(V_ASSET_ID, P_SET_OF_BOOKS_ID, V_FACTOR) * -1
              into V_SOB_AMT
              from PEND_TRANSACTION
             where PEND_TRANS_ID = P_PEND_TRANS_ID;
            -- We are done. Return it
            return V_SOB_AMT;
         end if;
      else

         if V_FERC_ACT_CODE = 2 then
            V_SIGN := -1;
         else
            V_SIGN := 1;
         end if;
         -- See if we have the asset id
         select count(1)
           into V_RTN
           from PEND_TRANSACTION A, CPR_LEDGER CPR
          where A.LDG_ASSET_ID = CPR.ASSET_ID
            and PEND_TRANS_ID = P_PEND_TRANS_ID;

         if V_RTN > 0 then
            -- Asset Found, get asset id from the FROM asset and get the factor and month
            select LDG_ASSET_ID, (POSTING_AMOUNT * V_SIGN) / ACCUM_COST, GL_POSTING_MO_YR
              into V_ASSET_ID, V_FACTOR, V_MONTH
              from PEND_TRANSACTION A, CPR_LEDGER CPR
             where A.LDG_ASSET_ID = CPR.ASSET_ID
               and PEND_TRANS_ID = P_PEND_TRANS_ID;
            -- Call function to get sob amount * sign on this posting amount
            select GET_CPR_SOB_AMT(V_ASSET_ID, P_SET_OF_BOOKS_ID, V_FACTOR) * V_SIGN
              into V_SOB_AMT
              from PEND_TRANSACTION
             where PEND_TRANS_ID = P_PEND_TRANS_ID;
            -- We are done. Return it
            return V_SOB_AMT;
         end if;
   end case;
   -- if we get to this point, we cannot find the asset id on ldg_asset_id, Use basis to get the SOB amt

   select max(BOOK_SUMMARY_ID) into V_NUMBASIS from BOOK_SUMMARY;

   V_SOB_AMT := 0;
   --If currently we do not have any pend_basis. Don't go through this...
   select count(1) into V_RTN from PEND_BASIS where PEND_TRANS_ID = P_PEND_TRANS_ID;
   if V_RTN < 1 then
      return V_SOB_AMT;
   end if;

   for I in 1 .. V_NUMBASIS
   loop
      V_SQLS := 'select sum(nvl(basis_' || TO_CHAR(I) || ',0) * nvl(b.basis_' || TO_CHAR(I) ||
                '_indicator,0)) basis_amount from pend_basis a, set_of_books b where a.pend_trans_id = ' ||
                TO_CHAR(P_PEND_TRANS_ID) || ' and b.set_of_books_id = ' ||
                TO_CHAR(P_SET_OF_BOOKS_ID);
      DBMS_OUTPUT.PUT_LINE(V_SQLS);
      open C for V_SQLS;
      loop
         fetch C
            into V_RTN;
         DBMS_OUTPUT.PUT_LINE(V_RTN);
         exit when C%notfound;
         -- process row here
      end loop;
      close C;
      V_SOB_AMT := V_SOB_AMT + V_RTN;
      DBMS_OUTPUT.PUT_LINE(V_SOB_AMT);
   end loop;

   return V_SOB_AMT;
end;
/

--
-- GET_PEND_SOB_RESERVE
--
create or replace function GET_PEND_SOB_RESERVE(P_PEND_TRANS_ID   number,
                                                P_SET_OF_BOOKS_ID number) return number is
   V_RTN                number(22, 2);
   V_ASSET_ID           number(22, 0);
   V_MONTH              date;
   V_FACTOR             number(22, 8);
   V_SUBLEDGER_IND      number(22, 0);
   V_FERC_ACTIVITY_CODE number(22, 0);
   V_ACTIVITY_CODE      varchar2(10);
   V_RESERVE_AMT        number(22, 2);

begin

   select NVL(SUBLEDGER_INDICATOR, 0), FERC_ACTIVITY_CODE, UPPER(trim(ACTIVITY_CODE))
     into V_SUBLEDGER_IND, V_FERC_ACTIVITY_CODE, V_ACTIVITY_CODE
     from PEND_TRANSACTION
    where PEND_TRANS_ID = P_PEND_TRANS_ID;

   case V_FERC_ACTIVITY_CODE
      when 2 then
         --Retirement
         -- See if we have the asset id
         select count(1)
           into V_RTN
           from PEND_TRANSACTION A, CPR_LEDGER CPR
          where A.LDG_ASSET_ID = CPR.ASSET_ID
            and PEND_TRANS_ID = P_PEND_TRANS_ID;

         if V_RTN > 0 then
            -- Asset Found, get asset id from the FROM asset and get the factor and month
            select LDG_ASSET_ID, (POSTING_AMOUNT * -1) / ACCUM_COST, GL_POSTING_MO_YR
              into V_ASSET_ID, V_FACTOR, V_MONTH
              from PEND_TRANSACTION A, CPR_LEDGER CPR
             where A.LDG_ASSET_ID = CPR.ASSET_ID
               and PEND_TRANS_ID = P_PEND_TRANS_ID;
            -- Call function to get sob amount * sign on this posting amount
            select GET_CPR_SOB_RESERVE(V_ASSET_ID, P_SET_OF_BOOKS_ID, V_FACTOR, V_MONTH)
              into V_RESERVE_AMT
              from PEND_TRANSACTION
             where PEND_TRANS_ID = P_PEND_TRANS_ID;
            -- We are done. Return it
            return V_RESERVE_AMT;
         else
            return 0;
         end if;
      when 4 then
         -- Transfer
         if V_ACTIVITY_CODE = 'UTRT' then
            -- See if we have the asset id
            select count(1)
              into V_RTN
              from PEND_TRANSACTION UTRT, PEND_TRANSACTION UTRF, CPR_LEDGER CPR
             where UTRT.LDG_ASSET_ID = UTRF.PEND_TRANS_ID
               and CPR.ASSET_ID = UTRF.LDG_ASSET_ID
               and UTRT.PEND_TRANS_ID = P_PEND_TRANS_ID;

            if V_RTN > 0 then
               -- Asset Found, get asset id from the FROM asset and get the factor and month
               select UTRF.LDG_ASSET_ID,
                      (UTRF.POSTING_AMOUNT * -1) / ACCUM_COST,
                      UTRF.GL_POSTING_MO_YR
                 into V_ASSET_ID, V_FACTOR, V_MONTH
                 from PEND_TRANSACTION UTRT, PEND_TRANSACTION UTRF, CPR_LEDGER CPR
                where UTRT.LDG_ASSET_ID = UTRF.PEND_TRANS_ID
                  and CPR.ASSET_ID = UTRF.LDG_ASSET_ID
                  and UTRT.PEND_TRANS_ID = P_PEND_TRANS_ID;
               -- Call function to get sob amount * sign on this posting amount
               select GET_CPR_SOB_RESERVE(V_ASSET_ID, P_SET_OF_BOOKS_ID, V_FACTOR, V_MONTH)
                 into V_RESERVE_AMT
                 from PEND_TRANSACTION
                where PEND_TRANS_ID = P_PEND_TRANS_ID;
               -- We are done. Return it
               return V_RESERVE_AMT;
            end if;
         else
            -- See if we have the asset id
            select count(1)
              into V_RTN
              from PEND_TRANSACTION A, CPR_LEDGER CPR
             where A.LDG_ASSET_ID = CPR.ASSET_ID
               and PEND_TRANS_ID = P_PEND_TRANS_ID;

            if V_RTN > 0 then
               -- Asset Found, get asset id from the FROM asset and get the factor and month
               select LDG_ASSET_ID, POSTING_AMOUNT / ACCUM_COST, GL_POSTING_MO_YR
                 into V_ASSET_ID, V_FACTOR, V_MONTH
                 from PEND_TRANSACTION A, CPR_LEDGER CPR
                where A.LDG_ASSET_ID = CPR.ASSET_ID
                  and PEND_TRANS_ID = P_PEND_TRANS_ID;
               -- Call function to get sob amount * sign on this posting amount
               select GET_CPR_SOB_RESERVE(V_ASSET_ID, P_SET_OF_BOOKS_ID, V_FACTOR, V_MONTH)
                 into V_RESERVE_AMT
                 from PEND_TRANSACTION
                where PEND_TRANS_ID = P_PEND_TRANS_ID;
               -- We are done. Return it
               return V_RESERVE_AMT;
            else
               return 0;
            end if;
         end if;
      else
         --Case else
         return 0;
   end case;
end;
/

--
-- GET_PEND_SOB_GL
--
create or replace function GET_PEND_SOB_GL(P_PEND_TRANS_ID   number,
                                           P_SET_OF_BOOKS_ID number,
                                           P_RESERVE         number,
                                           P_POSTING_AMT     number) return number is
   V_RTN                number(22, 2);
   V_SUBLEDGER_IND      number(22, 0);
   V_ACTIVITY_CODE      varchar2(10);
   V_FERC_ACTIVITY_CODE number(22, 0);
   V_GL_DEFAULT         number(22, 0);
   V_GL_AMT             number(22, 2);
   V_PCT_REM            number(22, 8);

begin
   V_GL_AMT := 0;
   select NVL(SUBLEDGER_INDICATOR, 0), trim(ACTIVITY_CODE), FERC_ACTIVITY_CODE
     into V_SUBLEDGER_IND, V_ACTIVITY_CODE, V_FERC_ACTIVITY_CODE
     from PEND_TRANSACTION
    where PEND_TRANS_ID = P_PEND_TRANS_ID;
   case V_FERC_ACTIVITY_CODE
      when 2 then
         --Get the percent removal Salvage
         select NVL(PERCENT_REMOVAL_SALVAGE, 0)
           into V_PCT_REM
           from COMPANY_SET_OF_BOOKS A, PEND_TRANSACTION B
          where A.COMPANY_ID = B.COMPANY_ID
            and SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
            and PEND_TRANS_ID = P_PEND_TRANS_ID;
         if V_SUBLEDGER_IND = 0 then
            --Check if gain loss default for set of books if it's set to 0 then return 0
            select count(1)
              into V_RTN
              from DEPR_GROUP A, DEPR_METHOD_RATES B, PEND_TRANSACTION C
             where A.DEPR_GROUP_ID = C.LDG_DEPR_GROUP_ID
               and A.DEPR_METHOD_ID = B.DEPR_METHOD_ID
               and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
               and PEND_TRANS_ID = P_PEND_TRANS_ID
               and B.EFFECTIVE_DATE =
                   (select max(EFFECTIVE_DATE)
                      from DEPR_METHOD_RATES M
                     where M.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                       and M.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
                       and M.EFFECTIVE_DATE <= C.GL_POSTING_MO_YR);
            if V_RTN > 0 then
               select NVL(B.GAIN_LOSS_DEFAULT, 0)
                 into V_GL_DEFAULT
                 from DEPR_GROUP A, DEPR_METHOD_RATES B, PEND_TRANSACTION C
                where A.DEPR_GROUP_ID = C.LDG_DEPR_GROUP_ID
                  and A.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                  and B.SET_OF_BOOKS_ID = P_SET_OF_BOOKS_ID
                  and PEND_TRANS_ID = P_PEND_TRANS_ID
                  and B.EFFECTIVE_DATE =
                      (select max(EFFECTIVE_DATE)
                         from DEPR_METHOD_RATES M
                        where M.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                          and M.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID
                          and M.EFFECTIVE_DATE <= C.GL_POSTING_MO_YR);
               if V_GL_DEFAULT = 1 then
                  -- calculate gain_loss
                  select NVL((SALVAGE_RETURNS * V_PCT_REM + SALVAGE_CASH * V_PCT_REM +
                             COST_OF_REMOVAL * V_PCT_REM + RESERVE_CREDITS * V_PCT_REM + P_RESERVE +
                             P_POSTING_AMT) * -1,
                             0)
                    into V_GL_AMT
                    from PEND_TRANSACTION
                   where PEND_TRANS_ID = P_PEND_TRANS_ID;
               else
                  V_GL_AMT := 0;
               end if;
            else
               V_GL_AMT := 0;
            end if;
         else
            -- Get CPR Depr G/L
            select NVL((SALVAGE_RETURNS * V_PCT_REM + SALVAGE_CASH * V_PCT_REM +
                       COST_OF_REMOVAL * V_PCT_REM + RESERVE_CREDITS * V_PCT_REM + P_RESERVE +
                       P_POSTING_AMT) * -1,
                       0)
              into V_GL_AMT
              from PEND_TRANSACTION
             where PEND_TRANS_ID = P_PEND_TRANS_ID;
         end if;
      else
         return 0;
   end case;
   return V_GL_AMT;
end;
/

--
-- PEND_TRANS_ADD_SOB
--
create or replace trigger PEND_TRANS_ADD_SOB
   after update or insert on PEND_BASIS
   for each row
declare
   V_RTN              number(22, 0);
   V_DEPR_GROUP_ID    number(22, 0);
   V_ASSET_ID         number(22, 0);
   V_VINTAGE          number(22, 0);
   V_GL_POSTING_MO_YR date;
   V_ACTIVITY_CODE    varchar2(10);

begin
   update PEND_TRANSACTION
      set (ADJUSTED_COST_OF_REMOVAL,
            ADJUSTED_SALVAGE_CASH,
            ADJUSTED_SALVAGE_RETURNS,
            ADJUSTED_RESERVE_CREDITS) =
           (select A.COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE,
                   A.RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE
              from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
             where A.COMPANY_ID = B.COMPANY_ID
               and B.SET_OF_BOOKS_ID = 1
               and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID)
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   insert into PEND_TRANSACTION_SET_OF_BOOKS
      (PEND_TRANS_ID, SET_OF_BOOKS_ID, ACTIVITY_CODE, ADJUSTED_COST_OF_REMOVAL,
       ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, GAIN_LOSS, RESERVE, FERC_ACTIVITY_CODE,
       ADJUSTED_RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, POSTING_AMOUNT,
       POSTING_QUANTITY, IMPAIRMENT_EXPENSE_AMOUNT)
      select PEND_TRANS_ID,
             SET_OF_BOOKS_ID,
             ACTIVITY_CODE,
             COST_OF_REMOVAL,
             SALVAGE_CASH,
             SALVAGE_RETURNS,
             GET_PEND_SOB_GL(PEND_TRANS_ID, SET_OF_BOOKS_ID, RESERVE, POSTING_AMOUNT) GAIN_LOSS,
             RESERVE,
             FERC_ACTIVITY_CODE,
             RESERVE_CREDITS,
             REPLACEMENT_AMOUNT,
             GAIN_LOSS_REVERSAL,
             POSTING_AMOUNT,
             POSTING_QUANTITY,
             IMPAIRMENT_EXPENSE_AMOUNT
        from (select A.PEND_TRANS_ID,
                     B.SET_OF_BOOKS_ID,
                     ACTIVITY_CODE,
                     NVL(COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE, 0) COST_OF_REMOVAL,
                     NVL(SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_CASH,
                     NVL(SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_RETURNS,
                     GET_PEND_SOB_RESERVE(PEND_TRANS_ID, B.SET_OF_BOOKS_ID) RESERVE,
                     FERC_ACTIVITY_CODE,
                     NVL(RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE, 0) RESERVE_CREDITS,
                     NVL(REPLACEMENT_AMOUNT, 0) REPLACEMENT_AMOUNT,
                     NVL(GAIN_LOSS_REVERSAL, 0) GAIN_LOSS_REVERSAL,
                     (select NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                             NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                             NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                             NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                             NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                             NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                             NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                             NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                             NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                             NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                             NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                             NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                             NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                             NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                             NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                             NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                             NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                             NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                             NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                             NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                             NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                             NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                             NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                             NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                             NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                             NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                             NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                             NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                             NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                             NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                             NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                             NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                             NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                             NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                             NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                             NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                             NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                             NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                             NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                             NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                             NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                             NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                             NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                             NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                             NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                             NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                             NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                             NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                             NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                             NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                             NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                             NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                             NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                             NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                             NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                             NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                             NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                             NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                             NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                             NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                             NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                             NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                             NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                             NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                             NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                             NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                             NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                             NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                             NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                             NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR
                        from SET_OF_BOOKS
                       where SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID) POSTING_AMOUNT,
                     NVL(POSTING_QUANTITY, 0) POSTING_QUANTITY,
                     NVL(IMPAIRMENT_EXPENSE_AMOUNT, 0) IMPAIRMENT_EXPENSE_AMOUNT
                from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
               where A.COMPANY_ID = B.COMPANY_ID
                 and B.SET_OF_BOOKS_ID <> 1
                 and LOWER(trim(DECODE(A.FERC_ACTIVITY_CODE, 2, A.DESCRIPTION, ' '))) <> 'unretire'
                 and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                 and (PEND_TRANS_ID, SET_OF_BOOKS_ID) in
                     (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                        from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                       where A.COMPANY_ID = B.COMPANY_ID
                         and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                      minus
                      select PEND_TRANS_ID, SET_OF_BOOKS_ID
                        from PEND_TRANSACTION_SET_OF_BOOKS
                       where PEND_TRANS_ID = :NEW.PEND_TRANS_ID));
end;
/

--
-- PEND_TRANS_ALL_SOB_V
--
create or replace view PEND_TRANS_ALL_SOB_V as
select A.PEND_TRANS_ID,
       1 SET_OF_BOOKS_ID,
       A.TIME_STAMP,
       A.USER_ID,
       A.ACTIVITY_CODE,
       A.POSTING_QUANTITY,
       A.POSTING_AMOUNT,
       A.COST_OF_REMOVAL,
       A.SALVAGE_CASH,
       A.SALVAGE_RETURNS,
       A.GAIN_LOSS,
       A.RESERVE,
       A.FERC_ACTIVITY_CODE,
       A.RESERVE_CREDITS,
       A.REPLACEMENT_AMOUNT,
       A.GAIN_LOSS_REVERSAL,
       A.IMPAIRMENT_EXPENSE_AMOUNT,
       A.LDG_ASSET_ID,
       A.LDG_ACTIVITY_ID,
       A.LDG_DEPR_GROUP_ID,
       A.BOOKS_SCHEMA_ID,
       A.RETIREMENT_UNIT_ID,
       A.UTILITY_ACCOUNT_ID,
       A.BUS_SEGMENT_ID,
       A.FUNC_CLASS_ID,
       A.SUB_ACCOUNT_ID,
       A.ASSET_LOCATION_ID,
       A.GL_ACCOUNT_ID,
       A.COMPANY_ID,
       A.GL_POSTING_MO_YR,
       A.SUBLEDGER_INDICATOR,
       A.GL_JE_CODE,
       A.WORK_ORDER_NUMBER,
       A.USER_ID1,
       A.USER_ID2,
       A.IN_SERVICE_YEAR,
       A.DESCRIPTION,
       A.LONG_DESCRIPTION,
       A.PROPERTY_GROUP_ID,
       A.RETIRE_METHOD_ID,
       A.POSTING_ERROR,
       A.POSTING_STATUS,
       A.MISC_DESCRIPTION,
       A.SERIAL_NUMBER,
       A.USER_ID3,
       A.DISPOSITION_CODE,
       A.MINOR_PEND_TRAN_ID,
       A.WIP_COMP_TRANSACTION,
       A.WIP_COMPUTATION_ID,
       A.TAX_ORIG_MONTH_NUMBER
  from PEND_TRANSACTION A
 where A.POSTING_STATUS <> 2
   and A.POSTING_STATUS <> 6
union all
select B.PEND_TRANS_ID,
       B.SET_OF_BOOKS_ID,
       A.TIME_STAMP,
       A.USER_ID,
       B.ACTIVITY_CODE,
       B.POSTING_QUANTITY,
       B.POSTING_AMOUNT,
       B.ADJUSTED_COST_OF_REMOVAL,
       B.ADJUSTED_SALVAGE_CASH,
       B.ADJUSTED_SALVAGE_RETURNS,
       B.GAIN_LOSS,
       B.RESERVE,
       B.FERC_ACTIVITY_CODE,
       B.ADJUSTED_RESERVE_CREDITS,
       B.REPLACEMENT_AMOUNT,
       B.GAIN_LOSS_REVERSAL,
       B.IMPAIRMENT_EXPENSE_AMOUNT,
       A.LDG_ASSET_ID,
       A.LDG_ACTIVITY_ID,
       A.LDG_DEPR_GROUP_ID,
       A.BOOKS_SCHEMA_ID,
       A.RETIREMENT_UNIT_ID,
       A.UTILITY_ACCOUNT_ID,
       A.BUS_SEGMENT_ID,
       A.FUNC_CLASS_ID,
       A.SUB_ACCOUNT_ID,
       A.ASSET_LOCATION_ID,
       A.GL_ACCOUNT_ID,
       A.COMPANY_ID,
       A.GL_POSTING_MO_YR,
       A.SUBLEDGER_INDICATOR,
       A.GL_JE_CODE,
       A.WORK_ORDER_NUMBER,
       A.USER_ID1,
       A.USER_ID2,
       A.IN_SERVICE_YEAR,
       A.DESCRIPTION,
       A.LONG_DESCRIPTION,
       A.PROPERTY_GROUP_ID,
       A.RETIRE_METHOD_ID,
       A.POSTING_ERROR,
       A.POSTING_STATUS,
       A.MISC_DESCRIPTION,
       A.SERIAL_NUMBER,
       A.USER_ID3,
       A.DISPOSITION_CODE,
       A.MINOR_PEND_TRAN_ID,
       A.WIP_COMP_TRANSACTION,
       A.WIP_COMPUTATION_ID,
       A.TAX_ORIG_MONTH_NUMBER
  from PEND_TRANSACTION A, PEND_TRANSACTION_SET_OF_BOOKS B
 where A.PEND_TRANS_ID = B.PEND_TRANS_ID
   and A.POSTING_STATUS <> 2
   and A.POSTING_STATUS <> 6;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (274, 0, 10, 4, 0, 0, 11106, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011106_pend_trans_set_of_books.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;