/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011042_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/06/2012 Julia Breuer   Point Release
||============================================================================
*/

-- Update the system option description.
update PTC_SYSTEM_OPTIONS
   set LONG_DESCRIPTION = 'A comma-separated list of "Unspecified" states (using the "State ID" field) that should be used for national allocations.'
 where SYSTEM_OPTION_ID = 'Returns Center - Allocation - Unspecified State Definition';

-- Delete the values - it will now be a freeform field.
delete from PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES
 where SYSTEM_OPTION_ID = 'Returns Center - Allocation - Unspecified State Definition';

-- Add a state_id column to the user input table - we will have a separate user input for each "unspecified" state.
alter table PWRPLANT.PT_USER_INPUT_LEDGER add STATE_ID char(18);

alter table PWRPLANT.PT_USER_INPUT_LEDGER
   add constraint PT_UI_LDG_STATE_FK
       foreign key (STATE_ID)
       references PWRPLANT.STATE;

-- Update the state ID on the record for "System Generated (Multistate)", if the client is using national allocations.
update PT_USER_INPUT_LEDGER
   set (DESCRIPTION, STATE_ID) =
        (select 'Multistate (' || SUBSTR(trim(STATE.LONG_DESCRIPTION), 1, 22) || ')', STATE.STATE_ID
           from PTC_SYSTEM_OPTIONS, STATE
          where PTC_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
                'Returns Center - Allocation - Unspecified State Definition'
            and UPPER(trim(PTC_SYSTEM_OPTIONS.OPTION_VALUE)) = UPPER(trim(STATE.STATE_ID)))
 where USER_INPUT = 3
   and exists
 (select STATE.STATE_ID
          from PTC_SYSTEM_OPTIONS, STATE
         where PTC_SYSTEM_OPTIONS.SYSTEM_OPTION_ID =
               'Returns Center - Allocation - Unspecified State Definition'
           and UPPER(trim(PTC_SYSTEM_OPTIONS.OPTION_VALUE)) = UPPER(trim(STATE.STATE_ID)));

-- If the multistate user input did not get updated with a state, the client is not using national allocations, so delete the option.
delete from PWRPLANT.PT_USER_INPUT_LEDGER
 where USER_INPUT = 3
   and STATE_ID is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (246, 0, 10, 4, 0, 0, 11042, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011042_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
