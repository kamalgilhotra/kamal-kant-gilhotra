/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033250_depr_arc_table.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/15/2013 Brandon Beck
||============================================================================
*/

create table CPR_DEPR_CALC_STG_ARC as select * from CPR_DEPR_CALC_STG;

comment on table CPR_DEPR_CALC_STG_ARC is '(S)  [01] An archive table that houses historical individual depreciation calculation.';
comment on column CPR_DEPR_CALC_STG.DEPR_CALC_MESSAGE is 'A column to hold specific error messaging based on the asset being processes';
comment on column CPR_DEPR_CALC_STG.SUBLEDGER_TYPE_ID is 'The subledger indicator';
comment on column CPR_DEPR_CALC_STG.DEPR_CALC_STATUS is 'Status column';
comment on column CPR_DEPR_CALC_STG.NET_TRF is 'Net transfers (trans in + trans out)';
comment on column CPR_DEPR_CALC_STG.NET_RES_TRF is 'Net reserve transfers';
comment on column CPR_DEPR_CALC_STG.NET_IMP_AMT is 'Net impairment amount';
comment on column CPR_DEPR_CALC_STG.ACTIVITY is 'The plant activity';
comment on column CPR_DEPR_CALC_STG.BEG_RES_AMT is 'The beginning reserve amount';
comment on column CPR_DEPR_CALC_STG.ACTIVITY_3 is 'The plant activity';
comment on column CPR_DEPR_CALC_STG.DEPR_METHOD_SLE_EXP is 'The expense taken for SLE method';
comment on column CPR_DEPR_CALC_STG.BEG_BAL_AMT is 'The beginning plant amount';
comment on column CPR_DEPR_CALC_STG.SCH_RATE is 'The rate used for schedule method';
comment on column CPR_DEPR_CALC_STG.SCH_FACTOR is 'The monthly factor used for schedule method';
comment on column CPR_DEPR_CALC_STG.MIN_MPC is 'The minimum mid period convention.  Used to calculate remaining life.';
comment on column CPR_DEPR_CALC_STG.TRUEUP_ADJ is 'The true up adjustment';
comment on column CPR_DEPR_CALC_STG.DEPR_EXP_ADJUST is 'Depreciation Expense Adjustment';
comment on column CPR_DEPR_CALC_STG.OTHER_CREDITS_AND_ADJUST is 'Other adjustment';
comment on column CPR_DEPR_CALC_STG.GAIN_LOSS is 'The gain loss';
comment on column CPR_DEPR_CALC_STG.DEPRECIATION_BASE is 'The depreciation base';
comment on column CPR_DEPR_CALC_STG.CURR_DEPR_EXPENSE is 'The current month calculated depreciation expense';
comment on column CPR_DEPR_CALC_STG.DEPR_RESERVE is 'The depreciation reserve';
comment on column CPR_DEPR_CALC_STG.BEG_RESERVE_YEAR is 'The beginning reserve for the year';
comment on column CPR_DEPR_CALC_STG.YTD_DEPR_EXPENSE is 'Year to datre depreciation expense';
comment on column CPR_DEPR_CALC_STG.YTD_DEPR_EXP_ADJUST is 'Year to date depr expense adjustments';
comment on column CPR_DEPR_CALC_STG.PRIOR_YTD_DEPR_EXPENSE is 'Prior year depr expense';
comment on column CPR_DEPR_CALC_STG.PRIOR_YTD_DEPR_EXP_ADJUST is 'Prior year depr adjustment';
comment on column CPR_DEPR_CALC_STG.ACCT_DISTRIB is 'The account distribution';
comment on column CPR_DEPR_CALC_STG.MONTH_RATE is 'The monthly depr rate';
comment on column CPR_DEPR_CALC_STG.COMPANY_ID is 'Internal ID linking to company';
comment on column CPR_DEPR_CALC_STG.MID_PERIOD_METHOD is 'The mid period method';
comment on column CPR_DEPR_CALC_STG.MID_PERIOD_CONV is 'The percent of current activity to include in depreciation base';
comment on column CPR_DEPR_CALC_STG.DEPR_GROUP_ID is 'Internal ID linking to the depreciation group';
comment on column CPR_DEPR_CALC_STG.DEPR_EXP_ALLOC_ADJUST is 'Depreciation Expense Adjustment';
comment on column CPR_DEPR_CALC_STG.DEPR_METHOD_ID is 'Internal ID linking to the depreciation method';
comment on column CPR_DEPR_CALC_STG.TRUE_UP_CPR_DEPR is 'Trueup individually depreciated assets';
comment on column CPR_DEPR_CALC_STG.SALVAGE_EXPENSE is 'The salvage expense';
comment on column CPR_DEPR_CALC_STG.SALVAGE_EXP_ADJUST is 'The salvage expense adjustment';
comment on column CPR_DEPR_CALC_STG.SALVAGE_EXP_ALLOC_ADJUST is 'The salvage expense adjustment';
comment on column CPR_DEPR_CALC_STG.IMPAIRMENT_ASSET_AMOUNT is 'The amount of the impairment';
comment on column CPR_DEPR_CALC_STG.IMPAIRMENT_EXPENSE_AMOUNT is 'The expense amount being impaired';
comment on column CPR_DEPR_CALC_STG.DEPRECIATION_INDICATOR is 'An indiciator identifying the type of depreciation';
comment on column CPR_DEPR_CALC_STG.END_OF_LIFE is 'The end of the life for the asset';
comment on column CPR_DEPR_CALC_STG.ENG_IN_SERVICE_YEAR is 'the Engineering in service year';
comment on column CPR_DEPR_CALC_STG.ESTIMATED_PRODUCTION is 'The estimated production for unit of production calculations';
comment on column CPR_DEPR_CALC_STG.HAS_CFNU is '1 if the asset has been closed from non unitized';
comment on column CPR_DEPR_CALC_STG.HAS_NURV is '1 if the asset has non unitized reversals';
comment on column CPR_DEPR_CALC_STG.HAS_NURV_LAST_MONTH is '1 if the asset has non unitized reversals last month';
comment on column CPR_DEPR_CALC_STG.NET_GROSS is '1 means calculate depreciation based on the net balance.';
comment on column CPR_DEPR_CALC_STG.OVER_DEPR_CHECK is 'Perform overdepreciation checks';
comment on column CPR_DEPR_CALC_STG.PRODUCTION is 'The unit of production amounts';
comment on column CPR_DEPR_CALC_STG.RATE is 'The rate';
comment on column CPR_DEPR_CALC_STG.EXISTS_LAST_MONTH is 'Did the asset exist last month';
comment on column CPR_DEPR_CALC_STG.EXISTS_TWO_MONTHS is 'Did the asset exist two months ago';
comment on column CPR_DEPR_CALC_STG.EXISTS_ARO is 'Is the asset an ARO asset';
comment on column CPR_DEPR_CALC_STG.EFFECTIVE_DATE is 'Effective date';
comment on column CPR_DEPR_CALC_STG.TRF_WEIGHT is 'A percent of transfers that should included in plant activity';
comment on column CPR_DEPR_CALC_STG.ASSET_ID is 'An internal ID to the CPR';
comment on column CPR_DEPR_CALC_STG.SET_OF_BOOKS_ID is 'The set of books';
comment on column CPR_DEPR_CALC_STG.GL_POSTING_MO_YR is 'The accounting month';
comment on column CPR_DEPR_CALC_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column CPR_DEPR_CALC_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column CPR_DEPR_CALC_STG.INIT_LIFE is 'The initial life of the asset in months';
comment on column CPR_DEPR_CALC_STG.REMAINING_LIFE is 'The remaining life';
comment on column CPR_DEPR_CALC_STG.ESTIMATED_SALVAGE is 'The esitmate salvage for the asset';
comment on column CPR_DEPR_CALC_STG.BEG_ASSET_DOLLARS is 'The beginning plant dollars';
comment on column CPR_DEPR_CALC_STG.NET_ADDS_AND_ADJUST is 'Net additions (adds plus adjustment)';
comment on column CPR_DEPR_CALC_STG.RETIREMENTS is 'The retirement amount';
comment on column CPR_DEPR_CALC_STG.TRANSFERS_IN is 'Transfer In Amount';
comment on column CPR_DEPR_CALC_STG.TRANSFERS_OUT is 'Transfer Out Amount';
comment on column CPR_DEPR_CALC_STG.ASSET_DOLLARS is 'The ending Plant Dollars';
comment on column CPR_DEPR_CALC_STG.BEG_RESERVE_MONTH is 'The beginning reserve for the month';
comment on column CPR_DEPR_CALC_STG.SALVAGE_DOLLARS is 'The salvage amount';
comment on column CPR_DEPR_CALC_STG.RESERVE_ADJUSTMENT is 'Reserve adjustment amount';
comment on column CPR_DEPR_CALC_STG.COST_OF_REMOVAL is 'The cost of removal for the period';
comment on column CPR_DEPR_CALC_STG.RESERVE_TRANS_IN is 'Reserve transferred into the group';
comment on column CPR_DEPR_CALC_STG.RESERVE_TRANS_OUT is 'Reserve transferred out of the group';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (690, 0, 10, 4, 1, 1, 33250, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033250_depr_arc_table.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;