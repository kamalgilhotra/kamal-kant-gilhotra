/*
||=======================================================================================================
|| Application: PowerPlan
|| File Name:   maint_045564_pwrtax_reconcile_item_default_tax_include_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||=======================================================================================================
|| Version    Date       Revised By     				Reason for Change
|| ---------- ---------- ----------------------------	-------------------------------------------------
|| 2016.1.0.0 03/15/2016 Robert Burns/Stephanie Moore   Replace Default Tax Include IDs with Descriptions
||=======================================================================================================
*/

update powerplant_columns 
set dropdown_name = 'tax_include', pp_edit_type_id = 'p' 
where column_name = 'default_tax_include_id' 
	and table_name = 'tax_reconcile_item';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3103, 0, 2016, 1, 0, 0, 045564, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045564_pwrtax_reconcile_item_default_tax_include_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;