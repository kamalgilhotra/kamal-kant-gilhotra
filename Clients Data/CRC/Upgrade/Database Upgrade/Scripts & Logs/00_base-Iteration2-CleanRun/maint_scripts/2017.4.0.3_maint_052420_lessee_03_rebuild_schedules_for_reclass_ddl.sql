/*
||============================================================================
|| Application: PowerPlan 
|| File Name:   maint_052420_lessee_03_rebuild_schedules_for_reclass_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.3  10/10/2018 Sarah Byers      Rebuild the schedules to backfill the reclass and obligation/unaccrued interest remeasurement fields
||============================================================================
*/

-- Asset Schedule
create table rebuild_ls_asset_schedule as (
select LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, 
       BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, 
       INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, 
       EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, 
       EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, 
       EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, 
       CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, 
       CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
       CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, 
       CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, 
       CURRENT_LEASE_COST, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
       BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, 
       INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT, 
       IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, ROU_ASSET_REMEASUREMENT, 
       PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, BEG_ARREARS_ACCRUAL, 
       ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, PARTIAL_MONTH_PERCENT, REMAINING_PRINCIPAL,
       ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
       UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS
  from (
      select LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, 
             BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, 
             INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, 
             EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, 
             EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, 
             EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, 
             CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, 
             CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, 
             CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, 
             CURRENT_LEASE_COST, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
             BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, 
             INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT, 
             IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, ROU_ASSET_REMEASUREMENT, 
             PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, BEG_ARREARS_ACCRUAL, 
             ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, PARTIAL_MONTH_PERCENT, REMAINING_PRINCIPAL,
             ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
             UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS
        from (
            select row_number() over(partition by ls_asset_id, revision, set_of_books_id order by month) id, 
                   months_between(MAX(MONTH) OVER(PARTITION BY ls_asset_id, revision, set_of_books_id), MONTH) AS month_id,
                   LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, 
                   BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, 
                   INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, 
                   EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, 
                   EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, 
                   EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, 
                   CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, 
                   CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
                   CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, 
                   CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, 
                   CURRENT_LEASE_COST, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
                   BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, 
                   INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT, 
                   IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, ROU_ASSET_REMEASUREMENT, 
                   PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, BEG_ARREARS_ACCRUAL, 
                   ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, PARTIAL_MONTH_PERCENT, REMAINING_PRINCIPAL,
                   ST_OBLIGATION_REMEASUREMENT, 
                   LT_OBLIGATION_REMEASUREMENT, 
                   OBLIGATION_RECLASS,
                   UNACCRUED_INTEREST_REMEASURE, 
                   round(UNACCRUED_INTEREST_RECLASS,2) - (ROUND( END_LIABILITY , 2) - ROUND( END_LT_LIABILITY , 2)
                                                          - (ROUND( BEG_LIABILITY , 2) - ROUND( BEG_LT_LIABILITY , 2))
                                                          - ROUND( INTEREST_ACCRUAL , 2)
                                                          + ROUND( INTEREST_PAID , 2)
                                                          + ROUND( PRINCIPAL_PAID , 2)
                                                          - ROUND( OBLIGATION_RECLASS , 2)
                                                          - ROUND( ST_OBLIGATION_REMEASUREMENT , 2)
                                                          + ROUND( UNACCRUED_INTEREST_RECLASS , 2)
                                                          + ROUND( UNACCRUED_INTEREST_REMEASURE , 2)) as UNACCRUED_INTEREST_RECLASS
              from ls_asset_schedule
             where OBLIGATION_RECLASS is null
            )
      MODEL PARTITION BY(ls_asset_id, revision, set_of_books_id)
      DIMENSION BY(ID)
      MEASURES( RESIDUAL_AMOUNT, 
                TERM_PENALTY, 
                BPO_PRICE, 
                BEG_CAPITAL_COST, 
                END_CAPITAL_COST, 
                BEG_OBLIGATION, 
                END_OBLIGATION, 
                BEG_LT_OBLIGATION, 
                END_LT_OBLIGATION, 
                INTEREST_ACCRUAL, 
                PRINCIPAL_ACCRUAL, 
                INTEREST_PAID, 
                PRINCIPAL_PAID, 
                EXECUTORY_ACCRUAL1, 
                EXECUTORY_ACCRUAL2, 
                EXECUTORY_ACCRUAL3, 
                EXECUTORY_ACCRUAL4, 
                EXECUTORY_ACCRUAL5, 
                EXECUTORY_ACCRUAL6, 
                EXECUTORY_ACCRUAL7, 
                EXECUTORY_ACCRUAL8, 
                EXECUTORY_ACCRUAL9, 
                EXECUTORY_ACCRUAL10, 
                EXECUTORY_PAID1, 
                EXECUTORY_PAID2, 
                EXECUTORY_PAID3, 
                EXECUTORY_PAID4, 
                EXECUTORY_PAID5, 
                EXECUTORY_PAID6, 
                EXECUTORY_PAID7, 
                EXECUTORY_PAID8, 
                EXECUTORY_PAID9, 
                EXECUTORY_PAID10, 
                CONTINGENT_ACCRUAL1, 
                CONTINGENT_ACCRUAL2, 
                CONTINGENT_ACCRUAL3, 
                CONTINGENT_ACCRUAL4, 
                CONTINGENT_ACCRUAL5, 
                CONTINGENT_ACCRUAL6, 
                CONTINGENT_ACCRUAL7, 
                CONTINGENT_ACCRUAL8, 
                CONTINGENT_ACCRUAL9, 
                CONTINGENT_ACCRUAL10, 
                CONTINGENT_PAID1, 
                CONTINGENT_PAID2, 
                CONTINGENT_PAID3, 
                CONTINGENT_PAID4, 
                CONTINGENT_PAID5, 
                CONTINGENT_PAID6, 
                CONTINGENT_PAID7, 
                CONTINGENT_PAID8, 
                CONTINGENT_PAID9, 
                CONTINGENT_PAID10, 
                USER_ID, 
                TIME_STAMP, 
                IS_OM, 
                CURRENT_LEASE_COST, 
                BEG_DEFERRED_RENT, 
                DEFERRED_RENT, 
                END_DEFERRED_RENT, 
                BEG_ST_DEFERRED_RENT, 
                END_ST_DEFERRED_RENT, 
                PRINCIPAL_REMEASUREMENT, 
                LIABILITY_REMEASUREMENT, 
                INITIAL_DIRECT_COST, 
                INCENTIVE_AMOUNT, 
                BEG_PREPAID_RENT, 
                PREPAY_AMORTIZATION, 
                PREPAID_RENT, 
                END_PREPAID_RENT, 
                IDC_MATH_AMOUNT, 
                INCENTIVE_MATH_AMOUNT, 
                BEG_NET_ROU_ASSET, 
                END_NET_ROU_ASSET, 
                ROU_ASSET_REMEASUREMENT, 
                PARTIAL_TERM_GAIN_LOSS, 
                ADDITIONAL_ROU_ASSET, 
                EXECUTORY_ADJUST, 
                CONTINGENT_ADJUST, 
                BEG_ARREARS_ACCRUAL, 
                ARREARS_ACCRUAL, 
                END_ARREARS_ACCRUAL, 
                PARTIAL_MONTH_PERCENT, 
                REMAINING_PRINCIPAL,
                month as MONTH,
                month_id as MONTH_ID,
                principal_paid as PRIN_PAID,
                interest_accrual as INT_ACCRUAL,
                interest_paid as INT_PAID,
                BEG_LIABILITY,
                END_LIABILITY,
                BEG_LT_LIABILITY,
                END_LT_LIABILITY, 
                0 ST_OBLIGATION_REMEASUREMENT, 
                0 LT_OBLIGATION_REMEASUREMENT, 
                0 OBLIGATION_RECLASS,
                0 UNACCRUED_INTEREST_REMEASURE, 
                0 UNACCRUED_INTEREST_RECLASS,
                max(id) over (partition by ls_asset_id, revision, set_of_books_id) as LAST_ID
               )
      IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ] 
      ( 
        -- Short Term/Long Term Obligation Remeasurement
        LT_OBLIGATION_REMEASUREMENT [ ITERATION_NUMBER + 1 ] = CASE WHEN PRINCIPAL_REMEASUREMENT [ CV(ID) ] <> 0 THEN
                                              BEG_OBLIGATION [ CV(ID) + 12 ] - BEG_LT_OBLIGATION [ CV(ID) ]
                                            ELSE
                                              0
                                            END,
        ST_OBLIGATION_REMEASUREMENT [ ITERATION_NUMBER + 1 ] = CASE WHEN PRINCIPAL_REMEASUREMENT [ CV(ID) ] <> 0 THEN
                                              LIABILITY_REMEASUREMENT [ CV(ID) ] + ADDITIONAL_ROU_ASSET [ CV(ID) ] - LT_OBLIGATION_REMEASUREMENT [ CV(ID) ]
                                            ELSE
                                              0
                                            END,
        -- Obligation Reclass
        OBLIGATION_RECLASS [ ITERATION_NUMBER - 11 ] = BEG_LT_OBLIGATION [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) - END_LT_OBLIGATION [ CV(ID) ],
        -- Unaccrued Interest Remeasurement
        UNACCRUED_INTEREST_REMEASURE [ ITERATION_NUMBER + 1 ] = CASE WHEN PRINCIPAL_REMEASUREMENT [ CV(ID) ] <> 0 THEN
                                               (BEG_LIABILITY [ CV(ID) + 12 ] - BEG_LT_LIABILITY [ CV(ID) ]) - (BEG_OBLIGATION [ CV(ID) + 12 ] - BEG_LT_OBLIGATION [ CV(ID) ])
                                             ELSE
                                               0
                                             END,
        -- Unaccrued Interest Reclass
        UNACCRUED_INTEREST_RECLASS [ ITERATION_NUMBER - 11 ] = (BEG_LT_OBLIGATION [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) - END_LT_OBLIGATION [ CV(ID) ])
                                                                       - (BEG_LT_LIABILITY [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) + NVL(UNACCRUED_INTEREST_REMEASURE [ CV(ID) ], 0) - END_LT_LIABILITY[ CV(ID) ])
      )
      union
      select LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, 
             BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, 
             INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, 
             EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, 
             EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, 
             EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10, 
             CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, CONTINGENT_ACCRUAL5, 
             CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, 
             CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, 
             CURRENT_LEASE_COST, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
             BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, 
             INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, PREPAID_RENT, END_PREPAID_RENT, 
             IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, ROU_ASSET_REMEASUREMENT, 
             PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, BEG_ARREARS_ACCRUAL, 
             ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, PARTIAL_MONTH_PERCENT, REMAINING_PRINCIPAL,
             ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
             UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS
        from ls_asset_schedule
       where obligation_reclass is not null)
 where month is not null
);

-- Drop ls_asset_schedule
drop table ls_asset_schedule;
  
-- Rename to ls_asset_schedule
alter table rebuild_ls_asset_schedule rename to ls_asset_schedule;

-- Primary Key
alter table ls_asset_schedule add constraint 
pk_ls_asset_schedule primary key (ls_asset_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx;

-- Indexes
CREATE INDEX LS_ASSET_SCHEDULE_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (REVISION , LS_ASSET_ID , SET_OF_BOOKS_ID ) ;
CREATE INDEX LS_ASSET_SCHEDULE_TMTH_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (TRUNC(MONTH,'fmmonth')) ;
CREATE INDEX LS_ASSET_SCHEDULE_MANY_IDX ON PWRPLANT.LS_ASSET_SCHEDULE (LS_ASSET_ID, REVISION, MONTH, SET_OF_BOOKS_ID, TRUNC(MONTH,'fmmonth'));

-- Comments
comment ON TABLE ls_asset_schedule IS '(C)  [06]
The LS Asset Schedule table is holds the interest, principal, executory, and contingent amounts accrued and paid by period by set of books for a leased asset.';

COMMENT ON COLUMN ls_asset_schedule.ls_asset_id IS 'System-assigned identifier of a particular leased asset. The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_asset_schedule.revision IS 'The revision.';
COMMENT ON COLUMN ls_asset_schedule.set_of_books_id IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN ls_asset_schedule.month IS 'The month being processed.';
COMMENT ON COLUMN ls_asset_schedule.residual_amount IS 'The guaranteed residual amount.';
COMMENT ON COLUMN ls_asset_schedule.term_penalty IS 'The termination penalty amount.';
COMMENT ON COLUMN ls_asset_schedule.bpo_price IS 'The bargain purchase amount.';
COMMENT ON COLUMN ls_asset_schedule.beg_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_asset_schedule.end_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_asset_schedule.beg_obligation IS 'The beginning obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_obligation IS 'The ending obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_lt_obligation IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_lt_obligation IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.interest_accrual IS 'The amount of interest accrued in this period .';
COMMENT ON COLUMN ls_asset_schedule.principal_accrual IS 'The amount of principal accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.interest_paid IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN ls_asset_schedule.principal_paid IS 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN ls_asset_schedule.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_asset_schedule.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_asset_schedule.is_om IS '1 means O and M.  0 means Capital. 1 means O and M.  0 means Capital.';
COMMENT ON COLUMN ls_asset_schedule.current_lease_cost IS 'The total amount funded on the leased asset in the given month';
COMMENT ON COLUMN ls_asset_schedule.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_liability IS 'The beginning liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_liability IS 'The ending liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_lt_liability IS 'The beginning long term liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_lt_liability IS 'The ending long term liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_asset_schedule.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_asset_schedule.initial_direct_cost IS 'Holds Initial Direct Cost Amount for Asset Schedule.';
COMMENT ON COLUMN ls_asset_schedule.incentive_amount IS 'Holds Incentive Amount for Asset Schedule.';
COMMENT ON COLUMN ls_asset_schedule.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';
COMMENT ON COLUMN ls_asset_schedule.prepay_amortization IS 'The prepayment amortization amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.prepaid_rent IS 'The prepaid rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_prepaid_rent IS 'The ending prepaid rent for the period.';
COMMENT ON COLUMN ls_asset_schedule.idc_math_amount IS 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_asset_schedule.incentive_math_amount IS 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_asset_schedule.beg_net_rou_asset IS 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_asset_schedule.end_net_rou_asset IS 'End Capital Cost less End Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_asset_schedule.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_asset_schedule.partial_term_gain_loss IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_asset_schedule.executory_adjust IS 'Summated value of all Executory Adjusted values, by ILR';
COMMENT ON COLUMN ls_asset_schedule.contingent_adjust IS 'Summated value of all Contingent Adjusted values, by ILR';
COMMENT ON COLUMN ls_asset_schedule.additional_rou_asset IS 'Calculation of ROU Asset based on Quantity Retirement Method';
comment on column ls_asset_schedule.partial_month_percent is 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';
COMMENT ON COLUMN ls_asset_schedule.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN ls_asset_schedule.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';
comment on column ls_asset_schedule.remaining_principal is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc logic';
comment on column ls_asset_schedule.unaccrued_interest_reclass is 'The reclassification of unaccrued interest from short to long term.';
comment on column ls_asset_schedule.unaccrued_interest_remeasure is 'The unaccrued interest amount as a result of a remeasurement.';
comment on column ls_asset_schedule.st_obligation_remeasurement is 'The short term obligation remeasurement amount.';
comment on column ls_asset_schedule.lt_obligation_remeasurement is 'The long term obligation remeasurement amount.';
comment on column ls_asset_schedule.obligation_reclass is 'The reclassification of obligation from short to long term.';

-- ILR Schedule
create table rebuild_ls_ilr_schedule as (
select ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST, 
       BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, 
       PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, 
       EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, 
       EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, 
       EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, 
       CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
       CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, 
       CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, CURRENT_LEASE_COST, BEG_DEFERRED_RENT, 
       DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
       BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, 
       PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, 
       PREPAID_RENT, END_PREPAID_RENT, IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, 
       ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, 
       BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
       ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
       UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS
  from (
      select ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST, 
             BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, 
             PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, 
             EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, 
             EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, 
             EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, 
             CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, 
             CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, CURRENT_LEASE_COST, BEG_DEFERRED_RENT, 
             DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
             BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, 
             PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, 
             PREPAID_RENT, END_PREPAID_RENT, IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, 
             ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, 
             BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
             ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
             UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS
        from (
            select row_number() over(partition by ilr_id, revision, set_of_books_id order by month) id, 
                   months_between(MAX(MONTH) OVER(PARTITION BY ilr_id, revision, set_of_books_id), MONTH) AS month_id,
                   ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST, 
                   BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, 
                   PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, 
                   EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, 
                   EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, 
                   EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, 
                   CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
                   CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, 
                   CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, CURRENT_LEASE_COST, BEG_DEFERRED_RENT, 
                   DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
                   BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, 
                   PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, 
                   PREPAID_RENT, END_PREPAID_RENT, IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, 
                   ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, 
                   BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
                   ST_OBLIGATION_REMEASUREMENT, 
                   LT_OBLIGATION_REMEASUREMENT, 
                   OBLIGATION_RECLASS,
                   UNACCRUED_INTEREST_REMEASURE, 
                   round(UNACCRUED_INTEREST_RECLASS,2) - (ROUND( END_LIABILITY , 2) - ROUND( END_LT_LIABILITY , 2)
                                                          - (ROUND( BEG_LIABILITY , 2) - ROUND( BEG_LT_LIABILITY , 2))
                                                          - ROUND( INTEREST_ACCRUAL , 2)
                                                          + ROUND( INTEREST_PAID , 2)
                                                          + ROUND( PRINCIPAL_PAID , 2)
                                                          - ROUND( OBLIGATION_RECLASS , 2)
                                                          - ROUND( ST_OBLIGATION_REMEASUREMENT , 2)
                                                          + ROUND( UNACCRUED_INTEREST_RECLASS , 2)
                                                          + ROUND( UNACCRUED_INTEREST_REMEASURE , 2)) as UNACCRUED_INTEREST_RECLASS
              from ls_ilr_schedule
             where OBLIGATION_RECLASS is null
            )
      MODEL PARTITION BY(ilr_id, revision, set_of_books_id)
      DIMENSION BY(ID)
      MEASURES( RESIDUAL_AMOUNT, 
                TERM_PENALTY, 
                BPO_PRICE, 
                BEG_CAPITAL_COST, 
                END_CAPITAL_COST, 
                BEG_OBLIGATION, 
                END_OBLIGATION, 
                BEG_LT_OBLIGATION, 
                END_LT_OBLIGATION, 
                INTEREST_ACCRUAL, 
                PRINCIPAL_ACCRUAL, 
                INTEREST_PAID, 
                PRINCIPAL_PAID, 
                EXECUTORY_ACCRUAL1, 
                EXECUTORY_ACCRUAL2, 
                EXECUTORY_ACCRUAL3, 
                EXECUTORY_ACCRUAL4, 
                EXECUTORY_ACCRUAL5, 
                EXECUTORY_ACCRUAL6, 
                EXECUTORY_ACCRUAL7, 
                EXECUTORY_ACCRUAL8, 
                EXECUTORY_ACCRUAL9, 
                EXECUTORY_ACCRUAL10, 
                EXECUTORY_PAID1, 
                EXECUTORY_PAID2, 
                EXECUTORY_PAID3, 
                EXECUTORY_PAID4, 
                EXECUTORY_PAID5, 
                EXECUTORY_PAID6, 
                EXECUTORY_PAID7, 
                EXECUTORY_PAID8, 
                EXECUTORY_PAID9, 
                EXECUTORY_PAID10, 
                CONTINGENT_ACCRUAL1, 
                CONTINGENT_ACCRUAL2, 
                CONTINGENT_ACCRUAL3, 
                CONTINGENT_ACCRUAL4, 
                CONTINGENT_ACCRUAL5, 
                CONTINGENT_ACCRUAL6, 
                CONTINGENT_ACCRUAL7, 
                CONTINGENT_ACCRUAL8, 
                CONTINGENT_ACCRUAL9, 
                CONTINGENT_ACCRUAL10, 
                CONTINGENT_PAID1, 
                CONTINGENT_PAID2, 
                CONTINGENT_PAID3, 
                CONTINGENT_PAID4, 
                CONTINGENT_PAID5, 
                CONTINGENT_PAID6, 
                CONTINGENT_PAID7, 
                CONTINGENT_PAID8, 
                CONTINGENT_PAID9, 
                CONTINGENT_PAID10, 
                USER_ID, 
                TIME_STAMP, 
                IS_OM, 
                CURRENT_LEASE_COST, 
                BEG_DEFERRED_RENT, 
                DEFERRED_RENT, 
                END_DEFERRED_RENT, 
                BEG_ST_DEFERRED_RENT, 
                END_ST_DEFERRED_RENT, 
                PRINCIPAL_REMEASUREMENT, 
                LIABILITY_REMEASUREMENT, 
                INITIAL_DIRECT_COST, 
                INCENTIVE_AMOUNT, 
                BEG_PREPAID_RENT, 
                PREPAY_AMORTIZATION, 
                PREPAID_RENT, 
                END_PREPAID_RENT, 
                IDC_MATH_AMOUNT, 
                INCENTIVE_MATH_AMOUNT, 
                BEG_NET_ROU_ASSET, 
                END_NET_ROU_ASSET, 
                ROU_ASSET_REMEASUREMENT, 
                PARTIAL_TERM_GAIN_LOSS, 
                ADDITIONAL_ROU_ASSET, 
                EXECUTORY_ADJUST, 
                CONTINGENT_ADJUST, 
                BEG_ARREARS_ACCRUAL, 
                ARREARS_ACCRUAL, 
                END_ARREARS_ACCRUAL, 
                REMAINING_PRINCIPAL,
                month as MONTH,
                month_id as MONTH_ID,
                principal_paid as PRIN_PAID,
                interest_accrual as INT_ACCRUAL,
                interest_paid as INT_PAID,
                BEG_LIABILITY,
                END_LIABILITY,
                BEG_LT_LIABILITY,
                END_LT_LIABILITY, 
                0 ST_OBLIGATION_REMEASUREMENT, 
                0 LT_OBLIGATION_REMEASUREMENT, 
                0 OBLIGATION_RECLASS,
                0 UNACCRUED_INTEREST_REMEASURE, 
                0 UNACCRUED_INTEREST_RECLASS,
                max(id) over (partition by ilr_id, revision, set_of_books_id) as LAST_ID
               )
      IGNORE NAV RULES UPSERT ITERATE(100000) UNTIL ITERATION_NUMBER = MONTH_ID [ 1 ] 
      ( 
        -- Short Term/Long Term Obligation Remeasurement
        LT_OBLIGATION_REMEASUREMENT [ ITERATION_NUMBER + 1 ] = CASE WHEN PRINCIPAL_REMEASUREMENT [ CV(ID) ] <> 0 THEN
                                              BEG_OBLIGATION [ CV(ID) + 12 ] - BEG_LT_OBLIGATION [ CV(ID) ]
                                            ELSE
                                              0
                                            END,
        ST_OBLIGATION_REMEASUREMENT [ ITERATION_NUMBER + 1 ] = CASE WHEN PRINCIPAL_REMEASUREMENT [ CV(ID) ] <> 0 THEN
                                              LIABILITY_REMEASUREMENT [ CV(ID) ] + ADDITIONAL_ROU_ASSET [ CV(ID) ] - LT_OBLIGATION_REMEASUREMENT [ CV(ID) ]
                                            ELSE
                                              0
                                            END,
        -- Obligation Reclass
        OBLIGATION_RECLASS [ ITERATION_NUMBER - 11 ] = BEG_LT_OBLIGATION [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) - END_LT_OBLIGATION [ CV(ID) ],
        -- Unaccrued Interest Remeasurement
        UNACCRUED_INTEREST_REMEASURE [ ITERATION_NUMBER + 1 ] = CASE WHEN PRINCIPAL_REMEASUREMENT [ CV(ID) ] <> 0 THEN
                                               (BEG_LIABILITY [ CV(ID) + 12 ] - BEG_LT_LIABILITY [ CV(ID) ]) - (BEG_OBLIGATION [ CV(ID) + 12 ] - BEG_LT_OBLIGATION [ CV(ID) ])
                                             ELSE
                                               0
                                             END,
        -- Unaccrued Interest Reclass
        UNACCRUED_INTEREST_RECLASS [ ITERATION_NUMBER - 11 ] = (BEG_LT_OBLIGATION [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) - END_LT_OBLIGATION [ CV(ID) ])
                                                                       - (BEG_LT_LIABILITY [ CV(ID) ] + NVL(LT_OBLIGATION_REMEASUREMENT [ CV(ID) ],0) + NVL(UNACCRUED_INTEREST_REMEASURE [ CV(ID) ], 0) - END_LT_LIABILITY[ CV(ID) ])
      )
      union
      select ILR_ID, REVISION, SET_OF_BOOKS_ID, MONTH, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE, BEG_CAPITAL_COST, END_CAPITAL_COST, 
             BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, 
             PRINCIPAL_PAID, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2, EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, 
             EXECUTORY_ACCRUAL6, EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10, EXECUTORY_PAID1, 
             EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5, EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, 
             EXECUTORY_PAID9, EXECUTORY_PAID10, CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4, 
             CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8, CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, 
             CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5, CONTINGENT_PAID6, CONTINGENT_PAID7, 
             CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10, USER_ID, TIME_STAMP, IS_OM, CURRENT_LEASE_COST, BEG_DEFERRED_RENT, 
             DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT, 
             BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY, 
             PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INITIAL_DIRECT_COST, INCENTIVE_AMOUNT, BEG_PREPAID_RENT, PREPAY_AMORTIZATION, 
             PREPAID_RENT, END_PREPAID_RENT, IDC_MATH_AMOUNT, INCENTIVE_MATH_AMOUNT, BEG_NET_ROU_ASSET, END_NET_ROU_ASSET, 
             ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET, EXECUTORY_ADJUST, CONTINGENT_ADJUST, 
             BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
             ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
             UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS
        from ls_ilr_schedule
       where obligation_reclass is not null)
 where month is not null
);

-- Drop ls_ilr_schedule
drop table ls_ilr_schedule;
  
-- Rename to ls_ilr_schedule
alter table rebuild_ls_ilr_schedule rename to ls_ilr_schedule;

-- Primary Key
alter table ls_ilr_schedule add constraint 
pk_ls_ilr_schedule primary key (ilr_id, revision, set_of_books_id, month) using index tablespace pwrplant_idx;

-- Indexes
CREATE INDEX LS_ILR_SCHEDULE_IDX ON PWRPLANT.LS_ILR_SCHEDULE (REVISION , ILR_ID , SET_OF_BOOKS_ID ) ;
CREATE INDEX LS_ILR_SCHEDULE_MTH_IDX ON PWRPLANT.LS_ILR_SCHEDULE (MONTH) ;
CREATE INDEX LS_ILR_SCHEDULE_TMTH_IDX ON PWRPLANT.LS_ILR_SCHEDULE (TRUNC(MONTH,'fmmonth')) ;
 
-- Comments
COMMENT ON TABLE ls_ilr_schedule IS '(C)  [06] The ILR Schedule table records the ILR''s payment schedule.  The ILR''s schedule is allocated to its leased assets as part of the monthly lease expense calc process.';

COMMENT ON COLUMN ls_ilr_schedule.ilr_id IS 'System-assigned identifier of a particular ILR.';
COMMENT ON COLUMN ls_ilr_schedule.revision IS 'The revision.';
COMMENT ON COLUMN ls_ilr_schedule.set_of_books_id IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN ls_ilr_schedule.month IS 'The month being processed.';
COMMENT ON COLUMN ls_ilr_schedule.residual_amount IS 'The guaranteed residual amount.';
COMMENT ON COLUMN ls_ilr_schedule.term_penalty IS 'The termination penalty amount.';
COMMENT ON COLUMN ls_ilr_schedule.bpo_price IS 'The bargain purchase amount.';
COMMENT ON COLUMN ls_ilr_schedule.beg_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_ilr_schedule.end_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_ilr_schedule.beg_obligation IS 'The beginning obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_obligation IS 'The ending obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_lt_obligation IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_lt_obligation IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.interest_accrual IS 'Records the amount of interest accrued.  Accruals occur every month regardless of payment schedule.';
COMMENT ON COLUMN ls_ilr_schedule.principal_accrual IS 'Records the amount of principal accrued.  Accruals occur every month regardless of payment schedule.';
COMMENT ON COLUMN ls_ilr_schedule.interest_paid IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN ls_ilr_schedule.principal_paid IS 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN ls_ilr_schedule.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_ilr_schedule.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_ilr_schedule.is_om IS '1 means O and M.  0 means Capital.';
COMMENT ON COLUMN ls_ilr_schedule.current_lease_cost IS 'The total amount funded on the ILR in the given month';
COMMENT ON COLUMN ls_ilr_schedule.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_liability IS 'The beginning liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_liability IS 'The ending liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_lt_liability IS 'The beginning long term liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_lt_liability IS 'The ending long term liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_ilr_schedule.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_ilr_schedule.initial_direct_cost IS 'Holds Initial Direct Cost Amount for ILR Schedule.';
COMMENT ON COLUMN ls_ilr_schedule.incentive_amount IS 'Holds Incentive Amount for ILR Schedule.';
COMMENT ON COLUMN ls_ilr_schedule.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';
COMMENT ON COLUMN ls_ilr_schedule.prepay_amortization IS 'The prepayment amortization amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.prepaid_rent IS 'The prepaid rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_prepaid_rent IS 'The ending prepaid rent for the period.';
COMMENT ON COLUMN ls_ilr_schedule.idc_math_amount IS 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_ilr_schedule.incentive_math_amount IS 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_ilr_schedule.beg_net_rou_asset IS 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_ilr_schedule.end_net_rou_asset IS 'End Capital Cost less End Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_ilr_schedule.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_ilr_schedule.partial_term_gain_loss IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_ilr_schedule.executory_adjust IS 'Summated value of all Executory Adjusted values, by ILR';
COMMENT ON COLUMN ls_ilr_schedule.contingent_adjust IS 'Summated value of all Contingent Adjusted values, by ILR';
COMMENT ON COLUMN ls_ilr_schedule.additional_rou_asset IS 'Calculation of ROU Asset based on Quantity Retirement Method';
COMMENT ON COLUMN ls_ilr_schedule.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN ls_ilr_schedule.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';
comment on column ls_ilr_schedule.remaining_principal is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc logic';
comment on column ls_ilr_schedule.unaccrued_interest_reclass is 'The reclassification of unaccrued interest from short to long term.';
comment on column ls_ilr_schedule.unaccrued_interest_remeasure is 'The unaccrued interest amount as a result of a remeasurement.';
comment on column ls_ilr_schedule.st_obligation_remeasurement is 'The short term obligation remeasurement amount.';
comment on column ls_ilr_schedule.lt_obligation_remeasurement is 'The long term obligation remeasurement amount.';
comment on column ls_ilr_schedule.obligation_reclass is 'The reclassification of obligation from short to long term.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(10224, 0, 2017, 4, 0, 3, 52420, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052420_lessee_03_rebuild_schedules_for_reclass_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;