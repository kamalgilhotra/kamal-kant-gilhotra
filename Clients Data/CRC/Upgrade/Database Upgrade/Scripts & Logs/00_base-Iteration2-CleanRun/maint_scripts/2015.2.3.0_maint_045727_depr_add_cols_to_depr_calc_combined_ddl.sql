/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045727_depr_add_cols_to_depr_calc_combined_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2.3  06/02/2016 Anand R       Add new columns to combined depr calc arc and stg tables
||============================================================================
*/

alter table DEPR_CALC_COMBINED_STG
    add (CDG_SALVAGE_BASE number(22,2) default 0,
        SALV_AMT_TO_ALLOC number(22,2) default 0,
        CDG_SALVAGE_RATE  number(22,8) default 0 ,
        SALV_FACTOR       number(22,8) default 0, 
        TARGET_SALV_AMT   number(22,2) default 0
        );
        
alter table DEPR_CALC_COMBINED_ARC 
    add (CDG_SALVAGE_BASE number(22,2) default 0,
        SALV_AMT_TO_ALLOC number(22,2) default 0,
        CDG_SALVAGE_RATE  number(22,8) default 0,
        SALV_FACTOR       number(22,8) default 0, 
        TARGET_SALV_AMT   number(22,2) default 0
        );
        
comment on column DEPR_CALC_COMBINED_STG.CDG_SALVAGE_BASE is 'Combined salvage base';
comment on column DEPR_CALC_COMBINED_STG.SALV_AMT_TO_ALLOC is 'Salvage amount to allocate';
comment on column DEPR_CALC_COMBINED_STG.CDG_SALVAGE_RATE is 'Combined salvage rate';
comment on column DEPR_CALC_COMBINED_STG.SALV_FACTOR is 'Salvage factor';
comment on column DEPR_CALC_COMBINED_STG.TARGET_SALV_AMT is 'Target salvage amount';

comment on column DEPR_CALC_COMBINED_ARC.CDG_SALVAGE_BASE is 'Combined salvage base';
comment on column DEPR_CALC_COMBINED_ARC.SALV_AMT_TO_ALLOC is 'Salvage amount to allocate';
comment on column DEPR_CALC_COMBINED_ARC.CDG_SALVAGE_RATE is 'Combined salvage rate';
comment on column DEPR_CALC_COMBINED_ARC.SALV_FACTOR is 'Salvage factor';
comment on column DEPR_CALC_COMBINED_ARC.TARGET_SALV_AMT is 'Target salvage amount';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3203, 0, 2015, 2, 3, 0, 045727, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.3.0_maint_045727_depr_add_cols_to_depr_calc_combined_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;