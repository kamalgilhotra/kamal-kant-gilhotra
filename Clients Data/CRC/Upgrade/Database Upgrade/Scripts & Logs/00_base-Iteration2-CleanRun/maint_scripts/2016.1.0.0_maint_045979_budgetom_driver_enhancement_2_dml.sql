 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045979_budgetom_driver_enhancement_2_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || ----------- ---------- -------------- -------------------------------------
 || 2016.1.0.0  08/17/2016 Anand R       PP-45979. Budget O&M driver enhancements
 ||============================================================================
 */ 

update CR_BUDGET_DATA_LABOR2
set TAB_INDICATOR = -1;

insert into CR_BUDGET_LBR_DRIVER_TEMPLATE 
(LABOR_TEMPLATE_ID, TAB_INDICATOR, COLUMN_ORDER)
values (-1, -1, 1);

update CR_BUDGET_COMPONENT
set DESCRIPTION = 'Driver Based'
where COMPONENT_ID = 1;

update CR_BUDGET_COMPONENT
set DESCRIPTION = 'Non Driver Based'
where COMPONENT_ID = 2;

update CR_BUDGET_DATA_LABOR_SPREAD
set TAB_INDICATOR = -1;

--update to fix existing data at clients already using O&M module
update CR_BUDGET_DATA_ENTRY
set SPLIT_LABOR_ROW = -1
where SPLIT_LABOR_ROW = 1;

--move any custom datawindows on the Labor Driver to the new table, and
--	associate them with the default template and driver
insert into CR_BUDGET_DRIVER_FORMAT(TABLE_NAME, COLUMN_NAME, DDDW_NAME, 
    ID_COLUMN, DISPLAY_COLUMN, WIDTH, DDDW_LINES, DDDW_WIDTH_PERCENT, 
    ALIGNMENT, LABOR_TEMPLATE_ID, TAB_INDICATOR)
select TABLE_NAME, COLUMN_NAME, DDDW_NAME, ID_COLUMN, DISPLAY_COLUMN,
    WIDTH, DDDW_LINES, DDDW_WIDTH_PERCENT, ALIGNMENT, -1, -1
from CR_MASTER_ELEMENT_FORMAT where Trim(lower(TABLE_NAME)) = 'cr_budget_data_labor2_define';

delete from CR_MASTER_ELEMENT_FORMAT 
where Trim(lower(TABLE_NAME)) = 'cr_budget_data_labor2_define';


--change to prevent issues at existing clients
update CR_BUDGET_DATA_ENTRY
set SPLIT_LABOR_ROW = -2
where SPLIT_LABOR_ROW = 2;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3268, 0, 2016, 1, 0, 0, 045979, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045979_budgetom_driver_enhancement_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;