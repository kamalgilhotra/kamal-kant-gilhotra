/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048894_lessor_02_import_vp_formula_comp_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/31/2017 Alex Healey           create new import for VP variable component amounts
||============================================================================
*/
--Create new import type
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name)
VALUES      (511,
             'Add: Var Payment Variable Amounts',
             'VP Variable Components Amounts Add',
             'lsr_import_vp_var_comp_amt',
             'lsr_import_vp_var_comp_amt_arc',
             1,
             'nvo_lsr_logic_import');

--relate the import type to the lessor subsystem
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      (511,
             14);

--insert the columns we will need to use in the import
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      (511,
             'formula_component_id',
             'Formula Component Identifier',
             'formula_component_xlate',
             1,
             1,
             'number(22,0)',
             'lsr_formula_component',
             'formula_component_id');

--insert the columns we will need to use in the import
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      (511,
             'ilr_id',
             'ILR Identifier',
             'ilr_xlate',
             1,
             2,
             'number(22,0)',
             'lsr_asset',
             'ilr_id');

--insert the columns we will need to use in the import
INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      (511,
             'lsr_asset_id',
             'LSR Asset Identifier',
             'lsr_asset_xlate',
             1,
             2,
             'number(22,0)',
             'lsr_asset',
             'lsr_asset_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      (511,
             'incurred_month_number',
             'Month',
             NULL,
             1,
             1,
             'number(6,0)',
             NULL,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      (511,
             'amount',
             'Amount',
             NULL,
             0,
             1,
             'number(28,8)',
             NULL,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column)
VALUES      (511,
             'company_id',
             'Company Identifier',
             'company_xlate',
             1,
             1,
             'number(22,0)',
             'company_setup',
             'company_id');

--Asset Lookups
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1101,
             'Lsr Asset.Description',
             'Lessor Asset Description Lookup',
             'lsr_asset_id',
             '( select distinct b.lsr_asset_id from lsr_asset b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )',
             0,
             'lsr_asset',
             'description');
			 
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1102,
             'Lsr Asset.Long Description',
             'Lessor Asset Long Description Lookup',
             'lsr_asset_id',
             '( select distinct b.lsr_asset_id from lsr_asset b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )',
             0,
             'lsr_asset',
             'long_description');
			 
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      ( 1103,
             'Lsr Asset.Serial Number',
             'Lessor Asset Serial Number Lookup',
             'lsr_asset_id',
             '( select distinct b.lsr_asset_id from lsr_asset b where upper( trim( <importfield> ) ) = upper( trim( b.serial_number ) ) )',
             0,
             'lsr_asset',
             'serial_number');

--add new lookup for VC formula components
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name,
             lookup_values_alternate_sql)
VALUES      (1105,
             'Variable Component.Description',
             'The passed in value corresponds to the LSR Formula Component: Description field for Variable Formula Components. Translate to the Formula Component ID using the Description column on the LSR Formula Component table.',
             'formula_component_id',
             '( select f.formula_component_id from lsr_formula_component f where upper( trim( <importfield> ) ) = upper( trim( f.description ) ) )',
             0,
             'lsr_formula_component',
             'description',
             'select description from lsr_formula_component where component_type_id = 2');

--add the new lookup for ILR ID into the database
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name,
             lookup_values_alternate_sql,
             lookup_constraining_columns)
VALUES      (1106,
             'lsr_ILR.ILR_Number',
             'The passed in value corresponds to ILR Number field',
             'ilr_id',
             '( select b.ilr_id from lsr_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.ilr_number ) ) and <importtable>.company_id = b.company_id )',
             0,
             'lsr_ilr',
             'ilr_number',
             'select ilr_number from lsr_ilr where ilr_id in 
      (select distinct ilr_id from lsr_variable_payment_component a
         inner join lsr_ilr_payment_term_var_pay b
         on a.variable_payment_id = b.variable_payment_id
         where a.formula_component_id in (select formula_component_id from lsr_formula_component where component_type_id = 2)
       union
       select distinct ilr_id from lsr_variable_component_values)',
             'company_id');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name,
             lookup_values_alternate_sql)
VALUES      (1107,
             'Lease Company.Description',
             'The passed in value corresponds to the Lease company description',
             'company_id',
             '( select b.company_id from company b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) and b.is_lease_company = 1 )',
             0,
             'company',
             'description',
             'select description from company where is_lease_company = 1');

--associate the column in the import with the lookup translation
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (511,
             'formula_component_id',
             1105);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (511,
             'ilr_id',
             1106);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (511,
             'company_id',
             1107);
			 
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (511,
             'lsr_asset_id',
             1101);
			 
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (511,
             'lsr_asset_id',
             1102);
			 
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (511,
             'lsr_asset_id',
             1103);
             
--Need to burn some import_template_ids from missing in earlier lessor scripts
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;
SELECT pp_import_template_seq.NEXTVAL FROM dual;

--add the import template (based on max ID + 1) and the fields for the template
DECLARE
    templateid NUMBER;
BEGIN
    templateid := pp_import_template_seq.NEXTVAL;

    --add the specific template into the DB
    INSERT INTO PP_IMPORT_TEMPLATE
                (import_template_id,
                 import_type_id,
                 description,
                 long_description,
                 created_by,
                 created_date,
                 do_update_with_add)
    VALUES      ( templateid,
                 511,
                 'Add: Var Payment Variable Amounts',
                 'LSR Var Payment Variable Amounts',
                 'PWRPLANT',
                 SYSDATE,
                 1 );

    --add the fields for the template into the DB
    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 1,
                 511,
                 'formula_component_id',
                 1105);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 2,
                 511,
                 'company_id',
                 1107);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 3,
                 511,
                 'ilr_id',
                 1106);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 4,
                 511,
                 'lsr_asset_id',
                 1101);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 5,
                 511,
                 'incurred_month_number',
                 NULL);

    INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                (import_template_id,
                 field_id,
                 import_type_id,
                 column_name,
                 import_lookup_id)
    VALUES      (templateid,
                 6,
                 511,
                 'amount',
                 NULL);
END;

/ 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3967, 0, 2017, 1, 0, 0, 48894, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048894_lessor_02_import_vp_formula_comp_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
