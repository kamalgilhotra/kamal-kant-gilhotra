SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030778_proptax_04_rebuild_comp_view.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/27/2013 Lee Quinn      Point release
||============================================================================
*/

-- add powertax company id to the company table
begin
   execute immediate 'alter table company_setup add ( powertax_company_id number(22,0) )';
   DBMS_OUTPUT.PUT_LINE('Column added.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Column already existsed.');
end;
/


-- re-create the view
-- CUSTOMIZE FOR EACH CLIENT AS THE COMPANY_SETUP TABLE MAY HAVE DIFFERENT COLUMNS
CREATE OR REPLACE VIEW COMPANY AS
(
   SELECT company_setup."COMPANY_ID",
          company_setup."TIME_STAMP",
          company_setup."USER_ID",
          company_setup."GL_COMPANY_NO",
          company_setup."OWNED",
          company_setup."DESCRIPTION",
          company_setup."STATUS_CODE_ID",
          company_setup."SHORT_DESCRIPTION",
          company_setup."COMPANY_SUMMARY_ID",
          company_setup."AUTO_LIFE_MONTH",
          company_setup."AUTO_CURVE_MONTH",
          company_setup."AUTO_CLOSE_WO_NUM",
          company_setup."PARENT_COMPANY_ID",
          company_setup."TAX_RETURN_ID",
          company_setup."CLOSING_ROLLUP_ID",
          company_setup."AUTO_LIFE_MONTH_MONTHLY",
          company_setup."COMPANY_TYPE",
          company_setup."COMPANY_EIN",
          company_setup."CWIP_ALLOCATION_MONTHS",
          company_setup."PROP_TAX_COMPANY_ID",
          company_setup."POWERTAX_COMPANY_ID"
     FROM pwrplant.company_setup, pwrplant.pp_company_security
    WHERE company_setup.company_id = pp_company_security.company_id
      AND users = lower(USER)
      AND 1 <> (SELECT COUNT(*) FROM pwrplant.pp_company_security_temp WHERE rownum = 1)
   UNION ALL
   SELECT pp_company_security_temp."COMPANY_ID",
          pp_company_security_temp."TIME_STAMP",
          pp_company_security_temp."USER_ID",
          pp_company_security_temp."GL_COMPANY_NO",
          pp_company_security_temp."OWNED",
          pp_company_security_temp."DESCRIPTION",
          pp_company_security_temp."STATUS_CODE_ID",
          pp_company_security_temp."SHORT_DESCRIPTION",
          pp_company_security_temp."COMPANY_SUMMARY_ID",
          pp_company_security_temp."AUTO_LIFE_MONTH",
          pp_company_security_temp."AUTO_CURVE_MONTH",
          pp_company_security_temp."AUTO_CLOSE_WO_NUM",
          pp_company_security_temp."PARENT_COMPANY_ID",
          pp_company_security_temp."TAX_RETURN_ID",
          pp_company_security_temp."CLOSING_ROLLUP_ID",
          pp_company_security_temp."AUTO_LIFE_MONTH_MONTHLY",
          pp_company_security_temp."COMPANY_TYPE",
          pp_company_security_temp."COMPANY_EIN",
          pp_company_security_temp."CWIP_ALLOCATION_MONTHS",
          pp_company_security_temp."PROP_TAX_COMPANY_ID",
          pp_company_security_temp."POWERTAX_COMPANY_ID"
     FROM pwrplant.pp_company_security_temp);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (540, 0, 10, 4, 1, 0, 30778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030778_proptax_04_rebuild_comp_view.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
