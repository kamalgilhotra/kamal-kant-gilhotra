/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051665_lessee_01_add_payment_shift_import_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.1 06/07/2018 Crystal Yura      Add payment shift to ILR import
||============================================================================
*/


BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_ilr add payment_shift NUMBER(22,0) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/


BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_ilr_archive add payment_shift NUMBER(22,0) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/


COMMENT ON COLUMN ls_import_ilr.payment_shift IS 'The number of months that payments are shifted relative to the schedule months.';
COMMENT ON COLUMN ls_import_ilr_archive.payment_shift IS 'The number of months that payments are shifted relative to the schedule months.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(6582, 0, 2017, 3, 0, 1, 51665, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051665_lessee_01_add_payment_shift_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;