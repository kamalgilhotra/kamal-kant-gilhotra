/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037856_taxrpr_sys_controls.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 05/07/2014 Alex Pivoshenko
||============================================================================
*/

--
--   'Repairs - Ovh to Udgrd Rplcmt Check'
--
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION,
    ALLOW_COMPANY_OVERRIDE)
   select 'Repairs - Ovh to Udgrd Rplcmt Check', LONG_DESCRIPTION, 0, 'Yes', INITCAP(CONTROL_VALUE), 1, 1
     from PP_SYSTEM_CONTROL_COMPANY
    where CONTROL_NAME = 'Tax Exp - Ovh to Udgrd Rplcmt Check'
      and COMPANY_ID = -1;

insert into PPBASE_SYSTEM_OPTIONS_COMPANY
   (SYSTEM_OPTION_ID, COMPANY_ID, OPTION_VALUE)
   select 'Repairs - Ovh to Udgrd Rplcmt Check', COMPANY_ID, INITCAP(CONTROL_VALUE)
     from PP_SYSTEM_CONTROL_COMPANY
    where CONTROL_NAME = 'Tax Exp - Ovh to Udgrd Rplcmt Check'
      and COMPANY_ID <> -1;

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Ovh to Udgrd Rplcmt Check', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Ovh to Udgrd Rplcmt Check', 'No');

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Repairs - Ovh to Udgrd Rplcmt Check', 'REPAIRS');

--
--   'Repairs - Allow Non-Indexed Replace'
--

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION,
    ALLOW_COMPANY_OVERRIDE)
   select 'Repairs - Allow Non-Indexed Replace', LONG_DESCRIPTION, 0, 'Yes', INITCAP(CONTROL_VALUE), 1, 1
     from PP_SYSTEM_CONTROL_COMPANY
    where CONTROL_NAME = 'Tax Exp-Allow Non-Indexed Replace'
      and COMPANY_ID = -1;

insert into PPBASE_SYSTEM_OPTIONS_COMPANY
   (SYSTEM_OPTION_ID, COMPANY_ID, OPTION_VALUE)
   select 'Repairs - Allow Non-Indexed Replace', COMPANY_ID, INITCAP(CONTROL_VALUE)
     from PP_SYSTEM_CONTROL_COMPANY
    where CONTROL_NAME = 'Tax Exp-Allow Non-Indexed Replace'
      and COMPANY_ID <> -1;

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Allow Non-Indexed Replace', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Allow Non-Indexed Replace', 'No');

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Repairs - Allow Non-Indexed Replace', 'REPAIRS');

--
--   'Repairs - Fail Zero Qty Minor'
--

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION,
    ALLOW_COMPANY_OVERRIDE)
   select 'Repairs - Fail Zero Qty Minors', LONG_DESCRIPTION, 0, 'Yes', INITCAP(CONTROL_VALUE), 1, 1
     from PP_SYSTEM_CONTROL_COMPANY
    where CONTROL_NAME = 'REPAIRS-Fail Zero Qty Minors'
      and COMPANY_ID = -1;

insert into PPBASE_SYSTEM_OPTIONS_COMPANY
   (SYSTEM_OPTION_ID, COMPANY_ID, OPTION_VALUE)
   select 'Repairs - Fail Zero Qty Minors', COMPANY_ID, INITCAP(CONTROL_VALUE)
     from PP_SYSTEM_CONTROL_COMPANY
    where CONTROL_NAME = 'REPAIRS-Fail Zero Qty Minors'
      and COMPANY_ID <> -1;

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Fail Zero Qty Minors', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - Fail Zero Qty Minors', 'No');

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Repairs - Fail Zero Qty Minors', 'REPAIRS');

delete from PP_SYSTEM_CONTROL_COMPANY
 where CONTROL_NAME in
       ('Tax Exp - Ovh to Udgrd Rplcmt Check', 'Tax Exp-Allow Non-Indexed Replace', 'REPAIRS-Fail Zero Qty Minors');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1154, 0, 10, 4, 3, 0, 37856, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037856_taxrpr_sys_controls.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;