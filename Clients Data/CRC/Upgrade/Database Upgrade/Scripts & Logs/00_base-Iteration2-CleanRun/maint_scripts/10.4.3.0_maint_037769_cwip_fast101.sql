/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037769_cwip_fast101.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.3.0 05/13/2014 Sunjin Cone
||=================================================================================
*/

drop table UNITIZE_WO_UNITS_STG;

create table UNITIZE_WO_UNITS_STG
(
 ID                    number(22,0),
 COMPANY_ID            number(22,0),
 WORK_ORDER_ID         number(22,0),
 NEW_UNIT_ITEM_ID      number(22,0),
 UNIT_ITEM_SOURCE      varchar2(35),
 RETIREMENT_UNIT_ID    number(22,0),
 UTILITY_ACCOUNT_ID    number(22,0),
 BUS_SEGMENT_ID        number(22,0),
 SUB_ACCOUNT_ID        number(22,0),
 PROPERTY_GROUP_ID     number(22,0),
 ASSET_LOCATION_ID     number(22,0),
 MAJOR_LOCATION_ID     number(22,0),
 WO_MAJOR_LOCATION_ID  number(22,0),
 UA_FUNC_CLASS_ID      number(22,0),
 PROPERTY_UNIT_ID      number(22,0),
 LOCATION_TYPE_ID      number(22,0),
 SERIAL_NUMBER         varchar2(35),
 GL_ACCOUNT_ID         number(22,0),
 ASSET_ACCT_METH_ID    number(22,0),
 ACTIVITY_CODE         varchar2(8),
 DESCRIPTION           varchar2(254),
 SHORT_DESCRIPTION     varchar2(35),
 AMOUNT                number(22,2),
 QUANTITY              number(22,2),
 LDG_ASSET_ID          number(22,0),
 STATUS                number(22,0),
 EST_REVISION          number(22,0),
 UNIT_ESTIMATE_ID      number(22,0),
 MINOR_UNIT_ITEM_ID    number(22,0),
 IN_SERVICE_DATE       date,
 REPLACEMENT_AMOUNT    number(22,2),
 ERROR_MSG             varchar2(2000),
 TIME_STAMP            date,
 USER_ID               varchar2(18)
);

comment on table  UNITIZE_WO_UNITS_STG is '(C) [04] The list of unit items to use during unitization. No primary key.';
comment on column UNITIZE_WO_UNITS_STG.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_UNITS_STG.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_UNITS_STG.NEW_UNIT_ITEM_ID is 'System-assigned identifier of the new unit item.';
comment on column UNITIZE_WO_UNITS_STG.UNIT_ITEM_SOURCE is 'This field indicates if the source of the unit item creation is Actuals, Estimates, or CPR.';
comment on column UNITIZE_WO_UNITS_STG.RETIREMENT_UNIT_ID is 'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_WO_UNITS_STG.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.  It can be a FERC utility plant account such as 314 or the Companys own account number structure.';
comment on column UNITIZE_WO_UNITS_STG.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_WO_UNITS_STG.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_WO_UNITS_STG.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_WO_UNITS_STG.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_WO_UNITS_STG.MAJOR_LOCATION_ID is 'System-assigned identifier of a major location.';
comment on column UNITIZE_WO_UNITS_STG.WO_MAJOR_LOCATION_ID is 'System-assigned identifier of a major location on work order control.';
comment on column UNITIZE_WO_UNITS_STG.UA_FUNC_CLASS_ID is 'System-assigned identifier of a particular functional class on the utility account.';
comment on column UNITIZE_WO_UNITS_STG.PROPERTY_UNIT_ID is 'System-assigned identifier of a particular property unit.';
comment on column UNITIZE_WO_UNITS_STG.LOCATION_TYPE_ID is 'System-assigned identifier of a particular location type.';
comment on column UNITIZE_WO_UNITS_STG.SERIAL_NUMBER is 'Records the serial number associated with the asset component by the manufacturer.';
comment on column UNITIZE_WO_UNITS_STG.GL_ACCOUNT_ID is 'System-assigned identifier of a particular gl account.';
comment on column UNITIZE_WO_UNITS_STG.ASSET_ACCT_METH_ID is 'System-assigned identifier of a particular asset accounting method.';
comment on column UNITIZE_WO_UNITS_STG.ACTIVITY_CODE is 'An activity code from the Activity Code table.';
comment on column UNITIZE_WO_UNITS_STG.DESCRIPTION is 'Description of the particular asset (retirement unit).';
comment on column UNITIZE_WO_UNITS_STG.SHORT_DESCRIPTION is 'Short description filled in by user during unitization.';
comment on column UNITIZE_WO_UNITS_STG.AMOUNT is 'Amount in dollars for the CPR entry.';
comment on column UNITIZE_WO_UNITS_STG.QUANTITY is 'Quantity for the CPR entry.';
comment on column UNITIZE_WO_UNITS_STG.LDG_ASSET_ID is 'System-assigned identifier of the CPR asset identifier for a retirement or transfer.';
comment on column UNITIZE_WO_UNITS_STG.STATUS is 'Internally set processing indicated used in unitization allocation: Null =  From charges; 10 = Minor addition;  11 = From wo estimates';
comment on column UNITIZE_WO_UNITS_STG.EST_REVISION is 'WO Estimate revision used for unit item creation';
comment on column UNITIZE_WO_UNITS_STG.UNIT_ESTIMATE_ID is 'System-assigned identifier of a particular wo estimate.';
comment on column UNITIZE_WO_UNITS_STG.MINOR_UNIT_ITEM_ID is 'Item id from this table for a previously unitized item.  Used for minor items to allow copying of additional details.';
comment on column UNITIZE_WO_UNITS_STG.IN_SERVICE_DATE is 'Engineering in-service date, when using multiple in-service dates for a single work order.  (See PP System Control.)';
comment on column UNITIZE_WO_UNITS_STG.REPLACEMENT_AMOUNT is 'Dollar amount in current dollars to be (Handy Whitman) indexed to get original dollars when the retirement unit is using a retirement method of HW_Fifo or HW_Curve.';
comment on column UNITIZE_WO_UNITS_STG.ERROR_MSG is 'Error message';
comment on column UNITIZE_WO_UNITS_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_UNITS_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create global temporary table UNITIZE_ALLOC_EST_STG_TEMP
(
 WORK_ORDER_ID        number(22,0) not null,
 EXPENDITURE_TYPE_ID  number(22,0) not null,
 REVISION             number(22,0) not null,
 EST_CHG_TYPE_ID      number(22,0) not null,
 UTILITY_ACCOUNT_ID   number(22,0),
 BUS_SEGMENT_ID       number(22,0),
 SUB_ACCOUNT_ID       number(22,0),
 RETIREMENT_UNIT_ID   number(22,0),
 ASSET_LOCATION_ID    number(22,0),
 PROPERTY_GROUP_ID    number(22,0),
 SERIAL_NUMBER        varchar2(35),
 ASSET_ID             number(22,0),
 WO_EST_TRANS_TYPE_ID number(22,0),
 AMOUNT               number(22,2) not null,
 QUANTITY             number(22,2)
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_EST_STG_TEMP is '(C) [04] A global temporary table used in Unitization for summaried estimates. No primary key.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.REVISION is 'WO Estimate revision used for unit item creation';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.EST_CHG_TYPE_ID is 'System-assigned identifier of a particular estimated charge type.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.RETIREMENT_UNIT_ID is  'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.  It can be a FERC utility plant account such as 314 or the Companys own account number structure.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.SERIAL_NUMBER is 'User input serial number.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.ASSET_ID is 'System-assigned identifier of an asset.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.AMOUNT is 'The summarized estimate record amount.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.QUANTITY is 'The summarized estimate record quantity.';
comment on column UNITIZE_ALLOC_EST_STG_TEMP.WO_EST_TRANS_TYPE_ID is 'System-assigned identifier of an estimate transaction type.';


create global temporary table UNITIZE_ALLOC_UNITS_STG_TEMP
(
 WORK_ORDER_ID       number(22,0) not null,
 UNIT_ITEM_ID        number(22,0) not null,
 EXPENDITURE_TYPE_ID number(22,0) not null,
 UTILITY_ACCOUNT_ID  number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 RETIREMENT_UNIT_ID  number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 PROPERTY_GROUP_ID   number(22,0),
 SERIAL_NUMBER       varchar2(35),
 LDG_ASSET_ID        number(22,0),
 UNIT_ESTIMATE_ID    number(22,0),
 QUANTITY            number(22,2)
) on commit preserve rows;

comment on table  UNITIZE_ALLOC_UNITS_STG_TEMP is '(C) [04] A global temporary table used in calculation of the Unitization allocation statistics associated to each eligible Unit Item. No primary key.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.UNIT_ITEM_ID is 'System-assigned identifier of the Unit Item.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.  UADD and MADD are set to 1; all others are set to 2.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.RETIREMENT_UNIT_ID is  'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.  It can be a FERC utility plant account such as 314 or the Companys own account number structure.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.SERIAL_NUMBER is 'User input serial number.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.LDG_ASSET_ID is 'System-assigned identifier of an asset.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.UNIT_ESTIMATE_ID is 'The associated ESTIMATE_ID for Unit Items created from One for One estimates.';
comment on column UNITIZE_ALLOC_UNITS_STG_TEMP.QUANTITY is 'The summarized estimate record quantity.'; 

create table UNITIZE_ALLOC_EST_STATS
(
 COMPANY_ID          number(22,0) not null,
 WORK_ORDER_ID       number(22,0) not null,
 UNIT_ITEM_ID        number(22,0) not null,
 EXPENDITURE_TYPE_ID number(22,0) not null,
 ALLOC_ID            number(22,0) not null,
 ALLOC_TYPE          varchar2(35),
 ALLOC_BASIS         varchar2(35),
 UNITIZE_BY_ACCOUNT  number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 ALLOC_STATISTIC     number(22,2),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);


alter table UNITIZE_ALLOC_EST_STATS
   add constraint PK_UNITIZE_ALLOC_EST_STATS
       primary key (COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, ALLOC_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_EST_STATS is '(C) [04] Summarized and One for One Estimate information used to unitize charge types configured for Estimates based allocation.';
comment on column UNITIZE_ALLOC_EST_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_EST_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_EST_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_EST_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_TYPE is 'Allocation type: estimates.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_BASIS is 'Allocation basis: dollars, quantity.';
comment on column UNITIZE_ALLOC_EST_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_EST_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_EST_STATS.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_ALLOC_EST_STATS.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_EST_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_EST_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1160, 0, 10, 4, 3, 0, 37769, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037769_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;