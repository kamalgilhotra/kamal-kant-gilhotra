/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035731_lease_fix_import_ids.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014  Kyle Peterson
||============================================================================
*/

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   (select 250,
           DESCRIPTION || ' new',
           LONG_DESCRIPTION,
           IMPORT_TABLE_NAME,
           ARCHIVE_TABLE_NAME,
           PP_REPORT_FILTER_ID,
           ALLOW_UPDATES_ON_ADD,
           DELEGATE_OBJECT_NAME,
           ARCHIVE_ADDITIONAL_COLUMNS,
           AUTOCREATE_DESCRIPTION,
           AUTOCREATE_RESTRICT_SQL
      from PP_IMPORT_TYPE
     where LOWER(DESCRIPTION) = 'add: lessors');

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   (select 251,
           DESCRIPTION || ' new',
           LONG_DESCRIPTION,
           IMPORT_TABLE_NAME,
           ARCHIVE_TABLE_NAME,
           PP_REPORT_FILTER_ID,
           ALLOW_UPDATES_ON_ADD,
           DELEGATE_OBJECT_NAME,
           ARCHIVE_ADDITIONAL_COLUMNS,
           AUTOCREATE_DESCRIPTION,
           AUTOCREATE_RESTRICT_SQL
      from PP_IMPORT_TYPE
     where LOWER(DESCRIPTION) = 'add: mla''s');

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   (select 252,
           DESCRIPTION || ' new',
           LONG_DESCRIPTION,
           IMPORT_TABLE_NAME,
           ARCHIVE_TABLE_NAME,
           PP_REPORT_FILTER_ID,
           ALLOW_UPDATES_ON_ADD,
           DELEGATE_OBJECT_NAME,
           ARCHIVE_ADDITIONAL_COLUMNS,
           AUTOCREATE_DESCRIPTION,
           AUTOCREATE_RESTRICT_SQL
      from PP_IMPORT_TYPE
     where LOWER(DESCRIPTION) = 'add: ilr''s');

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   (select 253,
           DESCRIPTION || ' new',
           LONG_DESCRIPTION,
           IMPORT_TABLE_NAME,
           ARCHIVE_TABLE_NAME,
           PP_REPORT_FILTER_ID,
           ALLOW_UPDATES_ON_ADD,
           DELEGATE_OBJECT_NAME,
           ARCHIVE_ADDITIONAL_COLUMNS,
           AUTOCREATE_DESCRIPTION,
           AUTOCREATE_RESTRICT_SQL
      from PP_IMPORT_TYPE
     where LOWER(DESCRIPTION) = 'add: leased assets');

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   (select 254,
           DESCRIPTION || ' new',
           LONG_DESCRIPTION,
           IMPORT_TABLE_NAME,
           ARCHIVE_TABLE_NAME,
           PP_REPORT_FILTER_ID,
           ALLOW_UPDATES_ON_ADD,
           DELEGATE_OBJECT_NAME,
           ARCHIVE_ADDITIONAL_COLUMNS,
           AUTOCREATE_DESCRIPTION,
           AUTOCREATE_RESTRICT_SQL
      from PP_IMPORT_TYPE
     where LOWER(DESCRIPTION) = 'add: lease invoices');

update PP_IMPORT_TYPE_SUBSYSTEM
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors');

update PP_IMPORT_TYPE_SUBSYSTEM
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s');

update PP_IMPORT_TYPE_SUBSYSTEM
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s');

update PP_IMPORT_TYPE_SUBSYSTEM
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: leased assets new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: leased assets');

update PP_IMPORT_TYPE_SUBSYSTEM
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: lease invoices new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lease invoices');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   (select 250,
           COLUMN_NAME,
           DESCRIPTION,
           IMPORT_COLUMN_NAME,
           IS_REQUIRED,
           PROCESSING_ORDER,
           COLUMN_TYPE,
           PARENT_TABLE,
           PARENT_TABLE_PK_COLUMN2,
           IS_ON_TABLE,
           AUTOCREATE_IMPORT_TYPE_ID,
           PARENT_TABLE_PK_COLUMN
      from PP_IMPORT_COLUMN
     where IMPORT_TYPE_ID =
           (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors'));

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   (select 251,
           COLUMN_NAME,
           DESCRIPTION,
           IMPORT_COLUMN_NAME,
           IS_REQUIRED,
           PROCESSING_ORDER,
           COLUMN_TYPE,
           PARENT_TABLE,
           PARENT_TABLE_PK_COLUMN2,
           IS_ON_TABLE,
           AUTOCREATE_IMPORT_TYPE_ID,
           PARENT_TABLE_PK_COLUMN
      from PP_IMPORT_COLUMN
     where IMPORT_TYPE_ID =
           (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s'));

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   (select 252,
           COLUMN_NAME,
           DESCRIPTION,
           IMPORT_COLUMN_NAME,
           IS_REQUIRED,
           PROCESSING_ORDER,
           COLUMN_TYPE,
           PARENT_TABLE,
           PARENT_TABLE_PK_COLUMN2,
           IS_ON_TABLE,
           AUTOCREATE_IMPORT_TYPE_ID,
           PARENT_TABLE_PK_COLUMN
      from PP_IMPORT_COLUMN
     where IMPORT_TYPE_ID =
           (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s'));

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   (select 253,
           COLUMN_NAME,
           DESCRIPTION,
           IMPORT_COLUMN_NAME,
           IS_REQUIRED,
           PROCESSING_ORDER,
           COLUMN_TYPE,
           PARENT_TABLE,
           PARENT_TABLE_PK_COLUMN2,
           IS_ON_TABLE,
           AUTOCREATE_IMPORT_TYPE_ID,
           PARENT_TABLE_PK_COLUMN
      from PP_IMPORT_COLUMN
     where IMPORT_TYPE_ID = (select IMPORT_TYPE_ID
                               from PP_IMPORT_TYPE
                              where LOWER(DESCRIPTION) = 'add: leased assets'));

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   (select 254,
           COLUMN_NAME,
           DESCRIPTION,
           IMPORT_COLUMN_NAME,
           IS_REQUIRED,
           PROCESSING_ORDER,
           COLUMN_TYPE,
           PARENT_TABLE,
           PARENT_TABLE_PK_COLUMN2,
           IS_ON_TABLE,
           AUTOCREATE_IMPORT_TYPE_ID,
           PARENT_TABLE_PK_COLUMN
      from PP_IMPORT_COLUMN
     where IMPORT_TYPE_ID = (select IMPORT_TYPE_ID
                               from PP_IMPORT_TYPE
                              where LOWER(DESCRIPTION) = 'add: lease invoices'));

update PP_IMPORT_COLUMN_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors');

update PP_IMPORT_COLUMN_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s');

update PP_IMPORT_COLUMN_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s');

update PP_IMPORT_COLUMN_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: leased assets new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: leased assets');

update PP_IMPORT_COLUMN_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: lease invoices new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lease invoices');

update PP_IMPORT_TYPE_UPDATES_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors');

update PP_IMPORT_TYPE_UPDATES_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s');

update PP_IMPORT_TYPE_UPDATES_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s');

update PP_IMPORT_TYPE_UPDATES_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: leased assets new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: leased assets');

update PP_IMPORT_TYPE_UPDATES_LOOKUP
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: lease invoices new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lease invoices');

update PP_IMPORT_TEMPLATE_FIELDS
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors');

update PP_IMPORT_TEMPLATE_FIELDS
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s');

update PP_IMPORT_TEMPLATE_FIELDS
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s');

update PP_IMPORT_TEMPLATE_FIELDS
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: leased assets new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: leased assets');

update PP_IMPORT_TEMPLATE_FIELDS
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: lease invoices new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lease invoices');

update PP_IMPORT_TEMPLATE
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lessors');

update PP_IMPORT_TEMPLATE
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: mla''s');

update PP_IMPORT_TEMPLATE
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: ilr''s');

update PP_IMPORT_TEMPLATE
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: leased assets new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: leased assets');

update PP_IMPORT_TEMPLATE
   set (IMPORT_TYPE_ID) =
        (select IMPORT_TYPE_ID
           from PP_IMPORT_TYPE
          where LOWER(DESCRIPTION) = 'add: lease invoices new')
 where IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where LOWER(DESCRIPTION) = 'add: lease invoices');

delete from PP_IMPORT_TYPE_UPDATES_LOOKUP
 where IMPORT_TYPE_ID in (select IMPORT_TYPE_ID
                            from PP_IMPORT_TYPE
                           where LOWER(DESCRIPTION) in ('add: lessors',
                                                        'add: mla''s',
                                                        'add: ilr''s',
                                                        'add: leased assets',
                                                        'add: lease invoices'));

delete from PP_IMPORT_COLUMN_LOOKUP
 where IMPORT_TYPE_ID in (select IMPORT_TYPE_ID
                            from PP_IMPORT_TYPE
                           where LOWER(DESCRIPTION) in ('add: lessors',
                                                        'add: mla''s',
                                                        'add: ilr''s',
                                                        'add: leased assets',
                                                        'add: lease invoices'));

delete from PP_IMPORT_COLUMN
 where IMPORT_TYPE_ID in (select IMPORT_TYPE_ID
                            from PP_IMPORT_TYPE
                           where LOWER(DESCRIPTION) in ('add: lessors',
                                                        'add: mla''s',
                                                        'add: ilr''s',
                                                        'add: leased assets',
                                                        'add: lease invoices'));

delete from PP_IMPORT_TYPE_SUBSYSTEM
 where IMPORT_TYPE_ID in (select IMPORT_TYPE_ID
                            from PP_IMPORT_TYPE
                           where LOWER(DESCRIPTION) in ('add: lessors',
                                                        'add: mla''s',
                                                        'add: ilr''s',
                                                        'add: leased assets',
                                                        'add: lease invoices'));

delete from PP_IMPORT_TYPE
 where LOWER(DESCRIPTION) in
       ('add: lessors', 'add: mla''s', 'add: ilr''s', 'add: leased assets', 'add: lease invoices');

update PP_IMPORT_TYPE
   set DESCRIPTION = SUBSTR(DESCRIPTION, 1, INSTR(DESCRIPTION, ' new', 1, 1))
 where IMPORT_TYPE_ID between 250 and 254;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (886, 0, 10, 4, 2, 0, 35731, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035731_lease_fix_import_ids.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;