/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040144_depr_PKG_PP_DEPR_COMMON.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 
||============================================================================
*/

create or replace package PKG_PP_DEPR_COMMON
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: PKG_PP_DEPR_COMMON
|| Description: Common depr objects and functions
||============================================================================
|| Copyright (C) 2013 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version Date       Revised By     Reason for Change
|| ------- ---------- -------------- -----------------------------------------
|| 1.0      06/04/2013 Brandon Beck   initial creation
||============================================================================
*/
 as
   function F_GETDEPRPROCESS return number;
end PKG_PP_DEPR_COMMON;
/

create or replace package body PKG_PP_DEPR_COMMON as
   --**************************************************************
   --         TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --         VARIABLES
   --**************************************************************
   L_DEPRPROCESSID number;

   --**************************************************************
   --         PROCEDURES
   --**************************************************************
   procedure P_SETPROCESS is

   begin
      select PROCESS_ID
        into L_DEPRPROCESSID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = 'DEPRECIATION CALCULATION';
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_DEPRPROCESSID := -1;
   end P_SETPROCESS;

   --**************************************************************
   --         FUNCTIONS
   --**************************************************************
   -- Returns the process id for depr calc
   function F_GETDEPRPROCESS return number is

   begin
      return L_DEPRPROCESSID;
   end F_GETDEPRPROCESS;

begin
   -- initialize the package
   P_SETPROCESS;
end PKG_PP_DEPR_COMMON;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1493, 0, 10, 4, 3, 0, 40144, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040144_depr_PKG_PP_DEPR_COMMON.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;