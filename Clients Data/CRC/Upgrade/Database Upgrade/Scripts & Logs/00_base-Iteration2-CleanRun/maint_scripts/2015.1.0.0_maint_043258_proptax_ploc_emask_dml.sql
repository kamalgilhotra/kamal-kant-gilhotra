--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_043258_proptax_ploc_emask_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/13/2015 A Scott          Table maint shows pt location descriptions
--||                                        as "254" in grid.  remove bad edit mask.
--||============================================================================
--*/

update powerplant_columns
set edit_mask = ''
where lower(table_name) = 'prop_tax_location'
and lower(column_name) = 'description';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2372, 0, 2015, 1, 0, 0, 43258, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043258_proptax_ploc_emask_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;