/*
||==========================================================================================
|| Application: Lessee
|| File Name:   maint_041652_lessee_rates_pk_ddl.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 12/10/2014 B.Beck    	 Change the primary key for the lease tax rates
||==========================================================================================
*/
alter table ls_tax_state_rates drop primary key drop index;
alter table ls_tax_district_rates drop primary key drop index;

alter table ls_tax_state_rates
add constraint pk_ls_tax_state_rates
primary key (tax_local_id, state_id, effective_date);

alter table ls_tax_district_rates
add constraint pk_ls_tax_district_rates
primary key (tax_local_id, ls_tax_district_id, effective_date);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2111, 0, 10, 4, 3, 2, 041652, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041652_lessee_rates_pk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;