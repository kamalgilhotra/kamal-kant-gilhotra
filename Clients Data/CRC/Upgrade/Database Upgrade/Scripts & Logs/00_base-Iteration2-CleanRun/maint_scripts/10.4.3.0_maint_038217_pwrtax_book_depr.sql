/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038217_pwrtax_book_depr.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/21/2014 Alex P.
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'deferred_input_process_depr', 'Process Book Depr Alloc', 'uo_tax_dfit_act_wksp_processing',
    'Process Book Depreciation Allocation');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'deferred_input_process_depr', LABEL = 'Process Book Depr Alloc'
 where MODULE = 'powertax'
   and MENU_IDENTIFIER = 'deferred_input_process_depr';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1365, 0, 10, 4, 3, 0, 38217, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038217_pwrtax_book_depr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
