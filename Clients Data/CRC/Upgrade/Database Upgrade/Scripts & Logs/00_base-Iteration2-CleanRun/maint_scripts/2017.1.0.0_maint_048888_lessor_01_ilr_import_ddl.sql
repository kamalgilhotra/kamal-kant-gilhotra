/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048888_lessor_01_ilr_import_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/18/2017 Shane "C" Ward		Set up Lessor ILR Import Tables
||============================================================================
*/

CREATE TABLE LSR_IMPORT_ILR
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     time_stamp                 DATE NULL,
     user_id                    VARCHAR2(18) NULL,
     ilr_id                     NUMBER(22, 0) NULL,
     ilr_number                 VARCHAR2(35) NULL,
     lease_xlate                VARCHAR2(254) NULL,
     lease_id                   NUMBER(22, 0) NULL,
     company_xlate              VARCHAR2(254) NULL,
     company_id                 NUMBER(22, 0) NULL,
     est_in_svc_date            VARCHAR2(254) NULL,
     ilr_group_xlate            VARCHAR2(254) NULL,
     ilr_group_id               NUMBER(22, 0) NULL,
     workflow_type_xlate        VARCHAR2(254) NULL,
     workflow_type_id           NUMBER(22, 0) NULL,
     external_ilr               VARCHAR2(35) NULL,
     notes                      VARCHAR2(4000) NULL,
     inception_air              NUMBER(22, 8) NULL,
     purchase_option_type_xlate VARCHAR2(254) NULL,
     purchase_option_type_id    NUMBER(22, 0) NULL,
     purchase_option_amt        NUMBER(22, 2) NULL,
     renewal_option_type_xlate  VARCHAR2(254) NULL,
     renewal_option_type_id     NUMBER(22, 0) NULL,
     cancelable_type_xlate      VARCHAR2(254) NULL,
     cancelable_type_id         NUMBER(22, 0) NULL,
     itc_sw                     VARCHAR2(35) NULL,
     cap_type_xlate             VARCHAR2(254) NULL,
     cap_type_id                NUMBER(22, 0) NULL,
     termination_amt            NUMBER(22, 2) NULL,
     payment_term_id            NUMBER(22, 0) NULL,
     payment_term_id_xlate      VARCHAR2(254) NULL,
     payment_term_date          VARCHAR2(254) NULL,
     payment_freq_xlate         VARCHAR2(254) NULL,
     payment_freq_id            NUMBER(22, 0) NULL,
     number_of_terms            NUMBER(22, 0) NULL,
     paid_amount                NUMBER(22, 2) NULL,
     est_executory_cost         NUMBER(22, 2) NULL,
     contingent_amount          NUMBER(22, 2) NULL,
     class_code_xlate1          VARCHAR2(254) NULL,
     class_code_id1             NUMBER(22, 0) NULL,
     class_code_value1          VARCHAR2(254) NULL,
     class_code_xlate2          VARCHAR2(254) NULL,
     class_code_id2             NUMBER(22, 0) NULL,
     class_code_value2          VARCHAR2(254) NULL,
     class_code_xlate3          VARCHAR2(254) NULL,
     class_code_id3             NUMBER(22, 0) NULL,
     class_code_value3          VARCHAR2(254) NULL,
     class_code_xlate4          VARCHAR2(254) NULL,
     class_code_id4             NUMBER(22, 0) NULL,
     class_code_value4          VARCHAR2(254) NULL,
     class_code_xlate5          VARCHAR2(254) NULL,
     class_code_id5             NUMBER(22, 0) NULL,
     class_code_value5          VARCHAR2(254) NULL,
     class_code_xlate6          VARCHAR2(254) NULL,
     class_code_id6             NUMBER(22, 0) NULL,
     class_code_value6          VARCHAR2(254) NULL,
     class_code_xlate7          VARCHAR2(254) NULL,
     class_code_id7             NUMBER(22, 0) NULL,
     class_code_value7          VARCHAR2(254) NULL,
     class_code_xlate8          VARCHAR2(254) NULL,
     class_code_id8             NUMBER(22, 0) NULL,
     class_code_value8          VARCHAR2(254) NULL,
     class_code_xlate9          VARCHAR2(254) NULL,
     class_code_id9             NUMBER(22, 0) NULL,
     class_code_value9          VARCHAR2(254) NULL,
     class_code_xlate10         VARCHAR2(254) NULL,
     class_code_id10            NUMBER(22, 0) NULL,
     class_code_value10         VARCHAR2(254) NULL,
     class_code_xlate11         VARCHAR2(254) NULL,
     class_code_id11            NUMBER(22, 0) NULL,
     class_code_value11         VARCHAR2(254) NULL,
     class_code_xlate12         VARCHAR2(254) NULL,
     class_code_id12            NUMBER(22, 0) NULL,
     class_code_value12         VARCHAR2(254) NULL,
     class_code_xlate13         VARCHAR2(254) NULL,
     class_code_id13            NUMBER(22, 0) NULL,
     class_code_value13         VARCHAR2(254) NULL,
     class_code_xlate14         VARCHAR2(254) NULL,
     class_code_id14            NUMBER(22, 0) NULL,
     class_code_value14         VARCHAR2(254) NULL,
     class_code_xlate15         VARCHAR2(254) NULL,
     class_code_id15            NUMBER(22, 0) NULL,
     class_code_value15         VARCHAR2(254) NULL,
     class_code_xlate16         VARCHAR2(254) NULL,
     class_code_id16            NUMBER(22, 0) NULL,
     class_code_value16         VARCHAR2(254) NULL,
     class_code_xlate17         VARCHAR2(254) NULL,
     class_code_id17            NUMBER(22, 0) NULL,
     class_code_value17         VARCHAR2(254) NULL,
     class_code_xlate18         VARCHAR2(254) NULL,
     class_code_id18            NUMBER(22, 0) NULL,
     class_code_value18         VARCHAR2(254) NULL,
     class_code_xlate19         VARCHAR2(254) NULL,
     class_code_id19            NUMBER(22, 0) NULL,
     class_code_value19         VARCHAR2(254) NULL,
     class_code_xlate20         VARCHAR2(254) NULL,
     class_code_id20            NUMBER(22, 0) NULL,
     class_code_value20         VARCHAR2(254) NULL,
     loaded                     NUMBER(22, 0) NULL,
     is_modified                NUMBER(22, 0) NULL,
     error_message              VARCHAR2(4000) NULL,
     unique_ilr_identifier      VARCHAR2(35) NULL,
     c_bucket_1                 NUMBER(22, 2) NULL,
     c_bucket_2                 NUMBER(22, 2) NULL,
     c_bucket_3                 NUMBER(22, 2) NULL,
     c_bucket_4                 NUMBER(22, 2) NULL,
     c_bucket_5                 NUMBER(22, 2) NULL,
     c_bucket_6                 NUMBER(22, 2) NULL,
     c_bucket_7                 NUMBER(22, 2) NULL,
     c_bucket_8                 NUMBER(22, 2) NULL,
     c_bucket_9                 NUMBER(22, 2) NULL,
     c_bucket_10                NUMBER(22, 2) NULL,
     e_bucket_1                 NUMBER(22, 2) NULL,
     e_bucket_2                 NUMBER(22, 2) NULL,
     e_bucket_3                 NUMBER(22, 2) NULL,
     e_bucket_4                 NUMBER(22, 2) NULL,
     e_bucket_5                 NUMBER(22, 2) NULL,
     e_bucket_6                 NUMBER(22, 2) NULL,
     e_bucket_7                 NUMBER(22, 2) NULL,
     e_bucket_8                 NUMBER(22, 2) NULL,
     e_bucket_9                 NUMBER(22, 2) NULL,
     e_bucket_10                NUMBER(22, 2) NULL,
     sublease_flag              NUMBER(1, 0) NULL,
     sublease_flag_xlate        VARCHAR2(35) NULL,
     sublease_id                NUMBER(22, 0) NULL,
     sublease_id_xlate          VARCHAR2(254) NULL,
     likely_to_collect          NUMBER(1, 0) NULL,
     likely_to_collect_xlate    VARCHAR2(35) NULL,
     intent_to_purch            NUMBER(1, 0) NULL,
     intent_to_purch_xlate      VARCHAR2(35),
     specialized_asset          NUMBER(1, 0) NULL,
     specialized_asset_xlate    VARCHAR2(35) NULL,
     intercompany_lease         NUMBER(1, 0) NULL,
     intercompany_lease_xlate   VARCHAR2(35) NULL,
     intercompany_company       NUMBER(22, 0) NULL,
     intercompany_company_xlate VARCHAR2(254) NULL,
     init_direct_cost_amt       NUMBER(22, 2) NULL,
     sales_type_disc_rate       NUMBER(22, 8) NULL,
     direct_fin_disc_rate       NUMBER(22, 8) NULL,
     direct_fin_fmv_rate        NUMBER(22, 8) NULL,
     direct_fin_int_rate        NUMBER(22, 8) NULL
  );

ALTER TABLE LSR_IMPORT_ILR
  ADD CONSTRAINT lsr_import_ilr_pk PRIMARY KEY ( import_run_id, line_id );

ALTER TABLE LSR_IMPORT_ILR
  ADD CONSTRAINT lsr_import_ilr_run_fk FOREIGN KEY ( import_run_id ) REFERENCES PP_IMPORT_RUN ( import_run_id );

COMMENT ON TABLE LSR_IMPORT_ILR IS '(S)  [06]
The LS Import ILR table is an API table used to import Lessor ILRs.';

COMMENT ON COLUMN LSR_IMPORT_ILR.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPORT_ILR.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPORT_ILR.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR.ilr_id IS 'The internal ILR id within PowerPlant .';

COMMENT ON COLUMN LSR_IMPORT_ILR.ilr_number IS 'A field to hold the ilr number.';

COMMENT ON COLUMN LSR_IMPORT_ILR.lease_xlate IS 'Translation field for determining the MLA.';

COMMENT ON COLUMN LSR_IMPORT_ILR.lease_id IS 'The internal lease id within PowerPlant .';

COMMENT ON COLUMN LSR_IMPORT_ILR.company_xlate IS 'Translation field for determining the company.';

COMMENT ON COLUMN LSR_IMPORT_ILR.company_id IS 'The company.';

COMMENT ON COLUMN LSR_IMPORT_ILR.est_in_svc_date IS 'The date the ILR comes in service.';

COMMENT ON COLUMN LSR_IMPORT_ILR.ilr_group_xlate IS 'Translation field for determining the ILR group.';

COMMENT ON COLUMN LSR_IMPORT_ILR.ilr_group_id IS 'An internal PowerPlant id representing the ILR Group for the ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR.workflow_type_xlate IS 'Translation field for determining the workflow type.';

COMMENT ON COLUMN LSR_IMPORT_ILR.workflow_type_id IS 'An internal id used for routing records to approval.';

COMMENT ON COLUMN LSR_IMPORT_ILR.external_ilr IS 'A field to hold the relationship between PowerPlant and an external system.';

COMMENT ON COLUMN LSR_IMPORT_ILR.notes IS 'A freeform field for entering notes.';

COMMENT ON COLUMN LSR_IMPORT_ILR.inception_air IS 'The interest rate used to determine interest payments.  Also used to determine net present value.';

COMMENT ON COLUMN LSR_IMPORT_ILR.purchase_option_type_xlate IS 'Translation field for determining the purchase option type.';

COMMENT ON COLUMN LSR_IMPORT_ILR.purchase_option_type_id IS 'Represents if this ILR has a purchase option.';

COMMENT ON COLUMN LSR_IMPORT_ILR.purchase_option_amt IS 'The amount of the purchase option.';

COMMENT ON COLUMN LSR_IMPORT_ILR.renewal_option_type_xlate IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.renewal_option_type_id IS 'Represents if this ILR has a renewal option.';

COMMENT ON COLUMN LSR_IMPORT_ILR.cancelable_type_xlate IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.cancelable_type_id IS 'A PowerPlant internal id representing whether or not a lease can be cancelled before the lease terms are up.';

COMMENT ON COLUMN LSR_IMPORT_ILR.itc_sw IS 'A flag identifying whether this ILR has investment tax credits associate with it.';

COMMENT ON COLUMN LSR_IMPORT_ILR.cap_type_xlate IS 'Translation field for determining the lease cap type.';

COMMENT ON COLUMN LSR_IMPORT_ILR.cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';

COMMENT ON COLUMN LSR_IMPORT_ILR.termination_amt IS 'The amount for terminating the lease.';

COMMENT ON COLUMN LSR_IMPORT_ILR.payment_term_id IS 'System-assigned ID for this ILR payment term.';

COMMENT ON COLUMN LSR_IMPORT_ILR.payment_term_id_xlate IS 'Translation for System-assigned ID for this ILR payment term.';

COMMENT ON COLUMN LSR_IMPORT_ILR.payment_term_date IS 'The payment term date for this ILR payment term.';

COMMENT ON COLUMN LSR_IMPORT_ILR.payment_freq_xlate IS 'Translation field for determining the payment frequency.';

COMMENT ON COLUMN LSR_IMPORT_ILR.payment_freq_id IS 'The payment frequency on this ILR payment term. (1: Annual, 2: Semi-Annual, 3: Quarterly, 4: Monthly, 5: Actual).';

COMMENT ON COLUMN LSR_IMPORT_ILR.number_of_terms IS 'The number of payments on this ILR payment term record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.paid_amount IS 'The amount to be paid each term for this ILR payment term.';

COMMENT ON COLUMN LSR_IMPORT_ILR.est_executory_cost IS 'The estimated executory cost.';

COMMENT ON COLUMN LSR_IMPORT_ILR.contingent_amount IS 'The contingent amount to be paid.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate1 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id1 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value1 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate2 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id2 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value2 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate3 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id3 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value3 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate4 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id4 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value4 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate5 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id5 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value5 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate6 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id6 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value6 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate7 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id7 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value7 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate8 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id8 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value8 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate9 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id9 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value9 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate10 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id10 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value10 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate11 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id11 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value11 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate12 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id12 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value12 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate13 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id13 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value13 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate14 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id14 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value14 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate15 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id15 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value15 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate16 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id16 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value16 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate17 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id17 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value17 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate18 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id18 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value18 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate19 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id19 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value19 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_xlate20 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_id20 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR.class_code_value20 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';

COMMENT ON COLUMN LSR_IMPORT_ILR.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';

COMMENT ON COLUMN LSR_IMPORT_ILR.error_message IS 'Error messages resulting from data valdiation in the import process.';

COMMENT ON COLUMN LSR_IMPORT_ILR.unique_ilr_identifier IS 'Unique identifier for this ILR, for this import run.  Used to link rows in this table that are under the same ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_1 IS 'A column holding the contingent amount for bucket 1';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_2 IS 'A column holding the contingent amount for bucket 2';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_3 IS 'A column holding the contingent amount for bucket 3';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_4 IS 'A column holding the contingent amount for bucket 4';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_5 IS 'A column holding the contingent amount for bucket 5';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_6 IS 'A column holding the contingent amount for bucket 6';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_7 IS 'A column holding the contingent amount for bucket 7';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_8 IS 'A column holding the contingent amount for bucket 8';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_9 IS 'A column holding the contingent amount for bucket 9';

COMMENT ON COLUMN LSR_IMPORT_ILR.c_bucket_10 IS 'A column holding the contingent amount for bucket 10';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_1 IS 'A column holding the executory amount for bucket 1';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_2 IS 'A column holding the executory amount for bucket 2';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_3 IS 'A column holding the executory amount for bucket 3';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_4 IS 'A column holding the executory amount for bucket 4';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_5 IS 'A column holding the executory amount for bucket 5';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_6 IS 'A column holding the executory amount for bucket 6';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_7 IS 'A column holding the executory amount for bucket 7';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_8 IS 'A column holding the executory amount for bucket 8';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_9 IS 'A column holding the executory amount for bucket 9';

COMMENT ON COLUMN LSR_IMPORT_ILR.e_bucket_10 IS 'A column holding the executory amount for bucket 10';

COMMENT ON COLUMN LSR_IMPORT_ILR.sublease_flag IS 'ID for Yes/No on whether ILR has sublease. Is the lessee subleasing the asset?';

COMMENT ON COLUMN LSR_IMPORT_ILR.sublease_flag_xlate IS 'Translate Column for ID for Yes/No on whether ILR has sublease';

COMMENT ON COLUMN LSR_IMPORT_ILR.sublease_id IS 'ID of Sublease for ILR (ls_lease)';

COMMENT ON COLUMN LSR_IMPORT_ILR.sublease_id_xlate IS 'ID of Sublease for ILR (ls_lease)';

COMMENT ON COLUMN LSR_IMPORT_ILR.likely_to_collect IS 'ID for Yes/No on whether ILR is Likely to Collect. Is the business reasonably likely to collect on the lease payments?';

COMMENT ON COLUMN LSR_IMPORT_ILR.likely_to_collect_xlate IS 'Translate Column ID for Yes/No on whether ILR is Likely to Collect';

COMMENT ON COLUMN LSR_IMPORT_ILR.intent_to_purch IS 'ID for Yes/No for Intent to Purchase. Is the business planning on the lessee purchasing the asset.';

COMMENT ON COLUMN LSR_IMPORT_ILR.intent_to_purch_xlate IS 'Translate Column ID for Yes/No for Intent to Purchase';

COMMENT ON COLUMN LSR_IMPORT_ILR.specialized_asset IS 'ID for Yes/No for Specialized Asset. Is the asset so specialized in nature, there would be no other use for it after the term of the lease?';

COMMENT ON COLUMN LSR_IMPORT_ILR.specialized_asset_xlate IS 'Translate Column ID for Yes/No for Specialized Asset';

COMMENT ON COLUMN LSR_IMPORT_ILR.intercompany_lease IS 'ID for Yes/No for whether ILR is an Intercompany Lease. Is the lease with another company in the business?';

COMMENT ON COLUMN LSR_IMPORT_ILR.intercompany_lease_xlate IS 'Translate Column ID for Yes/No for whether ILR is an Intercompany Lease';

COMMENT ON COLUMN LSR_IMPORT_ILR.intercompany_company IS 'ID of Company ILR is associated with for Intercompany Lease. If the lease is with another company within the business, enter the company description here.';

COMMENT ON COLUMN LSR_IMPORT_ILR.intercompany_company_xlate IS 'Translate Column for ID of Company ILR is associated with for Intercompany Lease';

COMMENT ON COLUMN LSR_IMPORT_ILR.init_direct_cost_amt IS 'If there are initial direct costs, what is the total amount?';

COMMENT ON COLUMN LSR_IMPORT_ILR.sales_type_disc_rate IS 'Corresponds to the rate used throughout the Sales-Type lease calculations.';

COMMENT ON COLUMN LSR_IMPORT_ILR.direct_fin_disc_rate IS 'Corresponds to the rate used to calculate the Net Investment and all schedule calculations except for interest on Net Investment';

COMMENT ON COLUMN LSR_IMPORT_ILR.direct_fin_fmv_rate IS 'Corresponds to the rate used in the FMV comparison test to determine if lease is operating or not';

COMMENT ON COLUMN LSR_IMPORT_ILR.direct_fin_int_rate IS 'Corresponds to the rate used to calculate interest on the Net Investment';

CREATE TABLE LSR_IMPORT_ILR_ARCHIVE
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     time_stamp                 DATE NULL,
     user_id                    VARCHAR2(18) NULL,
     ilr_id                     NUMBER(22, 0) NULL,
     ilr_number                 VARCHAR2(35) NULL,
     lease_xlate                VARCHAR2(254) NULL,
     lease_id                   NUMBER(22, 0) NULL,
     company_xlate              VARCHAR2(254) NULL,
     company_id                 NUMBER(22, 0) NULL,
     est_in_svc_date            VARCHAR2(254) NULL,
     ilr_group_xlate            VARCHAR2(254) NULL,
     ilr_group_id               NUMBER(22, 0) NULL,
     workflow_type_xlate        VARCHAR2(254) NULL,
     workflow_type_id           NUMBER(22, 0) NULL,
     external_ilr               VARCHAR2(35) NULL,
     notes                      VARCHAR2(4000) NULL,
     inception_air              NUMBER(22, 8) NULL,
     purchase_option_type_xlate VARCHAR2(254) NULL,
     purchase_option_type_id    NUMBER(22, 0) NULL,
     purchase_option_amt        NUMBER(22, 2) NULL,
     renewal_option_type_xlate  VARCHAR2(254) NULL,
     renewal_option_type_id     NUMBER(22, 0) NULL,
     cancelable_type_xlate      VARCHAR2(254) NULL,
     cancelable_type_id         NUMBER(22, 0) NULL,
     itc_sw                     VARCHAR2(35) NULL,
     cap_type_xlate             VARCHAR2(254) NULL,
     cap_type_id                NUMBER(22, 0) NULL,
     termination_amt            NUMBER(22, 2) NULL,
     payment_term_id            NUMBER(22, 0) NULL,
     payment_term_id_xlate      VARCHAR2(254) NULL,
     payment_term_date          VARCHAR2(254) NULL,
     payment_freq_xlate         VARCHAR2(254) NULL,
     payment_freq_id            NUMBER(22, 0) NULL,
     number_of_terms            NUMBER(22, 0) NULL,
     paid_amount                NUMBER(22, 2) NULL,
     est_executory_cost         NUMBER(22, 2) NULL,
     contingent_amount          NUMBER(22, 2) NULL,
     class_code_xlate1          VARCHAR2(254) NULL,
     class_code_id1             NUMBER(22, 0) NULL,
     class_code_value1          VARCHAR2(254) NULL,
     class_code_xlate2          VARCHAR2(254) NULL,
     class_code_id2             NUMBER(22, 0) NULL,
     class_code_value2          VARCHAR2(254) NULL,
     class_code_xlate3          VARCHAR2(254) NULL,
     class_code_id3             NUMBER(22, 0) NULL,
     class_code_value3          VARCHAR2(254) NULL,
     class_code_xlate4          VARCHAR2(254) NULL,
     class_code_id4             NUMBER(22, 0) NULL,
     class_code_value4          VARCHAR2(254) NULL,
     class_code_xlate5          VARCHAR2(254) NULL,
     class_code_id5             NUMBER(22, 0) NULL,
     class_code_value5          VARCHAR2(254) NULL,
     class_code_xlate6          VARCHAR2(254) NULL,
     class_code_id6             NUMBER(22, 0) NULL,
     class_code_value6          VARCHAR2(254) NULL,
     class_code_xlate7          VARCHAR2(254) NULL,
     class_code_id7             NUMBER(22, 0) NULL,
     class_code_value7          VARCHAR2(254) NULL,
     class_code_xlate8          VARCHAR2(254) NULL,
     class_code_id8             NUMBER(22, 0) NULL,
     class_code_value8          VARCHAR2(254) NULL,
     class_code_xlate9          VARCHAR2(254) NULL,
     class_code_id9             NUMBER(22, 0) NULL,
     class_code_value9          VARCHAR2(254) NULL,
     class_code_xlate10         VARCHAR2(254) NULL,
     class_code_id10            NUMBER(22, 0) NULL,
     class_code_value10         VARCHAR2(254) NULL,
     class_code_xlate11         VARCHAR2(254) NULL,
     class_code_id11            NUMBER(22, 0) NULL,
     class_code_value11         VARCHAR2(254) NULL,
     class_code_xlate12         VARCHAR2(254) NULL,
     class_code_id12            NUMBER(22, 0) NULL,
     class_code_value12         VARCHAR2(254) NULL,
     class_code_xlate13         VARCHAR2(254) NULL,
     class_code_id13            NUMBER(22, 0) NULL,
     class_code_value13         VARCHAR2(254) NULL,
     class_code_xlate14         VARCHAR2(254) NULL,
     class_code_id14            NUMBER(22, 0) NULL,
     class_code_value14         VARCHAR2(254) NULL,
     class_code_xlate15         VARCHAR2(254) NULL,
     class_code_id15            NUMBER(22, 0) NULL,
     class_code_value15         VARCHAR2(254) NULL,
     class_code_xlate16         VARCHAR2(254) NULL,
     class_code_id16            NUMBER(22, 0) NULL,
     class_code_value16         VARCHAR2(254) NULL,
     class_code_xlate17         VARCHAR2(254) NULL,
     class_code_id17            NUMBER(22, 0) NULL,
     class_code_value17         VARCHAR2(254) NULL,
     class_code_xlate18         VARCHAR2(254) NULL,
     class_code_id18            NUMBER(22, 0) NULL,
     class_code_value18         VARCHAR2(254) NULL,
     class_code_xlate19         VARCHAR2(254) NULL,
     class_code_id19            NUMBER(22, 0) NULL,
     class_code_value19         VARCHAR2(254) NULL,
     class_code_xlate20         VARCHAR2(254) NULL,
     class_code_id20            NUMBER(22, 0) NULL,
     class_code_value20         VARCHAR2(254) NULL,
     loaded                     NUMBER(22, 0) NULL,
     is_modified                NUMBER(22, 0) NULL,
     error_message              VARCHAR2(4000) NULL,
     unique_ilr_identifier      VARCHAR2(35) NULL,
     c_bucket_1                 NUMBER(22, 2) NULL,
     c_bucket_2                 NUMBER(22, 2) NULL,
     c_bucket_3                 NUMBER(22, 2) NULL,
     c_bucket_4                 NUMBER(22, 2) NULL,
     c_bucket_5                 NUMBER(22, 2) NULL,
     c_bucket_6                 NUMBER(22, 2) NULL,
     c_bucket_7                 NUMBER(22, 2) NULL,
     c_bucket_8                 NUMBER(22, 2) NULL,
     c_bucket_9                 NUMBER(22, 2) NULL,
     c_bucket_10                NUMBER(22, 2) NULL,
     e_bucket_1                 NUMBER(22, 2) NULL,
     e_bucket_2                 NUMBER(22, 2) NULL,
     e_bucket_3                 NUMBER(22, 2) NULL,
     e_bucket_4                 NUMBER(22, 2) NULL,
     e_bucket_5                 NUMBER(22, 2) NULL,
     e_bucket_6                 NUMBER(22, 2) NULL,
     e_bucket_7                 NUMBER(22, 2) NULL,
     e_bucket_8                 NUMBER(22, 2) NULL,
     e_bucket_9                 NUMBER(22, 2) NULL,
     e_bucket_10                NUMBER(22, 2) NULL,
     sublease_flag              NUMBER(1, 0) NULL,
     sublease_flag_xlate        VARCHAR2(35) NULL,
     sublease_id                NUMBER(22, 0) NULL,
     sublease_id_xlate          VARCHAR2(254) NULL,
     likely_to_collect          NUMBER(1, 0) NULL,
     likely_to_collect_xlate    VARCHAR2(35) NULL,
     intent_to_purch            NUMBER(1, 0) NULL,
     intent_to_purch_xlate      VARCHAR2(35),
     specialized_asset          NUMBER(1, 0) NULL,
     specialized_asset_xlate    VARCHAR2(35) NULL,
     intercompany_lease         NUMBER(1, 0) NULL,
     intercompany_lease_xlate   VARCHAR2(35) NULL,
     intercompany_company       NUMBER(22, 0) NULL,
     intercompany_company_xlate VARCHAR2(254) NULL,
     init_direct_cost_amt       NUMBER(22, 2) NULL,
     sales_type_disc_rate       VARCHAR(35) NULL,
     direct_fin_disc_rate       VARCHAR(35) NULL,
     direct_fin_fmv_rate        VARCHAR(35) NULL,
     direct_fin_int_rate        VARCHAR(35) NULL
  );

ALTER TABLE LSR_IMPORT_ILR_ARCHIVE
  ADD CONSTRAINT lsr_import_ilr_archive_pk PRIMARY KEY ( import_run_id, line_id );

ALTER TABLE LSR_IMPORT_ILR_ARCHIVE
  ADD CONSTRAINT lsr_import_ilr_archive_run_fk FOREIGN KEY ( import_run_id ) REFERENCES PP_IMPORT_RUN ( import_run_id );

COMMENT ON TABLE LSR_IMPORT_ILR_ARCHIVE IS '(S)  [06]
The LS Import ILR table is an API table used to archive imported Lessor ILRs.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.ilr_id IS 'The internal ILR id within PowerPlant .';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.ilr_number IS 'A field to hold the ilr number.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.lease_xlate IS 'Translation field for determining the MLA.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.lease_id IS 'The internal lease id within PowerPlant .';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.company_xlate IS 'Translation field for determining the company.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.company_id IS 'The company.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.est_in_svc_date IS 'The date the ILR comes in service.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.ilr_group_xlate IS 'Translation field for determining the ILR group.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.ilr_group_id IS 'An internal PowerPlant id representing the ILR Group for the ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.workflow_type_xlate IS 'Translation field for determining the workflow type.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.workflow_type_id IS 'An internal id used for routing records to approval.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.external_ilr IS 'A field to hold the relationship between PowerPlant and an external system.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.notes IS 'A freeform field for entering notes.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.inception_air IS 'The interest rate used to determine interest payments.  Also used to determine net present value.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.purchase_option_type_xlate IS 'Translation field for determining the purchase option type.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.purchase_option_type_id IS 'Represents if this ILR has a purchase option.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.purchase_option_amt IS 'The amount of the purchase option.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.renewal_option_type_xlate IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.renewal_option_type_id IS 'Represents if this ILR has a renewal option.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.cancelable_type_xlate IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.cancelable_type_id IS 'A PowerPlant internal id representing whether or not a lease can be cancelled before the lease terms are up.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.itc_sw IS 'A flag identifying whether this ILR has investment tax credits associate with it.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.cap_type_xlate IS 'Translation field for determining the lease cap type.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.termination_amt IS 'The amount for terminating the lease.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.payment_term_id IS 'System-assigned ID for this ILR payment term.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.payment_term_date IS 'The payment term date for this ILR payment term.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.payment_freq_xlate IS 'Translation field for determining the payment frequency.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.payment_freq_id IS 'The payment frequency on this ILR payment term. (1: Annual, 2: Semi-Annual, 3: Quarterly, 4: Monthly, 5: Actual).';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.number_of_terms IS 'The number of payments on this ILR payment term record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.paid_amount IS 'The amount to be paid each term for this ILR payment term.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.est_executory_cost IS 'The estimated executory cost.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.contingent_amount IS 'The contingent amount to be paid.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate1 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id1 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value1 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate2 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id2 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value2 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate3 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id3 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value3 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate4 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id4 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value4 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate5 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id5 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value5 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate6 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id6 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value6 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate7 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id7 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value7 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate8 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id8 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value8 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate9 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id9 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value9 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate10 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id10 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value10 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate11 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id11 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value11 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate12 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id12 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value12 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate13 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id13 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value13 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate14 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id14 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value14 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate15 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id15 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value15 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate16 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id16 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value16 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate17 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id17 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value17 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate18 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id18 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value18 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate19 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id19 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value19 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_xlate20 IS 'Translation field for determining the class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_id20 IS 'The class code.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.class_code_value20 IS 'The class code value for this record.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.loaded IS 'System-assigned number to specify if this row has been loaded into its table.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.error_message IS 'Error messages resulting from data valdiation in the import process.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.unique_ilr_identifier IS 'Unique identifier for this ILR, for this import run.  Used to link rows in this table that are under the same ILR.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_1 IS 'A column holding the contingent amount for bucket 1';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_2 IS 'A column holding the contingent amount for bucket 2';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_3 IS 'A column holding the contingent amount for bucket 3';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_4 IS 'A column holding the contingent amount for bucket 4';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_5 IS 'A column holding the contingent amount for bucket 5';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_6 IS 'A column holding the contingent amount for bucket 6';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_7 IS 'A column holding the contingent amount for bucket 7';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_8 IS 'A column holding the contingent amount for bucket 8';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_9 IS 'A column holding the contingent amount for bucket 9';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.c_bucket_10 IS 'A column holding the contingent amount for bucket 10';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_1 IS 'A column holding the executory amount for bucket 1';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_2 IS 'A column holding the executory amount for bucket 2';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_3 IS 'A column holding the executory amount for bucket 3';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_4 IS 'A column holding the executory amount for bucket 4';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_5 IS 'A column holding the executory amount for bucket 5';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_6 IS 'A column holding the executory amount for bucket 6';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_7 IS 'A column holding the executory amount for bucket 7';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_8 IS 'A column holding the executory amount for bucket 8';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_9 IS 'A column holding the executory amount for bucket 9';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.e_bucket_10 IS 'A column holding the executory amount for bucket 10';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.sublease_flag IS 'ID for Yes/No on whether ILR has sublease. Is the lessee subleasing the asset?';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.sublease_flag_xlate IS 'Translate Column for ID for Yes/No on whether ILR has sublease';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.sublease_id IS 'ID of Sublease for ILR (ls_lease)';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.sublease_id_xlate IS 'ID of Sublease for ILR (ls_lease)';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.likely_to_collect IS 'ID for Yes/No on whether ILR is Likely to Collect. Is the business reasonably likely to collect on the lease payments?';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.likely_to_collect_xlate IS 'Translate Column ID for Yes/No on whether ILR is Likely to Collect';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.intent_to_purch IS 'ID for Yes/No for Intent to Purchase. Is the business planning on the lessee purchasing the asset.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.intent_to_purch_xlate IS 'Translate Column ID for Yes/No for Intent to Purchase';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.specialized_asset IS 'ID for Yes/No for Specialized Asset. Is the asset so specialized in nature, there would be no other use for it after the term of the lease?';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.specialized_asset_xlate IS 'Translate Column ID for Yes/No for Specialized Asset';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.intercompany_lease IS 'ID for Yes/No for whether ILR is an Intercompany Lease. Is the lease with another company in the business?';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.intercompany_lease_xlate IS 'Translate Column ID for Yes/No for whether ILR is an Intercompany Lease';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.intercompany_company IS 'ID of Company ILR is associated with for Intercompany Lease. If the lease is with another company within the business, enter the company description here.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.intercompany_company_xlate IS 'Translate Column for ID of Company ILR is associated with for Intercompany Lease';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.init_direct_cost_amt IS 'If there are initial direct costs, what is the total amount?';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.sales_type_disc_rate IS 'Corresponds to the rate used throughout the Sales-Type lease calculations.';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.direct_fin_disc_rate IS 'Corresponds to the rate used to calculate the Net Investment and all schedule calculations except for interest on Net Investment';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.direct_fin_fmv_rate IS 'Corresponds to the rate used in the FMV comparison test to determine if lease is operating or not';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.direct_fin_int_rate IS 'Corresponds to the rate used to calculate interest on the Net Investment';

COMMENT ON COLUMN LSR_IMPORT_ILR_ARCHIVE.payment_term_id_xlate IS 'Translation for System-assigned ID for this ILR payment term.'; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3798, 0, 2017, 1, 0, 0, 48888, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048888_lessor_01_ilr_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;