/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032232_lease_imports.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/08/2013 Ryan Oliveria  Point Release
||============================================================================
*/

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID =
        (select IMPORT_LOOKUP_ID
           from PP_IMPORT_LOOKUP
          where LOOKUP_TABLE_NAME = 'asset_location'
            and LOOKUP_COLUMN_NAME = 'ext_asset_location'
            and LOOKUP_CONSTRAINING_COLUMNS is null)
 where COLUMN_NAME = 'asset_location_id'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (595, 0, 10, 4, 1, 0, 32232, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032232_lease_imports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
