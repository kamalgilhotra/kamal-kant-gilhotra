/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035725_aro_regulated.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/16/2014 Ryan Oliveria
||============================================================================
*/

alter table ARO rename column REG_ARO_ACCT to COR_CREDIT_ACCT_ID;

update ARO set REGULATORY_OBLIGATION = 0 where REGULATORY_OBLIGATION is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (880, 0, 10, 4, 2, 0, 35725, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035725_aro_regulated.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;