/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037830_aro_accretion_stop.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 04/28/2014 Ryan Oliveria    Add accretion stop date to ARO
||============================================================================
*/

alter table ARO add ACCRETION_STOP_DATE date;

comment on column ARO.ACCRETION_STOP_DATE is 'The date that accretion will no longer be calculated for this ARO. If the stop date is May, accretion will be calculated for April, but not for May.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1120, 0, 10, 4, 3, 0, 37830, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037830_aro_accretion_stop.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;