/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038917_reg_clean_alloc_categories.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 10.4.2.6 07/13/2014 Sarah Byers
||========================================================================================
*/

delete from REG_ALLOC_CATEGORY where DESCRIPTION = 'Enter a New Category...';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1207, 0, 10, 4, 2, 6, 38917, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038917_reg_clean_alloc_categories.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;