/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_005806_02_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   12/06/2011 Blake Andrews  Point Release
||============================================================================
*/

update PP_REPORTS
   set DESCRIPTION = 'Current Provision-All',
       LONG_DESCRIPTION = 'Current Provision Report that starts with pre-tax income (not Federal Taxable Income) for all entities'
 where REPORT_ID = 51013;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (65, 0, 10, 3, 3, 0, 5806, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_005806_02_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
