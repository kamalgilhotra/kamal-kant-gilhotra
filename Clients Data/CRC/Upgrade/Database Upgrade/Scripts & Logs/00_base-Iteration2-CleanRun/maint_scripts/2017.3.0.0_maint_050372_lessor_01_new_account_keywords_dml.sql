/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050372_lessor_01_new_account_keywords_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/12/2018 Jared Watkins  Add journal layouts keywords for the new ILR accounts
||============================================================================
*/

INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
SELECT 'p_lsr_ilc' AS keyword,
       'Lessor Incurred Lease Costs' AS description,
       'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.incurred_costs_account_id' AS sqls,
       1 AS keyword_type,
       'all' AS valid_elements,
       13 AS bind_arg_code_id1,
       SYSDATE AS time_stamp,
       USER AS user_id
FROM dual
WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_ilc');

INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
SELECT 'p_lsr_dlc' AS keyword,
       'Lessor Deferred Lease Costs' AS description,
       'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.def_costs_account_id' AS sqls,
       1 AS keyword_type,
       'all' AS valid_elements,
       13 AS bind_arg_code_id1,
       SYSDATE AS time_stamp,
       USER AS user_id
FROM dual
WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_dlc');

INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
SELECT 'p_lsr_dsp' AS keyword,
       'Lessor Deferred Selling Profit' AS description,
       'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.def_selling_profit_account_id' AS sqls,
       1 AS keyword_type,
       'all' AS valid_elements,
       13 AS bind_arg_code_id1,
       SYSDATE AS time_stamp,
       USER AS user_id
FROM dual
WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_dsp');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4305, 0, 2017, 3, 0, 0, 50372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050372_lessor_01_new_account_keywords_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;