 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051712_lessee_01_lease_floating_rates_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.4.0.0 06/26/2018 Shane "C" Ward  New Floating Rates definition table
 ||============================================================================
 */

CREATE TABLE ls_lease_floating_rates
(lease_id NUMBER(22,0) ,
effective_date DATE,
rate NUMBER(22,8),
user_id VARCHAR2(18),
time_stamp date);

COMMENT ON TABLE ls_lease_floating_rates IS '(S)  [01] A table that holds floating rates and effective dates for an MLA.';
COMMENT ON COLUMN ls_lease_floating_rates.lease_id IS 'The unique lease identifier associated with a MLA.';
COMMENT ON COLUMN ls_lease_floating_rates.effective_date IS 'The effective date for the Floating Rate to be used.';
COMMENT ON COLUMN ls_lease_floating_rates.rate IS 'Rate to be leveraged.';
COMMENT ON COLUMN ls_lease_floating_rates.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_lease_floating_rates.user_id IS 'Standard system-assigned user id used for audit purposes.';


ALTER TABLE ls_lease_floating_rates ADD CONSTRAINT pk_ls_lease_floating_rates PRIMARY KEY (lease_id, effective_date);

ALTER TABLE ls_lease_floating_rates ADD CONSTRAINT fk_ls_lease_floating_rates1 FOREIGN KEY  (lease_id) REFERENCES ls_lease (lease_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7163, 0, 2017, 4, 0, 0, 51712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051712_lessee_01_lease_floating_rates_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;