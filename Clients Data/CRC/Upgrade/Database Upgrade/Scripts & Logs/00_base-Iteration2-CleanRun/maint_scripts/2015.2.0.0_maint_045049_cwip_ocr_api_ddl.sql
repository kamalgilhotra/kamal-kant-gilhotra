/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045049_cwip_ocr_api_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- --------------------  ---------------------------------
|| 2015.2   10/15/2015 Khamchanh Sisouphanh  Add column ocr_api to wo_estimate
||============================================================================
*/ 

-- OCR API
ALTER TABLE WO_ESTIMATE ADD (OCR_API NUMBER(22,0));
COMMENT ON COLUMN WO_ESTIMATE.OCR_API is 'System-assigned identifier for ocr api.  Possible values include: 0-Indicates that the tri-state checkbox is unchecked. 1-Indicates that the tri-state checkbox is checked. 2-Indicates that the tri-state checkbox is filled.'; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2926, 0, 2015, 2, 0, 0, 45049, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045049_cwip_ocr_api_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;