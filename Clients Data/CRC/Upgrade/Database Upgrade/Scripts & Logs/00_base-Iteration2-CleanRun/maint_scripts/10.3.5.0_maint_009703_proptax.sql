/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_009703_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    05/07/2012 Julia Breuer   Point Release
||============================================================================
*/

-- Correct misspelling on import field

update PT_IMPORT_COLUMN
   set DESCRIPTION = 'Beg Bal Adjustment'
 where COLUMN_NAME = 'beg_bal_adjustment';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (134, 0, 10, 3, 5, 0, 9703, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009703_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
