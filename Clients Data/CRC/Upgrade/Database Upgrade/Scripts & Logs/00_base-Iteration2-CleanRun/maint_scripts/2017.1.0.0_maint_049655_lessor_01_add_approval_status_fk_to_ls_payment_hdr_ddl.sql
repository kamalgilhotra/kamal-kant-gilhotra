/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049655_lessor_01_add_approval_status_fk_to_ls_payment_hdr_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/14/2017 Andrew Hill    Add foreign key to lsr_payment_hdr referecing approval status
||============================================================================
*/

ALTER TABLE ls_payment_hdr ADD CONSTRAINT ls_payment_hdr_app_stat_fk FOREIGN KEY (payment_status_id) REFERENCES approval_status(approval_status_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3959, 0, 2017, 1, 0, 0, 49655, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049655_lessor_01_add_approval_status_fk_to_ls_payment_hdr_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;