/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044200_budgetcap_respread_prj_ddl.sql.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- -------------------   ----------------------------------------------
|| 2015.1   07/17/2015 Khamchanh Sisouphanh  Add column to budget_process_control
||========================================================================================
*/

declare
begin
  execute immediate 'alter table budget_process_control add respread_projects date null';
  execute immediate 'comment on column budget_process_control.respread_projects is ''Records the last time Re-Spread Projects was run for this budget version.''';
exception 
  when others then 
    DBMS_OUTPUT.PUT_LINE('Column respread projects already exists on table budget_process_control.');
end;
/



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2696, 0, 2015, 2, 0, 0, 044200, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044200_budgetcap_respread_prj_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;