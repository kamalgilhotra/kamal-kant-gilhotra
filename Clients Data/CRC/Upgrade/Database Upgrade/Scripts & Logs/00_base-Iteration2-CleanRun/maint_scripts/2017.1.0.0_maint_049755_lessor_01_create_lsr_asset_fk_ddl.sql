/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049755_lessor_01_create_lsr_asset_fk_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 Josh Sandler     Create foreign key
||============================================================================
*/

ALTER TABLE lsr_asset
  ADD CONSTRAINT r_lsr_asset2 FOREIGN KEY (
    ilr_id,
		revision
  ) REFERENCES lsr_ilr_approval (
    ilr_id,
		revision
  )
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3837, 0, 2017, 1, 0, 0, 49755, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049755_lessor_01_create_lsr_asset_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;