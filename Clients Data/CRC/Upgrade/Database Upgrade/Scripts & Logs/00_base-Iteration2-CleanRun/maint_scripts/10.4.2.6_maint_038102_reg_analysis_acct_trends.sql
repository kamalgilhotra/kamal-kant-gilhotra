/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038102_reg_analysis_acct_trends.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/20/2014 Ryan Oliveria  Reg Audit Trail Script 2
||========================================================================================
*/

insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, DESCRIPTION, SORT_ORDER)
values
   (407, 'Regulatory Analysis', 7);

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER,
    OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'Expense Cost Trends',
          'Expense Cost Trends',
          null SUBSYSTEM,
          'dw_reg_report_expense_cost',
          'already has report time' SPECIAL_NOTE,
          null REPORT_TYPE,
          null TIME_OPTION,
          'Expense Cost Trends' REPORT_NUMBER,
          null INPUT_WINDOW,
          null FILTER_OPTION,
          null STATUS,
          101 PP_REPORT_SUBSYSTEM_ID,
          407 REPORT_TYPE_ID,
          null PP_REPORT_TIME_OPTION_ID,
          null PP_REPORT_FILTER_ID,
          1 PP_REPORT_STATUS_ID,
          7 PP_REPORT_ENVIR_ID, /*All Hide environment, so it will only appear in Reg Analysis Wksp*/
          null DOCUMENTATION,
          null USER_COMMENT,
          null LAST_APPROVED_DATE,
          null PP_REPORT_NUMBER,
          null OLD_REPORT_NUMBER,
          null DYNAMIC_DW
     from PP_REPORTS;

insert into REG_ANALYSIS_REPORTS
   (RA_PRESENT_ID, REPORT_ID, SHOW_DATA_FLAG, DEFAULT_FLAG)
   select 7, max(REPORT_ID), 0, 1 from PP_REPORTS where DATAWINDOW = 'dw_reg_report_expense_cost';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1147, 0, 10, 4, 2, 6, 38102, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038102_reg_analysis_acct_trends.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;