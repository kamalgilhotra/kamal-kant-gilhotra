/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044025_add_clc_to_schedules_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/15/2015 Will Davis     Add current lease cost to asset and ILR schedules
||============================================================================
*/

update
(select a.ls_asset_id, a.revision, a.month, a.current_lease_cost, lg.require_components
 from ls_asset_schedule a, ls_asset la, ls_ilr ilr, ls_lease ll, ls_lease_group lg
 where a.ls_asset_id = la.ls_asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ll.lease_group_id = lg.lease_group_id) las
set current_lease_cost =
case when las.require_components = 1 then
(select sum(cc.amount)
 from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll
 where las.ls_asset_id = la.ls_asset_id
  and cc.component_id = lc.component_id
  and lc.ls_asset_id = la.ls_asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and cc.interim_interest_start_date is not null
  and las.month >= add_months(trunc(cc.interim_interest_start_date, 'month'), case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day then 1 else 0 end))
else
  (select fmv
   from ls_asset la
   where la.ls_asset_id = las.ls_asset_id)
end
where ls_asset_id in (select ls_asset_id from ls_asset_schedule where current_lease_cost is null);

update
(select i.ilr_id, i.revision, i.month, i.current_lease_cost, lg.require_components
 from ls_ilr_schedule i, ls_ilr ilr, ls_lease ll, ls_lease_group lg
 where i.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ll.lease_group_id = lg.lease_group_id) lis
set current_lease_cost =
case when lis.require_components = 1 then
(select sum(cc.amount)
 from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll
 where lis.ilr_id = ilr.ilr_id
  and cc.component_id = lc.component_id
  and lc.ls_asset_id = la.ls_asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and cc.interim_interest_start_date is not null
  and lis.month >= add_months(trunc(cc.interim_interest_start_date, 'month'), case when extract(day from cc.interim_interest_start_date) >= ll.cut_off_day then 1 else 0 end))
else
  (select fmv
   from LS_ILR_STG STG
   WHERE STG.ILR_ID = LIS.ILR_ID
    AND STG.REVISION = LIS.REVISION)
end
where (ilr_id) in (select ilr_id from ls_ilr_schedule where current_lease_cost is null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2607, 0, 2015, 2, 0, 0, 044025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044025_add_clc_to_schedules_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;