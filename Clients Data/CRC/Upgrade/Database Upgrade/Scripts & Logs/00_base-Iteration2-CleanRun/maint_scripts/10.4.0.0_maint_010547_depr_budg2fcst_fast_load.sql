/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010547_depr_budg2fcst_fast_load.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   06/16/2012 David Liss     Maint 10547
||============================================================================
*/

create global temporary table FCST_CPR_DEPR_TEMP
(
 ASSET_ID                  number(22, 0) not null,
 SET_OF_BOOKS_ID           number(22, 0) not null,
 GL_POSTING_MO_YR          date not null,
 FCST_DEPR_VERSION_ID      number(22, 0) not null,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 INIT_LIFE                 number(22, 2) default 0,
 REMAINING_LIFE            number(22, 2) default 0,
 ESTIMATED_SALVAGE         number(22, 8) default 0,
 BEG_ASSET_DOLLARS         number(22, 2) default 0,
 NET_ADDS_AND_ADJUST       number(22, 2) default 0,
 RETIREMENTS               number(22, 2) default 0,
 TRANSFERS_IN              number(22, 2) default 0,
 TRANSFERS_OUT             number(22, 2) default 0,
 ASSET_DOLLARS             number(22, 2) default 0,
 BEG_RESERVE_MONTH         number(22, 2) default 0,
 SALVAGE_DOLLARS           number(22, 2) default 0,
 RESERVE_ADJUSTMENT        number(22, 2) default 0,
 COST_OF_REMOVAL           number(22, 2) default 0,
 RESERVE_TRANS_IN          number(22, 2) default 0,
 RESERVE_TRANS_OUT         number(22, 2) default 0,
 DEPR_EXP_ADJUST           number(22, 2) default 0,
 OTHER_CREDITS_AND_ADJUST  number(22, 2) default 0,
 GAIN_LOSS                 number(22, 2) default 0,
 DEPRECIATION_BASE         number(22, 2) default 0,
 CURR_DEPR_EXPENSE         number(22, 2) default 0,
 DEPR_RESERVE              number(22, 2) default 0,
 BEG_RESERVE_YEAR          number(22, 2) default 0,
 YTD_DEPR_EXPENSE          number(22, 2) default 0,
 YTD_DEPR_EXP_ADJUST       number(22, 2) default 0,
 PRIOR_YTD_DEPR_EXPENSE    number(22, 2) default 0,
 PRIOR_YTD_DEPR_EXP_ADJUST number(22, 2) default 0,
 ACCT_DISTRIB              varchar2(254),
 MONTH_RATE                number(22, 8) default 0,
 COMPANY_ID                number(22, 0),
 MID_PERIOD_METHOD         varchar2(35),
 MID_PERIOD_CONV           number(22, 8),
 FCST_DEPR_GROUP_ID        number(22, 0),
 DEPR_EXP_ALLOC_ADJUST     number(22, 2) default 0,
 FCST_DEPR_METHOD_ID       number(22, 0),
 LONG_DESCRIPTION          varchar2(254)
) on commit preserve rows;

alter table FCST_CPR_DEPR_TEMP
   add constraint PK_FCST_CPR_DEPR_TEMP
       primary key (FCST_DEPR_VERSION_ID, ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR);

create global temporary table FCST_BUDG_CPR_DEPR_CLOSING_TMP
(
 ID                    number(22, 0) not null,
 LOAD_ID               number(22, 0) not null,
 ASSET_ID              number(22, 0),
 SET_OF_BOOKS_ID       number(22, 0) not null,
 FCST_DEPR_GROUP_ID    number(22, 0),
 GL_POSTING_MO_YR      date not null,
 FCST_DEPR_VERSION_ID  number(22, 0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 INIT_LIFE             number(22, 2) default 0,
 NET_ADDS_AND_ADJUST   number(22, 2) default 0,
 RETIREMENTS           number(22, 2) default 0,
 COST_OF_REMOVAL       number(22, 2) default 0,
 SALVAGE_DOLLARS       number(22, 2) default 0,
 TAX_BASIS_ADJUSTMENTS number(22, 2) default 0,
 LONG_DESCRIPTION      varchar2(254)
) on commit preserve rows;

alter table FCST_BUDG_CPR_DEPR_CLOSING_TMP
   add constraint PK_FCST_BUDG_CD_CLOSING_TMP
       primary key (ID);

--create index FCST_BUDG_CD_IDX
--   on FCST_BUDG_CPR_DEPR_CLOSING_TMP
--     (ASSET_ID,
--      SET_OF_BOOKS_ID,
--      GL_POSTING_MO_YR,
--      FCST_DEPR_VERSION_ID,
--      LONG_DECRIPTION);

create global temporary table FCST_BUDGET_DEPR_CLOSINGS_TEMP
(
 SET_OF_BOOKS_ID       number(22, 0) not null,
 FCST_DEPR_GROUP_ID    number(22, 0) not null,
 GL_POST_MO_YR         date not null,
 FCST_DEPR_VERSION_ID  number(22, 0) not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 ADDITIONS             number(22, 2) default 0,
 RETIREMENTS           number(22, 2) default 0,
 COST_OF_REMOVAL       number(22, 2) default 0,
 SALVAGE_CASH          number(22, 2) default 0,
 TAX_BASIS_ADJUSTMENTS number(22, 2) default 0
) on commit preserve rows;

alter table FCST_BUDGET_DEPR_CLOSINGS_TEMP
   add constraint PK_FCST_BUDG_DEPR_CLOSINGS
       primary key (SET_OF_BOOKS_ID, FCST_DEPR_GROUP_ID, GL_POST_MO_YR, FCST_DEPR_VERSION_ID);

create or replace view SET_OF_BOOKS_ALL_BASIS_VIEW as
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_1_INDICATOR BASIS_STATUS
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 1
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_2_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 2
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_3_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 3
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_4_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 4
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_5_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where BS.BOOK_SUMMARY_ID = 5
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_6_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 6
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_7_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 7
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_8_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 8
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_9_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 9
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_10_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 10
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_11_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 11
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_12_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 12
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_13_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 13
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_14_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 14
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_15_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 15
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_16_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 16
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_17_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 17
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_18_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 18
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_19_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 19
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_20_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 20
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_21_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 21
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_22_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 22
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_23_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 23
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_24_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 24
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_25_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 25
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_26_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 26
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_27_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 27
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_28_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 28
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_29_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 29
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_30_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 30
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_31_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 31
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_32_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 32
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_33_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 33
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_34_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 34
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_35_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 35
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_36_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 36
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_37_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 37
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_38_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 38
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_39_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 39
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_40_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 40
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_41_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 41
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_42_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 42
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_43_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 43
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_44_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 44
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_45_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 45
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_46_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 46
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_47_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 47
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_48_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 48
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_49_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 49
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_50_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 50
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_51_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 51
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_52_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 52
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_53_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 53
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_54_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 54
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_55_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 55
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_56_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 56
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_57_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 57
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_58_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 58
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_59_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 59
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_60_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 60
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_61_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 61
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_62_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 62
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_63_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 63
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_64_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 64
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_65_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 65
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_66_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 66
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_67_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 67
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_68_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 68
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_69_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 69
union
select SOB.SET_OF_BOOKS_ID,
       SOB.DESCRIPTION,
       BS.BOOK_SUMMARY_ID,
       SOB.BASIS_70_INDICATOR
  from BOOK_SUMMARY BS,
       SET_OF_BOOKS SOB
 where bs.book_summary_id = 70;


create index FCST_BUDG_CPR_DPR_CLOS_TMP_IDX
   on FCST_BUDG_CPR_DEPR_CLOSING_TMP (LONG_DESCRIPTION, FCST_DEPR_GROUP_ID, INIT_LIFE,
                                      GL_POSTING_MO_YR, SET_OF_BOOKS_ID, NVL(ASSET_ID, -99));

create index FCST_BUDG_CPR_DPR_CLOS_TMP_IX2
   on FCST_BUDG_CPR_DEPR_CLOSING_TMP (NVL(ASSET_ID, -99));

create index FCST_CPR_DEPR_TEMP_IDX
   on FCST_CPR_DEPR_TEMP (LONG_DESCRIPTION, FCST_DEPR_GROUP_ID, INIT_LIFE, REMAINING_LIFE,
                          GL_POSTING_MO_YR, SET_OF_BOOKS_ID, ASSET_ID);

create global temporary table FCST_BUDG_CPR_DEPR_ASSET_TEMP
(
 ID       number(22,0),
 ASSET_ID number(22,0)
) on commit preserve rows;

alter table FCST_BUDG_CPR_DEPR_ASSET_TEMP
   add constraint PK_FCST_BUDG_CD_ASSET_TEMP
       primary key (ID);

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, TIME_STAMP, USER_ID, LONG_DESCRIPTION, EXECUTABLE_FILE,
    RUNNING_SESSION_ID, VERSION)
values
   ((select max(PROCESS_ID) + 1 from PP_PROCESSES), 'Budget to Forecast',
    TO_DATE('14-05-2012 10:03:24', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Budget to Forecast', '',
    null, '');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (236, 0, 10, 4, 0, 0, 10547, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010547_depr_budg2fcst_fast_load.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
