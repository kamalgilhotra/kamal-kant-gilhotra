/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049499_taxrpr_01_mixed_rl_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2017.1.0.0 10/19/2017 Eric Berger    DDL for mixed repair location method.
|| 2018.2.0.0 07/11/2018 Matt Kelley    DDL for repair_work_order_debug
||============================================================================
*/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_WORK_ORDER_TEMP ADD ( repair_location_source NUMBER(22, 0) NULL )';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column repair_location_source already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_WORK_ORDER_SEGMENTS ADD ( repair_location_source NUMBER(22, 0) NULL )';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column repair_location_source already exists.');
END;
/

BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE REPAIR_WORK_ORDER_DEBUG ADD ( repair_location_source NUMBER(22, 0) NULL )';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column repair_location_source already exists.');
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13733, 0, 2018, 2, 0, 0, 49499, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049499_taxrpr_01_mixed_rl_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;