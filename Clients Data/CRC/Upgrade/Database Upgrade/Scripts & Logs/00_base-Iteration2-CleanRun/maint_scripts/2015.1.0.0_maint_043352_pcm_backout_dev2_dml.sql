/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043352_pcm_backout_dev2_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2015.1.0.0 04/01/2015 Alex P.        Revert predelivered search grid datawindows
||============================================================================
*/

--* Revert predelivered search grid datawindows
update SELECT_TABS_GRIDS set DW_NAME = 'dw_fp_grid' where DW_NAME = 'dw_pcm_fp_search_grid';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_fp_grid_second_sob' where DW_NAME = 'dw_pcm_fp_search_grid_sob';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_fp_grid_alt' where DW_NAME = 'dw_pcm_fp_search_grid_simple';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_work_order_grid' where DW_NAME = 'dw_pcm_wo_search_grid';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_work_order_grid_second_sob' where DW_NAME = 'dw_pcm_wo_search_grid_sob';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_work_order_grid_alt' where DW_NAME = 'dw_pcm_wo_search_grid_simple';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2471, 0, 2015, 1, 0, 0, 043352, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043352_pcm_backout_dev2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;