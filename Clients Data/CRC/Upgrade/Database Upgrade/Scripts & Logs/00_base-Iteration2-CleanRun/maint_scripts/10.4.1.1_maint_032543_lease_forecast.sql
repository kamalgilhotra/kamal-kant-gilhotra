/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032543_lease_forecast.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Ryan Oliveria  Patch Release
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'forecast_tool', 'Forecast', 'uo_ls_forecastcntr_wksp');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'forecast_tool', 1, 9, 'Forecast', null, 'forecast_tool', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (648, 0, 10, 4, 1, 1, 32543, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032543_lease_forecast.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
