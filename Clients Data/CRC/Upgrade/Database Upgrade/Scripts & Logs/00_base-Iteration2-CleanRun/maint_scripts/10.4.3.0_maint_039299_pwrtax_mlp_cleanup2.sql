/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039299_pwrtax_mlp_cleanup2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/08/2014 Alex P.
||============================================================================
*/

update PP_DYNAMIC_FILTER
   set LABEL = 'Vintage (Copy From)'
 where FILTER_ID = 162
   and LABEL = 'Vintage';

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (185, 'Vintage (Copy To)', 'dw', '', 'dw_tax_vintage_unused_by_ver_filter', 'vintage_id', 'description', 'N', 1, 1,
    1, 0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 66, FILTER_ID from PP_DYNAMIC_FILTER where FILTER_ID in (185);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1308, 0, 10, 4, 3, 0, 39299, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039299_pwrtax_mlp_cleanup2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;