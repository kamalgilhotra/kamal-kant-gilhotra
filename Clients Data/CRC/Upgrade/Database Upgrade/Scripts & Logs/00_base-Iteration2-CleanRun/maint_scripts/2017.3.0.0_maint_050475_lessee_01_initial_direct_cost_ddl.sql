/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_01_initial_direct_cost_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/25/2018 Shane Ward    		Lessee Initial Direct Cost Table DDL
||============================================================================
*/

--Create Lessee IDC Group Table
CREATE TABLE ls_initial_direct_cost_group (
  idc_group_id     NUMBER(22,0)  NOT NULL,
  description      VARCHAR2(35)  NOT NULL,
  long_description VARCHAR2(254) NULL,
  user_id          VARCHAR2(18)  NULL,
  time_stamp       DATE          NULL
)
;

ALTER TABLE ls_initial_direct_cost_group
  ADD CONSTRAINT ls_idc_group_pk PRIMARY KEY (
    idc_group_id
  );

ALTER TABLE ls_initial_direct_cost_group
  ADD CONSTRAINT ls_idc_group_uk UNIQUE (
    description
  );

COMMENT ON TABLE ls_initial_direct_cost_group IS '(S) []LESSEE The table stores the Indirect Cost Group Information.';

COMMENT ON COLUMN ls_initial_direct_cost_group.idc_group_id IS 'System-assigned identifier of an indirect cost group.';
COMMENT ON COLUMN ls_initial_direct_cost_group.description IS 'Description of the indirect cost group.';
COMMENT ON COLUMN ls_initial_direct_cost_group.long_description IS 'Long description of the indirect cost group..';
COMMENT ON COLUMN ls_initial_direct_cost_group.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_initial_direct_cost_group.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--Missing Table Comment on Lessor IDC Group table
COMMENT ON TABLE lsr_initial_direct_cost_group IS '(S) []LESSOR The table stores the Indirect Cost Group Information.';

--Create Lessee IDC amounts Table
create table ls_ilr_initial_direct_cost (
  ilr_idc_id     NUMBER(22,0),
  ilr_id       NUMBER(22,0) not null,
  revision          NUMBER(22,0) not null,
  idc_group_id      NUMBER(22,0) not null,
  date_incurred     DATE,
  amount            NUMBER(22,2) default 0.0,
  description       VARCHAR2(254),
  user_id           VARCHAR2(18),
  time_stamp        DATE);

alter table ls_ilr_initial_direct_cost
  add constraint ls_ilr_idc_pk primary key (ilr_idc_id)
  using index tablespace PWRPLANT_IDX;

alter table ls_ilr_initial_direct_cost
  add constraint ls_ilr_idc_fk1 foreign key (ilr_id, revision)
  references ls_ilr_approval(ilr_id, revision) ;

alter table ls_ilr_initial_direct_cost
  add constraint ls_ilr_idc foreign key (idc_group_id)
  references ls_initial_direct_cost_group(idc_group_id) ;

-- Comments
comment on table ls_ilr_initial_direct_cost is '(S) [] The table stores the Indirect Cost groups associated to an ILR.';
comment on column ls_ilr_initial_direct_cost.ilr_idc_id is 'System-assigned identifier of a Lessee ILR IDC.';
comment on column ls_ilr_initial_direct_cost.ilr_id is 'System-assigned identifier of a Lessee ILR.';
comment on column ls_ilr_initial_direct_cost.revision is 'System-assigned identifier of a Lessee ILR revision.';
comment on column ls_ilr_initial_direct_cost.idc_group_id is 'System-assigned identifier of an indirect cost group.';
comment on column ls_ilr_initial_direct_cost.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_ilr_initial_direct_cost.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_ilr_initial_direct_cost.date_incurred is 'The date on which the cost incurred.';
comment on column ls_ilr_initial_direct_cost.amount is 'Cost incurred';
comment on column ls_ilr_initial_direct_cost.description is 'Description of the ILR indirect cost group assoiation.';

--ID sequence
CREATE SEQUENCE ls_ilr_idc_seq
  MINVALUE 1
  START WITH 1
  INCREMENT BY 1
  CACHE 20 ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4234, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_01_initial_direct_cost_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;