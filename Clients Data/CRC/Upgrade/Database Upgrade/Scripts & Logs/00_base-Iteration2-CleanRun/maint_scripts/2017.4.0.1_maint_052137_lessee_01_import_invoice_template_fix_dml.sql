/*
||============================================================================
|| Application: PowerPlan 
|| File Name:   maint_052137_lessee_01_import_invoice_template_fix_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.1  08/08/2018 Shane "C" Ward   Remove duplicate columns from delivered Invoice Import Template
||============================================================================
*/

--Delete the extra columns where the import hasn't been run and where the setup is still the same as it was delivered originally
DELETE FROM PP_IMPORT_TEMPLATE_FIELDS
 WHERE IMPORT_TEMPLATE_ID IN (SELECT IMPORT_TEMPLATE_ID
                                FROM PP_IMPORT_TEMPLATE
                               WHERE IMPORT_TYPE_ID = 254
                                 AND DESCRIPTION = 'Lease Invoice Add')
   AND ((FIELD_ID = 12 AND lower(COLUMN_NAME) = 'notes') OR (FIELD_ID = 13 AND lower(COLUMN_NAME) = 'description'))
   AND IMPORT_TEMPLATE_ID NOT IN (SELECT IMPORT_TEMPLATE_ID FROM PP_IMPORT_RUN);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8882, 0, 2017, 4, 0, 1, 52137, 'C:\PlasticWks\powerplant\sql\maint_scripts', ' maint_052137_lessee_01_import_invoice_template_fix_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		 
   
   
   