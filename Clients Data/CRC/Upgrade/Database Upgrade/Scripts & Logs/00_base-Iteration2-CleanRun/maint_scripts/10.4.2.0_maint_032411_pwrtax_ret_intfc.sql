SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032411_pwrtax_ret_intfc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.2.0 10/14/2013 Andrew Scott        Speed enhancements to PowerTax retirements interface
|| 10.4.2.0 10/28/2013 Andrew Scott        added user_id to tax_interface_cos table
|| 10.4.2.0 10/30/2013 Andrew Scott        comment typo corrections.
|| 10.4.2.0 11/01/2013 Andrew Scott        made all tax years number(22,2).
||                                         added grant & synonymns
||============================================================================
*/

-----drop new tables, column (to be repeatable)
--alter table tax_book_translate drop constraint tbt_group_fk;
--alter table tax_book_transactions_grp drop constraint tax_book_trans_grp_tbtgrp_fk;
--alter table tax_book_transactions drop constraint tbtx_group_fk;
--alter table tax_book_translate_group_map drop constraint tbtgm_tbtgroup_fk;

--drop table tax_book_translate_group;
--drop table tax_book_translate_group_map;
--drop table tax_book_transactions_grp;
--drop table tax_ret_audit_trail_grp;
--drop table tax_interface_cos;

--update tax_book_translate set tbt_group_id = null;
--commit;
--alter table tax_book_translate drop column tbt_group_id;
--alter table tax_book_transactions drop column tbt_group_id;
--drop index tbt_func_group_idx;


----new table: tax_book_translate_group
create table TAX_BOOK_TRANSLATE_GROUP
(
 TBT_GROUP_ID               number(22,0) not null,
 TAX_CLASS_ID_AGG           varchar2(254),
 START_EFF_BOOK_VINTAGE_AGG varchar2(254),
 END_EFF_BOOK_VINTAGE_AGG   varchar2(254),
 ACTIVE_AGG                 varchar2(254),
 DESCRIPTION                varchar2(254),
 TIME_STAMP                 date,
 USER_ID                    varchar2(18)
);

alter table TAX_BOOK_TRANSLATE_GROUP
   add constraint TAX_BOOK_TRANSLATE_GROUP_PK
       primary key (TBT_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_BOOK_TRANSLATE_GROUP is
'(C) [09] The Tax Book Translate Group is a grouping of distinct tax class combinations used in the collapsing of transactions for the retirements interface.';
comment on column TAX_BOOK_TRANSLATE_GROUP.TBT_GROUP_ID is 'System-assigned key for the tax book translate group';
comment on column TAX_BOOK_TRANSLATE_GROUP.TAX_CLASS_ID_AGG is 'Concatenated strings of tax book translate.tax classes that may be used for the transaction groupings';
comment on column TAX_BOOK_TRANSLATE_GROUP.START_EFF_BOOK_VINTAGE_AGG is 'Concatenated strings of tax book translate.start effective book vintages that may be used for the transaction groupings';
comment on column TAX_BOOK_TRANSLATE_GROUP.END_EFF_BOOK_VINTAGE_AGG is 'Concatenated strings of tax book translate.end effective book vintages that may be used for the transaction groupings';
comment on column TAX_BOOK_TRANSLATE_GROUP.ACTIVE_AGG is 'Concatenated strings of tax book translate.active status that may be used for the transaction groupings';
comment on column TAX_BOOK_TRANSLATE_GROUP.DESCRIPTION is 'Description of the tax book translate group';
comment on column TAX_BOOK_TRANSLATE_GROUP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column TAX_BOOK_TRANSLATE_GROUP.USER_ID is 'Standard system-assigned user id used for audit purposes.';

----add the group to the tax_book_translate table.  also add an index to help the backfilling of the new tbt_group_id
alter table TAX_BOOK_TRANSLATE add TBT_GROUP_ID number(22,0);

alter table TAX_BOOK_TRANSLATE
   add constraint TBT_GROUP_FK
       foreign key (TBT_GROUP_ID)
       references TAX_BOOK_TRANSLATE_GROUP (TBT_GROUP_ID);

comment on column TAX_BOOK_TRANSLATE.TBT_GROUP_ID is 'System-assigned identifier of a tax book translate group.  Used in the collapsing of retirements transactions.';

create index TBT_FUNC_GROUP_IDX on TAX_BOOK_TRANSLATE
   (nvl(COMPANY_ID, 0),
    nvl(BUS_SEGMENT_ID, 0),
    nvl(UTILITY_ACCOUNT_ID, 0),
    nvl(SUB_ACCOUNT_ID, 0),
    nvl(TAX_LOCATION_ID, 0),
    nvl(CLASS_CODE_ID, 0),
    nvl(CLASS_CODE_VALUE, 'xxxxx'),
    nvl(GL_ACCOUNT_ID, 0),
    nvl(TAX_DISTINCTION_ID, 0))
tablespace PWRPLANT_IDX;

----add the group to the tax_book_transactions table
alter table TAX_BOOK_TRANSACTIONS add TBT_GROUP_ID NUMBER(22,0);

alter table TAX_BOOK_TRANSACTIONS
   add constraint TBTX_GROUP_FK
       foreign key (TBT_GROUP_ID)
       references TAX_BOOK_TRANSLATE_GROUP (TBT_GROUP_ID);

----new table.  holds the tax book translate group id and the tax class & effective year combinations...
create table TAX_BOOK_TRANSLATE_GROUP_MAP
(
 TBT_GROUP_ID           NUMBER(22,0) not null,
 TAX_CLASS_ID           NUMBER(22,0) not null,
 START_EFF_BOOK_VINTAGE NUMBER(22,0),
 END_EFF_BOOK_VINTAGE   NUMBER(22,0),
 ACTIVE                 NUMBER(22,2),
 TIME_STAMP             DATE,
 USER_ID                VARCHAR2(18)
);

alter table TAX_BOOK_TRANSLATE_GROUP_MAP
   add constraint TB_TRANSLATE_GROUP_MAP_PK
       primary key (TBT_GROUP_ID, TAX_CLASS_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_BOOK_TRANSLATE_GROUP_MAP
   add constraint TBTGM_CLASS_FK
       foreign key (TAX_CLASS_ID)
       references TAX_CLASS (TAX_CLASS_ID);

alter table TAX_BOOK_TRANSLATE_GROUP_MAP
   add constraint TBTGM_TBTGROUP_FK
       foreign key (TBT_GROUP_ID)
       references TAX_BOOK_TRANSLATE_GROUP (TBT_GROUP_ID);

comment on table TAX_BOOK_TRANSLATE_GROUP_MAP is
'(C) [09] The Tax Book Translate Group Map holds the combination of tax book translation groups to tax classes and effective years and is refreshed during the loading of retirements activity.';
comment on column TAX_BOOK_TRANSLATE_GROUP_MAP.TBT_GROUP_ID is 'System-assigned identifier of a tax book translate group.';
comment on column TAX_BOOK_TRANSLATE_GROUP_MAP.TAX_CLASS_ID is 'System-assigned identifier of a tax class.';
comment on column TAX_BOOK_TRANSLATE_GROUP_MAP.START_EFF_BOOK_VINTAGE is 'Start eff book vintage from tax book translate.';
comment on column TAX_BOOK_TRANSLATE_GROUP_MAP.END_EFF_BOOK_VINTAGE is 'End eff book vintage from tax book translate.';
comment on column TAX_BOOK_TRANSLATE_GROUP_MAP.ACTIVE is 'Active status from tax book translate.';
comment on column TAX_BOOK_TRANSLATE_GROUP_MAP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column TAX_BOOK_TRANSLATE_GROUP_MAP.USER_ID is 'Standard system-assigned user id used for audit purposes.';

----new table: tax_book_transactions_grp.  will hopefully replace tax_book_transactions, summarized at the group level
create table TAX_BOOK_TRANSACTIONS_GRP
(
 TRANS_GRP_ID         number(22,0) not null,
 TBT_GROUP_ID         number(22,0),
 TAX_YEAR             number(22,2),
 START_MONTH          number(22,0),
 END_MONTH            number(22,0),
 TAX_ACTIVITY_CODE_ID number(22,0),
 COMPANY_ID           number(22,0),
 IN_SERVICE_YEAR      date,
 AMOUNT               number(22,2) default 0,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 FOUND_IND            number(22,0),
 FINAL_AMOUNT         number(22,2) default 0,
 RETIREMENT_STATUS    number(22,0),
 AMOUNT_UNPROCESSED   number(22,2)
);

alter table TAX_BOOK_TRANSACTIONS_GRP
   add constraint TAX_BOOK_TRANSACTIONS_GRP_PK
       primary key (TRANS_GRP_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_BOOK_TRANSACTIONS_GRP
   add constraint TAX_BOOK_TRANS_GRP_CO_FK
       foreign key (COMPANY_ID)
       references COMPANY_SETUP (COMPANY_ID);

alter table TAX_BOOK_TRANSACTIONS_GRP
   add constraint TAX_BOOK_TRANS_GRP_ACTCD_FK
       foreign key (TAX_ACTIVITY_CODE_ID)
       references TAX_ACTIVITY_CODE (TAX_ACTIVITY_CODE_ID);

alter table TAX_BOOK_TRANSACTIONS_GRP
   add constraint TAX_BOOK_TRANS_GRP_TBTGRP_FK
       foreign key (TBT_GROUP_ID)
       references TAX_BOOK_TRANSLATE_GROUP (TBT_GROUP_ID);

comment on table TAX_BOOK_TRANSACTIONS_GRP is
'(C) [09] The Tax Book Transactions Grp holds the summarization of records from Tax Book Transactions for faster retirements processing.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.TRANS_GRP_ID is 'System-assigned identifier of a transaction grouping on this table.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.TBT_GROUP_ID is 'System-assigned identifier of a tax book translate group.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.TAX_YEAR is 'Tax Year';
comment on column TAX_BOOK_TRANSACTIONS_GRP.START_MONTH is 'Start month to which transaction groupings apply.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.END_MONTH is 'End month to which transaction groupings apply.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.TAX_ACTIVITY_CODE_ID is 'System-assigned identifier of a tax activity code (e.g., add, retire, sale, etc.)';
comment on column TAX_BOOK_TRANSACTIONS_GRP.COMPANY_ID is 'System-assigned identifier of a company';
comment on column TAX_BOOK_TRANSACTIONS_GRP.IN_SERVICE_YEAR is 'Book in-service year.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.AMOUNT is 'Book dollar amount of the retirement transaction group.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.FOUND_IND is 'Indicator for whether or not a match was found.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.FINAL_AMOUNT is 'Amount actually spread.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.RETIREMENT_STATUS is 'Used to flag unprocessed records and to tag the iteration of the processing.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.AMOUNT_UNPROCESSED is 'Amount left to process.  This is updated with each iteration & batch for the next.';

----new table : tax_ret_audit_trail_grp.  will replace the tax_ret_audit_trail table, summarized at the group level
create table TAX_RET_AUDIT_TRAIL_GRP
(
 COMPANY_ID             number(22,0),
 TAX_ACTIVITY_CODE_ID   number(22,0),
 IN_SERVICE_YEAR        date,
 TAX_YEAR               number(22,2),
 START_MONTH            number(22,0),
 END_MONTH              number(22,0),
 TRANS_GRP_ID           number(22,0) not null,
 TBT_GROUP_ID           number(22,0),
 START_EFF_BOOK_VINTAGE number(22,0),
 END_EFF_BOOK_VINTAGE   number(22,0),
 BOOK_BALANCE           number(22,2),
 HOLD_SUM_BOOK_BALANCE  number(22,2),
 FINAL_AMOUNT           number(22,2) default 0,
 TRANS_AMOUNT           number(22,2),
 VINTAGE_YEAR           number(22,0),
 TAX_RECORD_ID          number(22,0) not null,
 RETIREMENT_STATUS      number(22,0),
 TRID_VINTAGE_YEAR      number(22,0),
 BATCH                  number(22,0),
 ITERATION              number(22,0)
);

create index TAX_RET_AUDIT_TRAIL_GRP_IDX
   on TAX_RET_AUDIT_TRAIL_GRP (TRANS_GRP_ID, TAX_YEAR, TAX_RECORD_ID)
      tablespace PWRPLANT_IDX;

comment on table TAX_RET_AUDIT_TRAIL_GRP is
'(C) [09] The Tax Book Audit Trail Group table contains data to link retirement transaction groupings from book to tax for audit purposes.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.COMPANY_ID is 'System-assigned identifier of a company';
comment on column TAX_RET_AUDIT_TRAIL_GRP.TAX_ACTIVITY_CODE_ID is 'System-assigned identifier of a tax activity code (e.g., add, retire, sale, etc.)';
comment on column TAX_RET_AUDIT_TRAIL_GRP.IN_SERVICE_YEAR is 'Book in-service year.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.TAX_YEAR is 'PowerTax tax year.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.START_MONTH is 'Transactions starting month.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.END_MONTH is 'Transactions ending month.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.TRANS_GRP_ID is 'System-assigned identifier of a particular transaction grouping.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.TBT_GROUP_ID is 'System-assigned identifier of a tax book translate group.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.START_EFF_BOOK_VINTAGE is 'Effecting starting vintage on the tax book translate table.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.END_EFF_BOOK_VINTAGE is 'Effecting ending vintage on the tax book translate table.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.BOOK_BALANCE is 'Beginning book balance (per tax) of this tax target grouping.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.HOLD_SUM_BOOK_BALANCE is 'Sum of the beginning book balance (per tax) of this tax target grouping.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.FINAL_AMOUNT is 'Amount of the retirement assigned to this tax record.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.TRANS_AMOUNT is 'Full transaction grouping book dollar amount';
comment on column TAX_RET_AUDIT_TRAIL_GRP.VINTAGE_YEAR is 'Book vintage year';
comment on column TAX_RET_AUDIT_TRAIL_GRP.TAX_RECORD_ID is 'Corresponding tax record id';
comment on column TAX_RET_AUDIT_TRAIL_GRP.RETIREMENT_STATUS is 'Indicates which iteration from the retirement rules processed this transaction grouping.  A negative value indicates that it did not process.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.TRID_VINTAGE_YEAR is 'Tax vintage year';
comment on column TAX_RET_AUDIT_TRAIL_GRP.BATCH is 'Processing batch of tax records within an iteration.';
comment on column TAX_RET_AUDIT_TRAIL_GRP.ITERATION is 'Processing iteration.';

----new table : tax_interface_cos.  populated with the interface being run and the companies selected.
----        this is needed for all companies with more than 1000 companies being processed.  All
----        tax interfaces should start to use this table.  the plsql function that will populate this
----        table truncates before populating, so no need to make a temporary table.
create table TAX_INTERFACE_COS
(
 COMPANY_ID       number(22,0) not null,
 INTERFACE_OBJECT varchar2(2000) not null,
 USER_ID          varchar2(18) not null
);

alter table TAX_INTERFACE_COS
   add constraint TAX_INTERFACE_COS_PK
       primary key (COMPANY_ID, INTERFACE_OBJECT, USER_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_INTERFACE_COS
   add constraint TAX_INTERFACE_COS_CO_FK
       foreign key (COMPANY_ID)
       references COMPANY_SETUP (COMPANY_ID);

comment on table TAX_INTERFACE_COS is
'(C) [09] The Tax Interface COs table contains all company ids selected by users and used for interface processing.';
comment on column TAX_INTERFACE_COS.COMPANY_ID is 'System-assigned identifier of a company';
comment on column TAX_INTERFACE_COS.INTERFACE_OBJECT is 'System object used to identify the interface being run.  This allows multiple sets of company ids to be selected and used for different interfaces.';
comment on column TAX_INTERFACE_COS.USER_ID is 'User id running the interface.  This allows multiple users to run interfaces.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (702, 0, 10, 4, 2, 0, 32411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_032411_pwrtax_ret_intfc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON
