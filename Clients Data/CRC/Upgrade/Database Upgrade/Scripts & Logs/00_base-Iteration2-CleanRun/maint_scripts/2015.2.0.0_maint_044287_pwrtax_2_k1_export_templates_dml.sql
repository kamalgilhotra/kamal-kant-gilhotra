/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044287_pwrtax_2_k1_export_templates_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/08/2015 Sarah Byers  	 Add new fields available for K1 export
||============================================================================
*/

-- Add a dummy template for any records in TAX_K1_EXPORT_CONFIG_SAVE so we can make the field not nullable
insert into tax_k1_export_config_template (
	k1_exp_config_temp_id, description)
select 1, 'Default'
  from tax_k1_export_config_save
 where rownum = 1;

-- Update the new fields on TAX_K1_EXPORT_CONFIG_SAVE
update tax_k1_export_config_save
	set k1_exp_config_save_id = rownum,
		 k1_exp_config_temp_id = 1;

-- Add the new fields to TAX_K1_EXPORT_CONFIG_AVAIL
insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Book Balance', 'book_balance', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Tax Balance', 'tax_balance', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Remaining Life', 'remaining_life', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Accumulated Reserve', 'accum_reserve', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Straight Line Reserve', 'sl_reserve', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Depreciable Base', 'depreciable_base', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Fixed Depreciable Base', 'fixed_depreciable_base', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Actual Salvage', 'actual_salvage', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Estimated Salvage', 'estimated_salvage', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Accumulated Salvage', 'accum_salvage', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Additions', 'additions', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Transfers', 'transfers', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Adjustments', 'Adjustments', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Retirements', 'retirements', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Extraordinary Retires', 'extraordinary_retires', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Accumulated Ordinary Retires', 'accum_ordinary_retires', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Depreciation', 'depreciation', 1
  from tax_k1_export_config_avail; 

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Cost of Removal', 'cost_of_removal', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'COR Expense', 'cor_expense', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Gain Loss', 'gain_loss', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Capital Gain Loss', 'capital_gain_loss', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Estimated Salvage Percentage', 'est_salvage_pct', 0
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Book Balance', 'book_balance_end', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Accumulated Salvage', 'accum_salvage_end', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Accumulated Ordinary Retires', 'accum_ordin_retires_end', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Straight Line Reserve', 'sl_reserve_end', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Retire Invol Conv', 'retire_invol_conv', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Salvage Invol Conv', 'salvage_invol_conv', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Extraordinary Salvage', 'salvage_extraord', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Calculated Depreciation', 'calc_depreciation', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Over Adj Depreciation', 'over_adj_depreciation', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Retirement Reserve Impact', 'retire_res_impact', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Transfer Reserve Impact', 'transfer_res_impact', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Salvage Reserve Impact', 'salvage_res_impact', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'COR Reserve Impact', 'cor_res_impact', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Adjusted Retirement Basis', 'adjusted_retire_basis', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Reserve at Switch', 'reserve_at_switch', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Capitalized Depreciation', 'capitalized_depr', 0
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Reserve at Switch', 'reserve_at_switch_end', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Beginning Number of Months', 'number_months_beg', 0
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Number of Months', 'number_months_end', 0
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Extraordinary Retirement Reserve Impact', 'ex_retire_res_impact', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Extraordinary Gain Loss', 'ex_gain_loss', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Quantity', 'quantity_end', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Ending Estimated Salvage', 'estimated_salvage_end', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Job Creation Amount', 'job_creation_amount', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Book Balance Adjustment', 'book_balance_adjust', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Accumulated Reserve Adjustment', 'accum_reserve_adjust', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Depreciable Base Adjustment', 'depreciable_base_adjust', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Depreciation Adjustment', 'depreciation_adjust', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Gain Loss Adjustment', 'gain_loss_adjust', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Capital Gain Loss Adjustment', 'cap_gain_loss_adjust', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Gross Retirements', 'gross_retirements', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Gross Accumulated Reserve Retirements', 'gross_accum_res_retire', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Basis Change Difference', 'basis_change_diff', 1
  from tax_k1_export_config_avail;

insert into tax_k1_export_config_avail (
	k1_exp_field_id, description, exp_field_sql, sum_field)
select max(k1_exp_field_id) + 1, 'Gross Additions', 'gross_additions', 1
  from tax_k1_export_config_avail;




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2687, 0, 2015, 2, 0, 0, 044287, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044287_pwrtax_2_k1_export_templates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;