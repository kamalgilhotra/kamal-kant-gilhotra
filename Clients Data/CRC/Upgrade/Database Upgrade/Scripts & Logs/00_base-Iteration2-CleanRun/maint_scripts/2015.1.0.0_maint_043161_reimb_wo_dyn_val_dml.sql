--/*
--||============================================================================
--|| Application: PowerPlan
--|| File Name:   maint_043161_reimb_wo_dyn_val_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/16/2015 Andrew Scott     Dynamic validation added to Reimb
--||                                        Work Order window update.
--||============================================================================
--*/

delete from wo_validation_type where wo_validation_type_id = 68;

insert into wo_validation_type (wo_validation_type_id, description, long_description, function, find_company, col1, col2)
values (68,'Reimbursable WO Update','Updating a Reimb Work Order','w_reimb_work_order',
'select company_id from reimb_bill_group where billing_group_id = <arg1>','billing_group_id','work_order_id');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2387, 0, 2015, 1, 0, 0, 43161, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043161_reimb_wo_dyn_val_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;