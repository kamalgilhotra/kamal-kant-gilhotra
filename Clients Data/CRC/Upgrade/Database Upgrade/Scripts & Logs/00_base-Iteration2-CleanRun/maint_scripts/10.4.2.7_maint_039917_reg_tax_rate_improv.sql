/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039917_reg_tax_rate_improv.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 09/10/2014 Shane Ward
||============================================================================
*/

--Drop table
drop table REG_TAX_RATE;

--Recreate it
create table REG_TAX_RATE
(
 TAX_ENTITY_ID       number(22,0) not null,
 EFF_DATE            date         not null,
 TAX_RATE            number(22,8) not null,
 REG_COMPANY_ID      number(22,0) not null,
 REG_JUR_TEMPLATE_ID number(22,0) not null,
 USER_ID             varchar2(18) null,
 TIME_STAMP          date         null
);

alter table REG_TAX_RATE
   add constraint PK_REG_TAX_RATE
       primary key (TAX_ENTITY_ID,
                    EFF_DATE,
                    REG_COMPANY_ID,
                    REG_JUR_TEMPLATE_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_TAX_RATE
   add constraint R_REG_TAX_RATE1
       foreign key (tax_entity_id)
       references REG_TAX_ENTITY (TAX_ENTITY_ID);

alter table REG_TAX_RATE
   add constraint R_REG_TAX_RATE2
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

alter table REG_TAX_RATE
   add constraint R_REG_TAX_RATE3
       foreign key (REG_JUR_TEMPLATE_ID)
       references REG_JUR_TEMPLATE (REG_JUR_TEMPLATE_ID);

comment on table REG_TAX_RATE is 'The Reg Tax Rate table stores tax rates by tax entities and effective date.';

comment on column REG_TAX_RATE.TAX_ENTITY_ID is 'System-assigned identifier of a tax entity (states, federal)';
comment on column REG_TAX_RATE.EFF_DATE is 'Effective date for the tax rate for the tax entity';
comment on column REG_TAX_RATE.TAX_RATE is 'Tax rate defined for the tax entity';
comment on column REG_TAX_RATE.REG_JUR_TEMPLATE_ID is 'System assigned identifier of Jurisdictional Template';
comment on column REG_TAX_RATE.REG_COMPANY_ID is 'System assigned identifier of Regulatory Company';
comment on column REG_TAX_RATE.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_TAX_RATE.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

--Fix translate for IFA FP Tax Translate
alter table REG_INCREMENTAL_FP_ADJ_TRANS modify BUDGET_VERSION_ID    number(22,0) null;
alter table REG_INCREMENTAL_FP_ADJ_TRANS modify FCST_DEPR_VERSION_ID number(22,0) null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1401, 0, 10, 4, 2, 7, 39917, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039917_reg_tax_rate_improv.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;