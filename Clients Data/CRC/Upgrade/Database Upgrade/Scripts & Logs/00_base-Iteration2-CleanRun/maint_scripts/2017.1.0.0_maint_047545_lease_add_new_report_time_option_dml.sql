/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047545_lease_add_new_report_time_option_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/11/2017 Shane Ward		 Add a new report time option to allow users to select contract/company currency.
||============================================================================
*/

INSERT INTO pp_reports_time_option (pp_report_time_option_id, description, parameter_uo_name, dwname1, label1, keycolumn1)
VALUES (204, 'Lease Currency Type Month Span', 'uo_ppbase_report_parms_dddw_mnum_span', 'dddw_ls_currency_type', 'Currency Type', 'ls_currency_type_id');

UPDATE pp_reports
SET pp_report_time_option_id = 204
WHERE datawindow = 'dw_ls_rpt_bucket_balance';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3490, 0, 2017, 1, 0, 0, 47545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047545_lease_add_new_report_time_option_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;