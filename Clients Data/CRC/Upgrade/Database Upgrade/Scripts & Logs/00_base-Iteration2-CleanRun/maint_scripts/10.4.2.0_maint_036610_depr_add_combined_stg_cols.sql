SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036610_depr_add_combined_stg_cols.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 02/24/2014 Charlie Shilling maint-36045 - add rounding plug columns
||============================================================================
*/

declare
   procedure DROP_COLUMN(V_TABLE_NAME varchar2,
                         V_COL_NAME   varchar2) is
   begin
      execute immediate 'alter table ' || V_TABLE_NAME || ' drop column ' || V_COL_NAME;
      DBMS_OUTPUT.PUT_LINE('Sucessfully dropped column ' || V_COL_NAME || ' from table ' ||
                           V_TABLE_NAME || '.');
   exception
      when others then
         if sqlcode = -904 then
            --904 is invalid identifier, which means that the table already does not have this column, so do nothing
            DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' on table ' || V_TABLE_NAME ||
                                 ' was already removed. No action necessary.');
         else
            RAISE_APPLICATION_ERROR(-20000,
                                    'Could not drop column ' || V_COL_NAME || ' from ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
         end if;
   end DROP_COLUMN;

   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin
   --add depr_rounding_plug
   ADD_COLUMN('depr_calc_combined_stg',
              'depr_rounding_plug',
              'number(22,2)',
              'The adjustment due to rounding error from allocating depreciation expense across child depreciation groups.');
   ADD_COLUMN('depr_calc_combined_arc',
              'depr_rounding_plug',
              'number(22,2)',
              'The adjustment due to rounding error from allocating depreciation expense across child depreciation groups.');
   ADD_COLUMN('fcst_depr_calc_combined_arc',
              'depr_rounding_plug',
              'number(22,2)',
              'The adjustment due to rounding error from allocating depreciation expense across child depreciation groups.');

   --add cor_rounding_plug
   ADD_COLUMN('depr_calc_combined_stg',
              'cor_rounding_plug',
              'number(22,2)',
              'The adjustment due to rounding error from allocating cost of removal reserve expense across child depreciation groups.');
   ADD_COLUMN('depr_calc_combined_arc',
              'cor_rounding_plug',
              'number(22,2)',
              'The adjustment due to rounding error from allocating cost of removal reserve expense across child depreciation groups.');
   ADD_COLUMN('fcst_depr_calc_combined_arc',
              'cor_rounding_plug',
              'number(22,2)',
              'The adjustment due to rounding error from allocating cost of removal reserve expense across child depreciation groups.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1004, 0, 10, 4, 2, 0, 36610, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036610_depr_add_combined_stg_cols.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;