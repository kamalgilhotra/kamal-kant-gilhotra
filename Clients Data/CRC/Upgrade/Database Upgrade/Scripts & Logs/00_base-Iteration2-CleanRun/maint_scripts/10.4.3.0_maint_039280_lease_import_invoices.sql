/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_039280_lease_import_invoices.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 10.4.3.0 08/05/2014 Daniel Motter  Creation
 ||============================================================================
 */

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (254, 'ilr_id', null, null, 'ILR', 'ilr_xlate', 0, 1, 'number(22,0)', 'ls_ilr', null, 1, null, 'ilr_id', null, null);

update PP_IMPORT_COLUMN
   set PARENT_TABLE = null,
       PARENT_TABLE_PK_COLUMN = null
 where COLUMN_NAME = 'payment_type_id';

alter table LS_IMPORT_INVOICE         add ILR_ID number(22,0);
alter table LS_IMPORT_INVOICE_ARCHIVE add ILR_ID number(22,0);

alter table LS_IMPORT_INVOICE         add ILR_XLATE varchar2(35);
alter table LS_IMPORT_INVOICE_ARCHIVE add ILR_XLATE varchar2(35);

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
select max(import_lookup_id) + 1, 'LS ILR.ILR Number', 'LS ILR.ILR Number', 'ilr_id', '( select b.ilr_id from ls_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.ilr_number ) ) )', 0, 'ls_ilr', 'ilr_number', NULL, NULL, NULL
  from PP_IMPORT_LOOKUP;

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
select 254, 'ilr_id', IMPORT_LOOKUP_ID
  from PP_IMPORT_LOOKUP P
 where P.DESCRIPTION = 'LS ILR.ILR Number';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1307, 0, 10, 4, 3, 0, 39280, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039280_lease_import_invoices.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;