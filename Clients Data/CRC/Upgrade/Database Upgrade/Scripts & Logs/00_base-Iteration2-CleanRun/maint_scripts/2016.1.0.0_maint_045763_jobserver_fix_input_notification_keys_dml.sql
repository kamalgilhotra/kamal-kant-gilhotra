/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045763_jobserver_fix_input_notification_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 06/20/2016 David Haupt  Fix the Delete keys for Input and Notification jobs
||============================================================================
*/

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Schedule.Delete.Input'
WHERE objects = 'JobService.Workflow.Delete.Input'
;

UPDATE pp_web_security_perm_control
SET objects = 'JobService.Schedule.Delete.Notification'
WHERE objects = 'JobService.Workflow.Delete.Notification'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3224, 0, 2016, 1, 0, 0, 045763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045763_jobserver_fix_input_notification_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;