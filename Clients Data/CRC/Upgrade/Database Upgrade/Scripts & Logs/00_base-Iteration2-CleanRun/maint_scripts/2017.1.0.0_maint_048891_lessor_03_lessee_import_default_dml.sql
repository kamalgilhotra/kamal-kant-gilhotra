/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048891_lessor_03_lessee_import_default_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/29/2017 Shane "C" Ward  Set up Lessor Lessee Import Tables
||============================================================================
*/
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
SELECT Max ( import_template_id ) + 1,
       500,
       'Lessor Lessee Add',
       'Default Lessor Lessee Add Template',
       1,
       0
FROM   PP_IMPORT_TEMPLATE;

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name)
SELECT import_template_id,
       1,
       500,
       'description'
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 500 AND
       ( import_template_id, 'description' ) NOT IN ( SELECT import_template_id,
                                                             column_name
                                                      FROM   PP_IMPORT_TEMPLATE_FIELDS
                                                      WHERE  import_type_id = 500 );

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name)
SELECT import_template_id,
       2,
       500,
       'external_lessee_number'
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 500 AND
       ( import_template_id, 'external_lessee_number' ) NOT IN ( SELECT import_template_id,
                                                                        column_name
                                                                 FROM   PP_IMPORT_TEMPLATE_FIELDS
                                                                 WHERE  import_type_id = 500 );

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name)
SELECT import_template_id,
       3,
       500,
       'lease_group_id'
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 500 AND
       ( import_template_id, 'long_description' ) NOT IN ( SELECT import_template_id,
                                                                  column_name
                                                           FROM   PP_IMPORT_TEMPLATE_FIELDS
                                                           WHERE  import_type_id = 500 );
																 
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name)
SELECT import_template_id,
       4,
       500,
       'long_description'
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 500 AND
       ( import_template_id, 'long_description' ) NOT IN ( SELECT import_template_id,
                                                                  column_name
                                                           FROM   PP_IMPORT_TEMPLATE_FIELDS
                                                           WHERE  import_type_id = 500 );

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name)
SELECT import_template_id,
       5,
       500,
       'unique_lessee_identifier'
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 500 AND
       ( import_template_id, 'unique_lessee_identifier' ) NOT IN ( SELECT import_template_id,
                                                                          column_name
                                                                   FROM   PP_IMPORT_TEMPLATE_FIELDS
                                                                   WHERE  import_type_id = 500 ); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
  (3748, 0, 2017, 1, 0, 0, 48891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048891_lessor_03_lessee_import_default_dml.sql', 1,
  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;