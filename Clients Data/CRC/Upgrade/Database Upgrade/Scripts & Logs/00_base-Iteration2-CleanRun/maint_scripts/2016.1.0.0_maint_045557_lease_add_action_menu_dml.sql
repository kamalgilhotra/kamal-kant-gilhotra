/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045557_lease_add_action_menu_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 03/17/2016 Anand R			 New Actions drop down for 
||											 Build/Modify Revision workspace
||============================================================================
*/ 

INSERT INTO ppbase_actions_windows (id, MODULE, action_identifier, action_text, action_order, action_event)
SELECT Nvl(Max(id) + 1, 1), 'LESSEE', 'reset_revision', 'Reset Revision', 5, 'ue_resetrevision'
FROM ppbase_actions_windows;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3109, 0, 2016, 1, 0, 0, 045557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045557_lease_add_action_menu_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;