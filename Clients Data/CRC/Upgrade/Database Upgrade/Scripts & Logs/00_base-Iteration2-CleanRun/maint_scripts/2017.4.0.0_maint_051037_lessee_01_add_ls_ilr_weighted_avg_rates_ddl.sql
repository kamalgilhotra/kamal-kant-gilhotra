/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051037_lessee_01_add_ls_ilr_weighted_avg_rates_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/24/2018 Josh Sandler 	  Create new table: LS_ILR_WEIGHTED_AVG_RATES
||============================================================================
*/

CREATE TABLE ls_ilr_weighted_avg_rates
  (
    ilr_id                    NUMBER(22),
    revision                  NUMBER(22),
    set_of_books_id           NUMBER(22),
    gross_weighted_avg_rate   NUMBER(22,8),
    net_weighted_avg_rate     NUMBER(22,8),
    user_id                   VARCHAR2(18),
    time_stamp                DATE
  );

ALTER TABLE ls_ilr_weighted_avg_rates
  ADD CONSTRAINT pk_ls_ilr_weighted_avg_rates PRIMARY KEY (ilr_id, revision, set_of_books_id) USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE ls_ilr_weighted_avg_rates
  ADD CONSTRAINT fk_avg_rates_ilr_rev FOREIGN KEY (
    ilr_id, revision
  ) REFERENCES ls_ilr_options (
    ilr_id, revision
  )
;

ALTER TABLE ls_ilr_weighted_avg_rates
  ADD CONSTRAINT fk_avg_rates_sob FOREIGN KEY (
    set_of_books_id
  ) REFERENCES set_of_books (
    set_of_books_id
  )
;

COMMENT ON COLUMN ls_ilr_weighted_avg_rates.ilr_id IS 'System-assigned identifier of an ILR.';
COMMENT ON COLUMN ls_ilr_weighted_avg_rates.revision IS 'System-assigned identifier of a revision.';
COMMENT ON COLUMN ls_ilr_weighted_avg_rates.set_of_books_id IS 'System-assigned identifier for the set of books.';
COMMENT ON COLUMN ls_ilr_weighted_avg_rates.gross_weighted_avg_rate IS 'The weighted average exchange rate calculated based on the Gross ROU Amount corresponding to each In Service Date Rate for an ILR.';
COMMENT ON COLUMN ls_ilr_weighted_avg_rates.net_weighted_avg_rate IS 'The weighted average exchange rate calculated based on the Net ROU Amount corresponding to each In Service Date Rate for an ILR.';
COMMENT ON COLUMN ls_ilr_weighted_avg_rates.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_ilr_weighted_avg_rates.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6362, 0, 2017, 4, 0, 0, 51037, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051037_lessee_01_add_ls_ilr_weighted_avg_rates_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;