/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_052803_lessee_01_post_schedule_dynamic_validation_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2018.1.0.1 12/7/2018 Crystal Yura     Add new dynamic validation for Post Schedule Build, Rename existing to Pre
||============================================================================
*/


INSERT INTO WO_VALIDATION_TYPE
  (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, FUNCTION, FIND_COMPANY, COL1, COL2, HARD_EDIT)
  SELECT 5015,
         'Lessee ILRs Post',
         'Lessee ILRs Post Schedule Build/Send for Approval',
         'uo_ls_ilrcntr_wksp_details',
         'select company_id from ls_ilr where ilr_id = <arg1>',
         'ilr_id',
	 'revision',
         '1'
    FROM DUAL
   WHERE NOT EXISTS (SELECT 1
            FROM WO_VALIDATION_TYPE
           WHERE WO_VALIDATION_TYPE_ID = 5015
             AND LOWER(FUNCTION) = 'uo_ls_ilrcntr_wksp_details');

UPDATE WO_VALIDATION_TYPE 
SET description = 'Lessee ILRs Pre', 
long_description = 'Lessee ILRs Pre Schedule Build'
WHERE WO_VALIDATION_TYPE_ID = 5001;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12884, 0, 2018, 1, 0, 1, 52803, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052803_lessee_01_post_schedule_dynamic_validation_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;