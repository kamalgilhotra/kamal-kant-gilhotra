/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029713_sys_04_enable_pend_trans_check_trig.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.2.0   09/05/2013 Charlie Shilling Maint-29713
||============================================================================
*/

alter trigger PEND_TRANS_CHECK_UPDATE enable;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (656, 0, 10, 4, 2, 0, 29713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029713_sys_04_enable_pend_trans_check_trig.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
