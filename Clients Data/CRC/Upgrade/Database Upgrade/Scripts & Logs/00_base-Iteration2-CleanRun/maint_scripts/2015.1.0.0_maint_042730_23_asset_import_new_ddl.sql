/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_23_asset_import_new_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

alter table ls_import_asset
add (used_yn_sw varchar2(35), gl_account_id number(22,0), gl_account_xlate varchar2(254))
;
alter table ls_import_asset_archive
add (used_yn_sw varchar2(35), gl_account_id number(22,0), gl_account_xlate varchar2(254))
;

alter table ls_import_asset
add (property_tax_date varchar2(254), property_tax_amount number(22,2))
;
alter table ls_import_asset_archive
add (property_tax_date varchar2(254), property_tax_amount number(22,2))
;

COMMENT ON COLUMN ls_import_asset.property_tax_date IS 'The property tax start date';
COMMENT ON COLUMN ls_import_asset.property_tax_amount IS 'The basis amount for property tax.  This field is only used for client specific property tax interfaces if the property tax basis is different than the capital book basis';
COMMENT ON COLUMN ls_import_asset.used_yn_sw IS 'Switch used to indicate whether a leased asset is used or new for bonus deprecaition purposed. 0 indicates new. 1 indicates used';
COMMENT ON COLUMN ls_import_asset.gl_account_xlate IS 'Translation field to determine CPR Ledger GL account associated with the leased asset';
COMMENT ON COLUMN ls_import_asset.gl_account_id IS 'CPR Ledger GL account associated with the leased asset';

COMMENT ON COLUMN ls_import_asset_archive.property_tax_date IS 'The property tax start date';
COMMENT ON COLUMN ls_import_asset_archive.property_tax_amount IS 'The basis amount for property tax.  This field is only used for client specific property tax interfaces if the property tax basis is different than the capital book basis';
COMMENT ON COLUMN ls_import_asset_archive.used_yn_sw IS 'Switch used to indicate whether a leased asset is used or new for bonus deprecaition purposed. 0 indicates new. 1 indicates used';
COMMENT ON COLUMN ls_import_asset_archive.gl_account_xlate IS 'Translation field to determine CPR Ledger GL account associated with the leased asset';
COMMENT ON COLUMN ls_import_asset_archive.gl_account_id IS 'CPR Ledger GL account associated with the leased asset';




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2395, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_23_asset_import_new_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;