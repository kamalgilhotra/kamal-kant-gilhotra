/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_050735_lessee_04_query_fcst_ILR_View_DDL.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0.0 05/31/2018   K. Powers   PP-50735 MLA Lease Forecast Query use 
 ||                                     MultiCurrency Views
 ||============================================================================
 */ 
 
CREATE OR REPLACE VIEW LS_FCST_ASSET_SCHED_BY_ILR_VW (
FORECAST_VERSION,
ILR_NUMBER,
CONVERSION_DATE,
LS_ASSET_ID,
LEASED_ASSET_NUMBER,
COMPANY_ID,
COMPANY_DESCRIPTION,
ILR_ID,
LEASE_ID,
LEASE_NUMBER,
LEASE_CAP_TYPE,
LOCATION,
REVISION,
SET_OF_BOOKS,
MONTHNUM,
CURRENCY_TYPE,
ISO_CODE,
CURRENCY_DISPLAY_SYMBOL,
BEG_CAPITAL_COST,
END_CAPITAL_COST,
BEG_OBLIGATION,
END_OBLIGATION,
BEG_LT_OBLIGATION,
END_LT_OBLIGATION,
INTEREST_ACCRUAL,
PRINCIPAL_ACCRUAL,
INTEREST_PAID,
PRINCIPAL_PAID,
EXECUTORY_ACCRUAL1,
EXECUTORY_ACCRUAL2,
EXECUTORY_ACCRUAL3,
EXECUTORY_ACCRUAL4,
EXECUTORY_ACCRUAL5,
EXECUTORY_ACCRUAL6,
EXECUTORY_ACCRUAL7,
EXECUTORY_ACCRUAL8,
EXECUTORY_ACCRUAL9,EXECUTORY_ACCRUAL10,
EXECUTORY_PAID1,
EXECUTORY_PAID2,
EXECUTORY_PAID3,
EXECUTORY_PAID4,
EXECUTORY_PAID5,
EXECUTORY_PAID6,
EXECUTORY_PAID7,
EXECUTORY_PAID8,
EXECUTORY_PAID9,
EXECUTORY_PAID10,
CONTINGENT_ACCRUAL1,
CONTINGENT_ACCRUAL2,
CONTINGENT_ACCRUAL3,
CONTINGENT_ACCRUAL4,
CONTINGENT_ACCRUAL5,
CONTINGENT_ACCRUAL6,
CONTINGENT_ACCRUAL7,
CONTINGENT_ACCRUAL8,
CONTINGENT_ACCRUAL9,
CONTINGENT_ACCRUAL10,
CONTINGENT_PAID1,
CONTINGENT_PAID2,
CONTINGENT_PAID3,
CONTINGENT_PAID4,
CONTINGENT_PAID5,
CONTINGENT_PAID6,
CONTINGENT_PAID7,
CONTINGENT_PAID8,
CONTINGENT_PAID9,
CONTINGENT_PAID10,
IS_OM,
CURRENT_LEASE_COST,
RESIDUAL_AMOUNT,
TERM_PENALTY,
BPO_PRICE
)
as
select lfv.description forecast_version, ilr.ilr_number, lfv.conversion_date as conversion_date, la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, sob.DESCRIPTION as set_of_books, to_char(las.MONTH, 'yyyymm') as monthnum,
       llct.description as CURRENCY_TYPE,las.ISO_CODE as ISO_CODE,las.CURRENCY_DISPLAY_SYMBOL,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2,  las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10,  las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,  las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, 
      v_ls_asset_schedule_fx_vw las, ls_forecast_version lfv,set_of_books sob,ls_lease_currency_type llct
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and lfv.revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id 
  and llct.ls_currency_type_id = las.ls_cur_type 
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = lfv.revision
  and abs(las.set_of_books_id)=abs(lfv.set_of_books_id)
  and las.set_of_books_id=sob.set_of_books_id
  and upper(ilr.ilr_number) in (select upper(filter_value) from pp_any_required_filter where upper(column_name) = 'ILR NUMBER')
  and upper(lfv.description) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = 'FORECAST VERSION');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6526, 0, 2017, 4, 0, 0, 50735, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050735_lessee_04_query_fcst_ILR_View_DDL.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;