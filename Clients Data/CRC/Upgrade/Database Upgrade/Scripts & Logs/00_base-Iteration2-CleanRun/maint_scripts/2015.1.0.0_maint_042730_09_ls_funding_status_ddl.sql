/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_09_ls_funding_status_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/


alter table ls_ilr
add funding_status_id number(1,0) default 0;

COMMENT ON COLUMN ls_ilr.funding_status_id IS 'Funding status to determine state of components related to ILR.';

create table ls_funding_status
(funding_status_id number(1,0) not null,
 description       varchar2(35),
 long_description  varchar2(256),
 time_stamp        date,
 user_id           varchar2(25));

alter table ls_funding_status
add constraint pk_ls_funding_status
primary key (funding_status_id)
using index tablespace pwrplant_idx;

create index component_id_idx
on ls_component_charge
(component_id)
TABLESPACE pwrplant_idx
/

create index ls_asset_id_idx
on ls_component
(ls_asset_id)
TABLESPACE pwrplant_idx
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2410, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_09_ls_funding_status_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;