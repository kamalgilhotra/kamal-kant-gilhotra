/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_system_proptax_bill_import_reconfig.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/05/2013 Stephen Wicks  Point Release
||============================================================================
*/

--
-- Create a new import for statements, which will be autocreated from bill lines.
--

insert into PWRPLANT.PP_IMPORT_SUBSYSTEM ( IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 7, sysdate, user, 'Property Tax - Hidden', 'Property Tax imports that are used for autocreations and should not be available to users.' );

insert into PWRPLANT.PP_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION ) values ( 42, sysdate, user, 'Add : Bills', 'Import Bills, including Statement Year, Statement Installment, and Statement Period records', 'pt_import_stmt', 'pt_import_stmt_archive', null, 0, 'uo_ptc_logic_import', 'statement_id', 'Bill' );

insert into PWRPLANT.PP_IMPORT_TYPE_SUBSYSTEM ( IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID ) values ( 42, 7, sysdate, user );

insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'statement_year_id', sysdate, user, 'Statement Year', 'statement_year_xlate', 1, 1, 'number(22,0)', 'pt_statement_year', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'state_id', sysdate, user, 'State', 'state_xlate', 1, 1, 'char(18)', 'state', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', 'state_id', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'prop_tax_company_id', sysdate, user, 'Prop Tax Company', 'prop_tax_company_xlate', 0, 1, 'number(22,2)', 'pt_company', '', 0, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(100)', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'account_number', sysdate, user, 'Account Number', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'statement_notes', sysdate, user, 'Notes (Statement)', '', 0, 1, 'varchar2(254)', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'assessment_year_id', sysdate, user, 'Assessment Year', 'assessment_year_xlate', 1, 1, 'number(22,0)', 'pt_assessment_year', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'statement_group_id', sysdate, user, 'Statement Group', 'statement_group_xlate', 1, 2, 'number(22,0)', 'pt_statement_group', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'statement_number', sysdate, user, 'Statement Number', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'year_notes', sysdate, user, 'Notes (Statement Year)', '', 0, 1, 'varchar2(2000)', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'received_date', sysdate, user, 'Received Date', '', 0, 1, 'date', '', '', 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 42, 'estimated_switch_yn', sysdate, user, 'Estimated Bill', 'estimated_switch_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null );

insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'statement_year_id', 179, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'statement_year_id', 180, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'statement_year_id', 181, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'prop_tax_company_id', 3, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'prop_tax_company_id', 5, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'prop_tax_company_id', 4, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'assessment_year_id', 185, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'assessment_year_id', 186, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'statement_group_id', 151, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'statement_group_id', 154, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'statement_group_id', 152, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'statement_group_id', 153, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'estimated_switch_yn', 77, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 42, 'estimated_switch_yn', 78, sysdate, user );

--
-- Create new tables.
--

-- Create new statement import tables, one for header records and one for lines
create table PWRPLANT.PT_IMPORT_STMT
(
 IMPORT_RUN_ID          number(22,0) not null,
 LINE_ID                number(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 DESCRIPTION            varchar2(254),
 LONG_DESCRIPTION       varchar2(254),
 ACCOUNT_NUMBER         varchar2(254),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 STATEMENT_XLATE        varchar2(254),
 STATEMENT_ID           number(22,0),
 STATEMENT_NOTES        varchar2(254),
 STATEMENT_YEAR_XLATE   varchar2(254),
 STATEMENT_YEAR_ID      number(22,0),
 ASSESSMENT_YEAR_XLATE  varchar2(254),
 ASSESSMENT_YEAR_ID     number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 STATEMENT_GROUP_XLATE  varchar2(254),
 STATEMENT_GROUP_ID     number(22,0),
 STATEMENT_NUMBER       varchar2(254),
 RECEIVED_DATE          varchar2(35),
 ESTIMATED_SWITCH_XLATE varchar2(254),
 ESTIMATED_SWITCH_YN    number(22,0),
 YEAR_NOTES             varchar2(2000),
 TAX_AMOUNT             varchar2(35),
 CREDIT_AMOUNT          varchar2(35),
 PENALTY_AMOUNT         varchar2(35),
 INTEREST_AMOUNT        varchar2(35),
 ASSESSMENT             varchar2(35),
 TAXABLE_VALUE          varchar2(35),
 ERROR_MESSAGE          varchar2(4000)
);

alter table PT_IMPORT_STATEMENT drop primary key drop index;

alter table PWRPLANT.PT_IMPORT_STMT
   add constraint PT_IMPORT_STMT_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_STMT
   add constraint PT_IMPT_STMT_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_STMT_ARCHIVE
(
 IMPORT_RUN_ID          number(22,0) not null,
 LINE_ID                number(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 DESCRIPTION            varchar2(254),
 LONG_DESCRIPTION       varchar2(254),
 ACCOUNT_NUMBER         varchar2(254),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 STATEMENT_XLATE        varchar2(254),
 STATEMENT_ID           number(22,0),
 STATEMENT_NOTES        varchar2(254),
 STATEMENT_YEAR_XLATE   varchar2(254),
 STATEMENT_YEAR_ID      number(22,0),
 ASSESSMENT_YEAR_XLATE  varchar2(254),
 ASSESSMENT_YEAR_ID     number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 STATEMENT_GROUP_XLATE  varchar2(254),
 STATEMENT_GROUP_ID     number(22,0),
 STATEMENT_NUMBER       varchar2(254),
 RECEIVED_DATE          varchar2(35),
 ESTIMATED_SWITCH_XLATE varchar2(254),
 ESTIMATED_SWITCH_YN    number(22,0),
 YEAR_NOTES             varchar2(2000),
 TAX_AMOUNT             varchar2(35),
 CREDIT_AMOUNT          varchar2(35),
 PENALTY_AMOUNT         varchar2(35),
 INTEREST_AMOUNT        varchar2(35),
 ASSESSMENT             varchar2(35),
 TAXABLE_VALUE          varchar2(35)
);

alter table PT_IMPORT_STATEMENT_ARCHIVE drop primary key drop index;

alter table PWRPLANT.PT_IMPORT_STMT_ARCHIVE
   add constraint PT_IMPORT_STMT_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_STMT_ARCHIVE
   add constraint PT_IMPT_STMT_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_STMT_LINE
(
 IMPORT_RUN_ID          number(22,0) not null,
 LINE_ID                number(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 STATEMENT_YEAR_XLATE   varchar2(254),
 STATEMENT_YEAR_ID      number(22,0),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 STATEMENT_XLATE        varchar2(254),
 STATEMENT_ID           number(22,0),
 TAX_AUTHORITY_XLATE    varchar2(254),
 TAX_AUTHORITY_ID       number(22,0),
 LEVY_CLASS_XLATE       varchar2(254),
 LEVY_CLASS_ID          number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 ASSESSOR_XLATE         varchar2(254),
 ASSESSOR_ID            number(22,0),
 PARCEL_XLATE           varchar2(254),
 PARCEL_ID              number(22,0),
 VERIFIED_STATUS_XLATE  varchar2(254),
 VERIFIED_STATUS_ID     number(22,0),
 FIRST_INSTALL_XLATE    varchar2(254),
 FIRST_INSTALL_YN       number(22,0),
 TAX_AMOUNT             varchar2(35),
 CREDIT_AMOUNT          varchar2(35),
 PENALTY_AMOUNT         varchar2(35),
 INTEREST_AMOUNT        varchar2(35),
 ASSESSMENT             varchar2(35),
 TAXABLE_VALUE          varchar2(35),
 NOTES                  varchar2(2000),
 ERROR_MESSAGE          varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_STMT_LINE
   add constraint PT_IMPORT_STMT_LN_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_STMT_LINE
   add constraint PT_IMPT_STMT_LN_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PWRPLANT.PP_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_STMT_LINE_ARCHIVE
(
 IMPORT_RUN_ID          number(22,0) not null,
 LINE_ID                number(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 STATEMENT_YEAR_XLATE   varchar2(254),
 STATEMENT_YEAR_ID      number(22,0),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 STATEMENT_XLATE        varchar2(254),
 STATEMENT_ID           number(22,0),
 TAX_AUTHORITY_XLATE    varchar2(254),
 TAX_AUTHORITY_ID       number(22,0),
 LEVY_CLASS_XLATE       varchar2(254),
 LEVY_CLASS_ID          number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 ASSESSOR_XLATE         varchar2(254),
 ASSESSOR_ID            number(22,0),
 PARCEL_XLATE           varchar2(254),
 PARCEL_ID              number(22,0),
 VERIFIED_STATUS_XLATE  varchar2(254),
 VERIFIED_STATUS_ID     number(22,0),
 FIRST_INSTALL_XLATE    varchar2(254),
 FIRST_INSTALL_YN       number(22,0),
 TAX_AMOUNT             varchar2(35),
 CREDIT_AMOUNT          varchar2(35),
 PENALTY_AMOUNT         varchar2(35),
 INTEREST_AMOUNT        varchar2(35),
 ASSESSMENT             varchar2(35),
 TAXABLE_VALUE          varchar2(35),
 NOTES                  varchar2(2000)
);

alter table PWRPLANT.PT_IMPORT_STMT_LINE_ARCHIVE
   add constraint PT_IMPORT_STMT_LN_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_STMT_LINE_ARCHIVE
   add constraint PT_IMPT_STMT_LN_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PWRPLANT.PP_IMPORT_RUN;

--
-- Update the existing import types for the changes.
-- - Set the new import table names
-- - Remove columns that are no longer part of the "Add : Lines" and "Update : Lines" imports since they will be in the autocreate import
-- - Rename the line_notes column
-- - Make tax_authority_id required on the "Add : Lines" import
-- - Remove the updates lookup for the "Add : Lines" import
-- - Add statement_id to the Add : Lines import and set it as an autocreate - also, remove it from archive additional columns
--
update PWRPLANT.PP_IMPORT_TYPE set IMPORT_TABLE_NAME = 'pt_import_stmt', ARCHIVE_TABLE_NAME = 'pt_import_stmt_archive' where IMPORT_TYPE_ID in ( 20 );
update PWRPLANT.PP_IMPORT_TYPE set IMPORT_TABLE_NAME = 'pt_import_stmt_line', ARCHIVE_TABLE_NAME = 'pt_import_stmt_line_archive' where IMPORT_TYPE_ID in ( 21, 22 );
delete from PWRPLANT.PP_IMPORT_COLUMN_LOOKUP where IMPORT_TYPE_ID = 22 and COLUMN_NAME in ( 'description', 'long_description', 'account_number', 'statement_notes', 'assessment_year_id', 'statement_group_id', 'statement_number', 'year_notes', 'received_date' );
delete from PWRPLANT.PP_IMPORT_COLUMN where IMPORT_TYPE_ID = 22 and COLUMN_NAME in ( 'description', 'long_description', 'account_number', 'statement_notes', 'assessment_year_id', 'statement_group_id', 'statement_number', 'year_notes', 'received_date' );
delete from PWRPLANT.PP_IMPORT_COLUMN where IMPORT_TYPE_ID = 21 and COLUMN_NAME in ( 'statement_number' );
update PWRPLANT.PP_IMPORT_COLUMN set COLUMN_NAME = 'notes' where IMPORT_TYPE_ID = 22 and COLUMN_NAME = 'line_notes';
update PWRPLANT.PP_IMPORT_COLUMN set IS_REQUIRED = 1 where IMPORT_TYPE_ID = 22 and COLUMN_NAME = 'tax_authority_id';
delete from PWRPLANT.PP_IMPORT_TYPE_UPDATES_LOOKUP where IMPORT_TYPE_ID = 22;

insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 22, 'statement_id', sysdate, user, 'Statement', 'statement_xlate', 1, 3, 'number(22,0)', 'pt_statement', '', 1, 42 );

insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 170, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 172, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 171, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 173, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 175, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 174, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 176, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 178, sysdate, user );
insert into PWRPLANT.PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 22, 'statement_id', 177, sysdate, user );

update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = null where IMPORT_TYPE_ID = 22;

--
-- Add a field to import type that allows us to store additional restrictions for what data gets loaded into the autocreate staging table.
-- Update it for statements.
--
alter table PWRPLANT.PP_IMPORT_TYPE add AUTOCREATE_RESTRICT_SQL varchar2(4000);
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_RESTRICT_SQL = 'or not exists ( select 1 from pt_statement_statement_year ssy where ssy.statement_year_id = impt.statement_year_id and ssy.statement_id = impt.statement_id )' where import_type_id = 42;

--
-- Clean up some SQL for lookup values.
--
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select pt_statement.description, state.<state_id>, county.<county_id> from pt_statement, state, county where pt_statement.state_id = state.state_id and pt_statement.state_id = county.state_id (+) and pt_statement.county_id = county.county_id (+) and 5=5' where IMPORT_LOOKUP_ID = 175;
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select pt_parcel.parcel_number, state.<state_id>, pt_assessor.<assessor_id>, pt_company.<prop_tax_company_id> from pt_parcel, state, pt_company, prop_tax_district, pt_assessor where pt_parcel.state_id = state.state_id and pt_parcel.prop_tax_company_id = pt_company.prop_tax_company_id and pt_parcel.tax_district_id = prop_tax_district.tax_district_id and prop_tax_district.assessor_id = pt_assessor.assessor_id (+) and 5=5' where import_lookup_id = 71;
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select town.description, state.<state_id>, county.<county_id> from town, state, county where town.state_id = state.state_id (+) and town.state_id = county.state_id (+) and town.county_id = county.county_id (+) and 5=5' where IMPORT_LOOKUP_ID = 150;
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select town.description, state.<state_id> from town, state where town.state_id = state.state_id (+) and 5=5' where IMPORT_LOOKUP_ID = 155;
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select town.town_code, state.<state_id>, county.<county_id> from town, state, county where town.state_id = state.state_id (+) and town.state_id = county.state_id (+) and town.county_id = county.county_id (+) and 5=5' where IMPORT_LOOKUP_ID = 156;
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select town.town_code, state.<state_id> from town, state where town.state_id = state.state_id (+) and 5=5' where IMPORT_LOOKUP_ID = 157;
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select pt_statement.account_number, state.<state_id>, county.<county_id> from pt_statement, state, county where pt_statement.state_id = state.state_id and pt_statement.state_id = county.state_id (+) and pt_statement.county_id = county.county_id (+) and 5=5' where IMPORT_LOOKUP_ID = 172;
update PWRPLANT.PP_IMPORT_LOOKUP set LOOKUP_VALUES_ALTERNATE_SQL = 'select pt_statement.long_description, state.<state_id>, county.<county_id> from pt_statement, state, county where pt_statement.state_id = state.state_id and pt_statement.state_id = county.state_id (+) and pt_statement.county_id = county.county_id (+) and 5=5' where IMPORT_LOOKUP_ID = 178;

--
-- Now that retirement unit and utility account are autocreated, we don't need these fields on the asset import.
--
delete from PWRPLANT.PP_IMPORT_COLUMN_LOOKUP where IMPORT_TYPE_ID = 25 and COLUMN_NAME in ( 'ua_description', 'status_code_id', 'ua_external_account_code', 'property_unit_id', 'ru_description', 'ru_long_description', 'ru_external_retire_unit', 'ua_line_id', 'ru_line_id' );
delete from PWRPLANT.PP_IMPORT_COLUMN where IMPORT_TYPE_ID = 25 and COLUMN_NAME in ( 'ua_description', 'status_code_id', 'ua_external_account_code', 'property_unit_id', 'ru_description', 'ru_long_description', 'ru_external_retire_unit', 'ua_line_id', 'ru_line_id' );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (293, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_system_proptax_bill_import_reconfig.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
