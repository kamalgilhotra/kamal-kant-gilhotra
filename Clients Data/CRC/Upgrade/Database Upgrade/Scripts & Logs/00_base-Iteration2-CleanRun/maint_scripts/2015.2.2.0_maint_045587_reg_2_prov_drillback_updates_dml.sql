/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045587_reg_2_prov_drillback_updates_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.2.0 04/13/2016 Sarah Byers  	 Updated Tax Provision Pull
||============================================================================
*/

-- Update reg_query_drillback
update reg_query_drillback
	set table_name = upper('reg_fcst_ta_defer_drill_view')
 where upper(table_name) = 'FCST_TAX_ACCRUAL_FAS109';

update reg_query_drillback
	set table_name = upper('reg_hist_ta_defer_drill_view')
 where upper(table_name) = 'TAX_ACCRUAL_FAS109';

-- Fix the query columns
delete from cr_dd_sources_criteria_fields
 where id in (
	select id from cr_dd_sources_criteria
	 where lower(table_name) in ('reg_hist_ta_defer_drill_view', 
										  'reg_fcst_ta_defer_drill_view',
										  'reg_fcst_ta_def_drill_inc_view',
										  'reg_hist_ta_def_drill_inc_view'));

-- reg_hist_ta_defer_drill_view
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'ADJUSTMENT', 17, to_date('2016-04-12 13:22:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Adjustment', 303, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'BEG_BALANCE', 12, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'COMPANY', 2, to_date('2016-04-12 13:25:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 253, 0, 'Upper', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'END_BALANCE', 18, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'GL_ACCOUNT', 8, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'GL Account', 308, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'GL_MONTH', 11, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'JE_TYPE', 5, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'JE Type', 226, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'M_ITEM', 3, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'M Item', 194, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'OPER_IND', 6, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Oper Ind', 244, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'OTHER', 19, to_date('2016-04-12 13:22:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Other', 162, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'PAYMENTS', 20, to_date('2016-04-12 13:22:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payments', 267, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'PROVISION', 13, to_date('2016-04-12 13:22:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Provision', 258, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'REG_ACCOUNT', 9, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Account', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'REVERSAL', 14, to_date('2016-04-12 13:22:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reversal', 249, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'TAXING_ENTITY', 7, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Taxing Entity', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'TA_NORM_SCHEMA', 4, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ta Norm Schema', 463, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'TA_VERSION', 1, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ta Version', 299, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'TRANSFER_IN', 15, to_date('2016-04-12 13:22:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfer In', 303, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'TRANSFER_OUT', 16, to_date('2016-04-12 13:22:39', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfer Out', 345, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_defer_drill_view'), 
'YEAR', 10, to_date('2016-04-12 13:22:22', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Year', 139, 0, 'Any', null, null, 0, null, null, 0) ; 

-- reg_fcst_ta_defer_drill_view
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'ADJUSTMENT', 17, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Adjustment', 303, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'BEG_BALANCE', 12, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'COMPANY', 2, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 253, 0, 'Upper', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'END_BALANCE', 18, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'GL_ACCOUNT', 8, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'GL Account', 308, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'GL_MONTH', 11, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'JE_TYPE', 5, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'JE Type', 226, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'M_ITEM', 3, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'M Item', 194, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'OPER_IND', 6, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Oper Ind', 244, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'OTHER', 19, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Other', 162, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'PAYMENTS', 20, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payments', 267, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'PROVISION', 13, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Provision', 258, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'REG_ACCOUNT', 9, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Account', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'REVERSAL', 14, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reversal', 249, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'TAXING_ENTITY', 7, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Taxing Entity', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'TA_NORM_SCHEMA', 4, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ta Norm Schema', 463, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'TA_VERSION', 1, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ta Version', 299, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'TRANSFER_IN', 15, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfer In', 303, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'TRANSFER_OUT', 16, to_date('2016-04-12 13:50:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfer Out', 345, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_defer_drill_view'), 
'YEAR', 10, to_date('2016-04-12 13:50:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Year', 139, 0, 'Any', null, null, 0, null, null, 0) ; 

-- reg_fcst_ta_def_drill_inc_view
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'AMOUNT_ACT', 13, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Act', 317, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP")
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'AMOUNT_ADJ', 16, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Adj', 313, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'AMOUNT_ALLOC', 17, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Alloc', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'AMOUNT_CALC', 14, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Calc', 345, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'AMOUNT_EST', 12, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Est', 313, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'AMOUNT_MANUAL', 15, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Manual', 418, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'BEG_BALANCE', 10, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'COMPANY', 4, to_date('2016-04-12 13:50:33', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 253, 0, 'Upper', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'END_BALANCE', 11, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'GL_MONTH', 9, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'M_ID', 1, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'M ID', 134, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'M_ITEM', 2, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'M Item', 194, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'OPER_IND', 5, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Oper Ind', 244, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'REG_ACCOUNT', 7, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Account', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'TAXING_ENTITY', 6, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Taxing Entity', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'TA_VERSION', 3, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ta Version', 299, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_fcst_ta_def_drill_inc_view'), 
'YEAR', 8, to_date('2016-04-12 13:50:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Year', 139, 0, 'Any', null, null, 0, null, null, 0) ; 

-- reg_hist_ta_def_drill_inc_view
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'AMOUNT_ACT', 13, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Act', 317, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'AMOUNT_ADJ', 16, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Adj', 313, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'AMOUNT_ALLOC', 17, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Alloc', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'AMOUNT_CALC', 14, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Calc', 345, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'AMOUNT_EST', 12, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Est', 313, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'AMOUNT_MANUAL', 15, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount Manual', 418, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'BEG_BALANCE', 10, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'COMPANY', 4, to_date('2016-04-12 13:37:38', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 253, 0, 'Upper', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'END_BALANCE', 11, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Balance', 349, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'GL_MONTH', 9, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'M_ID', 1, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'M ID', 134, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'M_ITEM', 2, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'M Item', 194, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'OPER_IND', 5, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Oper Ind', 244, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'REG_ACCOUNT', 7, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Account', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'TAXING_ENTITY', 6, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Taxing Entity', 358, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'TA_VERSION', 3, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ta Version', 299, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where lower(table_name) = 'reg_hist_ta_def_drill_inc_view'), 
'YEAR', 8, to_date('2016-04-12 13:37:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Year', 139, 0, 'Any', null, null, 0, null, null, 0) ; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3148, 0, 2015, 2, 2, 0, 45587, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045587_reg_2_prov_drillback_updates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;