/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009195_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.1   12/20/2011 Elhadj Bah     Point Release
||============================================================================
*/

drop table REPAIR_BLANKET_RESULTS;

create global temporary table REPAIR_BLANKET_RESULTS
(
 WORK_ORDER_ID             number(22,0),
 WORK_ORDER_NUMBER         varchar2(35),
 COMPANY_ID                number(22,0),
 REPAIR_UNIT_CODE_ID       number(22,0),
 ADD_QTY                   number(22,0),
 ADD_COST                  number(22,2),
 RETIRE_QTY                number(22,0),
 RETIRE_COST               number(22,2),
 BLANKET_METHOD            varchar2(35),
 EXPENSE_PCT               number(22,8),
 PER_SE_CAPITAL_PCT        number(22,8),
 CURR_ADDS_EXPENSE_AMOUNT  number(22,2),
 CURR_RETS_EXPENSE_AMOUNT  number(22,2),
 TOTAL_PERIOD_ADDS_EXPENSE number(22,2),
 TOTAL_PERIOD_RETS_EXPENSE number(22,2),
 ADDS_TAX_EXPENSE          number(22,2),
 RETS_TAX_EXPENSE          number(22,2)
) on commit preserve rows;

-- Let's modify the following columns so that the percentages work
alter table REPAIR_BLANKET_PROCESSING
   modify (PRORATA_THRESHOLD  number(22,8),
           PROP_THRESHOLD     number(22,8),
           COST_THRESHOLD     number(22,8),
           ALL_COST_THRESHOLD number(22,8));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (75, 0, 10, 3, 3, 1, 9195, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.1_maint_009195_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
