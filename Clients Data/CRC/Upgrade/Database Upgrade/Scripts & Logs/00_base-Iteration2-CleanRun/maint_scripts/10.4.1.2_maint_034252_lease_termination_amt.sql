/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034252_lease_termination_amt.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   12/02/2013 Ryan Oliveria  Need to track original/actual term amt separately
||============================================================================
*/

alter table LS_ASSET add ACTUAL_TERMINATION_AMOUNT number(22,2);

comment on column LS_ASSET.ACTUAL_TERMINATION_AMOUNT is 'The actual termination amount entered on retirement.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (778, 0, 10, 4, 1, 2, 34252, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_034252_lease_termination_amt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;