/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_039436_pwrtax_final_menu.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/10/2014 Andrew Sctt         Maint to finalize base gui menu for powertax.
||========================================================================================
*/

delete from ppbase_menu_items
where lower(module) = 'powertax';

delete from ppbase_workspace
where lower(module) = 'powertax';

insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_budget_additions','Additions','w_tax_depr_fcst_additions','Additions',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_budget_retirements','Retirements','w_tax_depr_fcst_retirements','Retirements',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_table_tax_limit','Tax Limits/Credits','w_tax_limit','Tax Limits/Credits',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_table_tax_rollup','Tax Rollup','w_tax_class_rollups','Tax Rollup',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_table_rates','Rates','w_tax_rate','Rates',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_table_conventions','Conventions','w_tax_conv_detail','Conventions',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_input_assign','Deferred Tax Assign','w_deferred_tax_schema','Deferred Tax Assign',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_setup_rates','Deferred Tax Rates','w_def_tax_rate_input','Deferred Tax Rates',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_setup_establish_groups','Establish Groups','w_establish_groups','Establish Groups',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_setup_grps_taxclass','Assign Groups to Tax Classes','w_assign_tax_class_groups','Assign Groups to Tax Classes',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_setup_grps_depr_grps','Assign Groups to Depr Groups','w_assign_book_account_groups_pp','Assign Groups to Depr Groups',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','reporting_query','Any Query','uo_tax_rpt_wksp_query','Any Query',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','reporting_verify','Verify','w_pp_verify2','Verify',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_iface_tax_loc','Assign Tax Locations','w_tax_location_change','Assign Tax Locations',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_iface_ret_unit','Assign Ret Unit Dist','w_retire_unit_tax_dist_assign','Assign RU Distinctions',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','admin_home','Home','uo_tax_admin_wksp_home','Home',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','admin_import_tool','Import Tool','uo_tax_admin_wksp_import','Import Tool',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','admin_systemoptions','System Options','uo_tax_admin_wksp_system_options','System Options',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','case_archive','Archive','w_tax_archive','Archive',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','case_manage_cases','Case Management','uo_tax_case_wksp_manage','Case Management',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_input_process_depr','Process Book Depr Alloc','w_tax_process_allocation_one_loop','Process Book Depreciation Allocation',2);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_normalization_schema','Normalization Schemas','uo_tax_dfit_wksp_normalization_schema','Normalization Schema',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_process','Processing','uo_tax_dfit_wksp_processing','Processing',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_review_tax_grid','Deferred Tax Grid','uo_tax_dfit_wksp_review','Deferred Tax Grid',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','deferred_setup_norm_schema','Normalization Schemas','uo_tax_dfit_wksp_normalization_schema','Normalization Schemas',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_book_additions','Additions','uo_tax_depr_act_wksp_intfc_adds','Additions',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_book_retirements','Retirements','uo_tax_depr_act_wksp_intfc_rets','Retirements',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_book_transfers','Transfers','uo_tax_depr_act_wksp_intfc_xfer','Transfers',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_iface_tax_book_xlate','Tax Book Translate','uo_tax_depr_act_wksp_translate','Tax Book Translate',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_iface_tax_depr_schema','Depreciation Schema','uo_tax_depr_act_wksp_intfc_depr','Tax Depreciation Schema',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_iface_tax_ret_rules','Tax Retire Rules','uo_tax_depr_act_wksp_retire_rules','Tax Retire Rules',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_mitem_alloc','M-Item Allocations','uo_tax_depr_act_wksp_mitem_alloc','M-Item Allocations',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_salvage_process','Process Salvage/COR','uo_tax_depr_act_wksp_prcs_salvage_cor','Process Salvage/COR',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_input_transfer_maint','Maintain Transfers','uo_tax_depr_act_wksp_pkg_search','Maintain Transfers',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_process','Processing','uo_tax_depr_wksp_processing','Processing',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','depr_review_asset_grid','Asset Grid','uo_tax_depr_asset_wksp_review','Asset Grid',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','forecast_manage','Forecast Management','uo_tax_fcst_wksp_manage','Forecat Management',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','forecast_process','Processing','uo_tax_fcst_wksp_processing','Processing',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','mlp_k1_export_gen','K1 Export Generation','uo_tax_mlp_wksp_k1_export_gen','K1 Export Generation',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','mlp_k1_export_search','K1 Export Search','uo_tax_mlp_wksp_k1_export_search','K1 Export Search',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','mlp_review_asset_grid','Asset Grid (MLP)','uo_tax_depr_asset_wksp_review','Asset Grid (MLP)',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','reporting_reports','Reports','uo_tax_rpt_wksp_reports','Reports',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','tax_control_input','Tax Control Input','uo_tax_depr_setup_wksp_tax_ctrl','Tax Control Input Grid',1);
insert into ppbase_workspace (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id) values ('powertax','tax_depr_act_load_salvage_cor','Load Salvage/COR','uo_tax_depr_act_wksp_load_salvage_cor','Load Salvage / COR',1);

insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','admin_home',1,1,'Home','','admin_home',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','client_extension',1,2,'Client Extension','','',0);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','case_management',1,3,'Case Management','','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','case_manage_cases',2,1,'Manage Cases','case_management','case_manage_cases',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','case_archive',2,2,'Archive','case_management','case_archive',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depreciation',1,4,'Depreciation','','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_review_asset_grid',2,1,'Asset Grid','depreciation','depr_review_asset_grid',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input',2,2,'Input Activity','depreciation','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_book',3,1,'Book Basis Activity','depr_input','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_book_additions',4,1,'Additions','depr_input_book','depr_input_book_additions',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_book_retirements',4,2,'Retirements','depr_input_book','depr_input_book_retirements',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_book_transfers',4,3,'Transfers','depr_input_book','depr_input_book_transfers',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_salvage',3,2,'Salvage/COR','depr_input','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_salvage_load',4,1,'Load Salvage/COR','depr_input_salvage','tax_depr_act_load_salvage_cor',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_salvage_process',4,2,'Process Salvage/COR','depr_input_salvage','depr_input_salvage_process',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_mitem_alloc',3,3,'M-Item Allocations','depr_input','depr_input_mitem_alloc',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_transfer_maint',3,4,'Maintain Transfers','depr_input','depr_input_transfer_maint',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_budget',3,5,'Budget Activity','depr_input','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_budget_additions',4,1,'Additions','depr_input_budget','depr_input_budget_additions',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_budget_retirements',4,2,'Retirements','depr_input_budget','depr_input_budget_retirements',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_process',2,3,'Processing','depreciation','depr_process',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_iface',3,7,'Interface Setup','depr_input','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_iface_tax_book_xlate',4,1,'Tax Book Translate','depr_input_iface','depr_input_iface_tax_book_xlate',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_iface_tax_ret_rules',4,2,'Tax Retire Rules','depr_input_iface','depr_input_iface_tax_ret_rules',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_iface_tax_depr_schema',4,3,'Depreciation Schema','depr_input_iface','depr_input_iface_tax_depr_schema',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_iface_tax_loc',4,4,'Assign Tax Locations','depr_input_iface','depr_input_iface_tax_loc',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_input_iface_ret_unit',4,5,'Assign Ret Unit Dist','depr_input_iface','depr_input_iface_ret_unit',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_table',2,4,'Standard Table Setup','depreciation','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_table_tax_control',3,1,'Tax Control','depr_table','tax_control_input',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_table_tax_limit',3,2,'Tax Limits/Credits','depr_table','depr_table_tax_limit',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_table_tax_rollup',3,3,'Tax Rollup','depr_table','depr_table_tax_rollup',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_table_rates',3,4,'Rates','depr_table','depr_table_rates',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','depr_table_conventions',3,5,'Conventions','depr_table','depr_table_conventions',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_taxes',1,5,'Deferred Taxes','','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_review_tax_grid',2,1,'Deferred Tax Grid','deferred_taxes','deferred_review_tax_grid',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_input',2,2,'Input Activity','deferred_taxes','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_input_load_depr',3,1,'Load Book Depreciation','deferred_input','tax_depr_act_load_salvage_cor',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_input_process_depr',3,2,'Process Book Depr Alloc','deferred_input','deferred_input_process_depr',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_input_assign',3,3,'Deferred Tax Assign','deferred_input','deferred_input_assign',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_process',2,3,'Processing','deferred_taxes','deferred_process',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_setup',2,4,'Setup','deferred_taxes','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_setup_norm_schema',3,1,'Normalization Schemas','deferred_setup','deferred_setup_norm_schema',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_setup_rates',3,2,'Deferred Tax Rates','deferred_setup','deferred_setup_rates',0);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_setup_alloc_groups',3,3,'Book Allocation Groups','deferred_setup','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_setup_establish_groups',4,1,'Establish Groups','deferred_setup_alloc_groups','deferred_setup_establish_groups',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_setup_grps_taxclass',4,2,'Groups to Tax Classes','deferred_setup_alloc_groups','deferred_setup_grps_taxclass',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','deferred_setup_grps_depr_grps',4,3,'Groups to Depr Groups','deferred_setup_alloc_groups','deferred_setup_grps_depr_grps',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','forecast',1,6,'Forecast - Run Out Existing','','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','forecast_manage',2,1,'Manage Forecasts','forecast','forecast_manage',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','forecast_process',2,2,'Processing','forecast','forecast_process',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','mlp',1,7,'MLP','','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','mlp_review_asset_grid',2,1,'Asset Grid','mlp','mlp_review_asset_grid',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','mlp_k1_export_gen',2,2,'K1 Export Generation','mlp','mlp_k1_export_gen',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','mlp_k1_export_search',2,3,'K1 Export Search','mlp','mlp_k1_export_search',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','reporting',1,8,'Reporting','','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','reporting_reports',2,1,'Reports','reporting','reporting_reports',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','reporting_query',2,2,'Any Query','reporting','reporting_query',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','reporting_verify',2,3,'Verify','reporting','reporting_verify',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','admin',1,9,'Admin','','',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','admin_import_tool',2,1,'Import Tool','admin','admin_import_tool',1);
insert into ppbase_menu_items (module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn) values ('powertax','admin_systemoptions',2,2,'System Options','admin','admin_systemoptions',1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1420, 0, 10, 4, 3, 0, 39436, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039436_pwrtax_final_menu.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
