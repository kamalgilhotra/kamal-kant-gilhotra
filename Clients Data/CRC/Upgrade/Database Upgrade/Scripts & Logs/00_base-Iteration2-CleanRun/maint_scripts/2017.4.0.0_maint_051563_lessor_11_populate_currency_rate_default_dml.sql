/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051563_lessor_11_populate_currency_rate_default_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/12/2017 Jared Watkins    populate currency_rate_default with all of the average exchange rates we will
||                                        need for multicurrency, including default 0/1 rates
||============================================================================
*/

insert into currency_rate_default(exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
(SELECT exchange_date, currency_from, currency_to, exchange_rate_type_id, rate
  FROM currency_rate a
  WHERE exchange_rate_type_id = 4  --only look at 'Average' exchange rates
  AND currency_from <> currency_to --eliminate translations from one currency to the same currency since that's taken care of below
  AND not exists (select 1 from currency_rate_default b where a.currency_from = b.currency_from and a.currency_to = b.currency_to and a.exchange_date = b.exchange_date and a.exchange_rate_type_id = b.exchange_rate_type_id) --prevent any issues for average rates already manually loaded into the default table
    UNION
  SELECT To_Date('190001','yyyymm'), currency_id, currency_id, 4, 1
  FROM currency                    --add exchange rates with a default rate of 1 from every currency to itself
    UNION
  SELECT To_Date('190001', 'yyyymm'), a.currency_id, b.currency_id, 4, 0
  FROM currency a, currency b      --add default 0 exchange rates for every combination of currencies in the system in case there are no real rates
  WHERE a.currency_id <> b.currency_id
);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8006, 0, 2017, 4, 0, 0, 51563, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051563_lessor_11_populate_currency_rate_default_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;