/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_system_autocreate.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/05/2013 Stephen Wicks  Point Release
||============================================================================
*/

-- add autocreate_import_type_id to pp_import_column
alter table PWRPLANT.PP_IMPORT_COLUMN add AUTOCREATE_IMPORT_TYPE_ID number(22,0);

alter table PWRPLANT.PP_IMPORT_COLUMN
   add constraint PP_IMPT_COL_AUTOCREATE_TYPE_FK
       foreign key (AUTOCREATE_IMPORT_TYPE_ID)
       references PP_IMPORT_TYPE;

-- drop allow_autocreate and autocreate_table_name from pp_import_column
alter table PWRPLANT.PP_IMPORT_COLUMN drop column ALLOW_AUTOCREATE;
alter table PWRPLANT.PP_IMPORT_COLUMN drop column AUTOCREATE_TABLE_NAME;

-- add has autocreate_import_template_id to pp_import_template_fields
alter table PWRPLANT.PP_IMPORT_TEMPLATE_FIELDS add AUTOCREATE_IMPORT_TEMPLATE_ID number(22,0);

alter table PWRPLANT.PP_IMPORT_TEMPLATE_FIELDS
   add constraint PP_IMPT_TEMP_FLDS_AUTO_TEMP_FK
       foreign key (AUTOCREATE_IMPORT_TEMPLATE_ID)
       references PP_IMPORT_TEMPLATE;

-- add derived template and field to pp_import_template_fields
alter table PWRPLANT.PP_IMPORT_TEMPLATE_FIELDS add DERIVE_IMPORT_TEMPLATE_ID number(22,0);
alter table PWRPLANT.PP_IMPORT_TEMPLATE_FIELDS add DERIVE_IMPORT_FIELD_ID number(22,0);

alter table PWRPLANT.PP_IMPORT_TEMPLATE_FIELDS
   add constraint PP_IMPT_TEMP_FLDS_DERV_FLD_FK
       foreign key (DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
       references PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID);

-- drop autocreate_yn from pp_import_template_fields
alter table PWRPLANT.PP_IMPORT_TEMPLATE_FIELDS drop column AUTOCREATE_YN;

-- add is_autocreate_template to pp_import_template
alter table PWRPLANT.PP_IMPORT_TEMPLATE add IS_AUTOCREATE_TEMPLATE number(22,0);

-- update property tax imports with the autocreate_import_type_id
-- Fields on asset location import
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 35 where IMPORT_TYPE_ID = 37 and COLUMN_NAME = 'major_location_id';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 32 where IMPORT_TYPE_ID = 37 and COLUMN_NAME = 'minor_location_id';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 32 where IMPORT_TYPE_ID = 37 and COLUMN_NAME = 'minor_location_id2';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 36 where IMPORT_TYPE_ID = 37 and COLUMN_NAME = 'prop_tax_location_id';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 31 where IMPORT_TYPE_ID = 37 and COLUMN_NAME = 'tax_district_id';

-- Fields on tax district import
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 29 where IMPORT_TYPE_ID = 31 and COLUMN_NAME = 'assessor_id';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 30 where IMPORT_TYPE_ID = 31 and COLUMN_NAME = 'type_code_id';

-- Fields on major location import
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 34 where IMPORT_TYPE_ID = 35 and COLUMN_NAME = 'division_id';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 33 where IMPORT_TYPE_ID = 35 and COLUMN_NAME = 'location_type_id';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 39 where IMPORT_TYPE_ID = 35 and COLUMN_NAME = 'municipality_id';

-- Fields on asset import
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 23 where IMPORT_TYPE_ID = 25 and COLUMN_NAME = 'retirement_unit_id';
update PWRPLANT.PP_IMPORT_COLUMN set AUTOCREATE_IMPORT_TYPE_ID = 24 where IMPORT_TYPE_ID = 25 and COLUMN_NAME = 'utility_account_id';

-- add autocreate_description to import type to differentiate fields belonging to an autocreate template in the template window.
alter table PWRPLANT.PP_IMPORT_TYPE add AUTOCREATE_DESCRIPTION varchar2(35);

-- populate autocreate_description for all existing import types.
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Ledger Item' where IMPORT_TYPE_ID = 1;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Statistic, Full' where IMPORT_TYPE_ID = 2;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Statistic, Incremental' where IMPORT_TYPE_ID = 3;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Escalated Value Indice' where IMPORT_TYPE_ID = 4;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Levy Rate' where IMPORT_TYPE_ID = 5;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel' where IMPORT_TYPE_ID = 6;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel' where IMPORT_TYPE_ID = 7;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Ledger Item' where IMPORT_TYPE_ID = 8;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Preallo Ledger Item' where IMPORT_TYPE_ID = 9;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Preallo Ledger Item' where IMPORT_TYPE_ID = 10;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Assessment, Parcel' where IMPORT_TYPE_ID = 11;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'CWIP Type Assignment' where IMPORT_TYPE_ID = 12;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Authority-District Combo' where IMPORT_TYPE_ID = 13;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel Responsible Entity' where IMPORT_TYPE_ID = 14;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel Responsibility' where IMPORT_TYPE_ID = 15;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel History' where IMPORT_TYPE_ID = 16;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Variable Value' where IMPORT_TYPE_ID = 17;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel Geography' where IMPORT_TYPE_ID = 18;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel Geography Type Factor' where IMPORT_TYPE_ID = 19;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Bill' where IMPORT_TYPE_ID = 20;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Bill Line' where IMPORT_TYPE_ID = 21;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Bill Line' where IMPORT_TYPE_ID = 22;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Retirement Unit' where IMPORT_TYPE_ID = 23;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Utility Account' where IMPORT_TYPE_ID = 24;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Asset' where IMPORT_TYPE_ID = 25;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Levy Rate, Composite' where IMPORT_TYPE_ID = 26;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Class Code' where IMPORT_TYPE_ID = 27;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Reserve Factor Percent' where IMPORT_TYPE_ID = 28;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Assessor' where IMPORT_TYPE_ID = 29;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Type Code' where IMPORT_TYPE_ID = 30;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Tax District' where IMPORT_TYPE_ID = 31;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Minor Location' where IMPORT_TYPE_ID = 32;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Location Type' where IMPORT_TYPE_ID = 33;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Division' where IMPORT_TYPE_ID = 34;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Major Location' where IMPORT_TYPE_ID = 35;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Prop Tax Location' where IMPORT_TYPE_ID = 36;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Asset Location' where IMPORT_TYPE_ID = 37;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Parcel-Location Mapping' where IMPORT_TYPE_ID = 38;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Municipality' where IMPORT_TYPE_ID = 39;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Asset Location' where IMPORT_TYPE_ID = 40;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Depr Ledger History' where IMPORT_TYPE_ID = 50;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'CPR Depr History' where IMPORT_TYPE_ID = 51;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'Depr Rate' where IMPORT_TYPE_ID = 52;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'CPR Asset History, New' where IMPORT_TYPE_ID = 53;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'CPR Asset Activity, Existing' where IMPORT_TYPE_ID = 54;
update PWRPLANT.PP_IMPORT_TYPE set AUTOCREATE_DESCRIPTION = 'CPR Class Code' where IMPORT_TYPE_ID = 55;

-- add a new field for derived lookups to indicate if they'll be created automatically if they're not translated
alter table PWRPLANT.PP_IMPORT_LOOKUP add DERIVED_AUTOCREATE_YN number(22,0);
update PWRPLANT.PP_IMPORT_LOOKUP set DERIVED_AUTOCREATE_YN = 0 where IS_DERIVED = 1;
update PWRPLANT.PP_IMPORT_LOOKUP set DERIVED_AUTOCREATE_YN = 1 where IMPORT_LOOKUP_ID = 184;
update PWRPLANT.PP_IMPORT_COLUMN set PROCESSING_ORDER = 4 where IMPORT_TYPE_ID in ( 21, 22) and COLUMN_NAME = 'tax_authority_id';

-- Remove import type 40 (Asset Locations Master).  This has been replaced with the ability to autocreate from the regular Asset Locations import.
delete from PWRPLANT.PP_IMPORT_TEMPLATE_FIELDS where IMPORT_TYPE_ID = 40;
delete from PWRPLANT.PP_IMPORT_TEMPLATE_EDITS where IMPORT_TEMPLATE_ID in (select IMPORT_TEMPLATE_ID from PWRPLANT.PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 40);
delete from PWRPLANT.PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 40;
delete from PWRPLANT.PP_IMPORT_TYPE_UPDATES_LOOKUP where IMPORT_TYPE_ID = 40;
delete from PWRPLANT.PP_IMPORT_COLUMN_LOOKUP where IMPORT_TYPE_ID = 40;
delete from PWRPLANT.PP_IMPORT_COLUMN where IMPORT_TYPE_ID = 40;
delete from PWRPLANT.PP_IMPORT_TYPE_SUBSYSTEM where IMPORT_TYPE_ID = 40;
delete from PWRPLANT.PP_IMPORT_TYPE where IMPORT_TYPE_ID = 40;

-- create pp_import_file
create table PWRPLANT.PP_IMPORT_FILE
(
 IMPORT_RUN_ID number(22,0) not null,
 LINE_ID       number(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 COL1          varchar2(4000),
 COL2          varchar2(4000),
 COL3          varchar2(4000),
 COL4          varchar2(4000),
 COL5          varchar2(4000),
 COL6          varchar2(4000),
 COL7          varchar2(4000),
 COL8          varchar2(4000),
 COL9          varchar2(4000),
 COL10         varchar2(4000),
 COL11         varchar2(4000),
 COL12         varchar2(4000),
 COL13         varchar2(4000),
 COL14         varchar2(4000),
 COL15         varchar2(4000),
 COL16         varchar2(4000),
 COL17         varchar2(4000),
 COL18         varchar2(4000),
 COL19         varchar2(4000),
 COL20         varchar2(4000),
 COL21         varchar2(4000),
 COL22         varchar2(4000),
 COL23         varchar2(4000),
 COL24         varchar2(4000),
 COL25         varchar2(4000),
 COL26         varchar2(4000),
 COL27         varchar2(4000),
 COL28         varchar2(4000),
 COL29         varchar2(4000),
 COL30         varchar2(4000),
 COL31         varchar2(4000),
 COL32         varchar2(4000),
 COL33         varchar2(4000),
 COL34         varchar2(4000),
 COL35         varchar2(4000),
 COL36         varchar2(4000),
 COL37         varchar2(4000),
 COL38         varchar2(4000),
 COL39         varchar2(4000),
 COL40         varchar2(4000),
 COL41         varchar2(4000),
 COL42         varchar2(4000),
 COL43         varchar2(4000),
 COL44         varchar2(4000),
 COL45         varchar2(4000),
 COL46         varchar2(4000),
 COL47         varchar2(4000),
 COL48         varchar2(4000),
 COL49         varchar2(4000),
 COL50         varchar2(4000),
 COL51         varchar2(4000),
 COL52         varchar2(4000),
 COL53         varchar2(4000),
 COL54         varchar2(4000),
 COL55         varchar2(4000),
 COL56         varchar2(4000),
 COL57         varchar2(4000),
 COL58         varchar2(4000),
 COL59         varchar2(4000),
 COL60         varchar2(4000),
 COL61         varchar2(4000),
 COL62         varchar2(4000),
 COL63         varchar2(4000),
 COL64         varchar2(4000),
 COL65         varchar2(4000),
 COL66         varchar2(4000),
 COL67         varchar2(4000),
 COL68         varchar2(4000),
 COL69         varchar2(4000),
 COL70         varchar2(4000),
 COL71         varchar2(4000),
 COL72         varchar2(4000),
 COL73         varchar2(4000),
 COL74         varchar2(4000),
 COL75         varchar2(4000),
 COL76         varchar2(4000),
 COL77         varchar2(4000),
 COL78         varchar2(4000),
 COL79         varchar2(4000),
 COL80         varchar2(4000),
 COL81         varchar2(4000),
 COL82         varchar2(4000),
 COL83         varchar2(4000),
 COL84         varchar2(4000),
 COL85         varchar2(4000),
 COL86         varchar2(4000),
 COL87         varchar2(4000),
 COL88         varchar2(4000),
 COL89         varchar2(4000),
 COL90         varchar2(4000),
 COL91         varchar2(4000),
 COL92         varchar2(4000),
 COL93         varchar2(4000),
 COL94         varchar2(4000),
 COL95         varchar2(4000),
 COL96         varchar2(4000),
 COL97         varchar2(4000),
 COL98         varchar2(4000),
 COL99         varchar2(4000),
 COL100        varchar2(4000),
 ERROR_MESSAGE varchar2(4000)
);

alter table PP_IMPORT_FILE
   add constraint PP_IMPORT_FILE_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table PP_IMPORT_FILE
   add constraint PP_IMPORT_FILE_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

-- create pp_temp_import_file_lines
create global temporary table PWRPLANT.PP_TEMP_IMPORT_FILE_LINES
(
 IMPORT_TEMPLATE_ID number(22,0),
 FILE_LINE_ID       number(22,0),
 STAGING_LINE_ID    number(22,0)
) on commit preserve rows;

create unique index PWRPLANT.PP_TEMP_IMPORT_FILE_LINES
   on PWRPLANT.PP_TEMP_IMPORT_FILE_LINES (IMPORT_TEMPLATE_ID, FILE_LINE_ID, STAGING_LINE_ID);

begin
   DBMS_STATS.SET_TABLE_STATS('PWRPLANT', 'PP_TEMP_IMPORT_FILE_LINES', '', NULL, NULL, 1, 1, 36, NULL);
   DBMS_STATS.LOCK_TABLE_STATS('PWRPLANT', 'PP_TEMP_IMPORT_FILE_LINES');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (290, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_system_autocreate.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
