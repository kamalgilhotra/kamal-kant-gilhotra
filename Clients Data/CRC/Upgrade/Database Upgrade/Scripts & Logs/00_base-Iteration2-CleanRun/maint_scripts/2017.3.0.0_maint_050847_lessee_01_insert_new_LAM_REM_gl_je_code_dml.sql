/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_050847_lessee_01_insert_new_LAM_REM_gl_je_code_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.3.0.0 04/04/2018   K. Powers    Add new GL JE Code for Lease Remeasurement
 ||============================================================================
 */

insert into STANDARD_JOURNAL_ENTRIES
(JE_ID,GL_JE_CODE, EXTERNAL_JE_CODE,DESCRIPTION,LONG_DESCRIPTION)
values
((select nvl(max(je_id) + 1,1) from standard_journal_entries ),'LEASEREM','LEASEREM','LAM Remeasurement','Leased Asset Remeasurements');

insert into GL_JE_CONTROL
(process_id,je_id,dr_table,dr_column,cr_table,cr_column)
values
('LAM REM', (select nvl(max(je_id),1) from standard_journal_entries) ,'NONE','NONE','NONE','NONE');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4306, 0, 2017, 3, 0, 0, 50847, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050847_lessee_01_insert_new_LAM_REM_gl_je_code_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;