/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_050824_01_lessor_remove_initial_var_payment_value_column_ddl.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2017.3.0.0 04/27/2018  Andrew Hill      Remove initial_var_payment_value from lsr_ilr_payment_term_var_pay (use function for calculation at month-end instead)
||============================================================================
*/

ALTER TABLE lsr_ilr_payment_term_var_pay DROP COLUMN initial_var_payment_value;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4842, 0, 2017, 3, 0, 0, 50824, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050824_01_lessor_remove_initial_var_payment_value_column_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;