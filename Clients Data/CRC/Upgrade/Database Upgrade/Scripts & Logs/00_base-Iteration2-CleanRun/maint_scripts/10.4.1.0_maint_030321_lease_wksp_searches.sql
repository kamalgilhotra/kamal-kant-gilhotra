/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030321_lease_wksp_searches.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/28/2013 Kyle Peterson  Point release
||============================================================================
*/

delete from PPBASE_MENU_ITEMS where MODULE = 'LESSEE';
delete from PPBASE_WORKSPACE where MODULE = 'LESSEE';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'menu_wksp_initiate', 'Initiate', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'initiate_mla', 'MLA', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'initiate_asset', 'Asset', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'initiate_ilr', 'ILR', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'initiate_ilr_new', 'New', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'initiate_ilr_from_assets', 'from Assets', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'menu_wksp_details', 'Details', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'details_mla', 'MLA', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'details_asset', 'Asset', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'details_ilr', 'ILR', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'details_schedules', 'Schedules', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'menu_wksp_control', 'Monthly Control', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'control_payments', 'Payments', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'control_invoices', 'Invoices', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'control_monthend', 'Month End', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'menu_wksp_approval', 'Approvals', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'approval_mla', 'MLA', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'approval_assets', 'Assets and Schedules', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'approval_trans_ret', 'Transfers and Retirements', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'approval_payments', 'Payments', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'menu_wksp_reports', 'Reports', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'menu_wksp_admin', 'Admin', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'admin_lessors', 'Lessors', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'admin_vendors', 'Vendors', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'admin_taxes', 'Taxes', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'admin_system_controls', 'System Controls', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'menu_wksp_import', 'Import Tools', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'import_mla', 'MLA', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'import_assets', 'Assets', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'import_lessors', 'Lessors', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'import_invoices', 'Invoices', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'search_mla', 'MLA', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'search_asset', 'Asset', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'search_ilr', 'ILR', ' ');
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'search_schedules', 'Schedules', ' ');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'menu_wksp_initiate', null, 1, 'Initiate', 1, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'initiate_mla', 'menu_wksp_initiate', 1, 'MLA', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'initiate_asset', 'menu_wksp_initiate', 2, 'Asset', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'initiate_ilr', 'menu_wksp_initiate', 3, 'ILR', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'initiate_ilr_new', 'initiate_ilr', 1, 'New', 1, 3);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'initiate_ilr_from_assets', 'initiate_ilr', 2, 'from Assets', 1, 3);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'menu_wksp_details', null, 2, 'Details', 1, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'search_mla', 'menu_wksp_details', 1, 'MLA', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'search_asset', 'menu_wksp_details', 2, 'Asset', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'search_ilr', 'menu_wksp_details', 3, 'ILR', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'search_schedules', 'menu_wksp_details', 4, 'Schedules', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'menu_wksp_control', null, 4, 'Monthly Control', 1, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'control_payments', 'menu_wksp_control', 1, 'Payments', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'control_invoices', 'menu_wksp_control', 2, 'Invoices', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'control_monthend', 'menu_wksp_control', 3, 'Month End', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'menu_wksp_approval', null, 3, 'Approvals', 1, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'approval_mla', 'menu_wksp_approval', 1, 'MLA', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'approval_assets', 'menu_wksp_approval', 2, 'Assets and Schedules', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'approval_trans_ret', 'menu_wksp_approval', 3, 'Transfers and Retirements', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'approval_payments', 'menu_wksp_approval', 4, 'Payments', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'menu_wksp_reports', null, 5, 'Reports', 1, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'menu_wksp_admin', null, 6, 'Admin', 1, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'admin_lessors', 'menu_wksp_admin', 1, 'Lessors', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'admin_vendors', 'menu_wksp_admin', 2, 'Vendors', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'admin_taxes', 'menu_wksp_admin', 3, 'Taxes', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'admin_system_controls', 'menu_wksp_admin', 4, 'System Controls', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'menu_wksp_import', null, 7, 'Import Tools', 1, 1);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'import_mla', 'menu_wksp_import', 1, 'MLA', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'import_assets', 'menu_wksp_import', 2, 'Assets', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'import_lessors', 'menu_wksp_import', 3, 'Lessors', 1, 2);
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, PARENT_MENU_IDENTIFIER, ITEM_ORDER, LABEL, ENABLE_YN, MENU_LEVEL)
values
   ('LESSEE', 'import_invoices', 'menu_wksp_import', 4, 'Invoices', 1, 2);

update PPBASE_MENU_ITEMS set WORKSPACE_IDENTIFIER = MENU_IDENTIFIER where MODULE = 'LESSEE';

update PPBASE_MENU_ITEMS set WORKSPACE_IDENTIFIER = '' where PARENT_MENU_IDENTIFIER is null AND module = 'LESSEE';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_assetcntr_wksp_search'
 where WORKSPACE_IDENTIFIER = 'search_asset'
 and MODULE = 'LESSEE';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_assetcntr_wksp_details'
 where WORKSPACE_IDENTIFIER = 'initiate_asset'
 and MODULE = 'LESSEE';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_lscntr_wksp_search'
 where WORKSPACE_IDENTIFIER = 'search_mla'
 and MODULE = 'LESSEE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (421, 0, 10, 4, 1, 0, 30321, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030321_lease_wksp_searches.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
