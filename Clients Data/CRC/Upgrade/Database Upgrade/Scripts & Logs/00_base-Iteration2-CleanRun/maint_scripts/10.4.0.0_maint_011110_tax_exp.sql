/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011110_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   09/17/2012 Sunjin Cone    Point Release
||============================================================================
*/

update PP_REPORTS
   set SPECIAL_NOTE = 'REPAIRS,ALREADY HAS REPORT TIME', DESCRIPTION = 'Annual Additions',
       LONG_DESCRIPTION = 'Annual CPR Additions and Tax Expense Activity for a Time Span for work orders with tax expense amount <> 0.  Time Span selection should include at least a two year span.'
 where DATAWINDOW = 'dw_repairs_106_101_activity_report';

update PP_REPORTS
   set SPECIAL_NOTE = 'REPAIRS,ALREADY HAS REPORT TIME', DESCRIPTION = 'Annual Additions by Batch',
       LONG_DESCRIPTION = 'Annual CPR Additions and Tax Expense Activity for Prior Years in a Tax Repairs Batch for work orders tax expense amount <> 0 (Only Posted status batches will show Tax Expense dollars in the CPR Tax Expense column.)'
 where DATAWINDOW = 'dw_repairs_106_101_batch_report';

update PP_REPORTS
   set SPECIAL_NOTE = 'REPAIRS,ALREADY HAS REPORT TIME',
       DESCRIPTION = 'Annual Additions by Tax Exp Test',
       LONG_DESCRIPTION = 'Annual CPR Additions and Tax Expense Activity for WOs Assigned to Tax Repairs Test for work orders with tax expense amount <> 0.  Time Span selection should include at least a two year span.'
 where DATAWINDOW = 'dw_repairs_106_101_taxtest_report';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (217, 0, 10, 4, 0, 0, 11110, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011110_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
