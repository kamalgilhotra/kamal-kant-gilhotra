/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032432_lease_payshift.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Brandon Beck   Point Release
||============================================================================
*/

alter table LS_ILR_GROUP   add PAYMENT_SHIFT number(22,0);
update LS_ILR_GROUP   set PAYMENT_SHIFT = 0 where PAYMENT_SHIFT is null;

alter table LS_ILR_OPTIONS add PAYMENT_SHIFT number(22,0);
update LS_ILR_OPTIONS set PAYMENT_SHIFT = 0 where PAYMENT_SHIFT is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (635, 0, 10, 4, 1, 1, 32432, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032432_lease_payshift.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
