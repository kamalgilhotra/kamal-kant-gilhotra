/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051029_lessor_04_add_mec_config_tables_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.1 04/27/2018 Crystal Yura     Add Lessor MEC Config Tables to Tables List
||============================================================================
*/
INSERT INTO "POWERPLANT_TABLES" ( "DESCRIPTION", "TABLE_NAME", "PP_TABLE_TYPE_ID", "SUBSYSTEM_SCREEN", "LONG_DESCRIPTION" ) VALUES ( 'Lessor Month End Configuration', 'lsr_mec_config', 's', 'always', 'Lessor Month End Configuration' );

INSERT INTO "POWERPLANT_COLUMNS" ( "COLUMN_NAME", "TABLE_NAME", "PP_EDIT_TYPE_ID", "DESCRIPTION", "COLUMN_RANK","DROPDOWN_NAME" ) VALUES ( 'accruals', 'lsr_mec_config', 'p', 'accruals', 3, 'yes_no' );

INSERT INTO "POWERPLANT_COLUMNS" ( "COLUMN_NAME", "TABLE_NAME", "PP_EDIT_TYPE_ID", "DESCRIPTION", "COLUMN_RANK","DROPDOWN_NAME" ) VALUES ( 'auto_termination', 'lsr_mec_config', 'p', 'auto termination', 2, 'yes_no' );

INSERT INTO "POWERPLANT_COLUMNS" ( "COLUMN_NAME", "TABLE_NAME", "PP_EDIT_TYPE_ID", "DESCRIPTION", "COLUMN_RANK","DROPDOWN_NAME" ) VALUES ( 'company_id', 'lsr_mec_config', 'p', 'company id', 1, 'company' );

INSERT INTO "POWERPLANT_COLUMNS" ( "COLUMN_NAME", "TABLE_NAME", "PP_EDIT_TYPE_ID", "DESCRIPTION", "COLUMN_RANK","DROPDOWN_NAME" ) VALUES ( 'currency_gain_loss', 'lsr_mec_config', 'p', 'currency gain loss', 4, 'yes_no' );

INSERT INTO "POWERPLANT_COLUMNS" ( "COLUMN_NAME", "TABLE_NAME", "PP_EDIT_TYPE_ID", "DESCRIPTION", "COLUMN_RANK","DROPDOWN_NAME" ) VALUES ( 'invoices', 'lsr_mec_config', 'p', 'invoices', 5, 'yes_no' );

INSERT INTO "POWERPLANT_COLUMNS" ( "COLUMN_NAME", "TABLE_NAME", "PP_EDIT_TYPE_ID", "DESCRIPTION", "COLUMN_RANK" ) VALUES ( 'time_stamp', 'lsr_mec_config', 'e', 'time stamp', 100 );

INSERT INTO "POWERPLANT_COLUMNS" ( "COLUMN_NAME", "TABLE_NAME", "PP_EDIT_TYPE_ID", "DESCRIPTION", "COLUMN_RANK" ) VALUES ( 'user_id', 'lsr_mec_config', 'e', 'user id', 101 );


insert into pp_table_audits (table_name,column_name,trigger_name,action, subsystem, use_pk_lookup, user_id, timestamp)
select table_name,
column_name, 
'PP_AU_LSR_MEC_CONFIG', 
'IUD',  
'LEASE',  
0,
'PWRPLANT',
sysdate
from sys.all_tab_columns where table_name = 'LSR_MEC_CONFIG' and column_name not in ('USER_ID', 'TIME_STAMP');



declare 
v_count number;
begin
select count(1) into v_count 
from pp_table_groups;
if v_count =0 then 
  return;
else 
insert into pp_table_groups
(groups, table_name)
values 
('system','lsr_mec_config');
end if;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6845, 0, 2017, 3, 0, 1, 51029, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051029_lessor_04_add_mec_config_tables_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

