/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043802_taxrpr_loc_rollup_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 05/05/2015 Alex P.          Repair Location Rollup Import Template
||============================================================================
*/

delete from pp_import_template_fields 
where import_template_id = 1021 
	and field_id = 3 
	and column_name = 'work_order_id';
	
delete from pp_import_template_fields 
where import_template_id = 1020 
	and field_id = 4 
	and column_name = 'work_order_id';
	
delete from pp_import_template_fields 
where import_template_id = 1020 
	and field_id = 3 
	and column_name = 'company_id';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2552, 0, 2015, 1, 0, 0, 043802, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043802_taxrpr_loc_rollup_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;