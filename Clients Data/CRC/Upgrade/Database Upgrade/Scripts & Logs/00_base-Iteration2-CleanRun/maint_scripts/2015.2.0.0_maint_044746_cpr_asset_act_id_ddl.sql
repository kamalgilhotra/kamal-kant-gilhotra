/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044746_CPR_asset_act_id_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2.0   08/19/2015 Sam Leach        Performance issue when assinging asset
||                                        activity ID's
||============================================================================
*/

create table cpr_ldg_act_import_temp (
	import_run_id number(22, 0),
	line_id number(22,0),
	id_field1 number(22,0),
	id_field2 number(22,0),
	desc_field1 varchar2(2000),
	desc_field2 varchar2(2000)
);
ALTER TABLE PWRPLANT.cpr_ldg_act_import_temp ADD ( CONSTRAINT pkcprldg_act_impt_temp PRIMARY KEY ( "IMPORT_RUN_ID", "LINE_ID" )) ;

CREATE INDEX "ACTCODE_DESC" ON PWRPLANT."ACTIVITY_CODE" (TRIM(UPPER("DESCRIPTION")) ) ;

comment on table cpr_ldg_act_import_temp is '(T) [01] The CPR Ledger Activity Import Temp table is a temporary table used to store data when assigning asset activity ID to improve performace with the CPR loader.';
comment on column cpr_ldg_act_import_temp.import_run_id is 'Standard system-assigned key reference PP_IMPORT_RUN table (taken from cpr_ledger_act_import_stg table).';
comment on column cpr_ldg_act_import_temp.line_id is 'Sequential next value for this Process run (taken from cpr_ledger_act_import_stg table).';
comment on column cpr_ldg_act_import_temp.id_field1 is 'Asset activity ID that needs to be inserted into cpr_ledger_act_import_stg table is populated here.';
comment on column cpr_ldg_act_import_temp.id_field2 is 'Optional asset activity ID that can be inserted into cpr_ledger_act_import_stg table is populated here.';
comment on column cpr_ldg_act_import_temp.desc_field1 is 'Description of asset activity corresponding to id_field1.';
comment on column cpr_ldg_act_import_temp.desc_field2 is 'Description of asset activity corresponding to id_field2.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2814, 0, 2015, 2, 0, 0, 044746, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044746_CPR_asset_act_id_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;