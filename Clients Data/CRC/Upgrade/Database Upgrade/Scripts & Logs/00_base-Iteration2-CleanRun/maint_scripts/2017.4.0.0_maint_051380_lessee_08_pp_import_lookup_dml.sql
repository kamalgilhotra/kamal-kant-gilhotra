/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051380_lessee_08_pp_import_lookup_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/30/2018  Alex Healey    Create new import lookup column setup for purchase options type, add lookups using new type
||============================================================================
*/

INSERT INTO
       pp_import_lookup
        (
                import_lookup_id           ,
                time_stamp                 ,
                user_id                    ,
                description                ,
                long_description           ,
                column_name                ,
                lookup_sql                 ,
                is_derived                 ,
                lookup_table_name          ,
                lookup_column_name         ,
                lookup_constraining_columns,
                lookup_values_alternate_sql,
                derived_autocreate_yn
        )
        VALUES
        (
                1117                                    ,
                SYSDATE                                 ,
                USER                                    ,
                'Ls ILR purchase option type'           ,
                'The type of purchase option for an ILR',
                'purchase_option_type'                  ,
                '(SELECT DISTINCT (b.description) FROM    ls_purchase_option_type b WHERE Upper(Trim(<importfield>)) = upper( trim(b.purchase_option_id)))',
				0,
                'ls_purchase_option_type',
                'purchase_option_type_id',
                null                   ,
                null                   ,
                NULL
        );
		
--Now that we've inserted the new lookup column, we can now use it for the purchase option type column's lookup
INSERT INTO
        pp_import_column_lookup
        (
                import_type_id  ,
                column_name     ,
                import_lookup_id,
                time_stamp      ,
                user_id
        )
        VALUES
        (
                268                   ,
                'ilr_id',
                1096                  ,
                SYSDATE               ,
                USER
        );

INSERT INTO
        pp_import_column_lookup
        (
                import_type_id  ,
                column_name     ,
                import_lookup_id,
                time_stamp      ,
                user_id
        )
        VALUES
        (
                268                          ,
                'ilr_purchase_probability_id',
                1086                         ,
                SYSDATE                      ,
                USER
        );

INSERT INTO
        pp_import_column_lookup
        (
                import_type_id  ,
                column_name     ,
                import_lookup_id,
                time_stamp      ,
                user_id
        )
        VALUES
        (
                268                   ,
                'purchase_option_type',
                1117                  ,
                SYSDATE               ,
                USER
        );	

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6084, 0, 2017, 4, 0, 0, 51380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051380_lessee_08_pp_import_lookup_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	