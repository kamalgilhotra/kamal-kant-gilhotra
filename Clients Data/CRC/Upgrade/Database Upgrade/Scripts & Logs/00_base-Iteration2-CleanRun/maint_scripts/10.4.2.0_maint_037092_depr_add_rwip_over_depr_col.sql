SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037092_depr_add_rwip_over_depr_col.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 02/24/2014 Charlie Shilling maint-36045 - add rounding plug columns
||============================================================================
*/

declare
   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                   V_COL_NAME   varchar2,
                   V_DATATYPE   varchar2,
                   V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                       V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                         V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                               V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                 'Could not add column ' || V_COL_NAME || ' to ' ||
                                 V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                       V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
                         ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                              'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                              V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin
   --add rwip_in_over_depr to normal staging tables and combined staging tables
   ADD_COLUMN('depr_calc_stg',
            'rwip_in_over_depr',
            'number(1,0) default 0',
            'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
   ADD_COLUMN('depr_calc_stg_arc',
            'rwip_in_over_depr',
            'number(1,0) default 0',
            'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
            'rwip_in_over_depr',
            'number(1,0) default 0',
            'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
   ADD_COLUMN('depr_calc_combined_stg',
            'rwip_in_over_depr',
            'number(1,0) default 0',
            'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
   ADD_COLUMN('depr_calc_combined_arc',
            'rwip_in_over_depr',
            'number(1,0) default 0',
            'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');
   ADD_COLUMN('fcst_depr_calc_combined_arc',
            'rwip_in_over_depr',
            'number(1,0) default 0',
            'Numeric representation of the "Include RWIP in Over Depr Check" system control. 0 = No, 1 = Yes');

   --add rwip_allocation to combined stg
   ADD_COLUMN('depr_calc_combined_stg',
            'rwip_allocation',
            'number(22,0) default 0',
            'The amount of rwip allocation to this group.');
   ADD_COLUMN('depr_calc_combined_arc',
            'rwip_allocation',
            'number(22,0) default 0',
            'The amount of rwip allocation to this group.');
   ADD_COLUMN('fcst_depr_calc_combined_arc',
            'rwip_allocation',
            'number(22,0) default 0',
            'The amount of rwip allocation to this group.');

   --add rwip_cost_of_removal to combined stg
   ADD_COLUMN('depr_calc_combined_stg',
            'rwip_cost_of_removal',
            'number(22,0) default 0',
            'The amount of RWIP Allocation that is cost of removal');
   ADD_COLUMN('depr_calc_combined_arc',
            'rwip_cost_of_removal',
            'number(22,0) default 0',
            'The amount of RWIP Allocation that is cost of removal');
   ADD_COLUMN('fcst_depr_calc_combined_arc',
            'rwip_cost_of_removal',
            'number(22,0) default 0',
            'The amount of RWIP Allocation that is cost of removal');

   --add defaults to rounding_plug columns.
   execute immediate 'alter table DEPR_CALC_COMBINED_STG MODIFY DEPR_ROUNDING_PLUG number(22, 2) default 0';
   execute immediate 'alter table DEPR_CALC_COMBINED_ARC MODIFY DEPR_ROUNDING_PLUG number(22, 2) default 0';
   execute immediate 'alter table FCST_DEPR_CALC_COMBINED_ARC MODIFY DEPR_ROUNDING_PLUG number(22, 2) default 0';

   execute immediate 'alter table DEPR_CALC_COMBINED_STG MODIFY COR_ROUNDING_PLUG number(22, 2) default 0';
   execute immediate 'alter table DEPR_CALC_COMBINED_ARC MODIFY COR_ROUNDING_PLUG number(22, 2) default 0';
   execute immediate 'alter table FCST_DEPR_CALC_COMBINED_ARC MODIFY COR_ROUNDING_PLUG number(22, 2) default 0';
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1040, 0, 10, 4, 2, 0, 37092, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037092_depr_add_rwip_over_depr_col.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;