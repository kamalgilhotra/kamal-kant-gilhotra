/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051581_lessee_01_pay_asset_penalty_proceed_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0.0 06/20/2018   K. Powers   PP-05181 JEs for retirements and penalties on Assets
 ||============================================================================
 */ 
 
ALTER TABLE LS_ASSET 
ADD (PAY_TERM_PENALTY   NUMBER(22,0) NULL,
     PAY_SALES_PROCEED  NUMBER(22,0) NULL);
     
comment on column ls_asset.pay_term_penalty is 'Indicates whether Termination Penalty Payment Lines should be generated for a retired asset (1 = Yes, 0 = No).';
comment on column ls_asset.pay_sales_proceed is 'Indicates whether Sales Proceed Payment Lines should be generated for a retired asset (1 = Yes, 0 = No).';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7422, 0, 2017, 4, 0, 0, 51581, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051581_lessee_01_pay_asset_penalty_proceed_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;