/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029297_cwip_3_CPR_RETIREMENT_PKG.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/20/2013 Joseph King    Point Release
||============================================================================
*/

create or replace package CPR_RETIREMENT_PKG
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: CPR_RETIREMENT_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Initial create
   ||============================================================================
   */
 as

   type NUMBER_ARRAY is table of number(22, 0) index by binary_integer;
   type DECIMAL_ARRAY is table of number(22, 2) index by binary_integer;

   function F_PP_OCR_ASSET_LOOKUP(A_COMPANY_ID         in number,
                                  A_BUS_SEGMENT_ID     in number,
                                  A_UTILITY_ACCOUNT_ID in number,
                                  A_SUB_ACCOUNT_ID     in number,
                                  A_PROPERTY_GROUP_ID  in number,
                                  A_RETIREMENT_UNIT_ID in number,
                                  A_ASSET_LOCATION_ID  in number,
                                  A_GL_ACCOUNT_ID      in number,
                                  A_SERIAL_NUMBER      in varchar2,
                                  A_VINTAGE            in number,
                                  A_QUANTITY           in number,
                                  A_AMOUNT             in number,
                                  A_ASSET_ID_ARRAY     out NUMBER_ARRAY,
                                  A_QUANTITY_ARRAY     out DECIMAL_ARRAY,
                                  A_AMOUNT_ARRAY       out DECIMAL_ARRAY) return number;
   /*
   ||============================================================================
   || Object Name: F_PP_OCR_ASSET_LOOKUP
   || Description: Lookup an asset_id from CPR_LEDGER with sufficient value to perform a specific retirement.
   ||
   ||   Arguments:
   ||               A_COMPANY_ID
   ||               A_BUS_SEGMENT_ID
   ||               A_UTILITY_ACCOUNT_ID
   ||               A_SUB_ACCOUNT_ID
   ||               A_PROPERTY_GROUP_ID
   ||               A_RETIREMENT_UNIT_ID
   ||               A_ASSET_LOCATION_ID
   ||               A_GL_ACCOUNT_ID
   ||               A_SERIAL_NUMBER    (optional)
   ||               A_VINTAGE
   ||               A_QUANTITY
   ||               A_AMOUNT
   ||               A_ASSET_ID_ARRAY out varchar2   (space delimited string of asset id)
   ||               A_QUANTITY_ARRAY out varchar2   (space delimited string of quantity)
   ||               A_AMOUNT_ARRAY   out varchar2   (space delimited string of amount)
   ||
   ||      Return: cpr_ledger.asset_id (success) or error codes below
   ||                  -1:  Insufficient assets found
   ||                  -10: Quantity and amount cannot both be zero
   ||                  -11: Max of 2 decimals are allowed for quantity / amount
   ||                  -12: Sign on quantity and amount do not match
   ||                  -13: Either quantity or amount must be 0.
   ||
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Original Version
   ||============================================================================
   */

   function F_PP_OCR_MASS_VALIDATE(A_COMPANY_ID         in number,
                                   A_BUS_SEGMENT_ID     in number,
                                   A_UTILITY_ACCOUNT_ID in number,
                                   A_SUB_ACCOUNT_ID     in number,
                                   A_PROPERTY_GROUP_ID  in number,
                                   A_RETIREMENT_UNIT_ID in number,
                                   A_ASSET_LOCATION_ID  in number,
                                   A_GL_ACCOUNT_ID      in number,
                                   A_QUANTITY           in number) return number;
   /*
   ||============================================================================
   || Object Name: F_PP_OCR_MASS_VALIDATE
   || Description: Validates that sufficient quantity exists in the CPR for mass retirements.
   ||              Validation is done against CPR_LEDGER_MASS_AVAIL_TO_RET_V that includes any
   ||              pending retirements.
   ||   Arguments:
   ||              A_COMPANY_ID
   ||              A_BUS_SEGMENT_ID
   ||              A_UTILITY_ACCOUNT_ID
   ||              A_SUB_ACCOUNT_ID
   ||              A_PROPERTY_GROUP_ID
   ||              A_RETIREMENT_UNIT_ID
   ||              A_ASSET_LOCATION_ID
   ||              A_GL_ACCOUNT_ID
   ||              A_QUANTITY
   ||
   ||      Return: 1 (sufficient quantity exists) or -1 (insufficient quantity exists)
   ||
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Original Version
   ||============================================================================
   */

   function F_PP_OCR_ASSET_ID_VALIDATE(A_ASSET_ID in number,
                                       A_QUANTITY in out number,
                                       A_AMOUNT   in out number) return number;
   /*
   ||============================================================================
   || Object Name: F_PP_OCR_ASSET_ID_VALIDATE
   || Description: Validate that the specified quantity / amount is available for retirement from the CPR_LEDGER.
   ||              If the amount provided is 0, the amount will be computed based on the unit cost and returned.
   ||   Arguments:
   ||              A_ASSET_ID
   ||              A_QUANTITY
   ||              A_AMOUNT
   ||
   ||      Return:  1 (success) or -1 (error)
   ||
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Original Version
   ||============================================================================
   */

   function F_PP_OCR_ASSET_LOOKUP_DEBUG(A_COMPANY_ID         in number,
                                        A_BUS_SEGMENT_ID     in number,
                                        A_UTILITY_ACCOUNT_ID in number,
                                        A_SUB_ACCOUNT_ID     in number,
                                        A_PROPERTY_GROUP_ID  in number,
                                        A_RETIREMENT_UNIT_ID in number,
                                        A_ASSET_LOCATION_ID  in number,
                                        A_GL_ACCOUNT_ID      in number,
                                        A_SERIAL_NUMBER      in varchar2,
                                        A_VINTAGE            in number,
                                        A_QUANTITY           in number,
                                        A_AMOUNT             in number) return number;
   /*
   ||============================================================================
   || Object Name: F_PP_OCR_ASSET_LOOKUP_DEBUG
   || Description: Function used to easily debug the F_PP_OCR_ASSET_LOOKUP_DEBUG
   ||
   ||      Return:  1 (success) or -1 (error)
   ||
   ||     Example:
   ||
   ||  select CPR_RETIREMENT_PKG.F_PP_OCR_ASSET_LOOKUP_DEBUG(34,4,439110,439110,217,7836,1365,101000,'',1993,1,697.26) from DUAL;
   ||
   ||  select 'select cpr_retirement_pkg.f_pp_ocr_asset_lookup_debug(' || company_id || ',' || bus_segment_id  || ',' ||
   ||         utility_account_id || ',' || sub_account_id  || ',' || property_group_id  || ',' || retirement_unit_id  || ',' ||
   ||         asset_location_id || ',' || gl_account_id  || ',' || ' '''' '  || ',' || to_char(eng_in_service_year,'yyyy')  || ',' ||
   ||         accum_quantity  || ',' || '0) from dual;' from cpr_ledger;
   ||
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Original Version
   ||============================================================================
   */

   function F_PP_OCR_ASSET_LOOKUP_DEBUG(A_ROW_ID in number) return number;
   /*
   ||============================================================================
   || Object Name: F_PP_OCR_ASSET_LOOKUP_DEBUG
   || Description: Function used to easily debug the F_PP_OCR_ASSET_LOOKUP_DEBUG based on data in WO_INTERFACE_OCR_STG
   ||
   ||      Return:
   ||
   ||     Example: select CPR_RETIREMENT_PKG.F_PP_OCR_ASSET_LOOKUP_DEBUG(1234) from DUAL;
   ||
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Original Version
   ||============================================================================
   */

   function F_PP_OCR_ASSET_LOOKUP_DEBUG return number;
   /*
   ||============================================================================
   || Object Name: F_PP_OCR_ASSET_LOOKUP_DEBUG
   || Description: Function used to easily debug the f_pp_ocr_asset_lookup_debug based on data in wo_interface_ocr_stg
   ||              processes all records in wo_interface_ocr_stg
   ||
   ||      Return:
   ||
   ||     Example: select CPR_RETIREMENT_PKG.F_PP_OCR_ASSET_LOOKUP_DEBUG() from DUAL;
   ||
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Original Version
   ||============================================================================
   */
end CPR_RETIREMENT_PKG;
/

create or replace package body CPR_RETIREMENT_PKG as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: CPR_RETIREMENT_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.0     03/20/2013 Joseph King    Initial create
   ||============================================================================
   */

   G_DEBUG boolean := false;

   --============================================================================
   --                          F_PP_OCR_MASS_VALIDATE
   --============================================================================
   function F_PP_OCR_MASS_VALIDATE(A_COMPANY_ID         in number,
                                   A_BUS_SEGMENT_ID     in number,
                                   A_UTILITY_ACCOUNT_ID in number,
                                   A_SUB_ACCOUNT_ID     in number,
                                   A_PROPERTY_GROUP_ID  in number,
                                   A_RETIREMENT_UNIT_ID in number,
                                   A_ASSET_LOCATION_ID  in number,
                                   A_GL_ACCOUNT_ID      in number,
                                   A_QUANTITY           in number) return number is

      L_COUNTER        number;
      L_QUANTITY_TOTAL number(22, 2);

   begin

      -- Requires quantity
      if A_QUANTITY = 0 then
         return -10;
      end if;

      -- Mass quantities must be whole numbers
      if ROUND(A_QUANTITY, 2) <> A_QUANTITY then
         return -11;
      end if;

      L_COUNTER        := 0;
      L_QUANTITY_TOTAL := 0;

      select count(*), sum(ACCUM_QUANTITY)
        into L_COUNTER, L_QUANTITY_TOTAL
        from CPR_LEDGER_MASS_AVAIL_TO_RET_V
       where COMPANY_ID = A_COMPANY_ID
         and BUS_SEGMENT_ID = A_BUS_SEGMENT_ID
         and UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID
         and SUB_ACCOUNT_ID = A_SUB_ACCOUNT_ID
         and PROPERTY_GROUP_ID = A_PROPERTY_GROUP_ID
         and RETIREMENT_UNIT_ID = A_RETIREMENT_UNIT_ID
         and ASSET_LOCATION_ID = A_ASSET_LOCATION_ID
         and GL_ACCOUNT_ID = A_GL_ACCOUNT_ID;

      if L_COUNTER = 0 then
         return -1;
      elsif ABS(L_QUANTITY_TOTAL) < ABS(A_QUANTITY) then
         return -1;
      else
         return 1;
      end if;

   end F_PP_OCR_MASS_VALIDATE;

   --============================================================================
   --                          F_PP_OCR_ASSET_LOOKUP
   --============================================================================
   function F_PP_OCR_ASSET_LOOKUP(A_COMPANY_ID         in number,
                                  A_BUS_SEGMENT_ID     in number,
                                  A_UTILITY_ACCOUNT_ID in number,
                                  A_SUB_ACCOUNT_ID     in number,
                                  A_PROPERTY_GROUP_ID  in number,
                                  A_RETIREMENT_UNIT_ID in number,
                                  A_ASSET_LOCATION_ID  in number,
                                  A_GL_ACCOUNT_ID      in number,
                                  A_SERIAL_NUMBER      in varchar2,
                                  A_VINTAGE            in number,
                                  A_QUANTITY           in number,
                                  A_AMOUNT             in number,
                                  A_ASSET_ID_ARRAY     out NUMBER_ARRAY,
                                  A_QUANTITY_ARRAY     out DECIMAL_ARRAY,
                                  A_AMOUNT_ARRAY       out DECIMAL_ARRAY) return number is

      L_MAX_VINTAGE_SEARCH number;
      L_CONTROL_VALUE      varchar2(35);
      L_COUNTER            number;
      L_COUNTER2           number;

      L_ASSET_ID        number;
      L_QUANTITY        number(22, 2);
      L_AMOUNT          number(22, 2);
      L_QUANTITY_TO_RET number(22, 2);
      L_AMOUNT_TO_RET   number(22, 2);
      L_QUANTITY_TOTAL  number(22, 2);
      L_AMOUNT_TOTAL    number(22, 2);

   begin

      -- Require either quantity or amount
      if A_AMOUNT = 0 and A_QUANTITY = 0 then
         return -10;
      end if;

      -- If they provided more than 2 decimals for either quantity or amount, bad things will happen
      if ROUND(A_AMOUNT, 2) <> A_AMOUNT or ROUND(A_QUANTITY, 2) <> A_QUANTITY then
         return -11;
      end if;

      -- Sign of a_amount and a_quantity must match then
      if SIGN(A_AMOUNT) * SIGN(A_QUANTITY) = -1 then
         return -12;
      end if;

      -- Only quantity or amount can be provided.  Not both
      if A_AMOUNT <> 0 and A_QUANTITY <> 0 then
         return -13;
      end if;

      L_CONTROL_VALUE := F_PP_SYSTEM_CONTROL(A_COMPANY_ID, 'MAXIMUM VINTAGE SEARCH WINDOW');
      if L_CONTROL_VALUE is null then
         L_CONTROL_VALUE := '2';
      end if;

      L_MAX_VINTAGE_SEARCH := TO_NUMBER(L_CONTROL_VALUE);
      L_COUNTER            := 0;
      L_COUNTER2           := 0;
      L_QUANTITY_TOTAL     := 0;
      L_AMOUNT_TOTAL       := 0;

      -- order by vintage (closest to requested)
      -- if exact match use it first
      -- if a_quantity <> 0 then quantity desc -- try to take it all from one asset first
      -- amount desc -- try to take it all from one asset first
      for ASSETS_TO_RET in (select ASSET_ID, ACCUM_QUANTITY, ACCUM_COST
                              from CPR_LEDGER_SPEC_AVAIL_TO_RET_V
                             where COMPANY_ID = A_COMPANY_ID
                               and BUS_SEGMENT_ID = A_BUS_SEGMENT_ID
                               and UTILITY_ACCOUNT_ID = A_UTILITY_ACCOUNT_ID
                               and SUB_ACCOUNT_ID = A_SUB_ACCOUNT_ID
                               and PROPERTY_GROUP_ID = A_PROPERTY_GROUP_ID
                               and RETIREMENT_UNIT_ID = A_RETIREMENT_UNIT_ID
                               and ASSET_LOCATION_ID = A_ASSET_LOCATION_ID
                               and GL_ACCOUNT_ID = A_GL_ACCOUNT_ID
                               and NVL(SERIAL_NUMBER, 'NULLSERIALNUMBER99812') =
                                   NVL(A_SERIAL_NUMBER, NVL(SERIAL_NUMBER, 'NULLSERIALNUMBER99812'))
                               and TO_CHAR(ENG_IN_SERVICE_YEAR, 'YYYY') between
                                   A_VINTAGE - L_MAX_VINTAGE_SEARCH and
                                   A_VINTAGE + L_MAX_VINTAGE_SEARCH
                               and SIGN(ACCUM_COST) * SIGN(ACCUM_QUANTITY) >= 0 -- Sign on accum_cost and accum_quantity must be the same
                               and SIGN(ACCUM_COST) * SIGN(A_AMOUNT) >= 0 -- Sign on accum_cost and a_amount must be the same
                               and DECODE(A_AMOUNT, 0, 1, ACCUM_COST) <> 0 -- if they gave us quantity, the the asset quantity cannot equal zero
                               and SIGN(ACCUM_QUANTITY) * SIGN(A_QUANTITY) >= 0 -- Sign on accum_quantity and a_quantity must be the same
                               and DECODE(A_QUANTITY, 0, 1, ACCUM_QUANTITY) <> 0 -- if they gave us amount, the the asset amount cannot equal zero
                             order by ABS(TO_CHAR(ENG_IN_SERVICE_YEAR, 'YYYY') - A_VINTAGE) asc,
                                      DECODE(A_QUANTITY, 0, ACCUM_COST, ACCUM_QUANTITY) desc)
      loop
         L_ASSET_ID := ASSETS_TO_RET.ASSET_ID;
         L_QUANTITY := ASSETS_TO_RET.ACCUM_QUANTITY;
         L_AMOUNT   := ASSETS_TO_RET.ACCUM_COST;

         L_COUNTER         := L_COUNTER + 1;
         L_QUANTITY_TO_RET := 0;
         L_AMOUNT_TO_RET   := 0;

         if A_QUANTITY < 0 then
            -- Specific dollar retirements - negative retirements of negative assets
            L_QUANTITY_TO_RET := GREATEST(L_QUANTITY, A_QUANTITY - L_QUANTITY_TOTAL);
            L_QUANTITY_TOTAL  := L_QUANTITY_TOTAL + L_QUANTITY_TO_RET;
         elsif A_QUANTITY > 0 then
            -- Specific dollar retirements - positive retirements of positive assets
            L_QUANTITY_TO_RET := LEAST(L_QUANTITY, A_QUANTITY - L_QUANTITY_TOTAL);
            L_QUANTITY_TOTAL  := L_QUANTITY_TOTAL + L_QUANTITY_TO_RET;
         else
            L_QUANTITY := 0;
         end if;

         if A_AMOUNT < 0 then
            -- Specific dollar retirements - negative retirements of negative assets
            L_AMOUNT_TO_RET := GREATEST(L_AMOUNT, A_AMOUNT - L_AMOUNT_TOTAL);
            L_AMOUNT_TOTAL  := L_AMOUNT_TOTAL + L_AMOUNT_TO_RET;
         elsif A_AMOUNT > 0 then
            -- Specific dollar retirements - positive retirements of positive assets
            L_AMOUNT_TO_RET := LEAST(L_AMOUNT, A_AMOUNT - L_AMOUNT_TOTAL);
            L_AMOUNT_TOTAL  := L_AMOUNT_TOTAL + L_AMOUNT_TO_RET;
         else
            -- Dollars not specified.  Use unit cost
            -- Since both a_amount and a_quantity cannot be 0, a_quantity must be non-zero
            -- Since a_quantity is non-zero, l_quantity(accum_quantity) must also be non-zero
            if L_QUANTITY_TO_RET = L_QUANTITY then
               -- will be a full retirement of this asset so take the full cost
               L_AMOUNT_TO_RET := L_AMOUNT;
            else
               -- will be a partial retirement of this asset.  Unit cost
               L_AMOUNT_TO_RET := ROUND(L_AMOUNT / L_QUANTITY * L_QUANTITY_TO_RET, 2);
            end if;
         end if;

         A_ASSET_ID_ARRAY(L_COUNTER) := L_ASSET_ID;
         A_QUANTITY_ARRAY(L_COUNTER) := L_QUANTITY_TO_RET;
         A_AMOUNT_ARRAY(L_COUNTER) := L_AMOUNT_TO_RET;

         if G_DEBUG then
            -- If called from the debug function, output the information to the screen and don't stop -- show all eligible assets
            DBMS_OUTPUT.PUT_LINE('.     Asset ID: ' || L_ASSET_ID || '       Quantity: ' ||
                                 L_QUANTITY_TO_RET || ' of ' || L_QUANTITY || '       Amount: ' ||
                                 L_AMOUNT_TO_RET || ' of ' || L_AMOUNT);
            if L_QUANTITY_TOTAL = A_QUANTITY and ABS(L_AMOUNT_TOTAL) >= ABS(A_AMOUNT) and
               L_COUNTER2 = 0 then
               DBMS_OUTPUT.PUT_LINE('.      ------------------------------------------------------------ ');
               L_COUNTER2 := 1;
            end if;
         else
            exit when L_QUANTITY_TOTAL = A_QUANTITY and ABS(L_AMOUNT_TOTAL) >= ABS(A_AMOUNT);
         end if;
      end loop;

      -- If we didn't find sufficient retirements then exit
      if L_QUANTITY_TOTAL = A_QUANTITY and ABS(L_AMOUNT_TOTAL) >= ABS(A_AMOUNT) then
         return L_COUNTER;
      else
         return -1;
      end if;

   end F_PP_OCR_ASSET_LOOKUP;

   --============================================================================
   --                          F_PP_OCR_ASSET_ID_VALIDATE
   --============================================================================
   function F_PP_OCR_ASSET_ID_VALIDATE(A_ASSET_ID in number,
                                       A_QUANTITY in out number,
                                       A_AMOUNT   in out number) return number is

      L_ASSET_ID number(22, 0);
      L_QUANTITY number(22, 2);
      L_AMOUNT   number(22, 2);

   begin

      -- Require either quantity or amount
      if A_AMOUNT = 0 and A_QUANTITY = 0 then
         return -10;
      end if;

      -- If they provided more than 2 decimals for either quantity or amount, bad things will happen
      if ROUND(A_AMOUNT, 2) <> A_AMOUNT or ROUND(A_QUANTITY, 2) <> A_QUANTITY then
         return -11;
      end if;

      -- Sign of a_amount and a_quantity must match then
      if SIGN(A_AMOUNT) * SIGN(A_QUANTITY) = -1 then
         return -12;
      end if;

      -- Only quantity or amount can be provided.  Not both
      if A_AMOUNT <> 0 and A_QUANTITY <> 0 then
         return -13;
      end if;

      for ASSETS_TO_RET in (select ASSET_ID, ACCUM_QUANTITY, ACCUM_COST
                              from CPR_LEDGER_SPEC_AVAIL_TO_RET_V
                             where ASSET_ID = A_ASSET_ID
                               and SIGN(ACCUM_COST) * SIGN(ACCUM_QUANTITY) >= 0 -- Sign on accum_cost and accum_quantity must be the same
                               and SIGN(ACCUM_COST) * SIGN(A_AMOUNT) >= 0 -- Sign on accum_cost and a_amount must be the same
                               and ABS(ACCUM_COST) >= A_AMOUNT
                               and ABS(ACCUM_QUANTITY) >= A_QUANTITY)
      loop
         L_ASSET_ID := ASSETS_TO_RET.ASSET_ID;
         L_QUANTITY := ASSETS_TO_RET.ACCUM_QUANTITY;
         L_AMOUNT   := ASSETS_TO_RET.ACCUM_COST;

         if A_AMOUNT = 0 then
            -- Dollars not specified.  Use unit cost
            -- Since both a_amount and a_quantity cannot be 0, a_quantity must be non-zero
            -- Since a_quantity is non-zero, l_quantity(accum_cost) must also be non-zero
            if A_QUANTITY = L_QUANTITY then
               -- will be a full retirement of this asset so take the full cost
               A_AMOUNT := L_AMOUNT;
            else
               -- will be a partial retirement of this asset.  Unit cost
               A_AMOUNT := ROUND(L_AMOUNT / L_QUANTITY * A_QUANTITY, 2);
            end if;
         end if;

         if G_DEBUG then
            -- If called from the debug function, output the information to the screen and don't stop -- show all eligible assets
            DBMS_OUTPUT.PUT_LINE('.      Asset ID: ' || L_ASSET_ID || '       Quantity: ' ||
                                 A_QUANTITY || ' of ' || L_QUANTITY || '       Amount: ' ||
                                 A_AMOUNT || ' of ' || L_AMOUNT);
         end if;

         return 1;
      end loop;

      -- If we didn't find sufficient retirements then exit
      return -1;
   end F_PP_OCR_ASSET_ID_VALIDATE;

   --============================================================================
   --                          F_PP_OCR_ASSET_LOOKUP_DEBUG
   --============================================================================
   function F_PP_OCR_ASSET_LOOKUP_DEBUG(A_COMPANY_ID         in number,
                                        A_BUS_SEGMENT_ID     in number,
                                        A_UTILITY_ACCOUNT_ID in number,
                                        A_SUB_ACCOUNT_ID     in number,
                                        A_PROPERTY_GROUP_ID  in number,
                                        A_RETIREMENT_UNIT_ID in number,
                                        A_ASSET_LOCATION_ID  in number,
                                        A_GL_ACCOUNT_ID      in number,
                                        A_SERIAL_NUMBER      in varchar2,
                                        A_VINTAGE            in number,
                                        A_QUANTITY           in number,
                                        A_AMOUNT             in number) return number is

      ASSET_ID_ARRAY NUMBER_ARRAY;
      QUANTITY_ARRAY DECIMAL_ARRAY;
      AMOUNT_ARRAY   DECIMAL_ARRAY;
      COUNTER        number := 0;

   begin

      G_DEBUG := true;

      DBMS_OUTPUT.PUT_LINE('.   Test Mass Validation Function');
      COUNTER := F_PP_OCR_MASS_VALIDATE(A_COMPANY_ID,
                                        A_BUS_SEGMENT_ID,
                                        A_UTILITY_ACCOUNT_ID,
                                        A_SUB_ACCOUNT_ID,
                                        A_PROPERTY_GROUP_ID,
                                        A_RETIREMENT_UNIT_ID,
                                        A_ASSET_LOCATION_ID,
                                        A_GL_ACCOUNT_ID,
                                        A_QUANTITY);
      DBMS_OUTPUT.PUT_LINE('.   Return: ' || COUNTER);
      DBMS_OUTPUT.PUT_LINE('. ');
      DBMS_OUTPUT.PUT_LINE('. ');
      DBMS_OUTPUT.PUT_LINE('.   Test Asset Lookup Function');
      COUNTER := F_PP_OCR_ASSET_LOOKUP(A_COMPANY_ID,
                                       A_BUS_SEGMENT_ID,
                                       A_UTILITY_ACCOUNT_ID,
                                       A_SUB_ACCOUNT_ID,
                                       A_PROPERTY_GROUP_ID,
                                       A_RETIREMENT_UNIT_ID,
                                       A_ASSET_LOCATION_ID,
                                       A_GL_ACCOUNT_ID,
                                       A_SERIAL_NUMBER,
                                       A_VINTAGE,
                                       A_QUANTITY,
                                       A_AMOUNT,
                                       ASSET_ID_ARRAY,
                                       QUANTITY_ARRAY,
                                       AMOUNT_ARRAY);
      G_DEBUG := false;
      DBMS_OUTPUT.PUT_LINE('.   Return: ' || COUNTER);

      return COUNTER;

   end F_PP_OCR_ASSET_LOOKUP_DEBUG;

   --============================================================================
   --                          F_PP_OCR_ASSET_LOOKUP_DEBUG
   --============================================================================
   function F_PP_OCR_ASSET_LOOKUP_DEBUG(A_ROW_ID in number) return number is

      RTN number;

   begin

      RTN := 0;

      for OCR_REC in (select * from WO_INTERFACE_OCR_STG where ROW_ID = A_ROW_ID)
      loop
         RTN := F_PP_OCR_ASSET_LOOKUP_DEBUG(OCR_REC.COMPANY_ID,
                                            OCR_REC.BUS_SEGMENT_ID,
                                            OCR_REC.UTILITY_ACCOUNT_ID,
                                            OCR_REC.SUB_ACCOUNT_ID,
                                            OCR_REC.PROPERTY_GROUP_ID,
                                            OCR_REC.RETIREMENT_UNIT_ID,
                                            OCR_REC.ASSET_LOCATION_ID,
                                            OCR_REC.GL_ACCOUNT_ID,
                                            OCR_REC.SERIAL_NUMBER,
                                            OCR_REC.VINTAGE,
                                            OCR_REC.QUANTITY,
                                            OCR_REC.AMOUNT);
      end loop;

      return RTN;

   end F_PP_OCR_ASSET_LOOKUP_DEBUG;

   --============================================================================
   --                          F_PP_OCR_ASSET_LOOKUP_DEBUG
   --============================================================================
   function F_PP_OCR_ASSET_LOOKUP_DEBUG return number is

      RTN number;

   begin

      RTN := 0;

      for OCR_REC in (select * from WO_INTERFACE_OCR_STG)
      loop
         DBMS_OUTPUT.PUT_LINE('----------------------------------------------------------------------');
         DBMS_OUTPUT.PUT_LINE('Row ID: ' || OCR_REC.ROW_ID);
         RTN := F_PP_OCR_ASSET_LOOKUP_DEBUG(OCR_REC.COMPANY_ID,
                                            OCR_REC.BUS_SEGMENT_ID,
                                            OCR_REC.UTILITY_ACCOUNT_ID,
                                            OCR_REC.SUB_ACCOUNT_ID,
                                            OCR_REC.PROPERTY_GROUP_ID,
                                            OCR_REC.RETIREMENT_UNIT_ID,
                                            OCR_REC.ASSET_LOCATION_ID,
                                            OCR_REC.GL_ACCOUNT_ID,
                                            OCR_REC.SERIAL_NUMBER,
                                            OCR_REC.VINTAGE,
                                            OCR_REC.QUANTITY,
                                            OCR_REC.AMOUNT);
         DBMS_OUTPUT.PUT_LINE('. ');
         DBMS_OUTPUT.PUT_LINE('. ');
         DBMS_OUTPUT.PUT_LINE('. ');
      end loop;

      return RTN;

   end F_PP_OCR_ASSET_LOOKUP_DEBUG;

end CPR_RETIREMENT_PKG;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (331, 0, 10, 4, 1, 0, 29297, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029297_cwip_3_CPR_RETIREMENT_PKG.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
