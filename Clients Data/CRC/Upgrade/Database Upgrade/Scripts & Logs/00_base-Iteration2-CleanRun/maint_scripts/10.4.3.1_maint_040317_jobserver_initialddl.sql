/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_040317_jobserver_initialddl.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.1   11/19/2014 Paul Cordero    	 Creation
||==========================================================================================
*/

SET serveroutput ON size 30000;

DECLARE
   doesObjectExist      NUMBER := 0;
   doesTableColumnExist NUMBER := 0;
BEGIN
	--------------------------------------------------------
	--  DDL for Table PP_JOB_EXECUTABLE
	--------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_EXECUTABLE Objects' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_EXECUTABLE', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_EXECUTABLE table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_EXECUTABLE Table' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_EXECUTABLE         
							(           
							"JOB_EXECUTABLE_ID" number(16,0) CONSTRAINT PP_JOB_EXECUTABLE03 NOT NULL,           
							"JOB_TYPE" varchar2(25) CONSTRAINT PP_JOB_EXECUTABLE01 NOT NULL,           
							"JOB_NAME" varchar2(50) CONSTRAINT PP_JOB_EXECUTABLE02 NOT NULL,           
							"EXECUTABLE" VARCHAR2(250),           
							"ARGUMENTS" VARCHAR2(4000 BYTE),                    

							CONSTRAINT PP_JOB_EXECUTABLE_PK PRIMARY KEY ("JOB_EXECUTABLE_ID"),     
							CONSTRAINT PP_JOB_EXE_TYPE_NAME UNIQUE ("JOB_TYPE", "JOB_NAME")         
							) TABLESPACE PWRPLANT';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTABLE"."JOB_EXECUTABLE_ID" IS ''Unique Identifier for this Primary Key''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTABLE"."JOB_TYPE" IS ''The type of Executable to run (e.g. C#, PowerBuilder, VB, JavaScript). Currently only supports C# and PowerBuilder.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTABLE"."JOB_NAME" IS ''Customizable Name of Executable Job to appear in DropDown.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTABLE"."EXECUTABLE" IS ''Actual Full FileName to be Executed.  Files need to be placed in the execution folder.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTABLE"."ARGUMENTS" IS ''JSON representing the Command Line Arguments to be passed to Exe to be called.''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_EXECUTABLE"  IS ''(S) [21] Lists all exe job names/types that a user can select as a ''''exe'''' or PB ''''exe job''''Lists all exe job names/types that a user can select as a ''''exe'''' or PB ''''exe job''''

The executable jobs are non-PowerBuilder executable applications. These can be executable applications written in C#, VB, etc.� The only requirement is that the applications be coded as a command line application that takes arguments and returns a value. The list of executable applications for the Job Name dropdown comes from the �PP_JOB_EXECUTABLE� table. JobType for this table can be any of the following: �CSharp�, �VB� or �PowerBuilder�. Currently the only supported type of executable jobs are C# and PowerBuilder. Executable applications created for this type of job must also reside within a specific folder on the server where the JobServer service resides. PowerBuilder executable applications must also reside on the server.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_EXECUTABLE for PWRPLANT.PP_JOB_EXECUTABLE';
          doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_EXECUTABLE_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
               EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_EXECUTABLE_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;
		 
        EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_EXECUTABLE      
							BEFORE INSERT ON PP_JOB_EXECUTABLE      
							FOR EACH ROW      
							BEGIN      
							:new.JOB_EXECUTABLE_ID := PP_JOB_EXECUTABLE_SEQ.nextval;     
							END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_EXECUTABLE" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL on PP_JOB_EXECUTABLE to pwrplant_role_dev';

      END;
   END IF;

   --------------------------------------------------------
   --  DDL for Table PP_JOB_EXECUTION
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_EXECUTION Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_EXECUTION', 'PWRPLANT' ) ;

   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_EXECUTION table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_EXECUTION' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_EXECUTION   
				(      
				"JOB_EXECUTION_ID" NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION01 NOT NULL,      
				"JOB_WORKFLOW_ID"  NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION03 NOT NULL,      
				"TITLE"            VARCHAR2 ( 50 ) CONSTRAINT PP_JOB_EXECUTION02 NOT NULL,      
				"DESCRIPTION"      VARCHAR2 ( 250 ),      
				"VARIABLES"        VARCHAR2 ( 4000 ),      
				"DESIGNER_ZOOM"    NUMBER ( 4, 2 ),      

				CONSTRAINT "PP_JOB_EXECUTION_PK" PRIMARY KEY ( "JOB_EXECUTION_ID" )   
				)   
				TABLESPACE PWRPLANT';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION"."JOB_EXECUTION_ID" IS ''Unique ID for the Execution.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION"."JOB_WORKFLOW_ID" IS ''Links this Execution to the Workflow that is being Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION"."TITLE" IS ''Title of the JobFlow to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION"."DESCRIPTION" IS ''Description of the JobFlow to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION"."VARIABLES" IS ''Variables to be used in the JobFlow to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION"."DESIGNER_ZOOM" IS ''Level of magnification in the UI for this JobFlow to be Executed.  NOT USED BY THE EXECUTION''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_EXECUTION"  IS ''(S) [21] Lists the JobFlows (Workflow) to be executed next.  Syncs to PP_JOB_WORKFLOW.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_EXECUTION FOR PWRPLANT.PP_JOB_EXECUTION';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_EXECUTION_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_EXECUTION_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;
		 
         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_EXECUTION       
				BEFORE INSERT ON PP_JOB_EXECUTION       
				FOR EACH ROW       
				BEGIN          
				:new.JOB_EXECUTION_ID := PP_JOB_EXECUTION_SEQ.nextval;      
				END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_EXECUTION" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_EXECUTION TO pwrplant_role_dev';
      END;
   END IF;

   --------------------------------------------------------
   --  DDL for Table PP_JOB_EXECUTION_JOB
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_EXECUTION_JOB Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_EXECUTION_JOB', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_EXECUTION_JOB table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_EXECUTION_JOB' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_EXECUTION_JOB         
				(            
				"JOB_EXECUTION_JOB_ID" NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION_JOB01 NOT NULL,            
				"JOB_EXECUTION_ID"     NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION_JOB02 NOT NULL,            
				"JOB_WORKFLOW_JOB_ID"  NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION_JOB07 NOT NULL,            
				"TITLE"                VARCHAR2 ( 50 ) CONSTRAINT PP_JOB_EXECUTION_JOB03 NOT NULL,            
				"DESCRIPTION"          VARCHAR2 ( 250 ),            
				"TYPE"                 VARCHAR2 ( 25 ) CONSTRAINT PP_JOB_EXECUTION_JOB04 NOT NULL,            
				"NAME"                 VARCHAR2 ( 50 ) CONSTRAINT PP_JOB_EXECUTION_JOB05 NOT NULL,            
				"EXECUTABLE"           VARCHAR2 ( 250 ),            
				"ARGUMENTS"            VARCHAR2 ( 2000 ),            
				"DATA"                 VARCHAR ( 4000 ),            
				"WEIGHT"               NUMBER ( 3, 0 ) CONSTRAINT PP_JOB_EXECUTION_JOB06 NOT NULL,            
				"EXECUTE_AFTER_TIME"   VARCHAR2 ( 8 ),            
				"ERROR_RETRY_COUNT"    NUMBER ( 3, 0 ),            
				"ERROR_RETRY_DELAY"    NUMBER ( 4, 0 ),            
				"DESIGNER_ROW"         NUMBER ( 3, 0 ),            
				"DESIGNER_COLUMN"      NUMBER ( 3, 0 ),            
				"PROCESS_ID"           NUMBER ( 22, 0 ),            
				"QUEUED_DATE"          DATE,            
				"QUEUED_BY"            VARCHAR2 ( 18 ),            
				"STARTED_DATE"         DATE,            
				"COMPLETED_DATE"       DATE,            
				"RESULT"               VARCHAR2 ( 4000 ),                  

				CONSTRAINT PP_JOB_EXECUTION_JOB_PK PRIMARY KEY ( "JOB_EXECUTION_JOB_ID" )         
				)         
				TABLESPACE PWRPLANT';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."JOB_EXECUTION_JOB_ID" IS ''Unique Identifier for the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."JOB_EXECUTION_ID" IS ''Foriegn Key from the PP_JOB_EXECUTION Table.  Key for the JobFlow to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."JOB_WORKFLOW_JOB_ID" IS ''Foriegn Key from the JOB_WORKFLOW_JOB Table.  Key relating the JobFlow to the JobFlow to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."TITLE" IS ''Title of the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."DESCRIPTION" IS ''Description of the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."TYPE" IS ''Type of the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."NAME" IS ''SubType of the Type of the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."EXECUTABLE" IS ''Executable File Name of the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."ARGUMENTS" IS ''Arguments to be used by the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."DATA" IS ''JSON Array of Data to be used by the JOB to be Executed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."WEIGHT" IS ''Allows Real Time Prioritization. [FUTURE USE]''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."EXECUTE_AFTER_TIME" IS ''If populated then this JOB is only to be executed between this time and Midnight.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."ERROR_RETRY_COUNT" IS ''Number of times to allow this JOB to fail.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."ERROR_RETRY_DELAY" IS ''Time in milliseconds to wait before retrying JOB if it fails.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."DESIGNER_ROW" IS ''Row in the JobFlow page that this Job appears [NOT USED BY EXECUTION]''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."DESIGNER_COLUMN" IS ''COLUMN in the JobFlow page that this Job appears [NOT USED BY EXECUTION]''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."PROCESS_ID" IS ''Relates to PP_PROCESSES which is used by PowerPlans Job Processer.  This is only used for PowerBuilder Jobs.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."QUEUED_DATE" IS ''Date this Job was Queued to Execute''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."QUEUED_BY" IS ''User_ID of person who Queued this Job''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."STARTED_DATE" IS ''Date and Time this Job was initiated.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."COMPLETED_DATE" IS ''Date and Time this job was completed''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."RESULT" IS ''Any data values returned by this JOB.''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB"  IS ''(S) [21] List of all Jobs to queue for execution.  Syncs to the PP_JOB_WORKFLOW_JOB table.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_EXECUTION_JOB FOR PWRPLANT.PP_JOB_EXECUTION_JOB';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_EXECUTION_JOB_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_EXECUTION_JOB_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;
		 
         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_EXECUTION_JOB       
			BEFORE INSERT ON PP_JOB_EXECUTION_JOB       
			FOR EACH ROW       
			BEGIN          
			:new.JOB_EXECUTION_JOB_ID := PP_JOB_EXECUTION_JOB_SEQ.nextval;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_EXECUTION_JOB" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_EXECUTION_JOB TO pwrplant_role_dev';
      END;
   END IF;

   --------------------------------------------------------
   --  DDL for Table PP_JOB_EXECUTION_TRANSITION
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_EXECUTION_TRANSITION Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_EXECUTION_TRANSITION', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_EXECUTION_TRANSITION table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN

         dbms_output.put_line ( 'Creating PP_JOB_EXECUTION_TRANSITION' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_EXECUTION_TRANSITION         
			(            
			"JOB_EXECUTION_TRANSITION_ID" NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION_TRANSITION01 NOT NULL,            
			"FROM_JOB_EXECUTION_JOB_ID"   NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION_TRANSITION02 NOT NULL,            
			"TO_JOB_EXECUTION_JOB_ID"     NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_EXECUTION_TRANSITION03 NOT NULL,            
			"CONDITIONS"                  VARCHAR2 ( 4000 ),            
			"RESULT"                      VARCHAR2 ( 5 ),                  

			CONSTRAINT PP_JOB_EXECUTION_TRANSITION_PK PRIMARY KEY ( "JOB_EXECUTION_TRANSITION_ID" )         
			)         
			TABLESPACE PWRPLANT';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION"."JOB_EXECUTION_TRANSITION_ID" IS ''Unique Id for this Transition to be processed.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION"."FROM_JOB_EXECUTION_JOB_ID" IS ''Unique Id of the JOB to transition From.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION"."TO_JOB_EXECUTION_JOB_ID" IS ''Unique Id of the JOB to transition To.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION"."CONDITIONS" IS ''JSON Array of Conditions to be met to transition.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION"."RESULT" IS ''The result from processing this transition.''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION"  IS ''(S) [21] For Jobs currently in the queue to execute, lists which job calls the next job. Also lists any conditional criteria needed to determine if the next job is called. This is synced with PP_JOB_WORKFLOW_TRANSITION.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_EXECUTION_TRANSITION FOR PWRPLANT.PP_JOB_EXECUTION_TRANSITION';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_EXECUTION_TRAN_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_EXECUTION_TRAN_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_EXECUTION_TRANSITION       
			BEFORE INSERT ON PP_JOB_EXECUTION_TRANSITION       
			FOR EACH ROW       
			BEGIN          
			:new.JOB_EXECUTION_TRANSITION_ID := PP_JOB_EXECUTION_TRAN_SEQ.nextval;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_EXECUTION_TRANSITION TO pwrplant_role_dev';
      END;
   END IF;

   --------------------------------------------------------
   --  DDL for Table PP_JOB_HISTORY
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_HISTORY Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_HISTORY', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_HISTORY table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_HISTORY' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_JOB_HISTORY"         
			(            
			"JOB_HISTORY_ID"       NUMBER ( 22, 0 ) CONSTRAINT "PP_JOB_HISTORY01" NOT NULL,            
			"JOB_EXECUTION_JOB_ID" NUMBER ( 22, 0 ) CONSTRAINT "PP_JOB_HISTORY02" NOT NULL,            
			"REFERENCE"            VARCHAR2 ( 50 BYTE ),            
			"REFERENCE_KEY"        VARCHAR2 ( 50 BYTE ),            
			"WEIGHT"               NUMBER ( 3, 0 ) CONSTRAINT "PP_JOB_HISTORY05" NOT NULL,            
			"SERVER"               VARCHAR2 ( 50 BYTE ) CONSTRAINT "PP_JOB_HISTORY09" NOT NULL,            
			"UUID"                 VARCHAR2 ( 36 BYTE ),            
			"PROCESS_ID"           NUMBER ( 22, 0 ),            
			"WAS_KILLED"           NUMBER ( 1, 0 ) CONSTRAINT "PP_JOB_HISTORY13" NOT NULL,            
			"WAS_KILLED_BY"        VARCHAR2 ( 18 BYTE ),            
			CONSTRAINT "PP_JOB_HISTORY_PK" PRIMARY KEY ( "JOB_HISTORY_ID" )         
			)         
			TABLESPACE "PWRPLANT" ';

         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."JOB_HISTORY_ID" IS ''Primary Key''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."JOB_EXECUTION_JOB_ID" IS ''ID of Executed Job''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."REFERENCE" IS ''Scheduled or Run Now''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."REFERENCE_KEY" IS ''Oracle Sequence to uniquely tie together''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."WEIGHT" IS ''Weight this Job had when executed''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."SERVER" IS ''Server this Job was executed on''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."UUID" IS ''GUID grouping Jobs to run within a Process or time frame''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."PROCESS_ID" IS ''PowerPlant Process Id''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."WAS_KILLED" IS ''Was this Job Killed instead of ran?''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_HISTORY"."WAS_KILLED_BY" IS ''If this Job was Killed, then who murdered it?''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_HISTORY"  IS ''(S) [21] List of all jobs that have run and their details (results).''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_HISTORY FOR PWRPLANT.PP_JOB_HISTORY';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_HISTORY_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_HISTORY_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_JOB_HISTORY"       
			BEFORE INSERT ON PP_JOB_HISTORY       
			FOR EACH ROW       
			BEGIN         
			SELECT PP_JOB_HISTORY_SEQ.nextval INTO :new.job_history_id FROM dual;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_HISTORY" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_HISTORY TO pwrplant_role_dev';
      END;
   END IF;

   --------------------------------------------------------
   --  DDL for Table PP_JOB_QUEUE
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_QUEUE Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_QUEUE', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_QUEUE table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_QUEUE' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_QUEUE         
			(            
			"JOB_QUEUE_ID"         NUMBER ( 22, 0 ) CONSTRAINT "PP_JOB_QUEUE01" NOT NULL,            
			"JOB_EXECUTION_JOB_ID" NUMBER ( 22, 0 ) CONSTRAINT "PP_JOB_QUEUE02" NOT NULL,            
			"REFERENCE"            VARCHAR2 ( 50 BYTE ),            
			"REFERENCE_KEY"        VARCHAR2 ( 50 BYTE ),            
			"WEIGHT"               NUMBER ( 3, 0 ) CONSTRAINT "PP_JOB_QUEUE03" NOT NULL,            
			"SERVER"               VARCHAR2 ( 50 BYTE ),            
			"UUID"                 VARCHAR2 ( 36 BYTE ),            
			"PRIORITY"             NUMBER ( 3, 0 ) CONSTRAINT "PP_JOB_QUEUE04" NOT NULL,            
			"PRIORITY_RANK"        NUMBER ( 3, 0 ) CONSTRAINT "PP_JOB_QUEUE05" NOT NULL,            
			"PRESUME_DEAD_DATE"    DATE,            
			"SHOULD_KILL"          NUMBER ( 1, 0 ) CONSTRAINT "PP_JOB_QUEUE06" NOT NULL,            
			"SHOULD_KILL_BY"       VARCHAR2 ( 18 BYTE ),                  

			CONSTRAINT "PP_JOB_QUEUE_PK" PRIMARY KEY ( "JOB_QUEUE_ID" )         
			)         
			TABLESPACE "PWRPLANT" ';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."JOB_QUEUE_ID" IS ''Unique Identifier for Queued Job''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."JOB_EXECUTION_JOB_ID" IS ''Identifier for Queued Job''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."REFERENCE" IS ''Scheduled or Run Now''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."REFERENCE_KEY" IS ''Oracle Sequence to uniquely tie together''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."WEIGHT" IS ''Allows Weighted processing on server [FUTURE FUNCTIONALITY]''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."SERVER" IS ''Server this JOB is Queued Up on [FUTURE FUNCTIONALITY]''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."UUID" IS ''GUID grouping Jobs to run within a Process or time frame''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."PRIORITY" IS ''Allows real time reprioritizing of JOBS''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."PRIORITY_RANK" IS ''Allows further granulization of real time reprioritization.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."PRESUME_DEAD_DATE" IS ''Drop Dead date.  If it''''s not run by this date then don''''t run.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."SHOULD_KILL" IS ''Allows real time "Killing" of job in queue.  True (1) kills the job.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_QUEUE"."SHOULD_KILL_BY" IS ''User ID of person who modified the "Should_Kill" flag.''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_QUEUE"  IS ''(S) [21] The current job queue list.  This entity  is populated by evaluating the PP_JOB_EXECUTION_JOB entity.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_QUEUE FOR PWRPLANT.PP_JOB_QUEUE';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_QUEUE_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_QUEUE_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;
		 
         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER "PWRPLANT"."PP_JOB_QUEUE"       
			BEFORE INSERT ON PP_JOB_QUEUE       
			FOR EACH ROW       
			BEGIN         
			SELECT PP_JOB_QUEUE_SEQ.nextval INTO :new.job_queue_id FROM dual;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_QUEUE" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_QUEUE TO pwrplant_role_dev';
		 END;
   END IF;


   --------------------------------------------------------
   --  DDL for Table PP_JOB_SCHEDULE
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_SCHEDULE Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_SCHEDULE', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_SCHEDULE table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_SCHEDULE' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_SCHEDULE                   
			(                       
			"JOB_SCHEDULE_ID" number(16,0) CONSTRAINT PP_JOB_SCHEDULE01 NOT NULL,                       
			"JOB_WORKFLOW_ID" number(16,0) CONSTRAINT PP_JOB_SCHEDULE02 NOT NULL,                       
			"PRIORITY" number(3,0) CONSTRAINT PP_JOB_SCHEDULE03 NOT NULL,                       
			"RECURRENCE" varchar2(250),                       
			"RECURRENCE_DATA" varchar2(4000),                       
			"OCCURRENCE" number(16,0),                       
			"START_DATE" DATE CONSTRAINT PP_JOB_SCHEDULE04 NOT NULL,                       
			"SERVER" VARCHAR2(50),                       
			"UUID" VARCHAR2(36),                       
			"TIME_STAMP" DATE,                       
			"USER_ID" VARCHAR2(18),                                            

			CONSTRAINT "PP_JOB_SCHEDULE_PK" PRIMARY KEY ("JOB_SCHEDULE_ID")                  
			) TABLESPACE PWRPLANT';

         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."JOB_SCHEDULE_ID" IS ''Primary key for this Scheduled Job Flow''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."JOB_WORKFLOW_ID" IS ''FK for WorkFlow''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."PRIORITY" IS ''Queueing Priority for this JobFlow''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."RECURRENCE" IS ''Human Readable Recurrence rules''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."RECURRENCE_DATA" IS ''JSON object containing the recurrence rules''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."OCCURRENCE" IS ''How often this Schedule is to re-occur''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."START_DATE" IS ''Date this Job Flow is to begin.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."SERVER" IS ''Server this Job Flow is to run on [FUTURE FUNCTIONALITY]''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."UUID" IS ''GUID grouping Jobs to run within a Process or time frame''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."TIME_STAMP" IS ''Created On Date''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."USER_ID" IS ''Createdy By User''';
         --EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_SCHEDULE"."QUEUED_DATE" IS ''Date the initial Job from this Job Flow was queued.''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_SCHEDULE"  IS ''(S) [21] Data linked to the calendar for JobFlow execution. Which JobFlow, start date, recurrence, etc.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_SCHEDULE FOR PWRPLANT.PP_JOB_SCHEDULE';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_SCHEDULE_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_SCHEDULE_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_SCHEDULE       
			BEFORE INSERT ON PP_JOB_SCHEDULE       
			FOR EACH ROW       
			BEGIN          
			:new.JOB_SCHEDULE_ID := PP_JOB_SCHEDULE_SEQ.nextval;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_SCHEDULE" ENABLE';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_SCHEDULE_AUDIT       
			BEFORE UPDATE OR INSERT ON PWRPLANT.PP_JOB_SCHEDULE       
			FOR EACH row       
			BEGIN          
			:new.user_id := USER;         
			:new.time_stamp := SYSDATE;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_SCHEDULE_AUDIT" ENABLE';
		 
         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_SCHEDULE TO pwrplant_role_dev';
      END;
   END IF;

   --------------------------------------------------------
   --  DDL for Table PP_JOB_WORKFLOW
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_WORKFLOW Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_WORKFLOW', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_WORKFLOW table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_WORKFLOW' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_WORKFLOW         
			(            
			"JOB_WORKFLOW_ID" NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_WORKFLOW01 NOT NULL,            
			"KEY"             VARCHAR2 ( 30 ),            
			"TITLE"           VARCHAR2 ( 50 ) CONSTRAINT PP_JOB_WORKFLOW02 NOT NULL,            
			"DESCRIPTION"     VARCHAR2 ( 250 ),            
			"VARIABLES"       VARCHAR2 ( 4000 ),            
			"DESIGNER_ZOOM"   NUMBER ( 4, 2 ),            
			"IS_INACTIVE"     CHAR(1 BYTE) DEFAULT ''0'' CONSTRAINT PP_JOB_WORKFLOW03 NOT NULL,            
			"TIME_STAMP"      DATE,            
			"USER_ID"         VARCHAR2 ( 18 ),                  

			CONSTRAINT "PP_JOB_WORKFLOW_PK" PRIMARY KEY ( "JOB_WORKFLOW_ID" )         
			)         
			TABLESPACE PWRPLANT';

         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."JOB_WORKFLOW_ID" IS ''Unique Identifier for every JobFlow''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."TITLE" IS ''Free Form short  title for this JobFlow''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."DESCRIPTION" IS ''Free Form Description for this JobFlow''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."VARIABLES" IS ''JSON Object Array containing any Variables specific to this WorkFlow.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."DESIGNER_ZOOM" IS ''Zoom Level of the JobFlow''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."TIME_STAMP" IS ''Created On Date''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."USER_ID" IS ''Created By User''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW"."IS_INACTIVE" IS ''Binary Flag to Deactivate/Pause the JobFlow.''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_WORKFLOW"  IS ''(S) [21] Data for the workflow definition. Title, description, key, and variables defined at the WF level''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_WORKFLOW FOR PWRPLANT.PP_JOB_WORKFLOW';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_WORKFLOW_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_WORKFLOW_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_WORKFLOW       
			BEFORE INSERT ON PP_JOB_WORKFLOW       
			FOR EACH ROW       
			BEGIN          
			:new.JOB_WORKFLOW_ID := PP_JOB_WORKFLOW_SEQ.nextval;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_WORKFLOW" ENABLE';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_WORKFLOW_AUDIT       
			BEFORE UPDATE OR INSERT ON PWRPLANT.PP_JOB_WORKFLOW       
			FOR EACH row       
			BEGIN          
			:new.user_id := USER;         
			:new.time_stamp := SYSDATE;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_WORKFLOW_AUDIT" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_WORKFLOW TO pwrplant_role_dev';
      END;
   END IF;


   --------------------------------------------------------
   --  DDL for Table PP_JOB_WORKFLOW_JOB
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_WORKFLOW_JOB Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_WORKFLOW_JOB', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_WORKFLOW_JOB table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_WORKFLOW_JOB' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_WORKFLOW_JOB         
			(            
			"JOB_WORKFLOW_JOB_ID" NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_WORKFLOW_JOB01 NOT NULL,            
			"JOB_WORKFLOW_ID"     NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_WORKFLOW_JOB02 NOT NULL,            
			"TITLE"               VARCHAR2 ( 50 ) CONSTRAINT PP_JOB_WORKFLOW_JOB03 NOT NULL,            
			"DESCRIPTION"         VARCHAR2 ( 250 ),            
			"TYPE"                VARCHAR2 ( 25 ) CONSTRAINT PP_JOB_WORKFLOW_JOB04 NOT NULL,            
			"NAME"                VARCHAR2 ( 50 ) CONSTRAINT PP_JOB_WORKFLOW_JOB05 NOT NULL,            
			"EXECUTABLE"          VARCHAR2 ( 250 ),            
			"ARGUMENTS"           VARCHAR2 ( 2000 ),            
			"ARGUMENTS_OBJECT"    VARCHAR2 ( 3000 ),            
			"DATA"                VARCHAR ( 4000 ),            
			"WEIGHT"              NUMBER ( 3, 0 ) CONSTRAINT PP_JOB_WORKFLOW_JOB06 NOT NULL,            
			"EXECUTE_AFTER_TIME"  VARCHAR2 ( 8 ),            
			"ERROR_RETRY_COUNT"   NUMBER ( 3, 0 ) DEFAULT 3 CONSTRAINT PP_JOB_WORKFLOW_JOB07 NOT NULL,            
			"ERROR_RETRY_DELAY"   NUMBER ( 4, 0 ),            
			"DESIGNER_ROW"        NUMBER ( 3, 0 ),            
			"DESIGNER_COLUMN"     NUMBER ( 3, 0 ),            
			"TIME_STAMP"          DATE,            
			"USER_ID"             VARCHAR2 ( 18 ),                  

			CONSTRAINT PP_JOB_WORKFLOW_JOB_PK PRIMARY KEY ( "JOB_WORKFLOW_JOB_ID" )         
			)         
			TABLESPACE PWRPLANT';

         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."JOB_WORKFLOW_JOB_ID" IS ''Unique Identifier for the Job''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."JOB_WORKFLOW_ID" IS ''Workflow ID that this Job belongs to.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."TITLE" IS ''Short title of the Job''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."DESCRIPTION" IS ''Long Description of the Job''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."TYPE" IS ''Job Type (SQL, C#, etc.)''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."NAME" IS ''Sub Type of the Job (Query, Email, etc.)''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."EXECUTABLE" IS ''Executable to be called on Execution.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."ARGUMENTS" IS ''Arguments to be populated in Job (not to be confused with JobFlow Variables)''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."DATA" IS ''JSON Object Array containing any data to be used on Execution.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."WEIGHT" IS ''Allows Real Time Prioritization. [FUTURE USE]''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."EXECUTE_AFTER_TIME" IS ''Time Restriction to ensure execution between this time and Midnight.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."ERROR_RETRY_COUNT" IS ''How many execution failures are allowed for this job.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."ERROR_RETRY_DELAY" IS ''How long to wait between failures before trying again.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."DESIGNER_ROW" IS ''What row does this job appear on in the JobFlow.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."DESIGNER_COLUMN" IS ''What column does this job appear on in the JobFlow.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."TIME_STAMP" IS ''Created On Date''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."USER_ID" IS ''Created By User''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB"  IS ''(S) [21] WorkFlow job information / data stored here. Link to WF, type of WF, arguements, Designer position and error time and count.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_WORKFLOW_JOB FOR PWRPLANT.PP_JOB_WORKFLOW_JOB';


         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_WORKFLOW_JOB_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_WORKFLOW_JOB_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_WORKFLOW_JOB       
			BEFORE INSERT ON PP_JOB_WORKFLOW_JOB       
			FOR EACH ROW       
			BEGIN          
			:new.JOB_WORKFLOW_JOB_ID := PP_JOB_WORKFLOW_JOB_SEQ.nextval;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_WORKFLOW_JOB" ENABLE';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_WORKFLOW_JOB_AUDIT       
			BEFORE UPDATE OR INSERT ON PWRPLANT.PP_JOB_WORKFLOW_JOB       
			FOR EACH row       
			BEGIN          
			:new.user_id := USER;         
			:new.time_stamp := SYSDATE;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_WORKFLOW_JOB_AUDIT" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_WORKFLOW_JOB TO pwrplant_role_dev';
	END;
   END IF;

   --------------------------------------------------------
   --  DDL for Table PP_JOB_WORKFLOW_TRANSITION
   --------------------------------------------------------
   dbms_output.put_line ( 'Creating Job Server PP_JOB_WORKFLOW_TRANSITION Table' ) ;
   doesObjectExist := SYS_DOES_TABLE_EXIST ( 'PP_JOB_WORKFLOW_TRANSITION', 'PWRPLANT' ) ;
   IF doesObjectExist = 1 THEN
      BEGIN
         dbms_output.put_line ( 'PP_JOB_WORKFLOW_TRANSITION table found, not CREATEd.' ) ;
      END;
   ELSE
      BEGIN
         dbms_output.put_line ( 'Creating PP_JOB_WORKFLOW_TRANSITION' ) ;
         EXECUTE IMMEDIATE 'CREATE TABLE PWRPLANT.PP_JOB_WORKFLOW_TRANSITION         
			(            
			"JOB_WORKFLOW_TRANSITION_ID" NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_WORKFLOW_TRANSITION01 NOT NULL,            
			"FROM_JOB_WORKFLOW_JOB_ID"   NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_WORKFLOW_TRANSITION02 NOT NULL,            
			"TO_JOB_WORKFLOW_JOB_ID"     NUMBER ( 16, 0 ) CONSTRAINT PP_JOB_WORKFLOW_TRANSITION03 NOT NULL,            
			"CONDITIONS"                 VARCHAR2 ( 4000 ),            
			"TIME_STAMP"                 DATE,            
			"USER_ID"                    VARCHAR2 ( 18 ),                  

			CONSTRAINT PP_JOB_TRANSITION_PK PRIMARY KEY ( "JOB_WORKFLOW_TRANSITION_ID" )         
			)         
			TABLESPACE PWRPLANT';

         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION"."JOB_WORKFLOW_TRANSITION_ID" IS ''Primary Key for this Transition.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION"."FROM_JOB_WORKFLOW_JOB_ID" IS ''Unique Id of the JOB to transtion from.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION"."TO_JOB_WORKFLOW_JOB_ID" IS ''Unique Id of the JOB to transition to.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION"."CONDITIONS" IS ''JSON Array of Conditions to be met to transition.''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION"."TIME_STAMP" IS ''Created On Date''';
         EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION"."USER_ID" IS ''Created By User Id''';
         EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION"  IS ''(S) [21] Transition data between jobs is stored here. The conditional transition decisions are stored within the CONDITIONS column.''';

         EXECUTE IMMEDIATE 'CREATE OR REPLACE PUBLIC SYNONYM PP_JOB_WORKFLOW_TRANSITION FOR PWRPLANT.PP_JOB_WORKFLOW_TRANSITION';

         doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'PP_JOB_WORKFLOW_TRANSITION_SEQ', 'PWRPLANT' ) ;
         IF doesObjectExist != 1 THEN
            BEGIN
				EXECUTE IMMEDIATE 'CREATE SEQUENCE PP_JOB_WORKFLOW_TRANSITION_SEQ START WITH 1 INCREMENT BY 1 NOMAXVALUE';
            END;
         END IF;

         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_WORKFLOW_TRANSITION BEFORE      
			INSERT ON PP_JOB_WORKFLOW_TRANSITION       
			FOR EACH ROW       
			BEGIN         
			:new.JOB_WORKFLOW_TRANSITION_ID := PP_JOB_WORKFLOW_TRANSITION_SEQ.nextval;      

			END;';
         EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER PWRPLANT.PP_JOB_WORKFLOW_TRANSITION_A       
			BEFORE UPDATE OR INSERT ON PWRPLANT.PP_JOB_WORKFLOW_TRANSITION       
			FOR EACH row       
			BEGIN          
			:new.user_id := USER;         
			:new.time_stamp := SYSDATE;      
			END;';
         EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION_A" ENABLE';

         EXECUTE IMMEDIATE 'GRANT ALL ON PP_JOB_WORKFLOW_TRANSITION TO pwrplant_role_dev';
      END;
   END IF;
END;
/

--------------------------------------------------------
--  Create All Foriegn Keys
--------------------------------------------------------
DECLARE
   doesObjectExist       NUMBER := 0;
BEGIN
   dbms_output.put_line ( 'Creating Foriegn Keys' ) ;

	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PP_JOB_EXEC_JOB_EXEC_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" ADD CONSTRAINT "FK_PP_JOB_EXEC_JOB_EXEC_ID" FOREIGN KEY ("JOB_EXECUTION_ID")
			REFERENCES "PWRPLANT"."PP_JOB_EXECUTION" ("JOB_EXECUTION_ID") ENABLE';
    END;
	END IF;
	
	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PPJOBEXEC_TRANS_FRM_JOB_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION" ADD CONSTRAINT "FK_PPJOBEXEC_TRANS_FRM_JOB_ID" FOREIGN KEY ("FROM_JOB_EXECUTION_JOB_ID")
			REFERENCES "PWRPLANT"."PP_JOB_EXECUTION_JOB" ("JOB_EXECUTION_JOB_ID") ENABLE';
    END;
	END IF;
	
	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PP_JOB_EXEC_TRANS_TO_JOB_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_TRANSITION" ADD CONSTRAINT "FK_PP_JOB_EXEC_TRANS_TO_JOB_ID" FOREIGN KEY ("TO_JOB_EXECUTION_JOB_ID")
			REFERENCES "PWRPLANT"."PP_JOB_EXECUTION_JOB" ("JOB_EXECUTION_JOB_ID") ENABLE';
    END;
	END IF;
	
	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PP_JOB_HIST_JOB_EXEC_JOB_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_HISTORY" ADD CONSTRAINT "FK_PP_JOB_HIST_JOB_EXEC_JOB_ID" FOREIGN KEY ("JOB_EXECUTION_JOB_ID")
		  REFERENCES "PWRPLANT"."PP_JOB_EXECUTION_JOB" ("JOB_EXECUTION_JOB_ID") ENABLE';
    END;
	END IF;
	
	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PP_JOB_Q_JOB_EXEC_JOB_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_QUEUE" ADD CONSTRAINT "FK_PP_JOB_Q_JOB_EXEC_JOB_ID" FOREIGN KEY ("JOB_EXECUTION_JOB_ID")
		  REFERENCES "PWRPLANT"."PP_JOB_EXECUTION_JOB" ("JOB_EXECUTION_JOB_ID") ENABLE';
    END;
	END IF;
	
	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PP_JOB_WF_JOB_WF_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" ADD CONSTRAINT "FK_PP_JOB_WF_JOB_WF_ID" FOREIGN KEY ("JOB_WORKFLOW_ID")
		  REFERENCES "PWRPLANT"."PP_JOB_WORKFLOW" ("JOB_WORKFLOW_ID") ENABLE';
    END;
	END IF;
	
	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PP_JOB_WF_TRANS_FROM_JOB_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION" ADD CONSTRAINT "FK_PP_JOB_WF_TRANS_FROM_JOB_ID" FOREIGN KEY ("FROM_JOB_WORKFLOW_JOB_ID")
		  REFERENCES "PWRPLANT"."PP_JOB_WORKFLOW_JOB" ("JOB_WORKFLOW_JOB_ID") ENABLE';
    END;
	END IF;
	
	doesObjectExist := SYS_DOES_OBJECT_EXIST ( 'FK_PP_JOB_EXEC_JOB_EXEC_ID', 'PWRPLANT' ) ;
	IF doesObjectExist != 1 THEN
	BEGIN
		dbms_output.put_line ( 'Creating FK_PP_JOB_WF_TRANS_TO_JOB_ID' ) ;
		EXECUTE IMMEDIATE 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_TRANSITION" ADD CONSTRAINT "FK_PP_JOB_WF_TRANS_TO_JOB_ID" FOREIGN KEY ("TO_JOB_WORKFLOW_JOB_ID")
			REFERENCES "PWRPLANT"."PP_JOB_WORKFLOW_JOB" ("JOB_WORKFLOW_JOB_ID") ENABLE';
	END;
	END IF;
END;
/

DECLARE
  recCount      NUMBER := 0;
BEGIN
  SELECT COUNT(1) into recCount FROM PWRPLANT.PP_JOB_EXECUTABLE;
  IF (recCount = 0) THEN
	  BEGIN
		dbms_output.put_line ( 'INSERTING into PWRPLANT.PP_JOB_EXECUTABLE' );
		--SET DEFINE OFF;
		INSERT INTO pp_job_executable VALUES (
			12,
			'SSP',
			'Budget AFUDC',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"","label":"Budget Version"},{"type":"string","value":"","label":"Table  Name"},{"type":"boolean","value":"<false,false,false,false,true,false,false>","label":"Budget AFUDC"}]}');
		INSERT INTO pp_job_executable VALUES (
			13,
			'SSP',
			'Budget Automatic Processing',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"<201206>","label":"Budget Version"},{"type":"string","value":"<WO_EST_MONTHLY>","label":"Table Name"},{"type":"boolean","value":"<false, false, false, false, true, false, false>","label":"AFUDC"}]}');
		INSERT INTO pp_job_executable VALUES (
			14,
			'SSP',
			'Budget Escalations',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"<201227>","label":"Budget Version"},{"type":"string","value":"<FP,  >","label":"Process Level"},{"type":"boolean","value":"<false, false, true, false, false, false, false>","label":"Escalations"}]}');
		INSERT INTO pp_job_executable VALUES (
			15,
			'SSP',
			'Budget Overheads',
			'ssp_budget.exe',
			'{"parms":[{"type":"string","value":"<fp, select 1149254~comma~ 12 from dual >","label":"Process Level"},{"type":"boolean","value":"<false, false, false, true, false, false, false>","label":"Overheads"}]}');
		INSERT INTO pp_job_executable VALUES (
			16,
			'SSP',
			'Budget To CR',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"<201208, 9, 8, 4>","label":"Budget Version ID; Insert Where Clause ID; Delete Where Clause ID; Posting ID"},{"type":"string","value":"<NA,  , cr_budget_data, 2011 6/6 Forecast>","label":"CR Table; CR Budget Version"},{"type":"boolean","value":"<false, false, false, false, false, false, true>","label":"CR to Budget to CR"},{"type":"boolean2","value":"<false, true>","label":"Only in BV"}]}');
		INSERT INTO pp_job_executable VALUES (
			17,
			'SSP',
			'Budget Update with Actuals',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"<201225>","label":"Budget Version"},{"type":"string","value":"<fp,  >","label":"WO/FP Indicator"},{"type":"boolean","value":"<false, true, false, false, false, false, false>","label":"Update with Actuals"}]}');
		INSERT INTO pp_job_executable VALUES (
			18,
			'SSP',
			'Budget WIP Computations',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"<0>","label":"Not Applicable"},{"type":"string","value":"<FP, select 3798738~comma~ 1 from dual >","label":"Process Level"},{"type":"boolean","value":"<false, false, false, false, false, true, false>","label":"WIP Comps"}]}');
		INSERT INTO pp_job_executable VALUES (
			19,
			'SSP',
			'Incremental Forecast by FP',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"<833453>","label":"Forecast Depr Version"},{"type":"long2","value":"<647833>","label":"Array of FPs"},{"type":"long3","value":"<19>","label":"Array of Revisions"},{"type":"string","value":"<INCREMENTAL>","label":"Depreciation Type"},{"type":"date","value":"<02/01/2012, 03/01/2012, 04/01/2012, 05/01/2012, 06/01/2012, 07/01/2012, 08/01/2012, 09/01/2012, 10/01/2012,11/01/2012, 12/01/2012, 01/01/2013, 02/01/2013, 03/01/2013, 04/01/2013, 05/01/2013, 06/01/2013, 07/01/2013, 08/01/2013, 09/01/2013,10/01/2013, 11/01/2013, 12/01/2013, 01/01/2014, 02/01/2014, 03/01/2014, 04/01/2014, 05/01/2014, 06/01/2014, 07/01/2014, 08/01/2014,09/01/2014, 10/01/2014, 11/01/2014, 12/01/2014, 01/01/2015, 02/01/2015, 03/01/2015, 04/01/2015, 05/01/2015, 06/01/2015, 07/01/2015,08/01/2015, 09/01/2015, 10/01/2015, 11/01/2015, 12/01/2015, 01/01/2016, 02/01/2016, 03/01/2016, 04/01/2016, 05/01/2016, 06/01/2016,07/01/2016,08/01/2016,09/01/2016,10/01/2016,11/01/2016,12/01/2016>","label":"Start and End Dates"}]}');
		INSERT INTO pp_job_executable VALUES (
			20,
			'SSP',
			'Load Budget Dollars from FP',
			'ssp_budget.exe',
			'{"parms":[{"type":"long","value":"<201205>","label":"Budget Version"},{"type":"boolean","value":"<true, false, false, false, false, false, false>","label":"Load"},{"type":"boolean2","value":"<false>","label":"Update with Actuals"}]}');
		INSERT INTO pp_job_executable VALUES (
			25,
			'SSP',
			'Depreciation Forecast: Individual',
			'SSP_budget.exe',
			'{"parms":[{"type":"long","value":"<629520757>","label":"Version"},{"type":"long3","value":"<277>","label":"Process Control Group"},{"type":"string","value":"<FCST: Individual>","label":"Depreciation Type"},{"type":"date","value":"","label":"Month"}]}');
		INSERT INTO pp_job_executable VALUES (
			26,
			'SSP',
			'Close CPR',
			'SSP_cpr_close_month.exe',
			'{"parms":[{"type":"long","value":"<1>","label":"Companies"},{"type":"string","value":"<CLOSE>","label":"Task Type"},{"type":"date","value":"<06/01/2010>","label":"Original Month"}]}');
		INSERT INTO pp_job_executable VALUES (
			27,
			'SSP',
			'New month on CPR',
			'SSP_cpr_new_month.exe',
			'{"parms":[{"type":"long","value":"<1>","label":"Companies"},{"type":"date","value":"<06/01/2010>","label":"Original Month"}]}');
		INSERT INTO pp_job_executable VALUES (
			28,
			'SSP',
			'ARO Processing',
			'SSP_aro_approve.exe',
			'{"parms":[{"type":"long","value":"<4>","label":"Companies"},{"type":"datetime","value":"<05/01/2010 00:00:00>","label":"Month"}]}');
		INSERT INTO pp_job_executable VALUES (
			29,
			'SSP',
			'ARO Approval',
			'SSP_aro_approve.exe',
			'{"parms":[{"type":"long","value":"<84>","label":"Companies"},{"type":"datetime","value":"<07/01/2010 00:00:00>","label":"Month"}]}');
		INSERT INTO pp_job_executable VALUES (
			30,
			'PowerBuilder',
			'CR Allocations',
			'cr_allocations.exe',
			'{"parms":[{"type":"long","value":"","label":"Start Priority","required":"true"},{"type":"long","value":"","label":"End Priority","required":"true"},{"type":"string","value":"Not Used for Actuals","label":"Budget Version","required":"true"},{"type":"string","value":"ACTUAL","label":"Test or Actual","required":"true"},{"type":"long","value":"","label":"Start Month","required":"true"},{"type":"long","value":"","label":"End Month","required":"true"},{"type":"string","value":"No","label":"Run As Commitments","required":"true"}]}');
		INSERT INTO pp_job_executable VALUES (
			31,
			'PowerBuilder',
			'Automated Review',
			'automatedreview.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			32,
			'PowerBuilder',
			'Batch Reporter',
			'batreporter.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			33,
			'PowerBuilder',
			'Budget Allocations',
			'budget_allocations.exe',
			'{"parms":[{"type":"long","value":"","label":"Start Priority","required":"true"},{"type":"long","value":"","label":"End Priority","required":"true"},{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"ACTUAL","label":"Test or Actual","required":"true"},{"type":"long","value":"","label":"Start Month","required":"true"},{"type":"long","value":"","label":"End Month","required":"true"}]}');
		INSERT INTO pp_job_executable VALUES (
			34,
			'PowerBuilder',
			'CR Balances',
			'cr_balances.exe',
			'{"parms":[{"type":"string","value":"","label":"Month Number(s)","required":"false"}]}');
		INSERT INTO pp_job_executable VALUES (
			35,
			'PowerBuilder',
			'CR Balances Budget',
			'cr_balances_bdg.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			36,
			'PowerBuilder',
			'CR Batch Derivations',
			'cr_batch_derivation.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			37,
			'PowerBuilder',
			'CR Batch Derivation Budget',
			'cr_batch_derivation_bdg.exe',
			'{"parms":[{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"","label":"Derivation Type","required":"true"}, {"type":"long","value":"","label":"Start Month Number","required":"false"},{"type":"long","value":"","label":"End Month Number","required":"false"},{"type":"string","value":"","label":"Company","required":"false"}]}');
		INSERT INTO pp_job_executable VALUES (
			38,
			'PowerBuilder',
			'CR Budget Entry Calc All',
			'cr_budget_entry_calc_all.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			39,
			'PowerBuilder',
			'CR Build Combos',
			'cr_build_combos.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			40,
			'PowerBuilder',
			'CR Build Deriver',
			'cr_build_deriver.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			41,
			'PowerBuilder',
			'CR Delete Budget Allocations',
			'cr_delete_budget_allocations.exe',
			'{"parms":[{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"","label":"Company","required":"true"}]}');
		INSERT INTO pp_job_executable VALUES (
			42,
			'PowerBuilder',
			'CR Financial Reports Build',
			'cr_financial_reports_build.exe',
			'{"parms":[{"type":"string","value":"","label":"User ID","required":"true"},{"type":"long","value":"","label":"Session ID","required":"true"}]}');
		INSERT INTO pp_job_executable VALUES (
			43,
			'PowerBuilder',
			'CR Flatten Structures',
			'cr_flatten_structures.exe',
			'{"parms":[{"type":"long","value":"","label":"Structure IDs","required":"false"}]}');
		INSERT INTO pp_job_executable VALUES (
			44,
			'PowerBuilder',
			'CR Manual Journal Reversals',
			'cr_man_jrnl_reversals.exe',
			'{"parms":[{"type":"string","value":"","label":"Company","required":"false"}]}');
		INSERT INTO pp_job_executable VALUES (
			45,
			'PowerBuilder',
			'CR Posting',
			'cr_posting.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			46,
			'PowerBuilder',
			'CR Posting Budget',
			'cr_posting_bdg.exe',
			'{"parms":[{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"","label":"Posting IDs","required":"false"}]}');
		INSERT INTO pp_job_executable VALUES (
			47,
			'PowerBuilder',
			'CR SAP Trueup',
			'cr_sap_trueup.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			48,
			'PowerBuilder',
			'CR Sum',
			'cr_sum.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			49,
			'PowerBuilder',
			'CR Sum AB',
			'cr_sum_ab.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			50,
			'PowerBuilder',
			'CR To Commitments',
			'cr_to_commitments.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			51,
			'PowerBuilder',
			'CWIP Charge',
			'cwip_charge',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			52,
			'PowerBuilder',
			'Populate Material Reconciliation',
			'populate_matlrec.exe',
			NULL);
		INSERT INTO pp_job_executable VALUES (
			53,
			'PowerBuilder',
			'PP Integration',
			'pp_integration.exe',
			'{"parms":[{"type":"string","value":"","label":"PP Integration Command Line","required":"true"}]}');
		INSERT INTO pp_job_executable VALUES (
			54,
			'PowerBuilder',
			'PP to CR',
			'pptocr.exe',
			'{"parms":[{"type":"long","value":"","label":"Company","required":"true"}]}');
		INSERT INTO pp_job_executable VALUES (
			55,
			'PowerBuilder',
			'PP Verify',
			'ppverify.exe',
			NULL);
	  END;
  END IF;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2017, 0, 10, 4, 3, 1, 040317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.1_maint_040317_jobserver_initialddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;