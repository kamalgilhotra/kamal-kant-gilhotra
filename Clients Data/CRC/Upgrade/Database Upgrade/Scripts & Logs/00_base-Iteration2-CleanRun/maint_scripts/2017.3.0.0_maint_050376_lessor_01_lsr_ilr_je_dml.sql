/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050376_lessor_01_lsr_ilr_je_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 02/21/2018 Anand R        Add Lessor trans type
||============================================================================
*/

/* Add new trans type 4055*/
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4055, 'Lessor Deferred Selling Profit Credit');

/* Modify the description of trans type from 4001 to 4008 */
update je_trans_type set description = 'Lessor PP&' || 'E Adjustment Credit'
where TRANS_TYPE = 4001;

update je_trans_type set description = 'Lessor Short Term Receivable Debit'
where TRANS_TYPE = 4002;

update je_trans_type set description = 'Lessor Long Term Receivable Debit'
where TRANS_TYPE = 4003;

update je_trans_type set description = 'Lessor Unguaranteed Residual Debit'
where TRANS_TYPE = 4004;

update je_trans_type set description = 'Lessor Selling Profit Credit'
where TRANS_TYPE = 4005;

update je_trans_type set description = 'Lessor Selling Loss Debit'
where TRANS_TYPE = 4006;

update je_trans_type set description = 'Lessor Capitalized Initial Direct Cost Credit'
where TRANS_TYPE = 4007;

update je_trans_type set description = 'Lessor Initial Direct Cost Expense Debit'
where TRANS_TYPE = 4008;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4153, 0, 2017, 3, 0, 0, 50376, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050376_lessor_01_lsr_ilr_je_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 