/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_proptax_updates.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/19/2012 Stephen Wicks  Point Release
||============================================================================
*/

--
-- Set the "Is On Table" column for all Property Tax import columns that do not have it set.
--

-- Ledger/Preallo
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (1, 9)
   and COLUMN_NAME not in ('assessor_id', 'county_id');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (1, 9)
   and COLUMN_NAME in ('assessor_id', 'county_id');

-- Statistics
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (2, 3)
   and COLUMN_NAME not in ('assessor_id',
                           'county_id',
                           'parcel_type_id',
                           'prop_tax_company_id',
                           'state_id',
                           'tax_district_id');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (2, 3)
   and COLUMN_NAME in ('assessor_id',
                       'county_id',
                       'parcel_type_id',
                       'prop_tax_company_id',
                       'state_id',
                       'tax_district_id');

-- Assessments
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (11)
   and COLUMN_NAME not in
       ('county_id', 'parcel_type_id', 'prop_tax_company_id', 'state_id', 'tax_district_id');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (11)
   and COLUMN_NAME in
       ('county_id', 'parcel_type_id', 'prop_tax_company_id', 'state_id', 'tax_district_id');

-- Authority/District
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (13)
   and COLUMN_NAME not in ('county_id', 'state_id');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (13)
   and COLUMN_NAME in ('county_id', 'state_id');

-- CWIP Types
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (12)
   and COLUMN_NAME not in ('company_id');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (12)
   and COLUMN_NAME in ('company_id');

-- Levy Rates
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (5, 26)
   and COLUMN_NAME not in
       ('county_id', 'state_id', 'prop_tax_company_id', 'statement_group_id', 'tax_district_id');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (5, 26)
   and COLUMN_NAME in
       ('county_id', 'state_id', 'prop_tax_company_id', 'statement_group_id', 'tax_district_id');

-- Adjust Ledger/Preallo
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (8, 10)
   and COLUMN_NAME in ('additions_adjustment',
                       'adjustment_notes',
                       'beg_bal_adjustment',
                       'cwip_adjustment',
                       'end_bal_adjustment',
                       'property_tax_adjust_id',
                       'property_tax_ledger_id',
                       'preallo_ledger_id',
                       'reserve_adjustment',
                       'retirements_adjustment',
                       'tax_basis_adjustment',
                       'transfers_adjustment',
                       'tax_year');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (8, 10)
   and COLUMN_NAME not in ('additions_adjustment',
                           'adjustment_notes',
                           'beg_bal_adjustment',
                           'cwip_adjustment',
                           'end_bal_adjustment',
                           'property_tax_adjust_id',
                           'property_tax_ledger_id',
                           'preallo_ledger_id',
                           'reserve_adjustment',
                           'retirements_adjustment',
                           'tax_basis_adjustment',
                           'transfers_adjustment',
                           'tax_year');

-- Bill Lines/Bills
update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 1
 where IMPORT_TYPE_ID in (21, 20)
   and COLUMN_NAME not in ('assessor_id', 'prop_tax_company_id');

update PWRPLANT.PP_IMPORT_COLUMN
   set IS_ON_TABLE = 0
 where IMPORT_TYPE_ID in (21, 20)
   and COLUMN_NAME in ('assessor_id', 'prop_tax_company_id');

-- Remaining (for types that have all fields on table) - Esc Values, Reserve Fctrs, Update Parcels
update PWRPLANT.PP_IMPORT_COLUMN set IS_ON_TABLE = 1 where IMPORT_TYPE_ID in (4, 28, 7);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (267, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_proptax_updates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
