/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045320_cpr_02_add_ignore_for_106_to_pp_columns_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.4.0 01/14/2016 Charlie Shilling add new column to powerplant_columns so that it can be edited through the tables module
||============================================================================
*/
INSERT INTO powerplant_columns (column_name, table_name, dropdown_name, pp_edit_type_id, description, column_rank)
VALUES ('ignore_for_106_add','class_code','yes_no','p','Ignore for 106 Additions', 10);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3028, 0, 2015, 1, 4, 0, 045320, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.4.0_maint_045320_cpr_02_add_ignore_for_106_to_pp_columns_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;