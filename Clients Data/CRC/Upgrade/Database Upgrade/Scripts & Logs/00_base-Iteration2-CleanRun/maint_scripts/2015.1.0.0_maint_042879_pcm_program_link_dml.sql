/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042879_pcm_program_link_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/12/2015 Sarah Byers      Allow for opening non-response/child windows
||============================================================================
*/
-- Populate all existing records with 0
update ppbase_workspace_links
	set linked_window_opensheet = 0;

-- PCM Program link options
delete from ppbase_workspace_links
 where module = 'pcm'
	and linked_window = 'w_budget_select_tabs';

insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_window, linked_window_label, linked_window_opensheet)
select module, workspace_identifier, 2, 'w_budget_select_tabs', 'Programs Search', 1
  from ppbase_workspace
 where workspace_identifier in ('fp_search', 'wo_search');

-- PCM Maint workspaces links
insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_workspace_identifier)
select module, workspace_identifier, 1, decode(substr(workspace_identifier,1,2),'fp', 'fp_search', 'wo', 'wo_search')
  from ppbase_workspace
 where workspace_identifier not in ('fp_search', 'wo_search')
	and (workspace_identifier like 'fp%' or workspace_identifier like 'wo%')
	and workspace_uo_name like 'uo_pcm_maint%';

insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_window, linked_window_label, linked_window_opensheet)
select module, workspace_identifier, 2, 'w_budget_select_tabs', 'Programs Search', 1
  from ppbase_workspace
 where workspace_identifier not in ('fp_search', 'wo_search')
	and (workspace_identifier like 'fp%' or workspace_identifier like 'wo%')
	and workspace_uo_name like 'uo_pcm_maint%';

insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_workspace_identifier)
select module, workspace_identifier, 3, decode(substr(workspace_identifier,1,2),'fp', 'fp_maint_job_tasks', 'wo', 'wo_maint_job_tasks')
  from ppbase_workspace
 where workspace_identifier not in ('fp_search', 'wo_search', 'fp_maint_job_tasks', 'wo_maint_job_tasks')
	and (workspace_identifier like 'fp%' or workspace_identifier like 'wo%')
	and workspace_uo_name like 'uo_pcm_maint%';

insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_workspace_identifier)
select module, workspace_identifier, 4, 'rpt_reports'
  from ppbase_workspace
 where workspace_identifier not in ('fp_search', 'wo_search')
	and (workspace_identifier like 'fp%' or workspace_identifier like 'wo%')
	and workspace_uo_name like 'uo_pcm_maint%';

update ppbase_workspace_links
	set link_order = 3
 where workspace_identifier in ('fp_maint_job_tasks', 'wo_maint_job_tasks')
	and link_order = 4;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2289, 0, 2015, 1, 0, 0, 042879, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042879_pcm_program_link_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;