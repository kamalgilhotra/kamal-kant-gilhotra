/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi7.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/03/2013 Brandon Beck   Point release
||============================================================================
*/

create table LS_PEND_SET_OF_BOOKS
(
 LS_PEND_TRANS_ID number(22,0),
 SET_OF_BOOKS_ID  number(22,0),
 POSTING_AMOUNT   number(22,2),
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table LS_PEND_SET_OF_BOOKS
   add constraint LS_PEND_SET_OF_BOOKS_PK
       primary key (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID)
       using index tablespace PWRPLANT_IDX;

create table LS_PEND_SET_OF_BOOKS_ARC
(
 LS_PEND_TRANS_ID number(22,0),
 SET_OF_BOOKS_ID  number(22,0),
 POSTING_AMOUNT   number(22,2),
 TIME_STAMP       date,
 USER_ID          varchar2(18)
);

alter table LS_PEND_SET_OF_BOOKS_ARC
   add constraint LS_PEND_SET_OF_BOOKS_ARC_PK
       primary key (LS_PEND_TRANS_ID, SET_OF_BOOKS_ID)
       using index tablespace PWRPLANT_IDX;

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'reporting_query', 'Query', 'uo_ls_querycntr_wksp', null);

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where MODULE = 'LESSEE'
   and MENU_LEVEL = 1
   and ITEM_ORDER >= 6;

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'reporting_query', 1, 6, 'Query', null, null, 'reporting_query', 1);

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = null
 where MENU_IDENTIFIER = 'admin_system_controls'
   and MODULE = 'LESSEE';

update PPBASE_WORKSPACE
   set LABEL = 'Table Maintenance', WORKSPACE_IDENTIFIER = 'admin_table_maint',
       WORKSPACE_UO_NAME = 'uo_ls_admincntr_wksp_table_maint'
 where WORKSPACE_IDENTIFIER = 'admin_system_controls'
   and MODULE = 'LESSEE';

update PPBASE_MENU_ITEMS
   set LABEL = 'Table Maintenance', MENU_IDENTIFIER = 'admin_table_maint',
       WORKSPACE_IDENTIFIER = 'admin_table_maint'
 where MENU_IDENTIFIER = 'admin_system_controls'
   and MODULE = 'LESSEE';

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM)
values
   ('ls_lease_cap_type', 's', 'Lease Cap Type', '<Long>', 'lease', null, 'lease', 'lease');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('ls_lease_cap_type_id', 'ls_lease_cap_type', null, 'e', null, 'Lease Cap Type ID', null, 0,
    null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('description', 'ls_lease_cap_type', null, 'e', null, 'Description', null, 1, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('long_description', 'ls_lease_cap_type', null, 'e', null, 'Long Description', null, 2, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('book_summary_id', 'ls_lease_cap_type', 'book_summary', 'p', null, 'Book Summary', null, 3,
    null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('is_om', 'ls_lease_cap_type', 'yes_no', 'p', null, 'Is OM', null, 4, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('time_stamp', 'ls_lease_cap_type', null, 'e', null, 'Time Stamp', null, 2, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('user_id', 'ls_lease_cap_type', null, 'e', null, 'User', null, 2, null, 0);

--Remove pp import column / tempfields where lease_id, ilr_id, etc.
delete from PP_IMPORT_COLUMN_LOOKUP
 where (IMPORT_TYPE_ID, COLUMN_NAME) in
       (select IMPORT_TYPE_ID, COLUMN_NAME
          from PP_IMPORT_COLUMN
         where LOWER(COLUMN_NAME) in ('lessor_id', 'vendor_id', 'lease_id', 'ilr_id', 'ls_asset_id')
           and IMPORT_COLUMN_NAME is null);

delete from PP_IMPORT_TEMPLATE_FIELDS
 where (IMPORT_TYPE_ID, COLUMN_NAME) in
       (select IMPORT_TYPE_ID, COLUMN_NAME
          from PP_IMPORT_COLUMN
         where LOWER(COLUMN_NAME) in ('lessor_id', 'vendor_id', 'lease_id', 'ilr_id', 'ls_asset_id')
           and IMPORT_COLUMN_NAME is null);

delete from PP_IMPORT_COLUMN
 where LOWER(COLUMN_NAME) in ('lessor_id', 'vendor_id', 'lease_id', 'ilr_id', 'ls_asset_id')
   and IMPORT_COLUMN_NAME is null;

--clean up multiple lookups
update PP_IMPORT_TEMPLATE_FIELDS A
   set IMPORT_LOOKUP_ID =
        (select max(IMPORT_LOOKUP_ID)
           from PP_IMPORT_LOOKUP B
          where B.DESCRIPTION = (select C.DESCRIPTION
                                   from PP_IMPORT_LOOKUP C
                                  where C.IMPORT_LOOKUP_ID = A.IMPORT_LOOKUP_ID))
 where (select C.DESCRIPTION from PP_IMPORT_LOOKUP C where C.IMPORT_LOOKUP_ID = A.IMPORT_LOOKUP_ID) like
       'Lessee%';

delete from PP_IMPORT_COLUMN_LOOKUP Z
 where Z.IMPORT_LOOKUP_ID in
       (select A.IMPORT_LOOKUP_ID
          from PP_IMPORT_LOOKUP A
         where A.DESCRIPTION like 'Lessee%'
           and exists (select 1
                  from PP_IMPORT_LOOKUP B
                 where B.DESCRIPTION = A.DESCRIPTION
                   and B.IMPORT_LOOKUP_ID > A.IMPORT_LOOKUP_ID)
           and not exists (select 1
                  from PP_IMPORT_TEMPLATE_FIELDS B
                 where B.IMPORT_LOOKUP_ID = A.IMPORT_LOOKUP_ID));

delete from PP_IMPORT_LOOKUP A
 where A.DESCRIPTION like 'Lessee%'
   and exists (select 1
          from PP_IMPORT_LOOKUP B
         where B.DESCRIPTION = A.DESCRIPTION
           and B.IMPORT_LOOKUP_ID > A.IMPORT_LOOKUP_ID)
   and not exists
 (select 1 from PP_IMPORT_TEMPLATE_FIELDS B where B.IMPORT_LOOKUP_ID = A.IMPORT_LOOKUP_ID);

--Clean up descriptions on lookups
update PP_IMPORT_LOOKUP
   set DESCRIPTION = INITCAP(replace(LOOKUP_TABLE_NAME, '_', ' ')) || '.' ||
                      INITCAP(replace(LOOKUP_COLUMN_NAME, '_', ' '))
 where DESCRIPTION like 'Lessee%';

--Make unique identifiers required
update PP_IMPORT_COLUMN
   set IS_REQUIRED = 1
 where COLUMN_NAME like 'unique%identifier'
   and IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Add more lookups where needed
update PP_IMPORT_LOOKUP
   set COLUMN_NAME = 'ls_lease_cap_type_id'
 where COLUMN_NAME = 'lease_cap_type_id';

insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Purchase Option Type.Long Description', 'Ls Purchase Option Type.Long Description', 'purchase_option_type_id', '( select b.purchase_option_type_id from ls_purchase_option_type b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_purchase_option_type', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Asset.Long Description', 'Ls Asset.Long Description', 'ls_asset_id', '( select b.ls_asset_id from ls_asset b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_asset', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Asset.Leased Asset Number', 'Ls Asset.Leased Asset Number', 'ls_asset_id', '( select b.ls_asset_id from ls_asset b where upper( trim( <importfield> ) ) = upper( trim( b.leased_asset_number ) ) )', 0,'ls_asset', 'leased_asset_number', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Vendor.Long Description', 'Ls Vendor.Long Description', 'vendor_id', '( select b.vendor_id from ls_vendor b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_vendor', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Vendor.External Vendor Number', 'Ls Vendor.External Vendor Number', 'vendor_id', '( select b.vendor_id from ls_vendor b where upper( trim( <importfield> ) ) = upper( trim( b.external_vendor_number ) ) )', 0,'ls_vendor', 'external_vendor_number', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Lessor.Long Description', 'Ls Lessor.Long Description', 'lessor_id', '( select b.lessor_id from ls_lessor b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_lessor', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Lessor.External Lessor Number', 'Ls Lessor.External Lessor Number', 'lessor_id', '( select b.lessor_id from ls_lessor b where upper( trim( <importfield> ) ) = upper( trim( external_lessor_number ) ) )', 0,'ls_lessor', 'external_lessor_number', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Renewal Option Type.Long Description', 'Ls Renewal Option Type.Long Description', 'renewal_option_type_id', '( select b.renewal_option_type_id from ls_renewal_option_type b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_renewal_option_type', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Lease Type.Long Description', 'Ls Lease Type.Long Description', 'lease_type_id', '( select b.lease_type_id from ls_lease_type b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_lease_type', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Ilr Group.Long Description', 'Ls Ilr Group.Long Description', 'ilr_group_id', '( select b.ilr_group_id from ls_ilr_group b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_ilr_group', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Lease.Long Description', 'Ls Lease.Long Description', 'lease_id', '( select b.lease_id from ls_lease b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_lease', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Lease.Lease Number', 'Ls Lease.Lease Number', 'lease_id', '( select b.lease_id from ls_lease b where upper( trim( <importfield> ) ) = upper( trim( b.lease_number ) ) )', 0,'ls_lease', 'lease_number', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Cancelable Type.Long Description', 'Ls Cancelable Type.Long Description', 'cancelable_type_id', '( select b.cancelable_type_id from ls_cancelable_type b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_cancelable_type', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Lease Cap Type.Long Description', 'Ls Lease Cap Type.Long Description', 'ls_lease_cap_type_id', '( select b.ls_lease_cap_type_id from ls_lease_cap_type b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_lease_cap_type', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Payment Freq.Long Description', 'Ls Payment Freq.Long Description', 'payment_freq_id', '( select b.payment_freq_id from ls_payment_freq b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_payment_freq', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Ilr.Description', 'Ls Ilr.Description', 'ilr_id', '( select b.ilr_id from ls_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )', 0,'ls_Ilr', 'description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Ilr.Long Description', 'Ls Ilr.Long Description', 'ilr_id', '( select b.ilr_id from ls_ilr b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_Ilr', 'long_description', '' from PP_IMPORT_LOOKUP ;
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) select max(IMPORT_LOOKUP_ID)+1, sysdate, user, 'Ls Lease Group.Long Description', 'Ls Lease Group.Long Description', 'lease_group_id', '( select b.lease_group_id from ls_lease_group b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )', 0,'ls_lease_group', 'long_description', '' from PP_IMPORT_LOOKUP ;


--Update PPBASE_WORKSPACE for new payment approvals UO
update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_pymntcntr_wksp_approvals_open'
 where WORKSPACE_IDENTIFIER = 'approval_payments'
   and MODULE = 'LESSEE';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_importcntr_wksp'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'menu_wksp_import';

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'menu_wksp_import'
 where MODULE = 'LESSEE'
   and MENU_IDENTIFIER = 'menu_wksp_import';

delete from PPBASE_MENU_ITEMS
 where MODULE = 'LESSEE'
   and PARENT_MENU_IDENTIFIER = 'menu_wksp_import';

update PPBASE_MENU_ITEMS
   set LABEL = 'Import'
 where MODULE = 'LESSEE'
   and MENU_IDENTIFIER = 'menu_wksp_import';

update PPBASE_WORKSPACE
   set LABEL = 'Import'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'menu_wksp_import';

--Make field id's sequential
update PP_IMPORT_TEMPLATE_FIELDS A
   set A.FIELD_ID =
        (select count(1)
           from PP_IMPORT_TEMPLATE_FIELDS B
          where B.IMPORT_TEMPLATE_ID = A.IMPORT_TEMPLATE_ID
            and B.FIELD_ID <= A.FIELD_ID)
 where IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Now make UNIQUE IDENTIFIER fields the first field
update PP_IMPORT_TEMPLATE_FIELDS A
   set A.FIELD_ID = A.FIELD_ID + 1
 where A.IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update PP_IMPORT_TEMPLATE_FIELDS A
   set A.FIELD_ID = 1
 where COLUMN_NAME like 'unique%identifier'
   and A.IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

--Consolidate ILR init menu items
update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'initiate_ilr'
 where MODULE = 'LESSEE'
   and MENU_IDENTIFIER = 'initiate_ilr';

update PPBASE_WORKSPACE
   set LABEL = 'Initiate ILR', WORKSPACE_UO_NAME = 'uo_ls_ilrcntr_wksp_init_lease'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'initiate_ilr';

delete from PPBASE_MENU_ITEMS
 where MODULE = 'LESSEE'
   and PARENT_MENU_IDENTIFIER = 'initiate_ilr';

delete from PPBASE_WORKSPACE
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER in ('initiate_ilr_new', 'initiate_ilr_from_assets');

--IMPORTS: Make field id's sequential
update PP_IMPORT_TEMPLATE_FIELDS A
   set A.FIELD_ID =
        (select count(1)
           from PP_IMPORT_TEMPLATE_FIELDS B
          where B.IMPORT_TEMPLATE_ID = A.IMPORT_TEMPLATE_ID
            and B.FIELD_ID <= A.FIELD_ID)
 where IMPORT_TYPE_ID in
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'ls_import%');

update LS_LEASE
   set LEASE_GROUP_ID = null
 where not exists (select 1 from LS_LEASE_GROUP where LEASE_GROUP_ID = LS_LEASE.LEASE_GROUP_ID);

alter table LS_LEASE
   add constraint FK_LEASE_LEASE_GROUP
       foreign key (LEASE_GROUP_ID)
       references LS_LEASE_GROUP (LEASE_GROUP_ID);

update LS_ILR
   set ILR_GROUP_ID = null
 where not exists (select 1 from LS_ILR_GROUP where ILR_GROUP_ID = LS_ILR.ILR_GROUP_ID);

alter table LS_ILR
   add constraint FK_ILR_ILR_GROUP
       foreign key (ILR_GROUP_ID)
       references LS_ILR_GROUP (ILR_GROUP_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (567, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi7.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
