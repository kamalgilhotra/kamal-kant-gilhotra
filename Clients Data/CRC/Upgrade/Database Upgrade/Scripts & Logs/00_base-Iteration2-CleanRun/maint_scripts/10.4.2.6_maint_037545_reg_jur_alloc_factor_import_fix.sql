/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037545_reg_jur_alloc_factor_import_fix.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.6 06/10/2014 Kyle Peterson
||============================================================================
*/

update PP_IMPORT_COLUMN
   set IS_REQUIRED = 1
 where COLUMN_NAME = 'annual_reg_factor'
   and IMPORT_TYPE_ID = 150;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1206, 0, 10, 4, 2, 6, 37545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_037545_reg_jur_alloc_factor_import_fix.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;