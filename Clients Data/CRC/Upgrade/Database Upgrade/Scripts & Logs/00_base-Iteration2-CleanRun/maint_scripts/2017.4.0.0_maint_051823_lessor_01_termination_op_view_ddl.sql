/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051823_lessor_01_termination_op_view_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/11/2018 Anand R          PP-51823 Use rate stored in termination table
||============================================================================
*/

drop view V_LSR_TERMINTION_OP_VW;

CREATE OR REPLACE VIEW V_LSR_TERMINATION_OP_VW AS
with cur AS (
  SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id,
    contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code, 1 historic_rate
  FROM currency contract_cur
  UNION
  SELECT 2, company_cur.currency_id,
    company_cur.currency_display_symbol, company_cur.iso_code, 0
  FROM currency company_cur
)
SELECT
 lito.ilr_id,
 lito.termination_date,
 lito.rent_balance * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) rent_balance,
 lito.deferred_costs * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) deferred_costs,
 lito.penalty_other_payment * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) penalty_other_payment,
 lito.income_gainloss * decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) income_gainloss,
 lito.rent_balance_acct_id rent_balance_acct_id,
 lito.deferred_costs_acct_id deferred_costs_acct_id,
 lito.penalty_other_payment_acct_id penalty_other_payment_acct_id,
 lito.income_gainloss_acct_id income_gainloss_acct_id,
 lito.termination_source termination_source,
 lito.comments comments,
 ilr.company_id company_id,
 cur.ls_cur_type ls_cur_type,
 lease.contract_currency_id,
 cur.currency_id display_currency_id,
 cs.currency_id company_currency_id,
 decode(ls_cur_type, 2, nvl(lito.termination_exchange_rate, historic_rate), 1) rate,
 cur.iso_code,
 cur.currency_display_symbol
 FROM lsr_ilr_termination_op lito
 INNER JOIN lsr_ilr ilr
    ON ilr.ilr_id = lito.ilr_id
 INNER JOIN currency_schema cs
    ON ilr.company_id = cs.company_id
 INNER JOIN lsr_lease lease
    ON ilr.lease_id = lease.lease_id
 INNER JOIN cur
    ON (cur.ls_cur_type = 1 AND
       cur.currency_id = lease.contract_currency_id)
    OR (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
 WHERE cs.currency_type_id = 1;
 
 --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7923, 0, 2017, 4, 0, 0, 51823, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051823_lessor_01_termination_op_view_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
 
