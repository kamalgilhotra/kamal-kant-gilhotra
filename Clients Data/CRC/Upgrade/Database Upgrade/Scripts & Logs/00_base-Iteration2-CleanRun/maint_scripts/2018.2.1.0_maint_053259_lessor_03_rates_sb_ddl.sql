/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053259_lessor_03_rates_sb_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 03/22/2019 B. Beck    	Remove lsr_ilr_rates, rename conversion table.
||										CREATE views
||============================================================================
*/

-- Drop Current table
drop table lsr_ilr_rates;


-- Rename conversion table
alter table lsr_ilr_rates_conversion rename to lsr_ilr_rates;


-- Rename conversion table pk
alter table lsr_ilr_rates rename constraint pk_lsr_ilr_rates_conv to pk_lsr_ilr_rates;

alter index pk_lsr_ilr_rates_conv rename to pk_lsr_ilr_rates;


-- add foreign keys back
alter table lsr_ilr_rates 
add constraint lsr_ilr_rates_ilr_rev_fk
foreign key ( ilr_id, revision ) 
references lsr_ilr_options (ilr_id, revision);

alter table lsr_ilr_rates 
add constraint lsr_ilr_rates_rate_type_fk
foreign key ( rate_type_id ) 
references lsr_ilr_rate_types (rate_type_id);


	
-- add a foreign key to set of books
alter table lsr_ilr_rates
add constraint lsr_ilr_rates_books_fk
foreign key (set_of_books_id)
references set_of_books (set_of_books_id);



CREATE OR REPLACE VIEW v_lsr_ilr_rates_compounded AS
SELECT  lsr_ilr_rates.ilr_id,
        lsr_ilr_rates.revision,
		lsr_ilr_rates.set_of_books_id,
        lsr_ilr_rates.rate_type_id,
        lsr_ilr_rate_types.DESCRIPTION AS rate_type_description,
        pkg_financial_calcs.f_annual_to_implicit_rate(lsr_ilr_rates.rate,2) AS rate_implicit,
        rate AS annual_discount_rate
FROM lsr_ilr_rates
JOIN lsr_ilr_rate_types ON lsr_ilr_rates.rate_type_id = lsr_ilr_rate_types.rate_type_id;


--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16166, 0, 2018, 2, 1, 0, 53259 , 'C:\PlasticWks\powerplant\sql\maint_scripts','2018.2.1.0_maint_053259_lessor_03_rates_sb_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;