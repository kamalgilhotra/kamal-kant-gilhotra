/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008322_prov_TAX_ACCRUAL_M_ROLLUP_VIEW.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/09/2012 Blake Andrews  Point Release
||============================================================================
*/

create or replace view TAX_ACCRUAL_M_ROLLUP_VIEW
as
select ROLLUP.TAX_ACCRUAL_ROLLUP_ID               TAX_ACCRUAL_ROLLUP_ID,
       ROLLUP.DESCRIPTION                         ROLLUP_DESCRIPTION,
       ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_DETAIL_ID TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       ROLLUP_DETAIL.DESCRIPTION                  ROLLUP_DETAIL_DESCRIPTION,
       ROLLUP_DETAIL.HIDE_DETAIL                  HIDE_DETAIL,
       ROLLUP_DETAIL.IS_DISCRETE                  IS_DISCRETE,
       M_ROLLUPS.M_ITEM_ID                        M_ITEM_ID
  from TAX_ACCRUAL_ROLLUP         rollup,
       TAX_ACCRUAL_ROLLUP_DETAIL  ROLLUP_DETAIL,
       TAX_ACCRUAL_M_ITEM_ROLLUPS M_ROLLUPS
 where ROLLUP.TAX_ACCRUAL_ROLLUP_ID = ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_ID
   and ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_DETAIL_ID = M_ROLLUPS.TAX_ACCRUAL_ROLLUP_DETAIL_ID
union all((select ROLLUP.TAX_ACCRUAL_ROLLUP_ID TAX_ACCRUAL_ROLLUP_ID,
                  ROLLUP.DESCRIPTION ROLLUP_DESCRIPTION,
                  0 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
                  'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
                  0 HIDE_DETAIL,
                  0 IS_DISCRETE,
                  M_MASTER.M_ITEM_ID M_ITEM_ID
             from TAX_ACCRUAL_ROLLUP rollup, TAX_ACCRUAL_M_MASTER M_MASTER
           union all
           select ROLLUP.TAX_ACCRUAL_ROLLUP_ID TAX_ACCRUAL_ROLLUP_ID,
                  ROLLUP.DESCRIPTION ROLLUP_DESCRIPTION,
                  0 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
                  'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
                  0 HIDE_DETAIL,
                  0 IS_DISCRETE,
                  -1 M_ITEM_ID
             from TAX_ACCRUAL_ROLLUP rollup)
minus
select ROLLUP.TAX_ACCRUAL_ROLLUP_ID TAX_ACCRUAL_ROLLUP_ID,
       ROLLUP.DESCRIPTION ROLLUP_DESCRIPTION,
       0 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
       0 HIDE_DETAIL,
       0 IS_DISCRETE,
       M_ROLLUPS.M_ITEM_ID M_ITEM_ID
  from TAX_ACCRUAL_ROLLUP         rollup,
       TAX_ACCRUAL_ROLLUP_DETAIL  ROLLUP_DETAIL,
       TAX_ACCRUAL_M_ITEM_ROLLUPS M_ROLLUPS
 where ROLLUP.TAX_ACCRUAL_ROLLUP_ID = ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_ID
   and ROLLUP_DETAIL.TAX_ACCRUAL_ROLLUP_DETAIL_ID = M_ROLLUPS.TAX_ACCRUAL_ROLLUP_DETAIL_ID)
union all
select -1000 TAX_ACCRUAL_ROLLUP_ID,
       'M Type' ROLLUP_DESCRIPTION,
       M_MASTER.M_TYPE_ID TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       M_TYPE.DESCRIPTION ROLLUP_DETAIL_DESCRIPTION,
       HIDE_DETAIL_ON_REPORT HIDE_DETAIL,
       IS_DISCRETE IS_DISCRETE,
       M_MASTER.M_ITEM_ID M_ITEM_ID
  from TAX_ACCRUAL_M_MASTER M_MASTER, TAX_ACCRUAL_M_TYPE M_TYPE
 where M_MASTER.M_TYPE_ID = M_TYPE.M_TYPE_ID
union all
select -1000 TAX_ACCRUAL_ROLLUP_ID,
       'M Type' ROLLUP_DESCRIPTION,
       -10 TAX_ACCRUAL_ROLLUP_DETAIL_ID,
       'No Rollup' ROLLUP_DETAIL_DESCRIPTION,
       HIDE_DETAIL_ON_REPORT HIDE_DETAIL,
       IS_DISCRETE IS_DISCRETE,
       -1 M_ITEM_ID
  from TAX_ACCRUAL_M_TYPE
 where M_TYPE_ID = 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (250, 0, 10, 4, 0, 0, 8322, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_008322_prov_TAX_ACCRUAL_M_ROLLUP_VIEW.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;