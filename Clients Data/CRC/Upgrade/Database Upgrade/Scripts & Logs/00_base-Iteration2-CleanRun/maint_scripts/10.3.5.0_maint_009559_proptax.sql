/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_009559_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    04/27/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Add a field to PT Import Type indicating whether import types should only be available to clients with the asset shell.
-- This will be used for imports that are loading data into the asset system, and thus should not be used by clients with the
-- PowerPlan Asset Management system.
--
alter table PT_IMPORT_TYPE add ASSET_SYSTEM_DATA number(22,0);
update PT_IMPORT_TYPE set ASSET_SYSTEM_DATA = 0 where ASSET_SYSTEM_DATA is null;
alter table PT_IMPORT_TYPE modify ASSET_SYSTEM_DATA not null;

--
-- Create a system control that indicates whether the asset data imports should be available to users.
--
insert into PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Admin Center - Import - Allow Asset System Data Imports', sysdate, user, 'Indicates whether users are allowed to import asset system data, such as asset locations, major locations, assets, etc.  This option should only be ''Yes'' if the client does not use the PowerPlan Asset Management system and is using Property Tax as a standalone.', 1, 'No', null, 1 );

--
-- Create the new import types.
--

-- Assessor
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 29, sysdate, user, 'Add : Assessors', 'Import Assessors', 'pt_import_assessor', 'pt_import_assessor_archive', null, 1, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'name_line_1', sysdate, user, 'Name Line 1', '', 0, 1, 'varchar2(50)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'name_line_2', sysdate, user, 'Name Line 2', '', 0, 1, 'varchar2(50)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'name_line_3', sysdate, user, 'Name Line 3', '', 0, 1, 'varchar2(50)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'address_line_1', sysdate, user, 'Address Line 1', '', 0, 1, 'varchar2(50)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'address_line_2', sysdate, user, 'Address Line 2', '', 0, 1, 'varchar2(50)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'address_line_3', sysdate, user, 'Address Line 3', '', 0, 1, 'varchar2(50)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'city', sysdate, user, 'City', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'state', sysdate, user, 'State', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'zip_code', sysdate, user, 'Zip Code', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'phone_1', sysdate, user, 'Phone Number 1', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'phone_2', sysdate, user, 'Phone Number 2', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'ext_assessor_code', sysdate, user, 'External Assessor Code', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 29, 'notes', sysdate, user, 'Notes', '', 0, 1, 'varchar2(2000)', '', 1 );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 29, 67, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 29, 69, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 29, 68, sysdate, user );

create table PWRPLANT.PT_IMPORT_ASSESSOR
(
 IMPORT_RUN_ID       number(22,0)   not null,
 LINE_ID             number(22,0)   not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 DESCRIPTION         varchar2(254),
 NAME_LINE_1         varchar2(254),
 NAME_LINE_2         varchar2(254),
 NAME_LINE_3         varchar2(254),
 ADDRESS_LINE_1      varchar2(254),
 ADDRESS_LINE_2      varchar2(254),
 ADDRESS_LINE_3      varchar2(254),
 CITY                varchar2(254),
 STATE               varchar2(254),
 ZIP_CODE            varchar2(254),
 PHONE_1             varchar2(254),
 PHONE_2             varchar2(254),
 EXT_ASSESSOR_CODE   varchar2(254),
 NOTES               varchar2(2000),
 ASSESSOR_XLATE      varchar2(254),
 ASSESSOR_ID         number(22,0),
 IS_MODIFIED         number(22,0),
 ERROR_MESSAGE       varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_ASSESSOR
   add constraint PT_IMPORT_ASSR_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_ASSESSOR
   add constraint PT_IMPORT_ASSR_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_ASSESSOR_ARCHIVE
(
 IMPORT_RUN_ID     number(22,0)   not null,
 LINE_ID           number(22,0)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(254),
 NAME_LINE_1       varchar2(254),
 NAME_LINE_2       varchar2(254),
 NAME_LINE_3       varchar2(254),
 ADDRESS_LINE_1    varchar2(254),
 ADDRESS_LINE_2    varchar2(254),
 ADDRESS_LINE_3    varchar2(254),
 CITY              varchar2(254),
 STATE             varchar2(254),
 ZIP_CODE          varchar2(254),
 PHONE_1           varchar2(254),
 PHONE_2           varchar2(254),
 EXT_ASSESSOR_CODE varchar2(254),
 NOTES             varchar2(2000),
 ASSESSOR_XLATE    varchar2(254),
 ASSESSOR_ID       number(22,0)
);

alter table PWRPLANT.PT_IMPORT_ASSESSOR_ARCHIVE
   add constraint PT_IMPORT_ASSR_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_ASSESSOR_ARCHIVE
   add constraint PT_IMPORT_ASSR_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Type Codes
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 30, sysdate, user, 'Add : Type Codes', 'Import Prop Tax Type Codes', 'pt_import_type_code', 'pt_import_type_code_archive', null, 1, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 30, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 30, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 30, 'external_code', sysdate, user, 'External Code', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 30, 'parent_type_code_id', sysdate, user, 'Parent Type Code', 'parent_type_code_xlate', 0, 1, 'number(22,0)', 'prop_tax_type_code', 1 );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 30, 74, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 30, 76, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 30, 75, sysdate, user );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 30, 'parent_type_code_id', 74, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 30, 'parent_type_code_id', 76, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 30, 'parent_type_code_id', 75, sysdate, user );

create table PWRPLANT.PT_IMPORT_TYPE_CODE
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 DESCRIPTION            varchar2(254),
 LONG_DESCRIPTION       varchar2(254),
 EXTERNAL_CODE          varchar2(254),
 PARENT_TYPE_CODE_XLATE varchar2(254),
 PARENT_TYPE_CODE_ID    number(22,0),
 TYPE_CODE_XLATE        varchar2(254),
 TYPE_CODE_ID           number(22,0),
 IS_MODIFIED            number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_TYPE_CODE
   add constraint PT_IMPORT_TC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_TYPE_CODE
   add constraint PT_IMPORT_TC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_TYPE_CODE_ARCHIVE
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 DESCRIPTION            varchar2(254),
 LONG_DESCRIPTION       varchar2(254),
 EXTERNAL_CODE          varchar2(254),
 PARENT_TYPE_CODE_XLATE varchar2(254),
 PARENT_TYPE_CODE_ID    number(22,0),
 TYPE_CODE_XLATE        varchar2(254),
 TYPE_CODE_ID           number(22,0)
);

alter table PWRPLANT.PT_IMPORT_TYPE_CODE_ARCHIVE
   add constraint PT_IMPORT_TC_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_TYPE_CODE_ARCHIVE
   add constraint PT_IMPORT_TC_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Tax Districts
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 31, sysdate, user, 'Add : Tax Districts', 'Import Prop Tax Districts', 'pt_import_tax_dist', 'pt_import_tax_dist_archive', null, 1, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'county_id', sysdate, user, 'County', 'county_xlate', 1, 2, 'char(18)', 'county', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'state_id', sysdate, user, 'State', 'state_xlate', 1, 1, 'char(18)', 'state', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'assignment_indicator', sysdate, user, 'Assignment Indicator', '', 0, 1, 'number(22,0)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'type_code_id', sysdate, user, 'Type Code', 'type_code_xlate', 0, 1, 'number(22,0)', 'prop_tax_type_code', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(100)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'grid_coordinate', sysdate, user, 'Grid Coordinate', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'tax_district_code', sysdate, user, 'Tax District Code', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'assessor_id', sysdate, user, 'Assessor', 'assessor_xlate', 0, 1, 'number(22,0)', 'pt_assessor', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'is_parcel_one_to_one', sysdate, user, 'Is District 1-to-1 with Parcel', 'is_parcel_one_to_one_xlate', 1, 1, 'number(22,0)', 'yes_no', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 31, 'use_composite_authority_yn', sysdate, user, 'Use Composite Authorities', 'use_composite_auth_xlate', 1, 1, 'number(22,0)', 'yes_no', 1 );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 79, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 85, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 82, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 105, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 107, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 106, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 80, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 86, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 83, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 81, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 87, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 84, sysdate, user );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'type_code_id', 74, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'type_code_id', 76, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'type_code_id', 75, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'assessor_id', 67, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'assessor_id', 68, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'assessor_id', 69, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'is_parcel_one_to_one', 77, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'is_parcel_one_to_one', 78, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'use_composite_authority_yn', 77, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 31, 'use_composite_authority_yn', 78, sysdate, user );

create table PWRPLANT.PT_IMPORT_TAX_DIST
(
 IMPORT_RUN_ID              number(22,0)   not null,
 LINE_ID                    number(22,0)   not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 STATE_XLATE                varchar2(254),
 STATE_ID                   char(18),
 COUNTY_XLATE               varchar2(254),
 COUNTY_ID                  char(18),
 ASSIGNMENT_INDICATOR       varchar2(254),
 TYPE_CODE_XLATE            varchar2(254),
 TYPE_CODE_ID               number(22,0),
 DESCRIPTION                varchar2(254),
 LONG_DESCRIPTION           varchar2(254),
 GRID_COORDINATE            varchar2(254),
 TAX_DISTRICT_CODE          varchar2(254),
 ASSESSOR_XLATE             varchar2(254),
 ASSESSOR_ID                number(22,0),
 IS_PARCEL_ONE_TO_ONE_XLATE varchar2(254),
 IS_PARCEL_ONE_TO_ONE       number(22,0),
 USE_COMPOSITE_AUTH_XLATE   varchar2(254),
 USE_COMPOSITE_AUTHORITY_YN number(22,0),
 TAX_DISTRICT_XLATE         varchar2(254),
 TAX_DISTRICT_ID            number(22,0),
 IS_MODIFIED                number(22,0),
 ERROR_MESSAGE              varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_TAX_DIST
   add constraint PT_IMPORT_TD_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_TAX_DIST
   add constraint PT_IMPORT_TD_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_TAX_DIST_ARCHIVE
(
 IMPORT_RUN_ID              number(22,0)   not null,
 LINE_ID                    number(22,0)   not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 STATE_XLATE                varchar2(254),
 STATE_ID                   char(18),
 COUNTY_XLATE               varchar2(254),
 COUNTY_ID                  char(18),
 ASSIGNMENT_INDICATOR       varchar2(254),
 TYPE_CODE_XLATE            varchar2(254),
 TYPE_CODE_ID               number(22,0),
 DESCRIPTION                varchar2(254),
 LONG_DESCRIPTION           varchar2(254),
 GRID_COORDINATE            varchar2(254),
 TAX_DISTRICT_CODE          varchar2(254),
 ASSESSOR_XLATE             varchar2(254),
 ASSESSOR_ID                number(22,0),
 IS_PARCEL_ONE_TO_ONE_XLATE varchar2(254),
 IS_PARCEL_ONE_TO_ONE       number(22,0),
 USE_COMPOSITE_AUTH_XLATE   varchar2(254),
 USE_COMPOSITE_AUTHORITY_YN number(22,0),
 TAX_DISTRICT_XLATE         varchar2(254),
 TAX_DISTRICT_ID            number(22,0)
);

alter table PWRPLANT.PT_IMPORT_TAX_DIST_ARCHIVE
   add constraint PT_IMPORT_TD_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_TAX_DIST_ARCHIVE
   add constraint PT_IMPORT_TD_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Minor Locations
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 32, sysdate, user, 'Add : Minor Locations', 'Import Minor Locations', 'pt_import_minor_loc', 'pt_import_minor_loc_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 32, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 32, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1 );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 130, sysdate, user, 'Minor Location.Description', 'The passed in value corresponds to the Minor Location: Description field.  Translate to the Minor Location ID using the Description column on the Minor Location table.', 'minor_location_id', '( select ml.minor_location_id from minor_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.description ) ) )', 0, 0, 'minor_location', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 131, sysdate, user, 'Minor Location.Long Description', 'The passed in value corresponds to the Minor Location: Long Description field.  Translate to the Minor Location ID using the Long Description column on the Minor Location table.', 'minor_location_id', '( select ml.minor_location_id from minor_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.long_description ) ) )', 0, 0, 'minor_location', 'long_description', '' );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 32, 130, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 32, 131, sysdate, user );

create table PWRPLANT.PT_IMPORT_MINOR_LOC
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(254),
 LONG_DESCRIPTION     varchar2(254),
 MINOR_LOCATION_XLATE varchar2(254),
 MINOR_LOCATION_ID    number(22,0),
 IS_MODIFIED          number(22,0),
 ERROR_MESSAGE        varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_MINOR_LOC
   add constraint PT_IMPORT_MINLOC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_MINOR_LOC
   add constraint PT_IMPORT_MINLOC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_MINOR_LOC_ARCHIVE
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(254),
 LONG_DESCRIPTION     varchar2(254),
 MINOR_LOCATION_XLATE varchar2(254),
 MINOR_LOCATION_ID    number(22,0)
);

alter table PWRPLANT.PT_IMPORT_MINOR_LOC_ARCHIVE
   add constraint PT_IMPORT_MINLOC_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_MINOR_LOC_ARCHIVE
   add constraint PT_IMPORT_MINLOC_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Location Types
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 33, sysdate, user, 'Add : Location Types', 'Import Location Types', 'pt_import_loc_type', 'pt_import_loc_type_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 33, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 33, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1 );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 132, sysdate, user, 'Location Type.Description', 'The passed in value corresponds to the Location Type: Description field.  Translate to the Location Type ID using the Description column on the Location Type table.', 'location_type_id', '( select lt.location_type_id from location_type lt where upper( trim( <importfield> ) ) = upper( trim( lt.description ) ) )', 0, 0, 'location_type', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 133, sysdate, user, 'Location Type.Long Description', 'The passed in value corresponds to the Location Type: Long Description field.  Translate to the Location Type ID using the Long Description column on the Location Type table.', 'location_type_id', '( select lt.location_type_id from location_type lt where upper( trim( <importfield> ) ) = upper( trim( lt.long_description ) ) )', 0, 0, 'location_type', 'long_description', '' );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 33, 132, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 33, 133, sysdate, user );

create table PWRPLANT.PT_IMPORT_LOC_TYPE
(
 IMPORT_RUN_ID       number(22,0)   not null,
 LINE_ID             number(22,0)   not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 DESCRIPTION         varchar2(254),
 LONG_DESCRIPTION    varchar2(254),
 LOCATION_TYPE_XLATE varchar2(254),
 LOCATION_TYPE_ID    number(22,0),
 IS_MODIFIED         number(22,0),
 ERROR_MESSAGE       varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_LOC_TYPE
   add constraint PT_IMPORT_LT_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_LOC_TYPE
   add constraint PT_IMPORT_LT_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_LOC_TYPE_ARCHIVE
(
 IMPORT_RUN_ID       number(22,0)   not null,
 LINE_ID             number(22,0)   not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 DESCRIPTION         varchar2(254),
 LONG_DESCRIPTION    varchar2(254),
 LOCATION_TYPE_XLATE varchar2(254),
 LOCATION_TYPE_ID    number(22,0)
);

alter table PWRPLANT.PT_IMPORT_LOC_TYPE_ARCHIVE
   add constraint PT_IMPORT_LT_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_LOC_TYPE_ARCHIVE
   add constraint PT_IMPORT_LT_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Divisions
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 34, sysdate, user, 'Add : Divisions', 'Import Divisions', 'pt_import_division', 'pt_import_division_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 34, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 34, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 0, 1, 'number(22,0)', 'business_segment', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 34, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 34, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1 );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 134, sysdate, user, 'Division.Description', 'The passed in value corresponds to the Division: Description field.  Translate to the Division ID using the Description column on the Division table.', 'division_id', '( select d.division_id from division d where upper( trim( <importfield> ) ) = upper( trim( d.description ) ) )', 0, 0, 'division', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 135, sysdate, user, 'Division.Long Description', 'The passed in value corresponds to the Division: Long Description field.  Translate to the Division ID using the Long Description column on the Division table.', 'division_id', '( select d.division_id from division d where upper( trim( <importfield> ) ) = upper( trim( d.long_description ) ) )', 0, 0, 'division', 'long_description', '' );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 34, 134, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 34, 135, sysdate, user );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 34, 'company_id', 19, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 34, 'company_id', 21, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 34, 'company_id', 20, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 34, 'bus_segment_id', 25, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 34, 'bus_segment_id', 26, sysdate, user );

create table PWRPLANT.PT_IMPORT_DIVISION
(
 IMPORT_RUN_ID      number(22,0)   not null,
 LINE_ID            number(22,0)   not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 COMPANY_XLATE      varchar2(254),
 COMPANY_ID         number(22,0),
 BUS_SEGMENT_XLATE  varchar2(254),
 BUS_SEGMENT_ID     number(22,0),
 DESCRIPTION        varchar2(254),
 LONG_DESCRIPTION   varchar2(254),
 DIVISION_XLATE     varchar2(254),
 DIVISION_ID        number(22,0),
 IS_MODIFIED        number(22,0),
 ERROR_MESSAGE      varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_DIVISION
   add constraint PT_IMPORT_DIV_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_DIVISION
   add constraint PT_IMPORT_DIV_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_DIVISION_ARCHIVE
(
 IMPORT_RUN_ID     number(22,0)   not null,
 LINE_ID           number(22,0)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 COMPANY_XLATE     varchar2(254),
 COMPANY_ID        number(22,0),
 BUS_SEGMENT_XLATE varchar2(254),
 BUS_SEGMENT_ID    number(22,0),
 DESCRIPTION       varchar2(254),
 LONG_DESCRIPTION  varchar2(254),
 DIVISION_XLATE    varchar2(254),
 DIVISION_ID       number(22,0)
);

alter table PWRPLANT.PT_IMPORT_DIVISION_ARCHIVE
   add constraint PT_IMPORT_DIV_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_DIVISION_ARCHIVE
   add constraint PT_IMPORT_DIV_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Municipalities
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 39, sysdate, user, 'Add : Municipalities', 'Import Municipalities', 'pt_import_municipality', 'pt_import_municipality_archive', null, 0, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 39, 'municipality_id', sysdate, user, 'Municipality', '', 1, 1, 'char(18)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 39, 'state_id', sysdate, user, 'State', 'state_xlate', 1, 1, 'char(18)', 'state', 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 39, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 39, 'state_id', 2, sysdate, user );

create table PWRPLANT.PT_IMPORT_MUNICIPALITY
(
 IMPORT_RUN_ID   number(22,0)   not null,
 LINE_ID         number(22,0)   not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 MUNICIPALITY_ID varchar2(254),
 STATE_XLATE     varchar2(254),
 STATE_ID        char(18),
 ERROR_MESSAGE   varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_MUNICIPALITY
   add constraint PT_IMPORT_MUNI_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_MUNICIPALITY
   add constraint PT_IMPORT_MUNI_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_MUNICIPALITY_ARCHIVE
(
 IMPORT_RUN_ID   number(22,0)   not null,
 LINE_ID         number(22,0)   not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 MUNICIPALITY_ID varchar2(254),
 STATE_XLATE     varchar2(254),
 STATE_ID        char(18)
);

alter table PWRPLANT.PT_IMPORT_MUNICIPALITY_ARCHIVE
   add constraint PT_IMPORT_MUNI_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_MUNICIPALITY_ARCHIVE
   add constraint PT_IMPORT_MUNI_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Major Locations
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 35, sysdate, user, 'Add : Major Locations', 'Import Major Locations', 'pt_import_major_loc', 'pt_import_major_loc_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'division_id', sysdate, user, 'Division', 'division_xlate', 1, 1, 'number(22,0)', 'division', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'municipality_id', sysdate, user, 'Municipality', 'municipality_xlate', 0, 2, 'char(18)', 'municipality', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'state_id', sysdate, user, 'State', 'state_xlate', 1, 1, 'char(18)', 'state', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'location_type_id', sysdate, user, 'Location Type', 'location_type_xlate', 1, 1, 'number(22,0)', 'location_type', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'external_location_id', sysdate, user, 'External Location', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'grid_coordinate', sysdate, user, 'Grid Coordinate', '', 0, 1, 'number(22,0)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'address', sysdate, user, 'Address', '', 0, 1, 'varchar2(254)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'zip_code', sysdate, user, 'Zip Code', '', 0, 1, 'number(22,0)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'location_report', sysdate, user, 'Location Report', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'rate_area_id', sysdate, user, 'Rate Area', 'rate_area_xlate', 0, 1, 'number(22,0)', 'rate_area', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 35, 'status_code_id', sysdate, user, 'Status Code', 'status_code_xlate', 0, 1, 'number(22,0)', 'status_code', 1 );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 136, sysdate, user, 'Rate Area.Description', 'The passed in value corresponds to the Rate Area: Description field.  Translate to the Rate Area ID using the Description column on the Rate Area table.', 'rate_area_id', '( select ra.rate_area_id from rate_area ra where upper( trim( <importfield> ) ) = upper( trim( ra.description ) ) )', 0, 0, 'rate_area', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 137, sysdate, user, 'Rate Area.Long Description', 'The passed in value corresponds to the Rate Area: Long Description field.  Translate to the Rate Area ID using the Long Description column on the Rate Area table.', 'rate_area_id', '( select ra.rate_area_id from rate_area ra where upper( trim( <importfield> ) ) = upper( trim( ra.long_description ) ) )', 0, 0, 'rate_area', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 138, sysdate, user, 'Status Code.Description', 'The passed in value corresponds to the Status Code: Description field.  Translate to the Status Code ID using the Description column on the Status Code table.', 'status_code_id', '( select sc.status_code_id from status_code sc where upper( trim( <importfield> ) ) = upper( trim( sc.description ) ) )', 0, 0, 'status_code', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 139, sysdate, user, 'Status Code.Long Description', 'The passed in value corresponds to the Status Code: Long Description field.  Translate to the Status Code ID using the Long Description column on the Status Code table.', 'status_code_id', '( select sc.status_code_id from status_code sc where upper( trim( <importfield> ) ) = upper( trim( sc.long_description ) ) )', 0, 0, 'status_code', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 140, sysdate, user, 'Major Location.Description', 'The passed in value corresponds to the Major Location: Description field.  Translate to the Major Location ID using the Description column on the Major Location table.', 'major_location_id', '( select ml.major_location_id from major_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.description ) ) )', 0, 0, 'major_location', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 141, sysdate, user, 'Major Location.Long Description', 'The passed in value corresponds to the Major Location: Long Description field.  Translate to the Major Location ID using the Long Description column on the Major Location table.', 'major_location_id', '( select ml.major_location_id from major_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.long_description ) ) )', 0, 0, 'major_location', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 142, sysdate, user, 'Major Location.External Location', 'The passed in value corresponds to the Major Location: External Location field.  Translate to the Major Location ID using the External Location column on the Major Location table.', 'major_location_id', '( select ml.major_location_id from major_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.external_location_id ) ) )', 0, 0, 'major_location', 'external_location_id', '' );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 140, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 141, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 142, sysdate, user );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'division_id', 134, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'division_id', 135, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'location_type_id', 132, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'location_type_id', 133, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'rate_area_id', 136, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'rate_area_id', 137, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'status_code_id', 138, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 35, 'status_code_id', 139, sysdate, user );

create table PWRPLANT.PT_IMPORT_MAJOR_LOC
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 STATE_XLATE          varchar2(254),
 STATE_ID             char(18),
 MUNICIPALITY_XLATE   varchar2(254),
 MUNICIPALITY_ID      char(18),
 DIVISION_XLATE       varchar2(254),
 DIVISION_ID          number(22,0),
 LOCATION_TYPE_XLATE  varchar2(254),
 LOCATION_TYPE_ID     number(22,0),
 EXTERNAL_LOCATION_ID varchar2(254),
 GRID_COORDINATE      varchar2(254),
 DESCRIPTION          varchar2(254),
 LONG_DESCRIPTION     varchar2(254),
 ADDRESS              varchar2(254),
 ZIP_CODE             varchar2(254),
 LOCATION_REPORT      varchar2(254),
 RATE_AREA_XLATE      varchar2(254),
 RATE_AREA_ID         number(22,0),
 STATUS_CODE_XLATE    varchar2(254),
 STATUS_CODE_ID       number(22,0),
 MAJOR_LOCATION_XLATE varchar2(254),
 MAJOR_LOCATION_ID    number(22,0),
 IS_MODIFIED          number(22,0),
 ERROR_MESSAGE        varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_MAJOR_LOC
   add constraint PT_IMPORT_MAJLOC_PK
      primary key ( IMPORT_RUN_ID, LINE_ID )
      using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_MAJOR_LOC
   add constraint PT_IMPORT_MAJLOC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_MAJOR_LOC_ARCHIVE
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 STATE_XLATE          varchar2(254),
 STATE_ID             char(18),
 MUNICIPALITY_XLATE   varchar2(254),
 MUNICIPALITY_ID      char(18),
 DIVISION_XLATE       varchar2(254),
 DIVISION_ID          number(22,0),
 LOCATION_TYPE_XLATE  varchar2(254),
 LOCATION_TYPE_ID     number(22,0),
 EXTERNAL_LOCATION_ID varchar2(254),
 GRID_COORDINATE      varchar2(254),
 DESCRIPTION          varchar2(254),
 LONG_DESCRIPTION     varchar2(254),
 ADDRESS              varchar2(254),
 ZIP_CODE             varchar2(254),
 LOCATION_REPORT      varchar2(254),
 RATE_AREA_XLATE      varchar2(254),
 RATE_AREA_ID         number(22,0),
 STATUS_CODE_XLATE    varchar2(254),
 STATUS_CODE_ID       number(22,0),
 MAJOR_LOCATION_XLATE varchar2(254),
 MAJOR_LOCATION_ID    number(22,0)
);

alter table PWRPLANT.PT_IMPORT_MAJOR_LOC_ARCHIVE
   add constraint PT_IMPORT_MAJLOC_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_MAJOR_LOC_ARCHIVE
   add constraint PT_IMPORT_MAJLOC_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Prop Tax Locations
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 36, sysdate, user, 'Add : Prop Tax Locations', 'Import Prop Tax Locations', 'pt_import_proptax_loc', 'pt_import_proptax_loc_archive', null, 1, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(254)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'location_code', sysdate, user, 'Location Code', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'voltage', sysdate, user, 'Voltage', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'address_1', sysdate, user, 'Address Line 1', '', 0, 1, 'varchar2(100)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'address_2', sysdate, user, 'Address Line 2', '', 0, 1, 'varchar2(100)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'city', sysdate, user, 'City', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'zip_code', sysdate, user, 'Zip Code', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 36, 'location_rollup_id', sysdate, user, 'Location Rollup', 'location_rollup_xlate', 0, 1, 'number(22,0)', 'pt_location_rollup', 1 );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 143, sysdate, user, 'PT Location Rollup.Description', 'The passed in value corresponds to the PT Location Rollup: Description field.  Translate to the Location Rollup  ID using the Description column on the PT Location Rollup table.', 'location_rollup_id', '( select lr.location_rollup_id from pt_location_rollup lr where upper( trim( <importfield> ) ) = upper( trim( lr.description ) ) )', 0, 0, 'pt_location_rollup', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 144, sysdate, user, 'PT Location Rollup.Long Description', 'The passed in value corresponds to the PT Location Rollup: Long Description field.  Translate to the Location Rollup  ID using the Long Description column on the PT Location Rollup table.', 'location_rollup_id', '( select lr.location_rollup_id from pt_location_rollup lr where upper( trim( <importfield> ) ) = upper( trim( lr.long_description ) ) )', 0, 0, 'pt_location_rollup', 'long_description', '' );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 7, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 44, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 8, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 45, sysdate, user );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 'location_rollup_id', 143, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 36, 'location_rollup_id', 144, sysdate, user );

create table PWRPLANT.PT_IMPORT_PROPTAX_LOC
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 STATE_XLATE             varchar2(254),
 STATE_ID                char(18),
 DESCRIPTION             varchar2(254),
 LOCATION_CODE           varchar2(254),
 VOLTAGE                 varchar2(254),
 ADDRESS_1               varchar2(254),
 ADDRESS_2               varchar2(254),
 CITY                    varchar2(254),
 ZIP_CODE                varchar2(254),
 LOCATION_ROLLUP_XLATE   varchar2(254),
 LOCATION_ROLLUP_ID      number(22,0),
 PROP_TAX_LOCATION_XLATE varchar2(254),
 PROP_TAX_LOCATION_ID    number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_PROPTAX_LOC
   add constraint PT_IMPORT_PTLOC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_PROPTAX_LOC
   add constraint PT_IMPORT_PTLOC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_PROPTAX_LOC_ARCHIVE
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 STATE_XLATE             varchar2(254),
 STATE_ID                char(18),
 DESCRIPTION             varchar2(254),
 LOCATION_CODE           varchar2(254),
 VOLTAGE                 varchar2(254),
 ADDRESS_1               varchar2(254),
 ADDRESS_2               varchar2(254),
 CITY                    varchar2(254),
 ZIP_CODE                varchar2(254),
 LOCATION_ROLLUP_XLATE   varchar2(254),
 LOCATION_ROLLUP_ID      number(22,0),
 PROP_TAX_LOCATION_XLATE varchar2(254),
 PROP_TAX_LOCATION_ID    number(22,0)
);

alter table PWRPLANT.PT_IMPORT_PROPTAX_LOC_ARCHIVE
   add constraint PT_IMPORT_PTLOC_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_PROPTAX_LOC_ARCHIVE
   add constraint PT_IMPORT_PTLOC_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Asset Locations
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 37, sysdate, user, 'Add : Asset Locations', 'Import Asset Locations', 'pt_import_asset_loc', 'pt_import_asset_loc_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'tax_location_id', sysdate, user, 'Tax Location', 'tax_location_xlate', 0, 1, 'number(22,0)', 'tax_location', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'minor_location_id', sysdate, user, 'Minor Location', 'minor_location_xlate', 0, 1, 'number(22,0)', 'minor_location', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'tax_district_id', sysdate, user, 'Tax District', 'tax_district_xlate', 0, 3, 'number(22,0)', 'prop_tax_district', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'major_location_id', sysdate, user, 'Major Location', 'major_location_xlate', 1, 2, 'number(22,0)', 'major_location', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'minor_location_id2', sysdate, user, 'Minor Location 2', 'minor_location2_xlate', 0, 1, 'number(22,0)', 'minor_location', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'line_number_id', sysdate, user, 'Line Number', 'line_number_xlate', 0, 1, 'number(22,0)', 'trans_line_number', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'ext_asset_location', sysdate, user, 'External Asset Location', '', 1, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'long_description', sysdate, user, 'Long Description', '', 1, 1, 'varchar2(254)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'status_code_id', sysdate, user, 'Status Code', 'status_code_xlate', 1, 1, 'number(22,0)', 'status_code', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'grid_coordinate', sysdate, user, 'Grid Coordinate', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'prop_tax_location_id', sysdate, user, 'Prop Tax Location', 'prop_tax_location_xlate', 0, 2, 'number(22,0)', 'prop_tax_location', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'address', sysdate, user, 'Address', '', 0, 1, 'varchar2(254)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'zip_code', sysdate, user, 'Zip Code', '', 0, 1, 'number(22,0)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'town_id', sysdate, user, 'Town', 'town_xlate', 0, 3, 'number(22,0)', 'town', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'charge_location_id', sysdate, user, 'Charge Location', 'charge_location_xlate', 0, 1, 'number(22,0)', 'charge_location', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'grid_coordinate1', sysdate, user, 'Grid Coordinate 1', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'grid_coordinate2', sysdate, user, 'Grid Coordinate 2', '', 0, 1, 'varchar2(35)', '', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 37, 'repair_location_id', sysdate, user, 'Repair Location', 'repair_location_xlate', 0, 1, 'number(22,0)', 'repair_location', 1 );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 145, sysdate, user, 'Tax Location.Description', 'The passed in value corresponds to the Tax Location: Description field.  Translate to the Tax Location  ID using the Description column on the Tax Location table.', 'tax_location_id', '( select tl.tax_location_id from tax_location tl where upper( trim( <importfield> ) ) = upper( trim( tl.description ) ) )', 0, 0, 'tax_location', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 146, sysdate, user, 'Trans Line Number.Description', 'The passed in value corresponds to the Trans Line Number: Description field.  Translate to the Line Number  ID using the Description column on the Trans Line Number table.', 'line_number_id', '( select tln.line_number_id from trans_line_number tln where upper( trim( <importfield> ) ) = upper( trim( tln.description ) ) )', 0, 0, 'trans_line_number', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 147, sysdate, user, 'Trans Line Number.Long Description', 'The passed in value corresponds to the Trans Line Number: Long Description field.  Translate to the Line Number  ID using the Long Description column on the Trans Line Number table.', 'line_number_id', '( select tln.line_number_id from trans_line_number tln where upper( trim( <importfield> ) ) = upper( trim( tln.long_description ) ) )', 0, 0, 'trans_line_number', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 148, sysdate, user, 'Town.Description', 'The passed in value corresponds to the Town: Description field.  Translate to the Town ID using the Description column on the Town table.', 'town_id', '( select t.town_id from town t where upper( trim( <importfield> ) ) = upper( trim( t.description ) ) )', 0, 0, 'town', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 149, sysdate, user, 'Town.Town Code', 'The passed in value corresponds to the Town: Town Code field.  Translate to the Town ID using the Town Code column on the Town table.', 'town_id', '( select t.town_id from town t where upper( trim( <importfield> ) ) = upper( trim( t.town_code ) ) )', 0, 0, 'town', 'town_code', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 150, sysdate, user, 'Town.Description (for given State and County)', 'The passed in value corresponds to the Town: Description field (for the given state and county).  Translate to the Town ID using the Description, State, and County columns on the Town table.', 'town_id', '( select t.town_id from town t where upper( trim( <importfield> ) ) = upper( trim( t.description ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( t.state_id, ''sww'' ) and nvl( <importtable>.county_id, ''sww'' ) = nvl( t.county_id, ''sww'' ) )', 0, 0, 'town', 'description', 'state_id, county_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 155, sysdate, user, 'Town.Description (for given State)', 'The passed in value corresponds to the Town: Description field (for the given state).  Translate to the Town ID using the Description and State columns on the Town table.', 'town_id', '( select t.town_id from town t where upper( trim( <importfield> ) ) = upper( trim( t.description ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( t.state_id, ''sww'' ) )', 0, 0, 'town', 'description', 'state_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 156, sysdate, user, 'Town.Town Code (for given State and County)', 'The passed in value corresponds to the Town: Town Code field (for the given state and county).  Translate to the Town ID using the Town Code, State, and County columns on the Town table.', 'town_id', '( select t.town_id from town t where upper( trim( <importfield> ) ) = upper( trim( t.town_code ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( t.state_id, ''sww'' ) and nvl( <importtable>.county_id, ''sww'' ) = nvl( t.county_id, ''sww'' ) )', 0, 0, 'town', 'town_code', 'state_id, county_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 157, sysdate, user, 'Town.Town Code (for given State)', 'The passed in value corresponds to the Town: Town Code field (for the given state).  Translate to the Town ID using the Town Code and State columns on the Town table.', 'town_id', '( select t.town_id from town t where upper( trim( <importfield> ) ) = upper( trim( t.town_code ) ) and nvl( <importtable>.state_id, ''sww'' ) = nvl( t.state_id, ''sww'' ) )', 0, 0, 'town', 'town_code', 'state_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 158, sysdate, user, 'Charge Location.Description', 'The passed in value corresponds to the Charge Location: Description field.  Translate to the Charge Location  ID using the Description column on the Charge Location table.', 'charge_location_id', '( select cl.charge_location_id from charge_location cl where upper( trim( <importfield> ) ) = upper( trim( cl.description ) ) )', 0, 0, 'charge_location', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 159, sysdate, user, 'Charge Location.Long Description', 'The passed in value corresponds to the Charge Location: Long Description field.  Translate to the Charge Location  ID using the Long Description column on the Charge Location table.', 'charge_location_id', '( select cl.charge_location_id from charge_location cl where upper( trim( <importfield> ) ) = upper( trim( cl.long_description ) ) )', 0, 0, 'charge_location', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 162, sysdate, user, 'Charge Location.External Charge Location', 'The passed in value corresponds to the Charge Location: External Charge Location field.  Translate to the Charge Location  ID using the External Charge Location column on the Charge Location table.', 'charge_location_id', '( select cl.charge_location_id from charge_location cl where upper( trim( <importfield> ) ) = upper( trim( cl.external_charge_location ) ) )', 0, 0, 'charge_location', 'external_charge_location', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 163, sysdate, user, 'Repair Location.Description', 'The passed in value corresponds to the Repair Location: Description field.  Translate to the Repair Location  ID using the Description column on the Repair Location table.', 'repair_location_id', '( select rl.repair_location_id from repair_location rl where upper( trim( <importfield> ) ) = upper( trim( rl.description ) ) )', 0, 0, 'repair_location', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 164, sysdate, user, 'Repair Location.Long Description', 'The passed in value corresponds to the Repair Location: Long Description field.  Translate to the Repair Location  ID using the Long Description column on the Repair Location table.', 'repair_location_id', '( select rl.repair_location_id from repair_location rl where upper( trim( <importfield> ) ) = upper( trim( rl.long_description ) ) )', 0, 0, 'repair_location', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 165, sysdate, user, 'Major Location.Description (for given State)', 'The passed in value corresponds to the Major Location: Description field (for the given state).  Translate to the Major Location ID using the Description and State columns on the Major Location table.', 'major_location_id', '( select ml.major_location_id from major_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.description ) ) and <importtable>.state_id = ml.state_id )', 0, 0, 'major_location', 'description', 'state_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 166, sysdate, user, 'Major Location.Long Description (for given State)', 'The passed in value corresponds to the Major Location: Long Description field (for the given state).  Translate to the Major Location ID using the Long Description and State columns on the Major Location table.', 'major_location_id', '( select ml.major_location_id from major_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.long_description ) ) and <importtable>.state_id = ml.state_id )', 0, 0, 'major_location', 'long_description', 'state_id' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 167, sysdate, user, 'Major Location.External Location (for given State)', 'The passed in value corresponds to the Major Location: External Location field (for the given state).  Translate to the Major Location ID using the External Location and State columns on the Major Location table.', 'major_location_id', '( select ml.major_location_id from major_location ml where upper( trim( <importfield> ) ) = upper( trim( ml.external_location_id ) ) and <importtable>.state_id = ml.state_id )', 0, 0, 'major_location', 'external_location_id', 'state_id' );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 93, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 99, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 89, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 95, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 90, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 96, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 91, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 97, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 92, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 98, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 88, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 94, sysdate, user );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_location_id', 145, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'minor_location_id', 130, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'minor_location_id', 131, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 79, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 85, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 82, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 105, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 107, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 106, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 80, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 86, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 83, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 81, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 87, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'tax_district_id', 84, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'major_location_id', 140, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'major_location_id', 141, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'major_location_id', 142, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'major_location_id', 165, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'major_location_id', 166, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'major_location_id', 167, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'minor_location_id2', 130, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'minor_location_id2', 131, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'line_number_id', 146, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'line_number_id', 147, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'status_code_id', 138, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'status_code_id', 139, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'prop_tax_location_id', 7, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'prop_tax_location_id', 44, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'prop_tax_location_id', 8, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'prop_tax_location_id', 45, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'town_id', 148, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'town_id', 149, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'town_id', 150, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'town_id', 155, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'town_id', 156, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'town_id', 157, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'charge_location_id', 158, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'charge_location_id', 159, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'charge_location_id', 162, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'repair_location_id', 163, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 37, 'repair_location_id', 164, sysdate, user );

create table PWRPLANT.PT_IMPORT_ASSET_LOC
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_LOCATION_XLATE      varchar2(254),
 TAX_LOCATION_ID         number(22,0),
 MINOR_LOCATION_XLATE    varchar2(254),
 MINOR_LOCATION_ID       number(22,0),
 TAX_DISTRICT_XLATE      varchar2(254),
 TAX_DISTRICT_ID         number(22,0),
 MAJOR_LOCATION_XLATE    varchar2(254),
 MAJOR_LOCATION_ID       number(22,0),
 MINOR_LOCATION2_XLATE   varchar2(254),
 MINOR_LOCATION_ID2      number(22,0),
 LINE_NUMBER_XLATE       varchar2(254),
 LINE_NUMBER_ID          number(22,0),
 EXT_ASSET_LOCATION      varchar2(254),
 LONG_DESCRIPTION        varchar2(254),
 STATUS_CODE_XLATE       varchar2(254),
 STATUS_CODE_ID          number(22,0),
 GRID_COORDINATE         varchar2(254),
 PROP_TAX_LOCATION_XLATE varchar2(254),
 PROP_TAX_LOCATION_ID    number(22,0),
 ADDRESS                 varchar2(254),
 ZIP_CODE                varchar2(254),
 TOWN_XLATE              varchar2(254),
 TOWN_ID                 number(22,0),
 STATE_XLATE             varchar2(254),
 STATE_ID                char(18),
 COUNTY_XLATE            varchar2(254),
 COUNTY_ID               char(18),
 CHARGE_LOCATION_XLATE   varchar2(254),
 CHARGE_LOCATION_ID      number(22,0),
 GRID_COORDINATE1        varchar2(254),
 GRID_COORDINATE2        varchar2(254),
 REPAIR_LOCATION_XLATE   varchar2(254),
 REPAIR_LOCATION_ID      number(22,0),
 ASSET_LOCATION_XLATE    varchar2(254),
 ASSET_LOCATION_ID       number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_ASSET_LOC
   add constraint PT_IMPORT_ASTLOC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_ASSET_LOC
   add constraint PT_IMPORT_ASTLOC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_ASSET_LOC_ARCHIVE
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 TAX_LOCATION_XLATE      varchar2(254),
 TAX_LOCATION_ID         number(22,0),
 MINOR_LOCATION_XLATE    varchar2(254),
 MINOR_LOCATION_ID       number(22,0),
 TAX_DISTRICT_XLATE      varchar2(254),
 TAX_DISTRICT_ID         number(22,0),
 MAJOR_LOCATION_XLATE    varchar2(254),
 MAJOR_LOCATION_ID       number(22,0),
 MINOR_LOCATION2_XLATE   varchar2(254),
 MINOR_LOCATION_ID2      number(22,0),
 LINE_NUMBER_XLATE       varchar2(254),
 LINE_NUMBER_ID          number(22,0),
 EXT_ASSET_LOCATION      varchar2(254),
 LONG_DESCRIPTION        varchar2(254),
 STATUS_CODE_XLATE       varchar2(254),
 STATUS_CODE_ID          number(22,0),
 GRID_COORDINATE         varchar2(254),
 PROP_TAX_LOCATION_XLATE varchar2(254),
 PROP_TAX_LOCATION_ID    number(22,0),
 ADDRESS                 varchar2(254),
 ZIP_CODE                varchar2(254),
 TOWN_XLATE              varchar2(254),
 TOWN_ID                 number(22,0),
 STATE_XLATE             varchar2(254),
 STATE_ID                char(18),
 COUNTY_XLATE            varchar2(254),
 COUNTY_ID               char(18),
 CHARGE_LOCATION_XLATE   varchar2(254),
 CHARGE_LOCATION_ID      number(22,0),
 GRID_COORDINATE1        varchar2(254),
 GRID_COORDINATE2        varchar2(254),
 REPAIR_LOCATION_XLATE   varchar2(254),
 REPAIR_LOCATION_ID      number(22,0),
 ASSET_LOCATION_XLATE    varchar2(254),
 ASSET_LOCATION_ID       number(22,0)
);

alter table PWRPLANT.PT_IMPORT_ASSET_LOC_ARCHIVE
   add constraint PT_IMPORT_ASTLOC_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_ASSET_LOC_ARCHIVE
   add constraint PT_IMPORT_ASTLOC_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

-- Parcel-Location Mappings
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 38, sysdate, user, 'Add : Parcel-Location Mappings', 'Import Parcel-Asset Location Mappings', 'pt_import_prcl_loc', 'pt_import_prcl_loc_archive', null, 0, 0 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'asset_location_id', sysdate, user, 'Asset Location', 'asset_location_xlate', 1, 2, 'number(22,0)', 'asset_location', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'prop_tax_company_id', sysdate, user, 'Prop Tax Company', 'prop_tax_company_xlate', 1, 1, 'number(22,0)', 'pt_company', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'parcel_type_id_map', sysdate, user, 'Parcel Type (Mapping)', 'parcel_type_xlate_map', 0, 1, 'number(22,0)', 'pt_parcel_type', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'allocated_yn', sysdate, user, 'Allocated', 'allocated_xlate', 1, 1, 'number(22,0)', 'yes_no', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'parcel_id', sysdate, user, 'Parcel', 'parcel_xlate', 0, 4, 'number(22,0)', 'pt_parcel', 1 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', 0 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', 0 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'tax_district_id', sysdate, user, 'Tax District', 'tax_district_xlate', 0, 3, 'number(22,0)', 'prop_tax_district', 0 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'assessor_id', sysdate, user, 'Assessor', 'assessor_xlate', 0, 1, 'number(22,0)', 'pt_assessor', 0 );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE ) values ( 38, 'parcel_type_id', sysdate, user, 'Parcel Type (Parcel)', 'parcel_type_xlate', 0, 1, 'number(22,0)', 'pt_parcel_type', 0 );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 168, sysdate, user, 'PT Parcel Type.Description', 'The passed in value corresponds to the PT Parcel Type: Description field.  Translate to the Parcel Type ID using the Description column on the PT Parcel Type table.  Accepts 0 for a parcel type of All, to be used in parcel-location mappings.', 'parcel_type_id', 'decode( upper( trim( <importfield> ) ), ''ALL'', 0, ( select pt.parcel_type_id from pt_parcel_type pt where upper( trim( <importfield> ) ) = upper( trim( pt.description ) ) and pt.parcel_type_id <> -100 ) )', 0, 0, 'pt_parcel_type', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 169, sysdate, user, 'PT Parcel Type.Long Description', 'The passed in value corresponds to the PT Parcel Type: Long Description field.  Translate to the Parcel Type ID using the Long Description column on the PT Parcel Type table.  Accepts 0 for a parcel type of All, to be used in parcel-location mappings.', 'parcel_type_id', 'decode( upper( trim( <importfield> ) ), ''ALL'', 0, ( select pt.parcel_type_id from pt_parcel_type pt where upper( trim( <importfield> ) ) = upper( trim( pt.long_description ) ) and pt.parcel_type_id <> -100 ) )', 0, 0, 'pt_parcel_type', 'long_description', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 93, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 99, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 89, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 95, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 90, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 96, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 91, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 97, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 92, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 98, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 88, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'asset_location_id', 94, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'prop_tax_company_id', 3, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'prop_tax_company_id', 5, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'prop_tax_company_id', 4, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'prop_tax_company_id', 6, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_type_id', 72, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_type_id', 73, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'allocated_yn', 78, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'allocated_yn', 77, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 102, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 103, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 101, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 104, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 15, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 16, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 11, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 12, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 13, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 14, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 9, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 10, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 71, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 70, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 17, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_id', 18, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 79, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 85, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 82, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 105, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 107, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 106, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 80, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 86, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 83, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 81, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 87, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'tax_district_id', 84, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'assessor_id', 67, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'assessor_id', 69, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'assessor_id', 68, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_type_id_map', 168, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 38, 'parcel_type_id_map', 169, sysdate, user );

create table PWRPLANT.PT_IMPORT_PRCL_LOC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 ASSESSOR_XLATE         varchar2(254),
 ASSESSOR_ID            number(22,0),
 TAX_DISTRICT_XLATE     varchar2(254),
 TAX_DISTRICT_ID        number(22,0),
 PARCEL_TYPE_XLATE      varchar2(254),
 PARCEL_TYPE_ID         number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 PARCEL_XLATE           varchar2(254),
 PARCEL_ID              number(22,0),
 ASSET_LOCATION_XLATE   varchar2(254),
 ASSET_LOCATION_ID      number(22,0),
 ALLOCATED_XLATE        varchar2(254),
 ALLOCATED_YN           number(22,0),
 PARCEL_TYPE_XLATE_MAP  varchar2(254),
 PARCEL_TYPE_ID_MAP     number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_PRCL_LOC
   add constraint PT_IMPORT_PRCLLOC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_PRCL_LOC
   add constraint PT_IMPORT_PRCLLOC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_PRCL_LOC_ARCHIVE
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 STATE_XLATE            varchar2(254),
 STATE_ID               char(18),
 COUNTY_XLATE           varchar2(254),
 COUNTY_ID              char(18),
 ASSESSOR_XLATE         varchar2(254),
 ASSESSOR_ID            number(22,0),
 TAX_DISTRICT_XLATE     varchar2(254),
 TAX_DISTRICT_ID        number(22,0),
 PARCEL_TYPE_XLATE      varchar2(254),
 PARCEL_TYPE_ID         number(22,0),
 PROP_TAX_COMPANY_XLATE varchar2(254),
 PROP_TAX_COMPANY_ID    number(22,0),
 PARCEL_XLATE           varchar2(254),
 PARCEL_ID              number(22,0),
 ASSET_LOCATION_XLATE   varchar2(254),
 ASSET_LOCATION_ID      number(22,0),
 ALLOCATED_XLATE        varchar2(254),
 ALLOCATED_YN           number(22,0),
 PARCEL_TYPE_XLATE_MAP  varchar2(254),
 PARCEL_TYPE_ID_MAP     number(22,0)
);

alter table PWRPLANT.PT_IMPORT_PRCL_LOC_ARCHIVE
   add constraint PT_IMPORT_PRCLLOC_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_PRCL_LOC_ARCHIVE
   add constraint PT_IMPORT_PRCLLOC_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

--
-- Update the lookups for Yes/No.
--
update PWRPLANT.PT_IMPORT_LOOKUP set DESCRIPTION = 'Yes No.Description (Yes or No)', LOOKUP_TABLE_NAME = 'yes_no', LOOKUP_COLUMN_NAME = 'description', LOOKUP_SQL = '( select yn.yes_no_id from yes_no yn where upper( trim( <importfield> ) ) = upper( trim( yn.description ) ) )' where IMPORT_LOOKUP_ID = 77;
update PWRPLANT.PT_IMPORT_LOOKUP set DESCRIPTION = 'Yes No.Short Description (Y or N)', LOOKUP_TABLE_NAME = 'yes_no', LOOKUP_COLUMN_NAME = 'short_description', LOOKUP_SQL = '( select yn.yes_no_id from yes_no yn where upper( trim( <importfield> ) ) = substr( upper( trim( yn.description ) ), 1, 1 ) )' where IMPORT_LOOKUP_ID = 78;

--
-- Add a column that indicates if a field will be auto-created during the import.  This will prevent the import from
-- failing if the field doesn't translate, since it will be created later.
-- We'll include the column on pt_import_column to indicate if the column is allowed to be auto-created, and we'll
-- include it on pt_import_template_fields so the user can indicate if they want it to be auto-created.
-- Also store the autocreate import table name, so that we can add/delete from it as needed.
--
alter table PWRPLANT.PT_IMPORT_COLUMN          add ALLOW_AUTOCREATE number(22,0);
alter table PWRPLANT.PT_IMPORT_TEMPLATE_FIELDS add AUTOCREATE_YN number(22,0);

update PWRPLANT.PT_IMPORT_COLUMN          set ALLOW_AUTOCREATE = 0 where ALLOW_AUTOCREATE is null;
update PWRPLANT.PT_IMPORT_TEMPLATE_FIELDS set AUTOCREATE_YN = 0 where AUTOCREATE_YN is null;

alter table PWRPLANT.PT_IMPORT_COLUMN          modify ALLOW_AUTOCREATE not null;
alter table PWRPLANT.PT_IMPORT_TEMPLATE_FIELDS modify AUTOCREATE_YN not null;

alter table PWRPLANT.PT_IMPORT_COLUMN add AUTOCREATE_TABLE_NAME varchar2(35);

--
-- Locations (Master)
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 40, sysdate, user, 'Add : Asset Locations (Master)', 'Import Asset Locations.  Automatically creates data that does not exist (assessors, type codes, tax districts, parcels, minor locations, location types, divisions, municipalities, major locations, prop tax locations, and parcel-location mappings).', 'pt_import_loc_master', 'pt_import_loc_master_archive', null, 0, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_address_line_1', sysdate, user, 'Assessor Address Line 1', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_address_line_2', sysdate, user, 'Assessor Address Line 2', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_address_line_3', sysdate, user, 'Assessor Address Line 3', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_city', sysdate, user, 'Assessor City', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_description', sysdate, user, 'Assessor Description', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_ext_assessor_code', sysdate, user, 'Assessor External Code', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_name_line_1', sysdate, user, 'Assessor Name Line 1', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_name_line_2', sysdate, user, 'Assessor Name Line 2', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_name_line_3', sysdate, user, 'Assessor Name Line 3', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_notes', sysdate, user, 'Assessor Notes', '', 0, 1, 'varchar2(2000)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_phone_1', sysdate, user, 'Assessor Phone Number 1', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_phone_2', sysdate, user, 'Assessor Phone Number 2', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_state', sysdate, user, 'Assessor State', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_zip_code', sysdate, user, 'Assessor Zip Code', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'address', sysdate, user, 'Asset Location Address', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'charge_location_id', sysdate, user, 'Charge Location', 'charge_location_xlate', 0, 1, 'number(22,0)', 'charge_location', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'county_id', sysdate, user, 'County', 'county_xlate', 0, 2, 'char(18)', 'county', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'ext_asset_location', sysdate, user, 'External Asset Location', '', 1, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'grid_coordinate', sysdate, user, 'Asset Location Grid Coordinate', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'grid_coordinate1', sysdate, user, 'Asset Location Grid Coordinate 1', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'grid_coordinate2', sysdate, user, 'Asset Location Grid Coordinate 2', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'line_number_id', sysdate, user, 'Line Number', 'line_number_xlate', 0, 1, 'number(22,0)', 'trans_line_number', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'long_description', sysdate, user, 'Asset Location Long Description', '', 1, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_location_id', sysdate, user, 'Major Location', 'major_location_xlate', 1, 2, 'number(22,0)', 'major_location', null, 1, 'pt_import_major_loc' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'minor_location_id', sysdate, user, 'Minor Location', 'minor_location_xlate', 0, 1, 'number(22,0)', 'minor_location', null, 1, 'pt_import_minor_loc' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'minor_location_id2', sysdate, user, 'Minor Location 2', 'minor_location2_xlate', 0, 1, 'number(22,0)', 'minor_location', null, 1, 'pt_import_minor_loc' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_location_id', sysdate, user, 'Prop Tax Location', 'prop_tax_location_xlate', 0, 2, 'number(22,0)', 'prop_tax_location', null, 1, 'pt_import_proptax_loc' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'repair_location_id', sysdate, user, 'Repair Location', 'repair_location_xlate', 0, 1, 'number(22,0)', 'repair_location', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'status_code_id', sysdate, user, 'Asset Location Status Code', 'status_code_xlate', 1, 1, 'number(22,0)', 'status_code', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'tax_district_id', sysdate, user, 'Tax District', 'tax_district_xlate', 0, 3, 'number(22,0)', 'prop_tax_district', null, 1, 'pt_import_tax_dist' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'tax_location_id', sysdate, user, 'Tax Location', 'tax_location_xlate', 0, 1, 'number(22,0)', 'tax_location', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'town_id', sysdate, user, 'Town', 'town_xlate', 0, 3, 'number(22,0)', 'town', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'zip_code', sysdate, user, 'Asset Location Zip Code', '', 0, 1, 'number(22,0)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'bus_segment_id', sysdate, user, 'Division Business Segment', 'bus_segment_xlate', 0, 1, 'number(22,0)', 'business_segment', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'company_id', sysdate, user, 'Division Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'division_description', sysdate, user, 'Division Description', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'division_long_description', sysdate, user, 'Division Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'location_type_description', sysdate, user, 'Location Type Description', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'location_type_long_description', sysdate, user, 'Location Type Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_loc_address', sysdate, user, 'Major Location Address', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_loc_description', sysdate, user, 'Major Location Description', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'division_id', sysdate, user, 'Major Location Division', 'division_xlate', 0, 1, 'number(22,0)', 'division', null, 1, 'pt_import_division' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_loc_external_location_id', sysdate, user, 'Major Location External Location', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_loc_grid_coordinate', sysdate, user, 'Major Location Grid Coordinate', '', 0, 1, 'number(22,0)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_loc_location_report', sysdate, user, 'Major Location Location Report', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'location_type_id', sysdate, user, 'Major Location Location Type', 'location_type_xlate', 0, 1, 'number(22,0)', 'location_type', null, 1, 'pt_import_loc_type' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_loc_long_description', sysdate, user, 'Major Location Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'municipality_id', sysdate, user, 'Municipality', 'municipality_xlate', 0, 2, 'char(18)', 'municipality', null, 1, 'pt_import_municipality' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'rate_area_id', sysdate, user, 'Major Location Rate Area', 'rate_area_xlate', 0, 1, 'number(22,0)', 'rate_area', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'major_loc_zip_code', sysdate, user, 'Major Location Zip Code', '', 0, 1, 'number(22,0)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'minor_loc_description', sysdate, user, 'Minor Location Description', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'minor_loc_long_description', sysdate, user, 'Minor Location Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'minor_loc2_description', sysdate, user, 'Minor Location 2 Description', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'minor_loc2_long_description', sysdate, user, 'Minor Location 2 Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_address_1', sysdate, user, 'Parcel Address Line 1', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_address_2', sysdate, user, 'Parcel Address Line 2', '', 0, 1, 'varchar2(50)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_city', sysdate, user, 'Parcel City', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_company_grid_number', sysdate, user, 'Parcel Company Grid Number', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_description', sysdate, user, 'Parcel Description', '', 0, 1, 'varchar2(100)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_1', sysdate, user, 'Parcel Flex Field 1', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_10', sysdate, user, 'Parcel Flex Field 10', '', 0, 1, 'date', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_11', sysdate, user, 'Parcel Flex Field 11', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_12', sysdate, user, 'Parcel Flex Field 12', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_13', sysdate, user, 'Parcel Flex Field 13', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_14', sysdate, user, 'Parcel Flex Field 14', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_15', sysdate, user, 'Parcel Flex Field 15', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_16', sysdate, user, 'Parcel Flex Field 16', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_17', sysdate, user, 'Parcel Flex Field 17', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_18', sysdate, user, 'Parcel Flex Field 18', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_19', sysdate, user, 'Parcel Flex Field 19', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_2', sysdate, user, 'Parcel Flex Field 2', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_20', sysdate, user, 'Parcel Flex Field 20', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_21', sysdate, user, 'Parcel Flex Field 21', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_22', sysdate, user, 'Parcel Flex Field 22', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_23', sysdate, user, 'Parcel Flex Field 23', '', 0, 1, 'varchar2(2000)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_3', sysdate, user, 'Parcel Flex Field 3', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_4', sysdate, user, 'Parcel Flex Field 4', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_5', sysdate, user, 'Parcel Flex Field 5', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_6', sysdate, user, 'Parcel Flex Field 6', '', 0, 1, 'date', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_7', sysdate, user, 'Parcel Flex Field 7', '', 0, 1, 'date', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_8', sysdate, user, 'Parcel Flex Field 8', '', 0, 1, 'date', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_flex_9', sysdate, user, 'Parcel Flex Field 9', '', 0, 1, 'date', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_grantor', sysdate, user, 'Parcel Grantor', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_improve_sq_ft', sysdate, user, 'Parcel Improve Sq Ft', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_land_acreage', sysdate, user, 'Parcel Land Acres', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_legal_description', sysdate, user, 'Parcel Legal Desc (1st 4000 chars)', '', 0, 1, 'varchar2(4000)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_legal_description_b', sysdate, user, 'Parcel Legal Desc (2nd 4000 chars)', '', 0, 1, 'varchar2(4000)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_legal_description_c', sysdate, user, 'Parcel Legal Desc (3rd 4000 chars)', '', 0, 1, 'varchar2(4000)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_legal_description_d', sysdate, user, 'Parcel Legal Desc (4th 4000 chars)', '', 0, 1, 'varchar2(4000)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_long_description', sysdate, user, 'Parcel Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_mineral_acreage', sysdate, user, 'Parcel Mineral Acres', '', 0, 1, 'number(22,8)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_notes', sysdate, user, 'Parcel Notes', '', 0, 1, 'varchar2(4000)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_parcel_number', sysdate, user, 'Parcel Number', '', 0, 1, 'varchar2(100)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_type_id', sysdate, user, 'Parcel Type (Parcel)', 'parcel_type_xlate', 0, 1, 'number(22,0)', 'pt_parcel_type', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_company_id', sysdate, user, 'Parcel Prop Tax Company', 'prop_tax_company_xlate', 0, 1, 'number(22,0)', 'pt_company', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_retired_date', sysdate, user, 'Parcel Retired Date', '', 0, 1, 'date', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_state_grid_number', sysdate, user, 'Parcel State Grid Number', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_x_coordinate', sysdate, user, 'Parcel X Coordinate', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_y_coordinate', sysdate, user, 'Parcel Y Coordinate', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_zip_code', sysdate, user, 'Parcel Zip Code', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_loc_address_1', sysdate, user, 'Prop Tax Location Address Line 1', '', 0, 1, 'varchar2(100)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_loc_address_2', sysdate, user, 'Prop Tax Location Address Line 2', '', 0, 1, 'varchar2(100)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_loc_city', sysdate, user, 'Prop Tax Location City', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_loc_description', sysdate, user, 'Prop Tax Location Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_loc_location_code', sysdate, user, 'Prop Tax Location Code', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'location_rollup_id', sysdate, user, 'Prop Tax Location Rollup', 'location_rollup_xlate', 0, 1, 'number(22,0)', 'pt_location_rollup', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_loc_voltage', sysdate, user, 'Prop Tax Location Voltage', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'prop_tax_loc_zip_code', sysdate, user, 'Prop Tax Location Zip Code', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'assessor_id', sysdate, user, 'Assessor', 'assessor_xlate', 0, 1, 'number(22,0)', 'pt_assessor', null, 1, 'pt_import_assessor' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'tax_dist_assignment_indicator', sysdate, user, 'Tax District Assignment Indicator', '', 0, 1, 'number(22,0)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'tax_dist_description', sysdate, user, 'Tax District Description', '', 0, 1, 'varchar2(100)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'tax_dist_grid_coordinate', sysdate, user, 'Tax District Grid Coordinate', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'is_parcel_one_to_one', sysdate, user, 'Is District 1-to-1 with Parcel', 'is_parcel_one_to_one_xlate', 0, 1, 'number(22,0)', 'yes_no', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'tax_dist_long_description', sysdate, user, 'Tax District Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'tax_dist_tax_district_code', sysdate, user, 'Tax District Code', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'type_code_id', sysdate, user, 'Tax District Type Code', 'type_code_xlate', 0, 1, 'number(22,0)', 'prop_tax_type_code', null, 1, 'pt_import_type_code' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'use_composite_authority_yn', sysdate, user, 'Use Composite Authorities', 'use_composite_auth_xlate', 0, 1, 'number(22,0)', 'yes_no', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'type_code_description', sysdate, user, 'Type Code Description', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'type_code_external_code', sysdate, user, 'Type Code External Code', '', 0, 1, 'varchar2(35)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'type_code_long_description', sysdate, user, 'Type Code Long Description', '', 0, 1, 'varchar2(254)', '', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parent_type_code_id', sysdate, user, 'Type Code Parent Type Code', 'parent_type_code_xlate', 0, 1, 'number(22,0)', 'prop_tax_type_code', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_id', sysdate, user, 'Parcel', 'parcel_xlate', 0, 4, 'number(22,0)', 'pt_parcel', null, 1, 'pt_import_parcel' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'allocated_yn', sysdate, user, 'Allocated', 'allocated_xlate', 0, 1, 'number(22,0)', 'yes_no', null, 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 40, 'parcel_type_id_map', sysdate, user, 'Parcel Type (Mapping)', 'parcel_type_xlate_map', 0, 1, 'number(22,0)', 'pt_parcel_type', null, 0, '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'charge_location_id', 158, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'charge_location_id', 159, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'charge_location_id', 162, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'county_id', 43, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'county_id', 61, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'county_id', 62, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'county_id', 63, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'county_id', 64, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'line_number_id', 146, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'line_number_id', 147, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'major_location_id', 140, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'major_location_id', 141, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'major_location_id', 142, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'major_location_id', 165, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'major_location_id', 166, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'major_location_id', 167, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'minor_location_id', 130, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'minor_location_id', 131, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'minor_location_id2', 130, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'minor_location_id2', 131, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'prop_tax_location_id', 7, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'prop_tax_location_id', 8, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'prop_tax_location_id', 44, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'prop_tax_location_id', 45, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'repair_location_id', 163, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'repair_location_id', 164, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'status_code_id', 138, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'status_code_id', 139, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 79, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 80, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 81, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 82, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 83, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 84, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 85, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 86, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 87, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 105, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 106, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_district_id', 107, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'tax_location_id', 145, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'town_id', 148, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'town_id', 149, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'town_id', 150, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'town_id', 155, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'town_id', 156, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'town_id', 157, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'bus_segment_id', 25, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'bus_segment_id', 26, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'company_id', 19, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'company_id', 20, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'company_id', 21, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'division_id', 134, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'division_id', 135, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'location_type_id', 132, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'location_type_id', 133, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'rate_area_id', 136, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'rate_area_id', 137, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_type_id', 72, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_type_id', 73, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'prop_tax_company_id', 3, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'prop_tax_company_id', 4, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'prop_tax_company_id', 5, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'location_rollup_id', 143, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'location_rollup_id', 144, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'assessor_id', 67, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'assessor_id', 68, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'assessor_id', 69, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'is_parcel_one_to_one', 77, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'is_parcel_one_to_one', 78, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'type_code_id', 74, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'type_code_id', 75, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'type_code_id', 76, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'use_composite_authority_yn', 77, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'use_composite_authority_yn', 78, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parent_type_code_id', 74, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parent_type_code_id', 75, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parent_type_code_id', 76, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 9, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 10, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 11, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 12, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 13, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 14, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 15, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 16, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 17, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 18, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 70, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 71, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 101, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 102, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 103, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_id', 104, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'allocated_yn', 77, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'allocated_yn', 78, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_type_id_map', 168, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 40, 'parcel_type_id_map', 169, sysdate, user );

create table PWRPLANT.PT_IMPORT_LOC_MASTER
(
 IMPORT_RUN_ID                   number(22,0)   not null,
 LINE_ID                         number(22,0)   not null,
 TIME_STAMP                      date,
 USER_ID                         varchar2(18),
 TAX_LOCATION_XLATE              varchar2(254),
 TAX_LOCATION_ID                 number(22,0),
 MINOR_LOCATION_XLATE            varchar2(254),
 MINOR_LOCATION_ID               number(22,0),
 TAX_DISTRICT_XLATE              varchar2(254),
 TAX_DISTRICT_ID                 number(22,0),
 MAJOR_LOCATION_XLATE            varchar2(254),
 MAJOR_LOCATION_ID               number(22,0),
 MINOR_LOCATION2_XLATE           varchar2(254),
 MINOR_LOCATION_ID2              number(22,0),
 LINE_NUMBER_XLATE               varchar2(254),
 LINE_NUMBER_ID                  number(22,0),
 EXT_ASSET_LOCATION              varchar2(254),
 LONG_DESCRIPTION                varchar2(254),
 STATUS_CODE_XLATE               varchar2(254),
 STATUS_CODE_ID                  number(22,0),
 GRID_COORDINATE                 varchar2(254),
 PROP_TAX_LOCATION_XLATE         varchar2(254),
 PROP_TAX_LOCATION_ID            number(22,0),
 ADDRESS                         varchar2(254),
 ZIP_CODE                        varchar2(254),
 TOWN_XLATE                      varchar2(254),
 TOWN_ID                         number(22,0),
 STATE_XLATE                     varchar2(254),
 STATE_ID                        char(18),
 COUNTY_XLATE                    varchar2(254),
 COUNTY_ID                       char(18),
 CHARGE_LOCATION_XLATE           varchar2(254),
 CHARGE_LOCATION_ID              number(22,0),
 GRID_COORDINATE1                varchar2(254),
 GRID_COORDINATE2                varchar2(254),
 REPAIR_LOCATION_XLATE           varchar2(254),
 REPAIR_LOCATION_ID              number(22,0),
 ASSET_LOCATION_ID               number(22,0),
 PROP_TAX_LOC_DESCRIPTION        varchar2(254),
 PROP_TAX_LOC_LOCATION_CODE      varchar2(254),
 PROP_TAX_LOC_VOLTAGE            varchar2(254),
 PROP_TAX_LOC_ADDRESS_1          varchar2(254),
 PROP_TAX_LOC_ADDRESS_2          varchar2(254),
 PROP_TAX_LOC_CITY               varchar2(254),
 PROP_TAX_LOC_ZIP_CODE           varchar2(254),
 LOCATION_ROLLUP_XLATE           varchar2(254),
 LOCATION_ROLLUP_ID              number(22,0),
 MUNICIPALITY_XLATE              varchar2(254),
 MUNICIPALITY_ID                 char(18),
 DIVISION_XLATE                  varchar2(254),
 DIVISION_ID                     number(22,0),
 LOCATION_TYPE_XLATE             varchar2(254),
 LOCATION_TYPE_ID                number(22,0),
 MAJOR_LOC_EXTERNAL_LOCATION_ID  varchar2(254),
 MAJOR_LOC_GRID_COORDINATE       varchar2(254),
 MAJOR_LOC_DESCRIPTION           varchar2(254),
 MAJOR_LOC_LONG_DESCRIPTION      varchar2(254),
 MAJOR_LOC_ADDRESS               varchar2(254),
 MAJOR_LOC_ZIP_CODE              varchar2(254),
 MAJOR_LOC_LOCATION_REPORT       varchar2(254),
 RATE_AREA_XLATE                 varchar2(254),
 RATE_AREA_ID                    number(22,0),
 COMPANY_XLATE                   varchar2(254),
 COMPANY_ID                      number(22,0),
 BUS_SEGMENT_XLATE               varchar2(254),
 BUS_SEGMENT_ID                  number(22,0),
 DIVISION_DESCRIPTION            varchar2(254),
 DIVISION_LONG_DESCRIPTION       varchar2(254),
 LOCATION_TYPE_DESCRIPTION       varchar2(254),
 LOCATION_TYPE_LONG_DESCRIPTION  varchar2(254),
 MINOR_LOC_DESCRIPTION           varchar2(254),
 MINOR_LOC_LONG_DESCRIPTION      varchar2(254),
 MINOR_LOC2_DESCRIPTION          varchar2(254),
 MINOR_LOC2_LONG_DESCRIPTION     varchar2(254),
 PROP_TAX_COMPANY_XLATE          varchar2(254),
 PROP_TAX_COMPANY_ID             number(22,0),
 PARCEL_TYPE_XLATE               varchar2(254),
 PARCEL_TYPE_ID                  number(22,0),
 PARCEL_DESCRIPTION              varchar2(254),
 PARCEL_LONG_DESCRIPTION         varchar2(254),
 PARCEL_PARCEL_NUMBER            varchar2(254),
 PARCEL_GRANTOR                  varchar2(254),
 PARCEL_LAND_ACREAGE             varchar2(35),
 PARCEL_IMPROVE_SQ_FT            varchar2(35),
 PARCEL_MINERAL_ACREAGE          varchar2(35),
 PARCEL_COMPANY_GRID_NUMBER      varchar2(254),
 PARCEL_STATE_GRID_NUMBER        varchar2(254),
 PARCEL_X_COORDINATE             varchar2(254),
 PARCEL_Y_COORDINATE             varchar2(254),
 PARCEL_RETIRED_DATE             varchar2(35),
 PARCEL_ADDRESS_1                varchar2(254),
 PARCEL_ADDRESS_2                varchar2(254),
 PARCEL_CITY                     varchar2(254),
 PARCEL_ZIP_CODE                 varchar2(254),
 PARCEL_NOTES                    varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION        varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION_B      varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION_C      varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION_D      varchar2(4000),
 PARCEL_FLEX_1                   varchar2(35),
 PARCEL_FLEX_2                   varchar2(35),
 PARCEL_FLEX_3                   varchar2(35),
 PARCEL_FLEX_4                   varchar2(35),
 PARCEL_FLEX_5                   varchar2(35),
 PARCEL_FLEX_6                   varchar2(35),
 PARCEL_FLEX_7                   varchar2(35),
 PARCEL_FLEX_8                   varchar2(35),
 PARCEL_FLEX_9                   varchar2(35),
 PARCEL_FLEX_10                  varchar2(35),
 PARCEL_FLEX_11                  varchar2(254),
 PARCEL_FLEX_12                  varchar2(254),
 PARCEL_FLEX_13                  varchar2(254),
 PARCEL_FLEX_14                  varchar2(254),
 PARCEL_FLEX_15                  varchar2(254),
 PARCEL_FLEX_16                  varchar2(254),
 PARCEL_FLEX_17                  varchar2(254),
 PARCEL_FLEX_18                  varchar2(254),
 PARCEL_FLEX_19                  varchar2(254),
 PARCEL_FLEX_20                  varchar2(254),
 PARCEL_FLEX_21                  varchar2(254),
 PARCEL_FLEX_22                  varchar2(254),
 PARCEL_FLEX_23                  varchar2(2000),
 PARCEL_XLATE                    varchar2(254),
 PARCEL_ID                       number(22,0),
 ALLOCATED_XLATE                 varchar2(254),
 ALLOCATED_YN                    number(22,0),
 PARCEL_TYPE_XLATE_MAP           varchar2(254),
 PARCEL_TYPE_ID_MAP              number(22,0),
 TAX_DIST_ASSIGNMENT_INDICATOR   varchar2(254),
 TYPE_CODE_XLATE                 varchar2(254),
 TYPE_CODE_ID                    number(22,0),
 TAX_DIST_DESCRIPTION            varchar2(254),
 TAX_DIST_LONG_DESCRIPTION       varchar2(254),
 TAX_DIST_GRID_COORDINATE        varchar2(254),
 TAX_DIST_TAX_DISTRICT_CODE      varchar2(254),
 ASSESSOR_XLATE                  varchar2(254),
 ASSESSOR_ID                     number(22,0),
 IS_PARCEL_ONE_TO_ONE_XLATE      varchar2(254),
 IS_PARCEL_ONE_TO_ONE            number(22,0),
 USE_COMPOSITE_AUTH_XLATE        varchar2(254),
 USE_COMPOSITE_AUTHORITY_YN      number(22,0),
 TYPE_CODE_DESCRIPTION           varchar2(254),
 TYPE_CODE_LONG_DESCRIPTION      varchar2(254),
 TYPE_CODE_EXTERNAL_CODE         varchar2(254),
 PARENT_TYPE_CODE_XLATE          varchar2(254),
 PARENT_TYPE_CODE_ID             number(22,0),
 ASSESSOR_DESCRIPTION            varchar2(254),
 ASSESSOR_NAME_LINE_1            varchar2(254),
 ASSESSOR_NAME_LINE_2            varchar2(254),
 ASSESSOR_NAME_LINE_3            varchar2(254),
 ASSESSOR_ADDRESS_LINE_1         varchar2(254),
 ASSESSOR_ADDRESS_LINE_2         varchar2(254),
 ASSESSOR_ADDRESS_LINE_3         varchar2(254),
 ASSESSOR_CITY                   varchar2(254),
 ASSESSOR_STATE                  varchar2(254),
 ASSESSOR_ZIP_CODE               varchar2(254),
 ASSESSOR_PHONE_1                varchar2(254),
 ASSESSOR_PHONE_2                varchar2(254),
 ASSESSOR_EXT_ASSESSOR_CODE      varchar2(254),
 ASSESSOR_NOTES                  varchar2(2000),
 PROP_TAX_LOC_LINE_ID            number(22,0),
 MAJOR_LOC_LINE_ID               number(22,0),
 MUNI_LINE_ID                    number(22,0),
 DIVISION_LINE_ID                number(22,0),
 LOCATION_TYPE_LINE_ID           number(22,0),
 MINOR_LOC_LINE_ID               number(22,0),
 MINOR_LOC2_LINE_ID              number(22,0),
 TAX_DIST_LINE_ID                number(22,0),
 TYPE_CODE_LINE_ID               number(22,0),
 ASSESSOR_LINE_ID                number(22,0),
 PARCEL_LINE_ID                  number(22,0),
 ERROR_MESSAGE                   varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_LOC_MASTER
   add constraint PT_IMPORT_LOCMAST_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_LOC_MASTER
   add constraint PT_IMPORT_LOCMAST_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_LOC_MASTER_ARCHIVE
(
 IMPORT_RUN_ID                    number(22,0)   not null,
 LINE_ID                          number(22,0)   not null,
 TIME_STAMP                       date,
 USER_ID                          varchar2(18),
 TAX_LOCATION_XLATE               varchar2(254),
 TAX_LOCATION_ID                  number(22,0),
 MINOR_LOCATION_XLATE             varchar2(254),
 MINOR_LOCATION_ID                number(22,0),
 TAX_DISTRICT_XLATE               varchar2(254),
 TAX_DISTRICT_ID                  number(22,0),
 MAJOR_LOCATION_XLATE             varchar2(254),
 MAJOR_LOCATION_ID                number(22,0),
 MINOR_LOCATION2_XLATE            varchar2(254),
 MINOR_LOCATION_ID2               number(22,0),
 LINE_NUMBER_XLATE                varchar2(254),
 LINE_NUMBER_ID                   number(22,0),
 EXT_ASSET_LOCATION               varchar2(254),
 LONG_DESCRIPTION                 varchar2(254),
 STATUS_CODE_XLATE                varchar2(254),
 STATUS_CODE_ID                   number(22,0),
 GRID_COORDINATE                  varchar2(254),
 PROP_TAX_LOCATION_XLATE          varchar2(254),
 PROP_TAX_LOCATION_ID             number(22,0),
 ADDRESS                          varchar2(254),
 ZIP_CODE                         varchar2(254),
 TOWN_XLATE                       varchar2(254),
 TOWN_ID                          number(22,0),
 STATE_XLATE                      varchar2(254),
 STATE_ID                         char(18),
 COUNTY_XLATE                     varchar2(254),
 COUNTY_ID                        char(18),
 CHARGE_LOCATION_XLATE            varchar2(254),
 CHARGE_LOCATION_ID               number(22,0),
 GRID_COORDINATE1                 varchar2(254),
 GRID_COORDINATE2                 varchar2(254),
 REPAIR_LOCATION_XLATE            varchar2(254),
 REPAIR_LOCATION_ID               number(22,0),
 ASSET_LOCATION_ID                number(22,0),
 PROP_TAX_LOC_DESCRIPTION         varchar2(254),
 PROP_TAX_LOC_LOCATION_CODE       varchar2(254),
 PROP_TAX_LOC_VOLTAGE             varchar2(254),
 PROP_TAX_LOC_ADDRESS_1           varchar2(254),
 PROP_TAX_LOC_ADDRESS_2           varchar2(254),
 PROP_TAX_LOC_CITY                varchar2(254),
 PROP_TAX_LOC_ZIP_CODE            varchar2(254),
 LOCATION_ROLLUP_XLATE            varchar2(254),
 LOCATION_ROLLUP_ID               number(22,0),
 MUNICIPALITY_XLATE               varchar2(254),
 MUNICIPALITY_ID                  char(18),
 DIVISION_XLATE                   varchar2(254),
 DIVISION_ID                      number(22,0),
 LOCATION_TYPE_XLATE              varchar2(254),
 LOCATION_TYPE_ID                 number(22,0),
 MAJOR_LOC_EXTERNAL_LOCATION_ID   varchar2(254),
 MAJOR_LOC_GRID_COORDINATE        varchar2(254),
 MAJOR_LOC_DESCRIPTION            varchar2(254),
 MAJOR_LOC_LONG_DESCRIPTION       varchar2(254),
 MAJOR_LOC_ADDRESS                varchar2(254),
 MAJOR_LOC_ZIP_CODE               varchar2(254),
 MAJOR_LOC_LOCATION_REPORT        varchar2(254),
 RATE_AREA_XLATE                  varchar2(254),
 RATE_AREA_ID                     number(22,0),
 COMPANY_XLATE                    varchar2(254),
 COMPANY_ID                       number(22,0),
 BUS_SEGMENT_XLATE                varchar2(254),
 BUS_SEGMENT_ID                   number(22,0),
 DIVISION_DESCRIPTION             varchar2(254),
 DIVISION_LONG_DESCRIPTION        varchar2(254),
 LOCATION_TYPE_DESCRIPTION        varchar2(254),
 LOCATION_TYPE_LONG_DESCRIPTION   varchar2(254),
 MINOR_LOC_DESCRIPTION            varchar2(254),
 MINOR_LOC_LONG_DESCRIPTION       varchar2(254),
 MINOR_LOC2_DESCRIPTION           varchar2(254),
 MINOR_LOC2_LONG_DESCRIPTION      varchar2(254),
 PROP_TAX_COMPANY_XLATE           varchar2(254),
 PROP_TAX_COMPANY_ID              number(22,0),
 PARCEL_TYPE_XLATE                varchar2(254),
 PARCEL_TYPE_ID                   number(22,0),
 PARCEL_DESCRIPTION               varchar2(254),
 PARCEL_LONG_DESCRIPTION          varchar2(254),
 PARCEL_PARCEL_NUMBER             varchar2(254),
 PARCEL_GRANTOR                   varchar2(254),
 PARCEL_LAND_ACREAGE              varchar2(35),
 PARCEL_IMPROVE_SQ_FT             varchar2(35),
 PARCEL_MINERAL_ACREAGE           varchar2(35),
 PARCEL_COMPANY_GRID_NUMBER       varchar2(254),
 PARCEL_STATE_GRID_NUMBER         varchar2(254),
 PARCEL_X_COORDINATE              varchar2(254),
 PARCEL_Y_COORDINATE              varchar2(254),
 PARCEL_RETIRED_DATE              varchar2(35),
 PARCEL_ADDRESS_1                 varchar2(254),
 PARCEL_ADDRESS_2                 varchar2(254),
 PARCEL_CITY                      varchar2(254),
 PARCEL_ZIP_CODE                  varchar2(254),
 PARCEL_NOTES                     varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION         varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION_B       varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION_C       varchar2(4000),
 PARCEL_LEGAL_DESCRIPTION_D       varchar2(4000),
 PARCEL_FLEX_1                    varchar2(35),
 PARCEL_FLEX_2                    varchar2(35),
 PARCEL_FLEX_3                    varchar2(35),
 PARCEL_FLEX_4                    varchar2(35),
 PARCEL_FLEX_5                    varchar2(35),
 PARCEL_FLEX_6                    varchar2(35),
 PARCEL_FLEX_7                    varchar2(35),
 PARCEL_FLEX_8                    varchar2(35),
 PARCEL_FLEX_9                    varchar2(35),
 PARCEL_FLEX_10                   varchar2(35),
 PARCEL_FLEX_11                   varchar2(254),
 PARCEL_FLEX_12                   varchar2(254),
 PARCEL_FLEX_13                   varchar2(254),
 PARCEL_FLEX_14                   varchar2(254),
 PARCEL_FLEX_15                   varchar2(254),
 PARCEL_FLEX_16                   varchar2(254),
 PARCEL_FLEX_17                   varchar2(254),
 PARCEL_FLEX_18                   varchar2(254),
 PARCEL_FLEX_19                   varchar2(254),
 PARCEL_FLEX_20                   varchar2(254),
 PARCEL_FLEX_21                   varchar2(254),
 PARCEL_FLEX_22                   varchar2(254),
 PARCEL_FLEX_23                   varchar2(2000),
 PARCEL_XLATE                     varchar2(254),
 PARCEL_ID                        number(22,0),
 ALLOCATED_XLATE                  varchar2(254),
 ALLOCATED_YN                     number(22,0),
 PARCEL_TYPE_XLATE_MAP            varchar2(254),
 PARCEL_TYPE_ID_MAP               number(22,0),
 TAX_DIST_ASSIGNMENT_INDICATOR    varchar2(254),
 TYPE_CODE_XLATE                  varchar2(254),
 TYPE_CODE_ID                     number(22,0),
 TAX_DIST_DESCRIPTION             varchar2(254),
 TAX_DIST_LONG_DESCRIPTION        varchar2(254),
 TAX_DIST_GRID_COORDINATE         varchar2(254),
 TAX_DIST_TAX_DISTRICT_CODE       varchar2(254),
 ASSESSOR_XLATE                   varchar2(254),
 ASSESSOR_ID                      number(22,0),
 IS_PARCEL_ONE_TO_ONE_XLATE       varchar2(254),
 IS_PARCEL_ONE_TO_ONE             number(22,0),
 USE_COMPOSITE_AUTH_XLATE         varchar2(254),
 USE_COMPOSITE_AUTHORITY_YN       number(22,0),
 TYPE_CODE_DESCRIPTION            varchar2(254),
 TYPE_CODE_LONG_DESCRIPTION       varchar2(254),
 TYPE_CODE_EXTERNAL_CODE          varchar2(254),
 PARENT_TYPE_CODE_XLATE           varchar2(254),
 PARENT_TYPE_CODE_ID              number(22,0),
 ASSESSOR_DESCRIPTION             varchar2(254),
 ASSESSOR_NAME_LINE_1             varchar2(254),
 ASSESSOR_NAME_LINE_2             varchar2(254),
 ASSESSOR_NAME_LINE_3             varchar2(254),
 ASSESSOR_ADDRESS_LINE_1          varchar2(254),
 ASSESSOR_ADDRESS_LINE_2          varchar2(254),
 ASSESSOR_ADDRESS_LINE_3          varchar2(254),
 ASSESSOR_CITY                    varchar2(254),
 ASSESSOR_STATE                   varchar2(254),
 ASSESSOR_ZIP_CODE                varchar2(254),
 ASSESSOR_PHONE_1                 varchar2(254),
 ASSESSOR_PHONE_2                 varchar2(254),
 ASSESSOR_EXT_ASSESSOR_CODE       varchar2(254),
 ASSESSOR_NOTES                   varchar2(2000),
 PROP_TAX_LOC_LINE_ID             number(22,0),
 MAJOR_LOC_LINE_ID                number(22,0),
 MUNI_LINE_ID                     number(22,0),
 DIVISION_LINE_ID                 number(22,0),
 LOCATION_TYPE_LINE_ID            number(22,0),
 MINOR_LOC_LINE_ID                number(22,0),
 MINOR_LOC2_LINE_ID               number(22,0),
 TAX_DIST_LINE_ID                 number(22,0),
 TYPE_CODE_LINE_ID                number(22,0),
 ASSESSOR_LINE_ID                 number(22,0),
 PARCEL_LINE_ID                   number(22,0)
);

alter table PWRPLANT.PT_IMPORT_LOC_MASTER_ARCHIVE
   add constraint PT_IMPORT_LOCMAST_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_LOC_MASTER_ARCHIVE
   add constraint PT_IMPORT_LOCMAST_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

--
-- Maint 9371 added 'parent_table' to pt_import_column so that fields with no translations could be validated to make sure they exist.
-- This worked fine for fields that have a one-column primary key, but it doesn't work for the fields that have a multi-column primary key (such as county).
-- Add a field to store other columns that are part of the primary key on the parent table.
--
alter table PWRPLANT.PT_IMPORT_COLUMN add PARENT_TABLE_PK_COLUMN2 varchar2(35);

update PWRPLANT.PT_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN2 = 'state_id' where COLUMN_NAME = 'county_id' and IMPORT_COLUMN_NAME is not null;
update PWRPLANT.PT_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN2 = 'bus_segment_id' where COLUMN_NAME = 'utility_account_id'and IMPORT_COLUMN_NAME is not null;
update PWRPLANT.PT_IMPORT_COLUMN set PARENT_TABLE_PK_COLUMN2 = 'state_id' where COLUMN_NAME = 'municipality_id'and IMPORT_COLUMN_NAME is not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (151, 0, 10, 3, 5, 0, 9559, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009559_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
