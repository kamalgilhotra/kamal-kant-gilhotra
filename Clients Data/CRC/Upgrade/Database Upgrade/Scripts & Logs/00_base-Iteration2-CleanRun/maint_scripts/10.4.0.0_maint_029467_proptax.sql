SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029467_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   03/12/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Migrate data from the client-specific Property Tax import tables (pt_import_xxx) to the
-- PP Base versions of the tables (pp_import_xxx).  The data in the fixed tables was moved in a separate script.
-- This script just moves client-specific data and only needs to be run at clients with Property Tax.
--
-- Don't migrate import type ID 40, as it has been removed due to the new autocreate capability.
--
-- This should be run AFTER the PP Base import scripts have been run, as it references the latest column
-- definitions on the pp_import_xxx tables.
--
-- This should ONLY be run if upgrading Property Tax to v10.4 or later.
--

-- populate pp_import_template from pt_import_template
insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE,
    IS_AUTOCREATE_TEMPLATE)
   select IMPORT_TEMPLATE_ID,
          TIME_STAMP,
          USER_ID,
          IMPORT_TYPE_ID,
          DESCRIPTION,
          LONG_DESCRIPTION,
          CREATED_BY,
          CREATED_DATE,
          DO_UPDATE_WITH_ADD,
          UPDATES_IMPORT_LOOKUP_ID,
          FILTER_CLAUSE,
          0
     from PT_IMPORT_TEMPLATE
    where IMPORT_TYPE_ID <> 40;

-- populate pp_import_template_fields from pt_import_template_fields
insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME,
    IMPORT_LOOKUP_ID)
   select PT_IMPORT_TEMPLATE_FIELDS.IMPORT_TEMPLATE_ID,
          PT_IMPORT_TEMPLATE_FIELDS.FIELD_ID,
          PT_IMPORT_TEMPLATE_FIELDS.TIME_STAMP,
          PT_IMPORT_TEMPLATE_FIELDS.USER_ID,
          PT_IMPORT_TEMPLATE_FIELDS.IMPORT_TYPE_ID,
          PT_IMPORT_TEMPLATE_FIELDS.COLUMN_NAME,
          PT_IMPORT_TEMPLATE_FIELDS.IMPORT_LOOKUP_ID
     from PT_IMPORT_TEMPLATE_FIELDS, PT_IMPORT_TEMPLATE
    where PT_IMPORT_TEMPLATE_FIELDS.IMPORT_TEMPLATE_ID = PT_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID
      and PT_IMPORT_TEMPLATE.IMPORT_TYPE_ID <> 40;


-- populate pp_import_template_edits from pt_import_template_edits
insert into PP_IMPORT_TEMPLATE_EDITS
   (IMPORT_TEMPLATE_ID, SEQUENCE_NUMBER, TIME_STAMP, USER_ID, COLUMN_NAME, ORIGINAL_VALUE,
    NEW_VALUE, MADE_BY, MADE_AT)
   select PT_IMPORT_TEMPLATE_EDITS.IMPORT_TEMPLATE_ID,
          PT_IMPORT_TEMPLATE_EDITS.SEQUENCE_NUMBER,
          PT_IMPORT_TEMPLATE_EDITS.TIME_STAMP,
          PT_IMPORT_TEMPLATE_EDITS.USER_ID,
          PT_IMPORT_TEMPLATE_EDITS.COLUMN_NAME,
          PT_IMPORT_TEMPLATE_EDITS.ORIGINAL_VALUE,
          PT_IMPORT_TEMPLATE_EDITS.NEW_VALUE,
          PT_IMPORT_TEMPLATE_EDITS.MADE_BY,
          PT_IMPORT_TEMPLATE_EDITS.MADE_AT
     from PT_IMPORT_TEMPLATE_EDITS, PT_IMPORT_TEMPLATE
    where PT_IMPORT_TEMPLATE_EDITS.IMPORT_TEMPLATE_ID = PT_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID
      and PT_IMPORT_TEMPLATE.IMPORT_TYPE_ID <> 40;

-- populate pp_import_run from pt_import_run
insert into PP_IMPORT_RUN
   (IMPORT_RUN_ID, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_TEMPLATE_ID, RUN_BY, RUN_AT,
    IS_COMPLETE, NOTES)
   select PT_IMPORT_RUN.IMPORT_RUN_ID,
          PT_IMPORT_RUN.TIME_STAMP,
          PT_IMPORT_RUN.USER_ID,
          PT_IMPORT_RUN.DESCRIPTION,
          PT_IMPORT_RUN.IMPORT_TEMPLATE_ID,
          PT_IMPORT_RUN.RUN_BY,
          PT_IMPORT_RUN.RUN_AT,
          PT_IMPORT_RUN.IS_COMPLETE,
          PT_IMPORT_RUN.NOTES
     from PT_IMPORT_RUN, PT_IMPORT_TEMPLATE
    where PT_IMPORT_RUN.IMPORT_TEMPLATE_ID = PT_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID
      and PT_IMPORT_TEMPLATE.IMPORT_TYPE_ID <> 40;

--
-- change all the import_run_id foreign keys on property tax import staging tables to point to pp_import_run instead of pt_import_run
--
alter table PT_IMPORT_ASSESSOR             drop constraint PT_IMPORT_ASSR_RUN_FK;
alter table PT_IMPORT_ASSESSOR_ARCHIVE     drop constraint PT_IMPORT_ASSR_ARC_RUN_FK;
alter table PT_IMPORT_ASSET_LOC            drop constraint PT_IMPORT_ASTLOC_RUN_FK;
alter table PT_IMPORT_ASSET_LOC_ARCHIVE    drop constraint PT_IMPORT_ASTLOC_ARC_RUN_FK;
alter table PT_IMPORT_ASSETS               drop constraint PT_IMPORT_ASSETS_RUN_FK;
alter table PT_IMPORT_ASSETS_ARCHIVE       drop constraint PT_IMPORT_ASSETS_ARC_RUN_FK;
alter table PT_IMPORT_AUTH_DIST            drop constraint PT_IMPT_AUTHDIST_IMPT_RUN_FK;
alter table PT_IMPORT_AUTH_DIST_ARCHIVE    drop constraint PT_IMPT_ADIST_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_CLASS_CODE           drop constraint PT_IMPORT_CC_RUN_FK;
alter table PT_IMPORT_CLASS_CODE_ARCHIVE   drop constraint PT_IMPORT_CC_ARC_RUN_FK;
alter table PT_IMPORT_CWIP_ASSIGN          drop constraint PT_IMPORT_CWIPASGN_IMPT_RUN_FK;
alter table PT_IMPORT_CWIP_ASSIGN_ARCHIVE  drop constraint PT_IMPT_CASN_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_DIVISION             drop constraint PT_IMPORT_DIV_RUN_FK;
alter table PT_IMPORT_DIVISION_ARCHIVE     drop constraint PT_IMPORT_DIV_ARC_RUN_FK;
alter table PT_IMPORT_ESCVAL_INDEX         drop constraint PT_IMPT_ESCVAL_NDX_IMPT_RUN_FK;
alter table PT_IMPORT_ESCVAL_INDEX_ARCHIVE drop constraint PT_IMPT_ESCVALNDX_ARCH_IRUN_FK;
alter table PT_IMPORT_LEDGER               drop constraint PT_IMPT_LDG_IMPT_RUN_FK;
alter table PT_IMPORT_LEDGER_ARCHIVE       drop constraint PT_IMPT_LDG_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_LEVY_RATES           drop constraint PT_IMPT_LEVY_RATES_IMPT_RUN_FK;
alter table PT_IMPORT_LEVY_RATES_ARCHIVE   drop constraint PT_IMPT_LEVYRATES_ARCH_IRUN_FK;
alter table PT_IMPORT_LOC_MASTER           drop constraint PT_IMPORT_LOCMAST_RUN_FK;
alter table PT_IMPORT_LOC_MASTER_ARCHIVE   drop constraint PT_IMPORT_LOCMAST_ARC_RUN_FK;
alter table PT_IMPORT_LOC_TYPE             drop constraint PT_IMPORT_LT_RUN_FK;
alter table PT_IMPORT_LOC_TYPE_ARCHIVE     drop constraint PT_IMPORT_LT_ARC_RUN_FK;
alter table PT_IMPORT_MAJOR_LOC            drop constraint PT_IMPORT_MAJLOC_RUN_FK;
alter table PT_IMPORT_MAJOR_LOC_ARCHIVE    drop constraint PT_IMPORT_MAJLOC_ARC_RUN_FK;
alter table PT_IMPORT_MINOR_LOC            drop constraint PT_IMPORT_MINLOC_RUN_FK;
alter table PT_IMPORT_MINOR_LOC_ARCHIVE    drop constraint PT_IMPORT_MINLOC_ARC_RUN_FK;
alter table PT_IMPORT_MUNICIPALITY         drop constraint PT_IMPORT_MUNI_RUN_FK;
alter table PT_IMPORT_MUNICIPALITY_ARCHIVE drop constraint PT_IMPORT_MUNI_ARC_RUN_FK;
alter table PT_IMPORT_PARCEL               drop constraint PT_IMPORT_PARCEL_IMPT_RUN_FK;
alter table PT_IMPORT_PARCEL_ARCHIVE       drop constraint PT_IMPT_PRCL_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_PRCL_ASMT            drop constraint PT_IMPORT_PASMT_IMPT_RUN_FK;
alter table PT_IMPORT_PRCL_ASMT_ARCHIVE    drop constraint PT_IMPT_PASMT_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_PRCL_GEO             drop constraint PT_IMPORT_PR_GEO_RUN_FK;
alter table PT_IMPORT_PRCL_GEO_ARCHIVE     drop constraint PT_IMPORT_PR_GEO_ARC_RUN_FK;
alter table PT_IMPORT_PRCL_HISTORY         drop constraint PT_IMPORT_PR_HIST_RUN_FK;
alter table PT_IMPORT_PRCL_HISTORY_ARCHIVE drop constraint PT_IMPORT_PR_HIST_ARC_RUN_FK;
alter table PT_IMPORT_PRCL_LOC             drop constraint PT_IMPORT_PRCLLOC_RUN_FK;
alter table PT_IMPORT_PRCL_LOC_ARCHIVE     drop constraint PT_IMPORT_PRCLLOC_ARC_RUN_FK;
alter table PT_IMPORT_PRCL_RSP             drop constraint PT_IMPORT_PR_RSP_RUN_FK;
alter table PT_IMPORT_PRCL_RSP_ARCHIVE     drop constraint PT_IMPORT_PR_RSP_ARC_RUN_FK;
alter table PT_IMPORT_PRCL_RSP_ENT         drop constraint PT_IMPORT_PR_RSP_ENT_RUN_FK;
alter table PT_IMPORT_PRCL_RSP_ENT_ARCHIVE drop constraint PT_IMPORT_PR_RSP_ENT_ARC_RN_FK;
alter table PT_IMPORT_PREALLO              drop constraint PT_IMPT_PRE_IMPT_RUN_FK;
alter table PT_IMPORT_PREALLO_ARCHIVE      drop constraint PT_IMPT_PRE_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_PROPTAX_LOC          drop constraint PT_IMPORT_PTLOC_RUN_FK;
alter table PT_IMPORT_PROPTAX_LOC_ARCHIVE  drop constraint PT_IMPORT_PTLOC_ARC_RUN_FK;
alter table PT_IMPORT_RETIRE_UNIT          drop constraint PT_IMPORT_RU_RUN_FK;
alter table PT_IMPORT_RETIRE_UNIT_ARCHIVE  drop constraint PT_IMPORT_RU_ARC_RUN_FK;
alter table PT_IMPORT_RSVFCTR_PCTS         drop constraint PT_IMPORT_RF_PCTS_RUN_FK;
alter table PT_IMPORT_RSVFCTR_PCTS_ARCHIVE drop constraint PT_IMPORT_RF_PCTS_ARC_RUN_FK;
alter table PT_IMPORT_STATEMENT            drop constraint PT_IMPT_STMT_IMPT_RUN_FK;
alter table PT_IMPORT_STATEMENT_ARCHIVE    drop constraint PT_IMPT_STMT_ARC_IMPT_RUN_FK;
alter table PT_IMPORT_STATS_FULL           drop constraint PT_IMPT_STATSFULL_IMPT_RUN_FK;
alter table PT_IMPORT_STATS_FULL_ARCHIVE   drop constraint PT_IMPT_SFULL_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_STATS_INCR           drop constraint PT_IMPT_STATSINCR_IMPT_RUN_FK;
alter table PT_IMPORT_STATS_INCR_ARCHIVE   drop constraint PT_IMPT_SINCR_ARCH_IMPT_RUN_FK;
alter table PT_IMPORT_TAX_DIST             drop constraint PT_IMPORT_TD_RUN_FK;
alter table PT_IMPORT_TAX_DIST_ARCHIVE     drop constraint PT_IMPORT_TD_ARC_RUN_FK;
alter table PT_IMPORT_TYPE_CODE            drop constraint PT_IMPORT_TC_RUN_FK;
alter table PT_IMPORT_TYPE_CODE_ARCHIVE    drop constraint PT_IMPORT_TC_ARC_RUN_FK;
alter table PT_IMPORT_UTIL_ACCOUNT         drop constraint PT_IMPORT_UA_RUN_FK;
alter table PT_IMPORT_UTIL_ACCOUNT_ARCHIVE drop constraint PT_IMPORT_UA_ARC_RUN_FK;

alter table PT_IMPORT_ASSESSOR             add constraint PT_IMPORT_ASSR_RUN_FK          foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_ASSESSOR_ARCHIVE     add constraint PT_IMPORT_ASSR_ARC_RUN_FK      foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_ASSET_LOC            add constraint PT_IMPORT_ASTLOC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_ASSET_LOC_ARCHIVE    add constraint PT_IMPORT_ASTLOC_ARC_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_ASSETS               add constraint PT_IMPORT_ASSETS_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_ASSETS_ARCHIVE       add constraint PT_IMPORT_ASSETS_ARC_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_AUTH_DIST            add constraint PT_IMPT_AUTHDIST_IMPT_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_AUTH_DIST_ARCHIVE    add constraint PT_IMPT_ADIST_ARCH_IMPT_RUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_CLASS_CODE           add constraint PT_IMPORT_CC_RUN_FK            foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_CLASS_CODE_ARCHIVE   add constraint PT_IMPORT_CC_ARC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_CWIP_ASSIGN          add constraint PT_IMPORT_CWIPASGN_IMPT_RUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_CWIP_ASSIGN_ARCHIVE  add constraint PT_IMPT_CASN_ARCH_IMPT_RUN_FK  foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_DIVISION             add constraint PT_IMPORT_DIV_RUN_FK           foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_DIVISION_ARCHIVE     add constraint PT_IMPORT_DIV_ARC_RUN_FK       foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_ESCVAL_INDEX         add constraint PT_IMPT_ESCVAL_NDX_IMPT_RUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_ESCVAL_INDEX_ARCHIVE add constraint PT_IMPT_ESCVALNDX_ARCH_IRUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LEDGER               add constraint PT_IMPT_LDG_IMPT_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LEDGER_ARCHIVE       add constraint PT_IMPT_LDG_ARCH_IMPT_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LEVY_RATES           add constraint PT_IMPT_LEVY_RATES_IMPT_RUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LEVY_RATES_ARCHIVE   add constraint PT_IMPT_LEVYRATES_ARCH_IRUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LOC_MASTER           add constraint PT_IMPORT_LOCMAST_RUN_FK       foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LOC_MASTER_ARCHIVE   add constraint PT_IMPORT_LOCMAST_ARC_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LOC_TYPE             add constraint PT_IMPORT_LT_RUN_FK            foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_LOC_TYPE_ARCHIVE     add constraint PT_IMPORT_LT_ARC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_MAJOR_LOC            add constraint PT_IMPORT_MAJLOC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_MAJOR_LOC_ARCHIVE    add constraint PT_IMPORT_MAJLOC_ARC_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_MINOR_LOC            add constraint PT_IMPORT_MINLOC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_MINOR_LOC_ARCHIVE    add constraint PT_IMPORT_MINLOC_ARC_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_MUNICIPALITY         add constraint PT_IMPORT_MUNI_RUN_FK          foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_MUNICIPALITY_ARCHIVE add constraint PT_IMPORT_MUNI_ARC_RUN_FK      foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PARCEL               add constraint PT_IMPORT_PARCEL_IMPT_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PARCEL_ARCHIVE       add constraint PT_IMPT_PRCL_ARCH_IMPT_RUN_FK  foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_ASMT            add constraint PT_IMPORT_PASMT_IMPT_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_ASMT_ARCHIVE    add constraint PT_IMPT_PASMT_ARCH_IMPT_RUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_GEO             add constraint PT_IMPORT_PR_GEO_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_GEO_ARCHIVE     add constraint PT_IMPORT_PR_GEO_ARC_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_HISTORY         add constraint PT_IMPORT_PR_HIST_RUN_FK       foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_HISTORY_ARCHIVE add constraint PT_IMPORT_PR_HIST_ARC_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_LOC             add constraint PT_IMPORT_PRCLLOC_RUN_FK       foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_LOC_ARCHIVE     add constraint PT_IMPORT_PRCLLOC_ARC_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_RSP             add constraint PT_IMPORT_PR_RSP_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_RSP_ARCHIVE     add constraint PT_IMPORT_PR_RSP_ARC_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_RSP_ENT         add constraint PT_IMPORT_PR_RSP_ENT_RUN_FK    foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PRCL_RSP_ENT_ARCHIVE add constraint PT_IMPORT_PR_RSP_ENT_ARC_RN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PREALLO              add constraint PT_IMPT_PRE_IMPT_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PREALLO_ARCHIVE      add constraint PT_IMPT_PRE_ARCH_IMPT_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PROPTAX_LOC          add constraint PT_IMPORT_PTLOC_RUN_FK         foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_PROPTAX_LOC_ARCHIVE  add constraint PT_IMPORT_PTLOC_ARC_RUN_FK     foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_RETIRE_UNIT          add constraint PT_IMPORT_RU_RUN_FK            foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_RETIRE_UNIT_ARCHIVE  add constraint PT_IMPORT_RU_ARC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_RSVFCTR_PCTS         add constraint PT_IMPORT_RF_PCTS_RUN_FK       foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_RSVFCTR_PCTS_ARCHIVE add constraint PT_IMPORT_RF_PCTS_ARC_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_STATEMENT            add constraint PT_IMPT_STMT_IMPT_RUN_FK       foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_STATEMENT_ARCHIVE    add constraint PT_IMPT_STMT_ARC_IMPT_RUN_FK   foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_STATS_FULL           add constraint PT_IMPT_STATSFULL_IMPT_RUN_FK  foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_STATS_FULL_ARCHIVE   add constraint PT_IMPT_SFULL_ARCH_IMPT_RUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_STATS_INCR           add constraint PT_IMPT_STATSINCR_IMPT_RUN_FK  foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_STATS_INCR_ARCHIVE   add constraint PT_IMPT_SINCR_ARCH_IMPT_RUN_FK foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_TAX_DIST             add constraint PT_IMPORT_TD_RUN_FK            foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_TAX_DIST_ARCHIVE     add constraint PT_IMPORT_TD_ARC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_TYPE_CODE            add constraint PT_IMPORT_TC_RUN_FK            foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_TYPE_CODE_ARCHIVE    add constraint PT_IMPORT_TC_ARC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_UTIL_ACCOUNT         add constraint PT_IMPORT_UA_RUN_FK            foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);
alter table PT_IMPORT_UTIL_ACCOUNT_ARCHIVE add constraint PT_IMPORT_UA_ARC_RUN_FK        foreign key (IMPORT_RUN_ID) references PP_IMPORT_RUN (IMPORT_RUN_ID);

--
-- drop the property tax-specific import tables (since we now have the pp_import tables)
--
--** 05282013 - Changed to ignore if they can not be dropped.

begin
   begin
      execute immediate 'drop public synonym PT_IMPORT_RUN';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_RUN dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_RUN does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_TYPE_UPDATES_LOOKUP';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TYPE_UPDATES_LOOKUP dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TYPE_UPDATES_LOOKUP does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_TEMPLATE_EDITS';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TEMPLATE_EDITS dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TEMPLATE_EDITS does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_TEMPLATE_FIELDS';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TEMPLATE_FIELDS dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TEMPLATE_FIELDS does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_TEMPLATE';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TEMPLATE dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TEMPLATE does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_COLUMN_LOOKUP';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_COLUMN_LOOKUP dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_COLUMN_LOOKUP does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_LOOKUP';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_LOOKUP dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_LOOKUP does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_COLUMN';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_COLUMN dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_COLUMN does not exist.');
   end;

   begin
      execute immediate 'drop public synonym PT_IMPORT_TYPE';
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TYPE dropped.');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_TYPE does not exist.');
   end;
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Could not drop public synonyms.');
end;
/


drop table PT_IMPORT_RUN;
drop table PT_IMPORT_TYPE_UPDATES_LOOKUP;
drop table PT_IMPORT_TEMPLATE_EDITS;
drop table PT_IMPORT_TEMPLATE_FIELDS;
drop table PT_IMPORT_TEMPLATE;
drop table PT_IMPORT_COLUMN_LOOKUP;
drop table PT_IMPORT_LOOKUP;
drop table PT_IMPORT_COLUMN;
drop table PT_IMPORT_TYPE;

--
-- Remove import type 40 (Asset Locations Master).  This has been replaced with the ability to autocreate from the regular Asset Locations import.
--

begin
   execute immediate 'drop public synonym PT_IMPORT_LOC_MASTER_ARCHIVE';
   DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_LOC_MASTER_ARCHIVE dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_LOC_MASTER_ARCHIVE does not exist.');
end;
/

drop table PWRPLANT.PT_IMPORT_LOC_MASTER_ARCHIVE;

begin
   execute immediate 'drop public synonym PT_IMPORT_LOC_MASTER';
   DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_LOC_MASTER dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_LOC_MASTER does not exist.');
end;
/

drop table PWRPLANT.PT_IMPORT_LOC_MASTER;

--
-- Drop the old statement import table
--
begin
   execute immediate 'drop public synonym PT_IMPORT_STATEMENT_ARCHIVE';
   DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_STATEMENT_ARCHIVE dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_STATEMENT_ARCHIVE does not exist.');
end;
/

drop table PWRPLANT.PT_IMPORT_STATEMENT_ARCHIVE;

begin
   execute immediate 'drop public synonym PT_IMPORT_STATEMENT';
   DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_STATEMENT dropped.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Public synonym PT_IMPORT_STATEMENT does not exist.');
end;
/

drop table PWRPLANT.PT_IMPORT_STATEMENT;

--
-- Now that retirement unit and utility account are autocreated, we don't need these fields on the asset import.
--
alter table PWRPLANT.PT_IMPORT_ASSETS drop column UA_DESCRIPTION;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column STATUS_CODE_XLATE;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column STATUS_CODE_ID;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column UA_EXTERNAL_ACCOUNT_CODE;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column PROPERTY_UNIT_XLATE;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column PROPERTY_UNIT_ID;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column RU_DESCRIPTION;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column RU_LONG_DESCRIPTION;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column RU_EXTERNAL_RETIRE_UNIT;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column UA_LINE_ID;
alter table PWRPLANT.PT_IMPORT_ASSETS drop column RU_LINE_ID;

alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column UA_DESCRIPTION;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column STATUS_CODE_XLATE;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column STATUS_CODE_ID;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column UA_EXTERNAL_ACCOUNT_CODE;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column PROPERTY_UNIT_XLATE;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column PROPERTY_UNIT_ID;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column RU_DESCRIPTION;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column RU_LONG_DESCRIPTION;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column RU_EXTERNAL_RETIRE_UNIT;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column UA_LINE_ID;
alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE drop column RU_LINE_ID;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (317, 0, 10, 4, 0, 0, 29467, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029467_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
