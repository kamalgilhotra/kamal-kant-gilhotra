/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052091_lessee_01_make_gain_loss_offset_nullable_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/26/2018 Jared Watkins    Make Currency Gain/Loss Offset Account ID nullable
||============================================================================
*/

alter table lsr_ilr_group
modify curr_gain_loss_offset_acct_id number(22,0) null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8722, 0, 2017, 4, 0, 0, 52091, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052091_lessee_01_make_gain_loss_offset_nullable_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;