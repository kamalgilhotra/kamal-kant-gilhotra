/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_050076_reg_01_import_case_factor_stat_value_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.1.0.0  12/07/2017 Shane "C" Ward Make Stat Value required on Case Factor Import
||============================================================================
*/

UPDATE PP_IMPORT_COLUMN
SET    is_required = 1
WHERE  import_type_id = 151 AND
       column_name = 'statistical_value';

	   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4060, 0, 2017, 1, 0, 0, 50076, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_050076_reg_01_import_case_factor_stat_value_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	   
	   
