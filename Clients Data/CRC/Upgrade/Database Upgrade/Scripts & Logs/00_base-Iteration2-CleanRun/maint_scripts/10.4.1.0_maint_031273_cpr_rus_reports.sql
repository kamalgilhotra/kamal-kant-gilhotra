SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031273_cpr_rus_reports.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   08/09/2013 Alexey Zarnitsyn
||============================================================================
*/


--1) Add Reports to PP_REPORTS
--2) Add new columns\tables
--3) Populate new columns\table
--4) Table Doc Changes

--1) Add Reports to PP_REPORTS

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   ((select max(REPORT_ID) + 1 from PP_REPORTS), 'PWRPLANT', sysdate, 'RUS Report H Sec A',
    'RUS Report H Sec A', null, 'dw_rus_parth-seca', null, null, null, 'RUS Report H Sec A',
    'dw_company_select', null, null, 9, 5, 2, 0, 1, 1, null, null, sysdate, null, null, 0);

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   ((select max(REPORT_ID) + 1 from PP_REPORTS), 'PWRPLANT', sysdate, 'RUS Report H Sec B',
    'RUS Report H Sec B', null, 'dw_rus_parth-secb', null, null, null, 'RUS Report H Sec B',
    'dw_company_select', null, null, 9, 5, 2, 0, 1, 1, null, null, sysdate, null, null, 0);

--2) Add new columns\tables

create table RUS_LINE_ITEMS
(
 RUS_LINE_NUMBER number(22, 0) not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 DESCRIPTION     varchar2(254)
);

alter table RUS_LINE_ITEMS
   add constraint PK_RUS_LINE_ITEMS
       primary key(RUS_LINE_NUMBER)
       using index tablespace PWRPLANT_IDX;

alter table FERC_PLANT_ACCOUNT add RUS_LINE_NUMBER number(22, 0) null;

alter table FERC_SYS_OF_ACCTS add RUS_LINE_NUMBER number(22, 0) null;

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('rus_line_items', TO_DATE('2013-07-18 12:02:24', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 's',
    'RUS Line Items', null, null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'rus_line_items', TO_DATE('2013-07-18 12:02:29', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'description', 0, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('rus_line_number', 'rus_line_items', TO_DATE('2013-07-18 12:02:29', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'rus line number', 2, null, null, null, null, null, null, null,
    null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'rus_line_items', TO_DATE('2013-07-18 12:02:29', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'time stamp', 100, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'rus_line_items', TO_DATE('2013-07-18 12:02:29', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'user id', 101, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('rus_line_number', 'ferc_plant_account',
    TO_DATE('2013-07-18 12:13:02', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 'e', null,
    'RUS Line Number', 110, null, null, null, null, null, null, null, null);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('rus_line_number', 'ferc_sys_of_accts', TO_DATE('2013-07-18 12:17:12', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'RUS Line Number', 110, null, null, null, null, null, null, null,
    null);


--3) Populate new columns\tables

insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1, null, null, 'Total Intangible Plant (301 thru 303)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (2, null, null, 'Total Steam Production Plant (310 thru 317)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (3, null, null, 'Total Nuclear Production Plant (320 thru 326)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (4, null, null, 'Total Hydro Production Plant (330 thru 337)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (5, null, null, 'Total Other Production Plant (340 thru 347)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (6, null, null, 'Total Production Plant (2 thru 5)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (7, null, null, 'Land and Land Rights (350)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (8, null, null, 'Structures and Improvements (352)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (9, null, null, 'Station Equipment (353)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (10, null, null, 'Other Transmission Plant (354 thru 359.1)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (11, null, null, 'Total Transmission Plant (7 thru 10)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (12, null, null, 'Land and Land Rights (360)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (13, null, null, 'Structures and Improvements (361)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (14, null, null, 'Station Equipment (362)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (15, null, null, 'Other Distribution Plant (363 thru 374)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (16, null, null, 'Total Distribution Plant (12 thru 15)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (17, null, null, 'RTO/ISO Plant (380 thru 386)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (18, null, null, 'Total General Plant (389 thru 399.1)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (19, null, null, 'Electric Plant in Service' || CHR(13) || '(1 + 6 +11 + 16 thru 18)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (20, null, null, 'Electric Plant Purchased or Sold (102)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (21, null, null, 'Electric Plant Leased to Others (104)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (22, null, null, 'Electric Plant Held for Future Use (105)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (23, null, null, 'Completed Construction Not Classified (106)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (24, null, null, 'Acquisition Adjustments (114)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (25, null, null, 'Other Utility Plant (118)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (26, null, null, 'Nuclear Fuel Assemblies (120.1 thru 120.4)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (27, null, null, 'Total Utility Plant in Service (19 thru 26)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (28, null, null, 'Construction Work in Progress (107)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (29, null, null, 'Total Utility Plant (27 + 28)');

insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1001, null, null, 'Depr. of Steam Prod. Plant (108.1)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1002, null, null, 'Depr. of Nuclear Prod. Plant (108.2)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1003, null, null, 'Depr. of Hydraulic Prod. Plant (108.3)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1004, null, null, 'Depr. of Other Prod. Plant (108.4)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1005, null, null, 'Depr. of Transmission Plant (108.5)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1006, null, null, 'Depr. of Distribution Plant (108.6)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1007, null, null, 'Depr. of General Plant (108.7)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1008, null, null, 'Retirement Work in Progress (108.8)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1009, null, null, 'Total Depr. for Elec. Plant in Serv.' || CHR(13) || '(1 thru 8)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1010, null, null, 'Depr. of Plant Leased to Others (109)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1011, null, null, 'Depr. of Plant Held for Future Use (110)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1012, null, null, 'Amort. of Elec. Plant in Service (111)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1013, null, null, 'Amort. of Leased Plant (112)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1014, null, null, 'Amort. of Plant Held for Future Use');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1015, null, null, 'Amort. of Acquisition Adj. (115)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1016, null, null, 'Depr. & Amort. Other Plant (119)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1017, null, null, 'Amort. of Nuclear Fuel (120.5)');
insert into RUS_LINE_ITEMS
   (RUS_LINE_NUMBER, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1018, null, null, 'Total Prov. for Depr. & Amort.' || CHR(13) || '(9 thru 17)');

insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1081, null, null, 'RUS: 108.1 Depr. of Steam Prod. Pla',
    'RUS: 108.1 Depr. of Steam Prod. Plant', 1001);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1082, null, null, 'RUS: 108.2 Depr. of Nuclear Prod. P',
    'RUS: 108.2 Depr. of Nuclear Prod. Plant', 1002);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1083, null, null, 'RUS: 108.3 Depr. of Hydraulic Prod.',
    'RUS: 108.3 Depr. of Hydraulic Prod. Plant', 1003);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1084, null, null, 'RUS: 108.4 Depr. of Other Prod. Pla',
    'RUS: 108.4 Depr. of Other Prod. Plant', 1004);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1085, null, null, 'RUS: 108.5 Depr. of Transmission Pl',
    'RUS: 108.5 Depr. of Transmission Plant', 1005);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1086, null, null, 'RUS: 108.6 Depr. of Distribution Pl',
    'RUS: 108.6 Depr. of Distribution Plant', 1006);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1087, null, null, 'RUS: 108.7 Depr. of General Plant', 'RUS: 108.6 Depr. of General Plant',
    1007);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1088, null, null, 'RUS: 108.8 Retirement Work in Pro', 'RUS: 108.8 Retirement Work in Progress',
    1008);

insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1090, null, null, 'RUS: 109 Depr. of Plant Leased to O',
    'RUS: 109 Depr. of Plant Leased to Others', 1010);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1100, null, null, 'RUS: 110 Depr. of Plant Held for Fu',
    'RUS: 110 Depr. of Plant Held for Future Use', 1011);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1120, null, null, 'RUS: 112 Amort. of Leased Plant', 'RUS: 112 Amort. of Leased Plant', 1013);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1130, null, null, 'RUS: 113 Amort. of Plant Held for F',
    'RUS: 113 Amort. of Plant Held for Future Use', 1014);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1150, null, null, 'RUS: 115 Amort. of Acquisition Adj.', 'RUS: 115 Amort. of Acquisition Adj.',
    1015);
insert into FERC_SYS_OF_ACCTS
   (FERC_SYS_OF_ACCTS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RUS_LINE_NUMBER)
values
   (1205, null, null, 'RUS: 120.5 Amort. of Nuclear Fuel', 'RUS: 120.5 Amort. of Nuclear Fuel',
    1017);

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 1
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 301
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 303;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 2
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 310
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 317;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 3
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 320
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 326;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 4
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 330
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 337;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 5
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 340
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 347;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 7
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) = 350;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 8
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) = 352;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 9
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) = 353;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 10
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 354
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 359.1;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 12
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) = 360;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 13
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) = 361;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 14
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) = 362;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 15
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 363
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 374;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 17
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 380
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 386;

update FERC_PLANT_ACCOUNT
   set RUS_LINE_NUMBER = 18
 where SUBSTR(FERC_PLT_ACCT_ID, 1, 1) = 1
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) >= 389
   and SUBSTR(FERC_PLT_ACCT_ID, 2, 3) <= 399.1;

update FERC_SYS_OF_ACCTS set RUS_LINE_NUMBER = 20 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 3) = 102;

update FERC_SYS_OF_ACCTS set RUS_LINE_NUMBER = 21 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 3) = 104;

update FERC_SYS_OF_ACCTS set RUS_LINE_NUMBER = 22 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 3) = 105;

update FERC_SYS_OF_ACCTS set RUS_LINE_NUMBER = 23 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 3) = 106;

update FERC_SYS_OF_ACCTS set RUS_LINE_NUMBER = 24 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 3) = 114;

update FERC_SYS_OF_ACCTS set RUS_LINE_NUMBER = 25 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 3) = 118;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 26
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) >= 1201
   and SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) <= 1204;

update FERC_SYS_OF_ACCTS set RUS_LINE_NUMBER = 28 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 3) = 107;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1001
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1081;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1002
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1082;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1003
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1083;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1004
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1084;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1005
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1085;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1006
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1086;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1007
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1087;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1008
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1088;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1010
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1090;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1011
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1100;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1012
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1110;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1013
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1120;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1015
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1150;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1016
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1190;

update FERC_SYS_OF_ACCTS
   set RUS_LINE_NUMBER = 1017
 where SUBSTR(FERC_SYS_OF_ACCTS_ID, 1, 4) = 1205;

--4) Table Doc Changes

--A)Ferc_sys_of_accts

comment on table FERC_SYS_OF_ACCTS is '(S) [11] The FERC System of Accounts data table maintains the list of regulated accounts (e.g. FERC, RUS, etc.) used by the utility. Non-regulated subsidiaries may or may not reference a FERC account.';
comment on column FERC_SYS_OF_ACCTS.RUS_LINE_NUMBER is 'The line number on the RUS report: FINANCIAL AND OPERATING REPORT ELECTRIC POWER SUPPLY PART H - ANNUAL SUPPLEMENT.';

--B) Ferc_plant_account

comment on column FERC_PLANT_ACCOUNT.RUS_LINE_NUMBER is 'The line number on the RUS report: FINANCIAL AND OPERATING REPORT ELECTRIC POWER SUPPLY PART H - ANNUAL SUPPLEMENT.';

--C) Rus_line_items

comment on table RUS_LINE_ITEMS is '(F) [16]Rus_line_items table is used for creating the RUS report: FINANCIAL AND OPERATING REPORT ELECTRIC POWER SUPPLY PART H - ANNUAL SUPPLEMENT.';
comment on column RUS_LINE_ITEMS.RUS_LINE_NUMBER is 'The line number on the RUS report: FINANCIAL AND OPERATING REPORT ELECTRIC POWER SUPPLY PART H - ANNUAL SUPPLEMENT.';
comment on column RUS_LINE_ITEMS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column RUS_LINE_ITEMS.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column RUS_LINE_ITEMS.DESCRIPTION is 'The description for the Ferc Plant Accounts, Ferc System of Accounts or Subtotal corresponding to the RUS line number';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (506, 0, 10, 4, 1, 0, 31273, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031273_cpr_rus_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON