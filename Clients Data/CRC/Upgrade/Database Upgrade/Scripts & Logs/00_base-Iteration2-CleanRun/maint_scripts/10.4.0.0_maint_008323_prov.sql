/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008323_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   11/30/2012 Blake Andrews  Point Release
||============================================================================
*/

-- Provision Maint 8323: PowerTax for Non Regs

insert into TAX_RECONCILE_ITEM
   (RECONCILE_ITEM_ID, DESCRIPTION, INPUT_RETIRE_IND)
   select -1, 'None - Provision', 0 from DUAL;

create table TAX_ACCRUAL_POWERTAX_MAP_NR
(
 COMPANY_ID           number(22,0) not null,
 TAX_BOOK_ID          number(22,0) not null,
 TAX_ROLLUP_DETAIL_ID number(22,0) not null,
 RECONCILE_ITEM_ID    number(22,0) default -1 not null,
 OPER_IND             number(22,0),
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 MID_DEPR             number(22,0),
 MID_CAP_DEPR         number(22,0),
 MID_GAINLOSS         number(22,0),
 MID_COR              number(22,0),
 MID_BONUS            number(22,0),
 MID_BASIS            number(22,0)
);

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_POWERTAX_MAP_NR_PK
       primary key (COMPANY_ID, TAX_BOOK_ID, TAX_ROLLUP_DETAIL_ID, RECONCILE_ITEM_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_CO_FK
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_MID_DEPR_FK
       foreign key (MID_DEPR)
       references TAX_ACCRUAL_M_MASTER;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_MID_CDEPR_FK
       foreign key (MID_CAP_DEPR)
       references TAX_ACCRUAL_M_MASTER;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_MID_GAINLOSS_FK
       foreign key (MID_GAINLOSS)
       references TAX_ACCRUAL_M_MASTER;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_MID_COR_FK
       foreign key (MID_COR)
       references TAX_ACCRUAL_M_MASTER;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_MID_BONUS_FK
       foreign key (MID_BONUS)
       references TAX_ACCRUAL_M_MASTER;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_BOOK_ID_FK
       foreign key (TAX_BOOK_ID)
       references TAX_BOOK;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_OPER_FK
       foreign key (OPER_IND)
       references TAX_ACCRUAL_OPER_IND;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_ROLLUP_FK
       foreign key (TAX_ROLLUP_DETAIL_ID)
       references TAX_ROLLUP_DETAIL;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_REC_ITEM_FK
       foreign key (RECONCILE_ITEM_ID)
       references TAX_RECONCILE_ITEM;

alter table TAX_ACCRUAL_POWERTAX_MAP_NR
   add constraint TA_PT_MAP_NR_MID_BASIS_FK
       foreign key (MID_BASIS)
       references TAX_ACCRUAL_M_MASTER;

create table TAX_ACCRUAL_POWERTAX_RETR_NR
(
 T_ID                 number(22,0) not nulL,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 TAX_YEAR             number(22,2),
 VERSION_ID           number(22,0),
 COMPANY_ID           number(22,0),
 OPER_IND             number(22,0),
 TAX_BOOK_ID          number(22,0),
 MID_DEPR             number(22,0),
 MID_CAP_DEPR         number(22,0),
 MID_GAINLOSS         number(22,0),
 MID_COR              number(22,0),
 MID_BONUS            number(22,0),
 MID_BASIS            number(22,0),
 DEPRECIATION         number(22,2),
 CAP_DEPRECIATION     number(22,2),
 GAIN_LOSS            number(22,2),
 COR_EXPENSE          number(22,2),
 ORIG_DIFF            number(22,2), -- tax_book_reconcile.basis_amount_activity
 GROUP_ID             number(22,0),
 --M_ITEM_ID          number(22,0),
 TAX_ROLLUP_DETAIL_ID number(22,0),
 TA_VERSION_ID        number(22,2),
 M_BEG_DEPR           number(22,2),
 --M_BEG_GAINLOSS     number(22,2),
 --M_BEG_COR          number(22,2),
 --M_BEG_BONUS        number(22,2),
 --M_BEG_BASIS        number(22,2),
 M_ACT_DEPR           number(22,2),
 M_ACT_CAP_DEPR       number(22,2),
 M_ACT_GAINLOSS       number(22,2),
 M_ACT_COR            number(22,2),
 M_ACT_BONUS          number(22,2),
 M_ACT_BASIS          number(22,2),
 ALLOC_IND            number(22,0),
 FIRST_IN_GROUP       number(22,0),
 MID_CONTROL_DEPR     number(22,0),
 MID_CONTROL_CAP_DEPR number(22,0),
 MID_CONTROL_GAINLOSS number(22,0),
 MID_CONTROL_COR      number(22,0),
 MID_CONTROL_BONUS    number(22,0),
 MID_CONTROL_BASIS    number(22,0)
);

alter table TAX_ACCRUAL_POWERTAX_RETR_NR
   add constraint TA_POWERTAX_RETR_NR_PK
       primary key (T_ID)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (254, 0, 10, 4, 0, 0, 8323, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_008323_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
