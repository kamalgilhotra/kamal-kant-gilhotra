/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051053_lessor_01_remove_fk_from_mc_audit_ls_process_control_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.0  05/01/2018 Jared Watkins    Remove the FK from LS_MC_GL_TRANSACTION_AUDIT to LS_PROCESS_CONTROL
||============================================================================
*/

ALTER TABLE ls_mc_gl_transaction_audit drop constraint ls_mc_gl_trans_proc_ctrl_fk;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4943, 0, 2017, 3, 0, 0, 51053, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051053_lessor_01_remove_fk_from_mc_audit_ls_process_control_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
