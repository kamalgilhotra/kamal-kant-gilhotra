/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035547_taxrpr_rpt.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Alex P.
||============================================================================
*/

create table REPAIR_PRETEST_EXPLAIN
(
 ID               number(22,0) not null,
 USER_ID          varchar2(18),
 TIME_STAMP       date,
 DESCRIPTION      varchar2(35),
 LONG_DESCRIPTION varchar2(1000)
);

alter table REPAIR_PRETEST_EXPLAIN
   add constraint PK_REPAIR_PRETEST_EXPLAIN
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

comment on table REPAIR_PRETEST_EXPLAIN is '(F) [18] Repair pretest Explain tables stores a list of possible values to explain the results of Tax Repairs processing.';
comment on column REPAIR_PRETEST_EXPLAIN.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column REPAIR_PRETEST_EXPLAIN.USER_ID is 'Standard System-assigned user id used for audit purposes.';
comment on column REPAIR_PRETEST_EXPLAIN.ID is 'Fixed System-assigned identifier of a given explanation.';
comment on column REPAIR_PRETEST_EXPLAIN.DESCRIPTION is 'Short description / Code for the processing outcome. Used by reports.';
comment on column REPAIR_PRETEST_EXPLAIN.LONG_DESCRIPTION is 'Long description description of the reason for the processing outcome.';

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (1, null, null, 'No Pretests', 'Pretests are not applicable and were not run');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (2, null, null, 'PASS Pretests', 'Pretests were run and passed');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (3, null, null, 'FAIL Pretests', 'Pretests failed');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (4, null, null, 'UOP EXCL',
    'Given Tax Unit of Property is exluded from processing via WO-TaxUOP Exclusions screen');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (5, null, null, 'OH/UG',
    'Geven Tax Unit of Property failed, since it was deemed to be and overhead-underground UOP');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (6, null, null, '0 ADD', 'Failed, since Add Quantity is 0');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (7, null, null, 'CIRCT Petest',
    'Failed Circuit Pretest: (add qty - retire qty) > (replacement_quantity * add_retire_circuit_pct)');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (8, null, null, 'WO Pretest',
    'Failed Work Order Pretest: (add qty / retire qty ) > add_retire_tolerance_pct');

insert into REPAIR_PRETEST_EXPLAIN
   (ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION)
values
   (9, null, null, 'RPR LOC', 'Invalid Repair Location is used');

alter table REPAIR_WORK_ORDER_SEGMENTS add PRETEST_EXPLAIN_ID number(22, 0);
comment on column REPAIR_WORK_ORDER_SEGMENTS.PRETEST_EXPLAIN_ID is 'Fixed System-assigned identifier for the explanation of the processing outcome.';

alter table REPAIR_WO_SEG_REPORTING add PRETEST_EXPLAIN_ID number(22, 0);
comment on column REPAIR_WO_SEG_REPORTING.PRETEST_EXPLAIN_ID is 'Fixed System-assigned identifier for the explanation of the processing outcome.';

/* Can not create on temporary table
alter table REPAIR_WORK_ORDER_SEGMENTS
   add constraint FK_RWOS_PRETEST_EXPLAIN_ID
       foreign key (PRETEST_EXPLAIN_ID)
       references REPAIR_PRETEST_EXPLAIN;
*/

alter table REPAIR_WO_SEG_REPORTING
   add constraint FK_RWSR_PRETEST_EXPLAIN_ID
       foreign key (PRETEST_EXPLAIN_ID)
       references REPAIR_PRETEST_EXPLAIN;

update REPAIR_WO_SEG_REPORTING
   set PRETEST_EXPLAIN_ID = 5
 where MY_EVALUATE = 0
   and NVL(OVH_TO_UDG, 0) <> 0;

update REPAIR_WO_SEG_REPORTING
   set PRETEST_EXPLAIN_ID = DECODE(NVL(ADD_QUANTITY, 0),
                                    0,
                                    6,
                                    DECODE(REPAIR_QTY_INCLUDE,
                                           0,
                                           1,
                                           DECODE(ADD_RETIRE_CIRCUIT_PCT,
                                                  null,
                                                  DECODE(ADD_RETIRE_TOLERANCE_PCT,
                                                         null,
                                                         1,
                                                         DECODE(MY_EVALUATE, 0, 3, 2)),
                                                  DECODE(UPPER(NVL(ADD_RETIRE_CIRCUIT_PASS_FAIL, 'N/A')),
                                                                 'PASS', 2,
                                                                 'FAIL', 7,
                                                                 1))));

insert into REPAIR_PRETEST_EXPLAIN
   (USER_ID, TIME_STAMP, ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (null, null, 10, 'OVER THRSH', 'Failed because threshold percent was exceeded.');

update REPAIR_WO_SEG_REPORTING
   set MY_EVALUATE = 0, PRETEST_EXPLAIN_ID = 10
 where MY_TEST_QUANTITY > MY_ADD_THRESHOLD
   and MY_EVALUATE = 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (901, 0, 10, 4, 2, 0, 35547, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035547_taxrpr_rpt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;