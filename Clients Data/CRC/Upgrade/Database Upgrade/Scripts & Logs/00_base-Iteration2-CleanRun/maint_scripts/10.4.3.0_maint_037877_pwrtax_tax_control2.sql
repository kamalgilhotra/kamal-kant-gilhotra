/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037877_pwrtax_tax_control2.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 06/12/2014 Alex P.
||========================================================================================
*/

update PP_REPORTS_FILTER
   set FILTER_UO_NAME = 'uo_ppbase_tab_filter_dynamic'
 where FILTER_UO_NAME = 'uo_tax_control_input';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_tax_depr_setup_wksp_tax_ctrl'
 where WORKSPACE_UO_NAME = 'uo_tax_control_input'
   and MODULE = 'powertax';

update PP_DYNAMIC_FILTER
   set EXCLUDE_FROM_WHERE = 0
 where EXCLUDE_FROM_WHERE = 1
   and LABEL = 'Tax Year'
   and FILTER_ID = 156;

update PP_DYNAMIC_FILTER set DW = 'dw_tax_convention_filter' where DW = 'dw_tax_convention_control_display_filter';
update PP_DYNAMIC_FILTER set DW = 'dw_recovery_period_filter' where DW = 'dw_recovery_period_control_display_filter';
update PP_DYNAMIC_FILTER set DW = 'dw_summary_4562_filter' where DW = 'dw_summary_4562_control_display_filter';
update PP_DYNAMIC_FILTER set DW = 'dw_tax_limit_filter' where DW = 'dw_tax_limit_control_display_filter';
update PP_DYNAMIC_FILTER set DW = 'dw_tax_credit_filter' where DW = 'dw_tax_credit_control_display_filter';
update PP_DYNAMIC_FILTER set DW = 'dw_deferred_tax_schema_filter' where DW = 'dw_deferred_tax_schema_control_display_filter';

update PP_DYNAMIC_FILTER
   set INPUT_TYPE = 'operation', DW = null, DW_ID = null, DW_DESCRIPTION = null, DW_ID_DATATYPE = null, REQUIRED = null,
       SINGLE_SELECT_ONLY = null, EXCLUDE_FROM_WHERE = null, NOT_RETRIEVE_IMMEDIATE = null, INVISIBLE = null
 where LABEL in ('Asset ID', 'Tax Record ID')
   and FILTER_ID in (99, 108);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1197, 0, 10, 4, 3, 0, 37877, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037877_pwrtax_tax_control2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;