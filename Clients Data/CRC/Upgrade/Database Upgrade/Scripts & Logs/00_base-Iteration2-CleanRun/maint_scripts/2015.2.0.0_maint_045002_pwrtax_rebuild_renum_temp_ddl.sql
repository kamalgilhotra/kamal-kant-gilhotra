/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045002_pwrtax_rebuild_renum_temp_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2.0 10/14/2015 Andrew Scott   Rebuild tax renumber tables as temp tables
 ||============================================================================
 */
 SET SERVEROUTPUT ON
 
 begin
   for CSR_ALTER_SQL in (select 'DROP TABLE ' || OWNER || '.' || TABLE_NAME ALTER_SQL
                         from sys.all_Tables where TABLE_NAME in ('TEMP_RENUMBER_TRIDS','TEMP_RENUMBER_TRANSFERS') 
						)
   loop
      DBMS_OUTPUT.PUT_LINE('SQL = ' || CSR_ALTER_SQL.ALTER_SQL || ';');
	  begin
		execute immediate CSR_ALTER_SQL.ALTER_SQL;
		DBMS_OUTPUT.PUT_LINE('Successfully executed the above SQL.');
	  exception
		when others then
			DBMS_OUTPUT.PUT_LINE('Unable to execute the above SQL.');
	  end;
   end loop;
end;
/

-- Create table
create global temporary table TEMP_RENUMBER_TRIDS
(
  rnum          NUMBER(22,0),
  tax_record_id NUMBER(22,0)
) on commit preserve rows;

-- Create primary key constraint
alter table TEMP_RENUMBER_TRIDS
  add constraint TEMP_RENUMBER_TRIDSPK primary key (TAX_RECORD_ID);

-- Add comments to the table 
comment on table TEMP_RENUMBER_TRIDS
  is '(C) [09] Table used in renumbering tax record ids in the case copying process.';
-- Add comments to the columns 
comment on column TEMP_RENUMBER_TRIDS.rnum
  is 'The tax record id to be used for the new records.';
comment on column TEMP_RENUMBER_TRIDS.tax_record_id
  is 'The original tax record id, used to link back information to the original record.';

-- Create table
create global temporary table TEMP_RENUMBER_TRANSFERS
(
  rnum            NUMBER(22,0),
  tax_transfer_id NUMBER(22,0)
) on commit preserve rows;

-- Create primary key constraint
alter table TEMP_RENUMBER_TRANSFERS
  add constraint TEMP_RENUMBER_XFERSPK primary key (TAX_TRANSFER_ID);

-- Add comments to the table 
comment on table TEMP_RENUMBER_TRANSFERS
  is '(C) [09] Table used in renumbering transfer ids in the case copying process.';
-- Add comments to the columns 
comment on column TEMP_RENUMBER_TRANSFERS.rnum
  is 'The tax transfer id to be used for the new records.';
comment on column TEMP_RENUMBER_TRANSFERS.tax_transfer_id
  is 'The original tax transfer id, used to link back information to the original record.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2929, 0, 2015, 2, 0, 0, 45002, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045002_pwrtax_rebuild_renum_temp_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;