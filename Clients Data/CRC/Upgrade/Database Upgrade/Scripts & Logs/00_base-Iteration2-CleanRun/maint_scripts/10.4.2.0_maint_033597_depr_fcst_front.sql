/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033597_depr_fcst_front.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   11/22/2013 Sunjin COne
||============================================================================
*/

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK)
values
   ('company_id', 'fcst_depreciation_method', 'company', 'p', 'Company', 2);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK)
values
   ('depr_blending_type_id', 'fcst_depreciation_method', 'depr_blending_type_control', 'p',
    'Depr Blending Type', 5);
insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK)
values
   ('exclude_from_rwip', 'fcst_depreciation_method', 'yes_no', 'p', 'Exclude from RWIP', 6);

comment on column FCST_DEPR_VERSION.CURRENT_DATE is 'Month and year of last month of actual depreciation and reserve.';
comment on column FCST_DEPR_VERSION.ACTUALS_DEPR_MONTH is 'Not Used.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (772, 0, 10, 4, 2, 0, 33597, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033597_depr_fcst_front.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;