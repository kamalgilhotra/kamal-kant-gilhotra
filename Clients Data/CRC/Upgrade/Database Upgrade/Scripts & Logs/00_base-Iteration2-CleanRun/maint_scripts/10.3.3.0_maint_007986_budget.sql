/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007986_budget.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/31/2011 Chris Mardis   Point Release
||============================================================================
*/

--
-- Adjust saved reports for objects that moved around on the window
--

update PP_MYPP_USER_REPORT_PARM
   set object = 'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_budget.dw_budget'
 where object = 'w_reporting_main.tab_main.tabpage_selection.dw_budget';
update PP_MYPP_USER_REPORT_PARM
   set object = 'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_budget.lb_year'
 where object = 'w_reporting_main.tab_main.tabpage_selection.lb_year';
update PP_MYPP_USER_REPORT_PARM
   set object = 'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_budget.lb_month'
 where object = 'w_reporting_main.tab_main.tabpage_selection.lb_month';
update PP_MYPP_USER_REPORT_PARM
   set object = 'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_subtotal.dw_subtotal_select'
 where object = 'w_reporting_main.tab_main.tabpage_selection.dw_subtotal_select';
update PP_MYPP_USER_REPORT_PARM
   set object = 'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_subtotal.dw_wo_subtotal_select'
 where object = 'w_reporting_main.tab_main.tabpage_selection.dw_wo_subtotal_select';

insert into PP_MYPP_USER_REPORT_PARM
   (PP_MYPP_REPORT_ID, USERS, REPORT_ID, object, DW_COLUMN_NAME, USER_VALUES, SET_OF_BOOKS_ID)
   select PP_MYPP_REPORT_ID,
          USERS,
          REPORT_ID,
          'w_reporting_main.tab_main.tabpage_selection.dw_budget',
          DW_COLUMN_NAME,
          USER_VALUES,
          SET_OF_BOOKS_ID
     from PP_MYPP_USER_REPORT_PARM
    where object =
          'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_budget.dw_budget';
insert into PP_MYPP_USER_REPORT_PARM
   (PP_MYPP_REPORT_ID, USERS, REPORT_ID, object, DW_COLUMN_NAME, USER_VALUES, SET_OF_BOOKS_ID)
   select PP_MYPP_REPORT_ID,
          USERS,
          REPORT_ID,
          'w_reporting_main.tab_main.tabpage_selection.lb_year',
          DW_COLUMN_NAME,
          USER_VALUES,
          SET_OF_BOOKS_ID
     from PP_MYPP_USER_REPORT_PARM
    where object = 'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_budget.lb_year';
insert into PP_MYPP_USER_REPORT_PARM
   (PP_MYPP_REPORT_ID, USERS, REPORT_ID, object, DW_COLUMN_NAME, USER_VALUES, SET_OF_BOOKS_ID)
   select PP_MYPP_REPORT_ID,
          USERS,
          REPORT_ID,
          'w_reporting_main.tab_main.tabpage_selection.lb_month',
          DW_COLUMN_NAME,
          USER_VALUES,
          SET_OF_BOOKS_ID
     from PP_MYPP_USER_REPORT_PARM
    where object =
          'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_budget.lb_month';
insert into PP_MYPP_USER_REPORT_PARM
   (PP_MYPP_REPORT_ID, USERS, REPORT_ID, object, DW_COLUMN_NAME, USER_VALUES, SET_OF_BOOKS_ID)
   select PP_MYPP_REPORT_ID,
          USERS,
          REPORT_ID,
          'w_reporting_main.tab_main.tabpage_selection.dw_subtotal_select',
          DW_COLUMN_NAME,
          USER_VALUES,
          SET_OF_BOOKS_ID
     from PP_MYPP_USER_REPORT_PARM
    where object =
          'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_subtotal.dw_subtotal_select';
insert into PP_MYPP_USER_REPORT_PARM
   (PP_MYPP_REPORT_ID, USERS, REPORT_ID, object, DW_COLUMN_NAME, USER_VALUES, SET_OF_BOOKS_ID)
   select PP_MYPP_REPORT_ID,
          USERS,
          REPORT_ID,
          'w_reporting_main.tab_main.tabpage_selection.dw_wo_subtotal_select',
          DW_COLUMN_NAME,
          USER_VALUES,
          SET_OF_BOOKS_ID
     from PP_MYPP_USER_REPORT_PARM
    where object =
          'w_reporting_main.tab_main.tabpage_selection.tab_options.tabpage_subtotal.dw_wo_subtotal_select';

--
-- New functionality for dynamic subtotaling
--

create table WO_REPORT_DYNAMIC_SUBTOTAL
(
 PROJ_LEVEL     varchar2(35),
 SUBTOTAL_VALUE varchar2(60),
 SCALAR_SQL     varchar2(4000),
 USER_ID        varchar2(18),
 TIME_STAMP     date
);

alter table WO_REPORT_DYNAMIC_SUBTOTAL
   add constraint PK_WO_REPORT_DYN_SUBTOTAL
       primary key (PROJ_LEVEL, SUBTOTAL_VALUE)
       using index tablespace PWRPLANT_IDX;

insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Budget Number',
    'select b.budget_number from budget b where b.budget_id = work_order_control.budget_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Budget Organization',
    'select bo.description from budget b, budget_organization bo where b.budget_id = work_order_control.budget_id and b.budget_organization_id = bo.budget_organization_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Budget Rollup',
    'select br.description from budget b, budget_organization bo, budget_rollup br where b.budget_id = work_order_control.budget_id and b.budget_organization_id = bo.budget_organization_id and bo.budget_rollup_id = br.budget_rollup_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Budget Summary',
    'select bs.description from budget b, budget_summary bs where b.budget_id = work_order_control.budget_id and b.budget_summary_id = bs.budget_summary_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Budget Summary Rollup',
    'select bsr.description from budget b, budget_summary bs, budget_summary_rollup bsr where b.budget_id = work_order_control.budget_id and b.budget_summary_id = bs.budget_summary_id and bs.budget_summary_rollup_id = bsr.budget_summary_rollup_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Business Segment',
    'select bs.description from business_segment bs where bs.bus_segment_id = work_order_control.bus_segment_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Department',
    'select d.description from department d where d.department_id = work_order_control.department_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Department (Code)',
    'select d.external_department_code||'' - ''||d.description from department d where d.department_id = work_order_control.department_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Functional Class',
    'select fc.description from work_order_account woa, func_class fc where woa.work_order_id = work_order_control.work_order_id and woa.func_class_id = fc.func_class_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'State',
    'select ml.state_id from major_location ml where ml.major_location_id = work_order_control.major_location_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Work Order Group',
    'select wog.description from work_order_account woa, work_order_group wog where woa.work_order_id = work_order_control.work_order_id and woa.work_order_grp_id = wog.work_order_grp_id');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('funding project', 'Work Order Type',
    'select wot.description from work_order_type wot where wot.work_order_type_id = work_order_control.work_order_type_id');


update WO_REPORT_DYNAMIC_SUBTOTAL set SCALAR_SQL = 'nvl((' || SCALAR_SQL || '),''(None)'')';

insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'Budget Organization',
    'nvl((select bo.description from budget_organization bo where budget.budget_organization_id = bo.budget_organization_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'Budget Rollup',
    'nvl((select br.description from budget b, budget_organization bo, budget_rollup br where budget.budget_organization_id = bo.budget_organization_id and bo.budget_rollup_id = br.budget_rollup_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'Budget Summary',
    'nvl((select bs.description from budget_summary bs where budget.budget_summary_id = bs.budget_summary_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'Budget Summary Rollup',
    'nvl((select bsr.description from budget_summary bs, budget_summary_rollup bsr where budget.budget_summary_id = bs.budget_summary_id and bs.budget_summary_rollup_id = bsr.budget_summary_rollup_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'Business Segment',
    'nvl((select bs.description from business_segment bs where bs.bus_segment_id = budget.bus_segment_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'Department',
    'nvl((select d.description from department d where d.department_id = budget.department_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'Department (Code)',
    'nvl((select d.external_department_code||'' - ''||d.description from department d where d.department_id = budget.department_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('budget item', 'State',
    'nvl((select ml.state_id from major_location ml where ml.major_location_id = budget.major_location_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
values
   ('work order', 'Funding Project',
    'nvl((select woc.work_order_number from work_order_control woc where work_order_control.funding_wo_id = woc.work_order_id),''(None)'')');
insert into WO_REPORT_DYNAMIC_SUBTOTAL
   (PROJ_LEVEL, SUBTOTAL_VALUE, SCALAR_SQL)
   select 'work order', SUBTOTAL_VALUE, SCALAR_SQL
     from WO_REPORT_DYNAMIC_SUBTOTAL
    where PROJ_LEVEL = 'funding project';

--
-- Reporting option to populate temp_work_order or temp_budget based on the selected budget version(s)
--

create global temporary table TEMP_BUDGET_VERSION
(
 BATCH_REPORT_ID   number(22),
 BUDGET_VERSION_ID number(22)
) on commit preserve rows;

--
-- Reporting option to be able to run reports in either calendar year or fiscal year format
--

create table PP_REPORT_FISCAL_YEAR_REPLACE
(
 REPLACE_ID          number(22),
 CALENDAR_YEAR_VALUE varchar2(35),
 FISCAL_YEAR_VALUE   varchar2(35)
);

alter table PP_REPORT_FISCAL_YEAR_REPLACE
   add constraint PP_REPORT_FY_REPLACE_PK
       primary key (REPLACE_ID)
       using index tablespace PWRPLANT_IDX;

insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (1, 'month_periods_sv', 'month_periods_fy_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (2, 'wo_est_monthly_sv', 'wo_est_monthly_fy_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (3, 'wo_est_monthly_deesc_sv', 'wo_est_monthly_fy_deesc_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (4, 'budg_monthly_data_sv', 'budg_monthly_data_fy_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (5, 'budg_monthly_data_deesc_sv', 'budg_monthly_data_fy_deesc_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (6, 'cwip_fp_sv', 'cwip_fp_fy_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (7, 'cwip_bi_sv', 'cwip_bi_fy_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (8, 'commitments_fp_sv', 'commitments_fp_fy_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (9, 'commitments_bi_sv', 'commitments_bi_fy_sv');
insert into PP_REPORT_FISCAL_YEAR_REPLACE
   (REPLACE_ID, CALENDAR_YEAR_VALUE, FISCAL_YEAR_VALUE)
values
   (10, 'budget_afudc_calc_sv', 'budget_afudc_calc_fy_sv');
commit;

insert into PP_REPORTS_TIME_OPTION (PP_REPORT_TIME_OPTION_ID, DESCRIPTION) values (55, 'Year');

--
-- If fiscal year client, pp_table_months needs to be updated to reflect fiscal calendar
--

update PP_TABLE_MONTHS P
   set FISCAL_PERIOD =
        (select A.FISCAL_MONTH
           from (select distinct TO_NUMBER(SUBSTR(MONTH_NUMBER, 5, 2)) CAL_MONTH, FISCAL_MONTH
                   from PP_CALENDAR) A
          where A.CAL_MONTH = P.MONTH_NUM);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (29, 0, 10, 3, 3, 0, 7986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007986_budget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
