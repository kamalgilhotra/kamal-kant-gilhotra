/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049557_lessor_02_ilr_renewal_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/10/2017 Shane "C" Ward    Updates to Lessor Renewal Options
||============================================================================
*/

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             is_required,
             processing_order,
             column_type,
             is_on_table)
VALUES      (515,
             'renewal_start_date',
             'Renewal Start Date',
             0,
             1,
             'varchar(35)',
             1);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             is_required,
             processing_order,
             column_type,
             is_on_table)
VALUES      (515,
             'ilr_renewal_option_id',
             'Renewal Option',
             1,
             1,
             'Number(22,0)',
             1);

UPDATE PP_IMPORT_COLUMN
SET    column_type = 'date mmddyyyy format'
WHERE  import_type_id = 515 AND
       column_name = 'renewal_start_date';

UPDATE PP_IMPORT_COLUMN
SET    column_type = 'number(22,0)'
WHERE  import_type_id = 515 AND
       column_name = 'ilr_renewal_option_id';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             8,
             515,
             'renewal_start_date' );

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 515 AND
                      description = 'Add: Lessor ILR Renewal Options' ),
             9,
             515,
             'ilr_renewal_option_id' );

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = ( -9999 + ROWNUM )
WHERE  import_type_id = 515;

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 1
WHERE  import_type_id = 515 AND
       column_name = 'ilr_id';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 2
WHERE  import_type_id = 515 AND
       column_name = 'revision';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 3
WHERE  import_type_id = 515 AND
       column_name = 'ilr_renewal_option_id';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 4
WHERE  import_type_id = 515 AND
       column_name = 'ilr_renewal_prob_id';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 5
WHERE  import_type_id = 515 AND
       column_name = 'renewal_notice';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 6
WHERE  import_type_id = 515 AND
       column_name = 'renewal_frequency_id';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 7
WHERE  import_type_id = 515 AND
       column_name = 'renewal_number_of_terms';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 8
WHERE  import_type_id = 515 AND
       column_name = 'renewal_amount_per_term';

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = 9
WHERE  import_type_id = 515 AND
       column_name = 'renewal_start_date';

UPDATE PP_IMPORT_COLUMN
SET    is_required = 1
WHERE  column_name IN ( 'ilr_id', 'renewal_option' ) AND
       import_type_id = 515;

UPDATE PP_IMPORT_COLUMN
SET    is_required = 0
WHERE  column_name IN ( 'ilr_id', 'renewal_option' ) AND
       import_type_id = 515;

UPDATE PP_IMPORT_TYPE
SET    archive_additional_columns = 'ilr_renewal_id'
WHERE  import_type_id = 515; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3926, 0, 2017, 1, 0, 0, 49557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049557_lessor_02_ilr_renewal_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
