/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037925_pwrtax_act_intfc_patch1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.1 05/15/2014 Andrew Scott        1. filtering functionality from retire rules
||                                         was not brought over to pl/sql.  fixed in package.
||                                         this script backs up the retire rules table as
||                                         the implementor may have to tweak the contents.
||                                         2. dc reported tbt grouping pkey issue.
||                                         vintage st and end on group map table should be part
||                                         of pkey and used in batching logic.
||============================================================================
*/

----back up the tax retire rules table.
create table D1_TAX_RETIRE_RULES_10421 as
   select * from TAX_RETIRE_RULES;

------the filtering joining logic will most often use these attributes
------if more filtering were in place, the implementor can add tbt_group_id
------to the filter clause
alter table TAX_BOOK_TRANSACTIONS_GRP
   add (EXT_CLASS_CODE_VALUE varchar2(35),
        BONUS_VALUE          varchar2(35));

comment on column TAX_BOOK_TRANSACTIONS_GRP.EXT_CLASS_CODE_VALUE
  is 'For bonus depreciation: 2 = 30%; 3 = 50%; 1 = Nonqualifying; 0 = blanket.';
comment on column TAX_BOOK_TRANSACTIONS_GRP.BONUS_VALUE
  is '0, 30, or 50.';

----change the "from columns" to just the column on the transfer tables so the filters will work

alter table TAX_BOOK_TRANSFERS_GRP rename column AMOUNT_UNPROCESSED_FROM to AMOUNT_UNPROCESSED;
alter table TAX_BOOK_TRANSFERS_GRP rename column TRANSFER_STATUS_FROM to TRANSFER_STATUS;
alter table TAX_BOOK_TRANSFERS_GRP rename column BOOK_SALE_AMOUNT_FROM to BOOK_SALE_AMOUNT;
alter table TAX_BOOK_TRANSFERS_GRP rename column AMOUNT_FROM to AMOUNT;
alter table TAX_BOOK_TRANSFERS_GRP rename column IN_SERVICE_YEAR_FROM to IN_SERVICE_YEAR;
alter table TAX_BOOK_TRANSFERS_GRP rename column COMPANY_ID_FROM to COMPANY_ID;
alter table TAX_BOOK_TRANSFERS_GRP rename column TAX_ACTIVITY_CODE_ID_FROM to TAX_ACTIVITY_CODE_ID;
alter table TAX_BOOK_TRANSFERS_GRP rename column TBT_GROUP_ID_FROM to TBT_GROUP_ID;

alter table TAX_BOOK_TRANSFERS_GRP
   add (EXT_CLASS_CODE_VALUE    varchar2(35),
        EXT_CLASS_CODE_VALUE_TO varchar2(35));

comment on column TAX_BOOK_TRANSFERS_GRP.EXT_CLASS_CODE_VALUE
  is 'For bonus depreciation: 2 = 30%; 3 = 50%; 1 = Nonqualifying; 0 = blanket. (of the transfer "from")';
comment on column TAX_BOOK_TRANSFERS_GRP.EXT_CLASS_CODE_VALUE_TO
  is 'For bonus depreciation: 2 = 30%; 3 = 50%; 1 = Nonqualifying; 0 = blanket. (of the transfer "to")';


----rebuild the group map table's pkey to include start and end eff book vintage.
alter table TAX_BOOK_TRANSLATE_GROUP_MAP drop primary key drop index;

alter table TAX_BOOK_TRANSLATE_GROUP_MAP
   add constraint TB_TRANSLATE_GROUP_MAP_PK
       primary key (TBT_GROUP_ID, TAX_CLASS_ID, START_EFF_BOOK_VINTAGE, END_EFF_BOOK_VINTAGE)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1188, 0, 10, 4, 2, 1, 37925, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.1_maint_037925_pwrtax_act_intfc_patch1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;