/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049835_taxrpr_add_exp_wksp_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 11/03/2017 Eric Berger    DML multi-batch reporting.
||============================================================================
*/

--fix the tr menu structure
MERGE INTO ppbase_menu_items a USING
(
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_exp_tests' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	4 AS ITEM_ORDER,
	'Repair Tests' AS LABEL,
	'Repair Test Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_exp_tests' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_import' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	12 AS ITEM_ORDER,
	'Import' AS LABEL,
	'Import Center' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_import' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_book_summary' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	2 AS ITEM_ORDER,
	'Book Summary' AS LABEL,
	'Repair Book Summary Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_book_summary' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_repair_schema' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	3 AS ITEM_ORDER,
	'Repair Schema' AS LABEL,
	'Repair Schema Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_repair_schema' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_locations' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	5 AS ITEM_ORDER,
	'Repair Locations' AS LABEL,
	'Repair Location Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_locations' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_status' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	8 AS ITEM_ORDER,
	'Tax Status (Gen)' AS LABEL,
	'Tax Status Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_status' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_range_test' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	9 AS ITEM_ORDER,
	'Range Test (Gen)' AS LABEL,
	'Range Test Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_range_test' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_loc_rollups' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	10 AS ITEM_ORDER,
	'Repair Loc Rollups (Alloc)' AS LABEL,
	'Repair Location Rollup Configuration for Blanket Allocations' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_loc_rollups' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_units' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	6 AS ITEM_ORDER,
	'Tax Units of Property' AS LABEL,
	'Tax Units of Property Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_units' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_thresholds' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	7 AS ITEM_ORDER,
	'Tax Thresholds' AS LABEL,
	'Tax Threshold Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_thresholds' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_system_options' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	13 AS ITEM_ORDER,
	'System Options' AS LABEL,
	'System Options' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_system_options' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_yr' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	11 AS ITEM_ORDER,
	'Tax Year Configuration' AS LABEL,
	'Tax Year Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_yr' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
UNION ALL
SELECT
	'REPAIRS' AS MODULE,
	'menu_wksp_tax_rpr_companies' AS MENU_IDENTIFIER,
	2 AS MENU_LEVEL,
	1 AS ITEM_ORDER,
	'Company Configuration' AS LABEL,
	'Repair Company Configuration' AS MINIHELP,
	'menu_wksp_config' AS PARENT_MENU_IDENTIFIER,
	'menu_wksp_tax_rpr_companies' AS WORKSPACE_IDENTIFIER,
	1 AS ENABLE_YN
FROM DUAL
) b
ON (a.MODULE = b.MODULE
AND a.MENU_IDENTIFIER = b.MENU_IDENTIFIER)
WHEN NOT MATCHED THEN
INSERT
(
MODULE,
MENU_IDENTIFIER,
MENU_LEVEL,
ITEM_ORDER,
LABEL,
MINIHELP,
PARENT_MENU_IDENTIFIER,
WORKSPACE_IDENTIFIER,
ENABLE_YN
)
VALUES
(
b.MODULE,
b.MENU_IDENTIFIER,
b.MENU_LEVEL,
b.ITEM_ORDER,
b.LABEL,
b.MINIHELP,
b.PARENT_MENU_IDENTIFIER,
b.WORKSPACE_IDENTIFIER,
b.ENABLE_YN
)
WHEN MATCHED THEN
UPDATE SET
a.MENU_LEVEL = b.MENU_LEVEL,
a.ITEM_ORDER = b.ITEM_ORDER,
a.LABEL = b.LABEL,
a.MINIHELP = b.MINIHELP,
a.PARENT_MENU_IDENTIFIER = b.PARENT_MENU_IDENTIFIER,
a.WORKSPACE_IDENTIFIER = b.WORKSPACE_IDENTIFIER,
a.ENABLE_YN = b.ENABLE_YN;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13736, 0, 2018, 2, 0, 0, 49835, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049835_taxrpr_add_exp_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;