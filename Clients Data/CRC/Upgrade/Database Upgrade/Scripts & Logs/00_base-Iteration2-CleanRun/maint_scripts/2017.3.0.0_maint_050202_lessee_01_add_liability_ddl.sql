/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050202_lessee_01_add_liability_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 01/10/2018 Josh Sandler 		Add Lease Liability fields
||============================================================================
*/

ALTER TABLE ls_asset_schedule ADD beg_liability NUMBER(22,2) NULL;
ALTER TABLE ls_asset_schedule ADD end_liability NUMBER(22,2) NULL;
ALTER TABLE ls_asset_schedule ADD beg_lt_liability NUMBER(22,2) NULL;
ALTER TABLE ls_asset_schedule ADD end_lt_liability NUMBER(22,2) NULL;

ALTER TABLE ls_ilr_asset_schedule_stg ADD beg_liability NUMBER(22,2) NULL;
ALTER TABLE ls_ilr_asset_schedule_stg ADD end_liability NUMBER(22,2) NULL;
ALTER TABLE ls_ilr_asset_schedule_stg ADD beg_lt_liability NUMBER(22,2) NULL;
ALTER TABLE ls_ilr_asset_schedule_stg ADD end_lt_liability NUMBER(22,2) NULL;

ALTER TABLE ls_ilr_schedule ADD beg_liability NUMBER(22,2) NULL;
ALTER TABLE ls_ilr_schedule ADD end_liability NUMBER(22,2) NULL;
ALTER TABLE ls_ilr_schedule ADD beg_lt_liability NUMBER(22,2) NULL;
ALTER TABLE ls_ilr_schedule ADD end_lt_liability NUMBER(22,2) NULL;

ALTER TABLE ls_summary_forecast ADD beg_liability NUMBER(22,2) NULL;
ALTER TABLE ls_summary_forecast ADD end_liability NUMBER(22,2) NULL;
ALTER TABLE ls_summary_forecast ADD beg_lt_liability NUMBER(22,2) NULL;
ALTER TABLE ls_summary_forecast ADD end_lt_liability NUMBER(22,2) NULL;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4079, 0, 2017, 3, 0, 0, 50202, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050202_lessee_01_add_liability_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
