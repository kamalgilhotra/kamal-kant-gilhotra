/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038665_lease_asset_depr.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 06/25/2014 Charlie Shilling
||============================================================================
*/

alter table LS_ASSET add DEPR_GROUP_ID number(22,0);
comment on column LS_ASSET.DEPR_GROUP_ID is 'Indicates the depreciation group this asset maps to.';

alter table LS_ASSET
   add constraint FK_LS_ASSET_DG
       foreign key(DEPR_GROUP_ID)
       references DEPR_GROUP(DEPR_GROUP_ID);

create table LS_DEPR_FORECAST
(
 LS_ASSET_ID           number(22,0),
 REVISION              number(22,0),
 SET_OF_BOOKS_ID       number(22,0),
 "MONTH"               date,
 BEGIN_RESERVE         number(22,2),
 DEPR_EXPENSE          number(22,2),
 DEPR_EXP_ALLOC_ADJUST number(22,2),
 END_RESERVE           number(22,2),
 TIME_STAMP            date,
 USER_ID               varchar2(18)
);

alter table LS_DEPR_FORECAST
   add constraint FK_LS_DEPR_ASSET_ID
       foreign key(LS_ASSET_ID)
       references LS_ASSET(LS_ASSET_ID);

alter table LS_DEPR_FORECAST
   add constraint FK_LS_SOB_ID
       foreign key(SET_OF_BOOKS_ID)
       references SET_OF_BOOKS(SET_OF_BOOKS_ID);

alter table LS_DEPR_FORECAST
   add constraint PK_DEPR_FCST
       primary key (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, "MONTH")
       using index tablespace PWRPLANT_IDX;

comment on table LS_DEPR_FORECAST is 'A table that holds the forecast depreciation for a leased asset (prior to in service)';

comment on column LS_DEPR_FORECAST.LS_ASSET_ID is 'The leased asset.';
comment on column LS_DEPR_FORECAST.REVISION is 'The revision of the leased asset.';
comment on column LS_DEPR_FORECAST.SET_OF_BOOKS_ID is 'Set of books.';
comment on column LS_DEPR_FORECAST."MONTH" is 'The month for this depreciation.';
comment on column LS_DEPR_FORECAST.BEGIN_RESERVE is 'The beginning reserve for this asset.';
comment on column LS_DEPR_FORECAST.DEPR_EXPENSE is 'The depreciation expense for the month.';
comment on column LS_DEPR_FORECAST.DEPR_EXP_ALLOC_ADJUST is 'The adjustment to the depreciation expense.';
comment on column LS_DEPR_FORECAST.END_RESERVE is 'The ending reserve for this asset.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1232, 0, 10, 4, 3, 0, 38665, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038665_lease_asset_depr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
