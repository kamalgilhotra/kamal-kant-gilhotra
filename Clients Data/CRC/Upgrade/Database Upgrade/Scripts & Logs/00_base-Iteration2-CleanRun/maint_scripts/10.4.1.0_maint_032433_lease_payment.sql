/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032433_lease_payment.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/16/2013 Ryan Oliveria  Point Release
||============================================================================
*/

alter table LS_PAYMENT_HDR add SENT_TO_AP number(22,0);

update LS_PAYMENT_HDR set SENT_TO_AP = 0 where SENT_TO_AP is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (619, 0, 10, 4, 1, 0, 32433, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032433_lease_payment.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
