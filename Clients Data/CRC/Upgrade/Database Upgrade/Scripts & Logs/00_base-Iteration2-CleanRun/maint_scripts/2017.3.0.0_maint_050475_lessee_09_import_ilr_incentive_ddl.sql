/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_09_import_ilr_incentive_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/25/2018 Shane "C" Ward    Create tables for Lessee Incentive Import
||============================================================================
*/

CREATE TABLE LS_IMPORT_ILR_INCENTIVE
  (
     import_run_id      NUMBER(22, 0) NOT NULL,
     line_id            NUMBER(22, 0) NOT NULL,
     error_message      VARCHAR2(4000),
     time_stamp         DATE,
     user_id            VARCHAR2(18),
     ilr_incentive_id         NUMBER(22, 0),
     ilr_id             NUMBER(22, 0),
     ilr_id_xlate       VARCHAR2(254),
     revision           NUMBER(22, 0),
     incentive_group_id       NUMBER(22, 0),
     incentive_group_id_xlate VARCHAR2(254),
     date_incurred      VARCHAR2(35),
     amount             NUMBER(22, 2),
     description        VARCHAR2(254)
  );

ALTER TABLE LS_IMPORT_ILR_INCENTIVE
  ADD CONSTRAINT LS_IMPORT_ILR_INCENTIVE_pk PRIMARY KEY ( import_run_id, line_id );

ALTER TABLE LS_IMPORT_ILR_INCENTIVE
  ADD CONSTRAINT LS_IMPORT_ILR_INCENTIVE_fk FOREIGN KEY ( import_run_id ) REFERENCES PP_IMPORT_RUN ( import_run_id );

COMMENT ON TABLE LS_IMPORT_ILR_INCENTIVE IS '(S) [06] The LSR Import ILR Initial Direct Cost table is an API table used to import Lessee ILR Initial Direct Costs.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.ilr_incentive_id IS 'System-assigned identifier of a Lessee ILR Incentive Line.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.ilr_id IS 'System-assigned identifier of a Lessee ILR.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.ilr_id_xlate IS 'Translate field for System-assigned identifier of a Lessee ILR.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.revision IS 'System-assigned identifier of a Lessee ILR revision.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.incentive_group_id IS 'System-assigned identifier of an incentive group.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.incentive_group_id_xlate IS 'Translate field for System-assigned identifier of an incentive group.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.date_incurred IS 'The date on which the Incentive Amount applied. Entered in MM/DD/YYYY format';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.amount IS 'Incentive Amount';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE.description IS 'Description of the ILR incentive group assoiation.';

CREATE TABLE LS_IMPORT_ILR_INCENTIVE_ARC
  (
     import_run_id      NUMBER(22, 0) NOT NULL,
     line_id            NUMBER(22, 0) NOT NULL,
     error_message      VARCHAR2(4000),
     time_stamp         DATE,
     user_id            VARCHAR2(18),
     ilr_incentive_id         NUMBER(22, 0),
     ilr_id             NUMBER(22, 0),
     ilr_id_xlate       VARCHAR2(254),
     revision           NUMBER(22, 0),
     incentive_group_id       NUMBER(22, 0),
     incentive_group_id_xlate VARCHAR2(254),
     date_incurred      VARCHAR2(35),
     amount             NUMBER(22, 2),
     description        VARCHAR2(254)
  );

ALTER TABLE LS_IMPORT_ILR_INCENTIVE_ARC
  ADD CONSTRAINT ls_import_ilr_incentive_arc_pk PRIMARY KEY ( import_run_id, line_id );

ALTER TABLE LS_IMPORT_ILR_INCENTIVE_ARC
  ADD CONSTRAINT ls_import_ilr_incentive_arc_fk FOREIGN KEY ( import_run_id ) REFERENCES PP_IMPORT_RUN ( import_run_id );

COMMENT ON TABLE LS_IMPORT_ILR_INCENTIVE_ARC IS '(S) [06] The Lessee Import ILR Incentive table is an API table used to import Lessee Incentives.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.ilr_incentive_id IS 'System-assigned identifier of a Lessee ILR Incentive Line.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.ilr_id IS 'System-assigned identifier of a Lessee ILR.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.ilr_id_xlate IS 'Translate field for System-assigned identifier of a Lessee ILR.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.revision IS 'System-assigned identifier of a Lessee ILR revision.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.incentive_group_id IS 'System-assigned identifier of an incentive group.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.incentive_group_id_xlate IS 'Translate field for System-assigned identifier of an incentive group.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.date_incurred IS 'The date on which the incentive amount applied. Entered in MM/DD/YYYY format';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.amount IS 'Incentive Amount';

COMMENT ON COLUMN LS_IMPORT_ILR_INCENTIVE_ARC.description IS 'Description of the ILR incentive group assoiation.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4626, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_09_import_ilr_incentive_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;