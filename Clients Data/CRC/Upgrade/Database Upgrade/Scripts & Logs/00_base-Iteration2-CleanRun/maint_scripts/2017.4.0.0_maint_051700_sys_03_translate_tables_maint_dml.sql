/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051700_sys_03_translate_tables_maint_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/29/2018 TechProdMgmt	New Tables to support generic translates
||============================================================================
*/

--pp tables
insert into powerplant_tables (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values ('pp_translate_elements', null, null, 's', 'Translate Elements', 'Translate Elements', 'always', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

--pp columns
insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('element_id', 'pp_translate_elements', null, null, null, 's', null, 'Element ID', null, 1, null, null, null, null, null, null, null, null);

insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('description', 'pp_translate_elements', null, null, null, 'e', null, 'Description', null, 2, null, null, null, null, null, null, null, null);

insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('label', 'pp_translate_elements', null, null, null, 'e', null, 'Label', null, 3, null, null, null, null, null, null, null, null);

insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('data_column', 'pp_translate_elements', null, null, null, 'e', null, 'Data Column', null, 5, null, null, null, null, null, null, null, null);

insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('display_column', 'pp_translate_elements', null, null, null, 'e', null, 'Display Column', null, 6, null, null, null, null, null, null, null, null);

insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('time_stamp', 'pp_translate_elements', null, null, null, 'e', null, 'Time Stamp', null, 100, null, null, null, null, null, null, null, null);

insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('user_id', 'pp_translate_elements', null, null, null, 'e', null, 'User ', null, 100, null, null, null, null, null, null, null, null);

insert into powerplant_columns (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values ('table_name', 'pp_translate_elements', null, null, 'table_names', 'p', null, 'Table Name', null, 4, null, null, null, null, null, null, null, null);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7593, 0, 2017, 4, 0, 0, 51700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051700_sys_03_translate_tables_maint_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
