/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030545_depr_01_cpr_depr_calc_stg.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/09/2013 B. Beck        Point release
||============================================================================
*/

create global temporary table CPR_DEPR_CALC_STG
(
 ASSET_ID                  number(22,0),
 SET_OF_BOOKS_ID           number(22,0),
 GL_POSTING_MO_YR          date,
 TIME_STAMP                date,
 USER_ID                   varchar2(18),
 INIT_LIFE                 number(22,2),
 REMAINING_LIFE            number(22,2),
 ESTIMATED_SALVAGE         number(22,8),
 BEG_ASSET_DOLLARS         number(22,2),
 NET_ADDS_AND_ADJUST       number(22,2),
 RETIREMENTS               number(22,2),
 TRANSFERS_IN              number(22,2),
 TRANSFERS_OUT             number(22,2),
 ASSET_DOLLARS             number(22,2),
 BEG_RESERVE_MONTH         number(22,2),
 SALVAGE_DOLLARS           number(22,2),
 RESERVE_ADJUSTMENT        number(22,2),
 COST_OF_REMOVAL           number(22,2),
 RESERVE_TRANS_IN          number(22,2),
 RESERVE_TRANS_OUT         number(22,2),
 DEPR_EXP_ADJUST           number(22,2),
 OTHER_CREDITS_AND_ADJUST  number(22,2),
 GAIN_LOSS                 number(22,2),
 DEPRECIATION_BASE         number(22,2),
 CURR_DEPR_EXPENSE         number(22,2),
 DEPR_RESERVE              number(22,2),
 BEG_RESERVE_YEAR          number(22,2),
 YTD_DEPR_EXPENSE          number(22,2),
 YTD_DEPR_EXP_ADJUST       number(22,2),
 PRIOR_YTD_DEPR_EXPENSE    number(22,2),
 PRIOR_YTD_DEPR_EXP_ADJUST number(22,2),
 ACCT_DISTRIB              varchar2(254),
 MONTH_RATE                number(22,8),
 COMPANY_ID                number(22,0),
 MID_PERIOD_METHOD         varchar2(35),
 MID_PERIOD_CONV           number(22,8),
 DEPR_GROUP_ID             number(22,0),
 DEPR_EXP_ALLOC_ADJUST     number(22,2),
 DEPR_METHOD_ID            number(22,0),
 TRUE_UP_CPR_DEPR          number(22,0),
 SALVAGE_EXPENSE           number(22,2),
 SALVAGE_EXP_ADJUST        number(22,2),
 SALVAGE_EXP_ALLOC_ADJUST  number(22,2),
 IMPAIRMENT_ASSET_AMOUNT   number(22,2),
 IMPAIRMENT_EXPENSE_AMOUNT number(22,2),
 DEPRECIATION_INDICATOR    number(22,0),
 END_OF_LIFE               number(22,0),
 ENG_IN_SERVICE_YEAR       date,
 ESTIMATED_PRODUCTION      number(22,2),
 HAS_CFNU                  number(1,0),
 HAS_NURV                  number(1,0),
 HAS_NURV_LAST_MONTH       number(1,0),
 NET_GROSS                 number(1,0),
 OVER_DEPR_CHECK           number(1,0),
 PRODUCTION                number(22,2),
 RATE                      number(22,8),
 EXISTS_LAST_MONTH         number(1,0),
 EXISTS_TWO_MONTHS         number(1,0),
 EXISTS_ARO                number(1,0),
 EFFECTIVE_DATE            date
) on commit preserve rows;

alter table CPR_DEPR_CALC_STG
   add constraint PK_CPR_DEPR_STG
       primary key (ASSET_ID, SET_OF_BOOKS_ID, GL_POSTING_MO_YR);

create index IX_CPR_DEPR_STG_COMP_DATE
   on CPR_DEPR_CALC_STG (COMPANY_ID, GL_POSTING_MO_YR);

create index IX_CPR_DEPR_STG_DATE
   on CPR_DEPR_CALC_STG (GL_POSTING_MO_YR);

create index IX_CPR_DEPR_STG_ASSET
   on CPR_DEPR_CALC_STG (ASSET_ID);

create index IX_CPR_DEPR_STG_DEPR_GROUP
   on CPR_DEPR_CALC_STG (DEPR_GROUP_ID);

create index IX_CPR_DEPR_STG_MPM
   on CPR_DEPR_CALC_STG (MID_PERIOD_METHOD);

create index IX_CPR_DEPR_STG_MPC
   on CPR_DEPR_CALC_STG (MID_PERIOD_CONV);

create index IX_CPR_DEPR_STG_LM
   on CPR_DEPR_CALC_STG (EXISTS_LAST_MONTH);

create index IX_CPR_DEPR_STG_TM
   on CPR_DEPR_CALC_STG (EXISTS_TWO_MONTHS);

create index IX_CPR_DEPR_STG_ARO
   on CPR_DEPR_CALC_STG (EXISTS_ARO);


create index IX_DEPR_GROUP_CO
   on DEPR_GROUP (COMPANY_ID)
      tablespace PWRPLANT_IDX;

create index IX_DM_RATES_SOB_METHOD_ID
   on DEPR_METHOD_RATES (DEPR_METHOD_ID, SET_OF_BOOKS_ID)
      tablespace PWRPLANT_IDX;

create index IX_CPRACT_ASSET_GLMO_ACT_CD
   on CPR_ACTIVITY (ASSET_ID, GL_POSTING_MO_YR, TRIM(ACTIVITY_CODE))
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (449, 0, 10, 4, 1, 0, 30545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030545_depr_01_cpr_depr_calc_stg.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
