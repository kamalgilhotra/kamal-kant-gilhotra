/*
||=============================================================================
|| Application: PowerPlant
|| File Name:   maint_044379_cr_pp_integration_3_dml.sql
||=============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- -------------------------------------
|| 2015.2     08/11/2015 Kevin Dettbarn   Created tables for PP Integration
||                                        Import / Export tool.
||=============================================================================
*/

insert into pp_integration_components (component, argument1_title) values ('PP File Import', 'Batch Name (case sensitive)');

insert into pp_integration_components (component, argument1_title) values ('PP File Export', 'Batch Name (case sensitive)');

insert into pp_integration_components (component, argument1_title) values ('PP XML Export', 'Batch Name (case sensitive)');

insert into pp_integration_components (component, argument1_title) values ('PP XML Import', 'Batch Name (case sensitive)');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2769, 0, 2015, 2, 0, 0, 044379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044379_cr_pp_integration_3_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;