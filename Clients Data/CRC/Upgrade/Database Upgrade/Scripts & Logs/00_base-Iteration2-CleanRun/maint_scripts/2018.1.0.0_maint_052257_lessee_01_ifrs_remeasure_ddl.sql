/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052257_lessee_01_ifrs_remeasure_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/15/2018 Shane "C" Ward Add column to LS_ILR_SCHEDULE_STG to hold Escalation Amount (amount Payment is escalated by)
||============================================================================
*/

--Schedule calc columns for tracking escalations and IFRS flags
ALTER TABLE LS_ILR_SCHEDULE_STG ADD ESCALATION_AMT NUMBER(22,2);

COMMENT ON COLUMN ls_ilr_schedule_stg.escalation_amt IS 'Holds Amount by which the payment is escalated by multiplying Escalation Percent with the orignal Payment Amount';

ALTER TABLE ls_ilr_stg ADD is_remeasurement NUMBER(1);

COMMENT ON COLUMN ls_ilr_stg.is_remeasurement IS 'Flag for whether the ILR and Set of Books is calculating as a Remeasurement';

ALTER TABLE ls_ilr_stg ADD include_esc_in_pay NUMBER(1);

COMMENT ON COLUMN ls_ilr_stg.include_esc_in_pay IS 'Flag for whether the amount the payment is escalated by is included in the payment or put into a contingent bucket';

--Update Comment on Remeasurement Type for new Remeasure Type
COMMENT ON COLUMN ls_ilr_options.remeasurement_type IS 'System assigned value for type of remeasurement for ILR (0: Remeasurement, 1: Partial Termination Liability, 2: Partial Termination Quantity, 3: IFRS Remeasurement).  User cannot select any of these values.';

--New Table for holding Remeasurement Exclusions
create table ls_ifrs_remeasure_exclude
(ilr_id number(22,0) not null,
gl_posting_mo_yr date not null,
time_stamp date null,
user_id varchar2(18) null);

ALTER TABLE ls_ifrs_remeasure_exclude ADD CONSTRAINT ls_ifrs_remeasure_exclude_pk PRIMARY KEY (
ilr_id, gl_posting_mo_yr)
USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE ls_ifrs_remeasure_exclude
  ADD CONSTRAINT ls_ifrs_remeasure_exclude_fk1 FOREIGN KEY (
    ilr_id
  ) REFERENCES ls_ilr (
    ilr_id
  )
;

COMMENT ON TABLE ls_ifrs_remeasure_exclude IS '(S) [06] Holds a list of ILRs by Set of Books and Month for which IFRS remeasurements were created and excluded by user or by system calculation.';
COMMENT ON COLUMN ls_ifrs_remeasure_exclude.ilr_id IS 'System assigned ID uniquely identifying the ILR.';
COMMENT ON COLUMN ls_ifrs_remeasure_exclude.gl_posting_mo_yr IS 'GL Month the IFRS remeasurement was calculated for';
COMMENT ON COLUMN ls_ifrs_remeasure_exclude.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_ifrs_remeasure_exclude.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--Refresh Asset Schedule VIEW
CREATE OR REPLACE VIEW V_LS_ASSET_REMEASUREMENT_AMTS AS
SELECT ilr_id, revision, ls_asset_id, set_of_books_id, remeasurement_date, current_revision, om_to_cap_indicator,
prior_month_end_obligation,
prior_month_end_liability,
prior_month_end_lt_obligation,
prior_month_end_lt_liability,
prior_month_end_deferred_rent,
prior_month_end_prepaid_rent,
prior_month_end_depr_reserve,
prior_month_end_nbv,
prior_month_end_capital_cost,
prior_month_net_rou_asset,
prior_month_end_arrears_accr,
remeasurement_type,
partial_termination,
original_asset_quantity,
current_asset_quantity,
is_remeasurement,
Sum(unpaid_accrued_interest) AS unpaid_accrued_interest
FROM (
  SELECT stg.ilr_id, stg.revision, sch.ls_asset_id, sch.set_of_books_id, o.remeasurement_date, ilr.current_revision,
  CASE
    WHEN sch.is_om = 1 AND stg.is_om = 0 THEN
      1
    ELSE
      0
  END om_to_cap_indicator,
  sch.end_obligation AS prior_month_end_obligation, sch.end_liability AS prior_month_end_liability,
  sch.end_lt_obligation AS prior_month_end_lt_obligation, sch.end_lt_liability AS prior_month_end_lt_liability,
  sch.end_deferred_rent AS prior_month_end_deferred_rent,
  sch.end_prepaid_rent AS prior_month_end_prepaid_rent,
  depr.end_reserve AS prior_month_end_depr_reserve,
  sch.end_capital_cost - depr.end_reserve AS prior_month_end_nbv,
  sch.end_capital_cost AS prior_month_end_capital_cost,
  sch.end_net_rou_asset as prior_month_net_rou_asset,
  sch.end_arrears_accrual AS prior_month_end_arrears_accr,
  o.remeasurement_type as remeasurement_type,
  o.partial_termination as partial_termination,
  prior_opt.asset_quantity as original_asset_quantity,
  o.asset_quantity AS current_asset_quantity,
  1 is_remeasurement,
  CASE WHEN int_sch.MONTH >= Nvl(
                                  Add_Months(Max(Decode(int_sch.interest_paid, 0, NULL, int_sch.MONTH)) OVER(PARTITION BY int_sch.ls_asset_id, int_sch.revision, int_sch.set_of_books_id), Decode(mla.pre_payment_sw, 0, 1, 0)),
                                  First_Value(int_sch.MONTH) OVER(PARTITION BY int_sch.ls_asset_id, int_sch.revision, int_sch.set_of_books_id ORDER BY int_sch.month)
                                )
  THEN
    int_sch.interest_accrual
  ELSE
    0
  END unpaid_accrued_interest
  FROM ls_ilr_stg stg
    JOIN ls_ilr ilr ON stg.ilr_id = ilr.ilr_id
    JOIN ls_ilr_options o ON stg.ilr_id = o.ilr_id AND stg.revision = o.revision
    JOIN ls_ilr_options prior_opt ON stg.ilr_id = prior_opt.ilr_id AND prior_opt.revision = ilr.current_revision
    JOIN ls_ilr_asset_map map ON map.ilr_id = stg.ilr_id AND map.revision = ilr.current_revision
    JOIN ls_asset_schedule sch ON map.ls_asset_id = sch.ls_asset_id AND sch.revision = ilr.current_revision AND sch.set_of_books_id = stg.set_of_books_id
    left OUTER JOIN ls_depr_forecast depr ON depr.ls_asset_id = map.ls_asset_id AND depr.revision = ilr.current_revision AND depr.set_of_books_id = stg.set_of_books_id
    JOIN ls_lease mla ON mla.lease_id = ilr.lease_id
    JOIN ls_asset_schedule int_sch ON int_sch.ls_asset_id = sch.ls_asset_id AND int_sch.revision = ilr.current_revision AND int_sch.set_of_books_id = sch.set_of_books_id AND int_sch.MONTH < Trunc(o.remeasurement_date, 'month')
  WHERE ilr.current_revision <> stg.revision
  AND o.remeasurement_date IS NOT NULL
  AND stg.npv_start_date is not null -- Filter out Sets of Books not doing Remeasurement (NPV Start Date is null)
  AND Trunc(o.remeasurement_date, 'month') = Add_Months(sch.MONTH,1)
  AND (Trunc(o.remeasurement_date, 'month') = Add_Months(depr.MONTH,1) OR depr.MONTH IS NULL)
)
GROUP BY ilr_id, revision, ls_asset_id, set_of_books_id, remeasurement_date, current_revision, om_to_cap_indicator,
prior_month_end_obligation,
prior_month_end_liability,
prior_month_end_lt_obligation,
prior_month_end_lt_liability,
prior_month_end_deferred_rent,
prior_month_end_prepaid_rent,
prior_month_end_depr_reserve,
prior_month_end_nbv,
prior_month_end_capital_cost,
prior_month_net_rou_asset,
prior_month_end_arrears_accr,
remeasurement_type,
partial_termination,
original_asset_quantity,
current_asset_quantity,
is_remeasurement;

--Rebuild IDC Incentive Accrual VIEW
CREATE OR replace VIEW v_ls_ilr_idc_inc_accrued_fx_vw
AS
WITH ilr_info AS
	 (SELECT stg.ilr_id,
			 stg.revision,
			 appr.revision appr_revision,
			 decode(sign(stg.revision), -1, npv_start_date, curr.remeasurement_date) remeasurement_date,
			 i.est_in_svc_date,
			 stg.is_om curr_is_om,
			 stg.set_of_books_id,
			 npv_start_date
		FROM ls_ilr         i,
			 ls_ilr_options curr,
			 ls_ilr_options appr,
			 ls_ilr_stg     stg
	   WHERE i.ilr_id = curr.ilr_id
		 AND curr.ilr_id = stg.ilr_id
		 AND curr.revision = stg.revision
		 AND i.current_revision = appr.revision
		 AND i.ilr_id = appr.ilr_id),
	num_months AS
	 (SELECT ilr.ilr_id,
			 ilr.set_of_books_id,
			 COUNT(MONTH) num_months
		FROM ls_ilr_schedule sched,
			 ilr_info        ilr
	   WHERE sched.ilr_id = ilr.ilr_id
		 AND sched.revision = ilr.appr_revision
		 AND sched.set_of_books_id = ilr.set_of_books_id
	   GROUP BY ilr.ilr_id,
				ilr.set_of_books_id)
	SELECT i.ilr_id ilr_id,
		   i.revision revision,
		   i.set_of_books_id set_of_books_id,
		   i.npv_start_date,
		   n.num_months,
       Sum(Nvl(initial_direct_cost,0)) accrued_idc_from_cap,
		   round((SUM(initial_direct_cost) / n.num_months) * months_between(remeasurement_date, MIN(s.month)), 2) accrued_idc_from_om, -- Rounding plug at end of calc will take care of rounding issues
		   SUM(incentive_amount - nvl(decode(s.month, i.est_in_svc_date, 0, incentive_math_amount), 0)) accrued_incentive,
		   months_between(remeasurement_date, MIN(s.month)) months_already_accrued,
		   i.remeasurement_date,
		   i.curr_is_om curr_is_om,
		   s.is_om approved_is_om,
		   decode(s.is_om, 1, decode(i.curr_is_om, 0, 1), 0) switch_to_cap --whether or not we are converting cap types on the schedule being calculated
	  FROM ls_ilr_schedule s,
		   ilr_info        i,
		   num_months      n
	 WHERE s.ilr_id = i.ilr_id
	   AND s.revision = i.appr_revision
	   AND s.set_of_books_id = i.set_of_books_id
	   AND n.ilr_id = i.ilr_id
	   AND n.set_of_books_id = i.set_of_books_id
	   AND s.month < i.npv_start_date
	   AND i.npv_start_date is not null -- Make sure to filter out Sets of Books in ILR_STG that have no NPV Start Date Which means no IDC Incentive Accrual
	 GROUP BY i.ilr_id,
			  i.revision,
			  i.set_of_books_id,
			  i.remeasurement_date,
			  i.curr_is_om,
			  s.is_om,
			  npv_start_date,
			  n.num_months
	UNION
	SELECT DISTINCT ilr_id,
					revision,
					set_of_books_id,
					npv_start_date,
					0               num_months,
					0               accrued_idc_from_cap,
					0               accrued_idc_from_om,
					0               accrued_incentive,
					NULL            months_already_accrued,
					NULL            remeasurement_date,
					0               curr_is_om,
					0               approved_is_om,
					0               switch_to_cap
	  FROM ls_ilr_stg
	 WHERE npv_start_date IS NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11442, 0, 2018, 1, 0, 0, 50961, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052257_lessee_01_ifrs_remeasure_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;