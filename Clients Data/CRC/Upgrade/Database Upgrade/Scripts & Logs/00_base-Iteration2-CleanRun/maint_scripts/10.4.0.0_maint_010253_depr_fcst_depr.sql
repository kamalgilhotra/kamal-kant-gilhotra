/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010253_depr_fcst_depr.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   10/10/2012 Colin Lee
||============================================================================
*/

alter table FCST_DEPR_GROUP         drop column TAX_FCST_SUM_ID;
alter table FCST_DEPR_GROUP_VERSION drop column TAX_FCST_SUM_ID;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (237, 0, 10, 4, 0, 0, 10253, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010253_depr_fcst_depr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
