/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051711_lessee_01_purchase_option_config_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.4.0.0 06/07/2018 Sarah Byers      New Sys Control and Trans Types for PO JE's
||============================================================================
*/

insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select *
from (select
      max(control_id)+1 AS control_id,
      'Lease Term PO Payment Include',
      'no' AS control_value,
      'dw_yes_no;1' AS description,
      'Yes: Include the Purchase Option amount as a payment line, No: Do not include the purchase option amount as a payment line.' AS long_description,
      -1 AS company_id
  from pp_system_control_company
  )
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease Term PO Payment Include');


insert into je_trans_type (trans_type, description)
values (3069, '3069 - Lease Purchase Option Debit');

insert into je_trans_type (trans_type, description)
values (3070, '3070 - Lease Purchase Option Credit');

insert into je_trans_type (trans_type, description)
values (3071, '3071 - Lease PO Gain/Loss Debit');

insert into je_trans_type (trans_type, description)
values (3072, '3072 - Lease PO Gain/Loss Credit');

insert into je_method_trans_type(je_method_id, trans_type)
select * 
from (select 1 AS je_method_id, trans_type 
    from je_trans_type 
    where trans_type in (3069, 3070, 3071, 3072)
   )
where not exists(
  select 1
  from je_method_trans_type
  where trans_type in (3069, 3070, 3071, 3072));

insert into ls_payment_type (payment_type_id, description)
values (31, 'Purchase Option');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7466, 0, 2017, 4, 0, 0, 51711, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051711_lessee_01_purchase_option_config_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
