/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052474_lessee_01_impair_wksp_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/01/2018 K Powers       Impairment Workspace
||============================================================================
*/

alter table LS_ILR_OPTIONS ADD IMPAIRMENT_DATE DATE;

-- Drop previous version of table

drop table ls_ILR_IMPAIR;

create table LS_ILR_IMPAIR
(ilr_id   number(22,0),
orig_revision  number(22,0),
processed_revision number(22,0),
month     date,
set_of_books_id number(22,0),
impairment_activity number,
processed_flag  number(22,0),
time_stamp date null,
user_id varchar2(18) null);

-- Add constraints

ALTER TABLE LS_ILR_IMPAIR 
ADD PRIMARY KEY(ilr_id, month, orig_revision, set_of_books_id) using index tablespace pwrplant_idx;

ALTER TABLE LS_ILR_IMPAIR 
ADD CONSTRAINT FK_LS_ILR_IMPAIR_1 
  FOREIGN KEY (ILR_ID)
  REFERENCES LS_ILR(ILR_ID);
  
ALTER TABLE LS_ILR_IMPAIR 
ADD CONSTRAINT FK_LS_ILR_IMPAIR_2 
  FOREIGN KEY (SET_OF_BOOKS_ID)
  REFERENCES SET_OF_BOOKS(SET_OF_BOOKS_ID);

-- Table comments

COMMENT ON TABLE LS_ILR_IMPAIR IS 'Table for ILR Impairments for the current month.';
COMMENT ON COLUMN LS_ILR_IMPAIR.ILR_ID IS 'The unique ILR identifier.';
COMMENT ON COLUMN LS_ILR_IMPAIR.ORIG_REVISION IS 'The ILR current approved revision.';
COMMENT ON COLUMN LS_ILR_IMPAIR.PROCESSED_REVISION IS 'The ILR revision being impaired.';
COMMENT ON COLUMN LS_ILR_IMPAIR.MONTH IS 'The current open month.';
COMMENT ON COLUMN LS_ILR_IMPAIR.SET_OF_BOOKS_ID IS 'Unique Set of Books identifier';
COMMENT ON COLUMN LS_ILR_IMPAIR.IMPAIRMENT_ACTIVITY IS 'Impairment dollar amount. A negative value is an impairment reversal.';
COMMENT ON COLUMN LS_ILR_IMPAIR.PROCESSED_FLAG IS 'Details status of ILR impairment: 0-Unprocessed; 1=Processed; 2=Approved.';
COMMENT ON COLUMN LS_ILR_IMPAIR.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN LS_ILR_IMPAIR.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';


COMMENT ON COLUMN LS_ILR_OPTIONS.IMPAIRMENT_DATE IS 'Accounting month of impairment.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (11451, 0, 2018, 1, 0, 0, 52474, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052474_lessee_01_impair_wksp_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;