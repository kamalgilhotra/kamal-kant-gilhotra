/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040645_pcm_fp_init_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 01/28/2015 Alex P.          WO/FP system options
||============================================================================
*/

-- Set up new System Options based on System Controls

-- Budget Initiate Messagebox
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Budget Initiate Messagebox', long_description, 0, 'Yes', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'budget initiate messagebox';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Budget Initiate Messagebox', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Budget Initiate Messagebox', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Budget Initiate Messagebox', 'pcm');
	
-- CapBud - WOT to Budget Summary
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'CapBud - WOT to Budget Summary', long_description, 0, 'Yes', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'capbud - wot to budget summary';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'CapBud - WOT to Budget Summary', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'CapBud - WOT to Budget Summary', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'CapBud - WOT to Budget Summary', 'pcm');	
	
-- FPInt-Validate WO Type/Bud Item Co
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'FPInt-Validate WO Type/Bud Item Co', long_description, 0, 'Yes', control_value, 1, 1
  from pp_system_control
 where lower(control_name) = 'fpint-validate wo type/bud item co';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPInt-Validate WO Type/Bud Item Co', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPInt-Validate WO Type/Bud Item Co', 'BS-Only');	
	
insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPInt-Validate WO Type/Bud Item Co', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'FPInt-Validate WO Type/Bud Item Co', 'pcm');

-- Funding Project Generation
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Funding Project Generation', long_description, 0, 'Not Automatic', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'funding project generation';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Funding Project Generation', 'Not Automatic');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Funding Project Generation', 'Automatic');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Funding Project Generation', 'pcm');
	
-- Override Edit Auto Gen FP Num
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Override Edit Auto Gen FP Num', long_description, 0, 'Yes', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'override edit auto gen fp num';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Override Edit Auto Gen FP Num', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Override Edit Auto Gen FP Num', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Override Edit Auto Gen FP Num', 'pcm');	
	
-- WOInit - Open Projects Only
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WOInit - Open Projects Only', long_description, 0, 'Yes', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'woinit - open projects only';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOInit - Open Projects Only', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOInit - Open Projects Only', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WOInit - Open Projects Only', 'pcm');
	
-- Work Order Type Default
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Work Order Type Default', long_description, 0, 'Yes', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'work order type default';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Type Default', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Type Default', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Work Order Type Default', 'pcm');
	
-- Work Order Type Default-Many
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Work Order Type Default-Many', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'work order type default-many';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Type Default-Many', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Type Default-Many', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Work Order Type Default-Many', 'pcm');	
	
-- Work Order Generation
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Work Order Generation', long_description, 0, 'Non Automatic', control_value, 1, 1
  from pp_system_control
 where lower(control_name) = 'work order generation';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Generation', 'Non Automatic');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Generation', 'Automatic');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Work Order Generation', 'pcm');
	
-- Work Order Initiation
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'Work Order Initiation', long_description, 0, 'Do Not Inherit', control_value, 1, 1
  from pp_system_control
 where lower(control_name) = 'work order initiation';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Initiation', 'Do Not Inherit');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Initiation', 'Inherit');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'Work Order Initiation', 'Inherit Require WO Type');
	
insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'Work Order Initiation', 'pcm');
	
---------------- WO/FP Detail System options --------------------------
-- FPEst - Validate vs FP Type
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'FPEst - Validate vs FP Type', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'fpest - validate vs fp type';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPEst - Validate vs FP Type', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPEst - Validate vs FP Type', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'FPEst - Validate vs FP Type', 'pcm');	
	
-- WODetail - Audit WO Dates vs. FP
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WODetail - Audit WO Dates vs. FP', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'wodetail - audit wo dates vs. fp';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Audit WO Dates vs. FP', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Audit WO Dates vs. FP', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODetail - Audit WO Dates vs. FP', 'pcm');	
	
-- WODetail - Dept/Div/Bus Seg
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WODetail - Dept/Div/Bus Seg', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'wodetail - dept/div/bus seg';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Dept/Div/Bus Seg', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Dept/Div/Bus Seg', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODetail - Dept/Div/Bus Seg', 'pcm');
	
-- WODetail - Dept/Div/Major Loc
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WODetail - Dept/Div/Major Loc', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'wodetail - dept/div/major loc';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Dept/Div/Major Loc', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Dept/Div/Major Loc', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODetail - Dept/Div/Major Loc', 'pcm');
	
-- WODetail - Edit FP Budget
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WODetail - Edit FP Budget', long_description, 0, 'Yes', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'wodetail - edit fp budget';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Edit FP Budget', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Edit FP Budget', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODetail - Edit FP Budget', 'pcm');
	
-- WODetail - Edit Funding Project
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WODetail - Edit Funding Project', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'wodetail - edit funding project';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Edit Funding Project', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Edit Funding Project', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODetail - Edit Funding Project', 'pcm');
	
-- WODetail - Edit Work Order Type
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WODetail - Edit Work Order Type', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'wodetail - edit work order type';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Edit Work Order Type', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODetail - Edit Work Order Type', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODetail - Edit Work Order Type', 'pcm');
	
-- WOEst - FP Date Change
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WOEst - FP Date Change', long_description, 0, 'Always Update', control_value, 1, 1
  from pp_system_control
 where lower(control_name) = 'woest - fp date change';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - FP Date Change', 'Never Update');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - FP Date Change', 'Always Update');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - FP Date Change', 'Update On Approval');
	
insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - FP Date Change', 'Update On Approval-Allow Hdr Edit');
	
insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WOEst - FP Date Change', 'pcm');
	
-- WOEst - WO Date Change
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WOEst - WO Date Change', long_description, 0, 'Never Update', control_value, 1, 1
  from pp_system_control
 where lower(control_name) = 'woest - wo date change';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - WO Date Change', 'Never Update');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - WO Date Change', 'Always Update');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - WO Date Change', 'Update On Approval');
	
insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOEst - WO Date Change', 'Update On Approval-Allow Hdr Edit');
	
insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WOEst - WO Date Change', 'pcm');
	
-- WOInit - Require Location
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'WOInit - Require Location', long_description, 0, 'No', initcap(control_value), 1, 1
  from pp_system_control
 where lower(control_name) = 'woinit - require location';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOInit - Require Location', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WOInit - Require Location', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WOInit - Require Location', 'pcm');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2232, 0, 2015, 1, 0, 0, 040645, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040645_pcm_fp_init_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;