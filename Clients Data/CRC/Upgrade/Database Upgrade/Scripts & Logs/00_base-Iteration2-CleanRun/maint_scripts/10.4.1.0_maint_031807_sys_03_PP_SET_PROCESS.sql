/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031807_sys_03_PP_SET_PROCESS.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 10.4.1.0   08/29/2013 Charlie Shilling maint-31807
||============================================================================
*/
create or replace function PP_SET_PROCESS(A_ARG varchar2) return varchar2 as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PP_SET_PROCESS
   || Description:
   ||============================================================================
   || Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.1                               Added a check for null argument
   || 1.1   08/29/2013 C Shilling       maint_31807 - Remove code - call package instead
   ||============================================================================
   */
begin
   return PP_PROCESS_PKG.PP_SET_PROCESS(A_ARG);
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (561, 0, 10, 4, 1, 0, 31807, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031807_sys_03_PP_SET_PROCESS.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
