/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033713_reg_02_chart_tables.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014  Sarah Byers
||============================================================================
*/

-- REG_CHART_GROUP
create table REG_CHART_GROUP
(
 REG_CHART_GROUP_ID  number(22,0) not null,
 DESCRIPTION         varchar2(35) not null,
 REG_JURISDICTION_ID number(22,0) not null,
 USER_ID             varchar2(18),
 TIME_STAMP          date
);

alter table REG_CHART_GROUP
   add constraint PK_REG_CHART_GROUP
       primary key (REG_CHART_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

-- REG_CHART_GROUP_LABELS
create table REG_CHART_GROUP_LABELS
(
 REG_CHART_GROUP_ID number(22,0) not null,
 SERIES_LABEL_ID    number(22,0) not null,
 DESCRIPTION        varchar2(35) not null,
 USER_ID            varchar2(18),
 TIME_STAMP         date
);

alter table REG_CHART_GROUP_LABELS
   add constraint PK_REG_CHART_GROUP_LABELS
       primary key (REG_CHART_GROUP_ID, SERIES_LABEL_ID)
       using index tablespace PWRPLANT_IDX;

-- REG_CHART_GROUP_CASES
create table REG_CHART_GROUP_CASES
(
 REG_CHART_GROUP_ID number(22,0) not null,
 SERIES_LABEL_ID    number(22,0) not null,
 REG_CASE_ID        number(22,0) not null,
 TEST_YEAR          number(6,0) not null,
 USER_ID            varchar2(18),
 TIME_STAMP         date
);

alter table REG_CHART_GROUP_CASES
   add constraint PK_REG_CHART_GROUP_CASES
       primary key (REG_CHART_GROUP_ID, SERIES_LABEL_ID, REG_CASE_ID, TEST_YEAR)
       using index tablespace PWRPLANT_IDX;

--------------------------
--FKs for REG_CHART_GROUP_LABELS
--------------------------
alter table REG_CHART_GROUP_LABELS
   add constraint R_REG_CHART_GROUP_LABELS1
       foreign key (REG_CHART_GROUP_ID)
       references REG_CHART_GROUP (REG_CHART_GROUP_ID);

--------------------------
--FKs for REG_CHART_GROUP_CASES
--------------------------
alter table REG_CHART_GROUP_CASES
   add constraint R_REG_CHART_GROUP_CASES1
       foreign key (REG_CHART_GROUP_ID)
       references REG_CHART_GROUP (REG_CHART_GROUP_ID);

alter table REG_CHART_GROUP_CASES
   add constraint R_REG_CHART_GROUP_CASES2
       foreign key (REG_CHART_GROUP_ID, SERIES_LABEL_ID)
       references REG_CHART_GROUP_LABELS (REG_CHART_GROUP_ID, SERIES_LABEL_ID);

alter table REG_CHART_GROUP_CASES
   add constraint R_REG_CHART_GROUP_CASES3
       foreign key (REG_CASE_ID)
       references REG_CASE (REG_CASE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (745, 0, 10, 4, 2, 0, 33713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033713_reg_02_chart_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
