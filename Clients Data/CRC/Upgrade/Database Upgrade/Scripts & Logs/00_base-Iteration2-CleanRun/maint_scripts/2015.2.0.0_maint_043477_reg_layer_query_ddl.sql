/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043477_reg_layer_query_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| --------   ---------- ---------------- --------------------------------------
|| 2015.2.0.0 06/23/2015 Sarah Byers		
||============================================================================
*/

-- Drop view REG_FORECAST_LEDGER_IFA_VIEW
drop view REG_FORECAST_LEDGER_IFA_VIEW;

-- Drop view REG_IFA_DETAILS_VIEW
drop view REG_IFA_DETAILS_VIEW;

-- Create view reg_forecast_ledger_layer_view
CREATE OR REPLACE VIEW reg_forecast_ledger_layer_view (
  reg_company,
  forecast_ledger,
  reg_account,
  reg_account_type,
  sub_account_type,
  reg_source,
  layer_type,
  layer_description,
  label,
  item,
  gl_month,
  fcst_amount,
  annualized_amt,
  adj_amount,
  adj_month,
  layer_amount,
  annualzied_layer_amount,
  recon_adj_amount,
  recon_adj_comment,
  total,
  annualized_total,
  reg_company_id,
  reg_acct_id,
  forecast_ledger_id,
  reg_source_id,
  reg_acct_type_id,
  sub_acct_type_id,
  incremental_adj_id,
  incremental_adj_type_id
) AS
(
select REG_COMPANY,
       FORECAST_LEDGER,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       REG_SOURCE,
       'Ledger' as LAYER_DESCRIPTION,
       '' as LAYER_TYPE,
       '' as LABEL,
       '' as ITEM,
       GL_MONTH,
       NVL(FCST_AMOUNT, 0) FCST_AMOUNT,
       NVL(ANNUALIZED_AMT, 0) ANNUALIZED_AMT,
       NVL(ADJ_AMOUNT, 0) ADJ_AMOUNT,
       ADJ_MONTH,
       0.00 as LAYER_AMOUNT,
       0.00 as ANNUALIZED_LAYER_AMOUNT,
       NVL(RECON_ADJ_AMOUNT, 0) RECON_ADJ_AMOUNT,
       RECON_ADJ_COMMENT,
       NVL(FCST_AMOUNT, 0) + NVL(ADJ_AMOUNT, 0) + NVL(RECON_ADJ_AMOUNT, 0) as TOTAL,
       NVL(ANNUALIZED_AMT, 0) as ANNUALIZED_TOTAL,
       REG_COMPANY_ID,
       s.REG_ACCT_ID,
       FORECAST_LEDGER_ID,
       s.REG_SOURCE_ID,
       t.REG_ACCT_TYPE_ID REG_ACCOUNT_TYPE_ID,
       st.SUB_ACCT_TYPE_ID SUB_ACCOUNT_TYPE_ID,
       -1 as INCREMENTAL_ADJ_ID,
       -1 as INCREMENTAL_ADJ_TYPE_ID
  from REG_FORECAST_LEDGER_ID_SV s,
  		 reg_acct_master m,
  		 reg_acct_type t,
  		 reg_sub_acct_type st
 WHERE s.reg_acct_id = m.reg_acct_id
	AND t.reg_acct_type_id = m.reg_acct_type_default
	AND t.reg_acct_type_id = st.reg_acct_type_id
	AND st.sub_acct_type_id = st.sub_acct_type_id
union
select RC.DESCRIPTION REG_COMPANY,
       RFV.DESCRIPTION as FORECAST_LEDGER,
       RAM.DESCRIPTION as REG_ACCOUNT,
       RAT.DESCRIPTION as REG_ACCOUNT_TYPE,
       RSAT.DESCRIPTION as SUB_ACCOUNT_TYPE,
       RS.DESCRIPTION as REG_SOURCE,
       RIT.DESCRIPTION as LAYER_TYPE,
       RIA.DESCRIPTION as LAYER_DESCRIPTION,
       IFAL.DESCRIPTION LABEL,
       IFAI.DESCRIPTION ITEM,
       RFL.GL_MONTH,
       0.00 as FCST_AMOUNT,
       0.00 as ANNUALZIED_AMT,
       0.00 as ADJ_AMOUNT,
       null as ADJ_MONTH,
       RFL.ADJ_AMOUNT as LAYER_AMOUNT,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_LAYER_AMOUNT,
       0 as RECON_ADJ_AMOUNT,
       '' as RECON_ADJ_COMMENT,
       RFL.ADJ_AMOUNT as TOTAL,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_TOTAL,
       RFL.REG_COMPANY_ID,
       RFL.REG_ACCT_ID,
       RFL.FORECAST_VERSION_ID as FORECAST_LEDGER_ID,
       RS.REG_SOURCE_ID,
       RAT.REG_ACCT_TYPE_ID AS REG_ACCOUNT_TYPE_ID,
       RSAT.SUB_ACCT_TYPE_ID AS SUB_ACCOUNT_TYPE_ID,
       RIA.INCREMENTAL_ADJ_ID INCREMENTAL_ADJ_ID,
       RIT.INCREMENTAL_ADJ_TYPE_ID INCREMENTAL_ADJ_TYPE_ID
  from REG_INCREMENTAL_ADJUSTMENT RIA
  join REG_INCREMENTAL_ADJ_VERSION RIAV on RIAV.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_INCREMENTAL_ADJUST_TYPE RIT on RIA.INCREMENTAL_ADJ_TYPE_ID = RIT.INCREMENTAL_ADJ_TYPE_ID
  join REG_FORECAST_VERSION RFV on RIAV.FORECAST_VERSION_ID = RFV.FORECAST_VERSION_ID
  join REG_FCST_INC_ADJ_LEDGER RFL on RIAV.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
                                  and RFL.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_ACCT_MASTER RAM on RAM.REG_ACCT_ID = RFL.REG_ACCT_ID
  join REG_ACCT_TYPE RAT on RAM.REG_ACCT_TYPE_DEFAULT = RAT.REG_ACCT_TYPE_ID
  join REG_SUB_ACCT_TYPE RSAT on RSAT.SUB_ACCT_TYPE_ID = RAM.SUB_ACCT_TYPE_ID
                              AND rsat.reg_acct_type_id = ram.reg_acct_type_default
  join REG_SOURCE RS on RFL.REG_SOURCE_ID = RS.REG_SOURCE_ID
  join REG_COMPANY RC on RFL.REG_COMPANY_ID = RC.REG_COMPANY_ID
  join COMPANY_SETUP CS on RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
  join COMPANY C on C.COMPANY_ID = CS.COMPANY_ID
  join REG_INCREMENTAL_FP_ADJ_TRANS RIFAT on RFL.REG_ACCT_ID =
                                             DECODE(RIFAT.OVERRIDE_TRANSLATE,
                                                    0,
                                                    RIFAT.REG_ACCT_ID,
                                                    RIFAT.OVERRIDE_REG_ACCT_ID)
                                         and RFL.INCREMENTAL_ADJ_ID = RIFAT.INCREMENTAL_ADJ_ID
                                         and RIFAT.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
  join INCREMENTAL_FP_ADJ_ITEM IFAI on RIFAT.ITEM_ID = IFAI.ITEM_ID
                                   and RIFAT.LABEL_ID = IFAI.LABEL_ID
  join INCREMENTAL_FP_ADJ_LABEL IFAL on RIFAT.LABEL_ID = IFAL.LABEL_ID
 where RIAV.STATUS_CODE_ID = 1
	and RIA.INCREMENTAL_ADJ_TYPE_ID in (1,4)
union
select RC.DESCRIPTION REG_COMPANY,
       RFV.DESCRIPTION as FORECAST_LEDGER,
       RAM.DESCRIPTION as REG_ACCOUNT,
       RAT.DESCRIPTION as REG_ACCOUNT_TYPE,
       RSAT.DESCRIPTION as SUB_ACCOUNT_TYPE,
       RS.DESCRIPTION as REG_SOURCE,
       RIT.DESCRIPTION as LAYER_TYPE,
       RIA.DESCRIPTION as LAYER_DESCRIPTION,
       IFAL.DESCRIPTION LABEL,
       IFAI.DESCRIPTION ITEM,
       RFL.GL_MONTH,
       0.00 as FCST_AMOUNT,
       0.00 as ANNUALZIED_AMT,
       0.00 as ADJ_AMOUNT,
       null as ADJ_MONTH,
       RFL.ADJ_AMOUNT as LAYER_AMOUNT,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_LAYER_AMOUNT,
       0 as RECON_ADJ_AMOUNT,
       '' as RECON_ADJ_COMMENT,
       RFL.ADJ_AMOUNT as TOTAL,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_TOTAL,
       RFL.REG_COMPANY_ID,
       RFL.REG_ACCT_ID,
       RFL.FORECAST_VERSION_ID as FORECAST_LEDGER_ID,
       RS.REG_SOURCE_ID,
       RAT.REG_ACCT_TYPE_ID AS REG_ACCOUNT_TYPE_ID,
       RSAT.SUB_ACCT_TYPE_ID AS SUB_ACCOUNT_TYPE_ID,
       RIA.INCREMENTAL_ADJ_ID INCREMENTAL_ADJ_ID,
       RIT.INCREMENTAL_ADJ_TYPE_ID INCREMENTAL_ADJ_TYPE_ID
  from REG_INCREMENTAL_ADJUSTMENT RIA
  join REG_INCREMENTAL_ADJ_VERSION RIAV on RIAV.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_INCREMENTAL_ADJUST_TYPE RIT on RIA.INCREMENTAL_ADJ_TYPE_ID = RIT.INCREMENTAL_ADJ_TYPE_ID
  join REG_FORECAST_VERSION RFV on RIAV.FORECAST_VERSION_ID = RFV.FORECAST_VERSION_ID
  join REG_FCST_INC_ADJ_LEDGER RFL on RIAV.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
                                  and RFL.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_ACCT_MASTER RAM on RAM.REG_ACCT_ID = RFL.REG_ACCT_ID
  join REG_ACCT_TYPE RAT on RAM.REG_ACCT_TYPE_DEFAULT = RAT.REG_ACCT_TYPE_ID
  join REG_SUB_ACCT_TYPE RSAT on RSAT.SUB_ACCT_TYPE_ID = RAM.SUB_ACCT_TYPE_ID
                              AND rsat.reg_acct_type_id = ram.reg_acct_type_default
  join REG_SOURCE RS on RFL.REG_SOURCE_ID = RS.REG_SOURCE_ID
  join REG_COMPANY RC on RFL.REG_COMPANY_ID = RC.REG_COMPANY_ID
  join COMPANY_SETUP CS on RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
  join COMPANY C on C.COMPANY_ID = CS.COMPANY_ID
  join REG_INCREMENTAL_DEPR_ADJ_TRANS RIFAT on RFL.REG_ACCT_ID =
                                             DECODE(RIFAT.OVERRIDE_TRANSLATE,
                                                    0,
                                                    RIFAT.REG_ACCT_ID,
                                                    RIFAT.OVERRIDE_REG_ACCT_ID)
                                         and RFL.INCREMENTAL_ADJ_ID = RIFAT.INCREMENTAL_ADJ_ID
                                         and RIFAT.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
  join INCREMENTAL_FP_ADJ_ITEM IFAI on RIFAT.ITEM_ID = IFAI.ITEM_ID
                                   and RIFAT.LABEL_ID = IFAI.LABEL_ID
  join INCREMENTAL_FP_ADJ_LABEL IFAL on RIFAT.LABEL_ID = IFAL.LABEL_ID
 where RIAV.STATUS_CODE_ID = 1
	and RIA.INCREMENTAL_ADJ_TYPE_ID in (2,3)
union
select RC.DESCRIPTION REG_COMPANY,
       RFV.DESCRIPTION as FORECAST_LEDGER,
       RAM.DESCRIPTION as REG_ACCOUNT,
       RAT.DESCRIPTION as REG_ACCOUNT_TYPE,
       RSAT.DESCRIPTION as SUB_ACCOUNT_TYPE,
       RS.DESCRIPTION as REG_SOURCE,
       RIT.DESCRIPTION as LAYER_TYPE,
       RIA.DESCRIPTION as LAYER_DESCRIPTION,
       IFAL.DESCRIPTION LABEL,
       IFAI.DESCRIPTION ITEM,
       RFL.GL_MONTH,
       0.00 as FCST_AMOUNT,
       0.00 as ANNUALZIED_AMT,
       0.00 as ADJ_AMOUNT,
       null as ADJ_MONTH,
       RFL.ADJ_AMOUNT as LAYER_AMOUNT,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_LAYER_AMOUNT,
       0 as RECON_ADJ_AMOUNT,
       '' as RECON_ADJ_COMMENT,
       RFL.ADJ_AMOUNT as TOTAL,
       RFL.ANNUALIZED_ADJ_AMT as ANNUALIZED_TOTAL,
       RFL.REG_COMPANY_ID,
       RFL.REG_ACCT_ID,
       RFL.FORECAST_VERSION_ID as FORECAST_LEDGER_ID,
       RS.REG_SOURCE_ID,
       RAT.REG_ACCT_TYPE_ID AS REG_ACCOUNT_TYPE_ID,
       RSAT.SUB_ACCT_TYPE_ID AS SUB_ACCOUNT_TYPE_ID,
       RIA.INCREMENTAL_ADJ_ID INCREMENTAL_ADJ_ID,
       RIT.INCREMENTAL_ADJ_TYPE_ID INCREMENTAL_ADJ_TYPE_ID
  from REG_INCREMENTAL_ADJUSTMENT RIA
  join REG_INCREMENTAL_ADJ_VERSION RIAV on RIAV.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_INCREMENTAL_ADJUST_TYPE RIT on RIA.INCREMENTAL_ADJ_TYPE_ID = RIT.INCREMENTAL_ADJ_TYPE_ID
  join REG_FORECAST_VERSION RFV on RIAV.FORECAST_VERSION_ID = RFV.FORECAST_VERSION_ID
  join REG_FCST_INC_ADJ_LEDGER RFL on RIAV.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
                                  and RFL.INCREMENTAL_ADJ_ID = RIA.INCREMENTAL_ADJ_ID
  join REG_ACCT_MASTER RAM on RAM.REG_ACCT_ID = RFL.REG_ACCT_ID
  join REG_ACCT_TYPE RAT on RAM.REG_ACCT_TYPE_DEFAULT = RAT.REG_ACCT_TYPE_ID
  join REG_SUB_ACCT_TYPE RSAT on RSAT.SUB_ACCT_TYPE_ID = RAM.SUB_ACCT_TYPE_ID
                              AND rsat.reg_acct_type_id = ram.reg_acct_type_default
  join REG_SOURCE RS on RFL.REG_SOURCE_ID = RS.REG_SOURCE_ID
  join REG_COMPANY RC on RFL.REG_COMPANY_ID = RC.REG_COMPANY_ID
  join COMPANY_SETUP CS on RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
  join COMPANY C on C.COMPANY_ID = CS.COMPANY_ID
  join REG_INC_HIST_LAYER_TRANS RIFAT on RFL.REG_ACCT_ID =
                                             DECODE(RIFAT.OVERRIDE_TRANSLATE,
                                                    0,
                                                    RIFAT.REG_ACCT_ID,
                                                    RIFAT.OVERRIDE_REG_ACCT_ID)
                                         and RFL.INCREMENTAL_ADJ_ID = RIFAT.INCREMENTAL_ADJ_ID
                                         and RIFAT.FORECAST_VERSION_ID = RFL.FORECAST_VERSION_ID
  join INCREMENTAL_FP_ADJ_ITEM IFAI on RIFAT.ITEM_ID = IFAI.ITEM_ID
                                   and RIFAT.LABEL_ID = IFAI.LABEL_ID
  join INCREMENTAL_FP_ADJ_LABEL IFAL on RIFAT.LABEL_ID = IFAL.LABEL_ID
 where RIAV.STATUS_CODE_ID = 1
	and RIA.INCREMENTAL_ADJ_TYPE_ID = 5
)
;

-- Create view reg_layer_fp_details_view
CREATE OR REPLACE VIEW reg_layer_fp_details_view (
	layer, long_description, funding_project_number, month_year, label, item, 
	original_revision, original_amount, updated_revision, updated_amount, difference,
   wo_description, label_sort, item_sort
) AS (
select x.description layer,
		 x.long_description long_description,
		 w.work_order_number funding_project_number,
		 x.month_year month_year,
	 	 l.description label,
		 i.description item,
		 x.orig_revision original_revision,
		 sum(x.orig_amount) original_amount,
		 x.updated_revision updated_revision,
		 sum(x.updated_amount) updated_amount,
		 sum(nvl(x.orig_amount,0) - nvl(x.updated_amount,0)) difference,
		 w.description wo_description,
		 l.sort_order label_sort,
		 i.sort_order item_sort
  from incremental_fp_adj_label l, incremental_fp_adj_item i, work_order_control w, 
		 (
			select a.description, a.long_description,
					 a.orig_work_order_id, a.orig_revision, a.updated_work_order_id, a.updated_revision,
					 d.label_id, d.item_id, 
					 d.month_year month_year, d.amount orig_amount, 0 updated_amount
			  from reg_incremental_adjustment a, incremental_fp_adj_data d 
			 where a.incremental_adj_type_id in (1, 4)
				and a.orig_work_order_id = d.work_order_id
				and a.orig_revision = d.revision
				and a.orig_budget_version_id = d.budget_version_id
				and a.orig_fcst_depr_version_id = d.fcst_depr_version_id
				and a.orig_version_id = d.version_id
			union
			select a.description, a.long_description,
					 a.orig_work_order_id, a.orig_revision, a.updated_work_order_id, a.updated_revision,
					 d.label_id, d.item_id, 
					 d.month_year month_year, 0 orig_amount, d.amount updated_amount
			  from reg_incremental_adjustment a, incremental_fp_adj_data d 
			 where a.incremental_adj_type_id in (1, 4)
				and a.updated_work_order_id = d.work_order_id
				and a.updated_revision = d.revision
				and a.updated_budget_version_id = d.budget_version_id
				and a.updated_fcst_depr_version_id = d.fcst_depr_version_id
				and a.updated_version_id = d.version_id
		 ) x
 where nvl(x.orig_work_order_id,x.updated_work_order_id) = nvl(x.updated_work_order_id,x.orig_work_order_id)
	and x.label_id = l.label_id
	and x.label_id = i.label_id
	and x.item_id = i.item_id
	and nvl(x.orig_work_order_id,x.updated_work_order_id) = w.work_order_id
 group by x.description, x.long_description, w.work_order_number, x.month_year,
			 l.description, i.description, x.orig_revision, x.updated_revision,
			 w.description, l.sort_order, i.sort_order
);


-- Create view reg_layer_ea_details_view
CREATE OR REPLACE VIEW reg_layer_ea_details_view (
	layer, long_description, selected_field, selected_value, as_of_date, start_month, end_month, 
	month_year, label, item, amount, label_sort, item_sort
) AS (
select layer, long_description, selected_field, selected_value, as_of_date, start_month, end_month, 
		 month_year, label, item, sum(amount) amount, label_sort, item_sort
  from (
	select a.description layer,
			 a.long_description long_description,
			 decode(h.selected_field, 1, 'Funding Project',
											  2, 'Work Order',
											  3, 'Class Code: Value') selected_field,
			 decode(h.selected_field, 1, w.work_order_number||': '||w.description,
											  2, w.work_order_number||': '||w.description,
											  3, c.description||': '||h.class_code_value) selected_value,
			 substr(to_char(h.as_of_date),5,2)||'/'||substr(to_char(h.as_of_date),1,4) as_of_date,
			 substr(to_char(h.start_month),5,2)||'/'||substr(to_char(h.start_month),1,4) start_month,
			 substr(to_char(h.end_month),5,2)||'/'||substr(to_char(h.end_month),1,4) end_month,
			 d.month_year month_year,
			 l.description label,
			 i.description item,
			 d.amount amount,
			 l.sort_order label_sort,
			 i.sort_order item_sort
			, a.hist_layer_id
	  from reg_incremental_adjustment a, work_order_control w, class_code c,
			 incremental_hist_layer_data d, incremental_fp_adj_label l, incremental_fp_adj_item i,
			 (
				select hist_layer_id, funding_project_id, work_order_id, class_code_id, class_code_value,
						 decode(nvl(funding_project_id,0), 0, decode(nvl(work_order_id,0), 0, 3, 2), 1) selected_field,
						 as_of_date, start_month, end_month
				  from incremental_hist_layer
			 ) h
	 where a.incremental_adj_type_id = 5
		and a.hist_layer_id = h.hist_layer_id
		and w.work_order_id (+) = decode(h.selected_field, 1, h.funding_project_id, 2, h.work_order_id)
		and c.class_code_id (+) = decode(h.selected_field, 3, h.class_code_id)
		and a.hist_layer_id = d.hist_layer_id
		and d.label_id = l.label_id
		and d.label_id = i.label_id
		and d.item_id = i.item_id)
 group by layer, long_description, selected_field, selected_value, as_of_date, start_month, end_month, 
			 month_year, label, item, label_sort, item_sort
);

-- Create view reg_layer_depr_details_view
CREATE OR REPLACE VIEW reg_layer_depr_details_view (
	layer, long_description, month_year, label, item, 
	original_fcst_depr_version, original_set_of_books, original_tax_case, original_amount, 
	updated_fcst_depr_version, updated_set_of_books, updated_tax_case, updated_amount, difference,
   label_sort, item_sort
) AS (
select a.description layer,
		 a.long_description long_description,
		 data1.month_year month_year,
		 l.description label,
		 i.description item,
		 v1.description original_fcst_depr_version,
		 a.orig_set_of_books_id original_set_of_books,
		 c1.description original_tax_case,
		 sum(nvl(data1.amount,0)) original_amount,
		 v2.description updated_fcst_depr_version, 
		 a.updated_set_of_books_id updated_set_of_books, 
		 c2.description updated_tax_case, 
		 sum(nvl(data2.amount,0)) updated_amount, 
		 sum(nvl(data1.amount,0) - nvl(data2.amount,0)) difference,
		 l.sort_order label_sort,
		 i.sort_order item_sort
  from reg_incremental_adjustment a, incremental_fp_adj_label l, incremental_fp_adj_item i,
		 fcst_depr_version v1, fcst_depr_version v2, version c1, version c2,
		 incremental_depr_adj_data data1, incremental_depr_adj_data data2
 where a.incremental_adj_type_id in (2,3)
	and a.orig_fcst_depr_version_id = data1.fcst_depr_version_id
	and a.orig_set_of_books_id = data1.set_of_books_id
	and nvl(a.orig_version_id,0) = nvl(data1.version_id,0)
	and a.updated_fcst_depr_version_id = data2.fcst_depr_version_id
	and a.updated_set_of_books_id = data2.set_of_books_id
	and nvl(a.updated_version_id,0) = nvl(data2.version_id,0)
	and data1.label_id = l.label_id
	and data1.label_id = i.label_id
	and data1.item_id = i.item_id
	and data1.fcst_depr_version_id = v1.fcst_depr_version_id
	and data2.fcst_depr_version_id = v2.fcst_depr_version_id
	and a.orig_version_id = c1.version_id (+)
	and a.updated_version_id = c2.version_id (+)
	and data1.label_id = data2.label_id
	and data1.item_id	= data2.item_id
	and data1.company_id = data2.company_id
	and data1.month_year = data2.month_year
 group by a.description, a.long_description, data1.month_year, l.description, i.description,
		    v1.description, a.orig_set_of_books_id, c1.description,
			 v2.description, a.updated_set_of_books_id, c2.description,
			 l.sort_order, i.sort_order
)
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2643, 0, 2015, 2, 0, 0, 043477, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043477_reg_layer_query_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;