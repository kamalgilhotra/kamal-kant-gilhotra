/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008209_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   09/29/2011 Blake Andrews  Point Release
||============================================================================
*/

insert into TAX_ACCRUAL_MONTH_TYPE
   (MONTH_TYPE_ID, DESCRIPTION, ACTIVITY_TYPE_ID, BEG_BAL_IND, LONG_DESCRIPTION)
values
   (4, 'Current Year - No Estimate', 2, 1,
    'Trues up year-to-date current tax activity as well as life-to-date deferred tax balances.  Use another month type to avoid ' ||
     'comingling current period activity with life-to-date true-up.  Activity will be pulled from the GL in this month type.  Estimates will not true up for ' ||
     'the current month with this option.');

delete from TAX_ACCRUAL_MONTH_TYPE_TRUEUP where MONTH_TYPE_ID = 4;

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select 4, TRUEUP_ID, FROM_PT_IND, 0, 0, 0, 0, PULL_ACTUALS
     from TAX_ACCRUAL_MONTH_TYPE_TRUEUP
    where MONTH_TYPE_ID = 2;


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (3, 0, 10, 3, 3, 0, 8209, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008209_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
