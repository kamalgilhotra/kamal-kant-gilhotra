/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029891_revised_ls_je_method_inserts.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   05/01/2013 Travis Nemes      Point Release
||============================================================================
*/

delete from LS_JE_METHOD;

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (0, sysdate, user, 'No Entries', 'No Entries');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (1, sysdate, user, 'GAAP/FERC Operating', 'GAAP/FERC Operating');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (2, sysdate, user, 'GAAP Capital', 'GAAP Capital');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (3, sysdate, user, 'FERC Capital', 'FERC Capital');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (4, sysdate, user, 'IFRS Operating', 'IFRS Operating');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (5, sysdate, user, 'IFRS Capital', 'IFRS Capital');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (6, sysdate, user, 'GAAP/FERC Operating, IFRS Operating', 'GAAP/FERC Operating, IFRS Operating');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (7, sysdate, user, 'GAAP/FERC Operating, IFRS Capital', 'GAAP/FERC Operating, IFRS Capital');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (8, sysdate, user, 'GAAP Capital, IFRS Capital', 'GAAP Capital, IFRS Capital');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (9, sysdate, user, 'FERC Capital, IFRS Capital', 'FERC Capital, IFRS Capital');

insert into LS_JE_METHOD
   (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (10, sysdate, user, 'Custom', 'Custom');


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (371, 0, 10, 4, 0, 0, 29891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029891_revised_ls_je_method_inserts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
