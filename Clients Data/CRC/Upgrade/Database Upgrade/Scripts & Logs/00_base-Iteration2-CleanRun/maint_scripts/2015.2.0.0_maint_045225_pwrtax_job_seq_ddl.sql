/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045225_pwrtax_job_seq_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 2015.2   11/18/2015 Andrew Scott        create new tax job seq
||============================================================================
*/

----no one *should* have this sequence. attempt to drop just in case and don't raise an error.
begin
   execute immediate 'drop sequence TAX_JOB_SEQ';
exception
   when others then
      null;
end;
/

SET SERVEROUTPUT ON

DECLARE
CURRENT_MAX_VALUE NUMBER;
SQLSTRING VARCHAR2(1000);
ERROR_CD NUMBER;
ERROR_MSG VARCHAR(2000);

BEGIN 
 
  SELECT MAX(NVL(JOB_NO,0)) + 1 INTO CURRENT_MAX_VALUE FROM TAX_JOB_PARAMS;

  IF CURRENT_MAX_VALUE IS NULL THEN
    CURRENT_MAX_VALUE := 0;
  END IF;
  
  SQLSTRING := '';
  SQLSTRING := 'CREATE SEQUENCE TAX_JOB_SEQ';
  SQLSTRING := SQLSTRING || ' MINVALUE ' || TO_CHAR(CURRENT_MAX_VALUE) ;
  SQLSTRING := SQLSTRING || ' INCREMENT BY 1 NOCACHE ' ;
  
  DBMS_OUTPUT.PUT_LINE(SQLSTRING);
  
  EXECUTE IMMEDIATE SQLSTRING;
  
  EXCEPTION
    WHEN OTHERS THEN
      ERROR_CD := SQLCODE;
      ERROR_MSG := SUBSTR(SQLERRM, 1, 2000);
      DBMS_OUTPUT.PUT_LINE ('THERE WAS A ERROR DURING EXECUTION. ' || ERROR_CD || ' : ' || ERROR_MSG);
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2977, 0, 2015, 2, 0, 0, 45225, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045225_pwrtax_job_seq_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
