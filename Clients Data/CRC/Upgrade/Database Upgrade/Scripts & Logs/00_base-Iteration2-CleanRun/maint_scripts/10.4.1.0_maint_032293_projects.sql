/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_032293_projects.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   09/09/2013 Chris Mardis
||============================================================================
*/

alter table WORK_ORDER_APPROVAL add USER_TEXT6  varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT7  varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT8  varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT9  varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT10 varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT11 varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT12 varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT13 varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT14 varchar2(2000);
alter table WORK_ORDER_APPROVAL add USER_TEXT15 varchar2(2000);

alter table WORK_ORDER_APPROVAL add USER_DATE6  date;
alter table WORK_ORDER_APPROVAL add USER_DATE7  date;
alter table WORK_ORDER_APPROVAL add USER_DATE8  date;
alter table WORK_ORDER_APPROVAL add USER_DATE9  date;
alter table WORK_ORDER_APPROVAL add USER_DATE10 date;
alter table WORK_ORDER_APPROVAL add USER_DATE11 date;
alter table WORK_ORDER_APPROVAL add USER_DATE12 date;
alter table WORK_ORDER_APPROVAL add USER_DATE13 date;
alter table WORK_ORDER_APPROVAL add USER_DATE14 date;
alter table WORK_ORDER_APPROVAL add USER_DATE15 date;

alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT6  varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT7  varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT8  varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT9  varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT10 varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT11 varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT12 varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT13 varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT14 varchar2(2000);
alter table WORK_ORDER_APPROVAL_ARCH add USER_TEXT15 varchar2(2000);

alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE6  date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE7  date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE8  date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE9  date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE10 date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE11 date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE12 date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE13 date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE14 date;
alter table WORK_ORDER_APPROVAL_ARCH add USER_DATE15 date;

comment on column WORK_ORDER_APPROVAL.USER_TEXT6  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT7  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT8  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT9  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT10 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT11 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT12 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT13 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT14 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_TEXT15 is 'User text for additional descriptive information about a revision.';

comment on column WORK_ORDER_APPROVAL.USER_DATE6  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE7  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE8  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE9  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE10 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE11 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE12 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE13 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE14 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL.USER_DATE15 is 'User dates for additional descriptive information about a revision.';

comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT6  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT7  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT8  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT9  is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT10 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT11 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT12 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT13 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT14 is 'User text for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_TEXT15 is 'User text for additional descriptive information about a revision.';

comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE6  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE7  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE8  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE9  is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE10 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE11 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE12 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE13 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE14 is 'User dates for additional descriptive information about a revision.';
comment on column WORK_ORDER_APPROVAL_ARCH.USER_DATE15 is 'User dates for additional descriptive information about a revision.';

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = DECODE(CONTROL_VALUE,
                               '2',
                               '1,2',
                               '3',
                               '1,2,3',
                               '4',
                               '1,2,3,4',
                               '5',
                               '1,2,3,4,5',
                               CONTROL_VALUE), DESCRIPTION = null,
       LONG_DESCRIPTION = 'Comma-delimited list of which text fields to display in the Forecasting Screen revision comments. Enter "0" to not display any text fields at all.'
 where LOWER(CONTROL_NAME) = LOWER('Revision Comments - Text');

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = DECODE(CONTROL_VALUE,
                               '2',
                               '1,2',
                               '3',
                               '1,2,3',
                               '4',
                               '1,2,3,4',
                               '5',
                               '1,2,3,4,5',
                               CONTROL_VALUE), DESCRIPTION = null,
       LONG_DESCRIPTION = 'Comma-delimited list of which date fields to display in the Forecasting Screen revision comments. Enter "0" to not display any date fields at all.'
 where LOWER(CONTROL_NAME) = LOWER('Revision Comments - Date');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (604, 0, 10, 4, 1, 0, 32293, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032293_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;