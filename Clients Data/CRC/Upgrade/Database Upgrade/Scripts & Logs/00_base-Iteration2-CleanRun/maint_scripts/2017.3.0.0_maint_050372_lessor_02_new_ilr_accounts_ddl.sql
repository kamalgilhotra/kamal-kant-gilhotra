/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050372_lessor_02_new_ilr_accounts_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 04/12/2018 Jared Watkins  Add new ILR accounts to ILR Groups and ILR Accounts
||============================================================================
*/

alter table lsr_ilr_group 
add (incurred_costs_account_id number(22,0) null,
     def_costs_account_id number(22,0) null,
     def_selling_profit_account_id number(22,0) null
);

comment on column lsr_ilr_group.incurred_costs_account_id is 'The incurred lease costs account';
comment on column lsr_ilr_group.def_costs_account_id is 'The deferred lease costs account';
comment on column lsr_ilr_group.def_selling_profit_account_id is 'The deferred selling profit account';


alter table lsr_ilr_account 
add (incurred_costs_account_id number(22,0) null,
     def_costs_account_id number(22,0) null,
     def_selling_profit_account_id number(22,0) null
);

comment on column lsr_ilr_account.incurred_costs_account_id is 'The incurred lease costs account';
comment on column lsr_ilr_account.def_costs_account_id is 'The deferred lease costs account';
comment on column lsr_ilr_account.def_selling_profit_account_id is 'The deferred selling profit account';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4310, 0, 2017, 3, 0, 0, 50372, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050372_lessor_02_new_ilr_accounts_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;