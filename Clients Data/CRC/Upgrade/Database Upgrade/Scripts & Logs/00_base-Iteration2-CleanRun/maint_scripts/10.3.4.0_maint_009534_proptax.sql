/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009534_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/06/2012 Julia Breuer   Point Release
||============================================================================
*/

alter table pwrplant.company_setup modify description varchar2(100);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (106, 0, 10, 3, 4, 0, 9534, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009534_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
