/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009370_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.2   02/09/2012 Sunjin Cone    Point Release
||============================================================================
*/

update WORK_ORDER_ACCOUNT A
   set A.TAX_EXPENSE_TEST_ID =
        (select B.TAX_EXPENSE_TEST_ID
           from WORK_ORDER_TYPE B, WORK_ORDER_CONTROL C
          where A.WORK_ORDER_ID = C.WORK_ORDER_ID
            and B.WORK_ORDER_TYPE_ID = C.WORK_ORDER_TYPE_ID)
 where A.TAX_EXPENSE_TEST_ID is null;

alter table WO_TAX_EXPENSE_TEST modify REPAIR_SCHEMA_ID not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (87, 0, 10, 3, 3, 2, 9370, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.2_maint_009370_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
