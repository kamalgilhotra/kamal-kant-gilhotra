/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034610_version-updates_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   12/11/2013 Lee Quinn      10.4.1.2 Patch
||============================================================================
*/

update PP_VERSION
   set PROP_TAX_VERSION = 'Version 10.4.1.2'
where PP_VERSION_ID = 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (805, 0, 10, 4, 1, 2, 34610, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_034610_version-updates_proptax.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'),
    SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
