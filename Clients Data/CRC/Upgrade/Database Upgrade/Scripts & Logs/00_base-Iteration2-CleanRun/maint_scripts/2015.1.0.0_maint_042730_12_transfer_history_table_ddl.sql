/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		maint_042730_12_transfer_history_table_ddl.sql
|| File Name:   LESSEE
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

--execute p_drop_table('ls_asset_transfer_history');
create table ls_asset_transfer_history
(from_ls_asset_id number(22,0),
 to_ls_asset_id number(22,0),
 month date,
 from_pend_trans_id number(22,0),
 to_pend_trans_id number(22,0),
 time_stamp date,
 user_id varchar2(20));

comment on table ls_asset_transfer_history is 'Gives from and to ls_asset_id''s associated with a transfer for reporting purposes';
comment on column ls_asset_transfer_history.from_ls_asset_id is 'The leased asset ID associated with the original leased asset';
comment on column ls_asset_transfer_history.to_ls_asset_id is 'The leased asset ID associated with the new transferred asset';
comment on column ls_asset_transfer_history.month is 'The GL posting month of the tranfer';
comment on column ls_asset_transfer_history.from_pend_trans_id is 'The Pending Transaction ID associated with the from side of the transfer';
comment on column ls_asset_transfer_history.to_pend_trans_id is 'The Pending Transaction ID associated with the to side of the transfer';
COMMENT ON COLUMN ls_asset_transfer_history.user_id IS 'Standard system-assigned user id user for audit purposes.';
COMMENT ON COLUMN ls_asset_transfer_history.time_stamp IS 'Standard system-assigned timestamp user for audit purposes.';


CREATE INDEX ix_ls_asset_xfer_fromid
  ON ls_asset_transfer_history (
    from_ls_asset_id
  )
  tablespace PWRPLANT_IDX
/

ALTER TABLE ls_asset_transfer_history
  ADD CONSTRAINT pk_ls_asset_transfer_history PRIMARY KEY (
    from_ls_asset_id,
    to_ls_asset_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2407, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_12_transfer_history_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;