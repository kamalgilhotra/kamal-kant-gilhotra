/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045048_lease_new_forecast_wksp_ddl.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/13/2015 Sarah Byers    New Fcst Version workspace and tables
||============================================================================
*/

-- LS_FORECAST_TYPE
create table ls_forecast_type (
forecast_type_id number(22,0) not null,
description varchar2(35) not null,
user_id varchar2(18),
time_stamp date);

alter table ls_forecast_type add (
constraint pk_ls_forecast_type primary key (forecast_type_id) using index tablespace pwrplant_idx);

comment on table ls_forecast_type is 'The LS Forecast Type table contains the types of forecast allowed in the lease forecasting functionality.';
comment on column ls_forecast_type.forecast_type_id is 'System assigned identifier of the forecast type.';
comment on column ls_forecast_type.description is 'Description of the forecast type.';
comment on column ls_forecast_type.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_type.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- LS_FORECAST_RATE_OPTIONS
create table ls_forecast_rate_options (
forecast_rate_option_id number(22,0) not null,
description varchar2(50) not null,
user_id varchar2(18),
time_stamp date);

alter table ls_forecast_rate_options add (
constraint pk_ls_forecast_rate_options primary key (forecast_rate_option_id) using index tablespace pwrplant_idx);

comment on table ls_forecast_rate_options is 'The LS Forecast Rate Options table contains the rate options available for ILRs being converted in the lease forecasting functionality.';
comment on column ls_forecast_rate_options.forecast_rate_option_id is 'System assigned identifier of the forecast rate option.';
comment on column ls_forecast_rate_options.description is 'Description of the forecast rate option.';
comment on column ls_forecast_rate_options.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_rate_options.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- LS_FORECAST_VERSION
create table ls_forecast_version (
revision number(22,0) not null,
set_of_books_id number(22,0) not null,
description varchar2(35) not null,
long_description varchar2(254) not null,
lookback_revision number(22,0) null,
conversion_date date not null,
lookback_date date null,
forecast_type_id number(22,0) not null,
forecast_rate_option_id number(22,0) null,
effd_rounding_id number(22,0) null,
term_rounding_id number(22,0) null,
user_id varchar2(18),
time_stamp date);

alter table ls_forecast_version add (
constraint pk_ls_forecast_version primary key (revision) using index tablespace pwrplant_idx);

alter table ls_forecast_version
add constraint ls_forecast_version_fk1
foreign key (set_of_books_id)
references set_of_books (set_of_books_id);

alter table ls_forecast_version
add constraint ls_forecast_version_fk2
foreign key (lookback_revision)
references ls_forecast_version (revision);

alter table ls_forecast_version
add constraint ls_forecast_version_fk3
foreign key (forecast_type_id)
references ls_forecast_type (forecast_type_id);

alter table ls_forecast_version
add constraint ls_forecast_version_fk4
foreign key (forecast_rate_option_id)
references ls_forecast_rate_options (forecast_rate_option_id);

comment on table ls_forecast_version is 'The LS Forecast Version table holds the options for running a lease forecast.';
comment on column ls_forecast_version.revision is 'System assigned identifier of the lease forecast version.';
comment on column ls_forecast_version.set_of_books_id is 'System assigned identifier of the set of books.';
comment on column ls_forecast_version.description is 'Description of the Forecast Version.';
comment on column ls_forecast_version.long_description is 'Long Description of the Forecast Version.';
comment on column ls_forecast_version.lookback_revision is 'System assigned identifier of the lookback forecast version.  The Lookback Revision will be shared on all ILRs that are created for lookback forecast purposes (revision on the ILR).  This value is negative to distinguish it from active revisions in the system.  A lookback revision is only created during a forecast if a lookback date is filled in.';
comment on column ls_forecast_version.conversion_date is 'Corresponds to the date from which leases will be transitioned to new capital treatments for the forecast.';
comment on column ls_forecast_version.lookback_date is 'Corresponds to the lookback period for comparable financials that will be created during the forecast.  If entered, this will cause the forecast logic to build two forecasts: one for the forecast related to the �conversion date� and one for the lookback related to the �lookback date�.';
comment on column ls_forecast_version.forecast_type_id is 'System assigned identifier of the forecast type.';
comment on column ls_forecast_version.forecast_rate_option_id is 'System assigned identifier of the forecast rate option';
comment on column ls_forecast_version.effd_rounding_id is 'Determines whether or not in-service dates are rounded up, down or not used for forecast purposes.  Valid entries are: 0: Effective Dates Not Used, No Rounding; 1: Round Up to Nearest In-Service Date; -1: Round Down to Nearest In-Service Date.';
comment on column ls_forecast_version.term_rounding_id is 'Determines whether or not terms are rounded up, down or not used for forecast purposes.  Valid entries are: 0: Effective Dates Not Used, No Rounding; 1: Round Up to Nearest Term; -1: Round Down to Nearest Term.';
comment on column ls_forecast_version.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_version.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- LS_FORECAST_RATES
create table ls_forecast_rates (
revision number(22,0) not null,
company_id number(22,0) not null,
in_service_date date not null,
number_of_terms number(22,0) not null,
rate number(22,8) not null,
user_id varchar2(18),
time_stamp date);

alter table ls_forecast_rates add (
constraint pk_ls_forecast_rates primary key (revision, company_id, in_service_date, number_of_terms) using index tablespace pwrplant_idx);

alter table ls_forecast_rates
add constraint ls_forecast_rates_fk1
foreign key (revision)
references ls_forecast_version (revision);

alter table ls_forecast_rates
add constraint ls_forecast_rates_fk2
foreign key (company_id)
references company_setup (company_id);

comment on table ls_forecast_rates is 'The LS Forecast Rates table contains effective dated rates by lease forecast version.';
comment on column ls_forecast_rates.revision is 'System assigned identifier of the lease forecast version.';
comment on column ls_forecast_rates.company_id is 'System assigned identifier of the PowerPlan company.';
comment on column ls_forecast_rates.in_service_date is 'Date corresponding to the in-service date of leases.';
comment on column ls_forecast_rates.number_of_terms is 'The number of months on a lease for which payments are made.  This field is used to match number of terms on leases with associated rates for leases of that length.';
comment on column ls_forecast_rates.rate is 'Effective-dated rate corresponding to a unique in-service date and number of terms combination.';
comment on column ls_forecast_rates.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_rates.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- LS_FORECAST_ILR_MASTER
create table ls_forecast_ilr_master (
revision number(22,0) not null,
ilr_id number(22,0) not null,
from_cap_type_id number(22,0) not null,
to_cap_type_id number(22,0) null,
user_id varchar2(18),
time_stamp date);

alter table ls_forecast_ilr_master add (
constraint pk_ls_forecast_ilr_master primary key (revision, ilr_id) using index tablespace pwrplant_idx);

alter table ls_forecast_ilr_master
add constraint ls_forecast_ilr_master_fk1
foreign key (revision)
references ls_forecast_version (revision);

alter table ls_forecast_ilr_master
add constraint ls_forecast_ilr_master_fk2
foreign key (ilr_id)
references ls_ilr (ilr_id);

alter table ls_forecast_ilr_master
add constraint ls_forecast_ilr_master_fk3
foreign key (from_cap_type_id)
references ls_lease_cap_type (ls_lease_cap_type_id);

comment on table ls_forecast_ilr_master is 'The LS Forecast ILR Master table contains the master data for ILRs being converted on the forecast version.';
comment on column ls_forecast_ilr_master.revision is 'System assigned identifier of the lease forecast version.';
comment on column ls_forecast_ilr_master.ilr_id is 'System assigned identifier of the ILR.';
comment on column ls_forecast_ilr_master.from_cap_type_id is 'System assigned identifier of the Capital/Operating treatment of the lease.';
comment on column ls_forecast_ilr_master.to_cap_type_id is 'System assigned identifier of the Capital/Operating treatment... ';
comment on column ls_forecast_ilr_master.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_ilr_master.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- LS_FORECAST_DEFAULT_CAP_TYPES
create table ls_forecast_default_cap_types (
revision number(22,0) not null,
from_cap_type_id number(22,0) not null,
to_cap_type_id number(22,0) not null,
user_id varchar2(18),
time_stamp date);

alter table ls_forecast_default_cap_types add (
constraint pk_ls_fcst_default_cap_types primary key (revision, from_cap_type_id, to_cap_type_id) using index tablespace pwrplant_idx);

alter table ls_forecast_default_cap_types
add constraint ls_fcst_default_cap_types_fk1
foreign key (revision)
references ls_forecast_version (revision);

alter table ls_forecast_default_cap_types
add constraint ls_fcst_default_cap_types_fk2
foreign key (from_cap_type_id)
references ls_lease_cap_type (ls_lease_cap_type_id);

alter table ls_forecast_default_cap_types
add constraint ls_fcst_default_cap_types_fk3
foreign key (to_cap_type_id)
references ls_lease_cap_type (ls_lease_cap_type_id);

comment on table ls_forecast_ilr_master is 'The LS Forecast Default Cap Types table determins the default Cap Type to which each ILR will transition for a forecast version.';
comment on column ls_forecast_ilr_master.revision is 'System assigned identifier of the lease forecast version.';
comment on column ls_forecast_ilr_master.from_cap_type_id is 'System assigned identifier of the Capital/Operating treatment of the lease; this field holds the existing value so that it can be converted to another value in the forecast.';
comment on column ls_forecast_ilr_master.to_cap_type_id is 'System assigned identifier of the Capital/Operating treatment of the lease; this field contains the default conversion value when transitioning records from existing capital types to new capital types that correspond to the new lease accounting standards.';
comment on column ls_forecast_ilr_master.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_forecast_ilr_master.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2917, 0, 2015, 2, 0, 0, 45048, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045048_lease_new_forecast_wksp_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
