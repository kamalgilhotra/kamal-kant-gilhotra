/*
||============================================================================================
|| Application: PowerPlant
|| File Name:   maint_047752_02_sys_alter_any_query_field_comment_ddl.sql
||============================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- -----------------------------------------------------
|| 2017.1.0.0  05/02/2017 David H        PP-47752 Enhance base queries to hide a field from the Filter Criteria and Results
||============================================================================================
*/
    
COMMENT ON COLUMN pp_any_query_criteria_fields.hide_from_filters IS 'This flag controls whether the specified field is hidden from the query criteria filters. This is accomplished by treating the field as an amount, which results in the field being listed in the Amounts datawindow. By default, columns are included in the criteria filters.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3474, 0, 2017, 1, 0, 0, 47752, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047752_02_sys_alter_any_query_field_comment_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;