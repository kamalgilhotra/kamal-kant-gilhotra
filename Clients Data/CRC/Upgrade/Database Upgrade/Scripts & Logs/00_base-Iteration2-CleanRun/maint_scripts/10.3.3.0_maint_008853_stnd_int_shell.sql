/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008853_stnd_int_shell.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/31/2011 Joseph King    Point Release
||============================================================================
*/

create table PP_VERSION_EXES
(
 pp_version         varchar2(35) not null,
 executable_file    varchar2(254) not null,
 executable_version varchar2(35),
 user_id            varchar2(18),
 time_stamp         date
);

alter table PP_VERSION_EXES
   add constraint PP_VERSION_EXES_PK
       primary key (PP_VERSION, EXECUTABLE_FILE)
       using index tablespace PWRPLANT_IDX;

-- Not really needed for 10.3.2.1, but makes this reverse compatible to that version if needed
-- and also serves as an example of how to use the table
insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
select '10.3.2.1', 'automatedreview.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'batreporter.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'budget_allocations.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_allocations.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_balances.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_batch_derivation.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_batch_derivation_bdg.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_budget_entry_calc_all.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_build_combos.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_build_deriver.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_delete_budget_allocations.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_financial_reports_build.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_flatten_structures.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_man_jrnl_reversals.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_posting.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_posting_bdg.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_sap_trueup.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_sum.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_sum_ab.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cr_to_commitments.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'cwip_charge.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'populate_matlrec.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'ppmetricbatch.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'pptocr.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'ppverify.exe', '10.3.2.1' from dual union all
select '10.3.2.1', 'pp_archive.exe', '10.3.2.1' from dual;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (26, 0, 10, 3, 3, 0, 8853, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008853_stnd_int_shell.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
