/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_051017_03_lessor_add_package_version_script_prereq_ddl.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2017.3.0.0 04/25/2018  Andrew Hill      Add specific version of package on which maint_050891_lessor_02_update_rates_view_ddl.sql depends
||============================================================================
*/

create or replace PACKAGE pkg_lessor_schedule AS
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LESSOR_SCHEDULE
   || Description:
   ||============================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   || Version     Date       Revised By     Reason for Change
   || ----------  ---------- -------------- ----------------------------------------
   || 2017.1.0.0  09/12/2017 A. Hill        Original Version
   || 2017.1.0.0  10/05/2017 A. Hill        Add Sales Type Calculation Logic
   ||==================================================================================================================================================
   */


  /*****************************************************************************
  * PROCEDURE: p_process_ilr
  * PURPOSE: Processes the Lessor ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/

  PROCEDURE p_process_ilr(a_ilr_id NUMBER,
                          a_revision NUMBER);

  /*****************************************************************************
  * PROCEDURE: p_process_ilrs
  * PURPOSE: Processes the Lessor ILR Schedule for the given ILR/Revisions
  * PARAMETERS:
  *   a_ilrs: The ILR ID/Revisions for which to process the schedule
  ******************************************************************************/
  PROCEDURE p_process_ilrs(a_ilrs t_lsr_ilr_id_revision_tab);
                          
  /*****************************************************************************
  * Function: f_build_op_schedule
  * PURPOSE: Builds the operating schedule for the given payment terms
  * PARAMETERS:
  *   a_payment_terms: The payment terms associated with this ILR. The schedule will
  *                     build for payment_month_frequency (i.e. the number of months between payments),
  *                               payment_term_start_date (i.e. the starting date of the payment term),
  *                               number_of_terms (i.e. the number of payments that will be made),
  *                               payment_amount (i.e. the amount of payment to apply)
  *                               is_prepay (0 = arrears / 1 = prepay)
  *   a_initial_direct_costs: The Initial Direct Costs associated with this ILR
  *   NOTE: Multiple payment terms can be defined. For example, for a 36 month, prepay monthly lease,
  *           with $500 payments in year 1, $550 payments in year 2, and $600 payments in year three,
  *           provide three payment terms (1, <year_1_start>, 12, 500, 1),
  *                                       (1, <year_2_start>, 12, 550, 1),
  *                                       (1, <year_3_start>, 12, 600, 1)
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/
  FUNCTION f_build_op_schedule( a_payment_terms lsr_ilr_op_sch_pay_term_tab, 
                                a_initial_direct_costs lsr_init_direct_cost_info_tab) 
  RETURN lsr_ilr_op_sch_result_tab PIPELINED DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_op_schedule
  * PURPOSE: Builds and returns the operating schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule. The schedule will
  *             build for payment payment terms given in table lsr_ilr_payment_term
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/

  FUNCTION f_get_op_schedule( a_ilr_id NUMBER, 
                              a_revision number) 
  RETURN lsr_ilr_op_sch_result_tab PIPELINED;
  
  
  /*****************************************************************************
  * Function: f_ilr_has_operating
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with operating cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_operating( a_ilr_id NUMBER, 
                                a_revision number) 
  return number;
  
  /*****************************************************************************
  * Function: f_ilr_has_sales_type
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with sales type cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_sales_type(a_ilr_id number, 
                                a_revision number) 
  return number;
  
  /*****************************************************************************
  * Function: f_ilr_has_direct_finance
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with direct finance cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_direct_finance(a_ilr_id number, 
                                    a_revision number) 
  RETURN NUMBER;
  
  /*****************************************************************************
  * Function: f_get_payment_terms
  * PURPOSE: Looks up and returns the payment terms for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with payment terms
  * NOTE: This now looks to the variable payments package for logic to get payment terms
  ******************************************************************************/

  FUNCTION f_get_payment_terms( a_ilr_id NUMBER, 
                                a_revision NUMBER) 
  RETURN lsr_ilr_op_sch_pay_term_tab;

  /*****************************************************************************
  * Function: f_get_initial_direct_costs
  * PURPOSE: Looks up and returns the initial direct costs for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with initial direct costs (pipelined)
  ******************************************************************************/

  FUNCTION f_get_initial_direct_costs(a_ilr_id NUMBER, 
                                      a_revision NUMBER) 
  RETURN lsr_init_direct_cost_info_tab;

  /*****************************************************************************
  * Function: f_get_sales_type_info
  * PURPOSE: Looks up and returns information necessary to complete the building of sales-type schedules
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve infomration
  *   a_revision: The revision of the ILR for which to retrieve information
  *
  * RETURNS: Table with sales-type information (pipelined)
  ******************************************************************************/

  FUNCTION f_get_sales_type_info( a_ilr_id NUMBER, 
                                  a_revision number) 
  return lsr_ilr_sales_sch_info;

  /*****************************************************************************
  * Function: f_get_payment_info_from_terms
  * PURPOSE: Transforms payment terms into a month-by-month listing of payment information
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to generate payment info
  *
  * RETURNS: Table with payment information
  ******************************************************************************/

  FUNCTION f_get_payment_info_from_terms(a_payment_terms lsr_ilr_op_sch_pay_term_tab) 
  RETURN lsr_ilr_op_sch_pay_info_tab DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_payments_from_info
  * PURPOSE: Transforms month-by-month payment information listing into month-by-month list of calculated payment amounts
  * PARAMETERS:
  *   a_payment_info: The payment information for which to calculate payment amounts
  *
  * RETURNS: Table with calculated payment amounts (pipelined)
  ******************************************************************************/

  FUNCTION f_get_payments_from_info(a_payment_info lsr_ilr_op_sch_pay_info_tab) 
  RETURN lsr_schedule_payment_def_tab DETERMINISTIC;

  /*****************************************************************************
  * Function: f_calculate_buckets
  * PURPOSE: Calculates executory and contingent bucket accruals and payments
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to process buckets
  *
  * RETURNS: Table with bucket information (pipelined)
  ******************************************************************************/

  FUNCTION f_calculate_buckets(payment_info lsr_ilr_op_sch_pay_info_tab) 
  RETURN lsr_bucket_result_tab PIPELINED DETERMINISTIC;


  /*****************************************************************************
  * Function: f_calc_rates_implicit_sales
  * PURPOSE: Calculates the rates implicit for a sales-type lease with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *
  * RETURNS: Rates implicit in lease
  * NOTE: For a sales-type lease, the rates for net investment do not differ from other rates
  ******************************************************************************/
  FUNCTION f_calc_rates_implicit_sales(a_payments lsr_schedule_payment_def_tab,
                                       a_initial_direct_costs lsr_init_direct_cost_info_tab, 
                                       a_sales_type_info lsr_ilr_sales_sch_info) 
  RETURN t_lsr_rates_implicit_in_lease DETERMINISTIC;
                                       
  /*****************************************************************************
  * Function: f_calc_rates_implicit_df
  * PURPOSE: Calculates the rates implicit for a direct-finanace-type lease with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *
  * RETURNS: Rates implicit in lease
  ******************************************************************************/
  function f_calc_rates_implicit_df(a_payments lsr_schedule_payment_def_tab,
                                    a_initial_direct_costs lsr_init_direct_cost_info_tab, 
                                    a_sales_type_info lsr_ilr_sales_sch_info) 
  RETURN t_lsr_rates_implicit_in_lease DETERMINISTIC;
                                    
  /*****************************************************************************
  * Function: f_get_override_rates_sales
  * PURPOSE: Looks up and return manual override rates for sales type ILRs
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to look for manually overriden rates
  *   a_ilr_id: The revision of a_ilr_id for which to look for manually overriden rates
  *
  * RETURNS: Manually overriden rates
  ******************************************************************************/  
  FUNCTION f_get_override_rates_sales(a_ilr_id NUMBER,
                                      a_revision NUMBER) 
  RETURN t_lsr_rates_implicit_in_lease;

  /*****************************************************************************
  * Function: f_get_override_rates_df
  * PURPOSE: Looks up and return manual override rates for direct-finance type ILRs
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to look for manually overriden rates
  *   a_ilr_id: The revision of a_ilr_id for which to look for manually overriden rates
  *
  * RETURNS: Manually overriden rates
  ******************************************************************************/
  FUNCTION f_get_override_rates_df( a_ilr_id NUMBER,
                                    a_revision NUMBER) 
  RETURN t_lsr_rates_implicit_in_lease;

   /*****************************************************************************
  * Function: f_get_prelim_info_sales
  * PURPOSE: Looks up and return all preliminary info used in building a sales schedule
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to gather preliminary info
  *   a_revision: The revision of a_ilr_id for which to gather preliminary info
  *
  * RETURNS: All preliminary info used in schedule building process
  ******************************************************************************/
  FUNCTION f_get_prelim_info_sales( a_ilr_id NUMBER,
                                    a_revision NUMBER) 
  RETURN t_lsr_ilr_sales_df_prelims;

  /*****************************************************************************
  * Function: f_get_prelim_info_df
  * PURPOSE: Looks up and return all preliminary info used in building DF schedule
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to gather preliminary info
  *   a_revision: The revision of a_ilr_id for which to gather preliminary info
  *
  * RETURNS All preliminary info used in schedule building process
  ******************************************************************************/
  FUNCTION f_get_prelim_info_df(a_ilr_id NUMBER,
                                a_revision NUMBER) 
  RETURN t_lsr_ilr_sales_df_prelims;
                                    
  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_rates_implicit: The rates to use in building the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_build_sales_schedule(a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                  a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                  a_sales_type_info lsr_ilr_sales_sch_info,
                                  a_rates_implicit t_lsr_rates_implicit_in_lease) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED DETERMINISTIC;
                                  
  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  function f_build_sales_schedule(a_payment_info lsr_ilr_op_sch_pay_info_tab, 
                                  a_initial_direct_costs lsr_init_direct_cost_info_tab, 
                                  a_sales_type_info lsr_ilr_sales_sch_info,
                                  a_rates_implicit t_lsr_rates_implicit_in_lease,
                                  is_finance_type NUMBER) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED DETERMINISTIC;

  /*****************************************************************************
  * Function: f_build_df_schedule
  * PURPOSE: Builds the direct-finance-type schedule for the given payment terms and 
              sales-type (also used for direct finance) information
  * PARAMETERS:
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_rates_implicit: The rates to use in building the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  function f_build_df_schedule( a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                a_sales_type_info lsr_ilr_sales_sch_info,
                                a_rates_implicit t_lsr_rates_implicit_in_lease) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED DETERMINISTIC;

  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_prelims: Sales Type Preliminary Info to use in schedule calculation
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  
  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER, 
                                a_revision NUMBER, 
                                a_prelims t_lsr_ilr_sales_df_prelims) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED;
  
  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER, 
                                a_revision NUMBER) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED;
  
  /*****************************************************************************
  * Function: f_get_df_schedule
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_prelims: Sales Type Preliminary Info to use in schedule calculation
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_get_df_schedule( a_ilr_id NUMBER, 
                              a_revision NUMBER, 
                              a_prelims t_lsr_ilr_sales_df_prelims) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED;
  
  /*****************************************************************************
  * Function: f_get_df_schedule
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_get_df_schedule( a_ilr_id NUMBER, 
                              a_revision NUMBER) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED;
  
    /*****************************************************************************
  * Function: f_get_op_sch_quash_exceptions
  * PURPOSE: Builds and returns the operating-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  *          CONSIDER THE IMPLICATIONS OF THIS BEFORE USING THIS FUNCTION
  ******************************************************************************/
  FUNCTION f_get_op_sch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_op_sch_result_tab PIPELINED;
  
  /*****************************************************************************
  * Function: f_getsalessch_quash_exceptions
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  *          CONSIDER THE IMPLICATIONS OF THIS BEFORE USING THIS FUNCTION
  ******************************************************************************/
  FUNCTION f_getsalessch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED;
  
    /*****************************************************************************
  * Function: f_get_df_sch_quash_exceptions
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  *         CONSIDER THE IMPLICATIONS OF THIS BEFORE USING THIS FUNCTION
  ******************************************************************************/
  FUNCTION f_get_df_sch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED;
  
  /*****************************************************************************
  * Function: f_annual_to_implicit_rate
  * PURPOSE: Converts from an "annualized" to the rate implicit
  * PARAMETERS:
  *   a_rate: The rate to convert
  * RETURNS: Converted rate
  ******************************************************************************/
  FUNCTION f_annual_to_implicit_rate(a_rate float) RETURN FLOAT DETERMINISTIC;
  
  /*****************************************************************************
  * Function: f_implicit_to_annual_rate
  * PURPOSE: Converts from the rate implicit to the "annualized" rate
  * PARAMETERS:
  *   a_rate: The rate to convert
  * RETURNS: Converted rate
  ******************************************************************************/
  FUNCTION f_implicit_to_annual_rate(a_rate FLOAT) RETURN FLOAT DETERMINISTIC;

END pkg_lessor_schedule;
/

CREATE OR REPLACE PACKAGE BODY pkg_lessor_schedule AS
  
  TYPE t_kickout IS RECORD (ilr_id lsr_ilr_schedule_kickouts.ilr_id%TYPE,
                            revision lsr_ilr_schedule_kickouts.revision%TYPE,
                            message lsr_ilr_schedule_kickouts.message%TYPE);
  TYPE t_kickout_tab IS TABLE OF t_kickout;
  
  PROCEDURE p_del_error_ilrs_from_sch_tbls(a_ilrs t_lsr_ilr_id_revision_tab) IS
  BEGIN
    DELETE FROM lsr_ilr_schedule_direct_fin
    WHERE (ilr_id, revision) IN ( SELECT to_number(ilr_id), to_number(revision)
                                  FROM err$_lsr_ilr_amounts
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM err$_lsr_ilr_rates
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_schedule
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_sch_sales_direct
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_sch_direct_fin
                                  UNION ALL
                                  SELECT ilr_id, revision
                                  FROM lsr_ilr_schedule_kickouts)
    AND t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs;
                                  
    DELETE FROM lsr_ilr_schedule_sales_direct
        WHERE (ilr_id, revision) IN ( SELECT to_number(ilr_id), to_number(revision)
                                  FROM err$_lsr_ilr_amounts
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM err$_lsr_ilr_rates
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_schedule
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_sch_sales_direct
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_sch_direct_fin
                                  UNION ALL
                                  SELECT ilr_id, revision
                                  FROM lsr_ilr_schedule_kickouts)
    AND t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs;
                                  
    DELETE FROM lsr_ilr_schedule
        WHERE (ilr_id, revision) IN ( SELECT to_number(ilr_id), to_number(revision)
                                  FROM err$_lsr_ilr_amounts
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM err$_lsr_ilr_rates
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_schedule
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_sch_sales_direct
                                  UNION ALL
                                  SELECT to_number(ilr_id), to_number(revision)
                                  FROM ERR$_lsr_ilr_sch_direct_fin
                                  UNION ALL
                                  SELECT ilr_id, revision
                                  FROM lsr_ilr_schedule_kickouts)
    AND t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs;
  END p_del_error_ilrs_from_sch_tbls;

  
  /*****************************************************************************
  * Procedure: p_log_kickouts
  * PURPOSE: Logs kickouts to the lsr_ilr_schedule_kickouts table
  * PARAMETERS:
  *   a_kickouts: The kickouts to log to the table
  * NOTES: Uses autonomous transaction
  ******************************************************************************/
  PROCEDURE p_log_kickouts(a_kickouts t_kickout_tab) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
    l_ids t_number_22_2_tab;
    l_count number;
  BEGIN    
    l_count := a_kickouts.COUNT;

    SELECT lsr_ilr_schedule_kickouts_seq.NEXTVAL
    BULK COLLECT INTO l_ids
    FROM dual
    WHERE l_count <> 0
    CONNECT BY LEVEL <= l_count;

    FORALL I IN 1..a_kickouts.COUNT
    INSERT INTO lsr_ilr_schedule_kickouts(kickout_id, 
                                          ilr_id, 
                                          revision, 
                                          message)
    VALUES( l_ids(I), 
            a_kickouts(I).ilr_id, 
            a_kickouts(I).revision, 
            a_kickouts(I).message);

    COMMIT;
  END p_log_kickouts;
  
  /*****************************************************************************
  * Procedure: p_clear_schedule_errors
  * PURPOSE: Clears the error tables associated with the schedule building process,
  *           and deletes any schedule information that may be been inserted related
  *           to erroring ILR ID/Revisions
  * PARAMETERS: a_ilrs: The ILR ID/Revisions for which to clear errors
  *             a_log_to_kickouts: Determines whether or not to log deleted errors
  *                                 to lsr_ilr_schedule_kickouts table
  * NOTES: Uses autonomous transaction
  ******************************************************************************/
  PROCEDURE p_clear_schedule_errors(a_ilrs t_lsr_ilr_id_revision_tab, 
                                          a_log_to_kickouts boolean := false)
  IS
    l_kickouts t_kickout_tab;
  BEGIN

    DELETE FROM ERR$_lsr_ilr_amounts
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    RETURNING ilr_id, revision, ora_err_mesg$
    BULK COLLECT INTO l_kickouts;

    IF a_log_to_kickouts THEN
      p_log_kickouts(l_kickouts);
    END IF;

    DELETE FROM err$_lsr_ilr_rates
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    RETURNING ilr_id, revision, ora_err_mesg$
    BULK COLLECT INTO l_kickouts;

    IF a_log_to_kickouts THEN
      p_log_kickouts(l_kickouts);
    END IF;

    DELETE FROM ERR$_lsr_ilr_schedule
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    RETURNING ilr_id, revision, ora_err_mesg$
    BULK COLLECT INTO l_kickouts;

    IF a_log_to_kickouts THEN
      p_log_kickouts(l_kickouts);
    END IF;

    DELETE FROM ERR$_lsr_ilr_sch_sales_direct
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    RETURNING ilr_id, revision, ora_err_mesg$
    BULK COLLECT INTO l_kickouts;

    IF a_log_to_kickouts THEN
      p_log_kickouts(l_kickouts);
    END IF;

    DELETE FROM ERR$_lsr_ilr_sch_direct_fin
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    RETURNING ilr_id, revision, ora_err_mesg$
    BULK COLLECT INTO l_kickouts;

    IF a_log_to_kickouts THEN
      p_log_kickouts(l_kickouts);
    END IF;
    
  END p_clear_schedule_errors;
  
  
  /*****************************************************************************
  * Procedure: p_initialize_kickouts
  * PURPOSE: Clears kickout and error tables for the given ILR/Revisions
  * PARAMETERS:
  *   a_ilrs: The ILR ID/Revisions for which to initialize the tables
  * NOTES: Uses autonomous transaction
  ******************************************************************************/
  PROCEDURE p_initialize_kickouts(a_ilrs t_lsr_ilr_id_revision_tab)
  IS
  BEGIN
    DELETE FROM lsr_ilr_schedule_kickouts
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs;

    p_clear_schedule_errors(a_ilrs);
    COMMIT;
  END;

  PROCEDURE p_log_top_level_exc_kickout(a_ilrs t_lsr_ilr_id_revision_tab, a_message varchar2)
  IS
    l_kickouts t_kickout_tab;
  BEGIN
    SELECT ilr_id, revision, substr('Top level exception occurred - ' || a_message, 4000)
    BULK COLLECT INTO l_kickouts
    FROM TABLE(a_ilrs);

    p_log_kickouts(l_kickouts);
  END p_log_top_level_exc_kickout;

  /*****************************************************************************
  * PROCEDURE: p_process_op_ilrs
  * PURPOSE: Processes operating-type ILR schedules for the given ILRs/Revisions
  * PARAMETERS:
  *   a_ilrs: ilr_ids/revision for which to process schedules
  ******************************************************************************/

  PROCEDURE p_process_op_ilrs(a_ilrs t_lsr_ilr_id_revision_tab)
  IS
  BEGIN

    pkg_pp_log.p_write_message('Processing operating schedules'); 
    pkg_pp_log.p_write_message('Calculating and inserting into lsr_ilr_schedule and lsr_ilr_amounts');

    INSERT ALL
    WHEN 1=1 THEN
    into lsr_ilr_schedule(ilr_id,
                          revision,
                          set_of_books_id,
                          MONTH,
                          interest_income_received,
                          interest_income_accrued,
                          interest_rental_recvd_spread,
                          beg_deferred_rev,
                          deferred_rev_activity,
                          end_deferred_rev,
                          beg_receivable,
                          end_receivable,
                          beg_lt_receivable,
                          end_lt_receivable,
                          initial_direct_cost,
                          executory_accrual1,
                          executory_accrual2,
                          executory_accrual3,
                          executory_accrual4,
                          executory_accrual5,
                          executory_accrual6,
                          executory_accrual7,
                          executory_accrual8,
                          executory_accrual9,
                          executory_accrual10,
                          executory_paid1,
                          executory_paid2,
                          executory_paid3,
                          executory_paid4,
                          executory_paid5,
                          executory_paid6,
                          executory_paid7,
                          executory_paid8,
                          executory_paid9,
                          executory_paid10,
                          contingent_accrual1,
                          contingent_accrual2,
                          contingent_accrual3,
                          contingent_accrual4,
                          contingent_accrual5,
                          contingent_accrual6,
                          contingent_accrual7,
                          contingent_accrual8,
                          contingent_accrual9,
                          contingent_accrual10,
                          contingent_paid1,
                          contingent_paid2,
                          contingent_paid3,
                          contingent_paid4,
                          contingent_paid5,
                          contingent_paid6,
                          contingent_paid7,
                          contingent_paid8,
                          contingent_paid9,
                          contingent_paid10) VALUES ( ilr_id,
                                                      revision,
                                                      set_of_books_id,
                                                      MONTH,
                                                      interest_income_received,
                                                      interest_income_accrued,
                                                      interest_rental_recvd_spread,
                                                      begin_deferred_rev,
                                                      deferred_rev,
                                                      end_deferred_rev,
                                                      begin_receivable,
                                                      end_receivable,
                                                      begin_lt_receivable,
                                                      end_lt_receivable,
                                                      initial_direct_cost,
                                                      executory_accrual1,
                                                      executory_accrual2,
                                                      executory_accrual3,
                                                      executory_accrual4,
                                                      executory_accrual5,
                                                      executory_accrual6,
                                                      executory_accrual7,
                                                      executory_accrual8,
                                                      executory_accrual9,
                                                      executory_accrual10,
                                                      executory_paid1,
                                                      executory_paid2,
                                                      executory_paid3,
                                                      executory_paid4,
                                                      executory_paid5,
                                                      executory_paid6,
                                                      executory_paid7,
                                                      executory_paid8,
                                                      executory_paid9,
                                                      executory_paid10,
                                                      contingent_accrual1,
                                                      contingent_accrual2,
                                                      contingent_accrual3,
                                                      contingent_accrual4,
                                                      contingent_accrual5,
                                                      contingent_accrual6,
                                                      contingent_accrual7,
                                                      contingent_accrual8,
                                                      contingent_accrual9,
                                                      contingent_accrual10,
                                                      contingent_paid1,
                                                      contingent_paid2,
                                                      contingent_paid3,
                                                      contingent_paid4,
                                                      contingent_paid5,
                                                      contingent_paid6,
                                                      contingent_paid7,
                                                      contingent_paid8,
                                                      contingent_paid9,
                                                      contingent_paid10)
    LOG ERRORS INTO err$_lsr_ilr_schedule REJECT LIMIT UNLIMITED
    WHEN sob_monthnum = 1 THEN
    INTO lsr_ilr_amounts(ilr_id,
                        revision,
                        set_of_books_id,
                        npv_lease_payments,
                        npv_guaranteed_residual,
                        npv_unguaranteed_residual,
                        selling_profit_loss,
                        beginning_lease_receivable,
                        beginning_net_investment,
                        cost_of_goods_sold,
                        schedule_rates) VALUES (ilr_id,
                                                    revision,
                                                    set_of_books_id,
                                                    npv_lease_payments,
                                                    npv_guaranteed_residual,
                                                    npv_unguaranteed_residual,
                                                    selling_profit_loss,
                                                    begin_receivable,
                                                    beginning_net_investment,
                                                    cost_of_goods_sold,
                                                    schedule_rates)
    LOG ERRORS INTO ERR$_lsr_ilr_amounts REJECT LIMIT UNLIMITED
    SELECT  ilr_id,
            revision,
            set_of_books_id,
            MONTH,
            interest_income_received,
            interest_income_accrued,
            interest_rental_recvd_spread,
            begin_deferred_rev,
            deferred_rev,
            end_deferred_rev,
            begin_receivable,
            end_receivable,
            begin_lt_receivable,
            end_lt_receivable,
            initial_direct_cost,
            executory_accrual1,
            executory_accrual2,
            executory_accrual3,
            executory_accrual4,
            executory_accrual5,
            executory_accrual6,
            executory_accrual7,
            executory_accrual8,
            executory_accrual9,
            executory_accrual10,
            executory_paid1,
            executory_paid2,
            executory_paid3,
            executory_paid4,
            executory_paid5,
            executory_paid6,
            executory_paid7,
            executory_paid8,
            executory_paid9,
            executory_paid10,
            contingent_accrual1,
            contingent_accrual2,
            contingent_accrual3,
            contingent_accrual4,
            contingent_accrual5,
            contingent_accrual6,
            contingent_accrual7,
            contingent_accrual8,
            contingent_accrual9,
            contingent_accrual10,
            contingent_paid1,
            contingent_paid2,
            contingent_paid3,
            contingent_paid4,
            contingent_paid5,
            contingent_paid6,
            contingent_paid7,
            contingent_paid8,
            contingent_paid9,
            contingent_paid10,
            0 AS npv_lease_payments,
            0 AS npv_guaranteed_residual,
            0 AS npv_unguaranteed_residual,
            0 AS selling_profit_loss,
            0 AS beginning_net_investment,
            0 AS cost_of_goods_sold,
            schedule_rates,
            row_number() OVER (PARTITION BY ilr_id, revision, set_of_books_id ORDER BY MONTH) AS sob_monthnum
    FROM (SELECT  results.ilr_id,
                  results.revision,
                  results.set_of_books_id,
                  cols.month,
                  cols.interest_income_received,
                  cols.interest_income_accrued,
                  cols.interest_rental_recvd_spread,
                  cols.begin_deferred_rev,
                  cols.deferred_rev,
                  cols.end_deferred_rev,
                  cols.begin_receivable,
                  cols.end_receivable,
                  cols.begin_lt_receivable,
                  cols.end_lt_receivable,
                  cols.initial_direct_cost,
                  cols.executory_accrual1,
                  cols.executory_accrual2,
                  cols.executory_accrual3,
                  cols.executory_accrual4,
                  cols.executory_accrual5,
                  cols.executory_accrual6,
                  cols.executory_accrual7,
                  cols.executory_accrual8,
                  cols.executory_accrual9,
                  cols.executory_accrual10,
                  cols.executory_paid1,
                  cols.executory_paid2,
                  cols.executory_paid3,
                  cols.executory_paid4,
                  cols.executory_paid5,
                  cols.executory_paid6,
                  cols.executory_paid7,
                  cols.executory_paid8,
                  cols.executory_paid9,
                  cols.executory_paid10,
                  cols.contingent_accrual1,
                  cols.contingent_accrual2,
                  cols.contingent_accrual3,
                  cols.contingent_accrual4,
                  cols.contingent_accrual5,
                  cols.contingent_accrual6,
                  cols.contingent_accrual7,
                  cols.contingent_accrual8,
                  cols.contingent_accrual9,
                  cols.contingent_accrual10,
                  cols.contingent_paid1,
                  cols.contingent_paid2,
                  cols.contingent_paid3,
                  cols.contingent_paid4,
                  cols.contingent_paid5,
                  cols.contingent_paid6,
                  cols.contingent_paid7,
                  cols.contingent_paid8,
                  cols.contingent_paid9,
                  cols.contingent_paid10,
                  t_lsr_ilr_schedule_all_rates( t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                                    t_lsr_rates_implicit_in_lease(NULL, NULL), 
                                                    t_lsr_rates_implicit_in_lease(NULL, NULL)) as schedule_rates
          FROM (SELECT  ilro.ilr_id,
                        ilro.revision,
                        fasb_sob.set_of_books_id,
                        pkg_lessor_schedule.f_get_op_sch_quash_exceptions(ilr_id,revision) sch
                FROM lsr_ilr_options ilro
                JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
                JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION) ) = 'operating'
                AND t_lsr_ilr_id_revision(ilro.ilr_id, ilro.revision) MEMBER OF a_ilrs) results, 
                TABLE ( results.sch ) (+) cols);

    pkg_pp_log.p_write_message(to_char(sql%rowcount) || ' record(s) inserted');

    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 and -20000 THEN
          RAISE;
        ELSE
          pkg_pp_log.p_write_message('Unhandled error occurred while processing operating ILR schedules: ' || sqlerrm || CHR(10) || f_get_call_stack);
          RAISE;
        END IF;

  END p_process_op_ilrs;

  /*****************************************************************************
  * PROCEDURE: p_process_sales_ilrs
  * PURPOSE: Processes a sales-type ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/

  PROCEDURE p_process_sales_ilrs(a_ilrs t_lsr_ilr_id_revision_tab)
  IS
  BEGIN
    pkg_pp_log.p_write_message('Processing sales-type schedules');


    pkg_pp_log.p_write_message('Calculating and inserting into lsr_ilr_schedule, lsr_ilr_schedule_sales_direct, and lsr_ilr_amounts');

    INSERT ALL
    WHEN 1=1 THEN
    into lsr_ilr_schedule(ilr_id,
                          revision,
                          set_of_books_id,
                          MONTH,
                          interest_income_received,
                          interest_income_accrued,
                          interest_rental_recvd_spread,
                          beg_deferred_rev,
                          deferred_rev_activity,
                          end_deferred_rev,
                          beg_receivable,
                          end_receivable,
                          beg_lt_receivable,
                          end_lt_receivable,
                          initial_direct_cost,
                          executory_accrual1,
                          executory_accrual2,
                          executory_accrual3,
                          executory_accrual4,
                          executory_accrual5,
                          executory_accrual6,
                          executory_accrual7,
                          executory_accrual8,
                          executory_accrual9,
                          executory_accrual10,
                          executory_paid1,
                          executory_paid2,
                          executory_paid3,
                          executory_paid4,
                          executory_paid5,
                          executory_paid6,
                          executory_paid7,
                          executory_paid8,
                          executory_paid9,
                          executory_paid10,
                          contingent_accrual1,
                          contingent_accrual2,
                          contingent_accrual3,
                          contingent_accrual4,
                          contingent_accrual5,
                          contingent_accrual6,
                          contingent_accrual7,
                          contingent_accrual8,
                          contingent_accrual9,
                          contingent_accrual10,
                          contingent_paid1,
                          contingent_paid2,
                          contingent_paid3,
                          contingent_paid4,
                          contingent_paid5,
                          contingent_paid6,
                          contingent_paid7,
                          contingent_paid8,
                          contingent_paid9,
                          contingent_paid10) values (ilr_id,
                                                      revision,
                                                      set_of_books_id,
                                                      month,
                                                      interest_income_received,
                                                      interest_income_accrued,
                                                      0,
                                                      0,
                                                      0,
                                                      0,
                                                      begin_receivable,
                                                      end_receivable,
                                                      begin_lt_receivable,
                                                      end_lt_receivable,
                                                      initial_direct_cost,
                                                      executory_accrual1,
                                                      executory_accrual2,
                                                      executory_accrual3,
                                                      executory_accrual4,
                                                      executory_accrual5,
                                                      executory_accrual6,
                                                      executory_accrual7,
                                                      executory_accrual8,
                                                      executory_accrual9,
                                                      executory_accrual10,
                                                      executory_paid1,
                                                      executory_paid2,
                                                      executory_paid3,
                                                      executory_paid4,
                                                      executory_paid5,
                                                      executory_paid6,
                                                      executory_paid7,
                                                      executory_paid8,
                                                      executory_paid9,
                                                      executory_paid10,
                                                      contingent_accrual1,
                                                      contingent_accrual2,
                                                      contingent_accrual3,
                                                      contingent_accrual4,
                                                      contingent_accrual5,
                                                      contingent_accrual6,
                                                      contingent_accrual7,
                                                      contingent_accrual8,
                                                      contingent_accrual9,
                                                      contingent_accrual10,
                                                      contingent_paid1,
                                                      contingent_paid2,
                                                      contingent_paid3,
                                                      contingent_paid4,
                                                      contingent_paid5,
                                                      contingent_paid6,
                                                      contingent_paid7,
                                                      contingent_paid8,
                                                      contingent_paid9,
                                                      contingent_paid10)
    LOG ERRORS INTO ERR$_lsr_ilr_schedule REJECT LIMIT UNLIMITED
    WHEN 1=1 THEN
    into lsr_ilr_schedule_sales_direct( ilr_id,
                                        revision,
                                        set_of_books_id,
                                        MONTH,
                                        principal_received,
                                        principal_accrued,
                                        beg_unguaranteed_residual,
                                        interest_unguaranteed_residual,
                                        ending_unguaranteed_residual,
                                        beg_net_investment,
                                        interest_net_investment,
                                        ending_net_investment) VALUES ( ilr_id,
                                                                  revision,
                                                                  set_of_books_id,
                                                                  month,
                                                                  principal_received,
                                                                  principal_accrued,
                                                                  begin_unguaranteed_residual,
                                                                  int_on_unguaranteed_residual,
                                                                  end_unguaranteed_residual,
                                                                  begin_net_investment,
                                                                  int_on_net_investment,
                                                                  end_net_investment)
    LOG ERRORS INTO ERR$_lsr_ilr_sch_sales_direct REJECT LIMIT UNLIMITED
    WHEN sob_monthnum = 1 THEN
    INTO lsr_ilr_amounts( ilr_id,
                          revision,
                          set_of_books_id,
                          npv_lease_payments,
                          npv_guaranteed_residual,
                          npv_unguaranteed_residual,
                          selling_profit_loss,
                          beginning_lease_receivable,
                          beginning_net_investment,
                          cost_of_goods_sold,
                          schedule_rates) VALUES( ilr_id,
                                                      revision,
                                                      set_of_books_id,
                                                      npv_lease_payments,
                                                      npv_guaranteed_residual,
                                                      npv_unguaranteed_residual,
                                                      selling_profit_loss,
                                                      begin_lease_receivable,
                                                      begin_net_investment,
                                                      cost_of_goods_sold,
                                                      schedule_rates)
    LOG ERRORS INTO ERR$_lsr_ilr_amounts REJECT LIMIT UNLIMITED
    SELECT  ilr_id,
            revision,
            set_of_books_id,
            MONTH,
            interest_income_received,
            interest_income_accrued,
            principal_accrued,
            principal_received,
            begin_receivable,
            end_receivable,
            begin_lt_receivable,
            end_lt_receivable,
            initial_direct_cost,
            executory_accrual1,
            executory_accrual2,
            executory_accrual3,
            executory_accrual4,
            executory_accrual5,
            executory_accrual6,
            executory_accrual7,
            executory_accrual8,
            executory_accrual9,
            executory_accrual10,
            executory_paid1,
            executory_paid2,
            executory_paid3,
            executory_paid4,
            executory_paid5,
            executory_paid6,
            executory_paid7,
            executory_paid8,
            executory_paid9,
            executory_paid10,
            contingent_accrual1,
            contingent_accrual2,
            contingent_accrual3,
            contingent_accrual4,
            contingent_accrual5,
            contingent_accrual6,
            contingent_accrual7,
            contingent_accrual8,
            contingent_accrual9,
            contingent_accrual10,
            contingent_paid1,
            contingent_paid2,
            contingent_paid3,
            contingent_paid4,
            contingent_paid5,
            contingent_paid6,
            contingent_paid7,
            contingent_paid8,
            contingent_paid9,
            contingent_paid10,
            begin_unguaranteed_residual,
            int_on_unguaranteed_residual,
            end_unguaranteed_residual,
            begin_net_investment,
            int_on_net_investment,
            end_net_investment,
            rate_implicit,
            discount_rate,
            npv_lease_payments,
            npv_guaranteed_residual,
            npv_unguaranteed_residual,
            selling_profit_loss,
            begin_lease_receivable,
            cost_of_goods_sold,
            schedule_rates,
            row_number() OVER (PARTITION BY ilr_id, revision, set_of_books_id ORDER BY MONTH) AS sob_monthnum
    FROM ( SELECT results.ilr_id,
                  results.revision,
                  results.set_of_books_id,
                  cols.month,
                  cols.principal_received,
                  cols.interest_income_received,
                  cols.interest_income_accrued,
                  cols.principal_accrued,
                  cols.begin_receivable,
                  cols.end_receivable,
                  cols.begin_lt_receivable,
                  cols.end_lt_receivable,
                  cols.initial_direct_cost,
                  cols.executory_accrual1,
                  cols.executory_accrual2,
                  cols.executory_accrual3,
                  cols.executory_accrual4,
                  cols.executory_accrual5,
                  cols.executory_accrual6,
                  cols.executory_accrual7,
                  cols.executory_accrual8,
                  cols.executory_accrual9,
                  cols.executory_accrual10,
                  cols.executory_paid1,
                  cols.executory_paid2,
                  cols.executory_paid3,
                  cols.executory_paid4,
                  cols.executory_paid5,
                  cols.executory_paid6,
                  cols.executory_paid7,
                  cols.executory_paid8,
                  cols.executory_paid9,
                  cols.executory_paid10,
                  cols.contingent_accrual1,
                  cols.contingent_accrual2,
                  cols.contingent_accrual3,
                  cols.contingent_accrual4,
                  cols.contingent_accrual5,
                  cols.contingent_accrual6,
                  cols.contingent_accrual7,
                  cols.contingent_accrual8,
                  cols.contingent_accrual9,
                  cols.contingent_accrual10,
                  cols.contingent_paid1,
                  cols.contingent_paid2,
                  cols.contingent_paid3,
                  cols.contingent_paid4,
                  cols.contingent_paid5,
                  cols.contingent_paid6,
                  cols.contingent_paid7,
                  cols.contingent_paid8,
                  cols.contingent_paid9,
                  cols.contingent_paid10,
                  cols.begin_unguaranteed_residual,
                  cols.int_on_unguaranteed_residual,
                  cols.end_unguaranteed_residual,
                  cols.begin_net_investment,
                  cols.int_on_net_investment,
                  cols.end_net_investment,
                  cols.rate_implicit,
                  cols.discount_rate,
                  cols.rate_implicit_ni,
                  cols.discount_rate_ni,
                  cols.begin_lease_receivable,
                  cols.npv_lease_payments,
                  cols.npv_guaranteed_residual,
                  cols.npv_unguaranteed_residual,
                  cols.selling_profit_loss,
                  cols.cost_of_goods_sold,
                  cols.schedule_rates
            FROM
              (SELECT ilro.ilr_id,
                      ilro.revision,
                      fasb_sob.set_of_books_id,
                      pkg_lessor_schedule.f_getsalessch_quash_exceptions(ilro.ilr_id,ilro.revision) sch
              FROM lsr_ilr_options ilro
              JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
              JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
              WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'sales type'
              AND t_lsr_ilr_id_revision(ilro.ilr_id, ilro.revision) MEMBER OF a_ilrs) results,
              TABLE ( results.sch ) (+) cols);

    pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) inserted');

    pkg_pp_log.p_write_message('Inserting calculated rates into lsr_ilr_rates');
    INSERT INTO lsr_ilr_rates(ilr_id, 
                              revision,
                              rate_type_id,
                              rate)
    SELECT  ilr_id,
            revision,
            rate_type_id,
            pkg_lessor_schedule.f_implicit_to_annual_rate(rate) as rate
    FROM (SELECT  ilr.ilr_id, 
                  ilr.revision, 
                  rate_types.rate_type_id,
                  amounts.schedule_rates.calculated_rates.rate_implicit as rate,
                  row_number() over (partition by ilr.ilr_id, ilr.revision ORDER BY amounts.set_of_books_id) as rn
          FROM TABLE(a_ilrs) ilr
          JOIN lsr_ilr_options ilro ON ilr.ilr_id = ilro.ilr_id AND ilr.revision = ilro.revision
          JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
          JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
          JOIN lsr_ilr_rate_types rate_types ON LOWER(TRIM(rate_types.DESCRIPTION)) = 'sales type discount rate'
          JOIN lsr_ilr_amounts amounts ON ilr.ilr_id = amounts.ilr_id 
                                        AND ilr.revision = ilr.revision 
                                        AND fasb_sob.set_of_books_id = amounts.set_of_books_id
          WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'sales type')
    WHERE rn = 1
    LOG ERRORS INTO err$_lsr_ilr_rates REJECT LIMIT UNLIMITED;

    pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) inserted');

    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 and -20000 THEN
          RAISE;
        ELSE
          pkg_pp_log.p_write_message('Unhandled error occurred while processing sales type ILR schedules: ' || sqlerrm || CHR(10) || f_get_call_stack);
          RAISE;
        END IF;

  END p_process_sales_ilrs;

  /*****************************************************************************
  * PROCEDURE: p_process_df_ilrs
  * PURPOSE: Processes a direct-finance-type ILR Schedule for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/

  PROCEDURE p_process_df_ilrs(a_ilrs t_lsr_ilr_id_revision_tab)
  IS
  BEGIN
    pkg_pp_log.p_write_message('Processing direct finance type schedules');
    pkg_pp_log.p_write_message('Calculating and inserting into lsr_ilr_schedule, lsr_ilr_schedule_sales_direct, lsr_ilr_schedule_direct_fin and lsr_ilr_amounts');

    INSERT ALL
    WHEN 1=1 THEN
    into lsr_ilr_schedule(ilr_id,
                          revision,
                          set_of_books_id,
                          MONTH,
                          interest_income_received,
                          interest_income_accrued,
                          interest_rental_recvd_spread,
                          beg_deferred_rev,
                          deferred_rev_activity,
                          end_deferred_rev,
                          beg_receivable,
                          end_receivable,
                          beg_lt_receivable,
                          end_lt_receivable,
                          initial_direct_cost,
                          executory_accrual1,
                          executory_accrual2,
                          executory_accrual3,
                          executory_accrual4,
                          executory_accrual5,
                          executory_accrual6,
                          executory_accrual7,
                          executory_accrual8,
                          executory_accrual9,
                          executory_accrual10,
                          executory_paid1,
                          executory_paid2,
                          executory_paid3,
                          executory_paid4,
                          executory_paid5,
                          executory_paid6,
                          executory_paid7,
                          executory_paid8,
                          executory_paid9,
                          executory_paid10,
                          contingent_accrual1,
                          contingent_accrual2,
                          contingent_accrual3,
                          contingent_accrual4,
                          contingent_accrual5,
                          contingent_accrual6,
                          contingent_accrual7,
                          contingent_accrual8,
                          contingent_accrual9,
                          contingent_accrual10,
                          contingent_paid1,
                          contingent_paid2,
                          contingent_paid3,
                          contingent_paid4,
                          contingent_paid5,
                          contingent_paid6,
                          contingent_paid7,
                          contingent_paid8,
                          contingent_paid9,
                          contingent_paid10) values ( ilr_id,
                                                      revision,
                                                      set_of_books_id,
                                                      month,
                                                      interest_income_received,
                                                      interest_income_accrued,
                                                      0,
                                                      0,
                                                      0,
                                                      0,
                                                      begin_receivable,
                                                      end_receivable,
                                                      begin_lt_receivable,
                                                      end_lt_receivable,
                                                      initial_direct_cost,
                                                      executory_accrual1,
                                                      executory_accrual2,
                                                      executory_accrual3,
                                                      executory_accrual4,
                                                      executory_accrual5,
                                                      executory_accrual6,
                                                      executory_accrual7,
                                                      executory_accrual8,
                                                      executory_accrual9,
                                                      executory_accrual10,
                                                      executory_paid1,
                                                      executory_paid2,
                                                      executory_paid3,
                                                      executory_paid4,
                                                      executory_paid5,
                                                      executory_paid6,
                                                      executory_paid7,
                                                      executory_paid8,
                                                      executory_paid9,
                                                      executory_paid10,
                                                      contingent_accrual1,
                                                      contingent_accrual2,
                                                      contingent_accrual3,
                                                      contingent_accrual4,
                                                      contingent_accrual5,
                                                      contingent_accrual6,
                                                      contingent_accrual7,
                                                      contingent_accrual8,
                                                      contingent_accrual9,
                                                      contingent_accrual10,
                                                      contingent_paid1,
                                                      contingent_paid2,
                                                      contingent_paid3,
                                                      contingent_paid4,
                                                      contingent_paid5,
                                                      contingent_paid6,
                                                      contingent_paid7,
                                                      contingent_paid8,
                                                      contingent_paid9,
                                                      contingent_paid10)
    LOG ERRORS INTO ERR$_lsr_ilr_schedule REJECT LIMIT UNLIMITED
    WHEN 1=1 THEN
    into lsr_ilr_schedule_sales_direct( ilr_id,
                                        revision,
                                        set_of_books_id,
                                        MONTH,
                                        principal_received,
                                        principal_accrued,
                                        beg_unguaranteed_residual,
                                        interest_unguaranteed_residual,
                                        ending_unguaranteed_residual,
                                        beg_net_investment,
                                        interest_net_investment,
                                        ending_net_investment) VALUES ( ilr_id,
                                                                        revision,
                                                                        set_of_books_id,
                                                                        month,
                                                                        principal_received,
                                                                        principal_accrued,
                                                                        begin_unguaranteed_residual,
                                                                        int_on_unguaranteed_residual,
                                                                        end_unguaranteed_residual,
                                                                        begin_net_investment,
                                                                        int_on_net_investment,
                                                                        end_net_investment)
    LOG ERRORS INTO ERR$_lsr_ilr_sch_sales_direct REJECT LIMIT UNLIMITED
    WHEN 1=1 THEN
    INTO lsr_ilr_schedule_direct_fin( ilr_id,
                                      revision,
                                      set_of_books_id,
                                      MONTH,
                                      begin_deferred_profit,
                                      recognized_profit,
                                      end_deferred_profit) VALUES ( ilr_id,
                                                                    revision,
                                                                    set_of_books_id,
                                                                    MONTH,
                                                                    begin_deferred_profit,
                                                                    recognized_profit,
                                                                    end_deferred_profit)
    LOG ERRORS INTO ERR$_lsr_ilr_sch_direct_fin REJECT LIMIT UNLIMITED
    WHEN sob_monthnum = 1 THEN
    INTO lsr_ilr_amounts( ilr_id,
                          revision,
                          set_of_books_id,
                          npv_lease_payments,
                          npv_guaranteed_residual,
                          npv_unguaranteed_residual,
                          selling_profit_loss,
                          beginning_lease_receivable,
                          beginning_net_investment,
                          cost_of_goods_sold,
                          schedule_rates) VALUES( ilr_id,
                                                      revision,
                                                      set_of_books_id,
                                                      npv_lease_payments,
                                                      npv_guaranteed_residual,
                                                      npv_unguaranteed_residual,
                                                      selling_profit_loss,
                                                      begin_lease_receivable,
                                                      begin_net_investment,
                                                      cost_of_goods_sold,
                                                      schedule_rates)
    LOG ERRORS INTO ERR$_lsr_ilr_amounts REJECT LIMIT UNLIMITED
    SELECT  ilr_id,
            revision,
            set_of_books_id,
            MONTH,
            interest_income_received,
            interest_income_accrued,
            principal_accrued,
            principal_received,
            begin_receivable,
            end_receivable,
            begin_lt_receivable,
            end_lt_receivable,
            initial_direct_cost,
            executory_accrual1,
            executory_accrual2,
            executory_accrual3,
            executory_accrual4,
            executory_accrual5,
            executory_accrual6,
            executory_accrual7,
            executory_accrual8,
            executory_accrual9,
            executory_accrual10,
            executory_paid1,
            executory_paid2,
            executory_paid3,
            executory_paid4,
            executory_paid5,
            executory_paid6,
            executory_paid7,
            executory_paid8,
            executory_paid9,
            executory_paid10,
            contingent_accrual1,
            contingent_accrual2,
            contingent_accrual3,
            contingent_accrual4,
            contingent_accrual5,
            contingent_accrual6,
            contingent_accrual7,
            contingent_accrual8,
            contingent_accrual9,
            contingent_accrual10,
            contingent_paid1,
            contingent_paid2,
            contingent_paid3,
            contingent_paid4,
            contingent_paid5,
            contingent_paid6,
            contingent_paid7,
            contingent_paid8,
            contingent_paid9,
            contingent_paid10,
            begin_unguaranteed_residual,
            int_on_unguaranteed_residual,
            end_unguaranteed_residual,
            begin_net_investment,
            int_on_net_investment,
            end_net_investment,
            begin_deferred_profit,
            recognized_profit,
            end_deferred_profit,
            rate_implicit,
            discount_rate,
            npv_lease_payments,
            npv_guaranteed_residual,
            npv_unguaranteed_residual,
            selling_profit_loss,
            begin_lease_receivable,
            cost_of_goods_sold,
            schedule_rates,
            row_number() OVER (PARTITION BY ilr_id, revision, set_of_books_id ORDER BY MONTH) AS sob_monthnum
    FROM ( SELECT ilr_id,
                  revision,
                  set_of_books_id,
                  month,
                  principal_received,
                  interest_income_received,
                  interest_income_accrued,
                  principal_accrued,
                  begin_receivable,
                  end_receivable,
                  begin_lt_receivable,
                  end_lt_receivable,
                  initial_direct_cost,
                  executory_accrual1,
                  executory_accrual2,
                  executory_accrual3,
                  executory_accrual4,
                  executory_accrual5,
                  executory_accrual6,
                  executory_accrual7,
                  executory_accrual8,
                  executory_accrual9,
                  executory_accrual10,
                  executory_paid1,
                  executory_paid2,
                  executory_paid3,
                  executory_paid4,
                  executory_paid5,
                  executory_paid6,
                  executory_paid7,
                  executory_paid8,
                  executory_paid9,
                  executory_paid10,
                  contingent_accrual1,
                  contingent_accrual2,
                  contingent_accrual3,
                  contingent_accrual4,
                  contingent_accrual5,
                  contingent_accrual6,
                  contingent_accrual7,
                  contingent_accrual8,
                  contingent_accrual9,
                  contingent_accrual10,
                  contingent_paid1,
                  contingent_paid2,
                  contingent_paid3,
                  contingent_paid4,
                  contingent_paid5,
                  contingent_paid6,
                  contingent_paid7,
                  contingent_paid8,
                  contingent_paid9,
                  contingent_paid10,
                  begin_unguaranteed_residual,
                  int_on_unguaranteed_residual,
                  end_unguaranteed_residual,
                  begin_net_investment,
                  int_on_net_investment,
                  end_net_investment,
                  begin_deferred_profit,
                  recognized_profit,
                  end_deferred_profit,
                  rate_implicit,
                  discount_rate,
                  rate_implicit_ni,
                  discount_rate_ni,
                  begin_lease_receivable,
                  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  cost_of_goods_sold,
                  schedule_rates
            FROM (SELECT  ilro.ilr_id,
                          ilro.revision,
                          fasb_sob.set_of_books_id,
                          pkg_lessor_schedule.f_get_df_sch_quash_exceptions(ilro.ilr_id, ilro.revision) sch
                  FROM lsr_ilr_options ilro
                  JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
                  JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                  WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION) ) = 'direct finance'
                  AND t_lsr_ilr_id_revision(ilro.ilr_id, ilro.revision) MEMBER OF a_ilrs) results,
                  TABLE (results.sch) (+) cols);

    pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) inserted');

    pkg_pp_log.p_write_message('Inserting calculated rates into lsr_ilr_rates');
      INSERT INTO lsr_ilr_rates(ilr_id, 
                                revision,
                                rate_type_id,
                                rate)
      SELECT  ilr_id,
              revision,
              rate_type_id,
              pkg_lessor_schedule.f_implicit_to_annual_rate(rate) as rate
      FROM (SELECT  ilr.ilr_id, 
                    ilr.revision, 
                    rate_types.rate_type_id,
                    amounts.schedule_rates.calculated_rates.rate_implicit as rate,
                    row_number() over (partition by ilr.ilr_id, ilr.revision ORDER BY amounts.set_of_books_id) as rn
            FROM TABLE(a_ilrs) ilr
            JOIN lsr_ilr_options ilro ON ilr.ilr_id = ilro.ilr_id AND ilr.revision = ilro.revision
            JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
            JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
            JOIN lsr_ilr_rate_types rate_types ON LOWER(TRIM(rate_types.DESCRIPTION)) = 'direct finance discount rate'
            JOIN lsr_ilr_amounts amounts ON ilr.ilr_id = amounts.ilr_id 
                                          AND ilr.revision = ilr.revision 
                                          AND fasb_sob.set_of_books_id = amounts.set_of_books_id
            WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'direct finance'
            UNION ALL
            SELECT  ilr.ilr_id, 
                    ilr.revision, 
                    rate_types.rate_type_id,
                    amounts.schedule_rates.calculated_rates.rate_implicit_ni as rate,
                    row_number() over (partition by ilr.ilr_id, ilr.revision ORDER BY amounts.set_of_books_id) as rn
            FROM TABLE(a_ilrs) ilr
            JOIN lsr_ilr_options ilro ON ilr.ilr_id = ilro.ilr_id AND ilr.revision = ilro.revision
            JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
            JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
            JOIN lsr_ilr_rate_types rate_types ON LOWER(TRIM(rate_types.DESCRIPTION)) = 'direct finance interest on net inv rate'
            JOIN lsr_ilr_amounts amounts ON ilr.ilr_id = amounts.ilr_id 
                                          AND ilr.revision = ilr.revision 
                                          AND fasb_sob.set_of_books_id = amounts.set_of_books_id
            WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'direct finance'
            UNION ALL
            SELECT  ilr.ilr_id, 
                    ilr.revision, 
                    rate_types.rate_type_id,
                    0 as rate,
                    row_number() over (partition by ilr.ilr_id, ilr.revision ORDER BY amounts.set_of_books_id) as rn
            FROM TABLE(a_ilrs) ilr
            JOIN lsr_ilr_options ilro ON ilr.ilr_id = ilro.ilr_id AND ilr.revision = ilro.revision
            JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
            JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
            JOIN lsr_ilr_rate_types rate_types ON LOWER(TRIM(rate_types.DESCRIPTION)) = 'direct finance fmv comparison rate'
            JOIN lsr_ilr_amounts amounts ON ilr.ilr_id = amounts.ilr_id 
                                          AND ilr.revision = ilr.revision 
                                          AND fasb_sob.set_of_books_id = amounts.set_of_books_id
            WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'direct finance')
      WHERE rn = 1
      LOG ERRORS INTO err$_lsr_ilr_rates REJECT LIMIT UNLIMITED;

      pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) inserted');

    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 and -20000 THEN
          RAISE;
        ELSE
          pkg_pp_log.p_write_message('Unhandled error occurred while processing direct finance ILR schedules: ' || sqlerrm || CHR(10) || f_get_call_stack);
          RAISE;
        END IF;


  END p_process_df_ilrs;

  /*****************************************************************************
  * PROCEDURE: p_check_prereqs
  * PURPOSE: Checks for existence of information used in generating and building schedule
  *           for the given ILR/Revision
  * PARAMETERS:
  *   a_ilr_id: ilr_id of the ILR for which to process the schedule
  *   a_revision: Revision of the ILR for which to process the schedule
  ******************************************************************************/
  PROCEDURE p_check_prereqs(a_ilr_id NUMBER, a_revision NUMBER) IS
    l_exists NUMBER;
  BEGIN

    IF a_ilr_id IS NULL OR a_revision IS NULL THEN
      pkg_pp_log.p_write_message('NULL ilr_id (' || nvl(to_char(a_ilr_id), 'NULL') || ') or revision (' || nvl(to_char(a_revision), 'NULL') ||') passed to p_check_prereqs');
    end if;

    SELECT  CASE
              WHEN EXISTS ( SELECT 1
                            FROM lsr_ilr
                            WHERE ilr_id = a_ilr_id)
                THEN 1
              ELSE 0
            END INTO l_exists
    FROM dual;

    IF l_exists = 0 THEN
      pkg_pp_log.p_write_message('Invalid ilr_id (' || to_char(a_ilr_id) || ') passed to p_check_prereqs (id not in lsr_ilr table)');
    END IF;

    SELECT  CASE
              WHEN EXISTS ( SELECT 1
                            FROM lsr_ilr_approval
                            WHERE ilr_id = a_ilr_id
                            AND revision = a_revision)
                THEN 1
              ELSE 0
            END INTO l_exists
    FROM dual;


    IF l_exists = 0 THEN
      pkg_pp_log.p_write_message('Invalid ilr_id/revision passed to p_check_prereqs (id/revision not in lsr_ilr_approval table)');
    END IF;

    SELECT  CASE
              WHEN EXISTS ( SELECT 1
                            FROM TABLE(f_get_payment_terms(a_ilr_id, a_revision)))
                THEN 1
              ELSE 0
            END INTO l_exists
    FROM dual;

    IF l_exists = 0 THEN
      pkg_pp_log.p_write_message('No payment terms found. Check that payment terms are defined for ILR/Revision ' || 
                                    nvl(to_char(a_ilr_id), 'NULL') || '/' || nvl(to_char(a_revision), 'NULL') ||
                                    CHR(10) || 
                                    'PKG_LESSOR_SCHEDULE.F_GET_PAYMENT_TERMS returned no rows');
    END IF;

    SELECT  CASE
              WHEN EXISTS ( SELECT 1
                            FROM (SELECT f_get_sales_type_info(a_ilr_id, a_revision) AS info
                                  FROM dual)
                            WHERE info IS NOT NULL
                            AND (f_ilr_has_sales_type(a_ilr_id, a_revision) = 1 OR f_ilr_has_direct_finance(a_ilr_id, a_revision) = 1))
                THEN 1
              ELSE 0
            END INTO l_exists
    FROM dual;

    IF l_exists = 0 THEN
      pkg_pp_log.p_write_message('No sales type or direct finance type schedules will be generated for ILR/Revision ' || 
                                    nvl(to_char(a_ilr_id), 'NULL') || '/' || nvl(to_char(a_revision), 'NULL') ||
                                    CHR(10) ||
                                    'Information necessary for sales-type and direct-finance-type schedule generation not found. ' ||
                                    'Check that there are assets assigned and that ILR options are populated correctly for this ILR/Revision.');
    END IF;

    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 and -20000 THEN
          RAISE;
        ELSE
          pkg_pp_log.p_write_message('Error checking schedule prerequisities: ' || sqlerrm || CHR(10) || f_get_call_stack);
          RAISE;
        END IF;

    END p_check_prereqs;


  /*****************************************************************************
  * PROCEDURE: p_process_ilrs
  * PURPOSE: Processes the Lessor ILR Schedule for the given ILR/Revisions
  * PARAMETERS:
  *   a_ilrs: The ILR ID/Revisions for which to process the schedule
  ******************************************************************************/
  PROCEDURE p_process_ilrs(a_ilrs t_lsr_ilr_id_revision_tab)
  IS
    l_cap_type_desc VARCHAR2(254);
    l_message VARCHAR2(4000);
    l_date DATE;
    l_exists NUMBER;
    l_kickouts t_kickout_tab;
  BEGIN

    pkg_pp_log.p_start_log(f_get_pp_process_id('Lessor - ILR Schedule'));

    pkg_pp_log.p_write_message('Starting ILR Schedule Processing for ' || to_char(a_ilrs.COUNT) || ' ILR/Revisions');

    pkg_pp_log.p_write_message('Initializing kickout tables');
    p_initialize_kickouts(a_ilrs);

    pkg_pp_log.p_write_message('Checking prerequisites');
    FOR ilr IN (SELECT ilr_id, revision FROM TABLE(a_ilrs))
    LOOP
      DECLARE
        l_kickout t_kickout;
      BEGIN
        p_check_prereqs(ilr.ilr_id, ilr.revision);
      EXCEPTION
        WHEN OTHERS THEN
          l_kickout.ilr_id := ilr.ilr_id;
          l_kickout.revision := ilr.revision;
          l_kickout.message := 'Error checking prerequisites: ' || sqlerrm;
          p_log_kickouts(t_kickout_tab(l_kickout));
          CONTINUE;
      END;
    END LOOP;

    pkg_pp_log.p_write_message('Deleting previously calculated rates (if any) from lsr_ilr_rates');

    DELETE FROM lsr_ilr_rates
    WHERE rate_type_id IN ( SELECT rate_type_id
                            FROM lsr_ilr_rate_types
                            WHERE LOWER(TRIM(DESCRIPTION)) IN ( 'sales type discount rate',
                                                                'direct finance discount rate', 
                                                                'direct finance interest on net inv rate',
                                                                'direct finance fmv comparison rate'))
    AND t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    LOG ERRORS INTO err$_lsr_ilr_rates REJECT LIMIT UNLIMITED;

    pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) deleted');

    pkg_pp_log.p_write_message('Deleting inapplicable override rates (if any) from lsr_ilr_rates');
    DELETE FROM lsr_ilr_rates
    WHERE (ilr_id, revision, rate_type_id) 
      NOT IN (SELECT ilr.ilr_id, ilr.revision, rate_types.rate_type_id
              FROM TABLE(a_ilrs) ilr
              JOIN lsr_ilr_options ilro ON ilr.ilr_id = ilro.ilr_id AND ilr.revision = ilro.revision
              JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
              JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
              JOIN lsr_ilr_rate_types rate_types ON LOWER(TRIM(fasb_cap_type.DESCRIPTION)) =
                                                      CASE LOWER(TRIM(rate_types.DESCRIPTION))
                                                        WHEN 'sales type discount rate override'
                                                          THEN 'sales type'
                                                        WHEN 'direct finance discount rate override'
                                                          THEN 'direct finance'
                                                        WHEN 'direct finance interest on net inv rate override'
                                                          THEN 'direct finance'
                                                        WHEN 'direct finance fmv comparison rate override'
                                                          THEN 'direct finance'
                                                        ELSE NULL
                                                      END)
    AND t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    LOG ERRORS INTO err$_lsr_ilr_rates REJECT LIMIT UNLIMITED;

    pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) deleted');


    pkg_pp_log.p_write_message('Deleting from lsr_ilr_amounts');

    DELETE FROM lsr_ilr_amounts
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    LOG ERRORS INTO err$_lsr_ilr_amounts REJECT LIMIT UNLIMITED;

    pkg_pp_log.p_write_message(to_char(sql%rowcount) || ' record(s) deleted');

    pkg_pp_log.p_write_message('Deleting from lsr_ilr_schedule_direct_fin');

    DELETE FROM lsr_ilr_schedule_direct_fin
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    LOG ERRORS INTO err$_lsr_ilr_sch_direct_fin REJECT LIMIT UNLIMITED;

    pkg_pp_log.p_write_message(to_char(sql%rowcount) || ' record(s) deleted');

    pkg_pp_log.p_write_message('Deleting from lsr_ilr_schedule_sales_direct');

    DELETE FROM lsr_ilr_schedule_sales_direct
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    LOG ERRORS INTO err$_lsr_ilr_sch_sales_direct REJECT LIMIT UNLIMITED;

    pkg_pp_log.p_write_message(to_char(sql%rowcount) || ' record(s) deleted');

    pkg_pp_log.p_write_message('Deleting from lsr_ilr_schedule');

    DELETE FROM lsr_ilr_schedule
    WHERE t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    LOG ERRORS INTO err$_lsr_ilr_schedule REJECT LIMIT UNLIMITED;

    pkg_pp_log.p_write_message(to_char(sql%rowcount) || ' record(s) deleted');

    pkg_pp_log.p_write_message('Prior schedule results cleared');

    p_process_op_ilrs(a_ilrs);

    pkg_pp_log.p_write_message('Deferring constraints r_lsr_ilr_sched_sales_direct3 and lsr_ilr_sch_direct_fin_sch_fk');
    --Oracle Bug 2891576 (INSERT ALL can cause FK error randomly with immediate constraint)
    execute immediate 'SET CONSTRAINTS r_lsr_ilr_sched_sales_direct3, lsr_ilr_sch_direct_fin_sch_fk DEFERRED';

    p_process_sales_ilrs(a_ilrs);
    p_process_df_ilrs(a_ilrs);

    pkg_pp_log.p_write_message('Checking for generated schedules');
    SELECT ilr_id, revision, msg
    BULK COLLECT INTO l_kickouts
    FROM (SELECT DISTINCT ilr_id, revision, 'No schedule rows present in lsr_ilr_schedule' AS msg
          FROM TABLE(a_ilrs)
          WHERE (ilr_id, revision) NOT IN (SELECT ilr_id, revision
                                            FROM lsr_ilr_schedule));
    IF SQL%rowcount > 0 THEN
      pkg_pp_log.p_write_message(sql%rowcount || ' ILR/Revision(s) did not generate schedule results');
      p_log_kickouts(l_kickouts);
    END IF;

    pkg_pp_log.p_write_message('Checking for invalid sales type/direct finance schedules');
    DELETE FROM lsr_ilr_schedule_sales_direct
    WHERE (ilr_id, revision, MONTH, set_of_books_id) NOT IN ( SELECT ilr_id, revision, MONTH, set_of_books_id
                                                              FROM lsr_ilr_schedule)
    AND t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    RETURNING ilr_id, revision, 'Orphan record(s) present in lsr_ilr_schedule_sales_direct'
    BULK COLLECT INTO l_kickouts;

    if sql%rowcount > 0 THEN
      pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) deleted');
      p_log_kickouts(l_kickouts);
    end if;

    DELETE FROM lsr_ilr_schedule_direct_fin
    WHERE (ilr_id, revision, MONTH, set_of_books_id) NOT IN ( SELECT ilr_id, revision, MONTH, set_of_books_id
                                                            FROM lsr_ilr_schedule_sales_direct)
    AND t_lsr_ilr_id_revision(ilr_id, revision) MEMBER OF a_ilrs
    RETURNING ilr_id, revision, 'Orphan record(s) present in lsr_ilr_schedule_direct_fin'
    BULK COLLECT INTO l_kickouts;

    if sql%rowcount > 0 THEN
      pkg_pp_log.p_write_message(to_char(SQL%rowcount) || ' record(s) deleted');
      p_log_kickouts(l_kickouts);
    end if;

    pkg_pp_log.p_write_message('Resetting constraints r_lsr_ilr_sched_sales_direct3 and lsr_ilr_sch_direct_fin_sch_fk to immediate');

    EXECUTE IMMEDIATE 'SET CONSTRAINTS r_lsr_ilr_sched_sales_direct3, lsr_ilr_sch_direct_fin_sch_fk IMMEDIATE';

    pkg_pp_log.p_write_message('Calculating variable payments');

    FOR ilr IN (SELECT DISTINCT sch.ilr_id, 
                                sch.revision 
                FROM lsr_ilr_options sch
                JOIN lsr_ilr_payment_term_var_pay vp ON sch.ilr_id = vp.ilr_id AND sch.revision = vp.revision
                WHERE t_lsr_ilr_id_revision(sch.ilr_id, sch.revision) MEMBER OF a_ilrs)
    LOOP
      DECLARE
        l_kickout t_kickout;
      BEGIN
        SELECT MIN(month) INTO l_date
        FROM lsr_ilr_schedule
        WHERE ilr_id = ilr.ilr_id
        AND revision = ilr.revision;

        l_message := pkg_lessor_var_payments.f_calc_ilr_var_payments(ilr.ilr_id, ilr.revision, l_date);

        IF l_message IS NULL OR l_message <> 'OK' THEN
          pkg_pp_log.p_write_message('Error calculating variable payments - ' || l_message || CHR(10)  || f_get_call_stack);
          l_kickout.ilr_id := ilr.ilr_id;
          l_kickout.revision := ilr.revision;
          l_kickout.message := l_message;
          p_log_kickouts(t_kickout_tab(l_kickout));
        END IF;

      EXCEPTION
        WHEN OTHERS THEN
          pkg_pp_log.p_write_message('Error calculating variable payments - ' || SQLERRM || CHR(10) || f_get_call_stack);
          l_kickout.ilr_id := ilr.ilr_id;
          l_kickout.revision := ilr.revision;
          l_kickout.message := sqlerrm;
          p_log_kickouts(t_kickout_tab(l_kickout));
          CONTINUE;
      END;
    END LOOP;

    pkg_pp_log.p_write_message('Variable payments calculated');

    --Refresh dense currency rates in case there is a new min/max month on the schedule tables
    --(which determine the months for which dense currency rates will be generated)

    pkg_pp_log.p_write_message('Checking if currency translation months need to be extended');
    SELECT CASE
            WHEN EXISTS ( SELECT 1
                          FROM (SELECT MAX(TRUNC(MONTH, 'MONTH')) AS max_month
                                FROM lsr_ilr_schedule)
                          WHERE max_month > ( SELECT MAX(TRUNC(exchange_date, 'MONTH'))
                                              FROM currency_rate_default_dense))
              THEN 1
            WHEN EXISTS ( SELECT 1
                          FROM (SELECT MIN(TRUNC(MONTH, 'MONTH')) AS min_month
                                FROM lsr_ilr_schedule)
                          WHERE min_month < ( SELECT MIN(TRUNC(exchange_date, 'MONTH'))
                                              FROM currency_rate_default_dense))
              THEN 1
            ELSE 0
          END into l_exists
    FROM dual;

    IF l_exists = 1 THEN
      pkg_pp_log.p_write_message('Extension required. Executing p_refresh_curr_rate_dflt_dense');
      p_refresh_curr_rate_dflt_dense;
    ELSE
      pkg_pp_log.p_write_message('Extension not required. Continuing');
    END IF;

    pkg_pp_log.p_write_message('Processing kickouts');

    p_clear_schedule_errors(a_ilrs, TRUE);
    p_del_error_ilrs_from_sch_tbls(a_ilrs);

    pkg_pp_log.p_write_message('Kickouts processed');

    pkg_pp_log.p_write_message('Schedule processing complete');

    pkg_pp_log.p_end_log;
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE BETWEEN -20999 AND -20000 THEN
        p_del_error_ilrs_from_sch_tbls(a_ilrs);
        p_clear_schedule_errors(a_ilrs, TRUE);
        p_log_top_level_exc_kickout(a_ilrs, sqlerrm);
        ROLLBACK;
        pkg_pp_log.p_write_message(sqlerrm);
        pkg_pp_log.p_end_log;
        RAISE;
      --PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
      ELSIF SQLCODE = 100 THEN
        p_del_error_ilrs_from_sch_tbls(a_ilrs);
        p_clear_schedule_errors(a_ilrs, TRUE);
        p_log_top_level_exc_kickout(a_ilrs, sqlerrm);
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
        pkg_pp_log.p_end_log;
        raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
      ELSE
        p_del_error_ilrs_from_sch_tbls(a_ilrs);
        p_clear_schedule_errors(a_ilrs, TRUE);
        p_log_top_level_exc_kickout(a_ilrs, sqlerrm);
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
        pkg_pp_log.p_end_log;
        RAISE;
      END IF;
      pkg_pp_log.p_end_log;
  END p_process_ilrs;
  
  /*****************************************************************************
  * PROCEDURE: p_process_ilrs
  * PURPOSE: Processes the Lessor ILR Schedule for a single ILR ID/Revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to process the schedule
  *   a_revision: The Revision for which to process the schedule
  ******************************************************************************/
  PROCEDURE p_process_ilr(a_ilr_id NUMBER, a_revision NUMBER) IS
    l_tab t_lsr_ilr_id_revision_tab;
    l_exists number;
  BEGIN
    pkg_pp_log.p_start_log(f_get_pp_process_id('Lessor - ILR Schedule'));
    
    l_tab := t_lsr_ilr_id_revision_tab(t_lsr_ilr_id_revision(a_ilr_id, a_revision));

    p_process_ilrs(l_tab);

    SELECT CASE
            WHEN EXISTS ( SELECT 1
                          FROM lsr_ilr_schedule_kickouts
                          WHERE ilr_id = a_ilr_id
                          AND revision = a_revision)
              THEN 1
            ELSE 0
          END
    INTO l_exists
    FROM dual;

    IF l_exists = 1 THEN
      raise_application_error(-20000, 'Error processing Lessor ILR ID/Revision: ' || to_char(a_ilr_id) || '/' || to_char(a_revision) || chr(10) || 'Check log -- kickouts generated');
    END IF;
  END p_process_ilr;

  /*****************************************************************************
  * Function: f_ilr_has_operating
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with operating cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_operating(a_ilr_id number, a_revision number) return number
  is
    l_exists number;
  BEGIN
    SELECT CASE
            WHEN EXISTS ( SELECT 1
                          FROM lsr_ilr_options ilro
                          JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
                          JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                          WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'operating'
                          AND ilr_id = a_ilr_id
                          AND revision = a_revision)
              THEN 1
            ELSE 0
          END
    INTO l_exists
    FROM dual;

    RETURN l_exists;
  END f_ilr_has_operating;

  /*****************************************************************************
  * Function: f_ilr_has_sales_type
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with sales type cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_sales_type(a_ilr_id number, a_revision number) return number
  is
    l_exists number;
  BEGIN
    SELECT CASE
            WHEN EXISTS ( SELECT 1
                          FROM lsr_ilr_options ilro
                          JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
                          JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                          WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'sales type'
                          AND ilr_id = a_ilr_id
                          AND revision = a_revision)
              THEN 1
            ELSE 0
          END
    INTO l_exists
    FROM dual;

    RETURN l_exists;
  END f_ilr_has_sales_type;

  /*****************************************************************************
  * Function: f_ilr_has_direct_finance
  * PURPOSE: Looks up and returns whether or not the ILR is associated to any sets of books with direct finance cap type
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR to check
  *   a_revision: The revision of the ILR to check
  *
  * RETURNS: 0 if no, 1 if yes
  ******************************************************************************/
  FUNCTION f_ilr_has_direct_finance(a_ilr_id number, a_revision number) return number
  is
    l_exists number;
  BEGIN
    SELECT CASE
            WHEN EXISTS ( SELECT 1
                          FROM lsr_ilr_options ilro
                          JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
                          JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                          WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'direct finance'
                          AND ilr_id = a_ilr_id
                          AND revision = a_revision)
              THEN 1
            ELSE 0
          END
    INTO l_exists
    FROM dual;

    RETURN l_exists;
  END f_ilr_has_direct_finance;

  /*****************************************************************************
  * Function: f_get_payment_terms
  * PURPOSE: Looks up and returns the payment terms for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with payment terms
  * NOTE: This now looks to the variable payments package for logic to get payment terms
  ******************************************************************************/

  FUNCTION f_get_payment_terms(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_op_sch_pay_term_tab 
  IS
    l_results lsr_ilr_op_sch_pay_term_tab;
  BEGIN
    -- If there's a way to avoid this unpacking/repacking, that would be awesome
    SELECT lsr_ilr_op_sch_pay_term( payment_month_frequency,
                                    payment_term_start_date,
                                    number_of_terms,
                                    payment_amount,
                                    executory_buckets,
                                    contingent_buckets,
                                    is_prepay)
    BULK COLLECT INTO l_results
    FROM TABLE(pkg_lessor_var_payments.f_get_initial_pay_terms(a_ilr_id, a_revision));

    return l_results;
  END f_get_payment_terms;

  /*****************************************************************************
  * Function: f_get_initial_direct_costs
  * PURPOSE: Looks up and returns the initial direct costs for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve payment terms
  *   a_revision: The revision of the ILR for which to retrieve payment terms
  *
  * RETURNS: Table with initial direct costs
  ******************************************************************************/
  FUNCTION f_get_initial_direct_costs(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_init_direct_cost_info_tab
  IS
    l_results lsr_init_direct_cost_info_tab;
  BEGIN
    SELECT COST
    BULK COLLECT INTO l_results
    FROM (SELECT lsr_init_direct_cost_info( idc_group_id,
                                            date_incurred,
                                            amount,
                                            DESCRIPTION) as COST
          FROM lsr_ilr_initial_direct_cost
          WHERE ilr_id = a_ilr_id
          AND revision = a_revision
          UNION ALL
          SELECT lsr_init_direct_cost_info( NULL,
                                            NULL,
                                            0,
                                            NULL)
          FROM lsr_ilr_initial_direct_cost
          WHERE ilr_id = a_ilr_id
          AND revision = a_revision
          HAVING COUNT(1) = 0);

    RETURN l_results;

  END f_get_initial_direct_costs;

  /*****************************************************************************
  * Function: f_get_sales_type_info
  * PURPOSE: Looks up and returns information necessary to complete the building of sales-type schedules
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to retrieve infomration
  *   a_revision: The revision of the ILR for which to retrieve information
  *
  * RETURNS: Table with sales-type information (pipelined)
  ******************************************************************************/

  FUNCTION f_get_sales_type_info(a_ilr_id NUMBER, a_revision NUMBER) RETURN lsr_ilr_sales_sch_info
  IS
    l_sales_type_info lsr_ilr_sales_sch_info;
  BEGIN
    SELECT  lsr_ilr_sales_sch_info( SUM(COALESCE(asset.carrying_cost, 0)),
                                    SUM(COALESCE(asset.carrying_cost_comp_curr, 0)),
                                    SUM(COALESCE(asset.fair_market_value, 0)),
                                    SUM(COALESCE(asset.fair_market_value_comp_curr, 0)),
                                    SUM(COALESCE(asset.guaranteed_residual_amount, 0)),
                                    SUM(COALESCE(asset.fair_market_value * asset.estimated_residual_pct, 0)),
                                    COALESCE(lease.days_in_year, 365),
                                    COALESCE(opt.purchase_option_amt, 0),
                                    COALESCE(opt.termination_amt, 0))
    INTO l_sales_type_info
    FROM lsr_ilr_options opt
    JOIN lsr_ilr ilr
      ON opt.ilr_id = ilr.ilr_id
    JOIN lsr_lease lease
      ON ilr.lease_id = lease.lease_id
    JOIN lsr_asset asset
      ON opt.ilr_id = asset.ilr_id
      AND opt.revision = asset.revision
    WHERE opt.ilr_id = a_ilr_id
    AND opt.revision = a_revision
    GROUP BY opt.ilr_id,
             opt.revision,
             lease.days_in_year,
             opt.purchase_option_amt,
             opt.termination_amt;
    return l_sales_type_info;
  END f_get_sales_type_info;

  /*****************************************************************************
  * Function: f_get_payment_info_from_terms
  * PURPOSE: Transforms payment terms into a month-by-month listing of payment information
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to generate payment info
  *
  * RETURNS: Table with payment information (pipelined)
  ******************************************************************************/

  FUNCTION f_get_payment_info_from_terms(a_payment_terms lsr_ilr_op_sch_pay_term_tab) RETURN lsr_ilr_op_sch_pay_info_tab
  IS
    l_pay_info_tab lsr_ilr_op_sch_pay_info_tab;
  BEGIN
    WITH payment_terms(payment_month_frequency,
                        payment_term_start_date,
                        MONTH,
                        number_of_terms,
                        payment_amount,
                        contingent_buckets,
                        executory_buckets,
                        is_prepay,
                        iter)
    AS (SELECT  payment_month_frequency,
                payment_term_start_date,
                payment_term_start_date as month,
                number_of_terms,
                payment_amount,
                contingent_buckets,
                executory_buckets,
                is_prepay,
                CASE is_prepay
                  WHEN 0 THEN 1
                  WHEN 1 THEN payment_month_frequency * number_of_terms
                  ELSE NULL
                END AS iter --We want to count down/up when prepay/arrears (so that payments will take place at the beginning/end of period (see payments below)
        FROM table(a_payment_terms)
        ----^^ Base Case
        UNION ALL
        --Recurisve case
        SELECT  payment_month_frequency,
                payment_term_start_date,
                add_months(month, 1) as month,
                number_of_terms,
                payment_amount,
                contingent_buckets,
                executory_buckets,
                is_prepay,
                CASE is_prepay
                  WHEN 0 THEN iter + 1
                  WHEN 1 THEN iter - 1
                  ELSE NULL
                END AS iter
        FROM payment_terms
        WHERE add_months(MONTH, 1) < add_months(payment_term_start_date, payment_month_frequency * number_of_terms))
      --Calculate payments based on payment terms
    SELECT  lsr_ilr_op_sch_pay_info(payment_month_frequency,
                                    payment_term_start_date,
                                    month,
                                    number_of_terms,
                                    payment_amount,
                                    contingent_buckets,
                                    executory_buckets,
                                    is_prepay,
                                    iter) BULK COLLECT INTO l_pay_info_tab
      FROM payment_terms;
      return l_pay_info_tab;
  END f_get_payment_info_from_terms;

  /*****************************************************************************
  * Function: f_get_payments_from_info
  * PURPOSE: Transforms month-by-month payment information listing into month-by-month list of calculated payment amounts
  * PARAMETERS:
  *   a_payment_info: The payment information for which to calculate payment amounts
  *
  * RETURNS: Table with calculated payment amounts
  ******************************************************************************/

  FUNCTION f_get_payments_from_info(a_payment_info lsr_ilr_op_sch_pay_info_tab) 
  RETURN lsr_schedule_payment_def_tab DETERMINISTIC
  IS
    l_payments lsr_schedule_payment_def_tab;
  BEGIN
      SELECT  lsr_schedule_payment_def( payment_month_frequency,
                                        is_prepay,
                                        DENSE_RANK() OVER (ORDER BY payment_term_start_date), --Group months based on date payment takes place
                                        month,
                                        number_of_terms,
                                        iter,
                                        CASE
                                          --We want payments to occur every ith month. The modulus of the current iteration will give us this (couning down/up for prepay/arrears)
                                          WHEN MOD(iter, payment_month_frequency) = 0 THEN payment_amount
                                          ELSE 0
                                        END) BULK COLLECT INTO l_payments
      FROM TABLE(a_payment_info);
      RETURN l_payments;
  END f_get_payments_from_info;

  /*****************************************************************************
  * Function: f_calculate_buckets
  * PURPOSE: Calculates executory and contingent bucket accruals and payments
  * PARAMETERS:
  *   a_payment_terms: The payment terms for which to process buckets
  *
  * RETURNS: Table with bucket information (pipelined)
  ******************************************************************************/

  FUNCTION f_calculate_buckets(payment_info lsr_ilr_op_sch_pay_info_tab)
  RETURN lsr_bucket_result_tab PIPELINED DETERMINISTIC
  IS
  BEGIN
    for rec in (
      WITH buckets
      as (SELECT  A.month,
                  b.bucket_name,
                  --In order to prevent rounding errors for accumulating, we round during non-payment months
                  -- and "true-up" during payment months to account for the difference
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) <> 0
                      THEN round(amount / payment_month_frequency, 2)
                      ELSE round(amount - (round((amount / payment_month_frequency), 2) * (payment_month_frequency -  1)), 2)
                  END as accrued,
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) = 0 THEN b.amount
                    ELSE 0
                  END AS received
          FROM TABLE(payment_info) a, TABLE(A.executory_buckets) (+) b
          UNION ALL
          SELECT  A.month,
                  b.bucket_name,
                  --In order to prevent rounding errors for accumulating, we round during non-payment months
                  -- and "true-up" during payment months to account for the difference
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) <> 0
                      THEN round(amount / payment_month_frequency, 2)
                      ELSE round(amount - (round((amount / payment_month_frequency), 2) * (payment_month_frequency -  1)), 2)
                  END as accrued,
                  CASE
                    WHEN MOD(A.iter, A.payment_month_frequency) = 0 THEN b.amount
                    ELSE 0
                  END AS received
          FROM TABLE(payment_info) A, TABLE(A.contingent_buckets) (+) b)
      SELECT  lsr_bucket_result(month,
                                executory_accrued_1,
                                executory_accrued_2,
                                executory_accrued_3,
                                executory_accrued_4,
                                executory_accrued_5,
                                executory_accrued_6,
                                executory_accrued_7,
                                executory_accrued_8,
                                executory_accrued_9,
                                executory_accrued_10,
                                executory_received_1,
                                executory_received_2,
                                executory_received_3,
                                executory_received_4,
                                executory_received_5,
                                executory_received_6,
                                executory_received_7,
                                executory_received_8,
                                executory_received_9,
                                executory_received_10,
                                contingent_accrued_1,
                                contingent_accrued_2,
                                contingent_accrued_3,
                                contingent_accrued_4,
                                contingent_accrued_5,
                                contingent_accrued_6,
                                contingent_accrued_7,
                                contingent_accrued_8,
                                contingent_accrued_9,
                                contingent_accrued_10,
                                contingent_received_1,
                                contingent_received_2,
                                contingent_received_3,
                                contingent_received_4,
                                contingent_received_5,
                                contingent_received_6,
                                contingent_received_7,
                                contingent_received_8,
                                contingent_received_9,
                                contingent_received_10) as bucket_result
      FROM(SELECT MONTH,
                  COALESCE(executory_accrued_1, 0) as executory_accrued_1,
                  COALESCE(executory_received_1, 0) as executory_received_1,
                  COALESCE(executory_accrued_2, 0) AS executory_accrued_2,
                  COALESCE(executory_received_2, 0) as executory_received_2,
                  COALESCE(executory_accrued_3, 0) as executory_accrued_3,
                  COALESCE(executory_received_3, 0) as executory_received_3,
                  coalesce(executory_accrued_4, 0) as executory_accrued_4,
                  COALESCE(executory_received_4, 0) as executory_received_4,
                  coalesce(executory_accrued_5, 0) as executory_accrued_5,
                  COALESCE(executory_received_5, 0) as executory_received_5,
                  coalesce(executory_accrued_6, 0) as executory_accrued_6,
                  COALESCE(executory_received_6, 0) as executory_received_6,
                  coalesce(executory_accrued_7, 0) as executory_accrued_7,
                  COALESCE(executory_received_7, 0) as executory_received_7,
                  coalesce(executory_accrued_8, 0) as executory_accrued_8,
                  COALESCE(executory_received_8, 0) as executory_received_8,
                  coalesce(executory_accrued_9, 0) as executory_accrued_9,
                  COALESCE(executory_received_9, 0) as executory_received_9,
                  coalesce(executory_accrued_10, 0) as executory_accrued_10,
                  COALESCE(executory_received_10, 0) as executory_received_10,
                  COALESCE(contingent_accrued_1, 0) as contingent_accrued_1,
                  COALESCE(contingent_received_1, 0) as contingent_received_1,
                  COALESCE(contingent_accrued_2, 0) AS contingent_accrued_2,
                  coalesce(contingent_received_2, 0) as contingent_received_2,
                  coalesce(contingent_accrued_3, 0) as contingent_accrued_3,
                  COALESCE(contingent_received_3, 0) as contingent_received_3,
                  coalesce(contingent_accrued_4, 0) as contingent_accrued_4,
                  COALESCE(contingent_received_4, 0) as contingent_received_4,
                  coalesce(contingent_accrued_5, 0) as contingent_accrued_5,
                  COALESCE(contingent_received_5, 0) as contingent_received_5,
                  coalesce(contingent_accrued_6, 0) as contingent_accrued_6,
                  COALESCE(contingent_received_6, 0) as contingent_received_6,
                  coalesce(contingent_accrued_7, 0) as contingent_accrued_7,
                  COALESCE(contingent_received_7, 0) as contingent_received_7,
                  coalesce(contingent_accrued_8, 0) as contingent_accrued_8,
                  COALESCE(contingent_received_8, 0) as contingent_received_8,
                  coalesce(contingent_accrued_9, 0) as contingent_accrued_9,
                  COALESCE(contingent_received_9, 0) as contingent_received_9,
                  COALESCE(contingent_accrued_10, 0) AS contingent_accrued_10,
                  COALESCE(contingent_received_10, 0) as contingent_received_10
          FROM (SELECT month, bucket_name || '_a' as bucket, accrued as amount
                FROM buckets
                UNION ALL
                SELECT MONTH, bucket_name || '_r', received
                FROM buckets)
          PIVOT (
            SUM(amount) --PIVOT requires an aggregate here. Should only be one amount per bucket accrued/received per month
            FOR bucket in ( 'e_bucket_1_a' as executory_accrued_1,
                            'e_bucket_1_r' AS executory_received_1,
                            'e_bucket_2_a' AS executory_accrued_2,
                            'e_bucket_2_r' as executory_received_2,
                            'e_bucket_3_a' AS executory_accrued_3,
                            'e_bucket_3_r' AS executory_received_3,
                            'e_bucket_4_a' AS executory_accrued_4,
                            'e_bucket_4_r' AS executory_received_4,
                            'e_bucket_5_a' AS executory_accrued_5,
                            'e_bucket_5_r' AS executory_received_5,
                            'e_bucket_6_a' AS executory_accrued_6,
                            'e_bucket_6_r' AS executory_received_6,
                            'e_bucket_7_a' AS executory_accrued_7,
                            'e_bucket_7_r' AS executory_received_7,
                            'e_bucket_8_a' AS executory_accrued_8,
                            'e_bucket_8_r' AS executory_received_8,
                            'e_bucket_9_a' AS executory_accrued_9,
                            'e_bucket_9_r' AS executory_received_9,
                            'e_bucket_10_a' AS executory_accrued_10,
                            'e_bucket_10_r' as executory_received_10,
                            'c_bucket_1_a' as contingent_accrued_1,
                            'c_bucket_1_r' AS contingent_received_1,
                            'c_bucket_2_a' AS contingent_accrued_2,
                            'c_bucket_2_r' as contingent_received_2,
                            'c_bucket_3_a' AS contingent_accrued_3,
                            'c_bucket_3_r' AS contingent_received_3,
                            'c_bucket_4_a' AS contingent_accrued_4,
                            'c_bucket_4_r' AS contingent_received_4,
                            'c_bucket_5_a' AS contingent_accrued_5,
                            'c_bucket_5_r' AS contingent_received_5,
                            'c_bucket_6_a' AS contingent_accrued_6,
                            'c_bucket_6_r' AS contingent_received_6,
                            'c_bucket_7_a' AS contingent_accrued_7,
                            'c_bucket_7_r' AS contingent_received_7,
                            'c_bucket_8_a' AS contingent_accrued_8,
                            'c_bucket_8_r' AS contingent_received_8,
                            'c_bucket_9_a' AS contingent_accrued_9,
                            'c_bucket_9_r' AS contingent_received_9,
                            'c_bucket_10_a' AS contingent_accrued_10,
                            'c_bucket_10_r' AS contingent_received_10))))
    LOOP
      pipe row(rec.bucket_result);
    END LOOP;
  END f_calculate_buckets;

  /*****************************************************************************
  * Function: f_calc_rates_implicit_sales
  * PURPOSE: Calculates the rates implicit for a sales-type lease with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *
  * RETURNS: Rates implicit in lease
  * NOTE: For a sales-type lease, the rates for net investment do not differ from other rates
  ******************************************************************************/
  FUNCTION f_calc_rates_implicit_sales( a_payments lsr_schedule_payment_def_tab, 
                                        a_initial_direct_costs lsr_init_direct_cost_info_tab, 
                                        a_sales_type_info lsr_ilr_sales_sch_info)
  RETURN t_lsr_rates_implicit_in_lease DETERMINISTIC
  IS
    l_costs t_number_22_2_tab;
    l_rate float;
  BEGIN
    SELECT  CAST(COLLECT(amount ORDER BY order_by) as t_number_22_2_tab)
    INTO l_costs
    FROM (SELECT  sum(amount) as amount,
                  order_by
          FROM (SELECT  -1 * (a_sales_type_info.fair_market_value + CASE
                                                                      WHEN a_sales_type_info.fair_market_value = a_sales_type_info.carrying_cost
                                                                        THEN idc.amount
                                                                      ELSE 0
                                                                    END) AS amount,
                        1 AS order_by
                FROM (SELECT COALESCE(SUM(amount), 0) AS amount
                    FROM TABLE(a_initial_direct_costs)) idc
                UNION ALL
                SELECT payment_amount + CASE MONTH
                                          WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                                            THEN  a_sales_type_info.estimated_residual + 
                                                  a_sales_type_info.purchase_option_amount +
                                                  a_sales_type_info.termination_amount
                                          ELSE 0
                                        END AS amount,
                        row_number() OVER(ORDER BY MONTH) + (1 - is_prepay) AS order_by
                FROM TABLE(a_payments))
        group by order_by);

    l_rate := pkg_financial_calcs.f_irr(l_costs);

    RETURN t_lsr_rates_implicit_in_lease(l_rate, l_rate);
  END f_calc_rates_implicit_sales;

  /*****************************************************************************
  * Function: f_calc_rates_implicit_df
  * PURPOSE: Calculates the rates implicit for a direct-finance-type lease with the given
  *           payment terms and initial costs
  * PARAMETERS:
  *   a_payments: The payments
  *   a_initial_direct_costs: The initial direct costs
  *   a_sales_type_info: The sales-type-specific info
  *
  * RETURNS: Rates implicit in lease
  ******************************************************************************/

  function f_calc_rates_implicit_df(a_payments lsr_schedule_payment_def_tab, 
                                    a_initial_direct_costs lsr_init_direct_cost_info_tab, 
                                    a_sales_type_info lsr_ilr_sales_sch_info)
  RETURN t_lsr_rates_implicit_in_lease DETERMINISTIC
  is
    l_costs t_number_22_2_tab;
    l_costs_ni t_number_22_2_tab;
  BEGIN
    SELECT  CAST(COLLECT(amount ORDER BY order_by) AS t_number_22_2_tab),
            cast(collect(amount_ni order by order_by) as t_number_22_2_tab)
    INTO l_costs, l_costs_ni
    FROM (SELECT  sum(amount) as amount,
                  sum(amount_ni) as amount_ni,
                  order_by
          FROM (SELECT  -1 * (a_sales_type_info.fair_market_value + idc.amount) AS amount,
                        -1 * (a_sales_type_info.carrying_cost + idc.amount) as amount_ni,
                        1 AS order_by
                FROM (SELECT COALESCE(SUM(amount), 0) AS amount
                    FROM TABLE(a_initial_direct_costs)) idc
                UNION ALL
                SELECT  amount,
                        amount,
                        order_by
                FROM (SELECT payment_amount + CASE MONTH
                                                WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                                                  THEN a_sales_type_info.estimated_residual + 
                                                        a_sales_type_info.purchase_option_amount + 
                                                        a_sales_type_info.termination_amount
                                                  ELSE 0
                                                END as amount,
                        payment_amount,
                        row_number() OVER(ORDER BY MONTH) + (1 - is_prepay) AS order_by
                    FROM TABLE(a_payments)))
        group by order_by);

    return t_lsr_rates_implicit_in_lease(pkg_financial_calcs.f_irr(l_costs), pkg_financial_calcs.f_irr(l_costs_ni));
  END f_calc_rates_implicit_df;

  /*****************************************************************************
  * Function: f_get_override_rates_sales
  * PURPOSE: Looks up and return manual override rates for sales type ILRs
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to look for manually overriden rates
  *   a_revision: The revision of a_ilr_id for which to look for manually overriden rates
  *
  * RETURNS: Manually overriden rates
  ******************************************************************************/  
  FUNCTION f_get_override_rates_sales(a_ilr_id NUMBER,
                                      a_revision NUMBER) 
  RETURN t_lsr_rates_implicit_in_lease
  IS
    l_override_rates t_lsr_rates_implicit_in_lease;
  BEGIN
    --ilr_id/revision/rate_type_id/description is unique. 
    --Select MIN to get a NULL back when nothing exists (instead of no rows found execption)
    --Rates in lsr_ilr_rates are stored as "annualized" rates
    SELECT t_lsr_rates_implicit_in_lease(rate, rate)
    INTO l_override_rates
    FROM (SELECT MIN(pkg_lessor_schedule.f_annual_to_implicit_rate(rate)) AS rate
          FROM lsr_ilr_rates R
          JOIN lsr_ilr_rate_types T ON R.rate_type_id = T.rate_type_id
          WHERE R.ilr_id = a_ilr_id
          AND R.revision = a_revision
          AND LOWER(TRIM(T.DESCRIPTION)) = 'sales type discount rate override');

    return l_override_rates;
    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 AND -20000 THEN
          raise;
        ELSE
          raise_application_error(-20000, substr('Error checking for sales-type override rates - ' || sqlerrm || CHR(10) || f_get_call_stack, 1, 2000));
        END IF;
  END f_get_override_rates_sales;

  /*****************************************************************************
  * Function: f_get_override_rates_df
  * PURPOSE: Looks up and return manual override rates for direct-finance type ILRs
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to look for manually overriden rates
  *   a_revision: The revision of a_ilr_id for which to look for manually overriden rates
  *
  * RETURNS: Manually overriden rates
  ******************************************************************************/
  FUNCTION f_get_override_rates_df( a_ilr_id NUMBER,
                                    a_revision NUMBER) 
  RETURN t_lsr_rates_implicit_in_lease
  IS
    l_override_rates t_lsr_rates_implicit_in_lease := t_lsr_rates_implicit_in_lease(NULL, NULL);
  BEGIN
    --ilr_id/revision/rate_type_id/description is unique. 
    --Select MIN to get a NULL back when nothing exists (instead of no rows found execption)
    SELECT MIN(pkg_lessor_schedule.f_annual_to_implicit_rate(rate)) INTO l_override_rates.rate_implicit
    FROM lsr_ilr_rates R
    JOIN lsr_ilr_rate_types T ON R.rate_type_id = T.rate_type_id
    WHERE R.ilr_id = a_ilr_id
    AND R.revision = a_revision
    AND lower(trim(t.description)) = 'direct finance discount rate override';

    SELECT MIN(pkg_lessor_schedule.f_annual_to_implicit_rate(rate)) INTO l_override_rates.rate_implicit_ni
    FROM lsr_ilr_rates R
    JOIN lsr_ilr_rate_types T ON R.rate_type_id = T.rate_type_id
    WHERE R.ilr_id = a_ilr_id
    AND R.revision = a_revision
    AND lower(trim(t.description)) = 'direct finance interest on net inv rate override';

    RETURN l_override_rates;

    EXCEPTION
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 AND -20000 THEN
          raise;
        ELSE
          raise_application_error(-20000, substr('Error checking for direct-finance override rates - ' || sqlerrm || CHR(10) || f_get_call_stack, 1, 2000));
        END IF;
  END f_get_override_rates_df;

  /*****************************************************************************
  * Function: f_get_prelim_info_sales
  * PURPOSE: Looks up and return all preliminary info used in building a sales schedule
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to gather preliminary info
  *   a_revision: The revision of a_ilr_id for which to gather preliminary info
  *
  * RETURNS: All preliminary info used in schedule building process
  ******************************************************************************/
  FUNCTION f_get_prelim_info_sales( a_ilr_id NUMBER,
                                    a_revision NUMBER) 
  RETURN t_lsr_ilr_sales_df_prelims
  IS
    l_payment_info lsr_ilr_op_sch_pay_info_tab;
    l_override_rates t_lsr_rates_implicit_in_lease;
    l_calculated_rates t_lsr_rates_implicit_in_lease;
    l_rates_used t_lsr_rates_implicit_in_lease;
    l_initial_direct_costs lsr_init_direct_cost_info_tab;
    l_sales_type_info lsr_ilr_sales_sch_info;
  BEGIN

    l_payment_info := pkg_lessor_schedule.f_get_payment_info_from_terms(pkg_lessor_var_payments.f_get_initial_pay_terms(a_ilr_id, a_revision));
    l_initial_direct_costs := f_get_initial_direct_costs(a_ilr_id, a_revision);
    l_sales_type_info := f_get_sales_type_info(a_ilr_id, a_revision);
    l_calculated_rates := pkg_lessor_schedule.f_calc_rates_implicit_sales(f_get_payments_from_info(l_payment_info),
                                                                          l_initial_direct_costs,
                                                                          l_sales_type_info);
    l_override_rates := f_get_override_rates_sales(a_ilr_id, a_revision);

    --Rate implicit and rate implicit for ni should always the be the same for sales type
    IF l_override_rates.rate_implicit IS NOT NULL THEN
      l_rates_used := t_lsr_rates_implicit_in_lease(l_override_rates.rate_implicit, l_override_rates.rate_implicit);
    ELSE
      l_rates_used := t_lsr_rates_implicit_in_lease(l_calculated_rates.rate_implicit, l_calculated_rates.rate_implicit);
    END IF;

    return t_lsr_ilr_sales_df_prelims(l_payment_info, 
                                      t_lsr_ilr_schedule_all_rates(l_calculated_rates, l_override_rates, l_rates_used),
                                      l_initial_direct_costs,
                                      l_sales_type_info);
  END f_get_prelim_info_sales;

  /*****************************************************************************
  * Function: f_get_prelim_info_df
  * PURPOSE: Looks up and return all preliminary info used in building DF schedule
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for which to gather preliminary info
  *   a_revision: The revision of a_ilr_id for which to gather preliminary info
  *
  * RETURNS All preliminary info used in schedule building process
  ******************************************************************************/
  FUNCTION f_get_prelim_info_df(a_ilr_id NUMBER,
                                a_revision NUMBER) 
  RETURN t_lsr_ilr_sales_df_prelims
  IS
    l_payment_info lsr_ilr_op_sch_pay_info_tab;
    l_override_rates t_lsr_rates_implicit_in_lease;
    l_calculated_rates t_lsr_rates_implicit_in_lease;
    l_rates_used t_lsr_rates_implicit_in_lease;
    l_initial_direct_costs lsr_init_direct_cost_info_tab;
    l_sales_type_info lsr_ilr_sales_sch_info;
  BEGIN
    l_payment_info := pkg_lessor_schedule.f_get_payment_info_from_terms(pkg_lessor_var_payments.f_get_initial_pay_terms(a_ilr_id, a_revision));
    l_initial_direct_costs := f_get_initial_direct_costs(a_ilr_id, a_revision);
    l_sales_type_info := f_get_sales_type_info(a_ilr_id, a_revision);
    l_override_rates := f_get_override_rates_df(a_ilr_id, a_revision);
    l_calculated_rates := pkg_lessor_schedule.f_calc_rates_implicit_df(f_get_payments_from_info(l_payment_info),
                                                                          l_initial_direct_costs,
                                                                          l_sales_type_info);

    l_rates_used := t_lsr_rates_implicit_in_lease(null, null);

    IF l_override_rates.rate_implicit IS NOT NULL THEN
      l_rates_used.rate_implicit := l_override_rates.rate_implicit;
    END IF;

    IF l_override_rates.rate_implicit_ni IS NOT NULL THEN
      l_rates_used.rate_implicit_ni := l_override_rates.rate_implicit_ni;
    END IF;

    IF l_override_rates.rate_implicit IS NULL THEN
      l_rates_used.rate_implicit := l_calculated_rates.rate_implicit;
    END IF;

    IF l_override_rates.rate_implicit_ni IS NULL THEN
      l_rates_used.rate_implicit_ni := l_calculated_rates.rate_implicit_ni;
    END IF;

    RETURN t_lsr_ilr_sales_df_prelims(l_payment_info, 
                                      t_lsr_ilr_schedule_all_rates(l_calculated_rates, l_override_rates, l_rates_used),
                                      l_initial_direct_costs,
                                      l_sales_type_info);
  END f_get_prelim_info_df;


  /*****************************************************************************
  * Function: f_build_op_schedule
  * PURPOSE: Builds the operating schedule for the given payment terms
  * PARAMETERS:
  *   a_payment_terms: The payment terms associated with this ILR. The schedule will
  *                     build for payment_month_frequency (i.e. the number of months between payments),
  *                               payment_term_start_date (i.e. the starting date of the payment term),
  *                               number_of_terms (i.e. the number of payments that will be made),
  *                               payment_amount (i.e. the amount of payment to apply)
  *                               is_prepay (0 = arrears / 1 = prepay)
  *   NOTE: Multiple payment terms can be defined. For example, for a 36 month, prepay monthly lease,
  *           with $500 payments in year 1, $550 payments in year 2, and $600 payments in year three,
  *           provide three payment terms (1, <year_1_start>, 12, 500, 1),
  *                                       (1, <year_2_start>, 12, 550, 1),
  *                                       (1, <year_3_start>, 12, 600, 1)
  *
  * RETURNS: Table with schedule results
  ******************************************************************************/
  FUNCTION f_build_op_schedule( a_payment_terms lsr_ilr_op_sch_pay_term_tab, 
                                a_initial_direct_costs lsr_init_direct_cost_info_tab) 
  RETURN lsr_ilr_op_sch_result_tab PIPELINED DETERMINISTIC
  IS
  BEGIN
    --Open implicit cursor for pipelined output
    FOR res IN (
      --Put payment term info into SQL table for use in SQL functions
      WITH payments
      AS (SELECT  payment_month_frequency,
                  payment_group, --Group months based on date payment takes place
                  month,
                  number_of_terms,
                  is_prepay,
                  iter,
                  payment_amount
          FROM TABLE(f_get_payments_from_info(f_get_payment_info_from_terms(a_payment_terms)))),
      initial_direct_costs
      as (SELECT  idc_group_id,
                  date_incurred,
                  amount,
                  DESCRIPTION
          FROM TABLE(a_initial_direct_costs)),
    	--Build values of income section of schedule
      income_info
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  number_of_terms,
                  is_prepay,
                  iter,
                  payment_amount AS interest_income_received,
                  --Add all amounts and divide by number of months to get the accrued income
                  SUM(payment_amount) OVER (PARTITION BY NULL) / (COUNT(1) OVER (PARTITION BY NULL)) AS interest_income_accrued,
                  --The spread is the amount over each group of months for a payment, divided by number of terms, divided by the frequency (based on schedule example)
                  (SUM(payment_amount) OVER (PARTITION BY payment_group) / number_of_terms) / payment_month_frequency AS interest_income_spread
          FROM payments),
      income_info_with_idc
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  number_of_terms,
                  is_prepay,
                  iter,
                  interest_income_received,
                  interest_income_accrued,
                  interest_income_spread,
                  idc.total_amount / COUNT(1) OVER (PARTITION BY NULL) AS initial_direct_cost
          FROM income_info
          CROSS JOIN (SELECT SUM(amount) AS total_amount
                      FROM initial_direct_costs) idc),
      income_info_penny_plug
      AS (SELECT  MONTH,
                  payment_month_frequency,
                  number_of_terms,
                  iter,
                  interest_income_received,
                  -- Do this rounding in the last period of the "term",
                  -- whether or not the lease is prepay, so use row_number instead of iter
                  CASE
                    WHEN MOD(row_number() OVER (ORDER BY MONTH), payment_month_frequency) = 0
                      THEN round(SUM(interest_income_accrued) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND CURRENT ROW) -
                                  --This will be null for monthly (nothing between 0 preceding and 1 preceding)
                                  COALESCE(SUM(round(interest_income_accrued, 2)) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND 1 PRECEDING), 0), 2)
                    ELSE round(interest_income_accrued, 2)
                  END AS interest_income_accrued,
                  -- Do this rounding in the last period of the "term",
                  -- whether or not the lease is prepay, so use row_number instead of iter
                  CASE
                    WHEN MOD(row_number() OVER (ORDER BY MONTH), payment_month_frequency) = 0
                      THEN round(SUM(interest_income_spread) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND CURRENT ROW) -
                                  --This will be null for monthly (nothing between 0 preceding and 1 preceding)
                                  COALESCE(SUM(round(interest_income_spread, 2)) OVER (ORDER BY MONTH ROWS BETWEEN (payment_month_frequency - 1) PRECEDING AND 1 PRECEDING), 0), 2)
                    ELSE round(interest_income_spread, 2)
                END AS interest_income_spread,
                CASE MONTH
                  WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    THEN round(SUM(initial_direct_cost) OVER (PARTITION BY NULL) - COALESCE(SUM(round(initial_direct_cost, 2)) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING),0), 2)
                    ELSE round(initial_direct_cost, 2)
                  END as initial_direct_cost
          FROM income_info_with_idc),
      income_info_final_plug
      AS (SELECT MONTH,
                 interest_income_received,
                 CASE MONTH
                  WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    THEN round(SUM(interest_income_received) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
                          COALESCE(SUM(round(interest_income_accrued, 2)) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING), 0), 2)
                  ELSE round(interest_income_accrued, 2)
                 END AS interest_income_accrued,
                 CASE MONTH
                  WHEN LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    THEN round(SUM(interest_income_received) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) -
                          COALESCE(SUM(round(interest_income_spread, 2)) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING), 0), 2)
                    ELSE round(interest_income_spread, 2)
                 END AS interest_income_spread,
                 initial_direct_cost
         FROM income_info_penny_plug),
      -- Build values for revenue section of schedule
      rev_info
      AS(SELECT month,
                interest_income_received,
                interest_income_accrued,
                interest_income_spread,
                interest_income_accrued - interest_income_spread AS deferred_rev,
                initial_direct_cost
      FROM income_info_final_plug),
      -- Build final schedule results
      schedule
      as (SELECT  month,
                  interest_income_received,
                  interest_income_accrued,
                  interest_income_spread,
                  --Beginning deferred revenue is the sum of deferred revenue from all previous months (first month will be null, convert to 0)
                  COALESCE(SUM(deferred_rev) OVER (ORDER BY month ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING), 0) AS begin_deferred_rev,
                  deferred_rev,
                  --Ending deferred revenue is the sum of deferred revenue from all previous months and the current month (first month will be null, convert to 0)
                  COALESCE(SUM(deferred_rev) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 0) AS end_deferred_rev,
                  --The beginning receivable is the total receviable (first sum), minus the amount received in all previous months (second sum)
                  SUM(interest_income_received) OVER (PARTITION BY NULL) -
                  coalesce(sum(interest_income_received) over (order by month rows between unbounded preceding and 1 preceding), 0)
                  AS begin_receivable,
                  --The ending receivable is the total receviable (first sum), minus the amount received in all previous months and this month(second sum)
                  SUM(interest_income_received) OVER (PARTITION BY NULL) -
                  COALESCE(SUM(interest_income_received) OVER (ORDER BY month ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 0)
                  AS end_receivable,
                  initial_direct_cost
          FROM rev_info),
        schedule_with_lt
        AS (SELECT  MONTH,
                    interest_income_received,
                    interest_income_accrued,
                    interest_income_spread,
                    begin_deferred_rev,
                    deferred_rev,
                    end_deferred_rev,
                    begin_receivable,
                    end_receivable,
                    LEAD(begin_receivable, 12, 0) OVER (ORDER BY MONTH) AS begin_lt_receivable,
                    LEAD(end_receivable, 12, 0) OVER (ORDER BY MONTH) AS end_lt_receivable,
                    initial_direct_cost
            FROM schedule)
      --Select final results
      SELECT  lsr_ilr_op_sch_result(sch.MONTH,
                                    sch.interest_income_received,
                                    sch.interest_income_accrued,
                                    sch.interest_income_spread,
                                    sch.begin_deferred_rev,
                                    sch.deferred_rev,
                                    sch.end_deferred_rev,
                                    sch.begin_receivable,
                                    sch.end_receivable,
                                    sch.begin_lt_receivable,
                                    sch.end_lt_receivable,
                                    sch.initial_direct_cost,
                                    buckets.executory_accrued_1,
                                    buckets.executory_accrued_2,
                                    buckets.executory_accrued_3,
                                    buckets.executory_accrued_4,
                                    buckets.executory_accrued_5,
                                    buckets.executory_accrued_6,
                                    buckets.executory_accrued_7,
                                    buckets.executory_accrued_8,
                                    buckets.executory_accrued_9,
                                    buckets.executory_accrued_10,
                                    buckets.executory_received_1,
                                    buckets.executory_received_2,
                                    buckets.executory_received_3,
                                    buckets.executory_received_4,
                                    buckets.executory_received_5,
                                    buckets.executory_received_6,
                                    buckets.executory_received_7,
                                    buckets.executory_received_8,
                                    buckets.executory_received_9,
                                    buckets.executory_received_10,
                                    buckets.contingent_accrued_1,
                                    buckets.contingent_accrued_2,
                                    buckets.contingent_accrued_3,
                                    buckets.contingent_accrued_4,
                                    buckets.contingent_accrued_5,
                                    buckets.contingent_accrued_6,
                                    buckets.contingent_accrued_7,
                                    buckets.contingent_accrued_8,
                                    buckets.contingent_accrued_9,
                                    buckets.contingent_accrued_10,
                                    buckets.contingent_received_1,
                                    buckets.contingent_received_2,
                                    buckets.contingent_received_3,
                                    buckets.contingent_received_4,
                                    buckets.contingent_received_5,
                                    buckets.contingent_received_6,
                                    buckets.contingent_received_7,
                                    buckets.contingent_received_8,
                                    buckets.contingent_received_9,
                                    buckets.contingent_received_10) AS sch_line
      FROM schedule_with_lt sch
      JOIN TABLE(f_calculate_buckets(f_get_payment_info_from_terms(a_payment_terms))) buckets on sch.month = buckets.month)
      LOOP
        --Pipe results to caller
        PIPE ROW (res.sch_line);
      END LOOP;
    EXCEPTION
      WHEN no_data_needed THEN
        RAISE; --Oracle uses NO_DATA_NEEDED to signal end of pipeline
      WHEN OTHERS THEN
        IF SQLCODE BETWEEN -20999 AND -20000 THEN
          raise;
        ELSE
          raise_application_error(-20000, substr('Error building lessor operating schedule - ' || sqlerrm || CHR(10) || f_get_call_stack, 1, 2000));
        END IF;
  END f_build_op_schedule;

  /*****************************************************************************
  * Function: f_get_op_schedule
  * PURPOSE: Builds and returns the operating schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule. The schedule will
  *             build for payment payment terms given in table lsr_ilr_payment_term
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule results (pipelined)
  ******************************************************************************/

  FUNCTION f_get_op_schedule(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_op_sch_result_tab PIPELINED
  IS
  BEGIN
    FOR res IN (SELECT  lsr_ilr_op_sch_result(MONTH,
                                              interest_income_received,
                                              interest_income_accrued,
                                              interest_rental_recvd_spread,
                                              begin_deferred_rev,
                                              deferred_rev,
                                              end_deferred_rev,
                                              begin_receivable,
                                              end_receivable,
                                              begin_lt_receivable,
                                              end_lt_receivable,
                                              initial_direct_cost,
                                              executory_accrual1,
                                              executory_accrual2,
                                              executory_accrual3,
                                              executory_accrual4,
                                              executory_accrual5,
                                              executory_accrual6,
                                              executory_accrual7,
                                              executory_accrual8,
                                              executory_accrual9,
                                              executory_accrual10,
                                              executory_paid1,
                                              executory_paid2,
                                              executory_paid3,
                                              executory_paid4,
                                              executory_paid5,
                                              executory_paid6,
                                              executory_paid7,
                                              executory_paid8,
                                              executory_paid9,
                                              executory_paid10,
                                              contingent_accrual1,
                                              contingent_accrual2,
                                              contingent_accrual3,
                                              contingent_accrual4,
                                              contingent_accrual5,
                                              contingent_accrual6,
                                              contingent_accrual7,
                                              contingent_accrual8,
                                              contingent_accrual9,
                                              contingent_accrual10,
                                              contingent_paid1,
                                              contingent_paid2,
                                              contingent_paid3,
                                              contingent_paid4,
                                              contingent_paid5,
                                              contingent_paid6,
                                              contingent_paid7,
                                              contingent_paid8,
                                              contingent_paid9,
                                              contingent_paid10) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_build_op_schedule(pkg_lessor_var_payments.f_get_initial_pay_terms(a_ilr_id, a_revision),
                                                                    f_get_initial_direct_costs(a_ilr_id, a_revision))))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
  END f_get_op_schedule;
  
  /*****************************************************************************
  * Function: f_get_op_sch_quash_exceptions
  * PURPOSE: Builds and returns the operating-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  ******************************************************************************/
  FUNCTION f_get_op_sch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_op_sch_result_tab PIPELINED
  IS
  BEGIN
    FOR res IN (SELECT  lsr_ilr_op_sch_result(MONTH,
                                              interest_income_received,
                                              interest_income_accrued,
                                              interest_rental_recvd_spread,
                                              begin_deferred_rev,
                                              deferred_rev,
                                              end_deferred_rev,
                                              begin_receivable,
                                              end_receivable,
                                              begin_lt_receivable,
                                              end_lt_receivable,
                                              initial_direct_cost,
                                              executory_accrual1,
                                              executory_accrual2,
                                              executory_accrual3,
                                              executory_accrual4,
                                              executory_accrual5,
                                              executory_accrual6,
                                              executory_accrual7,
                                              executory_accrual8,
                                              executory_accrual9,
                                              executory_accrual10,
                                              executory_paid1,
                                              executory_paid2,
                                              executory_paid3,
                                              executory_paid4,
                                              executory_paid5,
                                              executory_paid6,
                                              executory_paid7,
                                              executory_paid8,
                                              executory_paid9,
                                              executory_paid10,
                                              contingent_accrual1,
                                              contingent_accrual2,
                                              contingent_accrual3,
                                              contingent_accrual4,
                                              contingent_accrual5,
                                              contingent_accrual6,
                                              contingent_accrual7,
                                              contingent_accrual8,
                                              contingent_accrual9,
                                              contingent_accrual10,
                                              contingent_paid1,
                                              contingent_paid2,
                                              contingent_paid3,
                                              contingent_paid4,
                                              contingent_paid5,
                                              contingent_paid6,
                                              contingent_paid7,
                                              contingent_paid8,
                                              contingent_paid9,
                                              contingent_paid10) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_get_op_schedule(a_ilr_id, a_revision)))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
    EXCEPTION
      --Used to signal end of pipeline
      WHEN NO_DATA_NEEDED THEN
        RAISE;
      WHEN OTHERS THEN
        DECLARE
          l_kickout t_kickout;
        BEGIN
          l_kickout.ilr_id := a_ilr_id;
          l_kickout.revision := a_revision;
          l_kickout.message := 'Error building Operating Schedule: ' || sqlerrm;
          p_log_kickouts(t_kickout_tab(l_kickout));
        END;
  END f_get_op_sch_quash_exceptions;


  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_payment_info: The payment info to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_rates_implicit: The rates implicit to use in generating the schedule
  *   is_finance_type: Number indicating if the schedule is of the direct finance type (1) or not(0)
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_build_sales_schedule(a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                  a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                  a_sales_type_info lsr_ilr_sales_sch_info,
                                  a_rates_implicit t_lsr_rates_implicit_in_lease,
                                  is_finance_type NUMBER) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED DETERMINISTIC
  IS
  BEGIN
  FOR res in (
      WITH initial_direct_costs
      as (SELECT  idc_group_id,
                  date_incurred,
                  amount,
                  DESCRIPTION
          FROM TABLE(a_initial_direct_costs)),
      payment_extract
      as (SELECT  payment_month_frequency,
                  payment_group,
                  MONTH,
                  first_value(month) over (order by month rows between unbounded preceding and unbounded following) as first_month,
                  LAST_VALUE(month) over (order by month rows between unbounded preceding and unbounded following) as final_month,
                  number_of_terms,
                  is_prepay,
                  iter,
                  payment_amount,
                  CASE WHEN MOD(iter, payment_month_frequency) = 0 THEN 1 ELSE 0 END AS is_payment_month,
                  CASE is_prepay
                    WHEN 0 THEN LAST_VALUE(payment_amount) OVER (PARTITION BY payment_group ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                    WHEN 1 THEN FIRST_VALUE(payment_amount) OVER (PARTITION BY payment_group ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                  END AS group_fixed_payment
        FROM TABLE(pkg_lessor_schedule.f_get_payments_from_info(a_payment_info))),
      payments
      AS (SELECT  payment_month_frequency,
                  payment_group,
                  MONTH,
                  LAST_VALUE(CASE is_payment_month WHEN 1 THEN MONTH ELSE NULL END) IGNORE NULLS
                    OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) as prior_payment_month,      
                  CASE is_prepay
                    WHEN 0 THEN FIRST_VALUE(CASE is_payment_month WHEN 1 THEN MONTH ELSE NULL END) IGNORE NULLS
                            OVER (ORDER BY MONTH ROWS BETWEEN CURRENT ROW AND UNBOUNDED FOLLOWING)
                    WHEN 1 THEN FIRST_VALUE(CASE is_payment_month WHEN 1 THEN MONTH ELSE NULL END) IGNORE NULLS
                                  OVER(ORDER BY MONTH ROWS BETWEEN 1 FOLLOWING AND UNBOUNDED FOLLOWING)
                  END as accruing_payment_month,
                  first_month,
                  final_month,
                  number_of_terms,
                  is_prepay,
                  iter,
                  payment_amount,
                  is_payment_month,
                  group_fixed_payment
        FROM payment_extract),
      npv
      AS (SELECT  pkg_financial_calcs.f_npv(CAST(COLLECT(payment_amount ORDER BY order_by) AS t_number_22_2_tab), rate_implicit) AS npv_lease_payments,
                  pkg_financial_calcs.f_npv(CAST(COLLECT(guaranteed_residual ORDER BY order_by) AS t_number_22_2_tab), rate_implicit) AS npv_guaranteed_residual,
                  pkg_financial_calcs.f_npv(CAST(COLLECT((estimated_residual - guaranteed_residual) ORDER BY order_by) AS t_number_22_2_tab), rate_implicit) AS npv_unguaranteed_residual
          FROM (SELECT  SUM(payment_amount) AS payment_amount,
                        SUM(guaranteed_residual) AS guaranteed_residual,
                        SUM(estimated_residual) AS estimated_residual,
                        rate_implicit,
                        order_by
                FROM (SELECT   payment_amount + CASE MONTH
                                                WHEN final_month
                                                  THEN a_sales_type_info.purchase_option_amount + a_sales_type_info.termination_amount
                                                ELSE 0
                                              END AS payment_amount,
                              a_rates_implicit.rate_implicit as rate_implicit,
                              CASE MONTH
                                WHEN final_month
                                  THEN a_sales_type_info.guaranteed_residual
                                ELSE 0
                              END as guaranteed_residual,
                              CASE MONTH
                                WHEN final_month
                                  THEN a_sales_type_info.estimated_residual
                                ELSE 0
                              END AS estimated_residual,
                              row_number() OVER (ORDER BY MONTH) + (1 - is_prepay) as order_by
                      FROM payments
                      UNION ALL
                      SELECT  0 AS payment_amount,
                              a_rates_implicit.rate_implicit AS rate_implicit,
                              0 AS guaranteed_residual,
                              0 AS estimated_residual,
                              1 as order_by
                      FROM dual)
                GROUP BY rate_implicit, order_by)
            GROUP BY rate_implicit),
      sales_type_calc_info
      AS (SELECT  MONTH,
                  prior_payment_month,
                  accruing_payment_month,
                  first_month,
                  final_month,
                  payment_month_frequency,
                  payment_group,
                  number_of_terms,
                  is_prepay,
                  iter,
                  is_payment_month,
                  group_fixed_payment,
                  payment_amount as fixed_payment,
                  npv_lease_payments,
                  a_rates_implicit.rate_implicit AS rate_implicit,
                  pkg_lessor_schedule.f_implicit_to_annual_rate(a_rates_implicit.rate_implicit) as discount_rate,
                  a_rates_implicit.rate_implicit_ni AS rate_implicit_ni,
                  pkg_lessor_schedule.f_implicit_to_annual_rate(a_rates_implicit.rate_implicit_ni) AS discount_rate_ni,
                  a_sales_type_info.guaranteed_residual AS guaranteed_residual,
                  a_sales_type_info.purchase_option_amount AS purchase_option_amount,
                  a_sales_type_info.termination_amount as termination_amount,
                  npv_guaranteed_residual,
                  npv_lease_payments + npv_guaranteed_residual as initial_receivable,
                  npv_unguaranteed_residual,
                  LEAST(a_sales_type_info.fair_market_value, (npv_lease_payments + npv_guaranteed_residual)) -
                    (a_sales_type_info.carrying_cost - npv_unguaranteed_residual) -
                    CASE
                      WHEN a_sales_type_info.carrying_cost = a_sales_type_info.fair_market_value OR is_finance_type = 1
                        THEN idc.amount
                        ELSE 0
                    END AS selling_profit_loss,
                  a_sales_type_info.carrying_cost - (a_sales_type_info.estimated_residual - a_sales_type_info.guaranteed_residual) AS cost_of_goods_sold,
                  CASE
                    WHEN is_finance_type = 0 AND a_sales_type_info.fair_market_value_company_curr <> a_sales_type_info.carrying_cost_company_curr AND MONTH = first_month
                      THEN idc.amount
                    ELSE 0
                  END AS initial_direct_cost
          FROM payments
          CROSS JOIN (SELECT SUM(amount) AS amount
                      FROM initial_direct_costs) idc
          CROSS JOIN npv),
      sales_type_calc_info2
      AS (SELECT  MONTH,
                  prior_payment_month,
                  accruing_payment_month,
                  first_month,
                  final_month,
                  payment_month_frequency,
                  payment_group,
                  number_of_terms,
                  is_prepay,
                  iter,
                  is_payment_month,
                  group_fixed_payment,
                  fixed_payment,
                  npv_lease_payments,
                  rate_implicit,
                  discount_rate,
                  rate_implicit_ni,
                  discount_rate_ni,
                  guaranteed_residual,
                  purchase_option_amount,
                  termination_amount,
                  npv_guaranteed_residual,
                  initial_receivable,
                  npv_unguaranteed_residual,
                  initial_receivable + npv_unguaranteed_residual -  CASE
                                                                      WHEN is_finance_type = 1 
                                                                        AND selling_profit_loss > 0
                                                                        THEN selling_profit_loss
                                                                      ELSE 0
                                                                    END AS initial_net_investment,
                  selling_profit_loss,
                  cost_of_goods_sold,
                  initial_direct_cost
          FROM sales_type_calc_info),
      schedule_calc
      AS (SELECT  MONTH,
                  prior_payment_month,
                  accruing_payment_month,
                  first_month,
                  final_month,
                  payment_month_frequency,
                  iter,
                  payment_group,
                  number_of_terms,
                  is_prepay,
                  is_payment_month,
                  fixed_payment,
                  group_fixed_payment,
                  principal_received_rounded AS principal_received,
                  int_income_received_rounded AS interest_income_received,
                  int_income_accrued_rounded as interest_income_accrued,
                  principal_accrued,
                  begin_receivable,
                  end_receivable,
                  begin_lt_receivable,
                  end_lt_receivable,
                  begin_unguaranteed_residual,
                  int_on_unguaranteed_residual,
                  end_unguaranteed_residual,
                  begin_net_investment,
                  int_on_net_investment,
                  end_net_investment,
                  rate_implicit,
                  discount_rate,
                  rate_implicit_ni,
                  discount_rate_ni,
                  guaranteed_residual,
                  purchase_option_amount,
                  termination_amount,
                  initial_receivable,
                  npv_lease_payments,
                  npv_guaranteed_residual,
                  npv_unguaranteed_residual,
                  selling_profit_loss,
                  cost_of_goods_sold,
                  initial_direct_cost
        FROM sales_type_calc_info2
        MODEL 
        REFERENCE monthnum_ref ON ( SELECT month, row_number() over (order by month) as month_num
                                    FROM sales_type_calc_info2)
          DIMENSION BY(MONTH)
          MEASURES(month_num)
        MAIN main_model
          DIMENSION BY (row_number() OVER (ORDER BY MONTH) AS month_num)
          MEASURES (MONTH,
                    prior_payment_month,
                    accruing_payment_month,
                    first_month,
                    final_month,
                    payment_month_frequency,
                    iter,
                    payment_group,
                    number_of_terms,
                    is_prepay,
                    is_payment_month,
                    fixed_payment,
                    group_fixed_payment,
                    rate_implicit,
                    discount_rate,
                    rate_implicit_ni,
                    discount_rate_ni,
                    guaranteed_residual,
                    purchase_option_amount,
                    termination_amount,
                    initial_receivable,
                    initial_net_investment,
                    npv_lease_payments,
                    npv_guaranteed_residual,
                    npv_unguaranteed_residual,
                    selling_profit_loss,
                    cost_of_goods_sold,
                    initial_direct_cost,
                    0 AS interest_income_received,
                    0 AS int_income_received_rounded,
                    0 AS interest_income_accrued,
                    0 AS int_income_accrued_rounded,
                    0 AS principal_received,
                    0 AS principal_received_rounded,
                    0 as principal_accrued,
                    0 as begin_receivable,
                    0 AS end_receivable,
                    0 AS begin_lt_receivable,
                    0 as end_lt_receivable,
                    0 AS begin_unguaranteed_residual,
                    0 AS int_on_unguaranteed_residual,
                    0 AS end_unguaranteed_residual,
                    0 AS begin_net_investment,
                    0 AS int_on_net_investment,
                    0 AS end_net_investment,
                    0 as penny_plug,
                    COUNT(1) OVER (PARTITION BY NULL) AS cnt)
        RULES AUTOMATIC ORDER ( begin_receivable[1] = initial_receivable[1], --First receivable is the initial calculated from npv
                                begin_net_investment[1] = initial_net_investment[1],
                                begin_unguaranteed_residual[1] = npv_unguaranteed_residual[1],
                                begin_net_investment[month_num > 1] ORDER BY month_num = end_net_investment[cv() - 1],
                                begin_unguaranteed_residual[month_num > 1] ORDER BY month_num = end_unguaranteed_residual[cv() - 1],
                                interest_income_received[month_num] ORDER BY month_num =  CASE
                                                                                            WHEN is_payment_month[cv()] = 0 and cnt[cv()] <> cv(month_num)
                                                                                              THEN 0
                                                                                            WHEN is_prepay[cv()] = 0
                                                                                              THEN COALESCE(SUM(interest_income_accrued)[month_num BETWEEN coalesce(monthnum_ref.month_num[prior_payment_month[cv()]], 0) + 1 AND cv()], 0)
                                                                                            WHEN is_prepay[cv()] = 1
                                                                                              THEN COALESCE(SUM(interest_income_accrued)[month_num BETWEEN monthnum_ref.month_num[prior_payment_month[cv()]] AND cv() - 1], 0)
                                                                                          END,
                                int_income_received_rounded[month_num] ORDER BY month_num = interest_income_received[cv()] - 
                                                                                                  CASE
                                                                                                WHEN (is_prepay[cv()] = 0 AND cv(month_num) = cnt[cv()])
                                                                                                  OR (is_prepay[cv()] = 1 AND (cv(month_num) = cnt[cv()] - 1 OR cnt[cv()] = 1))
                                                                                                    THEN penny_plug[cv()]
                                                                                                ELSE 0
                                                                                              END,
                                interest_income_accrued[month_num] ORDER BY month_num = ROUND(rate_implicit[cv()] * CASE is_prepay[cv()]
                                                                                                                WHEN 0 THEN begin_receivable[cv()]
                                                                                                                WHEN 1 THEN CASE cv(month_num)
                                                                                                                              WHEN cnt[cv()] THEN 0
                                                                                                                              ELSE begin_receivable[cv()] - fixed_payment[cv()]
                                                                                                                            END
                                                                                                              END,
                                                                                              2),
                                int_income_accrued_rounded[month_num] order by month = interest_income_accrued[cv()] - 
                                                                                              CASE
                                                                                                WHEN (is_prepay[cv()] = 0 AND cv(month_num) = cnt[cv()])
                                                                                                  OR (is_prepay[cv()] = 1 AND (cv(month_num) = cnt[cv()] - 1 OR cnt[cv()] = 1))
                                                                                                    THEN penny_plug[cv()]
                                                                                                ELSE 0
                                                                                              END,
                                principal_received[month_num] ORDER BY month_num =  CASE
                                                                                      WHEN is_prepay[cv()] = 1 AND cv(month_num) = 1
                                                                                        THEN fixed_payment[cv()]
                                                                                      ELSE fixed_payment[cv()] - interest_income_received[cv()]
                                                                                    END,
                                principal_received_rounded[month_num] ORDER BY month_num = principal_received[cv()] + 
                                                                                            CASE
                                                                                            WHEN (is_prepay[cv()] = 0 AND cv(month_num) = cnt[cv()])
                                                                                                  OR (is_prepay[cv()] = 1 AND (cv(month_num) = cnt[cv()] - 1 OR cnt[cv()] = 1))
                                                                                              THEN penny_plug[cv()]
                                                                                            ELSE 0
                                                                                          END,
                                principal_accrued[month_num] ORDER BY month_num = ROUND(COALESCE(principal_received[monthnum_ref.month_num[accruing_payment_month[cv()]]], 0) 
                                                                                          / payment_month_frequency[cv()],
                                                                                        2) -  CASE
                                                                                                WHEN is_prepay[cv()] = 0 AND cv(month_num) = cnt[cv()]
                                                                                                  THEN purchase_option_amount[cv()] + termination_amount[cv()]
                                                                                                ELSE 0
                                                                                              END,
                                begin_receivable[month_num > 1] ORDER BY month_num = end_receivable[cv() - 1],
                                end_receivable[month_num] ORDER BY month_num = ROUND(begin_receivable[cv()] - 
                                                                                      fixed_payment[cv()] + 
                                                                                      int_income_accrued_rounded[cv()] -
                                                                                      CASE
                                                                                        WHEN cnt[cv()] = cv(month_num) THEN
                                                                                          purchase_option_amount[cv()] + termination_amount[cv()]
                                                                                        ELSE 0
                                                                                      END,
                                                                                    2),
                                begin_lt_receivable[month_num] = COALESCE(begin_receivable[cv() + 12], 0),
                                end_lt_receivable[month_num] = COALESCE(end_receivable[cv() + 12], 0),
                                int_on_net_investment[month_num] ORDER BY month_num = CASE
                                                                                        WHEN is_prepay[cv()] = 1 AND cv(month_num) = 1 THEN 0
                                                                                        ELSE begin_net_investment[cv()] * rate_implicit_ni[cv()]
                                                                                      END,
                                end_net_investment[month_num] ORDER BY month_num = begin_net_investment[cv()] -
                                                                                    fixed_payment[cv()] +
                                                                                    int_on_net_investment[cv()] -
                                                                                    CASE
                                                                                      WHEN cnt[cv()] = cv(month_num) THEN
                                                                                        purchase_option_amount[cv()] + termination_amount[cv()]
                                                                                      ELSE 0
                                                                                    END,
                                int_on_unguaranteed_residual[month_num] ORDER BY month_num =  CASE
                                                                                                WHEN is_prepay[cv()] = 1 AND cv(month_num) = 1 THEN 0
                                                                                                ELSE begin_unguaranteed_residual[cv()] * rate_implicit[cv()]
                                                                                              END,
                                end_unguaranteed_residual[month_num] ORDER BY month_num = begin_unguaranteed_residual[cv()] + int_on_unguaranteed_residual[cv()],
                                penny_plug[month_num] = round(begin_receivable[cv()], 2) -
                                                          (round(fixed_payment[cv()], 2) * CASE is_prepay[cv()] WHEN 0 THEN 1 WHEN 1 THEN 2 END) -
                                                          CASE 
                                                            WHEN cv(month_num) = cnt[cv()] THEN purchase_option_amount[cv()] + termination_amount[cv()]
                                                            ELSE 0
                                                          END +
                                                          round(interest_income_accrued[cv()], 2) -
                                                          round(guaranteed_residual[cv()], 2) -
                                                          round(purchase_option_amount[cv()], 2) -
                                                          round(termination_amount[cv()], 2)))
      SELECT  lsr_ilr_sales_sch_result( sch.MONTH,
                                        sch.principal_received,
                                        sch.interest_income_received,
                                        sch.interest_income_accrued,
                                        sch.principal_accrued,
                                        sch.begin_receivable,
                                        sch.end_receivable,
                                        sch.begin_lt_receivable,
                                        sch.end_lt_receivable,
                                        sch.initial_direct_cost,
                                        buckets.executory_accrued_1,
                                        buckets.executory_accrued_2,
                                        buckets.executory_accrued_3,
                                        buckets.executory_accrued_4,
                                        buckets.executory_accrued_5,
                                        buckets.executory_accrued_6,
                                        buckets.executory_accrued_7,
                                        buckets.executory_accrued_8,
                                        buckets.executory_accrued_9,
                                        buckets.executory_accrued_10,
                                        buckets.executory_received_1,
                                        buckets.executory_received_2,
                                        buckets.executory_received_3,
                                        buckets.executory_received_4,
                                        buckets.executory_received_5,
                                        buckets.executory_received_6,
                                        buckets.executory_received_7,
                                        buckets.executory_received_8,
                                        buckets.executory_received_9,
                                        buckets.executory_received_10,
                                        buckets.contingent_accrued_1,
                                        buckets.contingent_accrued_2,
                                        buckets.contingent_accrued_3,
                                        buckets.contingent_accrued_4,
                                        buckets.contingent_accrued_5,
                                        buckets.contingent_accrued_6,
                                        buckets.contingent_accrued_7,
                                        buckets.contingent_accrued_8,
                                        buckets.contingent_accrued_9,
                                        buckets.contingent_accrued_10,
                                        buckets.contingent_received_1,
                                        buckets.contingent_received_2,
                                        buckets.contingent_received_3,
                                        buckets.contingent_received_4,
                                        buckets.contingent_received_5,
                                        buckets.contingent_received_6,
                                        buckets.contingent_received_7,
                                        buckets.contingent_received_8,
                                        buckets.contingent_received_9,
                                        buckets.contingent_received_10,
                                        sch.begin_unguaranteed_residual,
                                        sch.int_on_unguaranteed_residual,
                                        sch.end_unguaranteed_residual,
                                        sch.begin_net_investment,
                                        sch.int_on_net_investment,
                                        sch.end_net_investment,
                                        sch.rate_implicit,
                                        sch.discount_rate,
                                        sch.rate_implicit_ni,
                                        sch.discount_rate_ni,
                                        sch.initial_receivable,
                                        sch.npv_lease_payments,
                                        sch.npv_guaranteed_residual,
                                        sch.npv_unguaranteed_residual,
                                        sch.selling_profit_loss,
                                        sch.cost_of_goods_sold,
                                        t_lsr_ilr_schedule_all_rates(a_rates_implicit, a_rates_implicit, a_rates_implicit)) AS sch_line
      FROM schedule_calc sch
      JOIN TABLE(f_calculate_buckets(a_payment_info)) buckets ON sch.MONTH = buckets.MONTH)
    LOOP
      PIPE ROW (res.sch_line);
    END LOOP;
  end f_build_sales_schedule;

  /*****************************************************************************
  * Function: f_build_sales_schedule
  * PURPOSE: Builds the sales-type schedule for the given payment terms and sales-type-specific information
  * PARAMETERS:
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_rates_implicit: The rates to use in building the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_build_sales_schedule(a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                  a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                  a_sales_type_info lsr_ilr_sales_sch_info,
                                  a_rates_implicit t_lsr_rates_implicit_in_lease) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED DETERMINISTIC
  IS
  BEGIN

    FOR res IN (SELECT  lsr_ilr_sales_sch_result( MONTH,
                                                  principal_received,
                                                  interest_income_received,
                                                  interest_income_accrued,
                                                  principal_accrued,
                                                  begin_receivable,
                                                  end_receivable,
                                                  begin_lt_receivable,
                                                  end_lt_receivable,
                                                  initial_direct_cost,
                                                  executory_accrual1,
                                                  executory_accrual2,
                                                  executory_accrual3,
                                                  executory_accrual4,
                                                  executory_accrual5,
                                                  executory_accrual6,
                                                  executory_accrual7,
                                                  executory_accrual8,
                                                  executory_accrual9,
                                                  executory_accrual10,
                                                  executory_paid1,
                                                  executory_paid2,
                                                  executory_paid3,
                                                  executory_paid4,
                                                  executory_paid5,
                                                  executory_paid6,
                                                  executory_paid7,
                                                  executory_paid8,
                                                  executory_paid9,
                                                  executory_paid10,
                                                  contingent_accrual1,
                                                  contingent_accrual2,
                                                  contingent_accrual3,
                                                  contingent_accrual4,
                                                  contingent_accrual5,
                                                  contingent_accrual6,
                                                  contingent_accrual7,
                                                  contingent_accrual8,
                                                  contingent_accrual9,
                                                  contingent_accrual10,
                                                  contingent_paid1,
                                                  contingent_paid2,
                                                  contingent_paid3,
                                                  contingent_paid4,
                                                  contingent_paid5,
                                                  contingent_paid6,
                                                  contingent_paid7,
                                                  contingent_paid8,
                                                  contingent_paid9,
                                                  contingent_paid10,
                                                  begin_unguaranteed_residual,
                                                  int_on_unguaranteed_residual,
                                                  end_unguaranteed_residual,
                                                  begin_net_investment,
                                                  int_on_net_investment,
                                                  end_net_investment,
                                                  rate_implicit,
                                                  discount_rate,
                                                  rate_implicit_ni,
                                                  discount_rate,
                                                  begin_lease_receivable,
                                                  npv_lease_payments,
                                                  npv_guaranteed_residual,
                                                  npv_unguaranteed_residual,
                                                  selling_profit_loss,
                                                  cost_of_goods_sold,
                                                  schedule_rates) AS sch_line
                FROM TABLE(f_build_sales_schedule(a_payment_info,
                                                  a_initial_direct_costs,
                                                  a_sales_type_info,
                                                  a_rates_implicit,
                                                  0)))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
  END f_build_sales_schedule;

  /*****************************************************************************
  * Function: f_build_df_schedule
  * PURPOSE: Builds the direct-finance-type schedule for the given payment terms and 
              sales-type (also used for direct finance) information
  * PARAMETERS:
  *   a_payment_terms: The payment terms to use in generating the schedule
  *   a_initial_direct_costs: The initial direct costs associated with the ILR
  *   a_sales_type_info: The sales-type-specific info to use in generating the schedule.
  *   a_rates_implicit: The rates to use in building the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  function f_build_df_schedule( a_payment_info lsr_ilr_op_sch_pay_info_tab,
                                a_initial_direct_costs lsr_init_direct_cost_info_tab,
                                a_sales_type_info lsr_ilr_sales_sch_info,
                                a_rates_implicit t_lsr_rates_implicit_in_lease) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED DETERMINISTIC
  IS
  BEGIN
    for res IN (WITH sch
                AS (SELECT  MONTH,
                            principal_received,
                            interest_income_received,
                            interest_income_accrued,
                            principal_accrued,
                            begin_receivable,
                            end_receivable,
                            begin_lt_receivable,
                            end_lt_receivable,
                            initial_direct_cost,
                            executory_accrual1,
                            executory_accrual2,
                            executory_accrual3,
                            executory_accrual4,
                            executory_accrual5,
                            executory_accrual6,
                            executory_accrual7,
                            executory_accrual8,
                            executory_accrual9,
                            executory_accrual10,
                            executory_paid1,
                            executory_paid2,
                            executory_paid3,
                            executory_paid4,
                            executory_paid5,
                            executory_paid6,
                            executory_paid7,
                            executory_paid8,
                            executory_paid9,
                            executory_paid10,
                            contingent_accrual1,
                            contingent_accrual2,
                            contingent_accrual3,
                            contingent_accrual4,
                            contingent_accrual5,
                            contingent_accrual6,
                            contingent_accrual7,
                            contingent_accrual8,
                            contingent_accrual9,
                            contingent_accrual10,
                            contingent_paid1,
                            contingent_paid2,
                            contingent_paid3,
                            contingent_paid4,
                            contingent_paid5,
                            contingent_paid6,
                            contingent_paid7,
                            contingent_paid8,
                            contingent_paid9,
                            contingent_paid10,
                            begin_unguaranteed_residual,
                            int_on_unguaranteed_residual,
                            end_unguaranteed_residual,
                            begin_net_investment,
                            int_on_net_investment,
                            end_net_investment,
                            rate_implicit,
                            discount_rate,
                            rate_implicit_ni,
                            discount_rate_ni,
                            begin_lease_receivable,
                            npv_lease_payments,
                            npv_guaranteed_residual,
                            npv_unguaranteed_residual,
                            selling_profit_loss,
                            cost_of_goods_sold
                    FROM TABLE(pkg_lessor_schedule.f_build_sales_schedule(a_payment_info,
                                                                          a_initial_direct_costs,
                                                                          a_sales_type_info,
                                                                          a_rates_implicit,
                                                                          1))),
                sch_df
                AS (SELECT  MONTH,
                            principal_received,
                            interest_income_received,
                            interest_income_accrued,
                            principal_accrued,
                            begin_receivable,
                            end_receivable,
                            begin_lt_receivable,
                            end_lt_receivable,
                            initial_direct_cost,
                            executory_accrual1,
                            executory_accrual2,
                            executory_accrual3,
                            executory_accrual4,
                            executory_accrual5,
                            executory_accrual6,
                            executory_accrual7,
                            executory_accrual8,
                            executory_accrual9,
                            executory_accrual10,
                            executory_paid1,
                            executory_paid2,
                            executory_paid3,
                            executory_paid4,
                            executory_paid5,
                            executory_paid6,
                            executory_paid7,
                            executory_paid8,
                            executory_paid9,
                            executory_paid10,
                            contingent_accrual1,
                            contingent_accrual2,
                            contingent_accrual3,
                            contingent_accrual4,
                            contingent_accrual5,
                            contingent_accrual6,
                            contingent_accrual7,
                            contingent_accrual8,
                            contingent_accrual9,
                            contingent_accrual10,
                            contingent_paid1,
                            contingent_paid2,
                            contingent_paid3,
                            contingent_paid4,
                            contingent_paid5,
                            contingent_paid6,
                            contingent_paid7,
                            contingent_paid8,
                            contingent_paid9,
                            contingent_paid10,
                            begin_unguaranteed_residual,
                            int_on_unguaranteed_residual,
                            end_unguaranteed_residual,
                            begin_net_investment,
                            int_on_net_investment,
                            end_net_investment,
                            CASE
                              WHEN selling_profit_loss > 0
                                THEN  CASE
                                        WHEN is_prepay = 1 and month <> first_value(month) over (order by month rows between unbounded preceding and unbounded following)
                                          THEN int_on_net_investment - LAG(interest_income_accrued, 1) OVER (ORDER BY MONTH) - int_on_unguaranteed_residual
                                        WHEN is_prepay = 0
                                          THEN int_on_net_investment - interest_income_accrued - int_on_unguaranteed_residual
                                        ELSE 0
                                    END
                              ELSE 0
                            END as recognized_profit,
                            rate_implicit,
                            discount_rate,
                            rate_implicit_ni,
                            discount_rate_ni,
                            begin_lease_receivable,
                            npv_lease_payments,
                            npv_guaranteed_residual,
                            npv_unguaranteed_residual,
                            selling_profit_loss,
                            cost_of_goods_sold
                    FROM sch
                    CROSS JOIN (SELECT DISTINCT is_prepay
                                FROM TABLE(a_payment_info))),
                sch_df_profits
                AS (SELECT  MONTH,
                            principal_received,
                            interest_income_received,
                            interest_income_accrued,
                            principal_accrued,
                            begin_receivable,
                            end_receivable,
                            begin_lt_receivable,
                            end_lt_receivable,
                            initial_direct_cost,
                            executory_accrual1,
                            executory_accrual2,
                            executory_accrual3,
                            executory_accrual4,
                            executory_accrual5,
                            executory_accrual6,
                            executory_accrual7,
                            executory_accrual8,
                            executory_accrual9,
                            executory_accrual10,
                            executory_paid1,
                            executory_paid2,
                            executory_paid3,
                            executory_paid4,
                            executory_paid5,
                            executory_paid6,
                            executory_paid7,
                            executory_paid8,
                            executory_paid9,
                            executory_paid10,
                            contingent_accrual1,
                            contingent_accrual2,
                            contingent_accrual3,
                            contingent_accrual4,
                            contingent_accrual5,
                            contingent_accrual6,
                            contingent_accrual7,
                            contingent_accrual8,
                            contingent_accrual9,
                            contingent_accrual10,
                            contingent_paid1,
                            contingent_paid2,
                            contingent_paid3,
                            contingent_paid4,
                            contingent_paid5,
                            contingent_paid6,
                            contingent_paid7,
                            contingent_paid8,
                            contingent_paid9,
                            contingent_paid10,
                            begin_unguaranteed_residual,
                            int_on_unguaranteed_residual,
                            end_unguaranteed_residual,
                            begin_net_investment,
                            int_on_net_investment,
                            end_net_investment,
                            CASE
                              WHEN selling_profit_loss <= 0
                                THEN 0
                                ELSE selling_profit_loss - COALESCE(SUM(recognized_profit) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING), 0)
                            END AS begin_deferred_profit,
                            recognized_profit,
                            CASE
                              WHEN selling_profit_loss <= 0
                                THEN 0
                              ELSE selling_profit_loss - SUM(recognized_profit) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW)
                            END as end_deferred_profit,
                            rate_implicit,
                            discount_rate,
                            rate_implicit_ni,
                            discount_rate_ni,
                            begin_lease_receivable,
                            npv_lease_payments,
                            npv_guaranteed_residual,
                            npv_unguaranteed_residual,
                            selling_profit_loss,
                            cost_of_goods_sold
                    FROM sch_df),
                sch_df_round
                AS (SELECT  MONTH,
                            principal_received,
                            interest_income_received,
                            interest_income_accrued,
                            principal_accrued,
                            begin_receivable,
                            end_receivable,
                            begin_lt_receivable,
                            end_lt_receivable,
                            initial_direct_cost,
                            executory_accrual1,
                            executory_accrual2,
                            executory_accrual3,
                            executory_accrual4,
                            executory_accrual5,
                            executory_accrual6,
                            executory_accrual7,
                            executory_accrual8,
                            executory_accrual9,
                            executory_accrual10,
                            executory_paid1,
                            executory_paid2,
                            executory_paid3,
                            executory_paid4,
                            executory_paid5,
                            executory_paid6,
                            executory_paid7,
                            executory_paid8,
                            executory_paid9,
                            executory_paid10,
                            contingent_accrual1,
                            contingent_accrual2,
                            contingent_accrual3,
                            contingent_accrual4,
                            contingent_accrual5,
                            contingent_accrual6,
                            contingent_accrual7,
                            contingent_accrual8,
                            contingent_accrual9,
                            contingent_accrual10,
                            contingent_paid1,
                            contingent_paid2,
                            contingent_paid3,
                            contingent_paid4,
                            contingent_paid5,
                            contingent_paid6,
                            contingent_paid7,
                            contingent_paid8,
                            contingent_paid9,
                            contingent_paid10,
                            begin_unguaranteed_residual,
                            int_on_unguaranteed_residual,
                            end_unguaranteed_residual,
                            begin_net_investment,
                            int_on_net_investment,
                            end_net_investment,
                            begin_deferred_profit,
                            CASE
                              WHEN MONTH = LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                                THEN round(recognized_profit,2) + (round(begin_deferred_profit,2) - round(recognized_profit,2))
                              ELSE recognized_profit
                            END as recognized_profit,
                            CASE
                              WHEN MONTH = LAST_VALUE(MONTH) OVER (ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                                THEN begin_deferred_profit - (round(recognized_profit, 2) + (round(begin_deferred_profit,2) - round(recognized_profit,2)))
                              ELSE end_deferred_profit
                            END as end_deferred_profit,
                            rate_implicit,
                            discount_rate,
                            rate_implicit_ni,
                            discount_rate_ni,
                            begin_lease_receivable,
                            npv_lease_payments,
                            npv_guaranteed_residual,
                            npv_unguaranteed_residual,
                            selling_profit_loss,
                            cost_of_goods_sold
                    FROM sch_df_profits)
                SELECT lsr_ilr_df_schedule_result(MONTH,
                                                  principal_received,
                                                  interest_income_received,
                                                  interest_income_accrued,
                                                  principal_accrued,
                                                  begin_receivable,
                                                  end_receivable,
                                                  begin_lt_receivable,
                                                  end_lt_receivable,
                                                  initial_direct_cost,
                                                  executory_accrual1,
                                                  executory_accrual2,
                                                  executory_accrual3,
                                                  executory_accrual4,
                                                  executory_accrual5,
                                                  executory_accrual6,
                                                  executory_accrual7,
                                                  executory_accrual8,
                                                  executory_accrual9,
                                                  executory_accrual10,
                                                  executory_paid1,
                                                  executory_paid2,
                                                  executory_paid3,
                                                  executory_paid4,
                                                  executory_paid5,
                                                  executory_paid6,
                                                  executory_paid7,
                                                  executory_paid8,
                                                  executory_paid9,
                                                  executory_paid10,
                                                  contingent_accrual1,
                                                  contingent_accrual2,
                                                  contingent_accrual3,
                                                  contingent_accrual4,
                                                  contingent_accrual5,
                                                  contingent_accrual6,
                                                  contingent_accrual7,
                                                  contingent_accrual8,
                                                  contingent_accrual9,
                                                  contingent_accrual10,
                                                  contingent_paid1,
                                                  contingent_paid2,
                                                  contingent_paid3,
                                                  contingent_paid4,
                                                  contingent_paid5,
                                                  contingent_paid6,
                                                  contingent_paid7,
                                                  contingent_paid8,
                                                  contingent_paid9,
                                                  contingent_paid10,
                                                  begin_unguaranteed_residual,
                                                  int_on_unguaranteed_residual,
                                                  end_unguaranteed_residual,
                                                  begin_net_investment,
                                                  int_on_net_investment,
                                                  end_net_investment,
                                                  begin_deferred_profit,
                                                  recognized_profit,
                                                  end_deferred_profit,
                                                  rate_implicit,
                                                  discount_rate,
                                                  rate_implicit_ni,
                                                  discount_rate_ni,
                                                  begin_lease_receivable,
                                                  npv_lease_payments,
                                                  npv_guaranteed_residual,
                                                  npv_unguaranteed_residual,
                                                  selling_profit_loss,
                                                  cost_of_goods_sold,
                                                  t_lsr_ilr_schedule_all_rates(a_rates_implicit, a_rates_implicit, a_rates_implicit)) AS sch_row
                FROM sch_df_round)
    LOOP
      PIPE ROW(res.sch_row);
    END LOOP;
  END f_build_df_schedule;

  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_prelims: Sales Type Preliminary Info to use in schedule calculation
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER, 
                                a_revision NUMBER, 
                                a_prelims t_lsr_ilr_sales_df_prelims)
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED
  IS
  BEGIN 
    FOR res IN (SELECT  lsr_ilr_sales_sch_result( MONTH,
                                                  principal_received,
                                                  interest_income_received,
                                                  interest_income_accrued,
                                                  principal_accrued,
                                                  begin_receivable,
                                                  end_receivable,
                                                  begin_lt_receivable,
                                                  end_lt_receivable,
                                                  initial_direct_cost,
                                                  executory_accrual1,
                                                  executory_accrual2,
                                                  executory_accrual3,
                                                  executory_accrual4,
                                                  executory_accrual5,
                                                  executory_accrual6,
                                                  executory_accrual7,
                                                  executory_accrual8,
                                                  executory_accrual9,
                                                  executory_accrual10,
                                                  executory_paid1,
                                                  executory_paid2,
                                                  executory_paid3,
                                                  executory_paid4,
                                                  executory_paid5,
                                                  executory_paid6,
                                                  executory_paid7,
                                                  executory_paid8,
                                                  executory_paid9,
                                                  executory_paid10,
                                                  contingent_accrual1,
                                                  contingent_accrual2,
                                                  contingent_accrual3,
                                                  contingent_accrual4,
                                                  contingent_accrual5,
                                                  contingent_accrual6,
                                                  contingent_accrual7,
                                                  contingent_accrual8,
                                                  contingent_accrual9,
                                                  contingent_accrual10,
                                                  contingent_paid1,
                                                  contingent_paid2,
                                                  contingent_paid3,
                                                  contingent_paid4,
                                                  contingent_paid5,
                                                  contingent_paid6,
                                                  contingent_paid7,
                                                  contingent_paid8,
                                                  contingent_paid9,
                                                  contingent_paid10,
                                                  begin_unguaranteed_residual,
                                                  int_on_unguaranteed_residual,
                                                  end_unguaranteed_residual,
                                                  begin_net_investment,
                                                  int_on_net_investment,
                                                  end_net_investment,
                                                  rate_implicit,
                                                  discount_rate,
                                                  rate_implicit_ni,
                                                  discount_rate_ni,
                                                  begin_lease_receivable,
                                                  npv_lease_payments,
                                                  npv_guaranteed_residual,
                                                  npv_unguaranteed_residual,
                                                  selling_profit_loss,
                                                  cost_of_goods_sold,
                                                  a_prelims.rates) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_build_sales_schedule(a_prelims.payment_info,
                                                                      a_prelims.initial_direct_costs,
                                                                      a_prelims.sales_type_info,
                                                                      a_prelims.rates.rates_used)))
    LOOP
      pipe row (res.sch_line);
    END LOOP;
  END f_get_sales_schedule;

  /*****************************************************************************
  * Function: f_get_sales_schedule
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_get_sales_schedule(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED
  IS
  BEGIN 
    FOR res IN (SELECT  lsr_ilr_sales_sch_result( MONTH,
                                                  principal_received,
                                                  interest_income_received,
                                                  interest_income_accrued,
                                                  principal_accrued,
                                                  begin_receivable,
                                                  end_receivable,
                                                  begin_lt_receivable,
                                                  end_lt_receivable,
                                                  initial_direct_cost,
                                                  executory_accrual1,
                                                  executory_accrual2,
                                                  executory_accrual3,
                                                  executory_accrual4,
                                                  executory_accrual5,
                                                  executory_accrual6,
                                                  executory_accrual7,
                                                  executory_accrual8,
                                                  executory_accrual9,
                                                  executory_accrual10,
                                                  executory_paid1,
                                                  executory_paid2,
                                                  executory_paid3,
                                                  executory_paid4,
                                                  executory_paid5,
                                                  executory_paid6,
                                                  executory_paid7,
                                                  executory_paid8,
                                                  executory_paid9,
                                                  executory_paid10,
                                                  contingent_accrual1,
                                                  contingent_accrual2,
                                                  contingent_accrual3,
                                                  contingent_accrual4,
                                                  contingent_accrual5,
                                                  contingent_accrual6,
                                                  contingent_accrual7,
                                                  contingent_accrual8,
                                                  contingent_accrual9,
                                                  contingent_accrual10,
                                                  contingent_paid1,
                                                  contingent_paid2,
                                                  contingent_paid3,
                                                  contingent_paid4,
                                                  contingent_paid5,
                                                  contingent_paid6,
                                                  contingent_paid7,
                                                  contingent_paid8,
                                                  contingent_paid9,
                                                  contingent_paid10,
                                                  begin_unguaranteed_residual,
                                                  int_on_unguaranteed_residual,
                                                  end_unguaranteed_residual,
                                                  begin_net_investment,
                                                  int_on_net_investment,
                                                  end_net_investment,
                                                  rate_implicit,
                                                  discount_rate,
                                                  rate_implicit_ni,
                                                  discount_rate_ni,
                                                  begin_lease_receivable,
                                                  npv_lease_payments,
                                                  npv_guaranteed_residual,
                                                  npv_unguaranteed_residual,
                                                  selling_profit_loss,
                                                  cost_of_goods_sold,
                                                  schedule_rates) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_get_sales_schedule(a_ilr_id,
                                                                    a_revision,
                                                                    f_get_prelim_info_sales(a_ilr_id, a_revision))))
    LOOP
      pipe row (res.sch_line);
    END LOOP;
  END f_get_sales_schedule;
  
  /*****************************************************************************
  * Function: f_getsalessch_quash_exceptions
  * PURPOSE: Builds and returns the sales-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  ******************************************************************************/
  FUNCTION f_getsalessch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_sales_sch_result_tab PIPELINED
  IS
  BEGIN 
    FOR res IN (SELECT  lsr_ilr_sales_sch_result( MONTH,
                                                  principal_received,
                                                  interest_income_received,
                                                  interest_income_accrued,
                                                  principal_accrued,
                                                  begin_receivable,
                                                  end_receivable,
                                                  begin_lt_receivable,
                                                  end_lt_receivable,
                                                  initial_direct_cost,
                                                  executory_accrual1,
                                                  executory_accrual2,
                                                  executory_accrual3,
                                                  executory_accrual4,
                                                  executory_accrual5,
                                                  executory_accrual6,
                                                  executory_accrual7,
                                                  executory_accrual8,
                                                  executory_accrual9,
                                                  executory_accrual10,
                                                  executory_paid1,
                                                  executory_paid2,
                                                  executory_paid3,
                                                  executory_paid4,
                                                  executory_paid5,
                                                  executory_paid6,
                                                  executory_paid7,
                                                  executory_paid8,
                                                  executory_paid9,
                                                  executory_paid10,
                                                  contingent_accrual1,
                                                  contingent_accrual2,
                                                  contingent_accrual3,
                                                  contingent_accrual4,
                                                  contingent_accrual5,
                                                  contingent_accrual6,
                                                  contingent_accrual7,
                                                  contingent_accrual8,
                                                  contingent_accrual9,
                                                  contingent_accrual10,
                                                  contingent_paid1,
                                                  contingent_paid2,
                                                  contingent_paid3,
                                                  contingent_paid4,
                                                  contingent_paid5,
                                                  contingent_paid6,
                                                  contingent_paid7,
                                                  contingent_paid8,
                                                  contingent_paid9,
                                                  contingent_paid10,
                                                  begin_unguaranteed_residual,
                                                  int_on_unguaranteed_residual,
                                                  end_unguaranteed_residual,
                                                  begin_net_investment,
                                                  int_on_net_investment,
                                                  end_net_investment,
                                                  rate_implicit,
                                                  discount_rate,
                                                  rate_implicit_ni,
                                                  discount_rate_ni,
                                                  begin_lease_receivable,
                                                  npv_lease_payments,
                                                  npv_guaranteed_residual,
                                                  npv_unguaranteed_residual,
                                                  selling_profit_loss,
                                                  cost_of_goods_sold,
                                                  schedule_rates) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_get_sales_schedule(a_ilr_id,
                                                                    a_revision)))
    LOOP
      pipe row (res.sch_line);
    END LOOP;
    EXCEPTION
      --Used to signal end of pipeline
      WHEN NO_DATA_NEEDED THEN
        RAISE;
      WHEN OTHERS THEN
        DECLARE
          l_kickout t_kickout;
        BEGIN
          l_kickout.ilr_id := a_ilr_id;
          l_kickout.revision := a_revision;
          l_kickout.message := 'Error building Sales Type Schedule: ' || sqlerrm;
          p_log_kickouts(t_kickout_tab(l_kickout));
        END;
  END f_getsalessch_quash_exceptions;

  /*****************************************************************************
  * Function: f_get_df_schedule
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *   a_prelims: Sales Type Preliminary Info to use in schedule calculation
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/
  FUNCTION f_get_df_schedule( a_ilr_id NUMBER, 
                              a_revision NUMBER, 
                              a_prelims t_lsr_ilr_sales_df_prelims) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED
  IS
  BEGIN
    FOR res IN (SELECT  lsr_ilr_df_schedule_result(MONTH,
                                                   principal_received,
                                                    interest_income_received,
                                                    interest_income_accrued,
                                                    principal_accrued,
                                                    begin_receivable,
                                                    end_receivable,
                                                    begin_lt_receivable,
                                                    end_lt_receivable,
                                                    initial_direct_cost,
                                                    executory_accrual1,
                                                    executory_accrual2,
                                                    executory_accrual3,
                                                    executory_accrual4,
                                                    executory_accrual5,
                                                    executory_accrual6,
                                                    executory_accrual7,
                                                    executory_accrual8,
                                                    executory_accrual9,
                                                    executory_accrual10,
                                                    executory_paid1,
                                                    executory_paid2,
                                                    executory_paid3,
                                                    executory_paid4,
                                                    executory_paid5,
                                                    executory_paid6,
                                                    executory_paid7,
                                                    executory_paid8,
                                                    executory_paid9,
                                                    executory_paid10,
                                                    contingent_accrual1,
                                                    contingent_accrual2,
                                                    contingent_accrual3,
                                                    contingent_accrual4,
                                                    contingent_accrual5,
                                                    contingent_accrual6,
                                                    contingent_accrual7,
                                                    contingent_accrual8,
                                                    contingent_accrual9,
                                                    contingent_accrual10,
                                                    contingent_paid1,
                                                    contingent_paid2,
                                                    contingent_paid3,
                                                    contingent_paid4,
                                                    contingent_paid5,
                                                    contingent_paid6,
                                                    contingent_paid7,
                                                    contingent_paid8,
                                                    contingent_paid9,
                                                    contingent_paid10,
                                                    begin_unguaranteed_residual,
                                                    int_on_unguaranteed_residual,
                                                    end_unguaranteed_residual,
                                                    begin_net_investment,
                                                    int_on_net_investment,
                                                    end_net_investment,
                                                    begin_deferred_profit,
                                                    recognized_profit,
                                                    end_deferred_profit,
                                                    rate_implicit,
                                                    discount_rate,
                                                    rate_implicit_ni,
                                                    discount_rate_ni,
                                                    begin_lease_receivable,
                                                    npv_lease_payments,
                                                    npv_guaranteed_residual,
                                                    npv_unguaranteed_residual,
                                                    selling_profit_loss,
                                                    cost_of_goods_sold,
                                                    a_prelims.rates) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_build_df_schedule( a_prelims.payment_info,
                                                                    a_prelims.initial_direct_costs,
                                                                    a_prelims.sales_type_info,
                                                                    a_prelims.rates.rates_used)))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
  END f_get_df_schedule;

  /*****************************************************************************
  * Function: f_get_df_schedule
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  ******************************************************************************/

  FUNCTION f_get_df_schedule(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED
  IS
  BEGIN
    FOR res IN (SELECT  lsr_ilr_df_schedule_result(MONTH,
                                                   principal_received,
                                                    interest_income_received,
                                                    interest_income_accrued,
                                                    principal_accrued,
                                                    begin_receivable,
                                                    end_receivable,
                                                    begin_lt_receivable,
                                                    end_lt_receivable,
                                                    initial_direct_cost,
                                                    executory_accrual1,
                                                    executory_accrual2,
                                                    executory_accrual3,
                                                    executory_accrual4,
                                                    executory_accrual5,
                                                    executory_accrual6,
                                                    executory_accrual7,
                                                    executory_accrual8,
                                                    executory_accrual9,
                                                    executory_accrual10,
                                                    executory_paid1,
                                                    executory_paid2,
                                                    executory_paid3,
                                                    executory_paid4,
                                                    executory_paid5,
                                                    executory_paid6,
                                                    executory_paid7,
                                                    executory_paid8,
                                                    executory_paid9,
                                                    executory_paid10,
                                                    contingent_accrual1,
                                                    contingent_accrual2,
                                                    contingent_accrual3,
                                                    contingent_accrual4,
                                                    contingent_accrual5,
                                                    contingent_accrual6,
                                                    contingent_accrual7,
                                                    contingent_accrual8,
                                                    contingent_accrual9,
                                                    contingent_accrual10,
                                                    contingent_paid1,
                                                    contingent_paid2,
                                                    contingent_paid3,
                                                    contingent_paid4,
                                                    contingent_paid5,
                                                    contingent_paid6,
                                                    contingent_paid7,
                                                    contingent_paid8,
                                                    contingent_paid9,
                                                    contingent_paid10,
                                                    begin_unguaranteed_residual,
                                                    int_on_unguaranteed_residual,
                                                    end_unguaranteed_residual,
                                                    begin_net_investment,
                                                    int_on_net_investment,
                                                    end_net_investment,
                                                    begin_deferred_profit,
                                                    recognized_profit,
                                                    end_deferred_profit,
                                                    rate_implicit,
                                                    discount_rate,
                                                    rate_implicit_ni,
                                                    discount_rate_ni,
                                                    begin_lease_receivable,
                                                    npv_lease_payments,
                                                    npv_guaranteed_residual,
                                                    npv_unguaranteed_residual,
                                                    selling_profit_loss,
                                                    cost_of_goods_sold,
                                                    schedule_rates) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_get_df_schedule( a_ilr_id,
                                                                  a_revision,
                                                                  f_get_prelim_info_df(a_ilr_id, a_revision))))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
  END f_get_df_schedule;
  
  /*****************************************************************************
  * Function: f_get_df_sch_quash_exceptions
  * PURPOSE: Builds and returns the direct-finance-type schedule for the given ILR/revision,
  *           logging exceptions and continuing instead of raising exceptions
  * PARAMETERS:
  *   a_ilr_id: The ILR ID for the ILR for which to build the schedule
  *   a_revision: The revision of the ILR for which to build the schedule
  *
  * RETURNS: Table with schedule (pipelined)
  * NOTES: Logs exceptions thrown using p_log_kickouts and continues normally on exception
  ******************************************************************************/

  FUNCTION f_get_df_sch_quash_exceptions(a_ilr_id NUMBER, a_revision NUMBER) 
  RETURN lsr_ilr_df_schedule_result_tab PIPELINED
  IS
  BEGIN
    FOR res IN (SELECT  lsr_ilr_df_schedule_result(MONTH,
                                                   principal_received,
                                                    interest_income_received,
                                                    interest_income_accrued,
                                                    principal_accrued,
                                                    begin_receivable,
                                                    end_receivable,
                                                    begin_lt_receivable,
                                                    end_lt_receivable,
                                                    initial_direct_cost,
                                                    executory_accrual1,
                                                    executory_accrual2,
                                                    executory_accrual3,
                                                    executory_accrual4,
                                                    executory_accrual5,
                                                    executory_accrual6,
                                                    executory_accrual7,
                                                    executory_accrual8,
                                                    executory_accrual9,
                                                    executory_accrual10,
                                                    executory_paid1,
                                                    executory_paid2,
                                                    executory_paid3,
                                                    executory_paid4,
                                                    executory_paid5,
                                                    executory_paid6,
                                                    executory_paid7,
                                                    executory_paid8,
                                                    executory_paid9,
                                                    executory_paid10,
                                                    contingent_accrual1,
                                                    contingent_accrual2,
                                                    contingent_accrual3,
                                                    contingent_accrual4,
                                                    contingent_accrual5,
                                                    contingent_accrual6,
                                                    contingent_accrual7,
                                                    contingent_accrual8,
                                                    contingent_accrual9,
                                                    contingent_accrual10,
                                                    contingent_paid1,
                                                    contingent_paid2,
                                                    contingent_paid3,
                                                    contingent_paid4,
                                                    contingent_paid5,
                                                    contingent_paid6,
                                                    contingent_paid7,
                                                    contingent_paid8,
                                                    contingent_paid9,
                                                    contingent_paid10,
                                                    begin_unguaranteed_residual,
                                                    int_on_unguaranteed_residual,
                                                    end_unguaranteed_residual,
                                                    begin_net_investment,
                                                    int_on_net_investment,
                                                    end_net_investment,
                                                    begin_deferred_profit,
                                                    recognized_profit,
                                                    end_deferred_profit,
                                                    rate_implicit,
                                                    discount_rate,
                                                    rate_implicit_ni,
                                                    discount_rate_ni,
                                                    begin_lease_receivable,
                                                    npv_lease_payments,
                                                    npv_guaranteed_residual,
                                                    npv_unguaranteed_residual,
                                                    selling_profit_loss,
                                                    cost_of_goods_sold,
                                                    schedule_rates) AS sch_line
                FROM TABLE(pkg_lessor_schedule.f_get_df_schedule( a_ilr_id,
                                                                  a_revision)))
    LOOP
      pipe row(res.sch_line);
    END LOOP;
    EXCEPTION
      --Used to signal end of pipeline
      WHEN NO_DATA_NEEDED THEN
        RAISE;
      WHEN OTHERS THEN
        DECLARE
          l_kickout t_kickout;
        BEGIN
          l_kickout.ilr_id := a_ilr_id;
          l_kickout.revision := a_revision;
          l_kickout.message := 'Error building Direct Finance Schedule: ' || sqlerrm;
          p_log_kickouts(t_kickout_tab(l_kickout));
        END;
  END f_get_df_sch_quash_exceptions;

  /*****************************************************************************
  * Function: f_annual_to_implicit_rate
  * PURPOSE: Converts from an "annualized" to the rate implicit
  * PARAMETERS:
  *   a_rate: The rate to convert
  * RETURNS: Converted rate
  ******************************************************************************/
  FUNCTION f_annual_to_implicit_rate(a_rate float) RETURN FLOAT DETERMINISTIC
  IS
  BEGIN
    RETURN POWER(1 + a_rate, 1/12) - 1;
  END f_annual_to_implicit_rate;

  /*****************************************************************************
  * Function: f_implicit_to_annual_rate
  * PURPOSE: Converts from the rate implicit to the "annualized" rate
  * PARAMETERS:
  *   a_rate: The rate to convert
  * RETURNS: Converted rate
  ******************************************************************************/
  FUNCTION f_implicit_to_annual_rate(a_rate FLOAT) RETURN FLOAT DETERMINISTIC
  IS
  BEGIN
    RETURN POWER(1 + a_rate, 12) - 1;
  END f_implicit_to_annual_rate;

END pkg_lessor_schedule;
/

--insert footer
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4784, 0, 2017, 3, 0, 0, 50891, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051017_03_lessor_add_package_version_script_prereq_ddl.sql', 1,
	sysdate, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
