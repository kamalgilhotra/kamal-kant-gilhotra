/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint-044726_lease_bring_queries_to_base.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2.0.0 8/17/2015  Will Davis       Add lease any queries to base
||============================================================================
*/


declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE OPEN MLA''S WITH NO ACTIVE ILR''S ');
delete from pp_any_query_criteria  where upper(description)='LEASE OPEN MLA''S WITH NO ACTIVE ILR''S ';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Open MLA''s with No Active ILR''s ', to_date('2015-08-12 10:39:49',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'none', 'lessee', 'user',
'select lls.description as lease_status, ll.LEASE_ID,ll.TIME_STAMP,ll.USER_ID,ll.LEASE_NUMBER,ll.DESCRIPTION,ll.LONG_DESCRIPTION,ll.LESSOR_ID,ll.LEASE_TYPE_ID,ll.LEASE_STATUS_ID,ll.APPROVAL_DATE,ll.MASTER_AGREEMENT_DATE,
ll.PAYMENT_DUE_DAY,ll.DAYS_IN_BASIS_YEAR,ll.PURCHASE_OPTION_NOTC,ll.PURCHASE_OPTION_PCT,ll.RENEWAL_OPTION_NOTIFICATION,ll.CANCEL_NOTIFICATION,ll.CANCEL_TERM,ll.LIMIT_ORIG_COST,ll.LIMIT_UNPAID_BAL,ll.ESTIMATED_PAYMENT,
ll.PRE_PAYMENT_SW,ll.MONTHS_FOR_LOCAL_TAX_BASE,ll.NOTES,ll.LEASE_GROUP_ID,ll.LEASE_CAP_TYPE_ID,ll.INITIATION_DATE,ll.CURRENT_REVISION,ll.WORKFLOW_TYPE_ID,ll.DAYS_IN_YEAR,ll.CUT_OFF_DAY,ll.DAYS_IN_MONTH_SW,ll.LEASE_END_DATE
from ls_lease ll, ls_lease_status lls
where ll.lease_status_id = lls.lease_status_id
  and ll.lease_status_id in (3,4,5)
  and not exists (select 1
                  from ls_ilr ilr
                  where ilr.lease_id = ll.lease_id
                    and ilr_status_id <> 3)
  ',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'approval_date', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Approval Date', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancel_notification', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancel Notification', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancel_term', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancel Term', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_revision', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Current Revision', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cut_off_day', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cut Off Day', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_basis_year', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Basis Year', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_month_sw', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Month Sw', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_year', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Year', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'description', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'estimated_payment', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Estimated Payment', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'initiation_date', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Initiation Date', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type_id', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_end_date', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease End Date', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_group_id', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Group Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_status', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Status', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_status_id', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Status Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_type_id', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Type Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lessor_id', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lessor Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'limit_orig_cost', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Limit Orig Cost', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'limit_unpaid_bal', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Limit Unpaid Bal', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'long_description', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'master_agreement_date', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Master Agreement Date', 300, null, null, 'DATE', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'months_for_local_tax_base', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Months For Local Tax Base', 300, null, null, 'NUMBER', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'notes', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Notes', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_due_day', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payment Due Day', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'pre_payment_sw', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Pre Payment Sw', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_notc', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Notc', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_pct', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Pct', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'renewal_option_notification', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Renewal Option Notification', 300, null, null, 'NUMBER', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'time_stamp', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Time Stamp', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'user_id', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'User Id', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'workflow_type_id', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Workflow Type Id', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='FUTURE MINIMUM LEASE PAYMENTS');
delete from pp_any_query_criteria  where upper(description)='FUTURE MINIMUM LEASE PAYMENTS';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Future Minimum Lease Payments', to_date('2015-08-11 08:44:19', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select    (select s.description from set_of_books s where s.set_of_books_id = sch2.set_of_books_id) as set_of_books,    c.description company_description,
       (select lg.description from ls_lease_group lg where mla.lease_group_id = lg.lease_group_id) as lease_group,    (select ls.description from ls_lessor ls where ls.lessor_id = mla.lessor_id)
       as lessor,    mla.lease_number as lease_number, mla.description as lease_description,    case when mla.pre_payment_sw = 1 then ''Prepay'' else ''Arrears'' end as prepay,
       (select ig.description from ls_ilr_group ig where ilr.ilr_group_id = ig.ilr_group_id) as ilr_group,    ilr.ilr_number as ilr_number,
       (select lct.description from ls_lease_cap_type lct where lct.ls_lease_cap_type_id = ilro.lease_cap_type_id) as capitalization_type,
       la.description as asset_description, la.serial_number as serial_number,    al.ext_asset_location as ext_asset_location, al.long_description as asset_loc_description,
       (select ua.description from utility_account ua where ua.utility_account_id = la.utility_account_id and ua.bus_segment_id = la.bus_segment_id) as utility_account,
       (select ru.description from retirement_unit ru where ru.retirement_unit_id = la.retirement_unit_id) as retirement_unit,    nvl(td.county_id, al.county_id) as county,
       nvl(td.state_id, al.state_id) as state,    td.tax_district_code as tax_district_code,    case when sch2.the_row = 1 then la.fmv else 0 end as fair_market_value,
       sch2.month_number as month_number,    case when sch2.the_row = 1 then sch2.beg_capital_cost else 0 end as beg_capital_cost,    nvl(sch2.residual_amount, 0) as residual_amount,
       nvl(sch2.termination_penalty_amount, 0) as termination_penalty_amount,    nvl(sch2.bargain_purchase_amount, 0) as bargain_purchase_amount,    nvl(sch2.beg_obligation, 0) as beg_obligation,
       nvl(sch2.beg_lt_obligation, 0) as beg_lt_obligation,    nvl(sch2.interest_accrual, 0) as interest_accrual, nvl(sch2.principal_accrual, 0) as principal_accrual,
       nvl(sch2.interest_paid, 0) as interest_paid, nvl(sch2.principal_paid, 0) as principal_paid from    ls_lease mla, ls_lease_company lsc,    ls_ilr ilr, ls_ilr_options ilro,    ls_asset la,    
	   (       select row_number() over( partition by sch.ls_asset_id, sch.revision, sch.set_of_books_id order by sch.month ) as the_row,          sch.set_of_books_id, sch.ls_asset_id, sch.revision,          
	   to_number(to_char(sch.month, ''yyyymm'')) as month_number,          sch.residual_amount as residual_amount, sch.term_penalty as termination_penalty_amount,          sch.bpo_price as bargain_purchase_amount, sch.beg_capital_cost as beg_capital_cost,          
	   sch.beg_obligation as beg_obligation, sch.beg_lt_obligation as beg_lt_obligation,          sch.interest_accrual as interest_accrual, sch.principal_accrual as principal_accrual,          sch.interest_paid as interest_paid, sch.principal_paid as principal_paid       
	   from ls_asset_schedule sch    ) sch2,    company c,    asset_location al, prop_tax_district td where mla.lease_id = lsc.lease_id and lsc.company_id = c.company_id and lsc.lease_id = ilr.lease_id (+) and lsc.company_id = ilr.company_id (+) and ilr.ilr_id = la.ilr_id (+) 
	   and ilr.ilr_id = ilro.ilr_id (+) and ilr.current_revision = ilro.revision (+) and la.ls_asset_id = sch2.ls_asset_id and la.approved_revision = sch2.revision and la.asset_location_id = al.asset_location_id (+) and al.tax_district_id = td.tax_district_id (+) order by 1, 2, 3, 5, 8, 9, 11, 21',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_description', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Description', 300, 'description', 'ls_asset', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_loc_description', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Location', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'bargain_purchase_amount', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Bargain Purchase Amount', 300, null, null, 'NUMBER', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'capitalization_type', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Capitalization Type', 300, null, null, 'VARCHAR2', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'county', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'County', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ext_asset_location', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ext Asset Location', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'fair_market_value', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Fair Market Value', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_group', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'ILR Group', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'ILR Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_accrual', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Accrual', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_paid', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_description', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Description', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_group', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Group', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lessor', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lessor', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'month_number', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Month Number', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'prepay', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Prepay', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_accrual', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Accrual', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_paid', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'residual_amount', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Residual Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'retirement_unit', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Retirement Unit', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'serial_number', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Serial Number', 300, 'serial_number', 'ls_component', 'VARCHAR2', 0,
'serial_number', null, null, 'serial_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Set Of Books', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'state', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'State', 300, 'state_id', 'state', 'VARCHAR2', 0, 'state_id', null, null, 'state_id')
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_district_code', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax District Code', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'termination_penalty_amount', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Termination Penalty Amount', 300, null, null, 'NUMBER', 0, null,
 null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'utility_account', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Utility Account', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='ILR BY SET OF BOOKS (ALL REVISIONS)');
delete from pp_any_query_criteria  where upper(description)='ILR BY SET OF BOOKS (ALL REVISIONS)';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'ILR by Set of Books (ALL Revisions)', to_date('2015-08-11 08:44:19', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select l.ilr_id as ilr_id,    ll.ilr_number as ilr_number,   l.revision as revision,    (select a.description from approval_status a, ls_ilr_approval la where la.approval_status_id = 
a.approval_status_id and l.ilr_id = la.ilr_id and l.revision = la.revision) as revision_status,   (select s.description from set_of_books s where l.set_of_books_id = s.set_of_books_id) as set_of_books,   
c.description as company,   l.net_present_value as net_present_value, l.capital_cost as capital_cost,    l.current_lease_cost as fair_market_value,   (select count(1) from ls_asset la where la.ilr_id = l.ilr_id) as qty_number_of_assets   
from ls_ilr_amounts_set_of_books l, ls_ilr ll, company c  where l.ilr_id = ll.ilr_id  and ll.company_id = c.company_id  order by 2, 3',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'capital_cost', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'fair_market_value', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Fair Market Value', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'ILR Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'net_present_value', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Net Present Value', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'qty_number_of_assets', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Qty Number Of Assets', 300, null, null, 'NUMBER', 1, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision_status', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision Status', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Set Of Books', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='ILR BY SET OF BOOKS (CURRENT REVISION)');
delete from pp_any_query_criteria  where upper(description)='ILR BY SET OF BOOKS (CURRENT REVISION)';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'ILR by Set of Books (CURRENT Revision)', to_date('2015-08-11 08:44:19',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'none', 'lessee', 'user',
'select l.ilr_id as ilr_id,    ll.ilr_number as ilr_number,   l.revision as revision,    (select a.description from approval_status a, ls_ilr_approval la where la.approval_status_id = a.approval_status_id and l.ilr_id = la.ilr_id and l.revision = la.revision) as revision_status,   
(select s.description from set_of_books s where l.set_of_books_id = s.set_of_books_id) as set_of_books,   c.description as company,   l.net_present_value as net_present_value, l.capital_cost as capital_cost,    l.current_lease_cost as fair_market_value,   
(select count(1) from ls_asset la where la.ilr_id = l.ilr_id) as qty_number_of_assets   from ls_ilr_amounts_set_of_books l, ls_ilr ll, company c  where l.ilr_id = ll.ilr_id  and ll.company_id = c.company_id  and ll.current_revision = l.revision  order by 2, 3',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'capital_cost', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'fair_market_value', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Fair Market Value', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'ILR Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'net_present_value', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Net Present Value', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'qty_number_of_assets', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Qty Number Of Assets', 300, null, null, 'NUMBER', 1, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision_status', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision Status', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Set Of Books', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE ASSET SCHEDULE BY LEASED ASSET');
delete from pp_any_query_criteria  where upper(description)='LEASE ASSET SCHEDULE BY LEASED ASSET';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Asset Schedule by Leased Asset', to_date('2015-08-11 09:10:54', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2,  las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10,  las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,  las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(la.leased_asset_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''LEASED ASSET NUMBER'')',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'bpo_price', 68, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Bpo Price', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual1', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual10', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual10', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual2', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual3', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual4', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual5', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual6', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual7', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual8', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual9', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid1', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid1', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid10', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid2', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid2', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid3', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid3', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid4', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid4', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid5', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid5', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid6', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid6', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid7', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid7', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid8', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid8', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid9', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid9', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_lease_cost', 65, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Current Lease Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual1', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual10', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual2', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual3', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual4', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual5', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual6', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual7', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual8', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual9', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid1', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid10', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid10', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid2', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid3', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid4', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid5', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid6', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid7', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid8', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid9', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_accrual', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Accrual', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_paid', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_om', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Om', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', 1, 2, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', null,
null, 'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_accrual', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Accrual', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_paid', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'residual_amount', 66, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Residual Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books_id', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Set Of Books Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'term_penalty', 67, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Term Penalty', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE BALANCE SHEET DETAIL BY ASSET');
delete from pp_any_query_criteria  where upper(description)='LEASE BALANCE SHEET DETAIL BY ASSET';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Balance Sheet Detail by Asset', to_date('2015-08-12 09:49:11', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select la.ls_asset_id, la.leased_asset_number, to_char(las.month,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.state_id as state, gl.external_account_code as account,
       ua.utility_account_id, ua.description as utility_account_description, fc.description as func_class_description,
       las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation,
       case when las.month > depr_calc.month then proj.begin_reserve else cprd.beg_reserve_month end as beg_reserve,
       case when las.month > depr_calc.month then proj.end_reserve else cprd.depr_reserve end - nvl(cprd.retirements,0) as end_reserve,
       nvl(cprd. net_adds_and_adjust,0) as additions, nvl(cprd.retirements,0) + nvl(gain_loss,0) as retirements, nvl(cprd.transfers_in,0) as transfers_in,
       nvl(cprd.transfers_out,0) as transfers_out, nvl(cprd.reserve_trans_in,0) as reserve_trans_in, nvl(cprd.reserve_trans_out,0) as reserve_trans_out,
       (las.end_capital_cost - las.end_obligation - case when las.month > depr_calc.month then proj.end_reserve else cprd.depr_reserve end ) as ferc_gaap_difference
from ls_asset la, ls_cpr_asset_map map, ls_ilr ilr, ls_lease ll, company co,
     cpr_ledger cpr, utility_account ua, business_segment bs, func_class fc, gl_account gl,
     ls_asset_schedule las, asset_location al, ls_depr_forecast proj,
     ls_ilr_options ilro, ls_lease_cap_type lct,
     (select m.ls_asset_id, c.*
      from cpr_depr c, ls_cpr_asset_map m, cpr_ledger l
      where c.asset_id = m.asset_id
        and m.asset_id = l.asset_id
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
        and l.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) cprd,
     (select company_id, max(gl_posting_mo_yr) month
      from ls_process_control where depr_calc is not null
         and company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      group by company_id) depr_calc
where la.ls_asset_id = map.ls_asset_id
  and map.asset_id = cpr.asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilro.revision = ilr.current_revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and lct.is_om = 0
  and cpr.gl_account_id = gl.gl_account_id
  and la.utility_account_id = ua.utility_account_id
  and la.bus_segment_id = ua.bus_segment_id
  and ua.func_class_id = fc.func_class_id
  and la.bus_segment_id = bs.bus_segment_id
  and co.company_id = la.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and depr_calc.company_id = co.company_id
  and las.month = cprd.gl_posting_mo_yr (+)
  and las.ls_asset_id = cprd.ls_asset_id (+)
  and las.set_of_books_id = cprd.set_of_books_id (+)
  and las.ls_asset_id = proj.ls_asset_id (+)
  and las.revision = proj.revision (+)
  and las.month = proj.month (+)
  and las.set_of_books_id = proj.set_of_books_id (+)
  and la.asset_location_id = al.asset_location_id
  and to_char(las.month, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'account', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Account', 300, 'description', 'ls_dist_gl_account', 'VARCHAR2', 0,
'external_account_code', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'additions', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Additions', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_description', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Description', 300, 'description', 'ls_asset', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_reserve', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Reserve', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_reserve', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Reserve', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'external_ilr', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'External Ilr', 300, 'external_ilr', 'ls_ilr', 'VARCHAR2', 0, 'external_ilr',
null, null, 'external_ilr') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ferc_gaap_difference', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Ferc Gaap Difference', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'func_class_description', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Func Class Description', 300, 'description', 'func_class',
'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'reserve_trans_in', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reserve Trans In', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'reserve_trans_out', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reserve Trans Out', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'retirements', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Retirements', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'state', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'State', 300, 'state_id', 'state', 'VARCHAR2', 0, 'state_id', null, null, 'state_id')
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'transfers_in', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfers In', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'transfers_out', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfers Out', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'utility_account_description', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Utility Account Description', 300, 'description',
'utility_account', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'utility_account_id', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Utility Account Id', 300, 'description', 'utility_account', 'NUMBER', 0,
 'utility_account_id', null, null, 'description') ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE BALANCE SHEET SPAN BY ASSET');
delete from pp_any_query_criteria  where upper(description)='LEASE BALANCE SHEET SPAN BY ASSET';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Balance Sheet Span by Asset', to_date('2015-08-12 09:50:02', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'with balance_sheet_detail as (
select la.ls_asset_id, la.leased_asset_number, to_char(las.month,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.state_id as state, gl.external_account_code as account,
       ua.utility_account_id, ua.description as utility_account_description, fc.description as func_class_description,
       las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation,
       case when las.month > depr_calc.month then proj.begin_reserve else cprd.beg_reserve_month end as beg_reserve,
       case when las.month > depr_calc.month then proj.end_reserve else cprd.depr_reserve end as end_reserve,
       nvl(cprd. net_adds_and_adjust,0) as additions, nvl(cprd.retirements,0) + nvl(gain_loss,0) as retirements, nvl(cprd.transfers_in,0) as transfers_in,
       nvl(cprd.transfers_out,0) as transfers_out, nvl(cprd.reserve_trans_in,0) as reserve_trans_in, nvl(cprd.reserve_trans_out,0) as reserve_trans_out,
       (las.end_capital_cost - las.end_obligation - case when las.month > depr_calc.month then proj.end_reserve else cprd.depr_reserve end) as ferc_gaap_difference
from ls_asset la, ls_cpr_asset_map map, ls_ilr ilr, ls_lease ll, company co,
     cpr_ledger cpr, utility_account ua, business_segment bs, func_class fc, gl_account gl,
     ls_asset_schedule las, asset_location al, ls_depr_forecast proj,
     ls_ilr_options ilro, ls_lease_cap_type lct,
     (select m.ls_asset_id, c.*
      from cpr_depr c, ls_cpr_asset_map m, cpr_ledger l
      where c.asset_id = m.asset_id
        and m.asset_id = l.asset_id
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') >= (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') <= (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')
        and l.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) cprd,
     (select company_id, max(gl_posting_mo_yr) month
      from ls_process_control where depr_calc is not null
        and company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      group by company_id) depr_calc
where la.ls_asset_id = map.ls_asset_id
  and map.asset_id = cpr.asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilro.revision = ilr.current_revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and lct.is_om = 0
  and cpr.gl_account_id = gl.gl_account_id
  and la.utility_account_id = ua.utility_account_id
  and la.bus_segment_id = ua.bus_segment_id
  and ua.func_class_id = fc.func_class_id
  and la.bus_segment_id = bs.bus_segment_id
  and co.company_id = la.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and depr_calc.company_id = co.company_id (+)
  and las.month = cprd.gl_posting_mo_yr (+)
  and las.ls_asset_id = cprd.ls_asset_id (+)
  and las.set_of_books_id = cprd.set_of_books_id (+)
  and las.ls_asset_id = proj.ls_asset_id (+)
  and las.revision = proj.revision (+)
  and las.month = proj.month (+)
  and las.set_of_books_id = proj.set_of_books_id (+)
  and la.asset_location_id = al.asset_location_id
  and to_char(las.month, ''yyyymm'') >= (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')
  and to_char(las.month, ''yyyymm'') <= (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID''))
--/********',

'********************************************************************************************/
select (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'') as start_monthnum,
       (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'') as end_monthnum,
       fm.LS_ASSET_ID, LEASED_ASSET_NUMBER, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
       EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
       FUNC_CLASS_DESCRIPTION,
       nvl(BEG_CAPITAL_COST,0) as BEG_CAPITAL_COST, nvl(END_CAPITAL_COST,0) as end_capital_cost, nvl(BEG_OBLIGATION,0) as beg_obligation,
       nvl(END_OBLIGATION,0) as end_obligation, nvl(BEG_LT_OBLIGATION,0) as beg_lt_obligation, nvl(END_LT_OBLIGATION,0) as end_lt_obligation,
       nvl(BEG_RESERVE,0) as beg_reserve, nvl(END_RESERVE,0) as end_reserve,
       sum(additions) as additions, sum(RETIREMENTS) as retirements, sum(TRANSFERS_IN) as transfers_in, sum(TRANSFERS_OUT) as transfers_out,
       sum(RESERVE_TRANS_IN) as reserve_trans_in, sum(RESERVE_TRANS_OUT) as reserve_trans_out, sum(FERC_GAAP_DIFFERENCE) as ferc_gaap_difference
from
(select monthnum, ls_asset_id, beg_capital_cost, beg_obligation, beg_lt_obligation, beg_reserve
 from balance_sheet_detail
 where monthnum = (select filter_value from pp_any_required_filter where upper(column_name)= ''START MONTHNUM'')) fm, /* first month */
(select monthnum, ls_asset_id, end_capital_cost, end_obligation, end_lt_obligation, end_reserve
 from balance_sheet_detail
 where monthnum = (select filter_value from pp_any_required_filter where upper(column_name)= ''END MONTHNUM'')) lm, /* last month */
(select LS_ASSET_ID, LEASED_ASSET_NUMBER, MONTHNUM, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
        EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
        FUNC_CLASS_DESCRIPTION, ADDITIONS, RETIREMENTS, TRANSFERS_IN, TRANSFERS_OUT, RESERVE_TRANS_IN, RESERVE_TRANS_OUT, FERC_GAAP_DIFFERENCE
 from balance_sheet_detail) am /* all months */
where 1=1
  and am.ls_asset_id = fm.ls_asset_id (+)
  and am.ls_asset_id = lm.ls_asset_id (+)
group by fm.monthnum, lm.monthnum, fm.LS_ASSET_ID, LEASED_ASSET_NUMBER, ASSET_DESCRIPTION, COMPANY_ID, COMPANY_DESCRIPTION, ILR_ID, ILR_NUMBER,
       EXTERNAL_ILR, LEASE_ID, LEASE_NUMBER, LEASE_CAP_TYPE, LOCATION, STATE, ACCOUNT, UTILITY_ACCOUNT_ID, UTILITY_ACCOUNT_DESCRIPTION,
       FUNC_CLASS_DESCRIPTION, BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION, END_LT_OBLIGATION, BEG_RESERVE,
       END_RESERVE
order by 1


',
 null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'account', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Account', 300, 'description', 'ls_dist_gl_account', 'VARCHAR2', 0,
'external_account_code', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'additions', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Additions', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_description', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Description', 300, 'description', 'ls_asset', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_reserve', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Reserve', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_monthnum', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'End Monthnum', 300, null, null, 'VARCHAR2', 0, null, 1, 1, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_reserve', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Reserve', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'external_ilr', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'External Ilr', 300, 'external_ilr', 'ls_ilr', 'VARCHAR2', 0, 'external_ilr',
null, null, 'external_ilr') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ferc_gaap_difference', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Ferc Gaap Difference', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'func_class_description', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Func Class Description', 300, 'description', 'func_class',
'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'reserve_trans_in', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reserve Trans In', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'reserve_trans_out', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reserve Trans Out', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'retirements', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Retirements', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'start_monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Start Monthnum', 300, null, null, 'VARCHAR2', 0, null, 1, 1, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'state', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'State', 300, 'state_id', 'state', 'VARCHAR2', 0, 'state_id', null, null, 'state_id')
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'transfers_in', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfers In', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'transfers_out', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfers Out', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'utility_account_description', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Utility Account Description', 300, 'description',
'utility_account', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'utility_account_id', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Utility Account Id', 300, 'description', 'utility_account', 'NUMBER', 0,
 'utility_account_id', null, null, 'description') ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE EFFECTIVE TAX RATES BY ASSET');
delete from pp_any_query_criteria  where upper(description)='LEASE EFFECTIVE TAX RATES BY ASSET';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Effective Tax Rates by Asset', to_date('2015-08-12 10:04:06', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'with rates as (
	select
    la.ls_asset_id,
		tl.tax_local_id,
		tsr.rate,
    al.state_id as state,
    null as tax_district_id,
    ''State'' as tax_type
	from ls_asset la, asset_location al, ls_tax_local tl, ls_lease ll,
       ls_ilr ilr, ls_lease_options llo, ls_asset_tax_map tax_map,
		(select tsr2.*, row_number() over(partition by tsr2.tax_local_id, tsr2.state_id order by tsr2.effective_date desc) as the_row
		 from ls_tax_state_rates tsr2
     where effective_date <= (select to_date(filter_value, ''yyyymmdd'') from pp_any_required_filter where upper(trim(column_name)) = ''EFFECTIVE DATE YYYYMMDD'')) tsr
	where tsr.tax_local_id = tl.tax_local_id
	and tl.tax_summary_id = llo.tax_summary_id
	and tsr.state_id = al.state_id
	and al.asset_location_id = la.asset_location_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ll.lease_id = llo.lease_id
  and ll.current_revision = llo.revision
  and tax_map.ls_asset_id = la.ls_asset_id
  and tax_map.tax_local_id = tl.tax_local_id
  and tax_map.status_code_id = 1
	and tsr.the_row = 1
union all
	select
		la.ls_asset_id,
		tl.tax_local_id,
		tdr.rate,
    null as state,
    td.tax_district_id,
    ''Local'' as tax_type
	from ls_asset la, ls_location_tax_district al, ls_tax_local tl, ls_lease ll,
       ls_ilr ilr, ls_lease_options llo, ls_asset_tax_map tax_map, tax_district td,
		(select tsr2.*, row_number() over(partition by tsr2.tax_local_id, tsr2.ls_tax_district_id order by tsr2.effective_date desc) as the_row
		 from ls_tax_district_rates tsr2
     where effective_date <= (select to_date(filter_value, ''yyyymmdd'') from pp_any_required_filter where upper(trim(column_name)) = ''EFFECTIVE DATE YYYYMMDD'')) tdr
	where tdr.tax_local_id = tl.tax_local_id
	and tl.tax_summary_id = llo.tax_summary_id
	and tdr.ls_tax_district_id = al.tax_district_id
	and al.asset_location_id = la.asset_location_id
	and tdr.the_row = 1
  and tax_map.ls_asset_id = la.ls_asset_id
  and tax_map.tax_local_id = tl.tax_local_id
  and tax_map.status_code_id = 1
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ll.lease_id = llo.lease_id
  and ll.current_revision = llo.revision
  and al.tax_district_id = td.tax_district_id)
/*-----------------------------------------------------------------------------------
------------------------------------------------------------------------------------- */
select (select filter_value from pp_any_required_filter where upper(trim(column_name)) = ''EFFECTIVE DATE YYYYMMDD'') effective_date_yyyymmdd,
       la.ls_asset_id, la.leased_asset_number, ilr.ilr_id, ilr.ilr_number,
       ll.lease_id, ll.lease_number, co.company_id, co.description as company_description, lct.description as lease_cap_type, al.long_description as location,
       ltl.tax_local_id, ltl.description as tax_local, rates.tax_type, td.description as tax_district, rates.state,
       rates.rate
/*, sum(rates.rate) over(partition by la.ls_asset_id) as total_asset_rate */
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, rates,
     tax_district td, ls_tax_local ltl, asset_location al, ls_ilr_options ilro, ls_lease_cap_type lct
where la.ls_asset_id = rates.ls_asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and la.company_id = co.company_id
  and la.asset_location_id = al.asset_location_id
  and rates.tax_district_id = td.tax_district_id (+)
  and rates.tax_local_id = ltl.tax_local_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'effective_date_yyyymmdd', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Effective Date Yyyymmdd', 300, null, null, 'VARCHAR2', 0, null, 1,
1, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'rate', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Rate', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'state', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'State', 300, 'state_id', 'state', 'VARCHAR2', 0, 'state_id', null, null, 'state_id')
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_district', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax District', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_local', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Local', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_local_id', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Local Id', 300, 'description', 'ls_tax_local', 'NUMBER', 0,
'tax_local_id', null, null, 'tax_local_id') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_type', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Type', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE ASSET LOCATION DETAIL');
delete from pp_any_query_criteria  where upper(description)='LEASE ASSET LOCATION DETAIL';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Asset Location Detail', to_date('2015-08-11 08:55:50', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select la.ls_asset_id, la.leased_asset_number, la.description as asset_description,
       ls.description as leased_asset_status, ilrs.description as ilr_status,
       lfs.description as funding_status, cap.description as lease_cap_type,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number,
       al.long_description as location, al.address, t.description as town, al.state_id as state, al.zip_code, al.county_id county, al.grid_coordinate
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_asset_status ls, ls_ilr_status ilrs, ls_funding_status lfs, town t, ls_lease_cap_type cap, ls_ilr_options ilro
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and la.company_id = co.company_id
  and la.asset_location_id = al.asset_location_id
  and la.ls_asset_status_id = ls.ls_asset_status_id
  and ilr.ilr_status_id = ilrs.ilr_status_id
  and ilr.funding_status_id = lfs.funding_status_id
  and al.town_id = t.town_id (+)
  and cap.ls_lease_cap_type_id = ilro.lease_cap_type_id
  and ilro.ilr_id = ilr.ilr_id
  and ilro.revision = ilr.current_revision',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'address', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Address', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_description', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Description', 300, 'description', 'ls_asset', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'county', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'County', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'external_ilr', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'External Ilr', 300, 'external_ilr', 'ls_ilr', 'VARCHAR2', 0, 'external_ilr',
null, null, 'external_ilr') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'funding_status', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Funding Status', 300, 'description', 'ls_funding_status', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'grid_coordinate', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Grid Coordinate', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_status', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Status', 300, 'description', 'ls_ilr_status', 'VARCHAR2', 0, 'description',
null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_status', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Status', 300, 'description', 'ls_asset_status', 'VARCHAR2',
 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'state', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'State', 300, 'state_id', 'state', 'VARCHAR2', 0, 'state_id', null, null, 'state_id')
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'town', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Town', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'zip_code', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Zip Code', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE ASSET SCHEDULE BY MLA');
delete from pp_any_query_criteria  where upper(description)='LEASE ASSET SCHEDULE BY MLA';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Asset Schedule By MLA', to_date('2015-08-11 08:58:52', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2, las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10, las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10, las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(ll.lease_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''LEASE NUMBER'')',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'bpo_price', 68, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Bpo Price', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual1', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual10', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual10', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual2', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual3', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual4', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual5', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual6', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual7', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual8', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual9', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid1', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid1', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid10', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid2', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid2', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid3', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid3', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid4', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid4', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid5', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid5', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid6', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid6', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid7', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid7', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid8', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid8', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid9', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid9', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_lease_cost', 65, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Current Lease Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual1', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual10', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual2', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual3', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual4', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual5', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual6', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual7', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual8', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual9', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid1', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid10', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid10', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid2', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid3', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid4', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid5', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid6', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid7', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid8', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid9', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_accrual', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Accrual', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_paid', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_om', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Om', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
1, 2, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', null,
null, 'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_accrual', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Accrual', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_paid', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'residual_amount', 66, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Residual Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books_id', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Set Of Books Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'term_penalty', 67, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Term Penalty', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE GL ACCOUNT BALANCES BY ASSET');
delete from pp_any_query_criteria  where upper(description)='LEASE GL ACCOUNT BALANCES BY ASSET';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease GL Account Balances by Asset', to_date('2015-08-12 10:07:34', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select ls_asset_id,
       company.description as company_description,
       company.company_id as company_id,
       (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'') as monthnum,
       gl.external_account_code,
       nvl(a.beg_cost,0) as beginning_cost,
       nvl(a.current_mo_adds, 0) as additions,
       nvl(a.current_mo_retirements,0) as retirements,
       nvl(a.current_mo_trf_to,0) as transfers_to,
       nvl(a.current_mo_trf_from,0) as transfers_from,
       nvl(a.end_cost,0) as ending_cost,
       decode(union_group, 2, sum(a.end_cost) over(partition by ls_asset_id),0) as GAAP
from gl_account gl, company,
     (select ls_asset.ls_asset_id, company.company_id, cpr.gl_account_id,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), 0,activity_cost)) beg_cost,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UADD'', activity_cost,0),0)) current_mo_adds,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             case when trim(activity_code) in (''URGL'', ''URET'') THEN  activity_cost ELSE 0 END,0)) current_mo_retirements,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UTRT'', activity_cost,0),0)) current_mo_trf_to,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UTRF'', activity_cost,0),0)) current_mo_trf_from,
             sum(cpra.activity_cost) end_cost,
             1 as union_group
      from cpr_activity cpra, ls_cpr_asset_map map, ls_asset, company, cpr_ledger cpr, ls_ilr, ls_lease
      where ls_asset.company_id = company.company_id
        and cpra.asset_id = map.asset_id
        and cpra.asset_id = cpr.asset_id
        and map.ls_asset_id = ls_asset.ls_asset_id
		and ls_asset.ilr_id = ls_ilr.ilr_id
		and ls_ilr.lease_id = ls_lease.lease_id
        and (ls_asset.ls_asset_status_id = 3
             or (ls_asset.ls_asset_status_id = 4 and ls_asset.retirement_date >=to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''), ''yyyymm'')))
        and cpra.gl_posting_mo_yr <= to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
        and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
       group by ls_asset.ls_asset_id,company.company_id, cpr.gl_account_id
UNION ALL
/****************************************************************************/select ls_asset.ls_asset_id,company.company_id, ilra.st_oblig_account_id,
       -1 * nvl(sum(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_obligation - las.end_lt_obligation)),0) as beg_cost,
       -1 * sum(case when (las.month=to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''), ''yyyymm'') and nvl(is_trf,0)=0 and nvl(is_trft,0) = 0 and nvl(ls_asset.retirement_date, to_date(1500,''yyyy'')) <> las.month)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation)else 0 end) as current_mo_adds,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.re',

'tirement_date and nvl(is_trf,0) <> 1)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation) else 0 end) as current_mo_retirements,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trft,0) = 1)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation) else 0 end) as current_mo_trf_to,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.retirement_date and nvl(is_trf,0) = 1)
                                      then (las.end_obligation - las.end_lt_obligation) - decode(las.beg_capital_cost,0,0, las.beg_obligation - las.beg_lt_obligation) else 0 end) as current_mo_trf_from,
       -1 * sum(decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), (las.end_obligation - las.end_lt_obligation))) as end_cost,
       2 as union_group
    from ls_ilr_account ilra, ls_ilr, ls_asset, ls_asset_schedule las, ls_ilr_options ilro, ls_lease_cap_type lct, company, ls_lease,
        (select a.*, 1 is_trf from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trf,
        (select a.*, 1 is_trft from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trft
    where las.ls_asset_id = ls_asset.ls_asset_id
      and las.revision = ls_asset.approved_revision
      and ls_asset.ilr_id = ls_ilr.ilr_id
      and ls_ilr.ilr_id = ilra.ilr_id
      and ls_ilr.ilr_id = ilro.ilr_id
      and ls_ilr.current_revision = ilro.revision
	  and ls_ilr.lease_id = ls_lease.lease_id
      and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
      and lct.is_om = 0
      and ls_asset.company_id = company.company_id
      and las.month in (to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1))
      and ls_asset.ls_asset_id = trf.from_ls_asset_id (+)
      and ls_asset.ls_asset_id = trft.to_ls_asset_id (+)
      and ls_asset.ls_asset_status_id >=3
       and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  group by ls_asset.ls_asset_id,ilra.st_oblig_account_id, company.company_id
UNION ALL
/****************************************************************************/      select ls_asset.ls_asset_id,company.company_id, ilra.lt_oblig_account_id,
       -1 * nvl(sum(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_obligation,0)),0) as beg_cost,
       -1 * sum(case when (las.month=to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''), ''yyyymm'') and nvl(is_trf,0)=0 and nvl(is_trft,0) = 0 and nvl(ls_asset.retirement_date, to_date(1500,''yyyy'')) <> las.month)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0, las.beg_lt_obligation)else 0 end) as current_mo_adds,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.retirement_date and nvl(is_trf,0) <> 1)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0, las.beg_lt_obligation) else 0 end) as current_mo_reti',

'rements,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trft,0) = 1)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0, las.beg_lt_obligation) else 0 end) as current_mo_trf_to,
       -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and las.month = ls_asset.retirement_date and nvl(is_trf,0) = 1)
                                      then las.end_lt_obligation - decode(las.beg_capital_cost,0,0,las.beg_lt_obligation) else 0 end) as current_mo_trf_from,
       -1 * nvl(sum(decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_lt_obligation)),0) as end_cost,
       3 as union_group
    from ls_ilr_account ilra, ls_ilr, ls_asset, ls_asset_schedule las, ls_ilr_options ilro, ls_lease_cap_type lct, company, ls_lease,
        (select a.*, 1 is_trf from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trf,
        (select a.*, 1 is_trft from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trft
    where las.ls_asset_id = ls_asset.ls_asset_id
      and las.revision = ls_asset.approved_revision
      and ls_asset.ilr_id = ls_ilr.ilr_id
      and ls_ilr.ilr_id = ilra.ilr_id
      and ls_ilr.ilr_id = ilro.ilr_id
	  and ls_ilr.lease_id = ls_lease.lease_id
      and ls_ilr.current_revision = ilro.revision
      and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
      and lct.is_om = 0
      and ls_asset.company_id = company.company_id
      and las.month in (to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1))
      and ls_asset.ls_asset_id = trf.from_ls_asset_id (+)
      and ls_asset.ls_asset_id = trft.to_ls_asset_id (+)
      and ls_asset.ls_asset_status_id >=3
       and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  group by ls_asset.ls_asset_id,ilra.lt_oblig_account_id, company.company_id
UNION ALL
/****************************************************************************/select ls_asset.ls_asset_id,company.company_id, dg.reserve_acct_id,
       -1 *  nvl(sum(nvl(cprd.beg_reserve_month,0)),0) as beg_cost,
       -1 * sum(nvl(curr_depr_expense,0) + nvl(depr_exp_alloc_adjust,0)) as current_mo_adds,
       -1 * sum(nvl(retirements,0) + nvl(gain_loss,0)) as current_mo_retirements,
       -1 * sum(nvl(reserve_trans_in,0)) as current_mo_trf_to,
       -1 * sum(nvl(reserve_trans_out,0)) as current_mo_trf_from,
       -1 * sum(nvl(depr_reserve,0)) as end_cost,
       4 as union_group
from company, cpr_depr cprd, cpr_ledger cpr, ls_cpr_asset_map map, depr_group dg, ls_asset, ls_ilr, ls_lease
where company.company_id = cpr.company_id
  and map.asset_id = cpr.asset_id
  and cpr.asset_id = cprd.asset_id
  and cprd.gl_posting_mo_yr = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
  and cpr.depr_group_id = dg.depr_group_id
  and ls_asset.ls_asset_id = map.ls_asset_id
  and ls_asset.ilr_id = ls_ilr.ilr_id
   and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  and ls_ilr.lease_id = ls_lease.lease_id
group by ls_asset.ls_asset_id,company.company_id, dg.reserve_acct_id
) a
where a.gl_account_id = gl.gl_account_id
  and a.company_id = company.company_id
order by external_account_code',
 null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'additions', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Additions', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beginning_cost', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beginning Cost', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ending_cost', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Ending Cost', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'external_account_code', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'External Account Code', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'gaap', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Gaap', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 1,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'retirements', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Retirements', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'transfers_from', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfers From', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'transfers_to', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Transfers To', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE IMPORT ASSET ARCHIVE');
delete from pp_any_query_criteria  where upper(description)='LEASE IMPORT ASSET ARCHIVE';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Import Asset Archive', to_date('2015-08-12 10:23:41', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select run.description import_run_description,
  arc.IMPORT_RUN_ID, LINE_ID, arc.TIME_STAMP, arc.USER_ID, LS_ASSET_ID, LEASED_ASSET_NUMBER, ILR_XLATE, ILR_ID, arc.DESCRIPTION, LONG_DESCRIPTION,
  QUANTITY, FMV, COMPANY_XLATE, COMPANY_ID, BUS_SEGMENT_XLATE, BUS_SEGMENT_ID, UTILITY_ACCOUNT_XLATE, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_XLATE,
  SUB_ACCOUNT_ID, RETIREMENT_UNIT_XLATE, RETIREMENT_UNIT_ID, PROPERTY_GROUP_XLATE, PROPERTY_GROUP_ID, WORK_ORDER_XLATE, WORK_ORDER_ID,
  ASSET_LOCATION_XLATE, ASSET_LOCATION_ID, GUARANTEED_RESIDUAL_AMOUNT, EXPECTED_LIFE, ECONOMIC_LIFE, arc.NOTES, CLASS_CODE_XLATE1, CLASS_CODE_ID1,
  CLASS_CODE_VALUE1, CLASS_CODE_XLATE2, CLASS_CODE_ID2, CLASS_CODE_VALUE2, CLASS_CODE_XLATE3, CLASS_CODE_ID3, CLASS_CODE_VALUE3, CLASS_CODE_XLATE4,
  CLASS_CODE_ID4, CLASS_CODE_VALUE4, CLASS_CODE_XLATE5, CLASS_CODE_ID5, CLASS_CODE_VALUE5, CLASS_CODE_XLATE6, CLASS_CODE_ID6, CLASS_CODE_VALUE6, CLASS_CODE_XLATE7,
  CLASS_CODE_ID7, CLASS_CODE_VALUE7, CLASS_CODE_XLATE8, CLASS_CODE_ID8, CLASS_CODE_VALUE8, CLASS_CODE_XLATE9, CLASS_CODE_ID9, CLASS_CODE_VALUE9, CLASS_CODE_XLATE10,
  CLASS_CODE_ID10, CLASS_CODE_VALUE10, CLASS_CODE_XLATE11, CLASS_CODE_ID11, CLASS_CODE_VALUE11, CLASS_CODE_XLATE12, CLASS_CODE_ID12, CLASS_CODE_VALUE12, CLASS_CODE_XLATE13,
  CLASS_CODE_ID13, CLASS_CODE_VALUE13, CLASS_CODE_XLATE14, CLASS_CODE_ID14, CLASS_CODE_VALUE14, CLASS_CODE_XLATE15, CLASS_CODE_ID15, CLASS_CODE_VALUE15, CLASS_CODE_XLATE16,
  CLASS_CODE_ID16, CLASS_CODE_VALUE16, CLASS_CODE_XLATE17, CLASS_CODE_ID17, CLASS_CODE_VALUE17, CLASS_CODE_XLATE18, CLASS_CODE_ID18, CLASS_CODE_VALUE18, CLASS_CODE_XLATE19,
  CLASS_CODE_ID19, CLASS_CODE_VALUE19, CLASS_CODE_XLATE20, CLASS_CODE_ID20, CLASS_CODE_VALUE20, LOADED, IS_MODIFIED, ERROR_MESSAGE, UNIQUE_ASSET_IDENTIFIER, SERIAL_NUMBER,
  TAX_ASSET_LOCATION_XLATE, TAX_ASSET_LOCATION_ID, DEPARTMENT_XLATE,
  DEPARTMENT_ID, ESTIMATED_RESIDUAL, TAX_SUMMARY_ID, TAX_SUMMARY_XLATE, USED_YN_SW, GL_ACCOUNT_ID, GL_ACCOUNT_XLATE, PROPERTY_TAX_DATE, PROPERTY_TAX_AMOUNT
  from pp_import_run run, ls_import_asset_archive arc
  where run.import_run_id = arc.import_run_id',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_location_id', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Location Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_location_xlate', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Location Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'bus_segment_id', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Bus Segment Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'bus_segment_xlate', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Bus Segment Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id1', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id10', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id10', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id11', 65, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id11', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id12', 68, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id12', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id13', 71, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id13', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id14', 74, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id14', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id15', 77, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id15', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id16', 80, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id16', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id17', 83, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id17', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id18', 86, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id18', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id19', 89, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id19', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id2', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id20', 92, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id20', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id3', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id4', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id5', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id6', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id7', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id8', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id9', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value1', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value1', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value10', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value10', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value11', 66, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value11', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value12', 69, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value12', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value13', 72, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value13', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value14', 75, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value14', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value15', 78, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value15', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value16', 81, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value16', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value17', 84, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value17', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value18', 87, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value18', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value19', 90, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value19', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value2', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value2', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value20', 93, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value20', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value3', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value3', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value4', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value4', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value5', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value5', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value6', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value6', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value7', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value7', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value8', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value8', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value9', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value9', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate1', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate1', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate10', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate10', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate11', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate11', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate12', 67, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate12', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate13', 70, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate13', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate14', 73, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate14', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate15', 76, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate15', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate16', 79, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate16', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate17', 82, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate17', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate18', 85, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate18', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate19', 88, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate19', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate2', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate2', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate20', 91, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate20', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate3', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate3', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate4', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate4', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate5', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate5', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate6', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate6', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate7', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate7', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate8', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate8', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate9', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate9', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_xlate', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'department_id', 102, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Department Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'department_xlate', 101, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Department Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'description', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'economic_life', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Economic Life', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'error_message', 96, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Error Message', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'estimated_residual', 103, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Estimated Residual', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'expected_life', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Expected Life', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'fmv', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Fmv', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'guaranteed_residual_amount', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Guaranteed Residual Amount', 300, null, null, 'NUMBER', 0, null,
 null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_xlate', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_description', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Description', 300, 'description',
'(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 253))', 'VARCHAR2', 0, 'description', 1, 2,
'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_modified', 95, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Modified', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'line_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Line Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'loaded', 94, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Loaded', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'long_description', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'notes', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Notes', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'property_group_id', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Property Group Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'property_group_xlate', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Property Group Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'property_tax_amount', 108, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Property Tax Amount', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'property_tax_date', 107, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Property Tax Date', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'quantity', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Quantity', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'retirement_unit_id', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Retirement Unit Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'retirement_unit_xlate', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Retirement Unit Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'serial_number', 98, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Serial Number', 300, 'serial_number', 'ls_component', 'VARCHAR2', 0,
'serial_number', null, null, 'serial_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sub_account_id', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Sub Account Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sub_account_xlate', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Sub Account Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_asset_location_id', 100, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Asset Location Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_asset_location_xlate', 99, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Asset Location Xlate', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_summary_id', 104, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Summary Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_summary_xlate', 105, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Summary Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'time_stamp', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Time Stamp', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'unique_asset_identifier', 97, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Unique Asset Identifier', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'used_yn_sw', 106, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Used Yn Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'user_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'User Id', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'utility_account_id', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Utility Account Id', 300, 'description', 'utility_account', 'NUMBER', 0,
 'utility_account_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'utility_account_xlate', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Utility Account Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'work_order_id', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Work Order Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'work_order_xlate', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Work Order Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE ILR''S PENDING APPROVAL');
delete from pp_any_query_criteria  where upper(description)='LEASE ILR''S PENDING APPROVAL';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease ILR''s Pending Approval', to_date('2015-08-12 10:20:45', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'  select
	pt.ilr_id as ilr_id,
	pt.ilr_number as ilr_number,
	c.description as company,
	pt.revision,
  u.last_name || '', '' || u.first_name as sending_user,
	aps.description as approval_status,
  amounts.current_lease_cost as current_lease_cost,
	amounts.CAPITAL_COST as Capital_cost,
	decode(current_lease_cost, 0, 0, round(100 * amounts.Capital_cost/amounts.current_lease_cost,2)) as capital_percent
from workflow wf, company c, approval_status aps,
	(select distinct p.ilr_id, p.revision, i.ilr_number, i.company_id, p.error_message
	from ls_pend_transaction p, ls_ilr i where i.ilr_id = p.ilr_id) pt, ls_ilr_amounts_set_of_books amounts, pp_security_users u
where wf.subsystem = ''ilr_approval''
and wf.id_field1 = to_char(pt.ilr_id)
and wf.id_field2 = to_char(pt.revision)
and c.company_id = pt.company_id
and wf.approval_status_id not in (1,3,6)
and amounts.ilr_id = pt.ilr_id
and amounts.revision = pt.revision
and wf.approval_status_id = aps.approval_status_id
and lower(trim(user_sent)) = lower(trim(u.users))',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'approval_status', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Approval Status', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'capital_cost', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'capital_percent', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Capital Percent', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_lease_cost', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Current Lease Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sending_user', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Sending User', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE IMPORT ASSET TAXES ARCHIVE');
delete from pp_any_query_criteria  where upper(description)='LEASE IMPORT ASSET TAXES ARCHIVE';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Import Asset Taxes Archive', to_date('2015-08-12 10:30:03', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.TIME_STAMP,arc.USER_ID,arc.LS_ASSET_XLATE,arc.LS_ASSET_ID,arc.TAX_LOCAL_XLATE,arc.TAX_LOCAL_ID,arc.STATUS_CODE_ID,arc.COMPANY_XLATE,arc.COMPANY_ID,arc.LOADED,arc.IS_MODIFIED,arc.ERROR_MESSAGE,arc.LS_ASSET_OVERRIDE
  from pp_import_run run, ls_import_asset_taxes_archive arc
  where run.import_run_id = arc.import_run_id',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_xlate', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'error_message', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Error Message', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_description', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Description', 300, 'description',
'(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 262))', 'VARCHAR2', 0, 'description', 1, 2,
'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_modified', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Modified', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'line_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Line Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'loaded', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Loaded', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_override', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Override', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_xlate', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'status_code_id', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Status Code Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_local_id', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Local Id', 300, 'description', 'ls_tax_local', 'NUMBER', 0, 'tax_local_id',
 null, null, 'tax_local_id') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_local_xlate', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Local Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'time_stamp', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Time Stamp', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'user_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'User Id', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE IMPORT COMPONENT ARCHIVE');
delete from pp_any_query_criteria  where upper(description)='LEASE IMPORT COMPONENT ARCHIVE';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Import Component Archive', to_date('2015-08-12 10:30:43', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.ERROR_MESSAGE,arc.TIME_STAMP,arc.USER_ID,arc.COMPONENT_ID,arc.LS_COMP_STATUS_ID,arc.COMPANY_ID,
arc.COMPANY_XLATE,arc.DATE_RECEIVED,arc.DESCRIPTION,arc.LONG_DESCRIPTION,arc.SERIAL_NUMBER,arc.PO_NUMBER,arc.AMOUNT,arc.LS_ASSET_ID,arc.LS_ASSET_XLATE,arc.INTERIM_INTEREST_START_DATE,arc.INTERIM_INTEREST,arc.INVOICE_NUMBER,arc.INVOICE_DATE,
arc.INVOICE_AMOUNT,arc.UNIQUE_COMPONENT_IDENTIFIER,arc.LS_ASSET_OVERRIDE
  from pp_import_run run, ls_import_component_archive arc
  where run.import_run_id = arc.import_run_id
  ',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'amount', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_xlate', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'component_id', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Component Id', 300, 'description', 'ls_component', 'NUMBER', 0, 'component_id',
 null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'date_received', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Date Received', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'description', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'error_message', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Error Message', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_description', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Description', 300, 'description',
'(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 255))', 'VARCHAR2', 0, 'description', 1, 2,
'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interim_interest', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interim Interest', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interim_interest_start_date', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Interim Interest Start Date', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'invoice_amount', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Invoice Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'invoice_date', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Invoice Date', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'invoice_number', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Invoice Number', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'line_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Line Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'long_description', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_override', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Override', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_xlate', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_comp_status_id', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Comp Status Id', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'po_number', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Po Number', 300, 'po_number', 'ls_component', 'VARCHAR2', 0, 'po_number', null,
null, 'po_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'serial_number', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Serial Number', 300, 'serial_number', 'ls_component', 'VARCHAR2', 0,
'serial_number', null, null, 'serial_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'time_stamp', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Time Stamp', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'unique_component_identifier', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Unique Component Identifier', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'user_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'User Id', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE IMPORT JE ALLOCATION ROWS');
delete from pp_any_query_criteria  where upper(description)='LEASE IMPORT JE ALLOCATION ROWS';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Import JE Allocation Rows', to_date('2015-08-12 10:32:35', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.TIME_STAMP,arc.USER_ID,arc.LS_ASSET_XLATE,arc.LS_ASSET_ID,arc.ILR_XLATE,arc.ILR_ID,arc.COMPANY_XLATE,arc.COMPANY_ID,arc.TRANS_TYPE_XLATE,
arc.TRANS_TYPE,arc.TAX_LOCAL_XLATE,arc.TAX_LOCAL_ID,arc.PERCENT,arc.ELEMENT_1,arc.ELEMENT_2,arc.ELEMENT_3,arc.ELEMENT_4,arc.ELEMENT_5,arc.ELEMENT_6,arc.ELEMENT_7,arc.ELEMENT_8,arc.ELEMENT_9,arc.ELEMENT_10,arc.ELEMENT_11,arc.ELEMENT_12,
arc.ELEMENT_13,arc.ELEMENT_14,arc.ELEMENT_15,arc.ELEMENT_16,arc.ELEMENT_17,arc.ELEMENT_18,arc.ELEMENT_19,arc.ELEMENT_20,arc.ELEMENT_21,arc.ELEMENT_22,arc.ELEMENT_23,arc.ELEMENT_24,arc.ELEMENT_25,arc.ELEMENT_26,arc.ELEMENT_27,arc.ELEMENT_28,
arc.ELEMENT_29,arc.ELEMENT_30,arc.LOADED,arc.IS_MODIFIED,arc.ERROR_MESSAGE,arc.LS_ASSET_OVERRIDE
  from pp_import_run run, ls_import_alloc_rows_archive arc
  where run.import_run_id = arc.import_run_id',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_xlate', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_1', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 1', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_10', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 10', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_11', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 11', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_12', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 12', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_13', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 13', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_14', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 14', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_15', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 15', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_16', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 16', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_17', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 17', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_18', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 18', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_19', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 19', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_2', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 2', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_20', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 20', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_21', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 21', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_22', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 22', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_23', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 23', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_24', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 24', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_25', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 25', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_26', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 26', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_27', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 27', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_28', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 28', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_29', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 29', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_3', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 3', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_30', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 30', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_4', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 4', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_5', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 5', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_6', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 6', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_7', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 7', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_8', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 8', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'element_9', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Element 9', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'error_message', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Error Message', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_xlate', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_description', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Description', 300, 'description',
'(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 261))', 'VARCHAR2', 0, 'description', 1, 2,
'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_modified', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Modified', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'line_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Line Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'loaded', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Loaded', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_override', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Override', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_xlate', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'percent', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Percent', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_local_id', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Local Id', 300, 'description', 'ls_tax_local', 'VARCHAR2', 0,
'tax_local_id', null, null, 'tax_local_id') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_local_xlate', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Local Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'time_stamp', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Time Stamp', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'trans_type', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Trans Type', 300, 'description',
'(select * from je_trans_type where trans_type >= 3000)', 'NUMBER', 0, 'trans_type', null, null, 'trans_type') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'trans_type_xlate', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Trans Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'user_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'User Id', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE OPEN MLA''S WITH NO PAYMENTS CALCULATED FOR MONTH');
delete from pp_any_query_criteria  where upper(description)='LEASE OPEN MLA''S WITH NO PAYMENTS CALCULATED FOR MONTH';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Open MLA''s with no Payments Calculated for Month', to_date('2015-08-12 10:40:41',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'none', 'lessee', 'user',
'
    select distinct (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'') as monthnum,
      lct.description as lease_cap_type,
      lls.description as lease_status, ll.LEASE_ID, LEASE_NUMBER, ll.DESCRIPTION,
      ll.LONG_DESCRIPTION, LESSOR_ID, LEASE_TYPE_ID, ll.LEASE_STATUS_ID,
      APPROVAL_DATE, MASTER_AGREEMENT_DATE, CUT_OFF_DAY, PAYMENT_DUE_DAY, DAYS_IN_BASIS_YEAR,
      PURCHASE_OPTION_NOTC, PURCHASE_OPTION_PCT, RENEWAL_OPTION_NOTIFICATION, CANCEL_NOTIFICATION,
      CANCEL_TERM, LIMIT_ORIG_COST, LIMIT_UNPAID_BAL, ESTIMATED_PAYMENT, PRE_PAYMENT_SW,
      MONTHS_FOR_LOCAL_TAX_BASE, ll.NOTES, LEASE_GROUP_ID, LEASE_CAP_TYPE_ID, INITIATION_DATE,
      ll.CURRENT_REVISION, ll.WORKFLOW_TYPE_ID, DAYS_IN_YEAR, LEASE_END_DATE, DAYS_IN_MONTH_SW
from ls_lease ll, ls_lease_status lls, ls_lease_cap_type lct
where ll.lease_status_id in (2,3,7)
and ll.lease_status_id = lls.lease_status_id
and ll.lease_cap_type_id = lct.ls_lease_cap_type_id
and not exists (select 1
                from ls_payment_hdr
                where lease_id = ll.lease_id
                  and to_char(gl_posting_mo_yr,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM''))
and not exists (select 1
                from ls_monthly_accrual_stg stg, ls_asset la, ls_ilr ilr
                where stg.ls_asset_id = la.ls_asset_id
                  and la.ilr_id = ilr.ilr_id
                  and ilr.lease_id = ll.lease_id
                  and to_char(gl_posting_mo_yr,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM''))',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'approval_date', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Approval Date', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancel_notification', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancel Notification', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancel_term', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancel Term', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_revision', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Current Revision', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cut_off_day', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cut Off Day', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_basis_year', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Basis Year', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_month_sw', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Month Sw', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_year', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Year', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'description', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'estimated_payment', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Estimated Payment', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'initiation_date', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Initiation Date', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type_id', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_end_date', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease End Date', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_group_id', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Group Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_status', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Status', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_status_id', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Status Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_type_id', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Type Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lessor_id', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lessor Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'limit_orig_cost', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Limit Orig Cost', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'limit_unpaid_bal', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Limit Unpaid Bal', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'long_description', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'master_agreement_date', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Master Agreement Date', 300, null, null, 'DATE', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'months_for_local_tax_base', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Months For Local Tax Base', 300, null, null, 'NUMBER', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'notes', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Notes', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_due_day', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payment Due Day', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'pre_payment_sw', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Pre Payment Sw', 300, null, null, 'NUMBER', 1, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_notc', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Notc', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_pct', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Pct', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'renewal_option_notification', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Renewal Option Notification', 300, null, null, 'NUMBER', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'workflow_type_id', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Workflow Type Id', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE PAYMENT HEADER AND INVOICE DETAIL');
delete from pp_any_query_criteria  where upper(description)='LEASE PAYMENT HEADER AND INVOICE DETAIL';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Payment Header and Invoice Detail', to_date('2015-08-17 12:54:36',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'none', 'lessee', 'user',
'select to_char(lph.gl_posting_mo_yr,''yyyymm'') as monthnum,  lph.company_id, lv.description as vendor, aps.description as payment_status,
       lpl.amount as calculated_amount, lpl.adjustment_amount as adjustments, lph.amount as total_payment,
       inv.invoice_number, inv.invoice_amount,
       decode(map.in_tolerance, 1, ''Yes'', null, null, ''No'') as in_tolerance,
       ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       nvl(ilr.ilr_id,-1) as ilr_id, nvl(ilr.ilr_number, ''N/A'') as ilr_number,
       nvl(lfs.description, ''N/A'') as funding_status,
       nvl(la.ls_asset_id, -1) as ls_asset_id, nvl(la.leased_asset_number, ''N/A'') as leased_asset_number,
       co.description as company_description
from ls_payment_hdr lph, ls_asset la, ls_ilr ilr, ls_lease ll,
     company co, ls_lease_cap_type lct, ls_funding_status lfs, ls_vendor lv, approval_status aps,
     ls_invoice_payment_map map, ls_invoice inv,
     (select payment_id, sum(nvl(amount,0)) as amount, sum(nvl(adjustment_amount,0)) as adjustment_amount
      from ls_payment_line
      where to_char(gl_posting_mo_yr,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name)) = ''MONTHNUM'')
      group by payment_id) lpl
where lph.lease_id = ll.lease_id
  and lph.ls_asset_id = la.ls_asset_id (+)
  and lph.ilr_id = ilr.ilr_id (+)
  and ilr.funding_status_id = lfs.funding_status_id (+)
  and co.company_id = lph.company_id
  and ll.lease_cap_type_id = lct.ls_lease_cap_type_id
  and lph.vendor_id = lv.vendor_id
  and lph.payment_status_id = aps.approval_status_id
  and to_char(lph.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name)) = ''MONTHNUM'')
  and lph.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name)) = ''COMPANY ID'')
  and lpl.payment_id = lph.payment_id
  and lph.payment_id = map.payment_id (+)
  and map.invoice_id = inv.invoice_id (+)',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'adjustments', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Adjustments', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'calculated_amount', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Calculated Amount', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'funding_status', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Funding Status', 300, 'description', 'ls_funding_status', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'in_tolerance', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'In Tolerance', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'invoice_amount', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Invoice Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'invoice_number', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Invoice Number', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_status', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Payment Status', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'total_payment', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Total Payment', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'vendor', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Vendor', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE PAYMENT DETAIL BY ASSET');
delete from pp_any_query_criteria  where upper(description)='LEASE PAYMENT DETAIL BY ASSET';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Payment Detail by Asset', to_date('2015-08-12 11:25:26', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select la.ls_asset_id, to_char(lpl.gl_posting_mo_yr, ''yyyymm'') as monthnum, co.company_id, co.description as company_description,
       la.leased_asset_number, ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
  nvl(sum(decode(lpl.payment_type_id, 1, lpl.amount, 0)), 0) principal_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.amount, 0)), 0) interest_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.adjustment_amount, 0)), 0) interest_adjustment,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.amount, 0)), 0) executory_payment1,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.adjustment_amount, 0)), 0) executory_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.amount, 0)), 0) executory_payment2,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.adjustment_amount, 0)), 0) executory_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.amount, 0)), 0) executory_payment3,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.adjustment_amount, 0)), 0) executory_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.amount, 0)), 0) executory_payment4,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.adjustment_amount, 0)), 0) executory_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.amount, 0)), 0) executory_payment5,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.adjustment_amount, 0)), 0) executory_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.amount, 0)), 0) executory_payment6,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.adjustment_amount, 0)), 0) executory_adjustment6,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.amount, 0)), 0) executory_payment7,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.adjustment_amount, 0)), 0) executory_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.amount, 0)), 0) executory_payment8,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.adjustment_amount, 0)), 0) executory_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.amount, 0)), 0) executory_payment9,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.adjustment_amount, 0)), 0) executory_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.amount, 0)), 0) executory_payment10,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.adjustment_amount, 0)), 0) executory_adjustment10,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.amount, 0)), 0) contingent_payment1,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.adjustment_amount, 0)), 0) contingent_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.amount, 0)), 0) contingent_payment2,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.adjustment_amount, 0)), 0) contingent_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.amount, 0)), 0) contingent_payment3,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.adjustment_amount, 0)), 0) contingent_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.amount, 0)), 0) contingent_payment4,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.adjustment_amount, 0)), 0) contingent_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.amount, 0)), 0) contingent_payment5,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.adjustment_amount, 0)), 0) contingent_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.amount, 0)), 0) contingent_payment6,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.adjustment_amount, 0)), 0) contingent_adjustment6,                              ',

'
	nvl(sum(decode(lpl.payment_type_id, 19, lpl.amount, 0)), 0) contingent_payment7,
	nvl(sum(decode(lpl.payment_type_id, 19, lpl.adjustment_amount, 0)), 0) contingent_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.amount, 0)), 0) contingent_payment8,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.adjustment_amount, 0)), 0) contingent_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.amount, 0)), 0) contingent_payment9,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.adjustment_amount, 0)), 0) contingent_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.amount, 0)), 0) contingent_payment10,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.adjustment_amount, 0)), 0) contingent_adjustment10,
  nvl(sum(decode(lpl.payment_type_id, 23, lpl.amount, 0)), 0) termination_penalty,
  nvl(sum(decode(lpl.payment_type_id, 24, lpl.amount, 0)), 0) sales_proceeds,
  nvl(tax_1,0) as tax_1, nvl(tax_1_adjustment,0) as tax_1_adjustment,
  nvl(tax_2,0) as tax_2, nvl(tax_2_adjustment,0) as tax_2_adjustment,
  nvl(tax_3,0) as tax_3, nvl(tax_3_adjustment,0) as tax_3_adjustment,
  nvl(tax_4,0) as tax_4, nvl(tax_4_adjustment,0) as tax_4_adjustment,
  nvl(tax_5,0) as tax_5, nvl(tax_5_adjustment,0) as tax_5_adjustment,
  nvl(tax_6,0) as tax_6, nvl(tax_6_adjustment,0) as tax_6_adjustment,
  nvl(tax_7,0) as tax_7, nvl(tax_7_adjustment,0) as tax_7_adjustment
from ls_asset la, company co, ls_payment_line lpl, asset_location al, ls_ilr ilr, ls_lease ll, ls_ilr_options ilro, ls_lease_cap_type lct,
      (select lmt.ls_asset_id,
        nvl(sum(decode(tax_local_id, 1, amount, 0)),0) as tax_1,
        nvl(sum(decode(tax_local_id, 1, adjustment_amount, 0)),0) as tax_1_adjustment,
        nvl(sum(decode(tax_local_id, 2, amount, 0)),0) as tax_2,
        nvl(sum(decode(tax_local_id, 2, adjustment_amount, 0)),0) as tax_2_adjustment,
        nvl(sum(decode(tax_local_id, 3, amount, 0)),0) as tax_3,
        nvl(sum(decode(tax_local_id, 3, adjustment_amount, 0)),0) as tax_3_adjustment,
        nvl(sum(decode(tax_local_id, 4, amount, 0)),0) as tax_4,
        nvl(sum(decode(tax_local_id, 4, adjustment_amount, 0)),0) as tax_4_adjustment,
        nvl(sum(decode(tax_local_id, 5, amount, 0)),0) as tax_5,
        nvl(sum(decode(tax_local_id, 5, adjustment_amount, 0)),0) as tax_5_adjustment,
        nvl(sum(decode(tax_local_id, 6, amount, 0)),0) as tax_6,
        nvl(sum(decode(tax_local_id, 6, adjustment_amount, 0)),0) as tax_6_adjustment,
        nvl(sum(decode(tax_local_id, 7, amount, 0)),0) as tax_7,
        nvl(sum(decode(tax_local_id, 7, adjustment_amount, 0)),0) as tax_7_adjustment
       from ls_monthly_tax lmt, ls_asset la
       where to_char(gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
          and lmt.accrual = 0
          and lmt.ls_asset_id = la.ls_asset_id
          and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
       group by lmt.ls_asset_id) taxes
where la.company_id = co.company_id
  and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
  and to_char(lpl.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
  and lpl.ls_asset_id = la.ls_asset_id
  and la.asset_location_id = al.asset_location_id
  and la.ilr_id = ilr.ilr_id   and ilr.lease_id = ll.lease_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.ls_asset_id = taxes.ls_asset_id (+)
group by la.ls_asset_id, lpl.gl_posting_mo_yr, co.company_id, co.description, la.leased_asset_number,
         ilr.ilr_id, ilr.ilr_number, lct.description, al.',

'long_description, ll.lease_id, ll.lease_number,
         tax_1, tax_1_adjustment, tax_2, tax_2_adjustment, tax_3, tax_3_adjustment, tax_4, tax_4_adjustment,
         tax_5, tax_5_adjustment, tax_6, tax_6_adjustment, tax_7, tax_7_adjustment',
 null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment1', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment1', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment10', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment10', 300, null, null, 'NUMBER', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment2', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment2', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment3', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment3', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment4', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment4', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment5', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment5', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment6', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment6', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment7', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment7', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment8', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment8', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment9', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment9', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment1', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment10', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment10', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment2', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment3', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment4', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment5', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment6', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment7', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment8', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment9', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment1', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment1', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment10', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment10', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment2', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment2', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment3', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment3', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment4', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment4', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment5', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment5', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment6', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment6', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment7', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment7', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment8', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment8', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment9', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment9', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment1', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment10', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment2', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment3', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment4', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment5', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment6', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment7', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment8', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment9', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_adjustment', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Adjustment', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_payment', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Payment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_payment', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Payment', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sales_proceeds', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Sales Proceeds', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_1', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_1_adjustment', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 1 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_2', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_2_adjustment', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 2 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_3', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_3_adjustment', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 3 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_4', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_4_adjustment', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 4 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_5', 65, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_5_adjustment', 66, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 5 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_6', 67, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_6_adjustment', 68, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 6 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_7', 69, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_7_adjustment', 70, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 7 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'termination_penalty', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Termination Penalty', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE RETIREMENTS - ACTUAL');
delete from pp_any_query_criteria  where upper(description)='LEASE RETIREMENTS - ACTUAL';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Retirements - Actual', to_date('2015-08-17 13:06:45', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select to_char(las.month,''yyyymm'') as monthnum, las.set_of_books_id, co.company_id, la.ls_asset_id, la.leased_asset_number, lst.description as leased_asset_status, ilr.ilr_id,
       ilr.ilr_number, lfs.description as funding_status, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       co.description as company_description,
       cpra.activity_cost as retirement_amount,
       las.beg_capital_cost, las.end_capital_cost,
       decode(las.is_om, 0, las.beg_obligation,0) as beg_obligation, decode(las.is_om, 0, las.end_obligation,0) as end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation,
       cprd.beg_reserve_month as beg_reserve, cprd.depr_reserve as end_reserve,
       cprd.beg_asset_dollars - cprd.beg_reserve_month - decode(las.is_om,0, las.beg_obligation,0) as gain_loss
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, ls_lease_cap_type lct, ls_ilr_options ilro,
     ls_funding_status lfs, ls_asset_status lst, ls_asset_schedule las, ls_cpr_asset_map map, cpr_activity cpra, cpr_depr cprd
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.funding_status_id = lfs.funding_status_id
  and la.company_id = co.company_id
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.ls_asset_status_id = lst.ls_asset_status_id
  and la.ls_asset_id = las.ls_asset_id
  and la.approved_revision = las.revision
  and to_char(las.month,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'')
  and map.ls_asset_id = la.ls_asset_id
  and map.asset_id = cpra.asset_id
  and trim(cpra.activity_code) = ''URGL''
  and to_char(cpra.gl_posting_mo_yr,''yyyymm'') in  (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'')
  and cpra.asset_id = cprd.asset_id
  and cpra.gl_posting_mo_yr = cprd.gl_posting_mo_yr
  and las.set_of_books_id = cprd.set_of_books_id',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 16, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 20, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 18, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_reserve', 22, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Reserve', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 14, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 4, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 17, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 21, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 19, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_reserve', 23, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Reserve', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'funding_status', 10, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Funding Status', 300, 'description', 'ls_funding_status', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'gain_loss', 24, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Gain Loss', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 8, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 9, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 13, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 11, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 12, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 6, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_status', 7, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Status', 300, 'description', 'ls_asset_status', 'VARCHAR2',
 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 5, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'retirement_amount', 15, to_date('2015-08-17 13:06:31', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Retirement Amount', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books_id', 3, to_date('2015-08-17 13:06:45', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 1, null, 'Set of Books ID', 300, null, null, 'VARCHAR2', null, null, null, null,
null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE PAYMENT DETAIL BY COMPANY');
delete from pp_any_query_criteria  where upper(description)='LEASE PAYMENT DETAIL BY COMPANY';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Payment Detail by Company', to_date('2015-08-13 09:56:25', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select to_char(lpl.gl_posting_mo_yr, ''yyyymm'') as monthnum, co.company_id, co.description as company_description, lct.description lease_cap_type,
nvl(sum(decode(lpl.payment_type_id, 1, lpl.amount, 0)), 0) principal_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.amount, 0)), 0) interest_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.adjustment_amount, 0)), 0) interest_adjustment,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.amount, 0)), 0) executory_payment1,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.adjustment_amount, 0)), 0) executory_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.amount, 0)), 0) executory_payment2,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.adjustment_amount, 0)), 0) executory_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.amount, 0)), 0) executory_payment3,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.adjustment_amount, 0)), 0) executory_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.amount, 0)), 0) executory_payment4,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.adjustment_amount, 0)), 0) executory_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.amount, 0)), 0) executory_payment5,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.adjustment_amount, 0)), 0) executory_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.amount, 0)), 0) executory_payment6,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.adjustment_amount, 0)), 0) executory_adjustment6,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.amount, 0)), 0) executory_payment7,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.adjustment_amount, 0)), 0) executory_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.amount, 0)), 0) executory_payment8,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.adjustment_amount, 0)), 0) executory_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.amount, 0)), 0) executory_payment9,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.adjustment_amount, 0)), 0) executory_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.amount, 0)), 0) executory_payment10,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.adjustment_amount, 0)), 0) executory_adjustment10,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.amount, 0)), 0) contingent_payment1,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.adjustment_amount, 0)), 0) contingent_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.amount, 0)), 0) contingent_payment2,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.adjustment_amount, 0)), 0) contingent_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.amount, 0)), 0) contingent_payment3,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.adjustment_amount, 0)), 0) contingent_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.amount, 0)), 0) contingent_payment4,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.adjustment_amount, 0)), 0) contingent_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.amount, 0)), 0) contingent_payment5,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.adjustment_amount, 0)), 0) contingent_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.amount, 0)), 0) contingent_payment6,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.adjustment_amount, 0)), 0) contingent_adjustment6,
	nvl(sum(decode(lpl.payment_type_id, 19, lpl.amount, 0)), 0) contingent_payment7,
	nvl(sum(decode(lpl.payment_type_id, 19, lpl.adju',

'stment_amount, 0)), 0) contingent_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.amount, 0)), 0) contingent_payment8,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.adjustment_amount, 0)), 0) contingent_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.amount, 0)), 0) contingent_payment9,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.adjustment_amount, 0)), 0) contingent_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.amount, 0)), 0) contingent_payment10,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.adjustment_amount, 0)), 0) contingent_adjustment10,
  nvl(sum(decode(lpl.payment_type_id, 23, lpl.amount, 0)), 0) termination_penalty,
  nvl(sum(decode(lpl.payment_type_id, 24, lpl.amount, 0)), 0) sales_proceeds,
  sum(nvl(tax_1,0)) as tax_1, sum(nvl(tax_1_adjustment,0)) as tax_1_adjustment,
  sum(nvl(tax_2,0)) as tax_2, sum(nvl(tax_2_adjustment,0)) as tax_2_adjustment,
  sum(nvl(tax_3,0)) as tax_3, sum(nvl(tax_3_adjustment,0)) as tax_3_adjustment,
  sum(nvl(tax_4,0)) as tax_4, sum(nvl(tax_4_adjustment,0)) as tax_4_adjustment,
  sum(nvl(tax_5,0)) as tax_5, sum(nvl(tax_5_adjustment,0)) as tax_5_adjustment,
  sum(nvl(tax_6,0)) as tax_6, sum(nvl(tax_6_adjustment,0)) as tax_6_adjustment,
  sum(nvl(tax_7,0)) as tax_7, sum(nvl(tax_7_adjustment,0)) as tax_7_adjustment
from ls_asset la, company co, ls_payment_line lpl, asset_location al, ls_ilr ilr, ls_lease ll, ls_ilr_options ilro, ls_lease_cap_type lct,
      (select lmt.ls_asset_id,
        nvl(sum(decode(tax_local_id, 1, amount, 0)),0) as tax_1,
        nvl(sum(decode(tax_local_id, 1, adjustment_amount, 0)),0) as tax_1_adjustment,
        nvl(sum(decode(tax_local_id, 2, amount, 0)),0) as tax_2,
        nvl(sum(decode(tax_local_id, 2, adjustment_amount, 0)),0) as tax_2_adjustment,
        nvl(sum(decode(tax_local_id, 3, amount, 0)),0) as tax_3,
        nvl(sum(decode(tax_local_id, 3, adjustment_amount, 0)),0) as tax_3_adjustment,
        nvl(sum(decode(tax_local_id, 4, amount, 0)),0) as tax_4,
        nvl(sum(decode(tax_local_id, 4, adjustment_amount, 0)),0) as tax_4_adjustment,
        nvl(sum(decode(tax_local_id, 5, amount, 0)),0) as tax_5,
        nvl(sum(decode(tax_local_id, 5, adjustment_amount, 0)),0) as tax_5_adjustment,
        nvl(sum(decode(tax_local_id, 6, amount, 0)),0) as tax_6,
        nvl(sum(decode(tax_local_id, 6, adjustment_amount, 0)),0) as tax_6_adjustment,
        nvl(sum(decode(tax_local_id, 7, amount, 0)),0) as tax_7,
        nvl(sum(decode(tax_local_id, 7, adjustment_amount, 0)),0) as tax_7_adjustment
       from ls_monthly_tax lmt, ls_asset la
       where to_char(gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
          and lmt.accrual = 0
          and lmt.ls_asset_id = la.ls_asset_id
          and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
       group by lmt.ls_asset_id) taxes
where la.company_id = co.company_id
  and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
  and to_char(lpl.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
  and lpl.ls_asset_id = la.ls_asset_id
  and la.asset_location_id = al.asset_location_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.ls_asset_id = taxes.ls_asset_id (+)
group by lpl.gl_posting_mo_yr, co.company_id, co.description, lct.description
',
 null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment1', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment1', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment10', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment10', 300, null, null, 'NUMBER', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment2', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment2', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment3', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment3', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment4', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment4', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment5', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment5', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment6', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment6', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment7', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment7', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment8', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment8', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment9', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment9', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment1', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment10', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment10', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment2', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment3', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment4', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment5', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment6', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment7', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment8', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment9', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment1', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment1', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment10', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment10', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment2', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment2', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment3', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment3', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment4', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment4', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment5', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment5', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment6', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment6', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment7', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment7', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment8', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment8', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment9', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment9', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment1', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment10', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment2', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment3', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment4', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment5', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment6', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment7', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment8', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment9', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_adjustment', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Adjustment', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_payment', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Payment', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_payment', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Payment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sales_proceeds', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Sales Proceeds', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_1', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_1_adjustment', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 1 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_2', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_2_adjustment', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 2 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_3', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_3_adjustment', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 3 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_4', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_4_adjustment', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 4 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_5', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_5_adjustment', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 5 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_6', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_6_adjustment', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 6 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_7', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_7_adjustment', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 7 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'termination_penalty', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Termination Penalty', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE RETIREMENTS - EXPECTED');
delete from pp_any_query_criteria  where upper(description)='LEASE RETIREMENTS - EXPECTED';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Retirements - Expected', to_date('2015-08-17 13:08:49', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select to_char(las.month,''yyyymm'') as monthnum, las.set_of_books_id, co.company_id, la.ls_asset_id, la.leased_asset_number, lst.description as leased_asset_status, ilr.ilr_id,
       ilr.ilr_number, lfs.description as funding_status, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       co.description as company_description,
       las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, ls_lease_cap_type lct, ls_ilr_options ilro,
     ls_funding_status lfs, ls_asset_status lst, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.funding_status_id = lfs.funding_status_id
  and la.company_id = co.company_id
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.ls_asset_status_id = lst.ls_asset_status_id
  and la.ls_asset_id = las.ls_asset_id
  and la.approved_revision = las.revision
  and las.month in (select max(month) from ls_asset_schedule where ls_asset_id = la.ls_asset_id and revision = la.approved_revision)
  and to_char(las.month,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'')
  ',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 14, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 18, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 16, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 13, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 3, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 15, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 19, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 17, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'funding_status', 9, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Funding Status', 300, 'description', 'ls_funding_status', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 7, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 8, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 12, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 10, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 11, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 5, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_status', 6, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Status', 300, 'description', 'ls_asset_status', 'VARCHAR2',
 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 4, to_date('2015-08-17 13:08:08', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books_id', 2, to_date('2015-08-17 13:08:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 1, null, 'Set of Books ID', 300, null, null, 'VARCHAR2', null, null, null, null,
null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE PAYMENT DETAIL BY LOCATION');
delete from pp_any_query_criteria  where upper(description)='LEASE PAYMENT DETAIL BY LOCATION';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Payment Detail by Location', to_date('2015-08-13 10:03:19', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select to_char(lpl.gl_posting_mo_yr, ''yyyymm'') as monthnum, co.company_id, co.description as company_description,
  lct.description as lease_cap_type, al.long_description as location,
  nvl(sum(decode(lpl.payment_type_id, 1, lpl.amount, 0)), 0) principal_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.amount, 0)), 0) interest_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.adjustment_amount, 0)), 0) interest_adjustment,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.amount, 0)), 0) executory_payment1,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.adjustment_amount, 0)), 0) executory_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.amount, 0)), 0) executory_payment2,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.adjustment_amount, 0)), 0) executory_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.amount, 0)), 0) executory_payment3,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.adjustment_amount, 0)), 0) executory_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.amount, 0)), 0) executory_payment4,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.adjustment_amount, 0)), 0) executory_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.amount, 0)), 0) executory_payment5,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.adjustment_amount, 0)), 0) executory_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.amount, 0)), 0) executory_payment6,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.adjustment_amount, 0)), 0) executory_adjustment6,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.amount, 0)), 0) executory_payment7,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.adjustment_amount, 0)), 0) executory_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.amount, 0)), 0) executory_payment8,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.adjustment_amount, 0)), 0) executory_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.amount, 0)), 0) executory_payment9,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.adjustment_amount, 0)), 0) executory_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.amount, 0)), 0) executory_payment10,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.adjustment_amount, 0)), 0) executory_adjustment10,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.amount, 0)), 0) contingent_payment1,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.adjustment_amount, 0)), 0) contingent_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.amount, 0)), 0) contingent_payment2,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.adjustment_amount, 0)), 0) contingent_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.amount, 0)), 0) contingent_payment3,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.adjustment_amount, 0)), 0) contingent_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.amount, 0)), 0) contingent_payment4,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.adjustment_amount, 0)), 0) contingent_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.amount, 0)), 0) contingent_payment5,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.adjustment_amount, 0)), 0) contingent_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.amount, 0)), 0) contingent_payment6,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.adjustment_amount, 0)), 0) contingent_adjustment6,
	nvl(sum(decode(lpl.payment_type_id, 19, lpl.amount, 0)), 0) contingent_payment7,
	nvl(sum(d',

'ecode(lpl.payment_type_id, 19, lpl.adjustment_amount, 0)), 0) contingent_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.amount, 0)), 0) contingent_payment8,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.adjustment_amount, 0)), 0) contingent_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.amount, 0)), 0) contingent_payment9,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.adjustment_amount, 0)), 0) contingent_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.amount, 0)), 0) contingent_payment10,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.adjustment_amount, 0)), 0) contingent_adjustment10,
  nvl(sum(decode(lpl.payment_type_id, 23, lpl.amount, 0)), 0) termination_penalty,
  nvl(sum(decode(lpl.payment_type_id, 24, lpl.amount, 0)), 0) sales_proceeds,
  sum(nvl(tax_1,0)) as tax_1, sum(nvl(tax_1_adjustment,0)) as tax_1_adjustment,
  sum(nvl(tax_2,0)) as tax_2, sum(nvl(tax_2_adjustment,0)) as tax_2_adjustment,
  sum(nvl(tax_3,0)) as tax_3, sum(nvl(tax_3_adjustment,0)) as tax_3_adjustment,
  sum(nvl(tax_4,0)) as tax_4, sum(nvl(tax_4_adjustment,0)) as tax_4_adjustment,
  sum(nvl(tax_5,0)) as tax_5, sum(nvl(tax_5_adjustment,0)) as tax_5_adjustment,
  sum(nvl(tax_6,0)) as tax_6, sum(nvl(tax_6_adjustment,0)) as tax_6_adjustment,
  sum(nvl(tax_7,0)) as tax_7, sum(nvl(tax_7_adjustment,0)) as tax_7_adjustment
from ls_asset la, company co, ls_payment_line lpl, asset_location al, ls_ilr ilr, ls_lease ll, ls_ilr_options ilro, ls_lease_cap_type lct,
      (select lmt.ls_asset_id,
        nvl(sum(decode(tax_local_id, 1, amount, 0)),0) as tax_1,
        nvl(sum(decode(tax_local_id, 1, adjustment_amount, 0)),0) as tax_1_adjustment,
        nvl(sum(decode(tax_local_id, 2, amount, 0)),0) as tax_2,
        nvl(sum(decode(tax_local_id, 2, adjustment_amount, 0)),0) as tax_2_adjustment,
        nvl(sum(decode(tax_local_id, 3, amount, 0)),0) as tax_3,
        nvl(sum(decode(tax_local_id, 3, adjustment_amount, 0)),0) as tax_3_adjustment,
        nvl(sum(decode(tax_local_id, 4, amount, 0)),0) as tax_4,
        nvl(sum(decode(tax_local_id, 4, adjustment_amount, 0)),0) as tax_4_adjustment,
        nvl(sum(decode(tax_local_id, 5, amount, 0)),0) as tax_5,
        nvl(sum(decode(tax_local_id, 5, adjustment_amount, 0)),0) as tax_5_adjustment,
        nvl(sum(decode(tax_local_id, 6, amount, 0)),0) as tax_6,
        nvl(sum(decode(tax_local_id, 6, adjustment_amount, 0)),0) as tax_6_adjustment,
        nvl(sum(decode(tax_local_id, 7, amount, 0)),0) as tax_7,
        nvl(sum(decode(tax_local_id, 7, adjustment_amount, 0)),0) as tax_7_adjustment
       from ls_monthly_tax lmt, ls_asset la
       where to_char(gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
          and lmt.accrual = 0
          and lmt.ls_asset_id = la.ls_asset_id
          and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
       group by lmt.ls_asset_id) taxes
where la.company_id = co.company_id
  and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
  and to_char(lpl.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
  and lpl.ls_asset_id = la.ls_asset_id
  and la.asset_location_id = al.asset_location_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.ls_asset_id = taxes.ls_asset_id (+)
group by lpl.gl_posting_mo_yr, co.company_id, co.description, lct.description, al.long_description',
 null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment1', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment1', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment10', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment10', 300, null, null, 'NUMBER', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment2', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment2', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment3', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment3', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment4', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment4', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment5', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment5', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment6', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment6', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment7', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment7', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment8', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment8', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_adjustment9', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Adjustment9', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment1', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment10', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment10', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment2', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment3', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment4', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment5', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment6', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment7', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment8', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_payment9', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Payment9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment1', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment1', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment10', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment10', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment2', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment2', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment3', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment3', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment4', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment4', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment5', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment5', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment6', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment6', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment7', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment7', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment8', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment8', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_adjustment9', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Adjustment9', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment1', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment10', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment2', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment3', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment4', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment5', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment6', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment7', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment8', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_payment9', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Payment9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_adjustment', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Adjustment', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_payment', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Payment', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_payment', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Payment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sales_proceeds', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Sales Proceeds', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_1', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_1_adjustment', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 1 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_2', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_2_adjustment', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 2 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_3', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_3_adjustment', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 3 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_4', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_4_adjustment', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 4 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_5', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_5_adjustment', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 5 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_6', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_6_adjustment', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 6 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_7', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_7_adjustment', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax 7 Adjustment', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'termination_penalty', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Termination Penalty', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE TAX DETAIL BY DISTRICT');
delete from pp_any_query_criteria  where upper(description)='LEASE TAX DETAIL BY DISTRICT';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Tax Detail by District', to_date('2015-08-17 13:19:29', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select la.ls_asset_id, la.leased_asset_number, tax.set_of_books_id, to_char(tax.gl_posting_mo_yr,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.EXT_ASSET_LOCATION as external_location_code,county.description as county, al.state_id as state,
       ltl.description as local_tax_type, decode(is_number(tax.tax_district_id), -1, tax.tax_district_id, td.description) as tax_district,
       ltl.pay_lessor,
       tax.tax_base as tax_base, tax.rate,
       nvl(tax.amount,0) as calc_amount, nvl(tax.adjustment_amount,0) as adjustment_amount,
       nvl(tax.amount,0) + nvl(tax.adjustment_amount,0) as total_tax
from ls_asset la, ls_ilr ilr, ls_lease ll,
     ls_monthly_tax tax, ls_tax_local ltl, company co,
     tax_district td, asset_location al, county,
     ls_ilr_options ilro, ls_lease_cap_type lct
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and tax.ls_asset_id = la.ls_asset_id
  and tax.accrual = 0
  and tax.tax_local_id = ltl.tax_local_id
  and tax.amount <> 0 and nvl(tax.rate,0) <> 0
  and case when is_number(tax.tax_district_id) = 1 then to_number(tax.tax_district_id) else -2 end = td.tax_district_id (+) /* outer join because converted data didn''t have tax district detail */
  and la.company_id = co.company_id
  and la.asset_location_id = al.asset_location_id
  and al.county_id = county.county_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  and to_char(tax.gl_posting_mo_yr,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'')
order by 1

',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'adjustment_amount', 23, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Adjustment Amount', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'asset_description', 5, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Asset Description', 300, 'description', 'ls_asset', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'calc_amount', 22, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Calc Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 7, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 6, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'county', 15, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'County', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'external_location_code', 14, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'External Location Code', 300, 'ext_asset_location',
'asset_location', 'VARCHAR2', 0, 'EXT_ASSET_LOCATION', null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 8, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 9, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 12, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 10, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 11, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'local_tax_type', 17, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Local Tax Type', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 13, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 4, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'pay_lessor', 19, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Pay Lessor', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'rate', 21, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Rate', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books_id', 3, to_date('2015-08-17 13:19:29', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 1, null, 'Set of Books ID', 300, null, null, 'VARCHAR2', null, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'state', 16, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'State', 300, 'state_id', 'state', 'VARCHAR2', 0, 'state_id', null, null, 'state_id')
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_base', 20, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Tax Base', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_district', 18, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax District', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'total_tax', 24, to_date('2015-08-17 13:19:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Total Tax', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE TRANSFER DETAIL');
delete from pp_any_query_criteria  where upper(description)='LEASE TRANSFER DETAIL';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Transfer Detail', to_date('2015-08-17 13:26:05', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
'none', 'lessee', 'user',
'select to_char(trf.month,''yyyymm'') as monthnum,
       la1.ls_asset_id as from_ls_asset_id,
       las1.set_of_books_id,
       lct.description as lease_cap_type,
       la1.leased_asset_number as from_leased_asset_number,
       ilr1.ilr_id as from_ilr_id, ilr1.ilr_number as from_ilr_number, co1.company_id as from_company_id,
       co1.description as from_company_description,
       al1.long_description as from_location, dg1.mid_period_method as from_depreciation_type,
       la2.ls_asset_id as to_ls_asset_id, la2.leased_asset_number as to_leased_asset_number,
       ilr2.ilr_id as to_ilr_id, ilr2.ilr_number as to_ilr_number,
       ll.lease_id, ll.lease_number,
       co2.company_id as to_company_id, co2.description as to_company_description,
       al2.long_description as to_location, dg2.mid_period_method as to_depreciation_type,
       las2.current_lease_cost,
       decode(las1.is_om,0, las1.beg_obligation,0) as from_beg_obligation,
       decode(las1.is_om,0, las1.end_obligation,0) as from_end_obligation,
       decode(las1.is_om,0, las1.beg_lt_obligation,0) as from_beg_lt_obligation,
       decode(las1.is_om,0, las1.end_lt_obligation,0) as from_end_lt_obligation,
       decode(las1.is_om,0, las1.beg_capital_cost,0) as from_begin_capital_cost,
       decode(las1.is_om,0, las1.end_capital_cost,0) as from_end_capital_cost,
       cprd1.beg_reserve_month, cprd1.reserve_trans_out,
       0 as to_beg_obligation,
       decode(las2.is_om,0, las2.end_obligation,0) as to_end_obligation,
       0 as to_beg_lt_obligation,
       decode(las2.is_om,0, las2.end_lt_obligation,0) as to_end_lt_obligation,
       0 as to_begin_capital_cost,
       decode(las2.is_om,0, las2.end_capital_cost,0) as to_end_capital_cost,
       cprd2.reserve_trans_in,
       cprd2.depr_exp_alloc_adjust as depreciation_true_up
from ls_asset la1, ls_asset la2, ls_ilr ilr1, ls_ilr ilr2, ls_lease ll,
     ls_asset_schedule las1, ls_asset_schedule las2, ls_asset_transfer_history trf, ls_lease_cap_type lct,
     company co1, company co2, cpr_depr cprd1, ls_cpr_asset_map map1,
     cpr_depr cprd2, depr_group dg1, depr_group dg2, ls_cpr_asset_map map2,
     asset_location al1, asset_location al2
where la1.ls_asset_id = las1.ls_asset_id
  and la1.approved_revision = las1.revision
  and la2.ls_asset_id = las2.ls_asset_id
  and la2.approved_revision = las2.revision
  and la1.ilr_id = ilr1.ilr_id
  and la2.ilr_id = ilr2.ilr_id
  and ilr1.lease_id = ll.lease_id
  and ll.lease_cap_type_id = lct.ls_lease_cap_type_id
  and trf.from_ls_asset_id = la1.ls_asset_id
  and trf.to_ls_asset_id = la2.ls_asset_id
  and las1.month = las2.month
  and las1.month = trf.month
  and la1.company_id = co1.company_id
  and la2.company_id = co2.company_id
  and la1.ls_asset_id = map1.ls_asset_id
  and map1.asset_id = cprd1.asset_id (+)
  and cprd1.gl_posting_mo_yr = las1.month (+)
  and cprd1.depr_group_id = dg1.depr_group_id (+)
  and cprd1.set_of_books_id = las1.set_of_books_id (+)
  and la2.ls_asset_id = map2.ls_asset_id
  and map2.asset_id = cprd2.asset_id (+)
  and cprd2.gl_posting_mo_yr = las2.month (+)
  and cprd2.depr_group_id = dg2.depr_group_id (+)
  and cprd2.set_of_books_id = las2.set_of_books_id (+)
  and las1.set_of_books_id = las2.set_of_books_id
  and al1.asset_location_id = la1.asset_location_id
  and al2.asset_location_id = la2.asset_location_id
  and to_char(trf.month, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name)) = ''MONTHNUM'')',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_reserve_month', 31, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Reserve Month', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_lease_cost', 24, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Current Lease Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'depreciation_true_up', 40, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Depreciation True Up', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_beg_lt_obligation', 27, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'From Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_beg_obligation', 25, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'From Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_begin_capital_cost', 29, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'From Begin Capital Cost', 300, null, null, 'NUMBER', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_company_description', 9, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_company_id', 8, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_depreciation_type', 12, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Depreciation Type', 300, null, null, 'VARCHAR2', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_end_capital_cost', 30, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'From End Capital Cost', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_end_lt_obligation', 28, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'From End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_end_obligation', 26, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'From End Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_ilr_id', 6, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_ilr_number', 7, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number',
null, null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_leased_asset_number', 5, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_location', 11, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'from_ls_asset_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'From Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 4, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 17, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 18, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
 null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'reserve_trans_in', 39, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reserve Trans In', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'reserve_trans_out', 32, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Reserve Trans Out', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books_id', 3, to_date('2015-08-17 13:25:35', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 1, null, 'Set of Books ID', 300, null, null, 'VARCHAR2', null, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_beg_lt_obligation', 35, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'To Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_beg_obligation', 33, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'To Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_begin_capital_cost', 37, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'To Begin Capital Cost', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_company_description', 20, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_company_id', 19, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_depreciation_type', 23, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Depreciation Type', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_end_capital_cost', 38, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'To End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_end_lt_obligation', 36, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'To End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_end_obligation', 34, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'To End Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_ilr_id', 15, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_ilr_number', 16, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number',
null, null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_leased_asset_number', 14, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_location', 22, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'to_ls_asset_id', 13, to_date('2015-08-17 13:25:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'To Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE ASSET ADDITION DETAIL');
delete from pp_any_query_criteria  where upper(description)='LEASE ASSET ADDITION DETAIL';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Asset Addition Detail', to_date('2015-08-11 08:44:20', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select las.ls_asset_id, la.leased_asset_number, al.long_description as location, to_char(las.month, ''yyyymm'') as monthnum, co.company_id, co.description as company_description,
       lct.description as lease_cap_type, cpra.activity_code,
       cpra.total_adds as total_adds, sob.description as set_of_books, las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation, las.beg_lt_obligation, las.end_lt_obligation, las.current_lease_cost
from ls_asset_schedule las, ls_asset la, asset_location al, ls_cpr_asset_map map, company co, ls_ilr_options ilro, ls_lease_cap_type lct, set_of_books sob,
     (select a.asset_id, a.gl_posting_mo_yr, a.activity_code, sum(activity_cost) as total_adds
      from cpr_activity a, ls_cpr_asset_map m, cpr_ledger cpr
      where to_char(a.gl_posting_mo_yr,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
        and a.asset_id = cpr.asset_id
        and cpr.asset_id = m.asset_id
        and cpr.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
        and a.ferc_activity_code in (1,3)
        and a.activity_cost <> 0
      group by a.asset_id, a.activity_code, a.gl_posting_mo_yr) cpra
where las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and la.asset_location_id = al.asset_location_id
  and la.ls_asset_id = map.ls_asset_id
  and to_char(las.month,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
  and la.company_id = co.company_id
  and co.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
  and cpra.asset_id = map.asset_id
  and cpra.gl_posting_mo_yr = las.month
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and las.set_of_books_id = sob.set_of_books_id ',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'activity_code', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Activity Code', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', 1, 2, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_lease_cost', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Current Lease Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', 1, 2,
'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', null, 1, null, 'Set of Books', 300, 'description', 'set_of_books', 'VARCHAR2', null,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'total_adds', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Total Adds', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE ASSET SCHEDULE BY ILR');
delete from pp_any_query_criteria  where upper(description)='LEASE ASSET SCHEDULE BY ILR';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Asset Schedule by ILR', to_date('2015-08-11 09:04:01', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT', 'none', 'lessee', 'user',
'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2,  las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10,  las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,  las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(ilr.ilr_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''ILR NUMBER'')',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_capital_cost', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_lt_obligation', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'beg_obligation', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Beg Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'bpo_price', 68, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Bpo Price', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_description', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Description', 300, 'description',
'(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual1', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual10', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual10', 300, null, null, 'NUMBER', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual2', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual3', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual4', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual5', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual6', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual7', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual8', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_accrual9', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Accrual9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid1', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid1', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid10', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid2', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid2', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid3', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid3', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid4', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid4', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid5', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid5', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid6', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid6', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid7', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid7', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid8', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid8', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_paid9', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Paid9', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'current_lease_cost', 65, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Current Lease Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_capital_cost', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Capital Cost', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_lt_obligation', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Lt Obligation', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'end_obligation', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'End Obligation', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual1', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual1', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual10', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual10', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual2', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual2', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual3', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual3', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual4', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual4', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual5', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual5', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual6', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual6', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual7', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual7', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual8', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual8', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_accrual9', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Accrual9', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid1', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid10', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid10', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid2', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid3', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid4', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid5', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid6', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid7', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid8', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'executory_paid9', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Executory Paid9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', 1, 2,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_accrual', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Accrual', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'interest_paid', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Interest Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_om', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Om', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0,
'description', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'leased_asset_number', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset',
'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'location', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0,
'long_description', null, null, 'long_description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_asset_id', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0,
'ls_asset_id', null, null, 'leased_asset_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'monthnum', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Monthnum', 300, 'month_number', 'ls_months', 'VARCHAR2', 0, 'month_number', null,
null, 'the_sort') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_accrual', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Accrual', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'principal_paid', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Principal Paid', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'residual_amount', 66, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Residual Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'revision', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Revision', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'set_of_books_id', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Set Of Books Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'term_penalty', 67, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Term Penalty', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE IMPORT ILR ARCHIVE');
delete from pp_any_query_criteria  where upper(description)='LEASE IMPORT ILR ARCHIVE';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Import ILR Archive', to_date('2015-08-12 10:31:36', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
'none', 'lessee', 'user',
'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.TIME_STAMP,arc.USER_ID,arc.ILR_ID,arc.ILR_NUMBER,arc.LEASE_XLATE,arc.LEASE_ID,arc.COMPANY_XLATE,arc.COMPANY_ID,arc.EST_IN_SVC_DATE,arc.ILR_GROUP_XLATE,
arc.ILR_GROUP_ID,arc.WORKFLOW_TYPE_XLATE,arc.WORKFLOW_TYPE_ID,arc.EXTERNAL_ILR,arc.NOTES,arc.INCEPTION_AIR,arc.PURCHASE_OPTION_TYPE_XLATE,arc.PURCHASE_OPTION_TYPE_ID,arc.PURCHASE_OPTION_AMT,arc.RENEWAL_OPTION_TYPE_XLATE,arc.RENEWAL_OPTION_TYPE_ID,
arc.CANCELABLE_TYPE_XLATE,arc.CANCELABLE_TYPE_ID,arc.ITC_SW,arc.PARTIAL_RETIRE_SW,arc.SUBLET_SW,arc.MUNI_BO_SW,arc.LEASE_CAP_TYPE_XLATE,arc.LEASE_CAP_TYPE_ID,arc.TERMINATION_AMT,arc.PAYMENT_TERM_ID,arc.PAYMENT_TERM_DATE,arc.PAYMENT_FREQ_XLATE,arc.PAYMENT_FREQ_ID,
arc.NUMBER_OF_TERMS,arc.PAID_AMOUNT,arc.EST_EXECUTORY_COST,arc.CONTINGENT_AMOUNT,arc.CLASS_CODE_XLATE1,arc.CLASS_CODE_ID1,arc.CLASS_CODE_VALUE1,arc.CLASS_CODE_XLATE2,arc.CLASS_CODE_ID2,arc.CLASS_CODE_VALUE2,arc.CLASS_CODE_XLATE3,arc.CLASS_CODE_ID3,arc.CLASS_CODE_VALUE3,
arc.CLASS_CODE_XLATE4,arc.CLASS_CODE_ID4,arc.CLASS_CODE_VALUE4,arc.CLASS_CODE_XLATE5,arc.CLASS_CODE_ID5,arc.CLASS_CODE_VALUE5,arc.CLASS_CODE_XLATE6,arc.CLASS_CODE_ID6,arc.CLASS_CODE_VALUE6,arc.CLASS_CODE_XLATE7,arc.CLASS_CODE_ID7,arc.CLASS_CODE_VALUE7,arc.CLASS_CODE_XLATE8,
arc.CLASS_CODE_ID8,arc.CLASS_CODE_VALUE8,arc.CLASS_CODE_XLATE9,arc.CLASS_CODE_ID9,arc.CLASS_CODE_VALUE9,arc.CLASS_CODE_XLATE10,arc.CLASS_CODE_ID10,arc.CLASS_CODE_VALUE10,arc.CLASS_CODE_XLATE11,arc.CLASS_CODE_ID11,arc.CLASS_CODE_VALUE11,arc.CLASS_CODE_XLATE12,arc.CLASS_CODE_ID12,
arc.CLASS_CODE_VALUE12,arc.CLASS_CODE_XLATE13,arc.CLASS_CODE_ID13,arc.CLASS_CODE_VALUE13,arc.CLASS_CODE_XLATE14,arc.CLASS_CODE_ID14,arc.CLASS_CODE_VALUE14,arc.CLASS_CODE_XLATE15,arc.CLASS_CODE_ID15,arc.CLASS_CODE_VALUE15,arc.CLASS_CODE_XLATE16,arc.CLASS_CODE_ID16,
arc.CLASS_CODE_VALUE16,arc.CLASS_CODE_XLATE17,arc.CLASS_CODE_ID17,arc.CLASS_CODE_VALUE17,arc.CLASS_CODE_XLATE18,arc.CLASS_CODE_ID18,arc.CLASS_CODE_VALUE18,arc.CLASS_CODE_XLATE19,arc.CLASS_CODE_ID19,arc.CLASS_CODE_VALUE19,arc.CLASS_CODE_XLATE20,arc.CLASS_CODE_ID20,
arc.CLASS_CODE_VALUE20,arc.LOADED,arc.IS_MODIFIED,arc.ERROR_MESSAGE,arc.UNIQUE_ILR_IDENTIFIER,arc.C_BUCKET_1,arc.C_BUCKET_2,arc.C_BUCKET_3,arc.C_BUCKET_4,arc.C_BUCKET_5,arc.C_BUCKET_6,arc.C_BUCKET_7,arc.C_BUCKET_8,arc.C_BUCKET_9,arc.C_BUCKET_10,arc.E_BUCKET_1,arc.E_BUCKET_2,
arc.E_BUCKET_3,arc.E_BUCKET_4,arc.E_BUCKET_5,arc.E_BUCKET_6,arc.E_BUCKET_7,arc.E_BUCKET_8,arc.E_BUCKET_9,arc.E_BUCKET_10,arc.INT_ACCRUAL_ACCOUNT_XLATE,arc.INT_ACCRUAL_ACCOUNT_ID,arc.INT_EXPENSE_ACCOUNT_XLATE,arc.INT_EXPENSE_ACCOUNT_ID,arc.EXEC_ACCRUAL_ACCOUNT_XLATE,
arc.EXEC_ACCRUAL_ACCOUNT_ID,arc.EXEC_EXPENSE_ACCOUNT_XLATE,arc.EXEC_EXPENSE_ACCOUNT_ID,arc.CONT_ACCRUAL_ACCOUNT_XLATE,arc.CONT_ACCRUAL_ACCOUNT_ID,arc.CONT_EXPENSE_ACCOUNT_XLATE,arc.CONT_EXPENSE_ACCOUNT_ID,arc.CAP_ASSET_ACCOUNT_XLATE,arc.CAP_ASSET_ACCOUNT_ID,
arc.ST_OBLIG_ACCOUNT_XLATE,arc.ST_OBLIG_ACCOUNT_ID,arc.LT_OBLIG_ACCOUNT_XLATE,arc.LT_OBLIG_ACCOUNT_ID,arc.AP_ACCOUNT_XLATE,arc.AP_ACCOUNT_ID,arc.RES_DEBIT_ACCOUNT_XLATE,arc.RES_DEBIT_ACCOUNT_ID,arc.RES_CREDIT_ACCOUNT_XLATE,arc.RES_CREDIT_ACCOUNT_ID,arc.FUNDING_STATUS_ID,arc.FUNDING_STATUS_XLATE
  from pp_import_run run, ls_import_ilr_archive arc
  where run.import_run_id = arc.import_run_id',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_2', 107, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_3', 108, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_4', 109, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_5', 110, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_6', 111, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_7', 112, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_8', 113, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_9', 114, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancelable_type_id', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancelable Type Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancelable_type_xlate', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancelable Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cap_asset_account_id', 139, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cap Asset Account Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cap_asset_account_xlate', 138, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cap Asset Account Xlate', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id1', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id10', 70, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id10', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id11', 73, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id11', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id12', 76, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id12', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id13', 79, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id13', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id14', 82, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id14', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id15', 85, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id15', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id16', 88, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id16', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id17', 91, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id17', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id18', 94, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id18', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id19', 97, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id19', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id2', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id20', 100, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id20', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id3', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id4', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id5', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id6', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id7', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id8', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id9', 67, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value1', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value1', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value10', 71, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value10', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value11', 74, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value11', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value12', 77, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value12', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value13', 80, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value13', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value14', 83, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value14', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value15', 86, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value15', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value16', 89, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value16', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value17', 92, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value17', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value18', 95, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value18', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value19', 98, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value19', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value2', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value2', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value20', 101, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value20', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value3', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value3', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value4', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value4', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value5', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value5', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value6', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value6', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value7', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value7', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value8', 65, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value8', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value9', 68, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value9', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate1', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate1', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate10', 69, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate10', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate11', 72, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate11', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate12', 75, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate12', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate13', 78, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate13', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate14', 81, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate14', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate15', 84, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate15', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate16', 87, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate16', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate17', 90, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate17', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate18', 93, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate18', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate19', 96, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate19', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate2', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate2', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate20', 99, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate20', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate3', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate3', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate4', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate4', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate5', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate5', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate6', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate6', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate7', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate7', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate8', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate8', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate9', 66, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate9', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_xlate', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cont_accrual_account_id', 135, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Cont Accrual Account Id', 300, null, null, 'NUMBER', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cont_accrual_account_xlate', 134, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cont Accrual Account Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cont_expense_account_id', 137, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cont Expense Account Id', 300, null, null, 'NUMBER', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cont_expense_account_xlate', 136, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cont Expense Account Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'contingent_amount', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Contingent Amount', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_1', 116, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_10', 125, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 10', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_2', 117, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_3', 118, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_4', 119, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_5', 120, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_6', 121, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_7', 122, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_8', 123, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'e_bucket_9', 124, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'E Bucket 9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'error_message', 104, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Error Message', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'est_executory_cost', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Est Executory Cost', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'est_in_svc_date', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Est In Svc Date', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'exec_accrual_account_id', 131, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Exec Accrual Account Id', 300, null, null, 'NUMBER', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'exec_accrual_account_xlate', 130, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Exec Accrual Account Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'exec_expense_account_id', 133, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Exec Expense Account Id', 300, null, null, 'NUMBER', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'exec_expense_account_xlate', 132, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Exec Expense Account Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'external_ilr', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'External Ilr', 300, 'external_ilr', 'ls_ilr', 'VARCHAR2', 0, 'external_ilr',
null, null, 'external_ilr') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'funding_status_id', 150, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Funding Status Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'funding_status_xlate', 151, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Funding Status Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_group_id', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Group Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_group_xlate', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Group Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null,
'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ilr_number', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null,
null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_description', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Description', 300, 'description',
'(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 252))', 'VARCHAR2', 0, 'description', 1, 2,
'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'inception_air', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Inception Air', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'int_accrual_account_id', 127, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Int Accrual Account Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'int_accrual_account_xlate', 126, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Int Accrual Account Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'int_expense_account_id', 129, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Int Expense Account Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'int_expense_account_xlate', 128, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Int Expense Account Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_modified', 103, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Modified', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'itc_sw', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Itc Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type_id', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type_xlate', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_xlate', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'line_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Line Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'loaded', 102, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Loaded', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lt_oblig_account_id', 143, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lt Oblig Account Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lt_oblig_account_xlate', 142, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lt Oblig Account Xlate', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'muni_bo_sw', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Muni Bo Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'notes', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Notes', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'number_of_terms', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Number Of Terms', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'paid_amount', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Paid Amount', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'partial_retire_sw', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Partial Retire Sw', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_freq_id', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payment Freq Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_freq_xlate', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Payment Freq Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_term_date', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Payment Term Date', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_term_id', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payment Term Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_amt', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Amt', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_type_id', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Type Id', 300, null, null, 'NUMBER', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_type_xlate', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Type Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'renewal_option_type_id', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Renewal Option Type Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'renewal_option_type_xlate', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Renewal Option Type Xlate', 300, null, null, 'VARCHAR2', 0, null,
 null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'res_credit_account_id', 149, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Res Credit Account Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'res_credit_account_xlate', 148, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Res Credit Account Xlate', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'res_debit_account_id', 147, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Res Debit Account Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'res_debit_account_xlate', 146, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Res Debit Account Xlate', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'st_oblig_account_id', 141, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'St Oblig Account Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'st_oblig_account_xlate', 140, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'St Oblig Account Xlate', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sublet_sw', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Sublet Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'termination_amt', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Termination Amt', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'time_stamp', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Time Stamp', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'unique_ilr_identifier', 105, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Unique Ilr Identifier', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0,
'ilr_id', null, null, 'ilr_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'user_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'User Id', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'workflow_type_id', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Workflow Type Id', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'workflow_type_xlate', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Workflow Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ap_account_id', 145, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ap Account Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ap_account_xlate', 144, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ap Account Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_1', 106, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'c_bucket_10', 115, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'C Bucket 10', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
end;
/

declare
query_id number;
begin
select max(id) + 1 into query_id from pp_any_query_criteria;
delete from pp_any_query_criteria_fields where id=
(select id from pp_any_query_criteria  where upper(description)='LEASE IMPORT MLA ARCHIVE');
delete from pp_any_query_criteria  where upper(description)='LEASE IMPORT MLA ARCHIVE';
INSERT INTO "PP_ANY_QUERY_CRITERIA" ("ID", "SOURCE_ID", "CRITERIA_FIELD", "TABLE_NAME", "DESCRIPTION", "TIME_STAMP", "USER_ID", "FEEDER_FIELD", "SUBSYSTEM", "QUERY_TYPE", "SQL",
"SQL2", "SQL3", "SQL4", "SQL5", "SQL6") VALUES (query_id, 1001, 'none', 'Dynamic View', 'Lease Import MLA Archive', to_date('2015-08-12 10:33:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
'none', 'lessee', 'user',
'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.TIME_STAMP,arc.USER_ID,arc.LEASE_ID,arc.LEASE_NUMBER,arc.DESCRIPTION,arc.LONG_DESCRIPTION,arc.LESSOR_XLATE,arc.LESSOR_ID,arc.LEASE_TYPE_XLATE,
arc.LEASE_TYPE_ID,arc.PAYMENT_DUE_DAY,arc.PRE_PAYMENT_SW,arc.LEASE_GROUP_XLATE,arc.LEASE_GROUP_ID,arc.LEASE_CAP_TYPE_XLATE,arc.LEASE_CAP_TYPE_ID,arc.WORKFLOW_TYPE_XLATE,arc.WORKFLOW_TYPE_ID,arc.MASTER_AGREEMENT_DATE,arc.NOTES,
arc.PURCHASE_OPTION_TYPE_XLATE,arc.PURCHASE_OPTION_TYPE_ID,arc.PURCHASE_OPTION_AMT,arc.RENEWAL_OPTION_TYPE_XLATE,arc.RENEWAL_OPTION_TYPE_ID,arc.CANCELABLE_TYPE_XLATE,arc.CANCELABLE_TYPE_ID,arc.ITC_SW,arc.PARTIAL_RETIRE_SW,
arc.SUBLET_SW,arc.MUNI_BO_SW,arc.COMPANY_XLATE,arc.COMPANY_ID,arc.MAX_LEASE_LINE,arc.VENDOR_XLATE,arc.VENDOR_ID,arc.PAYMENT_PCT,arc.CLASS_CODE_XLATE1,arc.CLASS_CODE_ID1,arc.CLASS_CODE_VALUE1,arc.CLASS_CODE_XLATE2,arc.CLASS_CODE_ID2,
arc.CLASS_CODE_VALUE2,arc.CLASS_CODE_XLATE3,arc.CLASS_CODE_ID3,arc.CLASS_CODE_VALUE3,arc.CLASS_CODE_XLATE4,arc.CLASS_CODE_ID4,arc.CLASS_CODE_VALUE4,arc.CLASS_CODE_XLATE5,arc.CLASS_CODE_ID5,arc.CLASS_CODE_VALUE5,arc.CLASS_CODE_XLATE6,
arc.CLASS_CODE_ID6,arc.CLASS_CODE_VALUE6,arc.CLASS_CODE_XLATE7,arc.CLASS_CODE_ID7,arc.CLASS_CODE_VALUE7,arc.CLASS_CODE_XLATE8,arc.CLASS_CODE_ID8,arc.CLASS_CODE_VALUE8,arc.CLASS_CODE_XLATE9,arc.CLASS_CODE_ID9,arc.CLASS_CODE_VALUE9,
arc.CLASS_CODE_XLATE10,arc.CLASS_CODE_ID10,arc.CLASS_CODE_VALUE10,arc.CLASS_CODE_XLATE11,arc.CLASS_CODE_ID11,arc.CLASS_CODE_VALUE11,arc.CLASS_CODE_XLATE12,arc.CLASS_CODE_ID12,arc.CLASS_CODE_VALUE12,arc.CLASS_CODE_XLATE13,arc.CLASS_CODE_ID13,
arc.CLASS_CODE_VALUE13,arc.CLASS_CODE_XLATE14,arc.CLASS_CODE_ID14,arc.CLASS_CODE_VALUE14,arc.CLASS_CODE_XLATE15,arc.CLASS_CODE_ID15,arc.CLASS_CODE_VALUE15,arc.CLASS_CODE_XLATE16,arc.CLASS_CODE_ID16,arc.CLASS_CODE_VALUE16,arc.CLASS_CODE_XLATE17,
arc.CLASS_CODE_ID17,arc.CLASS_CODE_VALUE17,arc.CLASS_CODE_XLATE18,arc.CLASS_CODE_ID18,arc.CLASS_CODE_VALUE18,arc.CLASS_CODE_XLATE19,arc.CLASS_CODE_ID19,arc.CLASS_CODE_VALUE19,arc.CLASS_CODE_XLATE20,arc.CLASS_CODE_ID20,arc.CLASS_CODE_VALUE20,arc.LOADED,
arc.IS_MODIFIED,arc.ERROR_MESSAGE,arc.UNIQUE_LEASE_IDENTIFIER,arc.AUTO_APPROVE,arc.AUTO_APPROVE_XLATE,arc.TAX_SUMMARY_XLATE,arc.TAX_SUMMARY_ID,arc.TAX_RATE_OPTION_XLATE,arc.TAX_RATE_OPTION_ID,arc.AUTO_GENERATE_INVOICES_XLATE,arc.AUTO_GENERATE_INVOICES,
arc.LS_RECONCILE_TYPE_ID,arc.LS_RECONCILE_TYPE_XLATE,arc.DAYS_IN_YEAR,arc.CUT_OFF_DAY,arc.LEASE_END_DATE,arc.DAYS_IN_MONTH_SW
  from pp_import_run run, ls_import_lease_archive arc
  where run.import_run_id = arc.import_run_id',
 null, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate18', 92, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate18', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate19', 95, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate19', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate2', 44, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate2', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate20', 98, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate20', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate3', 47, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate3', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate4', 50, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate4', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate5', 53, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate5', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate6', 56, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate6', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate7', 59, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate7', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate8', 62, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate8', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate9', 65, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate9', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_id', 36, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Id', 300, 'description',
'(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'company_xlate', 35, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Company Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cut_off_day', 116, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cut Off Day', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_month_sw', 118, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Month Sw', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'days_in_year', 115, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Days In Year', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'description', 8, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'error_message', 103, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Error Message', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_description', 1, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Description', 300, 'description',
'(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 251))', 'VARCHAR2', 0, 'description', 1, 2,
'description') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'import_run_id', 2, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Import Run Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'is_modified', 102, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Is Modified', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'itc_sw', 31, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Itc Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type_id', 19, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_cap_type_xlate', 18, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Cap Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_end_date', 117, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease End Date', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_group_id', 17, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Group Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_group_xlate', 16, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Group Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_id', 6, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null,
'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_number', 7, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number',
null, null, 'lease_number') ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_type_id', 13, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Type Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lease_type_xlate', 12, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lease Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lessor_id', 11, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lessor Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'lessor_xlate', 10, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Lessor Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'line_id', 3, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Line Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'loaded', 101, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Loaded', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'long_description', 9, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 300, null, null, 'VARCHAR2', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_reconcile_type_id', 113, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Reconcile Type Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'ls_reconcile_type_xlate', 114, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Ls Reconcile Type Xlate', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'master_agreement_date', 22, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Master Agreement Date', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'max_lease_line', 37, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Max Lease Line', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'muni_bo_sw', 34, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Muni Bo Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'notes', 23, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Notes', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'partial_retire_sw', 32, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Partial Retire Sw', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_due_day', 14, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payment Due Day', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'payment_pct', 40, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Payment Pct', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'pre_payment_sw', 15, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Pre Payment Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_amt', 26, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Amt', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_type_id', 25, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Type Id', 300, null, null, 'NUMBER', 0, null, null,
 null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'purchase_option_type_xlate', 24, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Purchase Option Type Xlate', 300, null, null, 'VARCHAR2', 0,
null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'renewal_option_type_id', 28, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Renewal Option Type Id', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'renewal_option_type_xlate', 27, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Renewal Option Type Xlate', 300, null, null, 'VARCHAR2', 0, null,
 null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'sublet_sw', 33, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Sublet Sw', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_rate_option_id', 110, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Rate Option Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_rate_option_xlate', 109, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Rate Option Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_summary_id', 108, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Summary Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'tax_summary_xlate', 107, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Tax Summary Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'time_stamp', 4, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Time Stamp', 300, null, null, 'DATE', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'unique_lease_identifier', 104, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Unique Lease Identifier', 300, null, null, 'VARCHAR2', 0, null,
null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'user_id', 5, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'User Id', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'vendor_id', 39, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Vendor Id', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'vendor_xlate', 38, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Vendor Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'workflow_type_id', 21, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Workflow Type Id', 300, null, null, 'NUMBER', 0, null, null, null, null)
;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'workflow_type_xlate', 20, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Workflow Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
 null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'auto_approve', 105, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Auto Approve', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'auto_approve_xlate', 106, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Auto Approve Xlate', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'auto_generate_invoices', 112, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Auto Generate Invoices', 300, null, null, 'NUMBER', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'auto_generate_invoices_xlate', 111, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Auto Generate Invoices Xlate', 300, null, null, 'VARCHAR2',
0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancelable_type_id', 30, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancelable Type Id', 300, null, null, 'NUMBER', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'cancelable_type_xlate', 29, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Cancelable Type Xlate', 300, null, null, 'VARCHAR2', 0, null, null,
null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id1', 42, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id1', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id10', 69, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id10', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id11', 72, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id11', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id12', 75, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id12', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id13', 78, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id13', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id14', 81, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id14', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id15', 84, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id15', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id16', 87, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id16', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id17', 90, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id17', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id18', 93, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id18', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id19', 96, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id19', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id2', 45, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id2', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id20', 99, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id20', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id3', 48, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id3', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id4', 51, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id4', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id5', 54, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id5', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id6', 57, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id6', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id7', 60, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id7', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id8', 63, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id8', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_id9', 66, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Id9', 300, null, null, 'NUMBER', 0, null, null, null, null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value1', 43, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value1', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value10', 70, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value10', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value11', 73, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value11', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value12', 76, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value12', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value13', 79, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value13', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value14', 82, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value14', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value15', 85, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value15', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value16', 88, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value16', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value17', 91, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value17', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value18', 94, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value18', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value19', 97, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value19', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value2', 46, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value2', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value20', 100, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value20', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value3', 49, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value3', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value4', 52, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value4', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value5', 55, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value5', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value6', 58, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value6', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value7', 61, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value7', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value8', 64, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value8', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_value9', 67, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Value9', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate1', 41, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate1', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate10', 68, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate10', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate11', 71, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate11', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate12', 74, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate12', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate13', 77, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate13', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate14', 80, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate14', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate15', 83, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate15', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate16', 86, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate16', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
INSERT INTO "PP_ANY_QUERY_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE",
"COLUMN_HEADER", "COLUMN_WIDTH", "DISPLAY_FIELD", "DISPLAY_TABLE", "COLUMN_TYPE", "QUANTITY_FIELD", "DATA_FIELD", "REQUIRED_FILTER", "REQUIRED_ONE_MULT", "SORT_COL") VALUES (query_id,
'class_code_xlate17', 89, to_date('2015-08-17 12:54:13', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Class Code Xlate17', 300, null, null, 'VARCHAR2', 0, null, null, null,
null) ;
end;
/

UPDATE pp_any_query_criteria_fields
set display_table = '(select month_number from pp_calendar)'
where UPPER(display_table) = 'LS_MONTHS';

update pp_any_query_criteria_fields
set display_field = 'description',
    data_field = 'set_of_books_id',
    display_table = 'set_of_books'
where id in (select id from pp_any_query_criteria where lower(subsystem) = 'lessee')
and detail_field = 'set_of_books_id';

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2795, 0, 2015, 2, 0, 0, 044726, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044726_lease_bring_queries_to_base_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;