/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044543_rpr_40_repair_blkt_results_add_precision_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 2015.2   07/29/2015 Andrew Hill      Adding precision to expense percent field (script 5 of 5)
||============================================================================
*/
ALTER TABLE repair_blkt_results_reporting
drop column EXPENSE_PCT2;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2725, 0, 2015, 2, 0, 0, 044543, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044543_rpr_40_repair_blkt_results_add_precision_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;