/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050220_lessee_05_sch_remove_unused_objects_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.2.0.0 01/24/2018 Shane "C" Ward 	Lesseee Calculation Performance enhancements
||============================================================================
*/

DROP VIEW v_multicurrency_lis_inner;

DROP VIEW v_multicurrency_ls_asset_inner;

DROP materialized VIEW mv_multicurr_lis_inner_amounts;

DROP materialized VIEW mv_multicurr_lis_inner_depr;

DROP materialized VIEW mv_multicurr_ls_asset_inner;

DROP materialized VIEW log ON ls_asset;

DROP materialized VIEW log ON ls_asset_schedule;

DROP materialized VIEW log ON ls_depr_forecast;

DROP materialized VIEW log ON ls_ilr;

DROP materialized VIEW log ON ls_ilr_amounts_set_of_books;

DROP materialized VIEW log ON ls_ilr_options;

DROP materialized VIEW log ON ls_ilr_schedule;

DROP materialized VIEW log ON ls_lease; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4093, 0, 2017, 2, 0, 0, 50220, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050220_lessee_05_sch_remove_unused_objects_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	