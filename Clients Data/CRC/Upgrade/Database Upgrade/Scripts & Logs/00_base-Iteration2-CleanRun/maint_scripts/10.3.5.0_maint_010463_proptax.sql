/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010463_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/03/2012 Julia Breuer   Point Release
||============================================================================
*/

alter table PWRPLANT.PT_LEDGER_TAX_YEAR add BEGINNING_TAX_BASIS        number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_BASIS_ADDITIONS        number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_BASIS_RETIREMENTS      number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_BASIS_TRANS_ADJ        number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add BEGINNING_TAX_RESERVE      number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_RESERVE_ADDITIONS      number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_RESERVE_RETIREMENTS    number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_RESERVE_TRANS_ADJ      number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add BEG_TAX_BASIS_ADJUSTMENT   number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_BASIS_ADDS_ADJUSTMENT  number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_BASIS_RETS_ADJUSTMENT  number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_TAX_YEAR add TAX_BASIS_TRANS_ADJUSTMENT number(22,2) default 0;

alter table PWRPLANT.PT_LEDGER_ADJUSTMENT add BEG_TAX_BASIS_ADJUSTMENT   number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_ADJUSTMENT add TAX_BASIS_ADDS_ADJUSTMENT  number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_ADJUSTMENT add TAX_BASIS_RETS_ADJUSTMENT  number(22,2) default 0;
alter table PWRPLANT.PT_LEDGER_ADJUSTMENT add TAX_BASIS_TRANS_ADJUSTMENT number(22,2) default 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (164, 0, 10, 3, 5, 0, 10463, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010463_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
