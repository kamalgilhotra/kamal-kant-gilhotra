/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048741_lessor_03_add_lessor_ilr_schedule_build_pp_process_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/22/2017 Andrew Hill      Add Lessor - ILR Schedule Process to PP Processes
||============================================================================
*/

MERGE INTO pp_processes a
USING (SELECT max_id + ROWNUM AS process_id, DESCRIPTION, long_description
      FROM (SELECT 'Lessor - ILR Schedule' AS DESCRIPTION, 'Process to build Lessor ILR Schedule' AS long_description
            FROM dual)
      CROSS JOIN (SELECT MAX(process_id) AS max_id
                  FROM pp_processes)) b ON (UPPER(TRIM(A.DESCRIPTION)) = UPPER(TRIM(b.DESCRIPTION)))
WHEN MATCHED THEN
  UPDATE SET A.long_description = b.long_description
  where decode(a.long_description, b.long_description, 1, 0) = 0
WHEN NOT MATCHED THEN
  INSERT(A.process_id, A.DESCRIPTION, A.long_description)
  values(b.process_id, b.description, b.long_description);
  
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3734, 0, 2017, 1, 0, 0, 48741, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048741_lessor_03_add_lessor_ilr_schedule_build_pp_process_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;