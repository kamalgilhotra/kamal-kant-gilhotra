/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046763_taxrpr_01_companies_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2017.1.0.0 11/03/2017 Eric Berger    DML multi-batch reporting.
|| 2018.2.0.0 07/11/2018 Matt Kelley    Made scripts rerunnable
||============================================================================
*/

--first structural changes to the company setup table
BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE company_setup ADD is_tax_repairs_company NUMBER(22,0) DEFAULT 0';
EXCEPTION
    WHEN OTHERS THEN
        Dbms_Output.put_line('Column is_tax_repairs_company already exists.');
END;
/


--add column to company security temp table
BEGIN
    EXECUTE IMMEDIATE 'ALTER TABLE pp_company_security_temp ADD is_tax_repairs_company NUMBER(22,0) DEFAULT 0';
EXCEPTION
    WHEN OTHERS THEN
         Dbms_Output.put_line('Column is_tax_repairs_company already exists.');
END;
/

--add the new column to the company view
CREATE OR REPLACE VIEW company (
  company_id,
  time_stamp,
  user_id,
  gl_company_no,
  owned,
  description,
  status_code_id,
  short_description,
  company_summary_id,
  auto_life_month,
  auto_curve_month,
  auto_close_wo_num,
  parent_company_id,
  tax_return_id,
  closing_rollup_id,
  auto_life_month_monthly,
  company_type,
  company_ein,
  cwip_allocation_months,
  prop_tax_company_id,
  powertax_company_id,
  is_lease_company,
  is_tax_repairs_company
) AS
select COMPANY_SETUP.COMPANY_ID,
       COMPANY_SETUP.TIME_STAMP,
       COMPANY_SETUP.USER_ID,
       COMPANY_SETUP.GL_COMPANY_NO,
       COMPANY_SETUP.OWNED,
       COMPANY_SETUP.DESCRIPTION,
       COMPANY_SETUP.STATUS_CODE_ID,
       COMPANY_SETUP.SHORT_DESCRIPTION,
       COMPANY_SETUP.COMPANY_SUMMARY_ID,
       COMPANY_SETUP.AUTO_LIFE_MONTH,
       COMPANY_SETUP.AUTO_CURVE_MONTH,
       COMPANY_SETUP.AUTO_CLOSE_WO_NUM,
       COMPANY_SETUP.PARENT_COMPANY_ID,
       COMPANY_SETUP.TAX_RETURN_ID,
       COMPANY_SETUP.CLOSING_ROLLUP_ID,
       COMPANY_SETUP.AUTO_LIFE_MONTH_MONTHLY,
       COMPANY_SETUP.COMPANY_TYPE,
       COMPANY_SETUP.COMPANY_EIN,
       COMPANY_SETUP.CWIP_ALLOCATION_MONTHS,
       COMPANY_SETUP.PROP_TAX_COMPANY_ID,
       COMPANY_SETUP.POWERTAX_COMPANY_ID,
       COMPANY_SETUP.IS_LEASE_COMPANY,
       COMPANY_SETUP.IS_TAX_REPAIRS_COMPANY
  from COMPANY_SETUP, PP_COMPANY_SECURITY
 where COMPANY_SETUP.COMPANY_ID = PP_COMPANY_SECURITY.COMPANY_ID
   and USERS = LOWER(user)
   and 1 <> (select count(*) from PP_COMPANY_SECURITY_TEMP where ROWNUM = 1)
union all
select PP_COMPANY_SECURITY_TEMP.COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.TIME_STAMP,
       PP_COMPANY_SECURITY_TEMP.USER_ID,
       PP_COMPANY_SECURITY_TEMP.GL_COMPANY_NO,
       PP_COMPANY_SECURITY_TEMP.OWNED,
       PP_COMPANY_SECURITY_TEMP.DESCRIPTION,
       PP_COMPANY_SECURITY_TEMP.STATUS_CODE_ID,
       PP_COMPANY_SECURITY_TEMP.SHORT_DESCRIPTION,
       PP_COMPANY_SECURITY_TEMP.COMPANY_SUMMARY_ID,
       PP_COMPANY_SECURITY_TEMP.AUTO_LIFE_MONTH,
       PP_COMPANY_SECURITY_TEMP.AUTO_CURVE_MONTH,
       PP_COMPANY_SECURITY_TEMP.AUTO_CLOSE_WO_NUM,
       PP_COMPANY_SECURITY_TEMP.PARENT_COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.TAX_RETURN_ID,
       PP_COMPANY_SECURITY_TEMP.CLOSING_ROLLUP_ID,
       PP_COMPANY_SECURITY_TEMP.AUTO_LIFE_MONTH_MONTHLY,
       PP_COMPANY_SECURITY_TEMP.COMPANY_TYPE,
       PP_COMPANY_SECURITY_TEMP.COMPANY_EIN,
       PP_COMPANY_SECURITY_TEMP.CWIP_ALLOCATION_MONTHS,
       PP_COMPANY_SECURITY_TEMP.PROP_TAX_COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.POWERTAX_COMPANY_ID,
       PP_COMPANY_SECURITY_TEMP.IS_LEASE_COMPANY,
       PP_COMPANY_SECURITY_TEMP.IS_TAX_REPAIRS_COMPANY
  from PP_COMPANY_SECURITY_TEMP
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13722, 0, 2018, 2, 0, 0, 46763, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_046763_taxrpr_01_companies_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
