/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009769_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.1   05/08/2012 Sunjin Cone    Point Release
||============================================================================
*/

--* Adding WMIS %UoP method and extending WMIS feed functionality. Will need to retain the old method, so must differentiate between new and old processing
--  the WMIS_METHOD will determine how the WMIS feed is processed.
--
--  Average Unit Cost (old method)
--  Percent UoP       (New method, checks each WR one by one, determines which pass, then takes the total passing over the total existing to determine an allocation %)
--  NULL = Average Unit Cost (to maintain backwards compatability)

alter table WO_TAX_EXPENSE_TEST add WMIS_METHOD varchar2(35);

--* Add processing_method to repair_work_order_segments
--  Used to indicate the processing that needs to happen, particularly when multiple options need to process differently in this table (ie WMIS UoP % vs WMIS Average cost)
--
--  Add WMIS_UOP_PERCENT - Used to store the % of expense expected based on UOP % processing
--

alter table REPAIR_WORK_ORDER_SEGMENTS add PROCESSING_METHOD varchar2(35);
alter table REPAIR_WORK_ORDER_SEGMENTS add WMIS_UOP_PERCENT  number(24,22);

--* Add processing_method to repair_wo_seg_reporting, this field is the reporting version of the processing_method on repair_work_order_segments
--
--  Add WMIS_UOP_PERCENT - Used to store the % of expense expected based on UOP % processing

alter table REPAIR_WO_SEG_REPORTING add PROCESSING_METHOD varchar2(35);
alter table REPAIR_WO_SEG_REPORTING add WMIS_UOP_PERCENT  number(24,22);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (133, 0, 10, 3, 4, 1, 9769, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.1_maint_009769_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
