/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047024_lease_var_payments_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 02/23/2017 Anand R          Add new columns for ls_ilr_payment_terms
||============================================================================
*/

alter table LS_ILR_PAYMENT_TERM add 
(VARIABLE_PAYMENT_ID NUMBER(22,0),
 INCLUDE_INIT_MEASUREMENT NUMBER(22,0)
);

comment on column LS_ILR_PAYMENT_TERM.VARIABLE_PAYMENT_ID IS 'system generated id of variable payment';
comment on column LS_ILR_PAYMENT_TERM.INCLUDE_INIT_MEASUREMENT IS 'Include variable payment in initial measurement. 1=yes, 0=no';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3376, 0, 2017, 1, 0, 0, 47024, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047024_lease_var_payments_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;