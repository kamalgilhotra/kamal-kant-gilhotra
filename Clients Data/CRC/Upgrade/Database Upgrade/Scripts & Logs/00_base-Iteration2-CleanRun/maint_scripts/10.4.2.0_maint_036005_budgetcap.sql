/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036005_budgetcap.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/27/2014 Chris Mardis
||============================================================================
*/

alter table BUDGET_AFUDC_CALC      add RETIREMENTS number(22,2);
alter table BUDGET_AFUDC_CALC_TEMP add RETIREMENTS number(22,2);
alter table BUDGET_AFUDC_CALC      add CIAC number(22,2);
alter table BUDGET_AFUDC_CALC_TEMP add CIAC number(22,2);
alter table BUDGET_AFUDC_CALC      add ORIG_ELIGIBLE_FOR_AFUDC number(22);
alter table BUDGET_AFUDC_CALC_TEMP add ORIG_ELIGIBLE_FOR_AFUDC number(22);
alter table BUDGET_AFUDC_CALC      add ORIG_ELIGIBLE_FOR_CPI number(22);
alter table BUDGET_AFUDC_CALC_TEMP add ORIG_ELIGIBLE_FOR_CPI number(22);
alter table BUDGET_AFUDC_CALC      add ACTUALS_MN_PROJ number(22);
alter table BUDGET_AFUDC_CALC_TEMP add ACTUALS_MN_PROJ number(22);
alter table BUDGET_AFUDC_CALC      add ACTUALS_MN_BV number(22);
alter table BUDGET_AFUDC_CALC_TEMP add ACTUALS_MN_BV number(22);
alter table BUDGET_AFUDC_CALC      add BUDGET_VERSION_ID number(22);
alter table BUDGET_AFUDC_CALC_TEMP add BUDGET_VERSION_ID number(22);
alter table BUDGET_AFUDC_CALC      add ORIG_EST_START_DATE date;
alter table BUDGET_AFUDC_CALC_TEMP add ORIG_EST_START_DATE date;
alter table BUDGET_AFUDC_CALC      add ORIG_EST_IN_SERVICE_DATE date;
alter table BUDGET_AFUDC_CALC_TEMP add ORIG_EST_IN_SERVICE_DATE date;
alter table BUDGET_AFUDC_CALC      add ORIG_EST_COMPLETE_DATE date;
alter table BUDGET_AFUDC_CALC_TEMP add ORIG_EST_COMPLETE_DATE date;
alter table BUDGET_AFUDC_CALC      add CWIP_START_MONTH number(22);
alter table BUDGET_AFUDC_CALC_TEMP add CWIP_START_MONTH number(22);

comment on column BUDGET_AFUDC_CALC.RETIREMENTS is 'Snapshot of estimated Retirements for the month.';
comment on column BUDGET_AFUDC_CALC.CIAC is 'Snapshot of estimated CIAC for the month.';
comment on column BUDGET_AFUDC_CALC.ORIG_ELIGIBLE_FOR_AFUDC is 'Snapshot of afudc eligibility flag.';
comment on column BUDGET_AFUDC_CALC.ORIG_ELIGIBLE_FOR_CPI is 'Snapshot of cpi eligibility flag.';
comment on column BUDGET_AFUDC_CALC.ACTUALS_MN_PROJ is 'Snapshot of the actuals month on the revision.';
comment on column BUDGET_AFUDC_CALC.ACTUALS_MN_BV is 'Snapshot of the actuals month on the budget version.';
comment on column BUDGET_AFUDC_CALC.BUDGET_VERSION_ID is 'Snapshot of the budget version being processed.';
comment on column BUDGET_AFUDC_CALC.ORIG_EST_START_DATE is 'Snapshot of the estimated start date on the project revision.';
comment on column BUDGET_AFUDC_CALC.ORIG_EST_IN_SERVICE_DATE is 'Snapshot of the estimated in service date on the project revision.';
comment on column BUDGET_AFUDC_CALC.ORIG_EST_COMPLETE_DATE is 'Snapshot of the estimated complete date on the project revision.';
comment on column BUDGET_AFUDC_CALC.CWIP_START_MONTH is 'Snapshot of the month at which the budget afudc calc started.';

comment on column BUDGET_AFUDC_CALC_TEMP.RETIREMENTS is 'Snapshot of estimated Retirements for the month.';
comment on column BUDGET_AFUDC_CALC_TEMP.CIAC is 'Snapshot of estimated CIAC for the month.';
comment on column BUDGET_AFUDC_CALC_TEMP.ORIG_ELIGIBLE_FOR_AFUDC is 'Snapshot of afudc eligibility flag.';
comment on column BUDGET_AFUDC_CALC_TEMP.ORIG_ELIGIBLE_FOR_CPI is 'Snapshot of cpi eligibility flag.';
comment on column BUDGET_AFUDC_CALC_TEMP.ACTUALS_MN_PROJ is 'Snapshot of the actuals month on the revision.';
comment on column BUDGET_AFUDC_CALC_TEMP.ACTUALS_MN_BV is 'Snapshot of the actuals month on the budget version.';
comment on column BUDGET_AFUDC_CALC_TEMP.BUDGET_VERSION_ID is 'Snapshot of the budget version being processed.';
comment on column BUDGET_AFUDC_CALC_TEMP.ORIG_EST_START_DATE is 'Snapshot of the estimated start date on the project revision.';
comment on column BUDGET_AFUDC_CALC_TEMP.ORIG_EST_IN_SERVICE_DATE is 'Snapshot of the estimated in service date on the project revision.';
comment on column BUDGET_AFUDC_CALC_TEMP.ORIG_EST_COMPLETE_DATE is 'Snapshot of the estimated complete date on the project revision.';
comment on column BUDGET_AFUDC_CALC_TEMP.CWIP_START_MONTH is 'Snapshot of the month at which the budget afudc calc started.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (917, 0, 10, 4, 2, 0, 36005, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036005_budgetcap.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;