SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009860_cpr_detect_errors.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   04/16/2013 Charlie Shilling
|| 10.4.0.0   05/28/2013 Charlie Shilling Put in fix for data.
||============================================================================
*/

declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';
   PPCORA varchar2(10) := 'PPC-ORA' || '-> ';

   LL_BAD_COUNT number;
   -- change value of LB_SKIP_CHECK to TRUE if you determine there is data that is OK but will always
   -- show up as an exception.
   LB_SKIP_CHECK boolean := false;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   if not LB_SKIP_CHECK then

      select count(*)
        into LL_BAD_COUNT
        from CPR_LEDGER A, RETIREMENT_UNIT B, PROPERTY_UNIT C, CPR_ACTIVITY E
       where A.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
         and B.PROPERTY_UNIT_ID = C.PROPERTY_UNIT_ID
         and (C.RETIRE_METHOD_ID in (3, 5) or
             (C.RETIRE_METHOD_ID = 9 and exists
              (select 1
                  from PROPERTY_UNIT_DEFAULT_LIFE D
                 where NVL(D.RETIRE_METHOD_ID, 0) in (3, 5)
                   and A.UTILITY_ACCOUNT_ID = D.UTILITY_ACCOUNT_ID
                   and A.BUS_SEGMENT_ID = D.BUS_SEGMENT_ID
                   and C.PROPERTY_UNIT_ID = D.PROPERTY_UNIT_ID
                   and D.COMPANY_ID in (A.COMPANY_ID, -1))))
         and A.ASSET_ID = E.ASSET_ID
         and UPPER(trim(E.ACTIVITY_CODE)) in ('URET', 'SALE', 'URGL', 'SAGL')
         and TO_NUMBER(TO_CHAR(E.OUT_OF_SERVICE_MO_YR, 'YYYYMM')) <
             TO_NUMBER(TO_CHAR(E.GL_POSTING_MO_YR, 'YYYYMM'))
         and E.OUT_OF_SERVICE_MO_YR is not null;

      if LL_BAD_COUNT > 0 then
         DBMS_OUTPUT.PUT_LINE(PPCMSG || LL_BAD_COUNT ||
                              ' data errors found for maint 9860. See JIRA for details.');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Fixing data errors on cpr_activity now...');

         update CPR_ACTIVITY E
            set OUT_OF_SERVICE_MO_YR = GL_POSTING_MO_YR
          where exists (select 1
                   from CPR_LEDGER A, RETIREMENT_UNIT B, PROPERTY_UNIT C
                  where A.RETIREMENT_UNIT_ID = B.RETIREMENT_UNIT_ID
                    and B.PROPERTY_UNIT_ID = C.PROPERTY_UNIT_ID
                    and (C.RETIRE_METHOD_ID in (3, 5) or
                        (C.RETIRE_METHOD_ID = 9 and exists
                         (select 1
                             from PROPERTY_UNIT_DEFAULT_LIFE D
                            where NVL(D.RETIRE_METHOD_ID, 0) in (3, 5)
                              and A.UTILITY_ACCOUNT_ID = D.UTILITY_ACCOUNT_ID
                              and A.BUS_SEGMENT_ID = D.BUS_SEGMENT_ID
                              and C.PROPERTY_UNIT_ID = D.PROPERTY_UNIT_ID
                              and D.COMPANY_ID in (A.COMPANY_ID, -1))))
                    and A.ASSET_ID = E.ASSET_ID
                    and UPPER(trim(E.ACTIVITY_CODE)) in ('URET', 'SALE', 'URGL', 'SAGL')
                    and TO_NUMBER(TO_CHAR(E.OUT_OF_SERVICE_MO_YR, 'YYYYMM')) <
                        TO_NUMBER(TO_CHAR(E.GL_POSTING_MO_YR, 'YYYYMM'))
                    and E.OUT_OF_SERVICE_MO_YR is not null);

         commit;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'SQL%ROWCOUNT rows updated - Data errors fixed.');
      else
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Data is OK.');
      end if;
   else
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Check not performed.');
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (345, 0, 10, 4, 0, 0, 9860, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_009860_cpr_detect_errors.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;