/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050585_lessor_01_add_ilr_rates_used_column_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/19/2018 Andrew Hill      Add column to store rates used in building ilr schedule
||============================================================================
*/

ALTER TABLE lsr_ilr_amounts
ADD (schedule_rates t_lsr_ilr_schedule_all_rates);

UPDATE lsr_ilr_amounts
SET schedule_rates = t_lsr_ilr_schedule_all_rates(t_lsr_rates_implicit_in_lease(NULL, NULL), t_lsr_rates_implicit_in_lease(NULL, NULL), t_lsr_rates_implicit_in_lease(NULL, NULL));

ALTER TABLE lsr_ilr_amounts
MODIFY (schedule_rates NOT NULL);

COMMENT ON COLUMN lsr_ilr_amounts.schedule_rates IS 'Rates used during schedule building process for this ILR/Revision/Set of Books';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4215, 0, 2017, 3, 0, 0, 50585, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050585_lessor_01_add_ilr_rates_used_column_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 