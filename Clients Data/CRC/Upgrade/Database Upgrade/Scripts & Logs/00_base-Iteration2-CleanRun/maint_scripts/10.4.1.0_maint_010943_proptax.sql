/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010943_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/18/2012 Julia Breuer   Point Release
||============================================================================
*/

insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 30, 'notes', sysdate, user, 'Notes', null, 0, 1, 'varchar2(4000)', null, null, 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 31, 'notes', sysdate, user, 'Notes', null, 0, 1, 'varchar2(4000)', null, null, 1, null );
insert into PWRPLANT.PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 36, 'notes', sysdate, user, 'Notes', null, 0, 1, 'varchar2(4000)', null, null, 1, null );

alter table PWRPLANT.PT_IMPORT_TYPE_CODE           add NOTES varchar2(4000);
alter table PWRPLANT.PT_IMPORT_TAX_DIST            add NOTES varchar2(4000);
alter table PWRPLANT.PT_IMPORT_PROPTAX_LOC         add NOTES varchar2(4000);
alter table PWRPLANT.PT_IMPORT_TYPE_CODE_ARCHIVE   add NOTES varchar2(4000);
alter table PWRPLANT.PT_IMPORT_TAX_DIST_ARCHIVE    add NOTES varchar2(4000);
alter table PWRPLANT.PT_IMPORT_PROPTAX_LOC_ARCHIVE add NOTES varchar2(4000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (226, 0, 10, 4, 1, 0, 10943, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010943_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
