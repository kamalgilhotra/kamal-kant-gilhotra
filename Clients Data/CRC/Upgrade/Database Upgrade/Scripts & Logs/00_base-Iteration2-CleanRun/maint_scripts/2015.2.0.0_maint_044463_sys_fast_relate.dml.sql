/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044463_sys_fast_relate_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2     07/22/2015 Matt DeVries   Maint 44463 - Add Comp/Prop Unit Table
||============================================================================
*/


/* add company_property_unit table to powerplant tables and columns tables */
INSERT INTO powerplant_tables
(table_name, pp_table_type_id, description, long_description, subsystem_screen,
 select_window, subsystem_display, subsystem, delivery, notes, asset_management, budget,
 charge_repository, cwip_accounting, depr_studies, lease, property_tax, powertax_provision, powertax,
 system, unitization, work_order_management, client, where_clause)
select 'company_property_unit', 'spl*',	'Company Property Unit', 'Company Property Unit', 'always',
       NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
       NULL, NULL, NULL, NULL
from dual z
where not exists (select 1
                  from powerplant_tables x
                  where x.table_name = 'company_property_unit');


INSERT INTO powerplant_columns
(column_name, table_name, dropdown_name, pp_edit_type_id, help_index, description,
 long_description, column_rank, short_long_description, edit_mask, dropdown_percent,
 dropdown_restrict, related_type, related_table)
select 'company_id', 'company_property_unit', 'company', 'p', NULL, 'company id',
       NULL, 1, NULL, NULL, NULL, NULL, 'Primary', NULL
from dual z
where not exists (select 1
                  from powerplant_columns x
                  where x.column_name = 'company_id'
                  and x.table_name = 'company_property_unit');


INSERT INTO powerplant_columns
(column_name, table_name, dropdown_name, pp_edit_type_id, help_index, description,
 long_description, column_rank, short_long_description, edit_mask, dropdown_percent,
 dropdown_restrict, related_type, related_table)
select 'property_unit_id', 'company_property_unit', 'property_unit', 'p', NULL,
       'property unit id', NULL, 2, NULL, NULL, NULL, NULL, 'Secondary', NULL
from dual z
where not exists (select 1
                  from powerplant_columns x
                  where x.column_name = 'property_unit_id'
                  and x.table_name = 'company_property_unit');


INSERT INTO powerplant_columns
(column_name, table_name, dropdown_name, pp_edit_type_id, help_index, description,
 long_description, column_rank, short_long_description, edit_mask, dropdown_percent,
 dropdown_restrict, related_type, related_table)
select 'time_stamp', 'company_property_unit', NULL, 'e', NULL, 'time stamp', NULL,
       100, NULL, NULL, NULL, NULL, NULL, NULL
from dual z
where not exists (select 1
                  from powerplant_columns x
                  where x.column_name = 'time_stamp'
                  and x.table_name = 'company_property_unit');


INSERT INTO powerplant_columns
(column_name, table_name, dropdown_name, pp_edit_type_id, help_index, description,
 long_description, column_rank, short_long_description, edit_mask, dropdown_percent,
 dropdown_restrict, related_type, related_table)
select 'user_id', 'company_property_unit', NULL, 'e', NULL, 'user id', NULL, 101,
       NULL, NULL, NULL, NULL, NULL, NULL
from dual z
where not exists (select 1
                  from powerplant_columns x
                  where x.column_name = 'user_id'
                  and x.table_name = 'company_property_unit');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2705, 0, 2015, 2, 0, 0, 044463, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044463_sys_fast_relate.dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;