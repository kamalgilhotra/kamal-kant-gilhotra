/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_041179_jobserver_inc_20141202_ddl.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     12/5/2014  Paul Cordero    	 Creating column OS_RETURN_VALUE for PP_JOB_EXECUTION_JOB table
||==========================================================================================
*/

set serveroutput on;

declare 
  doesTableColumnExist number := 0;
  begin
       begin
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('OS_RETURN_VALUE', 'PP_JOB_EXECUTION_JOB','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column OS_RETURN_VALUE for PP_JOB_EXECUTION_JOB table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
              ADD 
              (
                "OS_RETURN_VALUE" NUMBER(6,0)
              )';    

              execute immediate 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."OS_RETURN_VALUE" IS ''Exit Code returned form JobServer Executables and other code jobs.''';
			  
            end;
          else
            begin
              dbms_output.put_line('Column OS_RETURN_VALUE for PP_JOB_EXECUTION_JOB table exists');
            end;
          end if;
       end;
  end;
 /
 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2340, 0, 2015, 1, 0, 0, 041179, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041179_jobserver_inc_20141202_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;