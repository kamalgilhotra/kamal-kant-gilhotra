/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039331_pwrtax_rpts_mods.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/20/2014 Anand Rajashekar    script changes to for report parameters update
||============================================================================
*/

-- change time option to 52 for the selected reports
update PP_REPORTS set PP_REPORT_TIME_OPTION_ID = 52 where REPORT_ID in (400001, 403001, 403005, 403010, 403012);

-- Add a new dynamic filter for report 707
insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (74, 110, 'PWRPLANT', TO_DATE('28-JUL-14', 'DD-MON-RR'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1364, 0, 10, 4, 3, 0, 39331, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039331_pwrtax_rpts_mods.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;