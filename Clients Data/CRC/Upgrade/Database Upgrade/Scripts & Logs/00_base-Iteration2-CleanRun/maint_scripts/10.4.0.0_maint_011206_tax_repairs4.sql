/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs4.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   03/12/2013 Alex Pivoshenko
||============================================================================
*/

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID,
    DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP,
    VALID_OPERATORS, EXCLUDE_FROM_WHERE)
values
   (53, 'Work Order Description', 'sle', 'work_order_control.description', null, null, null, null,
    null, null, null, null, null, null, null, null);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (34, 53, null, null);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (35, 53, null, null);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (36, 53, null, null);

update PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'nvl(work_order_tax_status.tax_status_id, -1)'
 where FILTER_ID = 36
   and LABEL = 'Repair Status';

update PP_DATAWINDOW_HINTS
   set SELECT_NUMBER = 2, HINT = '/*+ no_merge(cpr_max) */',
       REASON = 'Improves Row_Number() function performance'
 where DATAWINDOW = 'TAXREP UPDATE INSERT_ACTIVITY_ID';

drop table REPAIR_WORK_ORDER_DEBUG;
create table REPAIR_WORK_ORDER_DEBUG as select * from REPAIR_WORK_ORDER_TEMP;

drop table REPAIR_WORK_ORDER_ORIG_DEBUG;
create table REPAIR_WORK_ORDER_ORIG_DEBUG as select * from REPAIR_WORK_ORDER_TEMP_ORIG;

drop table REPAIR_AMOUNT_ALLOCATE_DEBUG;
create table REPAIR_AMOUNT_ALLOCATE_DEBUG as select * from REPAIR_AMOUNT_ALLOCATE;

drop table REPAIR_BLANKET_PROC_DEBUG;
create table REPAIR_BLANKET_PROC_DEBUG as select * from REPAIR_BLANKET_PROCESSING;

drop function F_PP_GET_REPAIR_METHOD;

drop table REPAIR_CALC_PEND_TRANS;

--
-- Add charge_id to the Repair Calc Asset table.
--
alter table REPAIR_CALC_ASSET add CHARGE_ID number(22,0);

--
-- Add the home workspace.
--
update PPBASE_MENU_ITEMS
   set UO_WKSP_NAME = 'uo_rpr_home_wksp_main'
 where MODULE = 'REPAIRS'
   and IDENTIFIER = 'menu_wksp_home';

delete from PP_INTERFACE_DATES
 where INTERFACE_ID in (select INTERFACE_ID from PP_INTERFACE where WINDOW = 'w_tax_expense');

delete from PP_INTERFACE where WINDOW = 'w_tax_expense';

insert into WO_DELETE
   (ID, SORT_ORDER, EXCEPTION_FLAG, DESCRIPTION, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE)
values
   (53, 5300, 0, 'WORK_ORDER_TAX_REPAIRS', -1, 'WORK_ORDER_TAX_REPAIRS',
    'where work_order_id = <<wo_id>>');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (318, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs4.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;