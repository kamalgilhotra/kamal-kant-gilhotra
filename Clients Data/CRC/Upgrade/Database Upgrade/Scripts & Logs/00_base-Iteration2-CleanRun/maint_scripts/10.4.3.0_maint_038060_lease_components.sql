/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_038060_lease_components.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By        Reason for Change
|| -------- ---------- ----------------- ------------------------------------------
|| 10.4.3.0 06/04/2014 Shane "C" Ward    Create Lease Component
||=================================================================================
*/

create table LS_COMPONENT
(
 LS_COMPONENT_ID             number(22,0) not null,
 LS_COMP_STATUS_ID           number(22,0) not null,
 COMPANY_ID                  number(22,0) not null,
 DATE_RECEIVED               date,
 DESCRIPTION                 varchar2(35) not null,
 LONG_DESCRIPTION            varchar2(254),
 SERIAL_NUMBER               varchar2(35),
 PO_NUMBER                   varchar2(35),
 AMOUNT                      number(22,2) not null,
 LS_ASSET_ID                 number(22,0),
 INTERIM_INTEREST_START_DATE date,
 INTERIM_INTEREST            number(22,2),
 USER_ID                     varchar2(18),
 TIME_STAMP                  date
);

comment on table LS_COMPONENT is 'Table of Components that make up a lease.';
comment on column LS_COMPONENT.LS_COMPONENT_ID is 'System assigned identifier of component';
comment on column LS_COMPONENT.LS_COMP_STATUS_ID is 'Status of component, set list located in ls_component_status';
comment on column LS_COMPONENT.COMPANY_ID is 'System assigned identifier of company';
comment on column LS_COMPONENT.DATE_RECEIVED is 'Date component was received by lessor';
comment on column LS_COMPONENT.DESCRIPTION is 'Description of component.';
comment on column LS_COMPONENT.LONG_DESCRIPTION is 'Description of component.';
comment on column LS_COMPONENT.SERIAL_NUMBER is 'Serial Number for component/piece.';
comment on column LS_COMPONENT.PO_NUMBER is 'Powerplan Item Number.';
comment on column LS_COMPONENT.AMOUNT is 'Total amount assigned to component.';
comment on column LS_COMPONENT.LS_ASSET_ID is 'Asset the component is linked to.';
comment on column LS_COMPONENT.INTERIM_INTEREST_START_DATE is 'Date interest begins to be owed.';
comment on column LS_COMPONENT.INTERIM_INTEREST is 'Calculated interest for component.';

alter table LS_COMPONENT
   add constraint PK_LS_COMPONENT
       primary key (LS_COMPONENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_COMPONENT
   add constraint FK_COMPONENT_STATUS
       foreign key (LS_COMP_STATUS_ID)
       references LS_COMPONENT_STATUS (LS_COMPONENT_STATUS_ID);

alter table LS_COMPONENT
   add constraint FK_COMPONENT_COMPANY
       foreign key (COMPANY_ID)
       references COMPANY_SETUP (COMPANY_ID);

alter table LS_COMPONENT
   add constraint FK_COMPONENT_ASSET
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

--Lease Interim Rate
create table LS_LEASE_INTERIM_RATES
(
 LS_LEASE_ID number(22,0) not null,
 MONTH       date,
 RATE        number(22,8),
 TIME_STAMP  date,
 USER_ID     varchar2(18)
);

comment on table ls_lease_interim_rates is 'Stores monthly interim interest rates for a lease';
comment on column ls_lease_interim_rates.ls_lease_id is 'Lease Identifier';
comment on column ls_lease_interim_rates.month is 'Month interim interest rate goes into service';
comment on column ls_lease_interim_rates.rate is 'Monthly rate';

alter table LS_LEASE_INTERIM_RATES
   add constraint PK_LS_LEASE_INTERIM_RATES
       primary key (LS_LEASE_ID, MONTH)
       using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_INTERIM_RATES
   add constraint FK_LEASE_INTERIM_RATE
       foreign key (LS_LEASE_ID)
       references LS_LEASE (LEASE_ID);

--Lease Update
alter table LS_LEASE add DAYS_IN_YEAR number(3,0) default 365;
comment on column ls_lease.days_in_year is 'User specified days in year for calculating interest';
alter table LS_LEASE modify CUT_OFF_DAY  number(2,0) default 0;

--Lease Group Update
alter table LS_LEASE_GROUP add REQUIRE_COMPONENTS number(1,0) default 0;
comment on column ls_lease_group.require_components is 'Specifies whether the lease group requires components be assigned to assets in group';
alter table LS_LEASE_GROUP
   add constraint FK_REQUIRE_COMPONENTS
       foreign key (REQUIRE_COMPONENTS)
       references YES_NO (YES_NO_ID);

--Clean up LS_COMPONENT_STATUS
delete from LS_COMPONENT_STATUS where LS_COMPONENT_STATUS_ID > 3;

--Initiate
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'initiate_component', 'Initiate Component', 'uo_ls_component_wksp_wizard');
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'initiate_component', 2, 4, 'Component', 'menu_wksp_initiate', 'initiate_component', 1);

--Search
insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME)
values
   ('LESSEE', 'search_component', 'Search Component', 'uo_ls_component_wksp_search');

update PPBASE_MENU_ITEMS set ITEM_ORDER = 5 where WORKSPACE_IDENTIFIER = 'lessee_search';

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'search_component', 2, 4, 'Component', 'menu_wksp_details', 'search_component', 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1181, 0, 10, 4, 3, 0, 38060, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038060_lease_components.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;