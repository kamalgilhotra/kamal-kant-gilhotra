/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_031408_cpr_depr_calc_stg.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/04/2013 Brandon Beck   Point Release
||============================================================================
*/

alter table CPR_DEPR_CALC_STG
   add (TRF_WEIGHT          number(22,2),
        DEPR_CALC_MESSAGE   varchar2(255),
        SUBLEDGER_TYPE_ID   number(22,0),
        DEPR_CALC_STATUS    number(3,0),
        NET_TRF             number(22,2),
        NET_RES_TRF         number(22,2),
        NET_IMP_AMT         number(22,2),
        ACTIVITY            number(22,2),
        BEG_RES_AMT         number(22,2),
        ACTIVITY_3          number(22,2),
        DEPR_METHOD_SLE_EXP number(22,2),
        BEG_BAL_AMT         number(22,2),
        SCH_RATE            number(22,8),
        SCH_FACTOR          number(22,8));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (575, 0, 10, 4, 1, 0, 31408, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031408_cpr_depr_calc_stg.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;