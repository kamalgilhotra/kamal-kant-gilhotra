/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042927_pcm_old_windows_03_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 		B.Beck			Open old windows from left hand navigation 
||==========================================================================================
*/

update ppbase_menu_items
set item_order = item_order + 10
where module = 'pcm'
and parent_menu_identifier = 'fp_search'
;

update ppbase_menu_items
set item_order = 1
where module = 'pcm'
and menu_identifier = 'fp_maintain'
;

update ppbase_menu_items
set item_order = 2
where module = 'pcm'
and menu_identifier = 'fp_estimate'
;

update ppbase_menu_items
set item_order = 3
where module = 'pcm'
and menu_identifier = 'fp_justify'
;

update ppbase_menu_items
set item_order = 4
where module = 'pcm'
and menu_identifier = 'fp_review'
;

update ppbase_menu_items
set item_order = 5
where module = 'pcm'
and menu_identifier = 'fp_authorize'
;

update ppbase_menu_items
set item_order = 6
where module = 'pcm'
and menu_identifier = 'fp_monitor'
;

update ppbase_menu_items
set item_order = item_order + 10
where module = 'pcm'
and parent_menu_identifier = 'wo_search'
;

update ppbase_menu_items
set item_order = 1
where module = 'pcm'
and menu_identifier = 'wo_maintain'
;

update ppbase_menu_items
set item_order = 2
where module = 'pcm'
and menu_identifier = 'wo_estimate'
;

update ppbase_menu_items
set item_order = 3
where module = 'pcm'
and menu_identifier = 'wo_justify'
;

update ppbase_menu_items
set item_order = 4
where module = 'pcm'
and menu_identifier = 'wo_authorize'
;

update ppbase_menu_items
set item_order = 5
where module = 'pcm'
and menu_identifier = 'wo_monitor'
;

update ppbase_menu_items
set label = 'As Built'
where module = 'pcm'
and menu_identifier = 'wo_est_as_built'
;


insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
('pcm', 'fp_est_cashflows', 'Cashflow Estimates (FP)', 'uo_pcm_cntr_wksp_stub', 'Cashflow Estimates (FP)', 1);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
('pcm', 'fp_est_property', 'Property Estimates (FP)', 'uo_pcm_cntr_wksp_stub', 'Property Estimates (FP)', 1);

insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values
('pcm', 'fp_est_as_built', 'As Built (FP)', 'uo_pcm_cntr_wksp_stub', 'As Built (FP)', 1);

insert into ppbase_menu_items
(module, menu_identifier, menu_level, item_order, 
label, minihelp, parent_menu_identifier,
workspace_identifier, enable_yn)
values
('pcm', 'fp_est_cashflows', 4, 1,
'Cashflow Estimates', 'Funding Project Cashflow Estimates', 'fp_estimate',
'fp_est_cashflows', 1);

insert into ppbase_menu_items
(module, menu_identifier, menu_level, item_order, 
label, minihelp, parent_menu_identifier,
workspace_identifier, enable_yn)
values
('pcm', 'fp_est_property', 4, 2,
'Property Estimates', 'Funding Project Property Estimates', 'fp_estimate',
'fp_est_property', 0);

insert into ppbase_menu_items
(module, menu_identifier, menu_level, item_order, 
label, minihelp, parent_menu_identifier,
workspace_identifier, enable_yn)
values
('pcm', 'fp_est_as_built', 4, 3,
'As Built Estimates', 'Funding Project As Built Estimates', 'fp_estimate',
'fp_est_as_built', 0);

delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier = 'config_sysoptions'
;

delete from ppbase_workspace
where module = 'pcm'
and workspace_identifier = 'config_sysoptions'
;

update ppbase_workspace
set workspace_uo_name = 'w_wo_budget_review_list'
where module = 'pcm'
and workspace_identifier = 'fp_pend_budg_rev'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2302, 0, 2015, 1, 0, 0, 042927, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042927_pcm_old_windows_03_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;