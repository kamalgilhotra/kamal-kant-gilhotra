/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029825_prov.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/02/2013 Ron Ferentini  Point Release
||============================================================================
*/

insert into TAX_ACCRUAL_REPORT_OPTION
   (REPORT_OPTION_ID, DESCRIPTION)
   select 35, 'PAYMENT_RECON' from DUAL;
commit;

insert into TAX_ACCRUAL_REPORT_OPTION_DTL
   (REPORT_OPTION_ID, REPORT_OBJECT_ID)
   select 35, REPORT_OBJECT_ID
     from TAX_ACCRUAL_REPORT_OPTION_DTL
    where REPORT_OPTION_ID = 25
      and REPORT_OBJECT_ID not in (61, 62)
   union
   select 35, 53
     from DUAL
   union
   select 35, 54
     from DUAL;

insert into TAX_ACCRUAL_REP_SPECIAL_NOTE
   (SPECIAL_NOTE_ID, DESCRIPTION, REPORT_OPTION_ID, MONTH_REQUIRED, COMPARE_CASE, M_ROLLUP_REQUIRED,
    CALC_RATES, REPORT_TYPE, DW_PARAMETER_LIST, JE_TEMP_TABLE_IND, NS2FAS109_TEMP_TABLE_IND,
    REP_ROLLUP_GROUP_ID, REP_CONS_CATEGORY_ID)
   select 77,
          'PAYMENT_RECON',
          35,
          MONTH_REQUIRED,
          COMPARE_CASE,
          M_ROLLUP_REQUIRED,
          CALC_RATES,
          REPORT_TYPE,
          DW_PARAMETER_LIST,
          JE_TEMP_TABLE_IND,
          NS2FAS109_TEMP_TABLE_IND,
          null                     REP_ROLLUP_GROUP_ID,
          7                        REP_CONS_CATEGORY_ID
     from TAX_ACCRUAL_REP_SPECIAL_NOTE
    where DESCRIPTION = 'PAYMENTS';

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID)
   select 58020,
          'Payable Reconciliation Report',
          'Payable Reconciliation Report - Reports Beginning Balance, Accruals, Payments, Ending Balance By Tax Year',
          'Tax Accrual',
          'dw_tax_accrual_sub_recon_rpt',
          'PAYMENT_RECON',
          'Tax Accrual - 58020',
          3,
          31,
          1,
          1,
          1,
          1
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_TYPE
   (REP_CONS_TYPE_ID, DESCRIPTION, DATAWINDOW, SORT_SPEC, REP_CONS_CATEGORY_ID, BALANCES_IND)
   select 70,
          'Payable Recon by Year',
          'dw_tax_accrual_sub_recon_rpt',
          'ta_version_id, company_id, gl_account, tax_year, sort, detail_description',
          7,
          0
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_COLS
   (REP_CONS_TYPE_ID, CONS_COLNAME, RANK, CONS_COLTYPE, CONS_DESCR_COLNAME, GROUP_ID)
   select 70, 'tax_year', 1, 'string', 'tax_year', 0
     from DUAL
   union
   select 70, 'detail_description', 1, 'string', 'detail_description', 1
     from DUAL
   union
   select 70, 'gl_account', 1, 'string', 'gl_account', 3
     from DUAL
   union
   select 70, 'sort_type', 1, 'string', 'sort_type', 2
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70,
          1,
          'H',
          -1,
          1,
          1,
          'TEXT',
          1,
          'Payable Reconciliation by Tax Year',
          'TEXT',
          'Payable Reconciliation by Tax Year',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 2, 'H', -1, 2, 1, 'USER_TITLE', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70,
          3,
          'H',
          -1,
          3,
          1,
          'COL_VALUE_TEXT',
          0,
          'tree_type_descr',
          'COL_VALUE_TEXT',
          'tree_type_descr',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 4, 'H', -1, 4, 1, 'COL_VALUE_TEXT', 0, 'case_descr', null, null, 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 5, 'H', -1, 5, 0, 'FILLER', 0, null, null, null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 6, 'H', 3, 2, 1, 'TEXT', 0, null, 'COL_VALUE_TEXT', 'gl_account', 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 7, 'H', 2, 1, 0, 'TEXT', 0, null, 'COL_VALUE_TEXT', 'sort_type', 'T', null, null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70,
          8,
          'D',
          1,
          1,
          0,
          'COL_VALUE_NUM',
          0,
          'amount',
          'COL_VALUE_TEXT',
          'detail_description',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70,
          9,
          'F',
          3,
          1,
          1,
          'COL_VALUE_NUM',
          0,
          null,
          'COL_VALUE_TEXT',
          'gl_account',
          'T',
          null,
          null
     from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 10, 'F', 3, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 11, 'F', 2, 1, 0, 'TEXT', 0, null, 'FILLER', null, 'T', '0', null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 12, 'F', 99, 2, 0, 'TEXT', 0, null, 'FILLER', null, 'T', null, null from DUAL;

insert into TAX_ACCRUAL_REP_CONS_ROWS
   (REP_CONS_TYPE_ID, ROW_ID, ROW_TYPE, GROUP_ID, RANK, COLOR_ID, VALUE_TYPE, IS_REPORT_TITLE,
    ROW_VALUE, LABEL_VALUE_TYPE, LABEL_VALUE, COL_FORMAT, DISPLAY_IND_FIELD, TOTAL_COL_FIELDNAME)
   select 70, 13, 'F', 99, 1, 1, 'COL_VALUE_NUM', 0, null, 'TEXT', 'Grand Total', 'T', null, null
     from DUAL;

-- not sure why but 51024 grid is not showing grid options due to rep_cons_category_id = 0
update TAX_ACCRUAL_REP_SPECIAL_NOTE
   set REP_CONS_CATEGORY_ID = 2
 where DESCRIPTION = 'CONSOLIDATING-ACCOUNT-CONS';

-- not sure why there's duplicate rows of this special note in the base, but deleting ones with rep_cons_category_id = 0
delete from TAX_ACCRUAL_REP_SPECIAL_NOTE
 where DESCRIPTION = 'DT_BALANCES_ACCT_CONS'
   and REP_CONS_CATEGORY_ID = 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (682, 0, 10, 4, 2, 0, 29825, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029825_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
