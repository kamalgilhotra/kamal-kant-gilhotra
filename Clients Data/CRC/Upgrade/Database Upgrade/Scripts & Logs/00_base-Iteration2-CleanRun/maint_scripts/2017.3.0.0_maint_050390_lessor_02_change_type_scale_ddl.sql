/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050930_lessor_02_change_type_scale_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 02/09/2018 Andrew Hill    Remove scale specification from types
||============================================================================
*/

drop type lsr_bucket_result_tab;

CREATE OR REPLACE TYPE lsr_bucket_result AS OBJECT (MONTH DATE,
                                                    executory_accrued_1 NUMBER,
                                                    executory_accrued_2 NUMBER,
                                                    executory_accrued_3 NUMBER,
                                                    executory_accrued_4 NUMBER,
                                                    executory_accrued_5 NUMBER,
                                                    executory_accrued_6 NUMBER,
                                                    executory_accrued_7 NUMBER,
                                                    executory_accrued_8 NUMBER,
                                                    executory_accrued_9 NUMBER,
                                                    executory_accrued_10 NUMBER,
                                                    executory_received_1 NUMBER,
                                                    executory_received_2 NUMBER,
                                                    executory_received_3 NUMBER,
                                                    executory_received_4 NUMBER,
                                                    executory_received_5 NUMBER,
                                                    executory_received_6 NUMBER,
                                                    executory_received_7 NUMBER,
                                                    executory_received_8 NUMBER,
                                                    executory_received_9 NUMBER,
                                                    executory_received_10 NUMBER,
                                                    contingent_accrued_1 NUMBER,
                                                    contingent_accrued_2 NUMBER,
                                                    contingent_accrued_3 NUMBER,
                                                    contingent_accrued_4 NUMBER,
                                                    contingent_accrued_5 NUMBER,
                                                    contingent_accrued_6 NUMBER,
                                                    contingent_accrued_7 NUMBER,
                                                    contingent_accrued_8 NUMBER,
                                                    contingent_accrued_9 NUMBER,
                                                    contingent_accrued_10 NUMBER,
                                                    contingent_received_1 NUMBER,
                                                    contingent_received_2 NUMBER,
                                                    contingent_received_3 NUMBER,
                                                    contingent_received_4 NUMBER,
                                                    contingent_received_5 NUMBER,
                                                    contingent_received_6 NUMBER,
                                                    contingent_received_7 NUMBER,
                                                    contingent_received_8 NUMBER,
                                                    contingent_received_9 NUMBER,
                                                    contingent_received_10 NUMBER);
/            

CREATE TYPE lsr_bucket_result_tab AS TABLE OF lsr_bucket_result;
/

DROP type lsr_ilr_df_schedule_result_tab;

CREATE OR REPLACE TYPE lsr_ilr_df_schedule_result IS OBJECT(MONTH DATE,
                                                            principal_received NUMBER,
                                                            interest_income_received      NUMBER,
                                                            interest_income_accrued      NUMBER,
                                                            principal_accrued            NUMBER,
                                                            begin_receivable              NUMBER,
                                                            end_receivable                NUMBER,
                                                            begin_lt_receivable          NUMBER,
                                                            end_lt_receivable            NUMBER,
                                                            initial_direct_cost          NUMBER,
                                                            executory_accrual1           NUMBER,
                                                            executory_accrual2           NUMBER,
                                                            executory_accrual3           NUMBER,
                                                            executory_accrual4           NUMBER,
                                                            executory_accrual5           NUMBER,
                                                            executory_accrual6           NUMBER,
                                                            executory_accrual7           NUMBER,
                                                            executory_accrual8           NUMBER,
                                                            executory_accrual9           NUMBER,
                                                            executory_accrual10          NUMBER,
                                                            executory_paid1              NUMBER,
                                                            executory_paid2              NUMBER,
                                                            executory_paid3              NUMBER,
                                                            executory_paid4              NUMBER,
                                                            executory_paid5              NUMBER,
                                                            executory_paid6              NUMBER,
                                                            executory_paid7              NUMBER,
                                                            executory_paid8              NUMBER,
                                                            executory_paid9              NUMBER,
                                                            executory_paid10             NUMBER,
                                                            contingent_accrual1          NUMBER,
                                                            contingent_accrual2          NUMBER,
                                                            contingent_accrual3          NUMBER,
                                                            contingent_accrual4          NUMBER,
                                                            contingent_accrual5          NUMBER,
                                                            contingent_accrual6          NUMBER,
                                                            contingent_accrual7          NUMBER,
                                                            contingent_accrual8          NUMBER,
                                                            contingent_accrual9          NUMBER,
                                                            contingent_accrual10         NUMBER,
                                                            contingent_paid1             NUMBER,
                                                            contingent_paid2             NUMBER,
                                                            contingent_paid3             NUMBER,
                                                            contingent_paid4             NUMBER,
                                                            contingent_paid5             NUMBER,
                                                            contingent_paid6             NUMBER,
                                                            contingent_paid7             NUMBER,
                                                            contingent_paid8             NUMBER,
                                                            contingent_paid9             NUMBER,
                                                            contingent_paid10            NUMBER,
                                                            begin_unguaranteed_residual NUMBER,
                                                            int_on_unguaranteed_residual NUMBER,
                                                            end_unguaranteed_residual NUMBER,
                                                            begin_net_investment NUMBER,
                                                            int_on_net_investment NUMBER,
                                                            end_net_investment NUMBER,
                                                            begin_deferred_profit number,
                                                            recognized_profit number,
                                                            end_deferred_profit number,
                                                            rate_implicit FLOAT,
                                                            compounded_rate FLOAT,
                                                            discount_rate FLOAT,
                                                            rate_implicit_ni FLOAT,
                                                            compounded_rate_ni FLOAT,
                                                            discount_rate_ni FLOAT,
                                                            begin_lease_receivable number,
                                                            npv_lease_payments NUMBER,
                                                            npv_guaranteed_residual NUMBER,
                                                            npv_unguaranteed_residual NUMBER,
                                                            selling_profit_loss NUMBER,
                                                            cost_of_goods_sold NUMBER);
/

CREATE TYPE lsr_ilr_df_schedule_result_tab AS TABLE OF lsr_ilr_df_schedule_result;
/

DROP TYPE lsr_ilr_op_sch_pay_term_tab;

CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_PAY_TERM AS OBJECT( payment_month_frequency NUMBER,
                                                          payment_term_start_date DATE,
                                                          number_of_terms NUMBER,
                                                          payment_amount NUMBER,
                                                          executory_buckets lsr_bucket_amount_tab,
                                                          contingent_buckets lsr_bucket_amount_tab,
                                                          is_prepay NUMBER(1,0));
/

CREATE TYPE lsr_ilr_op_sch_pay_term_tab AS TABLE OF lsr_ilr_op_sch_pay_term;
/

DROP TYPE lsr_ilr_op_sch_result_tab;

CREATE OR REPLACE TYPE lsr_ilr_op_sch_result IS OBJECT (MONTH DATE,
                                                        interest_income_received      NUMBER,
                                                        interest_income_accrued      NUMBER,
                                                        interest_rental_recvd_spread NUMBER,
                                                        begin_deferred_rev            NUMBER,
                                                        deferred_rev                  NUMBER,
                                                        end_deferred_rev              NUMBER,
                                                        begin_receivable              NUMBER,
                                                        end_receivable                NUMBER,
                                                        begin_lt_receivable          NUMBER,
                                                        end_lt_receivable            NUMBER,
                                                        initial_direct_cost          NUMBER,
                                                        executory_accrual1           NUMBER,
                                                        executory_accrual2           NUMBER,
                                                        executory_accrual3           NUMBER,
                                                        executory_accrual4           NUMBER,
                                                        executory_accrual5           NUMBER,
                                                        executory_accrual6           NUMBER,
                                                        executory_accrual7           NUMBER,
                                                        executory_accrual8           NUMBER,
                                                        executory_accrual9           NUMBER,
                                                        executory_accrual10          NUMBER,
                                                        executory_paid1              NUMBER,
                                                        executory_paid2              NUMBER,
                                                        executory_paid3              NUMBER,
                                                        executory_paid4              NUMBER,
                                                        executory_paid5              NUMBER,
                                                        executory_paid6              NUMBER,
                                                        executory_paid7              NUMBER,
                                                        executory_paid8              NUMBER,
                                                        executory_paid9              NUMBER,
                                                        executory_paid10             NUMBER,
                                                        contingent_accrual1          NUMBER,
                                                        contingent_accrual2          NUMBER,
                                                        contingent_accrual3          NUMBER,
                                                        contingent_accrual4          NUMBER,
                                                        contingent_accrual5          NUMBER,
                                                        contingent_accrual6          NUMBER,
                                                        contingent_accrual7          NUMBER,
                                                        contingent_accrual8          NUMBER,
                                                        contingent_accrual9          NUMBER,
                                                        contingent_accrual10         NUMBER,
                                                        contingent_paid1             NUMBER,
                                                        contingent_paid2             NUMBER,
                                                        contingent_paid3             NUMBER,
                                                        contingent_paid4             NUMBER,
                                                        contingent_paid5             NUMBER,
                                                        contingent_paid6             NUMBER,
                                                        contingent_paid7             NUMBER,
                                                        contingent_paid8             NUMBER,
                                                        contingent_paid9             NUMBER,
                                                        contingent_paid10            NUMBER);
/
                        
CREATE TYPE lsr_ilr_op_sch_result_tab AS TABLE OF lsr_ilr_op_sch_result;
/

drop type LSR_ILR_SALES_SCH_INFO_tab;

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO AS OBJECT(carrying_cost NUMBER,
                                                        fair_market_value NUMBER,
                                                        guaranteed_residual NUMBER,
                                                        estimated_residual NUMBER,
                                                        days_in_year NUMBER,
                                                        purchase_option_amount NUMBER,
                                                        termination_amount NUMBER);
/

create or replace type LSR_ILR_SALES_SCH_INFO_tab as table of lsr_ilr_sales_sch_info;
/

drop type LSR_ILR_SALES_SCH_RESULT_tab;

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER,
                                                          interest_income_received      NUMBER,
                                                          interest_income_accrued      NUMBER,
                                                          principal_accrued            NUMBER,
                                                          begin_receivable              NUMBER,
                                                          end_receivable                NUMBER,
                                                          begin_lt_receivable          NUMBER,
                                                          end_lt_receivable            NUMBER,
                                                          initial_direct_cost          NUMBER,
                                                          executory_accrual1           NUMBER,
                                                          executory_accrual2           NUMBER,
                                                          executory_accrual3           NUMBER,
                                                          executory_accrual4           NUMBER,
                                                          executory_accrual5           NUMBER,
                                                          executory_accrual6           NUMBER,
                                                          executory_accrual7           NUMBER,
                                                          executory_accrual8           NUMBER,
                                                          executory_accrual9           NUMBER,
                                                          executory_accrual10          NUMBER,
                                                          executory_paid1              NUMBER,
                                                          executory_paid2              NUMBER,
                                                          executory_paid3              NUMBER,
                                                          executory_paid4              NUMBER,
                                                          executory_paid5              NUMBER,
                                                          executory_paid6              NUMBER,
                                                          executory_paid7              NUMBER,
                                                          executory_paid8              NUMBER,
                                                          executory_paid9              NUMBER,
                                                          executory_paid10             NUMBER,
                                                          contingent_accrual1          NUMBER,
                                                          contingent_accrual2          NUMBER,
                                                          contingent_accrual3          NUMBER,
                                                          contingent_accrual4          NUMBER,
                                                          contingent_accrual5          NUMBER,
                                                          contingent_accrual6          NUMBER,
                                                          contingent_accrual7          NUMBER,
                                                          contingent_accrual8          NUMBER,
                                                          contingent_accrual9          NUMBER,
                                                          contingent_accrual10         NUMBER,
                                                          contingent_paid1             NUMBER,
                                                          contingent_paid2             NUMBER,
                                                          contingent_paid3             NUMBER,
                                                          contingent_paid4             NUMBER,
                                                          contingent_paid5             NUMBER,
                                                          contingent_paid6             NUMBER,
                                                          contingent_paid7             NUMBER,
                                                          contingent_paid8             NUMBER,
                                                          contingent_paid9             NUMBER,
                                                          contingent_paid10            NUMBER,
                                                          begin_unguaranteed_residual NUMBER,
                                                          int_on_unguaranteed_residual NUMBER,
                                                          end_unguaranteed_residual NUMBER,
                                                          begin_net_investment NUMBER,
                                                          int_on_net_investment NUMBER,
                                                          end_net_investment NUMBER,
                                                          rate_implicit FLOAT,
                                                          compounded_rate FLOAT,
                                                          discount_rate FLOAT,
                                                          rate_implicit_ni FLOAT,
                                                          compounded_rate_ni FLOAT,
                                                          discount_rate_ni FLOAT,
                                                          begin_lease_receivable number,
                                                          npv_lease_payments NUMBER,
                                                          npv_guaranteed_residual NUMBER,
                                                          npv_unguaranteed_residual NUMBER,
                                                          selling_profit_loss NUMBER,
                                                          cost_of_goods_sold NUMBER);
/

CREATE TYPE lsr_ilr_sales_sch_result_tab AS TABLE OF lsr_ilr_sales_sch_result;
/

DROP type LSR_INIT_DIRECT_COST_INFO_tab;

CREATE OR REPLACE TYPE LSR_INIT_DIRECT_COST_INFO AS OBJECT (idc_group_id NUMBER,
                                                            date_incurred DATE,
                                                            amount NUMBER,
                                                            DESCRIPTION VARCHAR2(254));
/

CREATE TYPE lsr_init_direct_cost_info_tab AS TABLE OF lsr_init_direct_cost_info;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4128, 0, 2017, 3, 0, 0, 50930, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050930_lessor_02_change_type_scale_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;