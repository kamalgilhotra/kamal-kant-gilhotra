/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_037471_proptax_in_schd_g_mod.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 09/09/2014 Anand Rajashekar    Changes to report 513506
||========================================================================================
*/

--Changes to report 513506

update PP_REPORTS
   set REPORT_NUMBER = 'IN-Central Sch E-Assess',
       DESCRIPTION = 'IN-Central Sched E Assess Distrib',
       LONG_DESCRIPTION = 'IN-Central Sched E Assess Distrib'
 where REPORT_ID = 513506;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1410, 0, 10, 4, 3, 0, 37471, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037471_proptax_in_schd_g_mod.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
