/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052193_lessee_02_lease_group_security_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/15/2018 K Powers       Add lease group security to payment wksp
||============================================================================
*/

-- Set up LS_LEASE_GROUP_SECURITY for Table Maintenance

insert into POWERPLANT_TABLES
(table_name,pp_table_type_id,description,long_description,subsystem_screen,subsystem_display,subsystem)
values
('ls_lease_group_security','s','Lease Group Security','Lease Group Security','lessee','lessee','lessee');

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,
COLUMN_RANK,READ_ONLY)
values
('lease_group_id','ls_lease_group_security','lease_group','p','Lease Group','Lease Group ID',1,0);

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,
COLUMN_RANK,READ_ONLY)
values
('lease_user_id','ls_lease_group_security','users','p','User ID','Lease User ID',2,0);

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,
COLUMN_RANK,READ_ONLY)
values
('time_stamp','ls_lease_group_security','e','Time Stamp','Time Stamp',100,0);

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,
COLUMN_RANK,READ_ONLY)
values
('user_id','ls_lease_group_security','e','User ID','User ID',100,0);

INSERT INTO POWERPLANT_DDDW 
(dropdown_name,table_name,code_col,display_col)
values
('lease_group','ls_lease_group','lease_group_id','description');

INSERT INTO POWERPLANT_DDDW 
(dropdown_name,table_name,code_col,display_col)
values
('users','pp_security_users','user_id','description');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(10463, 0, 2018, 1, 0, 0, 52193, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052193_lessee_02_lease_group_security_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;