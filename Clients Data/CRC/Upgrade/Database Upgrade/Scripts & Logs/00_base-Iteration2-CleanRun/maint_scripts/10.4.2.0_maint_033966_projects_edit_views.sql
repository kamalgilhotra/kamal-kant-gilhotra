/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033966_projects_edit_views.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   11/20/2013 Stephen Motter
||============================================================================
*/

create or replace view WO_EST_MONTH_IS_EDITABLE as
select
   a.work_order_number, a.work_order_id, a.revision, to_number(a.year) year,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'01')),1,0,decode(sign(to_number(a.year||'01') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'01') - actuals_month_number),1,1,0))))))) january,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'02')),1,0,decode(sign(to_number(a.year||'02') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'02') - actuals_month_number),1,1,0))))))) february,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'03')),1,0,decode(sign(to_number(a.year||'03') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'03') - actuals_month_number),1,1,0))))))) march,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'04')),1,0,decode(sign(to_number(a.year||'04') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'04') - actuals_month_number),1,1,0))))))) april,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'05')),1,0,decode(sign(to_number(a.year||'05') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'05') - actuals_month_number),1,1,0))))))) may,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'06')),1,0,decode(sign(to_number(a.year||'06') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'06') - actuals_month_number),1,1,0))))))) june,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'07')),1,0,decode(sign(to_number(a.year||'07') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'07') - actuals_month_number),1,1,0))))))) july,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'08')),1,0,decode(sign(to_number(a.year||'08') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'08') - actuals_month_number),1,1,0))))))) august,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'09')),1,0,decode(sign(to_number(a.year||'09') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'09') - actuals_month_number),1,1,0))))))) september,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'10')),1,0,decode(sign(to_number(a.year||'10') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'10') - actuals_month_number),1,1,0))))))) october,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'11')),1,0,decode(sign(to_number(a.year||'11') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'11') - actuals_month_number),1,1,0))))))) november,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'12')),1,0,decode(sign(to_number(a.year||'12') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'12') - actuals_month_number),1,1,0))))))) december,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,1)))) revision_edit, actuals_month_number
from (
   select woc.funding_wo_indicator,
      woc.work_order_number,
      woa.work_order_id,
      woa.revision,
      pp.year,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and open_for_entry = 0) not_open_for_entry,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and bring_in_subs = 1) subs_only,
      to_number(to_char(woa.est_start_date, 'yyyymm')) start_month_number,
      to_number(to_char(woa.est_complete_date, 'yyyymm')) end_month_number,
      greatest(nvl(woa.actuals_month_number,0),(select nvl(max(nvl(actuals_month,0)),0) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and active = 1)) actuals_month_number,
      (
         select decode(woc.funding_wo_indicator,1,
            decode(lower(trim(nvl(max(control_value),'no'))),'no',
               decode(woa.approval_status_id,2,1,3,1,5,1,0),
               0),
            decode(woa.approval_status_id,2,1,3,1,5,1,0))
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FUNDPROJ - Edit Approved FP'))
      ) approval_status_id,
      (
         select decode(lower(trim(nvl(max(control_value),'yes'))),'yes',
            decode(woa.review_status,2,1,3,1,0),
            0)
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FP EST - Budget Review Locks Ests'))
      ) review_status_id
   from work_order_control woc, work_order_approval woa, pp_table_years pp, temp_work_order two
   where woc.work_order_id = woa.work_order_id
   and woc.work_order_id = two.work_order_id
   ) a;

create or replace view WO_EST_MONTH_IS_EDITABLE_FY as
select
   a.work_order_number, a.work_order_id, a.revision, to_number(a.year) year,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'01')),1,0,decode(sign(to_number(a.year||'01') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'01') - actuals_month_number),1,1,0))))))) january,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'02')),1,0,decode(sign(to_number(a.year||'02') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'02') - actuals_month_number),1,1,0))))))) february,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'03')),1,0,decode(sign(to_number(a.year||'03') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'03') - actuals_month_number),1,1,0))))))) march,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'04')),1,0,decode(sign(to_number(a.year||'04') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'04') - actuals_month_number),1,1,0))))))) april,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'05')),1,0,decode(sign(to_number(a.year||'05') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'05') - actuals_month_number),1,1,0))))))) may,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'06')),1,0,decode(sign(to_number(a.year||'06') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'06') - actuals_month_number),1,1,0))))))) june,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'07')),1,0,decode(sign(to_number(a.year||'07') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'07') - actuals_month_number),1,1,0))))))) july,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'08')),1,0,decode(sign(to_number(a.year||'08') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'08') - actuals_month_number),1,1,0))))))) august,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'09')),1,0,decode(sign(to_number(a.year||'09') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'09') - actuals_month_number),1,1,0))))))) september,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'10')),1,0,decode(sign(to_number(a.year||'10') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'10') - actuals_month_number),1,1,0))))))) october,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'11')),1,0,decode(sign(to_number(a.year||'11') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'11') - actuals_month_number),1,1,0))))))) november,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(start_month_number - to_number(a.year||'12')),1,0,decode(sign(to_number(a.year||'12') - end_month_number),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'12') - actuals_month_number),1,1,0))))))) december,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,1)))) revision_edit, actuals_month_number
from (
   select woc.funding_wo_indicator,
      woc.work_order_number,
      woa.work_order_id,
      woa.revision,
      pp.year,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and open_for_entry = 0) not_open_for_entry,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and bring_in_subs = 1) subs_only,
      (select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(woa.est_start_date, 'yyyymm'))) start_month_number,
      (select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(woa.est_complete_date, 'yyyymm'))) end_month_number,
      nvl((select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = greatest(nvl(woa.actuals_month_number,0),(select nvl(max(nvl(actuals_month,0)),0) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and active = 1))),0) actuals_month_number,
      (
         select decode(woc.funding_wo_indicator,1,
            decode(lower(trim(nvl(max(control_value),'no'))),'no',
               decode(woa.approval_status_id,2,1,3,1,5,1,0),
               0),
            decode(woa.approval_status_id,2,1,3,1,5,1,0))
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FUNDPROJ - Edit Approved FP'))
      ) approval_status_id,
      (
         select decode(lower(trim(nvl(max(control_value),'yes'))),'yes',
            decode(woa.review_status,2,1,3,1,0),
            0)
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FP EST - Budget Review Locks Ests'))
      ) review_status_id
   from work_order_control woc, work_order_approval woa, pp_table_years pp, temp_work_order two
   where woc.work_order_id = woa.work_order_id
   and woc.work_order_id = two.work_order_id
   ) a;

create or replace view WO_EST_MONTH_IS_EDITABLE_JT as
select
   a.work_order_number, a.work_order_id, a.revision, a.job_task_id, to_number(a.year) year,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'01')),1,0,decode(sign(to_number(a.year||'01') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'01') - actuals_month_number),1,1,0))))))) january,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'02')),1,0,decode(sign(to_number(a.year||'02') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'02') - actuals_month_number),1,1,0))))))) february,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'03')),1,0,decode(sign(to_number(a.year||'03') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'03') - actuals_month_number),1,1,0))))))) march,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'04')),1,0,decode(sign(to_number(a.year||'04') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'04') - actuals_month_number),1,1,0))))))) april,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'05')),1,0,decode(sign(to_number(a.year||'05') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'05') - actuals_month_number),1,1,0))))))) may,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'06')),1,0,decode(sign(to_number(a.year||'06') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'06') - actuals_month_number),1,1,0))))))) june,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'07')),1,0,decode(sign(to_number(a.year||'07') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'07') - actuals_month_number),1,1,0))))))) july,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'08')),1,0,decode(sign(to_number(a.year||'08') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'08') - actuals_month_number),1,1,0))))))) august,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'09')),1,0,decode(sign(to_number(a.year||'09') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'09') - actuals_month_number),1,1,0))))))) september,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'10')),1,0,decode(sign(to_number(a.year||'10') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'10') - actuals_month_number),1,1,0))))))) october,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'11')),1,0,decode(sign(to_number(a.year||'11') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'11') - actuals_month_number),1,1,0))))))) november,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'12')),1,0,decode(sign(to_number(a.year||'12') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'12') - actuals_month_number),1,1,0))))))) december,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,1)))) revision_edit, actuals_month_number
from (
   select woc.funding_wo_indicator,
      woc.work_order_number,
      woa.work_order_id,
      woa.revision,
      jt.job_task_id,
      pp.year,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and open_for_entry = 0) not_open_for_entry,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and bring_in_subs = 1) subs_only,
      to_number(to_char(woa.est_start_date, 'yyyymm')) start_month_number,
      to_number(to_char(woa.est_complete_date, 'yyyymm')) end_month_number,
      greatest(nvl(woa.actuals_month_number,0),(select nvl(max(nvl(actuals_month,0)),0) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and active = 1)) actuals_month_number,
      (
         select decode(woc.funding_wo_indicator,1,
            decode(lower(trim(nvl(max(control_value),'no'))),'no',
               decode(woa.approval_status_id,2,1,3,1,5,1,0),
               0),
            decode(woa.approval_status_id,2,1,3,1,5,1,0))
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FUNDPROJ - Edit Approved FP'))
      ) approval_status_id,
      (
         select decode(lower(trim(nvl(max(control_value),'yes'))),'yes',
            decode(woa.review_status,2,1,3,1,0),
            0)
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FP EST - Budget Review Locks Ests'))
      ) review_status_id,
      nvl(to_number(to_char(jt.est_start_date, 'yyyymm')),0) jt_start_month_number,
      nvl(to_number(to_char(jt.est_complete_date, 'yyyymm')),999999) jt_end_month_number
   from work_order_control woc, work_order_approval woa, pp_table_years pp, job_task jt, temp_job_task tjt
   where woc.work_order_id = woa.work_order_id
   and woc.work_order_id = jt.work_order_id
   and jt.work_order_id = tjt.work_order_id
   and jt.job_task_id = tjt.job_task_id
   ) a;

create or replace view WO_EST_MONTH_IS_EDITABLE_JT_FY as
select
   a.work_order_number, a.work_order_id, a.revision, a.job_task_id, to_number(a.year) year,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'01')),1,0,decode(sign(to_number(a.year||'01') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'01') - actuals_month_number),1,1,0))))))) january,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'02')),1,0,decode(sign(to_number(a.year||'02') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'02') - actuals_month_number),1,1,0))))))) february,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'03')),1,0,decode(sign(to_number(a.year||'03') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'03') - actuals_month_number),1,1,0))))))) march,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'04')),1,0,decode(sign(to_number(a.year||'04') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'04') - actuals_month_number),1,1,0))))))) april,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'05')),1,0,decode(sign(to_number(a.year||'05') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'05') - actuals_month_number),1,1,0))))))) may,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'06')),1,0,decode(sign(to_number(a.year||'06') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'06') - actuals_month_number),1,1,0))))))) june,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'07')),1,0,decode(sign(to_number(a.year||'07') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'07') - actuals_month_number),1,1,0))))))) july,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'08')),1,0,decode(sign(to_number(a.year||'08') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'08') - actuals_month_number),1,1,0))))))) august,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'09')),1,0,decode(sign(to_number(a.year||'09') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'09') - actuals_month_number),1,1,0))))))) september,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'10')),1,0,decode(sign(to_number(a.year||'10') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'10') - actuals_month_number),1,1,0))))))) october,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'11')),1,0,decode(sign(to_number(a.year||'11') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'11') - actuals_month_number),1,1,0))))))) november,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(sign(greatest(start_month_number,jt_start_month_number) - to_number(a.year||'12')),1,0,decode(sign(to_number(a.year||'12') - least(end_month_number,jt_end_month_number)),1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,decode(sign(to_number(a.year||'12') - actuals_month_number),1,1,0))))))) december,
   decode(not_open_for_entry,1,0,decode(subs_only,1,0,decode(approval_status_id,1,0,decode(review_status_id,1,0,1)))) revision_edit, actuals_month_number
from (
   select woc.funding_wo_indicator,
      woc.work_order_number,
      woa.work_order_id,
      woa.revision,
      jt.job_task_id,
      pp.year,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and open_for_entry = 0) not_open_for_entry,
      (select decode(count(*),0,0,1) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and fp.active = 1 and bring_in_subs = 1) subs_only,
      (select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(woa.est_start_date, 'yyyymm'))) start_month_number,
      (select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(woa.est_complete_date, 'yyyymm'))) end_month_number,
      nvl((select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = greatest(nvl(woa.actuals_month_number,0),(select nvl(max(nvl(actuals_month,0)),0) from budget_version_fund_proj fp, budget_version bv where fp.work_order_id = woa.work_order_id and fp.revision = woa.revision and bv.budget_version_id = fp.budget_version_id and active = 1))),0) actuals_month_number,
      (
         select decode(woc.funding_wo_indicator,1,
            decode(lower(trim(nvl(max(control_value),'no'))),'no',
               decode(woa.approval_status_id,2,1,3,1,5,1,0),
               0),
            decode(woa.approval_status_id,2,1,3,1,5,1,0))
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FUNDPROJ - Edit Approved FP'))
      ) approval_status_id,
      (
         select decode(lower(trim(nvl(max(control_value),'yes'))),'yes',
            decode(woa.review_status,2,1,3,1,0),
            0)
         from pp_system_control_companies sc
         where sc.company_id = woc.company_id
         and lower(trim(sc.control_name)) = lower(trim('FP EST - Budget Review Locks Ests'))
      ) review_status_id,
      nvl((select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(jt.est_start_date, 'yyyymm'))),0) jt_start_month_number,
      nvl((select fiscal_year * 100 + fiscal_month from pp_calendar where month_number = to_number(to_char(jt.est_complete_date, 'yyyymm'))),999999) jt_end_month_number
   from work_order_control woc, work_order_approval woa, pp_table_years pp, job_task jt, temp_job_task tjt
   where woc.work_order_id = woa.work_order_id
   and woc.work_order_id = jt.work_order_id
   and jt.work_order_id = tjt.work_order_id
   and jt.job_task_id = tjt.job_task_id
   ) a;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (766, 0, 10, 4, 2, 0, 33966, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033966_projects_edit_views.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;