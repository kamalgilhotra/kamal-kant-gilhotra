/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009895_sys_ANALYZE_TABLE.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   05/17/2012 Lee Quinn      Point Release
||============================================================================
*/

-- Grant added to v10_sys_grants.sql
-- grant select on sys.v_$instance to PWRPLANT;
-- grant analyze any to PWRPLANT;

create or replace function ANALYZE_TABLE(A_TABLENAME in varchar2,
                                         PCT         in number default 10) return number as

   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: ANALYZE_TABLE
   || Description:
   ||============================================================================
   || Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version Date       Revised By     Reason for Change
   || ------- ---------- -------------- -----------------------------------------
   || 1.3     04/13/2005 Roger Roach    Create
   || 1.4     07/14/2005 Roger Roach    Added check for Global Temp Tables
   || 1.5     02/23/2006 Roger Roach    Allow Verion 9 and higher to analyze Global Temp Tables
   || 1.6     06/23/2006 Roger Roach    ADDED APS code for partition tables
   || 1.7     06/28/2006 Roger Roach    Allow the pct of data changed to be argument
   || 1.8     07/20/2007 Roger Roach    Removed flush_database_monitoring_info
   || 1.9     08/15/2007 Roger Roach    Add flush_database_monitoring_info, but now checks for errors
   || 1.9     08/15/2007 Roger Roach    Add flush_database_monitoring_info, but now checks for errors
   || 2.0     10/08/2007 Roger Roach    Fixed problem with checking for Oracle Version
   || 2.1     11/15/2007 Roger Roach    Fixed problem with the anaylze not working on Oracle 9 Temporary Tables
   || 2.2     03/20/2008 Roger Roach    Added support for user's tables
   || 2.3     04/09/2008 Roger Roach    Added a check for new tables with no statistics (irow is null)
   || 2.4     04/09/2008 Roger Roach    Added a force analyze command with a negative pct
   || 2.5     05/09/2008 Roger Roach    Removed version check
   || 2.6     09/31/2008 Roger Roach    Aadded nvl to the column AT.num_rows
   || 2.7     10/20/2008 Roger Roach    Re-added support for user's tables
   || 2.8     06/03/2009 Roger Roach    Fixed problem when analyzing user tables when the pwrplant table did not exist
   || 2.10    02/16/2010 Roger Roach    Do not analyze temporary tables
   || 2.11    11/17/2010 Roger Roach    Do analyze temporary tables
   || 2.12    06/21/2011 Roger Roach    Analyze temporary tables fix
   || 2.13    06/28/2011 Roger Roach    Analyze temporary tables fix2
   || 2.14    08/16/2011 Roger Roach    Added check for percent table changes of 2% or more
   || 2.15    03/07/2012 Lee Quinn      Added LOCKED STATISTICS Logic.
   ||============================================================================
   */

   LOCKED_STATS exception;
   -- This is the error code returned from DBMS_OUTPUT.GATHER_TABLE_STATS
   -- if the table has it's statistics locked.
   -- We will capture the exception and just return a -1.  We will not raise an exception.
   pragma exception_init(LOCKED_STATS, -20005);

   IROWS            int;
   VCMONITORING     varchar2(5);
   VCPARTIONED      varchar2(5);
   ICHANGES         int;
   NPERCENT         number(22, 2);
   VCPARTIONNAME    varchar(30);
   VCTRUNCATED      varchar2(5);
   NUM              number(22, 0);
   CODE             number(22, 0);
   TABLENAME        varchar(40);
   TAB_OWNER        varchar2(40) := 'PWRPLANT';
   SKIP_MOD_CHANGES boolean := false;

   cursor CURCHANGES is
      select UTM.DELETES + UTM.INSERTS + UTM.UPDATES,
             UTM.PARTITION_NAME,
             UTM.TRUNCATED,
             PT.NUM_ROWS
        from USER_TAB_MODIFICATIONS UTM, ALL_TAB_PARTITIONS PT
       where UTM.TABLE_NAME = PT.TABLE_NAME
         and UTM.PARTITION_NAME = PT.PARTITION_NAME
         and UPPER(UTM.TABLE_NAME) = UPPER(TABLENAME);

begin
   TABLENAME := UPPER(A_TABLENAME);
   -- ** Check for Temporary Table **
   select count(*)
     into NUM
     from ALL_TABLES
    where UPPER(TABLENAME) = TABLE_NAME
      and TEMPORARY = 'Y'
      and ROWNUM = 1;

   --IF num > 0
   --THEN
   --   RETURN 0;
   --END IF;

   -- ** check for table in user's schema **
   select count(*)
     into NUM
     from ALL_TABLES
    where UPPER(TABLENAME) = TABLE_NAME
      and OWNER = user;

   if NUM > 0 then
      if IS_NUMBER(SUBSTR(user, 1, 1)) = 1 then
         TAB_OWNER := '"' || UPPER(user) || '"';
      else
         TAB_OWNER := '' || UPPER(user) || '';
      end if;
   end if;

   if PCT < 0 then
      -- ** if pct is negative, then  always analyze the table **
      DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => TAB_OWNER,
                                    TABNAME          => UPPER(TABLENAME),
                                    ESTIMATE_PERCENT => 10,
                                    CASCADE          => true);
      return 0;
   end if;
   begin
      select NVL(AT.NUM_ROWS, 0), AT.MONITORING, AT.PARTITIONED
        into IROWS, VCMONITORING, VCPARTIONED
        from ALL_TABLES AT
       where UPPER(AT.OWNER) = TAB_OWNER
         and UPPER(AT.TABLE_NAME) = UPPER(TABLENAME);
   exception
      when others then
         VCMONITORING := 'NO';
   end;
   if VCMONITORING = 'NO' then
      -- ** table is not monitored **
      DBMS_STATS.GATHER_TABLE_STATS(OWNNAME => TAB_OWNER,
                                    TABNAME => UPPER(TABLENAME),
                                    CASCADE => true);
   else
      -- ** flush all changes so that they get into the view **
      begin
         DBMS_STATS.FLUSH_DATABASE_MONITORING_INFO;
      exception
         when others then
            SKIP_MOD_CHANGES := true;
      end;

      if VCPARTIONED = 'NO' then
         -- ** table is not partitioned **
         begin
            if SKIP_MOD_CHANGES = false then
               select UTM.DELETES + UTM.INSERTS + UTM.UPDATES, UTM.TRUNCATED
                 into ICHANGES, VCTRUNCATED
                 from USER_TAB_MODIFICATIONS UTM
                where UPPER(UTM.TABLE_NAME) = UPPER(TABLENAME);
            else
               IROWS := 0;
            end if;

            if IROWS = 0 or VCTRUNCATED = 'YES' or SKIP_MOD_CHANGES = true then
               -- ** 0 rows usually means that table has never been analyzed **
               NPERCENT := 100;
            else
               NPERCENT := ROUND(ICHANGES / NVL(IROWS, 1) * 100, 2);
            end if;

            if NPERCENT >= 2 then
               -- more than 2% changed - do analyze
               DBMS_STATS.GATHER_TABLE_STATS(OWNNAME          => TAB_OWNER,
                                             TABNAME          => UPPER(TABLENAME),
                                             ESTIMATE_PERCENT => PCT,
                                             CASCADE          => true);
            end if;
         exception
            when NO_DATA_FOUND then
               -- ** no modifications to table - do not need to analyze **
               return 0;
         end;
      else

         -- ** table is partitioned **
         open CURCHANGES;
         -- ** step through cursor **
         loop
            fetch CURCHANGES
               into ICHANGES, VCPARTIONNAME, VCTRUNCATED, IROWS;
            exit when CURCHANGES%notfound;

            if IROWS = 0 or VCTRUNCATED = 'YES' then
               -- ** 0 rows usually means that partition has never been analyzed **
               NPERCENT := 100;
            else
               NPERCENT := ROUND(ICHANGES / IROWS * 100, 2);
            end if;

            if NPERCENT >= PCT then
               -- ** more than 10 % changed - do analyze **
               DBMS_STATS.GATHER_TABLE_STATS(OWNNAME     => TAB_OWNER,
                                             TABNAME     => UPPER(TABLENAME),
                                             PARTNAME    => VCPARTIONNAME,
                                             GRANULARITY => 'partition', -- only analyze partition
                                             CASCADE     => true);
            end if;
         end loop;
         close CURCHANGES;
      end if;
   end if;

   return 0;
exception
   when LOCKED_STATS then
      return -1;
   when NO_DATA_FOUND then
      RAISE_APPLICATION_ERROR(-20000,
                              'Failed Anaylze Table ' || TAB_OWNER || '.' || TABLENAME ||
                              ' - Table Does not exist');
      return -1;
   when others then
      CODE := sqlcode;
      RAISE_APPLICATION_ERROR(-20000,
                              'Failed Anaylze Table ' || TAB_OWNER || '.' || TABLENAME ||
                              ' Error: ' || TO_CHAR(CODE) || '' || sqlerrm(CODE));
      return -1;
end;
/

SHOW ERRORS;

--create or replace public synonym ANALYZE_TABLE for PWRPLANT.ANALYZE_TABLE;
--grant execute on PWRPLANT.ANALYZE_TABLE TO PWRPLANT_ROLE_DEV;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (140, 0, 10, 3, 5, 0, 9895, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009895_sys_ANALYZE_TABLE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
