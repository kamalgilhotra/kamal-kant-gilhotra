/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052436_lessee_04_import_ilr_rev_cleanup_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 3/14/2019 	 Shane "C" Ward  Clean up ILR Revision import issues
||============================================================================
*/

UPDATE pp_import_column
SET    import_column_name = 'payment_term_xlate'
WHERE  import_type_id = 272
AND    column_name = 'payment_term_type_id';

UPDATE pp_import_column
SET    import_column_name = NULL
WHERE  import_type_id = 272
AND    column_name NOT IN (SELECT column_name
                           FROM   pp_import_column_lookup
                           WHERE  import_type_id = 272);
						   
--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15842, 0, 2018, 2, 0, 0, 52346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052346_lessee_04_import_ilr_rev_cleanup_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;						   