/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049550_lessor_01_ilr_import_xchg_rate_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/25/2017 Shane "C" Ward  Add columns to import for in service exchange rate
||============================================================================
*/

ALTER TABLE lsr_import_ilr ADD in_service_exchange_rate NUMBER(22,8);
ALTER TABLE lsr_import_ilr_archive ADD in_service_exchange_rate NUMBER(22,8);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3831, 0, 2017, 1, 0, 0, 49550, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049550_lessor_01_ilr_import_xchg_rate_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;