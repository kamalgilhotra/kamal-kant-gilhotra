 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046379_projects_amp_proj_id_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 09/12/2016 Sarah Byers    Add AMP_PROJECT_ID to WORK_ORDER_CONTROL
 ||============================================================================
 */ 

alter table work_order_control add amp_project_id varchar2(36) null;

comment on column work_order_control.amp_project_id is 'System assigned identifier of the AMP Project ID.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3290, 0, 2016, 1, 0, 0, 046379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046379_projects_amp_proj_id_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;