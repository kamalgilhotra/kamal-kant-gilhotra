/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044021_lease_fast_approval_system_control_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/12/2015 William Davis  Add option for clients to skip full 
||                                      workflow processing for lease payments.
||                                      This is for performance reasons.
||============================================================================
*/

insert into pp_system_control_company
(CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID, USER_DISABLED, CONTROL_TYPE)
values 
((select max(control_id) from pp_system_control_company) + 1,
'Lease Fast Payment Approvals', 
'no', 
'Allows lease approvals to skip workflow processing',
'Allows lease approvals to skip workflow processing',
-1, 
null, 
null);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2596, 0, 2015, 2, 0, 0, 044021, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044021_lease_fast_approval_system_control_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;