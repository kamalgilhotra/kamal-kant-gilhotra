/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036600_depr_depr_beg_bals.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/25/2014 Brandon Beck
||============================================================================
*/

create global temporary table DEPR_BEG_BALS
(
 DEPR_GROUP_ID   number(22,0),
 SET_OF_BOOKS_ID number(22,0),
 BEGIN_BALANCE   number(22,2),
 CUM_RETIREMENTS number(22,2)
) on commit preserve rows;

comment on table DEPR_BEG_BALS is 'A temporary table to hold beginning balances for the fiscal year and cumulative retirements.  Used for Forecasted Auto Retirements';

comment on column DEPR_BEG_BALS.DEPR_GROUP_ID is 'An Internal key to link to the depreciation group';
comment on column DEPR_BEG_BALS.SET_OF_BOOKS_ID is 'An Internal key to link to the set of books';
comment on column DEPR_BEG_BALS.BEGIN_BALANCE is 'The beginning balance for the fiscal year';
comment on column DEPR_BEG_BALS.CUM_RETIREMENTS is 'The cumulative retirements for the fiscal year';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1000, 0, 10, 4, 2, 0, 36600, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036600_depr_depr_beg_bals.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;