/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038858_reg_grossup_factor.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.2.7 07/03/2014 Ryan Oliveria        Reg Revenue Grossup Factor Input Scale
||========================================================================================
*/

insert into REG_CO_INPUT_SCALE
   (SCALE_ID, REG_COMPANY_ID, INPUT_SCALE)
   select 10, REG_COMPANY_ID, 8
     from REG_COMPANY
    where REG_COMPANY_ID <> 0
      and not exists (select 1
             from REG_CO_INPUT_SCALE
            where SCALE_ID = 10
              and REG_COMPANY_ID = REG_COMPANY.REG_COMPANY_ID);

insert into REG_JUR_INPUT_SCALE
   (SCALE_ID, REG_JUR_TEMPLATE_ID, INPUT_SCALE)
   select 10, REG_JUR_TEMPLATE_ID, 8
     from REG_JUR_TEMPLATE
    where not exists (select 1
             from REG_JUR_INPUT_SCALE
            where SCALE_ID = 10
              and REG_JUR_TEMPLATE_ID = REG_JUR_TEMPLATE.REG_JUR_TEMPLATE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1170, 0, 10, 4, 2, 7, 38858, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038858_reg_grossup_factor.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;