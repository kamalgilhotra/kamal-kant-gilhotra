/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047870_lease_update_report_time_opt_L1005_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/16/2017 Shane Ward		 Set Lessee - 1005 Time Option to Multi Currency
||============================================================================
*/

UPDATE pp_reports_time_option
   SET description = 'Lease Currecy Type No Month'
 WHERE pp_report_time_option_id = 203;

INSERT INTO pp_reports_time_option
  (pp_report_time_option_id,
   description,
   parameter_uo_name,
   dwname1,
   label1,
   keycolumn1)
VALUES
  (205,
   'Lease Currency Type Single Month',
   'uo_ppbase_report_parms_dddw_mnum',
   'dddw_ls_currency_type',
   'Currency Type',
   'ls_currency_type_id');

UPDATE pp_reports
   SET pp_report_time_option_id = 205
 WHERE datawindow = 'dw_ls_rpt_future_est_min_payment';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3500, 0, 2017, 1, 0, 0, 47870, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047870_lease_update_report_time_opt_L1005_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 