 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044342_sys_ssp_versions_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2015.1.2.0  08/03/2015 David Haupt    Increasing field width for robustness
 ||============================================================================
 */

ALTER TABLE pp_custom_pbd_versions
  MODIFY pbd_name VARCHAR2(100);

ALTER TABLE pp_custom_pbd_versions
  MODIFY version VARCHAR2(100);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2737, 0, 2015, 1, 2, 0, 044342, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.2.0_maint_044342_sys_ssp_versions_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;