/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045412_jobserver_03fix_invalid_job_arguments_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 06/07/2016 Jared Watkins		   Fix the arguments for some jobs that are in invalid format
||============================================================================
*/

update pp_job_executable set arguments = '{"parms": [{"type": "string", "value": "Create Funding Project", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company ID (-1 for All Companies)"},{"type": "string", "value": "", "label": "Batch / Instance" }]}'
where job_type = 'SSP' and job_name = 'Create Funding Project';

update pp_job_executable set arguments = '{"parms": [{"type": "string", "value": "Load Monthly Estimates", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company ID (-1 for All Companies)" },{"type": "string", "value": "", "label": "Replace Estimates (true/false)"},{"type": "string", "value": "", "label": "Batch / Instance"},{"type": "string", "value": "", "label": "Replace Year (true / false)"},{"type": "string", "value": "", "label": "Funding WO Indicator" }]}'
where job_type = 'SSP' and job_name = 'Load Monthly Estimates';

update pp_job_executable set arguments = '{"parms": [{"type": "string", "value": "Create Work Order", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company ID (-1 for All Companies)"},{"type": "string", "value": "", "label": "Batch / Instance" }]}'
where job_type = 'SSP' and job_name = 'Create Work Order';

update pp_job_executable set arguments = '{"parms": [{"type": "string", "value": "Create Job Task", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company ID (-1 for All Companies)"},{"type": "string", "value": "", "label": "Batch / Instance" }]}'
where job_type = 'SSP' and job_name = 'Create Job Task';

update pp_job_executable set arguments = '{"parms": [{"type": "string", "value": "Load Unit Est", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company ID (-1 for All Companies)"},{"type": "string", "value": "", "label": "Replace Estimates (true / false)"},{"type": "string", "value": "", "label": "Process OCR (true / false)"},{"type": "string", "value": "", "label": "Batch / Instance"}]}'
where job_type = 'SSP' and job_name = 'Load Unit Est';

update pp_job_executable set arguments = '{"parms":[{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"","label":"Company","required":"true"}]}'
where job_type = 'PowerBuilder' and job_name = 'CR Delete Budget Allocations';

update pp_job_executable set arguments = '{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"}]}'
where job_type = 'PowerBuilder' and job_name = 'PP to CR';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3212, 0, 2016, 1, 0, 0, 045412, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045412_jobserver_03fix_invalid_job_arguments_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;