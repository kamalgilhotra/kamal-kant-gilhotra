/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038216_pwrtax_rpts_conv.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version   Date       Revised By          Reason for Change
|| --------- ---------- ------------------- ------------------------------------
|| 10.4.3.0  07/07/2014 Alex Pivoshenko     Initial conversion of 10 PowerTax reports
||                      Andrew Scott        to Base Gui
||                      Anand Rajashekar
||============================================================================
*/

----7/7 : Alex config and original 5 report conversions
insert into PWRPLANT.PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403003, sysdate, user, 'Schedule G Overlay Report',
    'Depreciation detail report for compliance filing with tax books overlayed', 'PowerTax',
    'dw_tax_rpt_sched_g_overlay_grp_cons', '', 'PowerTax_Rollup', '', 'PwrTax - 8', '', '', '', 1, 130, 52, 53, 1, 3, '',
    '', null, '', '', 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY,
    EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (164, 'Company', 'dw', 'dw_tax_company_by_version_filter', 'company_id', 'description', 'N', 0, 0, 1, 0, 0);

update PP_DYNAMIC_FILTER_MAPPING
   set FILTER_ID = 164
 where FILTER_ID = 101
   and PP_REPORT_FILTER_ID = 53;

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403004, sysdate, user, 'Depreciation Summary Report 6', 'Depreciation Summary', 'PowerTax',
    'dw_tax_rpt_depr_summ_rep6_grp_cons', '', 'PowerTax_Rollup', '', 'PwrTax - 16', '', '', '', 1, 130, 52, 53, 1, 3, '',
    '', null, '', '', 0);
insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403006, sysdate, user, 'Depreciation Balance Report 2',
    'Depreciation Balance Report 2. Balances by vintage and tax class', 'PowerTax', 'dw_tax_rpt_depr_balance_2_grp_cons',
    '', 'PowerTax_Rollup', '', 'PwrTax - 22', '', '', '', 1, 130, 52, 53, 1, 3, '', '', null, '', '', 0);

drop view COMPANY_CONSOL_VIEW;

create table COMPANY_CONSOL_VIEW
(
 PP_TREE_CATEGORY_ID number(22,0) not null,
 CONSOLIDATION_ID    number(22,0) not null,
 PARENT_ID           number(22,0) not null,
 COMPANY_ID          number(22,0) not null,
 DESCRIPTION         varchar2(254),
 SORT_ORDER          number(22,0) not null
);

alter table COMPANY_CONSOL_VIEW
   add constraint PK_CCV_CAT_CONSOL_PRNT_CO
       primary key (PP_TREE_CATEGORY_ID, CONSOLIDATION_ID, PARENT_ID, COMPANY_ID)
       using index tablespace PWRPLANT_IDX;

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403007, sysdate, user, 'Depreciation Balance Report 6', 'Depreciation Balance Report 6. Balances by tax class',
    'PowerTax', 'dw_tax_rpt_depr_balance_6_grp_cons', '', 'PowerTax_Rollup', '', 'PwrTax - 26', '', '', '', 1, 130, 52,
    53, 1, 3, '', '', null, '', '', 0);
insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403011, sysdate, user, 'Gain Loss Analysis Report Ext', 'Gain Loss Analysis Report Extraordinary Gain/Loss',
    'PowerTax', 'dw_tax_rpt_gl_ex_conv_ovrlay_grp_cons', '', 'PowerTax_Rollup', '', 'PwrTax - 51', '', '', '', 1, 130,
    52, 53, 1, 3, '', '', null, '', '', 0);

----7/9 Andrew: report # 6 converted.
insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (403013, sysdate, user, 'Gain Loss Analysis Report-Bonus',
    'Analyzes the gain/loss calculation.  Retirements from Basis and Retirements from Reserve columns include retirements for any tax credit m-item.',
    'PowerTax', 'dw_tax_rpt_gain_loss_analysis_bonus', '', 'PowerTax_Rollup', '', 'PwrTax - 53', '',
    '', '', 1, 130, 52, 53, 1, 3, '', '', null, '', '', 0);

----7/11 Andrew: report # 7 converted.
insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (400002, sysdate, user, 'PowerTax Detail Vintage',
    'PowerTax Detail Vintage.  Includes all header and balance information.  Grouped by vintage, convention, and tax book.',
    'PowerTax', 'dw_tax_rpt_record_vint_detail', '', ' ', '', 'PwrTax - 7', '', '', '', 1, 29, 50,
    50, 1, 3, '', '', null, '', '', 0);

----7/11 Alex: report # 8 converted. + another dynamic filter.
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
values
   (68, 'Powertax - tax book compare', 'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (167, 'Tax Book', 'dw', 'tax_book.tax_book_id', 'dw_tax_book_filter', 'tax_book_id', 'description', 'n', 1, 1, 0, 0,
    0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (168, 'Tax Book 2', 'dw', 'tax_book2.tax_book_id', 'dw_tax_book_filter', 'tax_book_id', 'description', 'n', 1, 1, 0,
    0, 0);

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (68, 102);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (68, 103);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (68, 110);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (68, 164);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (68, 167);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (68, 168);

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (400004, sysdate, user, 'Depreciation Compare Books 1',
    'Depreciation Compare Books 1. For Preference Items  By Vintage and Tax Class', 'PowerTax',
    'dw_tax_rpt_depr_comp_1_grp_cons', '', 'PowerTax_Rollup', '', 'PwrTax - 35', '', '', '', 1, 29,
    52, 68, 1, 3, '', '', null, '', '', 0);

----7/11, 7/16 Andrew: report # 9 converted.  + created a new report filter.
insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (69, 'PowerTax - Depreciation - Tax Book', 'tax_record_control', 'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values
   (169, 'Tax Book', 'dw', 'tax_book.tax_book_id', 'dw_tax_book_filter', 'tax_book_id', 'description', 'N', 0, 0, 0, 0,
    0);

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (69, 169);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (69, 102);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (69, 103);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (69, 110);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (69, 164);

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (400003, sysdate, user, 'Depreciation Summary Report 2',
    'PowerTax Depreciation Summary Report 2.  By Vintage and Tax Class', 'PowerTax', 'dw_tax_rpt_depr_sum_rep2', '',
    'PowerTax_Rollup', '', 'PwrTax - 12', '', '', '', 1, 29, 52, 69, 1, 3, '', '', null, '', '', 0);

----7/11 Andrew: report # 10 converted.
insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (400005, sysdate, user, 'Depreciation Compare Books 2',
    'Depreciation Compare Books 2.  For Preference Items  By Vintage', 'PowerTax', 'dw_tax_rpt_depr_comp_2_grp_cons', '',
    'PowerTax_Rollup', '', 'PwrTax - 36', '', '', '', 1, 29, 52, 68, 1, 3, '', '', null, '', '', 0);

---- Alex: revert the change above due to performance issues
update PP_DYNAMIC_FILTER set SQLS_COLUMN_EXPRESSION = 'tax_book_view_a.x' where FILTER_ID = 167;

update PP_DYNAMIC_FILTER set SQLS_COLUMN_EXPRESSION = 'tax_book_view_b.x' where FILTER_ID = 168;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1261, 0, 10, 4, 3, 0, 38216, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038216_pwrtax_rpts_conv.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;