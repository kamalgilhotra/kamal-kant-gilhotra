/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051330_lessor_03_recreate_termination_op_table_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/24/2018 Anand R     	  Create new table to store termination info for Operating ILR
||============================================================================
*/
drop table lsr_ilr_termination_op;

CREATE TABLE lsr_ilr_termination_op (
  ilr_id                        NUMBER(22,0)   NOT NULL,
  termination_date              DATE           NOT NULL,
  rent_balance                  NUMBER(22,2)   NOT NULL,
  deferred_costs                NUMBER(22,2)   NOT NULL,
  penalty_other_payment         NUMBER(22,2)   NOT NULL,
  income_gainloss               NUMBER(22,2)   NOT NULL,
  rent_balance_acct_id          NUMBER(22,0)   NULL,
  deferred_costs_acct_id        NUMBER(22,0)   NULL,
  penalty_other_payment_acct_id NUMBER(22,0)   NULL,
  income_gainloss_acct_id       NUMBER(22,0)   NULL,
  termination_source            VARCHAR2(35)   NOT NULL,
  comments                      VARCHAR2(2000) NOT NULL,
  user_id                       VARCHAR2(18)   NULL,
  time_stamp                    DATE           NULL
);

ALTER TABLE lsr_ilr_termination_op
  ADD CONSTRAINT lsr_ilr_termination_op_pk PRIMARY KEY (
    ilr_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

ALTER TABLE lsr_ilr_termination_op
  ADD CONSTRAINT lsr_ilr_termination_op_fk FOREIGN KEY (
    ilr_id
  ) REFERENCES lsr_ilr (
    ilr_id
  )
;

COMMENT ON TABLE lsr_ilr_termination_op IS 'Table to store termination information for Operating type ILR';

COMMENT ON COLUMN lsr_ilr_termination_op.ilr_id IS 'System generated id of the ILR';
COMMENT ON COLUMN lsr_ilr_termination_op.termination_date IS 'Date when the ILR will be terminated';
COMMENT ON COLUMN lsr_ilr_termination_op.rent_balance IS 'Sum of Deferred rent balance and accrued rent balance';
COMMENT ON COLUMN lsr_ilr_termination_op.deferred_costs IS 'Unamortized amount, initial direct cost less amortized costs';
COMMENT ON COLUMN lsr_ilr_termination_op.penalty_other_payment IS 'Penalty or Other payment amount paid towards ILR termination';
COMMENT ON COLUMN lsr_ilr_termination_op.income_gainloss IS 'Calculated gain/loss for the ILR termination';
COMMENT ON COLUMN lsr_ilr_termination_op.rent_balance_acct_id IS 'System generated account ID for rent balance';
COMMENT ON COLUMN lsr_ilr_termination_op.deferred_costs_acct_id IS 'System generated account ID for deferred costs';
COMMENT ON COLUMN lsr_ilr_termination_op.penalty_other_payment_acct_id IS 'System generated account ID for penalty or other payment';
COMMENT ON COLUMN lsr_ilr_termination_op.income_gainloss_acct_id IS 'System generated account ID for gain/loss payment';
COMMENT ON COLUMN lsr_ilr_termination_op.termination_source IS 'Stores where the termination originated - Early or Auto Termination';
COMMENT ON COLUMN lsr_ilr_termination_op.comments IS 'Comments for the ILR termination';
COMMENT ON COLUMN lsr_ilr_termination_op.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_termination_op.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6303, 0, 2017, 4, 0, 0, 51330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051330_lessor_02_recreate_termination_op_table_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;