/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044736_budgetcap_extend_department_id_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2   08/18/2015 Jared Watkins  The column needs to hold larger data
 ||                                    since it is mapped to by a varchar2(60).
 ||============================================================================
 */ 

ALTER TABLE wo_est_monthly_upload MODIFY department_id VARCHAR2(60);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2812, 0, 2015, 2, 0, 0, 044736, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044736_budgetcap_extend_department_id_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;