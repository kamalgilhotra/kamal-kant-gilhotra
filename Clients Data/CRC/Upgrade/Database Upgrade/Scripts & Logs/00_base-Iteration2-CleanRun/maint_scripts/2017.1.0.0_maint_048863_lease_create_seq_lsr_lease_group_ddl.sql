/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048863_lease_create_seq_lsr_lease_group_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 08/31/2017 Anand R       Create new sequence for lsr_lease_group table
||============================================================================
*/ 

--create sequence to be used by lsr_lease_group
create sequence LSR_LEASE_GROUP_SEQ
 MINVALUE 1
 START WITH 1 
 INCREMENT BY 1 
 CACHE 20;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3686, 0, 2017, 1, 0, 0, 48863, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048863_lease_create_seq_lsr_lease_group_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;