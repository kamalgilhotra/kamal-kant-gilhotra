/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044449_depr_group_control_syntax_error_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2     07/21/2015 Jared Watkins    maint 44449
||============================================================================
*/

update powerplant_columns set dropdown_name='prop_tax_district' where table_name = 'asset_location' and column_name ='tax_district_id';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2698, 0, 2015, 2, 0, 0, 044449, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044449_depr_group_control_syntax_error_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;