/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047019_lease_add_pay_term_var_payments_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 02/23/2017 Anand R          Add new table to associate ilr payment terms to variable payments
||============================================================================
*/

--drop the two columns from ls_ilr_payment_term. They are not needed
alter table LS_ILR_PAYMENT_TERM set unused
( VARIABLE_PAYMENT_ID, INCLUDE_INIT_MEASUREMENT );

create table LS_ILR_PAYMENT_TERM_VAR_PAYMNT (
ILR_ID number(22,0) not null,
REVISION number(22,0) not null,
PAYMENT_TERM_ID number(22,0) not null,
RENT_TYPE VARCHAR2(35) DEFAULT 'Executory',
BUCKET_NUMBER number(2,0) not null,
VARIABLE_PAYMENT_ID number(22,0) not null,
INCL_IN_INITIAL_MEASURE number(22,0) not null,
USER_ID       varchar2(18),
TIME_STAMP    date
);

alter table LS_ILR_PAYMENT_TERM_VAR_PAYMNT
      add constraint LS_ILR_PAY_TERM_VAR_PAYMNT_PK
      primary key (ILR_ID, REVISION, PAYMENT_TERM_ID, BUCKET_NUMBER )
      using index tablespace PWRPLANT_IDX;
       

alter table LS_ILR_PAYMENT_TERM_VAR_PAYMNT
      add constraint LS_ILR_PAY_TERM_VAR_PAYMNT_FK1
      foreign key (ILR_ID, REVISION, PAYMENT_TERM_ID )
      references LS_ILR_PAYMENT_TERM(ILR_ID, REVISION, PAYMENT_TERM_ID);
             
      
alter table LS_ILR_PAYMENT_TERM_VAR_PAYMNT
      add constraint LS_ILR_PAY_TERM_VAR_PAYMNT_FK2
      foreign key ( RENT_TYPE, BUCKET_NUMBER )
      references LS_RENT_BUCKET_ADMIN(RENT_TYPE, BUCKET_NUMBER);      
      
alter table LS_ILR_PAYMENT_TERM_VAR_PAYMNT
      add constraint LS_ILR_PAY_TERM_VAR_PAYMNT_FK3
      foreign key (VARIABLE_PAYMENT_ID )
      references LS_VARIABLE_PAYMENT(VARIABLE_PAYMENT_ID);       
    
comment on table PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT IS 'Lease table that stores the association between payment terms and variable payments';    
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.ILR_ID IS 'System assigned identifier for an Individual Lease Record or ILR';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.REVISION IS 'System assigned identifier for a revision of an ILR';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.PAYMENT_TERM_ID IS 'System assigned identifier for a payment term on an ILR';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.RENT_TYPE IS 'System assigned value for rent type used in Lease';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.BUCKET_NUMBER IS 'System assigned identifier for a bucket used in Lease';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.VARIABLE_PAYMENT_ID IS 'System assigned identifier for a variable payment';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.INCL_IN_INITIAL_MEASURE IS 'Include variable payments in initial schedule measurement, 1=Yes, 0=No';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.USER_ID IS 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.LS_ILR_PAYMENT_TERM_VAR_PAYMNT.TIME_STAMP IS 'Standard system-assigned user id used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3378, 0, 2017, 1, 0, 0, 47019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047019_lease_add_pay_term_var_payments_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;


