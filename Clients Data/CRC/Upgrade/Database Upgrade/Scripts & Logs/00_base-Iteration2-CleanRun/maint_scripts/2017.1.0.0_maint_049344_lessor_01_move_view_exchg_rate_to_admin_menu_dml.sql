/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049344_lessor_01_move_view_exchg_rate_to_admin_menu_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/16/2017 Alex Healey    Display view exchange rates under the proper heading - Admin
||============================================================================
*/
   
UPDATE  ppbase_menu_items
SET    item_order = 7
WHERE
  item_order          = 8
  AND menu_identifier = 'admin_view_exchange_rates'
  AND module          = 'LESSOR'
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3796, 0, 2017, 1, 0, 0, 49344, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049344_lessor_01_move_view_exchg_rate_to_admin_menu_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;