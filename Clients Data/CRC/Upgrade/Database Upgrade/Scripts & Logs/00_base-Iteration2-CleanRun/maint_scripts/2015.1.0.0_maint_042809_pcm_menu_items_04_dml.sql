/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042809_pcm_menu_items_04_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 		B.Beck			Modify Left Hand Navigation
||==========================================================================================
*/

update ppbase_menu_items
set item_order = decode(item_order - 1, 0, 3, item_order - 1)
where module = 'pcm'
and parent_menu_identifier = 'wo_monitor'
;

update ppbase_menu_items
set label = 'CashFlow Estimates'
where module = 'pcm'
and menu_identifier = 'wo_est_cashflows';

update ppbase_menu_items
set label = 'Property Estimates'
where module = 'pcm'
and menu_identifier = 'wo_est_property';

update ppbase_menu_items
set label = 'Material Reconciliation', item_order = 6, enable_yn = 0
where module = 'pcm'
and menu_identifier = 'wo_est_matl_rec';

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'wo_est_as_built', 4, 3, 'As Builts', 
	'Work Order As Builts', 'wo_estimate', '', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'wo_est_grid', 4, 4, 'Grid Estimates', 
	'Work Order Grid Estimates', 'wo_estimate', '', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'wo_est_eng', 4, 5, 'Engineering Estimates', 
	'Work Order Engineering Estimates', 'wo_estimate', '', 1
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2282, 0, 2015, 1, 0, 0, 042809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042809_pcm_menu_items_04_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;