/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045250_mappr_remove_options_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 12/03/2015 Andrew Scott     mobile updates remove options not fully developed
||============================================================================
*/


delete from mobile_disp_inpts_mapping where input_id in (select input_id from mobile_inputs where description in ('Completion Date', 'Estimated Completion Date'));
delete from mobile_inputs where description in ('Completion Date', 'Estimated Completion Date');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2990, 0, 2015, 2, 0, 0, 45250, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045250_mappr_remove_options_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
   SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
