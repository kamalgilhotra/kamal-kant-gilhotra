/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_01_add_prepay_amortization_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/13/2018 Josh Sandler 		Add prepay columns
||============================================================================
*/

ALTER TABLE LS_ASSET_SCHEDULE
  ADD beg_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ASSET_SCHEDULE
  ADD prepay_amortization NUMBER(22, 2) NULL;

ALTER TABLE LS_ASSET_SCHEDULE
  ADD prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ASSET_SCHEDULE
  ADD end_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD beg_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD prepay_amortization NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD end_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD beg_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD prepay_amortization NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD end_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_SCHEDULE
  ADD beg_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_SCHEDULE
  ADD prepay_amortization NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_SCHEDULE
  ADD prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_SCHEDULE
  ADD end_prepaid_rent NUMBER(22, 2) NULL;

--Missing remeasurement columns on LS_SUMMARY_FORECAST
ALTER TABLE LS_SUMMARY_FORECAST
  ADD principal_remeasurement NUMBER(22, 2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
  ADD liability_remeasurement NUMBER(22, 2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
  ADD beg_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
  ADD prepay_amortization NUMBER(22, 2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
  ADD prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_SUMMARY_FORECAST
  ADD end_prepaid_rent NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_STG
  ADD prepaid_rent_balance NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_STG.prepaid_rent_balance IS 'Total Prepaid Rent Amount from prior revision used in calculation of asset cost.';

ALTER TABLE LS_ILR_ASSET_STG
  ADD alloc_prepaid_rent NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_ASSET_STG.alloc_prepaid_rent IS 'Prepaid rent amount allocated to each asset record. Used in capital cost calculation.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.prepay_amortization IS 'The prepayment amortization amount for the period.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.prepaid_rent IS 'The prepaid rent amount for the period.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.end_prepaid_rent IS 'The ending prepaid rent for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.prepay_amortization IS 'The prepayment amortization amount for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.prepaid_rent IS 'The prepaid rent amount for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.end_prepaid_rent IS 'The ending prepaid rent for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.prepay_amortization IS 'The prepayment amortization amount for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.prepaid_rent IS 'The prepaid rent amount for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_prepaid_rent IS 'The ending prepaid rent for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.prepay_amortization IS 'The prepayment amortization amount for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.prepaid_rent IS 'The prepaid rent amount for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.end_prepaid_rent IS 'The ending prepaid rent for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.prepay_amortization IS 'The prepayment amortization amount for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.prepay_amortization IS 'The prepayment amortization amount for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.prepaid_rent IS 'The prepaid rent amount for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.end_prepaid_rent IS 'The ending prepaid rent for the period.';

ALTER TABLE LS_ILR_ACCOUNT
  ADD prepaid_rent_account_id NUMBER(22);

COMMENT ON COLUMN LS_ILR_ACCOUNT.prepaid_rent_account_id IS 'The prepaid rent account.';

ALTER TABLE LS_ILR_ACCOUNT
  ADD CONSTRAINT fk_ls_ilr_account_prepaid FOREIGN KEY ( prepaid_rent_account_id ) REFERENCES GL_ACCOUNT ( gl_account_id );

ALTER TABLE LS_ILR_GROUP
  ADD prepaid_rent_account_id NUMBER(22);

COMMENT ON COLUMN LS_ILR_GROUP.prepaid_rent_account_id IS 'The prepaid rent account.';

ALTER TABLE LS_ILR_GROUP
  ADD CONSTRAINT fk_ls_ilr_group_prepaid FOREIGN KEY ( prepaid_rent_account_id ) REFERENCES GL_ACCOUNT ( gl_account_id );

ALTER TABLE LS_IMPORT_ILR
  ADD prepaid_rent_account_xlate VARCHAR2(254);

ALTER TABLE LS_IMPORT_ILR
  ADD prepaid_rent_account_id NUMBER(22);

COMMENT ON COLUMN LS_IMPORT_ILR.prepaid_rent_account_xlate IS 'Translation field for determining prepaid rent account.';

COMMENT ON COLUMN LS_IMPORT_ILR.prepaid_rent_account_id IS 'The short term prepaid rent account.';

ALTER TABLE LS_IMPORT_ILR_ARCHIVE
  ADD prepaid_rent_account_xlate VARCHAR2(254);

ALTER TABLE LS_IMPORT_ILR_ARCHIVE
  ADD prepaid_rent_account_id NUMBER(22);

COMMENT ON COLUMN LS_IMPORT_ILR_ARCHIVE.prepaid_rent_account_xlate IS 'Translation field for determining prepaid rent account.';

COMMENT ON COLUMN LS_IMPORT_ILR_ARCHIVE.prepaid_rent_account_id IS 'The short term prepaid rent account.';

--Comments for columns added in other PES branch tickets which are missing comments
COMMENT ON COLUMN LS_ASSET_SCHEDULE.beg_liability IS 'The beginning liability for the period.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.end_liability IS 'The ending liability for the period.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.beg_lt_liability IS 'The beginning long term liability for the period.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.end_lt_liability IS 'The ending long term liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.beg_liability IS 'The beginning liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.end_liability IS 'The ending liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.beg_lt_liability IS 'The beginning long term liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.end_lt_liability IS 'The ending long term liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_liability IS 'The beginning liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_liability IS 'The ending liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_lt_liability IS 'The beginning long term liability for the period.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_lt_liability IS 'The ending long term liability for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.beg_liability IS 'The beginning liability for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.end_liability IS 'The ending liability for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.beg_lt_liability IS 'The beginning long term liability for the period.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.end_lt_liability IS 'The ending long term liability for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.beg_liability IS 'The beginning liability for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.end_liability IS 'The ending liability for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.beg_lt_liability IS 'The beginning long term liability for the period.';

COMMENT ON COLUMN LS_SUMMARY_FORECAST.end_lt_liability IS 'The ending long term liability for the period.';

COMMENT ON COLUMN LS_ILR_OPTIONS.remeasurement_date IS 'The remeasurement date of the ILR/revision.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(4297, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_01_add_prepay_amortization_columns_ddl.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
