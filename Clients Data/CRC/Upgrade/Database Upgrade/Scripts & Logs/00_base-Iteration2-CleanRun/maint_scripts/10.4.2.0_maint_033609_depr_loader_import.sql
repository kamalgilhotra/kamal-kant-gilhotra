/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033609_depr_loader_import.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/24/2013 Ryan Oliveria
||============================================================================
*/

--We need a table specification for set_of_books_id, because the import tool adds another table to this SQL
update PP_IMPORT_LOOKUP
   set LOOKUP_SQL = '( select sob.set_of_books_id from set_of_books sob where upper( trim( <importfield> ) ) = upper( trim( sob.description ) ) )'
 where COLUMN_NAME = 'set_of_books_id'
   and LOOKUP_SQL = '( select set_of_books_id from set_of_books sob where upper( trim( <importfield> ) ) = upper( trim( sob.description ) ) )';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (706, 0, 10, 4, 2, 0, 33609, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033609_depr_loader_import.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;