/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044286_pwrtax_add_column_to_table_maint_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/06/2015 Daniel Motter  Creation
||============================================================================
*/

delete from powerplant_columns 
where column_name = 'k1_export_gen_include' 
and table_name = 'tax_reconcile_item';

insert into powerplant_columns
(column_name, table_name, dropdown_name, pp_edit_type_id, description, column_rank, read_only)
values
('k1_export_gen_include', 'tax_reconcile_item', 'yes_no', 'p', 'K1 Export Gen Include', 10, 0);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2677, 0, 2015, 2, 0, 0, 044286, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044286_pwrtax_add_column_to_table_maint_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;