/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050220_lessee_04_sch_indices_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.2.0.0 01/24/2018 Shane "C" Ward 	Lesseee Calculation Performance enhancements
||============================================================================
*/

CREATE INDEX ls_ilr_asset_schedule_stg_idx
  ON ls_ilr_asset_schedule_stg(ilr_id, ls_asset_id, revision, set_of_books_id, month);

CREATE INDEX ls_ilr_asset_sch_calc_stg_idx
  ON ls_ilr_asset_schedule_calc_stg(ilr_id, revision, set_of_books_id, ls_asset_id, month);

CREATE INDEX pp_system_ctl_company_ltn_idx
  ON pp_system_control_company(Trim( Lower( control_name ) ))
TABLESPACE pwrplant_idx;

CREATE INDEX ls_ilr_schedule_mth_idx
  ON ls_ilr_schedule(month)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_lease_id_num_idx
  ON ls_lease(lease_id, lease_number)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_ilr_ilr_ls_cmp_ilnum_idx
  ON ls_ilr(ilr_id, lease_id, company_id, ilr_number)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_asset_many_idx
  ON ls_asset(ls_asset_id, ilr_id, asset_location_id, utility_account_id, ls_asset_status_id, retirement_date, leased_asset_number)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_asset_many2_idx
  ON ls_asset(ls_asset_id, ls_asset_status_id, ilr_id, approved_revision, company_id)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_asset_status_retdate_idx
  ON ls_asset(ls_asset_status_id, retirement_date)
TABLESPACE pwrplant_idx;

CREATE INDEX pp_any_reqfiltr_uname_val_idx
  ON pp_any_required_filter(Upper( column_name ), filter_value);

CREATE INDEX ls_depr_info_tmp_idx
  ON ls_depr_info_tmp(ilr_id, revision, fasb_cap_type_id, mid_period_method);

CREATE INDEX ls_calc_sch_ndx
  ON ls_ilr_asset_schedule_calc_stg(ls_asset_id, revision, id, month);
  
DROP INDEX ls_ilr_asset_sch_calc_stg_idx;

CREATE INDEX ls_ilr_asset_sch_calc_stg_idx
  ON ls_ilr_asset_schedule_calc_stg(ilr_id, revision, set_of_books_id, ls_asset_id, month);

CREATE INDEX ls_ilr_payment_term_idx
  ON ls_ilr_payment_term(payment_term_type_id, ilr_id, revision)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_ilr_pay_term_ilr_rev_dt_idx
  ON ls_ilr_payment_term(ilr_id, revision, payment_term_date)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_asset_cmpy_ilr_ast_idx
  ON ls_asset(company_id, ilr_id, ls_asset_id)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_asset_many3_idx
  ON ls_asset(contract_currency_id, company_id, ilr_id, ls_asset_id, leased_asset_number, ls_asset_status_id, retirement_date)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_asset_schedule_many_idx
  ON ls_asset_schedule(ls_asset_id, revision, month, set_of_books_id, Trunc( month, 'MONTH' ))
TABLESPACE pwrplant_idx;

CREATE INDEX company_setup_id_descrip_idx
  ON company_setup(company_id, description)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_cap_type_id_descrip_idx
  ON ls_lease_cap_type(ls_lease_cap_type_id, description)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_ilr_id_rev_num_idx
  ON ls_ilr(ilr_id, current_revision, ilr_number)
TABLESPACE pwrplant_idx;

CREATE INDEX set_of_books_id_descrip_idx
  ON set_of_books(set_of_books_id, description)
TABLESPACE pwrplant_idx;

CREATE INDEX currency_id_symbol_label_idx
  ON currency(currency_id, iso_code, iso_code
|| ' '
|| currency_display_symbol)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_ilr_options_many_idx
  ON ls_ilr_options(ilr_id, revision, lease_cap_type_id, in_service_exchange_rate)
TABLESPACE pwrplant_idx;

CREATE INDEX currency_rate_dflt_dense2_idx
  ON currency_rate_default_dense(exchange_rate_type_id)
TABLESPACE pwrplant_idx;

CREATE INDEX ls_rent_bucket_admin_many_idx
  ON ls_rent_bucket_admin(rent_type, bucket_number, bucket_name)
TABLESPACE pwrplant_idx; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4092, 0, 2017, 2, 0, 0, 50220, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050220_lessee_04_sch_indices_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	