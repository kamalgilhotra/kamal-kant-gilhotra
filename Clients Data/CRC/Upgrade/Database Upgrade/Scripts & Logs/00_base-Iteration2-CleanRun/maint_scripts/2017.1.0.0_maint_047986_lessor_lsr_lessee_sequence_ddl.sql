/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047986_lessor_lsr_lessee_sequence_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- --------------    ------------------------------------
|| 2017.1.0.0 08/29/2017 Johnny Sisouphanh Create sequence for lsr_lessee
||============================================================================
*/

create sequence LSR_LESSEE_SEQ
   minvalue 1
   start with 1
   increment by 1
   cache 20;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3687, 0, 2017, 1, 0, 0, 47986, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047986_lessor_lsr_lessee_sequence_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;