/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045645_lease_remove_cap_type_column_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2015.2.2.0  04/19/2016 David Haupt    Removing FASB Cap Type ID from powerplant_columns
||============================================================================
*/

delete from powerplant_columns where table_name = 'ls_lease_cap_type' and column_name = 'fasb_cap_type_id';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3162, 0, 2015, 2, 2, 0, 45645, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045645_lease_remove_cap_type_column_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;