SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039866_depr_add_stg_column.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/04/2014 Charlie Shilling maint-39866
||============================================================================
*/

-- This is a PLSQL Script with functions to add and drop columns from tables in the DB
declare
   procedure ADD_COLUMN(V_TABLE_NAME varchar2,
                        V_COL_NAME   varchar2,
                        V_DATATYPE   varchar2,
                        V_COMMENT    varchar2) is
   begin
      begin
         execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
                           V_DATATYPE;
         DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
                              V_TABLE_NAME || '.');
      exception
         when others then
            if sqlcode = -1430 then
               --1430 is "column being added already exists in table", so we are good here
               DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
                                    V_TABLE_NAME || '. No action necessasry.');
            else
               RAISE_APPLICATION_ERROR(-20001,
                                       'Could not add column ' || V_COL_NAME || ' to ' ||
                                       V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
            end if;
      end;

      begin
         execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
                           V_COMMENT || '''';
         DBMS_OUTPUT.PUT_LINE('  Sucessfully added the comment to column ' || V_COL_NAME ||
                              ' to table ' || V_TABLE_NAME || '.');
      exception
         when others then
            RAISE_APPLICATION_ERROR(-20002,
                                    'Could not add comment to column ' || V_COL_NAME || ' to ' ||
                                    V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
      end;
   end ADD_COLUMN;
begin
   --add spread_factor_id
   ADD_COLUMN('depr_calc_stg',
              'est_in_zero_check',
              'varchar2(254)',
              'Value of the "Estimate in Zero Check" system control.');
   ADD_COLUMN('depr_calc_stg_arc',
              'est_in_zero_check',
              'varchar2(254)',
              'Value of the "Estimate in Zero Check" system control.');
   ADD_COLUMN('fcst_depr_calc_stg_arc',
              'est_in_zero_check',
              'varchar2(254)',
              'Value of the "Estimate in Zero Check" system control.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1377, 0, 10, 4, 2, 4, 39866, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.4_maint_039866_depr_add_stg_column.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;