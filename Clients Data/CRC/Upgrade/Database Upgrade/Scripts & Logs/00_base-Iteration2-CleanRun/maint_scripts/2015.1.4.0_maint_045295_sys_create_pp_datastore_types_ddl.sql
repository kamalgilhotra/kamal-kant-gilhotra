/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045295_sys_create_pp_datastore_types_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.4.0 01/13/2016 Charlie Shilling move type creation to database level due to changes in Oracle 12c. 
||============================================================================
*/

CREATE OR REPLACE type t_bind_vars is table of varchar2(2000);
/

CREATE OR REPLACE type t_table_vars is table of T_BIND_VARS;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3020, 0, 2015, 1, 4, 0, 045295, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.4.0_maint_045295_sys_create_pp_datastore_types_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;