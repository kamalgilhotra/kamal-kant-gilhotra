/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_052170_lessee_01_fix_subsystem_ilr_approval_query_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.4.0.1  08/10/2018 C. Yura        Fix subsystem value and add set of books column
||============================================================================
*/
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_id NUMBER;
  l_cnt number;
  l_sql1 VARCHAR2(4000);
BEGIN
  l_query_description := 'Lease ILR''s Pending Approval';
  l_query_sql := '  select
	pt.ilr_id as ilr_id,
	pt.ilr_number as ilr_number,
	c.description as company,
	pt.revision,
  u.last_name || '', '' || u.first_name as sending_user,
	aps.description as approval_status,
	sob.description as set_of_books,
  amounts.current_lease_cost as current_lease_cost,
	amounts.CAPITAL_COST as Capital_cost,
	decode(current_lease_cost, 0, 0, round(100 * amounts.Capital_cost/amounts.current_lease_cost,2)) as capital_percent
from workflow wf, company c, approval_status aps,
	(select distinct p.ilr_id, p.revision, i.ilr_number, i.company_id, p.error_message
	from ls_pend_transaction p, ls_ilr i where i.ilr_id = p.ilr_id) pt, ls_ilr_amounts_set_of_books amounts, set_of_books sob, pp_security_users u
where wf.subsystem = ''lessee_ilr_approval''
and wf.id_field1 = to_char(pt.ilr_id)
and wf.id_field2 = to_char(pt.revision)
and c.company_id = pt.company_id
and wf.approval_status_id not in (1,3,6)
and amounts.ilr_id = pt.ilr_id
and amounts.revision = pt.revision
and amounts.set_of_books_id = sob.set_of_books_id
and wf.approval_status_id = aps.approval_status_id
and lower(trim(user_sent)) = lower(trim(u.users))          ';
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

	l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
    l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);

	update pp_any_query_criteria set SQL = l_sql1 where id = l_id;

	update pp_any_query_criteria_fields 
	set column_order = 9 
	where id = l_id 
	and lower(detail_field) = 'capital_cost';

	update pp_any_query_criteria_fields 
	set column_order = 10
	where id = l_id 
	and lower(detail_field) = 'capital_percent';

	update pp_any_query_criteria_fields 
	set column_order = 8
	where id = l_id 
	and lower(detail_field) = 'current_lease_cost';

	INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
	SELECT  l_id,
			'set_of_books',
			7,
			0,
			1,
			'',
			'Set Of Books',
			300,
			'',
			'',
			'VARCHAR2',
			0,
			'',
			null,
			null,
			'',
			0,
			0
	FROM dual where not exists ( select 1 from pp_any_query_criteria_fields where id = l_id and lower(detail_field) = 'set_of_books') ;

END IF;
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9122, 0, 2017, 4, 0, 1, 52170, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052170_lessee_01_fix_subsystem_ilr_approval_query_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;