/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030171_projects.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.1   06/10/2013 Chris Mardis   Patch Release
||============================================================================
*/

update WO_VALIDATION_TYPE set HARD_EDIT = 1 where WO_VALIDATION_TYPE_ID = 6;

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, function, TIME_STAMP, USER_ID,
    FIND_COMPANY, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10, COL11, COL12, COL13,
    COL14, COL15, COL16, COL17, COL18, COL19, COL20, HARD_EDIT)
   select WO_VALIDATION_TYPE_ID + 60,
          replace(DESCRIPTION, 'Detail Update', 'Detail Init Update'),
          replace(LONG_DESCRIPTION, 'Detail Update', 'Detail Init Update'),
          'w_wo_detail_init',
          TIME_STAMP,
          USER_ID,
          FIND_COMPANY,
          COL1,
          COL2,
          COL3,
          COL4,
          COL5,
          COL6,
          COL7,
          COL8,
          COL9,
          COL10,
          COL11,
          COL12,
          COL13,
          COL14,
          COL15,
          COL16,
          COL17,
          COL18,
          COL19,
          COL20,
          HARD_EDIT
     from WO_VALIDATION_TYPE
    where WO_VALIDATION_TYPE_ID = 6;

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, function, TIME_STAMP, USER_ID,
    FIND_COMPANY, COL1, COL2, COL3, COL4, COL5, COL6, COL7, COL8, COL9, COL10, COL11, COL12, COL13,
    COL14, COL15, COL16, COL17, COL18, COL19, COL20, HARD_EDIT)
   select WO_VALIDATION_TYPE_ID + 60,
          replace(DESCRIPTION, 'Detail Open', 'Detail Init Open'),
          replace(LONG_DESCRIPTION, 'Detail Open', 'Detail Init Open'),
          'w_wo_detail_init',
          TIME_STAMP,
          USER_ID,
          FIND_COMPANY,
          COL1,
          COL2,
          COL3,
          COL4,
          COL5,
          COL6,
          COL7,
          COL8,
          COL9,
          COL10,
          COL11,
          COL12,
          COL13,
          COL14,
          COL15,
          COL16,
          COL17,
          COL18,
          COL19,
          COL20,
          HARD_EDIT
     from WO_VALIDATION_TYPE
    where WO_VALIDATION_TYPE_ID = 7;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (396, 0, 10, 4, 0, 1, 30171, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_030171_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));

commit;