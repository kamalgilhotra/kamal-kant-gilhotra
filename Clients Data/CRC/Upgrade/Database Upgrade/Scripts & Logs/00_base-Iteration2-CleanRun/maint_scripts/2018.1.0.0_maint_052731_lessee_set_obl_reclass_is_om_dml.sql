/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052731_lessee_set_obl_reclass_is_om_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/20/2018 B Morrison      PP-52731 obligation_reclass should be 0 for off-balance sheet lease (is_om = 1)
||============================================================================
*/

UPDATE ls_asset_schedule las
SET  obligation_reclass = 0
where is_om = 1
AND obligation_reclass  <> 0
;

UPDATE ls_ilr_schedule lis
SET obligation_reclass = 0
where is_om = 1
AND obligation_reclass  <> 0
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12242, 0, 2018, 1, 0, 0, 52731, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052731_lessee_set_obl_reclass_is_om_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;