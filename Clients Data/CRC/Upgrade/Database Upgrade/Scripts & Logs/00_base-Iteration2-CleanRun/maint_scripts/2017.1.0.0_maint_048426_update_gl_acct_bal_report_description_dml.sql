/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048426_update_gl_acct_bal_report_description_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/20/2017 Josh Sandler     Update Lessee - 1009 report long description to include "(Company Currency)"
||============================================================================
*/

UPDATE pp_reports
SET long_description = 'GL Account Balances (Company Currency)'
WHERE report_number = 'Lessee - 1009';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3598, 0, 2017, 1, 0, 0, 48426, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048426_update_gl_acct_bal_report_description_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;