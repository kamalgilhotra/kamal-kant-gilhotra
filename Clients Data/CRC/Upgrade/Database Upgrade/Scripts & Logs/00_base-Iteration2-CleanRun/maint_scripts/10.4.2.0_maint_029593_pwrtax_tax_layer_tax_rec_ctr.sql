/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029593_pwrtax_tax_layer_tax_rec_ctr.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014 Ron Ferentini
||============================================================================
*/

alter table TAX_RECORD_CONTROL add TAX_LAYER_ID number(22, 0);

comment on column TAX_RECORD_CONTROL.TAX_LAYER_ID is 'System-assigned key that identifies an individual tax layer in the tax layer table.';

alter table TAX_RECORD_CONTROL add ASSET_ID number(22, 0);

comment on column TAX_RECORD_CONTROL.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.';

create table TAX_LAYER
(
 TAX_LAYER_ID number(22) not null,
 TIME_STAMP   date,
 USER_ID      varchar2(18),
 DESCRIPTION  varchar2(35) not null
);

alter table TAX_LAYER
   add constraint TAX_LAYER_PK
       primary key (TAX_LAYER_ID)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_LAYER is 'The table holds by Tax Layers.';
comment on column TAX_LAYER.TAX_LAYER_ID is 'System-assigned identifier for each Tax Layer.';
comment on column TAX_LAYER.DESCRIPTION is 'Records a short descriptive identifier for each Tax Layer.';

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
values
   ('Individual Asset Depreciation', sysdate, user,
    'Whether or not the client is using individual asset depreciation.', 1, 'No', null, 1);
commit;

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID)
values
   ('Individual Asset Depreciation', 'Yes', sysdate, user);
commit;

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID)
values
   ('Individual Asset Depreciation', 'No', sysdate, user);
commit;

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID)
values
   ('Individual Asset Depreciation', 'powertax', sysdate, user);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (907, 0, 10, 4, 2, 0, 29593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029593_pwrtax_tax_layer_tax_rec_ctr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;