/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_052031_lessee_01_lessor_zip_postal_vch2_import_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.4.0.1  7/19/2018 build script   allow non numeric and extend zip/postal
||============================================================================
*/

--This was supposed to be done on PP-49815
alter table LS_IMPORT_LESSOR add POSTAL_OLD varchar2(4);
update LS_IMPORT_LESSOR set POSTAL_OLD = POSTAL;
update LS_IMPORT_LESSOR set POSTAL = NULL;
alter table LS_IMPORT_LESSOR modify "POSTAL" varchar2(10);
update LS_IMPORT_LESSOR set POSTAL = POSTAL_OLD;
alter table LS_IMPORT_LESSOR drop column POSTAL_OLD;

alter table LS_IMPORT_LESSOR add "ZIP_OLD" varchar2(5);
update LS_IMPORT_LESSOR set ZIP_OLD = ZIP;
update LS_IMPORT_LESSOR set ZIP = NULL;
alter table LS_IMPORT_LESSOR modify "ZIP" varchar2(10);
update LS_IMPORT_LESSOR set ZIP = ZIP_OLD;
alter table LS_IMPORT_LESSOR drop column ZIP_OLD;

alter table LS_IMPORT_LESSOR_ARCHIVE add POSTAL_OLD varchar2(4);
update LS_IMPORT_LESSOR_ARCHIVE set POSTAL_OLD = POSTAL;
update LS_IMPORT_LESSOR_ARCHIVE set POSTAL = NULL;
alter table LS_IMPORT_LESSOR_ARCHIVE modify "POSTAL" varchar2(10);
update LS_IMPORT_LESSOR_ARCHIVE set POSTAL = POSTAL_OLD;
alter table LS_IMPORT_LESSOR_ARCHIVE drop column POSTAL_OLD;

alter table LS_IMPORT_LESSOR_ARCHIVE add "ZIP_OLD" varchar2(5);
update LS_IMPORT_LESSOR_ARCHIVE set ZIP_OLD = ZIP;
update LS_IMPORT_LESSOR_ARCHIVE set ZIP = NULL;
alter table LS_IMPORT_LESSOR_ARCHIVE modify "ZIP" varchar2(10);
update LS_IMPORT_LESSOR_ARCHIVE set ZIP = ZIP_OLD;
alter table LS_IMPORT_LESSOR_ARCHIVE drop column ZIP_OLD;

--Repeat PP-49357 and PP-49815 for Lessor module equivalent tables
alter table LSR_LESSEE add POSTAL_OLD varchar2(4);
update LSR_LESSEE set POSTAL_OLD = POSTAL;
update LSR_LESSEE set POSTAL = NULL;
alter table LSR_LESSEE modify "POSTAL" varchar2(10);
update LSR_LESSEE set POSTAL = POSTAL_OLD;
alter table LSR_LESSEE drop column POSTAL_OLD;

alter table LSR_LESSEE add "ZIP_OLD" varchar2(5);
update LSR_LESSEE set ZIP_OLD = ZIP;
update LSR_LESSEE set ZIP = NULL;
alter table LSR_LESSEE modify "ZIP" varchar2(10);
update LSR_LESSEE set ZIP = ZIP_OLD;
alter table LSR_LESSEE drop column ZIP_OLD;


alter table LSR_IMPORT_LESSEE add POSTAL_OLD varchar2(4);
update LSR_IMPORT_LESSEE set POSTAL_OLD = POSTAL;
update LSR_IMPORT_LESSEE set POSTAL = NULL;
alter table LSR_IMPORT_LESSEE modify "POSTAL" varchar2(10);
update LSR_IMPORT_LESSEE set POSTAL = POSTAL_OLD;
alter table LSR_IMPORT_LESSEE drop column POSTAL_OLD;

alter table LSR_IMPORT_LESSEE add "ZIP_OLD" varchar2(5);
update LSR_IMPORT_LESSEE set ZIP_OLD = ZIP;
update LSR_IMPORT_LESSEE set ZIP = NULL;
alter table LSR_IMPORT_LESSEE modify "ZIP" varchar2(10);
update LSR_IMPORT_LESSEE set ZIP = ZIP_OLD;
alter table LSR_IMPORT_LESSEE drop column ZIP_OLD;

alter table LSR_IMPORT_LESSEE_ARCHIVE add POSTAL_OLD varchar2(4);
update LSR_IMPORT_LESSEE_ARCHIVE set POSTAL_OLD = POSTAL;
update LSR_IMPORT_LESSEE_ARCHIVE set POSTAL = NULL;
alter table LSR_IMPORT_LESSEE_ARCHIVE modify "POSTAL" varchar2(10);
update LSR_IMPORT_LESSEE_ARCHIVE set POSTAL = POSTAL_OLD;
alter table LSR_IMPORT_LESSEE_ARCHIVE drop column POSTAL_OLD;

alter table LSR_IMPORT_LESSEE_ARCHIVE add "ZIP_OLD" varchar2(5);
update LSR_IMPORT_LESSEE_ARCHIVE set ZIP_OLD = ZIP;
update LSR_IMPORT_LESSEE_ARCHIVE set ZIP = NULL;
alter table LSR_IMPORT_LESSEE_ARCHIVE modify "ZIP" varchar2(10);
update LSR_IMPORT_LESSEE_ARCHIVE set ZIP = ZIP_OLD;
alter table LSR_IMPORT_LESSEE_ARCHIVE drop column ZIP_OLD;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8944, 0, 2017, 4, 0, 1, 52031, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052031_lessee_01_lessor_zip_postal_vch2_import_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		   