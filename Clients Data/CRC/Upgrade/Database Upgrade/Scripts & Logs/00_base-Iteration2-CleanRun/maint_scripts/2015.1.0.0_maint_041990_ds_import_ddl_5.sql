/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041990_ds_import_ddl_5.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   01/21/2015 A Scott          Import tool setup for depr studies.
||                                      Changes made based on QA, including
||                                      depr group validations, acct backfills.
||============================================================================
*/

--alter table ds_import_depr_group drop column default_gl_acct_id;
--alter table ds_import_depr_group_arc drop column default_gl_acct_id;

alter table ds_import_depr_group
add 
(
  default_gl_acct_id        NUMBER(22,0)
);

comment on column ds_import_depr_group.default_gl_acct_id is 'Default gl account id used in required accounting fields as depr groups are created for DS Stand Alone clients.';


alter table ds_import_depr_group_arc
add 
(
  default_gl_acct_id        NUMBER(22,0)
);

comment on column ds_import_depr_group_arc.default_gl_acct_id is 'Default gl account id used in required accounting fields as depr groups are created for DS Stand Alone clients.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2205, 0, 2015, 1, 0, 0, 41990, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041990_ds_import_ddl_5.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;