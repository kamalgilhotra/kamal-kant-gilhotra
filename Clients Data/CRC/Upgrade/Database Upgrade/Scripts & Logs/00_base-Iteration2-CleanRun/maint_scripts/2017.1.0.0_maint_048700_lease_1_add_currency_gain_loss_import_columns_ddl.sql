/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048700_lease_1_add_currency_gain_loss_import_columns_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/04/2017 Josh Sandler     Add columns for currency gain/loss and currency gain/loss offset accounts for import tool
||============================================================================
*/

ALTER TABLE ls_import_ilr
  ADD (curr_gain_loss_dr_acct_xlate VARCHAR2(254) NULL,
      currency_gain_loss_dr_acct_id NUMBER(22) NULL,
      curr_gain_loss_cr_acct_xlate VARCHAR2(254) NULL,
      currency_gain_loss_cr_acct_id NUMBER(22) NULL)
;

COMMENT ON COLUMN ls_import_ilr.curr_gain_loss_dr_acct_xlate IS 'Translation field for determining currency gain/loss account.';
COMMENT ON COLUMN ls_import_ilr.currency_gain_loss_dr_acct_id IS 'The currency gain/loss account.';
COMMENT ON COLUMN ls_import_ilr.curr_gain_loss_cr_acct_xlate IS 'Translation field for determining currency gain/loss offset account.';
COMMENT ON COLUMN ls_import_ilr.currency_gain_loss_cr_acct_id IS 'The currency gain/loss offset account.';

ALTER TABLE ls_import_ilr_archive
  ADD (curr_gain_loss_dr_acct_xlate VARCHAR2(254) NULL,
      currency_gain_loss_dr_acct_id NUMBER(22) NULL,
      curr_gain_loss_cr_acct_xlate VARCHAR2(254) NULL,
      currency_gain_loss_cr_acct_id NUMBER(22) NULL)
;

COMMENT ON COLUMN ls_import_ilr_archive.curr_gain_loss_dr_acct_xlate IS 'Translation field for determining currency gain/loss account.';
COMMENT ON COLUMN ls_import_ilr_archive.currency_gain_loss_dr_acct_id IS 'The currency gain/loss account.';
COMMENT ON COLUMN ls_import_ilr_archive.curr_gain_loss_cr_acct_xlate IS 'Translation field for determining currency gain/loss offset account.';
COMMENT ON COLUMN ls_import_ilr_archive.currency_gain_loss_cr_acct_id IS 'The currency gain/loss offset account.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3625, 0, 2017, 1, 0, 0, 48700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048700_lease_1_add_currency_gain_loss_import_columns_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;