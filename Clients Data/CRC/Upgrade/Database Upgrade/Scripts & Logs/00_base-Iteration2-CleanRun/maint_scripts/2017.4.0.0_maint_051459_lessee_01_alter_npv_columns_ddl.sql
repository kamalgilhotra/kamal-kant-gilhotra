/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051459_lessee_01_alter_npv_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/23/2018 Josh Sandler     Alter table data type
||============================================================================
*/

alter table LS_ILR_SCHEDULE_STG modify NPV NUMBER (32,12);
alter table LS_ILR_ASSET_SCHEDULE_STG modify NPV NUMBER(32,12);
alter table LS_ILR_ASSET_SCHEDULE_CALC_STG modify NPV NUMBER(32,12);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5945, 0, 2017, 4, 0, 0, 51459, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051459_lessee_01_alter_npv_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;