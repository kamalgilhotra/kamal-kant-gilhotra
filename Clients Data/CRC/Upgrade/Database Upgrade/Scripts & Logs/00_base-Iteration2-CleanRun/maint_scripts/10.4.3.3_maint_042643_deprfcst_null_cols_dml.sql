/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint-042643_deprfcst_null_cols_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.3.3    02/19/2015  Anand R          Update null columns 
||============================================================================
*/

-- update null values in salvage_expense_acct_id, salvage_reserve_acct_id and true_up_cpr_depr
-- in fcst_depr_group table from depr_group

UPDATE fcst_depr_group c
SET salvage_expense_acct_id = (
		SELECT min(a.salvage_expense_acct_id)
		FROM depr_group a,
			fcst_depr_group_xlat b
		WHERE a.depr_group_id = b.depr_group_id
			AND b.fcst_depr_group_id = c.fcst_depr_group_id
		)
WHERE EXISTS (
		SELECT 1
		FROM depr_group a,
			fcst_depr_group_xlat b
		WHERE a.depr_group_id = b.depr_group_id
			AND b.fcst_depr_group_id = c.fcst_depr_group_id
		)
	AND salvage_expense_acct_id IS NULL;

UPDATE fcst_depr_group c
SET salvage_reserve_acct_id = (
		SELECT min(a.salvage_reserve_acct_id)
		FROM depr_group a,
			fcst_depr_group_xlat b
		WHERE a.depr_group_id = b.depr_group_id
			AND b.fcst_depr_group_id = c.fcst_depr_group_id
		)
WHERE EXISTS (
		SELECT 1
		FROM depr_group a,
			fcst_depr_group_xlat b
		WHERE a.depr_group_id = b.depr_group_id
			AND b.fcst_depr_group_id = c.fcst_depr_group_id
		)
	AND salvage_reserve_acct_id IS NULL;

UPDATE fcst_depr_group c
SET true_up_cpr_depr = (
		SELECT min(a.true_up_cpr_depr)
		FROM depr_group a,
			fcst_depr_group_xlat b
		WHERE a.depr_group_id = b.depr_group_id
			AND b.fcst_depr_group_id = c.fcst_depr_group_id
		)
WHERE EXISTS (
		SELECT 1
		FROM depr_group a,
			fcst_depr_group_xlat b
		WHERE a.depr_group_id = b.depr_group_id
			AND b.fcst_depr_group_id = c.fcst_depr_group_id
		)
	AND true_up_cpr_depr IS NULL;

-- update null values in salvage_expense_acct_id, salvage_reserve_acct_id and true_up_cpr_depr 
-- in fcst_depr_group_version table from fcst_depr_group 

UPDATE fcst_depr_group_version a
SET salvage_expense_acct_id = (
		SELECT b.salvage_expense_acct_id
		FROM fcst_depr_group b
		WHERE a.fcst_depr_group_id = b.fcst_depr_group_id
		)
WHERE a.salvage_expense_acct_id IS NULL;

UPDATE fcst_depr_group_version a
SET salvage_reserve_acct_id = (
		SELECT b.salvage_reserve_acct_id
		FROM fcst_depr_group b
		WHERE a.fcst_depr_group_id = b.fcst_depr_group_id
		)
WHERE a.salvage_reserve_acct_id IS NULL;

UPDATE fcst_depr_group_version a
SET true_up_cpr_depr = (
		SELECT b.true_up_cpr_depr
		FROM fcst_depr_group b
		WHERE a.fcst_depr_group_id = b.fcst_depr_group_id
		)
WHERE a.true_up_cpr_depr IS NULL;

COMMIT;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2315, 0, 10, 4, 3, 3, 42643, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.3_maint_042643_deprfcst_null_cols_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;