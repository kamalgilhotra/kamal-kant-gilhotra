/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_051514_lessor_any_query_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 05/08/2019 B. Beck     	Add new fields to the Lessor ILR Schedule Any Query
||============================================================================
*/

update pp_any_query_criteria
set "SQL" = 
'SELECT lct.description currency_type, c.description company, ls.lease_number lease_number, ilr.ilr_number, st.description ' ||
'  ilr_status, lg.description lease_group, ig.description ilr_group, lse.description lessee, ct.description capitalization_type, ' ||
'  sob.description set_of_books, To_Char(schedule.MONTH, ''yyyymm'') month_number, schedule.iso_code currency, ' ||
'  Nvl(schedule.principal_received,0) principal_received, schedule.interest_income_received, Nvl(schedule.principal_accrued,0) principal_accrued, ' ||
'  schedule.interest_income_accrued, schedule.deferred_rent, schedule.accrued_rent, schedule.receivable_remeasurement, schedule.unguaran_residual_remeasure, ' ||
'  schedule.beg_receivable, schedule.end_receivable, schedule.beg_long_term_receivable, schedule.end_long_term_receivable, ' ||
'  schedule.lt_receivable_remeasurement, nvl(schedule.beginning_remaining_payments, 0) as beginning_remaining_payments, ' ||
'  nvl(schedule.ending_remaining_payments, 0) as ending_remaining_payments, schedule.initial_direct_cost, schedule.beg_unguaranteed_residual, ' || 
'  schedule.interest_unguaranteed_residual, schedule.ending_unguaranteed_residual, schedule.beg_net_investment, ' || 
'  schedule.interest_net_investment, schedule.ending_net_investment, nvl(schedule.net_investment_remeasurement, 0) as net_investment_remeasurement, ' ||
'  schedule.beginning_deferred_profit, schedule.recognized_profit, schedule.ending_deferred_profit, nvl(schedule.deferred_profit_remeasurement, 0) as deferred_profit_remeasurement, ' ||
'  schedule.beginning_lt_deferred_profit, schedule.ending_lt_deferred_profit, nvl(schedule.lt_deferred_profit_remeasure, 0) lt_deferred_profit_remeasure, ' || 
'  schedule.contingent_paid1+schedule.contingent_paid2+schedule.contingent_paid3+schedule.contingent_paid4+schedule.contingent_paid5+ ' ||
'  schedule.contingent_paid6+schedule.contingent_paid7+schedule.contingent_paid8+schedule.contingent_paid9+schedule.contingent_paid10 contingent_received, ' || 
'  schedule.executory_paid1+schedule.executory_paid2+schedule.executory_paid3+schedule.executory_paid4+schedule.executory_paid5+ ' ||
'  schedule.executory_paid6+schedule.executory_paid7+schedule.executory_paid8+schedule.executory_paid9+schedule.executory_paid10 executory_received, ' || 
'  currency_display_symbol AS currency_symbol ' || 
'FROM v_lsr_ilr_mc_schedule schedule ' || 
'JOIN company c ON (schedule.company_id = c.company_id) JOIN lsr_ilr ilr ON (schedule.ilr_id = ilr.ilr_id AND schedule.revision = ilr.current_revision) ' || 
'JOIN ls_lease_currency_type lct ON (schedule.ls_cur_type = lct.ls_currency_type_id) ' || 
'JOIN lsr_lease ls ON (ilr.lease_id = ls.lease_id) ' || 
'JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id) ' || 
'JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id) ' || 
'JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id) ' || 
'JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision) ' || 
'JOIN lsr_cap_type ct ON (o.lease_cap_type_id = ct.cap_type_id) ' || 
'JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id) ' || 
'JOIN set_of_books sob ON (schedule.set_of_books_id = sob.set_of_books_id) ' || 
'ORDER BY ilr_number, currency_type, set_of_books, MONTH '
where description = 'Lessor Schedule by ILR'
;

update pp_any_query_criteria_fields set column_order = 1 where detail_field = 'currency_type'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 2 where detail_field = 'company'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 3 where detail_field = 'lease_number'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 4 where detail_field = 'ilr_number'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 5 where detail_field = 'ilr_status'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 6 where detail_field = 'lease_group'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 7 where detail_field = 'ilr_group'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 8 where detail_field = 'lessee'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 9 where detail_field = 'capitalization_type'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 10 where detail_field = 'set_of_books'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 11 where detail_field = 'month_number'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 12 where detail_field = 'currency'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 13 where detail_field = 'principal_received'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 14 where detail_field = 'interest_income_received'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 15 where detail_field = 'principal_accrued'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 16 where detail_field = 'interest_income_accrued'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 17 where detail_field = 'deferred_rent'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 18 where detail_field = 'accrued_rent'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 19 where detail_field = 'beg_receivable'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 20 where detail_field = 'end_receivable'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 21 where detail_field = 'receivable_remeasurement'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 22 where detail_field = 'beg_long_term_receivable'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 23 where detail_field = 'end_long_term_receivable'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 24 where detail_field = 'lt_receivable_remeasurement'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 27 where detail_field = 'initial_direct_cost'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 28 where detail_field = 'contingent_received'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 29 where detail_field = 'executory_received'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 30 where detail_field = 'beg_unguaranteed_residual'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 31 where detail_field = 'interest_unguaranteed_residual'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 32 where detail_field = 'ending_unguaranteed_residual'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 33 where detail_field = 'unguaran_residual_remeasure'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 34 where detail_field = 'beg_net_investment'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 35 where detail_field = 'interest_net_investment'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 36 where detail_field = 'ending_net_investment'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 38 where detail_field = 'beginning_deferred_profit'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 39 where detail_field = 'recognized_profit'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 40 where detail_field = 'ending_deferred_profit'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 42 where detail_field = 'beginning_lt_deferred_profit'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 43 where detail_field = 'ending_lt_deferred_profit'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 44 where detail_field = 'lt_deferred_profit_remeasure'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);

update pp_any_query_criteria_fields set column_order = 45 where detail_field = 'currency_symbol'
and id =
(
      select a.id
      from pp_any_query_criteria a
      where a.description = 'Lessor Schedule by ILR'
);


insert into pp_any_query_criteria_fields
(
	ID,	DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, 
	COLUMN_HEADER, COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS
)
select a.id, 'beginning_remaining_payments', 25, 1, 1,
	'Beginning Remaining Payments', 300, 'NUMBER', 0, 0, 0
from pp_any_query_criteria a
where a.description = 'Lessor Schedule by ILR'
and not exists
(
	select 1
	from pp_any_query_criteria_fields b
	where b.id = a.id
	and b.detail_field = 'beginning_remaining_payments'
);


insert into pp_any_query_criteria_fields
(
	ID,	DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, 
	COLUMN_HEADER, COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS
)
select a.id, 'ending_remaining_payments', 26, 1, 1,
	'Ending Remaining Payments', 300, 'NUMBER', 0, 0, 0
from pp_any_query_criteria a
where a.description = 'Lessor Schedule by ILR'
and not exists
(
	select 1
	from pp_any_query_criteria_fields b
	where b.id = a.id
	and b.detail_field = 'ending_remaining_payments'
);

insert into pp_any_query_criteria_fields
(
	ID,	DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, 
	COLUMN_HEADER, COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS
)
select a.id, 'net_investment_remeasurement', 37, 1, 1,
	'Net Investment Remeasurement', 300, 'NUMBER', 0, 0, 0
from pp_any_query_criteria a
where a.description = 'Lessor Schedule by ILR'
and not exists
(
	select 1
	from pp_any_query_criteria_fields b
	where b.id = a.id
	and b.detail_field = 'net_investment_remeasurement'
);


insert into pp_any_query_criteria_fields
(
	ID,	DETAIL_FIELD, COLUMN_ORDER, AMOUNT_FIELD, INCLUDE_IN_SELECT_CRITERIA, 
	COLUMN_HEADER, COLUMN_WIDTH, COLUMN_TYPE, QUANTITY_FIELD, HIDE_FROM_RESULTS, HIDE_FROM_FILTERS
)
select a.id, 'deferred_profit_remeasurement', 41, 1, 1,
	'Deferred Profit Remeasurement', 300, 'NUMBER', 0, 0, 0
from pp_any_query_criteria a
where a.description = 'Lessor Schedule by ILR'
and not exists
(
	select 1
	from pp_any_query_criteria_fields b
	where b.id = a.id
	and b.detail_field = 'deferred_profit_remeasurement'
);

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(17702, 0, 2018, 2, 1, 0, 51514, 'C:\BitBucketRepos\classic_pb\scripts\00_base\maint_scripts', '2018.2.1.0_maint_051514_lessor_any_query_dml.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;