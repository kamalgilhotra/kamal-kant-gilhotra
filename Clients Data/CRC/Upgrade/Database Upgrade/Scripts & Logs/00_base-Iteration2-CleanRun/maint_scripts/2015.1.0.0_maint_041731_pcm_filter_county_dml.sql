 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_041731_pcm_filter_county_dml.sql
|| Description: Fix County Filter
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	12/31/2014 Ryan Oliveria  New Module
||============================================================================
*/

update PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'asset_location.state_id||''|''||asset_location.county_id',
   	   DW_ID = 'state_id_county_id'
 where FILTER_ID = 257;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2136, 0, 2015, 1, 0, 0, 041731, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041731_pcm_filter_county_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;