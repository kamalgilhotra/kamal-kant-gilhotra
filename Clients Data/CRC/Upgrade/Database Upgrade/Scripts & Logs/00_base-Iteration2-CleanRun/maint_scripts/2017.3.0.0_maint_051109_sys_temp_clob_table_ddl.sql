/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051109_sys_temp_clob_table_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 05/08/2018 Lee Quinn   	  DBAdmin table for comparing PLSQL objects
||============================================================================
*/

declare
l_sql varchar2(2000);
l_count integer;
begin
  
  select count(1) into l_count from all_tables where lower(trim(table_name)) = 'temp_clob_table' ;
  IF l_count = 0 then 
	l_sql := '  create global temporary table TEMP_CLOB_TABLE ';
    l_sql := concat(l_sql,  '  (  ID       number(22), ');
	l_sql := concat(l_sql, + ' CLOB_COL clob  )  on commit preserve rows');

	execute immediate l_sql;
  
	l_sql := 'comment on table TEMP_CLOB_TABLE  is ''Global temporary table that is used by DBAdmin to compare database PLSQL code.''';
	execute immediate l_sql;

	l_sql := 'comment on column TEMP_CLOB_TABLE.ID is ''Unique identifier for all rows.''';
	execute immediate l_sql;
  
	l_sql := 'comment on column TEMP_CLOB_TABLE.CLOB_COL is ''Character large objects stored for PLSQL comparison.''';
	execute immediate l_sql;
  END IF;
  
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5242, 0, 2017, 3, 0, 0, 51109, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051109_sys_temp_clob_table_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;