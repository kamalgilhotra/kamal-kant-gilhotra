/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050698_lessor_01_create_reclass_gl_je_code_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/30/2018 Johnny Sisouphanh Create the new GL JE Code we need for Lessor Reclass
||============================================================================
*/

/* Add GL_JE_CODE for Lessor Reclass  */
insert into standard_journal_entries (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
values ( (select nvl((max(je_id) + 1),1) from standard_journal_entries), 'Lessor Reclass', 'Lessor Reclass', 'Lessor Reclass', 'Lessor Reclass');

insert into gl_je_control (PROCESS_ID, JE_ID, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN)
values ('Lessor Reclass', (select je_id from standard_journal_entries where gl_je_code = 'Lessor Reclass'), 'NONE', 'NONE', 'NONE', 'NONE');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4246, 0, 2017, 3, 0, 0, 50698, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050698_lessor_01_create_reclass_gl_je_code_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;