/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029774_lease_fixed_prin.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.1   06/04/2013 Travis Nemes   Point Release
||============================================================================
*/

CREATE TABLE LS_ILR_FIXED_PRIN_TERM
(
 ILR_ID             number(22,0) not null,
 PAYMENT_TERM_ID    number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 PAYMENT_TERM_DATE  date         not null,
 PAYMENT_FREQ_ID    number(22,0) not null,
 NUMBER_OF_TERMS    number(22,0) not null,
 INTEREST_RATE      number(22,8) default 0,
 PAID_AMOUNT        number(22,2) default 0,
 EST_EXECUTORY_COST number(22,2) default 0,
 CONTINGENT_AMOUNT  number(22,2) default 0
);

alter table LS_ILR_FIXED_PRIN_TERM
   add constraint PK_LS_ILR_FIXED_PRIN_TERM
       primary key (ILR_ID, PAYMENT_TERM_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ILR_FIXED_PRIN_TERM
   add constraint FK_ILR_FIXEDTERMS_ILR
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);

alter table LS_ILR_FIXED_PRIN_TERM
   add constraint R_LS_ILR_FIXEDTERMS_FREQ
       foreign key (PAYMENT_FREQ_ID)
       references LS_PAYMENT_FREQ (PAYMENT_FREQ_ID);

insert into LS_LEASE_TYPE
   (LEASE_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (2, sysdate, user, 'Variable', 'Variable');

alter table LS_API_ILR_TERMS add VAR_INT_RATE number(22,8);

alter table LS_ILR add LEASE_AIR_SW number(1,0);

alter table LS_API_ILR add LEASE_AIR_SW number(1,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (392, 0, 10, 4, 0, 1, 29774, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_029774_lease_fixed_prin.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
