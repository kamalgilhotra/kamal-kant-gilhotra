/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047531_lease_add_cur_type_report_arg_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/25/2017 Charlie Shilling Add arguments to correspond to the time option added in other script
||============================================================================
*/

--Create new report_arg for the time option being used
INSERT INTO pp_reports_args (report_id, dw_id, dw_single_multi, dw_retrieve, dw_sql_marker, table_name, column_name, arg_description, dw_name)
SELECT report_id, 1, 1, 1, '111=111', 'ls_ilr_schedule', 'ls_cur_type', 'Currency Type', 'dddw_ls_currency_type'
FROM pp_reports
WHERE datawindow = 'dw_ls_rpt_schedule_by_ilr';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3465, 0, 2017, 1, 0, 0, 47531, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047531_lease_add_cur_type_report_arg_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
