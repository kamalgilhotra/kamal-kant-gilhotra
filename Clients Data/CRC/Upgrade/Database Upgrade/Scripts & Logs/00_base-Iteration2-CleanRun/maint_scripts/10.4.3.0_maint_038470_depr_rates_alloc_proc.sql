/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038470_depr_rates_alloc_proc.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/17/2014 Shane Ward       Null Allocation Procedures cause error,
||                                      switching column to NOT NULL
||============================================================================
*/

--To copy the allocation procedure from the previous effective dated row to the current:
update DEPR_METHOD_RATES X
   set ALLOCATION_PROCEDURE = NVL((select B.ALLOCATION_PROCEDURE
                                     from DEPR_METHOD_RATES B
                                    where X.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                                      and B.EFFECTIVE_DATE =
                                          (select max(C.EFFECTIVE_DATE)
                                             from DEPR_METHOD_RATES C
                                            where B.DEPR_METHOD_ID = C.DEPR_METHOD_ID
                                              and C.ALLOCATION_PROCEDURE is not null)),
                                   'BROAD')
 where exists
 (select 1
          from DEPR_METHOD_RATES A, DEPR_GROUP C
         where A.EFFECTIVE_DATE = (select max(B.EFFECTIVE_DATE)
                                     from DEPR_METHOD_RATES B
                                    where A.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                                      and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID)
           and A.ALLOCATION_PROCEDURE is null
           and A.MORTALITY_CURVE_ID is not null
           and A.EXPECTED_AVERAGE_LIFE is not null
           and A.DEPR_METHOD_ID = C.DEPR_METHOD_ID
           and X.DEPR_METHOD_ID = A.DEPR_METHOD_ID
           and X.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
           and X.EFFECTIVE_DATE = A.EFFECTIVE_DATE
        union all
        select 1
          from DEPR_METHOD_RATES A, COMBINED_DEPR_GROUP C
         where A.EFFECTIVE_DATE = (select max(B.EFFECTIVE_DATE)
                                     from DEPR_METHOD_RATES B
                                    where A.DEPR_METHOD_ID = B.DEPR_METHOD_ID
                                      and A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID)
           and A.ALLOCATION_PROCEDURE is null
           and A.MORTALITY_CURVE_ID is not null
           and A.EXPECTED_AVERAGE_LIFE is not null
           and A.DEPR_METHOD_ID = C.DEPR_METHOD_ID
           and X.DEPR_METHOD_ID = A.DEPR_METHOD_ID
           and X.SET_OF_BOOKS_ID = A.SET_OF_BOOKS_ID
           and X.EFFECTIVE_DATE = A.EFFECTIVE_DATE);

--If the above statement does not fill in the field, update the ALLOCATION PROCEDURE to 'BROAD'
update DEPR_METHOD_RATES set ALLOCATION_PROCEDURE = 'BROAD' where ALLOCATION_PROCEDURE is null;

--Set ALLOCATION_PROCEDURE to not nullable
alter table DEPR_METHOD_RATES modify ALLOCATION_PROCEDURE varchar2(5) not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1429, 0, 10, 4, 3, 0, 38470, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038470_depr_rates_alloc_proc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
