/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050585_lessor_03_increase_rate_precision_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/26/2018 Andrew Hill       Change rate precision
||============================================================================
*/
ALTER TABLE lsr_ilr_rates MODIFY (rate FLOAT);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4259, 0, 2017, 3, 0, 0, 50585, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050585_lessor_03_increase_rate_precision_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;