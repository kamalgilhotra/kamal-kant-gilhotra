/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050180_lessor_03_add_rates_to_rate_types_fk_ddl.sql
|| Description:	Add new rate types to rate type table
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/20/2018 Andrew Hill    Add missing FK from lsr_ilr_rates to lsr_ilr_rate_types
||============================================================================
*/

ALTER TABLE lsr_ilr_rates ADD CONSTRAINT lsr_ilr_rates_rate_type_fk FOREIGN KEY (rate_type_id) REFERENCES lsr_ilr_rate_types(rate_type_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4221, 0, 2017, 3, 0, 0, 50180, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050180_lessor_03_add_rates_to_rate_types_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 