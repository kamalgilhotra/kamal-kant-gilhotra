/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050220_lessee_03_V_LS_ILR_SCHEDULE_FX_VW_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.2.0.0 01/24/2018 Shane "C" Ward 	Lesseee Calculation Performance enhancements
||============================================================================
*/

CREATE OR replace VIEW v_ls_ilr_schedule_fx_vw
AS
  WITH cur
       AS ( SELECT ls_currency_type_id AS ls_cur_type,
                   currency_id,
                   currency_display_symbol,
                   iso_code,
                   CASE ls_currency_type_id
                     WHEN 1 THEN 1
                     ELSE NULL
                   END                 AS contract_approval_rate
            FROM   CURRENCY
                   cross join LS_LEASE_CURRENCY_TYPE ),
       open_month
       AS ( SELECT company_id,
                   Min( gl_posting_mo_yr ) open_month
            FROM   LS_PROCESS_CONTROL
            WHERE  open_next IS NULL
            GROUP  BY company_id ),
       calc_rate
       AS ( SELECT a.company_id,
                   a.contract_currency_id,
                   a.company_currency_id,
                   a.accounting_month,
                   a.exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM   LS_LEASE_CALCULATED_DATE_RATES a
                   left outer join LS_LEASE_CALCULATED_DATE_RATES b
                                ON a.company_id = b.company_id AND
                                   a.contract_currency_id = b.contract_currency_id AND
                                   a.accounting_month = Add_months( b.accounting_month, 1 ) ),
       cr_now
       AS ( SELECT currency_from,
                   currency_to,
                   rate
            FROM   ( SELECT currency_from,
                            currency_to,
                            rate,
                            Row_number( )
                              over(
                                PARTITION BY currency_from, currency_to
                                ORDER BY exchange_date DESC ) AS rn
                     FROM   CURRENCY_RATE_DEFAULT_DENSE
                     WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                            exchange_rate_type_id = 1 )
            WHERE  rn = 1 )
  SELECT lis.ilr_id                                                                                                      ilr_id,
         lis.ilr_number,
         lease.lease_id,
         lease.lease_number,
         lis.current_revision,
         lis.revision                                                                                                    revision,
         lis.set_of_books_id                                                                                             set_of_books_id,
         lis.month                                                                                                       month,
         OPEN_MONTH.company_id,
         OPEN_MONTH.open_month,
         CUR.ls_cur_type                                                                                                 AS ls_cur_type,
         cr.exchange_date,
         CALC_RATE.exchange_date                                                                                         prev_exchange_date,
         lease.contract_currency_id,
         CUR.currency_id                                                                                                 display_currency_id,
         cr.rate,
         CALC_RATE.rate                                                                                                  calculated_rate,
         CALC_RATE.prev_rate                                                                                             previous_calculated_rate,
         CUR.iso_code,
         CUR.currency_display_symbol,
         lis.is_om,
         lis.purchase_option_amt * Nvl( CALC_RATE.rate, cr.rate )                                                        purchase_option_amt,
         lis.termination_amt * Nvl( CALC_RATE.rate, cr.rate )                                                            termination_amt,
         lis.net_present_value * Nvl( CALC_RATE.rate, cr.rate )                                                          net_present_value,
         lis.capital_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )          capital_cost,
         lis.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )      beg_capital_cost,
         lis.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )      end_capital_cost,
         lis.beg_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                             beg_obligation,
         lis.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                             end_obligation,
         lis.beg_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                          beg_lt_obligation,
         lis.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                          end_lt_obligation,
         lis.interest_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                           interest_accrual,
         lis.principal_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                          principal_accrual,
         lis.interest_paid * Nvl( CALC_RATE.rate, cr.rate )                                                              interest_paid,
         lis.principal_paid * Nvl( CALC_RATE.rate, cr.rate )                                                             principal_paid,
         lis.executory_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual1,
         lis.executory_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual2,
         lis.executory_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual3,
         lis.executory_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual4,
         lis.executory_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual5,
         lis.executory_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual6,
         lis.executory_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual7,
         lis.executory_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual8,
         lis.executory_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                         executory_accrual9,
         lis.executory_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                        executory_accrual10,
         lis.executory_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid1,
         lis.executory_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid2,
         lis.executory_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid3,
         lis.executory_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid4,
         lis.executory_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid5,
         lis.executory_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid6,
         lis.executory_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid7,
         lis.executory_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid8,
         lis.executory_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                            executory_paid9,
         lis.executory_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                           executory_paid10,
         lis.contingent_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual1,
         lis.contingent_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual2,
         lis.contingent_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual3,
         lis.contingent_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual4,
         lis.contingent_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual5,
         lis.contingent_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual6,
         lis.contingent_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual7,
         lis.contingent_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual8,
         lis.contingent_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                        contingent_accrual9,
         lis.contingent_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                       contingent_accrual10,
         lis.contingent_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid1,
         lis.contingent_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid2,
         lis.contingent_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid3,
         lis.contingent_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid4,
         lis.contingent_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid5,
         lis.contingent_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid6,
         lis.contingent_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid7,
         lis.contingent_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid8,
         lis.contingent_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                           contingent_paid9,
         lis.contingent_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                          contingent_paid10,
         lis.current_lease_cost * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )    current_lease_cost,
         lis.beg_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                          beg_deferred_rent,
         lis.deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                              deferred_rent,
         lis.end_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                          end_deferred_rent,
         lis.beg_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                       beg_st_deferred_rent,
         lis.end_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                       end_st_deferred_rent,
         lis.beg_obligation * ( Nvl( CALC_RATE.rate, 0 ) - Nvl( CALC_RATE.prev_rate, 0 ) )                               gain_loss_fx,
         lis.depr_expense * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )          depr_expense,
         lis.begin_reserve * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )         begin_reserve,
         lis.end_reserve * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate )           end_reserve,
         lis.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, lis.in_service_exchange_rate ), CR_NOW.rate ) depr_exp_alloc_adjust
  FROM   ( SELECT a.ilr_id,
                  a.ilr_number,
                  a.current_revision,
                  a.revision,
                  a.set_of_books_id,
                  a.month,
                  a.beg_capital_cost,
                  a.end_capital_cost,
                  a.beg_obligation,
                  a.end_obligation,
                  a.beg_lt_obligation,
                  a.end_lt_obligation,
                  a.interest_accrual,
                  a.principal_accrual,
                  a.interest_paid,
                  a.principal_paid,
                  a.executory_accrual1,
                  a.executory_accrual2,
                  a.executory_accrual3,
                  a.executory_accrual4,
                  a.executory_accrual5,
                  a.executory_accrual6,
                  a.executory_accrual7,
                  a.executory_accrual8,
                  a.executory_accrual9,
                  a.executory_accrual10,
                  a.executory_paid1,
                  a.executory_paid2,
                  a.executory_paid3,
                  a.executory_paid4,
                  a.executory_paid5,
                  a.executory_paid6,
                  a.executory_paid7,
                  a.executory_paid8,
                  a.executory_paid9,
                  a.executory_paid10,
                  a.contingent_accrual1,
                  a.contingent_accrual2,
                  a.contingent_accrual3,
                  a.contingent_accrual4,
                  a.contingent_accrual5,
                  a.contingent_accrual6,
                  a.contingent_accrual7,
                  a.contingent_accrual8,
                  a.contingent_accrual9,
                  a.contingent_accrual10,
                  a.contingent_paid1,
                  a.contingent_paid2,
                  a.contingent_paid3,
                  a.contingent_paid4,
                  a.contingent_paid5,
                  a.contingent_paid6,
                  a.contingent_paid7,
                  a.contingent_paid8,
                  a.contingent_paid9,
                  a.contingent_paid10,
                  a.current_lease_cost,
                  a.beg_deferred_rent,
                  a.deferred_rent,
                  a.end_deferred_rent,
                  a.beg_st_deferred_rent,
                  a.end_st_deferred_rent,
                  depr.depr_expense,
                  depr.begin_reserve,
                  depr.end_reserve,
                  depr.depr_exp_alloc_adjust,
                  a.lease_id,
                  a.company_id,
                  a.in_service_exchange_rate,
                  a.purchase_option_amt,
                  a.termination_amt,
                  a.net_present_value,
                  a.capital_cost,
                  a.is_om
           FROM   ( SELECT lis.ilr_id,
                           ilr.ilr_number,
                           ilr.current_revision,
                           lis.revision,
                           lis.set_of_books_id,
                           lis.month,
                           lis.beg_capital_cost,
                           lis.end_capital_cost,
                           lis.beg_obligation,
                           lis.end_obligation,
                           lis.beg_lt_obligation,
                           lis.end_lt_obligation,
                           lis.interest_accrual,
                           lis.principal_accrual,
                           lis.interest_paid,
                           lis.principal_paid,
                           lis.executory_accrual1,
                           lis.executory_accrual2,
                           lis.executory_accrual3,
                           lis.executory_accrual4,
                           lis.executory_accrual5,
                           lis.executory_accrual6,
                           lis.executory_accrual7,
                           lis.executory_accrual8,
                           lis.executory_accrual9,
                           lis.executory_accrual10,
                           lis.executory_paid1,
                           lis.executory_paid2,
                           lis.executory_paid3,
                           lis.executory_paid4,
                           lis.executory_paid5,
                           lis.executory_paid6,
                           lis.executory_paid7,
                           lis.executory_paid8,
                           lis.executory_paid9,
                           lis.executory_paid10,
                           lis.contingent_accrual1,
                           lis.contingent_accrual2,
                           lis.contingent_accrual3,
                           lis.contingent_accrual4,
                           lis.contingent_accrual5,
                           lis.contingent_accrual6,
                           lis.contingent_accrual7,
                           lis.contingent_accrual8,
                           lis.contingent_accrual9,
                           lis.contingent_accrual10,
                           lis.contingent_paid1,
                           lis.contingent_paid2,
                           lis.contingent_paid3,
                           lis.contingent_paid4,
                           lis.contingent_paid5,
                           lis.contingent_paid6,
                           lis.contingent_paid7,
                           lis.contingent_paid8,
                           lis.contingent_paid9,
                           lis.contingent_paid10,
                           lis.current_lease_cost,
                           lis.beg_deferred_rent,
                           lis.deferred_rent,
                           lis.end_deferred_rent,
                           lis.beg_st_deferred_rent,
                           lis.end_st_deferred_rent,
                           ilr.lease_id,
                           ilr.company_id,
                           opt.in_service_exchange_rate,
                           opt.purchase_option_amt,
                           opt.termination_amt,
                           liasob.net_present_value,
                           liasob.capital_cost,
                           lis.is_om
                    FROM   LS_ILR_SCHEDULE lis,
                           LS_ILR_OPTIONS opt,
                           LS_ILR_AMOUNTS_SET_OF_BOOKS liasob,
                           LS_ILR ilr,
                           LS_LEASE lease
                    WHERE  lis.ilr_id = opt.ilr_id AND
                           lis.revision = opt.revision AND
                           lis.ilr_id = liasob.ilr_id AND
                           lis.revision = liasob.revision AND
                           lis.set_of_books_id = liasob.set_of_books_id AND
                           lis.ilr_id = ilr.ilr_id AND
                           ilr.lease_id = lease.lease_id ) a
                  left outer join ( SELECT la.ilr_id,
                                           ldf.revision,
                                           ldf.set_of_books_id,
                                           ldf.month,
                                           SUM( ldf.depr_expense )          AS depr_expense,
                                           SUM( ldf.begin_reserve )         AS begin_reserve,
                                           SUM( ldf.end_reserve )           AS end_reserve,
                                           SUM( ldf.depr_exp_alloc_adjust ) AS depr_exp_alloc_adjust
                                    FROM   LS_ASSET la,
                                           LS_DEPR_FORECAST ldf
                                    WHERE  la.ls_asset_id = ldf.ls_asset_id
                                    GROUP  BY la.ilr_id,ldf.revision,ldf.set_of_books_id,ldf.month ) depr
                               ON a.ilr_id = depr.ilr_id AND
                                  a.revision = depr.revision AND
                                  a.set_of_books_id = depr.set_of_books_id AND
                                  a.month = depr.month ) lis
         inner join LS_LEASE lease
                 ON lis.lease_id = lease.lease_id
         inner join CURRENCY_SCHEMA cs
                 ON lis.company_id = cs.company_id
         inner join cur
                 ON CUR.currency_id = CASE CUR.ls_cur_type
                                        WHEN 1 THEN lease.contract_currency_id
                                        WHEN 2 THEN cs.currency_id
                                        ELSE NULL
                                      END
         inner join open_month
                 ON lis.company_id = OPEN_MONTH.company_id
         inner join CURRENCY_RATE_DEFAULT_DENSE cr
                 ON CUR.currency_id = cr.currency_to AND
                    lease.contract_currency_id = cr.currency_from AND
                    Trunc( cr.exchange_date, 'MONTH' ) = Trunc( lis.month, 'MONTH' )
         inner join cr_now
                 ON CUR.currency_id = CR_NOW.currency_to AND
                    lease.contract_currency_id = CR_NOW.currency_from
         left outer join calc_rate
                      ON lease.contract_currency_id = CALC_RATE.contract_currency_id AND
                         CUR.currency_id = CALC_RATE.company_currency_id AND
                         lis.company_id = CALC_RATE.company_id AND
                         lis.month = CALC_RATE.accounting_month
  WHERE  cs.currency_type_id = 1 AND
         cr.exchange_rate_type_id = 1; 

		 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4091, 0, 2017, 2, 0, 0, 50220, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050220_lessee_03_V_LS_ILR_SCHEDULE_FX_VW_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		 		 