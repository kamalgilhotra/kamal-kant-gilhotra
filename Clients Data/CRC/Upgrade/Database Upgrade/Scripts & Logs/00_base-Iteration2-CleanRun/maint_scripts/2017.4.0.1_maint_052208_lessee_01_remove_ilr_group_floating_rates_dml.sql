/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_052208_lessee_01_remove_ilr_group_floating_rates_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0.1 08/24/2018 DConway        Removed ls_ilr_group_floating_rates
 ||                                        table from Lessee Table Maintenance.
 ||============================================================================
 */

delete from powerplant_columns
where lower(table_name) = 'ls_ilr_group_floating_rates';

delete from pp_table_groups
where lower(table_name) = 'ls_ilr_group_floating_rates';

delete from powerplant_tables
where lower(table_name) = 'ls_ilr_group_floating_rates';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(9262, 0, 2017, 4, 0, 1, 52208, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052208_lessee_01_remove_ilr_group_floating_rates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;