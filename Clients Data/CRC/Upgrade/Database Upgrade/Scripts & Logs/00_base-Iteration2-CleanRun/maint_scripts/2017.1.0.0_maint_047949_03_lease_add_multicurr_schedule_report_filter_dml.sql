/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047949_03_lease_add_multicurr_schedule_report_filter_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/11/2017 Jared Watkins    add a new MC schedule report filter and update Lessee - 1015 report to use it
||============================================================================
*/

insert into pp_reports_filter(pp_report_filter_id, description, table_name, filter_uo_name)
select 102, 'Lessee - Multicurrency Schedule', 'company_setup', 'uo_ls_selecttabs_multicurr_schedule' 
from dual
where not exists (select 1 from pp_reports_filter where pp_report_filter_id = 102);

update pp_reports set pp_report_filter_id = 102
where pp_report_filter_id = 42 and description = 'Schedule by ILR' and report_number = 'Lessee - 1015';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3493, 0, 2017, 1, 0, 0, 47949, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047949_03_lease_add_multicurr_schedule_report_filter_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;