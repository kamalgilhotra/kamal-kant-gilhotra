/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053053_pwrtax_01_book_bal_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 02/12/2019 K Powers	    PP-53053 - Book balance compare
|| 2018.2.0.0 03/07/2019 K Powers	  Modified for hardcoded ID fields from MasterDB
||============================================================================
*/

DELETE FROM pp_reports WHERE datawindow='dw_tax_rpt_book_balance_compare' ;

delete from pp_dynamic_filter_mapping where pp_report_filter_id = 106;

delete from pp_reports_filter where pp_report_filter_id = 106;

DELETE FROM pp_dynamic_filter WHERE filter_id IN (288,289);

INSERT INTO pp_reports_filter (pp_report_filter_id, description, table_name, filter_uo_name)
VALUES (106, 'PowerTax - Book Balance Compare', NULL, 'uo_ppbase_tab_filter_dynamic');

INSERT INTO pp_dynamic_filter (filter_id, label, input_type, sqls_column_expression, dw, dw_id, dw_description, dw_id_datatype, required)
SELECT 288
      , label
      , input_type
      , 'book_alloc_group.company_id'
      , dw
      , dw_id
      , dw_description
      , dw_id_datatype
      , 0
FROM  pp_dynamic_filter
WHERE filter_id = 101
;

INSERT INTO pp_dynamic_filter (filter_id, label, input_type, sqls_column_expression, dw, dw_id, dw_description, dw_id_datatype, required)
SELECT 289
      , 'Book Alloc Group'
      , input_type
      , 'book_alloc_group.book_alloc_group_id'
      , 'dw_book_alloc_group' dw
      , dw_id
      , dw_description
      , dw_id_datatype
      , 0
FROM  pp_dynamic_filter
WHERE filter_id = 165
;

INSERT INTO pp_dynamic_filter_mapping (pp_report_filter_id, filter_id)
VALUES
(106,288);

INSERT INTO pp_dynamic_filter_mapping (pp_report_filter_id, filter_id)
VALUES
(106,289);

INSERT INTO pp_reports (report_id,description,long_description,subsystem,datawindow,special_note,
        report_type,time_option,report_number,input_window,filter_option,status,pp_report_subsystem_id,
        report_type_id,pp_report_time_option_id,pp_report_filter_id,pp_report_status_id,pp_report_envir_id,
        documentation,user_comment,last_approved_date,pp_report_number,old_report_number,dynamic_dw,turn_off_multi_thread)
SELECT  (SELECT Max(report_id) FROM pp_reports WHERE report_id BETWEEN 400000 AND 499999) + 1
        , 'Book to Tax book balance comparison'
        , 'Book to Tax book balance comparison by book allocation group.'
        , a.subsystem
        , 'dw_tax_rpt_book_balance_compare' AS datawindow
        , a.special_note
        , a.report_type
        , a.time_option
        , 'PwrTax - 110'
        , a.input_window
        , a.filter_option
        , a.status
        , a.pp_report_subsystem_id
        , 129 AS report_type_id --deferred tax report group
        , 50 AS pp_report_time_option_id --single year select dddw
        , 106 AS pp_report_filter_id
        , a.pp_report_status_id
        , a.pp_report_envir_id
        , a.documentation
        , a.user_comment
        , a.last_approved_date
        , a.pp_report_number
        , a.old_report_number
        , a.dynamic_dw
        , a.turn_off_multi_thread
FROM    pp_reports a
WHERE   a.report_id = (SELECT report_id  FROM pp_reports WHERE report_number = 'PwrTax - 052')
AND NOT EXISTS
	      (SELECT 1 FROM pp_reports x WHERE Lower(x.datawindow) LIKE Lower('dw_tax_rpt_book_balance_compare'))
;

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15122, 0, 2018, 2, 0, 0, 53053, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053053_pwrtax_01_book_bal_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'),
    SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
    
COMMIT;

