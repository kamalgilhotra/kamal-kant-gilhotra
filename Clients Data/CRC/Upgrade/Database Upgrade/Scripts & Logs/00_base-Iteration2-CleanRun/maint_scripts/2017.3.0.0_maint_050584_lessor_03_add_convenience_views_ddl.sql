/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050584_lessor_03_add_convenience_views_ddl.sql
|| Description:	Add views for conveniently accessing schedule calc
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 03/12/2018 Andrew Hill    Add views
||============================================================================
*/

CREATE OR REPLACE VIEW v_lsr_ilr_op_schedule_calc AS
SELECT results.ilr_id, results.revision, cols.*
FROM (SELECT ilr_id, revision, pkg_lessor_schedule.f_get_op_schedule(ilr_id, revision) sch
      from lsr_ilr_options ilro
      JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
      JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
      WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'operating') results, TABLE(results.sch) (+) cols;
      
CREATE OR REPLACE VIEW v_lsr_ilr_sales_schedule_calc AS     
SELECT results.ilr_id, results.revision, cols.*
FROM (SELECT ilro.ilr_id, ilro.revision, pkg_lessor_schedule.f_get_sales_schedule(ilro.ilr_id, ilro.revision) sch
      FROM lsr_ilr_options ilro
      JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
      JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
      WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'sales type') results, TABLE(results.sch) (+) cols;

CREATE OR REPLACE VIEW v_lsr_ilr_df_schedule_calc AS     
SELECT results.ilr_id, results.revision, cols.*
FROM (SELECT ilro.ilr_id, ilro.revision, pkg_lessor_schedule.f_get_df_schedule(ilro.ilr_id, ilro.revision) sch
      FROM lsr_ilr_options ilro
      JOIN lsr_fasb_type_sob fasb_sob ON ilro.lease_cap_type_id = fasb_sob.cap_type_id
      JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
      WHERE LOWER(TRIM(fasb_cap_type.DESCRIPTION)) = 'direct finance') results, TABLE(results.sch) (+) cols;
	  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4185, 0, 2017, 3, 0, 0, 50584, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050584_lessor_03_add_convenience_views_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;