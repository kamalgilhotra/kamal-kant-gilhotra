/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031041_prov.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/26/2013 Nathan Hollis  Point Release
||============================================================================
*/

create table TAX_ACCRUAL_BS_TAX_BASIS_TYPE
(
 TAX_BASIS_TYPE number(22),
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(35)
);

alter table TAX_ACCRUAL_BS_TAX_BASIS_TYPE
   add constraint TA_BS_TB_TYPE
       primary key (TAX_BASIS_TYPE)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_ACCRUAL_BS_TAX_BASIS_TYPE is '(F) [08] The tax accrual basis type table stores possible options for the tax basis type for m-items. There are four options: book=tax, tax=zero, property, and other.';
comment on column TAX_ACCRUAL_BS_TAX_BASIS_TYPE.TAX_BASIS_TYPE is 'The ID number of the tax basis type (1,2,3, or 10).';
comment on column TAX_ACCRUAL_BS_TAX_BASIS_TYPE.DESCRIPTION is 'The description text of the tax basis type.';
	   
insert into TAX_ACCRUAL_BS_TAX_BASIS_TYPE
   (TAX_BASIS_TYPE, DESCRIPTION)
   select 1, 'Book = Tax'
     from DUAL
   union all
   select 2, 'Tax = Zero'
     from DUAL
   union all
   select 3, 'Property'
     from DUAL
   union all
   select 10, 'Other'
     from DUAL;

alter table TAX_ACCRUAL_BS_LINE_ITEM add TAX_BASIS_TYPE number(22);

comment on column TAX_ACCRUAL_BS_LINE_ITEM.TAX_BASIS_TYPE is 'The tax basis type for the line item. Can be 1 - book=tax, 2 - tax=zero, 3 - property, or 10 - other.';

alter table TAX_ACCRUAL_BS_LINE_ITEM
   add constraint TA_BS_LI_BASIS_TYPE_FK
       foreign key (TAX_BASIS_TYPE)
       references TAX_ACCRUAL_BS_TAX_BASIS_TYPE(TAX_BASIS_TYPE);

alter table TAX_ACCRUAL_BS add TAX_BASIS_TYPE number(22);

comment on column TAX_ACCRUAL_BS.TAX_BASIS_TYPE is 'The tax basis type for the line item. Can be 1 - book=tax, 2 - tax=zero, 3 - property, or 10 - other.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (535, 0, 10, 4, 1, 0, 31041, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031041_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
