/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048894_lessor_01_import_vp_formula_comp_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/31/2017 Alex Healey           create the staging table(s) needed for CF amounts import
||============================================================================
*/
--create the staging table to hold the imported values
CREATE TABLE LSR_IMPORT_VP_VAR_COMP_AMT
  (
     import_run_id           NUMBER(22, 0) NOT NULL,
     line_id                 NUMBER(22, 0) NOT NULL,
     error_message           VARCHAR2(4000),
     time_stamp              DATE,
     user_id                 VARCHAR2(18),
     formula_component_id    NUMBER(22, 0),
     formula_component_xlate VARCHAR2(254),
     ilr_id                  NUMBER(22, 0),
     ilr_xlate               VARCHAR2(254),
     lsr_asset_id            NUMBER(22, 0),
     lsr_asset_xlate         VARCHAR2(254),
     incurred_month_number   VARCHAR2(254),
     amount                  VARCHAR2(254),
     company_id              NUMBER(22, 0),
     company_xlate           VARCHAR2(254)
  );

ALTER TABLE LSR_IMPORT_VP_VAR_COMP_AMT
  ADD CONSTRAINT lsr_import_vp_var_comp_amt_pk PRIMARY KEY (import_run_id, line_id) USING INDEX TABLESPACE pwrplant_idx;

ALTER TABLE LSR_IMPORT_VP_VAR_COMP_AMT
  ADD CONSTRAINT lsr_import_vp_var_comp_amt_fk1 FOREIGN KEY (import_run_id) REFERENCES PP_IMPORT_RUN (import_run_id);

COMMENT ON TABLE LSR_IMPORT_VP_VAR_COMP_AMT IS '(S) [06] The Lessor Import VP Var Comp Amt table is an API table used to import Variable Formula Component Amounts.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.error_message IS 'Error messages resulting from data validation in the import process.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.formula_component_id IS 'The internal Formula Component id within PowerPlant.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.formula_component_xlate IS 'Translation field for determining the Formula Component.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.ilr_id IS 'System assigned identifier for an ILR';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.ilr_xlate IS 'Translation field for determining the ILR.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.lsr_asset_id IS 'System assigned identifier for a Lessor asset';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.lsr_asset_xlate IS 'Translation field for determining the Lessor asset';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.incurred_month_number IS 'The date on which the amount for the variable component formula was incurred';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.amount IS 'The amount we are loading for the specific Formula Component.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.company_id IS 'System assigned identifier for a Company.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT.company_id IS 'Translation field for determining the Company.';

--create the archive staging table
CREATE TABLE LSR_IMPORT_VP_VAR_COMP_AMT_ARC
  (
     import_run_id           NUMBER(22, 0) NOT NULL,
     line_id                 NUMBER(22, 0) NOT NULL,
     time_stamp              DATE,
     user_id                 VARCHAR2(18),
     formula_component_id    NUMBER(22, 0),
     formula_component_xlate VARCHAR2(254),
     ilr_id                  NUMBER(22, 0),
     ilr_xlate               VARCHAR2(254),
     lsr_asset_id            NUMBER(22, 0),
     lsr_asset_xlate         VARCHAR2(254),
     incurred_month_number   VARCHAR2(254),
     amount                  VARCHAR2(254),
     company_id              NUMBER(22, 0),
     company_xlate           VARCHAR2(254)
  );

COMMENT ON TABLE LSR_IMPORT_VP_VAR_COMP_AMT_ARC IS '(S) [06] The Lessor Import VP Var Comp Amt table is an API table used to import Variable Formula Component Amounts.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.formula_component_id IS 'The internal Formula Component id within PowerPlant.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.formula_component_xlate IS 'Translation field for determining the Formula Component.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.ilr_id IS 'System assigned identifier for an ILR';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.ilr_xlate IS 'Translation field for determining the ILR.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.lsr_asset_id IS 'System assigned identifier for a Lessor asset';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.lsr_asset_xlate IS 'Translation field for determining the Lessor asset';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.incurred_month_number IS 'The date on which the amount for the variable component formula was incurred';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.amount IS 'The amount we are loading for the specific Formula Component.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.company_id IS 'System assigned identifier for a Company.';

COMMENT ON COLUMN LSR_IMPORT_VP_VAR_COMP_AMT_ARC.company_id IS 'Translation field for determining the Company.'; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3966, 0, 2017, 1, 0, 0, 48894, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048894_lessor_01_import_vp_formula_comp_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

