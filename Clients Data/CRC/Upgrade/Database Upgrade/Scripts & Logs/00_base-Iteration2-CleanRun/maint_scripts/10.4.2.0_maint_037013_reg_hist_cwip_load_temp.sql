/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037013_reg_hist_cwip_load_temp.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/10/2014 Aaron Smith         Missing from db
||============================================================================
*/

-- Temp table for loading CWIP data
create global temporary table REG_HIST_CWIP_LOAD_TEMP
(
 REG_COMPANY_ID      number(22,0),
 REG_COMPONENT_ID    number(22,0),
 REG_MEMBER_ID       number(22,0),
 REG_MEMBER_RULE_ID  number(22,0),
 HISTORIC_VERSION_ID number(22,0),
 GL_MONTH            number(22,1),
 AMOUNT              number(22,2)
) on commit preserve rows;

alter table REG_HIST_CWIP_LOAD_TEMP
   add constraint PK_REG_HIST_CWIP_LOAD_TEMP
       primary key (REG_COMPANY_ID, REG_COMPONENT_ID, REG_MEMBER_ID, REG_MEMBER_RULE_ID, HISTORIC_VERSION_ID, GL_MONTH);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1020, 0, 10, 4, 2, 0, 37013, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037013_reg_hist_cwip_load_temp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;