/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031841_projects_sys_ctrl_new_doc_on_new_rev.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By      Reason for Change
|| -------- ---------- --------------- ---------------------------------------
|| 10.4.1.0 08/28/2013 Chris Mardis    Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'Funding Just-New Doc on New Rev-WO',
          'never',
          'always;never;on send',
          'Work Orders-Controls whether a new justification document is created at the same time as a new revision. "Always" will always create a new justification document for new revisions. "Never" will never create a new justification document for new revisions. "On Send" will only create a new justification document when a new revision is created on sending another revision for approval.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'Funding Just-New Doc on New Rev-FP',
          'never',
          'always;never;on send',
          'Funding Projects-Controls whether a new justification document is created at the same time as a new revision. "Always" will always create a new justification document for new revisions. "Never" will never create a new justification document for new revisions. "On Send" will only create a new justification document when a new revision is created on sending another revision for approval.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (548, 0, 10, 4, 1, 0, 31841, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031841_projects_sys_ctrl_new_doc_on_new_rev.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
