/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_042827_jobserver_inc_20150302_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/03/2014 Paul Cordero    	 Creating new column 'ARE_LINKED_JOBS_COMPLETE' for 'PP_JOB_WORKFLOW_JOB' table
||==========================================================================================
*/

set serveroutput on;

declare 
  doesTableColumnExist number := 0;
  begin
       begin
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('ARE_LINKED_JOBS_COMPLETE', 'PP_JOB_WORKFLOW_JOB','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column ARE_LINKED_JOBS_COMPLETE for PP_JOB_WORKFLOW_JOB table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW_JOB" 
              ADD 
              (
                "ARE_LINKED_JOBS_COMPLETE" CHAR(1 BYTE) DEFAULT ''0'' CONSTRAINT PP_JOB_WORKFLOW_JOB08 NOT NULL ENABLE
              )';    

              execute immediate 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_WORKFLOW_JOB"."ARE_LINKED_JOBS_COMPLETE" IS ''Option flag to pause job execution until all linked jobs are completed.''';
			  
            end;
          else
            begin
              dbms_output.put_line('Column ARE_LINKED_JOBS_COMPLETE for PP_JOB_WORKFLOW_JOB table exists');
            end;
          end if;     

		  
          doesTableColumnExist := SYS_DOES_TABLE_COLUMN_EXIST('ARE_LINKED_JOBS_COMPLETE', 'PP_JOB_EXECUTION_JOB','PWRPLANT');
          
          if doesTableColumnExist = 0 then
            begin
              dbms_output.put_line('Creating column ARE_LINKED_JOBS_COMPLETE for PP_JOB_EXECUTION_JOB table');
                            
              execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_EXECUTION_JOB" 
              ADD 
              (
                "ARE_LINKED_JOBS_COMPLETE" CHAR(1 BYTE) DEFAULT ''0'' CONSTRAINT PP_JOB_EXECUTION_JOB08 NOT NULL ENABLE
              )';   

              execute immediate 'COMMENT ON COLUMN "PWRPLANT"."PP_JOB_EXECUTION_JOB"."ARE_LINKED_JOBS_COMPLETE" IS ''Option flag to pause job execution until all linked jobs are completed.''';
			  			  
            end;
          else
            begin
              dbms_output.put_line('Column ARE_LINKED_JOBS_COMPLETE for PP_JOB_EXECUTION_JOB table exists');
            end;
          end if;   		  
		  
	   end;
  end;
 /

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2342, 0, 2015, 1, 0, 0, 042827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042827_jobserver_inc_20150302_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;