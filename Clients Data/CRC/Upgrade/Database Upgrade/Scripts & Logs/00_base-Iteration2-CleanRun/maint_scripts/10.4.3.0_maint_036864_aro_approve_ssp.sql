/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036864_aro_approve_ssp.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.3.0   09/12/2014 Daniel Motter    ARO Approval Process
||============================================================================
*/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED,
    SYSTEM_LOCK, ASYNC_EMAIL_ON_COMPLETE)
   select (select max(PROCESS_ID) + 1 from PP_PROCESSES), 'ARO Approval', 'ARO Approval', 'ssp_aro_approve.exe', '10.4.3.0', 0, 0, 0, 0
     from DUAL
    where not exists (select 1
                        from PP_PROCESSES
                       where EXECUTABLE_FILE = 'ssp_aro_approve.exe'
                         and VERSION = '10.4.3.0');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1422, 0, 10, 4, 3, 0, 36864, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036864_aro_approve_ssp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
