/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049511_lessor_01_add_rate_types_dml.sql
|| Description:	Add new rate types to rate type table
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.2.0.0 01/18/2018 Andrew Hill    Add/update rate types
||============================================================================
*/

UPDATE lsr_ilr_rate_types
SET DESCRIPTION = 'Annual Discount Rate'
where description = 'Sales Type Discount Rate';

UPDATE lsr_ilr_rate_types
SET DESCRIPTION = 'Annual Discount Rate (Net Investment)'
WHERE description = 'Direct Finance Discount Rate';

MERGE INTO lsr_ilr_rate_types A
USING ( SELECT 5 AS rate_type_id, 'Rate Implicit in the Lease' AS DESCRIPTION
        FROM dual
        UNION ALL
        SELECT 6 as rate_type_id, 'Compounded by Frequency Rate' as description
        from dual
	UNION ALL
	SELECT 7 AS rate_type_id, 'Compounded by Frequency Rate (Net Investment)' as description
	FROM dual) b ON (LOWER(TRIM(A.DESCRIPTION)) = LOWER(TRIM(b.DESCRIPTION)) and a.rate_type_id = b.rate_type_id)
WHEN NOT MATCHED THEN
  INSERT(A.rate_type_id, A.DESCRIPTION)
  VALUES(b.rate_type_id, b.description);

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4094, 0, 2017, 3, 0, 0, 49511, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_049511_lessor_01_add_rate_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 


