/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050896_lease_01_rename_menu_items_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 04/30/2018 Shane "C" Ward 	Rename Initiate Menu Items so PPBase Security will work on them
||============================================================================
*/

UPDATE ppbase_menu_items
SET label = 'New '||label
WHERE Upper(MODULE) IN ('LESSEE', 'LESSOR')
and Upper(parent_menu_identifier) = 'MENU_WKSP_INITIATE'
AND LOWER(LABEL) IN ('mla', 'asset', 'ilr', 'component');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4903, 0, 2017, 4, 0, 0, 50896, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050896_lease_01_rename_menu_items_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;