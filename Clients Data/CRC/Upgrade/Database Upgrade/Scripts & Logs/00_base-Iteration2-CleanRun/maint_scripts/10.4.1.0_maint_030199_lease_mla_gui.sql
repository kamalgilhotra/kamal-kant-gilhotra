/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030199_lease_mla_gui.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/11/2013 Brandon Beck   Point Release
||============================================================================
*/

alter table LS_LEASE_COMPANY modify deposit_amount null;

alter table LS_LEASE add NOTES varchar2(4000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (418, 0, 10, 4, 1, 0, 30199, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030199_lease_mla_gui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
