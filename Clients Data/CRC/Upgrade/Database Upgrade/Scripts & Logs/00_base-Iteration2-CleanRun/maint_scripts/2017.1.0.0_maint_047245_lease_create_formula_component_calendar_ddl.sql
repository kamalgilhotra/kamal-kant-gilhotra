/*
||============================================================================================
|| Application: PowerPlan
|| File Name: maint_047245_lease_create_formula_component_calendar_ddl.sql
||============================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------- ---------------------------------------------------
|| 2017.1.0.0 03/16/2017 Johnny Sisouphanh Create a table to store payment month relationships
||============================================================================================
*/ 

--create the lease formula component calendar table
--this is a lease table that associates a payment month to each calendar month for a given variable component.
create table ls_formula_component_calendar (
  formula_component_id  number(22) not null,
  load_month_number   	number(2) not null,
  payment_month_number  number(2) not null,
  user_id				varchar2(18),
  time_stamp			date
);

alter table ls_formula_component_calendar 
  add constraint ls_variable_formula_calendarpk
  primary key (formula_component_id, load_month_number)
  using index tablespace pwrplant_idx;

alter table ls_formula_component_calendar 
  add constraint ls_variable_formula_calendarfk
  foreign key (formula_component_id)
  references ls_formula_component (formula_component_id);
  
comment on table ls_formula_component_calendar is '(F) [06] The LS Formula Component Calendar table is a relational table that associates a payment month to each calendar month for a given variable component.';
comment on column ls_formula_component_calendar.formula_component_id is 'A reference to the formula component for which the load month and payment month will be stored.';
comment on column ls_formula_component_calendar.load_month_number is 'The month number to store values for the load month.';
comment on column ls_formula_component_calendar.payment_month_number is 'The month number to store values for the payment month'; 
comment on column ls_formula_component_calendar.user_id    is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_formula_component_calendar.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3389, 0, 2017, 1, 0, 0, 47245, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047245_lease_create_formula_component_calendar_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	