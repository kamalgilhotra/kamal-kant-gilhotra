/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048890_lessor_01_ext_asset_import_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 Shane "C" Ward		Set up Lessor External Asset Import Tables
||============================================================================
*/
CREATE TABLE LSR_IMPORT_EXT_ASSET
  (
     import_run_id          NUMBER(22, 0) NOT NULL,
     line_id                NUMBER(22, 0) NOT NULL,
     time_stamp             DATE NULL,
     user_id                VARCHAR2(18) NULL,
     error_message          VARCHAR2(4000) NULL,
     is_modified            NUMBER(22, 0) NULL,
     ilr_id                 NUMBER(22, 0) NULL,
     ilr_id_xlate           VARCHAR2(254) NULL,
     revision               NUMBER(22, 0) NULL,
     asset_id               NUMBER(22, 0) NULL,
     fair_market_value      NUMBER(22, 2) NULL,
     guaranteed_residual    NUMBER(22, 2) NULL,
     estimated_residual_pct NUMBER(22, 8) NULL,
     estimated_residual     NUMBER(22, 0) NULL,
     expected_life          NUMBER(22, 0) NULL,
     economic_life          NUMBER(22, 0) NULL,
     serial_number          VARCHAR2(35) NULL,
     description            VARCHAR2(35) NULL,
     long_description       VARCHAR2(254) NULL,
     in_svc_exchange_rate   NUMBER(22, 8) NULL,
     carrying_cost          NUMBER(22, 2) NULL,
     actual_residual        NUMBER(22, 2) NULL,
     unguaranteed_residual  NUMBER(22, 2) NULL
  );

ALTER TABLE LSR_IMPORT_EXT_ASSET
  ADD CONSTRAINT lsr_import_ext_asset_pk PRIMARY KEY ( import_run_id, line_id );

ALTER TABLE LSR_IMPORT_EXT_ASSET
  ADD CONSTRAINT lsr_import_ext_asset_run_fk FOREIGN KEY ( import_run_id ) REFERENCES PP_IMPORT_RUN ( import_run_id );

COMMENT ON TABLE LSR_IMPORT_EXT_ASSET IS '(S)  [06]
The LSR Import External Asset table is an API table used to import Non-PowerPlan Asset assignment and data to ILRs.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.error_message IS 'Error messages resulting from data valdiation in the import process.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.ilr_id IS 'The internal ILR id within PowerPlant .';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.ilr_id_xlate IS 'Translation field for determining the ILR.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.revision IS 'Revision of the ILR, if null defaults to current_revision of ILR .';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.asset_id IS 'The internal Asset id within Lessor .';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.description IS 'POPULATED BY IMPORT Records a brief description of the asset.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.serial_number IS 'POPULATED BY IMPORT The serial number.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.long_description IS 'POPULATED BY IMPORT Records a more detailed description of the asset.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.fair_market_value IS 'The fair market value in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.carrying_cost IS 'The carrying cost in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.guaranteed_residual IS 'The guaranteed residual amount in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.actual_residual IS 'The actual residual amount in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.estimated_residual_pct IS 'The estimated residual percent calculated as Estimated Residual Amount/Fair Market Value.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.estimated_residual IS 'The estimated residual used to derive estimated residual percent calculated as Estimated Residual Amount/Fair Market Value.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.unguaranteed_residual IS 'The unguaranteed residual amount in contract currency calculated as Estimated Residual-Guaranteed Residual.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.expected_life IS 'The user-entered expected life in months.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.economic_life IS 'The user-entered economic life in months.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET.in_svc_exchange_rate IS 'POPULATED BY IMPORT the exchange rate used for translating company amounts to contract currency for ILR.';

CREATE TABLE LSR_IMPORT_EXT_ASSET_ARCHIVE
  (
     import_run_id          NUMBER(22, 0) NOT NULL,
     line_id                NUMBER(22, 0) NOT NULL,
     time_stamp             DATE NULL,
     user_id                VARCHAR2(18) NULL,
     error_message          VARCHAR2(4000) NULL,
     is_modified            NUMBER(22, 0) NULL,
     ilr_id                 NUMBER(22, 0) NULL,
     ilr_id_xlate           VARCHAR2(254) NULL,
     revision               NUMBER(22, 0) NULL,
     asset_id               NUMBER(22, 0) NULL,
     fair_market_value      NUMBER(22, 2) NULL,
     guaranteed_residual    NUMBER(22, 2) NULL,
     estimated_residual_pct NUMBER(22, 8) NULL,
     estimated_residual     NUMBER(22, 0) NULL,
     expected_life          NUMBER(22, 0) NULL,
     economic_life          NUMBER(22, 0) NULL,
     serial_number          VARCHAR2(35) NULL,
     description            VARCHAR2(35) NULL,
     long_description       VARCHAR2(254) NULL,
     in_svc_exchange_rate   NUMBER(22, 8) NULL,
     carrying_cost          NUMBER(22, 2) NULL,
     actual_residual        NUMBER(22, 2) NULL,
     unguaranteed_residual  NUMBER(22, 2) NULL
  );

COMMENT ON TABLE LSR_IMPORT_EXT_ASSET_ARCHIVE IS '(S)  [06]
The LSR Import External Asset table is an API table used to import Non-PowerPlan Asset assignment and data to ILRs.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.is_modified IS 'System-assigned number to specify if this row has been modified from its original values.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.error_message IS 'Error messages resulting from data valdiation in the import process.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.ilr_id IS 'The internal ILR id within PowerPlant .';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.ilr_id_xlate IS 'Translation field for determining the ILR.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.revision IS 'Revision of the ILR, if null defaults to current_revision of ILR .';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.asset_id IS 'The internal Asset id within Lessor .';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.description IS 'POPULATED BY IMPORT Records a brief description of the asset.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.serial_number IS 'POPULATED BY IMPORT The serial number.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.long_description IS 'POPULATED BY IMPORT Records a more detailed description of the asset.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.fair_market_value IS 'The fair market value in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.carrying_cost IS 'The carrying cost in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.guaranteed_residual IS 'The guaranteed residual amount in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.actual_residual IS 'The actual residual amount in contract currency converted from the company currency amount.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.estimated_residual_pct IS 'The estimated residual percent calculated as Estimated Residual Amount/Fair Market Value.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.estimated_residual IS 'The estimated residual used to derive estimated residual percent calculated as Estimated Residual Amount/Fair Market Value.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.unguaranteed_residual IS 'The unguaranteed residual amount in contract currency calculated as Estimated Residual-Guaranteed Residual.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.expected_life IS 'The user-entered expected life in months.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.economic_life IS 'The user-entered economic life in months.';

COMMENT ON COLUMN LSR_IMPORT_EXT_ASSET_ARCHIVE.in_svc_exchange_rate IS 'POPULATED BY IMPORT the exchange rate used for translating company amounts to contract currency for ILR.'; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3851, 0, 2017, 1, 0, 0, 48890, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048890_lessor_01_ext_asset_import_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
