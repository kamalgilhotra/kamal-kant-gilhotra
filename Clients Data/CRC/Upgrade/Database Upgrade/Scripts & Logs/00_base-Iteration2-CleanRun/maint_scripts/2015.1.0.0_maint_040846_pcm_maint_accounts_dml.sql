/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_040846_pcm_maint_accounts_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 12/16/2014    Alex P         Set up accounts and reporting workspaces
||============================================================================
*/

-- Feedback from usability lab
UPDATE pp_dynamic_filter
SET sqls_column_expression = 'work_order_control.description || '' '' || work_order_control.work_order_number',
	label = 'Funding Project'	
WHERE label = 'Funding Project Description'
	AND filter_id = 214;
	
-- Set up reporting workspace
update ppbase_workspace 
set workspace_uo_name = 'uo_pcm_rpt_wksp_reports'
where module = 'pcm' and workspace_identifier = 'rpt_reports';

-- Set up accounts workspace
update ppbase_workspace 
set workspace_uo_name = 'uo_pcm_maint_wksp_accounts'
where module = 'pcm'
 and workspace_identifier in ( 'wo_maint_accounts', 'fp_maint_accounts' );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2117, 0, 2015, 1, 0, 0, 40846, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040846_pcm_maint_accounts_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;