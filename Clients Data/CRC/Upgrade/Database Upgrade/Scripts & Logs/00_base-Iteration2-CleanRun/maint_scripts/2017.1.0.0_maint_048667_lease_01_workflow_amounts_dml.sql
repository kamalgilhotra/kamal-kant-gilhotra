/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048667_lease_01_workflow_amounts_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/24/2017 Shane "C" Ward  Update base/default Workflow Amount SQL for Lessee/Lessor
||
||============================================================================
*/

UPDATE workflow_amount_sql SET sql = 'select nvl(sum(max_lease_line),0) appr_amount from LS_LEASE_COMPANY where lease_id = <<id_field1>>'
WHERE subsystem = 'lessee_mla_approval';

UPDATE workflow_amount_sql SET sql =
'select nvl(max(beg_obligation),0) appr_amount from LS_ILR_SCHEDULE
where ilr_id = <<id_field1>> and revision = <<id_field2>>
and month = (select min(month) from ls_ilr_schedule
where ilr_id = <<id_field1>> and revision = <<id_field2>>)'
WHERE subsystem = 'lessee_ilr_approval';

UPDATE workflow_amount_sql SET sql = 'select nvl(sum(amount),0) appr_amount from all LS_PAYMENT_HDR where payment_id = <<id_field1>>'
WHERE subsystem = 'lessee_mla_approval';

UPDATE workflow_amount_sql SET sql = 'select nvl(max(beg_receivable),0) appr_amount from LSR_ILR_SCHEDULE
where ilr_id = <<id_field1>> and revision = <<id_field2>>
and month = (select min(month) from lsr_ilr_schedule
where ilr_id = <<id_field1>> and revision = <<id_field2>>)'
WHERE subsystem = 'lessor_ilr_approval';

UPDATE workflow_amount_sql SET sql = 'select 0 appr_amount from dual'
WHERE subsystem = 'lessor_mla_approval';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3827, 0, 2017, 1, 0, 0, 48667, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048667_lease_01_workflow_amounts_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
