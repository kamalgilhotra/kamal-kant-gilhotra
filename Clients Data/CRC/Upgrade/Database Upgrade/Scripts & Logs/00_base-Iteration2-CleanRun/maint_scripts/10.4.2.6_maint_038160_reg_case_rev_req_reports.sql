/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038160_reg_case_rev_req_reports.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.6 05/22/2014 Shane Ward     Added reports for reg case rev requirements
||========================================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID)
values
   ((select max(REPORT_ID) + 1 from PP_REPORTS), 'Case Revenue Requirement ROE',
    'Case Revenue Requirement Return on Equity', 'dw_reg_report_case_rev_req_return', 'RR - 101', 101, 402, 104,
    1, 1, 3);
insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID)
values
   ((select max(REPORT_ID) + 1 from PP_REPORTS), 'Case Revenue Requirement',
    'Case Revenue Requirement Return', 'dw_reg_report_case_rev_req', 'RR - 102', 101, 402, 104,
    1, 1, 3);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1150, 0, 10, 4, 2, 6, 38160, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038160_reg_case_rev_req_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;