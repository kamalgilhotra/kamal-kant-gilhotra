/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045511_pwrtax_plant_recon_new_wksp_dml.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 03/08/2016 David Haupt    New Plant Reconciliation workspace
||============================================================================
*/

UPDATE ppbase_menu_items SET item_order = item_order + 1
WHERE menu_level = 1
AND Lower(MODULE) = 'powertax'
AND Lower(menu_identifier) IN ('reporting', 'admin');

INSERT INTO ppbase_workspace
(MODULE, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
VALUES
('powertax', 'uo_tax_plant_recon_wksp', 'Plant Reconciliation', 'uo_tax_plant_recon_wksp', 'Plant Reconciliation', 1);

INSERT INTO ppbase_menu_items
(MODULE, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
VALUES
('powertax', 'find_assets', 1, 8, 'Plant Reconciliation', 'Plant Reconciliation', NULL, 'uo_tax_plant_recon_wksp', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3094, 0, 2016, 1, 0, 0, 045511, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045511_pwrtax_plant_recon_new_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;