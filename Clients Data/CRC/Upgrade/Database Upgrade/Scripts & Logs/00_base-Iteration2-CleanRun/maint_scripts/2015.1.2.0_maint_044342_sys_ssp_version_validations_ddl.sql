 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044342_sys_ssp_version_validations_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2015.1.2.0  07/29/2015 David Haupt    Adding a new table for use in SSP Version Validation
 ||============================================================================
 */

CREATE TABLE pp_custom_pbd_versions
(
      process_id NUMBER(22,0) NOT NULL,
      pbd_name VARCHAR2(35),
      version VARCHAR2(35),
      user_id VARCHAR2(18),
      time_stamp date
);

ALTER TABLE pp_custom_pbd_versions
  ADD CONSTRAINT FK_PP_CUSTOM_PBD_VERSIONS_1
  FOREIGN KEY (process_id)
  REFERENCES pp_processes (process_id);

ALTER TABLE pp_custom_pbd_versions
  ADD CONSTRAINT pk_pp_custom_pbd_versions
  PRIMARY KEY (process_id, pbd_name)
  USING INDEX TABLESPACE pwrplant_idx;

COMMENT ON TABLE pp_custom_pbd_versions               IS 'The PP_CUSTOM_PBD_VERSIONS table is where custom SSP versions are configured';
COMMENT ON COLUMN pp_custom_pbd_versions.process_id    IS 'ID of the process (interface) with custom code';
COMMENT ON COLUMN pp_custom_pbd_versions.pbd_name      IS 'Name of the PBD file containing custom code. Must include ".pbd" at the end';
COMMENT ON COLUMN pp_custom_pbd_versions.version       IS 'Version of the the custom PBD for an interface. Does not necessarily need to match PowerPlan application version';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2726, 0, 2015, 1, 2, 0, 044342, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.2.0_maint_044342_sys_ssp_version_validations_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;