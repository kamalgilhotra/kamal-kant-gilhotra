 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048762_01_lease_ilr_options_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 09/18/2017 Shane "C" Ward   New Lessor ILR Options and new Fields 
 ||											to Lessee ILR Options
 ||============================================================================
 */

--Modify LS ILR Options to include some new fields
ALTER TABLE ls_ilr_options ADD specialized_asset NUMBER(1,0) DEFAULT 0 NOT null;
ALTER TABLE ls_ilr_options ADD intent_to_purchase NUMBER(1,0) DEFAULT 0 NOT NULL;
ALTER TABLE ls_ilr_options ADD likely_to_collect NUMBER(1,0) DEFAULT 1 NOT NULL;
ALTER TABLE ls_ilr_options ADD sublease_flag NUMBER(1,0);
ALTER TABLE ls_ilr_options ADD sublease_id NUMBER(22);
ALTER TABLE ls_ilr_options ADD interco_lease_flag NUMBER(1,0) DEFAULT 0 NOT NULL;
ALTER TABLE ls_ilr_options ADD interco_lease_company NUMBER(22,0);

COMMENT ON COLUMN ls_ilr_options.specialized_asset IS 'Flag for whether the ILR includes Specialized Assets. 0 = False 1 = True';
COMMENT ON COLUMN ls_ilr_options.intent_to_purchase IS 'Flag for whether the ILR includes Intent to Purchase at Lease End 0 = False 1 = True';
cOMMENT ON COLUMN ls_ilr_options.likely_to_collect IS 'Flag for whether the ILR Payments are to be collected by the Lessor. 0 = False 1 = True';
COMMENT ON COLUMN ls_ilr_options.sublease_flag IS 'Flag for whether the ILR includes a Sublease. 0 = false 1 = true';
COMMENT ON COLUMN ls_ilr_options.sublease_id IS 'ID of Sublease associated with the ILR';
COMMENT ON COLUMN ls_ilr_options.interco_lease_flag IS 'Flag for whether the ILR includes an InterCompany Lease. 0 = false 1 = true';
COMMENT ON COLUMN ls_ilr_options.interco_lease_company IS 'Company ID (from PP Company) signifying the other Company associated with the ILR.';

ALTER TABLE ls_ilr_options
  ADD CONSTRAINT fk_ls_ilr_company FOREIGN KEY (
    interco_lease_company
  ) REFERENCES company_setup (
    company_id
  );

ALTER TABLE ls_ilr_options
  ADD CONSTRAINT fk_ls_ilr_sublease FOREIGN KEY (
    sublease_id
  ) REFERENCES ls_lease (
    lease_id
  );

--LSR ILR options new Table
CREATE TABLE lsr_ilr_options (
  ilr_id                   NUMBER(22,0) NOT NULL,
  revision                 NUMBER(22,0) NOT NULL,
  time_stamp               DATE         NULL,
  user_id                  VARCHAR2(18) NULL,
  purchase_option_type_id  NUMBER(22,0) NULL,
  purchase_option_amt      NUMBER(22,2) NULL,
  renewal_option_type_id   NUMBER(22,0) NULL,
  cancelable_type_id       NUMBER(22,0) NULL,
  itc_sw                   NUMBER(1,0)  NULL,
  partial_retire_sw        NUMBER(1,0)  NULL,
  sublet_sw                NUMBER(1,0)  NULL,
  muni_bo_sw               NUMBER(1,0)  NULL,
  lease_cap_type_id        NUMBER(22,0) NULL,
  termination_amt          NUMBER(22,2) NULL,
  payment_shift            NUMBER(22,0) NULL,
  import_run_id            NUMBER(22,0) NULL,
  initial_lease_cost       NUMBER(22,2) DEFAULT 0 NOT NULL,
  in_service_exchange_rate NUMBER(22,8) NULL,
  intent_to_purchase       NUMBER(1,0)  DEFAULT 0 NOT NULL,
  specialized_asset        NUMBER(1,0)  DEFAULT 0 NOT NULL,
  likely_to_collect        NUMBER(1,0)  DEFAULT 1 NOT NULL,
  sublease_flag                 NUMBER(1,0)  NULL,
  sublease_id              NUMBER(22,0) NULL,
  interco_lease_flag       NUMBER(1,0)  DEFAULT 0 NOT NULL,
  interco_lease_company    NUMBER(22,0) NULL
);

ALTER TABLE lsr_ilr_options
  ADD CONSTRAINT pk_lsr_ilr_options PRIMARY KEY (
    ilr_id,
    revision
  );

ALTER TABLE lsr_ilr_options
  ADD CONSTRAINT fk_lsr_ilr_opt_cap_type FOREIGN KEY (
    lease_cap_type_id
  ) REFERENCES ls_lease_cap_type (
    ls_lease_cap_type_id
  );

ALTER TABLE lsr_ilr_options
  ADD CONSTRAINT fk_lsr_ilr_opt_imp_run FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  );

ALTER TABLE lsr_ilr_options
  ADD CONSTRAINT fk_lsr_ilr_options FOREIGN KEY (
    ilr_id,
    revision
  ) REFERENCES lsr_ilr_approval (
    ilr_id,
    revision
  );

ALTER TABLE lsr_ilr_options
  ADD CONSTRAINT fk_lsr_ilr_company FOREIGN KEY (
    interco_lease_company
  ) REFERENCES company_setup (
    company_id
  );

ALTER TABLE lsr_ilr_options
  ADD CONSTRAINT fk_lsr_ilr_sublease FOREIGN KEY (
    sublease_id
  ) REFERENCES lsr_lease (
    lease_id
  );


COMMENT ON TABLE lsr_ilr_options IS '(S)  [06]
The Lessor ILR Options table holds various options that affect the net present value of an ILR.  These include: Extended Rental Agreement, Bargain Purchase Amount, Termination Penalty.';

COMMENT ON COLUMN lsr_ilr_options.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN lsr_ilr_options.revision IS 'The revision.';
COMMENT ON COLUMN lsr_ilr_options.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_options.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_options.purchase_option_type_id IS 'Represents if this ILR has a purchase option.';
COMMENT ON COLUMN lsr_ilr_options.purchase_option_amt IS 'The amount of the purchase option.';
COMMENT ON COLUMN lsr_ilr_options.renewal_option_type_id IS 'Represents if this ILR has a renewal option.';
COMMENT ON COLUMN lsr_ilr_options.cancelable_type_id IS 'A PowerPlant internal id representing whether or not a lease can be cancelled before the lease terms are up.';
COMMENT ON COLUMN lsr_ilr_options.itc_sw IS 'A flag identifying whether this ILR has Investment Tax Credits associated with it.';
COMMENT ON COLUMN lsr_ilr_options.partial_retire_sw IS 'A flag identifying whether or not partial retirements are allowed.';
COMMENT ON COLUMN lsr_ilr_options.sublet_sw IS 'A flag identifying if the lease allows subletting.';
COMMENT ON COLUMN lsr_ilr_options.lease_cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';
COMMENT ON COLUMN lsr_ilr_options.termination_amt IS 'The amount for terminating the lease.';
COMMENT ON COLUMN lsr_ilr_options.payment_shift IS 'The number of months that payments are shifted relative to the schedule months.';
COMMENT ON COLUMN lsr_ilr_options.import_run_id IS 'The internal PowerPlan ID for the import run in which the ILR was imported. Null if created via the system.';
COMMENT ON COLUMN lsr_ilr_options.initial_lease_cost IS 'Initial costs incurred in the creation of the lease contract that are included in the NPV';
COMMENT ON COLUMN lsr_ilr_options.in_service_exchange_rate IS 'The exchange rate used to convert from the lease contract currency to the company''s currency at the time that the ILR revision was put in-service. This is used to calculate a gain/loss as the exchange rate changes over time.';
COMMENT ON COLUMN lsr_ilr_options.specialized_asset IS 'Flag for whether the ILR includes Specialized Assets. 0 = False 1 = True';
COMMENT ON COLUMN lsr_ilr_options.intent_to_purchase IS 'Flag for whether the ILR includes Intent to Purchase at Lease End 0 = False 1 = True';
cOMMENT ON COLUMN lsr_ilr_options.likely_to_collect IS 'Flag for whether the ILR Payments are to be collected by the Lessor. 0 = False 1 = True';
COMMENT ON COLUMN lsr_ilr_options.sublease_flag IS 'Flag for whether the ILR includes a Sublease. 0 = false 1 = true';
COMMENT ON COLUMN lsr_ilr_options.sublease_id IS 'ID of Sublease associated with the ILR';
COMMENT ON COLUMN lsr_ilr_options.interco_lease_flag IS 'Flag for whether the ILR includes an InterCompany Lease. 0 = false 1 = true';
COMMENT ON COLUMN lsr_ilr_options.interco_lease_company IS 'Company ID (from PP Company) signifying the other Company associated with the ILR.';

--New Rate Types table, holds definition of Rates used by Lessor ILR
CREATE TABLE lsr_ilr_rate_types (
rate_type_id number(22) NOT NULL,
description VARCHAR2(70) NOT NULL,
 time_stamp               DATE         NULL,
 user_id                  VARCHAR2(18) NULL);

ALTER TABLE lsr_ilr_rate_types
  ADD CONSTRAINT pk_lsr_ilr_rate_types PRIMARY KEY (
    rate_type_id
  );

COMMENT ON TABLE lsr_ilr_rate_types IS '(S)  [06]
The Lessor ILR Rate Types table holds available Rate Types to be used with an ILR. This is a definition table, the rates are inputted by ILR in lsr_ilr_rates';
COMMENT ON COLUMN lsr_ilr_rate_types.rate_type_id IS 'System Assigned ID of a Lessor ILR Rate Type';
COMMENT ON COLUMN lsr_ilr_rate_types.description IS 'Description of the Rate Type displayed to User';
COMMENT ON COLUMN lsr_ilr_rate_types.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_rate_types.user_id IS 'Standard system-assigned user id used for audit purposes.';

CREATE TABLE lsr_ilr_rates (
ilr_id NUMBER(22,0) NOT NULL,
revision NUMBER(22,0) NOT NULL,
rate_type_id NUMBER(22,0) NOT NULL,
rate NUMBER (22, 8) NULL,
time_stamp               DATE         NULL,
 user_id                  VARCHAR2(18) NULL);

ALTER TABLE lsr_ilr_rates
  ADD CONSTRAINT pk_lsr_ilr_rates PRIMARY KEY (
    rate_type_id,
    ilr_id,
    revision
  );

COMMENT ON TABLE lsr_ilr_rates IS '(C) [06]
The Lessor ILR Rates table holds applicable rates for an ILR for a specific Revision';
COMMENT ON COLUMN lsr_ilr_rates.rate_type_id IS 'System Assigned ID of a Lessor ILR Rate Type';
COMMENT ON COLUMN lsr_ilr_rates.ilr_id IS 'System Assigned ID of a Lessor ILR';
COMMENT ON COLUMN lsr_ilr_rates.revision IS 'Identifier of a Lessor ILR Revision';
COMMENT ON COLUMN lsr_ilr_rates.rate IS 'Percentage Rate to be used for the Rate Type assigned. Stored in Decimal format not Percent';
COMMENT ON COLUMN lsr_ilr_rates.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_ilr_rates.user_id IS 'Standard system-assigned user id used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3714, 0, 2017, 1, 0, 0, 48762, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048762_01_lease_ilr_options_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
