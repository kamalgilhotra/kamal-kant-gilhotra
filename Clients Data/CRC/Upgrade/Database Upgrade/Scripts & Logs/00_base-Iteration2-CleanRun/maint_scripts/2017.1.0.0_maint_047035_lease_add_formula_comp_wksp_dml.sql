/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047035_lease_add_formula_comp_wksp_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 02/21/2017 Jared Watkins    Add new formula components workspace to menu
||============================================================================
*/

INSERT INTO ppbase_workspace (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP, OBJECT_TYPE_ID) 
VALUES ('LESSEE', 'manage_formula_components', 'Manage Formula Components', 'uo_ls_varpayments_formula_components_wksp', 'Manage Formula Components',	1);
	
INSERT INTO ppbase_menu_items (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN) VALUES (
	'LESSEE','manage_formula_components', 2, 2, 'Formula Components', 'Formula Components', 'menu_wksp_var_payments', 'manage_formula_components', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3379, 0, 2017, 1, 0, 0, 47035, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047035_lease_add_formula_comp_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	