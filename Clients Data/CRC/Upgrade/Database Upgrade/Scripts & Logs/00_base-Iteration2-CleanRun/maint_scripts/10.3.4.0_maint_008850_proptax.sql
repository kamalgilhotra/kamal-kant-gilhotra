/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008850_proptax.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   11/29/2011 Julia Breuer   Point Release
||============================================================================
*/

--
-- Maint 8850
--

--
--    Change the ledger adjustment import so that it can accept dollar adjustments or percentage adjustments.
--
alter table PWRPLANT.PT_IMPORT_LEDGER add ADJUST_IS_PERCENT_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEDGER add ADJUST_IS_PERCENT_YN    number(22,0);

alter table PWRPLANT.PT_IMPORT_LEDGER_ARCHIVE add ADJUST_IS_PERCENT_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEDGER_ARCHIVE add ADJUST_IS_PERCENT_YN    number(22,0);

insert into PWRPLANT.PT_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE)
values
   (8, 'adjust_is_percent_yn', 'Adjusment is Percent', 'adjust_is_percent_xlate', 0, 1,
    'number(22,0)');

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (8, 'adjust_is_percent_yn', 77);

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (8, 'adjust_is_percent_yn', 78);

update PWRPLANT.PT_IMPORT_COLUMN
   set COLUMN_TYPE = 'number(22,8)'
 where IMPORT_TYPE_ID = 8
   and COLUMN_NAME in ('additions_adjustment',
                       'beg_bal_adjustment',
                       'cwip_adjustment',
                       'end_bal_adjustment',
                       'reserve_adjustment',
                       'retirements_adjustment',
                       'tax_basis_adjustment',
                       'transfers_adjustment');
commit;

--
--    Change the ledger adjustment import so that it can accept 'spread on book' or 'spread on adjusted' adjustments.
--
alter table PWRPLANT.PT_IMPORT_LEDGER add SPREAD_ON_BOOK_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEDGER add SPREAD_ON_BOOK_YN    number(22,0);

alter table PWRPLANT.PT_IMPORT_LEDGER_ARCHIVE add SPREAD_ON_BOOK_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEDGER_ARCHIVE add SPREAD_ON_BOOK_YN    number(22,0);

insert into PWRPLANT.PT_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE)
values
   (8, 'spread_on_book_yn', 'Spread on Book Balance', 'spread_on_book_xlate', 0, 1, 'number(22,0)');

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (8, 'spread_on_book_yn', 77);

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (8, 'spread_on_book_yn', 78);

commit;

--
--    Change the ledger adjustment import so that it takes in whether to auto-calculate the reserve adjustment.
--
alter table PWRPLANT.PT_IMPORT_LEDGER add AUTOCALC_RSV_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEDGER add AUTOCALC_RSV_YN    number(22,0);

alter table PWRPLANT.PT_IMPORT_LEDGER_ARCHIVE add AUTOCALC_RSV_XLATE varchar2(254);
alter table PWRPLANT.PT_IMPORT_LEDGER_ARCHIVE add AUTOCALC_RSV_YN    number(22,0);

insert into PWRPLANT.PT_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE)
values
   (8, 'autocalc_rsv_yn', 'Auto-Calculate Reserve Adjustment', 'autocalc_rsv_xlate', 0, 1,
    'number(22,0)');

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (8, 'autocalc_rsv_yn', 77);
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (8, 'autocalc_rsv_yn', 78);

commit;

--
-- Create a temp table to store the ledger items being adjusted.
--
create global temporary table PWRPLANT.PT_TEMP_LEDGER_ADJS
(
 PROPERTY_TAX_LEDGER_ID number(22,0) not null,
 PROPERTY_TAX_ADJUST_ID number(22,0) not null,
 USER_INPUT             number(22,0) not null,
 TAX_YEAR               number(22,0) not null,
 BEG_BAL_ADJUSTMENT     number(22,2) default 0 not null,
 ADDITIONS_ADJUSTMENT   number(22,2) default 0 not null,
 RETIREMENTS_ADJUSTMENT number(22,2) default 0 not null,
 TRANSFERS_ADJUSTMENT   number(22,2) default 0 not null,
 END_BAL_ADJUSTMENT     number(22,2) default 0 not null,
 CWIP_ADJUSTMENT        number(22,2) default 0 not null,
 TAX_BASIS_ADJUSTMENT   number(22,2) default 0 not null,
 RESERVE_ADJUSTMENT     number(22,2) default 0 not null,
 NOTES                  varchar2(4000)
) on commit preserve rows;

create unique index PWRPLANT.PT_TEMP_LEDGER_ADJS_PK on PWRPLANT.PT_TEMP_LEDGER_ADJS ( PROPERTY_TAX_LEDGER_ID );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (61, 0, 10, 3, 4, 0, 8850, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_008850_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
