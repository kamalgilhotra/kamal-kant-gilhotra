/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049488_lessor_01_add_variable_payment_menu_items_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/20/2017 Jared Watkins  Add Lessor menu items/workspaces for Variable Payments
||============================================================================
*/

--create the workspaces to link with the VP menu items
insert into ppbase_workspace(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'manage_var_payments', 'Manage Variable Payments', 'uo_lsr_var_payments_wksp', 'Manage Variable Payments', 1);

insert into ppbase_workspace(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'manage_formula_components', 'Manage Formula Components', 'uo_lsr_var_payments_formula_components_wksp', 'Manage Formula Components', 1);

insert into ppbase_workspace(module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values ('LESSOR', 'manage_formula_comp_amounts', 'Formula Component Amts', 'uo_lsr_var_payments_component_amounts_wksp', 'Formula Component Amts', 1);

--insert top-level VPs menu item
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'menu_wksp_var_payments', 1, 9, 'Variable Payments', 'Variable Payments', null, null, 1);

--insert workspace-level VP menu items
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'manage_var_payments', 2, 1, 'Manage Variable Payments', 'Manage Variable Payments', 'menu_wksp_var_payments', 'manage_var_payments', 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'manage_formula_components', 2, 2, 'Formula Components', 'Manage Formula Components', 'menu_wksp_var_payments', 'manage_formula_components', 1);

insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'manage_formula_comp_amounts', 2, 3, 'Component Amounts', 'Formula Component Amounts', 'menu_wksp_var_payments', 'manage_formula_comp_amounts', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3802, 0, 2017, 1, 0, 0, 49488, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049488_lessor_01_add_variable_payment_menu_items_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;