/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038358_lease_estimated_residual.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------
|| 10.4.3.0 06/04/2014 Kyle Peterson
||============================================================================
*/

alter table LS_ASSET add ESTIMATED_RESIDUAL number(22, 8) default 0;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1178, 0, 10, 4, 3, 0, 38358, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038358_lease_estimated_residual.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
