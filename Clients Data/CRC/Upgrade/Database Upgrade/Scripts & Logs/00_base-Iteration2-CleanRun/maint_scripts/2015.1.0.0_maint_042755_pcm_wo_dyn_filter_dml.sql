/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042755_pcm_wo_dyn_filter_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [2015.1 02/05/2015 	B.Beck				PCM Work Order Type filter should limit to just work orders.
||==========================================================================================
*/

insert into pp_dynamic_filter
(filter_id, label, input_type, sqls_column_expression, dw, dw_id, dw_description, dw_id_datatype)
values
(
262, 'Work Order Type', 'dw', 'work_order_control.work_order_type_id', 'dw_pp_work_order_type_filter_wos',
'work_order_type_id', 'description', 'N'
);

update pp_dynamic_filter_mapping
set filter_id = 262
where filter_id = 34
and pp_report_filter_id = 87
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2257, 0, 2015, 1, 0, 0, 042755, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042755_pcm_wo_dyn_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;