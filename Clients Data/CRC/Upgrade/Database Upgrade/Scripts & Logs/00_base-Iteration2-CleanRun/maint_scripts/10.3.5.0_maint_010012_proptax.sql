/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010012_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    07/17/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create an import for utility accounts.
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 24, sysdate, user, 'Add : Utility Accounts', 'Import Utility Accounts', 'pt_import_util_account', 'pt_import_util_account_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 24, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 1, 1, 'number(22,0)', 'business_segment', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 24, 'ferc_plt_acct_id', sysdate, user, 'Ferc Plant Account', 'ferc_plt_acct_xlate', 0, 1, 'number(22,0)', 'ferc_plant_account', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 24, 'func_class_id', sysdate, user, 'Functional Class', 'func_class_xlate', 1, 1, 'number(22,0)', 'func_class', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 24, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 24, 'status_code_id', sysdate, user, 'Status Code', 'status_code_xlate', 1, 1, 'number(22,0)', 'status_code', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 24, 'external_account_code', sysdate, user, 'External Account Code', '', 0, 1, 'varchar2(35)', '', 1, '', 0, '' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 191, sysdate, user, 'FERC Plant Account.Description', 'The passed in value corresponds to the FERC Plant Account: Description field.  Translate to the FERC Plant Account ID using the Description column on the FERC Plant Account table.', 'ferc_plt_acct_id', '( select fp.ferc_plt_acct_id from ferc_plant_account fp where upper( trim( <importfield> ) ) = upper( trim( fp.description ) ) )', 0, 0, 'ferc_plant_account', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 192, sysdate, user, 'FERC Plant Account.Long Description', 'The passed in value corresponds to the FERC Plant Account: Long Description field.  Translate to the FERC Plant Account ID using the Long Description column on the FERC Plant Account table.', 'ferc_plt_acct_id', '( select fp.ferc_plt_acct_id from ferc_plant_account fp where upper( trim( <importfield> ) ) = upper( trim( fp.long_description ) ) )', 0, 0, 'ferc_plant_account', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 190, sysdate, user, 'Functional Class.Description', 'The passed in value corresponds to the Functional Class: Description field.  Translate to the Functional Class ID using the Description column on the Functional Class table.', 'func_class_id', '( select fc.func_class_id from func_class fc where upper( trim( <importfield> ) ) = upper( trim( fc.description ) ) )', 0, 0, 'func_class', 'description', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 'bus_segment_id', 25, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 'bus_segment_id', 26, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 'ferc_plt_acct_id', 191, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 'ferc_plt_acct_id', 192, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 'func_class_id', 190, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 'status_code_id', 138, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 'status_code_id', 139, sysdate, user );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 28, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 24, 30, sysdate, user );

create table PWRPLANT.PT_IMPORT_UTIL_ACCOUNT
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 BUS_SEGMENT_XLATE     varchar2(254),
 BUS_SEGMENT_ID        number(22,0),
 FERC_PLT_ACCT_XLATE   varchar2(254),
 FERC_PLT_ACCT_ID      number(22,0),
 FUNC_CLASS_XLATE      varchar2(254),
 FUNC_CLASS_ID         number(22,0),
 DESCRIPTION           varchar2(254),
 STATUS_CODE_XLATE     varchar2(254),
 STATUS_CODE_ID        number(22,0),
 EXTERNAL_ACCOUNT_CODE varchar2(254),
 UTILITY_ACCOUNT_XLATE varchar2(254),
 UTILITY_ACCOUNT_ID    number(22,0),
 IS_MODIFIED           number(22,0),
 ERROR_MESSAGE         varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_UTIL_ACCOUNT
   add constraint PT_IMPORT_UA_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_UTIL_ACCOUNT
   add constraint PT_IMPORT_UA_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_UTIL_ACCOUNT_ARCHIVE
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 BUS_SEGMENT_XLATE     varchar2(254),
 BUS_SEGMENT_ID        number(22,0),
 FERC_PLT_ACCT_XLATE   varchar2(254),
 FERC_PLT_ACCT_ID      number(22,0),
 FUNC_CLASS_XLATE      varchar2(254),
 FUNC_CLASS_ID         number(22,0),
 DESCRIPTION           varchar2(254),
 STATUS_CODE_XLATE     varchar2(254),
 STATUS_CODE_ID        number(22,0),
 EXTERNAL_ACCOUNT_CODE varchar2(254),
 UTILITY_ACCOUNT_XLATE varchar2(254),
 UTILITY_ACCOUNT_ID    number(22,0)
);

alter table PWRPLANT.PT_IMPORT_UTIL_ACCOUNT_ARCHIVE
   add constraint PT_IMPORT_UA_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_UTIL_ACCOUNT_ARCHIVE
   add constraint PT_IMPORT_UA_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

--
-- Create an import for retirement units.
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 23, sysdate, user, 'Add : Retirement Units', 'Import Retirement Units', 'pt_import_retire_unit', 'pt_import_retire_unit_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 23, 'property_unit_id', sysdate, user, 'Property Unit', 'property_unit_xlate', 1, 1, 'number(22,0)', 'property_unit', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 23, 'external_retire_unit', sysdate, user, 'External Retire Unit', '', 0, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 23, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 23, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 23, 'status_code_id', sysdate, user, 'Status Code', 'status_code_xlate', 1, 1, 'number(22,0)', 'status_code', 1, '', 0, '' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 193, sysdate, user, 'Property Unit.Description', 'The passed in value corresponds to the Property Unit: Description field.  Translate to the Property Unit ID using the Description column on the Property Unit table.', 'property_unit_id', '( select pu.property_unit_id from property_unit pu where upper( trim( <importfield> ) ) = upper( trim( pu.description ) ) )', 0, 0, 'property_unit', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 194, sysdate, user, 'Property Unit.Long Description', 'The passed in value corresponds to the Property Unit: Long Description field.  Translate to the Property Unit ID using the Long Description column on the Property Unit table.', 'property_unit_id', '( select pu.property_unit_id from property_unit pu where upper( trim( <importfield> ) ) = upper( trim( pu.long_description ) ) )', 0, 0, 'property_unit', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 195, sysdate, user, 'Property Unit.External Prop Unit', 'The passed in value corresponds to the Property Unit: External Prop Unit field.  Translate to the Property Unit ID using the External Prop Unit column on the Property Unit table.', 'property_unit_id', '( select pu.property_unit_id from property_unit pu where upper( trim( <importfield> ) ) = upper( trim( pu.external_prop_unit ) ) )', 0, 0, 'property_unit', 'external_prop_unit', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 196, sysdate, user, 'Retirement Unit.Description', 'The passed in value corresponds to the Retirement Unit: Description field.  Translate to the Retirement Unit ID using the Description column on the Retirement Unit table.', 'retirement_unit_id', '( select ru.retirement_unit_id from retirement_unit ru where upper( trim( <importfield> ) ) = upper( trim( ru.description ) ) )', 0, 0, 'retirement_unit', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 197, sysdate, user, 'Retirement Unit.Long Description', 'The passed in value corresponds to the Retirement Unit: Long Description field.  Translate to the Retirement Unit ID using the Long Description column on the Retirement Unit table.', 'retirement_unit_id', '( select ru.retirement_unit_id from retirement_unit ru where upper( trim( <importfield> ) ) = upper( trim( ru.long_description ) ) )', 0, 0, 'retirement_unit', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 198, sysdate, user, 'Retirement Unit.External Retire Unit', 'The passed in value corresponds to the Retirement Unit: External Retire Unit field.  Translate to the Retirement Unit ID using the External Retire Unit column on the Retirement Unit table.', 'retirement_unit_id', '( select ru.retirement_unit_id from retirement_unit ru where upper( trim( <importfield> ) ) = upper( trim( ru.external_retire_unit ) ) )', 0, 0, 'retirement_unit', 'external_retire_unit', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 'property_unit_id', 193, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 'property_unit_id', 194, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 'property_unit_id', 195, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 'status_code_id', 138, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 'status_code_id', 139, sysdate, user );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 196, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 197, sysdate, user );
insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 23, 198, sysdate, user );

create table PWRPLANT.PT_IMPORT_RETIRE_UNIT
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(254),
 LONG_DESCRIPTION      varchar2(254),
 PROPERTY_UNIT_XLATE   varchar2(254),
 PROPERTY_UNIT_ID      number(22,0),
 STATUS_CODE_XLATE     varchar2(254),
 STATUS_CODE_ID        number(22,0),
 EXTERNAL_RETIRE_UNIT  varchar2(254),
 RETIREMENT_UNIT_XLATE varchar2(254),
 RETIREMENT_UNIT_ID    number(22,0),
 IS_MODIFIED           number(22,0),
 ERROR_MESSAGE         varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_RETIRE_UNIT
   add constraint PT_IMPORT_RU_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_RETIRE_UNIT
   add constraint PT_IMPORT_RU_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_RETIRE_UNIT_ARCHIVE
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(254),
 LONG_DESCRIPTION      varchar2(254),
 PROPERTY_UNIT_XLATE   varchar2(254),
 PROPERTY_UNIT_ID      number(22,0),
 STATUS_CODE_XLATE     varchar2(254),
 STATUS_CODE_ID        number(22,0),
 EXTERNAL_RETIRE_UNIT  varchar2(254),
 RETIREMENT_UNIT_XLATE varchar2(254),
 RETIREMENT_UNIT_ID    number(22,0)
);

alter table PWRPLANT.PT_IMPORT_RETIRE_UNIT_ARCHIVE
   add constraint PT_IMPORT_RU_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_RETIRE_UNIT_ARCHIVE
   add constraint PT_IMPORT_RU_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

--
-- Create an import for assets.
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 25, sysdate, user, 'Add : Assets', 'Import Assets - For Asset Shell Clients', 'pt_import_assets', 'pt_import_assets_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'retirement_unit_id', sysdate, user, 'Retirement Unit', 'retirement_unit_xlate', 1, 1, 'number(22,0)', 'retirement_unit', 1, '', 1, 'pt_import_retire_unit' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 1, 1, 'number(22,0)', 'business_segment', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'utility_account_id', sysdate, user, 'Utility Account', 'utility_account_xlate', 1, 2, 'number(22,0)', 'utility_account', 1, 'bus_segment_id', 1, 'pt_import_util_account' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'gl_account_id', sysdate, user, 'GL Account', 'gl_account_xlate', 1, 1, 'number(22,0)', 'gl_account', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'asset_location_id', sysdate, user, 'Asset Location', 'asset_location_xlate', 1, 2, 'number(22,0)', 'asset_location', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'work_order_number', sysdate, user, 'Work Order Number', '', 0, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'in_service_year', sysdate, user, 'In Service Year', '', 1, 1, 'date', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'quantity', sysdate, user, 'Quantity', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'cost', sysdate, user, 'Cost', '', 0, 1, 'number(22,2)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'eng_in_service_year', sysdate, user, 'Engineering In Service  Year', '', 1, 1, 'date', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'serial_number', sysdate, user, 'Serial Number', '', 1, 1, 'varchar2(35)', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'disposal_date', sysdate, user, 'Disposal Date', '', 0, 1, 'date', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'posting_date', sysdate, user, 'Activity Date', '', 1, 1, 'date', '', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'ferc_plt_acct_id', sysdate, user, 'Util Acct Ferc Plant Account', 'ferc_plt_acct_xlate', 0, 1, 'number(22,0)', 'ferc_plant_account', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'func_class_id', sysdate, user, 'Util Acct Functional Class', 'func_class_xlate', 0, 1, 'number(22,0)', 'func_class', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'ua_description', sysdate, user, 'Util Acct Description', '', 0, 1, 'varchar2(35)', '', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'status_code_id', sysdate, user, 'Status Code', 'status_code_xlate', 0, 1, 'number(22,0)', 'status_code', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'ua_external_account_code', sysdate, user, 'Util Acct External Account Code', '', 0, 1, 'varchar2(35)', '', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'property_unit_id', sysdate, user, 'Ret Unit Property Unit', 'property_unit_xlate', 0, 1, 'number(22,0)', 'property_unit', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'ru_external_retire_unit', sysdate, user, 'Ret Unit External Retire Unit', '', 0, 1, 'varchar2(35)', '', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'ru_description', sysdate, user, 'Ret Unit Description', '', 0, 1, 'varchar2(35)', '', 0, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 25, 'ru_long_description', sysdate, user, 'Ret Unit Long Description', '', 0, 1, 'varchar2(254)', '', 0, '', 0, '' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 187, sysdate, user, 'CPR Ledger.Description', 'The passed in value corresponds to the CPR Ledger: Description field.  Translate to the Asset ID using the Description column on the CPR Ledger table.', 'asset_id', '( select cpr.asset_id from cpr_ledger cpr where upper( trim( <importfield> ) ) = upper( trim( cpr.description ) ) )', 0, 0, 'cpr_ledger', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 188, sysdate, user, 'CPR Ledger.Long Description', 'The passed in value corresponds to the CPR Ledger: Long Description field.  Translate to the Asset ID using the Long Description column on the CPR Ledger table.', 'asset_id', '( select cpr.asset_id from cpr_ledger cpr where upper( trim( <importfield> ) ) = upper( trim( cpr.long_description ) ) )', 0, 0, 'cpr_ledger', 'long_description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 189, sysdate, user, 'CPR Ledger.Serial Number', 'The passed in value corresponds to the CPR Ledger: Serial Number field.  Translate to the Asset ID using the Serial Number column on the CPR Ledger table.', 'asset_id', '( select cpr.asset_id from cpr_ledger cpr where upper( trim( <importfield> ) ) = upper( trim( cpr.serial_number ) ) )', 0, 0, 'cpr_ledger', 'serial_number', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'retirement_unit_id', 196, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'retirement_unit_id', 197, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'retirement_unit_id', 198, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'bus_segment_id', 25, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'bus_segment_id', 26, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'company_id', 19, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'company_id', 21, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'company_id', 20, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'utility_account_id', 28, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'utility_account_id', 30, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'gl_account_id', 22, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'gl_account_id', 24, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'gl_account_id', 23, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 93, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 99, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 89, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 95, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 90, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 96, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 91, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 97, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 92, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 98, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 88, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'asset_location_id', 94, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'state_id', 1, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'state_id', 2, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'ferc_plt_acct_id', 191, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'ferc_plt_acct_id', 192, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'func_class_id', 190, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'status_code_id', 138, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'status_code_id', 139, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'property_unit_id', 193, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'property_unit_id', 194, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 'property_unit_id', 195, sysdate, user );

insert into PWRPLANT.PT_IMPORT_TYPE_UPDATES_LOOKUP ( IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 25, 189, sysdate, user );

create table PWRPLANT.PT_IMPORT_ASSETS
(
 IMPORT_RUN_ID                 number(22,0)   not null,
 LINE_ID                       number(22,0)   not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 COMPANY_XLATE                 varchar2(254),
 COMPANY_ID                    number(22,0),
 GL_ACCOUNT_XLATE              varchar2(254),
 GL_ACCOUNT_ID                 number(22,0),
 BUS_SEGMENT_XLATE             varchar2(254),
 BUS_SEGMENT_ID                number(22,0),
 UTILITY_ACCOUNT_XLATE         varchar2(254),
 UTILITY_ACCOUNT_ID            number(22,0),
 RETIREMENT_UNIT_XLATE         varchar2(254),
 RETIREMENT_UNIT_ID            number(22,0),
 STATE_XLATE                   varchar2(254),
 STATE_ID                      char(18),
 ASSET_LOCATION_XLATE          varchar2(254),
 ASSET_LOCATION_ID             number(22,0),
 DESCRIPTION                   varchar2(254),
 LONG_DESCRIPTION              varchar2(254),
 SERIAL_NUMBER                 varchar2(254),
 WORK_ORDER_NUMBER             varchar2(254),
 IN_SERVICE_YEAR               varchar2(254),
 ENG_IN_SERVICE_YEAR           varchar2(254),
 QUANTITY                      varchar2(254),
 COST                          varchar2(254),
 DISPOSAL_DATE                 varchar2(254),
 POSTING_DATE                  varchar2(254),
 ASSET_ID                      number(22,0),
 PROPERTY_GROUP_ID             number(22,0),
 DEPR_GROUP_ID                 number(22,0),
 BOOKS_SCHEMA_ID               number(22,0),
 SUB_ACCOUNT_ID                number(22,0),
 LEDGER_STATUS                 number(22,0),
 SUBLEDGER_INDICATOR           number(22,0),
 ACT_ASSET_ACTIVITY_ID         number(22,0),
 ACT_GL_POSTING_MO_YR          date,
 ACT_CPR_POSTING_MO_YR         date,
 ACT_GL_JE_CODE                char(18),
 ACT_DESCRIPTION               varchar2(35),
 ACT_LONG_DESCRIPTION          varchar2(254),
 ACT_ACTIVITY_CODE             char(8),
 ACT_ACTIVITY_STATUS           number(22,0),
 ACT_ACTIVITY_QUANTITY         number(22,2),
 ACT_ACTIVITY_COST             number(22,2),
 ACT_FERC_ACTIVITY_CODE        number(22,0),
 ACT_MONTH_NUMBER              number(22,0),
 TRANS_FROM_ASSET_ID           number(22,0),
 TRANS_TO_ASSET_ID             number(22,0),
 TRANS_FROM_ASSET_ACTIVITY_ID  number(22,0),
 TRANS_TO_ASSET_ACTIVITY_ID1   number(22,0),
 TRANS_TO_ASSET_ACTIVITY_ID2   number(22,0),
 TRANS_GL_POSTING_MO_YR        date,
 TRANS_CPR_POSTING_MO_YR       date,
 TRANS_GL_JE_CODE              char(18),
 TRANS_DESCRIPTION             varchar2(35),
 TRANS_LONG_DESCRIPTION        varchar2(254),
 TRANS_TO_ACTIVITY_CODE1       char(8),
 TRANS_TO_ACTIVITY_CODE2       char(8),
 TRANS_FROM_ACTIVITY_CODE      char(8),
 TRANS_ACTIVITY_STATUS         number(22,0),
 TRANS_TO_ACTIVITY_QUANTITY1   number(22,2),
 TRANS_TO_ACTIVITY_COST1       number(22,2),
 TRANS_TO_ACTIVITY_QUANTITY2   number(22,2),
 TRANS_TO_ACTIVITY_COST2       number(22,2),
 TRANS_FROM_ACTIVITY_QUANTITY  number(22,2),
 TRANS_FROM_ACTIVITY_COST      number(22,2),
 TRANS_TO_FERC_ACTIVITY_CODE1  number(22,0),
 TRANS_TO_FERC_ACTIVITY_CODE2  number(22,0),
 TRANS_FROM_FERC_ACTIVITY_CODE number(22,0),
 TRANS_MONTH_NUMBER            number(22,0),
 DISP_ASSET_ACTIVITY_ID        number(22,0),
 DISP_GL_POSTING_MO_YR         date,
 DISP_CPR_POSTING_MO_YR        date,
 DISP_OUT_OF_SERVICE_MO_YR     date,
 DISP_GL_JE_CODE               char(18),
 DISP_DESCRIPTION              varchar2(35),
 DISP_LONG_DESCRIPTION         varchar2(254),
 DISP_DISPOSITION_CODE         number(22,0),
 DISP_ACTIVITY_CODE            char(8),
 DISP_ACTIVITY_STATUS          number(22,0),
 DISP_ACTIVITY_QUANTITY        number(22,2),
 DISP_ACTIVITY_COST            number(22,2),
 DISP_FERC_ACTIVITY_CODE       number(22,0),
 DISP_MONTH_NUMBER             number(22,0),
 COMPUTE_ACT_STATUS            number(22,0),
 FERC_PLT_ACCT_XLATE           varchar2(254),
 FERC_PLT_ACCT_ID              number(22,0),
 FUNC_CLASS_XLATE              varchar2(254),
 FUNC_CLASS_ID                 number(22,0),
 UA_DESCRIPTION                varchar2(254),
 STATUS_CODE_XLATE             varchar2(254),
 STATUS_CODE_ID                number(22,0),
 UA_EXTERNAL_ACCOUNT_CODE      varchar2(254),
 PROPERTY_UNIT_XLATE           varchar2(254),
 PROPERTY_UNIT_ID              number(22,0),
 RU_DESCRIPTION                varchar2(254),
 RU_LONG_DESCRIPTION           varchar2(254),
 RU_EXTERNAL_RETIRE_UNIT       varchar2(254),
 UA_LINE_ID                    number(22,0),
 RU_LINE_ID                    number(22,0),
 LOADED                        number(22,0),
 IS_MODIFIED                   number(22,0),
 ERROR_MESSAGE                 varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_ASSETS
   add constraint PT_IMPORT_ASSETS_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_ASSETS
   add constraint PT_IMPORT_ASSETS_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE
(
 IMPORT_RUN_ID                 number(22,0)   not null,
 LINE_ID                       number(22,0)   not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 COMPANY_XLATE                 varchar2(254),
 COMPANY_ID                    number(22,0),
 GL_ACCOUNT_XLATE              varchar2(254),
 GL_ACCOUNT_ID                 number(22,0),
 BUS_SEGMENT_XLATE             varchar2(254),
 BUS_SEGMENT_ID                number(22,0),
 UTILITY_ACCOUNT_XLATE         varchar2(254),
 UTILITY_ACCOUNT_ID            number(22,0),
 RETIREMENT_UNIT_XLATE         varchar2(254),
 RETIREMENT_UNIT_ID            number(22,0),
 STATE_XLATE                   varchar2(254),
 STATE_ID                      char(18),
 ASSET_LOCATION_XLATE          varchar2(254),
 ASSET_LOCATION_ID             number(22,0),
 DESCRIPTION                   varchar2(254),
 LONG_DESCRIPTION              varchar2(254),
 SERIAL_NUMBER                 varchar2(254),
 WORK_ORDER_NUMBER             varchar2(254),
 IN_SERVICE_YEAR               varchar2(254),
 ENG_IN_SERVICE_YEAR           varchar2(254),
 QUANTITY                      varchar2(254),
 COST                          varchar2(254),
 DISPOSAL_DATE                 varchar2(254),
 POSTING_DATE                  varchar2(254),
 ASSET_ID                      number(22,0),
 PROPERTY_GROUP_ID             number(22,0),
 DEPR_GROUP_ID                 number(22,0),
 BOOKS_SCHEMA_ID               number(22,0),
 SUB_ACCOUNT_ID                number(22,0),
 LEDGER_STATUS                 number(22,0),
 SUBLEDGER_INDICATOR           number(22,0),
 ACT_ASSET_ACTIVITY_ID         number(22,0),
 ACT_GL_POSTING_MO_YR          date,
 ACT_CPR_POSTING_MO_YR         date,
 ACT_GL_JE_CODE                char(18),
 ACT_DESCRIPTION               varchar2(35),
 ACT_LONG_DESCRIPTION          varchar2(254),
 ACT_ACTIVITY_CODE             char(8),
 ACT_ACTIVITY_STATUS           number(22,0),
 ACT_ACTIVITY_QUANTITY         number(22,2),
 ACT_ACTIVITY_COST             number(22,2),
 ACT_FERC_ACTIVITY_CODE        number(22,0),
 ACT_MONTH_NUMBER              number(22,0),
 TRANS_FROM_ASSET_ID           number(22,0),
 TRANS_TO_ASSET_ID             number(22,0),
 TRANS_FROM_ASSET_ACTIVITY_ID  number(22,0),
 TRANS_TO_ASSET_ACTIVITY_ID1   number(22,0),
 TRANS_TO_ASSET_ACTIVITY_ID2   number(22,0),
 TRANS_GL_POSTING_MO_YR        date,
 TRANS_CPR_POSTING_MO_YR       date,
 TRANS_GL_JE_CODE              char(18),
 TRANS_DESCRIPTION             varchar2(35),
 TRANS_LONG_DESCRIPTION        varchar2(254),
 TRANS_TO_ACTIVITY_CODE1       char(8),
 TRANS_TO_ACTIVITY_CODE2       char(8),
 TRANS_FROM_ACTIVITY_CODE      char(8),
 TRANS_ACTIVITY_STATUS         number(22,0),
 TRANS_TO_ACTIVITY_QUANTITY1   number(22,2),
 TRANS_TO_ACTIVITY_COST1       number(22,2),
 TRANS_TO_ACTIVITY_QUANTITY2   number(22,2),
 TRANS_TO_ACTIVITY_COST2       number(22,2),
 TRANS_FROM_ACTIVITY_QUANTITY  number(22,2),
 TRANS_FROM_ACTIVITY_COST      number(22,2),
 TRANS_TO_FERC_ACTIVITY_CODE1  number(22,0),
 TRANS_TO_FERC_ACTIVITY_CODE2  number(22,0),
 TRANS_FROM_FERC_ACTIVITY_CODE number(22,0),
 TRANS_MONTH_NUMBER            number(22,0),
 DISP_ASSET_ACTIVITY_ID        number(22,0),
 DISP_GL_POSTING_MO_YR         date,
 DISP_CPR_POSTING_MO_YR        date,
 DISP_OUT_OF_SERVICE_MO_YR     date,
 DISP_GL_JE_CODE               char(18),
 DISP_DESCRIPTION              varchar2(35),
 DISP_LONG_DESCRIPTION         varchar2(254),
 DISP_DISPOSITION_CODE         number(22,0),
 DISP_ACTIVITY_CODE            char(8),
 DISP_ACTIVITY_STATUS          number(22,0),
 DISP_ACTIVITY_QUANTITY        number(22,2),
 DISP_ACTIVITY_COST            number(22,2),
 DISP_FERC_ACTIVITY_CODE       number(22,0),
 DISP_MONTH_NUMBER             number(22,0),
 COMPUTE_ACT_STATUS            number(22,0),
 FERC_PLT_ACCT_XLATE           varchar2(254),
 FERC_PLT_ACCT_ID              number(22,0),
 FUNC_CLASS_XLATE              varchar2(254),
 FUNC_CLASS_ID                 number(22,0),
 UA_DESCRIPTION                varchar2(254),
 STATUS_CODE_XLATE             varchar2(254),
 STATUS_CODE_ID                number(22,0),
 UA_EXTERNAL_ACCOUNT_CODE      varchar2(254),
 PROPERTY_UNIT_XLATE           varchar2(254),
 PROPERTY_UNIT_ID              number(22,0),
 RU_DESCRIPTION                varchar2(254),
 RU_LONG_DESCRIPTION           varchar2(254),
 RU_EXTERNAL_RETIRE_UNIT       varchar2(254),
 UA_LINE_ID                    number(22,0),
 RU_LINE_ID                    number(22,0),
 LOADED                        number(22,0)
);

alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE
   add constraint PT_IMPORT_ASSETS_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_ASSETS_ARCHIVE
   add constraint PT_IMPORT_ASSETS_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

--
-- Create an import for class codes.
--
insert into PWRPLANT.PT_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, ASSET_SYSTEM_DATA ) values ( 27, sysdate, user, 'Add : Class Codes', 'Import Class Code Assignments for Assets', 'pt_import_class_code', 'pt_import_class_code_archive', null, 1, 1 );

insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 27, 'class_code_id', sysdate, user, 'Class Code', 'class_code_xlate', 1, 1, 'number(22,0)', 'class_code', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 27, 'asset_id', sysdate, user, 'Asset', 'asset_xlate', 1, 1, 'number(22,0)', 'cpr_ledger', 1, '', 0, '' );
insert into PWRPLANT.PT_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN2, ALLOW_AUTOCREATE, AUTOCREATE_TABLE_NAME ) values ( 27, 'value', sysdate, user, 'Class Code Value', '', 1, 1, 'varchar2(254)', '', 1, '', 0, '' );

insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 199, sysdate, user, 'Class Code.Description', 'The passed in value corresponds to the Class Code: Description field.  Translate to the Class Code ID using the Description column on the Class Code table.', 'class_code_id', '( select cc.class_code_id from class_code cc where upper( trim( <importfield> ) ) = upper( trim( cc.description ) ) )', 0, 0, 'class_code', 'description', '' );
insert into PWRPLANT.PT_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, IS_SUBSTRINGED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS ) values ( 200, sysdate, user, 'Class Code.Long Description', 'The passed in value corresponds to the Class Code: Long Description field.  Translate to the Class Code ID using the Long Description column on the Class Code table.', 'class_code_id', '( select cc.class_code_id from class_code cc where upper( trim( <importfield> ) ) = upper( trim( cc.long_description ) ) )', 0, 0, 'class_code', 'long_description', '' );

insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 27, 'class_code_id', 199, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 27, 'class_code_id', 200, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 27, 'asset_id', 187, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 27, 'asset_id', 188, sysdate, user );
insert into PWRPLANT.PT_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 27, 'asset_id', 189, sysdate, user );

create table PWRPLANT.PT_IMPORT_CLASS_CODE
(
 IMPORT_RUN_ID    number(22,0)   not null,
 LINE_ID          number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 CLASS_CODE_XLATE varchar2(254),
 CLASS_CODE_ID    number(22,0),
 ASSET_XLATE      varchar2(254),
 ASSET_ID         number(22,0),
 VALUE            varchar2(254),
 IS_MODIFIED      number(22,0),
 ERROR_MESSAGE    varchar2(4000)
);

alter table PWRPLANT.PT_IMPORT_CLASS_CODE
   add constraint PT_IMPORT_CC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_CLASS_CODE
   add constraint PT_IMPORT_CC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

create table PWRPLANT.PT_IMPORT_CLASS_CODE_ARCHIVE
(
 IMPORT_RUN_ID    number(22,0)   not null,
 LINE_ID          number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 CLASS_CODE_XLATE varchar2(254),
 CLASS_CODE_ID    number(22,0),
 ASSET_XLATE      varchar2(254),
 ASSET_ID         number(22,0),
 VALUE            varchar2(254)
);

alter table PWRPLANT.PT_IMPORT_CLASS_CODE_ARCHIVE
   add constraint PT_IMPORT_CC_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.PT_IMPORT_CLASS_CODE_ARCHIVE
   add constraint PT_IMPORT_CC_ARC_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PWRPLANT.PT_IMPORT_RUN;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (184, 0, 10, 3, 5, 0, 10012, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010012_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
