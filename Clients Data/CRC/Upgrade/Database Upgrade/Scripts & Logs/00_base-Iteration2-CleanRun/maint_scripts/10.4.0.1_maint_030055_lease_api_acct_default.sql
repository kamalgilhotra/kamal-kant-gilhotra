/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030055_lease_api_acct_default.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.1   06/4/2013  Travis Nemes   Patch Release
||============================================================================
*/

alter table LS_API_ASSET add ACCOUNTING_DEFAULT_ID number(22,0) null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (395, 0, 10, 4, 0, 1, 30055, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.1_maint_030055_lease_api_acct_default.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
