/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051688_lessor_04_update_pay_term_type_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2018.1.0.0  09/17/2018 Jared Watkins    Update the payment term type to include the remeasurement date
||                                           so we can use it to filter the months considered during schedule build
||============================================================================
*/

DROP TYPE LSR_ILR_OP_SCH_PAY_TERM_TAB
/

CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_PAY_TERM AS OBJECT( payment_month_frequency NUMBER,
                                                          payment_term_start_date date,
                                                          number_of_terms NUMBER,
                                                          payment_amount NUMBER,
                                                          executory_buckets lsr_bucket_amount_tab,
                                                          contingent_buckets lsr_bucket_amount_tab,
                                                          is_prepay number(1,0),
                                                          remeasurement_date date)
/
                                                                           
CREATE OR REPLACE  TYPE "PWRPLANT"."LSR_ILR_OP_SCH_PAY_TERM_TAB" as table of lsr_ilr_op_sch_pay_term
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9782, 0, 2018, 1, 0, 0, 51688, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_051688_lessor_04_update_pay_term_type_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;