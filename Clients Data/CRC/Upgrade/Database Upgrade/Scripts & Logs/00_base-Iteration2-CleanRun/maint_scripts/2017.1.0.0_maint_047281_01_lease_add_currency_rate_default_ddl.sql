/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_01_lease_add_currency_rate_default_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/21/2017 Jared Watkins    add table to store all of the exchange rates we will
||                                        need for multicurrency, including default 0/1 rates
||============================================================================
*/

CREATE TABLE currency_rate_default (
  exchange_date         DATE         NOT NULL,
  currency_from         NUMBER(22,0) NOT NULL,
  currency_to           NUMBER(22,0) NOT NULL,
  exchange_rate_type_id NUMBER(22,0) NOT NULL,
  time_stamp            DATE         NULL,
  user_id               VARCHAR2(18) NULL,
  rate                  NUMBER(22,8) NOT NULL
);

ALTER TABLE currency_rate_default 
ADD CONSTRAINT currency_rate_default_pk 
PRIMARY KEY (exchange_date, currency_from, currency_to, exchange_rate_type_id)
USING INDEX TABLESPACE pwrplant_idx
;

ALTER TABLE currency_rate_default
ADD CONSTRAINT currfromdefaultfk 
FOREIGN KEY (currency_from) 
REFERENCES currency (currency_id)
;

ALTER TABLE currency_rate_default
ADD CONSTRAINT currtodefaultfk 
FOREIGN KEY (currency_to) 
REFERENCES currency (currency_id)
;

ALTER TABLE currency_rate_default
ADD CONSTRAINT exchratetypedefaultfk 
FOREIGN KEY (exchange_rate_type_id) 
REFERENCES currency_rate_type (exchange_rate_type_id)
;

CREATE INDEX currency_rate_default_idx1 
ON currency_rate_default
(exchange_date, currency_from, currency_to)
;

comment on table currency_rate_default is '(S) [11] This table maintains the exchange rates used in Lease Multicurrency between different currencies used by the enterprise. It includes any relevant exchange rates from the CURRENCY_RATE table, as well as default rates of 0/1 depending on if you are translating between different or the same currencies.';
comment on column currency_rate_default.exchange_date IS 'Date of the exchange rate.  (The system will use the most recent date specified within the month if the exact date does not match.)';
comment on column currency_rate_default.currency_from IS 'Currency being translated from. FK to the CURRENCY table.';
comment on column currency_rate_default.currency_to IS 'Currency being translated to. FK to the CURRENCY table.';
comment on column currency_rate_default.exchange_rate_type_id IS 'System-assigned identifier of a particular exchange rate type. Should always be 1 in this table.';
comment on column currency_rate_default.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
comment on column currency_rate_default.user_id IS 'Standard system-assigned user id used for audit purposes.';
comment on column currency_rate_default.rate IS 'Exchange rate between the currency_from and currency_to for the given date on this row.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3442, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_01_lease_add_currency_rate_default_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
