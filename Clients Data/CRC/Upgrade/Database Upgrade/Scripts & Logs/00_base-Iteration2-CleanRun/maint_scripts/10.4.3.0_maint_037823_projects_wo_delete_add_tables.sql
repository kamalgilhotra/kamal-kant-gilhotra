/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037823_projects_wo_delete_add_tables.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 04/28/2014 Shane "C" Ward WO Tables to Delete From updated
||========================================================================================
*/

insert into WO_DELETE
   (ID, SORT_ORDER, EXCEPTION_FLAG, DESCRIPTION, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
values
   ((select max(ID) + 1 from WO_DELETE), 4550, 0, 'WO ENGINE ESTIMATE STATUS', -1, 'WO_ENG_ESTIMATE_STATUS',
    'where work_order_id = <<wo_id>>', null);

insert into WO_DELETE
   (ID, SORT_ORDER, EXCEPTION_FLAG, DESCRIPTION, FUNDING_WO_INDICATOR, TABLE_NAME, WHERE_CLAUSE, EXCEPTION_MESSAGE)
values
   ((select max(ID) + 1 from WO_DELETE), 4560, 0, 'WO ENGINE ESTIMATEs', -1, 'WO_ENG_ESTIMATE',
    'where work_order_id = <<wo_id>>', null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1121, 0, 10, 4, 3, 0, 37823, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037823_projects_wo_delete_add_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;