/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039953_aro_process_name.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/16/2014 Ryan Oliveria   Change name of executable
||============================================================================
*/

update PP_PROCESSES
   set EXECUTABLE_FILE = 'ssp_aro_calc.exe'
 where EXECUTABLE_FILE = 'aro_calc.exe';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1427, 0, 10, 4, 3, 0, 39953, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039953_aro_process_name.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;