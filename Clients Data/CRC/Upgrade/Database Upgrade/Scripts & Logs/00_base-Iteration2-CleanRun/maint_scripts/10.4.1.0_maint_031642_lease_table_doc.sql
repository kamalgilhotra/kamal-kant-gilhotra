SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_031642_lease_table_doc.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/08/2013 B.Beck         Original Version
||============================================================================
*/

comment on table depr_calc_stg is '(T) [01] The Depr Calc STG table is a global temporary table that is used for calculating depreciation.';
comment on table cpr_depr_calc_stg is '(T) [01] The CPR Depr Calc STG is a global temporary table that is used for calculating individually depreciated assets';

comment on table LS_ACCRUAL_TYPE is '(F) [06] The ls accrual type table holds a fixed list representing the different types of Accruals calculated in the Lease Module.  It is also used during Month End and Journal Entry creation.';
comment on table LS_ASSET is '(S) [06] The ls asset table holds leased assets in the lease module.';
comment on table LS_ASSET_CLASS_CODE is '(S) [06] The ls asset class code table is is a class code table for leased assets.';
comment on table LS_ASSET_DOCUMENT is '(S) [06] The ls asset document table holds documents associated with the leased assets (PDFs, DOC, XLS, etc).';
comment on table LS_ASSET_SCHEDULE is '(C) [06] The ls asset schedule table is holds the interest, principal, executory, and contingent amounts accrued and paid by period by set of books for a leased asset.';
comment on table LS_ASSET_STATUS is '(F) [06] The ls asset status table is the list of valid leased asset statuses.';
comment on table LS_CANCELABLE_TYPE is '(F) [06] The ls cancelable type table is a list of the valid cancellable options for a lease.';
comment on table LS_CPR_ASSET_MAP is '(C) [06] The ls cpr asset map table holds the mapping between a Leased Asset and the CPR asset.';
comment on table LS_EXTENDED_RENTAL_TYPE is '(F) [06] The ls extended rental type table is holds a list of options for extended rental agreements.';
comment on table LS_ILR is '(S) [06] The ls ilr table holds the ilrs in lease.  It is the header information.';
comment on table LS_ILR_AMOUNTS_SET_OF_BOOKS is '(C) [06] The ls ilr amounts set of books table holds amoutns by set of book for a given revision and ILR combination.  It is populated by the schedule building process.';
comment on table LS_ILR_APPROVAL is '(C) [06] The ls ilr approval table maintains the status of ILR approvals by revision.';
comment on table LS_ILR_ASSET_MAP is '(C) [06] The ls ilr asset map table holds the list of leased assets that are included in an ILR revision.';
comment on table LS_ILR_ASSET_SCHEDULE_CALC_STG is '(C) [06] The ls ilr asset schedule calc stg table holds the schedule calculation results for an asset / revision.';
comment on table LS_ILR_ASSET_SCHEDULE_STG is '(T) [06] The ls ilr asset schedule stg table isa global temp table that builds the asset schedule based on payment temrs, options, etc from the ILR.';
comment on table LS_ILR_ASSET_STG is '(T) [06] The ls ilr asset stg table is a global temp table that holds the assets that need to have a shcedule built.';
comment on table LS_ILR_CLASS_CODE is '(S) [06] The ls ilr class code table is a class coded table for ILRs.';
comment on table LS_ILR_DOCUMENT is '(S) [06] The ls ilr document table is a table that holds documents for ILRs.';
comment on table LS_ILR_GROUP is '(S) [06] The ls ilr group table is a table that holds groups of ILRs. This allows reporting rollups for ILRs at different levels than MLA and company.';
comment on table LS_ILR_OPTIONS is '(S) [06] The ls ilr options table holds various options that affect the net present value of an ILR.  These include: Extended Rental Agreement, Bargain Purchase Amount, Termination Penalty, and Discount Rate.';
comment on table LS_ILR_PAYMENT_TERM is '(S) [06] The ls ilr payment term table holds the start date, amount of payment, and length of the term for an ILR revision.';
comment on table LS_ILR_SCHEDULE is '(C) [06] The ls ilr schedule table holds the monthly accruals and payments for an ILRs revision life.';
comment on table LS_ILR_SCHEDULE_STG is '(T) [06] The ls ilr schedule stg table is a global temp table that holds the ILR schedule information so the asset schedules can be built.';
comment on table LS_ILR_STATUS is '(F) [06] The ls ilr status table holds a list of statuses for an ILR.';
comment on table LS_ILR_STG is '(T) [06] The ls ilr stg table is  a global temp table that holds the ILRs that need the schedule built.';
comment on table LS_IMPORT_ASSET is '(S) [06] The ls import asset table is an API table used to import leased assets.';
comment on table LS_IMPORT_ASSET_ARCHIVE is '(S) [06] The ls import asset archive table holds records of previous leased asset imports.';
comment on table LS_IMPORT_ILR is '(S) [06] The ls import ilr table is an API table used to import ILRs.';
comment on table LS_IMPORT_ILR_ARCHIVE is '(S) [06] The ls import ilr archive table holds records of previous ILR imports.';
comment on table LS_IMPORT_INVOICE is '(S) [06] The ls import invoice table is an API table used to import Lease Invoices.';
comment on table LS_IMPORT_INVOICE_ARCHIVE is '(S) [06] The ls import invoice archive table holds records of previous invoice imports.';
comment on table LS_IMPORT_LEASE is '(S) [06] The ls import lease table is an API table used to import MLAs.';
comment on table LS_IMPORT_LEASE_ARCHIVE is '(S) [06] The ls import lease archive table holds records of previous MLA imports.';
comment on table LS_IMPORT_LESSOR is '(S) [06] The ls import lessor table is an API table used to import lessors.';
comment on table LS_IMPORT_LESSOR_ARCHIVE is '(S) [06] The ls import lessor archive table holds records of previous lessor imports.';
comment on table LS_INVOICE is '(S) [06] The ls invoice table holds lessor invoice records.';
comment on table LS_INVOICE_LINE is '(S) [06] The ls invoice line table contains line level information for a given invoice. Amounts on this table can be for an entire invoice or at the asset level.';
comment on table LS_INVOICE_PAYMENT_MAP is '(S) [06] The ls invoice payment map table maps invoices to calculated payments.';
comment on table LS_LEASE is '(S) [06] The ls lease table contains descriptive information for MLAs.';
comment on table LS_LEASE_APPROVAL is '(S) [06] The ls lease approval table holds the approval status for MLA revisions.';
comment on table LS_LEASE_CAP_TYPE is '(S) [06] The ls lease cap type table is a configuration table that holds data related to whether or not the lease is an O&M lease or a capital lease.  It holds the mapping to CPR basis buckets as well.';
comment on table LS_LEASE_COMPANY is '(S) [06] The ls lease company table determines which companies are valid for a given MLA.';
comment on table LS_LEASE_DOCUMENT is '(S) [06] The ls lease document table holds documents associated with the leased assets (PDFs, DOC, XLS, etc).';
comment on table LS_LEASE_GROUP is '(S) [06] The ls lease group table is a table that holds default processing for a Leases payments, and allows grouping leases together';
comment on table LS_LEASE_GROUP_USER is '(S) [06] The ls lease group user table is a security table that assigns users to a lease group.';
comment on table LS_LEASE_OPTIONS is '(S) [06] The ls lease options table holds different lease options by revision.';
comment on table LS_LEASE_STATUS is '(F) [06] The ls lease status table holds the eligible statuses for a lease.';
comment on table LS_LEASE_VENDOR is '(S) [06] The ls lease vendor table connects an MLA to a Vendor, and holds the payment percentage information for that relationship.';
comment on table LS_LESSOR is '(S) [06] The ls lessor table holds lessor information in the Lessee module.';
comment on table LS_MLA_CLASS_CODE is '(S) [06] The ls mla class code table allows additional information to be stored on the MLA.';
comment on table LS_MONTHLY_ACCRUAL_STG is '(S) [06] The ls monthly accrual stg table holds accrual amounts at an asset level for a given month and set of books.';
comment on table LS_PAYMENT_APPROVAL is '(S) [06] The ls payment approval table holds payment approval statuses.';
comment on table LS_PAYMENT_FREQ is '(S) [06] The ls payment freq table is a fixed table that maps payment_freq_ids to common lease payment frequencies.';
comment on table LS_PAYMENT_HDR is '(S) [06] The ls payment hdr table holds the total payment amount for a given month for a MLA, vendor, company combination.';
comment on table LS_PAYMENT_LINE is '(S) [06] The ls payment line table contains payment line level information. This can be at an asset level or for the entire payment.';
comment on table LS_PAYMENT_TERM_TYPE is '(S) [06] The ls payment term type table is a fixed table that maps payment_term_type_ids to common payment term types.';
comment on table LS_PAYMENT_TYPE is '(S) [06] The ls payment type table is a fixed table that maps payment_type_ids to payment types (principal, interest, etc).';
comment on table LS_PEND_BASIS is '(S) [06] The ls pend basis table is the pending Addition and Adjustment table by basis bucket for lease assets.';
comment on table LS_PEND_BASIS_ARC is '(S) [06] The ls pend basis arc table holds the historical posting by basis bucket for leased additions and adjustments.';
comment on table LS_PEND_CLASS_CODE is '(S) [06] The ls pend class code table stores the class codes that will be added to the CPR asset when the leased asset transaction is booked';
comment on table LS_PEND_CLASS_CODE_ARC is '(S) [06] The ls pend class code arc table is an archive table to hold historical values posted';
comment on table LS_PEND_SET_OF_BOOKS is '(S) [06] The ls pend set of books table holds the leased asset posting by set of books.';
comment on table LS_PEND_SET_OF_BOOKS_ARC is '(S) [06] The ls pend set of books arc table is an archive table to store histoircal postings by set of book.';
comment on table LS_PEND_TRANSACTION is '(S) [06] The ls pend transaction table is the primary table holding pending leased additions and adjustments.';
comment on table LS_PEND_TRANSACTION_ARC is '(S) [06] The ls pend transaction arc table is an archive table to hold histoircal leased asset additions and adjustments.';
comment on table LS_PROCESS_CONTROL is '(S) [06] The ls process control table contains information about completed month end processes at a company-by-company level..';
comment on table LS_PURCHASE_OPTION_TYPE is '(F) [06] The ls purchase option type table holds the list of purchase option types.';
comment on table LS_RENEWAL_OPTION_TYPE is '(F) [06] The ls renewal option type table stores the list of values for lease renewal eligiblity.';
comment on table LS_RENT_BUCKET_ADMIN is '(S) [06] The ls rent bucket admin table is a configuration table to configure up to 10 executory and contigent buckets.';
comment on table LS_RETIREMENT_NEW_TERMS is '(T) [06] The ls retirement new terms table is a temp table that stores the retirement new payment term until the retirement is booked.';
comment on table LS_VENDOR is '(S) [06] The ls vendor table holds vendor information in the Lessee module.  All vendors are related to one Lessor.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (594, 0, 10, 4, 1, 0, 31642, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031642_lease_table_doc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON