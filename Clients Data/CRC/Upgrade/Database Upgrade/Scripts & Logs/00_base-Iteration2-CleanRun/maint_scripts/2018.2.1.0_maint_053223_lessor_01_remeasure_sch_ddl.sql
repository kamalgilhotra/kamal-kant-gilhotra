/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053223_lessor_01_remeasure_sch_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 03/14/2019 B. Beck	    Add new columns for remeasurement calcultion for lessor ilrs
||============================================================================
*/

-- add new columns to lsr_ilr_schedule_sales_direct

alter table lsr_ilr_schedule_sales_direct add
(	
	beginning_remaining_payments number(22,2) default 0 not null,
	ending_remaining_payments number(22,2) default 0 not null,
	net_investment_remeasurement number(22,2) default 0 not null
);

alter table lsr_ilr_schedule_direct_fin add
(
	deferred_profit_remeasurement number(22,2) default 0 not null
);

comment on column lsr_ilr_schedule_sales_direct.beginning_remaining_payments is 'An amount field that specifies the beginning monthly remaining payment for the ILR.';
comment on column lsr_ilr_schedule_sales_direct.ending_remaining_payments is 'An amount field that specifies the ending monthly remaining payment for the ILR.';
comment on column lsr_ilr_schedule_sales_direct.net_investment_remeasurement is 'An amount field that specifies the net investment amount that is part of a remeasurement reclassificiation.';
comment on column lsr_ilr_schedule_direct_fin.deferred_profit_remeasurement is 'An amount field that specifies the deferred profit amount that is part of a remeasurement reclassificiation.';


-- add the payment fields to the prior schedule temp
alter table lsr_ilr_prior_schedule_temp add
(	
	beginning_remaining_payments number(22,2) default 0 not null,
	ending_remaining_payments number(22,2) default 0 not null
);

comment on column lsr_ilr_prior_schedule_temp.beginning_remaining_payments is 'An amount field that specifies the beginning monthly remaining payment for the ILR.';
comment on column lsr_ilr_prior_schedule_temp.ending_remaining_payments is 'An amount field that specifies the ending monthly remaining payment for the ILR.';

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT FORCE IS OBJECT(MONTH DATE,
                                                          principal_received NUMBER,
                                                          interest_income_received     NUMBER,
                                                          interest_income_accrued      NUMBER,
                                                          principal_accrued            NUMBER,
                                                          begin_receivable         NUMBER,
                                                          end_receivable         NUMBER,
                                                          begin_lt_receivable          NUMBER,
                                                          end_lt_receivable            NUMBER,
                                                          initial_direct_cost          NUMBER,
                                                          executory_accrual1           NUMBER,
                                                          executory_accrual2           NUMBER,
                                                          executory_accrual3           NUMBER,
                                                          executory_accrual4           NUMBER,
                                                          executory_accrual5           NUMBER,
                                                          executory_accrual6           NUMBER,
                                                          executory_accrual7           NUMBER,
                                                          executory_accrual8           NUMBER,
                                                          executory_accrual9           NUMBER,
                                                          executory_accrual10          NUMBER,
                                                          executory_paid1              NUMBER,
                                                          executory_paid2              NUMBER,
                                                          executory_paid3              NUMBER,
                                                          executory_paid4              NUMBER,
                                                          executory_paid5              NUMBER,
                                                          executory_paid6              NUMBER,
                                                          executory_paid7              NUMBER,
                                                          executory_paid8              NUMBER,
                                                          executory_paid9              NUMBER,
                                                          executory_paid10             NUMBER,
                                                          contingent_accrual1          NUMBER,
                                                          contingent_accrual2          NUMBER,
                                                          contingent_accrual3          NUMBER,
                                                          contingent_accrual4          NUMBER,
                                                          contingent_accrual5          NUMBER,
                                                          contingent_accrual6          NUMBER,
                                                          contingent_accrual7          NUMBER,
                                                          contingent_accrual8          NUMBER,
                                                          contingent_accrual9          NUMBER,
                                                          contingent_accrual10         NUMBER,
                                                          contingent_paid1             NUMBER,
                                                          contingent_paid2             NUMBER,
                                                          contingent_paid3             NUMBER,
                                                          contingent_paid4             NUMBER,
                                                          contingent_paid5             NUMBER,
                                                          contingent_paid6             NUMBER,
                                                          contingent_paid7             NUMBER,
                                                          contingent_paid8             NUMBER,
                                                          contingent_paid9             NUMBER,
                                                          contingent_paid10            NUMBER,
                                                          begin_unguaranteed_residual NUMBER,
                                                          int_on_unguaranteed_residual NUMBER,
                                                          end_unguaranteed_residual NUMBER,
                                                          begin_net_investment NUMBER,
                                                          int_on_net_investment NUMBER,
                                                          end_net_investment NUMBER,
                                                          receivable_remeasurement number,
														  lt_receivable_remeasurement  NUMBER,
                                                          unguaran_residual_remeasure number,
                                                          rate_implicit FLOAT,
                                                          discount_rate FLOAT,
                                                          rate_implicit_ni FLOAT,
                                                          discount_rate_ni FLOAT,
                                                          begin_lease_receivable number,
                                                          original_net_investment number,
                                                          npv_lease_payments NUMBER,
                                                          npv_guaranteed_residual NUMBER,
                                                          npv_unguaranteed_residual NUMBER,
                                                          selling_profit_loss NUMBER,
                                                          cost_of_goods_sold NUMBER,
                                                          schedule_rates t_lsr_ilr_schedule_all_rates,
														  beginning_remaining_payments NUMBER,
														  ending_remaining_payments NUMBER,
														  net_investment_remeasurement NUMBER,
														  beg_deferred_rent          NUMBER,
														  deferred_rent                NUMBER,
														  end_deferred_rent            NUMBER,
														  beg_accrued_rent           NUMBER,
														  accrued_rent                 NUMBER,
														  end_accrued_rent             NUMBER
														  );
/
							  
-- rebuild dependent type
CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_RESULT_TAB AS TABLE OF lsr_ilr_sales_sch_result;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15963, 0, 2018, 2, 1, 0, 53233, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_053223_lessor_01_remeasure_sch_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
