/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_050310_lessee_02_invoice_notes_descr_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.2.0.0  02/06/2018 Sarah Byers    Add ability to import notes and description on invoices
||============================================================================
*/

insert into pp_import_column(import_type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select 254, 'description', 'Description', 0, 1, 'varchar2(35)', 1 
from dual
where not exists(
  select 1
  from pp_import_column
  where import_type_id = 254
  and column_name = 'description')
;

insert into pp_import_column(import_type_id, column_name, description, is_required, processing_order, column_type, is_on_table)
select 254, 'notes', 'Notes', 0, 1, 'varchar2(4000)', 1 
from dual
where not exists(
  select 1
  from pp_import_column
  where import_type_id = 254
  and column_name = 'notes')
;

insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name)
select import_template_id, max(field_id)+1, 254, 'description'
from pp_import_template_fields
where import_type_id = 254
group by import_template_id
;

insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name)
select import_template_id, max(field_id)+1, 254, 'notes'
from pp_import_template_fields
where import_type_id = 254
group by import_template_id
;

INSERT INTO pp_import_template_fields VALUES ( 
(SELECT import_template_id FROM pp_import_template WHERE description='Lease Invoice Add'), 
(select max(field_id)+1 from pp_import_template_fields where import_template_id =
        (SELECT import_template_id FROM pp_import_template WHERE description='Lease Invoice Add')),
 null, null, 254, 'notes', NULL, NULL, NULL, NULL
 );
 
INSERT INTO pp_import_template_fields VALUES ( 
(SELECT import_template_id FROM pp_import_template WHERE description='Lease Invoice Add'), 
(select max(field_id)+1 from pp_import_template_fields where import_template_id =
        (SELECT import_template_id FROM pp_import_template WHERE description='Lease Invoice Add')),
 null, null, 254, 'description', NULL, NULL, NULL, NULL
 );

 --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4116, 0, 2017, 2, 0, 0, 50310, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050310_lessee_02_invoice_notes_descr_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;