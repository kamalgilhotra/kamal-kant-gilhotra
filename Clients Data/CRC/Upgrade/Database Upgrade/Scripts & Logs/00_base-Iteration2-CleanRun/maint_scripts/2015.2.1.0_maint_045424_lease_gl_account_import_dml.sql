/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045424_lease_gl_account_import_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.1.0 02/12/2016 Will Davis  Remove GL account from asset import
||============================================================================
*/

delete From pp_import_template_fields
where import_template_id in (select import_template_id from pp_import_template where upper(trim(description)) = 'LEASED ASSET ADD')
and column_name in ('gl_account_id');

delete From pp_import_template_fields
where column_name = 'gl_account_id'
  and import_type_id in (select import_type_id from pp_import_type where upper(trim(description)) = 'ADD: LEASED ASSETS');

delete From pp_import_column
where column_name = 'gl_account_id'
and import_type_id in (select import_type_id from pp_import_type where upper(trim(description)) = 'ADD: LEASED ASSETS');

/* We have to re-sort our fields */
merge into pp_import_template_fields a using (
select column_name, import_template_id, row_number() over(partition by import_template_id order by field_id) the_row
From pp_import_template_fields a
where import_template_id in (select import_template_id from pp_import_template where upper(description) = 'LEASED ASSET ADD')) b
on (a.import_template_id = b.import_template_id and a.column_name = b.column_name)
when matched then update set a.field_id = b.the_row;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3059, 0, 2015, 2, 1, 0, 045424, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045424_lease_gl_account_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;