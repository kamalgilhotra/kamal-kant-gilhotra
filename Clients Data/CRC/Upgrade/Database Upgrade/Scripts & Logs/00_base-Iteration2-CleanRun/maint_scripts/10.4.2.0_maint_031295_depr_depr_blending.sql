/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031295_depr_depr_blending.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/17/2014 Kyle Peterson  Point Release
||============================================================================
*/

create global temporary table DEPR_LEDGER_BLENDING_STG
(
 SOURCE_SOB_ID                  number(22,0),
 TARGET_SOB_ID                  number(22,0),
 DEPR_GROUP_ID                  number(22,0),
 GL_POST_MO_YR                  date,
 PRIORITY                       number(22,0),
 DEPR_LEDGER_STATUS             number(22,0),
 SOURCE_PERCENT                 number(22,8),
 PRIOR_PERCENT                  number(22,8) default 0,
 LAST_MONTHS_RESERVE_RATIO      number(22,8),
 LAST_MONTHS_COR_RATIO          number(22,8),
 SOURCE_BEGIN_BALANCE           number(22,2) default 0,
 SOURCE_BEG_COR_BALANCE         number(22,2) default 0,
 BEGIN_RESERVE_BL               number(22,2) default 0,
 END_RESERVE_BL                 number(22,2) default 0,
 RESERVE_BAL_PROVISION_BL       number(22,2) default 0,
 RESERVE_BAL_COR_BL             number(22,2) default 0,
 SALVAGE_BALANCE_BL             number(22,2) default 0,
 RESERVE_BAL_ADJUST_BL          number(22,2) default 0,
 RESERVE_BAL_RETIREMENTS_BL     number(22,2) default 0,
 RESERVE_BAL_TRAN_IN_BL         number(22,2) default 0,
 RESERVE_BAL_TRAN_OUT_BL        number(22,2) default 0,
 RESERVE_BAL_OTHER_CREDITS_BL   number(22,2) default 0,
 RESERVE_BAL_GAIN_LOSS_BL       number(22,2) default 0,
 RESERVE_ALLOC_FACTOR_BL        number(22,2) default 0,
 BEGIN_BALANCE_BL               number(22,2) default 0,
 ADDITIONS_BL                   number(22,2) default 0,
 RETIREMENTS_BL                 number(22,2) default 0,
 TRANSFERS_IN_BL                number(22,2) default 0,
 TRANSFERS_OUT_BL               number(22,2) default 0,
 ADJUSTMENTS_BL                 number(22,2) default 0,
 DEPRECIATION_BASE_BL           number(22,2) default 0,
 END_BALANCE_BL                 number(22,2) default 0,
 DEPRECIATION_RATE_BL           number(22,8) default 0,
 DEPRECIATION_EXPENSE_BL        number(22,2) default 0,
 DEPR_EXP_ADJUST_BL             number(22,2) default 0,
 DEPR_EXP_ALLOC_ADJUST_BL       number(22,2) default 0,
 RETRO_DEPR_ADJ_BL              number(22,2) default 0,
 OVER_DEPR_ADJ_BL               number(22,2) default 0,
 COST_OF_REMOVAL_BL             number(22,2) default 0,
 RESERVE_RETIREMENTS_BL         number(22,2) default 0,
 SALVAGE_RETURNS_BL             number(22,2) default 0,
 SALVAGE_CASH_BL                number(22,2) default 0,
 RESERVE_CREDITS_BL             number(22,2) default 0,
 RESERVE_ADJUSTMENTS_BL         number(22,2) default 0,
 RESERVE_TRAN_IN_BL             number(22,2) default 0,
 RESERVE_TRAN_OUT_BL            number(22,2) default 0,
 GAIN_LOSS_BL                   number(22,2) default 0,
 VINTAGE_NET_SALVAGE_AMORT_BL   number(22,2) default 0,
 VINTAGE_NET_SALVAGE_RESERVE_BL number(22,2) default 0,
 CURRENT_NET_SALVAGE_AMORT_BL   number(22,2) default 0,
 CURRENT_NET_SALVAGE_RESERVE_BL number(22,2) default 0,
 IMPAIRMENT_RESERVE_BEG_BL      number(22,2) default 0,
 IMPAIRMENT_RESERVE_ACT_BL      number(22,2) default 0,
 IMPAIRMENT_RESERVE_END_BL      number(22,2) default 0,
 EST_ANN_NET_ADDS_BL            number(22,2) default 0,
 RWIP_ALLOCATION_BL             number(22,2) default 0,
 COR_BEG_RESERVE_BL             number(22,2) default 0,
 COR_EXPENSE_BL                 number(22,2) default 0,
 COR_EXP_ADJUST_BL              number(22,2) default 0,
 COR_EXP_ALLOC_ADJUST_BL        number(22,2) default 0,
 RETRO_COR_ADJ_BL               number(22,2) default 0,
 OVER_DEPR_ADJ_COR_BL           number(22,2) default 0,
 COR_RES_TRAN_IN_BL             number(22,2) default 0,
 COR_RES_TRAN_OUT_BL            number(22,2) default 0,
 COR_RES_ADJUST_BL              number(22,2) default 0,
 COR_END_RESERVE_BL             number(22,2) default 0,
 COST_OF_REMOVAL_RATE_BL        number(22,8) default 0,
 COST_OF_REMOVAL_BASE_BL        number(22,2) default 0,
 RWIP_COST_OF_REMOVAL_BL        number(22,2) default 0,
 RWIP_SALVAGE_CASH_BL           number(22,2) default 0,
 RWIP_SALVAGE_RETURNS_BL        number(22,2) default 0,
 RWIP_RESERVE_CREDITS_BL        number(22,2) default 0,
 SALVAGE_RATE_BL                number(22,8) default 0,
 SALVAGE_BASE_BL                number(22,2) default 0,
 SALVAGE_EXPENSE_BL             number(22,2) default 0,
 SALVAGE_EXP_ADJUST_BL          number(22,2) default 0,
 SALVAGE_EXP_ALLOC_ADJUST_BL    number(22,2) default 0,
 RETRO_SALV_ADJ_BL              number(22,2) default 0,
 OVER_DEPR_ADJ_SALV_BL          number(22,2) default 0,
 RESERVE_BAL_SALVAGE_EXP_BL     number(22,2) default 0,
 BLENDING_ADJUSTMENT            number(22,2) default 0,
 RESERVE_BLENDING_ADJUSTMENT    number(22,2) default 0,
 COR_BLENDING_ADJUSTMENT        number(22,2) default 0,
 RESERVE_BLENDING_TRANSFER      number(22,2) default 0,
 COR_BLENDING_TRANSFER          number(22,2) default 0,
 IMPAIRMENT_ASSET_AMOUNT_BL     number(22,2) default 0,
 IMPAIRMENT_EXPENSE_AMOUNT_BL   number(22,2) default 0,
 RESERVE_BAL_IMPAIRMENT_BL      number(22,2) default 0
) on commit preserve rows;

comment on table DEPR_LEDGER_BLENDING_STG is '(T)  [01]
A temporary table to perform depreciation blending calculations.  It is the callers repsonsibility to stage the data and handle the results.';

comment on column DEPR_LEDGER_BLENDING_STG.SOURCE_SOB_ID is 'Source Set of Books based on Depreciation Method Blending configuration.  System-assigned identifier of a unique set of books maintained by the utility in PowerPlan.';
comment on column DEPR_LEDGER_BLENDING_STG.TARGET_SOB_ID is 'Target Set of Books based on Depreciation Method Blending configuration.  System-assigned identifier of a unique set of books maintained by the utility in PowerPlan.';
comment on column DEPR_LEDGER_BLENDING_STG.DEPR_GROUP_ID is 'System-assigned identifier of a particular depreciation group.';
comment on column DEPR_LEDGER_BLENDING_STG.GL_POST_MO_YR is 'Records for a given depreciation group the accounting month and year of activity the entry represents.  Each month a new set of entries is filled in by the system.';
comment on column DEPR_LEDGER_BLENDING_STG.LAST_MONTHS_RESERVE_RATIO is 'The reserve ratio for the previous month.';
comment on column DEPR_LEDGER_BLENDING_STG.DEPR_LEDGER_STATUS is 'Represents the processing status of a depreciation ledger record.  An example would be ''OPEN'', meaning the record is available for posting.  These are coded values for program processing only.';
comment on column DEPR_LEDGER_BLENDING_STG.SOURCE_PERCENT is 'Blending percentage from Depreciation Method Blending used to calculate amounts for this record.';
comment on column DEPR_LEDGER_BLENDING_STG.PRIOR_PERCENT is 'Blending percentage from Depreciation Method Blending for last month used to calculate amounts for this record.';
comment on column DEPR_LEDGER_BLENDING_STG.SOURCE_BEGIN_BALANCE is 'The begin balance for the source set of books, not multiplied by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG.SOURCE_BEG_COR_BALANCE is 'The begin COR balance for the source set of books, not multiplied by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG.PRIORITY is 'The order in which blended sets of books are to be calculated.';
comment on column DEPR_LEDGER_BLENDING_STG.BEGIN_RESERVE_BL is 'Records the beginning balance of the Depreciation Reserve (including the Accumulated Provision) for the depreciation group for the month in dollars.  Note that a normal reserve (credit balance) is positive.  This excludes 1.) the impairment reserve, 2.) allocated RWIP and 3.) the COR Reserves.';
comment on column DEPR_LEDGER_BLENDING_STG.END_RESERVE_BL is '=  begin_reserve_bl
+ retirements_bl
+ gain_loss_bl
+ salvage_cash_bl
+ salvage_returns
+ reserve_credits
+ reserve_tran_in_bl
+ reserve_tran_out_bl
+ reserve_adjustment_bl
+ depr_exp_alloc_adjust_bl
+ depr_exp_adjust_bl
+ depreciation_expense_bl
current_net_salvage_amort_bl
+ salvage_expense_bl
+ salvage_exp_adjust_bl
+ salvage_exp_alloc_adjust_bl
+ reserve_blending_adjustment
+ reserve_blending_transfer
+  impairment_asset_amount_bl
+ impairment_expense_amount_bl
Depreciation Ledger Blending Adjustment  and  Transfer Example
1.??? From the losers (in allocation %), transfer out the reserve associated with the "transfer" of the asset:
2.??? Allocate the reserve transferred out to the gainers, e.g. the kth gainer gets:
3.??? Note:  Cost of Removal and Salvage Proceeds are applied on a pro rata (Jur%) basis
Year 1   100% Reserve    Jurisdictional Reserve';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_PROVISION_BL is 'Records the accumulated provision for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated depreciation provision (credit balance) is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_COR_BL is 'Records the accumulated cost of removal (incurred) in the COR reserve for the depreciation group in dollars at month end for closed months.  Note that the normal accumulated cost of removal (debit balance) is held as a negative.';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_BALANCE_BL is 'Records the accumulated salvage proceeds in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated salvage (credit balance) is held as a positive.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_ADJUST_BL is 'Records the accumulated reserve adjustments in the reserve for the depreciation group in dollars at month-end for closed months.  An accumulated credit is positive; an accumulated debit is negative.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_RETIREMENTS_BL is 'Records the accumulated retirements in the reserve for the depreciation group at month-end for closed months.  Note that the original cost allowed is included in this number, even when a gain/loss is explicitly calculated and not rolled into the reserve.  In this case the reserve_bal_gain_loss will hold the offset.  The normal balance (debit) is negative.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_TRAN_IN_BL is 'Records the accumulated reserve transfers into the depreciation group in dollars at month-end for closed months.  The normal balance (credit) is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_TRAN_OUT_BL is 'Records the accumulated reserve transfers out of the depreciation group in dollars at month-end for closed months.  The normal balance (debit) is negative.';
comment on column DEPR_LEDGER_BLENDING_STG.reserve_bal_other_credits_bl is 'Records the accumulated other credits in the reserve for the depreciation group at month-end for closed months.  The normal balance (credit) is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.reserve_bal_gain_loss_bl is 'Records the accumulated gain/loss recorded on the income statement for asset dispositions.  At month-end for closed months this is essentially a contra to the other accumulated reserve activities.  The normal balance (credit) is negative.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_ALLOC_FACTOR_BL is '(Not currently used)';
comment on column DEPR_LEDGER_BLENDING_STG.BEGIN_BALANCE_BL is 'Records the asset balance for the depreciation group at the beginning of the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG.ADDITIONS_BL is 'Records the asset additions activity that occurred during the month for the depreciation group in dollars.  The interpretation is the FERC activity, e.g., including adjustment transactions reclassified as a FERC add.  Normally positive.';
comment on column DEPR_LEDGER_BLENDING_STG.RETIREMENTS_BL is 'Records the asset retirement activity that occurred during the month in dollars.  These are just the retirement transactions.  Normally negative.  These are also used to reduce the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.TRANSFERS_IN_BL is 'Records the asset transfers into the depreciation group for the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally positive.';
comment on column DEPR_LEDGER_BLENDING_STG.TRANSFERS_OUT_BL is 'Records the asset transfers out of the depreciation group that occurred during the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally negative.';
comment on column DEPR_LEDGER_BLENDING_STG.ADJUSTMENTS_BL is 'Records the asset adjustments that occurred to the depreciation group during the month in dollars.  The interpretation is the FERC activity, e.g. it excludes adjustment transactions that have been reclassified as FERC additions or retirements.  (Increases in the depreciable balance are positive.)';
comment on column DEPR_LEDGER_BLENDING_STG.DEPRECIATION_BASE_BL is 'Records the calculated depreciable base for the depreciation group for the month in dollars.  It is the dollar base to which a monthly rate is applied.  This is dependent on the method and convention.  It can also be factored up (or down) for retroactive rate adjustments and adjustments/catch up for the curve and yearly methods.';
comment on column DEPR_LEDGER_BLENDING_STG.END_BALANCE_BL is 'Records the asset balance for the depreciation group at the end of the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG.DEPRECIATION_RATE_BL is 'Records the annual rate initially used to calculate the month''s depreciation expense, (for audit trail only).  It is not necessarily the effective rate; it does not take into account input adjustments or true-up (curve method) or retroactive true-ups.  (See depreciation_base.)';
comment on column DEPR_LEDGER_BLENDING_STG.DEPRECIATION_EXPENSE_BL is 'Records the depreciation calculated for the depreciation group for the month in dollars.  (This may be charged to an account other than depreciation expense.)  Note:  If the cost of removal has been broken out, this does not contain the cost of removal provision, which is then in ''cor_expense'' below.';
comment on column DEPR_LEDGER_BLENDING_STG.DEPR_EXP_ADJUST_BL is 'Records any input adjustment to the depreciation expense for the depreciation group for the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG.DEPR_EXP_ALLOC_ADJUST_BL is 'Records any calculated adjustment to depreciation expense for the depreciation group for the month, for example, combined group adjustments, depreciation check adjustment, or retroactive rate recalculation adjustments.';
comment on column DEPR_LEDGER_BLENDING_STG.COST_OF_REMOVAL_BL is 'Records the unitized actual removal costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is negative, reducing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_RETIREMENTS_BL is '(Not Used.  See ''retirements'').';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_RETURNS_BL is 'Records the unitized salvaged returns costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_CASH_BL is 'Records the unitized cash salvage incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_CREDITS_BL is 'Records the other credits incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_ADJUSTMENTS_BL is 'Records the input dollar adjustments incurred against the Depreciation reserve for the depreciation group for the month.  (An increase to reserve, e.g. a credit, is positive.)';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_TRAN_IN_BL is 'Records the reserve transfers in to the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_TRAN_OUT_BL is 'Records the reserve transfers out of the depreciation group for the month.  The normal sign is negative, decreasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.GAIN_LOSS_BL is 'Records the gain/loss calculated for the month.  It is a contra to the retirements and is accumulated in reserve_bal_gain_loss.  A gain is normally negative; a loss positive.';
comment on column DEPR_LEDGER_BLENDING_STG.VINTAGE_NET_SALVAGE_AMORT_BL is 'Amount of net salvage amortization in dollars for the current period pertaining to the particular salvage given by the gl_post_mo_year.  E.g., for the 8/1998 gl_post_mo_yr, this item will contain the amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the amortization from a previous month is overlaid.  That is why it is saved in total in ''current_net_salvage_amort'' below.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.';
comment on column DEPR_LEDGER_BLENDING_STG.VINTAGE_NET_SALVAGE_RESERVE_BL is 'Cumulative amount of net salvage amortization in dollars for the current period pertaining to the particular salvage and cost of removal for the vintage month/year given by the gl_post_mo_yr.  E.g., for the 8/1998 gl_post_mo_yr this item will contain the cumulative month-end amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the cumulative amortization from a previous month is overlaid.  That is why it is saved in total in ''current_net_salvage_reserve'' below.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort".';
comment on column DEPR_LEDGER_BLENDING_STG.CURRENT_NET_SALVAGE_AMORT_BL is 'Total amount in dollars of net salvage amortization for the current month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.  Note:  This variable has the opposite sign ? an expense is negative.  Its impact is in the "end_reserve."  The equivalent for cost of removal is in the "cor_expense."';
comment on column DEPR_LEDGER_BLENDING_STG.CURRENT_NET_SALVAGE_RESERVE_BL is 'Total cumulative amount (in dollars) of net salvage for the end of the month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.  Reserve is negative if there is negative net salvage.  The reserve is not reduced for fully amortized basis.';
comment on column DEPR_LEDGER_BLENDING_STG.IMPAIRMENT_RESERVE_BEG_BL is 'Records beginning of the month impairment reserve in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.IMPAIRMENT_RESERVE_ACT_BL is 'Records the impairment reserve activity for the month in dollars whether input or calculated from retirements.  A credit balance is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.IMPAIRMENT_RESERVE_END_BL is 'Records the impairment reserve at the end of the month in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.EST_ANN_NET_ADDS_BL is 'Records the estimated annual net additions used during that month''s calculation ? used for audit trail only.';
comment on column DEPR_LEDGER_BLENDING_STG.RWIP_ALLOCATION_BL is 'Records the optional allocation of the month end total RWIP balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.  It is the sum of the following four items:  1.) rwip_cost_of_removal, 2.) rwip_salvage_cash, 3.) rwip_salvage_returns, and 4.) rwip_reserve_credits.';
comment on column DEPR_LEDGER_BLENDING_STG.COR_BEG_RESERVE_BL is 'Beginning of the month cost of removal reserve balance in dollars.  Includes both the book provision and actual incurred.';
comment on column DEPR_LEDGER_BLENDING_STG.COR_EXPENSE_BL is 'Cost of removal expense provision (when identified) in dollars.  It also includes any cost of removal being amortized.  (See current Net Salvage Amort.)';
comment on column DEPR_LEDGER_BLENDING_STG.COR_EXP_ADJUST_BL is 'Input cost of removal expense adjustment for the month, in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG.COR_EXP_ALLOC_ADJUST_BL is 'Calculated cost of removal expense adjustment for the month, in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG.COR_RES_TRAN_IN_BL is 'Cost of removal balance transferred in during the month, normally associated with asset transfers.';
comment on column DEPR_LEDGER_BLENDING_STG.COR_RES_TRAN_OUT_BL is 'Cost of removal transferred out during the month in dollars, normally associated with asset transfers.';
comment on column DEPR_LEDGER_BLENDING_STG.COR_RES_ADJUST_BL is 'Input adjustments to the cost of removal reserve for the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG.COR_END_RESERVE_BL is 'Month end (net) cost of removal reserve balance in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG.COST_OF_REMOVAL_RATE_BL is 'Annual rate used to calculate the cost of removal provision; Expressed as a decimal.  Not the effective rate (e.g., it does not take into account adjustments).';
comment on column DEPR_LEDGER_BLENDING_STG.COST_OF_REMOVAL_BASE_BL is 'Records the dollar base against which the cost of removal rate was applied in that month.';
comment on column DEPR_LEDGER_BLENDING_STG.RWIP_COST_OF_REMOVAL_BL is 'Records the optional allocation of the month end RWIP cost of removal balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.RWIP_SALVAGE_CASH_BL is 'Records the optional allocation of the month end RWIP salvage cash balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.RWIP_SALVAGE_RETURNS_BL is 'Records the optional allocation of the month end RWIP salvage returns balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.RWIP_RESERVE_CREDITS_BL is 'Records the optional allocation of the month end RWIP reserve credits balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_RATE_BL is 'Annual rate used to calculate the salvage provision; Expressed as a decimal. Not the effective rate (i.e. it does not take into expense adjustments). The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_BASE_BL is 'Records the dollar base against which the salvage rate was applied in that month. The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_EXPENSE_BL is 'Salvage "expense" provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_EXP_ADJUST_BL is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG.SALVAGE_EXP_ALLOC_ADJUST_BL is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_SALVAGE_EXP_BL is 'Records the accumulated salvage provision (when identified) for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months. Note that the normal accumulated depreciation salvage provision (debit balance) is negative because it decreases total reserve.';
comment on column DEPR_LEDGER_BLENDING_STG.BLENDING_ADJUSTMENT is 'The plant balance adjustment due to a change in blending percentage from last month to this moth (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BLENDING_ADJUSTMENT is 'The life reserve balance adjustment due to a change in blending percentage from last month to this month (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG.COR_BLENDING_ADJUSTMENT is 'The COR reserve balance adjustment due to a change in blending percentage from last month to this month (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BLENDING_TRANSFER is 'The life reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG.COR_BLENDING_TRANSFER is 'The COR reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG.IMPAIRMENT_ASSET_AMOUNT_BL is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column DEPR_LEDGER_BLENDING_STG.IMPAIRMENT_EXPENSE_AMOUNT_BL is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';
comment on column DEPR_LEDGER_BLENDING_STG.RESERVE_BAL_IMPAIRMENT_BL is 'The accumulated impairment reserve balance (impairment_asset_amount_bl + impairment_expense_amount_bl)
PowerPlan Depreciation Ledger Blending Illustration';
comment on column DEPR_LEDGER_BLENDING_STG.RETRO_DEPR_ADJ_BL is 'The adjustment to depreciation expense due to the retroactive rate change calculation multiplied by the blending percentage. This value makes up part of the depr_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_LEDGER_BLENDING_STG.OVER_DEPR_ADJ_BL is 'The over depr adjustment multipled by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG.RETRO_COR_ADJ_BL is 'The adjustment to cost of removal expense due to the retroactive rate change calculation multiplied by the blending percentage. This value makes up part of the cor_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_LEDGER_BLENDING_STG.OVER_DEPR_ADJ_COR_BL is 'The over depr adjustment from COR multipled by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG.RETRO_SALV_ADJ_BL is 'The adjustment to salvage expene due to the retroactive rate change calculation multiplied by the blending percentage. This value makes up part of the salv_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_LEDGER_BLENDING_STG.OVER_DEPR_ADJ_SALV_BL is 'The over depr adjustment from Salvage multiplied by the blending percentage.';


create table DEPR_LEDGER_BLENDING_STG_ARC
(
 SOURCE_SOB_ID                  number(22,0),
 TARGET_SOB_ID                  number(22,0),
 DEPR_GROUP_ID                  number(22,0),
 GL_POST_MO_YR                  date,
 PRIORITY                       number(22,0),
 DEPR_LEDGER_STATUS             number(22,0),
 SOURCE_PERCENT                 number(22,8),
 PRIOR_PERCENT                  number(22,8) default 0,
 LAST_MONTHS_RESERVE_RATIO      number(22,8),
 LAST_MONTHS_COR_RATIO          number(22,8),
 SOURCE_BEGIN_BALANCE           number(22,2) default 0,
 SOURCE_BEG_COR_BALANCE         number(22,2) default 0,
 BEGIN_RESERVE_BL               number(22,2) default 0,
 END_RESERVE_BL                 number(22,2) default 0,
 RESERVE_BAL_PROVISION_BL       number(22,2) default 0,
 RESERVE_BAL_COR_BL             number(22,2) default 0,
 SALVAGE_BALANCE_BL             number(22,2) default 0,
 RESERVE_BAL_ADJUST_BL          number(22,2) default 0,
 RESERVE_BAL_RETIREMENTS_BL     number(22,2) default 0,
 RESERVE_BAL_TRAN_IN_BL         number(22,2) default 0,
 RESERVE_BAL_TRAN_OUT_BL        number(22,2) default 0,
 RESERVE_BAL_OTHER_CREDITS_BL   number(22,2) default 0,
 RESERVE_BAL_GAIN_LOSS_BL       number(22,2) default 0,
 RESERVE_ALLOC_FACTOR_BL        number(22,2) default 0,
 BEGIN_BALANCE_BL               number(22,2) default 0,
 ADDITIONS_BL                   number(22,2) default 0,
 RETIREMENTS_BL                 number(22,2) default 0,
 TRANSFERS_IN_BL                number(22,2) default 0,
 TRANSFERS_OUT_BL               number(22,2) default 0,
 ADJUSTMENTS_BL                 number(22,2) default 0,
 DEPRECIATION_BASE_BL           number(22,2) default 0,
 END_BALANCE_BL                 number(22,2) default 0,
 DEPRECIATION_RATE_BL           number(22,8) default 0,
 DEPRECIATION_EXPENSE_BL        number(22,2) default 0,
 DEPR_EXP_ADJUST_BL             number(22,2) default 0,
 DEPR_EXP_ALLOC_ADJUST_BL       number(22,2) default 0,
 RETRO_DEPR_ADJ_BL              number(22,2) default 0,
 OVER_DEPR_ADJ_BL               number(22,2) default 0,
 COST_OF_REMOVAL_BL             number(22,2) default 0,
 RESERVE_RETIREMENTS_BL         number(22,2) default 0,
 SALVAGE_RETURNS_BL             number(22,2) default 0,
 SALVAGE_CASH_BL                number(22,2) default 0,
 RESERVE_CREDITS_BL             number(22,2) default 0,
 RESERVE_ADJUSTMENTS_BL         number(22,2) default 0,
 RESERVE_TRAN_IN_BL             number(22,2) default 0,
 RESERVE_TRAN_OUT_BL            number(22,2) default 0,
 GAIN_LOSS_BL                   number(22,2) default 0,
 VINTAGE_NET_SALVAGE_AMORT_BL   number(22,2) default 0,
 VINTAGE_NET_SALVAGE_RESERVE_BL number(22,2) default 0,
 CURRENT_NET_SALVAGE_AMORT_BL   number(22,2) default 0,
 CURRENT_NET_SALVAGE_RESERVE_BL number(22,2) default 0,
 IMPAIRMENT_RESERVE_BEG_BL      number(22,2) default 0,
 IMPAIRMENT_RESERVE_ACT_BL      number(22,2) default 0,
 IMPAIRMENT_RESERVE_END_BL      number(22,2) default 0,
 EST_ANN_NET_ADDS_BL            number(22,2) default 0,
 RWIP_ALLOCATION_BL             number(22,2) default 0,
 COR_BEG_RESERVE_BL             number(22,2) default 0,
 COR_EXPENSE_BL                 number(22,2) default 0,
 COR_EXP_ADJUST_BL              number(22,2) default 0,
 COR_EXP_ALLOC_ADJUST_BL        number(22,2) default 0,
 RETRO_COR_ADJ_BL               number(22,2) default 0,
 OVER_DEPR_ADJ_COR_BL           number(22,2) default 0,
 COR_RES_TRAN_IN_BL             number(22,2) default 0,
 COR_RES_TRAN_OUT_BL            number(22,2) default 0,
 COR_RES_ADJUST_BL              number(22,2) default 0,
 COR_END_RESERVE_BL             number(22,2) default 0,
 COST_OF_REMOVAL_RATE_BL        number(22,8) default 0,
 COST_OF_REMOVAL_BASE_BL        number(22,2) default 0,
 RWIP_COST_OF_REMOVAL_BL        number(22,2) default 0,
 RWIP_SALVAGE_CASH_BL           number(22,2) default 0,
 RWIP_SALVAGE_RETURNS_BL        number(22,2) default 0,
 RWIP_RESERVE_CREDITS_BL        number(22,2) default 0,
 SALVAGE_RATE_BL                number(22,8) default 0,
 SALVAGE_BASE_BL                number(22,2) default 0,
 SALVAGE_EXPENSE_BL             number(22,2) default 0,
 SALVAGE_EXP_ADJUST_BL          number(22,2) default 0,
 SALVAGE_EXP_ALLOC_ADJUST_BL    number(22,2) default 0,
 RETRO_SALV_ADJ_BL              number(22,2) default 0,
 OVER_DEPR_ADJ_SALV_BL          number(22,2) default 0,
 RESERVE_BAL_SALVAGE_EXP_BL     number(22,2) default 0,
 BLENDING_ADJUSTMENT            number(22,2) default 0,
 RESERVE_BLENDING_ADJUSTMENT    number(22,2) default 0,
 COR_BLENDING_ADJUSTMENT        number(22,2) default 0,
 RESERVE_BLENDING_TRANSFER      number(22,2) default 0,
 COR_BLENDING_TRANSFER          number(22,2) default 0,
 IMPAIRMENT_ASSET_AMOUNT_BL     number(22,2) default 0,
 IMPAIRMENT_EXPENSE_AMOUNT_BL   number(22,2) default 0,
 RESERVE_BAL_IMPAIRMENT_BL      number(22,2) default 0
);

comment on table DEPR_LEDGER_BLENDING_STG_ARC is '(T)  [01]
An archive table for DEPR_LEDGER_BLENDING_STG, a table used in depreciation calculations.';

comment on column DEPR_LEDGER_BLENDING_STG_ARC.SOURCE_SOB_ID is 'Source Set of Books based on Depreciation Method Blending configuration.  System-assigned identifier of a unique set of books maintained by the utility in PowerPlan.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.TARGET_SOB_ID is 'Target Set of Books based on Depreciation Method Blending configuration.  System-assigned identifier of a unique set of books maintained by the utility in PowerPlan.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.DEPR_GROUP_ID is 'System-assigned identifier of a particular depreciation group.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.GL_POST_MO_YR is 'Records for a given depreciation group the accounting month and year of activity the entry represents.  Each month a new set of entries is filled in by the system.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.LAST_MONTHS_RESERVE_RATIO is 'The reserve ratio for the previous month.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.DEPR_LEDGER_STATUS is 'Represents the processing status of a depreciation ledger record.  An example would be ''OPEN'', meaning the record is available for posting.  These are coded values for program processing only.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SOURCE_PERCENT is 'Blending percentage from Depreciation Method Blending used to calculate amounts for this record.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.PRIOR_PERCENT is 'Blending percentage from Depreciation Method Blending for last month used to calculate amounts for this record.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SOURCE_BEGIN_BALANCE is 'The begin balance for the source set of books, not multiplied by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SOURCE_BEG_COR_BALANCE is 'The begin COR balance for the source set of books, not multiplied by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.PRIORITY is 'The order in which blended sets of books are to be calculated.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.BEGIN_RESERVE_BL is 'Records the beginning balance of the Depreciation Reserve (including the Accumulated Provision) for the depreciation group for the month in dollars.  Note that a normal reserve (credit balance) is positive.  This excludes 1.) the impairment reserve, 2.) allocated RWIP and 3.) the COR Reserves.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.END_RESERVE_BL is '=  begin_reserve_bl
+ retirements_bl
+ gain_loss_bl
+ salvage_cash_bl
+ salvage_returns
+ reserve_credits
+ reserve_tran_in_bl
+ reserve_tran_out_bl
+ reserve_adjustment_bl
+ depr_exp_alloc_adjust_bl
+ depr_exp_adjust_bl
+ depreciation_expense_bl
current_net_salvage_amort_bl
+ salvage_expense_bl
+ salvage_exp_adjust_bl
+ salvage_exp_alloc_adjust_bl
+ reserve_blending_adjustment
+ reserve_blending_transfer
+  impairment_asset_amount_bl
+ impairment_expense_amount_bl
Depreciation Ledger Blending Adjustment  and  Transfer Example
1.??? From the losers (in allocation %), transfer out the reserve associated with the "transfer" of the asset:
2.??? Allocate the reserve transferred out to the gainers, e.g. the kth gainer gets:
3.??? Note:  Cost of Removal and Salvage Proceeds are applied on a pro rata (Jur%) basis
Year 1   100% Reserve    Jurisdictional Reserve';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_PROVISION_BL is 'Records the accumulated provision for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated depreciation provision (credit balance) is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_COR_BL is 'Records the accumulated cost of removal (incurred) in the COR reserve for the depreciation group in dollars at month end for closed months.  Note that the normal accumulated cost of removal (debit balance) is held as a negative.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_BALANCE_BL is 'Records the accumulated salvage proceeds in the reserve for the depreciation group in dollars at month-end for closed months.  Note that the normal accumulated salvage (credit balance) is held as a positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_ADJUST_BL is 'Records the accumulated reserve adjustments in the reserve for the depreciation group in dollars at month-end for closed months.  An accumulated credit is positive; an accumulated debit is negative.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_RETIREMENTS_BL is 'Records the accumulated retirements in the reserve for the depreciation group at month-end for closed months.  Note that the original cost allowed is included in this number, even when a gain/loss is explicitly calculated and not rolled into the reserve.  In this case the reserve_bal_gain_loss will hold the offset.  The normal balance (debit) is negative.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_TRAN_IN_BL is 'Records the accumulated reserve transfers into the depreciation group in dollars at month-end for closed months.  The normal balance (credit) is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_TRAN_OUT_BL is 'Records the accumulated reserve transfers out of the depreciation group in dollars at month-end for closed months.  The normal balance (debit) is negative.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.reserve_bal_other_credits_bl is 'Records the accumulated other credits in the reserve for the depreciation group at month-end for closed months.  The normal balance (credit) is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.reserve_bal_gain_loss_bl is 'Records the accumulated gain/loss recorded on the income statement for asset dispositions.  At month-end for closed months this is essentially a contra to the other accumulated reserve activities.  The normal balance (credit) is negative.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_ALLOC_FACTOR_BL is '(Not currently used)';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.BEGIN_BALANCE_BL is 'Records the asset balance for the depreciation group at the beginning of the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.ADDITIONS_BL is 'Records the asset additions activity that occurred during the month for the depreciation group in dollars.  The interpretation is the FERC activity, e.g., including adjustment transactions reclassified as a FERC add.  Normally positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RETIREMENTS_BL is 'Records the asset retirement activity that occurred during the month in dollars.  These are just the retirement transactions.  Normally negative.  These are also used to reduce the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.TRANSFERS_IN_BL is 'Records the asset transfers into the depreciation group for the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.TRANSFERS_OUT_BL is 'Records the asset transfers out of the depreciation group that occurred during the month in dollars. (Excludes transfers directly from non-unitized to unitized.).  The interpretation is the FERC activity, e.g. it excludes transfer transactions that have been reclassified as FERC additions.  Normally negative.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.ADJUSTMENTS_BL is 'Records the asset adjustments that occurred to the depreciation group during the month in dollars.  The interpretation is the FERC activity, e.g. it excludes adjustment transactions that have been reclassified as FERC additions or retirements.  (Increases in the depreciable balance are positive.)';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.DEPRECIATION_BASE_BL is 'Records the calculated depreciable base for the depreciation group for the month in dollars.  It is the dollar base to which a monthly rate is applied.  This is dependent on the method and convention.  It can also be factored up (or down) for retroactive rate adjustments and adjustments/catch up for the curve and yearly methods.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.END_BALANCE_BL is 'Records the asset balance for the depreciation group at the end of the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.DEPRECIATION_RATE_BL is 'Records the annual rate initially used to calculate the month''s depreciation expense, (for audit trail only).  It is not necessarily the effective rate; it does not take into account input adjustments or true-up (curve method) or retroactive true-ups.  (See depreciation_base.)';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.DEPRECIATION_EXPENSE_BL is 'Records the depreciation calculated for the depreciation group for the month in dollars.  (This may be charged to an account other than depreciation expense.)  Note:  If the cost of removal has been broken out, this does not contain the cost of removal provision, which is then in ''cor_expense'' below.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.DEPR_EXP_ADJUST_BL is 'Records any input adjustment to the depreciation expense for the depreciation group for the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.DEPR_EXP_ALLOC_ADJUST_BL is 'Records any calculated adjustment to depreciation expense for the depreciation group for the month, for example, combined group adjustments, depreciation check adjustment, or retroactive rate recalculation adjustments.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COST_OF_REMOVAL_BL is 'Records the unitized actual removal costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is negative, reducing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_RETIREMENTS_BL is '(Not Used.  See ''retirements'').';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_RETURNS_BL is 'Records the unitized salvaged returns costs incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_CASH_BL is 'Records the unitized cash salvage incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_CREDITS_BL is 'Records the other credits incurred against the Depreciation Reserve for the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_ADJUSTMENTS_BL is 'Records the input dollar adjustments incurred against the Depreciation reserve for the depreciation group for the month.  (An increase to reserve, e.g. a credit, is positive.)';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_TRAN_IN_BL is 'Records the reserve transfers in to the depreciation group for the month.  The normal sign is positive, increasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_TRAN_OUT_BL is 'Records the reserve transfers out of the depreciation group for the month.  The normal sign is negative, decreasing the reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.GAIN_LOSS_BL is 'Records the gain/loss calculated for the month.  It is a contra to the retirements and is accumulated in reserve_bal_gain_loss.  A gain is normally negative; a loss positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.VINTAGE_NET_SALVAGE_AMORT_BL is 'Amount of net salvage amortization in dollars for the current period pertaining to the particular salvage given by the gl_post_mo_year.  E.g., for the 8/1998 gl_post_mo_yr, this item will contain the amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the amortization from a previous month is overlaid.  That is why it is saved in total in ''current_net_salvage_amort'' below.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.VINTAGE_NET_SALVAGE_RESERVE_BL is 'Cumulative amount of net salvage amortization in dollars for the current period pertaining to the particular salvage and cost of removal for the vintage month/year given by the gl_post_mo_yr.  E.g., for the 8/1998 gl_post_mo_yr this item will contain the cumulative month-end amortization of 8/1998 (vintage) net salvage in all successive months.  Note that the cumulative amortization from a previous month is overlaid.  That is why it is saved in total in ''current_net_salvage_reserve'' below.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort".';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.CURRENT_NET_SALVAGE_AMORT_BL is 'Total amount in dollars of net salvage amortization for the current month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.  Note:  This variable has the opposite sign ? an expense is negative.  Its impact is in the "end_reserve."  The equivalent for cost of removal is in the "cor_expense."';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.CURRENT_NET_SALVAGE_RESERVE_BL is 'Total cumulative amount (in dollars) of net salvage for the end of the month, summarized and saved.  This variable applies only if the salvage treatment or COR treatment on depr group is ''Amort''.  Reserve is negative if there is negative net salvage.  The reserve is not reduced for fully amortized basis.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.IMPAIRMENT_RESERVE_BEG_BL is 'Records beginning of the month impairment reserve in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.IMPAIRMENT_RESERVE_ACT_BL is 'Records the impairment reserve activity for the month in dollars whether input or calculated from retirements.  A credit balance is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.IMPAIRMENT_RESERVE_END_BL is 'Records the impairment reserve at the end of the month in dollars.  This is excluded from the other reserve balances.  A credit balance is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.EST_ANN_NET_ADDS_BL is 'Records the estimated annual net additions used during that month''s calculation ? used for audit trail only.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RWIP_ALLOCATION_BL is 'Records the optional allocation of the month end total RWIP balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.  It is the sum of the following four items:  1.) rwip_cost_of_removal, 2.) rwip_salvage_cash, 3.) rwip_salvage_returns, and 4.) rwip_reserve_credits.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_BEG_RESERVE_BL is 'Beginning of the month cost of removal reserve balance in dollars.  Includes both the book provision and actual incurred.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_EXPENSE_BL is 'Cost of removal expense provision (when identified) in dollars.  It also includes any cost of removal being amortized.  (See current Net Salvage Amort.)';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_EXP_ADJUST_BL is 'Input cost of removal expense adjustment for the month, in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_EXP_ALLOC_ADJUST_BL is 'Calculated cost of removal expense adjustment for the month, in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_RES_TRAN_IN_BL is 'Cost of removal balance transferred in during the month, normally associated with asset transfers.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_RES_TRAN_OUT_BL is 'Cost of removal transferred out during the month in dollars, normally associated with asset transfers.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_RES_ADJUST_BL is 'Input adjustments to the cost of removal reserve for the month in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_END_RESERVE_BL is 'Month end (net) cost of removal reserve balance in dollars.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COST_OF_REMOVAL_RATE_BL is 'Annual rate used to calculate the cost of removal provision; Expressed as a decimal.  Not the effective rate (e.g., it does not take into account adjustments).';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COST_OF_REMOVAL_BASE_BL is 'Records the dollar base against which the cost of removal rate was applied in that month.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RWIP_COST_OF_REMOVAL_BL is 'Records the optional allocation of the month end RWIP cost of removal balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RWIP_SALVAGE_CASH_BL is 'Records the optional allocation of the month end RWIP salvage cash balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RWIP_SALVAGE_RETURNS_BL is 'Records the optional allocation of the month end RWIP salvage returns balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RWIP_RESERVE_CREDITS_BL is 'Records the optional allocation of the month end RWIP reserve credits balance (in dollars) to depreciation group for the month.  It is recalculated each month.  A credit is positive.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_RATE_BL is 'Annual rate used to calculate the salvage provision; Expressed as a decimal. Not the effective rate (i.e. it does not take into expense adjustments). The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_BASE_BL is 'Records the dollar base against which the salvage rate was applied in that month. The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_EXPENSE_BL is 'Salvage "expense" provison (when identified) in dollars. The default is 0.   Generally negative.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_EXP_ADJUST_BL is 'Input salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.SALVAGE_EXP_ALLOC_ADJUST_BL is 'Calculated salvage provision expense adjustment for the month, in dollars. The default is 0.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_SALVAGE_EXP_BL is 'Records the accumulated salvage provision (when identified) for depreciation included in the reserve for the depreciation group in dollars at month-end for closed months. Note that the normal accumulated depreciation salvage provision (debit balance) is negative because it decreases total reserve.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.BLENDING_ADJUSTMENT is 'The plant balance adjustment due to a change in blending percentage from last month to this moth (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BLENDING_ADJUSTMENT is 'The life reserve balance adjustment due to a change in blending percentage from last month to this month (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_BLENDING_ADJUSTMENT is 'The COR reserve balance adjustment due to a change in blending percentage from last month to this month (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BLENDING_TRANSFER is 'The life reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.COR_BLENDING_TRANSFER is 'The COR reserve transfer necessary to offset the reserve blending adjustment to zero across all source sets of books (see example in Depr Ledger Blending below).';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.IMPAIRMENT_ASSET_AMOUNT_BL is 'The amount to be posted to the CPR and recorded in an asset impairment bucket which reduces the asset cost and reduces the reserve balance.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.IMPAIRMENT_EXPENSE_AMOUNT_BL is 'The expense amount is equal to the change in NBV, e.g. the amount booked to an expense account, and only affects the reserve balance.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RESERVE_BAL_IMPAIRMENT_BL is 'The accumulated impairment reserve balance (impairment_asset_amount_bl + impairment_expense_amount_bl)
PowerPlan Depreciation Ledger Blending Illustration';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RETRO_DEPR_ADJ_BL is 'The adjustment to depreciation expense due to the retroactive rate change calculation multiplied by the blending percentage. This value makes up part of the depr_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.OVER_DEPR_ADJ_BL is 'The over depr adjustment multipled by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RETRO_COR_ADJ_BL is 'The adjustment to cost of removal expense due to the retroactive rate change calculation multiplied by the blending percentage. This value makes up part of the cor_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.OVER_DEPR_ADJ_COR_BL is 'The over depr adjustment from COR multipled by the blending percentage.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.RETRO_SALV_ADJ_BL is 'The adjustment to salvage expene due to the retroactive rate change calculation multiplied by the blending percentage. This value makes up part of the salv_exp_alloc_adjust column on depr_ledger.';
comment on column DEPR_LEDGER_BLENDING_STG_ARC.OVER_DEPR_ADJ_SALV_BL is 'The over depr adjustment from Salvage multiplied by the blending percentage.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (867, 0, 10, 4, 2, 0, 31295, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031295_depr_depr_blending.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
