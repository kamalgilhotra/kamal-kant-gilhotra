/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_050770_lessee_01_ppbase_actions_windows_action_text_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.3.0.0 04/12/2018   K. Powers      PP-50770 - Increase field length to 35
 ||============================================================================
 */

alter table PPBASE_ACTIONS_WINDOWS
  modify ACTION_TEXT VARCHAR2(35);

 --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4307, 0, 2017, 3, 0, 0, 50770, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050770_lessee_01_ppbase_actions_windows_action_text_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;