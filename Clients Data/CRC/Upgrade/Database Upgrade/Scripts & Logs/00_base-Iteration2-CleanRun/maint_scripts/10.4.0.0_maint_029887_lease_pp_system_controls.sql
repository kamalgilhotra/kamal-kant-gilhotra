/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029887_lease_pp_system_controls.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   05/01/2013 Travis Nemes   Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'Allow Back Dated Leases',
          'Yes',
          'dw_yes_no;1',
          'Allow users to enter back dated leases in the lease module',
          -1
     from PP_SYSTEM_CONTROL;
insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'MLA Payment Terms',
          'NO',
          'dw_yes_no;1',
          'Yes if using MLA payment terms',
          -1
     from PP_SYSTEM_CONTROL;
insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1, 'MLA AIR', 'NO', 'dw_yes_no;1', 'YES if using MLA air feature', -1
     from PP_SYSTEM_CONTROL;
insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'LAM GL VALIDATE - CLIENT RULES',
          'NO',
          'dw_yes_no;1',
          'YES if using client validations',
          -1
     from PP_SYSTEM_CONTROL;
insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'Lease Vouchers Exist',
          'YES',
          'dw_yes_no;1',
          'Yes if client uses vouchers for invoicing',
          -1
     from PP_SYSTEM_CONTROL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (372, 0, 10, 4, 0, 0, 29887, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029887_lease_pp_system_controls.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
