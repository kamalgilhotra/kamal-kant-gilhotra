/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044456_sys_preview_je_1_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 08/05/2015 Andrew Scott   New preview JE feature
||============================================================================
*/

--drop table GL_TRANSACTION_PREVIEW;

create table GL_TRANSACTION_PREVIEW
(
  gl_trans_id            NUMBER(22,0) not null,
  time_stamp             DATE,
  user_id                VARCHAR2(18),
  month                  DATE not null,
  company_number         CHAR(10) not null,
  gl_account             VARCHAR2(2000) not null,
  debit_credit_indicator NUMBER(22,0) not null,
  amount                 NUMBER(22,2) default 0 not null,
  gl_je_code             CHAR(18) not null,
  gl_status_id           NUMBER(22,0) not null,
  description            VARCHAR2(254),
  source                 VARCHAR2(35),
  originator             VARCHAR2(18),
  comments               VARCHAR2(2000),
  pend_trans_id          NUMBER(22,0),
  asset_id               NUMBER(22,0),
  amount_type            NUMBER(22,0),
  je_method_id           NUMBER(22,0),
  tax_orig_month_number  NUMBER(22,0),
  trans_type             NUMBER(22,0)
);

-- Create primary and foreign keys
alter table GL_TRANSACTION_PREVIEW
  add constraint PK_GL_TRANS_PREV primary key (GL_TRANS_ID)
  using index tablespace PWRPLANT_IDX;

alter table GL_TRANSACTION_PREVIEW
  add constraint R_GL_TRANS_PREVIEW1 foreign key (GL_STATUS_ID)
  references GL_TRANS_STATUS (GL_STATUS_ID);
alter table GL_TRANSACTION_PREVIEW
  add constraint R_GL_TRANS_PREVIEW2 foreign key (TRANS_TYPE)
  references JE_TRANS_TYPE (TRANS_TYPE);

-- Create indexes 
create index GLTRANSPMO on GL_TRANSACTION_PREVIEW (MONTH)
  tablespace PWRPLANT_IDX;
create index GL_TRANS_PREV_STATUS_IDX on GL_TRANSACTION_PREVIEW (GL_STATUS_ID)
  tablespace PWRPLANT;

-- Add comments to the table 
comment on table GL_TRANSACTION_PREVIEW
  is '(C)  [01] [11] {A}
The GL Transaction Preview table contains a preview of the general ledger transactions created by PowerPlan to be sent to the company''s general ledger system.  These are then reformatted to the company''s GL entries.';
-- Add comments to the columns 
comment on column GL_TRANSACTION_PREVIEW.gl_trans_id
  is 'Key for preview general ledger transaction.';
comment on column GL_TRANSACTION_PREVIEW.time_stamp
  is 'Standard System-assigned timestamp used for audit purposes.';
comment on column GL_TRANSACTION_PREVIEW.user_id
  is 'Standard System-assigned user id used for audit purposes.';
comment on column GL_TRANSACTION_PREVIEW.month
  is 'This is the accounting month year.';
comment on column GL_TRANSACTION_PREVIEW.company_number
  is 'This is the external company name.';
comment on column GL_TRANSACTION_PREVIEW.gl_account
  is 'Generated external general ledger account';
comment on column GL_TRANSACTION_PREVIEW.debit_credit_indicator
  is 'Indicates whether a number is a debit or a credit 0. Credit 1. Debit';
comment on column GL_TRANSACTION_PREVIEW.amount
  is 'Dollar amount of transaction.';
comment on column GL_TRANSACTION_PREVIEW.gl_je_code
  is 'Journal entry code or number.';
comment on column GL_TRANSACTION_PREVIEW.gl_status_id
  is 'Reference to GL status:  Pending, Approved, Sent.';
comment on column GL_TRANSACTION_PREVIEW.description
  is 'Description of the transaction.';
comment on column GL_TRANSACTION_PREVIEW.source
  is 'Internal field recording sources; used in reversing transactions when the reopen month is used. Values include: ''Depr Approval''.';
comment on column GL_TRANSACTION_PREVIEW.originator
  is 'User creating a manual entry';
comment on column GL_TRANSACTION_PREVIEW.comments
  is 'User comments for a manual entry';
comment on column GL_TRANSACTION_PREVIEW.pend_trans_id
  is 'Originating Pending transaction ID.';
comment on column GL_TRANSACTION_PREVIEW.asset_id
  is 'Originating Asset ID.';
comment on column GL_TRANSACTION_PREVIEW.amount_type
  is 'The CR amount type, e.g. 1 (Actuals), 2 (Budget), 3 (Statistics); 99 (Tax M Item).';
comment on column GL_TRANSACTION_PREVIEW.je_method_id
  is 'The system-assigned id of a je method (e.g. GAAP, reverse GAAP,  IFRS, etc.)';
comment on column GL_TRANSACTION_PREVIEW.tax_orig_month_number
  is 'The original month associated with a tax transaction if different from the posting month.  Format is YYYYMM.';
comment on column GL_TRANSACTION_PREVIEW.trans_type
  is 'The trans_type used to generate the gl_account string.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2748, 0, 2015, 2, 0, 0, 044456, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044456_sys_preview_je_1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;