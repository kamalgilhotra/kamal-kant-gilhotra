/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052112_lessee_01_add_deriver_control_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.1 08/02/2018 Crystal Yura Need to add required columns to cr_deriver_control if not added already
||============================================================================
*/

BEGIN
   EXECUTE IMMEDIATE 'alter table cr_deriver_control add tax_local_id NUMBER(22,0) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/


BEGIN
   EXECUTE IMMEDIATE 'alter table cr_deriver_control add ls_je_trans_Type varchar2(4) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/


COMMENT ON COLUMN cr_deriver_control.tax_local_id IS 'The internal local tax id within PowerPlant.';
COMMENT ON COLUMN cr_deriver_control.ls_je_trans_type IS 'The JE trans type associated with Lease journal entries.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8942, 0, 2017, 4, 0, 1, 52112, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052112_lessee_01_add_deriver_control_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		   