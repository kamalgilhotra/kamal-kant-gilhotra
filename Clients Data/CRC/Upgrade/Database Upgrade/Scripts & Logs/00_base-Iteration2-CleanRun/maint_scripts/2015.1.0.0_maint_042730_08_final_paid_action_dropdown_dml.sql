/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_08_final_paid_action_dropdown_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

insert into ppbase_actions_windows
(ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
select
(select max(id) + 1 from ppbase_actions_windows),
'LESSEE',
'mark_final_paid',
'Mark Final Paid',
1,
'ue_mark_final_paid'
from dual
where not exists (select 1 from ppbase_actions_windows where action_identifier='mark_final_paid');


insert into ppbase_workspace
(module, workspace_identifier, label, workspace_uo_name, object_type_id)
values
('LESSEE', 'lessee_pend_transaction', 'Pending Transactions', 'w_ls_trans_manage', 2); 

insert into ppbase_menu_items
(MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values 
('LESSEE', 'lessee_pend_transaction', 2, 7, 'Pending Transactions', 'Pending Transactions', 'menu_wksp_admin', 'lessee_pend_transaction', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2411, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_08_final_paid_action_dropdown_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;