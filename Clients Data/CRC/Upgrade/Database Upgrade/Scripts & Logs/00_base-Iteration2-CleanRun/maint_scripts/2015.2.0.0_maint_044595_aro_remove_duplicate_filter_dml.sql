/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044595_aro_remove_duplicate_filter_dml.sql.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2 08/04/2015   Andrew Hill    Remove duplicate filters on ARO - 1000
||============================================================================
*/
UPDATE pp_reports
SET pp_report_filter_id = 1
WHERE report_number = 'ARO - 1000';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2743, 0, 2015, 2, 0, 0, 044595, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044595_aro_remove_duplicate_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;