/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037615_budgetcap_PP_CURRENCY_EXCHANGE_BUDGET.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/08/2014
||============================================================================
*/

  CREATE OR REPLACE FUNCTION "PWRPLANT"."PP_CURRENCY_EXCHANGE_BUDGET" (a_from_curr    VARCHAR2,
                                                       a_to_curr      VARCHAR2,
                                                       a_month_number NUMBER)
   RETURN NUMBER IS
   exchange_rate NUMBER(22,
                        8);
   counter       NUMBER(22,
                        0);
   monum         NUMBER(22,
                        0);
   sys_control   VARCHAR2(35);
BEGIN

   SELECT MAX(a)
     INTO monum
     FROM (SELECT to_number(MAX(to_char(exchange_date,
                                        'yyyymm'))) a
             FROM currency_rate
            WHERE exchange_rate_type_id = 2
              AND (currency_to = a_to_curr OR currency_from = a_from_curr)
           UNION
           SELECT 0
             FROM dual);

   SELECT MIN(a)
     INTO counter
     FROM (SELECT 4 a
             FROM dual
           UNION ALL
           SELECT 1
             FROM currency_rate
            WHERE currency_to = a_to_curr
              AND currency_from = a_from_curr
              AND to_char(exchange_date,
                          'yyyymm') = monum
           UNION
           SELECT 2
             FROM currency_rate
            WHERE currency_to = a_from_curr
              AND currency_from = a_to_curr
              AND to_char(exchange_date,
                          'yyyymm') = monum
           UNION
           SELECT 3
             FROM currency_rate a,
                  currency_rate b
            WHERE a.currency_from = a_from_curr
              AND b.currency_from = a_to_curr
              AND a.currency_to = b.currency_to
              AND to_char(a.exchange_date,
                          'yyyymm') = monum
              AND to_char(b.exchange_date,
                          'yyyymm') = monum);

   IF counter = 4
   THEN
      RETURN 1;
   ELSIF counter = 1
   THEN
      SELECT rate
        INTO exchange_rate
        FROM currency_rate
       WHERE exchange_rate_type_id = 2
         AND exchange_date =
             (SELECT MAX(exchange_date)
                FROM currency_rate
               WHERE to_char(exchange_date,
                             'yyyymm') = monum
                 AND exchange_rate_type_id = 2)
         AND currency_from = a_from_curr
         AND currency_to = a_to_curr
         AND decode(to_char(exchange_date,
                            'yyyymm'),
                    monum,
                    -2,
                    decode(to_char(exchange_date,
                                   'yyyy'),
                           substr(monum,
                                  1,
                                  4),
                           -1,
                           decode(sign(months_between(to_date(monum,
                                                              'yyyymm'),
                                                      exchange_date)),
                                  0,
                                  0,
                                  1,
                                  months_between(to_date(monum,
                                                         'yyyymm'),
                                                 exchange_date),
                                  months_between(to_date(monum,
                                                         'yyyymm'),
                                                 exchange_date) * -1))) =
             (SELECT MIN(decode(to_char(exchange_date,
                                        'yyyymm'),
                                monum,
                                -2,
                                decode(to_char(exchange_date,
                                               'yyyy'),
                                       substr(monum,
                                              1,
                                              4),
                                       -1,
                                       decode(sign(months_between(to_date(monum,
                                                                          'yyyymm'),
                                                                  exchange_date)),
                                              0,
                                              0,
                                              1,
                                              months_between(to_date(monum,
                                                                     'yyyymm'),
                                                             exchange_date),
                                              months_between(to_date(monum,
                                                                     'yyyymm'),
                                                             exchange_date) * -1))))
                FROM currency_rate
               WHERE exchange_rate_type_id = 2
                 AND currency_from = a_from_curr
                 AND currency_to = a_to_curr);
   ELSIF counter = 2
   THEN
      SELECT 1 / rate
        INTO exchange_rate
        FROM currency_rate
       WHERE exchange_rate_type_id = 2
         AND currency_to = a_from_curr
         AND currency_from = a_to_curr
         AND exchange_date =
             (SELECT MAX(exchange_date)
                FROM currency_rate
               WHERE to_char(exchange_date,
                             'yyyymm') = monum
                 AND exchange_rate_type_id = 2)
         AND decode(to_char(exchange_date,
                            'yyyymm'),
                    monum,
                    -2,
                    decode(to_char(exchange_date,
                                   'yyyy'),
                           substr(monum,
                                  1,
                                  4),
                           -1,
                           decode(sign(months_between(to_date(monum,
                                                              'yyyymm'),
                                                      exchange_date)),
                                  0,
                                  0,
                                  1,
                                  months_between(to_date(monum,
                                                         'yyyymm'),
                                                 exchange_date),
                                  months_between(to_date(monum,
                                                         'yyyymm'),
                                                 exchange_date) * -1))) =
             (SELECT MIN(decode(to_char(exchange_date,
                                        'yyyymm'),
                                monum,
                                -2,
                                decode(to_char(exchange_date,
                                               'yyyy'),
                                       substr(monum,
                                              1,
                                              4),
                                       -1,
                                       decode(sign(months_between(to_date(monum,
                                                                          'yyyymm'),
                                                                  exchange_date)),
                                              0,
                                              0,
                                              1,
                                              months_between(to_date(monum,
                                                                     'yyyymm'),
                                                             exchange_date),
                                              months_between(to_date(monum,
                                                                     'yyyymm'),
                                                             exchange_date) * -1))))
                FROM currency_rate
               WHERE exchange_rate_type_id = 2
                 AND currency_to = a_from_curr
                 AND currency_from = a_to_curr);
   ELSIF counter = 3
   THEN
      SELECT round(from_curr_view.rate / to_curr_view.rate,
                   8)
        INTO exchange_rate
        FROM (SELECT rate,
                     currency_to
                FROM currency_rate
               WHERE exchange_rate_type_id = 2
                 AND currency_from = a_from_curr
                 AND exchange_date =
                     (SELECT MAX(exchange_date)
                        FROM currency_rate
                       WHERE to_char(exchange_date,
                                     'yyyymm') = monum
                         AND exchange_rate_type_id = 2)
                 AND decode(to_char(exchange_date,
                                    'yyyymm'),
                            monum,
                            -2,
                            decode(to_char(exchange_date,
                                           'yyyy'),
                                   substr(monum,
                                          1,
                                          4),
                                   -1,
                                   decode(sign(months_between(to_date(monum,
                                                                      'yyyymm'),
                                                              exchange_date)),
                                          0,
                                          0,
                                          1,
                                          months_between(to_date(monum,
                                                                 'yyyymm'),
                                                         exchange_date),
                                          months_between(to_date(monum,
                                                                 'yyyymm'),
                                                         exchange_date) * -1))) =
                     (SELECT MIN(decode(to_char(exchange_date,
                                                'yyyymm'),
                                        monum,
                                        -2,
                                        decode(to_char(exchange_date,
                                                       'yyyy'),
                                               substr(monum,
                                                      1,
                                                      4),
                                               -1,
                                               decode(sign(months_between(to_date(monum,
                                                                                  'yyyymm'),
                                                                          exchange_date)),
                                                      0,
                                                      0,
                                                      1,
                                                      months_between(to_date(monum,
                                                                             'yyyymm'),
                                                                     exchange_date),
                                                      months_between(to_date(monum,
                                                                             'yyyymm'),
                                                                     exchange_date) * -1))))
                        FROM currency_rate
                       WHERE exchange_rate_type_id = 2
                         AND currency_from = a_from_curr)) from_curr_view,
             (SELECT rate,
                     currency_to
                FROM currency_rate
               WHERE exchange_rate_type_id = 2
                 AND currency_from = a_to_curr
                 AND exchange_date =
                     (SELECT MAX(exchange_date)
                        FROM currency_rate
                       WHERE to_char(exchange_date,
                                     'yyyymm') = monum
                         AND exchange_rate_type_id = 2)
                 AND decode(to_char(exchange_date,
                                    'yyyymm'),
                            monum,
                            -2,
                            decode(to_char(exchange_date,
                                           'yyyy'),
                                   substr(monum,
                                          1,
                                          4),
                                   -1,
                                   decode(sign(months_between(to_date(monum,
                                                                      'yyyymm'),
                                                              exchange_date)),
                                          0,
                                          0,
                                          1,
                                          months_between(to_date(monum,
                                                                 'yyyymm'),
                                                         exchange_date),
                                          months_between(to_date(monum,
                                                                 'yyyymm'),
                                                         exchange_date) * -1))) =
                     (SELECT MIN(decode(to_char(exchange_date,
                                                'yyyymm'),
                                        monum,
                                        -2,
                                        decode(to_char(exchange_date,
                                                       'yyyy'),
                                               substr(monum,
                                                      1,
                                                      4),
                                               -1,
                                               decode(sign(months_between(to_date(monum,
                                                                                  'yyyymm'),
                                                                          exchange_date)),
                                                      0,
                                                      0,
                                                      1,
                                                      months_between(to_date(monum,
                                                                             'yyyymm'),
                                                                     exchange_date),
                                                      months_between(to_date(monum,
                                                                             'yyyymm'),
                                                                     exchange_date) * -1))))
                        FROM currency_rate
                       WHERE exchange_rate_type_id = 2
                         AND currency_from = a_to_curr)) to_curr_view
       WHERE from_curr_view.currency_to = to_curr_view.currency_to;
   END IF;

   RETURN exchange_rate;
END;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1102, 0, 10, 4, 2, 0, 37615, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037615_budgetcap_PP_CURRENCY_EXCHANGE_BUDGET.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
