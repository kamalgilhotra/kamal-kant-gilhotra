/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051115_lessor_01_add_auto_approve_import_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.1  05/09/2018 Crystal Yura    Add Auto Approve columns for Lessor MLA Import
||============================================================================
*/

BEGIN
   EXECUTE IMMEDIATE 'alter table lsr_import_lease add auto_approve NUMBER(22,0) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table lsr_import_lease add auto_approve_xlate varchar2(35) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table lsr_import_lease_archive add auto_approve NUMBER(22,0) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table lsr_import_lease_archive add auto_approve_xlate varchar2(35) DEFAULT NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/



COMMENT ON COLUMN lsr_import_lease.auto_approve IS 'A number to determine if this MLA should be auto approved on import.';
COMMENT ON COLUMN lsr_import_lease.auto_approve_xlate IS 'Translation field for determining if this MLA should be auto approved on import.';
COMMENT ON COLUMN lsr_import_lease_archive.auto_approve IS 'A number to determine if this MLA should be auto approved on import.';
COMMENT ON COLUMN lsr_import_lease_archive.auto_approve_xlate IS 'Translation field for determining if this MLA should be auto approved on import.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(6584, 0, 2017, 3, 0, 1, 51115, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051115_lessor_01_add_auto_approve_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;