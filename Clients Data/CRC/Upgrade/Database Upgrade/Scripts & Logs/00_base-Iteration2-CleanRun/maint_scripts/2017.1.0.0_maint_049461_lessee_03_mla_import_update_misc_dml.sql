/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049461_lessee_03_mla_import_update_misc_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/12/2017 Shane "C" Ward    Updates to Lessee ILR Import
||============================================================================
*/

UPDATE PP_IMPORT_LOOKUP
SET    lookup_sql = '( select b.lease_id from lsr_lease b where upper(trim( <importfield> ) ) = upper( trim( b.lease_number ) ) )'
WHERE  import_lookup_id = 1085; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3983, 0, 2017, 1, 0, 0, 49461, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049461_lessee_03_mla_import_update_misc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;