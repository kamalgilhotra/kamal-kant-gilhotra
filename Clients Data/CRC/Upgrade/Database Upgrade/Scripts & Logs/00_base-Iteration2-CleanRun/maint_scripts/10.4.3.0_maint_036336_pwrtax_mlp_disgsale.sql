/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036336_pwrtax_mlp_disgsale.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/19/2014 Andrew Scott        MLP Disguised Sale logic
||============================================================================
*/

----
----  insert new tax event (fixed table)
----

insert into TAX_EVENT (TAX_EVENT_ID, DESCRIPTION, DISPLAY_FOR_K1_EXPORT) values (5, 'Disguised Sale', 1);

----
----  create new temporary tables needed for the processing
----
create global temporary table tax_temp_mlp_dis_sale
(
tax_record_id number(22,0) not null,
fmv_allocator number(22,8),
fmv_allo_denom number(22,8),
new_tax_record_id number(22,0)
)
on commit preserve rows;

create index TAX_TEMP_MLP_DIS_SALE_IDX1
   on TAX_TEMP_MLP_DIS_SALE (TAX_RECORD_ID);

comment on table TAX_TEMP_MLP_DIS_SALE is '(T) [09] Tax Temp MLP Dis Sale is a temporary table that holds record mapping information used for disguised sales processing.';
comment on column TAX_TEMP_MLP_DIS_SALE.TAX_RECORD_ID is 'The tax record id selected in the disguised sale window.';
comment on column TAX_TEMP_MLP_DIS_SALE.FMV_ALLOCATOR is 'The tax records'' allocator used in the disgused sale processing.';
comment on column TAX_TEMP_MLP_DIS_SALE.FMV_ALLO_DENOM is 'The tax records'' denominator used in the disgused sale processing.';
comment on column TAX_TEMP_MLP_DIS_SALE.NEW_TAX_RECORD_ID is 'The tax record id for the newly created sales tax record that links back to the original tax record.';

create global temporary table TAX_TEMP_MLP_DIS_SALE_TBTI
(
 TAX_BOOK_ID    number(22,0) not null,
 TAX_INCLUDE_ID number(22,0) not null
) on commit preserve rows;

create index TAX_TEMP_MLP_DIS_SALE_TBIDX1
   on TAX_TEMP_MLP_DIS_SALE_TBTI (TAX_BOOK_ID);

comment on table TAX_TEMP_MLP_DIS_SALE_TBTI is '(T) [09] Tax Temp MLP Dis Sale TB TI is a temporary table that holds the selected tax books and tax includes used for disguised sales processing.';
comment on column TAX_TEMP_MLP_DIS_SALE_TBTI.TAX_BOOK_ID is 'The tax book id selected in the disguised sale window.';
comment on column TAX_TEMP_MLP_DIS_SALE_TBTI.TAX_INCLUDE_ID is 'The tax include id selected in the disguised sale window.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1368, 0, 10, 4, 3, 0, 36336, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036336_pwrtax_mlp_disgsale.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;