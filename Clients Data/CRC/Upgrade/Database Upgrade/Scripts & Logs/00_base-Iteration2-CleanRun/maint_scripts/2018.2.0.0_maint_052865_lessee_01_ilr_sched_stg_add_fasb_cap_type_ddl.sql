/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052865_lessee_01_ilr_sched_stg_add_fasb_cap_type_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 01/17/2019 Sarah Byers    Add FASB Cap Type ID to ls_ilr_schedule_stg
||============================================================================
*/

alter table LS_ILR_SCHEDULE_STG add fasb_cap_type_id number(22,0);

comment on column LS_ILR_SCHEDULE_STG.fasb_cap_type_id is 'System assigned identifier for the FASB Cap Type.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(14131, 0, 2018, 2, 0, 0, 52865, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052865_lessee_01_ilr_sched_stg_add_fasb_cap_type_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;