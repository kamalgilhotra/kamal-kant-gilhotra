set serveroutput on;
/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040318_anlyt_14_roles.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 10/24/2014 Chad Theilman    Asset Analytics - Roles
||============================================================================
*/

declare 
	doesRoleExist number := 0;
	
	begin
		doesRoleExist := PWRPLANT.SYS_DOES_ROLE_EXIST('PA_SELECT_ROLE');
		
		if doesRoleExist = 0 then
			begin
				execute immediate 'CREATE ROLE PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_ACCOUNTS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_AUDIT_HISTORY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_AVERAGE_AGES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CASE_PRIORITIES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CASE_STATUSES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CASES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CASES_COMMENTS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CHART to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CHART_SERIES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_COLUMN_FORMAT to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_COLUMNGROUP to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CONFIG to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CONTROL_GROUPS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CONTROL_LOCATIONS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CONTROL_PROCESS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CPR_ACTIVITY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CPR_ACTSUM to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CPR_BALANCES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_CPR_LEDGER to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DASHBOARD to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DASHBOARD_CHARTS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DASHBOARD_SHORTCUTS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DATAPOINT to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DATASOURCE to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DEPR_BALANCES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DEPR_BALANCES_YR to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_DEPR_GROUP to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXCLUDE_ACCOUNTS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXCLUDE_ACTIVITY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXCLUDE_ASSETS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXCLUDE_COMPANIES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXCLUDE_DEPR_GROUP to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXCLUDE_LOCATIONS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXCLUDE_PROPERTY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXPLORATION to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXPLORE_DATASOURCE TO PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_EXPLORE_GRIDFILTERS TO PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_FILTER_LOOKUP_CACHE to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_FILTER2 to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_FILTERGROUP to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_FILTERGROUP_FILTERS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_HEATMAP_COLOR to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_LOCATION_AGG to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_LOCATIONS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_PROPERTY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_RETIRE_WO to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.PA_VINTAGE_SPREADS to PA_SELECT_ROLE';
				
				execute immediate 'grant select on PWRANLYT.PA_CPR_LEDGER_TMP to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRPLANT.PP_COMPANY_SECURITY_TEMP TO PA_SELECT_ROLE';
				
				execute immediate 'grant select on PWRANLYT.V_PA_ACCOUNTS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_AVERAGE_AGES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_COMPANY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CONTROL_FILTERBY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CONTROL_GROUPBY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CONTROL_LOCN_FILTERBY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CONTROL_LOCN_GROUPBY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CPR_ACTIVITY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CPR_ACTSUM to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CPR_BALANCES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CPR_LEDGER to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_CURVE_COMPARISON to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_DEPR_BALANCES to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_DEPR_BALANCES_YR to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_DEPR_GROUP to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_LOCATIONS to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_LOCATION_AGG to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_PROPERTY to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_RETIRE_WO to PA_SELECT_ROLE';
				execute immediate 'grant select on PWRANLYT.V_PA_VINTAGE_SPREADS to PA_SELECT_ROLE';
				
				execute immediate 'grant select on PWRANLYT.PA_ANALYTICS_SEQ to PA_SELECT_ROLE'; 
				execute immediate 'grant PA_SELECT_ROLE TO PWRPLANT WITH ADMIN OPTION';

				execute immediate 'grant insert, update, delete on PWRANLYT.PA_DASHBOARD_SHORTCUTS TO PA_SELECT_ROLE';
				execute immediate 'grant insert, update, delete on PWRANLYT.PA_CASES TO PA_SELECT_ROLE';
				execute immediate 'grant insert, update, delete on PWRANLYT.PA_CASES_COMMENTS TO PA_SELECT_ROLE';
			end;
		end if;
	end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1579, 0, 10, 4, 3, 0, 40318, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040318_anlyt_14_roles.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;