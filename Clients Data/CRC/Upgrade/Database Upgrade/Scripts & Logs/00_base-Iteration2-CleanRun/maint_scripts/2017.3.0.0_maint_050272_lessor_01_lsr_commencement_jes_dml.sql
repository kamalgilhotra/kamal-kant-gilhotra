/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050272_lessor_01_lsr_commencement_jes_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 01/02/2018 Anand R        Add Lessor Commencement JEs
||============================================================================
*/

/* Add 8 trans types for lessor ilr commencement  */

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4001, 'PP&'||'E Adjustment Cr');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4002, 'Short Term Receivable Dr');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4003, 'Long Term Receivable Dr');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4004, 'Unguaranteed Residual Dr');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4005, 'Selling Profit Cr');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4006, 'Selling Loss Dr');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4007, 'Capitalized Initial Direct Costs Cr');

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION)
values (4008, 'Initial Direct Costs Dr');

/* Add GL_JE_CODE   */

insert into standard_journal_entries (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
values ( (select nvl((max(je_id) + 1),1) from standard_journal_entries), 'LSR Commencement', 'Lessor Commencement', 'Lessor Commencement', 'Lessor Commencement');

insert into gl_je_control (PROCESS_ID, JE_ID, JE_TABLE, JE_COLUMN, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN, CUSTOM_CALC, DR_ACCOUNT, CR_ACCOUNT)
values ('LSR Commencement', (select max(je_id) from standard_journal_entries ), null, null, 'NONE', 'NONE', 'NONE', 'NONE', null, null, null);

/* Add binding argument for Lessor ILR_ID */

insert into pp_jl_bind_arg_code (BIND_ARG_CODE_ID, VARIABLE_NAME, DESCRIPTION, KEYWORD_TYPE)
values (13, 'A_LSR_ILR_ID', 'oracle fxn parameter', 1);

/* Add Lessor keywords  */

INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_ppe' AS keyword,
         'Lessor Property Plant Equipment' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.prop_plant_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_ppe');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_iacr' AS keyword,
         'Lessor Interest Accrual' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.int_accrual_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_iacr');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_iexp' AS keyword,
         'Lessor Interest Expense' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.int_expense_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_iexp');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_eacr' AS keyword,
         'Lessor Executory Accrual' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.exec_accrual_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_eacr');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_eexp' AS keyword,
         'Lessor Executory Expense' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.exec_expense_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_eexp');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_cacr' AS keyword,
         'Lessor Contingent Accrual' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.cont_accrual_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_cacr');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_cexp' AS keyword,
         'Lessor Contingent Expense' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.cont_expense_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_cexp');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_strc' AS keyword,
         'Lessor Short Term Receivable' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.st_receivable_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_strc');
 
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_ltrc' AS keyword,
         'Lessor Long Term Receivable' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.lt_receivable_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_ltrc');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_ar' AS keyword,
         'Lessor Accounts Receivable' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.ar_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_ar');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_ugr' AS keyword,
         'Lessor Unguaranteed residual' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.unguaran_res_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_ugr');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_iugr' AS keyword,
         'Lessor Interest on Unguaranteed Residual' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.int_unguaran_res_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_iugr');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_sps' AS keyword,
         'Lessor Selling Profit Loss' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.sell_profit_loss_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_sps');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_idc' AS keyword,
         'Lessor Initial Direct Cost' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.ini_direct_cost_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_idc');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_std' AS keyword,
         'Lessor Short Term Deferred' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.st_deferred_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_std');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_ltd' AS keyword,
         'Lessor Long Term Deferred' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.lt_deferred_account_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_ltd');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_cgl' AS keyword,
         'Lessor Currency Gain Loss' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.curr_gain_loss_acct_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_cgl');
  
INSERT INTO pp_journal_keywords (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, TIME_STAMP, USER_ID)
 SELECT 'p_lsr_cglo' AS keyword,
         'Lessor Currency Gain Loss Offset' AS description,
         'select nvl(gl.external_account_code, '' '') from lsr_ilr_account ilr, gl_account gl where ilr.ilr_id = <arg1> and gl.gl_account_id = ilr.curr_gain_loss_offset_acct_id' AS sqls,
         1 AS keyword_type,
         'all' AS valid_elements,
         13 AS bind_arg_code_id1,
         SYSDATE AS time_stamp,
         USER AS user_id
  FROM dual
  WHERE NOT EXISTS (SELECT 1 FROM pp_journal_keywords WHERE keyword = 'p_lsr_cglo');
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4099, 0, 2017, 3, 0, 0, 50272, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050272_lessor_01_lsr_commencement_jes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 