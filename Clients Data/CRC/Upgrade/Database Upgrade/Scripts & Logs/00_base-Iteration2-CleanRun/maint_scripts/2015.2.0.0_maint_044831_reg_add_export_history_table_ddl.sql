/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044831_reg_add_export_history_table_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   08/31/2015 Daniel Motter  Creation
||============================================================================
*/

-- Create the table
CREATE TABLE reg_export_history (
    reg_export_id       NUMBER(22,0)    NOT NULL,
    reg_company_id      NUMBER(22,0)    NOT NULL,
    reg_jurisdiction_id NUMBER(22,0)    NOT NULL,
    reg_case_id         NUMBER(22,0)    NOT NULL,
    month_number        NUMBER(22,0)    NOT NULL,
    file_name           VARCHAR(254)    NOT NULL,
    computer_name       VARCHAR(254)    NOT NULL,
    date_processed      DATE            NULL,
    processor           VARCHAR(18)     NOT NULL,
    return_code         NUMBER(1)       NULL,
    user_id             VARCHAR(18)     NULL,
    time_stamp          DATE            NULL
);

-- Add the primary key
ALTER TABLE reg_export_history
  ADD CONSTRAINT pk_reg_export_history PRIMARY KEY (
    reg_export_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

-- Add the foreign keys
ALTER TABLE reg_export_history
  ADD CONSTRAINT fk_reg_export_company FOREIGN KEY (
    reg_company_id
  ) REFERENCES reg_company (
    reg_company_id
  )
/

ALTER TABLE reg_export_history
  ADD CONSTRAINT fk_reg_export_jur FOREIGN KEY (
    reg_jurisdiction_id
  ) REFERENCES reg_jurisdiction (
    reg_jurisdiction_id
  )
/

ALTER TABLE reg_export_history
  ADD CONSTRAINT fk_reg_export_case FOREIGN KEY (
    reg_case_id
  ) REFERENCES reg_case (
    reg_case_id
  )
/

-- Create a sequence to generate unique export id's
CREATE SEQUENCE reg_export_seq MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1;

-- Comments
COMMENT ON TABLE reg_export_history IS '(S) [19] Stores a log of basic information about past Regulatory Export Model runs.';
COMMENT ON COLUMN reg_export_history.reg_export_id IS 'A unique, system-assigned identifier for this row.';
COMMENT ON COLUMN reg_export_history.reg_company_id IS 'The regulatory company id the export was for.';
COMMENT ON COLUMN reg_export_history.reg_jurisdiction_id IS 'The regulatory jurisdiction id the export was for.';
COMMENT ON COLUMN reg_export_history.reg_case_id IS 'The regulatory case id the export was for.';
COMMENT ON COLUMN reg_export_history.month_number IS 'The month number the export was for.';
COMMENT ON COLUMN reg_export_history.file_name IS 'The full file path and name the export file was saved as.';
COMMENT ON COLUMN reg_export_history.computer_name IS 'The name of the computer that requested this export.';
COMMENT ON COLUMN reg_export_history.date_processed IS 'The date and time the export successfully finished processing. Will be null if the export failed or is still processing.';
COMMENT ON COLUMN reg_export_history.processor IS 'The user who requested the export.';
COMMENT ON COLUMN reg_export_history.return_code IS 'Indicates if the export was successful. 1=success, 0=in progress, -1=fail.';
COMMENT ON COLUMN reg_export_history.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN reg_export_history.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2835, 0, 2015, 2, 0, 0, 044831, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044831_reg_add_export_history_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;