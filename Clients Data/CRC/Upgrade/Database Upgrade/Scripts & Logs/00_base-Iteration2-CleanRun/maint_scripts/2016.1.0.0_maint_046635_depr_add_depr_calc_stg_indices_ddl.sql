/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046635_depr_add_depr_calc_stg_indices_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0  10/12/2016 Jared Watkins  Add indices to DEPR_CALC_STG and DEPR_LEDGER_BLENDING_STG for performance
||============================================================================
*/

CREATE INDEX DEPR_LEDGER_BL_STG_BL_ADJ_KEY
  ON DEPR_LEDGER_BLENDING_STG (DEPR_GROUP_ID, SOURCE_SOB_ID, GL_POST_MO_YR, INCREMENTAL_PROCESS_ID);

CREATE UNIQUE INDEX DEPR_CALC_STG_LOGICAL_KEYDEP
  ON DEPR_CALC_STG (DEPR_GROUP_ID, SET_OF_BOOKS_ID, GL_POST_MO_YR, INCREMENTAL_PROCESS_ID);

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3315, 0, 2016, 1, 0, 0, 046635, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046635_depr_add_depr_calc_stg_indices_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;