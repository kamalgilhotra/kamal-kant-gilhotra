/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029581_pwrtax_TAX_DFIT_CALC.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/14/2013 Roger Roach      Changed the Fast Tax to use Oracle Packages
||============================================================================
*/

create or replace package TAX_DFIT_CALC is
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: TAX_DEPR_CALC
   --|| Description:
   --||============================================================================
   --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 5.0      03/14/2013 Roger Roach    Original Version
   --|| 5.10     04/05/2013 Roger Roach    Added session parameter option
   --||============================================================================

   G_LIMIT constant pls_integer := 10000;
   subtype HASH_T is varchar2(200);
   G_SEP constant varchar2(1) := '-';
   G_START_TIME timestamp;

   G_TAX_DETAIL_LAST_RECORD_ID pls_integer;
   G_TAX_DETAIL_LAST_TAX_MONTH pls_integer;
   G_TAX_DETAIL_LAST_NORM_ID   pls_integer;

   G_DEF_TAX_BASIS_LAST_RECORD_ID pls_integer;
   G_DEF_TAX_BASIS_LAST_TAX_MONTH pls_integer;
   G_DEF_TAX_BASIS_LAST_NORM_ID   pls_integer;

   G_TAX_YEAR_COUNT      number(22, 2);
   G_TABLE_NORM_COUNT    number(22, 0);
   G_TABLE_VINTAGE_COUNT number(22, 0);
   G_TABLE_CLASS_COUNT   number(22, 0);
   G_TABLE_COMPANY_COUNT number(22, 0);

   G_VERSION           number(22, 0);
   G_START_YEAR        number(22, 2);
   G_END_YEAR          number(22, 2);
   G_TABLE_VINTAGE_IDS TABLE_LIST_ID_TYPE;
   G_TABLE_CLASS_IDS   TABLE_LIST_ID_TYPE;
   G_TABLE_COMPANY_IDS TABLE_LIST_ID_TYPE;
   G_TABLE_NORM_IDS    TABLE_LIST_ID_TYPE;

   type TAX_DETAIL_HASH_TYPE is table of number(22, 0) index by HASH_T;

   G_TAX_DETAIL_HASH TAX_DETAIL_HASH_TYPE;

   G_DEF_TAX_BASIS_HASH TAX_DETAIL_HASH_TYPE;

   type TAX_DETAIL_BALS_REC is record(
      ID       number(22, 0),
      TAX_YEAR number(22, 2));

   type TAX_DETAIL_BALS_REC_TYPE is table of TAX_DETAIL_BALS_REC index by pls_integer;

   type TAX_DETAIL_BALS_TABLE_TYPE is table of TAX_DETAIL_BALS_REC_TYPE index by HASH_T;

   G_TAX_DETAIL_BALS_HASH    TAX_DETAIL_BALS_TABLE_TYPE;
   G_DEF_TAX_BASIS_BALS_HASH TAX_DETAIL_BALS_TABLE_TYPE;

   type TAX_RATES_REC is record(
      ID      number(22, 0),
      RATE_ID number(22, 0));

   type TAX_RATES_REC_TYPE is table of TAX_RATES_REC index by pls_integer;

   type TAX_RATES_TABLE_TYPE is table of TAX_RATES_REC_TYPE index by HASH_T;

   G_TAX_RATES_HASH  TAX_RATES_TABLE_TYPE;
   G_DFIT_RATES_HASH TAX_RATES_TABLE_TYPE;

   cursor TAX_BASIS_AMOUNT_CUR is
      select NORMALIZATION_SCHEMA.JURISDICTION_ID,
             JS.TAX_BOOK_ID,
             DIT.TAX_RECORD_ID TAX_RECORD_ID,
             DIT.TAX_YEAR TAX_YEAR,
             sum(DIT.NORM_DIFF_BALANCE_BEG +
                 NVL(DEFERRED_INCOME_TAX_TRANSFER.NORM_DIFF_BALANCE_BEG, 0)) TAX_ONLY_NORM_DIFF_AMOUNT,
             sum(DECODE(BASIS_AMOUNT_ACTIVITY, 0, 0, -BASIS_DIFF_ADD_RET)) ORIG_DIFF,
             sum(TAX_BOOK_RECONCILE.BASIS_AMOUNT_BEG +
                 NVL(TAX_BOOK_RECONCILE_TRANSFER.BASIS_AMOUNT_BEG, 0)) BASIS_AMOUNT_BEG,
             sum(TAX_BOOK_RECONCILE.BASIS_AMOUNT_END) BASIS_AMOUNT_END
        from DEFERRED_INCOME_TAX DIT,
             NORMALIZATION_SCHEMA,
             JURISDICTION JS,
             TAX_BOOK_RECONCILE,
             TAX_RECORD_CONTROL TRC,
             TAX_INCLUDE_ACTIVITY,
             (select TAX_RECORD_ID,
                     TAX_YEAR,
                     TAX_MONTH,
                     NORMALIZATION_ID,
                     TIME_SLICE_ID,
                     sum(DEF_INCOME_TAX_BALANCE_BEG) DEF_INCOME_TAX_BALANCE_BEG,
                     sum(NORM_DIFF_BALANCE_BEG) NORM_DIFF_BALANCE_BEG
                from DEFERRED_INCOME_TAX_TRANSFER
               group by TAX_RECORD_ID, TAX_YEAR, TAX_MONTH, NORMALIZATION_ID, TIME_SLICE_ID) DEFERRED_INCOME_TAX_TRANSFER,
             (select TAX_INCLUDE_ID,
                     TAX_YEAR,
                     TAX_RECORD_ID,
                     RECONCILE_ITEM_ID,
                     sum(BASIS_AMOUNT_BEG) BASIS_AMOUNT_BEG
                from TAX_BOOK_RECONCILE_TRANSFER
               group by TAX_INCLUDE_ID, TAX_YEAR, TAX_RECORD_ID, RECONCILE_ITEM_ID) TAX_BOOK_RECONCILE_TRANSFER
       where (DIT.NORMALIZATION_ID = NORMALIZATION_SCHEMA.NORMALIZATION_ID)
         and (NORMALIZATION_SCHEMA.RECONCILE_ITEM_ID = TAX_BOOK_RECONCILE.RECONCILE_ITEM_ID)
         and TAX_BOOK_RECONCILE.TAX_INCLUDE_ID = TAX_INCLUDE_ACTIVITY.TAX_INCLUDE_ID
         and TAX_INCLUDE_ACTIVITY.TAX_BOOK_ID = NORMALIZATION_SCHEMA.NORM_FROM_TAX
         and (TRC.TAX_RECORD_ID = DIT.TAX_RECORD_ID)
         and (TAX_BOOK_RECONCILE.TAX_YEAR = DIT.TAX_YEAR)
         and (TAX_BOOK_RECONCILE.TAX_RECORD_ID = DIT.TAX_RECORD_ID)
         and DIT.TAX_RECORD_ID = DEFERRED_INCOME_TAX_TRANSFER.TAX_RECORD_ID(+)
         and DIT.TAX_YEAR = DEFERRED_INCOME_TAX_TRANSFER.TAX_YEAR(+)
         and DIT.TAX_MONTH = DEFERRED_INCOME_TAX_TRANSFER.TAX_MONTH(+)
         and DIT.NORMALIZATION_ID = DEFERRED_INCOME_TAX_TRANSFER.NORMALIZATION_ID(+)
         and DIT.TIME_SLICE_ID = DEFERRED_INCOME_TAX_TRANSFER.TIME_SLICE_ID(+)
         and NORMALIZATION_SCHEMA.JURISDICTION_ID = JS.JURISDICTION_ID
         and TAX_BOOK_RECONCILE.TAX_INCLUDE_ID = TAX_BOOK_RECONCILE_TRANSFER.TAX_INCLUDE_ID(+)
         and TAX_BOOK_RECONCILE.TAX_YEAR = TAX_BOOK_RECONCILE_TRANSFER.TAX_YEAR(+)
         and TAX_BOOK_RECONCILE.TAX_RECORD_ID = TAX_BOOK_RECONCILE_TRANSFER.TAX_RECORD_ID(+)
         and TAX_BOOK_RECONCILE.RECONCILE_ITEM_ID =
             TAX_BOOK_RECONCILE_TRANSFER.RECONCILE_ITEM_ID(+)
         and DIT.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and (NORMALIZATION_SCHEMA.NORMALIZATION_ID in (select * from table(G_TABLE_NORM_IDS)) or
             G_TABLE_NORM_COUNT = 0)
         and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and (NORMALIZATION_SCHEMA.AMORTIZATION_TYPE_ID = -1)
         and TRC.VERSION_ID = G_VERSION
       group by NORMALIZATION_SCHEMA.JURISDICTION_ID,
                DIT.TAX_RECORD_ID,
                DIT.TAX_YEAR,
                JS.TAX_BOOK_ID;

   type TYPE_TAX_BASIS_AMOUNT_REC is table of TAX_BASIS_AMOUNT_CUR%rowtype;

   type TAX_BASIS_AMOUNT_STRUCT_REC is record(
      JURISDICTION_ID           number(22, 0),
      TAX_BOOK_ID               number(22, 0),
      TAX_RECORD_ID             number(22, 0),
      TAX_YEAR                  number(24, 2),
      TAX_ONLY_NORM_DIFF_AMOUNT float,
      ORIG_DIFF                 float,
      BASIS_AMOUNT_BEG          float,
      BASIS_AMOUNT_END          float);

   --type type_tax_basis_amount_rec is table of TAX_BASIS_AMOUNT_STRUCT_REC;

   G_TAX_BASIS_AMOUNT_REC TYPE_TAX_BASIS_AMOUNT_REC;

   cursor DEF_INCOME_TAX_TRANSFERS_CUR is
      select sum(DT.NORM_DIFF_BALANCE_BEG) NORM_DIFF_BALANCE_BEG,
             sum(DT.DEF_INCOME_TAX_BALANCE_BEG) DEF_INCOME_TAX_BALANCE_BEG,
             DT.TAX_RECORD_ID,
             DT.TAX_YEAR,
             DT.NORMALIZATION_ID
        from DEFERRED_INCOME_TAX_TRANSFER DT
       where (DT.NORMALIZATION_ID in (select * from table(G_TABLE_NORM_IDS)) or
             G_TABLE_NORM_COUNT = 0)
         and DT.TAX_YEAR between G_START_YEAR and G_END_YEAR
       group by DT.TAX_RECORD_ID, DT.TAX_YEAR, DT.NORMALIZATION_ID;

   type TYPE_DEF_INCOME_TAX_TRANS_REC is table of DEF_INCOME_TAX_TRANSFERS_CUR%rowtype;

   type DEF_INCOME_TAX_TRANS_STR_REC is record(
      NORM_DIFF_BALANCE_BEG      number(22, 2),
      DEF_INCOME_TAX_BALANCE_BEG number(22, 2),
      TAX_RECORD_ID              number(22, 0),
      TAX_YEAR                   number(22, 2),
      NORMALIZATION_ID           number(22, 0));

   --type type_def_income_tax_trans_rec is table of  def_income_tax_trans_str_REC;

   G_DEF_INCOME_TAX_TRANSFERS_REC TYPE_DEF_INCOME_TAX_TRANS_REC;

   cursor TAX_DETAIL_CUR /*(a_tax_basis_amount_rec type_tax_basis_amount_rec,a_def_income_tax_trans  type_def_income_tax_trans_rec) */
   is
      select TDA.ACCUM_RESERVE - TDB.ACCUM_RESERVE DIFF_ACCUM_RESERVE,
             NS.NORM_FROM_TAX,
             NS.NORM_TO_TAX,
             TRC.TAX_CLASS_ID,
             TRC.VINTAGE_ID,
             TO_NUMBER(TO_CHAR(NVL(TRC.IN_SERVICE_MONTH, TO_DATE('111111', 'YYMMDD')), 'YYYYMMDD')) IN_SERVICE_MONTH,
             DFI.DEF_INCOME_TAX_BALANCE_BEG,
             DFI.DEF_INCOME_TAX_PROVISION,
             DFI.DEF_INCOME_TAX_REVERSAL,
             DFI.ARAM_RATE,
             DFI.DEF_INCOME_TAX_BALANCE_END,
             DFI.NORM_DIFF_BALANCE_BEG,
             DFI.NORM_DIFF_BALANCE_END,
             DFI.LIFE,
             DFI.DEF_INCOME_TAX_ADJUST,
             DFI.DEF_INCOME_TAX_RETIRE,
             DFI.DEF_INCOME_TAX_GAIN_LOSS,
             DFI.ARAM_RATE_END,
             TDA.DEPRECIATION DA_DEPRECIATION,
             TDA.GAIN_LOSS DA_GAIN_LOSS,
             TDA.COR_EXPENSE DA_COR_EXPENSE,
             TDA.RETIREMENTS DA_RETIREMENTS,
             TDA.EXTRAORDINARY_RETIRES DA_EXTRAORDINARY_RETIRES,
             TDA.CAPITALIZED_DEPR DA_CAPITALIZED_DEPR,
             TDB.DEPRECIATION DB_DEPRECIATION,
             TDB.GAIN_LOSS DB_GAIN_LOSS,
             TDB.COR_EXPENSE DB_COR_EXPENSE,
             TDB.CAPITALIZED_DEPR DB_CAPITALIZED_DEPR,
             TRC.COMPANY_ID,
             DFI.TAX_YEAR,
             NS.JURISDICTION_ID,
             NS.DEF_INCOME_TAX_RATE_ID,
             DFI.TAX_RECORD_ID,
             DFI.TAX_MONTH,
             DFI.NORMALIZATION_ID NORMALIZATION_ID,
             DFI.GAIN_LOSS_DEF_TAX_BALANCE,
             TC.TAX_RATE_ID,
             V.YEAR VINTAGE_YEAR,
             TDA.TAX_BOOK_ID,
             DFI.GAIN_LOSS_DEF_TAX_BALANCE_END,
             DFI.BASIS_DIFF_ADD_RET,
             NS.COST_OF_REMOVAL_FT,
             NS.CAP_DEPR_IND,
             0 DIT_RATE,
             DECODE(TDA.TAX_BALANCE_END,
                    0,
                    -DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID,
                            0,
                            TTB.TAX_ONLY_NORM_DIFF_AMOUNT,
                            0) +
                    DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID, 0, TTB.ORIG_DIFF, 0),
                    - (((TDA.TAX_BALANCE_END - TDA.ACCUM_RESERVE_END) / TDA.TAX_BALANCE_END) *
                      BASIS_AMOUNT_END) - DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID,
                            0,
                            TTB.TAX_ONLY_NORM_DIFF_AMOUNT,
                            0) +
                     DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID, 0, TTB.ORIG_DIFF, 0)) AMOUNT_TO_AMORTIZE,
             NVL(TDITT.DEF_INCOME_TAX_BALANCE_BEG, 0) DEF_INCOME_TAX_BAL_TRANSFER,
             NVL(TDITT.NORM_DIFF_BALANCE_BEG, 0) NORM_DIFF_BALANCE_TRANSFER,
             TDA.TAX_BALANCE TAX_BALANCE_FROM,
             TDA.ACCUM_RESERVE ACCUM_RESERVE_FROM,
             NO_ZERO_CHECK,
             TAX_DEPR_ADJUST_A.ACCUM_RESERVE_ADJUST ACCUM_RESERVE_ADJUST_A,
             TAX_DEPR_ADJUST_B.ACCUM_RESERVE_ADJUST ACCUM_RESERVE_ADJUST_B
        from NORMALIZATION_SCHEMA NS,
             TAX_RECORD_CONTROL TRC,
             TAX_DEPRECIATION TDA,
             TAX_DEPRECIATION TDB,
             AMORTIZATION_TYPE ATT,
             TAX_DEPR_ADJUST TAX_DEPR_ADJUST_A,
             TAX_DEPR_ADJUST TAX_DEPR_ADJUST_B,
             DEFERRED_RATES DR,
             TAX_CONTROL TC,
             VINTAGE V,
             (select DEFERRED_INCOME_TAX.*,
                     NORMALIZATION_SCHEMA.JURISDICTION_ID,
                     NORMALIZATION_SCHEMA.NORM_FROM_TAX
                from DEFERRED_INCOME_TAX, NORMALIZATION_SCHEMA, TAX_RECORD_CONTROL TRC2
               where DEFERRED_INCOME_TAX.TAX_YEAR between G_START_YEAR and G_END_YEAR
                 and TRC2.VERSION_ID = G_VERSION
                 and DEFERRED_INCOME_TAX.TAX_RECORD_ID = TRC2.TAX_RECORD_ID
                 and DEFERRED_INCOME_TAX.NORMALIZATION_ID = NORMALIZATION_SCHEMA.NORMALIZATION_ID) DFI,
             --table(g_tax_basis_amount_rec) ttb,
             TEMP_GLOBAL_TAX_BASIS_AMOUNT TTB, -- temp_tax_basis_amount
             --table( g_def_income_tax_transfers_rec) tditt
             TEMP_G_DEF_INCOME_TAX_TRANS TDITT -- temp_def_income_tax_transfers
       where (NS.NORM_FROM_TAX = TDA.TAX_BOOK_ID)
         and NS.NORM_TO_TAX = TDB.TAX_BOOK_ID
         and NS.NORM_FROM_TAX = TAX_DEPR_ADJUST_A.TAX_BOOK_ID
         and NS.NORM_TO_TAX = TAX_DEPR_ADJUST_B.TAX_BOOK_ID
         and NS.AMORTIZATION_TYPE_ID = 1
         and (NS.NORMALIZATION_ID in (select * from table(G_TABLE_NORM_IDS)) or
             G_TABLE_NORM_COUNT = 0)
         and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and TDA.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and (DFI.JURISDICTION_ID = TTB.JURISDICTION_ID(+))
         and (DFI.NORM_FROM_TAX = TTB.TAX_BOOK_ID(+))
         and (DFI.TAX_RECORD_ID = TTB.TAX_RECORD_ID(+))
         and DFI.TAX_YEAR = TTB.TAX_YEAR(+)
         and DFI.TAX_RECORD_ID = TAX_DEPR_ADJUST_A.TAX_RECORD_ID
         and DFI.TAX_RECORD_ID = TAX_DEPR_ADJUST_B.TAX_RECORD_ID
         and (DFI.TAX_YEAR = TDITT.TAX_YEAR(+))
         and (DFI.TAX_RECORD_ID = TDITT.TAX_RECORD_ID(+))
         and (DFI.NORMALIZATION_ID = TDITT.NORMALIZATION_ID(+))
         and (NS.NORM_TO_TAX = TDB.TAX_BOOK_ID)
         and (NS.NORMALIZATION_ID = DFI.NORMALIZATION_ID)
         and (DFI.TAX_RECORD_ID = TRC.TAX_RECORD_ID)
         and (DFI.TAX_RECORD_ID = TDA.TAX_RECORD_ID)
         and (DFI.TAX_RECORD_ID = TDB.TAX_RECORD_ID)
         and (NS.AMORTIZATION_TYPE_ID = ATT.AMORTIZATION_TYPE_ID)
         and (NS.DEF_INCOME_TAX_RATE_ID = DR.DEF_INCOME_TAX_RATE_ID)
         and (TC.TAX_RECORD_ID = TDA.TAX_RECORD_ID)
         and (TC.TAX_BOOK_ID = TDA.TAX_BOOK_ID)
         and (V.VINTAGE_ID = TRC.VINTAGE_ID)
         and (DFI.TAX_YEAR = TDA.TAX_YEAR)
         and (TDB.TAX_YEAR = TDA.TAX_YEAR)
         and (DFI.TAX_YEAR = TAX_DEPR_ADJUST_A.TAX_YEAR)
         and (DFI.TAX_YEAR = TAX_DEPR_ADJUST_B.TAX_YEAR)
         and DFI.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and TRC.VERSION_ID = G_VERSION
       order by TRC.TAX_RECORD_ID, NORMALIZATION_ID, DFI.TAX_MONTH, TDA.TAX_YEAR;

   type TYPE_TAX_DETAIL_REC is varray(10040) of TAX_DETAIL_CUR%rowtype;

   type TYPE_TAX_DETAIL_SAVE_REC is varray(40) of TAX_DETAIL_CUR%rowtype;

   G_TAX_DETAIL_REC       TYPE_TAX_DETAIL_REC;
   G_TAX_DETAIL_SAVE2_REC TYPE_TAX_DETAIL_SAVE_REC;
   G_TAX_DETAIL_SAVE1_REC TYPE_TAX_DETAIL_SAVE_REC;

   G_TAX_DEPR_LAST_RECORD_ID number(22, 0);
   G_TAX_DEPR_LAST_NORM_ID   number(22, 0);

   cursor DFIT_RATES_CUR is
      select DEF_INCOME_TAX_RATE_ID,
             TO_NUMBER(TO_CHAR(EFFECTIVE_DATE, 'YYYYMMDD')) EFFECTIVE_DATE,
             MONTHLY_RATE,
             ANNUAL_RATE
        from DEFERRED_INCOME_TAX_RATES
       order by DEF_INCOME_TAX_RATE_ID, EFFECTIVE_DATE asc;

   type TYPE_DFIT_RATES_REC is table of DFIT_RATES_CUR%rowtype;

   G_DFIT_RATES_REC TYPE_DFIT_RATES_REC;

   cursor TAX_RATES_CUR is
      select TR.TAX_RATE_ID,
             RATE,
             TR.YEAR,
             NET_GROSS,
             LIFE,
             REMAINING_LIFE_PLAN,
             START_METHOD,
             ROUNDING_CONVENTION,
             SWITCHED_YEAR
        from TAX_RATES TR, TAX_RATE_CONTROL TRC
       where TR.TAX_RATE_ID = TRC.TAX_RATE_ID
       order by TR.TAX_RATE_ID, TR.YEAR;

   type TYPE_TAX_RATES_REC is table of TAX_RATES_CUR%rowtype;

   G_TAX_RATES_REC TYPE_TAX_RATES_REC;

   cursor DEF_TAX_BASIS_CUR is
      select TD.TAX_BALANCE_END,
             TD.ACCUM_RESERVE_END,
             NS.NORM_FROM_TAX,
             NS.NORM_TO_TAX,
             TRC.TAX_CLASS_ID,
             TRC.VINTAGE_ID,
             TO_NUMBER(TO_CHAR(NVL(TRC.IN_SERVICE_MONTH, TO_DATE('111111', 'YYMMDD')), 'YYYYMMDD')),
             DFI.DEF_INCOME_TAX_BALANCE_BEG,
             DFI.DEF_INCOME_TAX_PROVISION,
             DFI.DEF_INCOME_TAX_REVERSAL,
             DFI.ARAM_RATE,
             DFI.DEF_INCOME_TAX_BALANCE_END,
             DFI.NORM_DIFF_BALANCE_BEG,
             DFI.NORM_DIFF_BALANCE_END,
             DFI.LIFE,
             DFI.DEF_INCOME_TAX_ADJUST,
             DFI.DEF_INCOME_TAX_RETIRE,
             DFI.DEF_INCOME_TAX_GAIN_LOSS,
             DFI.ARAM_RATE_END,
             TRC.COMPANY_ID,
             DFI.TAX_YEAR,
             NS.JURISDICTION_ID,
             NS.DEF_INCOME_TAX_RATE_ID,
             DFI.TAX_RECORD_ID,
             DFI.TAX_MONTH,
             DFI.NORMALIZATION_ID,
             DFI.GAIN_LOSS_DEF_TAX_BALANCE,
             V.YEAR VINTAGE_YEAR,
             TIA.TAX_BOOK_ID,
             DFI.GAIN_LOSS_DEF_TAX_BALANCE_END,
             TBR.BASIS_AMOUNT_BEG,
             TBR.BASIS_AMOUNT_END,
             TBR.BASIS_AMOUNT_TRANSFER,
             TBR.BASIS_AMOUNT_ACTIVITY,
             NS.RECONCILE_ITEM_ID,
             DFI.BASIS_DIFF_ADD_RET,
             DFI.INPUT_AMORTIZATION,
             NS.BOOK_DEPR_ALLOC_IND,
             NS.BASIS_DIFF_RETIRE_REVERSAL,
             TD.NUMBER_MONTHS_BEG,
             1 DIT_RATE,
             NS.AMORTIZATION_TYPE_ID,
             NVL(TDITT.DEF_INCOME_TAX_BALANCE_BEG, 0) DEF_INCOME_TAX_BAL_TRANSFER,
             NVL(TDITT.NORM_DIFF_BALANCE_BEG, 0) NORM_DIFF_BALANCE_TRANSFER,
             NS.BASIS_DIFF_ACTIVITY_SPLIT
        from TAX_DEPRECIATION            TD,
             NORMALIZATION_SCHEMA        NS,
             TAX_RECORD_CONTROL          TRC,
             DEFERRED_INCOME_TAX         DFI,
             TAX_BOOK_RECONCILE          TBR,
             AMORTIZATION_TYPE           AT,
             TAX_INCLUDE_ACTIVITY        TIA,
             DEFERRED_RATES              DR,
             VINTAGE                     V,
             TEMP_G_DEF_INCOME_TAX_TRANS TDITT -- temp_def_income_tax_transfers
       where (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
             G_TABLE_VINTAGE_COUNT = 0)
         and (NS.NORMALIZATION_ID in (select * from table(G_TABLE_NORM_IDS)) or
             G_TABLE_NORM_COUNT = 0)
         and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
             G_TABLE_CLASS_COUNT = 0)
         and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
             G_TABLE_COMPANY_COUNT = 0)
         and TBR.TAX_YEAR between G_START_YEAR and G_END_YEAR
         and (DFI.NORMALIZATION_ID = TDITT.NORMALIZATION_ID(+))
         and (DFI.TAX_RECORD_ID = TDITT.TAX_RECORD_ID(+))
         and (DFI.TAX_YEAR = TDITT.TAX_YEAR(+))
         and (NS.NORMALIZATION_ID = DFI.NORMALIZATION_ID)
         and (DFI.TAX_RECORD_ID = TRC.TAX_RECORD_ID)
         and (NS.AMORTIZATION_TYPE_ID = AT.AMORTIZATION_TYPE_ID)
         and (NS.DEF_INCOME_TAX_RATE_ID = DR.DEF_INCOME_TAX_RATE_ID)
         and (V.VINTAGE_ID = TRC.VINTAGE_ID)
         and (TBR.TAX_INCLUDE_ID = TIA.TAX_INCLUDE_ID)
         and (NS.RECONCILE_ITEM_ID = TBR.RECONCILE_ITEM_ID)
         and (TRC.TAX_RECORD_ID = TBR.TAX_RECORD_ID)
         and (DFI.TAX_YEAR = TBR.TAX_YEAR)
         and (TD.TAX_RECORD_ID = DFI.TAX_RECORD_ID)
         and (TD.TAX_YEAR = DFI.TAX_YEAR)
         and (TD.TAX_BOOK_ID = NS.NORM_FROM_TAX)
         and (NS.AMORTIZATION_TYPE_ID <= 0)
         and (NS.NORM_FROM_TAX = TIA.TAX_BOOK_ID)
         and TRC.VERSION_ID = G_VERSION
       order by TD.TAX_RECORD_ID, DFI.NORMALIZATION_ID, DFI.TAX_MONTH, TBR.TAX_YEAR;

   type TYPE_DEF_TAX_BASIS_REC is varray(10040) of DEF_TAX_BASIS_CUR%rowtype;

   type TYPE_DEF_TAX_BASIS_SAVE_REC is varray(40) of DEF_TAX_BASIS_CUR%rowtype;

   G_DEF_TAX_BASIS_REC       TYPE_DEF_TAX_BASIS_REC;
   G_DEF_TAX_BASIS_SAVE2_REC TYPE_DEF_TAX_BASIS_SAVE_REC;
   G_DEF_TAX_BASIS_SAVE1_REC TYPE_DEF_TAX_BASIS_SAVE_REC;

   function GET_VERSION return varchar2;

   function CALC_DFIT(A_JOB_NO number) return integer;

   function CALC(A_TAX_DETAIL_INDEX      pls_integer,
                 A_TAX_RATES_SUB_REC     TYPE_TAX_RATES_REC,
                 A_TAX_RATES_ROWS        pls_integer,
                 A_TAX_DETAIL_BALS_INDEX pls_integer,
                 A_DFIT_RATES            TYPE_DFIT_RATES_REC,
                 A_DFIT_RATES_ROWS       pls_integer) return number;

   function BASIS_CALC(A_DEF_TAX_INDEX         pls_integer,
                       A_DEF_TAX_RATES_SUB_REC TYPE_DFIT_RATES_REC,
                       A_DEF_TAX_RATES_ROWS    pls_integer,
                       A_DEF_TAX_BALS_INDEX    pls_integer,
                       A_DEF_TAX_BALS_ROWS     pls_integer,
                       A_SHORT_MONTHS          pls_integer) return pls_integer;

   function UPDATE_DEFERRED_INCOME_TAX(A_START_ROW pls_integer,
                                       A_NUM_REC   pls_integer) return number;

   function UPDATE_DEF_TAX_BASIS(A_START_ROW pls_integer,
                                 A_NUM_REC   pls_integer) return number;

   procedure SET_SESSION_PARAMETER;

   procedure WRITE_LOG(A_JOB_NO     number,
                       A_ERROR_TYPE number,
                       A_CODE       number,
                       A_MSG        varchar2);

   G_JOB_NO  number(22, 0);
   G_LINE_NO number(22, 0);
end TAX_DFIT_CALC;
/


create or replace package body TAX_DFIT_CALC is

   /* function get_basis_amount return tax_basis_amount_cur%rowtype pipelined is
   l_tax_basis_table  type_tax_basis_amount_rec;
   begin

     FETCH tax_basis_amount_cur BULK COLLECT INTO  l_tax_basis_table;
     for i in 1..l_tax_basis_table.count loop
        pipe row(l_tax_basis_table(i));
     end loop;


   end get_basis_amount;*/

   -- =============================================================================
   --  Function GET_VERSION
   -- =============================================================================
   function GET_VERSION return varchar2 is
   begin
      return '5.10';
   end GET_VERSION;

   -- =============================================================================
   --  Function GET_TAX_DETAIL
   -- =============================================================================
   function GET_TAX_DETAIL(A_START_ROW out pls_integer) return pls_integer is
      L_CODE  pls_integer;
      L_SQL   varchar(4000);
      L_COUNT pls_integer;
      I       pls_integer;
      K       pls_integer;
      J       pls_integer;
      type TAX_BASIS_AMOUNT_STRUCT_REC is record(
         JURISDICTION_ID           number(22, 0),
         TAX_BOOK_ID               number(22, 0),
         TAX_RECORD_ID             number(22, 0),
         TAX_YEAR                  number(24, 2),
         TAX_ONLY_NORM_DIFF_AMOUNT float,
         ORIG_DIFF                 float,
         BASIS_AMOUNT_BEG          float,
         BASIS_AMOUNT_END          float);
      L_MOVE_ROW_COUNT              pls_integer;
      L_SAVE_COUNT                  integer;
      L_CURRENT_RECORD_ID           pls_integer;
      L_CURRENT_TAX_MONTH           pls_integer;
      L_CURRENT_NORM_ID             pls_integer;
      L_YEAR_COUNT                  pls_integer;
      L_TAX_DET_LAST_SAVE_RECORD_ID pls_integer;
      L_TAX_DET_LAST_SAVE_TAX_MONTH pls_integer;
      L_TAX_DET_LAST_SAVE_NORM_ID   pls_integer;

      L_TAX_BASIS_AMOUNT_REC TYPE_TAX_BASIS_AMOUNT_REC;
      type TAX_BASIS_AMOUNT_TYPE is table of TAX_BASIS_AMOUNT_STRUCT_REC;
      L_TAX_BASIS_TABLE          TAX_BASIS_AMOUNT_TYPE;
      L_DEF_INCOME_TAX_TRANS_REC TYPE_DEF_INCOME_TAX_TRANS_REC;
      cursor L_TAX_DETAIL_CUR(A_TAX_BASIS_AMOUNT_REC TYPE_TAX_BASIS_AMOUNT_REC,
                              A_DEF_INCOME_TAX_TRANS TYPE_DEF_INCOME_TAX_TRANS_REC) is
         select TDA.ACCUM_RESERVE - TDB.ACCUM_RESERVE DIFF_ACCUM_RESERVE,
                NS.NORM_FROM_TAX,
                NS.NORM_TO_TAX,
                TRC.TAX_CLASS_ID,
                TRC.VINTAGE_ID,
                TO_NUMBER(TO_CHAR(NVL(TRC.IN_SERVICE_MONTH, TO_DATE('111111', 'YYMMDD')),
                                  'YYYYMMDD')) IN_SERVICE_MONTH,
                DFI.DEF_INCOME_TAX_BALANCE_BEG,
                DFI.DEF_INCOME_TAX_PROVISION,
                DFI.DEF_INCOME_TAX_REVERSAL,
                DFI.ARAM_RATE,
                DFI.DEF_INCOME_TAX_BALANCE_END,
                DFI.NORM_DIFF_BALANCE_BEG,
                DFI.NORM_DIFF_BALANCE_END,
                DFI.LIFE,
                DFI.DEF_INCOME_TAX_ADJUST,
                DFI.DEF_INCOME_TAX_RETIRE,
                DFI.DEF_INCOME_TAX_GAIN_LOSS,
                DFI.ARAM_RATE_END,
                TDA.DEPRECIATION DA_DEPRECIATION,
                TDA.GAIN_LOSS DA_GAIN_LOSS,
                TDA.COR_EXPENSE DA_COR_EXPENSE,
                TDA.RETIREMENTS DA_RETIREMENTS,
                TDA.EXTRAORDINARY_RETIRES DA_EXTRAORDINARY_RETIRES,
                TDA.CAPITALIZED_DEPR DA_CAPITALIZED_DEPR,
                TDB.DEPRECIATION DB_DEPRECIATION,
                TDB.GAIN_LOSS DB_GAIN_LOSS,
                TDB.COR_EXPENSE DB_COR_EXPENSE,
                TDB.CAPITALIZED_DEPR DB_CAPITALIZED_DEPR,
                TRC.COMPANY_ID,
                DFI.TAX_YEAR,
                NS.JURISDICTION_ID,
                NS.DEF_INCOME_TAX_RATE_ID,
                DFI.TAX_RECORD_ID,
                DFI.TAX_MONTH,
                DFI.NORMALIZATION_ID NORMALIZATION_ID,
                DFI.GAIN_LOSS_DEF_TAX_BALANCE,
                TC.TAX_RATE_ID,
                V.YEAR VINTAGE_YEAR,
                TDA.TAX_BOOK_ID,
                DFI.GAIN_LOSS_DEF_TAX_BALANCE_END,
                DFI.BASIS_DIFF_ADD_RET,
                NS.COST_OF_REMOVAL_FT,
                NS.CAP_DEPR_IND,
                0 DIT_RATE,
                DECODE(TDA.TAX_BALANCE_END,
                       0,
                       -DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID,
                               0,
                               TTB.TAX_ONLY_NORM_DIFF_AMOUNT,
                               0) +
                       DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID, 0, TTB.ORIG_DIFF, 0),
                       - (((TDA.TAX_BALANCE_END - TDA.ACCUM_RESERVE_END) / TDA.TAX_BALANCE_END) *
                         BASIS_AMOUNT_END) - DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID,
                               0,
                               TTB.TAX_ONLY_NORM_DIFF_AMOUNT,
                               0) +
                        DECODE(NS.JURISDICTION_ID - TTB.JURISDICTION_ID, 0, TTB.ORIG_DIFF, 0)) AMOUNT_TO_AMORTIZE,
                NVL(TDITT.DEF_INCOME_TAX_BALANCE_BEG, 0) DEF_INCOME_TAX_BAL_TRANSFER,
                NVL(TDITT.NORM_DIFF_BALANCE_BEG, 0) NORM_DIFF_BALANCE_TRANSFER,
                TDA.TAX_BALANCE TAX_BALANCE_FROM,
                TDA.ACCUM_RESERVE ACCUM_RESERVE_FROM,
                NO_ZERO_CHECK,
                TAX_DEPR_ADJUST_A.ACCUM_RESERVE_ADJUST ACCUM_RESERVE_ADJUST_A,
                TAX_DEPR_ADJUST_B.ACCUM_RESERVE_ADJUST ACCUM_RESERVE_ADJUST_B
           from NORMALIZATION_SCHEMA NS,
                TAX_RECORD_CONTROL TRC,
                TAX_DEPRECIATION TDA,
                TAX_DEPRECIATION TDB,
                AMORTIZATION_TYPE ATT,
                TAX_DEPR_ADJUST TAX_DEPR_ADJUST_A,
                TAX_DEPR_ADJUST TAX_DEPR_ADJUST_B,
                DEFERRED_RATES DR,
                TAX_CONTROL TC,
                VINTAGE V,
                (select DEFERRED_INCOME_TAX.*,
                        NORMALIZATION_SCHEMA.JURISDICTION_ID,
                        NORMALIZATION_SCHEMA.NORM_FROM_TAX
                   from DEFERRED_INCOME_TAX, NORMALIZATION_SCHEMA, TAX_RECORD_CONTROL TRC2
                  where DEFERRED_INCOME_TAX.TAX_YEAR between G_START_YEAR and G_END_YEAR
                    and TRC2.VERSION_ID = G_VERSION
                    and DEFERRED_INCOME_TAX.TAX_RECORD_ID = TRC2.TAX_RECORD_ID
                    and DEFERRED_INCOME_TAX.NORMALIZATION_ID = NORMALIZATION_SCHEMA.NORMALIZATION_ID) DFI,
                -- table(a_tax_basis_amount_rec) ttb,
                TEMP_GLOBAL_TAX_BASIS_AMOUNT TTB, -- temp_tax_basis_amount
                -- table(a_def_income_tax_trans) tditt
                TEMP_G_DEF_INCOME_TAX_TRANS TDITT -- temp_def_income_tax_transfers
          where (NS.NORM_FROM_TAX = TDA.TAX_BOOK_ID)
            and NS.NORM_TO_TAX = TDB.TAX_BOOK_ID
            and NS.NORM_FROM_TAX = TAX_DEPR_ADJUST_A.TAX_BOOK_ID
            and NS.NORM_TO_TAX = TAX_DEPR_ADJUST_B.TAX_BOOK_ID
            and NS.AMORTIZATION_TYPE_ID = 1
            and (NS.NORMALIZATION_ID in (select * from table(G_TABLE_NORM_IDS)) or
                G_TABLE_NORM_COUNT = 0)
            and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
                G_TABLE_VINTAGE_COUNT = 0)
            and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
                G_TABLE_CLASS_COUNT = 0)
            and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
                G_TABLE_COMPANY_COUNT = 0)
            and TDA.TAX_YEAR between G_START_YEAR and G_END_YEAR
            and (DFI.JURISDICTION_ID = TTB.JURISDICTION_ID(+))
            and (DFI.NORM_FROM_TAX = TTB.TAX_BOOK_ID(+))
            and (DFI.TAX_RECORD_ID = TTB.TAX_RECORD_ID(+))
            and DFI.TAX_YEAR = TTB.TAX_YEAR(+)
            and DFI.TAX_RECORD_ID = TAX_DEPR_ADJUST_A.TAX_RECORD_ID
            and DFI.TAX_RECORD_ID = TAX_DEPR_ADJUST_B.TAX_RECORD_ID
            and (DFI.TAX_YEAR = TDITT.TAX_YEAR(+))
            and (DFI.TAX_RECORD_ID = TDITT.TAX_RECORD_ID(+))
            and (DFI.NORMALIZATION_ID = TDITT.NORMALIZATION_ID(+))
            and (NS.NORM_TO_TAX = TDB.TAX_BOOK_ID)
            and (NS.NORMALIZATION_ID = DFI.NORMALIZATION_ID)
            and (DFI.TAX_RECORD_ID = TRC.TAX_RECORD_ID)
            and (DFI.TAX_RECORD_ID = TDA.TAX_RECORD_ID)
            and (DFI.TAX_RECORD_ID = TDB.TAX_RECORD_ID)
            and (NS.AMORTIZATION_TYPE_ID = ATT.AMORTIZATION_TYPE_ID)
            and (NS.DEF_INCOME_TAX_RATE_ID = DR.DEF_INCOME_TAX_RATE_ID)
            and (TC.TAX_RECORD_ID = TDA.TAX_RECORD_ID)
            and (TC.TAX_BOOK_ID = TDA.TAX_BOOK_ID)
            and (V.VINTAGE_ID = TRC.VINTAGE_ID)
            and (DFI.TAX_YEAR = TDA.TAX_YEAR)
            and (TDB.TAX_YEAR = TDA.TAX_YEAR)
            and (DFI.TAX_YEAR = TAX_DEPR_ADJUST_A.TAX_YEAR)
            and (DFI.TAX_YEAR = TAX_DEPR_ADJUST_B.TAX_YEAR)
            and DFI.TAX_YEAR between G_START_YEAR and G_END_YEAR
            and TRC.VERSION_ID = G_VERSION
          order by TDA.TAX_YEAR;

   begin

      if not TAX_DETAIL_CUR%isopen then
         L_SQL := 'drop table temp_tax_basis_amount';
         --execute immediate l_sql;
         L_SQL := ' create table temp_tax_basis_amount  as SELECT normalization_schema.jurisdiction_id, ' ||
                  '     js.tax_book_id,dit.tax_record_id tax_record_id, dit.tax_year tax_year, ' ||
                  '      sum(dit.norm_diff_balance_beg + ' ||
                  '         nvl(deferred_income_tax_transfer.norm_diff_balance_beg,0) ) tax_only_norm_diff_amount, ' ||
                  '      sum(DECODE(basis_amount_activity, 0, 0, - basis_diff_add_ret)) orig_diff, ' ||
                  '      sum(tax_book_reconcile.basis_amount_beg + ' ||
                  '        nvl(tax_book_reconcile_transfer.basis_amount_beg,0)) basis_amount_beg, ' ||
                  '      sum(tax_book_reconcile.basis_amount_end) basis_amount_end   ' ||
                  '    FROM deferred_income_tax dit, ' || '        normalization_schema , ' ||
                  '        jurisdiction js, ' || '        tax_book_reconcile , ' ||
                  '        tax_record_control trc , ' || '        tax_include_activity, ' ||
                  '        (SELECT tax_record_id, ' || '           tax_year, ' ||
                  '            tax_month, ' || '          normalization_id, ' ||
                  '            time_slice_id, ' ||
                  '          sum(def_income_tax_balance_beg) def_income_tax_balance_beg, ' ||
                  '          sum(norm_diff_balance_beg) norm_diff_balance_beg         ' ||
                  '           FROM deferred_income_tax_transfer        ' ||
                  '            GROUP BY         tax_record_id, ' || '              tax_year, ' ||
                  '              tax_month, ' || '              normalization_id, ' ||
                  '              time_slice_id) deferred_income_tax_transfer, ' ||
                  '      (SELECT tax_include_id, ' || '          tax_year, ' ||
                  '            tax_record_id, ' || '            reconcile_item_id, ' ||
                  '            sum(basis_amount_beg) basis_amount_beg         ' ||
                  '          FROM tax_book_reconcile_transfer         ' ||
                  '                  GROUP BY          tax_include_id, ' ||
                  '                tax_year, ' || '                tax_record_id, ' ||
                  '                reconcile_item_id) tax_book_reconcile_transfer          ' ||
                  '     WHERE 1= 0 ' || ' GROUP BY normalization_schema.jurisdiction_id,  ' ||
                  '       dit.tax_record_id,dit.tax_year,js.tax_book_id ';
         -- execute immediate l_sql;

         insert into TEMP_GLOBAL_TAX_BASIS_AMOUNT
            select NORMALIZATION_SCHEMA.JURISDICTION_ID,
                   JS.TAX_BOOK_ID,
                   DIT.TAX_RECORD_ID TAX_RECORD_ID,
                   DIT.TAX_YEAR TAX_YEAR,
                   sum(DIT.NORM_DIFF_BALANCE_BEG +
                       NVL(DEFERRED_INCOME_TAX_TRANSFER.NORM_DIFF_BALANCE_BEG, 0)) TAX_ONLY_NORM_DIFF_AMOUNT,
                   sum(DECODE(BASIS_AMOUNT_ACTIVITY, 0, 0, -BASIS_DIFF_ADD_RET)) ORIG_DIFF,
                   sum(TAX_BOOK_RECONCILE.BASIS_AMOUNT_BEG +
                       NVL(TAX_BOOK_RECONCILE_TRANSFER.BASIS_AMOUNT_BEG, 0)) BASIS_AMOUNT_BEG,
                   sum(TAX_BOOK_RECONCILE.BASIS_AMOUNT_END) BASIS_AMOUNT_END
              from DEFERRED_INCOME_TAX DIT,
                   NORMALIZATION_SCHEMA,
                   JURISDICTION JS,
                   TAX_BOOK_RECONCILE,
                   TAX_RECORD_CONTROL TRC,
                   TAX_INCLUDE_ACTIVITY,
                   (select TAX_RECORD_ID,
                           TAX_YEAR,
                           TAX_MONTH,
                           NORMALIZATION_ID,
                           TIME_SLICE_ID,
                           sum(DEF_INCOME_TAX_BALANCE_BEG) DEF_INCOME_TAX_BALANCE_BEG,
                           sum(NORM_DIFF_BALANCE_BEG) NORM_DIFF_BALANCE_BEG
                      from DEFERRED_INCOME_TAX_TRANSFER
                     group by TAX_RECORD_ID, TAX_YEAR, TAX_MONTH, NORMALIZATION_ID, TIME_SLICE_ID) DEFERRED_INCOME_TAX_TRANSFER,
                   (select TAX_INCLUDE_ID,
                           TAX_YEAR,
                           TAX_RECORD_ID,
                           RECONCILE_ITEM_ID,
                           sum(BASIS_AMOUNT_BEG) BASIS_AMOUNT_BEG
                      from TAX_BOOK_RECONCILE_TRANSFER
                     group by TAX_INCLUDE_ID, TAX_YEAR, TAX_RECORD_ID, RECONCILE_ITEM_ID) TAX_BOOK_RECONCILE_TRANSFER
             where (DIT.NORMALIZATION_ID = NORMALIZATION_SCHEMA.NORMALIZATION_ID)
               and (NORMALIZATION_SCHEMA.RECONCILE_ITEM_ID = TAX_BOOK_RECONCILE.RECONCILE_ITEM_ID)
               and TAX_BOOK_RECONCILE.TAX_INCLUDE_ID = TAX_INCLUDE_ACTIVITY.TAX_INCLUDE_ID
               and TAX_INCLUDE_ACTIVITY.TAX_BOOK_ID = NORMALIZATION_SCHEMA.NORM_FROM_TAX
               and (TRC.TAX_RECORD_ID = DIT.TAX_RECORD_ID)
               and (TAX_BOOK_RECONCILE.TAX_YEAR = DIT.TAX_YEAR)
               and (TAX_BOOK_RECONCILE.TAX_RECORD_ID = DIT.TAX_RECORD_ID)
               and DIT.TAX_RECORD_ID = DEFERRED_INCOME_TAX_TRANSFER.TAX_RECORD_ID(+)
               and DIT.TAX_YEAR = DEFERRED_INCOME_TAX_TRANSFER.TAX_YEAR(+)
               and DIT.TAX_MONTH = DEFERRED_INCOME_TAX_TRANSFER.TAX_MONTH(+)
               and DIT.NORMALIZATION_ID = DEFERRED_INCOME_TAX_TRANSFER.NORMALIZATION_ID(+)
               and DIT.TIME_SLICE_ID = DEFERRED_INCOME_TAX_TRANSFER.TIME_SLICE_ID(+)
               and NORMALIZATION_SCHEMA.JURISDICTION_ID = JS.JURISDICTION_ID
               and TAX_BOOK_RECONCILE.TAX_INCLUDE_ID =
                   TAX_BOOK_RECONCILE_TRANSFER.TAX_INCLUDE_ID(+)
               and TAX_BOOK_RECONCILE.TAX_YEAR = TAX_BOOK_RECONCILE_TRANSFER.TAX_YEAR(+)
               and TAX_BOOK_RECONCILE.TAX_RECORD_ID = TAX_BOOK_RECONCILE_TRANSFER.TAX_RECORD_ID(+)
               and TAX_BOOK_RECONCILE.RECONCILE_ITEM_ID =
                   TAX_BOOK_RECONCILE_TRANSFER.RECONCILE_ITEM_ID(+)
               and DIT.TAX_YEAR between G_START_YEAR and G_END_YEAR
               and (NORMALIZATION_SCHEMA.NORMALIZATION_ID in
                   (select * from table(G_TABLE_NORM_IDS)) or G_TABLE_NORM_COUNT = 0)
               and (TRC.VINTAGE_ID in (select * from table(G_TABLE_VINTAGE_IDS)) or
                   G_TABLE_VINTAGE_COUNT = 0)
               and (TRC.TAX_CLASS_ID in (select * from table(G_TABLE_CLASS_IDS)) or
                   G_TABLE_CLASS_COUNT = 0)
               and (TRC.COMPANY_ID in (select * from table(G_TABLE_COMPANY_IDS)) or
                   G_TABLE_COMPANY_COUNT = 0)
               and (NORMALIZATION_SCHEMA.AMORTIZATION_TYPE_ID = -1)
               and TRC.VERSION_ID = G_VERSION
             group by NORMALIZATION_SCHEMA.JURISDICTION_ID,
                      DIT.TAX_RECORD_ID,
                      DIT.TAX_YEAR,
                      JS.TAX_BOOK_ID;

         L_SQL := ' create table temp_def_income_tax_transfers   as    SELECT    ' ||
                  '    sum(dt.norm_diff_balance_beg) norm_diff_balance_beg,  ' ||
                  '      sum(dt.def_income_tax_balance_beg) def_income_tax_balance_beg,   ' ||
                  '       dt.tax_record_id,dt.tax_year,dt.normalization_id    ' ||
                  '       from     deferred_income_tax_transfer  dt           ' || '     where   ' ||
                  '        1 = 0 ' ||
                  '     group by dt.tax_record_id,dt.tax_year,dt.normalization_id ';

         L_SQL := 'drop table temp_def_income_tax_transfers';
         -- execute immediate l_sql;
         insert into TEMP_G_DEF_INCOME_TAX_TRANS
            select sum(DT.NORM_DIFF_BALANCE_BEG) NORM_DIFF_BALANCE_BEG,
                   sum(DT.DEF_INCOME_TAX_BALANCE_BEG) DEF_INCOME_TAX_BALANCE_BEG,
                   DT.TAX_RECORD_ID,
                   DT.TAX_YEAR,
                   DT.NORMALIZATION_ID
              from DEFERRED_INCOME_TAX_TRANSFER DT
             where (DT.NORMALIZATION_ID in (select * from table(G_TABLE_NORM_IDS)) or
                   G_TABLE_NORM_COUNT = 0)
               and DT.TAX_YEAR between G_START_YEAR and G_END_YEAR
             group by DT.TAX_RECORD_ID, DT.TAX_YEAR, DT.NORMALIZATION_ID;

         open TAX_BASIS_AMOUNT_CUR;
         fetch TAX_BASIS_AMOUNT_CUR bulk collect
            into G_TAX_BASIS_AMOUNT_REC;
         close TAX_BASIS_AMOUNT_CUR;

         open DEF_INCOME_TAX_TRANSFERS_CUR;
         fetch DEF_INCOME_TAX_TRANSFERS_CUR bulk collect
            into G_DEF_INCOME_TAX_TRANSFERS_REC;
         close DEF_INCOME_TAX_TRANSFERS_CUR;

         open TAX_DETAIL_CUR; --(l_tax_basis_amount_rec,l_def_income_tax_trans_rec);
      else
         G_TAX_DETAIL_REC.DELETE;
      end if;

      A_START_ROW := 1;
      fetch TAX_DETAIL_CUR bulk collect
         into G_TAX_DETAIL_REC limit G_LIMIT;

      I       := G_TAX_DETAIL_REC.FIRST;
      L_COUNT := G_TAX_DETAIL_REC.COUNT;

      if L_COUNT = 0 then
         close TAX_DETAIL_CUR;
         return 0;
      else
         G_TAX_DETAIL_LAST_RECORD_ID := G_TAX_DETAIL_REC(L_COUNT).TAX_RECORD_ID;
         G_TAX_DETAIL_LAST_TAX_MONTH := G_TAX_DETAIL_REC(L_COUNT).TAX_MONTH;
         G_TAX_DETAIL_LAST_NORM_ID   := G_TAX_DETAIL_REC(L_COUNT).NORMALIZATION_ID;
      end if;

      -- save the last records that are not complete
      K := 0;
      G_TAX_DETAIL_SAVE2_REC.DELETE;
      if L_COUNT = G_LIMIT then
         J := 1;
         for J in reverse 1 .. L_COUNT
         loop
            if G_TAX_DETAIL_LAST_RECORD_ID = G_TAX_DETAIL_REC(J).TAX_RECORD_ID and
               G_TAX_DETAIL_LAST_TAX_MONTH = G_TAX_DETAIL_REC(J).TAX_MONTH and
               G_TAX_DETAIL_LAST_NORM_ID = G_TAX_DETAIL_REC(J).NORMALIZATION_ID then
               G_TAX_DETAIL_SAVE2_REC.EXTEND;
               K := K + 1;
               G_TAX_DETAIL_SAVE2_REC(K) := G_TAX_DETAIL_REC(J);
            else
               exit;
            end if;
         end loop;
         L_COUNT := L_COUNT - K;
      else
         G_TAX_DETAIL_LAST_RECORD_ID := 0;
         G_TAX_DETAIL_LAST_TAX_MONTH := 0;
         G_TAX_DETAIL_LAST_NORM_ID   := 0;
      end if;

      -- restore the save records
      J := 0;

      if G_TAX_DETAIL_SAVE1_REC.COUNT > 0 then
         L_SAVE_COUNT := G_TAX_DETAIL_SAVE1_REC.COUNT;
         for J in 1 .. L_SAVE_COUNT
         loop
            G_TAX_DETAIL_REC.EXTEND;
            G_TAX_DETAIL_REC(L_COUNT + J) := G_TAX_DETAIL_SAVE1_REC(J);
            K := K + 1;
         end loop;
         -- we must reorganize the collection
         L_TAX_DET_LAST_SAVE_RECORD_ID := G_TAX_DETAIL_SAVE1_REC(1).TAX_RECORD_ID;
         L_TAX_DET_LAST_SAVE_TAX_MONTH := G_TAX_DETAIL_SAVE1_REC(1).TAX_MONTH;
         L_TAX_DET_LAST_SAVE_NORM_ID   := G_TAX_DETAIL_SAVE1_REC(1).NORMALIZATION_ID;
         L_MOVE_ROW_COUNT              := 0;
         for J in 1 .. L_COUNT
         loop
            if L_TAX_DET_LAST_SAVE_RECORD_ID = G_TAX_DETAIL_REC(J).TAX_RECORD_ID and
               L_TAX_DET_LAST_SAVE_TAX_MONTH = G_TAX_DETAIL_REC(J).TAX_MONTH and
               L_TAX_DET_LAST_SAVE_NORM_ID = G_TAX_DETAIL_REC(J).NORMALIZATION_ID then
               L_MOVE_ROW_COUNT := L_MOVE_ROW_COUNT + 1;
            else
               exit;
            end if;
         end loop;

         L_COUNT := L_COUNT + L_SAVE_COUNT;
         for J in 1 .. L_MOVE_ROW_COUNT
         loop
            G_TAX_DETAIL_REC.EXTEND;
            G_TAX_DETAIL_REC(L_COUNT + J) := G_TAX_DETAIL_REC(J);
         end loop;

         L_COUNT     := L_COUNT + L_MOVE_ROW_COUNT;
         A_START_ROW := L_MOVE_ROW_COUNT + 1;

      end if;

      G_TAX_DETAIL_SAVE1_REC := G_TAX_DETAIL_SAVE2_REC;

      -- create an index to the data
      G_TAX_DETAIL_BALS_HASH.DELETE;
      G_TAX_DETAIL_HASH.DELETE;
      I                   := A_START_ROW;
      L_CURRENT_RECORD_ID := 0;
      L_CURRENT_TAX_MONTH := 0;
      L_CURRENT_NORM_ID   := 0;
      while I <= L_COUNT
      loop

         G_TAX_DETAIL_HASH(TO_CHAR(G_TAX_DETAIL_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_TAX_DETAIL_REC(I).TAX_MONTH) || G_SEP || TO_CHAR(G_TAX_DETAIL_REC(I).NORMALIZATION_ID) || G_SEP || TO_CHAR(G_TAX_DETAIL_REC(I).TAX_YEAR)) := I;
         if L_CURRENT_RECORD_ID = G_TAX_DETAIL_REC(I).TAX_RECORD_ID and
            L_CURRENT_NORM_ID = G_TAX_DETAIL_REC(I).NORMALIZATION_ID and
            L_CURRENT_TAX_MONTH = G_TAX_DETAIL_REC(I).TAX_MONTH then
            L_YEAR_COUNT := L_YEAR_COUNT + 1;
         else
            L_YEAR_COUNT        := 1;
            L_CURRENT_RECORD_ID := G_TAX_DETAIL_REC(I).TAX_RECORD_ID;
            L_CURRENT_TAX_MONTH := G_TAX_DETAIL_REC(I).TAX_MONTH;
            L_CURRENT_NORM_ID   := G_TAX_DETAIL_REC(I).NORMALIZATION_ID;
            if L_CURRENT_RECORD_ID = 117152 then
               J := 0;
            end if;
         end if;

         G_TAX_DETAIL_BALS_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_TAX_MONTH) || G_SEP || TO_CHAR(L_CURRENT_NORM_ID))(L_YEAR_COUNT).TAX_YEAR := G_TAX_DETAIL_REC(I).TAX_YEAR;
         G_TAX_DETAIL_BALS_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_TAX_MONTH) || G_SEP || TO_CHAR(L_CURRENT_NORM_ID))(L_YEAR_COUNT).ID := I;
         I := I + 1;
      end loop;

      -- g_tax_depr_rec.delete;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Detail Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;

   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   ' Tax Detail Amount ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end GET_TAX_DETAIL;

   -- =============================================================================
   --  Function GET_TAX_RATES
   -- =============================================================================
   function GET_TAX_RATES return pls_integer is

      L_COUNT        integer;
      I              pls_integer;
      CODE           pls_integer;
      L_LAST_RATE_ID pls_integer;
      L_ROWS         pls_integer;

   begin
      if not TAX_RATES_CUR%isopen then
         open TAX_RATES_CUR;
      end if;

      fetch TAX_RATES_CUR bulk collect
         into G_TAX_RATES_REC;

      I       := G_TAX_RATES_REC.FIRST;
      L_COUNT := G_TAX_RATES_REC.COUNT;

      close TAX_RATES_CUR;

      -- create an index to the data
      L_COUNT        := G_TAX_RATES_REC.COUNT;
      I              := 1;
      L_LAST_RATE_ID := 0;
      while I <= L_COUNT
      loop

         if G_TAX_RATES_REC(I).TAX_RATE_ID = L_LAST_RATE_ID then
            L_ROWS := L_ROWS + 1;
            G_TAX_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         else
            L_ROWS := 1;
            L_LAST_RATE_ID := G_TAX_RATES_REC(I).TAX_RATE_ID;
            G_TAX_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         end if;
         I := I + 1;
      end loop;

      WRITE_LOG(G_JOB_NO, 1, 0, 'Tax Rates Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;

   end GET_TAX_RATES;

   -- =============================================================================
   --  Function GET_DFIT_RATES
   -- =============================================================================
   function GET_DFIT_RATES return pls_integer is

      L_COUNT        integer;
      I              pls_integer;
      CODE           pls_integer;
      L_LAST_RATE_ID pls_integer;
      L_ROWS         pls_integer;

   begin
      if not DFIT_RATES_CUR%isopen then
         open DFIT_RATES_CUR;
      end if;

      fetch DFIT_RATES_CUR bulk collect
         into G_DFIT_RATES_REC;

      I       := G_DFIT_RATES_REC.FIRST;
      L_COUNT := G_DFIT_RATES_REC.COUNT;

      close DFIT_RATES_CUR;

      -- create an index to the data
      L_COUNT        := G_DFIT_RATES_REC.COUNT;
      I              := 1;
      L_LAST_RATE_ID := -1;
      while I <= L_COUNT
      loop
         if G_DFIT_RATES_REC(I).DEF_INCOME_TAX_RATE_ID = L_LAST_RATE_ID then
            L_ROWS := L_ROWS + 1;
            G_DFIT_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         else
            L_ROWS := 1;
            L_LAST_RATE_ID := G_DFIT_RATES_REC(I).DEF_INCOME_TAX_RATE_ID;
            G_DFIT_RATES_HASH(TO_CHAR(L_LAST_RATE_ID))(L_ROWS).ID := I;
         end if;
         I := I + 1;
      end loop;
      WRITE_LOG(G_JOB_NO, 1, 0, 'Dfit Rates Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   'Dfit Rates ' || sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;

   end GET_DFIT_RATES;

   -- =============================================================================
   --  Function GET_DEF_TAX_BASIS
   -- =============================================================================
   function GET_DEF_TAX_BASIS(A_START_ROW out pls_integer) return pls_integer is
      L_COUNT                        integer;
      CODE                           pls_integer;
      L_ROWS                         pls_integer;
      I                              pls_integer;
      K                              pls_integer;
      J                              pls_integer;
      L_SAVE_COUNT                   integer;
      L_CURRENT_RECORD_ID            pls_integer;
      L_CURRENT_TAX_MONTH            pls_integer;
      L_CURRENT_NORM_ID              pls_integer;
      L_YEAR_COUNT                   pls_integer;
      L_MOVE_ROW_COUNT               pls_integer;
      L_DEF_TAX_BASIS_SAVE_RECORD_ID pls_integer;
      L_DEF_TAX_BASIS_SAVE_TAX_MONTH pls_integer;
      L_DEF_TAX_BASIS_SAVE_NORM_ID   pls_integer;

   begin
      if not DEF_TAX_BASIS_CUR%isopen then
         open DEF_TAX_BASIS_CUR;
      end if;
      A_START_ROW := 1;
      fetch DEF_TAX_BASIS_CUR bulk collect
         into G_DEF_TAX_BASIS_REC limit G_LIMIT;

      I       := G_DEF_TAX_BASIS_REC.FIRST;
      L_COUNT := G_DEF_TAX_BASIS_REC.COUNT;

      -- create an index to the data
      L_COUNT := G_DEF_TAX_BASIS_REC.COUNT;

      if L_COUNT = 0 then
         close DEF_TAX_BASIS_CUR;
      else
         G_DEF_TAX_BASIS_LAST_RECORD_ID := G_DEF_TAX_BASIS_REC(L_COUNT).TAX_RECORD_ID;
         G_DEF_TAX_BASIS_LAST_TAX_MONTH := G_DEF_TAX_BASIS_REC(L_COUNT).TAX_MONTH;
         G_DEF_TAX_BASIS_LAST_NORM_ID   := G_DEF_TAX_BASIS_REC(L_COUNT).NORMALIZATION_ID;
      end if;

      -- save the last records that are not complete
      K := 0;
      G_DEF_TAX_BASIS_SAVE2_REC.DELETE;
      if L_COUNT = G_LIMIT then
         J := 1;
         for J in reverse 1 .. L_COUNT
         loop
            if G_DEF_TAX_BASIS_LAST_RECORD_ID = G_TAX_DETAIL_REC(J).TAX_RECORD_ID and
               G_TAX_DETAIL_LAST_TAX_MONTH = G_DEF_TAX_BASIS_REC(J).TAX_MONTH then
               G_DEF_TAX_BASIS_SAVE2_REC.EXTEND;
               K := K + 1;
               G_DEF_TAX_BASIS_SAVE2_REC(K) := G_DEF_TAX_BASIS_REC(J);
            else
               exit;
            end if;
         end loop;
         L_COUNT := L_COUNT - K;
      else
         G_DEF_TAX_BASIS_LAST_RECORD_ID := 0;
         G_DEF_TAX_BASIS_LAST_NORM_ID   := 0;
         G_DEF_TAX_BASIS_LAST_TAX_MONTH := 0;
      end if;

      -- restore the save records
      J := 0;

      if G_DEF_TAX_BASIS_SAVE1_REC.COUNT > 0 then
         L_SAVE_COUNT := G_DEF_TAX_BASIS_SAVE1_REC.COUNT;
         K            := 0;
         for J in 1 .. L_SAVE_COUNT
         loop
            G_DEF_TAX_BASIS_REC.EXTEND;
            G_DEF_TAX_BASIS_REC(L_COUNT + J) := G_DEF_TAX_BASIS_SAVE1_REC(J);
            K := K + 1;
         end loop;
         L_COUNT := L_COUNT + K;
         -- we must reorganize the collection
         L_DEF_TAX_BASIS_SAVE_RECORD_ID := G_DEF_TAX_BASIS_REC(1).TAX_RECORD_ID;
         L_DEF_TAX_BASIS_SAVE_TAX_MONTH := G_DEF_TAX_BASIS_REC(1).TAX_MONTH;
         L_DEF_TAX_BASIS_SAVE_NORM_ID   := G_DEF_TAX_BASIS_REC(1).NORMALIZATION_ID;
         L_MOVE_ROW_COUNT               := 0;
         for J in 1 .. L_COUNT
         loop
            if L_DEF_TAX_BASIS_SAVE_RECORD_ID = G_DEF_TAX_BASIS_REC(J).TAX_RECORD_ID and
               L_DEF_TAX_BASIS_SAVE_TAX_MONTH = G_DEF_TAX_BASIS_REC(J).TAX_MONTH and
               L_DEF_TAX_BASIS_SAVE_NORM_ID = G_DEF_TAX_BASIS_REC(J).NORMALIZATION_ID then
               L_MOVE_ROW_COUNT := L_MOVE_ROW_COUNT + 1;
            else
               exit;
            end if;
         end loop;
         L_COUNT := L_COUNT + L_SAVE_COUNT;
         for J in 1 .. L_MOVE_ROW_COUNT
         loop
            G_DEF_TAX_BASIS_REC.EXTEND;
            G_DEF_TAX_BASIS_REC(L_COUNT + J) := G_DEF_TAX_BASIS_REC(J);
         end loop;
         L_COUNT     := L_COUNT + L_MOVE_ROW_COUNT;
         A_START_ROW := L_MOVE_ROW_COUNT + 1;
      end if;

      G_DEF_TAX_BASIS_SAVE1_REC := G_DEF_TAX_BASIS_SAVE2_REC;

      -- create an index to the data

      I                   := A_START_ROW;
      L_CURRENT_RECORD_ID := 0;
      L_CURRENT_TAX_MONTH := 0;
      L_CURRENT_NORM_ID   := 0;
      while I <= L_COUNT
      loop
         if G_DEF_TAX_BASIS_LAST_RECORD_ID = G_DEF_TAX_BASIS_REC(I).TAX_RECORD_ID and
            G_DEF_TAX_BASIS_REC(I).TAX_MONTH = G_DEF_TAX_BASIS_LAST_TAX_MONTH and
            G_DEF_TAX_BASIS_LAST_NORM_ID = G_DEF_TAX_BASIS_REC(I).NORMALIZATION_ID then
            I := I + 1;
            CONTINUE;
         end if;

         G_DEF_TAX_BASIS_HASH(TO_CHAR(G_DEF_TAX_BASIS_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_DEF_TAX_BASIS_REC(I).TAX_MONTH) || G_SEP || TO_CHAR(G_DEF_TAX_BASIS_REC(I).NORMALIZATION_ID) || G_SEP || TO_CHAR(G_DEF_TAX_BASIS_REC(I).TAX_YEAR)) := I;
         if L_CURRENT_RECORD_ID = G_DEF_TAX_BASIS_REC(I).TAX_RECORD_ID and
            L_CURRENT_TAX_MONTH = G_DEF_TAX_BASIS_REC(I).TAX_MONTH and
            L_CURRENT_NORM_ID = G_DEF_TAX_BASIS_REC(I).NORMALIZATION_ID then
            L_YEAR_COUNT := L_YEAR_COUNT + 1;
            G_TAX_DETAIL_BALS_HASH(TO_CHAR(G_DEF_TAX_BASIS_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_DEF_TAX_BASIS_REC(I).TAX_MONTH) || G_SEP || TO_CHAR(G_DEF_TAX_BASIS_REC(I).NORMALIZATION_ID))(L_YEAR_COUNT).TAX_YEAR := G_DEF_TAX_BASIS_REC(I).TAX_YEAR;
            G_DEF_TAX_BASIS_BALS_HASH(TO_CHAR(G_DEF_TAX_BASIS_REC(I).TAX_RECORD_ID) || G_SEP || TO_CHAR(G_DEF_TAX_BASIS_REC(I).TAX_MONTH) || G_SEP || TO_CHAR(G_DEF_TAX_BASIS_REC(I).NORMALIZATION_ID))(L_YEAR_COUNT).ID := I;

         else
            L_YEAR_COUNT        := 1;
            L_CURRENT_RECORD_ID := G_DEF_TAX_BASIS_REC(I).TAX_RECORD_ID;
            L_CURRENT_TAX_MONTH := G_DEF_TAX_BASIS_REC(I).TAX_MONTH;
            L_CURRENT_NORM_ID   := G_DEF_TAX_BASIS_REC(I).NORMALIZATION_ID;
            if L_CURRENT_RECORD_ID = 117152 then
               J := 0;
            end if;
            G_DEF_TAX_BASIS_BALS_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_TAX_MONTH) || G_SEP || TO_CHAR(L_CURRENT_NORM_ID))(L_YEAR_COUNT).TAX_YEAR := G_DEF_TAX_BASIS_REC(I).TAX_YEAR;
            G_DEF_TAX_BASIS_BALS_HASH(TO_CHAR(L_CURRENT_RECORD_ID) || G_SEP || TO_CHAR(L_CURRENT_TAX_MONTH) || G_SEP || TO_CHAR(L_CURRENT_NORM_ID))(L_YEAR_COUNT).ID := I;

         end if;
         I := I + 1;
      end loop;

      WRITE_LOG(G_JOB_NO, 1, 0, 'Def Tax Basis Rows: ' || TO_CHAR(L_COUNT));
      return L_COUNT;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   CODE,
                   'Def Tax Basis ' || sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;

   end GET_DEF_TAX_BASIS;

   -- =============================================================================
   --  Function FILTER_TAX_DETAIL_BALS
   -- =============================================================================
   function FILTER_TAX_DETAIL_BALS(A_TAX_RECORD_ID pls_integer,
                                   A_TAX_MONTH     pls_integer,
                                   A_NORM_ID       pls_integer,
                                   A_TAX_YEAR      number) return pls_integer is
      I                       pls_integer;
      L_CODE                  pls_integer;
      L_COUNT                 pls_integer;
      L_ROW_COUNT             pls_integer;
      L_LOW_YEAR              pls_integer;
      L_TAX_YEAR              pls_integer;
      L_YEAR_COUNT            pls_integer;
      L_CURRENT_YEAR          number;
      L_TAX_DETAIL_BALS_INDEX pls_integer;
      L_FOUND                 boolean;

   begin
      L_TAX_DETAIL_BALS_INDEX := 0;
      L_CURRENT_YEAR          := 0;
      begin
         L_FOUND := G_TAX_DETAIL_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_MONTH) || G_SEP || TO_CHAR(A_NORM_ID)).EXISTS(1);
      exception
         when NO_DATA_FOUND then
            return 0;
         when others then
            L_CODE := sqlcode;
            WRITE_LOG(G_JOB_NO,
                      4,
                      L_CODE,
                      ' filter_tax_detail_bals record id =' || TO_CHAR(A_TAX_RECORD_ID) || '' ||
                      sqlerrm(L_CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            return 0;
      end;
      if G_TAX_DETAIL_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_MONTH) || G_SEP || TO_CHAR(A_NORM_ID)).EXISTS(1) then
         L_YEAR_COUNT := G_TAX_DETAIL_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_MONTH) || G_SEP || TO_CHAR(A_NORM_ID)).COUNT;
         for K in 1 .. L_YEAR_COUNT
         loop
            L_TAX_YEAR := G_TAX_DETAIL_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP ||
                                                 TO_CHAR(A_TAX_MONTH) || G_SEP ||
                                                 TO_CHAR(A_NORM_ID))(K).TAX_YEAR;
            if (L_TAX_YEAR > A_TAX_YEAR and (L_TAX_YEAR < L_CURRENT_YEAR or L_CURRENT_YEAR = 0)) then
               L_CURRENT_YEAR          := L_TAX_YEAR;
               L_TAX_DETAIL_BALS_INDEX := G_TAX_DETAIL_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP ||
                                                                 TO_CHAR(A_TAX_MONTH) || G_SEP ||
                                                                 TO_CHAR(A_NORM_ID))(K).ID;
            end if;
         end loop;
      end if;
      I := 0;
      return L_TAX_DETAIL_BALS_INDEX;

   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_detail_bals ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_DETAIL_BALS;

   -- =============================================================================
   --  Function FILTER_TAX_RATES
   -- =============================================================================
   function FILTER_TAX_RATES(A_TAX_RATES_SUB_REC in out nocopy TYPE_TAX_RATES_REC,
                             A_RATE_ID           pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;

   begin

      L_COUNT := G_TAX_RATES_REC.COUNT;
      if A_TAX_RATES_SUB_REC.EXISTS(1) then
         A_TAX_RATES_SUB_REC.DELETE;
      end if;
      L_ROW := 0;

      L_COUNT := G_TAX_RATES_HASH(A_RATE_ID).COUNT;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_TAX_RATES_HASH(A_RATE_ID)(I).ID;
         A_TAX_RATES_SUB_REC.EXTEND(1);
         A_TAX_RATES_SUB_REC(I) := G_TAX_RATES_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_tax_rates ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_TAX_RATES;

   -- =============================================================================
   --  Function FILTER_DFIT_RATES
   -- =============================================================================
   function FILTER_DFIT_RATES(A_DFIT_RATES_SUB_REC in out nocopy TYPE_DFIT_RATES_REC,
                              A_RATE_ID            pls_integer) return pls_integer is
      I       pls_integer;
      L_COUNT integer;
      L_ROW   integer;
      L_INDEX pls_integer;
      L_CODE  pls_integer;

   begin

      L_COUNT := G_DFIT_RATES_REC.COUNT;
      if A_DFIT_RATES_SUB_REC.EXISTS(1) then
         A_DFIT_RATES_SUB_REC.DELETE;
      end if;
      L_ROW := 0;

      L_COUNT := G_DFIT_RATES_HASH(A_RATE_ID).COUNT;
      for I in 1 .. L_COUNT
      loop
         L_INDEX := G_DFIT_RATES_HASH(A_RATE_ID)(I).ID;
         A_DFIT_RATES_SUB_REC.EXTEND(1);
         A_DFIT_RATES_SUB_REC(I) := G_DFIT_RATES_REC(L_INDEX);
      end loop;
      return L_COUNT;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Filter Dfit Rates ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_DFIT_RATES;

   -- =============================================================================
   --  Function FILTER_DEF_TAX_BASIS_BALS
   -- =============================================================================
   function FILTER_DEF_TAX_BASIS_BALS(A_TAX_RECORD_ID pls_integer,
                                      A_TAX_MONTH     pls_integer,
                                      A_NORM_ID       pls_integer,
                                      A_TAX_YEAR      number) return pls_integer is
      I                          pls_integer;
      L_CODE                     pls_integer;
      L_COUNT                    pls_integer;
      L_ROW_COUNT                pls_integer;
      L_LOW_YEAR                 pls_integer;
      L_TAX_YEAR                 pls_integer;
      L_YEAR_COUNT               pls_integer;
      L_CURRENT_YEAR             number;
      L_DEF_TAX_BASIS_BALS_INDEX pls_integer;
      L_FOUND                    boolean;

   begin
      L_DEF_TAX_BASIS_BALS_INDEX := 0;
      L_CURRENT_YEAR             := 0;
      begin
         L_FOUND := G_DEF_TAX_BASIS_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_MONTH) || G_SEP || TO_CHAR(A_NORM_ID)).EXISTS(1);
      exception
         when NO_DATA_FOUND then
            return 0;
         when others then
            L_CODE := sqlcode;
            WRITE_LOG(G_JOB_NO,
                      4,
                      L_CODE,
                      ' filter_def_tax_basis_bals record id =' || TO_CHAR(A_TAX_RECORD_ID) || '' ||
                      sqlerrm(L_CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
            return 0;
      end;
      if L_FOUND then
         L_YEAR_COUNT := G_DEF_TAX_BASIS_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP || TO_CHAR(A_TAX_MONTH) || G_SEP || TO_CHAR(A_NORM_ID)).COUNT;
         for K in 1 .. L_YEAR_COUNT
         loop
            L_TAX_YEAR := G_DEF_TAX_BASIS_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) || G_SEP ||
                                                    TO_CHAR(A_TAX_MONTH) || G_SEP ||
                                                    TO_CHAR(A_NORM_ID))(K).TAX_YEAR;
            if (L_TAX_YEAR > A_TAX_YEAR and (L_TAX_YEAR < L_CURRENT_YEAR or L_CURRENT_YEAR = 0)) then
               L_CURRENT_YEAR             := L_TAX_YEAR;
               L_DEF_TAX_BASIS_BALS_INDEX := G_DEF_TAX_BASIS_BALS_HASH(TO_CHAR(A_TAX_RECORD_ID) ||
                                                                       G_SEP ||
                                                                       TO_CHAR(A_TAX_MONTH) ||
                                                                       G_SEP || TO_CHAR(A_NORM_ID))(K).ID;
            end if;
         end loop;
      end if;
      I := 0;
      return L_DEF_TAX_BASIS_BALS_INDEX;

   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'filter_def_tax_basis_bals ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end FILTER_DEF_TAX_BASIS_BALS;

   -- =============================================================================
   --  Function CALC_DFIT
   -- =============================================================================
   function CALC_DFIT(A_JOB_NO number) return integer is
      cursor TAX_JOB_PARAMS_CUR is
         select VERSION, TAX_YEAR, VINTAGE, TAX_BOOK_ID, TAX_CLASS_ID, COMPANY_ID
           from TAX_JOB_PARAMS
          where JOB_NO = A_JOB_NO;
      L_VERSION            number(22, 0);
      L_TAX_YEAR           number(22, 2);
      L_VINTAGE            number(22, 0);
      L_NORM_ID            number(22, 0);
      L_TAX_MONTH          pls_integer;
      L_TAX_RECORD_ID      number(22, 0);
      L_TAX_CLASS_ID       number(22, 0);
      L_COMPANY_ID         number(22, 0);
      L_DFIT_RATES_ROWS    pls_integer;
      L_TAX_RATES_ROWS     pls_integer;
      L_TAX_RATES_SUB_ROWS pls_integer;
      L_TAX_DETAIL_ROWS    pls_integer;
      L_DEF_TAX_BASIS_ROWS pls_integer;
      type TAX_YEARS_TYPE is table of number(22, 2) index by pls_integer;
      TAX_YEARS      TAX_YEARS_TYPE;
      TAX_YEAR_COUNT number(22, 2);
      L_CURRENT_YEAR number;
      CODE           integer;
      ROWS           pls_integer;
      L_SHORT_MONTHS pls_integer;
      L_NEXT_YEAR    number(22, 2);
      L_YEAR         number(22, 2);
      L_YEAR_COUNT   pls_integer;
      K              pls_integer;

      L_CODE                  pls_integer;
      M                       pls_integer;
      L_TAX_RATES_SUB_REC     TYPE_TAX_RATES_REC;
      L_TAX_DETAIL_BALS_INDEX pls_integer;
      L_DFIT_RATES_SUB_REC    TYPE_DFIT_RATES_REC;
      L_DFIT_RATES_SUB_ROWS   pls_integer;

      L_DEF_TAX_BASIS_BALS_INDEX pls_integer;
      L_START_ROW                pls_integer;
      L_TOTAL_ROWS               pls_integer;

   begin
      G_START_TIME := CURRENT_TIMESTAMP;
      --pp_plsql_debug.debug_procedure_on('fast_tax');

      G_JOB_NO  := A_JOB_NO;
      G_LINE_NO := 1;
      WRITE_LOG(G_JOB_NO, 0, 0, 'Tax Deferred  Started Version=' || GET_VERSION());
      SET_SESSION_PARAMETER();
      DBMS_OUTPUT.ENABLE(10000);

      L_TAX_RATES_SUB_REC  := TYPE_TAX_RATES_REC();
      L_DFIT_RATES_SUB_REC := TYPE_DFIT_RATES_REC();

      G_TABLE_NORM_IDS       := TABLE_LIST_ID_TYPE(-1);
      G_TABLE_VINTAGE_IDS    := TABLE_LIST_ID_TYPE();
      G_TABLE_CLASS_IDS      := TABLE_LIST_ID_TYPE(-1);
      G_TABLE_COMPANY_IDS    := TABLE_LIST_ID_TYPE(-1);
      G_TAX_DETAIL_SAVE1_REC := TYPE_TAX_DETAIL_SAVE_REC();
      G_TAX_DETAIL_SAVE2_REC := TYPE_TAX_DETAIL_SAVE_REC();

      G_DEF_TAX_BASIS_SAVE1_REC := TYPE_DEF_TAX_BASIS_SAVE_REC();
      G_DEF_TAX_BASIS_SAVE2_REC := TYPE_DEF_TAX_BASIS_SAVE_REC();

      TAX_YEAR_COUNT        := 0;
      G_TABLE_NORM_COUNT    := 0;
      G_TABLE_VINTAGE_COUNT := 0;
      G_TABLE_CLASS_COUNT   := 0;
      G_TABLE_COMPANY_COUNT := 0;

      open TAX_JOB_PARAMS_CUR;
      loop
         fetch TAX_JOB_PARAMS_CUR
            into L_VERSION, L_TAX_YEAR, L_VINTAGE, L_NORM_ID, L_TAX_CLASS_ID, L_COMPANY_ID;
         if not L_VERSION is null then
            G_VERSION := L_VERSION;
         end if;
         if not L_TAX_YEAR is null then
            TAX_YEAR_COUNT := TAX_YEAR_COUNT + 1;
            TAX_YEARS(TAX_YEAR_COUNT) := L_TAX_YEAR;
         end if;
         if not L_VINTAGE is null then
            G_TABLE_VINTAGE_COUNT := 1 + G_TABLE_VINTAGE_COUNT;
            G_TABLE_VINTAGE_IDS.EXTEND;
            G_TABLE_VINTAGE_IDS(G_TABLE_VINTAGE_COUNT) := L_VINTAGE;
         end if;
         if not L_NORM_ID is null then
            G_TABLE_NORM_COUNT := 1 + G_TABLE_NORM_COUNT;
            G_TABLE_NORM_IDS.EXTEND;
            G_TABLE_NORM_IDS(G_TABLE_NORM_COUNT) := L_NORM_ID;
         end if;
         if not L_TAX_CLASS_ID is null then
            G_TABLE_CLASS_COUNT := 1 + G_TABLE_CLASS_COUNT;
            G_TABLE_CLASS_IDS.EXTEND;
            G_TABLE_CLASS_IDS(G_TABLE_CLASS_COUNT) := L_TAX_CLASS_ID;
         end if;
         if not L_COMPANY_ID is null then
            G_TABLE_COMPANY_COUNT := 1 + G_TABLE_COMPANY_COUNT;
            G_TABLE_COMPANY_IDS.EXTEND;
            G_TABLE_COMPANY_IDS(G_TABLE_COMPANY_COUNT) := L_COMPANY_ID;
         end if;
         exit when TAX_JOB_PARAMS_CUR%notfound;
      end loop;

      G_START_YEAR := TAX_YEARS(TAX_YEARS.FIRST);
      G_END_YEAR   := TAX_YEARS(TAX_YEARS.LAST);

      L_DFIT_RATES_ROWS := GET_DFIT_RATES();
      L_TAX_RATES_ROWS  := GET_TAX_RATES();
      L_TOTAL_ROWS      := 0;
      loop
         L_TAX_DETAIL_ROWS := GET_TAX_DETAIL(L_START_ROW);
         exit when L_TAX_DETAIL_ROWS = 0;
         L_TOTAL_ROWS := L_TOTAL_ROWS + L_TAX_DETAIL_ROWS - L_START_ROW;
         for L_TAX_DETAIL_INDEX in L_START_ROW .. L_TAX_DETAIL_ROWS
         loop
            L_TAX_RECORD_ID := G_TAX_DETAIL_REC(L_TAX_DETAIL_INDEX).TAX_RECORD_ID;
            L_TAX_MONTH     := G_TAX_DETAIL_REC(L_TAX_DETAIL_INDEX).TAX_MONTH;
            L_NORM_ID       := G_TAX_DETAIL_REC(L_TAX_DETAIL_INDEX).NORMALIZATION_ID;
            L_TAX_YEAR      := G_TAX_DETAIL_REC(L_TAX_DETAIL_INDEX).TAX_YEAR;
            if L_TAX_RECORD_ID = 18929 and L_NORM_ID = 2200 then
               null;
            end if;
            L_TAX_DETAIL_BALS_INDEX := FILTER_TAX_DETAIL_BALS(L_TAX_RECORD_ID,
                                                              L_TAX_MONTH,
                                                              L_NORM_ID,
                                                              L_TAX_YEAR);

            L_TAX_RATES_SUB_ROWS := FILTER_TAX_RATES(L_TAX_RATES_SUB_REC,
                                                     G_TAX_DETAIL_REC(L_TAX_DETAIL_INDEX).TAX_RATE_ID);

            L_DFIT_RATES_SUB_ROWS := FILTER_DFIT_RATES(L_DFIT_RATES_SUB_REC,
                                                       G_TAX_DETAIL_REC(L_TAX_DETAIL_INDEX).DEF_INCOME_TAX_RATE_ID);
            if L_TAX_DETAIL_BALS_INDEX = 1720 then
               null;
            end if;
            L_CODE := CALC(L_TAX_DETAIL_INDEX,
                           L_TAX_RATES_SUB_REC,
                           L_TAX_RATES_SUB_ROWS,
                           L_TAX_DETAIL_BALS_INDEX,
                           L_DFIT_RATES_SUB_REC,
                           L_DFIT_RATES_SUB_ROWS);
         end loop;
         L_CODE := UPDATE_DEFERRED_INCOME_TAX(L_START_ROW, L_TAX_DETAIL_ROWS);
      end loop;

      L_CURRENT_YEAR := 0;

      loop
         L_DEF_TAX_BASIS_ROWS := GET_DEF_TAX_BASIS(L_START_ROW);
         exit when L_DEF_TAX_BASIS_ROWS = 0;
         --*  Start Calculating BASIS Amount Deferred Income Tax

         for L_DEF_TAX_BASIS_INDEX in L_START_ROW .. L_DEF_TAX_BASIS_ROWS
         loop
            L_TAX_RECORD_ID := G_DEF_TAX_BASIS_REC(L_DEF_TAX_BASIS_INDEX).TAX_RECORD_ID;
            L_TAX_MONTH     := G_DEF_TAX_BASIS_REC(L_DEF_TAX_BASIS_INDEX).TAX_MONTH;
            L_NORM_ID       := G_DEF_TAX_BASIS_REC(L_DEF_TAX_BASIS_INDEX).NORMALIZATION_ID;
            L_TAX_YEAR      := G_DEF_TAX_BASIS_REC(L_DEF_TAX_BASIS_INDEX).TAX_YEAR;
            if L_TAX_RECORD_ID = 18929 and L_NORM_ID = 2200 then
               null;
            end if;
            L_DEF_TAX_BASIS_BALS_INDEX := FILTER_DEF_TAX_BASIS_BALS(L_TAX_RECORD_ID,
                                                                    L_TAX_MONTH,
                                                                    L_NORM_ID,
                                                                    L_TAX_YEAR);

            L_DFIT_RATES_SUB_ROWS := FILTER_DFIT_RATES(L_DFIT_RATES_SUB_REC,
                                                       G_DEF_TAX_BASIS_REC(L_DEF_TAX_BASIS_INDEX).DEF_INCOME_TAX_RATE_ID);

            if (L_CURRENT_YEAR != G_DEF_TAX_BASIS_REC(L_DEF_TAX_BASIS_INDEX).TAX_YEAR) then
               L_CURRENT_YEAR := G_DEF_TAX_BASIS_REC(L_DEF_TAX_BASIS_INDEX).TAX_YEAR;
               select NVL(MONTHS, 0)
                 into L_SHORT_MONTHS
                 from TAX_YEAR_VERSION
                where VERSION_ID = G_VERSION
                  and TAX_YEAR = L_CURRENT_YEAR;
               if (L_SHORT_MONTHS <= 0 or L_SHORT_MONTHS >= 12) then
                  L_SHORT_MONTHS := 12;
               end if;
            end if;
            L_CODE := BASIS_CALC(L_DEF_TAX_BASIS_INDEX,
                                 L_DFIT_RATES_SUB_REC,
                                 L_DFIT_RATES_SUB_ROWS,
                                 L_DEF_TAX_BASIS_BALS_INDEX,
                                 L_DEF_TAX_BASIS_BALS_INDEX,
                                 L_SHORT_MONTHS);

         end loop;
         L_CODE := UPDATE_DEF_TAX_BASIS(L_START_ROW, L_DEF_TAX_BASIS_ROWS);
      end loop;
      commit;
      WRITE_LOG(G_JOB_NO, 0, 0, 'Total Calculation Rows=' || TO_CHAR(L_TOTAL_ROWS));
      WRITE_LOG(G_JOB_NO, 0, 0, 'Finished');
      --pp_plsql_debug.debug_procedure_off('fast_tax');
      return 0;
   exception
      when others then
         CODE := sqlcode;
         WRITE_LOG(G_JOB_NO, 4, CODE, sqlerrm(CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end CALC_DFIT;

   -- =============================================================================
   --  Function UPDATE_DEFERRED_INCOME_TAX
   -- =============================================================================
   function UPDATE_DEFERRED_INCOME_TAX(A_START_ROW pls_integer,
                                       A_NUM_REC   pls_integer) return number as
      I      pls_integer;
      L_CODE pls_integer;

   begin
      forall I in A_START_ROW .. A_NUM_REC
         update DEFERRED_INCOME_TAX
            set DEF_INCOME_TAX_BALANCE_END = G_TAX_DETAIL_REC(I).DEF_INCOME_TAX_BALANCE_END,
                NORM_DIFF_BALANCE_END = G_TAX_DETAIL_REC(I).NORM_DIFF_BALANCE_END,
                DEF_INCOME_TAX_PROVISION = G_TAX_DETAIL_REC(I).DEF_INCOME_TAX_PROVISION,
                DEF_INCOME_TAX_REVERSAL = G_TAX_DETAIL_REC(I).DEF_INCOME_TAX_REVERSAL,
                DEF_INCOME_TAX_RETIRE = G_TAX_DETAIL_REC(I).DEF_INCOME_TAX_RETIRE,
                DEF_INCOME_TAX_GAIN_LOSS = G_TAX_DETAIL_REC(I).DEF_INCOME_TAX_GAIN_LOSS,
                ARAM_RATE_END = G_TAX_DETAIL_REC(I).ARAM_RATE_END,
                DEF_INCOME_TAX_BALANCE_BEG = G_TAX_DETAIL_REC(I).DEF_INCOME_TAX_BALANCE_BEG,
                NORM_DIFF_BALANCE_BEG = G_TAX_DETAIL_REC(I).NORM_DIFF_BALANCE_BEG,
                ARAM_RATE = G_TAX_DETAIL_REC(I).ARAM_RATE
          where TAX_RECORD_ID = G_TAX_DETAIL_REC(I).TAX_RECORD_ID
            and TAX_MONTH = G_TAX_DETAIL_REC(I).TAX_MONTH
            and TAX_YEAR = G_TAX_DETAIL_REC(I).TAX_YEAR
            and NORMALIZATION_ID = G_TAX_DETAIL_REC(I).NORMALIZATION_ID;

      WRITE_LOG(G_JOB_NO, 3, 0, 'Update Deferred Income Tax Rows=' || TO_CHAR(A_NUM_REC));
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Update  Deferred Income Tax ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end UPDATE_DEFERRED_INCOME_TAX;

   -- =============================================================================
   --  Function UPDATE_DEF_TAX_BASIS
   -- =============================================================================
   function UPDATE_DEF_TAX_BASIS(A_START_ROW pls_integer,
                                 A_NUM_REC   pls_integer) return number as
      I      pls_integer;
      L_CODE pls_integer;
   begin
      forall I in A_START_ROW .. A_NUM_REC
         update DEFERRED_INCOME_TAX
            set DEF_INCOME_TAX_BALANCE_END = G_DEF_TAX_BASIS_REC(I).DEF_INCOME_TAX_BALANCE_END,
                NORM_DIFF_BALANCE_END = G_DEF_TAX_BASIS_REC(I).NORM_DIFF_BALANCE_END,
                DEF_INCOME_TAX_PROVISION = G_DEF_TAX_BASIS_REC(I).DEF_INCOME_TAX_PROVISION,
                DEF_INCOME_TAX_REVERSAL = G_DEF_TAX_BASIS_REC(I).DEF_INCOME_TAX_REVERSAL,
                DEF_INCOME_TAX_RETIRE = G_DEF_TAX_BASIS_REC(I).DEF_INCOME_TAX_RETIRE,
                DEF_INCOME_TAX_GAIN_LOSS = G_DEF_TAX_BASIS_REC(I).DEF_INCOME_TAX_GAIN_LOSS,
                ARAM_RATE_END = G_DEF_TAX_BASIS_REC(I).ARAM_RATE_END,
                DEF_INCOME_TAX_BALANCE_BEG = G_DEF_TAX_BASIS_REC(I).DEF_INCOME_TAX_BALANCE_BEG,
                NORM_DIFF_BALANCE_BEG = G_DEF_TAX_BASIS_REC(I).NORM_DIFF_BALANCE_BEG,
                ARAM_RATE = G_DEF_TAX_BASIS_REC(I).ARAM_RATE
          where TAX_RECORD_ID = G_DEF_TAX_BASIS_REC(I).TAX_RECORD_ID
            and TAX_MONTH = G_DEF_TAX_BASIS_REC(I).TAX_MONTH
            and TAX_YEAR = G_DEF_TAX_BASIS_REC(I).TAX_YEAR
            and NORMALIZATION_ID = G_DEF_TAX_BASIS_REC(I).NORMALIZATION_ID;

      WRITE_LOG(G_JOB_NO, 3, 0, 'Update Def Tax Basis Rows=' || TO_CHAR(A_NUM_REC));
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Update  Deferred Income Tax ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return 0;
   end UPDATE_DEF_TAX_BASIS;

   -- =============================================================================
   --  Function CALC
   -- =============================================================================
   function CALC(A_TAX_DETAIL_INDEX      pls_integer,
                 A_TAX_RATES_SUB_REC     TYPE_TAX_RATES_REC,
                 A_TAX_RATES_ROWS        pls_integer,
                 A_TAX_DETAIL_BALS_INDEX pls_integer,
                 A_DFIT_RATES            TYPE_DFIT_RATES_REC,
                 A_DFIT_RATES_ROWS       pls_integer) return number is
      I            pls_integer;
      L_RATE_INDEX pls_integer;
      L_NYEARS     pls_integer;
      J            pls_integer;

      L_TAX_YEAR BINARY_DOUBLE;
      type NUM_ARY100_TYPE is table of BINARY_DOUBLE index by binary_integer;
      L_DEF_TAX_RATES NUM_ARY100_TYPE;

      L_DIFF_CURRENT BINARY_DOUBLE := 0;
      L_DIFF_FT      BINARY_DOUBLE := 0;
      L_REM_RATE     BINARY_DOUBLE := 0;

      type INT_ARY100_TYPE is table of pls_integer index by binary_integer;
      L_DEF_TAX_DATES INT_ARY100_TYPE; -- dates
      L_TAX_DATE      pls_integer; -- dates

      L_DEF_INCOME_TAX_BALANCE_BEG BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_BALANCE_END BINARY_DOUBLE := 0;
      L_NORM_DIFF_BALANCE_BEG      BINARY_DOUBLE := 0;
      L_NORM_DIFF_BALANCE_END      BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_PROVISION   BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_REVERSAL    BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_ADJUST      BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_RETIRE      BINARY_DOUBLE := 0;
      L_DEF_INCOME_TAX_GAIN_LOSS   BINARY_DOUBLE := 0;
      L_ARAM_RATE                  BINARY_DOUBLE := 0;
      L_ARAM_RATE_END              BINARY_DOUBLE := 0;
      L_LIFE                       BINARY_DOUBLE := 0;
      L_DEPRECIATION               BINARY_DOUBLE := 0;
      L_DEPRECIATION_2             BINARY_DOUBLE := 0;
      L_GAIN_LOSS                  BINARY_DOUBLE := 0;
      L_COR_EXPENSE                BINARY_DOUBLE := 0;
      L_GAIN_LOSS_2                BINARY_DOUBLE := 0;
      L_COR_EXPENSE_2              BINARY_DOUBLE := 0;
      L_CAPITALIZED_DEPR           BINARY_DOUBLE := 0;
      L_CAPITALIZED_DEPR_2         BINARY_DOUBLE := 0;
      L_RETIREMENTS                BINARY_DOUBLE := 0;
      L_EXTRAORDINARY_RETIRES      BINARY_DOUBLE := 0;
      L_COST_OF_REMOVAL_FT         BINARY_DOUBLE := 0;
      L_AMOUNT_TO_AMORTIZE         BINARY_DOUBLE := 0;
      L_TAX_BALANCE_FROM           BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE_FROM         BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE_ADJUST_A     BINARY_DOUBLE := 0;
      L_ACCUM_RESERVE_ADJUST_B     BINARY_DOUBLE := 0;

      L_CAP_DEPR_IND  pls_integer;
      L_NO_ZERO_CHECK pls_integer;

      L_SCOOP          pls_integer := 0;
      L_MORE_FROM_DEPR boolean := false;

      L_GAIN_LOSS_DFIT_BAL BINARY_DOUBLE := 0;

      L_DFIT_RATE_ORDER INT_ARY100_TYPE;
      L_COUNT           pls_integer;
      L_DEF_TAX_DATE    BINARY_DOUBLE;
      L_CODE            pls_integer;

   begin

      --
      -- Move the items in the deferred tax table to variables
      --   Get rid of the gets we don't need
      --

      L_TAX_YEAR                   := G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).TAX_YEAR;
      L_DEF_INCOME_TAX_BALANCE_BEG := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_BALANCE_BEG, 0) +
                                      NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_BAL_TRANSFER,
                                                   0);
      --def_income_tax_balance_end     = dw_def_tax.getitemnumber(r,'def_income_tax_balance_end ;
      L_NORM_DIFF_BALANCE_BEG := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).NORM_DIFF_BALANCE_BEG, 0) +
                                 NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).NORM_DIFF_BALANCE_TRANSFER, 0);
      --norm_diff_balance_end          = dw_def_tax.getitemnumber(r,'norm_diff_balance_end;
      --def_income_tax_provision       = dw_def_tax.getitemnumber(r,'def_income_tax_provision;
      --def_income_tax_reversal        = dw_def_tax.getitemnumber(r,'def_income_tax_reversal;
      L_DEF_INCOME_TAX_ADJUST := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_ADJUST, 0);
      --def_income_tax_retire          = dw_def_tax.getitemnumber(r,'def_income_tax_retire;
      --def_income_tax_gain_loss       = dw_def_tax.getitemnumber(r,'def_income_tax_gain_loss;
      L_ARAM_RATE := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).ARAM_RATE, 0);
      --aram_rate_end                  = dnvl(w_def_tax.getitemnumber(r,'aram_rate_end;
      L_LIFE                   := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).LIFE, 0);
      L_DEPRECIATION           := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DA_DEPRECIATION, 0);
      L_DEPRECIATION_2         := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DB_DEPRECIATION, 0);
      L_GAIN_LOSS              := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DA_GAIN_LOSS, 0);
      L_GAIN_LOSS_2            := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DB_GAIN_LOSS, 0);
      L_COR_EXPENSE            := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DA_COR_EXPENSE, 0);
      L_COR_EXPENSE_2          := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DB_COR_EXPENSE, 0);
      L_RETIREMENTS            := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DA_RETIREMENTS, 0);
      L_EXTRAORDINARY_RETIRES  := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DA_EXTRAORDINARY_RETIRES, 0);
      L_CAPITALIZED_DEPR       := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DA_CAPITALIZED_DEPR, 0);
      L_CAPITALIZED_DEPR_2     := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DB_CAPITALIZED_DEPR, 0);
      L_COST_OF_REMOVAL_FT     := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).COST_OF_REMOVAL_FT, 0);
      L_AMOUNT_TO_AMORTIZE     := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).AMOUNT_TO_AMORTIZE, 0);
      L_CAP_DEPR_IND           := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).CAP_DEPR_IND, 0);
      L_TAX_BALANCE_FROM       := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).TAX_BALANCE_FROM, 0);
      L_ACCUM_RESERVE_FROM     := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).ACCUM_RESERVE_FROM, 0);
      L_NO_ZERO_CHECK          := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).NO_ZERO_CHECK, 0);
      L_ACCUM_RESERVE_ADJUST_A := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).ACCUM_RESERVE_ADJUST_A, 0);
      L_ACCUM_RESERVE_ADJUST_B := NVL(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).ACCUM_RESERVE_ADJUST_B, 0);
      -- PSEG CONVERSION EQUATIONs:

      -- handled in the retrieve
      --if isnull(def_income_tax_adjust) then def_income_tax_adjust = 0

      --gain_loss_dfit_bal             = g_tax_detail_rec(a_tax_detail_index).gain_loss_def_tax_balance;

      -- handled in the retrieve
      --if isnull(gain_loss_dfit_bal) then gain_loss_dfit_bal = 0

      L_DEF_INCOME_TAX_BALANCE_BEG := L_DEF_INCOME_TAX_BALANCE_BEG + L_GAIN_LOSS_DFIT_BAL;

      --norm_diff_balance_beg          = g_tax_detail_rec(a_tax_detail_index).pseg_beg_norm;

      --/////////////////////////////////////////////////////////////////////////
      --  We may need Deferred Tax Rates
      --  Get the most recent effective deferred tax rate
      --  The datawindow comes in ordered by ascending effective date
      --/////////////////////////////////////////////////////////////////////////

      -- dw_def_tax_rates.setsort('effective_date a;
      -- w_def_tax_rates.sort()
      -- Sort Deferred Tax Rates
      for I in 1 .. A_DFIT_RATES_ROWS
      loop
         L_COUNT := 1;
         for J in 1 .. A_DFIT_RATES_ROWS
         loop
            if (A_DFIT_RATES(I).EFFECTIVE_DATE > A_DFIT_RATES(J).EFFECTIVE_DATE) then
               L_COUNT := 1 + L_COUNT;
            elsif (A_DFIT_RATES(I).EFFECTIVE_DATE = A_DFIT_RATES(J).EFFECTIVE_DATE and I > J) then
               L_COUNT := 1 + L_COUNT;
            end if;
         end loop;
         L_DFIT_RATE_ORDER(L_COUNT) := I;
      end loop;

      for I in 1 .. A_DFIT_RATES_ROWS
      loop
         J := L_DFIT_RATE_ORDER(I);
         L_DEF_TAX_RATES(I) := A_DFIT_RATES(J).ANNUAL_RATE;
         L_DEF_TAX_DATES(I) := A_DFIT_RATES(J).EFFECTIVE_DATE;
      end loop;
      -- YYYYMMDD
      L_TAX_DATE := (ROUND(L_TAX_YEAR, 0) * 10000) + 100 + 1;

      L_RATE_INDEX := 0;

      for I in 1 .. A_DFIT_RATES_ROWS
      loop
         --PostMsg(5,tax_date,"tax_date","Rate Dates','Tax Rates");
         --PostMsg(5,def_tax_dates[i - 1],"def_tax_datese","Rate Dates",'Tax Rates");
         L_DEF_TAX_DATE := L_DEF_TAX_DATES(I);
         if (L_TAX_DATE >= L_DEF_TAX_DATE) then
            L_RATE_INDEX := I;
         end if;
      end loop;

      --PostMsg(5,rate_index,"Calc","Rate index','Tax Rates");
      if (L_RATE_INDEX = 0) then
         --PostMsg(5,1L,"Calc","Warning','No Good Def Tax Rates");
         L_RATE_INDEX := 1;
      end if;

      --
      --
      --
      --
      --

      -- the 'ending aram rate' is the one we use this year

      L_ARAM_RATE_END := 0; --this is a div by 0 check

      if (L_NORM_DIFF_BALANCE_BEG != 0) then
         L_ARAM_RATE_END := L_DEF_INCOME_TAX_BALANCE_BEG / L_NORM_DIFF_BALANCE_BEG;
      else
         L_ARAM_RATE_END := L_DEF_TAX_RATES(L_RATE_INDEX);
      end if;

      --* temporary fix for jonathan 4.15.5
      --if( norm_diff_balance_beg == 0  depreciation == 0 )
      --    goto have_calced;

      --diff_current = (depreciation * (1 - (capitalized_depr * ( 1 - cap_depr_ind)))  - gain_loss + cor_expense )  -
      --               (depreciation_2  * (1 - (capitalized_depr_2 * (1 - cap_depr_ind))) - gain_loss_2 + cor_expense_2 );

      L_DIFF_CURRENT := (L_DEPRECIATION * (1 - (L_CAPITALIZED_DEPR * (1 - L_CAP_DEPR_IND))) -
                        L_GAIN_LOSS + L_COR_EXPENSE + L_ACCUM_RESERVE_ADJUST_A) -
                        (L_DEPRECIATION_2 * (1 - (L_CAPITALIZED_DEPR_2 * (1 - L_CAP_DEPR_IND))) -
                        L_GAIN_LOSS_2 + L_COR_EXPENSE_2 + L_ACCUM_RESERVE_ADJUST_B);

      -- for tax_only basis differnces
      L_DIFF_CURRENT := L_DIFF_CURRENT - L_AMOUNT_TO_AMORTIZE;

      L_DIFF_FT := (L_COR_EXPENSE * L_COST_OF_REMOVAL_FT) -
                   (L_COR_EXPENSE_2 * L_COST_OF_REMOVAL_FT);

      --diff_current = (depreciation - gain_loss + cor_expense) - (depreciation_2 - gain_loss_2 + cor_expense_2);

      -- if both the current difference and the balance have the same sign
      -- then deferred taxes are building-up

      if ((L_DIFF_CURRENT * L_NORM_DIFF_BALANCE_BEG) >= 0) then
         L_DEF_INCOME_TAX_PROVISION := (L_DIFF_CURRENT - L_DIFF_FT) * L_DEF_TAX_RATES(L_RATE_INDEX);
         goto HAVE_CALCED;
      end if;

      -- We are in a reversal, because the current difference and the balance
      -- do not have the same sign

      -- if if there is no zero-check necessity, then the calc is straightforward.

      if (ABS(L_DIFF_CURRENT - L_DIFF_FT) <= ABS(L_NORM_DIFF_BALANCE_BEG)) then
         L_DEF_INCOME_TAX_REVERSAL := (L_DIFF_CURRENT - L_DIFF_FT) * L_ARAM_RATE_END;
         goto HAVE_CALCED;
      end if;

      -- Need to do a zero check because the current turnaround difference is greater than the
      -- accumulated difference.

      L_DEF_INCOME_TAX_REVERSAL := (-L_NORM_DIFF_BALANCE_BEG) * L_ARAM_RATE_END;

      -- Will need to know if there is more 'from' difference coming in the future to do
      -- the following check for finished with def tax, or just another cross-over point
      -- Determine if there is more depreciation out there

      L_MORE_FROM_DEPR := false;

      L_NYEARS := ROUND(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).TAX_YEAR - G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).VINTAGE_YEAR + 2);

      if (L_NYEARS <= A_TAX_RATES_ROWS) then
         L_REM_RATE := 0;

         -- these are sorted in asc year order
         for J in L_NYEARS .. A_TAX_RATES_ROWS
         loop
            L_REM_RATE := L_REM_RATE + A_TAX_RATES_SUB_REC(J).RATE;
         end loop;
         if (L_REM_RATE != 0) then
            L_MORE_FROM_DEPR := true;
         end if;
         -- nyears <= ...
      end if;
      if (L_NO_ZERO_CHECK = 1) then
         L_MORE_FROM_DEPR := true;
      end if;
      -- Now, if there are more future 'from' tax rates then this is
      -- a temporary cross-over point. Apply the remainder of the current difference as a build-up
      -- at the statutory rate

      if (L_TAX_BALANCE_FROM * L_ACCUM_RESERVE_FROM > 0 and
         ABS(L_TAX_BALANCE_FROM) > ABS(L_ACCUM_RESERVE_FROM)) then
         L_MORE_FROM_DEPR := true;
      end if;

      if (L_MORE_FROM_DEPR = true) then
         L_DEF_INCOME_TAX_PROVISION := (L_DIFF_CURRENT + L_NORM_DIFF_BALANCE_BEG) *
                                       L_DEF_TAX_RATES(L_RATE_INDEX);

         goto HAVE_CALCED;
      else
         L_DIFF_CURRENT := (-L_NORM_DIFF_BALANCE_BEG);
         goto HAVE_CALCED;
      end if;

      <<HAVE_CALCED>>

      L_DEF_INCOME_TAX_BALANCE_END := L_DEF_INCOME_TAX_BALANCE_BEG + L_DEF_INCOME_TAX_PROVISION +
                                      L_DEF_INCOME_TAX_REVERSAL + L_DEF_INCOME_TAX_ADJUST;

      L_NORM_DIFF_BALANCE_END := L_NORM_DIFF_BALANCE_BEG + L_DIFF_CURRENT;

      --//////////////////////////////////////////////////////////////////////////////////
      -- Put Everything back into the datawindows
      --//////////////////////////////////////////////////////////////////////////////////

      -- PSEG TEMP: fill in beginning difference balance

      --g_tax_detail_rec(a_tax_detail_index).norm_diff_balance_beg = norm_diff_balance_beg;

      G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_BALANCE_END := L_DEF_INCOME_TAX_BALANCE_END;
      G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).NORM_DIFF_BALANCE_END := L_NORM_DIFF_BALANCE_END;
      G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_PROVISION := L_DEF_INCOME_TAX_PROVISION;
      G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_REVERSAL := L_DEF_INCOME_TAX_REVERSAL;
      G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_RETIRE := L_DEF_INCOME_TAX_RETIRE;
      G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).DEF_INCOME_TAX_GAIN_LOSS := L_AMOUNT_TO_AMORTIZE; --def_income_tax_gain_loss;
      if (ABS(L_ARAM_RATE_END) < .0000001) then
         L_ARAM_RATE_END := 0;
      end if;
      G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).ARAM_RATE_END := L_ARAM_RATE_END;

      if A_TAX_DETAIL_BALS_INDEX <> 0 then
         G_TAX_DETAIL_REC(A_TAX_DETAIL_BALS_INDEX).DEF_INCOME_TAX_BALANCE_BEG := L_DEF_INCOME_TAX_BALANCE_END;
         G_TAX_DETAIL_REC(A_TAX_DETAIL_BALS_INDEX).NORM_DIFF_BALANCE_BEG := L_NORM_DIFF_BALANCE_END;
         G_TAX_DETAIL_REC(A_TAX_DETAIL_BALS_INDEX).ARAM_RATE := L_ARAM_RATE_END;
      end if;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   4,
                   L_CODE,
                   'Calc Record ID=' || TO_CHAR(G_TAX_DETAIL_REC(A_TAX_DETAIL_INDEX).TAX_RECORD_ID) || '  ' ||
                   sqlerrm(L_CODE) || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return - 1;
   end CALC;

   -- =============================================================================
   --  Function BASIS_CALC
   -- =============================================================================
   function BASIS_CALC(A_DEF_TAX_INDEX         pls_integer,
                       A_DEF_TAX_RATES_SUB_REC TYPE_DFIT_RATES_REC,
                       A_DEF_TAX_RATES_ROWS    pls_integer,
                       A_DEF_TAX_BALS_INDEX    pls_integer,
                       A_DEF_TAX_BALS_ROWS     pls_integer,
                       A_SHORT_MONTHS          pls_integer) return pls_integer is

      L_TAX_YEAR     BINARY_DOUBLE;
      L_VINTAGE_YEAR pls_integer;
      I              pls_integer;
      L_RATE_INDEX   pls_integer;
      L_NYEARS       pls_integer;
      J              pls_integer;

      type NUM_ARY200_TYPE is table of BINARY_DOUBLE index by binary_integer;
      L_DEF_TAX_RATES NUM_ARY200_TYPE;

      L_ACTIVITY   BINARY_DOUBLE;
      L_ADDITION   BINARY_DOUBLE;
      L_RETIREMENT BINARY_DOUBLE;
      L_AMORT      BINARY_DOUBLE;
      L_REM_RATE   BINARY_DOUBLE;
      L_REM_LIFE   BINARY_DOUBLE;
      L_RATIO      BINARY_DOUBLE;

      type INT_ARY200_TYPE is table of pls_integer index by binary_integer;
      L_DEF_TAX_DATES INT_ARY200_TYPE; --date
      L_TAX_DATE      pls_integer; --date

      L_DEF_INCOME_TAX_BALANCE_BEG BINARY_DOUBLE;
      L_DEF_INCOME_TAX_BALANCE_END BINARY_DOUBLE;
      L_NORM_DIFF_BALANCE_BEG      BINARY_DOUBLE;
      L_NORM_DIFF_BALANCE_END      BINARY_DOUBLE;
      L_DEF_INCOME_TAX_PROVISION   BINARY_DOUBLE;
      L_DEF_INCOME_TAX_REVERSAL    BINARY_DOUBLE;
      L_DEF_INCOME_TAX_ADJUST      BINARY_DOUBLE;
      L_DEF_INCOME_TAX_RETIRE      BINARY_DOUBLE;
      L_ARAM_RATE                  BINARY_DOUBLE;
      L_ARAM_RATE_END              BINARY_DOUBLE;
      L_LIFE                       BINARY_DOUBLE;
      L_BASIS_AMOUNT_BEG           BINARY_DOUBLE;
      L_BASIS_AMOUNT_END           BINARY_DOUBLE;
      L_BASIS_AMOUNT_ACTIVITY      BINARY_DOUBLE;
      L_BASIS_AMOUNT_TRANSFER      BINARY_DOUBLE;
      L_NUMBER_MONTHS_BEG          BINARY_DOUBLE;
      L_INPUT_AMORTIZATION         BINARY_DOUBLE;
      L_AMOUNT_TO_AMORTIZE         BINARY_DOUBLE;

      L_TAX_BALANCE_END           BINARY_DOUBLE;
      L_ACCUM_RESERVE_END         BINARY_DOUBLE;
      L_BASIS_DIFF_ADD_RET        BINARY_DOUBLE;
      L_BASIS_DIFF_ACTIVITY_SPLIT BINARY_DOUBLE;

      L_SCOOP             pls_integer;
      L_MORE_FROM_DEPR    pls_integer := 0;
      L_ALLOCATION        boolean;
      L_RETIRE_REVERSAL   boolean;
      L_TAX_DEPR_REVERSAL boolean;

      --
      -- Move the items in the deferred tax table to variables
      --   Get rid of the gets we don't need
      --

   begin

      L_TAX_YEAR                   := G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).TAX_YEAR;
      L_DEF_INCOME_TAX_BALANCE_BEG := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).DEF_INCOME_TAX_BALANCE_BEG, 0) +
                                      NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).DEF_INCOME_TAX_BAL_TRANSFER, 0);
      --def_income_tax_balance_end     := dw_def_tax.getitemnumber(r,'def_income_tax_balance_end ')
      L_NORM_DIFF_BALANCE_BEG := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).NORM_DIFF_BALANCE_BEG, 0) +
                                 NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).NORM_DIFF_BALANCE_TRANSFER, 0);
      L_BASIS_AMOUNT_BEG      := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_AMOUNT_BEG, 0);
      L_BASIS_AMOUNT_END      := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_AMOUNT_END, 0);
      L_BASIS_AMOUNT_ACTIVITY := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_AMOUNT_ACTIVITY, 0);
      L_BASIS_AMOUNT_TRANSFER := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_AMOUNT_TRANSFER, 0);

      --norm_diff_balance_end          := nvl(g_def_tax_basis_rec(a_def_tax_index ).norm_diff_balance_end;
      --def_income_tax_provision       := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_provision;
      --def_income_tax_reversal        := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_reversal;
      L_DEF_INCOME_TAX_ADJUST := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).DEF_INCOME_TAX_ADJUST, 0);
      --def_income_tax_retire          := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_retire;
      --def_income_tax_gain_loss       := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_gain_loss;
      L_ARAM_RATE := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).ARAM_RATE, 0);
      --aram_rate_end                  := nvl(g_def_tax_basis_rec(a_def_tax_index ).aram_rate_end;
      L_LIFE               := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).LIFE, 0);
      L_VINTAGE_YEAR       := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).VINTAGE_YEAR, 0);
      L_INPUT_AMORTIZATION := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).INPUT_AMORTIZATION, 0);
      L_NUMBER_MONTHS_BEG  := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).NUMBER_MONTHS_BEG, 0);
      --amount_to_amortize             := nvl(g_def_tax_basis_rec(a_def_tax_index ).amount_to_amortize;
      L_TAX_BALANCE_END           := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).TAX_BALANCE_END, 0);
      L_ACCUM_RESERVE_END         := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).ACCUM_RESERVE_END, 0);
      L_BASIS_DIFF_ADD_RET        := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_DIFF_ADD_RET, 0);
      L_BASIS_DIFF_ACTIVITY_SPLIT := NVL(G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_DIFF_ACTIVITY_SPLIT, 0);

      --env_temp = getenv("TEMP");
      --sprintf(filename,"%s\\tfitcalc.txt",env_temp);
      --unit = fopen(filename,"a+");
      --fprintf(unit ,"%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t\n",
      --    tax_year ,
      --def_income_tax_balance_beg ,
      --norm_diff_balance_beg ,
      --basis_amount_beg,
      --basis_amount_end,
      --basis_amount_activity,
      --basis_amount_transfer,
      --def_income_tax_adjust ,
      --aram_rate ,
      --life ,
      --vintage_year ,
      --input_amortization ,
      --number_months_beg,
      --amount_to_amortize ,
      --def_tax->book_depr_alloc_ind,
      --def_tax->basis_diff_retire_reversal,
      --def_tax->amortization_type_id);

      /*
       if(def_tax->tax_balance_end == 0)
          amount_to_amortize = - (def_tax->norm_diff_balance_beg -
                            def_tax->basis_amount_activity);
       else
          amount_to_amortize =  - (((def_tax->tax_balance_end  -
                                   def_tax->accum_reserve_end)
                                 / def_tax->tax_balance_end ) *
                             def_tax->basis_amount_end )
                                   -  (def_tax->norm_diff_balance_beg
                            -   def_tax->basis_amount_activity);
      */

      if (G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BOOK_DEPR_ALLOC_IND >= 1) then
         L_ALLOCATION := true;
      else
         L_ALLOCATION := false;
      end if;

      if (G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_DIFF_RETIRE_REVERSAL = 1) then
         L_RETIRE_REVERSAL := false;
      else
         L_RETIRE_REVERSAL := true;
      end if;

      if (G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).AMORTIZATION_TYPE_ID = -1) then
         L_TAX_DEPR_REVERSAL := true;
         L_RETIRE_REVERSAL   := false; -- this combination is always true,so force it
      else
         L_TAX_DEPR_REVERSAL := false;
      end if;

      -- Calculate amount of tax-only defferred tax to amortize
      if (L_TAX_BALANCE_END = 0) then
         L_AMOUNT_TO_AMORTIZE := - (L_NORM_DIFF_BALANCE_BEG - L_BASIS_AMOUNT_ACTIVITY);
      else
         L_AMOUNT_TO_AMORTIZE := - (((L_TAX_BALANCE_END - L_ACCUM_RESERVE_END) / L_TAX_BALANCE_END) *
                                    L_BASIS_AMOUNT_END) - L_NORM_DIFF_BALANCE_BEG;
         if (L_BASIS_AMOUNT_ACTIVITY != 0) then
            L_AMOUNT_TO_AMORTIZE := L_AMOUNT_TO_AMORTIZE - L_BASIS_DIFF_ADD_RET;
         end if;
      end if;

      --
      --  We may need Deferred Tax Rates
      --  Get the most recent effective deferred tax rate
      --  The datawindow comes in ordered by ascending effective date
      --

      --dw_def_tax_rates.setsort('effective_date a')
      --dw_def_tax_rates.sort()

      for I in 1 .. A_DEF_TAX_RATES_ROWS
      loop
         L_DEF_TAX_RATES(I) := A_DEF_TAX_RATES_SUB_REC(I).ANNUAL_RATE;
         L_DEF_TAX_DATES(I) := A_DEF_TAX_RATES_SUB_REC(I).EFFECTIVE_DATE;
      end loop;

      -- YYYYMMDD
      L_TAX_DATE := (ROUND(L_TAX_YEAR) * 10000) + 100 + 1;
      --tax_date := date('1/1/' +string(tax_year))

      L_RATE_INDEX := 0;

      for I in 1 .. A_DEF_TAX_RATES_ROWS
      loop
         if (L_TAX_DATE >= L_DEF_TAX_DATES(I)) then
            L_RATE_INDEX := I;
         end if;
      end loop;

      if (L_RATE_INDEX = 0) then
         --messagebox('Warning','No Good Def Tax Rates')
         L_RATE_INDEX := 1;
      end if;

      -- Remaining Life
      if (L_VINTAGE_YEAR = ROUND(L_TAX_YEAR)) then
         L_REM_LIFE := L_LIFE;
      else
         L_REM_LIFE := ((L_LIFE * 12) - (L_NUMBER_MONTHS_BEG - 6)) / 12;
      end if;
      -- rem_life := vintage_year + life - tax_year + .5;

      if (L_REM_LIFE < 1) then
         L_REM_LIFE := 1;
      end if;

      -- the 'ending aram rate' is the one we use this year

      L_ARAM_RATE_END := 0; --this is a div by 0 check

      if (L_NORM_DIFF_BALANCE_BEG <> 0) then
         L_ARAM_RATE_END := L_DEF_INCOME_TAX_BALANCE_BEG / L_NORM_DIFF_BALANCE_BEG;
      else
         L_ARAM_RATE_END := L_DEF_TAX_RATES(L_RATE_INDEX);
      end if;

      -- Check for activity
      -- The signs are confusing here,  Book-Basis differences in tax_book_reconcile are
      -- Negative.  The natural sign for a retirement is going to be negative.

      L_ACTIVITY := - (L_BASIS_AMOUNT_END - L_BASIS_AMOUNT_BEG - L_BASIS_AMOUNT_TRANSFER);

      -- if the activity has a different sign than the Diff_balance, then it is a retirement
      -- also, if norm_diff_balance_beg is zero then this is probably initial addition.

      if (L_BASIS_AMOUNT_ACTIVITY = 0) then
         L_RETIREMENT := L_ACTIVITY;
         L_ADDITION   := 0;
      else
         --addition   := activity;
         --retirement := 0;
         if (L_BASIS_AMOUNT_TRANSFER <> 0) then
            L_ADDITION   := L_ACTIVITY;
            L_RETIREMENT := 0;
         else
            --retirement := -(basis_amount_end - (basis_amount_beg + basis_amount_activity));
            --addition   := activity - retirement;
            if (L_BASIS_DIFF_ACTIVITY_SPLIT = 1) then
               L_RETIREMENT := - (L_BASIS_AMOUNT_END -
                                  (L_BASIS_AMOUNT_BEG + L_BASIS_AMOUNT_ACTIVITY));
               L_ADDITION   := L_ACTIVITY - L_RETIREMENT;
            else
               L_ADDITION   := L_ACTIVITY;
               L_RETIREMENT := 0;
            end if;
         end if;
      end if;

      -- If it is a retirement, then ratio a retirement amount out of the
      -- unamortized balance

      if ((L_BASIS_AMOUNT_BEG + L_BASIS_AMOUNT_TRANSFER + L_ADDITION) = 0) then
         --div by 0 check
         L_RATIO := 0;
      else
         L_RATIO := (-L_NORM_DIFF_BALANCE_BEG + L_ADDITION) /
                    (L_BASIS_AMOUNT_BEG + L_BASIS_AMOUNT_TRANSFER + L_ADDITION);
      end if;

      L_RETIREMENT := L_RETIREMENT * L_RATIO;

      if (L_RETIRE_REVERSAL = false) then
         L_RETIREMENT := 0;
      end if;
      -- Check for end-of-life; or just calc the amortization

      --if rem_life := 1 or
      if (ROUND(L_NORM_DIFF_BALANCE_BEG + L_ADDITION, 1) *
         ROUND(L_NORM_DIFF_BALANCE_BEG + L_ADDITION + L_RETIREMENT, 1) <= 0) then
         L_RETIREMENT := - (L_NORM_DIFF_BALANCE_BEG + L_ADDITION);
         L_AMORT      := 0;
      else
         if (L_TAX_DEPR_REVERSAL = true) then
            L_AMORT := L_AMOUNT_TO_AMORTIZE;
         else
            L_AMORT := - (L_NORM_DIFF_BALANCE_BEG + (L_ADDITION * .5) + L_RETIREMENT) *
                        (1 / L_REM_LIFE) * (A_SHORT_MONTHS / 12);
         end if;
      end if;
      -- OVERRIDE with input, if entered

      if (L_ALLOCATION = true) then
         L_AMORT := L_INPUT_AMORTIZATION;
      end if;

      L_DEF_INCOME_TAX_RETIRE    := L_RETIREMENT * L_ARAM_RATE_END;
      L_DEF_INCOME_TAX_PROVISION := L_ADDITION * L_DEF_TAX_RATES(L_RATE_INDEX);
      L_DEF_INCOME_TAX_REVERSAL  := (L_AMORT + L_RETIREMENT) * L_ARAM_RATE_END;

      L_DEF_INCOME_TAX_BALANCE_END := L_DEF_INCOME_TAX_BALANCE_BEG + L_DEF_INCOME_TAX_PROVISION +
                                      L_DEF_INCOME_TAX_REVERSAL + L_DEF_INCOME_TAX_ADJUST;

      L_NORM_DIFF_BALANCE_END := L_NORM_DIFF_BALANCE_BEG + L_ADDITION + L_RETIREMENT + L_AMORT;

      --
      -- Put Everything back into the datawindows
      --

      if (ABS(L_DEF_INCOME_TAX_BALANCE_END) < .0000001) then
         L_DEF_INCOME_TAX_BALANCE_END := 0;
      end if;
      G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).DEF_INCOME_TAX_BALANCE_END := L_DEF_INCOME_TAX_BALANCE_END;

      if (ABS(L_NORM_DIFF_BALANCE_END) < .0000001) then
         L_NORM_DIFF_BALANCE_END := 0;
      end if;
      G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).NORM_DIFF_BALANCE_END := L_NORM_DIFF_BALANCE_END;

      if (ABS(L_DEF_INCOME_TAX_PROVISION) < .0000001) then
         L_DEF_INCOME_TAX_PROVISION := 0;
      end if;
      G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).DEF_INCOME_TAX_PROVISION := L_DEF_INCOME_TAX_PROVISION;

      if (ABS(L_DEF_INCOME_TAX_REVERSAL) < .0000001) then
         L_DEF_INCOME_TAX_REVERSAL := 0;
      end if;
      G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).DEF_INCOME_TAX_REVERSAL := L_DEF_INCOME_TAX_REVERSAL;

      if (ABS(L_DEF_INCOME_TAX_RETIRE) < .0000001) then
         L_DEF_INCOME_TAX_RETIRE := 0;
      end if;
      G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).DEF_INCOME_TAX_RETIRE := L_DEF_INCOME_TAX_RETIRE;

      if (ABS(L_ARAM_RATE_END) < .0000001) then
         L_ARAM_RATE_END := 0;
      end if;
      G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).ARAM_RATE_END := L_ARAM_RATE_END;

      if (ABS(L_ADDITION + L_RETIREMENT) < .0000001) then
         G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_DIFF_ADD_RET := 0;
      else
         G_DEF_TAX_BASIS_REC(A_DEF_TAX_INDEX).BASIS_DIFF_ADD_RET := L_ADDITION + L_RETIREMENT;
      end if;

      -- the following for display purposes only in Pat's window

      --dw_def_tax.setitem(r,'ratio',                      ratio)

      -- Next year's balances

      if (A_DEF_TAX_BALS_INDEX <> 0) then
         if (ABS(L_DEF_INCOME_TAX_BALANCE_END) < .0000001) then
            L_DEF_INCOME_TAX_BALANCE_END := 0;
         end if;
         G_DEF_TAX_BASIS_REC(A_DEF_TAX_BALS_INDEX).DEF_INCOME_TAX_BALANCE_BEG := L_DEF_INCOME_TAX_BALANCE_END;
         if (ABS(L_NORM_DIFF_BALANCE_END) < .0000001) then
            L_NORM_DIFF_BALANCE_END := 0;
         end if;
         G_DEF_TAX_BASIS_REC(A_DEF_TAX_BALS_INDEX).NORM_DIFF_BALANCE_BEG := L_NORM_DIFF_BALANCE_END;
         if (ABS(L_ARAM_RATE_END) < .0000001) then
            L_ARAM_RATE_END := 0;
         end if;
         G_DEF_TAX_BASIS_REC(A_DEF_TAX_BALS_INDEX).ARAM_RATE := L_ARAM_RATE_END;
      end if;

      return 1;
   end BASIS_CALC;

   -- =============================================================================
   --  Procedure SET_SESSION_PARAMETER
   -- =============================================================================
   procedure SET_SESSION_PARAMETER is
      cursor SESSION_PARAM_CUR is
         select PARAMETER, value, type
           from PP_SESSION_PARAMETERS
          where LOWER(USERS) = 'all'
             or LOWER(USERS) = 'dfit';
      L_COUNT pls_integer;
      I       pls_integer;
      type TYPE_SESSION_PARAM_REC is table of SESSION_PARAM_CUR%rowtype;
      L_SESSION_PARAM_REC TYPE_SESSION_PARAM_REC;
      L_CODE              pls_integer;
      L_SQL               varchar2(200);

   begin

      open SESSION_PARAM_CUR;
      fetch SESSION_PARAM_CUR bulk collect
         into L_SESSION_PARAM_REC;
      close SESSION_PARAM_CUR;

      L_COUNT := L_SESSION_PARAM_REC.COUNT;
      for I in 1 .. L_COUNT
      loop
         if L_SESSION_PARAM_REC(I).TYPE = 'number' or L_SESSION_PARAM_REC(I).TYPE = 'boolean' then
            L_SQL := 'alter session set ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ' || L_SESSION_PARAM_REC(I).VALUE;
         elsif L_SESSION_PARAM_REC(I).TYPE = 'string' then
            L_SQL := 'alter session set ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ''' || L_SESSION_PARAM_REC(I).VALUE || '''';
         end if;
         execute immediate L_SQL;
         WRITE_LOG(G_JOB_NO,
                   0,
                   0,
                   'Set Session Parameter ' || L_SESSION_PARAM_REC(I).PARAMETER || ' = ' || L_SESSION_PARAM_REC(I).VALUE);
      end loop;
      return;
   exception
      when NO_DATA_FOUND then
         return;
      when others then
         L_CODE := sqlcode;
         WRITE_LOG(G_JOB_NO,
                   5,
                   L_CODE,
                   'Set Session Parameter ' || sqlerrm(L_CODE) || ' ' ||
                   DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return;
   end SET_SESSION_PARAMETER;

   -- =============================================================================
   --  Procedure WRITE_LOG
   -- =============================================================================
   procedure WRITE_LOG(A_JOB_NO     number,
                       A_ERROR_TYPE number,
                       A_CODE       number,
                       A_MSG        varchar2) as
      pragma autonomous_transaction;
      L_CODE   pls_integer;
      L_MSG    varchar(2000);
      L_CUR_TS timestamp;

   begin
      L_CUR_TS := CURRENT_TIMESTAMP;
      L_MSG    := A_MSG;
      DBMS_OUTPUT.PUT_LINE(L_MSG);
      insert into TAX_JOB_LOG
         (JOB_NO, LINE_NO, LOG_DATE, ERROR_TYPE, error_code, MSG)
      values
         (A_JOB_NO, G_LINE_NO, sysdate, A_ERROR_TYPE, A_CODE, L_MSG);
      commit;
      G_LINE_NO := G_LINE_NO + 1;
      return;
   exception
      when others then
         L_CODE := sqlcode;
         DBMS_OUTPUT.PUT_LINE('Write Log ' || sqlerrm(L_CODE) || ' ' ||
                              DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return;
   end WRITE_LOG;

end TAX_DFIT_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (409, 0, 10, 4, 1, 0, 29581, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029581_pwrtax_TAX_DFIT_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
