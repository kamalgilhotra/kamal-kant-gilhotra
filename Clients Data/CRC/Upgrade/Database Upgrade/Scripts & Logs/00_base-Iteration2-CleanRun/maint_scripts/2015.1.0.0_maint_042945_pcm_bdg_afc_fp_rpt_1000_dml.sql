--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042945_pcm_bdg_afc_fp_rpt_1000_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1.0.0 03/04/2015 Anand R          Budget - AFUDC FP 1000 Report Conversion.
--||============================================================================
--*/

--Delete old mapping. This came from Daniel.
delete from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 89 and filter_id = 245;

--Create new dynamic filters
insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (275, 'Contract Admin', 'dw', 'upper(work_order_initiator.contract_adm)', 'work_order_control.work_order_id in ( select distinct work_order_id from work_order_initiator where [selected_values] )', 'dw_pp_contract_admin_filter', 'contract_adm', 'description', 'C', null, null, null, null, 'PWRPLANT', to_date('21-11-2014 14:28:36', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (276, 'Engineer', 'dw', 'upper(work_order_initiator.engineer)', 'work_order_control.work_order_id in ( select distinct work_order_id from work_order_initiator where [selected_values] )', 'dw_pp_engineer_filter', 'engineer', 'description', 'C', null, null, null, null, 'PWRPLANT', to_date('21-11-2014 14:28:36', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (277, 'Initiator', 'dw', 'upper( work_order_initiator.users )', 'work_order_control.work_order_id in ( select distinct work_order_id from work_order_initiator where [selected_values] )', 'dw_pp_initiator_filter', 'users', 'description', 'C', null, null, null, null, 'PWRPLANT', to_date('05-02-2015 11:18:34', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (278, 'Plant Accountant', 'dw', 'upper(work_order_initiator.plant_accountant)', 'work_order_control.work_order_id in ( select distinct work_order_id from work_order_initiator where [selected_values] )', 'dw_pp_plant_accountant_filter', 'plant_accountant', 'description', 'C', null, null, null, null, 'PWRPLANT', to_date('21-11-2014 14:28:36', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);

insert into pp_dynamic_filter (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE)
values (279, 'Other', 'dw', 'upper(work_order_initiator.other)', 'work_order_control.work_order_id in ( select distinct work_order_id from work_order_initiator where [selected_values] )', 'dw_pp_other_filter', 'other', 'description', 'C', null, null, null, null, 'PWRPLANT', to_date('21-11-2014 14:28:36', 'dd-mm-yyyy hh24:mi:ss'), null, null, null);

-- Update the mapping for filter id 89

update pp_dynamic_filter_mapping 
set filter_id = 275
where pp_report_filter_id = 89 and filter_id = 238;

update pp_dynamic_filter_mapping 
set filter_id = 276
where pp_report_filter_id = 89 and filter_id = 242;

update pp_dynamic_filter_mapping 
set filter_id = 277
where pp_report_filter_id = 89 and filter_id = 243;

update pp_dynamic_filter_mapping 
set filter_id = 278
where pp_report_filter_id = 89 and filter_id = 244;

update pp_dynamic_filter_mapping 
set filter_id = 279
where pp_report_filter_id = 89 and filter_id = 247;

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2347, 0, 2015, 1, 0, 0, 42945, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042945_pcm_bdg_afc_fp_rpt_1000_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;