/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042713_pcm_comp_rollup_filter_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	02/05/2014 Ryan Oliveria		Company Rollup Filter
||========================================================================================
*/

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(260, 'Company Closing Rollup', 'dw', 'company.closing_rollup_id', 'dw_pp_company_closing_rollup_filter', 'closing_rollup_id', 'description', 'N');


insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 260) /*FP*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (87, 260) /*WO*/;

insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (74, 3, 260, 'company.closing_rollup_id');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2254, 0, 2015, 1, 0, 0, 042713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042713_pcm_comp_rollup_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;