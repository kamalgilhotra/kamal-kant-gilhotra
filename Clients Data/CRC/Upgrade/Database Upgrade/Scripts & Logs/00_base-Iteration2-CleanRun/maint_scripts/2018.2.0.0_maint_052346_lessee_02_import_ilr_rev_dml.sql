/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052436_lessee_02_import_ilr_rev_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 3/5/2019 	 Shane "C" Ward   Import Type for ILR Revisions
||============================================================================
*/

--Define new type
INSERT INTO pp_import_type
  (import_type_id,
   description,
   long_description,
   import_table_name,
   archive_table_name,
   allow_updates_on_add,
   delegate_object_name)
  SELECT 272,
         'Add: ILR Revisions',
         'Lessee ILR Revisions',
         'ls_import_ilr_rev',
         'ls_import_ilr_rev_arc',
         1 AS allow_updates_on_add,
         'nvo_ls_logic_import' AS delegate_object_name
  FROM   dual
;

INSERT INTO pp_import_type_subsystem (import_type_id, import_subsystem_id)
VALUES (272, 8);

--Define Columns
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'ilr_id', 'ILR', 'ilr_xlate', 1, 1, 'number(22,0)', 'ls_ilr', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'discount_rate', 'Discount Rate', 'discount_rate', 0, 1, 'number(22,8)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'payment_term_id', 'Payment Term ID', 'payment_term_id', 1, 1, 'number(22,0)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'payment_term_type_id', 'Payment Term Type', 'payment_term_type_xlate', 1, 1, 'number(22,0)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'payment_term_date', 'Payment Term Date', 'payment_term_date', 1, 1, 'date mmddyyyy format', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'payment_freq_id', 'Payment Frequency', 'payment_freq_xlate', 1, 1, 'number(22,0)', 'ls_payment_freq', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'number_of_terms', 'Number of Terms', 'number_of_terms', 1, 1, 'number(22,0)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'paid_amount', 'Paid Amount', 'paid_amount', 1, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'escalation', 'Escalation', 'escalation_xlate', 0, 1, 'number(22,0)', 'ls_variable_payment', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'escalation_freq_id', 'Escalation Frequency', 'escalation_freq_xlate', 0, 1, 'number(22,0)', 'ls_payment_freq', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'escalation_pct', 'Escalation Percent', 'escalation_pct', 0, 1, 'number(22,12)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'remeasurement_date', 'Remeasurement Date', 'remeasurement_date', 0, 1, 'date mmddyyyy format', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'lease_cap_type_id', 'Lease Cap Type', 'lease_cap_type_xlate', 0, 1, 'number(22,0)', 'ls_lease_cap_type', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_1', 'Executory Bucket 1', 'e_bucket_1', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_2', 'Executory Bucket 2', 'e_bucket_2', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_3', 'Executory Bucket 3', 'e_bucket_3', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_4', 'Executory Bucket 4', 'e_bucket_4', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_5', 'Executory Bucket 5', 'e_bucket_5', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_6', 'Executory Bucket 6', 'e_bucket_6', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_7', 'Executory Bucket 7', 'e_bucket_7', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_8', 'Executory Bucket 8', 'e_bucket_8', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_9', 'Executory Bucket 9', 'e_bucket_9', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'e_bucket_10', 'Executory Bucket 10', 'e_bucket_10', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_1', 'Contingent Bucket 1', 'c_bucket_1', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_2', 'Contingent Bucket 2', 'c_bucket_2', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_3', 'Contingent Bucket 3', 'c_bucket_3', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_4', 'Contingent Bucket 4', 'c_bucket_4', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_5', 'Contingent Bucket 5', 'c_bucket_5', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_6', 'Contingent Bucket 6', 'c_bucket_6', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_7', 'Contingent Bucket 7', 'c_bucket_7', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_8', 'Contingent Bucket 8', 'c_bucket_8', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_9', 'Contingent Bucket 9', 'c_bucket_9', 0, 1, 'number(22,2)', null, null);
INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_bucket_10', 'Contingent Bucket 10', 'c_bucket_10', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_1_incl_initial_measure', 'Contingent 1 Incl in Init Measure', 'c_1_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_2_incl_initial_measure', 'Contingent 2 Incl in Init Measure', 'c_2_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_3_incl_initial_measure', 'Contingent 3 Incl in Init Measure', 'c_3_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_4_incl_initial_measure', 'Contingent 4 Incl in Init Measure', 'c_4_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_5_incl_initial_measure', 'Contingent 5 Incl in Init Measure', 'c_5_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_6_incl_initial_measure', 'Contingent 6 Incl in Init Measure', 'c_6_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_7_incl_initial_measure', 'Contingent 7 Incl in Init Measure', 'c_7_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_8_incl_initial_measure', 'Contingent 8 Incl in Init Measure', 'c_8_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_9_incl_initial_measure', 'Contingent 9 Incl in Init Measure', 'c_9_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'c_10_incl_initial_measure', 'Contingent 10 Incl in Init Measure', 'c_10_incl_xlate', 0, 1, 'number(22,2)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'itc_sw', 'ITC Switch', 'itc_xlate', 0, 1, 'number(1,0)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'cancelable_type_id', 'Cancelable Type', 'cancelable_type_xlate', 0, 1, 'number(22,0)', 'ls_cancelable_type', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'specialized_asset', 'Specialized Asset', 'specialized_asset_xlate', 0, 1, 'number(1,0)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'intent_to_purch', 'Intent to Purchase', 'intent_to_purch_xlate', 0, 1, 'number(1,0)', null, null);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'interco_company_id', 'Intercompany Company', 'interco_company_xlate', 0, 1, 'number(22,0)', 'company', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'interco_lease', 'Intercompany Lease', 'interco_lease_xlate', 0, 1, 'number(1,0)', 'yes_no', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'sublease_flag', 'Sublease Flag', 'sublease_flag_xlate', 0, 1, 'number(1,0)', 'yes_no', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'sublease_id', 'Sublease Lease', 'sublease_xlate', 0, 1, 'number(22,0)', 'lsr_lease', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'depr_calc_method', 'Depr Units of Production', 'depr_calc_method_xlate', 0, 1, 'number(1,0)', 'yes_no', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'remeasure_calc_gain_loss', 'Remeasurement Calculate Gain/Loss', 'remeasure_calc_gain_xlate', 0, 1, 'number(1,0)', 'yes_no', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'default_rate_flag', 'Default Rate Flag', 'default_rate_flag_xlate', 0, 1, 'number(1,0)', 'yes_no', 1);

INSERT INTO pp_import_column (import_type_id, column_name, description, import_column_name, is_required, processing_order, column_type, parent_table,
is_on_table)
VALUES (272, 'borrowing_curr_id', 'Borrowing Currency', 'borrowing_curr_xlate', 0, 1, 'number(22,0)', 'currency', 1);

--Define new Lookups
--ILR ID
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'ilr_id', 1096);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'ilr_id', 2505);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'ilr_id', 2602);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'ilr_id', 4507);

--Payment Freq
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'payment_freq_id', 1047);

--Cap Type
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'lease_cap_type_id', 1046);

--Include in Initial Measure Switches (1-10)
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_1_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_1_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_2_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_2_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_3_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_3_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_4_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_4_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_5_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_5_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_6_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_6_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_7_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_7_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_8_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_8_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_9_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_9_incl_initial_measure', 78);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_10_incl_initial_measure', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'c_10_incl_initial_measure', 78);

--ITC Switch
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'itc_sw', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'itc_sw', 78);

--Cancelable Type
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'cancelable_type_id', 1044);

--Specialized Asset
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'specialized_asset', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'specialized_asset', 78);

--Intent to Purchase
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'intent_to_purch', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'intent_to_purch', 78);

--Intercompany Company ID
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'interco_company_id', 19);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'interco_company_id', 20);

--Intercompany Lease
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'interco_lease', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'interco_lease', 78);

--Sublease Lease
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'sublease_id', 1083);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'sublease_id', 1084);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'sublease_id', 1085);

--Sublease Flag
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'sublease_flag', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'sublease_flag', 78);

--Depr Calc Method
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'depr_calc_method', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'depr_calc_method', 78);

--Remeasure Calc Gain Loss
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'remeasure_calc_gain_loss', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'remeasure_calc_gain_loss', 78);

--Default Rate Flag
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'default_rate_flag', 77);
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'default_rate_flag', 78);

--Borrowing Currency
INSERT INTO pp_import_column_lookup
(import_type_id, column_name, import_lookup_id)
VALUES (272, 'borrowing_curr_id', 2509);

--Escalation
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (272,
             'escalation',
             1118);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (272,
             'escalation',
             1119);

--Escalation Frequency
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (272,
             'escalation_freq_id',
             1047);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (272,
             'escalation_freq_id',
             1064);
			 
--Payment Term Type
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (272,
             'payment_term_type_id',
             4513);
			 
--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15804, 0, 2018, 2, 0, 0, 52346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052346_lessee_02_import_ilr_rev_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;			 
