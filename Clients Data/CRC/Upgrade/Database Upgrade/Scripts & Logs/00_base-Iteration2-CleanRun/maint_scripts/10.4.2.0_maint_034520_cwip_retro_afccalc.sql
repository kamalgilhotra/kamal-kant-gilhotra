/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034520_cwip_retro_afccalc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By      Reason for Change
|| -------- ---------- --------------- ---------------------------------------
|| 10.4.2.0 12/3/2013  Sunjin Cone     Point Release
||============================================================================
*/

alter table AFUDC_CALC add CPI_RETRO number(22,2);

comment on column AFUDC_CALC.CPI_RETRO is 'The amount of cpi calculated in the Retro Calculation.';

update AFUDC_CALC B
   set B.CPI_RETRO =
        (select INSIDE.TOT_DIFF_AMOUNT
           from (select WORK_ORDER_ID,
                        TO_DATE(MONTH_NUMBER, 'yyyymm') month,
                        sum(DIFF_AMOUNT) TOT_DIFF_AMOUNT
                   from CPI_RETRO_COMPARE
                  where BATCH_ID in
                        (select BATCH_ID from CPI_RETRO_BATCH_CONTROL where POST_DATE is not null)
                  group by WORK_ORDER_ID, TO_DATE(MONTH_NUMBER, 'yyyymm')) INSIDE
          where INSIDE.WORK_ORDER_ID = B.WORK_ORDER_ID
            and INSIDE.MONTH = B.MONTH
            and INSIDE.TOT_DIFF_AMOUNT <> 0)
 where (B.WORK_ORDER_ID, B.MONTH) in
       (select INSIDE.WORK_ORDER_ID, INSIDE.MONTH
          from (select WORK_ORDER_ID,
                       TO_DATE(MONTH_NUMBER, 'yyyymm') month,
                       sum(DIFF_AMOUNT) TOT_DIFF_AMOUNT
                  from CPI_RETRO_COMPARE
                 where BATCH_ID in
                       (select BATCH_ID from CPI_RETRO_BATCH_CONTROL where POST_DATE is not null)
                 group by WORK_ORDER_ID, TO_DATE(MONTH_NUMBER, 'yyyymm')) INSIDE
         where INSIDE.WORK_ORDER_ID = B.WORK_ORDER_ID
           and INSIDE.MONTH = B.MONTH
           and INSIDE.TOT_DIFF_AMOUNT <> 0);

update AFUDC_CALC set CPI_RETRO = 0 where CPI_RETRO is null;

update AFUDC_CALC B
   set B.CPI =
        (select sum(A.AMOUNT) CWIP_CPI
           from CWIP_CHARGE A
          where A.EXPENDITURE_TYPE_ID = 1
            and A.STATUS = 1
            and A.COST_ELEMENT_ID in (select CPI_COST_ELEMENT from AFUDC_CONTROL)
            and (A.WORK_ORDER_ID, A.MONTH_NUMBER) in
                (select WORK_ORDER_ID, MONTH_NUMBER
                   from CPI_RETRO_COMPARE
                  where BATCH_ID in
                        (select BATCH_ID from CPI_RETRO_BATCH_CONTROL where POST_DATE is not null)
                    and DIFF_AMOUNT <> 0)
            and A.WORK_ORDER_ID = B.WORK_ORDER_ID
            and TO_DATE(A.MONTH_NUMBER, 'yyyymm') = B.MONTH
          group by A.MONTH_NUMBER, A.WORK_ORDER_ID)
 where exists
 (select 1
          from CWIP_CHARGE A
         where A.EXPENDITURE_TYPE_ID = 1
           and A.STATUS = 1
           and A.COST_ELEMENT_ID in (select CPI_COST_ELEMENT from AFUDC_CONTROL)
           and (A.WORK_ORDER_ID, A.MONTH_NUMBER) in
               (select WORK_ORDER_ID, MONTH_NUMBER
                  from CPI_RETRO_COMPARE
                 where BATCH_ID in
                       (select BATCH_ID from CPI_RETRO_BATCH_CONTROL where POST_DATE is not null)
                   and DIFF_AMOUNT <> 0)
           and A.WORK_ORDER_ID = B.WORK_ORDER_ID
           and TO_DATE(A.MONTH_NUMBER, 'yyyymm') = B.MONTH);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (784, 0, 10, 4, 2, 0, 34520, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034520_cwip_retro_afccalc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;