/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051019_lessee_01_add_fasb_ct_sob_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 04/20/2018 Josh Sandler     Add book summary to LS_FASB_CAP_TYPE_SOB_MAP
||============================================================================
*/

ALTER TABLE LS_FASB_CAP_TYPE_SOB_MAP
  ADD book_summary_id NUMBER(22) NULL;

COMMENT ON COLUMN LS_FASB_CAP_TYPE_SOB_MAP.book_summary_id IS 'The basis bucket that stores this Leases dollars.';

ALTER TABLE LS_FASB_CAP_TYPE_SOB_MAP
  ADD CONSTRAINT fk_ls_fasb_ct_bk_summary FOREIGN KEY (
    book_summary_id
  ) REFERENCES book_summary (
    book_summary_id
  );
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5022, 0, 2017, 4, 0, 0, 51019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051019_lessee_01_add_fasb_ct_sob_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  