/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033713_reg_04_views.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014  Sarah Byers
||============================================================================
*/

-- REG_COMPANY_SV
create or replace view REG_COMPANY_SV
as
select VALID_USER_ID,
       REG_COMPANY_ID,
       DESCRIPTION,
       PP_COMPANY_ID,
       CR_COMPANY_ID,
       CR_EXTERNAL_COMPANY_ID,
       STATUS_CODE_ID
  from (select RCS.VALID_USER_ID      VALID_USER_ID,
               RC.REG_COMPANY_ID      REG_COMPANY_ID,
               RC.DESCRIPTION         DESCRIPTION,
               CS.COMPANY_ID          PP_COMPANY_ID,
               CC.CR_COMPANY_ID       CR_COMPANY_ID,
               CC.EXTERNAL_COMPANY_ID CR_EXTERNAL_COMPANY_ID,
               RC.STATUS_CODE_ID      STATUS_CODE_ID
          from REG_COMPANY          RC,
               REG_COMPANY_SECURITY RCS,
               COMPANY_SETUP        CS,
               PP_COMPANY_SECURITY  PCS,
               CR_COMPANY           CC,
               CR_COMPANY_SECURITY  CCS
         where RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
           and CS.COMPANY_ID = CC.CR_COMPANY_ID
           and RC.REG_COMPANY_ID = RCS.VALID_COMPANY_ID
           and CS.COMPANY_ID = PCS.COMPANY_ID
           and CC.EXTERNAL_COMPANY_ID = CCS.VALID_VALUES
           and RCS.VALID_USER_ID = PCS.USERS
           and RCS.VALID_USER_ID = CCS.USERS
           and RCS.VALID_USER_ID = LOWER(user));

-- View used for the other tax item drillback
create or replace view REG_HIST_TA_OTHER_DRILL_VIEW as (
select M.M_ID          M_ID,
       T.DESCRIPTION   M_ITEM,
       V.DESCRIPTION   TA_VERSION,
       CS.DESCRIPTION  COMPANY,
       I.DESCRIPTION   OPER_IND,
       RAM.DESCRIPTION REG_ACCOUNT,
       RAM.REG_ACCT_ID REG_ACCT_ID,
       A.DESCRIPTION   TAXING_ENTITY,
       E.ENTITY_ID     ENTITY_ID,
       V.YEAR          year,
       M.GL_MONTH      GL_MONTH,
       M.BEG_BALANCE   BEG_BALANCE,
       M.END_BALANCE   END_BALANCE,
       M.AMOUNT_EST    AMOUNT_EST,
       M.AMOUNT_ACT    AMOUNT_ACT,
       M.AMOUNT_CALC   AMOUNT_CALC,
       M.AMOUNT_MANUAL AMOUNT_MANUAL,
       M.AMOUNT_ADJ    AMOUNT_ADJ,
       M.AMOUNT_ALLOC  AMOUNT_ALLOC
  from TAX_ACCRUAL_M_ITEM             M,
       TAX_ACCRUAL_CONTROL            C,
       TAX_ACCRUAL_VERSION            V,
       TAX_ACCRUAL_M_MASTER           T,
       TAX_ACCRUAL_ENTITY_INCLUDE_ACT E,
       TAX_ACCRUAL_OPER_IND           I,
       COMPANY_SETUP                  CS,
       REG_TA_CONTROL_MAP             X,
       REG_TA_VERSION_CONTROL         Y,
       REG_TA_M_TYPE_ENTITY_MAP       Z,
       REG_ACCT_MASTER                RAM,
       REG_COMPANY                    RC,
       TAX_ACCRUAL_ENTITY             A,
       CR_DD_REQUIRED_FILTER          HV,
       CR_DD_REQUIRED_FILTER          FC,
       CR_DD_REQUIRED_FILTER          MN
 where CS.COMPANY_ID = C.COMPANY_ID
   and M.M_ID = C.M_ID
   and C.TA_VERSION_ID = V.TA_VERSION_ID
   and X.COMPANY_ID = C.COMPANY_ID
   and X.OPER_IND = C.OPER_IND
   and X.M_ITEM_ID = C.M_ITEM_ID
   and X.HISTORIC_VERSION_ID = Y.HISTORIC_VERSION_ID
   and Y.TA_VERSION_ID = C.TA_VERSION_ID
   and X.HISTORIC_VERSION_ID = Z.HISTORIC_VERSION_ID
   and NVL(Z.TAX_CONTROL_ID, 0) <> 99
   and X.TAX_CONTROL_ID = Z.TAX_CONTROL_ID
   and E.ENTITY_ID = Z.ENTITY_ID
   and E.ENTITY_INCLUDE_ID = C.ENTITY_INCLUDE_ID
   and T.M_TYPE_ID = Z.M_TYPE_ID
   and T.M_ITEM_ID = C.M_ITEM_ID
   and C.OPER_IND = I.OPER_IND
   and E.ENTITY_ID = A.ENTITY_ID
   and X.REG_ACCT_ID = RAM.REG_ACCT_ID
   and TO_NUMBER(TO_CHAR(V.YEAR) ||
                 LPAD(TO_CHAR(DECODE(FLOOR(M.GL_MONTH), 13, 12, FLOOR(M.GL_MONTH))), 2, '0')) =
       TO_NUMBER(MN.FILTER_STRING)
   and UPPER(MN.COLUMN_NAME) = 'GL_MONTH'
   and RC.REG_COMPANY_ID = TO_NUMBER(FC.FILTER_STRING)
   and UPPER(FC.COLUMN_NAME) = 'REG_COMPANY_ID'
   and RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
   and X.HISTORIC_VERSION_ID = TO_NUMBER(HV.FILTER_STRING)
   and UPPER(HV.COLUMN_NAME) = 'HISTORIC_VERSION_ID'
   and RAM.REG_ACCT_ID in (select TO_NUMBER(FILTER_STRING)
                             from CR_DD_REQUIRED_FILTER
                            where UPPER(COLUMN_NAME) = 'REG_ACCT_ID'));


-- View used for the timing difference/deferreds drillback
create or replace view REG_HIST_TA_DEFER_DRILL_VIEW as (
select M.M_ID                        M_ID,
       T.DESCRIPTION                 M_ITEM,
       V.DESCRIPTION                 TA_VERSION,
       CS.DESCRIPTION                COMPANY,
       I.DESCRIPTION                 OPER_IND,
       RAM.DESCRIPTION               REG_ACCOUNT,
       RAM.REG_ACCT_ID               REG_ACCT_ID,
       E.DESCRIPTION                 TAXING_ENTITY,
       D.DESCRIPTION                 DAMPENING_TYPE,
       M.ENTITY_ID                   ENTITY_ID,
       V.YEAR                        year,
       M.GL_MONTH                    GL_MONTH,
       M.ACCUM_DIFF_BEG              ACCUM_DIFF_BEG,
       M.ACCUM_DIFF_END              ACCUM_DIFF_END,
       M.ACCUM_REG_DIT_BEG           ACCUM_REG_DIT_BEG,
       M.ACCUM_REG_DIT_END           ACCUM_REG_DIT_END,
       M.FAS109_LIAB_BEG             FAS109_LIAB_BEG,
       M.FAS109_LIAB_END             FAS109_LIAB_END,
       M.FAS109_INCREMENT_BEG        FAS109_INCREMENT_BEG,
       M.FAS109_INCREMENT_END        FAS109_INCREMENT_END,
       M.TOTAL_GROSSUP_BEG           TOTAL_GROSSUP_BEG,
       M.TOTAL_GROSSUP_END           TOTAL_GROSSUP_END,
       M.ENTITY_GROSSUP_BEG          ENTITY_GROSSUP_BEG,
       M.ENTITY_GROSSUP_END          ENTITY_GROSSUP_END,
       M.FAS109_LIAB_CURRENT         FAS109_LIAB_CURRENT,
       M.ENTITY_GROSSUP_CURRENT      ENTITY_GROSSUP_CURRENT,
       M.TA_NORM_ID                  TA_NORM_ID,
       M.FAS109_TAX_JE_ID            FAS109_TAX_JE_ID,
       M.FAS109_ENTITY_GROSSUP_JE_ID FAS109_ENTITY_GROSSUP_JE_ID
  from COMPANY_SETUP              CS,
       TAX_ACCRUAL_FAS109         M,
       TAX_ACCRUAL_FAS109_CONTROL F,
       TAX_ACCRUAL_CONTROL        C,
       TAX_ACCRUAL_VERSION        V,
       TAX_ACCRUAL_NORM_SCHEMA    N,
       REG_TA_COMPONENT           K,
       REG_ACCT_MASTER            RAM,
       TAX_ACCRUAL_M_MASTER       T,
       TAX_ACCRUAL_OPER_IND       I,
       TAX_ACCRUAL_ENTITY         E,
       TAX_ACCRUAL_DAMPENING_TYPE D,
       REG_TA_COMP_MEMBER_MAP     W,
       REG_TA_FAMILY_MEMBER_MAP   X,
       REG_TA_VERSION_CONTROL     Y,
       REG_COMPANY                RC,
       CR_DD_REQUIRED_FILTER      HV,
       CR_DD_REQUIRED_FILTER      FC,
       CR_DD_REQUIRED_FILTER      MN
 where M.M_ID = F.M_ID
   and M.TA_NORM_ID = F.TA_NORM_ID
   and M.M_ID = C.M_ID
   and C.M_ITEM_ID = T.M_ITEM_ID
   and C.OPER_IND = I.OPER_IND
   and C.COMPANY_ID = CS.COMPANY_ID
   and C.COMPANY_ID = X.COMPANY_ID
   and C.OPER_IND = X.OPER_IND
   and C.M_ITEM_ID = X.M_ITEM_ID
   and X.HISTORIC_VERSION_ID = Y.HISTORIC_VERSION_ID
   and C.TA_VERSION_ID = Y.TA_VERSION_ID
   and C.TA_VERSION_ID = V.TA_VERSION_ID
   and X.HISTORIC_VERSION_ID = W.HISTORIC_VERSION_ID
   and W.REG_COMPONENT_ID = NVL(RAM.REG_COMPONENT_ID, 0)
   and W.REG_FAMILY_ID = NVL(RAM.REG_FAMILY_ID, 0)
   and W.REG_MEMBER_ID = NVL(RAM.REG_MEMBER_ID, 0)
   and W.REG_MEMBER_ID = X.REG_MEMBER_ID
   and W.REG_COMPONENT_ID = K.REG_COMPONENT_ID
   and RAM.REG_SOURCE_ID = 6
   and N.DAMPENING_TYPE_ID in (
                               -- state and fed
                               select DECODE(mod(W.REG_COMPONENT_ID, 2), 0, 4, 1, 1)
                                 from DUAL
                               union
                               -- offsets
                               select DECODE(NVL(K.LOAD_OFFSETS, 0),
                                              0,
                                              -1,
                                              1,
                                              DECODE(mod(W.REG_COMPONENT_ID, 2), 0, 6, 1, 3))
                                 from DUAL)
   and F.TA_NORM_ID = N.TA_NORM_ID
   and N.DAMPENING_TYPE_ID = D.DAMPENING_TYPE_ID
   and N.ENTITY_ID = E.ENTITY_ID
   and TO_NUMBER(TO_CHAR(V.YEAR) ||
                 LPAD(TO_CHAR(DECODE(FLOOR(M.GL_MONTH), 13, 12, FLOOR(M.GL_MONTH))), 2, '0')) =
       TO_NUMBER(MN.FILTER_STRING)
   and UPPER(MN.COLUMN_NAME) = 'GL_MONTH'
   and RC.REG_COMPANY_ID = TO_NUMBER(FC.FILTER_STRING)
   and UPPER(FC.COLUMN_NAME) = 'REG_COMPANY_ID'
   and RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
   and X.HISTORIC_VERSION_ID = TO_NUMBER(HV.FILTER_STRING)
   and UPPER(HV.COLUMN_NAME) = 'HISTORIC_VERSION_ID'
   and RAM.REG_ACCT_ID in (select TO_NUMBER(FILTER_STRING)
                             from CR_DD_REQUIRED_FILTER
                            where UPPER(COLUMN_NAME) = 'REG_ACCT_ID'));


-- REG_HIST_DEPR_DRILLBACK_VIEW
create or replace view REG_HIST_DEPR_DRILLBACK_VIEW
as
select REG.CO_DESC,
       REG.COMPANY_ID,
       REG.DESCRIPTION,
       REG.DEPR_GROUP_ID,
       REG.REG_ACCT_DESC,
       REG.REG_ACCT_ID,
       SET_OF_BOOKS.DESCRIPTION BOOK,
       GL_POST_MO_YR,
       BEGIN_BALANCE,
       ADDITIONS,
       RETIREMENTS,
       TRANSFERS_IN,
       TRANSFERS_OUT,
       ADJUSTMENTS,
       END_BALANCE,
       BEGIN_RESERVE,
       DEPRECIATION_EXPENSE,
       DEPR_EXP_ADJUST,
       DEPR_EXP_ALLOC_ADJUST,
       SALVAGE_EXPENSE,
       SALVAGE_EXP_ADJUST,
       SALVAGE_EXP_ALLOC_ADJUST,
       SALVAGE_RETURNS,
       SALVAGE_CASH,
       RESERVE_CREDITS,
       RESERVE_ADJUSTMENTS,
       RESERVE_TRAN_IN,
       RESERVE_TRAN_OUT,
       GAIN_LOSS,
       END_RESERVE,
       COR_BEG_RESERVE,
       COR_EXPENSE,
       COR_EXP_ADJUST,
       COR_EXP_ALLOC_ADJUST,
       COST_OF_REMOVAL,
       COR_RES_TRAN_IN,
       COR_RES_TRAN_OUT,
       COR_RES_ADJUST,
       COR_END_RESERVE,
       RWIP_COST_OF_REMOVAL,
       RWIP_SALVAGE_CASH,
       RWIP_SALVAGE_RETURNS,
       RWIP_RESERVE_CREDITS
  from DEPR_LEDGER DL,
       SET_OF_BOOKS,
       (select distinct CO.DESCRIPTION   CO_DESC,
                        CO.COMPANY_ID,
                        DG.DESCRIPTION,
                        DG.DEPR_GROUP_ID,
                        RAM.DESCRIPTION  REG_ACCT_DESC,
                        RAM.REG_ACCT_ID
          from REG_ACCT_MASTER            RAM,
               REG_DEPR_FAMILY_MEMBER_MAP RFMM,
               DEPR_GROUP                 DG,
               REG_COMPANY                RC,
               COMPANY_SETUP              CO,
               CR_DD_REQUIRED_FILTER      HV,
               CR_DD_REQUIRED_FILTER      FC
         where RAM.REG_SOURCE_ID = 1
           and RAM.REG_FAMILY_ID = RFMM.REG_FAMILY_ID
           and RAM.REG_MEMBER_ID = RFMM.REG_MEMBER_ID
           and RFMM.HISTORIC_VERSION_ID = TO_NUMBER(HV.FILTER_STRING)
           and UPPER(HV.COLUMN_NAME) = 'HISTORIC_VERSION_ID'
           and RAM.REG_ACCT_ID in (select TO_NUMBER(FILTER_STRING)
                                     from CR_DD_REQUIRED_FILTER
                                    where UPPER(COLUMN_NAME) = 'REG_ACCT_ID')
           and RC.REG_COMPANY_ID = TO_NUMBER(FC.FILTER_STRING)
           and UPPER(FC.COLUMN_NAME) = 'REG_COMPANY_ID'
           and RC.REG_COMPANY_ID = CO.REG_COMPANY_ID
           and RFMM.DEPR_GROUP_ID = DG.DEPR_GROUP_ID) REG,
       CR_DD_REQUIRED_FILTER MN
 where DL.DEPR_GROUP_ID = REG.DEPR_GROUP_ID
   and DL.SET_OF_BOOKS_ID = SET_OF_BOOKS.SET_OF_BOOKS_ID
   and DL.SET_OF_BOOKS_ID = 1
   and DL.GL_POST_MO_YR = TO_DATE(MN.FILTER_STRING, 'yyyymm')
   and UPPER(MN.COLUMN_NAME) = 'GL_MONTH';

-- REG_HISTORY_LEDGER_SV
create or replace view REG_HISTORY_LEDGER_SV
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH
  from (select C.DESCRIPTION      REG_COMPANY,
               M.DESCRIPTION      REG_ACCOUNT,
               T.DESCRIPTION      REG_ACCOUNT_TYPE,
               S.DESCRIPTION      SUB_ACCOUNT_TYPE,
               V.LONG_DESCRIPTION HISTORIC_LEDGER,
               L.GL_MONTH         GL_MONTH,
               L.ACT_AMOUNT       ACT_AMOUNT,
               L.ANNUALIZED_AMT   ANNUALIZED_AMT,
               L.ADJ_AMOUNT       ADJ_AMOUNT,
               L.ADJ_MONTH        ADJ_MONTH
          from REG_COMPANY_SV       C,
               REG_ACCT_MASTER      M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER   L,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

-- REG_HISTORY_LEDGER_ID_SV
create or replace view REG_HISTORY_LEDGER_ID_SV
as
select REG_COMPANY,
       REG_ACCOUNT,
       REG_ACCOUNT_TYPE,
       SUB_ACCOUNT_TYPE,
       HISTORIC_LEDGER,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       HISTORIC_LEDGER_ID
  from (select C.DESCRIPTION         REG_COMPANY,
               M.DESCRIPTION         REG_ACCOUNT,
               T.DESCRIPTION         REG_ACCOUNT_TYPE,
               S.DESCRIPTION         SUB_ACCOUNT_TYPE,
               V.LONG_DESCRIPTION    HISTORIC_LEDGER,
               L.GL_MONTH            GL_MONTH,
               L.ACT_AMOUNT          ACT_AMOUNT,
               L.ANNUALIZED_AMT      ANNUALIZED_AMT,
               L.ADJ_AMOUNT          ADJ_AMOUNT,
               L.ADJ_MONTH           ADJ_MONTH,
               L.REG_COMPANY_ID      REG_COMPANY_ID,
               L.REG_ACCT_ID         REG_ACCT_ID,
               L.HISTORIC_VERSION_ID HISTORIC_LEDGER_ID
          from REG_COMPANY_SV       C,
               REG_ACCT_MASTER      M,
               REG_HISTORIC_VERSION V,
               REG_HISTORY_LEDGER   L,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID);

-- REG_HISTORIC_RECON_LEDGER_SV
create or replace view REG_HISTORIC_RECON_LEDGER_SV
as
select HISTORIC_LEDGER, RECON_ITEM, GL_MONTH, REG_COMPANY, CR_BALANCES_AMT, HIST_REG_ACCT_AMT
  from (select V.LONG_DESCRIPTION  HISTORIC_LEDGER,
               R.DESCRIPTION       RECON_ITEM,
               L.GL_MONTH          GL_MONTH,
               C.DESCRIPTION       REG_COMPANY,
               L.CR_BALANCES_AMT   CR_BALANCES_AMT,
               L.HIST_REG_ACCT_AMT HIST_REG_ACCT_AMT
          from REG_COMPANY_SV            C,
               REG_HIST_RECON_ITEMS      R,
               REG_HISTORIC_VERSION      V,
               REG_HISTORIC_RECON_LEDGER L
         where L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.RECON_ID = R.RECON_ID);

-- REG_FORECAST_LEDGER_SV
create or replace view REG_FORECAST_LEDGER_SV
as
select REG_COMPANY,
       REG_ACCOUNT,
       FORECAST_LEDGER,
       GL_MONTH,
       FCST_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH
  from (select C.DESCRIPTION      REG_COMPANY,
               M.DESCRIPTION      REG_ACCOUNT,
               V.LONG_DESCRIPTION FORECAST_LEDGER,
               L.GL_MONTH         GL_MONTH,
               L.FCST_AMOUNT      FCST_AMOUNT,
               L.ANNUALIZED_AMT   ANNUALIZED_AMT,
               L.ADJ_AMOUNT       ADJ_AMOUNT,
               L.ADJ_MONTH        ADJ_MONTH
          from REG_COMPANY_SV C, REG_ACCT_MASTER M, REG_FORECAST_VERSION V, REG_FORECAST_LEDGER L
         where L.FORECAST_VERSION_ID = V.FORECAST_VERSION_ID
           and L.REG_COMPANY_ID = C.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID);

-- REG_COMMON_EQUITY_RETURN_VIEW
create or replace view REG_COMMON_EQUITY_RETURN_VIEW
as
select CASE_NAME, COMPANY, JURISDICTION, MONTH_YEAR, RETURN_ON_COMMON, OVERALL_RETURN
  from (select 'Financial Monitoring' CASE_NAME,
               RC.DESCRIPTION COMPANY,
               J.DESCRIPTION JURISDICTION,
               TO_CHAR(S.MONTH_YEAR, 'MM / YYYY') MONTH_YEAR,
               S.RETURN_ON_COMMON RETURN_ON_COMMON,
               S.OVERALL_RETURN OVERALL_RETURN
          from REG_COMPANY RC, REG_JURISDICTION J, REG_FINANCIAL_MONITOR_SUMMARY S
         where S.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and S.REG_JURISDICTION_ID = J.REG_JURISDICTION_ID
        union
        select C.CASE_NAME CASE_NAME,
               RC.DESCRIPTION COMPANY,
               J.DESCRIPTION JURISDICTION,
               TO_CHAR(S.MONTH_YEAR, 'MM / YYYY') MONTH_YEAR,
               R.COMMON_EQUITY_RETURN RETURN_ON_COMMON,
               R.OVERALL_RETURN OVERALL_RETURN
          from REG_CASE                      C,
               REG_CASE_RESULTS              R,
               REG_COMPANY                   RC,
               REG_JURISDICTION              J,
               REG_FINANCIAL_MONITOR_SUMMARY S
         where S.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and S.REG_JURISDICTION_ID = J.REG_JURISDICTION_ID
           and C.REG_CASE_TYPE = 1
           and C.REG_CASE_ID = R.REG_CASE_ID
           and R.RESULT_TYPE = 1 /* Jur Result Type */
           and S.REG_JURISDICTION_ID = C.REG_JURISDICTION_ID
           and TO_NUMBER(TO_CHAR(S.MONTH_YEAR, 'YYYYMM')) = R.MONTH_YEAR);

-- REG_CASE_SV
create or replace view REG_CASE_SV
as
select R_COMP_SV.REG_COMPANY_ID,
       R_CASE.REG_CASE_ID,
       R_CASE.CASE_NAME,
       R_CASE.LONG_DESCRIPTION,
       R_CASE.REG_JURISDICTION_ID,
       R_CASE.REG_CASE_TYPE,
       R_CASE.REG_STATUS_ID,
       R_CASE.LAST_UPDATE,
       R_CASE.CASE_MANAGER_USER_ID,
       R_CASE.PARENT_CASE_ID,
       R_CASE.NOTES,
       R_CASE.JURISDICTIONAL_COS_YN,
       R_CASE.CUSTOMER_COS_YN,
       R_CASE.PERIOD_START,
       R_CASE.PERIOD_END,
       R_CASE.REG_JUR_TEMPLATE_ID,
       R_CASE.COST_OF_CAPITAL_LEVEL
  from REG_COMPANY_SV R_COMP_SV, REG_CASE R_CASE, REG_JURISDICTION R_JURISDICTION
 where R_COMP_SV.REG_COMPANY_ID = R_JURISDICTION.REG_COMPANY_ID
   and R_CASE.REG_JURISDICTION_ID = R_JURISDICTION.REG_JURISDICTION_ID;

-- REG_CASE_REV_REQ_COMPARE_V
create or replace view REG_CASE_REV_REQ_COMPARE_V
as
select C.REG_CHART_GROUP_ID CHART_GROUP_ID,
       C.TEST_YEAR TEST_YEAR,
       C.REG_CASE_ID REG_CASE_ID,
       L.DESCRIPTION SERIES_LABEL,
       RC.CASE_NAME CASE_NAME,
       NVL(RCR.RATE_BASE, 0) RATE_BASE,
       0 ALLOWED_RATE,
       NVL(RCR.NOI_ALLOWED, 0) RETURN_ON_RATE_BASE,
       NVL(RCR.TAXES, 0) TAXES,
       NVL(RCR.DEPRECIATION, 0) DEPR_AND_AMORT,
       NVL(RCR.OPERATING_EXPENSE, 0) O_AND_M,
       0 FUEL_PURCH_POWER,
       0 OTHER_EXPENSES,
       NVL(RCR.OVERALL_RETURN, 0) OVERALL_RETURN
  from REG_CHART_GROUP          G,
       REG_CHART_GROUP_LABELS   L,
       REG_CHART_GROUP_CASES    C,
       REG_CASE                 RC,
       REG_CASE_REV_REQ_RESULTS RCR
 where G.REG_CHART_GROUP_ID = L.REG_CHART_GROUP_ID
   and L.REG_CHART_GROUP_ID = C.REG_CHART_GROUP_ID
   and L.SERIES_LABEL_ID = C.SERIES_LABEL_ID
   and C.REG_CASE_ID = RC.REG_CASE_ID
   and RCR.REG_CASE_ID = RC.REG_CASE_ID
   and RCR.CASE_YEAR_END = C.TEST_YEAR;

-- REG_CASE_NET_OPER_INCOME_WF_V
create or replace view REG_CASE_NET_OPER_INCOME_WF_V
as
select X.CASE_NAME, X.LABEL, X.AMOUNT, X.SORT_ORDER
  from (select RC.CASE_NAME CASE_NAME,
               'System Per Books' LABEL,
               -1 * sum(DECODE(A.REG_ACCT_TYPE_ID, 5, L.TOTAL_CO_INCLUDED_AMOUNT, 0)) -
               sum(DECODE(A.REG_ACCT_TYPE_ID, 3, L.TOTAL_CO_INCLUDED_AMOUNT)) AMOUNT,
               1 SORT_ORDER
          from REG_CASE RC, REG_CASE_SUMMARY_LEDGER L, REG_CASE_ACCT A
         where RC.REG_CASE_ID = L.REG_CASE_ID
           and L.REG_CASE_ID = A.REG_CASE_ID
           and L.REG_ACCT_ID = A.REG_ACCT_ID
           and L.REG_CASE_ID = 19
           and L.CASE_YEAR = 201112
           and A.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME
        union
        select RC.CASE_NAME,
               A.DESCRIPTION,
               -1 * sum(DECODE(T.REG_ACCT_TYPE_ID, 5, D.ADJUST_AMOUNT, 0)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 3, D.ADJUST_AMOUNT)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 11, DECODE(T.SUB_ACCT_TYPE_ID, 2, D.ADJUST_AMOUNT, 0))),
               2
          from REG_CASE RC, REG_CASE_ADJUSTMENT A, REG_CASE_ADJUST_DETAIL D, REG_CASE_ACCT T
         where RC.REG_CASE_ID = A.REG_CASE_ID
           and A.REG_CASE_ID = D.REG_CASE_ID
           and A.ADJUSTMENT_ID = D.ADJUSTMENT_ID
           and D.REG_CASE_ID = T.REG_CASE_ID
           and D.REG_ACCT_ID = T.REG_ACCT_ID
           and T.REG_ACCT_TYPE_ID in (3, 5, 11)
           and D.REG_CASE_ID = 19
           and D.TEST_YR_ENDED = 201112
         group by RC.CASE_NAME, A.DESCRIPTION) X
union
select X.CASE_NAME, 'System Adjusted', sum(X.AMOUNT), 3
  from (select RC.CASE_NAME CASE_NAME,
               'System Per Books' LABEL,
               -1 * sum(DECODE(A.REG_ACCT_TYPE_ID, 5, L.TOTAL_CO_INCLUDED_AMOUNT, 0)) -
               sum(DECODE(A.REG_ACCT_TYPE_ID, 3, L.TOTAL_CO_INCLUDED_AMOUNT)) AMOUNT,
               1 SORT_ORDER
          from REG_CASE RC, REG_CASE_SUMMARY_LEDGER L, REG_CASE_ACCT A
         where RC.REG_CASE_ID = L.REG_CASE_ID
           and L.REG_CASE_ID = A.REG_CASE_ID
           and L.REG_ACCT_ID = A.REG_ACCT_ID
           and L.REG_CASE_ID = 19
           and L.CASE_YEAR = 201112
           and A.REG_ACCT_TYPE_ID in (3, 5, 11)
         group by RC.CASE_NAME
        union
        select RC.CASE_NAME,
               A.DESCRIPTION,
               -1 * sum(DECODE(T.REG_ACCT_TYPE_ID, 5, D.ADJUST_AMOUNT, 0)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 3, D.ADJUST_AMOUNT)) -
               sum(DECODE(T.REG_ACCT_TYPE_ID, 11, DECODE(T.SUB_ACCT_TYPE_ID, 2, D.ADJUST_AMOUNT, 0))),
               2
          from REG_CASE RC, REG_CASE_ADJUSTMENT A, REG_CASE_ADJUST_DETAIL D, REG_CASE_ACCT T
         where RC.REG_CASE_ID = A.REG_CASE_ID
           and A.REG_CASE_ID = D.REG_CASE_ID
           and A.ADJUSTMENT_ID = D.ADJUSTMENT_ID
           and D.REG_CASE_ID = T.REG_CASE_ID
           and D.REG_ACCT_ID = T.REG_ACCT_ID
           and T.REG_ACCT_TYPE_ID in (3, 5, 11)
           and D.REG_CASE_ID = 19
           and D.TEST_YR_ENDED = 201112
         group by RC.CASE_NAME, A.DESCRIPTION) X
 group by X.CASE_NAME
 order by SORT_ORDER;

-- REG_ANNUALIZED_ACCT_TREND_VIEW
create or replace view REG_ANNUALIZED_ACCT_TREND_VIEW
as
select COMPANY,
       ACCT_TYPE,
       SUB_ACCT_TYPE,
       REG_ACCT,
       GL_MONTH,
       MONTH_AMT,
       ANNUALIZED_AMT,
       HIST_OR_FCST,
       HISTORIC_VERSION,
       FORECAST_VERSION,
       ACCT_TYPE_SORT,
       SUB_ACCT_TYPE_SORT
  from (select RC.DESCRIPTION COMPANY,
               T.DESCRIPTION ACCT_TYPE,
               S.DESCRIPTION SUB_ACCT_TYPE,
               M.DESCRIPTION REG_ACCT,
               L.GL_MONTH GL_MONTH,
               L.ACT_AMOUNT MONTH_AMT,
               L.ANNUALIZED_AMT ANNUALIZED_AMT,
               'H' HIST_OR_FCST,
               V.LONG_DESCRIPTION HISTORIC_VERSION,
               '' FORECAST_VERSION,
               T.SORT_ORDER ACCT_TYPE_SORT,
               S.SORT_ORDER SUB_ACCT_TYPE_SORT
          from REG_COMPANY          RC,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S,
               REG_ACCT_MASTER      M,
               REG_HISTORY_LEDGER   L,
               REG_HISTORIC_VERSION V
         where L.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID
           and M.REG_ANNUALIZATION_ID in (1, 8, 2)
           and L.HISTORIC_VERSION_ID = V.HISTORIC_VERSION_ID
           and L.HISTORIC_VERSION_ID <> 0
           and L.ANNUALIZED_AMT is not null
        union
        select RC.DESCRIPTION COMPANY,
               T.DESCRIPTION ACCT_TYPE,
               S.DESCRIPTION SUB_ACCT_TYPE,
               M.DESCRIPTION REG_ACCT,
               L.GL_MONTH GL_MONTH,
               L.FCST_AMOUNT MONTH_AMT,
               L.ANNUALIZED_AMT ANNUALIZED_AMT,
               'F' HIST_OR_FCST,
               '' HISTORIC_VERSION,
               V.DESCRIPTION FORECAST_VERSION,
               T.SORT_ORDER ACCT_TYPE_SORT,
               S.SORT_ORDER SUB_ACCT_TYPE_SORT
          from REG_COMPANY          RC,
               REG_ACCT_TYPE        T,
               REG_SUB_ACCT_TYPE    S,
               REG_ACCT_MASTER      M,
               REG_FORECAST_LEDGER  L,
               REG_FORECAST_VERSION V
         where L.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and L.REG_ACCT_ID = M.REG_ACCT_ID
           and M.REG_ACCT_TYPE_DEFAULT = T.REG_ACCT_TYPE_ID
           and M.REG_ACCT_TYPE_DEFAULT = S.REG_ACCT_TYPE_ID
           and M.SUB_ACCT_TYPE_ID = S.SUB_ACCT_TYPE_ID
           and M.REG_ANNUALIZATION_ID in (1, 8, 2)
           and L.FORECAST_VERSION_ID = V.FORECAST_VERSION_ID
           and L.FORECAST_VERSION_ID <> 0
           and L.ANNUALIZED_AMT is not null)
 order by COMPANY, ACCT_TYPE_SORT, SUB_ACCT_TYPE_SORT, REG_ACCT, GL_MONTH;

-- REG_ALL_LEDGERS_VIEW
create or replace view REG_ALL_LEDGERS_VIEW
(SRC, VERSION_ID, REG_COMPANY_ID, REG_ACCT_ID, GL_MONTH, AMOUNT,
 ANNUALIZED_AMT, ADJ_AMOUNT, ADJ_MONTH, USER_ID, TIME_STAMP)
as
select 'Historic' as SRC,
       HISTORIC_VERSION_ID,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       GL_MONTH,
       ACT_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       USER_ID,
       TIME_STAMP
  from REG_HISTORY_LEDGER
 where GL_MONTH <> 201100
union
select 'Forecast' as SRC,
       FORECAST_VERSION_ID,
       REG_COMPANY_ID,
       REG_ACCT_ID,
       GL_MONTH,
       FCST_AMOUNT,
       ANNUALIZED_AMT,
       ADJ_AMOUNT,
       ADJ_MONTH,
       USER_ID,
       TIME_STAMP
  from REG_FORECAST_LEDGER
 where GL_MONTH <> 201100;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (747, 0, 10, 4, 2, 0, 33713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033713_reg_04_views.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
