/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048677_taxrpr_report_fixes_wtask_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 08/01/2017 Eric Berger    Updating the datawindow values in
||                                      reporting tables to reflect the new
||                                      tax repairs standard.
||============================================================================
*/

UPDATE pp_reports
SET datawindow = 'dw_rpr_3000'
WHERE report_number = 'UOPWT - 3000';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13730, 0, 2018, 2, 0, 0, 48677, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048677_taxrpr_report_fixes_wtask_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;