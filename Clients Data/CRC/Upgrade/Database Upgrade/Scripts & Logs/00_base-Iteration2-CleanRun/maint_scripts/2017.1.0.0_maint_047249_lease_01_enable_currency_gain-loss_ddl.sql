/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047249_lease_01_enable_currency_gain-loss_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 08/30/2017 Josh Sandler     Enable the Currency Gain/Loss on the Month End workspace by default
||============================================================================
*/

ALTER TABLE ls_mec_config
  MODIFY process_5 DEFAULT 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3684, 0, 2017, 1, 0, 0, 47249, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047249_lease_01_enable_currency_gain-loss_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;