/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045469_lease_modify_type_ab_expense_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0.0 02/25/2016 Sarah Byers  	 Change type_a/b_expense to number(22,2) on ls_summary_forecast
|| 2015.2.2.0 04/14/2016 David Haupt	  Backporting to 2015.2.2
||============================================================================
*/

alter table ls_summary_forecast modify (
	type_a_expense number(22,2),
	type_b_expense number(22,2));

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3070, 0, 2015, 2, 2, 0, 045469, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045469_lease_modify_type_ab_expense_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;