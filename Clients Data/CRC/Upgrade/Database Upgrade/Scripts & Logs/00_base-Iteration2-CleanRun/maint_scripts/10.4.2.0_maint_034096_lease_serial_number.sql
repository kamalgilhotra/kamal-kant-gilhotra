/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034096_lease_serial_number.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/17/20143 Kyle Peterson
||============================================================================
*/

update PP_IMPORT_COLUMN
   set IS_REQUIRED = 0
 where COLUMN_NAME = 'serial_number'
   and IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: Leased A%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (869, 0, 10, 4, 2, 0, 34096, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034096_lease_serial_number.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;