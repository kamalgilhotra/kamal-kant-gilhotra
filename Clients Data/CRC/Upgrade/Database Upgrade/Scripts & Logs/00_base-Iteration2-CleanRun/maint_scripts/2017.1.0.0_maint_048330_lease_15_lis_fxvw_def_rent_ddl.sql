/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_048330_lease_15_lis_fxvw_def_rent_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.1.0.0  08/25/2017 build script   2017.1.0.0 Release
||============================================================================
*/

CREATE OR REPLACE VIEW v_ls_ilr_schedule_fx_vw (
  ilr_id,
  ilr_number,
  lease_id,
  lease_number,
  current_revision,
  revision,
  set_of_books_id,
  month,
  company_id,
  open_month,
  ls_cur_type,
  exchange_date,
  prev_exchange_date,
  contract_currency_id,
  display_currency_id,
  rate,
  calculated_rate,
  previous_calculated_rate,
  iso_code,
  currency_display_symbol,
  is_om,
  purchase_option_amt,
  termination_amt,
  net_present_value,
  capital_cost,
  beg_capital_cost,
  end_capital_cost,
  beg_obligation,
  end_obligation,
  beg_lt_obligation,
  end_lt_obligation,
  interest_accrual,
  principal_accrual,
  interest_paid,
  principal_paid,
  executory_accrual1,
  executory_accrual2,
  executory_accrual3,
  executory_accrual4,
  executory_accrual5,
  executory_accrual6,
  executory_accrual7,
  executory_accrual8,
  executory_accrual9,
  executory_accrual10,
  executory_paid1,
  executory_paid2,
  executory_paid3,
  executory_paid4,
  executory_paid5,
  executory_paid6,
  executory_paid7,
  executory_paid8,
  executory_paid9,
  executory_paid10,
  contingent_accrual1,
  contingent_accrual2,
  contingent_accrual3,
  contingent_accrual4,
  contingent_accrual5,
  contingent_accrual6,
  contingent_accrual7,
  contingent_accrual8,
  contingent_accrual9,
  contingent_accrual10,
  contingent_paid1,
  contingent_paid2,
  contingent_paid3,
  contingent_paid4,
  contingent_paid5,
  contingent_paid6,
  contingent_paid7,
  contingent_paid8,
  contingent_paid9,
  contingent_paid10,
  current_lease_cost,
  beg_deferred_rent,
  deferred_rent,
  end_deferred_rent,
  beg_st_deferred_rent,
  end_st_defererd_rent,
  gain_loss_fx,
  depr_expense,
  begin_reserve,
  end_reserve,
  depr_exp_alloc_adjust
) AS
WITH cur AS ( SELECT ls_currency_type_id AS ls_cur_type,
       currency_id,
       currency_display_symbol,
       iso_code,
         CASE
      ls_currency_type_id
      WHEN
        1
      THEN
        1
      ELSE
        NULL
    END
  AS contract_approval_rate
FROM currency
  CROSS JOIN ls_lease_currency_type
),open_month AS ( SELECT company_id,
       MIN(gl_posting_mo_yr) open_month
FROM ls_process_control WHERE open_next IS NULL GROUP BY
  company_id
),calc_rate AS ( SELECT a.company_id,
       a.contract_currency_id,
       a.company_currency_id,
       a.accounting_month,
       a.exchange_date,
       a.rate,
       b.rate prev_rate
FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = add_months(b.accounting_month,1)
),cr_now AS ( SELECT currency_from,
       currency_to,
       rate
FROM ( SELECT currency_from,
         currency_to,
         rate,
         ROW_NUMBER() OVER(PARTITION BY
      currency_from,
      currency_to
      ORDER BY
        exchange_date DESC
    ) AS rn
  FROM currency_rate_default_dense WHERE trunc(exchange_date,'MONTH') <= trunc(SYSDATE,'MONTH')
    AND exchange_rate_type_id = 1
  )
WHERE rn = 1 ) SELECT lis.ilr_id ilr_id,
       lis.ilr_number,
       lease.lease_id,
       lease.lease_number,
       lis.current_revision,
       lis.revision revision,
       lis.set_of_books_id set_of_books_id,
       lis.month month,
       open_month.company_id,
       open_month.open_month,
       cur.ls_cur_type AS ls_cur_type,
       cr.exchange_date,
       calc_rate.exchange_date prev_exchange_date,
       lease.contract_currency_id,
       cur.currency_id display_currency_id,
       cr.rate,
       calc_rate.rate calculated_rate,
       calc_rate.prev_rate previous_calculated_rate,
       cur.iso_code,
       cur.currency_display_symbol,
       lis.is_om,
       lis.purchase_option_amt * nvl(
    calc_rate.rate,
    cr.rate
  ) purchase_option_amt,
       lis.termination_amt * nvl(
    calc_rate.rate,
    cr.rate
  ) termination_amt,
       lis.net_present_value * nvl(
    calc_rate.rate,
    cr.rate
  ) net_present_value,
       lis.capital_cost * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) capital_cost,
       lis.beg_capital_cost * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) beg_capital_cost,
       lis.end_capital_cost * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) end_capital_cost,
       lis.beg_obligation * nvl(
    calc_rate.rate,
    cr.rate
  ) beg_obligation,
       lis.end_obligation * nvl(
    calc_rate.rate,
    cr.rate
  ) end_obligation,
       lis.beg_lt_obligation * nvl(
    calc_rate.rate,
    cr.rate
  ) beg_lt_obligation,
       lis.end_lt_obligation * nvl(
    calc_rate.rate,
    cr.rate
  ) end_lt_obligation,
       lis.interest_accrual * nvl(
    calc_rate.rate,
    cr.rate
  ) interest_accrual,
       lis.principal_accrual * nvl(
    calc_rate.rate,
    cr.rate
  ) principal_accrual,
       lis.interest_paid * nvl(
    calc_rate.rate,
    cr.rate
  ) interest_paid,
       lis.principal_paid * nvl(
    calc_rate.rate,
    cr.rate
  ) principal_paid,
       lis.executory_accrual1 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual1,
       lis.executory_accrual2 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual2,
       lis.executory_accrual3 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual3,
       lis.executory_accrual4 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual4,
       lis.executory_accrual5 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual5,
       lis.executory_accrual6 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual6,
       lis.executory_accrual7 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual7,
       lis.executory_accrual8 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual8,
       lis.executory_accrual9 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual9,
       lis.executory_accrual10 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_accrual10,
       lis.executory_paid1 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid1,
       lis.executory_paid2 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid2,
       lis.executory_paid3 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid3,
       lis.executory_paid4 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid4,
       lis.executory_paid5 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid5,
       lis.executory_paid6 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid6,
       lis.executory_paid7 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid7,
       lis.executory_paid8 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid8,
       lis.executory_paid9 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid9,
       lis.executory_paid10 * nvl(
    calc_rate.rate,
    cr.rate
  ) executory_paid10,
       lis.contingent_accrual1 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual1,
       lis.contingent_accrual2 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual2,
       lis.contingent_accrual3 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual3,
       lis.contingent_accrual4 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual4,
       lis.contingent_accrual5 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual5,
       lis.contingent_accrual6 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual6,
       lis.contingent_accrual7 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual7,
       lis.contingent_accrual8 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual8,
       lis.contingent_accrual9 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual9,
       lis.contingent_accrual10 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_accrual10,
       lis.contingent_paid1 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid1,
       lis.contingent_paid2 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid2,
       lis.contingent_paid3 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid3,
       lis.contingent_paid4 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid4,
       lis.contingent_paid5 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid5,
       lis.contingent_paid6 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid6,
       lis.contingent_paid7 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid7,
       lis.contingent_paid8 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid8,
       lis.contingent_paid9 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid9,
       lis.contingent_paid10 * nvl(
    calc_rate.rate,
    cr.rate
  ) contingent_paid10,
       lis.current_lease_cost * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) current_lease_cost,
       lis.beg_deferred_rent * nvl(
    calc_rate.rate,
    cr.rate
  ) beg_deferred_rent,
         lis.deferred_rent * nvl(
    calc_rate.rate,
    cr.rate
  ) deferred_rent,
         lis.end_deferred_rent * nvl(
    calc_rate.rate,
    cr.rate
  ) end_deferred_rent,
         lis.beg_st_deferred_rent * nvl(
    calc_rate.rate,
    cr.rate
  ) beg_st_deferred_rent,
         lis.end_st_deferred_rent * nvl(
    calc_rate.rate,
    cr.rate
  ) end_st_deferred_rent,
       lis.beg_obligation * ( nvl(calc_rate.rate,0) - nvl(calc_rate.prev_rate,0) ) gain_loss_fx,
       lis.depr_expense * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) depr_expense,
       lis.begin_reserve * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) begin_reserve,
       lis.end_reserve * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) end_reserve,
       lis.depr_exp_alloc_adjust * nvl(
    nvl(
      cur.contract_approval_rate,
      lis.in_service_exchange_rate
    ),
    cr_now.rate
  ) depr_exp_alloc_adjust
FROM v_multicurrency_lis_inner lis
  INNER JOIN ls_lease lease ON lis.lease_id = lease.lease_id
  INNER JOIN currency_schema cs ON lis.company_id = cs.company_id
  INNER JOIN cur ON cur.currency_id =
    CASE
      cur.ls_cur_type
      WHEN
        1
      THEN
        lease.contract_currency_id
      WHEN
        2
      THEN
        cs.currency_id
      ELSE
        NULL
    END
  INNER JOIN open_month ON lis.company_id = open_month.company_id
  INNER JOIN currency_rate_default_dense cr ON cur.currency_id = cr.currency_to
  AND lease.contract_currency_id = cr.currency_from
  AND trunc(cr.exchange_date,'MONTH') = trunc(lis.month,'MONTH')
  INNER JOIN cr_now ON cur.currency_id = cr_now.currency_to
  AND lease.contract_currency_id = cr_now.currency_from
  LEFT OUTER JOIN calc_rate ON lease.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND lis.company_id = calc_rate.company_id
  AND lis.month = calc_rate.accounting_month
WHERE cs.currency_type_id = 1
  AND cr.exchange_rate_type_id = 1
/

GRANT DELETE,INSERT,SELECT,UPDATE ON v_ls_ilr_schedule_fx_vw TO pwrplant_role_dev;
GRANT SELECT ON v_ls_ilr_schedule_fx_vw TO pwrplant_role_rdonly;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4031, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_15_lis_fxvw_def_rent_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;