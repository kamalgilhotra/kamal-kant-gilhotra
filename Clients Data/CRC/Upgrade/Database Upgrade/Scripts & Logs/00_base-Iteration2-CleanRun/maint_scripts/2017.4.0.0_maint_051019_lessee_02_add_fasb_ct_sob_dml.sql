/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051019_lessee_02_add_fasb_ct_sob_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 04/20/2018 Josh Sandler     Copy book summary to new location
||============================================================================
*/

UPDATE ls_fasb_cap_type_sob_map fctsb
SET fctsb.book_summary_id = (
  SELECT lct.book_summary_id
  FROM ls_lease_cap_type lct
  WHERE lct.ls_lease_cap_type_id = fctsb.lease_cap_type_id
)
WHERE EXISTS (
  SELECT 1
  FROM ls_lease_cap_type lct
  WHERE lct.ls_lease_cap_type_id = fctsb.lease_cap_type_id
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5023, 0, 2017, 4, 0, 0, 51019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051019_lessee_02_add_fasb_ct_sob_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 