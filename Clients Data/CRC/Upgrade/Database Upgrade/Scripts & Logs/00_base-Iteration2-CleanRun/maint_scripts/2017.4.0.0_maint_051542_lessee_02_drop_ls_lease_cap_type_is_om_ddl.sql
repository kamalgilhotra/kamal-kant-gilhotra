/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051542_lessee_02_drop_ls_lease_cap_type_is_om_ddl.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/15/2018 David Conway     Off Balance Sheet Expense by Set of Books
||============================================================================
*/
alter table ls_lease_cap_type drop column is_om;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7463, 0, 2017, 4, 0, 0, 51542, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051542_lessee_02_drop_ls_lease_cap_type_is_om_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;