/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051018_lessee_01_update_descriptions_disc_rpts_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 5/2/2018  Alex Healey     Update short descriptions on lessee 2002 and 2003 to be consistent
||============================================================================
*/

update pp_reports
set description = 'Weighted Average Discount Rate'
where report_number = 'Lessee - 2003';


update pp_reports
set description = 'Weighted Average Lease Term'
where report_number = 'Lessee - 2002';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4982, 0, 2017, 3, 0, 0, 51018, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051018_lessee_01_update_descriptions_disc_rpts_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;