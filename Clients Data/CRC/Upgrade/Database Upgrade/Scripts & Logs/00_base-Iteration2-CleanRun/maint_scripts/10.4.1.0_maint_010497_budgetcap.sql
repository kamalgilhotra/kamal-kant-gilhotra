/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010497_budgetcap.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   08/30/2013 Chris Mardis
||============================================================================
*/

alter table BV_PROJ_RANKING add SHOW_ACTUALS number(22);

update BV_PROJ_RANKING set SHOW_ACTUALS = 1 where SHOW_ACTUALS is null;

comment on column BV_PROJ_RANKING.SHOW_ACTUALS is 'Yes/No (0/1) indiating whether the ranking scenario should display a column with actuals-to-date.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (554, 0, 10, 4, 1, 0, 10497, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010497_budgetcap.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
