/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042299_cpr_cwip_02_populate_PP_MONTH_END_OPTIONS_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/03/2015 Charlie Shilling maint-42299
||============================================================================
*/

-----------------------------------------
-- WORK ORDER NEW MONTH
-----------------------------------------
INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
	process_id,
	1 AS option_id,
	'Continue if Overheads and AFUDC have not been approved?',
	'If ''Yes'', a new month can be opened regardless of the Overheads and AFUDC approval status. If ''No'', the Overheads and AFUDC results must be approved for the latest month for the company prior to opening a new month, unless a user manually overrides this option via a prompt on the month end window.',
	'No',
	Sys_Context('USERENV','SESSION_USER'),
	SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_new_month.exe';

INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
	process_id,
	2 AS option_id,
	'Continue if Accruals have not been approved?',
	'If ''Yes'', a new month can be opened regardless of the Accrual approval status. If ''No'', the Accruals results must be approved for the latest month for the company prior to opening a new month, unless a user manually overrides this option via a prompt on the month end window. Only applies if the ''Work Order Accrual Calculation'' is set to ''Yes''.',
	'No',
	Sys_Context('USERENV','SESSION_USER'),
	SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_new_month.exe';

INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
	process_id,
	3 AS option_id,
	'Continue if Automatic Retirements have not completed?',
	'If ''Yes'', a new month can be opened regardless of the Automatic Retirements status. If ''No'', the Automatic Retirements process must complete successfully for the latest month for the company prior to opening a new month, unless a user manually overrides this option via a prompt on the month end window.',
	'No',
	Sys_Context('USERENV','SESSION_USER'),
	SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_new_month.exe';

INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
	process_id,
	4 AS option_id,
	'Continue if Auto Non-Unitization has not completed?',
	'If ''Yes'', a new month can be opened regardless of the Auto Non-Unitization status. If ''No'', the Auto Non-Unitization process must complete successfully for the latest month for the company prior to opening a new month, unless a user manually overrides this option via a prompt on the month end window.',
	'No',
	Sys_Context('USERENV','SESSION_USER'),
	SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_new_month.exe';

INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
	process_id,
	5 AS option_id,
	'Continue if Auto Unitization has not completed?',
	'If ''Yes'', a new month can be opened regardless of the Auto Unitization status. If ''No'', the Auto Unitization process must complete successfully for the latest month for the company prior to opening a new month, unless a user manually overrides this option via a prompt on the month end window.',
	'No',
	Sys_Context('USERENV','SESSION_USER'),
	SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_new_month.exe';

-----------------------------------------
-- CPR CLOSE POWERPLANT
-----------------------------------------
INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
	process_id,
	1 AS option_id,
	'Continue if GL Reconciliation has not been completed?',
	'If ''Yes'', the CPR Close PowerPlant process will continue even if GL Reconciliation has not been completed. If ''No'', GL Reconciliation must complete successfully prior to the month being closed, unless a user manually overrides this option via a prompt on the month end window.',
	'No',
	Sys_Context('USERENV','SESSION_USER'),
	SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_close_powerplant.exe';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2267, 0, 2015, 1, 0, 0, 042299, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042299_cpr_cwip_02_populate_PP_MONTH_END_OPTIONS_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;