/*
||=============================================================================================
|| Application: PowerPlan
|| File Name:   maint_051741_lessee_01_put_table_comments_back_on_ilr_and_asset_schedule_ddl.sql
||=============================================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- -----------------------------------------------------
|| 2017.4.0.0 06/28/2018 Sisouphanh       Put table comments back on ls_ilr_schedule and ls_asset_schedule
||=============================================================================================
*/

COMMENT ON TABLE ls_ilr_schedule IS '(C)  [06] The ILR Schedule table records the ILR''s payment schedule.  The ILR''s schedule is allocated to its leased assets as part of the monthly lease expense calc process.';

COMMENT ON COLUMN ls_ilr_schedule.ilr_id IS 'System-assigned identifier of a particular ILR.';
COMMENT ON COLUMN ls_ilr_schedule.revision IS 'The revision.';
COMMENT ON COLUMN ls_ilr_schedule.set_of_books_id IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN ls_ilr_schedule.month IS 'The month being processed.';
COMMENT ON COLUMN ls_ilr_schedule.residual_amount IS 'The guaranteed residual amount.';
COMMENT ON COLUMN ls_ilr_schedule.term_penalty IS 'The termination penalty amount.';
COMMENT ON COLUMN ls_ilr_schedule.bpo_price IS 'The bargain purchase amount.';
COMMENT ON COLUMN ls_ilr_schedule.beg_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_ilr_schedule.end_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_ilr_schedule.beg_obligation IS 'The beginning obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_obligation IS 'The ending obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_lt_obligation IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_lt_obligation IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN ls_ilr_schedule.interest_accrual IS 'Records the amount of interest accrued.  Accruals occur every month regardless of payment schedule.';
COMMENT ON COLUMN ls_ilr_schedule.principal_accrual IS 'Records the amount of principal accrued.  Accruals occur every month regardless of payment schedule.';
COMMENT ON COLUMN ls_ilr_schedule.interest_paid IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN ls_ilr_schedule.principal_paid IS 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_accrual10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN ls_ilr_schedule.executory_paid10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_accrual10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN ls_ilr_schedule.contingent_paid10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN ls_ilr_schedule.user_id IS 'Standard System-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_ilr_schedule.time_stamp IS 'Standard System-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_ilr_schedule.is_om IS '1 means O and M.  0 means Capital.';
COMMENT ON COLUMN ls_ilr_schedule.current_lease_cost IS 'The total amount funded on the ILR in the given month';
COMMENT ON COLUMN ls_ilr_schedule.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_liability IS 'The beginning liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_liability IS 'The ending liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_lt_liability IS 'The beginning long term liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_lt_liability IS 'The ending long term liability for the period.';
COMMENT ON COLUMN ls_ilr_schedule.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_ilr_schedule.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_ilr_schedule.initial_direct_cost IS 'Holds Initial Direct Cost Amount for ILR Schedule.';
COMMENT ON COLUMN ls_ilr_schedule.incentive_amount IS 'Holds Incentive Amount for ILR Schedule.';
COMMENT ON COLUMN ls_ilr_schedule.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';
COMMENT ON COLUMN ls_ilr_schedule.prepay_amortization IS 'The prepayment amortization amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.prepaid_rent IS 'The prepaid rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_prepaid_rent IS 'The ending prepaid rent for the period.';
COMMENT ON COLUMN ls_ilr_schedule.idc_math_amount IS 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_ilr_schedule.incentive_math_amount IS 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_ilr_schedule.beg_net_rou_asset IS 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_ilr_schedule.end_net_rou_asset IS 'End Capital Cost less End Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_ilr_schedule.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_ilr_schedule.partial_term_gain_loss IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_ilr_schedule.executory_adjust IS 'Summated value of all Executory Adjusted values, by ILR';
COMMENT ON COLUMN ls_ilr_schedule.contingent_adjust IS 'Summated value of all Contingent Adjusted values, by ILR';
COMMENT ON COLUMN ls_ilr_schedule.additional_rou_asset IS 'Calculation of ROU Asset based on Quantity Retirement Method';
COMMENT ON COLUMN ls_ilr_schedule.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN ls_ilr_schedule.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';

comment ON TABLE ls_asset_schedule IS '(C)  [06]
The LS Asset Schedule table is holds the interest, principal, executory, and contingent amounts accrued and paid by period by set of books for a leased asset.';

COMMENT ON COLUMN ls_asset_schedule.ls_asset_id IS 'System-assigned identifier of a particular leased asset. The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_asset_schedule.revision IS 'The revision.';
COMMENT ON COLUMN ls_asset_schedule.set_of_books_id IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN ls_asset_schedule.month IS 'The month being processed.';
COMMENT ON COLUMN ls_asset_schedule.residual_amount IS 'The guaranteed residual amount.';
COMMENT ON COLUMN ls_asset_schedule.term_penalty IS 'The termination penalty amount.';
COMMENT ON COLUMN ls_asset_schedule.bpo_price IS 'The bargain purchase amount.';
COMMENT ON COLUMN ls_asset_schedule.beg_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_asset_schedule.end_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN ls_asset_schedule.beg_obligation IS 'The beginning obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_obligation IS 'The ending obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_lt_obligation IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_lt_obligation IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN ls_asset_schedule.interest_accrual IS 'The amount of interest accrued in this period .';
COMMENT ON COLUMN ls_asset_schedule.principal_accrual IS 'The amount of principal accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.interest_paid IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN ls_asset_schedule.principal_paid IS 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_accrual10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN ls_asset_schedule.executory_paid10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_accrual10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN ls_asset_schedule.contingent_paid10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN ls_asset_schedule.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_asset_schedule.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_asset_schedule.is_om IS '1 means O and M.  0 means Capital. 1 means O and M.  0 means Capital.';
COMMENT ON COLUMN ls_asset_schedule.current_lease_cost IS 'The total amount funded on the leased asset in the given month';
COMMENT ON COLUMN ls_asset_schedule.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_liability IS 'The beginning liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_liability IS 'The ending liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.beg_lt_liability IS 'The beginning long term liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_lt_liability IS 'The ending long term liability for the period.';
COMMENT ON COLUMN ls_asset_schedule.principal_remeasurement IS 'The principal/obligation adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_asset_schedule.liability_remeasurement IS 'The liability adjustment in the first period of a remeasurement.';
COMMENT ON COLUMN ls_asset_schedule.initial_direct_cost IS 'Holds Initial Direct Cost Amount for Asset Schedule.';
COMMENT ON COLUMN ls_asset_schedule.incentive_amount IS 'Holds Incentive Amount for Asset Schedule.';
COMMENT ON COLUMN ls_asset_schedule.beg_prepaid_rent IS 'The beginning prepaid rent for the period.';
COMMENT ON COLUMN ls_asset_schedule.prepay_amortization IS 'The prepayment amortization amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.prepaid_rent IS 'The prepaid rent amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_prepaid_rent IS 'The ending prepaid rent for the period.';
COMMENT ON COLUMN ls_asset_schedule.idc_math_amount IS 'Column used to hold shuffeled IDC amounts into the beginning month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_asset_schedule.incentive_math_amount IS 'Column used to hold shuffeled Incentive amounts into payment month of the Asset Schedule. Only used within Schedule Build and Asset Post and is not displayed to user';
COMMENT ON COLUMN ls_asset_schedule.beg_net_rou_asset IS 'Begin Capital Cost less Begin Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_asset_schedule.end_net_rou_asset IS 'End Capital Cost less End Reserve for all Assets on ILR';
COMMENT ON COLUMN ls_asset_schedule.rou_asset_remeasurement IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_asset_schedule.partial_term_gain_loss IS 'Remeasurement Amount used for adjusting the net ROU Asset Amount';
COMMENT ON COLUMN ls_asset_schedule.executory_adjust IS 'Summated value of all Executory Adjusted values, by ILR';
COMMENT ON COLUMN ls_asset_schedule.contingent_adjust IS 'Summated value of all Contingent Adjusted values, by ILR';
COMMENT ON COLUMN ls_asset_schedule.additional_rou_asset IS 'Calculation of ROU Asset based on Quantity Retirement Method';
comment on column ls_asset_schedule.partial_month_percent is 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';
COMMENT ON COLUMN ls_asset_schedule.beg_arrears_accrual IS 'The beginning arrears accrual balance for the period.';
COMMENT ON COLUMN ls_asset_schedule.arrears_accrual IS 'The arrears accrual amount for the period.';
COMMENT ON COLUMN ls_asset_schedule.end_arrears_accrual IS 'The ending arrears accrual balance for the period.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7682, 0, 2017, 4, 0, 0, 51741, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051741_lessee_01_put_table_comments_back_on_ilr_and_asset_schedule_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;