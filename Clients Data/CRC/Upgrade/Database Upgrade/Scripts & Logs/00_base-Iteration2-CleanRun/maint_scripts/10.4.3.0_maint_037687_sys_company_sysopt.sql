/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037687_sys_company_sysopt.sql
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.3.0 04/15/2014 Alex P.
||=================================================================================
*/

create table PPBASE_SYSTEM_OPTIONS_COMPANY
(
 SYSTEM_OPTION_ID varchar2(254) not null,
 COMPANY_ID       number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 OPTION_VALUE     varchar2(254)
);

alter table PPBASE_SYSTEM_OPTIONS_COMPANY
   add constraint PK_PSOC_SYSOPT_COMPANY
       primary key (SYSTEM_OPTION_ID, COMPANY_ID)
       using index tablespace PWRPLANT_IDX;

alter table PPBASE_SYSTEM_OPTIONS_COMPANY
   add constraint FK_PSOC_SYSTEM_OPTION_ID
       foreign key (SYSTEM_OPTION_ID)
       references PPBASE_SYSTEM_OPTIONS;

alter table PPBASE_SYSTEM_OPTIONS_COMPANY
   add constraint FK_PSOC_COMANY_ID
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

comment on table PPBASE_SYSTEM_OPTIONS_COMPANY is '(S) [10] PPBASE SYSTEM OPTIONS COMPANY table stores syetm option value overrides by company that are different from the default system option value assigned to all companies.';
comment on column PPBASE_SYSTEM_OPTIONS_COMPANY.SYSTEM_OPTION_ID is 'Unique system option identifier.';
comment on column PPBASE_SYSTEM_OPTIONS_COMPANY.COMPANY_ID is 'Unique company identifier for which override value is used.';
comment on column PPBASE_SYSTEM_OPTIONS_COMPANY.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PPBASE_SYSTEM_OPTIONS_COMPANY.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column PPBASE_SYSTEM_OPTIONS_COMPANY.OPTION_VALUE is 'Override system option value';

alter table PPBASE_SYSTEM_OPTIONS add ALLOW_COMPANY_OVERRIDE number(2,0);
comment on column PPBASE_SYSTEM_OPTIONS.ALLOW_COMPANY_OVERRIDE is 'A binary flag indicating whether the system option can be configired at the company level (1) or same value has to be used for all companies (0)';

update PPBASE_SYSTEM_OPTIONS set ALLOW_COMPANY_OVERRIDE = 0;

alter table PPBASE_SYSTEM_OPTIONS modify ALLOW_COMPANY_OVERRIDE not null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1116, 0, 10, 4, 3, 0, 37687, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037687_sys_company_sysopt.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;