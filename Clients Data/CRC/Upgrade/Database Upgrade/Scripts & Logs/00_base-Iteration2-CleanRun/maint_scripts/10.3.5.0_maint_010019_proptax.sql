/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010019_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- -------------------------------------
|| 10.3.5.0    05/07/2012 Julia Breuer   Point Release
|| 10.4.1.2    12/11/2013 Julia Breuer   Script was removed
||============================================================================
*/

--
-- Create new type rollup to be used for generic filing reports.
--
insert into PWRPLANT.PT_TYPE_ROLLUP ( TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION ) values ( 10000, sysdate, user, 'Filing Type (Standard)', 'Filing Type used for Standard Filing Reports' );

--
-- Create the generic filing reports.
--
insert into PWRPLANT.PP_REPORTS ( REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW ) values ( 511000, sysdate, user, 'Standard Property List', 'Standard Property List for Filing.  Includes serial number, description, prop tax balance, and market value.  Uses the Filing Type (Standard) rollup.  Grouped by active status, prop tax company, state, county, parcel, rollup value, and vintage.', 'dw_ptr_filing_std_property_list', 'STD Property List', 10, 200, 12, 7, 1, 3, 0 );
insert into PWRPLANT.PP_REPORTS ( REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW ) values ( 511001, sysdate, user, 'Standard Asset Summary', 'Standard Asset Summary for Filing.  Includes vintage, prop tax balance, and market value.  Uses the Filing Type (Standard) rollup.  Grouped by active status, prop tax company, state, county, parcel, and rollup value.', 'dw_ptr_filing_std_asset_summary', 'STD Asset Summary', 10, 200, 12, 7, 1, 3, 0 );
insert into PWRPLANT.PP_REPORTS ( REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW ) values ( 511002, sysdate, user, 'Standard Asset Detail', 'Standard Asset Detail for Filing.  Includes serial number, description, vintage, filing type, prior prop tax balance, and current prop tax balance.  Uses the Filing Type (Standard) rollup.  Grouped by prop tax company, state, county, and parcel.', 'dw_ptr_filing_std_asset_detail', 'STD Asset Detail', 10, 200, 12, 7, 1, 3, 0 );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (188, 0, 10, 3, 5, 0, 10019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010019_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
