/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010455_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   06/07/2012 Julia Breuer   Point Release
||============================================================================
*/

alter table PT_COMPANY         add notes varchar2(4000);
alter table PROP_TAX_DISTRICT  add notes varchar2(4000);
alter table PROP_TAX_TYPE_CODE add notes varchar2(4000);
alter table PROP_TAX_LOCATION  add notes varchar2(4000);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('notes', 'pt_company', sysdate, 'PWRPLANT', null, 'e', null, 'Notes', 'Notes', 15, 'Notes',
    null, null, null, null, null, 0, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('notes', 'prop_tax_district', sysdate, 'PWRPLANT', null, 'e', null, 'Notes', 'Notes', 15,
    'Notes', null, null, null, null, null, 0, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('notes', 'prop_tax_type_code', sysdate, 'PWRPLANT', null, 'e', null, 'Notes', 'Notes', 8,
    'Notes', null, null, null, null, null, 0, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('notes', 'prop_tax_location', sysdate, 'PWRPLANT', null, 'e', null, 'Notes', 'Notes', 13,
    'Notes', null, null, null, null, null, 0, null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (147, 0, 10, 3, 5, 0, 10455, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010455_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
