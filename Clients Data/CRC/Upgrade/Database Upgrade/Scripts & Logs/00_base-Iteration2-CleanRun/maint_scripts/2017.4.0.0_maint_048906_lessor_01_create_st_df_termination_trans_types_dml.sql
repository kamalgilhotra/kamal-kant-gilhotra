/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048906_lessor_01_create_st_df_termination_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.4.0.0 06/19/2018 Jared Watkins     Create the new Transaction types we need for ST/DF Termination JEs
||============================================================================
*/

/* Add new trans type 4060 - Receivable Termination Debit */
insert into je_trans_type (trans_type, description)
values (4060, '4060 - Lessor Receivable Termination Debit');

/* Add new trans type 4061 - Receivable Termination Credit */
insert into je_trans_type (trans_type, description)
values (4061, '4061 - Lessor Receivable Termination Credit');

/* Add new trans type 4062 - Unguaranteed Residual Debit */
insert into je_trans_type (trans_type, description)
values (4062, '4062 - Lessor Unguaranteed Residual Debit');

/* Add new trans type 4063 - Unguaranteed Residual Credit */
insert into je_trans_type (trans_type, description)
values (4063, '4063 - Lessor Unguaranteed Residual Credit');

/* Add new trans type 4064 - Future Payments Debit */
insert into je_trans_type (trans_type, description)
values (4064, '4064 - Lessor Future Payments Debit');

/* Add new trans type 4065 - Future Payments Credit */
insert into je_trans_type (trans_type, description)
values (4065, '4065 - Lessor Future Payments Credit');

/* Add new trans type 4066 - Deferred Profit Debit */
insert into je_trans_type (trans_type, description)
values (4066, '4066 - Lessor Deferred Profit Debit');

/* Add new trans type 4067 - Deferred Profit Credit */
insert into je_trans_type (trans_type, description)
values (4067, '4067 - Lessor Deferred Profit Credit');

/* Relate the new trans types to the delivered JE Method */
insert into je_method_trans_type(je_method_id, trans_type)
select 1, trans_type from je_trans_type 
where trans_type between 4060 and 4067;

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6822, 0, 2017, 4, 0, 0, 48906, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_048906_lessor_01_create_st_df_termination_trans_types_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;