 /*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_042051_pcm_review_wksp_dml.sql
|| Description: Add authorizations workspace to PCM port
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1 	01/15/2014 Ryan Oliveria  FP/WO Review Workspace
||============================================================================
*/


update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_pcm_auth_wksp'
 where MODULE = 'pcm'
   and WORKSPACE_IDENTIFIER = 'fp_review';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2178, 0, 2015, 1, 0, 0, 042051, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042051_pcm_review_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;