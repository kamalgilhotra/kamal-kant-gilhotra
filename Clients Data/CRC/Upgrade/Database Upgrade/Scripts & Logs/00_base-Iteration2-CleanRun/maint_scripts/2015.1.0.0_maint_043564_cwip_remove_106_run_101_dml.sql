/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043564_cwip_remove_106_run_101_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 04/08/2015 Sarah Byers		Remove the 'AUTO106 - Run AUTO101' system control
||============================================================================
*/

delete from pp_system_control_company
 where control_name = 'AUTO106 - Run AUTO101';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2480, 0, 2015, 1, 0, 0, 043564, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043564_cwip_remove_106_run_101_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;