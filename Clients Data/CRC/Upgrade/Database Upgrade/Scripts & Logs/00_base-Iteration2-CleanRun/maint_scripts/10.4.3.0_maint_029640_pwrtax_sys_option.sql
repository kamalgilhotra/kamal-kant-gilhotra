/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_029640_pwrtax_sys_option.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| PP Version    Date           Revised By     Reason for Change
|| ----------    -------------  -------------  -------------------------------
|| 10.4.3.0      10/01/2014     Andrew Scott   maint-29640.  system option needed for the maint.
||============================================================================
*/

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values
   ('Deferred-Allocate Bonus Depr Tax Recovery to Overheads',
    'Whether or not to allocate bonus depreciation tax recovery to M/L and tax overheads.', 0, 'No', 1, 0);

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Deferred-Allocate Bonus Depr Tax Recovery to Overheads', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Deferred-Allocate Bonus Depr Tax Recovery to Overheads', 'No');

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Deferred-Allocate Bonus Depr Tax Recovery to Overheads', 'powertax');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1464, 0, 10, 4, 3, 0, 29640, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_029640_pwrtax_sys_option.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;