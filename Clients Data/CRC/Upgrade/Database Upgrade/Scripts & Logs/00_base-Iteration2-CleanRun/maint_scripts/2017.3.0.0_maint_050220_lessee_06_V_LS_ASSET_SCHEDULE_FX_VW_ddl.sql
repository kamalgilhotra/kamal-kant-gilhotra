/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050220_lessee_06_V_LS_ASSET_SCHEDULE_FX_VW_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.2.0.0 01/24/2018 Shane "C" Ward 	Lesseee Calculation Performance enhancements
||============================================================================
*/

CREATE OR replace VIEW v_ls_asset_schedule_fx_vw
AS
  WITH cur
       AS ( SELECT ls_currency_type_id AS ls_cur_type,
                   currency_id,
                   currency_display_symbol,
                   iso_code,
                   CASE ls_currency_type_id
                     WHEN 1 THEN 1
                     ELSE NULL
                   END                 AS contract_approval_rate
            FROM   CURRENCY
                   cross join LS_LEASE_CURRENCY_TYPE ),
       open_month
       AS ( SELECT company_id,
                   Min( gl_posting_mo_yr ) open_month
            FROM   LS_PROCESS_CONTROL
            WHERE  open_next IS NULL
            GROUP  BY company_id ),
       calc_rate
       AS ( SELECT a.company_id,
                   a.contract_currency_id,
                   a.company_currency_id,
                   a.accounting_month,
                   a.exchange_date,
                   a.rate,
                   b.rate prev_rate
            FROM   LS_LEASE_CALCULATED_DATE_RATES a
                   left outer join LS_LEASE_CALCULATED_DATE_RATES b
                                ON a.company_id = b.company_id AND
                                   a.contract_currency_id = b.contract_currency_id AND
                                   a.accounting_month = Add_months( b.accounting_month, 1 ) ),
       cr_now
       AS ( SELECT currency_from,
                   currency_to,
                   rate
            FROM   ( SELECT currency_from,
                            currency_to,
                            rate,
                            Row_number( )
                              over(
                                PARTITION BY currency_from, currency_to
                                ORDER BY exchange_date DESC ) AS rn
                     FROM   CURRENCY_RATE_DEFAULT_DENSE
                     WHERE  Trunc( exchange_date, 'MONTH' ) <= Trunc( SYSDATE, 'MONTH' ) AND
                            exchange_rate_type_id = 1 )
            WHERE  rn = 1 )
  SELECT las.ilr_id                                                                                                                                               ilr_id,
         las.ls_asset_id                                                                                                                                          ls_asset_id,
         las.revision                                                                                                                                             revision,
         las.set_of_books_id                                                                                                                                      set_of_books_id,
         las.month                                                                                                                                                month,
         OPEN_MONTH.company_id,
         OPEN_MONTH.open_month,
         CUR.ls_cur_type                                                                                                                                          AS ls_cur_type,
         cr.exchange_date,
         CALC_RATE.exchange_date                                                                                                                                  prev_exchange_date,
         las.contract_currency_id,
         CUR.currency_id,
         cr.rate,
         CALC_RATE.rate                                                                                                                                           calculated_rate,
         CALC_RATE.prev_rate                                                                                                                                      previous_calculated_rate,
         CUR.iso_code,
         CUR.currency_display_symbol,
         las.residual_amount * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     residual_amount,
         las.term_penalty * Nvl( CALC_RATE.rate, cr.rate )                                                                                                        term_penalty,
         las.bpo_price * Nvl( CALC_RATE.rate, cr.rate )                                                                                                           bpo_price,
         las.beg_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                               beg_capital_cost,
         las.end_capital_cost * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                               end_capital_cost,
         las.beg_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      beg_obligation,
         las.end_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      end_obligation,
         las.beg_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   beg_lt_obligation,
         las.end_lt_obligation * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   end_lt_obligation,
         las.beg_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       beg_liability,
         las.end_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       end_liability,
         las.beg_lt_liability * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    beg_lt_liability,
         las.end_lt_liability * Nvl( CALC_RATE.rate, cr.rate )     END_lt_liability,
         las.interest_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    interest_accrual,
         las.principal_accrual * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   principal_accrual,
         las.interest_paid * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       interest_paid,
         las.principal_paid * Nvl( CALC_RATE.rate, cr.rate )                                                                                                      principal_paid,
         las.executory_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual1,
         las.executory_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual2,
         las.executory_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual3,
         las.executory_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual4,
         las.executory_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual5,
         las.executory_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual6,
         las.executory_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual7,
         las.executory_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual8,
         las.executory_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                  executory_accrual9,
         las.executory_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 executory_accrual10,
         las.executory_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid1,
         las.executory_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid2,
         las.executory_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid3,
         las.executory_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid4,
         las.executory_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid5,
         las.executory_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid6,
         las.executory_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid7,
         las.executory_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid8,
         las.executory_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                     executory_paid9,
         las.executory_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    executory_paid10,
         las.contingent_accrual1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual1,
         las.contingent_accrual2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual2,
         las.contingent_accrual3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual3,
         las.contingent_accrual4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual4,
         las.contingent_accrual5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual5,
         las.contingent_accrual6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual6,
         las.contingent_accrual7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual7,
         las.contingent_accrual8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual8,
         las.contingent_accrual9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                 contingent_accrual9,
         las.contingent_accrual10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                contingent_accrual10,
         las.contingent_paid1 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid1,
         las.contingent_paid2 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid2,
         las.contingent_paid3 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid3,
         las.contingent_paid4 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid4,
         las.contingent_paid5 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid5,
         las.contingent_paid6 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid6,
         las.contingent_paid7 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid7,
         las.contingent_paid8 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid8,
         las.contingent_paid9 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                    contingent_paid9,
         las.contingent_paid10 * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   contingent_paid10,
         las.current_lease_cost * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                             current_lease_cost,
         las.beg_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   beg_deferred_rent,
         las.deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                       deferred_rent,
         las.end_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                   end_deferred_rent,
         las.beg_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                beg_st_deferred_rent,
         las.end_st_deferred_rent * Nvl( CALC_RATE.rate, cr.rate )                                                                                                end_st_deferred_rent,
         Decode( CALC_RATE.rate, NULL, 0,
                                 ( las.beg_obligation * ( CALC_RATE.rate - Coalesce( CALC_RATE.prev_rate, las.in_service_exchange_rate, CALC_RATE.rate, 0 ) ) ) ) gain_loss_fx,
         las.depr_expense * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                                   depr_expense,
         las.begin_reserve * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                                  begin_reserve,
         las.end_reserve * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                                    end_reserve,
         las.depr_exp_alloc_adjust * Nvl( Nvl( CUR.contract_approval_rate, las.in_service_exchange_rate ), CR_NOW.rate )                                          depr_exp_alloc_adjust,
         las.asset_description,
         las.leased_asset_number,
         las.fmv * Nvl( CALC_RATE.rate, cr.rate )                                                                                                                 fmv,
         las.is_om,
         las.approved_revision,
         las.lease_cap_type_id,
         las.ls_asset_status_id,
         las.retirement_date
  FROM   ( SELECT la.ilr_id,
                  las.ls_asset_id,
                  las.revision,
                  las.set_of_books_id,
                  las.month,
                  la.contract_currency_id,
                  las.residual_amount,
                  las.term_penalty,
                  las.bpo_price,
                  las.beg_capital_cost,
                  las.end_capital_cost,
                  las.beg_obligation,
                  las.end_obligation,
                  las.beg_lt_obligation,
                  las.end_lt_obligation,
                           las.beg_liability,
         las.end_liability ,
         las.beg_lt_liability ,
         las.end_lt_liability ,
                  las.interest_accrual,
                  las.principal_accrual,
                  las.interest_paid,
                  las.principal_paid,
                  las.executory_accrual1,
                  las.executory_accrual2,
                  las.executory_accrual3,
                  las.executory_accrual4,
                  las.executory_accrual5,
                  las.executory_accrual6,
                  las.executory_accrual7,
                  las.executory_accrual8,
                  las.executory_accrual9,
                  las.executory_accrual10,
                  las.executory_paid1,
                  las.executory_paid2,
                  las.executory_paid3,
                  las.executory_paid4,
                  las.executory_paid5,
                  las.executory_paid6,
                  las.executory_paid7,
                  las.executory_paid8,
                  las.executory_paid9,
                  las.executory_paid10,
                  las.contingent_accrual1,
                  las.contingent_accrual2,
                  las.contingent_accrual3,
                  las.contingent_accrual4,
                  las.contingent_accrual5,
                  las.contingent_accrual6,
                  las.contingent_accrual7,
                  las.contingent_accrual8,
                  las.contingent_accrual9,
                  las.contingent_accrual10,
                  las.contingent_paid1,
                  las.contingent_paid2,
                  las.contingent_paid3,
                  las.contingent_paid4,
                  las.contingent_paid5,
                  las.contingent_paid6,
                  las.contingent_paid7,
                  las.contingent_paid8,
                  las.contingent_paid9,
                  las.contingent_paid10,
                  las.current_lease_cost,
                  las.beg_deferred_rent,
                  las.deferred_rent,
                  las.end_deferred_rent,
                  las.beg_st_deferred_rent,
                  las.end_st_deferred_rent,
                  ldf.depr_expense,
                  ldf.begin_reserve,
                  ldf.end_reserve,
                  ldf.depr_exp_alloc_adjust,
                  la.company_id,
                  opt.in_service_exchange_rate,
                  la.description AS asset_description,
                  la.leased_asset_number,
                  la.fmv,
                  las.is_om,
                  la.approved_revision,
                  opt.lease_cap_type_id,
                  la.ls_asset_status_id,
                  la.retirement_date
           FROM   LS_ASSET_SCHEDULE las,
                  LS_ASSET la,
                  LS_ILR_OPTIONS opt,
                  LS_DEPR_FORECAST ldf
           WHERE  las.ls_asset_id = la.ls_asset_id AND
                  la.ilr_id = opt.ilr_id AND
                  las.revision = opt.revision AND
                  las.ls_asset_id = ldf.ls_asset_id (+) AND
                  las.revision = ldf.revision (+) AND
                  las.set_of_books_id = ldf.set_of_books_id (+) AND
                  las.month = ldf.month (+) ) las
         inner join CURRENCY_SCHEMA cs
                 ON las.company_id = cs.company_id
         inner join cur
                 ON CUR.currency_id = CASE CUR.ls_cur_type
                                        WHEN 1 THEN las.contract_currency_id
                                        WHEN 2 THEN cs.currency_id
                                        ELSE NULL
                                      END
         inner join open_month
                 ON las.company_id = OPEN_MONTH.company_id
         inner join CURRENCY_RATE_DEFAULT_DENSE cr
                 ON CUR.currency_id = cr.currency_to AND
                    las.contract_currency_id = cr.currency_from AND
                    Trunc( cr.exchange_date, 'MONTH' ) = Trunc( las.month, 'MONTH' )
         inner join cr_now
                 ON CUR.currency_id = CR_NOW.currency_to AND
                    las.contract_currency_id = CR_NOW.currency_from
         left outer join calc_rate
                      ON las.contract_currency_id = CALC_RATE.contract_currency_id AND
                         CUR.currency_id = CALC_RATE.company_currency_id AND
                         las.company_id = CALC_RATE.company_id AND
                         las.month = CALC_RATE.accounting_month
  WHERE  cs.currency_type_id = 1 AND
         cr.exchange_rate_type_id = 1; 

		 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4123, 0, 2017, 3, 0, 0, 50220, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050220_lessee_06_V_LS_ASSET_SCHEDULE_FX_VW_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;		 			 
