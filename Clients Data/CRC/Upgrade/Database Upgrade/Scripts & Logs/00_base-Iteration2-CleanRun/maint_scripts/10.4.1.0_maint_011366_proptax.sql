/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011366_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/15/2013 Julia Breuer   Point Release
||============================================================================
*/

update PTC_SYSTEM_OPTIONS
   set LONG_DESCRIPTION = 'The default selection for the display of rows - All Rows or Only Rows with Errors.  This specifies the initial selection when the workspace is opened.'
 where SYSTEM_OPTION_ID = 'Admin Center - Import - Default Row Display';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (326, 0, 10, 4, 1, 0, 11366, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011366_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
