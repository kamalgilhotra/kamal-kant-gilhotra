/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034961_reg_REG_HIST_CWIP_ACTIVITY_VIEW.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/11/2014 Shane "C" Ward
||============================================================================
*/

--CWIP Activity View

create or replace view REG_HIST_CWIP_ACTIVITY_VIEW(
  COMPANY_ID, COMPANY_DESCRIPTION, CHARGE_ID, CHARGE_TYPE_ID, CHARGE_DESCRIPTION, MAJOR_LOCATION_ID,
  ASSET_LOCATION_ID, BUS_SEGMENT_ID, BUSINESS_SEGMENT, EXPENDITURE_TYPE,
  CHARGE_MONTH_YEAR, ORIGINAL_AMOUNT, AMOUNT, WORK_ORDER_ID, WORK_ORDER_NUMBER,
  WORK_ORDER_DESCRIPTION, WORK_ORDER_TYPE_ID, COST_ELEMENT, FUNC_CLASS_ID,
  REG_ACCT_DESCRIPTION) as (
   select W.COMPANY_ID,
          CS.DESCRIPTION COMPANY_DESCRIPTION,
          C.CHARGE_ID,
          C.CHARGE_TYPE_ID,
          C.DESCRIPTION CHARGE_DESCRIPTION,
          W.MAJOR_LOCATION_ID,
          W.ASSET_LOCATION_ID,
          W.BUS_SEGMENT_ID,
          B.DESCRIPTION BUSINESS_SEGMENT,
          E.DESCRIPTION EXPENDITURE_TYPE,
          TO_CHAR(CHARGE_MO_YR, 'mm/yyyy'),
          C.ORIGINAL_AMOUNT,
          C.AMOUNT,
          C.WORK_ORDER_ID,
          W.WORK_ORDER_NUMBER,
          W.DESCRIPTION WORK_ORDER_DESCRIPTION,
          W.WORK_ORDER_TYPE_ID,
          CO.DESCRIPTION COST_ELEMENT,
          A.FUNC_CLASS_ID,
          F.FILTER_STRING REG_ACCT_DESCRIPTION
     from CWIP_CHARGE               C,
          COMPANY_SETUP             CS,
          EXPENDITURE_TYPE          E,
          WORK_ORDER_CONTROL        W,
          BUSINESS_SEGMENT          B,
          COST_ELEMENT              CO,
          GL_ACCOUNT                G,
          CR_DD_REQUIRED_FILTER     F,
          CR_DD_REQUIRED_FILTER     MN,
          WORK_ORDER_ACCOUNT        A,
          CHARGE_TYPE               CT,
          REG_CWIP_COMPONENT_VALUES V,
          CR_DD_REQUIRED_FILTER     COMP
    where C.WORK_ORDER_ID = W.WORK_ORDER_ID
      and C.EXPENDITURE_TYPE_ID = E.EXPENDITURE_TYPE_ID
      and C.WORK_ORDER_ID = A.WORK_ORDER_ID
      and C.BUS_SEGMENT_ID = B.BUS_SEGMENT_ID
      and C.COST_ELEMENT_ID = CO.COST_ELEMENT_ID
      and C.GL_ACCOUNT_ID = G.GL_ACCOUNT_ID
      and W.COMPANY_ID = CS.COMPANY_ID
      and F.COLUMN_NAME = 'REG_ACCT_DESCRIPTION'
      and MN.COLUMN_NAME = 'MONTH_NUMBER'
      and MN.FILTER_STRING = TO_CHAR(C.CHARGE_MO_YR, 'yyyymm')
      and C.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID
      and CT.BOOK_SUMMARY_ID = V.ID_VALUE
      and V.REG_FAMILY_ID = 3
      and V.REG_COMPONENT_ID = TO_NUMBER(COMP.FILTER_STRING)
      and COMP.COLUMN_NAME = 'REG_COMPONENT_ID');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1035, 0, 10, 4, 2, 0, 34961, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034961_reg_REG_HIST_CWIP_ACTIVITY_VIEW.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;