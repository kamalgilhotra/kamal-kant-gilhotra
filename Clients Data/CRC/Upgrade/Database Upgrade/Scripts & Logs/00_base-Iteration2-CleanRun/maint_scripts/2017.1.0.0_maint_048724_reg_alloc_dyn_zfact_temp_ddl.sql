 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048724_reg_alloc_dyn_zfact_temp_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 08/10/2017 Sarah Byers    New temp table for RMS dynamic allocations
 ||============================================================================
 */ 

CREATE GLOBAL TEMPORARY TABLE reg_case_alloc_dyn_zfact_temp (
reg_case_id NUMBER(22,0) NOT NULL,
case_year NUMBER(22,0) NOT NULL,
reg_alloc_category_id NUMBER(22,0) NOT NULL,
reg_allocator_id number(22,0) not null,
row_number number(22,0) not null,
description varchar2(75) not null,
basis_string varchar2(2000) not null) ON COMMIT PRESERVE rows;

alter table reg_case_alloc_dyn_zfact_temp add (
constraint pk_reg_case_adyn_zfact_temp primary key (reg_case_id, case_year, reg_alloc_category_id, reg_allocator_id, row_number));

comment on table reg_case_alloc_dyn_zfact_temp is '(T) [19] Stores the allocators and basis definitions for allocators that cannot be calculated because all allocation accounts in the basis definition were assigned 0% factors for the prior step.';
comment on column reg_case_alloc_dyn_zfact_temp.reg_case_id is 'System assigned identifier of a regulatory case.';
comment on column reg_case_alloc_dyn_zfact_temp.case_year is 'Regulatory case year in YYYYMM format.';
comment on column reg_case_alloc_dyn_zfact_temp.reg_alloc_category_id is 'System assigned identifier of a regulatory allocation category.';
comment on column reg_case_alloc_dyn_zfact_temp.reg_allocator_id is 'System assigned identifier of a regulatory allocator.';
comment on column reg_case_alloc_dyn_zfact_temp.row_number is 'System assigned dynamic allocator basis row number';
comment on column reg_case_alloc_dyn_zfact_temp.description is 'The description of a regulatory allocator.';
comment on column reg_case_alloc_dyn_zfact_temp.basis_string is 'The basis definition by allocator and row number.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3636, 0, 2017, 1, 0, 0, 48724, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048724_reg_alloc_dyn_zfact_temp_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;