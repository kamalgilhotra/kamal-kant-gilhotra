/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009371_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   02/29/2012 Julia Breuer   Point Release
||============================================================================
*/

--
-- Correct the description on the column for county on the 'Add : Preallo Items' import type.
--
update pwrplant.pt_import_column set description = 'County' where import_type_id = 9 and column_name = 'county_id';

--
-- Add a 'parent_table' column to the pt_import_column table, to be used to verify data passed in with no translation.
--
alter table pwrplant.pt_import_column add parent_table varchar2(35);

update pwrplant.pt_import_column set parent_table = 'yes_no' where column_name = 'adjust_is_percent_yn';
update pwrplant.pt_import_column set parent_table = 'prop_tax_allo_definition' where column_name = 'allocation_id';
update pwrplant.pt_import_column set parent_table = 'pt_assessment_group' where column_name = 'assessment_group_id';
update pwrplant.pt_import_column set parent_table = 'pt_assessor' where column_name = 'assessor_id';
update pwrplant.pt_import_column set parent_table = 'asset_location' where column_name = 'asset_location_id';
update pwrplant.pt_import_column set parent_table = 'yes_no' where column_name = 'autocalc_rsv_yn';
update pwrplant.pt_import_column set parent_table = 'business_segment' where column_name = 'bus_segment_id';
update pwrplant.pt_import_column set parent_table = 'pt_case' where column_name = 'case_id';
update pwrplant.pt_import_column set parent_table = 'company' where column_name = 'company_id';
update pwrplant.pt_import_column set parent_table = 'county' where column_name = 'county_id';
update pwrplant.pt_import_column set parent_table = 'depreciation_group' where column_name = 'depr_group_id';
update pwrplant.pt_import_column set parent_table = 'yes_no' where column_name = 'entity_is_lessee';
update pwrplant.pt_import_column set parent_table = 'yes_no' where column_name = 'entity_is_lessor';
update pwrplant.pt_import_column set parent_table = 'pt_escalated_value_type' where column_name = 'escalated_value_type_id';
update pwrplant.pt_import_column set parent_table = 'pt_parcel_geography_type' where column_name = 'geography_type_id';
update pwrplant.pt_import_column set parent_table = 'gl_account' where column_name = 'gl_account_id';
update pwrplant.pt_import_column set parent_table = 'yes_no' where column_name = 'is_parcel_one_to_one';
update pwrplant.pt_import_column set parent_table = 'pt_ledger_detail' where column_name = 'ledger_detail_id';
update pwrplant.pt_import_column set parent_table = 'pt_levy_class' where column_name = 'levy_class_id';
update pwrplant.pt_import_column set parent_table = 'pt_parcel' where column_name = 'parcel_id';
update pwrplant.pt_import_column set parent_table = 'pt_parcel_type' where column_name = 'parcel_type_id';
update pwrplant.pt_import_column set parent_table = 'pt_preallo_ledger' where column_name = 'preallo_ledger_id';
update pwrplant.pt_import_column set parent_table = 'pt_company' where column_name = 'prop_tax_company_id';
update pwrplant.pt_import_column set parent_table = 'prop_tax_location' where column_name = 'prop_tax_location_id';
update pwrplant.pt_import_column set parent_table = 'property_tax_adjust' where column_name = 'property_tax_adjust_id';
update pwrplant.pt_import_column set parent_table = 'pt_ledger' where column_name = 'property_tax_ledger_id';
update pwrplant.pt_import_column set parent_table = 'property_tax_type_data' where column_name = 'property_tax_type_id';
update pwrplant.pt_import_column set parent_table = 'pt_parcel_responsibility_type' where column_name = 'responsibility_type_id';
update pwrplant.pt_import_column set parent_table = 'pt_parcel_responsible_entity' where column_name = 'responsible_entity_id';
update pwrplant.pt_import_column set parent_table = 'yes_no' where column_name = 'spread_on_book_yn';
update pwrplant.pt_import_column set parent_table = 'state' where column_name = 'state_id';
update pwrplant.pt_import_column set parent_table = 'property_tax_authority' where column_name = 'tax_authority_id';
update pwrplant.pt_import_column set parent_table = 'prop_tax_district' where column_name = 'tax_district_id';
update pwrplant.pt_import_column set parent_table = 'property_tax_year' where column_name = 'tax_year';
update pwrplant.pt_import_column set parent_table = 'prop_tax_type_code' where column_name = 'type_code_id';
update pwrplant.pt_import_column set parent_table = 'yes_no' where column_name = 'use_composite_authority_yn';
update pwrplant.pt_import_column set parent_table = 'utility_account' where column_name = 'utility_account_id';
update pwrplant.pt_import_column set parent_table = 'work_order_control' where column_name = 'work_order_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (100, 0, 10, 3, 4, 0, 9371, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009371_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
