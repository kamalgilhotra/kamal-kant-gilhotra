/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052187_lessee_02_Remove_is_om_filter_lesse_queries_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.1 8/14/2018  Alex Healey    Change the 'is_om' references from the lessee queries to use the schedule and be consistent with the disclosure reports
||============================================================================
*/


DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Balance Sheet Detail by Asset (Company Currency)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, to_char(las.month,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.state_id as state, gl.external_account_code as account,
       ua.utility_account_id, ua.description as utility_account_description, fc.description as func_class_description,
       las.currency_display_symbol,
       las.iso_code AS currency,
       las.beg_capital_cost, las.end_capital_cost, las.beg_obligation, las.end_obligation,
       las.beg_lt_obligation, las.end_lt_obligation,
       case when las.month > depr_calc.month then las.begin_reserve else cprd.beg_reserve_month end as beg_reserve,
       case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end - nvl(cprd.retirements,0) as end_reserve,
       nvl(cprd. net_adds_and_adjust,0) as additions, nvl(cprd.retirements,0) + nvl(gain_loss,0) as retirements, nvl(cprd.transfers_in,0) as transfers_in,
       nvl(cprd.transfers_out,0) as transfers_out, nvl(cprd.reserve_trans_in,0) as reserve_trans_in, nvl(cprd.reserve_trans_out,0) as reserve_trans_out,
       (las.end_capital_cost - las.end_obligation - case when las.month > depr_calc.month then las.end_reserve else cprd.depr_reserve end ) as ferc_gaap_difference
       ,las.set_of_books_id,
       las.revision
from ls_asset la, ls_cpr_asset_map map, ls_ilr ilr, ls_lease ll, company co,
     cpr_ledger cpr, utility_account ua, business_segment bs, func_class fc, gl_account gl,
     v_ls_asset_schedule_fx_vw las, asset_location al,
     ls_ilr_options ilro, ls_lease_cap_type lct,
     (select m.ls_asset_id, c.*
      from cpr_depr c, ls_cpr_asset_map m, cpr_ledger l
      where c.asset_id = m.asset_id
        and m.asset_id = l.asset_id
        and to_char(c.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
        and l.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')) cprd,
     (select company_id, max(gl_posting_mo_yr) month
      from ls_process_control where depr_calc is not null
         and company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      group by company_id) depr_calc
where la.ls_asset_id = map.ls_asset_id
  and map.asset_id = cpr.asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilro.revision = ilr.current_revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and las.is_om = 0
  and cpr.gl_account_id = gl.gl_account_id
  and la.utility_account_id = ua.utility_account_id
  and la.bus_segment_id = ua.bus_segment_id
  and ua.func_class_id = fc.func_class_id
  and la.bus_segment_id = bs.bus_segment_id
  and co.company_id = la.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and depr_calc.company_id = co.company_id
  and las.month = cprd.gl_posting_mo_yr (+)
  and las.ls_asset_id = cprd.ls_asset_id (+)
  and las.set_of_books_id = cprd.set_of_books_id (+)
  and las.ls_cur_type = 2
  and la.asset_location_id = al.asset_location_id
  and to_char(las.month, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name)= ''MONTHNUM'')
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'account',
        15,
        0,
        1,
        '',
        'Account',
        300,
        'description',
        'ls_dist_gl_account',
        'VARCHAR2',
        0,
        'external_account_code',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'additions',
        29,
        1,
        1,
        '',
        'Additions',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'asset_description',
        4,
        0,
        1,
        '',
        'Asset Description',
        300,
        'description',
        'ls_asset',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_capital_cost',
        21,
        1,
        1,
        '',
        'Beg Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_obligation',
        25,
        1,
        1,
        '',
        'Beg Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_obligation',
        23,
        1,
        1,
        '',
        'Beg Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_reserve',
        27,
        1,
        1,
        '',
        'Beg Reserve',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_description',
        6,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_id',
        5,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        1,
        2,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'currency',
        20,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_display_symbol',
        19,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'end_capital_cost',
        22,
        1,
        1,
        '',
        'End Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_lt_obligation',
        26,
        1,
        1,
        '',
        'End Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_obligation',
        24,
        1,
        1,
        '',
        'End Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_reserve',
        28,
        1,
        1,
        '',
        'End Reserve',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'external_ilr',
        9,
        0,
        1,
        '',
        'External Ilr',
        300,
        'external_ilr',
        'ls_ilr',
        'VARCHAR2',
        0,
        'external_ilr',
        null,
        null,
        'external_ilr',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ferc_gaap_difference',
        35,
        1,
        1,
        '',
        'Ferc Gaap Difference',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'func_class_description',
        18,
        0,
        1,
        '',
        'Func Class Description',
        300,
        'description',
        'func_class',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_id',
        7,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_number',
        8,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_cap_type',
        12,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_id',
        10,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_number',
        11,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'leased_asset_number',
        2,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'location',
        13,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ls_asset_id',
        1,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'monthnum',
        3,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        2,
        'the_sort',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'reserve_trans_in',
        33,
        1,
        1,
        '',
        'Reserve Trans In',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'reserve_trans_out',
        34,
        1,
        1,
        '',
        'Reserve Trans Out',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'retirements',
        30,
        1,
        1,
        '',
        'Retirements',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'state',
        14,
        0,
        1,
        '',
        'State',
        300,
        'state_id',
        'state',
        'VARCHAR2',
        0,
        'state_id',
        null,
        null,
        'state_id',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'transfers_in',
        31,
        1,
        1,
        '',
        'Transfers In',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'transfers_out',
        32,
        1,
        1,
        '',
        'Transfers Out',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'utility_account_description',
        17,
        0,
        1,
        '',
        'Utility Account Description',
        300,
        'description',
        'utility_account',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'utility_account_id',
        16,
        0,
        1,
        '',
        'Utility Account Id',
        300,
        'description',
        'utility_account',
        'NUMBER',
        0,
        'utility_account_id',
        null,
        null,
        'description',
        0,
        0

FROM dual
;

END;

/

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9055, 0, 2017, 4, 0, 1, 52187, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.1_maint_052187_lessee_02_Remove_is_om_filter_lesse_queries_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;