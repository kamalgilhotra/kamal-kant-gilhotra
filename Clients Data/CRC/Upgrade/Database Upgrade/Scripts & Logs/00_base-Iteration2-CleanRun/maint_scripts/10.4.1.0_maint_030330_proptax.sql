/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_030330_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/21/2013 Julia Breuer   Point Release
||============================================================================
*/

--
-- Create a temp table to use when making mass preallo adjustments.
--
create global temporary table pt_temp_preallo_adjs
(
 preallo_ledger_id      number(22,0) not null,
 property_tax_adjust_id number(22,0) not null,
 user_input             number(22,0) not null,
 beg_bal_adjustment     number(22,2) default 0 not null,
 additions_adjustment   number(22,2) default 0 not null,
 retirements_adjustment number(22,2) default 0 not null,
 transfers_adjustment   number(22,2) default 0 not null,
 end_bal_adjustment     number(22,2) default 0 not null,
 cwip_adjustment        number(22,2) default 0 not null,
 notes                  varchar2(4000)
) on commit preserve rows;

create unique index PT_TEMP_PREALLO_ADJS_PK
   on PT_TEMP_PREALLO_ADJS (PREALLO_LEDGER_ID);

begin
   DBMS_STATS.SET_TABLE_STATS ('PWRPLANT','PT_TEMP_PREALLO_ADJS','',NULL,NULL,1,1,36,NULL);
   DBMS_STATS.LOCK_TABLE_STATS ('PWRPLANT','PT_TEMP_PREALLO_ADJS');
end;
/

--
-- Add fields to the preallo adjustment import.
--
alter table PT_IMPORT_PREALLO add ADJUST_IS_PERCENT_XLATE varchar2(254);
alter table PT_IMPORT_PREALLO add ADJUST_IS_PERCENT_YN    number(22,0);
alter table PT_IMPORT_PREALLO add SPREAD_ON_BOOK_XLATE    varchar2(254);
alter table PT_IMPORT_PREALLO add SPREAD_ON_BOOK_YN       number(22,0);

alter table PT_IMPORT_PREALLO_ARCHIVE add ADJUST_IS_PERCENT_XLATE varchar2(254);
alter table PT_IMPORT_PREALLO_ARCHIVE add ADJUST_IS_PERCENT_YN    number(22,0);
alter table PT_IMPORT_PREALLO_ARCHIVE add SPREAD_ON_BOOK_XLATE    varchar2(254);
alter table PT_IMPORT_PREALLO_ARCHIVE add SPREAD_ON_BOOK_YN       number(22,0);

insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN ) values ( 10, 'adjust_is_percent_yn', sysdate, user, 'Adjusment is Percent', 'adjust_is_percent_xlate', 0, 1, 'number(22,0)', 'yes_no', null, 0, null, 'yes_no_id' );
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN ) values ( 10, 'spread_on_book_yn', sysdate, user, 'Spread on Book Balance', 'spread_on_book_xlate', 0, 1, 'number(22,0)', 'yes_no', null, 0, null, 'yes_no_id' );

insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 10, 'adjust_is_percent_yn', 77 );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 10, 'adjust_is_percent_yn', 78 );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 10, 'spread_on_book_yn', 77 );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID ) values ( 10, 'spread_on_book_yn', 78 );

--
-- When I did the ppbasegui conversion, I did not update the pp_import_type table with the new logic object.
-- Update that here.
--
update PP_IMPORT_TYPE set DELEGATE_OBJECT_NAME = 'nvo_ptc_logic_import' where DELEGATE_OBJECT_NAME = 'uo_ptc_logic_import';

--
-- Make percent not required for Geography Type Factors, so that they can leave it null to delete a percent.
--
update PP_IMPORT_COLUMN set IS_REQUIRED = 0 where IMPORT_TYPE_ID = 19 and COLUMN_NAME = 'percent';

--
-- Update the description for Assessor on the Parcel Assessments import.  It isn't just for 1-to-1 parcels,
-- as it gets stored on the assessment table.
--
update PP_IMPORT_COLUMN set DESCRIPTION = 'Assessor' where IMPORT_TYPE_ID = 11 and COLUMN_NAME = 'assessor_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (481, 0, 10, 4, 1, 0, 30330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030330_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;