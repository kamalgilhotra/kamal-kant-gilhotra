/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035740_lease_residual_jes.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/11/2014 Ryan Oliveria
||============================================================================
*/

alter table LS_ILR_GROUP add RES_DEBIT_ACCOUNT_ID number(22,0);
alter table LS_ILR_GROUP add RES_CREDIT_ACCOUNT_ID number(22,0);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK12
   foreign key (RES_DEBIT_ACCOUNT_ID)
      references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK13
   foreign key (RES_CREDIT_ACCOUNT_ID)
      references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT add RES_DEBIT_ACCOUNT_ID number(22,0);
alter table LS_ILR_ACCOUNT add RES_CREDIT_ACCOUNT_ID number(22,0);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK12
       foreign key (RES_DEBIT_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK13
       foreign key (RES_CREDIT_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

create or replace type T_NUM_ARRAY as table of number;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (957, 0, 10, 4, 2, 0, 35740, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035740_lease_residual_jes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;