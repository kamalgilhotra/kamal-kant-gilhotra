--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_043157_reimb_wo_est_pull_system_control_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/16/2015 Anand R          Reimbursables - Add new system control
--||============================================================================
--*/

insert into REIMB_SYSTEM_CONTROL( CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, DESCRIPTION)
values (43, 'Estimate Table', 'monthly', 'Specifies what table to pull work order estimates from. monthy - WO_EST_MONTHLY table, unit - WO_ESTIMATE table', 'monthly;unit');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2385, 0, 2015, 1, 0, 0, 43157, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043157_reimb_wo_est_pull_system_control_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;