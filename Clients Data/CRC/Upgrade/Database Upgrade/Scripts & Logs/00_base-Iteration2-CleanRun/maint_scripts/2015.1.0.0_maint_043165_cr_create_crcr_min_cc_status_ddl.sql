/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_043165_cr_create_crcr_min_cc_status_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 2015.1   03/18/2015 Daniel Motter        Creation
||========================================================================================
*/

create table crcr_min_cc_status (
    drilldown_key      number(22,0),
    cwip_charge_status number(22,0)
);

alter table crcr_min_cc_status 
    add constraint crcr_min_cc_status_pk
        primary key (drilldown_key)
        using index tablespace pwrplant_idx;

comment on table crcr_min_cc_status is '(C) [03] Used in the cwip charge executable to map drilldown keys to cwip charge statuses.';

comment on column crcr_min_cc_status.drilldown_key is 'The key linking the detail transactions to transactions in this table.  Used for drilldowns.';

comment on column crcr_min_cc_status.cwip_charge_status is 'Cwip charge status associated with the drilldown key.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2446, 0, 2015, 1, 0, 0, 43165, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043165_cr_create_crcr_min_cc_status_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;