/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039299_pwrtax_mlp_cleanup.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/01/2014 Alex P.
||============================================================================
*/

update PP_REPORTS_TIME_OPTION
   set LABEL1 = 'Tax Years'
 where DESCRIPTION = 'PowerTax - Multi Tax Year'
   and PP_REPORT_TIME_OPTION_ID = 52;

update PP_REPORTS
   set LONG_DESCRIPTION = 'A generic report of depreciation items where the depreciation items are reported by Company, Tax Book and Tax Class'
 where REPORT_ID = 403014
   and REPORT_NUMBER = 'PwrTax - 64';

alter table TAX_RECORD_CONTROL drop column TAX_LAYER_ID;

alter table TAX_DEPRECIATION add TAX_LAYER_ID number(22, 0);
comment on column TAX_DEPRECIATION.TAX_LAYER_ID is 'System-assigned identifier for each Tax Layer.';

alter table TAX_DEPRECIATION
   add constraint TAX_DEPRECIATION_LAYER_FK
       foreign key (TAX_LAYER_ID)
       references TAX_LAYER;

alter table TAX_RECORD_CONTROL add K1_EXPORT_ID number(22, 0);
comment on column TAX_RECORD_CONTROL.K1_EXPORT_ID is 'System-assigned identifier for each K1 export.';

update PP_DYNAMIC_FILTER
   set REQUIRED = 1
 where FILTER_ID = 179
   and LABEL = 'Tax Year';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1304, 0, 10, 4, 3, 0, 39299, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039299_pwrtax_mlp_cleanup.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;