/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051563_lessor_03_v_lsr_ilr_mc_schedule_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/28/2018 Powers           Update the rate logic for amounts to 
||										   use average rate versus end of period rates
||============================================================================
*/

CREATE OR REPLACE VIEW V_LSR_ILR_MC_SCHEDULE
(ilr_id, ilr_number, lease_id, lease_number, current_revision, revision, set_of_books_id, month, company_id, open_month, ls_cur_type, exchange_date, 
prev_exchange_date, contract_currency_id, display_currency_id, rate, calculated_rate, previous_calculated_rate, iso_code, currency_display_symbol, 
interest_income_received, interest_income_accrued, beg_deferred_rent, deferred_rent, end_deferred_rent, beg_accrued_rent, accrued_rent, end_accrued_rent, 
beg_receivable, end_receivable, beg_long_term_receivable, end_long_term_receivable,deferred_rev_activity,interest_rental_recvd_spread,end_deferred_rev,
initial_direct_cost, executory_accrual1, executory_accrual2, 
executory_accrual3, executory_accrual4, executory_accrual5, executory_accrual6, executory_accrual7, executory_accrual8, executory_accrual9, 
executory_accrual10, executory_paid1, executory_paid2, executory_paid3, executory_paid4, executory_paid5, executory_paid6, executory_paid7, 
executory_paid8, executory_paid9, executory_paid10, contingent_accrual1, contingent_accrual2, contingent_accrual3, contingent_accrual4, 
contingent_accrual5, contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10, contingent_paid1, 
contingent_paid2, contingent_paid3, contingent_paid4, contingent_paid5, contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, 
contingent_paid10, principal_received, principal_accrued, beg_unguaranteed_residual, interest_unguaranteed_residual, ending_unguaranteed_residual, 
beg_net_investment, interest_net_investment, ending_net_investment, beginning_deferred_profit, recognized_profit, ending_deferred_profit, 
gain_loss_fx, rates_exchange_date, rates_rate, rates_last_exchange_date, rates_last_rate,average_rate,ST_CURRENCY_GAIN_LOSS,LT_CURRENCY_GAIN_LOSS)
AS
WITH cur AS ( SELECT ls_currency_type_id AS ls_cur_type,
       currency_id,
       currency_display_symbol,
       iso_code,
         CASE
      ls_currency_type_id
      WHEN
        1
      THEN
        1
      ELSE
        NULL
    END
  AS contract_approval_rate
FROM currency CROSS JOIN ls_lease_currency_type),open_month AS ( SELECT company_id,
       MIN(gl_posting_mo_yr) open_month
FROM lsr_process_control WHERE open_next IS NULL GROUP BY
  company_id
),calc_rate AS ( SELECT a.company_id,
       a.contract_currency_id,
       a.company_currency_id,
       a.accounting_month,
       a.exchange_date,
       a.rate,
       b.rate prev_rate
FROM ls_lease_calculated_date_rates a
  LEFT OUTER JOIN ls_lease_calculated_date_rates b ON a.company_id = b.company_id
  AND a.contract_currency_id = b.contract_currency_id
  AND a.accounting_month = add_months(b.accounting_month,1) 
  AND a.exchange_rate_type_id =1 and b.exchange_rate_type_id =1
),rate_now AS ( SELECT currency_from,
       currency_to,
       rate
FROM ( SELECT currency_from,
         currency_to,
         rate,
         ROW_NUMBER() OVER(PARTITION BY
      currency_from,
      currency_to
      ORDER BY
        exchange_date
      DESC
    ) AS rn
  FROM currency_rate_default_dense WHERE trunc(exchange_date,'MONTH') <= trunc(SYSDATE,'MONTH')
    AND exchange_rate_type_id = 1
  )
WHERE rn = 1 ),
calc_avg_rate AS ( SELECT a.company_id,a.contract_currency_id,a.company_currency_id,
                   a.accounting_month,a.exchange_date, a.exchange_rate_type_id,
                   a.rate as rate,b.rate prev_rate
            FROM   LS_LEASE_CALCULATED_DATE_RATES a
                   left outer join LS_LEASE_CALCULATED_DATE_RATES b
                                ON a.company_id = b.company_id AND
                                   a.contract_currency_id = b.contract_currency_id AND
                                   a.accounting_month = Add_months( b.accounting_month, 1 ) 
            WHERE a.exchange_rate_type_id = 4 and b.exchange_rate_type_id = 4) 
SELECT schedule.ilr_id ilr_id,
  schedule.ilr_number,
  lease.lease_id,
  lease.lease_number,
  schedule.current_revision,
  schedule.revision revision,
  schedule.set_of_books_id set_of_books_id,
  schedule.month month,
  open_month.company_id,
  open_month.open_month,
  cur.ls_cur_type AS ls_cur_type,
  rates.exchange_date,
  calc_rate.exchange_date prev_exchange_date,
  lease.contract_currency_id,
  cur.currency_id display_currency_id,
  rates.rate,
  calc_rate.rate calculated_rate,
  calc_rate.prev_rate previous_calculated_rate,
  cur.iso_code,
  cur.currency_display_symbol,
  schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_income_received,
  schedule.interest_income_accrued * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_income_accrued,
  schedule.beg_deferred_rent * nvl(calc_rate.rate,rates.rate) beg_deferred_rev,
  schedule.deferred_rent * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) deferred_rent,
  schedule.end_deferred_rent * nvl(calc_rate.rate,rates.rate) end_deferred_rent,
  schedule.beg_accrued_rent * nvl(calc_rate.rate,rates.rate) beg_accrued_rent,
  schedule.accrued_rent * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) accrued_rent,
  schedule.end_accrued_rent * nvl(calc_rate.rate,rates.rate) end_accrued_rent,
  schedule.beg_receivable * nvl(calc_rate.rate,rates.rate) beg_receivable,
  schedule.end_receivable * nvl(calc_rate.rate,rates.rate) end_receivable,
  schedule.beg_lt_receivable * nvl(calc_rate.rate,rates.rate) beg_long_term_receivable,
  schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate) end_long_term_receivable,
  schedule.deferred_rev_activity * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) deferred_rev_activity,
  schedule.interest_rental_recvd_spread * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_rental_recvd_spread,
  schedule.end_deferred_rev * nvl(calc_rate.rate,rates.rate) end_deferred_rev,
  schedule.initial_direct_cost * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) initial_direct_cost,
  schedule.executory_accrual1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual1,
  schedule.executory_accrual2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual2,
  schedule.executory_accrual3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual3,
  schedule.executory_accrual4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual4,
  schedule.executory_accrual5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual5,
  schedule.executory_accrual6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual6,
  schedule.executory_accrual7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual7,
  schedule.executory_accrual8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual8,
  schedule.executory_accrual9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual9,
  schedule.executory_accrual10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_accrual10,
  schedule.executory_paid1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid1,
  schedule.executory_paid2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid2,
  schedule.executory_paid3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid3,
  schedule.executory_paid4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid4,
  schedule.executory_paid5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid5,
  schedule.executory_paid6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid6,
  schedule.executory_paid7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid7,
  schedule.executory_paid8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid8,
  schedule.executory_paid9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid9,
  schedule.executory_paid10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) executory_paid10,
  schedule.contingent_accrual1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual1,
  schedule.contingent_accrual2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual2,
  schedule.contingent_accrual3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual3,
  schedule.contingent_accrual4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual4,
  schedule.contingent_accrual5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual5,
  schedule.contingent_accrual6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual6,
  schedule.contingent_accrual7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual7,
  schedule.contingent_accrual8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual8,
  schedule.contingent_accrual9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual9,
  schedule.contingent_accrual10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_accrual10,
  schedule.contingent_paid1 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid1,
  schedule.contingent_paid2 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid2,
  schedule.contingent_paid3 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid3,
  schedule.contingent_paid4 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid4,
  schedule.contingent_paid5 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid5,
  schedule.contingent_paid6 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid6,
  schedule.contingent_paid7 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid7,
  schedule.contingent_paid8 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid8,
  schedule.contingent_paid9 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid9,
  schedule.contingent_paid10 * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) contingent_paid10,
  schedule.principal_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) principal_received,
  schedule.principal_accrued * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) principal_accrued,
  schedule.beg_unguaranteed_residual * nvl(calc_rate.rate,rates.rate) beg_unguaranteed_residual,
  schedule.interest_unguaranteed_residual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_unguaranteed_residual,
  schedule.ending_unguaranteed_residual * nvl(calc_rate.rate,rates.rate) ending_unguaranteed_residual,
  schedule.beg_net_investment * nvl(calc_rate.rate,rates.rate) beg_net_investment,
  schedule.interest_net_investment * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) interest_net_investment,
  schedule.ending_net_investment * nvl(calc_rate.rate,rates.rate) ending_net_investment,
  schedule.begin_deferred_profit * nvl(calc_rate.rate,rates.rate) beginning_deferred_profit,
  schedule.recognized_profit * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)) recognized_profit,
  schedule.end_deferred_profit * nvl(calc_rate.rate,rates.rate) ending_deferred_profit,
  case lease.lease_cap_type_id 
	  when 1 then 
		(
		  ((schedule.beg_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.rate,rates.rate)))
		  - ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate)))
		  - (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
		) 
		else  
		(((schedule.beg_receivable * nvl(calc_rate.rate,rates.rate)) + (schedule.beg_unguaranteed_residual * nvl(calc_rate.rate,rates.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.rate,rates.rate))) 
			- ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) + (schedule.ending_unguaranteed_residual * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate))) 
			+ ((schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) + (schedule.interest_unguaranteed_residual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))))
			- ((schedule.principal_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) + (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))))
		) end 
    + ((schedule.beg_lt_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate))) gain_loss_fx,
  rates.exchange_date,
  rates.rate,
  rates_last.exchange_date,
  rates_last.rate,
  Decode(lower(sc.CONTROL_VALUE), 'yes', CALC_AVG_RATE.rate, cr_avg.rate)                                      average_rate,
  case lease.lease_cap_type_id 
	  when 1 then 
		(
		  ((schedule.beg_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.rate,rates.rate)))
		  - ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate)))
		  - (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate)))
		) 
		else  
		(((schedule.beg_receivable * nvl(calc_rate.rate,rates.rate)) + (schedule.beg_unguaranteed_residual * nvl(calc_rate.rate,rates.rate)) - (schedule.beg_lt_receivable * nvl(calc_rate.rate,rates.rate))) 
			- ((schedule.end_receivable * nvl(calc_rate.rate,rates.rate)) + (schedule.ending_unguaranteed_residual * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate))) 
			+ ((schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) + (schedule.interest_unguaranteed_residual * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))))
			- ((schedule.principal_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))) + (schedule.interest_income_received * Decode(lower(sc.CONTROL_VALUE), 'yes', Nvl( CALC_AVG_RATE.rate, cr_avg.rate ), nvl(calc_rate.rate,rates.rate))))
		)
		end as st_currency_gain_loss ,                                                                                                             
    (
        (schedule.beg_lt_receivable * nvl(calc_rate.rate,rates.rate)) - (schedule.end_lt_receivable * nvl(calc_rate.rate,rates.rate))         
    ) lt_currency_gain_loss  
FROM (SELECT schedule.ilr_id,
			 ilr.ilr_number,
			 ilr.current_revision,
			 schedule.revision,
			 schedule.set_of_books_id,
			 schedule.month,
			 schedule.interest_income_received,
			 schedule.interest_income_accrued,
       schedule.beg_deferred_rent,
       schedule.deferred_rent,
       schedule.end_deferred_rent,
       schedule.beg_accrued_rent,
       schedule.accrued_rent,
       schedule.end_accrued_rent,
			 schedule.beg_receivable,
			 schedule.end_receivable,
			 schedule.beg_lt_receivable,
			 schedule.end_lt_receivable,
       schedule.deferred_rev_activity,
       schedule.interest_rental_recvd_spread,
       schedule.end_deferred_rev,
			 schedule.initial_direct_cost,
			 schedule.executory_accrual1,
			 schedule.executory_accrual2,
			 schedule.executory_accrual3,
			 schedule.executory_accrual4,
			 schedule.executory_accrual5,
			 schedule.executory_accrual6,
			 schedule.executory_accrual7,
			 schedule.executory_accrual8,
			 schedule.executory_accrual9,
			 schedule.executory_accrual10,
			 schedule.executory_paid1,
			 schedule.executory_paid2,
			 schedule.executory_paid3,
			 schedule.executory_paid4,
			 schedule.executory_paid5,
			 schedule.executory_paid6,
			 schedule.executory_paid7,
			 schedule.executory_paid8,
			 schedule.executory_paid9,
			 schedule.executory_paid10,
			 schedule.contingent_accrual1,
			 schedule.contingent_accrual2,
			 schedule.contingent_accrual3,
			 schedule.contingent_accrual4,
			 schedule.contingent_accrual5,
			 schedule.contingent_accrual6,
			 schedule.contingent_accrual7,
			 schedule.contingent_accrual8,
			 schedule.contingent_accrual9,
			 schedule.contingent_accrual10,
			 schedule.contingent_paid1,
			 schedule.contingent_paid2,
			 schedule.contingent_paid3,
			 schedule.contingent_paid4,
			 schedule.contingent_paid5,
			 schedule.contingent_paid6,
			 schedule.contingent_paid7,
			 schedule.contingent_paid8,
			 schedule.contingent_paid9,
			 schedule.contingent_paid10,
			 st_schedule.principal_received,
			 st_schedule.principal_accrued,
			 st_schedule.beg_unguaranteed_residual,
			 st_schedule.interest_unguaranteed_residual,
			 st_schedule.ending_unguaranteed_residual,
			 st_schedule.beg_net_investment,
			 st_schedule.interest_net_investment,
			 st_schedule.ending_net_investment,
			 df_schedule.begin_deferred_profit,
			 df_schedule.recognized_profit,
			 df_schedule.end_deferred_profit,
			 ilr.lease_id,
			 ilr.company_id,
			 options.in_service_exchange_rate,
			 options.purchase_option_amt,
			 options.termination_amt,
			 schedule.rowid AS lisrowid,
			 options.rowid AS optrowid,
			 ilr.rowid AS ilrrowid,
			 lease.rowid AS leaserowid,
			 st_schedule.rowid AS salesrowid
		FROM lsr_ilr_schedule schedule,
			 lsr_ilr_options options,
			 lsr_ilr ilr,
			 lsr_lease lease,
			 lsr_ilr_schedule_sales_direct st_schedule,
			 lsr_ilr_schedule_direct_fin df_schedule
		WHERE schedule.ilr_id = options.ilr_id
		  AND schedule.revision = options.revision
		  AND schedule.ilr_id = ilr.ilr_id
		  AND ilr.lease_id = lease.lease_id
		  AND st_schedule.ilr_id (+) = schedule.ilr_id
		  AND st_schedule.revision (+) = schedule.revision
		  AND st_schedule.month (+) = schedule.month
		  AND st_schedule.set_of_books_id (+) = schedule.set_of_books_id
		  AND df_schedule.ilr_id (+) = schedule.ilr_id
		  AND df_schedule.revision (+) = schedule.revision
		  AND df_schedule.month (+) = schedule.month
		  AND df_schedule.set_of_books_id (+) = schedule.set_of_books_id
  ) schedule
INNER JOIN lsr_lease lease ON schedule.lease_id = lease.lease_id
  INNER JOIN currency_schema cs ON schedule.company_id = cs.company_id
  INNER JOIN cur ON cur.currency_id =
    CASE
      cur.ls_cur_type
      WHEN
        1
      THEN
        lease.contract_currency_id
      WHEN
        2
      THEN
        cs.currency_id
    END
  INNER JOIN open_month ON schedule.company_id = open_month.company_id 
  INNER JOIN currency_rate_default_dense rates ON cur.currency_id = rates.currency_to
  AND lease.contract_currency_id = rates.currency_from
  AND trunc(rates.exchange_date,'MONTH') = trunc(schedule.month,'MONTH')
  INNER JOIN currency_rate_default_dense rates_last ON cur.currency_id = rates_last.currency_to
  AND lease.contract_currency_id = rates_last.currency_from
  AND trunc(rates_last.exchange_date,'MONTH') = trunc(Add_Months(schedule.MONTH, -1),'MONTH')
  INNER JOIN rate_now ON cur.currency_id = rate_now.currency_to
  AND lease.contract_currency_id = rate_now.currency_from
  LEFT OUTER JOIN calc_rate ON lease.contract_currency_id = calc_rate.contract_currency_id
  AND cur.currency_id = calc_rate.company_currency_id
  AND schedule.company_id = calc_rate.company_id
  AND schedule.month = calc_rate.accounting_month
  inner join pp_system_control_companies sc ON open_month.company_id = sc.company_id
  AND lower(trim(sc.control_name)) = 'lease mc: use average rates'
  inner join CURRENCY_RATE_DEFAULT_DENSE cr_avg ON CUR.currency_id = cr_avg.currency_to
  AND lease.contract_currency_id = cr_avg.currency_from
  AND Trunc( cr_avg.exchange_date, 'MONTH' ) = Trunc( schedule.month, 'MONTH' )
  and cr_avg.exchange_rate_type_id = decode(lower(sc.control_value),'yes',4,1)
  left outer join calc_avg_rate
  ON lease.contract_currency_id = calc_avg_rate.contract_currency_id
  AND CUR.currency_id = calc_avg_rate.company_currency_id
  AND schedule.company_id = calc_avg_rate.company_id
  AND schedule.month = calc_avg_rate.accounting_month
WHERE cs.currency_type_id = 1
  AND rates.exchange_rate_type_id = 1
  AND rates_last.exchange_rate_type_id = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7544, 0, 2017, 4, 0, 0, 51563, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051563_lessor_03_v_lsr_ilr_mc_schedule_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
