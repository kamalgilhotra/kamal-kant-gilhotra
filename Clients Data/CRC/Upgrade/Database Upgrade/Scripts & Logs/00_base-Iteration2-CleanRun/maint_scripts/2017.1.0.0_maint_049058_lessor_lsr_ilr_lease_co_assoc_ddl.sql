 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_049058_lessor_lsr_ilr_lease_co_assoc_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 09/25/2017 Shane "C" Ward   Remove UK for ILR Number-Co, re-implement lease company fk
 ||============================================================================
 */

ALTER TABLE LSR_ILR
  DROP CONSTRAINT lsr_ilr_number_company_uk;

ALTER TABLE LSR_ILR
  ADD CONSTRAINT r_lsr_ilr4 FOREIGN KEY ( lease_id, company_id) REFERENCES LSR_LEASE_COMPANY (lease_id, company_id);
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3739, 0, 2017, 1, 0, 0, 49058, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049058_lessor_lsr_ilr_lease_co_assoc_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
