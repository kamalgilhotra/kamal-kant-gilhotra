/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053259_lessor_01_rates_sb_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 03/22/2019 B. Beck    	Add set_of_books_id to lsr_ilr_rates
||										Create a conversion table with set of books added
||============================================================================
*/


-- LSR_ILR_RATES - will be used to convert exisitng records to new layout
create table lsr_ilr_rates_conversion
(
	ilr_id number(22,0) not null,
	revision number(22,0) not null,
	set_of_books_id number(22,0) not null,
	rate_type_id number(22, 0) not null,
	rate float,
	time_stamp date,
	user_id varchar2(18)
);

-- Only creating the Primary Key for conversion purposes, the foreign keys are added in a later script after the table is renamed
alter table lsr_ilr_rates_conversion add (
	constraint pk_lsr_ilr_rates_conv primary key (rate_type_id, ilr_id, revision, set_of_books_id) using index tablespace pwrplant_idx);
  
-- NOTE: This table will replace lsr_ilr_rates in a later script, so the table comment is written knowing that the table will be renamed                                                   
comment on table lsr_ilr_rates_conversion is 'The Lessor ILR Rates table holds applicable "annualized" rates for an ILR for a specific Revision / Set of book';

comment on column lsr_ilr_rates_conversion.ilr_id is 'System assigned identifier of a lessor ilr.';
comment on column lsr_ilr_rates_conversion.revision is 'Identifier of a Lessor ILR Revision .';
comment on column lsr_ilr_rates_conversion.set_of_books_id is 'System assigned identifier of a set of books.';
comment on column lsr_ilr_rates_conversion.rate_type_id is 'System Assigned ID of a Lessor ILR Rate Type .';
comment on column lsr_ilr_rates_conversion.rate is 'Percentage Rate to be used for the Rate Type assigned. Stored in Decimal format not Percent.';
comment on column lsr_ilr_rates_conversion.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column lsr_ilr_rates_conversion.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16164, 0, 2018, 2, 1, 0, 53259 , 'C:\PlasticWks\powerplant\sql\maint_scripts','2018.2.1.0_maint_053259_lessor_01_rates_sb_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;