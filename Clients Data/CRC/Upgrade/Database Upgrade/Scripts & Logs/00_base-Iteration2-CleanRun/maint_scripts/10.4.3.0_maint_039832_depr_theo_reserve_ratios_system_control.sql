/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039832_depr_theo_reserve_ratios_system_control.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/11/2014 Kyle Peterson
||============================================================================
*/

insert into PP_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION, COMPANY_ID)
   (select max(CONTROL_ID) + 1,
           'Theo Reserve Ratios Convention',
           .5,
           'This controls how the age for each vintage is computed as part of the theoretical reserve allocation. Default is 0.5 which equates to 1/2 year convention.',
           -1
      from PP_SYSTEM_CONTROL);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1416, 0, 10, 4, 3, 0, 39832, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039832_depr_theo_reserve_ratios_system_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
