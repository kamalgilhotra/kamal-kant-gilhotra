/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009182_sys.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   1/19/2012  Paul Bull      Table Maintenance
||============================================================================
*/

-- Inserts for pp_user_current_responses

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, COLUMN_RANK)
values
   ('wo_est_monthly_default', 'wo_work_order_id', 'Work Order Id', 'e', 12);

insert into POWERPLANT_COLUMNS
   (TABLE_NAME, COLUMN_NAME, DESCRIPTION, PP_EDIT_TYPE_ID, COLUMN_RANK)
values
   ('wo_est_rate_default', 'wo_work_order_id', 'Work Order Id', 'e', 12);


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (91, 0, 10, 3, 5, 0, 9182, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009182_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
