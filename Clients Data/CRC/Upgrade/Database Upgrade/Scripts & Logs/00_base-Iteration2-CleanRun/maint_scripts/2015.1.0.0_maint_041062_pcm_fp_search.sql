/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_41062_pcm_fp_search.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	11/11/2014 Ryan Oliveria		Adding dynamic filters for FP Search
||========================================================================================
*/

--* Reports Filters
insert into PP_REPORTS_FILTER
	(PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
	(86, 'PCM - Funding Projects', null, 'uo_pcm_fp_tab_search');



--* Dynamic Filters
insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION) values (213, 'Funding Project Number', 'sle', 'work_order_control.work_order_number');
insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION) values (214, 'Funding Project Description', 'sle', 'work_order_control.description');

update PP_DYNAMIC_FILTER
   set DW = 'dw_pp_budget_filter', LABEL = 'Program'
 where FILTER_ID = 29;

update PP_DYNAMIC_FILTER
   set DW = 'dw_pp_budget_number_filter', LABEL = 'Program Number'
 where FILTER_ID = 30;

update PP_DYNAMIC_FILTER
   set DW = 'dw_pp_budget_version_filter'
 where FILTER_ID = 31;


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(215, 'Major Location', 'dw', 'major_location.major_location_id', 'dw_pp_major_location_filter', 'major_location_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(216, 'Asset Location', 'dw', 'asset_location.asset_location_id', 'dw_pp_asset_location_filter', 'asset_location_id', 'long_description', 'N');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(217, 'Funding Project Type', 'dw', 'work_order_control.work_order_type_id', 'dw_pp_work_order_type_filter', 'work_order_type_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(218, 'Funding Project Status', 'dw', 'work_order_control.wo_status_id', 'dw_pp_work_order_status_filter', 'wo_status_id', 'description', 'N');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(219, 'Responsible Dept', 'dw', 'work_order_control.department_id', 'dw_pp_department_filter', 'department_id', 'dept_description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE,
	DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(220, 'Charging Dept', 'dw', 'wo_est_monthly.department_id', 'work_order_control.work_order_id in (select distinct work_order_id from wo_est_monthly where [selected_values])',
	'dw_pp_department_filter', 'department_id', 'dept_description', 'N');


update PP_DYNAMIC_FILTER
   set DW = 'dw_pp_work_order_group_filter'
 where FILTER_ID = 32;


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, VALID_OPERATORS)
values
	(221, 'Completion Date', 'daterange', 'work_order_control.completion_date', 'Between,=,<,>,<=,>=');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, VALID_OPERATORS)
values
	(222, 'Est Complete Date', 'daterange', 'work_order_control.est_complete_date', 'Between,=,<,>,<=,>=');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, VALID_OPERATORS)
values
	(223, 'Est In-Service Date', 'daterange', 'work_order_control.est_in_service_date', 'Between,=,<,>,<=,>=');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, VALID_OPERATORS)
values
	(224, 'Est Start Date', 'daterange', 'work_order_control.est_start_date', 'Between,=,<,>,<=,>=');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, VALID_OPERATORS)
values
	(225, 'In-Service Date', 'daterange', 'work_order_control.in_service_date', 'Between,=,<,>,<=,>=');


insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION) values (226, 'External WO Number', 'sle', 'work_order_control.external_wo_number');
insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION) values (227, 'Notes', 'sle', 'work_order_control.notes');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(228, 'Business Segment', 'dw', 'work_order_control.bus_segment_id', 'dw_pp_business_segment_filter', 'bus_segment_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(229, 'AFUDC Type', 'dw', 'work_order_account.afudc_type_id', 'dw_pp_afudc_type_filter', 'afudc_type_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(230, 'Agreement', 'dw', 'work_order_account.agreemnt_id', 'dw_pp_agreement_filter', 'agreemnt_id', 'description', 'N');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(231, 'Allocation Method', 'dw', 'work_order_account.alloc_method_type_id', 'dw_pp_alloc_method_filter', 'alloc_method_type_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE,
	DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(232, 'ARO Work Order', 'dw', 'aro_work_order.aro_id', 'work_order_control.work_order_id in (select distinct work_order_id from aro_work_order where [selected_values])',
	'dw_pp_aro_wo_filter', 'aro_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(233, 'Budget Organization', 'dw', 'budget.budget_organization_id', 'dw_pp_budget_org_filter', 'budget_organization_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(234, 'Budget Summary', 'dw', 'budget.budget_summary_id', 'dw_pp_budget_summary_filter', 'budget_summary_id', 'description', 'N');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, VALID_OPERATORS)
values
	(235, 'Close Date', 'daterange', 'work_order_control.close_date', 'Between,=,<,>,<=,>=');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, VALID_OPERATORS)
values
	(236, 'Date Initiated', 'daterange', 'work_order_initiator.initiation_date', 'Between,=,<,>,<=,>=');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(237, 'Closing Option', 'dw', 'work_order_account.closing_option_id', 'dw_pp_closing_option_filter', 'closing_option_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(238, 'Contract Admin', 'dw', 'work_order_initiator.contract_adm', 'dw_pp_contract_admin_filter', 'contract_adm', 'description', 'C');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(239, 'Division', 'dw', 'major_location.division_id', 'dw_pp_division_filter', 'division_id', 'description', 'N');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(240, 'Eligible for AFUDC', 'dw', 'work_order_account.eligible_for_afudc', 'dw_pp_yes_no_filter', 'yes_no_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(241, 'Eligible for CPI', 'dw', 'work_order_account.eligible_for_cpi', 'dw_pp_yes_no_filter', 'yes_no_id', 'description', 'N');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(242, 'Engineer', 'dw', 'work_order_initiator.engineer', 'dw_pp_engineer_filter', 'engineer', 'description', 'C');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(243, 'Initiator', 'dw', 'work_order_initiator.users', 'dw_pp_initiator_filter', 'users', 'description', 'C');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(244, 'Plant Accountant', 'dw', 'work_order_initiator.plant_accountant', 'dw_pp_plant_accountant_filter', 'plant_accountant', 'description', 'C');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(245, 'Project Manager', 'dw', 'work_order_initiator.proj_mgr', 'dw_pp_proj_mgr_filter', 'proj_mgr', 'description', 'C');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(246, 'Functional Class', 'dw', 'work_order_account.func_class_id', 'dw_pp_func_class_filter', 'func_class_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(247, 'Other', 'dw', 'work_order_initiator.other', 'dw_pp_other_filter', 'other', 'description', 'C');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(248, 'Reason Code', 'dw', 'work_order_control.reason_cd_id', 'dw_pp_reason_code_filter', 'reason_cd_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(249, 'Reimbursable', 'dw', 'work_order_account.reimbursable_type_id', 'dw_pp_reimbursable_filter', 'reimbursable_type_id', 'description', 'N');


insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE,
	DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(250, 'Tax Status', 'dw', 'work_order_tax_status.tax_status_id', 'work_order_control.work_order_id in (select work_order_id from work_order_tax_status where effective_accounting_month = (select max(effective_accounting_month) from work_order_tax_status wots2 where wots2.work_order_id = work_order_tax_status.work_order_id) and [selected_values] ) ',
	'dw_pp_wo_tax_status_filter', 'tax_status_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER
	(FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE)
values
	(251, 'Town', 'dw', 'asset_location.town_id', 'dw_pp_town_filter', 'town_id', 'description', 'N');

insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION) values (252, 'WO ID from Excel', 'sle', 'work_order_control.work_order_id');
insert into PP_DYNAMIC_FILTER (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION) values (253, 'WO Number from Excel', 'sle', 'work_order_control.work_order_number');



--* Dynamic Filter Mappings
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 213) /*FP Number*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 214) /*FP Description*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 3) /*Company (Description)*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 29) /*Budget (Description)*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 30) /*Budget (Budget Number)*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 31) /*Budget Version (Description)*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 215) /*Major Location*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 216) /*Asset Location*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 217) /*FP Type*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 218) /*FP Status*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 219) /*Responsible Dept*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 220) /*Charging Dept*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 32) /*Work Order Group*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 221) /*Completion Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 222) /*Est Complete Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 223) /*Est In-Service Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 224) /*Est Start Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 225) /*In-Service Date*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 226) /*External WO Number*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 227) /*Notes*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 228) /*Business Segment*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 229) /*AFUDC Type*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 230) /*Agreement*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 231) /*Allocation Method*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 232) /*ARO Work Order*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 233) /*Budget Organization*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 234) /*Budget Summary*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 235) /*Close Date*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 236) /*Date Initiated*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 237) /*Closing Option*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 238) /*Contract Admin*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 239) /*Division*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 240) /*Eligible for AFUDC*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 241) /*Eligible for CPI*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 242) /*Engineer*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 243) /*Initiator*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 244) /*Plant Accountant*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 245) /*Project Manager*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 246) /*Functional Class*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 247) /*Other*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 248) /*Reason Code*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 249) /*Reimbursable*/;

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 250) /*Tax Status*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 251) /*Town*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 252) /*WO ID from Excel*/;
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (86, 253) /*WO Number from Excel*/;


--* Restrictions

-- Asset Location by Major Location and Town
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (65, 216, 215, 'asset_location.major_location_id');
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (66, 216, 251, 'asset_location.town_id');

-- Responsible Department by Division
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (67, 219, 239, 'department.division_id');

-- Division by Company, Bus Segment
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (68, 239, 3, 'division.company_id');
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (69, 239, 228, 'division.bus_segment_id');

-- Major Location by Company
insert into PP_DYNAMIC_FILTER_RESTRICTIONS (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION) values (70, 215, 3, 'company_major_location.company_id');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2018, 0, 2015, 1, 0, 0, 041062, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041062_pcm_fp_search.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;