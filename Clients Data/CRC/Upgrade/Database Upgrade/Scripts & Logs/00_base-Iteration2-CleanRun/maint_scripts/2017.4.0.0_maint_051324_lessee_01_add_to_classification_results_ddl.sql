/*
||============================================================================
|| Application: PowerPlan  Disclosure Reports - ILR Descriptions
|| File Name:   maint_051324_lessee_01_add_to_classification_results_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/17/2018 Sarah Byers      Add new fields to store classfication results
||============================================================================
*/

alter table ls_ilr_classification_test add (
economic_life_percent number(22,8),
min_economic_life number(22,0),
max_economic_life number(22,0),
lease_term number(22,0),
fmv number(22,2),
npv number(22,2),
npv_percent number(22,8));

comment on column ls_ilr_classification_test.economic_life_percent is 'The percentage used for the classification test for Economic Life. Defaults to .75 when a value is not specified by the client.';
comment on column ls_ilr_classification_test.min_economic_life is 'The minimum economic life value based on the assets assigned to the ILR.';
comment on column ls_ilr_classification_test.max_economic_life is 'The maximum economic life value based on the assets assigned to the ILR.';
comment on column ls_ilr_classification_test.lease_term is 'The number of months in the lease term used to define the life of the ILR.';
comment on column ls_ilr_classification_test.fmv is 'The fair market value of the ILR.';
comment on column ls_ilr_classification_test.npv is 'The net present value of the ILR.';
comment on column ls_ilr_classification_test.npv_percent is 'The percentage used for the Net Present Value classfication test.  Defaults to .9.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5602, 0, 2017, 4, 0, 0, 51324, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051324_lessee_01_add_to_classification_results_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;