/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043464_reg_hist_lyr_2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/03/2015 Andrew Scott   populate the 2 newly created tables + 
||                                      backfill incremental process id to the 3 fp tables
||============================================================================
*/

insert into INCREMENTAL_PROCESS
   (INCREMENTAL_PROCESS_ID, FUNDING_WO_ID, REVISION)
   select ROWNUM, FUNDING_WO_ID, REVISION
     from (select distinct FUNDING_WO_ID, REVISION
             from FCST_DEPR_LEDGER_FP
           union
           select distinct FUNDING_WO_ID, REVISION
             from FCST_CPR_DEPR_FP
           union
           select distinct WORK_ORDER_ID, REVISION
             from INCREMENTAL_FP_RUN_LOG);

update FCST_DEPR_LEDGER_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select y.INCREMENTAL_PROCESS_ID
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update FCST_CPR_DEPR_FP x
set x.INCREMENTAL_PROCESS_ID = (
   select y.INCREMENTAL_PROCESS_ID
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update INCREMENTAL_FP_RUN_LOG x
set x.INCREMENTAL_PROCESS_ID = (
   select y.INCREMENTAL_PROCESS_ID
   from INCREMENTAL_PROCESS y
   where x.WORK_ORDER_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.WORK_ORDER_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

insert into INCREMENTAL_PROCESS_RUN_LOG
(INCREMENTAL_RUN_ID,
INCREMENTAL_PROCESS_ID,
BUDGET_VERSION_ID,
FCST_DEPR_VERSION_ID,
VERSION_ID,
DATE_RUN,
RUN_USER_ID)
select 
   INC_FP_RUN_ID,
   INCREMENTAL_PROCESS_ID,
   BUDGET_VERSION_ID,
   FCST_DEPR_VERSION_ID,
   VERSION_ID,
   DATE_RUN,
   RUN_USER_ID
from INCREMENTAL_FP_RUN_LOG;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2583, 0, 2015, 2, 0, 0, 043464, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043464_reg_hist_lyr_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;