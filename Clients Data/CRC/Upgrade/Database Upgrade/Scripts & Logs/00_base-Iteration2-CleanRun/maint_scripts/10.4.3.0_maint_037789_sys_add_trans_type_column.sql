SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037789_sys_add_trans_type_column.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 05/23/2014 Charlie Shilling maint_37789
||============================================================================
*/

declare
	procedure ADD_COLUMN(V_TABLE_NAME varchar2,
						 V_COL_NAME   varchar2,
						 V_DATATYPE   varchar2,
						 V_COMMENT    varchar2) is
	begin
		begin
			execute immediate 'alter table ' || V_TABLE_NAME || ' add ' || V_COL_NAME || ' ' ||
							  V_DATATYPE;
			DBMS_OUTPUT.PUT_LINE('Sucessfully added column ' || V_COL_NAME || ' to table ' ||
								 V_TABLE_NAME || '.');
		exception
			when others then
				if sqlcode = -1430 then
					--1430 is "column being added already exists in table", so we are good here
					DBMS_OUTPUT.PUT_LINE('Column ' || V_COL_NAME || ' already exists on table ' ||
										 V_TABLE_NAME || '. No action necessasry.');
				else
					RAISE_APPLICATION_ERROR(-20001,
											'Could not add column ' || V_COL_NAME || ' to ' ||
											V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
				end if;
		end;

		begin
			execute immediate 'comment on column ' || V_TABLE_NAME || '.' || V_COL_NAME || ' is ''' ||
							  V_COMMENT || '''';
			DBMS_OUTPUT.PUT_LINE('	Sucessfully added the comment to column ' || V_COL_NAME ||
								 ' to table ' || V_TABLE_NAME || '.');
		exception
			when others then
				RAISE_APPLICATION_ERROR(-20002,
										'Could not add comment to column ' || V_COL_NAME || ' to ' ||
										V_TABLE_NAME || '. SQL Error: ' || sqlerrm);
		end;
	end ADD_COLUMN;
begin
	--add trans_type
	ADD_COLUMN('gl_transaction',
			   'trans_type',
			   'number(22,0)',
			   'The trans_type used to generate the gl_account string.');
	ADD_COLUMN('arc_gl_transaction',
			   'trans_type',
			   'number(22,0)',
			   'The trans_type used to generate the gl_account string.');
end;
/

alter table GL_TRANSACTION ADD constraint R_GL_TRANSACTION2 FOREIGN KEY(TRANS_TYPE) REFERENCES JE_TRANS_TYPE(TRANS_TYPE);

alter table ARC_GL_TRANSACTION ADD constraint R_ARC_GL_TRANSACTION2 FOREIGN KEY(TRANS_TYPE) REFERENCES JE_TRANS_TYPE(TRANS_TYPE);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1164, 0, 10, 4, 3, 0, 37789, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037789_sys_add_trans_type_column.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;