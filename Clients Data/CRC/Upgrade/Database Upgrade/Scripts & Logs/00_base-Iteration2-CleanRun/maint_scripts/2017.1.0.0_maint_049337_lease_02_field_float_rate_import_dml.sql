/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049337_lease_02_field_float_rate_import_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

insert into pp_import_column(import_type_id, column_name, description, import_column_name, 
   is_required, processing_order, column_type, is_on_table)
select 
   (select import_type_id from pp_import_type where lower(import_table_name) = 'ls_import_float_rates') AS import_type_id,
   'lease_id' AS column_name,
   'Lease ID' AS description, 
   'lease_xlate' AS import_column_name,
   1 AS is_required, 
   1 AS processing_order,
   'number(22,0)' AS column_type,
   1 AS is_on_table
from dual
;

insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
select 
pit.import_template_id, max(field_id)+1, pit.import_type_id, 'lease_id', 
   (select import_lookup_id from pp_import_lookup where column_name = 'lease_id' and lookup_column_name = 'lease_number' and lookup_table_name = 'ls_lease')
from pp_import_template pit, pp_import_template_fields f
where pit.import_type_id = (select import_type_id from pp_import_type where lower(import_table_name) = 'ls_import_float_rates')
and pit.import_type_id = f.import_type_id
and pit.import_template_id = f.import_template_id
group by pit.import_template_id, pit.import_type_id;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
  (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
  SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
  (4018, 0, 2017, 1, 0, 0, 49337, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049337_lease_02_field_float_rate_import_dml.sql', 1,
  SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
  SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;