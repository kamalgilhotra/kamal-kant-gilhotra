/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041090_sys_short_date.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   11/06/2014 Anand Rajashekar    drop column short_date from pp_security_users
||========================================================================================
*/

Alter table PP_SECURITY_USERS
drop column SHORT_DATE;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (2009, 0, 2015, 1, 0, 0, 41090, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041090_sys_short_date.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;