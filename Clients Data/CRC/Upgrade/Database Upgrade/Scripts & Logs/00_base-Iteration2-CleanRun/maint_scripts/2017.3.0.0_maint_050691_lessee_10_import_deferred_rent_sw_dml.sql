/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_10_import_deferred_rent_sw_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/20/2018 Josh Sandler 	  Add deferred rent switch to import
||============================================================================
*/

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      (252,
             'deferred_rent_sw',
             'Deferred/Prepaid Rent',
             'deferred_rent_sw_xlate',
             0,
             1,
             'number(1)',
             'yes_no',
             1,
             'yes_no_id');

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'deferred_rent_sw',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (252,
             'deferred_rent_sw',
             78);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4545, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_10_import_deferred_rent_sw_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;