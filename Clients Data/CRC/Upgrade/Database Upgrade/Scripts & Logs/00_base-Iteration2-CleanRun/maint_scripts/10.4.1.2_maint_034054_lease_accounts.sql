/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034054_lease_accounts.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   12/04/2013 Kyle Peterson
||============================================================================
*/

alter table LS_ILR_ACCOUNT
   add (CAP_ASSET_ACCOUNT_ID number(22,0),
        ST_OBLIG_ACCOUNT_ID  number(22,0),
        LT_OBLIG_ACCOUNT_ID  number(22,0),
        AP_ACCOUNT_ID        number(22,0));

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK8
       foreign key (CAP_ASSET_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK9
       foreign key (ST_OBLIG_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK10
       foreign key (LT_OBLIG_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_ILR_ACCOUNT
   add constraint LS_ILR_ACCOUNT_FK11
       foreign key (AP_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

comment on column LS_ILR_ACCOUNT.CAP_ASSET_ACCOUNT_ID is 'The capital asset account.';
comment on column LS_ILR_ACCOUNT.ST_OBLIG_ACCOUNT_ID is 'The short term obligation account.';
comment on column LS_ILR_ACCOUNT.LT_OBLIG_ACCOUNT_ID is 'The long term obligation account.';
comment on column LS_ILR_ACCOUNT.AP_ACCOUNT_ID is 'The accounts payable account.';

alter table LS_ILR_GROUP
   add (CAP_ASSET_ACCOUNT_ID number(22,0),
        ST_OBLIG_ACCOUNT_ID  number(22,0),
        LT_OBLIG_ACCOUNT_ID  number(22,0),
        AP_ACCOUNT_ID        number(22,0));

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK8
       foreign key (CAP_ASSET_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK9
       foreign key (ST_OBLIG_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK10
       foreign key (LT_OBLIG_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

alter table LS_ILR_GROUP
   add constraint LS_ILR_GROUP_FK11
       foreign key (AP_ACCOUNT_ID)
       references GL_ACCOUNT(GL_ACCOUNT_ID);

comment on column LS_ILR_GROUP.CAP_ASSET_ACCOUNT_ID is 'The capital asset account.';
comment on column LS_ILR_GROUP.ST_OBLIG_ACCOUNT_ID is 'The short term obligation account.';
comment on column LS_ILR_GROUP.LT_OBLIG_ACCOUNT_ID is 'The long term obligation account.';
comment on column LS_ILR_GROUP.AP_ACCOUNT_ID is 'The accounts payable account.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (779, 0, 10, 4, 1, 2, 34054, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_034054_lease_accounts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;