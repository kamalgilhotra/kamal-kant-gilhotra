/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044456_sys_preview_je_3_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 08/06/2015 Andrew Scott   New preview JE feature (work with 
||                                      integration mgr + ssp pbd version validation)
||============================================================================
*/


INSERT INTO pp_job_executable
(job_type, job_name, executable, category, arguments, optional_file_path)
SELECT 
'SSP', 'Preview Journal Entries', 'ssp_preview_je.exe', 'Month End Processes',
TO_CLOB('{"parms": [{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<false>","label":"Preview Accruals","required":"true"},{"type":"boolean2","value":"<false>","label":"Preview AFUDC","required":"true"},{"type":"boolean3","value":"<false>","label":"Preview ARO","required":"true"},{"type":"boolean4","value":"<false>","label":"Preview Lease Depreciation","required":"true"},{"type":"boolean5","value":"<false>","label":"Preview Depreciation","required":"true"}]}'),
null
FROM DUAL;

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ppprojct_custom.pbd',
  '2015.2.0.0'
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_preview_je.exe';

INSERT INTO pp_custom_pbd_versions
(process_id, pbd_name, version)
SELECT
  process_id,
  'ssp_preview_je_custom.pbd',
  '2015.2.0.0'
FROM pp_processes
WHERE lower(trim(executable_file)) = 'ssp_preview_je.exe';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2753, 0, 2015, 2, 0, 0, 044456, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044456_sys_preview_je_3_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;