/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038738_lease_drill_to_component.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 06/24/2014 Ryan Oliveria    Added Drill to Component Action
||============================================================================
*/

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values
   ((select max(ID) + 1 from PPBASE_ACTIONS_WINDOWS), 'LESSEE', 'drill_component', 'Drill to Comp.', 5,
    'ue_drillcomponent');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1228, 0, 10, 4, 3, 0, 38738, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038738_lease_drill_to_component.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
