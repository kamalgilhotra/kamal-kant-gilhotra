/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049461_lessee_01_mla_import_update_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/12/2017 Shane "C" Ward    Updates to Lessee ILR Import
||============================================================================
*/
ALTER TABLE ls_import_lease add       specialized_asset NUMBER(22,0);
ALTER TABLE ls_import_lease add         specialized_asset_xlate VARCHAR2(35);
ALTER TABLE ls_import_lease add       intent_to_purchase_xlate VARCHAR2(35);
ALTER TABLE ls_import_lease add       intent_to_purchase NUMBER(22,0);
ALTER TABLE ls_import_lease add       sublease          NUMBER(22,0);
ALTER TABLE ls_import_lease add       sublease_xlate VARCHAR2(35);
ALTER TABLE ls_import_lease add       sublease_lease_id NUMBER(22,0);
ALTER TABLE ls_import_lease add       sublease_lease_xlate VARCHAR2(35);

COMMENT ON COLUMN ls_import_lease.specialized_asset IS 'Switch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN ls_import_lease.specialized_asset_xlate IS 'Translation for witch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN ls_import_lease.intent_to_purchase_xlate IS 'Translation for switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN ls_import_lease.intent_to_purchase IS 'Switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN ls_import_lease.sublease IS 'Switch indicating whether lease is a Sublease';
COMMENT ON COLUMN ls_import_lease.sublease_xlate IS 'Translation for switch indicating whether lease a Sublease';
COMMENT ON COLUMN ls_import_lease.sublease_lease_id IS 'ID of Lessor lease that is the sublease of this Lease';
COMMENT ON COLUMN ls_import_lease.sublease_lease_xlate IS 'Translation for ID of Lessor lease that is the sublease of this Lease';

ALTER TABLE ls_import_lease_archive add       specialized_asset NUMBER(22,0);
ALTER TABLE ls_import_lease_archive add         specialized_asset_xlate VARCHAR2(35);
ALTER TABLE ls_import_lease_archive add       intent_to_purchase_xlate VARCHAR2(35);
ALTER TABLE ls_import_lease_archive add       intent_to_purchase NUMBER(22,0);
ALTER TABLE ls_import_lease_archive add       sublease          NUMBER(22,0);
ALTER TABLE ls_import_lease_archive add       sublease_xlate VARCHAR2(35);
ALTER TABLE ls_import_lease_archive add       sublease_lease_id NUMBER(22,0);
ALTER TABLE ls_import_lease_archive add       sublease_lease_xlate VARCHAR2(35);


COMMENT ON COLUMN ls_import_lease_archive.specialized_asset IS 'Switch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN ls_import_lease_archive.specialized_asset_xlate IS 'Translation for witch indicating whether lease contains a specialized asset';
COMMENT ON COLUMN ls_import_lease_archive.intent_to_purchase_xlate IS 'Translation for switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN ls_import_lease_archive.intent_to_purchase IS 'Switch indicating whether Lessee intends to Purchase at end of Lease';
COMMENT ON COLUMN ls_import_lease_archive.sublease IS 'Switch indicating whether lease is a Sublease';
COMMENT ON COLUMN ls_import_lease_archive.sublease_xlate IS 'Translation for switch indicating whether lease a Sublease';
COMMENT ON COLUMN ls_import_lease_archive.sublease_lease_id IS 'ID of Lessor lease that is the sublease of this Lease';
COMMENT ON COLUMN ls_import_lease_archive.sublease_lease_xlate IS 'Translation for ID of Lessor lease that is the sublease of this Lease';

ALTER TABLE ls_lease_options
  ADD CONSTRAINT fk_ls_lease_sublease FOREIGN KEY (
    sublease_lease_id
  ) REFERENCES lsr_lease (
    lease_id
  );
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3936, 0, 2017, 1, 0, 0, 49461, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049461_lessee_01_mla_import_update_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;