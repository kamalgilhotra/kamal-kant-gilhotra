/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045650_reg_case_alloc_dyn_target_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2016.1.0.0 04/26/2016 Sarah Byers	   populate reg_case_alloc_dyn_target
||============================================================================
*/

insert into reg_case_alloc_dyn_target (
	reg_case_id, reg_allocator_id, reg_alloc_category_id, reg_alloc_target_id, target_used)
select reg_case_id, reg_allocator_id, category_id, reg_alloc_target_id, 1 
  from (
		select t.reg_case_id, a.reg_allocator_id, c.category_id, t.reg_alloc_target_id
		  from reg_case_target_forward t, reg_allocator a, reg_case_category_allocator b,
				 (
					select c.step_order step_order, c.reg_alloc_category_id category_id, 
							 p.reg_alloc_category_id parent_category_id,
							 c.reg_case_id
					  from reg_case_alloc_steps c, reg_case_alloc_steps p
					 where c.reg_case_id = p.reg_case_id
						and c.step_order = p.step_order + 1
						and c.step_order >= 2
				 ) c
		 where t.reg_case_id = c.reg_case_id
			and c.parent_category_id = t.reg_alloc_category_id
			and a.factor_type = 3
			and b.reg_case_id = t.reg_case_id
			and b.reg_allocator_id = a.reg_allocator_id
			and b.reg_alloc_category_id = c.parent_category_id
		minus
		select reg_case_id, reg_allocator_id, reg_alloc_category_id, reg_alloc_target_id
		  from reg_case_alloc_dyn_target);

insert into reg_jur_alloc_dyn_target (
	reg_jur_template_id, reg_allocator_id, reg_alloc_category_id, reg_alloc_target_id, target_used)
select reg_jur_template_id, reg_allocator_id, category_id, reg_alloc_target_id, 1 
  from (
		select t.reg_jur_template_id, a.reg_allocator_id, c.category_id, t.reg_alloc_target_id
		  from reg_jur_target_forward t, reg_allocator a, reg_jur_category_allocator b,
				 (
					select c.step_order step_order, c.reg_alloc_category_id category_id, 
							 p.reg_alloc_category_id parent_category_id,
							 c.reg_jur_template_id reg_jur_template_id
					  from reg_jur_alloc_steps c, reg_jur_alloc_steps p
					 where c.reg_jur_template_id = p.reg_jur_template_id
						and c.step_order = p.step_order + 1
						and c.step_order >= 2
				 ) c
		 where t.reg_jur_template_id = c.reg_jur_template_id
			and c.parent_category_id = t.reg_alloc_category_id
			and a.factor_type = 3
			and b.reg_jur_template_id = t.reg_jur_template_id
			and b.reg_allocator_id = a.reg_allocator_id
			and b.reg_alloc_category_id = c.parent_category_id
		minus
		select reg_jur_template_id, reg_allocator_id, reg_alloc_category_id, reg_alloc_target_id
		  from reg_jur_alloc_dyn_target);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3175, 0, 2016, 1, 0, 0, 45650, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045650_reg_case_alloc_dyn_target_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;