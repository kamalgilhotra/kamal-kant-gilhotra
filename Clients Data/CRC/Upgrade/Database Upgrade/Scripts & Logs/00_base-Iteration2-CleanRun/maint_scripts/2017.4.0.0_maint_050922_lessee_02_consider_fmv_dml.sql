/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050922_lessee_02_consider_fmv_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 04/20/2018 Josh Sandler     Populate new consider_fmv field with default
||============================================================================
*/

UPDATE ls_fasb_cap_type_sob_map
SET consider_fmv = 1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4863, 0, 2017, 4, 0, 0, 50922, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050922_lessee_02_consider_fmv_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;