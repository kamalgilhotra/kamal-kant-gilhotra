/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi4.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/03/2013 Brandon Beck   Point release
||============================================================================
*/

create table LS_IMPORT_ASSET
(
 IMPORT_RUN_ID              number(22,0) not null,
 LINE_ID                    number(22,0) not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 LEASED_ASSET_NUMBER        varchar2(35),
 DESCRIPTION                varchar2(35),
 LONG_DESCRIPTION           varchar2(35),
 QUANTITY                   number(22,2),
 FMV                        number(22,2),
 COMPANY_XLATE              varchar2(254),
 COMPANY_ID                 number(22,0),
 BUS_SEGMENT_XLATE          varchar2(254),
 BUS_SEMGENT_ID             number(22,0),
 UTILITY_ACCOUNT_XLATE      varchar2(254),
 UTILITY_ACCOUNT_ID         number(22,0),
 SUB_ACCOUNT_XLATE          varchar2(254),
 SUB_ACCOUNT_ID             number(22,0),
 RETIREMENT_UNIT_XLATE      varchar2(254),
 RETIREMENT_UNIT_ID         number(22,0),
 PROPERTY_GROUP_XLATE       varchar2(254),
 PROPERTY_GROUP_ID          number(22,0),
 WORK_ORDER_XLATE           varchar2(254),
 WORK_ORDER_ID              number(22,0),
 ASSET_LOCATION_XLATE       varchar2(254),
 ASSET_LOCATION_ID          number(22,0),
 GUARANTEED_RESIDUAL_AMOUNT number(22,2),
 EXPECTED_LIFE              number(22,0),
 ECONOMIC_LIFE              number(22,0),
 NOTES                      varchar2(4000),
 CLASS_CODE_XLATE1          varchar2(254),
 CLASS_CODE_ID1             number(22,0),
 CLASS_CODE_VALUE1          varchar2(254),
 CLASS_CODE_XLATE2          varchar2(254),
 CLASS_CODE_ID2             number(22,0),
 CLASS_CODE_VALUE2          varchar2(254),
 CLASS_CODE_XLATE3          varchar2(254),
 CLASS_CODE_ID3             number(22,0),
 CLASS_CODE_VALUE3          varchar2(254),
 CLASS_CODE_XLATE4          varchar2(254),
 CLASS_CODE_ID4             number(22,0),
 CLASS_CODE_VALUE4          varchar2(254),
 CLASS_CODE_XLATE5          varchar2(254),
 CLASS_CODE_ID5             number(22,0),
 CLASS_CODE_VALUE5          varchar2(254),
 CLASS_CODE_XLATE6          varchar2(254),
 CLASS_CODE_ID6             number(22,0),
 CLASS_CODE_VALUE6          varchar2(254),
 CLASS_CODE_XLATE7          varchar2(254),
 CLASS_CODE_ID7             number(22,0),
 CLASS_CODE_VALUE7          varchar2(254),
 CLASS_CODE_XLATE8          varchar2(254),
 CLASS_CODE_ID8             number(22,0),
 CLASS_CODE_VALUE8          varchar2(254),
 CLASS_CODE_XLATE9          varchar2(254),
 CLASS_CODE_ID9             number(22,0),
 CLASS_CODE_VALUE9          varchar2(254),
 CLASS_CODE_XLATE10         varchar2(254),
 CLASS_CODE_ID10            number(22,0),
 CLASS_CODE_VALUE10         varchar2(254),
 CLASS_CODE_XLATE11         varchar2(254),
 CLASS_CODE_ID11            number(22,0),
 CLASS_CODE_VALUE11         varchar2(254),
 CLASS_CODE_XLATE12         varchar2(254),
 CLASS_CODE_ID12            number(22,0),
 CLASS_CODE_VALUE12         varchar2(254),
 CLASS_CODE_XLATE13         varchar2(254),
 CLASS_CODE_ID13            number(22,0),
 CLASS_CODE_VALUE13         varchar2(254),
 CLASS_CODE_XLATE14         varchar2(254),
 CLASS_CODE_ID14            number(22,0),
 CLASS_CODE_VALUE14         varchar2(254),
 CLASS_CODE_XLATE15         varchar2(254),
 CLASS_CODE_ID15            number(22,0),
 CLASS_CODE_VALUE15         varchar2(254),
 CLASS_CODE_XLATE16         varchar2(254),
 CLASS_CODE_ID16            number(22,0),
 CLASS_CODE_VALUE16         varchar2(254),
 CLASS_CODE_XLATE17         varchar2(254),
 CLASS_CODE_ID17            number(22,0),
 CLASS_CODE_VALUE17         varchar2(254),
 CLASS_CODE_XLATE18         varchar2(254),
 CLASS_CODE_ID18            number(22,0),
 CLASS_CODE_VALUE18         varchar2(254),
 CLASS_CODE_XLATE19         varchar2(254),
 CLASS_CODE_ID19            number(22,0),
 CLASS_CODE_VALUE19         varchar2(254),
 CLASS_CODE_XLATE20         varchar2(254),
 CLASS_CODE_ID20            number(22,0),
 CLASS_CODE_VALUE20         varchar2(254),
 LOADED                     number(22,0),
 IS_MODIFIED                number(22,0),
 ERROR_MESSAGE              varchar2(4000)
);

create table LS_IMPORT_INVOICE
(
 IMPORT_RUN_ID      number(22,0) not null,
 LINE_ID            number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 COMPANY_XLATE      varchar2(254),
 COMPANY_ID         number(22,0),
 LEASE_XLATE        varchar2(254),
 LEASE_ID           number(22,0),
 GL_POSTING_MO_YR   varchar2(254),
 VENDOR_XLATE       varchar2(254),
 VENDOR_ID          number(22,0),
 PAYMENT_TYPE_XLATE varchar2(254),
 PAYMENT_TYPE_ID    number(22,0),
 AMOUNT             number(22,2),
 LS_ASSET_XLATE     varchar2(254),
 LS_ASSET_ID        number(22,0),
 LOADED             number(22,0),
 IS_MODIFIED        number(22,0),
 ERROR_MESSAGE      varchar2(4000)
);

create table LS_IMPORT_ILR
(
 IMPORT_RUN_ID           number(22,0) not null,
 LINE_ID                 number(22,0) not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 ILR_NUMBER              varchar2(35),
 LEASE_XLATE             varchar2(254),
 LEASE_ID                number(22,0),
 COMPANY_XLATE           varchar2(254),
 COMPANY_ID              number(22,0),
 EST_IN_SVC_DATE         varchar2(254),
 ILR_GROUP_XLATE         varchar2(254),
 ILR_GROUP_ID            number(22,0),
 WORK_FLOW_TYPE_XLATE    varchar2(254),
 WORK_FLOW_TYPE_ID       number(22,0),
 EXTERNAL_ILR            varchar2(35),
 NOTES                   varchar2(4000),
 INCEPTION_AIR           number(22,8),
 PURCHASE_OPTION_TYPE_ID varchar2(35),
 PURCHASE_OPTION_AMT     number(22,2),
 RENEWAL_OPTION_TYPE_ID  varchar2(35),
 CANCELABLE_TYPE_ID      varchar2(35),
 ITC_SW                  varchar2(35),
 PARTIAL_RETIRE_SW       varchar2(35),
 SUBLET_SW               varchar2(35),
 MUNI_BO_SW              varchar2(35),
 LEASE_CAP_TYPE_XLATE    varchar2(254),
 LEASE_CAP_TYPE_ID       number(22,0),
 TERMINATION_AMT         number(22,2),
 PAYMENT_TERM_ID         number(22,0),
 PAYMENT_TERM_DATE       varchar2(254),
 PAYMENT_FREQ_ID         varchar2(35),
 NUMBER_OF_TERMS         number(22,0),
 PAID_AMOUNT             number(22,2),
 EST_EXECUTORY_COST      number(22,2),
 CONTINGENT_AMOUNT       number(22,2),
 CLASS_CODE_XLATE1       varchar2(254),
 CLASS_CODE_ID1          number(22,0),
 CLASS_CODE_VALUE1       varchar2(254),
 CLASS_CODE_XLATE2       varchar2(254),
 CLASS_CODE_ID2          number(22,0),
 CLASS_CODE_VALUE2       varchar2(254),
 CLASS_CODE_XLATE3       varchar2(254),
 CLASS_CODE_ID3          number(22,0),
 CLASS_CODE_VALUE3       varchar2(254),
 CLASS_CODE_XLATE4       varchar2(254),
 CLASS_CODE_ID4          number(22,0),
 CLASS_CODE_VALUE4       varchar2(254),
 CLASS_CODE_XLATE5       varchar2(254),
 CLASS_CODE_ID5          number(22,0),
 CLASS_CODE_VALUE5       varchar2(254),
 CLASS_CODE_XLATE6       varchar2(254),
 CLASS_CODE_ID6          number(22,0),
 CLASS_CODE_VALUE6       varchar2(254),
 CLASS_CODE_XLATE7       varchar2(254),
 CLASS_CODE_ID7          number(22,0),
 CLASS_CODE_VALUE7       varchar2(254),
 CLASS_CODE_XLATE8       varchar2(254),
 CLASS_CODE_ID8          number(22,0),
 CLASS_CODE_VALUE8       varchar2(254),
 CLASS_CODE_XLATE9       varchar2(254),
 CLASS_CODE_ID9          number(22,0),
 CLASS_CODE_VALUE9       varchar2(254),
 CLASS_CODE_XLATE10      varchar2(254),
 CLASS_CODE_ID10         number(22,0),
 CLASS_CODE_VALUE10      varchar2(254),
 CLASS_CODE_XLATE11      varchar2(254),
 CLASS_CODE_ID11         number(22,0),
 CLASS_CODE_VALUE11      varchar2(254),
 CLASS_CODE_XLATE12      varchar2(254),
 CLASS_CODE_ID12         number(22,0),
 CLASS_CODE_VALUE12      varchar2(254),
 CLASS_CODE_XLATE13      varchar2(254),
 CLASS_CODE_ID13         number(22,0),
 CLASS_CODE_VALUE13      varchar2(254),
 CLASS_CODE_XLATE14      varchar2(254),
 CLASS_CODE_ID14         number(22,0),
 CLASS_CODE_VALUE14      varchar2(254),
 CLASS_CODE_XLATE15      varchar2(254),
 CLASS_CODE_ID15         number(22,0),
 CLASS_CODE_VALUE15      varchar2(254),
 CLASS_CODE_XLATE16      varchar2(254),
 CLASS_CODE_ID16         number(22,0),
 CLASS_CODE_VALUE16      varchar2(254),
 CLASS_CODE_XLATE17      varchar2(254),
 CLASS_CODE_ID17         number(22,0),
 CLASS_CODE_VALUE17      varchar2(254),
 CLASS_CODE_XLATE18      varchar2(254),
 CLASS_CODE_ID18         number(22,0),
 CLASS_CODE_VALUE18      varchar2(254),
 CLASS_CODE_XLATE19      varchar2(254),
 CLASS_CODE_ID19         number(22,0),
 CLASS_CODE_VALUE19      varchar2(254),
 CLASS_CODE_XLATE20      varchar2(254),
 CLASS_CODE_ID20         number(22,0),
 CLASS_CODE_VALUE20      varchar2(254),
 LOADED                  number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);


-- 30034
create table LS_RENT_BUCKET_ADMIN
(
 RENT_TYPE      varchar2(35) not null,
 BUCKET_NUMBER  number(2,0) not null,
 BUCKET_NAME    varchar2(35),
 STATUS_CODE_ID number(1,0),
 TIME_STAMP     date,
 USER_ID        varchar2(18)
);

alter table LS_RENT_BUCKET_ADMIN
   add constraint LS_RENT_BUCKET_ADMIN_PK
       primary key (RENT_TYPE, BUCKET_NUMBER)
       using index tablespace PWRPLANT_IDX;

--31626
--TO SET UP LESSEE SHORTCUTS for Reports
--Insert into PP_MYPP_TASK
insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Lessee Reporting', 'Lessee Reporting',
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee'), 0, null, null);

insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Lessee Reports', 'Lessee Reports',
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Reporting'), 1, null, null);

--Insert into PP_MYPP_TASK_STEP

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Reporting'), 1, null, null,
    'w_asset_main', 'm_asset_main.m_application.m_lease', 'clicked', null);

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Reports'), 1, null, null,
    'w_ls_center_main', 'dw_ls_report', 'clicked', null);

--Adding line into pp_report_type for lessee
insert into PP_REPORT_TYPE
   (REPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, SORT_ORDER)
values
   (306, sysdate, 'PWRPLANT', 'Main - Lessee', '');

--Associate type with task
insert into PP_MYPP_REPORT_TYPE_TASK
   (REPORT_TYPE_ID, TASK_ID, TIME_STAMP, USER_ID)
values
   ((select REPORT_TYPE_ID from PP_REPORT_TYPE where DESCRIPTION = 'Main - Lessee'),
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Reports'), sysdate, 'PWRPLANT');

--Update report types for lessee

update PP_REPORTS
   set REPORT_TYPE = 'Main - Lessee',
       REPORT_TYPE_ID =
        (select REPORT_TYPE_ID from PP_REPORT_TYPE where DESCRIPTION = 'Main - Lessee')
 where SUBSYSTEM = 'Lessee';

create global temporary table LS_RETIREMENT_NEW_TERMS
(
 LS_ASSET_ID          number(22,0),
 PAYMENT_TERM_ID      number(22,0),
 PAYMENT_TERM_TYPE_ID number(22,0),
 PAYMENT_TERM_DATE    date,
 PAYMENT_FREQ_ID      number(22,0),
 NUMBER_OF_TERMS      number(22,0),
 PAID_AMOUNT          number(22,2),
 EST_EXECUTORY_COST   number(22,2),
 CONTINGENT_AMOUNT    number(22,2)
) on commit preserve rows;


--Insert into pp_mypp_task
insert into PP_MYPP_TASK
   (TASK_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID, ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT,
    HELP_WINDOW_NAME)
   select max(TASK_ID) + 1, 'MLA Approval', 'Lessee > MLA Approval', null, 1, null, null
     from PP_MYPP_TASK;

insert into PP_MYPP_TASK
   (TASK_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID, ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT,
    HELP_WINDOW_NAME)
   select max(TASK_ID) + 1, 'ILR Approval', 'Lessee > ILR Approval', null, 1, null, null
     from PP_MYPP_TASK;

--Insert into pp_mypp_task_step
--(Click dw_approve_mla or dw_approve_assets)
insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
   select TASK_ID, 1, 'w_ls_center_main', null, 'opensheet', null
     from PP_MYPP_TASK
    where DESCRIPTION = 'MLA Approval';

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
   select TASK_ID, 2, 'w_ls_center_main', 'dw_approve_mla', 'clicked', null
     from PP_MYPP_TASK
    where DESCRIPTION = 'MLA Approval';

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
   select TASK_ID, 1, 'w_ls_center_main', null, 'opensheet', null
     from PP_MYPP_TASK
    where DESCRIPTION = 'ILR Approval';

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
   select TASK_ID, 2, 'w_ls_center_main', 'dw_approve_assets', 'clicked', null
     from PP_MYPP_TASK
    where DESCRIPTION = 'ILR Approval';

--Set task ID on pp_mypp_approval_control
update PP_MYPP_APPROVAL_CONTROL
   set TASK_ID =
        (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'MLA Approval')
 where LABEL_ONE = 'MLA to Approve';

update PP_MYPP_APPROVAL_CONTROL
   set TASK_ID =
        (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'ILR Approval')
 where LABEL_ONE = 'ILR to Approve';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (564, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi4.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
