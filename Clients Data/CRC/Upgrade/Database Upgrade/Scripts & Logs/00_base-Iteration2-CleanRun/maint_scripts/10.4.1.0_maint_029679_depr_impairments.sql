/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029679_depr_impairments.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/12/2013 Brandon Beck   Point Release
||============================================================================
*/

alter table DEPR_LEDGER
   add (IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2) default 0,
        IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2) default 0);

alter table CPR_DEPR
   add (IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2) default 0,
        IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2) default 0);

alter table FCST_DEPR_LEDGER
   add (IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2) default 0,
        IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2) default 0);

alter table FCST_CPR_DEPR
   add (IMPAIRMENT_ASSET_ACTIVITY_SALV number(22,2) default 0,
        IMPAIRMENT_ASSET_BEGIN_BALANCE number(22,2) default 0);

--
-- ONLY updates current open month (depr_ledger_status = 8)
--

declare
   MY_VAR  number(22, 0);
   MY_VAR2 number(22, 0);
   MY_VAR3 number(22, 0);
   SQLS    varchar2(4000);

begin
   select count(1) into MY_VAR from BOOK_SUMMARY where BOOK_SUMMARY_TYPE = 'ASSET IMPAIRMENT 1';
   select count(1) into MY_VAR2 from BOOK_SUMMARY where BOOK_SUMMARY_TYPE = 'ASSET IMPAIRMENT 2';
   select count(1) into MY_VAR3 from BOOK_SUMMARY where BOOK_SUMMARY_TYPE = 'ASSET IMPAIRMENT 3';

   if MY_VAR <> 0 then
      select BOOK_SUMMARY_ID
        into MY_VAR
        from BOOK_SUMMARY
       where BOOK_SUMMARY_TYPE = 'ASSET IMPAIRMENT 1';

      SQLS := 'update depr_ledger dl set impairment_asset_activity_salv = ( select sum(nvl(a.basis_' ||
              TO_CHAR(MY_VAR) ||
              ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number = to_number(to_char(dl.gl_post_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 1 and depr_ledger_status = 8';
      execute immediate SQLS;

      SQLS := 'update depr_ledger dl set impairment_asset_begin_balance = ( select sum(nvl(a.basis_' ||
              TO_CHAR(MY_VAR) ||
              ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number < to_number(to_char(dl.gl_post_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 1 and depr_ledger_status = 8';
      execute immediate SQLS;

      SQLS := 'update cpr_depr dl set impairment_asset_activity_salv = ( select sum(nvl(a.basis_' ||
              TO_CHAR(MY_VAR) ||
              ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and a.asset_id = dl.asset_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number = to_number(to_char(dl.gl_posting_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 1 and exists (select 1 from depr_ledger d where d.depr_group_id = dl.depr_group_id and dl.gl_posting_mo_yr = d.gl_post_mo_yr and d.depr_ledger_status = 8)';
      execute immediate SQLS;

      SQLS := 'update cpr_depr dl set impairment_asset_begin_balance = ( select sum(nvl(a.basis_' ||
              TO_CHAR(MY_VAR) ||
              ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and a.asset_id = dl.asset_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number < to_number(to_char(dl.gl_posting_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 1 and exists (select 1 from depr_ledger d where d.depr_group_id = dl.depr_group_id and dl.gl_posting_mo_yr = d.gl_post_mo_yr and d.depr_ledger_status = 8)';
      execute immediate SQLS;
   end if;

   if MY_VAR2 <> 0 then
      select BOOK_SUMMARY_ID
        into MY_VAR2
        from BOOK_SUMMARY
       where BOOK_SUMMARY_TYPE = 'ASSET IMPAIRMENT 2';

      SQLS := 'update depr_ledger dl set impairment_asset_activity_salv = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR2) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number = to_number(to_char(dl.gl_post_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 2 and depr_ledger_status = 8';
      execute immediate SQLS;

      SQLS := 'update depr_ledger dl set impairment_asset_begin_balance = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR2) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number < to_number(to_char(dl.gl_post_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 2 and depr_ledger_status = 8';
      execute immediate SQLS;

      SQLS := 'update cpr_depr dl set impairment_asset_activity_salv = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR2) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and a.asset_id = dl.asset_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number = to_number(to_char(dl.gl_posting_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 2 and exists (select 1 from depr_ledger d where d.depr_group_id = dl.depr_group_id and dl.gl_posting_mo_yr = d.gl_post_mo_yr and d.depr_ledger_status = 8)';
      execute immediate SQLS;

      SQLS := 'update cpr_depr dl set impairment_asset_begin_balance = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR2) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and a.asset_id = dl.asset_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number < to_number(to_char(dl.gl_posting_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 2 and exists (select 1 from depr_ledger d where d.depr_group_id = dl.depr_group_id and dl.gl_posting_mo_yr = d.gl_post_mo_yr and d.depr_ledger_status = 8)';
      execute immediate SQLS;
   end if;

   if MY_VAR3 <> 0 then
      select BOOK_SUMMARY_ID
        into MY_VAR3
        from BOOK_SUMMARY
       where BOOK_SUMMARY_TYPE = 'ASSET IMPAIRMENT 3';

      SQLS := 'update depr_ledger dl set impairment_asset_activity_salv = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR3) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number = to_number(to_char(dl.gl_post_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 3 and depr_ledger_status = 8';
      execute immediate SQLS;

      SQLS := 'update depr_ledger dl set impairment_asset_begin_balance = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR3) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number < to_number(to_char(dl.gl_post_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 3 and depr_ledger_status = 8';
      execute immediate SQLS;

      SQLS := 'update cpr_depr dl set impairment_asset_activity_salv = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR3) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and a.asset_id = dl.asset_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number = to_number(to_char(dl.gl_posting_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 3 and exists (select 1 from depr_ledger d where d.depr_group_id = dl.depr_group_id and dl.gl_posting_mo_yr = d.gl_post_mo_yr and d.depr_ledger_status = 8)';
      execute immediate SQLS;

      SQLS := 'update cpr_depr dl set impairment_asset_begin_balance = ( select sum(nvl(a.basis_' || TO_CHAR(MY_VAR3) || ', 0)) from cpr_activity aa, cpr_act_basis a, cpr_ledger l where aa.asset_id = l.asset_id and l.depr_group_id = dl.depr_group_id and a.asset_id = dl.asset_id and aa.asset_id = a.asset_id and aa.asset_activity_id = a.asset_activity_id and aa.month_number < to_number(to_char(dl.gl_posting_mo_yr, ''yyyymm''))) where dl.set_of_books_id = 3 and exists (select 1 from depr_ledger d where d.depr_group_id = dl.depr_group_id and dl.gl_posting_mo_yr = d.gl_post_mo_yr and d.depr_ledger_status = 8)';
      execute immediate SQLS;
   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (403, 0, 10, 4, 1, 0, 29679, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029679_depr_impairments.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
