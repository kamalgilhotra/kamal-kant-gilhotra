/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042935_pcm_auth_review_link_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/16/2015 Sarah Byers      Links for Auth/Review workspaces
||============================================================================
*/

-- PCM Maint workspaces links
insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_workspace_identifier)
select module, workspace_identifier, 1, decode(substr(workspace_identifier,1,2),'fp', 'wo_search', 'wo', 'fp_search')
  from ppbase_workspace
 where workspace_uo_name = 'uo_pcm_auth_wksp';

insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_window, linked_window_label, linked_window_opensheet)
select module, workspace_identifier, 2, 'w_budget_select_tabs', 'Search (Programs)', 1
  from ppbase_workspace
 where workspace_uo_name = 'uo_pcm_auth_wksp';

insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_window, linked_window_label, linked_window_opensheet)
select module, workspace_identifier, 3, 'w_task_select_tabs', 'Search (Tasks)', 1
  from ppbase_workspace
 where workspace_uo_name = 'uo_pcm_auth_wksp';

insert into ppbase_workspace_links (
	module, workspace_identifier, link_order, linked_workspace_identifier)
select module, workspace_identifier, 4, 'rpt_reports'
  from ppbase_workspace
 where workspace_uo_name = 'uo_pcm_auth_wksp';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2303, 0, 2015, 1, 0, 0, 042935, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042935_pcm_auth_review_link_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;