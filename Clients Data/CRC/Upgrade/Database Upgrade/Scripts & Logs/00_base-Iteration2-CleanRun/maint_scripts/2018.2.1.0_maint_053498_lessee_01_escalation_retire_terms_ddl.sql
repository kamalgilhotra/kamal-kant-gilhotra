/*
||============================================================================
|| Application: PowerPlan
|| File Name:   2018.2.1.0_maint_053498_lessee_01_escalation_retire_terms_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.1.0 04/29/2019 C Yura            Add escalation fields to new retire terms table
||============================================================================
*/

alter table ls_retirement_new_terms add ESCALATION NUMBER(22,0); 
   
alter table ls_retirement_new_terms add ESCALATION_FREQ_ID NUMBER(22,0); 
   
alter table ls_retirement_new_terms add  ESCALATION_PCT NUMBER(22,12);   

COMMENT ON COLUMN ls_retirement_new_terms.escalation IS 'The Variable Payment ID of VP based escalation or null for fixed escalation.';
COMMENT ON COLUMN ls_retirement_new_terms.escalation_freq_id IS 'The frequency that the escalation is compounded';
COMMENT ON COLUMN ls_retirement_new_terms.escalation_pct IS 'The escalation percent for a fixed escalation. Stored as a decimal.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (17443, 0, 2018, 2, 1, 0, 53498, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_053498_lessee_01_escalation_retire_terms_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;