/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029599_pwrtax_b.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/10/2014 Julia Breuer
||============================================================================
*/

--
-- Delete the Tax Expensing system options.  They are no longer used.
--
delete from PPBASE_SYSTEM_OPTIONS_MODULE where SYSTEM_OPTION_ID in ('Reverse Tax Expensing Months', 'Forward Tax Expensing Months');
delete from PPBASE_SYSTEM_OPTIONS_VALUES where SYSTEM_OPTION_ID in ('Reverse Tax Expensing Months', 'Forward Tax Expensing Months');
delete from PPBASE_SYSTEM_OPTIONS where SYSTEM_OPTION_ID in ('Reverse Tax Expensing Months', 'Forward Tax Expensing Months');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1021, 0, 10, 4, 2, 0, 29599, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029599_pwrtax_b.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;