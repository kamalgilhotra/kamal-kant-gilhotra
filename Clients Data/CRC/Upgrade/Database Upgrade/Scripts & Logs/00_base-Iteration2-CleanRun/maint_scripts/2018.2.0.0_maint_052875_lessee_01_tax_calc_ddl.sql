 /*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052875_lessee_01_tax_calc_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 02/05/2018 Sarah Byers    Moving tax calc to schedule calc
||============================================================================
*/

-- LS_ASSET_SCHEDULE_TAX - will store tax results from schedule build
create table ls_asset_schedule_tax (
ls_asset_id number(22,0) not null,
revision number(22,0) not null,
set_of_books_id number(22,0) not null,
schedule_month date not null,
gl_posting_mo_yr date,
tax_local_id number(22,0),
vendor_id number(22,0),
tax_district_id varchar2(18),
tax_base number(22,2),
tax_rate number(22,8),
payment_amount number(22,2),
accrual_amount number(22,2),
user_id varchar2(18),
time_stamp date);

alter table ls_asset_schedule_tax add (
constraint pk_ls_asset_schedule_tax primary key (ls_asset_id, revision, set_of_books_id, schedule_month, 
                                                 tax_local_id, vendor_id, tax_district_id) using index tablespace pwrplant_idx);
                                                 
alter table ls_asset_schedule_tax
add constraint fk_ls_asset_schedule_tax1
foreign key (ls_asset_id)
references ls_asset (ls_asset_id);

alter table ls_asset_schedule_tax
add constraint fk_ls_asset_schedule_tax2
foreign key (set_of_books_id)
references set_of_books (set_of_books_id);

alter table ls_asset_schedule_tax
add constraint fk_ls_asset_schedule_tax3
foreign key (tax_local_id)
references ls_tax_local (tax_local_id);
                                                 
comment on table ls_asset_schedule_tax is 'The LS Asset Schedule Tax table stores the results of the Lessee Tax Calculation by Asset, Revision, Set of Books, and Month.';
comment on column ls_asset_schedule_tax.ls_asset_id is 'System assigned identifier of a leased asset.';
comment on column ls_asset_schedule_tax.revision is 'The revision of the leased asset.';
comment on column ls_asset_schedule_tax.set_of_books_id is 'System assigned identifier of a set of books.';
comment on column ls_asset_schedule_tax.schedule_month is 'The month from the Asset Schedule from which the Tax Base is calculated.';
comment on column ls_asset_schedule_tax.gl_posting_mo_yr is 'The month in which Tax Journals should be posted; this date will align with the schedule month unless the related ILR has a payment shift.';
comment on column ls_asset_schedule_tax.tax_local_id is 'System assigned identifier of a local tax level.';
comment on column ls_asset_schedule_tax.vendor_id is 'System assigned identifier of a vendor; -1 is assigned when the Asset is not configured to pay a specific vendor.';
comment on column ls_asset_schedule_tax.tax_district_id is 'System assigned identifier of a Tax District; this can be either a State or specific Tax District.';
comment on column ls_asset_schedule_tax.tax_base is 'The total payments (Interest, Principal, Executory, and Contingent) for the period.';
comment on column ls_asset_schedule_tax.tax_rate is 'The effective tax rate used for the period';
comment on column ls_asset_schedule_tax.payment_amount is 'The taxed payment amount calculated as Tax Base multiplied by Tax Rate.';
comment on column ls_asset_schedule_tax.accrual_amount is 'The straight-lined accrual of the payment_amount based on the payment terms of the related ILR.';
comment on column ls_asset_schedule_tax.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_asset_schedule_tax.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
                                                 
-- LS_MONTHLY_TAX_CONVERSION - will be used to convert exisitng records to new layout
create table ls_monthly_tax_conversion (
ls_asset_id number(22,0) not null,
set_of_books_id number(22,0) not null,
schedule_month date not null,
gl_posting_mo_yr date,
tax_local_id number(22,0),
vendor_id number(22,0),
tax_district_id varchar2(18),
tax_base number(22,2),
tax_rate number(22,8),
payment_amount number(22,2),
accrual_amount number(22,2),
adjustment_amount number(22,2),
user_id varchar2(18),
time_stamp date);

-- Only creating the Primary Key for conversion purposes, the foreign keys are added in a later script after the table is renamed
alter table ls_monthly_tax_conversion add (
constraint pk_ls_monthly_tax_conversion primary key (ls_asset_id, set_of_books_id, gl_posting_mo_yr,
                                                     tax_local_id, vendor_id, tax_district_id) using index tablespace pwrplant_idx);
  
-- NOTE: This table will replace LS_MONTHLY_TAX in a later script, so the table comment is written knowing that the table will be renamed                                                   
comment on table ls_monthly_tax_conversion is 'The LS Monthly Tax table stores the results of the Lessee Tax Calculation by Asset, Set of Books, and Month and is used to generate month end journal entries.';
comment on column ls_monthly_tax_conversion.ls_asset_id is 'System assigned identifier of a leased asset.';
comment on column ls_monthly_tax_conversion.set_of_books_id is 'System assigned identifier of a set of books.';
comment on column ls_monthly_tax_conversion.schedule_month is 'The month from the Asset Schedule from which the Tax Base is calculated.';
comment on column ls_monthly_tax_conversion.gl_posting_mo_yr is 'The month in which Tax Journals should be posted; this date will align with the schedule month unless the related ILR has a payment shift.';
comment on column ls_monthly_tax_conversion.tax_local_id is 'System assigned identifier of a local tax level.';
comment on column ls_monthly_tax_conversion.vendor_id is 'System assigned identifier of a vendor; -1 is assigned when the Asset is not configured to pay a specific vendor.';
comment on column ls_monthly_tax_conversion.tax_district_id is 'System assigned identifier of a Tax District; this can be either a State or specific Tax District.';
comment on column ls_monthly_tax_conversion.tax_base is 'The total payments (Interest, Principal, Executory, and Contingent) for the period.';
comment on column ls_monthly_tax_conversion.tax_rate is 'The effective tax rate used for the period';
comment on column ls_monthly_tax_conversion.payment_amount is 'The taxed payment amount calculated as Tax Base multiplied by Tax Rate.';
comment on column ls_monthly_tax_conversion.accrual_amount is 'The straight-lined accrual of the payment_amount based on the payment terms of the related ILR.';
comment on column ls_monthly_tax_conversion.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_monthly_tax_conversion.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
                                                     
-- LS_MONTHLY_TAX_CHANGED_RATES - will be populated by a trigger from ls_tax_state_rates and ls_tax_district_rates 
--  to indicate that taxes should be recalculated during month end
create table ls_monthly_tax_changed_rates (
tax_local_id number(22,0) not null,
state_district_value varchar2(18) not null,
state_district_indicator number(1,0) not null,
user_id varchar2(18),
time_stamp date);

alter table ls_monthly_tax_changed_rates add (
constraint pk_ls_monthly_tax_chg_rates primary key (tax_local_id, state_district_value, state_district_indicator) using index tablespace pwrplant_idx);

alter table ls_monthly_tax_changed_rates
add constraint fk_ls_monthly_tax_chg_rates1
foreign key (tax_local_id)
references ls_tax_local (tax_local_id);

comment on table ls_monthly_tax_changed_rates is 'The LS Monthly Tax Changed Rates table stores information about any State or District Tax Rates that have changed and will require taxes to be calculated during month end close.';
comment on column ls_monthly_tax_changed_rates.tax_local_id is 'System assigned identifier of a local tax level.';
comment on column ls_monthly_tax_changed_rates.state_district_value is 'System assigned identifier of a State or Tax District.';
comment on column ls_monthly_tax_changed_rates.state_district_indicator is '1 indicates the record stores a State value, 2 indicates the record stores a Tax District value.';
comment on column ls_monthly_tax_changed_rates.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_monthly_tax_changed_rates.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

-- Tables for conversion only which will be dropped in script 3, so not adding comments
create table ls_tax_conv_pay_terms (
ls_asset_id number(22,0) not null,
approved_revision number(22,0) not null,
lease_id number(22,0) not null,
ilr_id number(22,0) not null,
payment_term_id number(22,0) not null,
payment_term_type_id NUMBER(22,0) NOT NULL,
payment_freq_id number(22,0) not null,
number_of_terms number(22,0) not null,
payment_term_date date not null,
prepay_switch NUMBER(22,0) NOT NULL,
the_max number(22,0) not null);

alter table ls_tax_conv_pay_terms add (
constraint pk_ls_tax_conv_pay_terms primary key (ls_asset_id, approved_revision, payment_term_id) using index tablespace pwrplant_idx);

CREATE INDEX ls_tax_conv_pay_terms_idx1 ON ls_tax_conv_pay_terms (ls_asset_id, approved_revision, ilr_id) TABLESPACE pwrplant_idx;
CREATE INDEX ls_tax_conv_pay_terms_idx2 ON ls_tax_conv_pay_terms (ls_asset_id, approved_revision, ilr_id, the_max) TABLESPACE pwrplant_idx;

create table ls_tax_conv_pay_months (
ls_asset_id number(22,0) not null,
ilr_id number(22,0) not null,
approved_revision number(22,0) not null,
schedule_month date not null,
prepay_switch number(22,0) not null,
months_to_accrue number(22,0) not null);

alter table ls_tax_conv_pay_months add (
constraint pk_ls_tax_conv_pay_months primary key (ls_asset_id, approved_revision, schedule_month) using index tablespace pwrplant_idx);

CREATE INDEX ls_tax_conv_pay_months_idx1 ON ls_tax_conv_pay_months (ls_asset_id, schedule_month) TABLESPACE pwrplant_idx;

CREATE TABLE ls_tax_conv_accrue_months (
ls_asset_id NUMBER(22,0) NOT null,
set_of_books_id NUMBER(22,0) NOT NULL,
payment_month date NOT NULL,
tax_local_id NUMBER(22,0) NOT NULL,
vendor_id NUMBER(22,0) NOT NULL,
tax_district_id VARCHAR2(18) NOT NULL,
accrue_to_month DATE NOT NULL,
accrual_amount NUMBER(22,2) NOT NULL,
rounder NUMBER(22,2) NOT NULL);

ALTER TABLE ls_tax_conv_accrue_months ADD (
CONSTRAINT pk_ls_tax_conv_accrue_months PRIMARY KEY (ls_asset_id, set_of_books_id, payment_month, tax_local_id, vendor_id, tax_district_id, accrue_to_month) USING INDEX TABLESPACE pwrplant_idx);

-- Triggers to populate ls_monthly_tax_changed_rates
create or replace trigger TRIG_MEC_LS_TAX_STATE_RATES
  after insert or update
  on ls_tax_state_rates 
  for each row
begin
  -- Store the rate information that has changed or been added
  -- so that the taxes related to those rates can be recalculated
  -- during the next month end close
  merge into ls_monthly_tax_changed_rates m
  using (
           select :new.tax_local_id tax_local_id, 
                  :new.state_id state_district_value,
                  1 state_district_indicator
             from dual
        ) n
  on (    m.tax_local_id = n.tax_local_id
      and m.state_district_value = n.state_district_value
      and m.state_district_indicator = n.state_district_indicator)
  when not matched then
    insert (tax_local_id, state_district_value, state_district_indicator)
    values (n.tax_local_id, n.state_district_value, n.state_district_indicator);
end TRIG_MEC_LS_TAX_STATE_RATES;
/

create or replace trigger TRIG_MEC_LS_TAX_DIST_RATES
  after insert or update
  on ls_tax_district_rates 
  for each row
begin
  -- Store the rate information that has changed or been added
  -- so that the taxes related to those rates can be recalculated
  -- during the next month end close
  merge into ls_monthly_tax_changed_rates m
  using (
           select :new.tax_local_id tax_local_id, 
                  :new.ls_tax_district_id state_district_value,
                  2 state_district_indicator
             from dual
        ) n
  on (    m.tax_local_id = n.tax_local_id
      and m.state_district_value = n.state_district_value
      and m.state_district_indicator = n.state_district_indicator)
  when not matched then
    insert (tax_local_id, state_district_value, state_district_indicator)
    values (n.tax_local_id, n.state_district_value, n.state_district_indicator);
end TRIG_MEC_LS_TAX_DIST_RATES;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13066, 0, 2018, 2, 0, 0, 52875, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052875_lessee_01_tax_calc_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;