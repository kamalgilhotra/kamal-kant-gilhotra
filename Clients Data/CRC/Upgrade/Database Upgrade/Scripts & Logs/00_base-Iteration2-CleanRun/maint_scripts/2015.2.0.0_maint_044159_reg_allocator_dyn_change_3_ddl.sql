/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044159_reg_allocator_dyn_change_3_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 08/07/2015 Sarah Byers 		 	Drop reg_allocator_dyn
||========================================================================================
*/

drop table reg_allocator_dyn;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2794, 0, 2015, 2, 0, 0, 044159, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044159_reg_allocator_dyn_change_3_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;