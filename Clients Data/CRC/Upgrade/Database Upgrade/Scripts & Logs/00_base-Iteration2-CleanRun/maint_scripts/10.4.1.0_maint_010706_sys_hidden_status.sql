/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010706_sys_hidden_status.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   05/19/2013 Alex P.        Point Release
||============================================================================
*/

update PP_REPORTS_STATUS set DESCRIPTION = 'Hidden' where PP_REPORT_STATUS_ID = 4;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (388, 0, 10, 4, 1, 0, 10706, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010706_sys_hidden_status.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;