/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037108_cwip_fast101.sql  (part of epic 9415)
||=================================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date     Revised By     Reason for Change
|| -------- -------- -------------- -----------------------------------------------
|| 10.4.2.0 3/14/2014 Sunjin Cone   More Auto101 changes for performance improvement
||=================================================================================
*/

drop table UNITIZE_ALLOC_ACT_STATS;
drop table UNITIZE_ALLOC_EST_STATS;
drop table UNITIZE_ALLOC_STND_STATS;
drop table UNITIZE_ALLOC_UNIT_ITEM_STATS;
drop table UNITIZE_ALLOC_ERRORS;

create table UNITIZE_ALLOC_UNIT_ITEM_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 ALLOC_ID               number(22,0) not null,
 ALLOC_PRIORITY         number(22,8) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_UT      number(22,8),
 ALLOC_RATIO_BY_SUB     number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_UNIT_ITEM_STATS
   add constraint PK_UNITIZE_ALLOC_UNIT_ITEM_STA
       primary key (COMPANY_ID, WORK_ORDER_ID, EXPENDITURE_TYPE_ID, ALLOC_ID, ALLOC_PRIORITY, UNIT_ITEM_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_UNIT_ITEM_STATS is '(C) [04] Allocation information used to unitization charge types configured for Actuals, Estimate, and Standards based allocation.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_TYPE is 'Allocation type: actuals.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_BASIS is 'Allocation basis: dollars, quantity.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization allocation.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_ALLOC_STND_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 ALLOC_ID               number(22,0) not null,
 ALLOC_PRIORITY         number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 UNITIZE_BY_ACCOUNT     number(22,0),
 UTILITY_ACCOUNT_ID     number(22,0),
 BUS_SEGMENT_ID         number(22,0),
 SUB_ACCOUNT_ID         number(22,0),
 ALLOC_STATISTIC        number(22,2),
 UNIT_QUANTITY          number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_UT      number(22,8),
 ALLOC_RATIO_BY_SUB     number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_STND_STATS
   add constraint PK_UNITIZE_ALLOC_STND_STATS
       primary key (COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, ALLOC_ID, ALLOC_PRIORITY)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_STND_STATS is '(C) [04] Allocation information used to unitization charge types configured for Standards based allocation.';
comment on column UNITIZE_ALLOC_STND_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_STND_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_STND_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_STND_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_TYPE is 'Allocation type: standards.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_BASIS is 'Allocation basis: standard material cost, standard labor_cost, standard labor hours, standard cor cost, standard cor hours.';
comment on column UNITIZE_ALLOC_STND_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_STND_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_STND_STATS.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_STND_STATS.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_STND_STATS.UNIT_QUANTITY is 'Unit Item quantity.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_RATIO_BY_UT  is 'Allocation percentage assigned to the unit item for a particular utility account over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_RATIO_BY_SUB  is 'Allocation percentage assigned to the unit item for a particular sub account over all unit items that have sub account information.';
comment on column UNITIZE_ALLOC_STND_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_STND_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_ALLOC_ERRORS
(
 COMPANY_ID           number(22,0) not null,
 WORK_ORDER_ID        number(22,0) not null,
 EXPENDITURE_TYPE_ID  number(22,0) not null,
 ALLOC_ID             number(22,0) not null,
 PRIORITY             number(22,0) not null,
 ERROR_MSG            varchar2(2000) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table UNITIZE_ALLOC_ERRORS
   add constraint PK_UNITIZE_ALLOC_ERRORS
       primary key (COMPANY_ID, WORK_ORDER_ID, EXPENDITURE_TYPE_ID, ALLOC_ID, PRIORITY, ERROR_MSG)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_ERRORS is '(C) [04] Unitization allocation errors saved for work orders being unitized.';
comment on column UNITIZE_ALLOC_ERRORS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_ERRORS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_ERRORS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_ERRORS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_ERRORS.PRIORITY  is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_ERRORS.ERROR_MSG is 'Description of the error message.';
comment on column UNITIZE_ALLOC_ERRORS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_ERRORS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table CHARGE_GROUP_CONTROL add ORIG_CHARGE_GROUP_ID number(22,0);
comment on column CHARGE_GROUP_CONTROL.ORIG_CHARGE_GROUP_ID is 'The original charge_group_id that links back to CWIP_CHARGE. If the value is NULL, it is the original record.  If the value is equal to charge_group_id, it is the original record that has been direct assigned.  If the value is different than the charge_group_id, it is associated to the original charge group split for allocation.  Added as of v10.4.2.0 (will be null for wos unitized using old 101 code).';
comment on column UNITIZE_WO_UNITS_STG.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account of the unit item.';
comment on column UNITIZE_WO_UNITS_STG.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utility account of the unit item.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1041, 0, 10, 4, 2, 0, 37108, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037108_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;