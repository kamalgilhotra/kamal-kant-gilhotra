
/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_044870_budgetcap_update_budget_forecast_reports_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- ----------------------------------------
|| 2015.2.0.0 09/03/2015 Alnoor Ruhani  Update the special note field on a few
||                                      1000 level actual reports so that we can
||                                      filter on set of books when the reports
||                                      are run.
||============================================================================
*/

UPDATE PP_REPORTS
SET special_note = special_note || ', INCLUDE SET OF BOOKS'
WHERE DATAWINDOW LIKE 'dw_fp_bdg_1%' OR datawindow LIKE 'dw_bdg_bi_1%';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2848, 0, 2015, 2, 0, 0, 044870, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044870_budgetcap_update_budget_forecast_reports_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;