/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039765_reg_escape_ampersands.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/26/2014 Kyle Peterson
||============================================================================
*/

--script to escape ampersands in the Reg menu items. Should only replace SINGLE
--ampersands with two ampersands.

update PPBASE_WORKSPACE set LABEL = REGEXP_REPLACE(LABEL, '[^&]&[^&]', '&&') where MODULE = 'REG';

update PPBASE_MENU_ITEMS set LABEL = REGEXP_REPLACE(LABEL, '[^&]&[^&]', '&&') where MODULE = 'REG';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1395, 0, 10, 4, 2, 7, 39765, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039765_reg_escape_ampersands.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;