/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_042773_jobserver_inc_20150205_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/03/2014 Paul Cordero    	 Altering column 'KEY' on 'PP_JOB_WORKFLOW', increase size from 30 to 50
||==========================================================================================
*/

set serveroutput on;

declare 
  doesTableColumnExistAs number := 0;
begin
	begin
	   
		select COUNT(*) INTO doesTableColumnExistAs
		from ALL_TAB_COLUMNS
		where DATA_TYPE = upper('VARCHAR2')
		  and DATA_LENGTH = 30
		  and COLUMN_NAME = upper('KEY')
		  and TABLE_NAME = upper('PP_JOB_WORKFLOW') 
		  and OWNER = upper('PWRPLANT');   	   
          
		if doesTableColumnExistAs = 1 then
			begin
			  dbms_output.put_line('Changing column KEY in PP_JOB_WORKFLOW table to varchar(50)');
							
			  execute immediate 'ALTER TABLE "PWRPLANT"."PP_JOB_WORKFLOW" 
			  MODIFY 
			  (
				"KEY" VARCHAR2(50 BYTE)
			  )';    
				  
			end;
		  else
			begin
			  dbms_output.put_line('KEY in PP_JOB_WORKFLOW table is varchar(50)');
			end;
		end if;
	end;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2344, 0, 2015, 1, 0, 0, 042773, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042773_jobserver_inc_20150205_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;