/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_046913_reg_update_case_processing_hints_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2017.1.0.0 01/13/2017 Sarah Byers		 Update hints for RMS case processing
||============================================================================
*/

-- remove existing entries
delete from pp_datawindow_hints
 where datawindow = 'UPDATE REG_CASE_ADJUST_ALLOC_RESULT PERCENT EXCEPT DISALLOWED ADJ';

-- Add materialize hints for select statements 1, 2, 4, and 5
insert into pp_datawindow_hints (
	datawindow, select_number, hint, change_by, change_date)
values (
	'UPDATE REG_CASE_ADJUST_ALLOC_RESULT PERCENT EXCEPT DISALLOWED ADJ', 1, '/*+ materialize */', user, sysdate);

insert into pp_datawindow_hints (
	datawindow, select_number, hint, change_by, change_date)
values (
	'UPDATE REG_CASE_ADJUST_ALLOC_RESULT PERCENT EXCEPT DISALLOWED ADJ', 2, '/*+ materialize */', user, sysdate);

insert into pp_datawindow_hints (
	datawindow, select_number, hint, change_by, change_date)
values (
	'UPDATE REG_CASE_ADJUST_ALLOC_RESULT PERCENT EXCEPT DISALLOWED ADJ', 4, '/*+ materialize */', user, sysdate);

insert into pp_datawindow_hints (
	datawindow, select_number, hint, change_by, change_date)
values (
	'UPDATE REG_CASE_ADJUST_ALLOC_RESULT PERCENT EXCEPT DISALLOWED ADJ', 5, '/*+ materialize */', user, sysdate);

-- new entry for calc dynamic factors, very specific hint used at Duke that isn't going into the base code
insert into pp_datawindow_hints (
	datawindow, select_number, hint, change_by, change_date)
values (
	'CALC DYNAMIC FACTOR USING TARGET', 1, ' ', user, sysdate);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3357, 0, 2017, 1, 0, 0, 46913, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_046913_reg_update_case_processing_hints_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
