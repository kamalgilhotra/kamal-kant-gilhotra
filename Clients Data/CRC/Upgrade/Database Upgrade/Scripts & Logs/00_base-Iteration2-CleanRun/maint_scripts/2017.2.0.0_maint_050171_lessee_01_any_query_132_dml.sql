/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_050171_lessee_01_any_query_132_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.2.0.0  01/22/2018 Anand R        add sob to Lease Payment Header and Invoice Detail query
||============================================================================
*/

DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Payment Header and Invoice Detail';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'WITH currency_selection AS (
SELECT lct.ls_currency_type_id, lct.description
FROM ls_lease_currency_type lct, pp_any_required_filter parf
where upper(trim(parf.column_name)) = ''CURRENCY TYPE''
AND  upper(trim(parf.filter_value)) =  upper(trim(lct.description))
)
select to_char(lph.gl_posting_mo_yr, ''yyyymm'') as monthnum,
       lph.company_id,
	   (select s.description from set_of_books s where s.set_of_books_id = lpl.set_of_books_id)  as set_of_books,
       lv.description as vendor,
       aps.description as payment_status,
       lpl.amount as calculated_amount, 
       lpl.adjustment_amount as adjustments, 
       lph.amount as total_payment, 
       inv.invoice_number,
       inv.invoice_amount, 
       decode(map.in_tolerance, 1, ''Yes'', null, null, ''No'') as in_tolerance,
       ll.lease_id,
       ll.lease_number,
       lct.description as lease_cap_type,
       nvl(ilr.ilr_id, -1) as ilr_id,
       nvl(ilr.ilr_number, ''N/A'') as ilr_number,
       nvl(lfs.description, ''N/A'') as funding_status,
       nvl(la.ls_asset_id, -1) as ls_asset_id,
       nvl(la.leased_asset_number, ''N/A'') as leased_asset_number,
       co.description as company_description,
       InitCap(currency_selection.description) currency_type,
       lph.iso_code currency,
       lph.currency_display_symbol currency_symbol
  from v_ls_payment_hdr_fx lph 
       left OUTER JOIN ls_asset la ON (lph.ls_asset_id = la.ls_asset_id)
       left JOIN ls_ilr ilr ON (lph.ilr_id = ilr.ilr_id)
       JOIN ls_lease ll ON (lph.lease_id = ll.lease_id)
       JOIN company co ON (co.company_id = lph.company_id)
       JOIN ls_lease_cap_type lct ON (ll.lease_cap_type_id = lct.ls_lease_cap_type_id)
       left OUTER JOIN ls_funding_status lfs ON (ilr.funding_status_id = lfs.funding_status_id)
       JOIN ls_vendor lv ON (lph.vendor_id = lv.vendor_id)
       JOIN approval_status aps ON (lph.payment_status_id = aps.approval_status_id)
       left JOIN ls_invoice_payment_map map ON (lph.payment_id = map.payment_id)
       left OUTER JOIN v_ls_invoice_fx_vw inv ON (map.invoice_id = inv.invoice_id)
       JOIN (select payment_id,
               sum(nvl(amount, 0)) as amount,
               sum(nvl(adjustment_amount, 0)) as adjustment_amount,
               ls_cur_type,
			   set_of_books_id
          from v_ls_payment_line_fx, currency_selection
         where v_ls_payment_line_fx.ls_cur_type = currency_selection.ls_currency_type_id
         AND to_char(gl_posting_mo_yr, ''yyyymm'') in
               (select filter_value
                  from pp_any_required_filter
                 where upper(trim(column_name)) = ''MONTHNUM'')       group by payment_id, ls_cur_type, set_of_books_id) lpl ON (lpl.ls_cur_type = lph.ls_cur_type AND lpl.payment_id = lph.payment_id)
       JOIN currency_selection ON (lpl.ls_cur_type = currency_selection.ls_currency_type_id AND inv.ls_cur_type = currency_selection.ls_currency_type_id)
 where to_char(lph.gl_posting_mo_yr, ''yyyymm'') in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''MONTHNUM'')
   and lph.company_id in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''COMPANY ID'')          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'adjustments',
        9,
        1,
        1,
        '',
        'Adjustments',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'calculated_amount',
        8,
        1,
        1,
        '',
        'Calculated Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT l_id,
    'set_of_books',
    3,
    0,
    1,
    '',
    'Set of Books',
    300,
    'description',
    'set_of_books',
    'VARCHAR2',
    0,
    'description',
    null,
    null,
    null,
    0,
    0
FROM DUAL

UNION ALL

SELECT  l_id,
        'company_description',
        4,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        2,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        6,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_symbol',
        23,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_type',
        22,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'funding_status',
        19,
        0,
        1,
        '',
        'Funding Status',
        300,
        'description',
        'ls_funding_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        17,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        18,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'in_tolerance',
        13,
        0,
        1,
        '',
        'In Tolerance',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'invoice_amount',
        12,
        1,
        1,
        '',
        'Invoice Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'invoice_number',
        11,
        0,
        1,
        '',
        'Invoice Number',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type',
        16,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_id',
        14,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        15,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'leased_asset_number',
        21,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        20,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'monthnum',
        1,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        2,
        'the_sort',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_status',
        7,
        0,
        1,
        '',
        'Payment Status',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'total_payment',
        10,
        1,
        1,
        '',
        'Total Payment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'vendor',
        5,
        0,
        1,
        '',
        'Vendor',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4085, 0, 2017, 2, 0, 0, 50171, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050171_lessee_01_any_query_132_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;