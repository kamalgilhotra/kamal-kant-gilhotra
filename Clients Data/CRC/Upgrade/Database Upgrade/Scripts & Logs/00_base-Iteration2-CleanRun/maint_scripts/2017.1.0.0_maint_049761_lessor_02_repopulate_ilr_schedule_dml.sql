/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049761_lessor_02_repopulate_ilr_schedule_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/09/2017 Andrew Hill      Add repopulate ILR schedule from renamed table
||============================================================================
*/

INSERT INTO lsr_ilr_schedule (ilr_id,
                              revision,
                              set_of_books_id,
                              month,
                              interest_income_received,
                              interest_income_accrued,
                              interest_rental_recvd_spread,
                              beg_deferred_rev,
                              deferred_rev_activity,
                              end_deferred_rev,
                              beg_receivable,
                              end_receivable,
                              beg_lt_receivable,
                              end_lt_receivable,
                              executory_accrual1,
                              executory_accrual2,
                              executory_accrual3,
                              executory_accrual4,
                              executory_accrual5,
                              executory_accrual6,
                              executory_accrual7,
                              executory_accrual8,
                              executory_accrual9,
                              executory_accrual10,
                              executory_paid1,
                              executory_paid2,
                              executory_paid3,
                              executory_paid4,
                              executory_paid5,
                              executory_paid6,
                              executory_paid7,
                              executory_paid8,
                              executory_paid9,
                              executory_paid10,
                              contingent_accrual1,
                              contingent_accrual2,
                              contingent_accrual3,
                              contingent_accrual4,
                              contingent_accrual5,
                              contingent_accrual6,
                              contingent_accrual7,
                              contingent_accrual8,
                              contingent_accrual9,
                              contingent_accrual10,
                              contingent_paid1,
                              contingent_paid2,
                              contingent_paid3,
                              contingent_paid4,
                              contingent_paid5,
                              contingent_paid6,
                              contingent_paid7,
                              contingent_paid8,
                              contingent_paid9,
                              contingent_paid10)

select  s.ilr_id,
        s.revision,
        s.set_of_books_id,
        s.MONTH,
        s.interest_income_received,
        s.interest_income_accrued,
        s.interest_rental_recvd_spread,
        s.beg_deferred_rev,
        s.deferred_rev_activity,
        s.end_deferred_rev,
        S.beg_receivable,
        s.end_receivable,
        COALESCE(sd.beg_long_term_receivable, 0) AS beg_lt_receivable,
        coalesce(sd.end_long_term_receivable, 0) as end_lt_receivable,
        s.executory_accrual1,
        s.executory_accrual2,
        s.executory_accrual3,
        s.executory_accrual4,
        s.executory_accrual5,
        s.executory_accrual6,
        s.executory_accrual7,
        s.executory_accrual8,
        s.executory_accrual9,
        s.executory_accrual10,
        s.executory_paid1,
        s.executory_paid2,
        s.executory_paid3,
        s.executory_paid4,
        s.executory_paid5,
        s.executory_paid6,
        s.executory_paid7,
        s.executory_paid8,
        s.executory_paid9,
        s.executory_paid10,
        s.contingent_accrual1,
        s.contingent_accrual2,
        s.contingent_accrual3,
        s.contingent_accrual4,
        s.contingent_accrual5,
        s.contingent_accrual6,
        s.contingent_accrual7,
        s.contingent_accrual8,
        s.contingent_accrual9,
        s.contingent_accrual10,
        s.contingent_paid1,
        s.contingent_paid2,
        s.contingent_paid3,
        s.contingent_paid4,
        s.contingent_paid5,
        s.contingent_paid6,
        s.contingent_paid7,
        s.contingent_paid8,
        s.contingent_paid9,
        s.contingent_paid10
FROM lsr_ilr_schedule_old S
LEFT JOIN lsr_ilr_schedule_sales_direct sd on s.ilr_id = sd.ilr_id and s.revision = sd.revision and s.month = sd.month and s.set_of_books_id = sd.set_of_books_id;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3904, 0, 2017, 1, 0, 0, 49761, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049761_lessor_02_repopulate_ilr_schedule_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;