/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045699_sys_add_cr_cosec_tables_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2016.1.0.0 06/27/2016 David Haupt    New table for CR sources requiring company security
||============================================================================
*/

CREATE TABLE cr_company_security_tables (
  table_name        VARCHAR2(50)  NOT NULL,
  user_id     VARCHAR2(18)  NULL,
  time_stamp  DATE          NULL
  )
/

ALTER TABLE cr_company_security_tables
  ADD CONSTRAINT cr_company_security_tables_pk PRIMARY KEY(
    table_name
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

COMMENT ON TABLE cr_company_security_tables IS '(F)  [15]
The CR Company Security Tables table holds the names of tables that must obey CR company security.';

COMMENT ON COLUMN cr_company_security_tables.table_name IS 'Table to check for CR company security.';
COMMENT ON COLUMN cr_company_security_tables.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN cr_company_security_tables.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3229, 0, 2016, 1, 0, 0, 045699, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045699_sys_add_cr_cosec_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;