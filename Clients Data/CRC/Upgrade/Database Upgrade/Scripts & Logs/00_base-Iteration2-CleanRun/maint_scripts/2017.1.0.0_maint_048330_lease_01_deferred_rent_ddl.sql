/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_01_deferred_rent_ddl.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding columns for deferred rent
||========================================================================================
*/

alter table ls_asset_schedule add beg_deferred_rent number(22,2);
alter table ls_ilr_schedule add beg_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_stg add beg_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_calc_stg add beg_deferred_rent number(22,2);

alter table ls_asset_schedule add deferred_rent number(22,2);
alter table ls_ilr_schedule add deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_stg add deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_calc_stg add deferred_rent number(22,2);

alter table ls_asset_schedule add end_deferred_rent number(22,2);
alter table ls_ilr_schedule add end_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_stg add end_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_calc_stg add end_deferred_rent number(22,2);

alter table ls_asset_schedule add beg_st_deferred_rent number(22,2);
alter table ls_ilr_schedule add beg_st_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_stg add beg_st_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_calc_stg add beg_st_deferred_rent number(22,2);

alter table ls_asset_schedule add end_st_deferred_rent number(22,2);
alter table ls_ilr_schedule add end_st_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_stg add end_st_deferred_rent number(22,2);
alter table ls_ilr_asset_schedule_calc_stg add end_st_deferred_rent number(22,2);

alter table ls_ilr_account add st_deferred_account_id number(22,0);
alter table ls_ilr_account add lt_deferred_account_id number(22,0);

alter table ls_ilr_group add st_deferred_account_id number(22,0);
alter table ls_ilr_group add lt_deferred_account_id number(22,0);

alter table ls_import_ilr add st_deferred_account_xlate varchar2(254);
alter table ls_import_ilr add st_deferred_account_id number(22,0);
alter table ls_import_ilr add lt_deferred_account_xlate varchar2(254);
alter table ls_import_ilr add lt_deferred_account_id number(22,0);

alter table ls_import_ilr_archive add st_deferred_account_xlate varchar2(254);
alter table ls_import_ilr_archive add st_deferred_account_id number(22,0);
alter table ls_import_ilr_archive add lt_deferred_account_xlate varchar2(254);
alter table ls_import_ilr_archive add lt_deferred_account_id number(22,0);

COMMENT ON COLUMN ls_asset_schedule.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_stg.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_calc_stg.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';


COMMENT ON COLUMN ls_asset_schedule.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_stg.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_calc_stg.deferred_rent IS 'The deferred rent amount for the period.';

COMMENT ON COLUMN ls_asset_schedule.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_stg.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_calc_stg.end_deferred_rent IS 'The ending deferred rent amount for the period.';

COMMENT ON COLUMN ls_asset_schedule.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_stg.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_calc_stg.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';

COMMENT ON COLUMN ls_asset_schedule.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_schedule.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_stg.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN ls_ilr_asset_schedule_calc_stg.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';

COMMENT ON COLUMN ls_ilr_account.st_deferred_account_id IS 'The short term deferred rent account.';
COMMENT ON COLUMN ls_ilr_account.lt_deferred_account_id IS 'The long term deferred rent account.';

COMMENT ON COLUMN ls_ilr_group.st_deferred_account_id IS 'The short term deferred rent account.';
COMMENT ON COLUMN ls_ilr_group.lt_deferred_account_id IS 'The long term deferred rent account.';

COMMENT ON COLUMN ls_import_ilr.st_deferred_account_xlate IS 'Translation field for determining short term deferred rent account.';
COMMENT ON COLUMN ls_import_ilr.st_deferred_account_id IS 'The short term deferred rent account.';
COMMENT ON COLUMN ls_import_ilr.lt_deferred_account_xlate IS 'Translation field for determining long term deferred rent account.';
COMMENT ON COLUMN ls_import_ilr.lt_deferred_account_id IS 'The long term deferred rent account.';

COMMENT ON COLUMN ls_import_ilr_archive.st_deferred_account_xlate IS 'Translation field for determining short term deferred rent account.';
COMMENT ON COLUMN ls_import_ilr_archive.st_deferred_account_id IS 'The short term deferred rent account.';
COMMENT ON COLUMN ls_import_ilr_archive.lt_deferred_account_xlate IS 'Translation field for determining long term deferred rent account.';
COMMENT ON COLUMN ls_import_ilr_archive.lt_deferred_account_id IS 'The long term deferred rent account.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3538, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_01_deferred_rent_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;