/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045666_lease_fix_import_archive_columns_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| --------    ---------- -------------- ----------------------------------------
|| 2015.2.2.0  04/27/2016 Jared Watkins  Remove estimated_residual_tmp from lease
||                                         pp_import_type archive columns
||============================================================================
*/

UPDATE pp_import_type
SET archive_additional_columns = 'ls_asset_id'
WHERE import_type_id = 253
AND archive_additional_columns = 'ls_asset_id, estimated_residual_tmp';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3173, 0, 2015, 2, 2, 0, 045666, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045666_lease_fix_import_archive_columns_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;