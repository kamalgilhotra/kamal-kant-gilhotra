/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052429_lessee_01_ifrs_dec_remeas_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/17/2018 K Powers       Add'l Gain/Loss option on remeasurement
||============================================================================
*/

ALTER TABLE LS_ILR_OPTIONS ADD REMEASURE_CALC_GAIN_LOSS NUMBER(22,0);

COMMENT ON COLUMN LS_ILR_OPTIONS.REMEASURE_CALC_GAIN_LOSS IS 'Indicates whether Gain/Loss should be calculated upon remeasurement.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (10664, 0, 2018, 1, 0, 0, 52429, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052429_lessee_01_ifrs_dec_remeas_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;