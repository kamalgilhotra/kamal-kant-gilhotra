/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029215_06_PEND_TRANS_ADD_SOB.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- --------------   ------------------------------------
|| 10.4.0.0   02/05/2013 Charlie Shilling Point Release
||============================================================================
*/

create or replace trigger PEND_TRANS_ADD_SOB
   after update or insert on PEND_BASIS
   for each row
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PEND_TRANS_ADD_SOB
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   ||============================================================================
   */

declare
   V_RTN              number(22, 0);
   V_DEPR_GROUP_ID    number(22, 0);
   V_ASSET_ID         number(22, 0);
   V_VINTAGE          number(22, 0);
   V_GL_POSTING_MO_YR date;
   V_ACTIVITY_CODE    varchar2(10);

begin
   update PEND_TRANSACTION
      set (ADJUSTED_COST_OF_REMOVAL,
            ADJUSTED_SALVAGE_CASH,
            ADJUSTED_SALVAGE_RETURNS,
            ADJUSTED_RESERVE_CREDITS) =
           (select A.COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE,
                   A.SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE,
                   A.RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE
              from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
             where A.COMPANY_ID = B.COMPANY_ID
               and B.SET_OF_BOOKS_ID = 1
               and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID)
    where PEND_TRANS_ID = :NEW.PEND_TRANS_ID;

   update PEND_TRANSACTION_SET_OF_BOOKS A
      set POSTING_AMOUNT =
           (select sum(NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                       NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                       NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                       NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                       NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                       NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                       NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                       NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                       NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                       NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                       NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                       NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                       NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                       NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                       NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                       NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                       NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                       NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                       NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                       NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                       NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                       NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                       NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                       NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                       NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                       NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                       NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                       NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                       NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                       NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                       NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                       NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                       NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                       NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                       NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                       NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                       NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                       NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                       NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                       NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                       NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                       NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                       NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                       NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                       NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                       NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                       NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                       NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                       NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                       NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                       NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                       NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                       NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                       NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                       NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                       NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                       NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                       NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                       NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                       NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                       NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                       NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                       NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                       NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                       NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                       NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                       NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                       NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                       NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                       NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR)
              from SET_OF_BOOKS B
             where A.SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID)
    where A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
      and exists (select 1
             from PEND_TRANSACTION C, COMPANY_SET_OF_BOOKS D
            where C.COMPANY_ID = D.COMPANY_ID
              and A.PEND_TRANS_ID = C.PEND_TRANS_ID
              and A.SET_OF_BOOKS_ID = D.SET_OF_BOOKS_ID
              and D.INCLUDE_INDICATOR = 1);

   insert into PEND_TRANSACTION_SET_OF_BOOKS
      (PEND_TRANS_ID, SET_OF_BOOKS_ID, ACTIVITY_CODE, ADJUSTED_COST_OF_REMOVAL,
       ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, GAIN_LOSS, RESERVE, FERC_ACTIVITY_CODE,
       ADJUSTED_RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, POSTING_AMOUNT,
       POSTING_QUANTITY, IMPAIRMENT_EXPENSE_AMOUNT)
      select PEND_TRANS_ID,
             SET_OF_BOOKS_ID,
             ACTIVITY_CODE,
             COST_OF_REMOVAL,
             SALVAGE_CASH,
             SALVAGE_RETURNS,
             GET_PEND_SOB_GL(PEND_TRANS_ID, SET_OF_BOOKS_ID, RESERVE, POSTING_AMOUNT) GAIN_LOSS,
             RESERVE,
             FERC_ACTIVITY_CODE,
             RESERVE_CREDITS,
             REPLACEMENT_AMOUNT,
             GAIN_LOSS_REVERSAL,
             POSTING_AMOUNT,
             POSTING_QUANTITY,
             IMPAIRMENT_EXPENSE_AMOUNT
        from (select A.PEND_TRANS_ID,
                     B.SET_OF_BOOKS_ID,
                     ACTIVITY_CODE,
                     NVL(COST_OF_REMOVAL * B.PERCENT_REMOVAL_SALVAGE, 0) COST_OF_REMOVAL,
                     NVL(SALVAGE_CASH * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_CASH,
                     NVL(SALVAGE_RETURNS * B.PERCENT_REMOVAL_SALVAGE, 0) SALVAGE_RETURNS,
                     GET_PEND_SOB_RESERVE(PEND_TRANS_ID, B.SET_OF_BOOKS_ID) RESERVE,
                     FERC_ACTIVITY_CODE,
                     NVL(RESERVE_CREDITS * B.PERCENT_REMOVAL_SALVAGE, 0) RESERVE_CREDITS,
                     NVL(REPLACEMENT_AMOUNT, 0) REPLACEMENT_AMOUNT,
                     NVL(GAIN_LOSS_REVERSAL, 0) GAIN_LOSS_REVERSAL,
                     (select NVL(:NEW.BASIS_1, 0) * BASIS_1_INDICATOR +
                             NVL(:NEW.BASIS_2, 0) * BASIS_2_INDICATOR +
                             NVL(:NEW.BASIS_3, 0) * BASIS_3_INDICATOR +
                             NVL(:NEW.BASIS_4, 0) * BASIS_4_INDICATOR +
                             NVL(:NEW.BASIS_5, 0) * BASIS_5_INDICATOR +
                             NVL(:NEW.BASIS_6, 0) * BASIS_6_INDICATOR +
                             NVL(:NEW.BASIS_7, 0) * BASIS_7_INDICATOR +
                             NVL(:NEW.BASIS_8, 0) * BASIS_8_INDICATOR +
                             NVL(:NEW.BASIS_9, 0) * BASIS_9_INDICATOR +
                             NVL(:NEW.BASIS_10, 0) * BASIS_10_INDICATOR +
                             NVL(:NEW.BASIS_11, 0) * BASIS_11_INDICATOR +
                             NVL(:NEW.BASIS_12, 0) * BASIS_12_INDICATOR +
                             NVL(:NEW.BASIS_13, 0) * BASIS_13_INDICATOR +
                             NVL(:NEW.BASIS_14, 0) * BASIS_14_INDICATOR +
                             NVL(:NEW.BASIS_15, 0) * BASIS_15_INDICATOR +
                             NVL(:NEW.BASIS_16, 0) * BASIS_16_INDICATOR +
                             NVL(:NEW.BASIS_17, 0) * BASIS_17_INDICATOR +
                             NVL(:NEW.BASIS_18, 0) * BASIS_18_INDICATOR +
                             NVL(:NEW.BASIS_19, 0) * BASIS_19_INDICATOR +
                             NVL(:NEW.BASIS_20, 0) * BASIS_20_INDICATOR +
                             NVL(:NEW.BASIS_21, 0) * BASIS_21_INDICATOR +
                             NVL(:NEW.BASIS_22, 0) * BASIS_22_INDICATOR +
                             NVL(:NEW.BASIS_23, 0) * BASIS_23_INDICATOR +
                             NVL(:NEW.BASIS_24, 0) * BASIS_24_INDICATOR +
                             NVL(:NEW.BASIS_25, 0) * BASIS_25_INDICATOR +
                             NVL(:NEW.BASIS_26, 0) * BASIS_26_INDICATOR +
                             NVL(:NEW.BASIS_27, 0) * BASIS_27_INDICATOR +
                             NVL(:NEW.BASIS_28, 0) * BASIS_28_INDICATOR +
                             NVL(:NEW.BASIS_29, 0) * BASIS_29_INDICATOR +
                             NVL(:NEW.BASIS_30, 0) * BASIS_30_INDICATOR +
                             NVL(:NEW.BASIS_31, 0) * BASIS_31_INDICATOR +
                             NVL(:NEW.BASIS_32, 0) * BASIS_32_INDICATOR +
                             NVL(:NEW.BASIS_33, 0) * BASIS_33_INDICATOR +
                             NVL(:NEW.BASIS_34, 0) * BASIS_34_INDICATOR +
                             NVL(:NEW.BASIS_35, 0) * BASIS_35_INDICATOR +
                             NVL(:NEW.BASIS_36, 0) * BASIS_36_INDICATOR +
                             NVL(:NEW.BASIS_37, 0) * BASIS_37_INDICATOR +
                             NVL(:NEW.BASIS_38, 0) * BASIS_38_INDICATOR +
                             NVL(:NEW.BASIS_39, 0) * BASIS_39_INDICATOR +
                             NVL(:NEW.BASIS_40, 0) * BASIS_40_INDICATOR +
                             NVL(:NEW.BASIS_41, 0) * BASIS_41_INDICATOR +
                             NVL(:NEW.BASIS_42, 0) * BASIS_42_INDICATOR +
                             NVL(:NEW.BASIS_43, 0) * BASIS_43_INDICATOR +
                             NVL(:NEW.BASIS_44, 0) * BASIS_44_INDICATOR +
                             NVL(:NEW.BASIS_45, 0) * BASIS_45_INDICATOR +
                             NVL(:NEW.BASIS_46, 0) * BASIS_46_INDICATOR +
                             NVL(:NEW.BASIS_47, 0) * BASIS_47_INDICATOR +
                             NVL(:NEW.BASIS_48, 0) * BASIS_48_INDICATOR +
                             NVL(:NEW.BASIS_49, 0) * BASIS_49_INDICATOR +
                             NVL(:NEW.BASIS_50, 0) * BASIS_50_INDICATOR +
                             NVL(:NEW.BASIS_51, 0) * BASIS_51_INDICATOR +
                             NVL(:NEW.BASIS_52, 0) * BASIS_52_INDICATOR +
                             NVL(:NEW.BASIS_53, 0) * BASIS_53_INDICATOR +
                             NVL(:NEW.BASIS_54, 0) * BASIS_54_INDICATOR +
                             NVL(:NEW.BASIS_55, 0) * BASIS_55_INDICATOR +
                             NVL(:NEW.BASIS_56, 0) * BASIS_56_INDICATOR +
                             NVL(:NEW.BASIS_57, 0) * BASIS_57_INDICATOR +
                             NVL(:NEW.BASIS_58, 0) * BASIS_58_INDICATOR +
                             NVL(:NEW.BASIS_59, 0) * BASIS_59_INDICATOR +
                             NVL(:NEW.BASIS_60, 0) * BASIS_60_INDICATOR +
                             NVL(:NEW.BASIS_61, 0) * BASIS_61_INDICATOR +
                             NVL(:NEW.BASIS_62, 0) * BASIS_62_INDICATOR +
                             NVL(:NEW.BASIS_63, 0) * BASIS_63_INDICATOR +
                             NVL(:NEW.BASIS_64, 0) * BASIS_64_INDICATOR +
                             NVL(:NEW.BASIS_65, 0) * BASIS_65_INDICATOR +
                             NVL(:NEW.BASIS_66, 0) * BASIS_66_INDICATOR +
                             NVL(:NEW.BASIS_67, 0) * BASIS_67_INDICATOR +
                             NVL(:NEW.BASIS_68, 0) * BASIS_68_INDICATOR +
                             NVL(:NEW.BASIS_69, 0) * BASIS_69_INDICATOR +
                             NVL(:NEW.BASIS_70, 0) * BASIS_70_INDICATOR
                        from SET_OF_BOOKS
                       where SET_OF_BOOKS_ID = B.SET_OF_BOOKS_ID) POSTING_AMOUNT,
                     NVL(POSTING_QUANTITY, 0) POSTING_QUANTITY,
                     NVL(IMPAIRMENT_EXPENSE_AMOUNT, 0) IMPAIRMENT_EXPENSE_AMOUNT
                from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
               where A.COMPANY_ID = B.COMPANY_ID
                 and B.SET_OF_BOOKS_ID <> 1
                 and LOWER(trim(DECODE(A.FERC_ACTIVITY_CODE, 2, A.DESCRIPTION, 'not unretire'))) <> 'unretire'
                 and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                 and (PEND_TRANS_ID, SET_OF_BOOKS_ID) in
                     (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                        from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B
                       where A.COMPANY_ID = B.COMPANY_ID
                         and A.PEND_TRANS_ID = :NEW.PEND_TRANS_ID
                      minus
                      select PEND_TRANS_ID, SET_OF_BOOKS_ID
                        from PEND_TRANSACTION_SET_OF_BOOKS
                       where PEND_TRANS_ID = :NEW.PEND_TRANS_ID));
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (288, 0, 10, 4, 0, 0, 29215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029215_06_PEND_TRANS_ADD_SOB.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;