/*
||=============================================================================
|| Application: PowerPlant
|| File Name:   maint_043960_reg_fin_mon_enhance_ddl.sql
||=============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||=============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- -------------------------------------
|| 2015.2   08/11/2015   Shane "C" Ward   Financial Monitoring Improvements
||=============================================================================
*/

ALTER TABLE reg_jurisdiction ADD monitoring_flag NUMBER(22,0);
alter table reg_monitor_jur_case add last_approved_return_tag number(22,0) null;
alter table reg_monitor_jur_case add monitoring_return_tag number(22,0) null;

comment on column reg_jurisdiction.monitoring_Flag is 'Flag to mark a jurisdiction for financial monitoring and not associated with Jur Templates';
comment on column reg_monitor_jur_case.last_approved_return_tag is 'Return tag to be used by last approved case';
comment on column reg_monitor_jur_case.monitoring_return_tag is 'Return tag to be used by monitoring case in Financial Monitoring';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2771, 0, 2015, 2, 0, 0, 043960, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043960_reg_fin_mon_enhance_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;