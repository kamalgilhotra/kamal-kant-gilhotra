
 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_043531_prov_20151_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/06/2015 	Jarrett Skov   Adding FK on m_id to tax_accrual_m_current
 ||============================================================================
 */ 

DELETE FROM tax_accrual_m_current a
WHERE NOT EXISTS
(SELECT 1 FROM tax_accrual_control b WHERE a.m_id = b.m_id);

ALTER TABLE tax_accrual_m_current
ADD CONSTRAINT ta_m_curr_m_id FOREIGN KEY (
m_id
) REFERENCES tax_accrual_control (
m_id
)
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2473, 0, 2015, 1, 0, 0, 43531, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043531_prov_20151_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;