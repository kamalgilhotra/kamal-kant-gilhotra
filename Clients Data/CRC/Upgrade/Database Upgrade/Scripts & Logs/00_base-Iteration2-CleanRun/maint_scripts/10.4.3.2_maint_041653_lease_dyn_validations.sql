/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_041653_lease_dyn_validations.sql.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 12/29/2014 B.Beck   	 Add Dynamic Validation Types for Lease
||==========================================================================================
*/
insert into wo_validation_type
(
wo_validation_type_id, description, long_description,
"FUNCTION", 
find_company, 
col1, hard_edit
)
values
(
5000, 'Leased Asset', 'Leased Asset Save',
'uo_ls_assetcntr_wksp_details', 
'select company_id from ls_asset where ls_asset_id = <arg1>',
'ls_asset_id', 1
);

insert into wo_validation_type
(
wo_validation_type_id, description, long_description,
"FUNCTION", 
find_company, 
col1, hard_edit
)
values
(
5001, 'Leased ILRs', 'Leased ILRs Save',
'uo_ls_ilrcntr_wksp_details', 
'select company_id from ls_ilr where ilr_id = <arg1>',
'ilr_id', 1
);


insert into wo_validation_type
(
wo_validation_type_id, description, long_description,
"FUNCTION", 
find_company, 
col1, hard_edit
)
values
(
5002, 'Leased MLAs', 'Leased MLAs Save',
'uo_ls_lscntr_wksp_details', 
'select -1 from dual',
'lease_id', 1
);


commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2130, 0, 10, 4, 3, 2, 041653, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041653_lease_dyn_validations.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;