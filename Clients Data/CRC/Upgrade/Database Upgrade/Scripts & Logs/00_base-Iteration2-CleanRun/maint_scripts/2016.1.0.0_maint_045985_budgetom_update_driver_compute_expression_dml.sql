/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_045985_budgetom_update_driver_compute_expression_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 09/05/2016 Charlie Shilling update naming convention for computed fields on driver templates
||============================================================================
*/

UPDATE cr_budget_data_labor2_define
SET expression = REPLACE(Lower(expression), 'dw_compute_', 'computed_')
WHERE Lower(expression) LIKE '%dw_compute%'
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3273, 0, 2016, 1, 0, 0, 045985, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045985_budgetom_update_driver_compute_expression_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;