/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041230_cr_incr_alloc_conv_3.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   11/20/2014 M Allen, A Scott Incremental Allocations Setup conv 3
||                                      (both DML and DDL for conversion stuff).
|| 2015.1	12/04/2014 M Allen          Change to how the update handles 1 DDK
||                                      linked to 1+ summary record. Update 3 will break
||                                      if the # of detail records <> # summary records
|| 2015.1   02/09/2015 A Scott          delivered database will not have cr_cost_repository
||                                      place logic in pl/sql wrapper that only exucutes
||                                      if the table exists.
||============================================================================
*/ 

SET SERVEROUTPUT ON


declare
   l_cr_allo_found number := 0;
   l_step varchar2(35) := '0';
begin

   select count(*)
   into l_cr_allo_found
   from all_tables
   where lower(table_name) = 'cr_allocations';

   if l_cr_allo_found < 1 then
      DBMS_OUTPUT.PUT_LINE('Table cr allocations not found, no translation needed.');
   else
      DBMS_OUTPUT.PUT_LINE('Table cr allocations found. Starting translation.');

      l_step := '1';
      execute immediate 'create table D1CONV_CR_ALLOC_DDK_FIX AS'||
      ' select CR.ID             CR_ID,'||
      '       CR.DRILLDOWN_KEY  CR_DDK,'||
      '       CRA.DRILLDOWN_KEY ALLOC_DDK,'||
      '       CRA.ID            ALLOC_ID,'||
      '       CR.month_number   month_number'||
      '  from CR_COST_REPOSITORY CR, CR_ALLOCATIONS CRA'||
      ' where CR.DRILLDOWN_KEY = CRA.DRILLDOWN_KEY'||
      '   and UPPER(trim(CRA.TARGET_CREDIT)) in (''RVTARG'', ''RVCRED'', ''RVINCO'')'||
      '   and UPPER(SUBSTR(CRA.DRILLDOWN_KEY, -6, 2)) = ''RV'''||
      '   and CR.source_id = 1';

      l_step := '2';
      execute immediate 'alter table D1CONV_CR_ALLOC_DDK_FIX'||
      ' add constraint D1CONV_CR_ALLOC_DDK_FIX_PK'||
      ' primary key ( CR_ID, ALLOC_ID )'||
      ' using index tablespace pwrplant_idx';

      l_step := '3';
      execute immediate 'create index D1CONV_CR_ALLOC_DDK_FIX_IDX2'||
      ' on D1CONV_CR_ALLOC_DDK_FIX ( ALLOC_DDK )'||
      ' tablespace pwrplant_idx';

      l_step := '4';
      execute immediate 'create table D1CONV_CR_ALLOC_DDK_FIX2 as'||
      ' select CR.ID             CR_ID'||
      '       ,CR.DRILLDOWN_KEY  CR_DDK'||
      '       ,CRA.DRILLDOWN_KEY ALLOC_DDK'||
      '       ,CRA.ID            ALLOC_ID'||
      '       ,CR.MONTH_NUMBER   MONTH_NUMBER'||
      '       ,DENSE_RANK() OVER (PARTITION BY CR.DRILLDOWN_KEY ORDER BY CR.ID) AS CR_RANK'||
      '       ,DENSE_RANK() OVER (PARTITION BY CRA.DRILLDOWN_KEY ORDER BY CRA.ID) AS ALLOC_RANK'||
      '  from CR_COST_REPOSITORY CR, CR_ALLOCATIONS CRA'||
      ' where CR.DRILLDOWN_KEY = CRA.DRILLDOWN_KEY'||
      '  and CRA.drilldown_key in (select Z.drilldown_key from (select count(*),alloc_ddk drilldown_key,alloc_id from D1CONV_CR_ALLOC_DDK_FIX'||
      '      group by alloc_ddk,alloc_id,month_number'||
      '      having count(*) > 1'||
      '      ) Z)';
           
      l_step := '5'; 
      execute immediate 'delete from D1CONV_CR_ALLOC_DDK_FIX2'||
      ' where cr_rank <> alloc_rank';

      l_step := '6';
      execute immediate 'alter table D1CONV_CR_ALLOC_DDK_FIX2'||
      ' add constraint D1CONV_CR_ALLOC_DDK_FIX2_PK'||
      ' primary key ( CR_ID, ALLOC_ID )'||
      ' using index tablespace pwrplant_idx';

      l_step := '7';
      execute immediate 'create index D1CONV_CR_ALLOC_DDK_FIX2_IDX2'||
      ' on D1CONV_CR_ALLOC_DDK_FIX2 ( ALLOC_DDK )'||
      ' tablespace pwrplant_idx';

      l_step := '8';
      execute immediate 'delete from D1CONV_CR_ALLOC_DDK_FIX A'||
      ' where exists ('||
      '   select 1'||
      '   from D1CONV_CR_ALLOC_DDK_FIX2 B'||
      '   where A.cr_ddk = B.cr_ddk'||
      '   )';

      l_step := '9';
      execute immediate 'create unique index D1CONV_CR_ALLOC_DDK_FIX_IDX1'||
      ' on D1CONV_CR_ALLOC_DDK_FIX ( ALLOC_ID )'||
      ' tablespace pwrplant_idx';

      l_step := '10';
      execute immediate 'create unique index D1CONV_CR_ALLOC_DDK_FIX2_IDX1'||
      ' on D1CONV_CR_ALLOC_DDK_FIX2 ( ALLOC_ID )'||
      ' tablespace pwrplant_idx';

      l_step := '11';
      execute immediate 'update CR_COST_REPOSITORY A'||
      ' set A.DRILLDOWN_KEY = TO_CHAR(A.ID)'||
      ' where exists (select 1 from D1CONV_CR_ALLOC_DDK_FIX B where A.ID = B.CR_ID)'||
      ' or exists (select 1 from D1CONV_CR_ALLOC_DDK_FIX2 C where A.ID = C.CR_ID)';

      l_step := '12';
      execute immediate 'update CR_ALLOCATIONS A'||
      ' set A.DRILLDOWN_KEY ='||
      '      (select TO_CHAR(B.CR_ID) from D1CONV_CR_ALLOC_DDK_FIX B where A.ID = B.ALLOC_ID)'||
      ' where exists (select 1 from D1CONV_CR_ALLOC_DDK_FIX B where A.ID = B.ALLOC_ID)'||
      ' and A.TARGET_CREDIT in (''RVTARG'', ''RVCRED'', ''RVINCO'')';

      l_step := '13';
      execute immediate 'update CR_ALLOCATIONS A'||
      ' set A.DRILLDOWN_KEY ='||
      '      (select TO_CHAR(B.CR_ID) from D1CONV_CR_ALLOC_DDK_FIX2 B where A.ID = B.ALLOC_ID and B.cr_rank = B.alloc_rank)'||
      ' where exists (select 1 from D1CONV_CR_ALLOC_DDK_FIX2 B where A.ID = B.ALLOC_ID)'||
      ' and A.TARGET_CREDIT in (''RVTARG'', ''RVCRED'', ''RVINCO'')';

      l_step := '14';
      execute immediate 'DROP TABLE D1CONV_CR_ALLOC_DDK_FIX';

      l_step := '15';
      execute immediate 'DROP TABLE D1CONV_CR_ALLOC_DDK_FIX2';

      DBMS_OUTPUT.PUT_LINE('Finished translation successfully.');

   end if;

exception
   when others then
      DBMS_OUTPUT.PUT_LINE( SUBSTR('Error on translation step '||l_step|| ': ' || sqlerrm, 1, 2000) );
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2058, 0, 2015, 1, 0, 0, 41230, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041230_cr_incr_alloc_03_conv.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
