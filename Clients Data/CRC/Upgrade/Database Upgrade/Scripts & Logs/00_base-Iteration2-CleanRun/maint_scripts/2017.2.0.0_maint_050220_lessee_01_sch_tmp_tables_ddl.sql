/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050220_lessee_01_sch_tmp_tables_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.2.0.0 01/24/2018 Shane "C" Ward 	Lesseee Calculation Performance enhancements
||============================================================================
*/
CREATE global TEMPORARY TABLE LS_DEPR_INFO_TMP
  (
     ilr_id            NUMBER(22, 2),
     ls_asset_id       NUMBER(22, 2),
     set_of_books_id   NUMBER(22, 0),
     revision          NUMBER(22, 0),
     month             DATE,
     end_capital_cost  NUMBER(22, 2),
     depr_group_id     NUMBER(22, 2),
     economic_life     NUMBER(22, 2),
     set_of_books      VARCHAR2(35),
     mid_period_conv   NUMBER(22, 8),
     mid_period_method VARCHAR2(35),
     fasb_cap_type_id  NUMBER(22, 0)
  );

COMMENT ON TABLE LS_DEPR_INFO_TMP IS '(T)[06] The LS Depr Info Temp table holds depreciation related information for an asset / revision.'; 

COMMENT ON COLUMN LS_DEPR_INFO_TMP.ilr_id is 'The identifier of the ILR the asset is related to';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.ls_asset_id is 'The unique identifier for the Leased asset';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.set_of_books_id is 'The unique identifier for the set of books whose amounts we are considering';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.revision is 'The specific revision of the Asset/ILR';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.month is 'The month related to the row';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.end_capital_cost is 'The ending capital cost for the given asset/month';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.depr_group_id is 'The unique identifier of the Depreciation Group assigned to the asset';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.economic_life is 'The economic life of the asset';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.set_of_books is 'The description of the SOB for this record';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.mid_period_conv is 'The Mid-Period Convention for this asset''s depr group';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.mid_period_method is 'The Mid-Period Method for this asset''s depr group';
COMMENT ON COLUMN LS_DEPR_INFO_TMP.fasb_cap_type_id is 'The FASB Cap Type for this asset/SOB';

DROP TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG;

CREATE global TEMPORARY TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  (
     id                   NUMBER(22, 0),
     ilr_id               NUMBER(22, 0),
     revision             NUMBER(4, 0),
     ls_asset_id          NUMBER(22, 0),
     set_of_books_id      NUMBER(22, 0),
     month                DATE,
     amount               NUMBER(22, 2),
     residual_amount      NUMBER(22, 2),
     term_penalty         NUMBER(22, 2),
     bpo_price            NUMBER(22, 2),
     prepay_switch        NUMBER(1, 0),
     payment_month        NUMBER(1, 0),
     months_to_accrue     NUMBER(2, 0),
     rate                 NUMBER(22, 12),
     npv                  NUMBER(22, 12),
     current_lease_cost   NUMBER(22, 8),
     beg_capital_cost     NUMBER(22, 8),
     end_capital_cost     NUMBER(22, 8),
     beg_obligation       NUMBER(22, 8),
     end_obligation       NUMBER(22, 8),
     beg_lt_obligation    NUMBER(22, 8),
     end_lt_obligation    NUMBER(22, 8),
     beg_liability       NUMBER(22, 8),
     end_liability       NUMBER(22, 8),
     beg_lt_liability    NUMBER(22, 8),
     end_lt_liability    NUMBER(22, 8),
     unpaid_balance       NUMBER(22, 8),
     interest_accrual     NUMBER(22, 8),
     principal_accrual    NUMBER(22, 8),
     interest_paid        NUMBER(22, 8),
     principal_paid       NUMBER(22, 8),
     penny_rounder        NUMBER(22, 2),
     penny_prin_rounder   NUMBER(22, 2),
     penny_int_rounder    NUMBER(22, 2),
     penny_end_rounder    NUMBER(22, 2),
     principal_accrued    NUMBER(22, 8),
     interest_accrued     NUMBER(22, 8),
     months_accrued       NUMBER(4, 0),
     prin_round_accrued   NUMBER(22, 2),
     int_round_accrued    NUMBER(22, 2),
     executory_accrual1   NUMBER(22, 2),
     executory_accrual2   NUMBER(22, 2),
     executory_accrual3   NUMBER(22, 2),
     executory_accrual4   NUMBER(22, 2),
     executory_accrual5   NUMBER(22, 2),
     executory_accrual6   NUMBER(22, 2),
     executory_accrual7   NUMBER(22, 2),
     executory_accrual8   NUMBER(22, 2),
     executory_accrual9   NUMBER(22, 2),
     executory_accrual10  NUMBER(22, 2),
     executory_paid1      NUMBER(22, 2),
     executory_paid2      NUMBER(22, 2),
     executory_paid3      NUMBER(22, 2),
     executory_paid4      NUMBER(22, 2),
     executory_paid5      NUMBER(22, 2),
     executory_paid6      NUMBER(22, 2),
     executory_paid7      NUMBER(22, 2),
     executory_paid8      NUMBER(22, 2),
     executory_paid9      NUMBER(22, 2),
     executory_paid10     NUMBER(22, 2),
     contingent_accrual1  NUMBER(22, 2),
     contingent_accrual2  NUMBER(22, 2),
     contingent_accrual3  NUMBER(22, 2),
     contingent_accrual4  NUMBER(22, 2),
     contingent_accrual5  NUMBER(22, 2),
     contingent_accrual6  NUMBER(22, 2),
     contingent_accrual7  NUMBER(22, 2),
     contingent_accrual8  NUMBER(22, 2),
     contingent_accrual9  NUMBER(22, 2),
     contingent_accrual10 NUMBER(22, 2),
     contingent_paid1     NUMBER(22, 2),
     contingent_paid2     NUMBER(22, 2),
     contingent_paid3     NUMBER(22, 2),
     contingent_paid4     NUMBER(22, 2),
     contingent_paid5     NUMBER(22, 2),
     contingent_paid6     NUMBER(22, 2),
     contingent_paid7     NUMBER(22, 2),
     contingent_paid8     NUMBER(22, 2),
     contingent_paid9     NUMBER(22, 2),
     contingent_paid10    NUMBER(22, 2),
     is_om                NUMBER(1, 0),
     payment_term_type_id NUMBER(22, 0),
     beg_deferred_rent    NUMBER(22, 2),
     deferred_rent        NUMBER(22, 2),
     end_deferred_rent    NUMBER(22, 2),
     beg_st_deferred_rent NUMBER(22, 2),
     end_st_deferred_rent NUMBER(22, 2)
  )
ON COMMIT preserve ROWS;

COMMENT ON TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG IS '(T)[06] The LS ILR Asset Schedule Calc Stg table holds the schedule calculation results for an asset / revision.'; 

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.id IS 'An internal system generated primairy key.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.revision IS 'The revision.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.ls_asset_id IS 'The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.set_of_books_id IS 'The internal set of books id within PowerPlant.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.month IS 'The month being processed.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.amount IS 'The total payment amounts (Principal + Interest) for this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.residual_amount IS 'The guaranteed residual amount.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.term_penalty IS 'The termination penalty amount.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.bpo_price IS 'The bargain purchase amount.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.prepay_switch IS 'A flag identifying whether or not interest is calculated in the first month of a lease.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.payment_month IS '1 means it is a payment month.  2 means it is the last payment month.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.months_to_accrue IS 'The number of months to accrue.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.rate IS 'The discount rate used to determine Net Present Value.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.npv IS 'The net present value.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.current_lease_cost IS 'The fair market value.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_capital_cost IS 'The capital amount.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_obligation IS 'The beginning obligation for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_obligation IS 'The ending obligation for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_lt_obligation IS 'The beginning long term obligation for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_lt_obligation IS 'The ending long term obligation for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.unpaid_balance IS 'A column representing the unpaid balance of a lease.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.interest_accrual IS 'The amount of interest accrued in this period .';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.principal_accrual IS 'The amount of principal accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.interest_paid IS 'The amount of interest paid in this period .';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.principal_paid IS 'The amount of principal paid in this period.  This will cause a reduction in the obligation.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.penny_rounder IS 'This columns is used to track penny rounding differences between ending obligation and the beginning obligation minus principal plaid amounts  .';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.penny_prin_rounder IS 'This columns is used to store any rounding associated with non monthly payments where sum(principal accrued) <> principal payment.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.penny_int_rounder IS 'This columns is used to store any rounding associated with non monthly payments where sum(interest accrued) <> interest payment.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.penny_end_rounder IS 'This column is used to track penny rounding issues that cause the end obligation to not equal the residual value + bpo + termination penalty.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.principal_accrued IS 'The amount of principal accrued this month.  No JEs get created from this result.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.interest_accrued IS 'The amount of interest accrued.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.months_accrued IS 'The number of months accrued in this payment term';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual1 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual2 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual3 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual4 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual5 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual6 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual7 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual8 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual9 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_accrual10 IS 'The amount of executory costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid1 IS 'The executory paid amount associated with the executory bucket number 1.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid2 IS 'The executory paid amount associated with the executory bucket number 2.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid3 IS 'The executory paid amount associated with the executory bucket number 3.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid4 IS 'The executory paid amount associated with the executory bucket number 4.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid5 IS 'The executory paid amount associated with the executory bucket number 5.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid6 IS 'The executory paid amount associated with the executory bucket number 6.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid7 IS 'The executory paid amount associated with the executory bucket number 7.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid8 IS 'The executory paid amount associated with the executory bucket number 8.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid9 IS 'The executory paid amount associated with the executory bucket number 9.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.executory_paid10 IS 'The executory paid amount associated with the executory bucket number 10.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual1 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual2 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual3 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual4 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual5 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual6 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual7 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual8 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual9 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_accrual10 IS 'The amount of contingent costs accrued in this period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid1 IS 'The contingent paid amount associated with the contingent bucket number 1.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid2 IS 'The contingent paid amount associated with the contingent bucket number 2.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid3 IS 'The contingent paid amount associated with the contingent bucket number 3.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid4 IS 'The contingent paid amount associated with the contingent bucket number 4.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid5 IS 'The contingent paid amount associated with the contingent bucket number 5.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid6 IS 'The contingent paid amount associated with the contingent bucket number 6.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid7 IS 'The contingent paid amount associated with the contingent bucket number 7.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid8 IS 'The contingent paid amount associated with the contingent bucket number 8.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid9 IS 'The contingent paid amount associated with the contingent bucket number 9.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.contingent_paid10 IS 'The contingent paid amount associated with the contingent bucket number 10.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.is_om IS '1 means O and M.  0 means Capital.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.payment_term_type_id IS 'The type of term.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_deferred_rent IS 'The beginning deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.deferred_rent IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_deferred_rent IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_st_deferred_rent IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_st_deferred_rent IS 'The ending short term deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_liability IS 'The deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_liability IS 'The ending deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.beg_lt_liability IS 'The beginning short term deferred rent amount for the period.';
COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.end_lt_liability IS 'The ending short term deferred rent amount for the period.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4089, 0, 2017, 2, 0, 0, 50220, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050220_lessee_01_sch_tmp_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;