/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044004_reg_rename_layer_menu_items_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/03/2015 Sarah Byers    Rename the IFA menu items to Layer
||============================================================================
*/

-- Rename the parent menu item
update ppbase_menu_items
	set label = 'Regulatory Layers',
		 minihelp = 'Regulatory Layers'
 where menu_identifier = 'IFA_TOP';

-- Rename IFA - Setup to Layer Setup
update ppbase_menu_items
	set label = 'Layer Setup',
		 minihelp = 'Layer Setup'
 where menu_identifier = 'IFA_SETUP';

update ppbase_workspace
	set label = 'Layer - Setup',
		 minihelp = 'Layer - Setup'
 where workspace_identifier = 'uo_reg_inc_setup';

-- Rename IFA - FP Select to Create - FP Adjustment
update ppbase_menu_items
	set label = 'Create - FP Adjustment',
		 minihelp = 'Create Layer - FP Adjustment'
 where menu_identifier = 'IFA_SELECT';

update ppbase_workspace
	set label = 'Create - FP Adjustment',
		 minihelp = 'Create - FP Adjustment'
 where workspace_identifier = 'uo_reg_inc_adj_ws';

-- Rename IFA - FP Snapshot to Create - FP Snapshot
update ppbase_menu_items
	set label = 'Create - FP Snapshot',
		 minihelp = 'Create Layer - FP Snapshot'
 where menu_identifier = 'IFA_SNAP';

update ppbase_workspace
	set label = 'Create - FP Snapshot',
		 minihelp = 'Create Layer - FP Snapshot'
 where workspace_identifier = 'uo_reg_inc_fp_snap_ws';

-- Rename IFA - Depr Select to Create - Depr Adjustment
update ppbase_menu_items
	set label = 'Create - Depr Adjustment',
		 minihelp = 'Create Layer - Depr Adjustment'
 where menu_identifier = 'ifa_depr_select';

update ppbase_workspace
	set label = 'Create - Depr Adjustment',
		 minihelp = 'Create Layer - Depr Adjustment'
 where workspace_identifier = 'uo_reg_fcst_depr_inc_adj_ws';

-- Rename IFA - Review to Layer Review
update ppbase_menu_items
	set label = 'Layer Review',
		 minihelp = 'Layer Review'
 where menu_identifier = 'IFA_REVIEW';

update ppbase_workspace
	set label = 'Layer Review',
		 minihelp = 'Layer Review'
 where workspace_identifier = 'uo_reg_inc_ifa_fp_review';

-- Rename IFA - Groups to Layer Groups
update ppbase_menu_items
	set label = 'Layer Groups',
		 minihelp = 'Layer Groups'
 where menu_identifier = 'IFA_GROUP';

update ppbase_workspace
	set label = 'Layer Groups',
		 minihelp = 'Layer Groups'
 where workspace_identifier = 'uo_reg_inc_group_ws';

-- Rename Find Assets to Create - Existing Assets
update ppbase_menu_items
	set label = 'Create - Existing Assets',
		 minihelp = 'Create Layer - Existing Assets'
 where menu_identifier = 'FIND_ASSETS';

update ppbase_workspace
	set label = 'Create - Existing Assets',
		 minihelp = 'Create Layer - Existing Assets'
 where workspace_identifier = 'uo_reg_inc_adj_find_assets';

-- Change the order
update ppbase_menu_items
	set item_order = decode(menu_identifier, 'IFA_SETUP', 7,
														  'IFA_SELECT', 2,
														  'IFA_SNAP', 3,
														  'ifa_depr_select', 5,
														  'IFA_REVIEW', 1,
														  'IFA_GROUP', 6,
														  'FIND_ASSETS', 4)
 where parent_menu_identifier = 'IFA_TOP';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2573, 0, 2015, 2, 0, 0, 044004, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044004_reg_rename_layer_menu_items_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;