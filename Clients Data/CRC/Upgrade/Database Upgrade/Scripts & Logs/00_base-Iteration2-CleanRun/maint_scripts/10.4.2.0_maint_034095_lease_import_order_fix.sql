/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034095_lease_import_order_fix.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/17/2014 Kyle Peterson
||============================================================================
*/

update PP_IMPORT_COLUMN
   set PROCESSING_ORDER = 2
 where COLUMN_NAME = 'sub_account_id'
   and IMPORT_TYPE_ID =
       (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: Leased A%');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (868, 0, 10, 4, 2, 0, 34095, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034095_lease_import_order_fix.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;