/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036968_pwrtax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/07/2014 Julia Breuer
||============================================================================
*/

--
-- Add the ability to search by Asset Description in the PowerTax grid.
--
insert into PWRPLANT.PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, REQUIRED,
    SINGLE_SELECT_ONLY, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE, INVISIBLE, USER_ID, TIME_STAMP)
values
   (94, 'Asset Description', 'sle', 'cpr_ledger.description',
    'tax_record_control.asset_id in (select asset_id from cpr_ledger where [selected_values])', 0, 0,
    0, 0, 0, user, sysdate);
insert into PWRPLANT.PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (49, 94, user, sysdate);

--
-- Update how the other CPR Ledger filters work in the PowerTax grid.
--
update PWRPLANT.PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'cpr_ledger.asset_location_id',
       SQLS_WHERE_CLAUSE = 'tax_record_control.asset_id in (select asset_id from cpr_ledger where [selected_values])'
 where FILTER_ID = 95;

update PWRPLANT.PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'cpr_ledger.bus_segment_id',
       SQLS_WHERE_CLAUSE = 'tax_record_control.asset_id in (select asset_id from cpr_ledger where [selected_values])'
 where FILTER_ID = 96;

update PWRPLANT.PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'cpr_ledger.utility_account_id',
       SQLS_WHERE_CLAUSE = 'tax_record_control.asset_id in (select asset_id from cpr_ledger where [selected_values])'
 where FILTER_ID = 97;

update PWRPLANT.PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'cpr_ledger.sub_account_id',
       SQLS_WHERE_CLAUSE = 'tax_record_control.asset_id in (select asset_id from cpr_ledger where [selected_values])'
 where FILTER_ID = 98;

update PWRPLANT.PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'tax_class_rollups.tax_rollup_detail_id',
       SQLS_WHERE_CLAUSE = 'tax_record_control.tax_class_id in (select distinct tax_class_id from tax_class_rollups where [selected_values])'
 where FILTER_ID = 110;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1014, 0, 10, 4, 2, 0, 36968, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036968_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;