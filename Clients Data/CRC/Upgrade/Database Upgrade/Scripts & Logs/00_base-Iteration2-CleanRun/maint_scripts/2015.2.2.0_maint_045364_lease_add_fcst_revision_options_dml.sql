/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046364_lease_add_fcst_revision_options_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 02/10/2016 Charlie Shilling Add actions drop down on forecast workspace
||============================================================================
*/

INSERT INTO ppbase_actions_windows (id, MODULE, action_identifier, action_text, action_order, action_event)
SELECT Nvl(Max(id) + 1, 1), 'LESSEE', 'build_revision', 'Build Revision', 1, 'ue_buildrevision'
FROM ppbase_actions_windows;

INSERT INTO ppbase_actions_windows (id, MODULE, action_identifier, action_text, action_order, action_event)
SELECT Nvl(Max(id) + 1, 1), 'LESSEE', 'remove_detail', 'Remove Detail', 2, 'ue_removedetail'
FROM ppbase_actions_windows;

INSERT INTO ppbase_actions_windows (id, MODULE, action_identifier, action_text, action_order, action_event)
SELECT Nvl(Max(id) + 1, 1), 'LESSEE', 'delete_all_schedules', 'Delete All', 3, 'ue_deleteallschedules'
FROM ppbase_actions_windows;

INSERT INTO ppbase_actions_windows (id, MODULE, action_identifier, action_text, action_order, action_event)
SELECT Nvl(Max(id) + 1, 1), 'LESSEE', 'transition_fcst', 'Put In-Service', 4, 'ue_transitionfcst'
FROM ppbase_actions_windows;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3054, 0, 2016, 1, 0, 0, 045364, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045364_lease_add_fcst_revision_options_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;