/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036068_reg_fed_state_tax_accts.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/03/2014 Sarah Byers
||============================================================================
*/

-- update sub acct type descriptions
update REG_SUB_ACCT_TYPE
   set DESCRIPTION = 'Rate Base Tax Effect (Federal)'
 where REG_ACCT_TYPE_ID = 11
   and SUB_ACCT_TYPE_ID = 1;

update REG_SUB_ACCT_TYPE
   set DESCRIPTION = 'Operating Income Tax Effect (Fed)'
 where REG_ACCT_TYPE_ID = 11
   and SUB_ACCT_TYPE_ID = 2;

update REG_SUB_ACCT_TYPE
   set DESCRIPTION = 'Rate Base Tax Effect (State)'
 where REG_ACCT_TYPE_ID = 11
   and SUB_ACCT_TYPE_ID = 3;

update REG_SUB_ACCT_TYPE
   set DESCRIPTION = 'Operating Income Tax Effect (State)'
 where REG_ACCT_TYPE_ID = 11
   and SUB_ACCT_TYPE_ID = 4;

update REG_SUB_ACCT_TYPE
   set LONG_DESCRIPTION = DESCRIPTION
 where REG_ACCT_TYPE_ID = 11
   and SUB_ACCT_TYPE_ID between 1 and 4;

-- update account descriptions
update REG_ACCT_MASTER
   set DESCRIPTION = 'Federal Calc Tax Effect: Rate Base',
       LONG_DESCRIPTION = 'Calculated Tax Effect for Federal Rate Base'
 where REG_ACCT_TYPE_DEFAULT = 11
   and SUB_ACCT_TYPE_ID = 1;

update REG_ACCT_MASTER
   set DESCRIPTION = 'Federal Calc Tax Effect: Operating Income',
       LONG_DESCRIPTION = 'Calculated Tax Effect for Federal Operating Income'
 where REG_ACCT_TYPE_DEFAULT = 11
   and SUB_ACCT_TYPE_ID = 2;

update REG_ACCT_MASTER
   set DESCRIPTION = 'State Calc Tax Effect: Rate Base',
       LONG_DESCRIPTION = 'Calculated Tax Effect for State Rate Base'
 where REG_ACCT_TYPE_DEFAULT = 11
   and SUB_ACCT_TYPE_ID = 3;

update REG_ACCT_MASTER
   set DESCRIPTION = 'State Calc Tax Effect: Operating Income',
       LONG_DESCRIPTION = 'Calculated Tax Effect for State Operating Income'
 where REG_ACCT_TYPE_DEFAULT = 11
   and SUB_ACCT_TYPE_ID = 4;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (938, 0, 10, 4, 2, 0, 36068, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036068_reg_fed_state_tax_accts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;