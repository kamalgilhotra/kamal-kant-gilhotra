/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040043_taxrpr_import_rpr_schema.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/25/2014 Alex P.
||============================================================================
*/

alter table RPR_IMPORT_SCHEMA add CPR_ONLY                number(1,0);
alter table RPR_IMPORT_SCHEMA add PROCESSING_PERIOD_ID    number(22,0);
alter table RPR_IMPORT_SCHEMA add PROCESSING_PERIOD_XLATE varchar2(254);

comment on column RPR_IMPORT_SCHEMA.CPR_ONLY is 'Numeric identifier defining whether the processing starts at Construction(0), Non-Unitization(1), or Unitization(2)';
comment on column RPR_IMPORT_SCHEMA.PROCESSING_PERIOD_ID is 'Numeric identifier defining whether the processing is performed for a specific Date Range(1) or Life-to_Date(2).';
comment on column RPR_IMPORT_SCHEMA.PROCESSING_PERIOD_XLATE is 'Value from the import file translated to obtain processing_period_id.';

alter table RPR_IMPORT_SCHEMA_ARC add CPR_ONLY                number(1,0);
alter table RPR_IMPORT_SCHEMA_ARC add PROCESSING_PERIOD_ID    number(22,0);
alter table RPR_IMPORT_SCHEMA_ARC add PROCESSING_PERIOD_XLATE varchar2(254);

comment on column RPR_IMPORT_SCHEMA_ARC.CPR_ONLY is 'Numeric identifier defining whether the processing starts at Construction(0), Non-Unitization(1), or Unitization(2)';
comment on column RPR_IMPORT_SCHEMA_ARC.PROCESSING_PERIOD_ID is 'Numeric identifier defining whether the processing is performed for a specific Date Range(1) or Life-to_Date(2).';
comment on column RPR_IMPORT_SCHEMA_ARC.PROCESSING_PERIOD_XLATE is 'Value from the import file translated to obtain processing_period_id.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (104, 'cpr_only', 'Deduct Starting In', '', 0, 1, 'number(1,0)',
    'repair_schema', 1, 'repair_schema_id');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (104, 'processing_period_id', 'Processing Period', 'processing_period_xlate', 1, 1, 'number(22,0)',
    'repair_schema', 1, 'repair_schema_id');

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS)
values
   (1073, sysdate, user, 'Processing Period Description',
    'The passed in value corresponds to the Processing Period Identifier.  Translate to the ID using the Repair Processing Period table.',
    'processing_period_id',
    '( select r.processing_period_id from repair_processing_period r where upper( trim( <importfield> ) ) = upper( trim( r.description ) ) )',
    0, 'repair_processing_period', 'description', '');


insert into PP_IMPORT_COLUMN_LOOKUP( import_type_id, Column_Name, Import_Lookup_Id )
values( 104, 'processing_period_id', 1073 );

insert into PP_IMPORT_TEMPLATE_FIELDS( Import_Template_Id, Field_Id, import_type_id, Column_Name, Import_Lookup_Id )
select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, 104, 'cpr_only', null
     from PP_IMPORT_TEMPLATE
    where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104
      and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add';

insert into PP_IMPORT_TEMPLATE_FIELDS( Import_Template_Id, Field_Id, import_type_id, Column_Name, Import_Lookup_Id )
select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, 104, 'cpr_only', null
     from PP_IMPORT_TEMPLATE
    where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104
      and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add/Update';

insert into PP_IMPORT_TEMPLATE_FIELDS( Import_Template_Id, Field_Id, import_type_id, Column_Name, Import_Lookup_Id )
select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 6, 104, 'processing_period_id', 1073
     from PP_IMPORT_TEMPLATE
    where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104
      and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add';

insert into PP_IMPORT_TEMPLATE_FIELDS( Import_Template_Id, Field_Id, import_type_id, Column_Name, Import_Lookup_Id )
select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 6, 104, 'processing_period_id', 1073
     from PP_IMPORT_TEMPLATE
    where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104
      and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add/Update';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1460, 0, 10, 4, 3, 0, 40043, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040043_taxrpr_import_rpr_schema.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;