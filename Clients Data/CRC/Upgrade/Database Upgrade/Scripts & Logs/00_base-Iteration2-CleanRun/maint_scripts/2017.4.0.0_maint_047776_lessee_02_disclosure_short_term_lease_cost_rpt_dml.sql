/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047776_lessee_02_disclosure_short_term_lease_cost_rpt_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  04/26/2018 David Conway     Update time option for MC
||============================================================================
*/

update pp_reports
   set pp_report_time_option_id = 208
 where report_number = 'Lessee - 2008';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4864, 0, 2017, 4, 0, 0, 47776, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_047776_lessee_02_disclosure_short_term_lease_cost_rpt_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;