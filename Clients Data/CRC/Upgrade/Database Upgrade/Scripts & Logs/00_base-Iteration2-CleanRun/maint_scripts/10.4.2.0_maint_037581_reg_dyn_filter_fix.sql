/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037581_reg_dyn_filter_fix.sql  (part of epic 9415)
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.2.0 04/07/2014 Shane "C" Ward Fix Dynamic Filter and update Workspace menu item
||=================================================================================
*/

update PP_DYNAMIC_FILTER
   set DW_ID = 'reg_case_acct_reg_acct_id', DW_DESCRIPTION = 'reg_acct_master_description'
 where DW_ID = 'reg_case_acct.reg_acct_id';

update PPBASE_WORKSPACE
   set LABEL = 'History Ledger Adj'
 where LABEL like 'History Ledger Adjustment%';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1101, 0, 10, 4, 2, 0, 37581, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037581_reg_dyn_filter_fix.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;