/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008689_cpr.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/28/2011 Elhadj Bah   Point Release
||============================================================================
*/

create table REPAIR_WORK_ORDER_TEMP_ORIG
(
 COMPANY_ID          number(22,0) not null,
 WORK_ORDER_NUMBER   varchar2(35) not null,
 REPAIR_UNIT_CODE_ID number(22,0) not null,
 REPAIR_LOCATION_ID  number(22,0) not null,
 VINTAGE             number(22,0) not null,
 FERC_ACTIVITY_CODE  number(22,0) not null,
 REPAIR_METHOD_ID    number(22,0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 QUANTITY            number(22,0),
 COST                number(22,2),
 UTILITY_ACCOUNT_ID  number(22,0) not null,
 BUS_SEGMENT_ID      number(22,0) not null,
 RETIREMENT_UNIT_ID  number(22,0) not null,
 OVH_TO_UDG          number(22,0) default 0
);

alter table REPAIR_WORK_ORDER_TEMP_ORIG
   add constraint PK_REPAIR_WORK_ORDER_TEMP_ORIG
       primary key (COMPANY_ID, WORK_ORDER_NUMBER, REPAIR_UNIT_CODE_ID,
                    REPAIR_LOCATION_ID, VINTAGE, FERC_ACTIVITY_CODE,
                    REPAIR_METHOD_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID,
                    RETIREMENT_UNIT_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (54, 0, 10, 3, 3, 0, 8689, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008689_cpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

