/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049514_lessor_01_rename_payments_to_invoices_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/26/2017 JSisouphanh      Rename menu items
||============================================================================
*/

INSERT INTO ppbase_workspace (MODULE, workspace_identifier, label, workspace_uo_name, object_type_id)
VALUES('LESSOR', 'approval_invoices', 'Invoices', 'uo_lsr_invoicecntr_wksp_approvals_open', '1');

update ppbase_menu_items
set menu_identifier      = 'approval_invoices',
	label = 'Invoices',
    workspace_identifier = 'approval_invoices'
where module = 'LESSOR'
and menu_identifier = 'approval_payments'
;

DELETE FROM ppbase_workspace
WHERE MODULE = 'LESSOR'
AND workspace_identifier = 'approval_payments';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3877, 0, 2017, 1, 0, 0, 49514, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049514_lessor_01_rename_payments_to_invoices_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
