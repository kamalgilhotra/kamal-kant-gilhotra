/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_06_add_idc_incentive_schedule_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/25/2018 Shane "C" Ward   Add IDC and Incentives to Schedule and Calc tables
||============================================================================
*/
--Begin: Add IDC and Initial Direct Cost columns to calc tables
ALTER TABLE LS_ILR_SCHEDULE
  ADD initial_direct_cost NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_SCHEDULE
  ADD incentive_amount NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_SCHEDULE.initial_direct_cost IS 'Holds Initial Direct Cost Amount for ILR Schedule.';

COMMENT ON COLUMN LS_ILR_SCHEDULE.incentive_amount IS 'Holds Incentive Amount for ILR Schedule.';

ALTER TABLE LS_ASSET_SCHEDULE
  ADD initial_direct_cost NUMBER(22, 2) NULL;

ALTER TABLE LS_ASSET_SCHEDULE
  ADD incentive_amount NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ASSET_SCHEDULE.initial_direct_cost IS 'Holds Initial Direct Cost Amount for Asset Schedule.';

COMMENT ON COLUMN LS_ASSET_SCHEDULE.incentive_amount IS 'Holds Incentive Amount for Asset Schedule.';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD initial_direct_cost NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD incentive_amount NUMBER(22, 2) NULL;
  
ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD incentive_math_amount NUMBER(22, 2) NULL;
  
ALTER TABLE LS_ILR_ASSET_SCHEDULE_STG
  ADD idc_math_amount NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.initial_direct_cost IS 'Holds Initial Direct Cost Amount for ILR Schedule.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.incentive_amount IS 'Holds Incentive Amount for ILR Schedule.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.incentive_math_amount IS 'Holds Incentive Amount used for Application of Incentives to Schedule Calc.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_STG.idc_math_amount IS 'Holds Initial Direct Cost Amount used for Application of IDC to Schedule Calc.';

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD initial_direct_cost NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD incentive_amount NUMBER(22, 2) NULL;
  
ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD incentive_math_amount NUMBER(22, 2) NULL;
  
ALTER TABLE LS_ILR_ASSET_SCHEDULE_CALC_STG
  ADD idc_math_amount NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.initial_direct_cost IS 'Holds Initial Direct Cost Amount for ILR Schedule.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.incentive_amount IS 'Holds Incentive Amount for ILR Schedule.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.incentive_math_amount IS 'Holds Incentive Amount used for Application of Incentives occuring within Payment Terms.';

COMMENT ON COLUMN LS_ILR_ASSET_SCHEDULE_CALC_STG.idc_math_amount IS 'Holds Initial Direct Cost Amount used for Application of IDC to Schedule Calc.';

ALTER TABLE LS_ILR_SCHEDULE_STG
  ADD initial_direct_cost NUMBER(22, 2) NULL;

ALTER TABLE LS_ILR_SCHEDULE_STG
  ADD incentive_amount NUMBER(22, 2) NULL;
  
ALTER TABLE LS_ILR_SCHEDULE_STG
  ADD incentive_math_amount NUMBER(22, 2) NULL;
  
ALTER TABLE LS_ILR_SCHEDULE_STG
  ADD idc_math_amount NUMBER(22, 2) NULL;

COMMENT ON COLUMN LS_ILR_SCHEDULE_STG.initial_direct_cost IS 'Holds Initial Direct Cost Amount for ILR Schedule.';

COMMENT ON COLUMN LS_ILR_SCHEDULE_STG.incentive_amount IS 'Holds Incentive Amount for ILR Schedule.';

COMMENT ON COLUMN LS_ILR_SCHEDULE_STG.incentive_math_amount IS 'Holds Incentive Amount used for Application of Incentives occuring within Payment Terms.';

COMMENT ON COLUMN LS_ILR_SCHEDULE_STG.idc_math_amount IS 'Holds Initial Direct Cost Amount used for Application of IDC to Schedule Calc.';
--End: Add IDC and Initial Direct Cost columns to calc tables

--Begin: Setup for new GL Accounts to be associated with Incentives and IDC
ALTER TABLE LS_ILR_ACCOUNT
  ADD incentive_account_id NUMBER(22);

COMMENT ON COLUMN LS_ILR_ACCOUNT.incentive_account_id IS 'The Incentive account.';

ALTER TABLE LS_ILR_ACCOUNT
  ADD CONSTRAINT fk_ls_ilr_account_incentive FOREIGN KEY ( incentive_account_id ) REFERENCES GL_ACCOUNT ( gl_account_id );

ALTER TABLE LS_ILR_ACCOUNT
  ADD init_direct_cost_account_id NUMBER(22);

COMMENT ON COLUMN LS_ILR_ACCOUNT.init_direct_cost_account_id IS 'The Initial Direct Cost account.';

ALTER TABLE LS_ILR_ACCOUNT
  ADD CONSTRAINT fk_ls_ilr_account_idc FOREIGN KEY ( init_direct_cost_account_id ) REFERENCES GL_ACCOUNT ( gl_account_id );

ALTER TABLE LS_ILR_GROUP
  ADD incentive_account_id NUMBER(22);

COMMENT ON COLUMN LS_ILR_GROUP.incentive_account_id IS 'The Incentive account.';

ALTER TABLE LS_ILR_GROUP
  ADD CONSTRAINT fk_ls_ilr_group_incentive FOREIGN KEY ( incentive_account_id ) REFERENCES GL_ACCOUNT ( gl_account_id );

ALTER TABLE LS_ILR_GROUP
  ADD init_direct_cost_account_id NUMBER(22);

COMMENT ON COLUMN LS_ILR_GROUP.init_direct_cost_account_id IS 'The Initial Direct Cost account.';

ALTER TABLE LS_ILR_GROUP
  ADD CONSTRAINT fk_ls_ilr_group_idc FOREIGN KEY ( init_direct_cost_account_id ) REFERENCES GL_ACCOUNT ( gl_account_id );
--End: Setup for new GL Accounts to be associated with Incentives and IDC

--Begin: Staging tables used in calcs for IDC and Incentives

CREATE global TEMPORARY TABLE LS_ILR_IDC_STG
  (
     ilr_id              NUMBER(22, 0),
     revision            NUMBER(22, 0),
     initial_direct_cost NUMBER(22, 2),
     date_incurred       DATE,
     user_id             VARCHAR2(18),
     time_stamp          DATE
  )
ON COMMIT DELETE ROWS;

CREATE global TEMPORARY TABLE LS_ILR_INC_STG
  (
     ilr_id           NUMBER(22, 0),
     revision         NUMBER(22, 0),
     incentive_amount NUMBER(22, 2),
     date_incurred    DATE,
     user_id          VARCHAR2(18),
     time_stamp       DATE
  )
ON COMMIT DELETE ROWS; 
--End: Staging tables used in calcs for IDC and Incentives

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4292, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_06_add_idc_incentive_schedule_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;