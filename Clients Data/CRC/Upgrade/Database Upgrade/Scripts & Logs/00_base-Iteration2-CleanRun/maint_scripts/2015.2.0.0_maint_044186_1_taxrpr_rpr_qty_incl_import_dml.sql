/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044186_1_taxrpr_rpr_qty_incl_import_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.2   07/14/2015 Anand R        update column ALLOW_UPDATES_ON_ADD to 0
||============================================================================
*/

--update column ALLOW_UPDATES_ON_ADD to 0
update PP_IMPORT_TYPE set ALLOW_UPDATES_ON_ADD = 0 where IMPORT_TYPE_ID = 121;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2693, 0, 2015, 2, 0, 0, 044186, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044186_1_taxrpr_rpr_qty_incl_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;