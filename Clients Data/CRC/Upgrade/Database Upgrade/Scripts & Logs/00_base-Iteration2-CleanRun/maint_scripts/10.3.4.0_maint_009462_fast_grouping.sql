/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009462_fast_grouping.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/02/2012 Sunjin Cone    Point Release
||============================================================================
*/

-- * Drop the table if it already exists
SET SERVEROUTPUT ON

declare
   COUNTER number;

begin
   select count(*)
     into COUNTER
     from ALL_TABLES
    where OWNER = 'PWRPLANT'
      and TABLE_NAME = 'WORK_ORDER_CHARGE_GROUP_TEMP';

   if COUNTER > 0 then
      execute immediate 'drop table WORK_ORDER_CHARGE_GROUP_TEMP';
      DBMS_OUTPUT.PUT_LINE('Table WORK_ORDER_CHARGE_GROUP_TEMP dropped.');
   else
      DBMS_OUTPUT.PUT_LINE('Table WORK_ORDER_CHARGE_GROUP_TEMP did not exist.');
   end if;
end;
/

create global temporary table WORK_ORDER_CHARGE_GROUP_TEMP
(
 WORK_ORDER_ID        number(22,0) not null,
 CHARGE_ID            number(22,0) not null,
 CHARGE_GROUP_ID      number(22,0),
 ALLOC_METHOD_TYPE_ID number(22,0),
 CHARGE_TYPE_ID       number(22,0),
 TIME_STAMP           date,
 USER_ID              varchar2(18)
) on commit preserve rows;

alter table WORK_ORDER_CHARGE_GROUP_TEMP
   add constraint WORK_ORDER_CHARGE_GROUP_TMPPK
       primary key (CHARGE_ID);

update PP_SYSTEM_CONTROL_COMPANY
   set CONTROL_VALUE = 'yes'
 where UPPER(CONTROL_NAME) = 'PREGROUP CHARGES';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (103, 0, 10, 3, 4, 0, 9462, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009462_fast_grouping.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
