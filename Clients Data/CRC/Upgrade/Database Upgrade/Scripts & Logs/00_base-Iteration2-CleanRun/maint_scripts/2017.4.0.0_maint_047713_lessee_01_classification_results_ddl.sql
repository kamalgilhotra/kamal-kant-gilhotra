/*
||============================================================================
|| Application: PowerPlan  Disclosure Reports - ILR Descriptions
|| File Name:   maint_047713_lessee_01_classification_results_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/14/2018 Sarah Byers      Add new table to store classfication results
||============================================================================
*/

create table ls_ilr_classification_test (
ilr_id number(22,0) not null,
revision number(22,0) not null,
user_id varchar2(18),
time_stamp date,
finance_tests number(22,0),
msg_finance_tests varchar2(1000),
economic_life number(22,0),
msg_economic_life varchar2(1000),
net_present_value number(22,0),
msg_net_present_value varchar2(1000),
transfer_of_ownership number(22,0),
msg_transfer_of_ownership varchar2(1000),
intent_to_purchase number(22,0),
msg_intent_to_purchase varchar2(1000),
specialized_asset number(22,0),
msg_specialized_asset varchar2(1000));

alter table ls_ilr_classification_test
   add constraint pk_ls_ilr_classification_test
       primary key (ilr_id, revision)
       using index tablespace PWRPLANT_IDX;

alter table ls_ilr_classification_test
   add constraint fk_ls_ilr_classification_test1
       foreign key (ilr_id, revision)
       references ls_ilr_approval;

comment on table ls_ilr_classification_test is '(C) [06] The LS ILR Classification Test table stores the results of an ILR Revision Classification Test.';
comment on column ls_ilr_classification_test.ilr_id is 'System assigned identifier of an ILR';
comment on column ls_ilr_classification_test.revision is 'System assigned identifier of an ILR revision';
comment on column ls_ilr_classification_test.user_id is 'Standard system-assigned user_id used for audit purposes.';
comment on column ls_ilr_classification_test.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_ilr_classification_test.finance_tests is 'Indicates whether the ILR passed any of the tests. 1: Pass, 0: Fail.';
comment on column ls_ilr_classification_test.msg_finance_tests is 'Message related to the Finance Tests result';
comment on column ls_ilr_classification_test.economic_life is 'Indicates whether the ILR passed the Economic Life Test. 1: Pass, 0: Fail.';
comment on column ls_ilr_classification_test.msg_economic_life is 'Message related to the Economic Life Test result';
comment on column ls_ilr_classification_test.net_present_value is 'Indicates whether the ILR passed the Net Present Value Test. 1: Pass, 0: Fail.';
comment on column ls_ilr_classification_test.msg_net_present_value is 'Message related to the Net Present Value Test result';
comment on column ls_ilr_classification_test.transfer_of_ownership is 'Indicates whether the ILR passed the Transfer of Ownership Test. 1: Pass, 0: Fail.';
comment on column ls_ilr_classification_test.msg_transfer_of_ownership is 'Message related to the Transfer of Ownership Test result';
comment on column ls_ilr_classification_test.intent_to_purchase is 'Indicates whether the ILR passed the Intent to Purchase Test. 1: Pass, 0: Fail.';
comment on column ls_ilr_classification_test.msg_intent_to_purchase is 'Message related to the Intent to Purchase Test result';
comment on column ls_ilr_classification_test.specialized_asset is 'Indicates whether the ILR passed the Specialized Asset Test. 1: Pass, 0: Fail.';
comment on column ls_ilr_classification_test.msg_specialized_asset is 'Message related to the Specialized Asset Test result';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5482, 0, 2017, 4, 0, 0, 47713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_047713_lessee_01_classification_results_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;