/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053118_lessee_02_purch_term_updates_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.2 02/26/2019  C yura           backfill purch/term option fields
||============================================================================
*/

SET SERVEROUTPUT ON


-- Raise exception for ILRs to be reviewed if there are revision 1 records missing and there are also revisions > 1 missing records
declare
   L_COUNT number;
begin
   L_COUNT := 0;

  select sum(cnt) into L_COUNT from (
   SELECT count(*) cnt
   FROM ls_ilr_options ilro
   WHERE ilro.revision = 1
   AND termination_amt <> 0
   AND NOT EXISTS(
   SELECT 1
   FROM ls_ilr_termination_options lipo
   WHERE lipo.ilr_id = ilro.ilr_id
   and lipo.revision = ilro.revision  )
   AND EXISTS(
   SELECT 1
   FROM ls_ilr_options lipo
   WHERE lipo.ilr_id = ilro.ilr_id
   and lipo.revision > 1 )
   AND NOT EXISTS(
   SELECT 1
   FROM ls_ilr_termination_options lipo
   WHERE lipo.ilr_id = ilro.ilr_id
   and lipo.revision > 1 )
   union all
   SELECT count(*) 
   FROM ls_ilr_options ilro
   WHERE ilro.revision = 1
   AND termination_amt <> 0
   AND NOT EXISTS(
   SELECT 1
   FROM ls_ilr_purchase_options lipo
   WHERE lipo.ilr_id = ilro.ilr_id
   and lipo.revision = ilro.revision  )
   AND EXISTS(
   SELECT 1
   FROM ls_ilr_options lipo
   WHERE lipo.ilr_id = ilro.ilr_id
   and lipo.revision > 1 )
   AND NOT EXISTS(
   SELECT 1
   FROM ls_ilr_purchase_options lipo
   WHERE lipo.ilr_id = ilro.ilr_id
   and lipo.revision > 1 )
  );

   if L_COUNT > 0 then
      RAISE_APPLICATION_ERROR(-20000,
                              'There are purchase or terminatior option amounts on ILRs Revision 1 with future revisions missing ls_ilr_purchase_options records. Resolve or Review and run pp_schema_change_log insert in this script manually if condition is valid.');
   end if;
	
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15483, 0, 2018, 1, 0, 2, 53118, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053118_lessee_02_purch_term_updates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;