/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051052_lessee_01_alter_queries_remove_st_deferred_rent_cols_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 5/1/2018  Alex Healey    Delete the Short term deferred rent fields from lease asset schedule queries
||============================================================================
*/

DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Schedule by ILR';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2,  las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10,  las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,  las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE,
       las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_LIABILITY, las.END_LIABILITY, las.BEG_LT_LIABILITY, las.END_LT_LIABILITY
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(ilr.ilr_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''ILR NUMBER'')          ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'beg_capital_cost',
        14,
        1,
        1,
        '',
        'Beg Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_deferred_rent',
        69,
        1,
        1,
        '',
        'Beg Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_obligation',
        18,
        1,
        1,
        '',
        'Beg Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_obligation',
        16,
        1,
        1,
        '',
        'Beg Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'bpo_price',
        68,
        1,
        1,
        '',
        'Bpo Price',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_description',
        4,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_id',
        3,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual1',
        44,
        1,
        1,
        '',
        'Contingent Accrual1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual10',
        53,
        1,
        1,
        '',
        'Contingent Accrual10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual2',
        45,
        1,
        1,
        '',
        'Contingent Accrual2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual3',
        46,
        1,
        1,
        '',
        'Contingent Accrual3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual4',
        47,
        1,
        1,
        '',
        'Contingent Accrual4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual5',
        48,
        1,
        1,
        '',
        'Contingent Accrual5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual6',
        49,
        1,
        1,
        '',
        'Contingent Accrual6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual7',
        50,
        1,
        1,
        '',
        'Contingent Accrual7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual8',
        51,
        1,
        1,
        '',
        'Contingent Accrual8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual9',
        52,
        1,
        1,
        '',
        'Contingent Accrual9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid1',
        54,
        1,
        1,
        '',
        'Contingent Paid1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid10',
        63,
        1,
        1,
        '',
        'Contingent Paid10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid2',
        55,
        1,
        1,
        '',
        'Contingent Paid2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid3',
        56,
        1,
        1,
        '',
        'Contingent Paid3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid4',
        57,
        1,
        1,
        '',
        'Contingent Paid4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid5',
        58,
        1,
        1,
        '',
        'Contingent Paid5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid6',
        59,
        1,
        1,
        '',
        'Contingent Paid6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid7',
        60,
        1,
        1,
        '',
        'Contingent Paid7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid8',
        61,
        1,
        1,
        '',
        'Contingent Paid8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid9',
        62,
        1,
        1,
        '',
        'Contingent Paid9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'current_lease_cost',
        65,
        1,
        1,
        '',
        'Current Lease Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'deferred_rent',
        70,
        1,
        1,
        '',
        'Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_capital_cost',
        15,
        1,
        1,
        '',
        'End Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_deferred_rent',
        71,
        1,
        1,
        '',
        'End Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_lt_obligation',
        19,
        1,
        1,
        '',
        'End Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_obligation',
        17,
        1,
        1,
        '',
        'End Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual1',
        24,
        1,
        1,
        '',
        'Executory Accrual1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual10',
        33,
        1,
        1,
        '',
        'Executory Accrual10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual2',
        25,
        1,
        1,
        '',
        'Executory Accrual2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual3',
        26,
        1,
        1,
        '',
        'Executory Accrual3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual4',
        27,
        1,
        1,
        '',
        'Executory Accrual4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual5',
        28,
        1,
        1,
        '',
        'Executory Accrual5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual6',
        29,
        1,
        1,
        '',
        'Executory Accrual6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual7',
        30,
        1,
        1,
        '',
        'Executory Accrual7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual8',
        31,
        1,
        1,
        '',
        'Executory Accrual8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual9',
        32,
        1,
        1,
        '',
        'Executory Accrual9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid1',
        34,
        1,
        1,
        '',
        'Executory Paid1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid10',
        43,
        1,
        1,
        '',
        'Executory Paid10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid2',
        35,
        1,
        1,
        '',
        'Executory Paid2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid3',
        36,
        1,
        1,
        '',
        'Executory Paid3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid4',
        37,
        1,
        1,
        '',
        'Executory Paid4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid5',
        38,
        1,
        1,
        '',
        'Executory Paid5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid6',
        39,
        1,
        1,
        '',
        'Executory Paid6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid7',
        40,
        1,
        1,
        '',
        'Executory Paid7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid8',
        41,
        1,
        1,
        '',
        'Executory Paid8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid9',
        42,
        1,
        1,
        '',
        'Executory Paid9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_id',
        5,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_number',
        6,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        1,
        2,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_accrual',
        20,
        1,
        1,
        '',
        'Interest Accrual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_paid',
        22,
        1,
        1,
        '',
        'Interest Paid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'is_om',
        64,
        0,
        1,
        '',
        'Is Om',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_cap_type',
        9,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_id',
        7,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_number',
        8,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'leased_asset_number',
        2,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'location',
        10,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ls_asset_id',
        1,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'monthnum',
        13,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        null,
        null,
        'the_sort',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_accrual',
        21,
        1,
        1,
        '',
        'Principal Accrual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_paid',
        23,
        1,
        1,
        '',
        'Principal Paid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'residual_amount',
        66,
        1,
        1,
        '',
        'Residual Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'revision',
        11,
        0,
        1,
        '',
        'Revision',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'set_of_books_id',
        12,
        0,
        1,
        '',
        'Set Of Books Id',
        300,
        'description',
        'set_of_books',
        'NUMBER',
        0,
        'set_of_books_id',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'term_penalty',
        67,
        1,
        1,
        '',
        'Term Penalty',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_liability',
        72,
        1,
        1,
        '',
        'Beg Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_liability',
        73,
        1,
        1,
        '',
        'Beg Lt Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_liability',
        74,
        1,
        1,
        '',
        'End Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_lt_liability',
        75,
        1,
        1,
        '',
        'End Lt Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual
;

END;

/




DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Schedule by Leased Asset';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2,  las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10,  las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,  las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE,
       las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_LIABILITY, las.END_LIABILITY, las.BEG_LT_LIABILITY, las.END_LT_LIABILITY
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(la.leased_asset_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''LEASED ASSET NUMBER'')          ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'beg_capital_cost',
        14,
        1,
        1,
        '',
        'Beg Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_deferred_rent',
        69,
        1,
        1,
        '',
        'Beg Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_obligation',
        18,
        1,
        1,
        '',
        'Beg Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_obligation',
        16,
        1,
        1,
        '',
        'Beg Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'bpo_price',
        68,
        1,
        1,
        '',
        'Bpo Price',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_description',
        4,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_id',
        3,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual1',
        44,
        1,
        1,
        '',
        'Contingent Accrual1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual10',
        53,
        1,
        1,
        '',
        'Contingent Accrual10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual2',
        45,
        1,
        1,
        '',
        'Contingent Accrual2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual3',
        46,
        1,
        1,
        '',
        'Contingent Accrual3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual4',
        47,
        1,
        1,
        '',
        'Contingent Accrual4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual5',
        48,
        1,
        1,
        '',
        'Contingent Accrual5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual6',
        49,
        1,
        1,
        '',
        'Contingent Accrual6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual7',
        50,
        1,
        1,
        '',
        'Contingent Accrual7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual8',
        51,
        1,
        1,
        '',
        'Contingent Accrual8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual9',
        52,
        1,
        1,
        '',
        'Contingent Accrual9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid1',
        54,
        1,
        1,
        '',
        'Contingent Paid1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid10',
        63,
        1,
        1,
        '',
        'Contingent Paid10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid2',
        55,
        1,
        1,
        '',
        'Contingent Paid2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid3',
        56,
        1,
        1,
        '',
        'Contingent Paid3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid4',
        57,
        1,
        1,
        '',
        'Contingent Paid4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid5',
        58,
        1,
        1,
        '',
        'Contingent Paid5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid6',
        59,
        1,
        1,
        '',
        'Contingent Paid6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid7',
        60,
        1,
        1,
        '',
        'Contingent Paid7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid8',
        61,
        1,
        1,
        '',
        'Contingent Paid8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid9',
        62,
        1,
        1,
        '',
        'Contingent Paid9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'current_lease_cost',
        65,
        1,
        1,
        '',
        'Current Lease Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'deferred_rent',
        70,
        1,
        1,
        '',
        'Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_capital_cost',
        15,
        1,
        1,
        '',
        'End Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_deferred_rent',
        71,
        1,
        1,
        '',
        'End Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_lt_obligation',
        19,
        1,
        1,
        '',
        'End Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_obligation',
        17,
        1,
        1,
        '',
        'End Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual1',
        24,
        1,
        1,
        '',
        'Executory Accrual1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual10',
        33,
        1,
        1,
        '',
        'Executory Accrual10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual2',
        25,
        1,
        1,
        '',
        'Executory Accrual2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual3',
        26,
        1,
        1,
        '',
        'Executory Accrual3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual4',
        27,
        1,
        1,
        '',
        'Executory Accrual4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual5',
        28,
        1,
        1,
        '',
        'Executory Accrual5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual6',
        29,
        1,
        1,
        '',
        'Executory Accrual6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual7',
        30,
        1,
        1,
        '',
        'Executory Accrual7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual8',
        31,
        1,
        1,
        '',
        'Executory Accrual8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual9',
        32,
        1,
        1,
        '',
        'Executory Accrual9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid1',
        34,
        1,
        1,
        '',
        'Executory Paid1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid10',
        43,
        1,
        1,
        '',
        'Executory Paid10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid2',
        35,
        1,
        1,
        '',
        'Executory Paid2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid3',
        36,
        1,
        1,
        '',
        'Executory Paid3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid4',
        37,
        1,
        1,
        '',
        'Executory Paid4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid5',
        38,
        1,
        1,
        '',
        'Executory Paid5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid6',
        39,
        1,
        1,
        '',
        'Executory Paid6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid7',
        40,
        1,
        1,
        '',
        'Executory Paid7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid8',
        41,
        1,
        1,
        '',
        'Executory Paid8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid9',
        42,
        1,
        1,
        '',
        'Executory Paid9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_id',
        5,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_number',
        6,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_accrual',
        20,
        1,
        1,
        '',
        'Interest Accrual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_paid',
        22,
        1,
        1,
        '',
        'Interest Paid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'is_om',
        64,
        0,
        1,
        '',
        'Is Om',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_cap_type',
        9,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_id',
        7,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_number',
        8,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'leased_asset_number',
        2,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        1,
        2,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'location',
        10,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ls_asset_id',
        1,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'monthnum',
        13,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        null,
        null,
        'the_sort',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_accrual',
        21,
        1,
        1,
        '',
        'Principal Accrual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_paid',
        23,
        1,
        1,
        '',
        'Principal Paid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'residual_amount',
        66,
        1,
        1,
        '',
        'Residual Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'revision',
        11,
        0,
        1,
        '',
        'Revision',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'set_of_books_id',
        12,
        0,
        1,
        '',
        'Set Of Books Id',
        300,
        'description',
        'set_of_books',
        'NUMBER',
        0,
        'set_of_books_id',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'term_penalty',
        67,
        1,
        1,
        '',
        'Term Penalty',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_liability',
        72,
        1,
        1,
        '',
        'Beg Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_liability',
        73,
        1,
        1,
        '',
        'End Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_liability',
        74,
        1,
        1,
        '',
        'Beg Lt Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_lt_liability',
        75,
        1,
        1,
        '',
        'End Lt Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual
;

END;

/


DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Schedule By MLA';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select la.ls_asset_id,          la.leased_asset_number,          co.company_id,          co.description               as company_description,
	 ilr.ilr_id,          ilr.ilr_number,          ll.lease_id,          ll.lease_number,          lct.description              as lease_cap_type,
	 al.long_description          as location,          las.REVISION,          las.SET_OF_BOOKS_ID,          to_char(las.MONTH, ''yyyymm'') as monthnum,
	 lcurt.DESCRIPTION AS currency_type,         las.iso_code currency,         las.currency_display_symbol,         las.BEG_CAPITAL_COST,
	 las.END_CAPITAL_COST,          las.BEG_OBLIGATION,          las.END_OBLIGATION,          las.BEG_LT_OBLIGATION,          las.END_LT_OBLIGATION,
	 las.INTEREST_ACCRUAL,          las.PRINCIPAL_ACCRUAL,          las.INTEREST_PAID,          las.PRINCIPAL_PAID,          las.EXECUTORY_ACCRUAL1,
	 las.EXECUTORY_ACCRUAL2,          las.EXECUTORY_ACCRUAL3,          las.EXECUTORY_ACCRUAL4,          las.EXECUTORY_ACCRUAL5,          las.EXECUTORY_ACCRUAL6,
	 las.EXECUTORY_ACCRUAL7,          las.EXECUTORY_ACCRUAL8,          las.EXECUTORY_ACCRUAL9,          las.EXECUTORY_ACCRUAL10,          las.EXECUTORY_PAID1,
	 las.EXECUTORY_PAID2,          las.EXECUTORY_PAID3,          las.EXECUTORY_PAID4,          las.EXECUTORY_PAID5,          las.EXECUTORY_PAID6,
	 las.EXECUTORY_PAID7,          las.EXECUTORY_PAID8,          las.EXECUTORY_PAID9,          las.EXECUTORY_PAID10,          las.CONTINGENT_ACCRUAL1,
	 las.CONTINGENT_ACCRUAL2,          las.CONTINGENT_ACCRUAL3,          las.CONTINGENT_ACCRUAL4,          las.CONTINGENT_ACCRUAL5,          las.CONTINGENT_ACCRUAL6,
	 las.CONTINGENT_ACCRUAL7,          las.CONTINGENT_ACCRUAL8,          las.CONTINGENT_ACCRUAL9,          las.CONTINGENT_ACCRUAL10, las.CONTINGENT_PAID1,
	 las.CONTINGENT_PAID2,          las.CONTINGENT_PAID3,          las.CONTINGENT_PAID4,          las.CONTINGENT_PAID5,          las.CONTINGENT_PAID6,
	 las.CONTINGENT_PAID7,          las.CONTINGENT_PAID8,          las.CONTINGENT_PAID9,          las.CONTINGENT_PAID10,          las.IS_OM,
	 las.CURRENT_LEASE_COST,          las.RESIDUAL_AMOUNT,          las.TERM_PENALTY,          las.BPO_PRICE,          las.BEG_DEFERRED_RENT,
	 las.DEFERRED_RENT,          las.END_DEFERRED_RENT,  las.BEG_LIABILITY,          las.END_LIABILITY,          las.BEG_LT_LIABILITY,
	 las.END_LT_LIABILITY   from   ls_asset la,          ls_ilr ilr,          ls_lease ll,          company co,          asset_location al,
	 ls_lease_cap_type lct,          ls_ilr_options ilro,          v_ls_asset_schedule_fx_vw las ,      ls_lease_currency_type lcurt
 where  la.ilr_id = ilr.ilr_id
	 and ilr.lease_id = ll.lease_id
	 and ilr.ilr_id = ilro.ilr_id
	 and ilr.current_revision = ilro.revision
	 and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
	 and la.asset_location_id = al.asset_location_id
	 and la.company_id = co.company_id
	 and las.ls_asset_id = la.ls_asset_id
	 and las.ls_cur_type = lcurt.ls_currency_type_id
	 and las.revision = la.approved_revision
	 and upper(ll.lease_number) in (select upper(filter_value)
									from   pp_any_required_filter
									where              upper(trim(column_name)) = ''LEASE NUMBER'')
	AND UPPER(TRIM(lcurt.DESCRIPTION)) IN (SELECT UPPER(filter_value)
									FROM pp_any_required_filter WHERE Upper(Trim(column_name)) = ''CURRENCY TYPE'')          ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'beg_capital_cost',
        16,
        1,
        1,
        '',
        'Beg Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_deferred_rent',
        72,
        1,
        1,
        '',
        'Beg Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_obligation',
        20,
        1,
        1,
        '',
        'Beg Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_obligation',
        18,
        1,
        1,
        '',
        'Beg Obligation',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'bpo_price',
        70,
        1,
        1,
        '',
        'Bpo Price',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_description',
        6,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        null,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_id',
        5,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        null,
        'company_id',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual1',
        46,
        1,
        1,
        '',
        'Contingent Accrual1',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual10',
        55,
        1,
        1,
        '',
        'Contingent Accrual10',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual2',
        47,
        1,
        1,
        '',
        'Contingent Accrual2',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual3',
        48,
        1,
        1,
        '',
        'Contingent Accrual3',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual4',
        49,
        1,
        1,
        '',
        'Contingent Accrual4',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual5',
        50,
        1,
        1,
        '',
        'Contingent Accrual5',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual6',
        51,
        1,
        1,
        '',
        'Contingent Accrual6',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual7',
        52,
        1,
        1,
        '',
        'Contingent Accrual7',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual8',
        53,
        1,
        1,
        '',
        'Contingent Accrual8',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_accrual9',
        54,
        1,
        1,
        '',
        'Contingent Accrual9',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid1',
        56,
        1,
        1,
        '',
        'Contingent Paid1',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid10',
        65,
        1,
        1,
        '',
        'Contingent Paid10',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid2',
        57,
        1,
        1,
        '',
        'Contingent Paid2',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid3',
        58,
        1,
        1,
        '',
        'Contingent Paid3',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid4',
        59,
        1,
        1,
        '',
        'Contingent Paid4',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid5',
        60,
        1,
        1,
        '',
        'Contingent Paid5',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid6',
        61,
        1,
        1,
        '',
        'Contingent Paid6',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid7',
        62,
        1,
        1,
        '',
        'Contingent Paid7',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid8',
        63,
        1,
        1,
        '',
        'Contingent Paid8',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_paid9',
        64,
        1,
        1,
        '',
        'Contingent Paid9',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'currency',
        1,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        null,
        '0',
        null,
        null,
        '',
        0,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_display_symbol',
        2,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        null,
        '0',
        null,
        null,
        '',
        0,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_type',
        71,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        null,
        'description',
        1,
        2,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'current_lease_cost',
        67,
        1,
        1,
        '',
        'Current Lease Cost',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'deferred_rent',
        73,
        1,
        1,
        '',
        'Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_capital_cost',
        17,
        1,
        1,
        '',
        'End Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_deferred_rent',
        74,
        1,
        1,
        '',
        'End Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_lt_obligation',
        21,
        1,
        1,
        '',
        'End Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_obligation',
        19,
        1,
        1,
        '',
        'End Obligation',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual1',
        26,
        1,
        1,
        '',
        'Executory Accrual1',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual10',
        35,
        1,
        1,
        '',
        'Executory Accrual10',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual2',
        27,
        1,
        1,
        '',
        'Executory Accrual2',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual3',
        28,
        1,
        1,
        '',
        'Executory Accrual3',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual4',
        29,
        1,
        1,
        '',
        'Executory Accrual4',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual5',
        30,
        1,
        1,
        '',
        'Executory Accrual5',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual6',
        31,
        1,
        1,
        '',
        'Executory Accrual6',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual7',
        32,
        1,
        1,
        '',
        'Executory Accrual7',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual8',
        33,
        1,
        1,
        '',
        'Executory Accrual8',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_accrual9',
        34,
        1,
        1,
        '',
        'Executory Accrual9',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid1',
        36,
        1,
        1,
        '',
        'Executory Paid1',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid10',
        45,
        1,
        1,
        '',
        'Executory Paid10',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid2',
        37,
        1,
        1,
        '',
        'Executory Paid2',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid3',
        38,
        1,
        1,
        '',
        'Executory Paid3',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid4',
        39,
        1,
        1,
        '',
        'Executory Paid4',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid5',
        40,
        1,
        1,
        '',
        'Executory Paid5',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid6',
        41,
        1,
        1,
        '',
        'Executory Paid6',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid7',
        42,
        1,
        1,
        '',
        'Executory Paid7',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid8',
        43,
        1,
        1,
        '',
        'Executory Paid8',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_accrual',
        22,
        1,
        1,
        '',
        'Interest Accrual',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_paid9',
        44,
        1,
        1,
        '',
        'Executory Paid9',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_id',
        7,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        null,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_number',
        8,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        null,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_paid',
        24,
        1,
        1,
        '',
        'Interest Paid',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'is_om',
        66,
        0,
        1,
        '',
        'Is Om',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_cap_type',
        11,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        null,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_id',
        9,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        null,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_number',
        10,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        null,
        'lease_number',
        1,
        2,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'leased_asset_number',
        4,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        null,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'location',
        12,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        null,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ls_asset_id',
        3,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        null,
        'leased_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'monthnum',
        15,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        null,
        'month_number',
        null,
        null,
        'the_sort',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_accrual',
        23,
        1,
        1,
        '',
        'Principal Accrual',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_paid',
        25,
        1,
        1,
        '',
        'Principal Paid',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'residual_amount',
        68,
        1,
        1,
        '',
        'Residual Amount',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'revision',
        13,
        0,
        1,
        '',
        'Revision',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'set_of_books_id',
        14,
        0,
        1,
        '',
        'Set Of Books Id',
        300,
        'description',
        'set_of_books',
        'NUMBER',
        null,
        'set_of_books_id',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'term_penalty',
        69,
        1,
        1,
        '',
        'Term Penalty',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_liability',
        75,
        1,
        1,
        '',
        'Beg Liability',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_liability',
        76,
        1,
        1,
        '',
        'Beg Lt Liability',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_liability',
        77,
        1,
        1,
        '',
        'End Liability',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_lt_liability',
        78,
        1,
        1,
        '',
        'End Lt Liability',
        300,
        '',
        '',
        'NUMBER',
        null,
        '0',
        null,
        null,
        '',
        0,
        0

FROM dual
;

END;

/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4948, 0, 2017, 3, 0, 0, 51052, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051052_lessee_01_alter_queries_remove_st_deferred_rent_cols_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;