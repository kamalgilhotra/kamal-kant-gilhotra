/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048217_lease_01_asset_import_add_contract_currency_ddl.sql
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2017.1.0.0 06/22/2017 Jared Watkins  Add the contract currency columns to the ls_import_asset table
||============================================================================
*/

alter table ls_import_asset
add contract_currency_id number(22,0);
alter table ls_import_asset
add contract_currency_xlate varchar2(254);

comment on column ls_import_asset.contract_currency_id is 'The Contract Currency for the Lease Asset.';
comment on column ls_import_asset.contract_currency_xlate is 'Translation field for determining Contract Currency.';

alter table ls_import_asset_archive
add contract_currency_id number(22,0);
alter table ls_import_asset_archive
add contract_currency_xlate varchar2(254);

comment on column ls_import_asset_archive.contract_currency_id is 'The Contract Currency for the Lease Asset.';
comment on column ls_import_asset_archive.contract_currency_xlate is 'Translation field for determining Contract Currency.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3529, 0, 2017, 1, 0, 0, 48217, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048217_lease_01_asset_import_add_contract_currency_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;