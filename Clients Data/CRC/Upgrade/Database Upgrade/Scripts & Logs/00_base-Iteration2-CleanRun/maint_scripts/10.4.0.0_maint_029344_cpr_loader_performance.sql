/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029344_cpr_loader_performance.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.0.0   02/27/2013 Cliff Robinson   Point Release
||============================================================================
*/

--Create cpr_ledger_act_import_stg indexes:
--import run id
create index CPR_LDG_ACT_IMP_RUN on PWRPLANT.CPR_LEDGER_ACT_IMPORT_STG (IMPORT_RUN_ID) tablespace PWRPLANT_IDX;

--Performance 1
create index CPR_LED_ACT_IMP_STG_PER1
   on CPR_LEDGER_ACT_IMPORT_STG (IMPORT_RUN_ID, LINE_ID, COMPANY_ID, GL_ACCOUNT_ID,
                                 FUNC_CLASS_ID, BUS_SEGMENT_ID, UTILITY_ACCOUNT_ID)
      tablespace PWRPLANT_IDX;

--Performance 2: import_run_id and asset_id
create index CPR_LED_ACT_IMP_STG_PER2
   on CPR_LEDGER_ACT_IMPORT_STG (IMPORT_RUN_ID, ASSET_ID)
      tablespace PWRPLANT_IDX;

-- cpr_ledger_act_import_aid_stg
create global temporary table PWRPLANT.CPR_LEDGER_ACT_IMPORT_AID_STG
(
 IMPORT_RUN_ID        number(22,0),
 COMPANY_ID           number(22,0),
 GL_ACCOUNT_ID        number(22,0),
 FUNC_CLASS_ID        number(22,0),
 BUS_SEGMENT_ID       number(22,0),
 SUBLEDGER_INDICATOR  number(22,0),
 UTILITY_ACCOUNT_ID   number(22,0),
 SUB_ACCOUNT_ID       number(22,0),
 PROPERTY_GROUP_ID    number(22,0),
 RETIREMENT_UNIT_ID   number(22,0),
 ASSET_LOCATION_ID    number(22,0),
 ENG_IN_SERVICE_YEAR  number(22,0),
 DESCRIPTION          varchar2(35),
 LONG_DESCRIPTION     varchar2(254),
 SERIAL_NUMBER        varchar2(35),
 ASSET_ID             number(22,0)
) on commit preserve rows;

create unique index IX_CPR_LDG_ACT_IMPRT_AIDSTG
   on PWRPLANT.CPR_LEDGER_ACT_IMPORT_AID_STG (IMPORT_RUN_ID, COMPANY_ID, GL_ACCOUNT_ID, FUNC_CLASS_ID,
                                              BUS_SEGMENT_ID, SUBLEDGER_INDICATOR, UTILITY_ACCOUNT_ID,
                                              SUB_ACCOUNT_ID, PROPERTY_GROUP_ID, RETIREMENT_UNIT_ID,
                                              ASSET_LOCATION_ID, ENG_IN_SERVICE_YEAR, DESCRIPTION,
                                              LONG_DESCRIPTION, NVL( SERIAL_NUMBER,' ' ));

create index IX_CPR_LDG_ACT_IMPRT_AIDLU
   on PWRPLANT.CPR_LEDGER_ACT_IMPORT_STG (IMPORT_RUN_ID, COMPANY_ID, GL_ACCOUNT_ID, FUNC_CLASS_ID,
                                          BUS_SEGMENT_ID, SUBLEDGER_INDICATOR, UTILITY_ACCOUNT_ID,
                                          SUB_ACCOUNT_ID, PROPERTY_GROUP_ID, RETIREMENT_UNIT_ID,
                                          ASSET_LOCATION_ID, ENG_IN_SERVICE_YEAR, DESCRIPTION,
                                          LONG_DESCRIPTION, NVL( SERIAL_NUMBER,' ' ));

--  *****************************************************************
--  Asset Activity ID Aid Table
--  *****************************************************************
create global temporary table PWRPLANT.CPR_LEDGER_ACT_IMP_ASTID_STG
(
 IMPORT_RUN_ID         number(22,0),
 LINE_ID               number(22,0),
 ASSET_ACTIVITY_ID     number(22,0),
 ACT_WORK_ORDER_NUMBER varchar2(35)
) on commit preserve rows;

alter table PWRPLANT.CPR_LEDGER_ACT_IMP_ASTID_STG
   add constraint ACTIVTY_STG_PK
       primary key (IMPORT_RUN_ID, LINE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (310, 0, 10, 4, 0, 0, 29344, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029344_cpr_loader_performance.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;