/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_050770_lessee_02_update_Capital_Test_action_name_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.3.0.0 04/04/2018   K. Powers      PP-50770 - Lease Brightline Test Changes
 ||============================================================================
 */

update PPBASE_ACTIONS_WINDOWS
set ACTION_TEXT = 'Classification Test'
where module = 'LESSEE'
and ACTION_TEXT = 'Capital Test';

 --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4308, 0, 2017, 3, 0, 0, 50770, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050770_lessee_02_update_Capital_Test_action_name_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;