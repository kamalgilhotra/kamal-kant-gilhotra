/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Security
|| File Name:   maint_044681_websys_security_permissions_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.2     09/03/2015 Ryan Oliveria    	 Creating new table 'PP_WEB_SECURITY_PERM_CONTROL'
||==========================================================================================
*/

SET serveroutput ON size 30000;

declare
	doesTableExist number := 0;
begin
	doesTableExist := PWRPLANT.SYS_DOES_TABLE_EXIST('PP_WEB_SECURITY_PERM_CONTROL','PWRPLANT');

	if doesTableExist = 0 then
        begin
			dbms_output.put_line('Creating table PP_WEB_SECURITY_PERM_CONTROL');

			EXECUTE IMMEDIATE 'CREATE TABLE "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL"
								(
									"OBJECTS" varchar2(100) not null,
									"MODULE_ID" number(22,0) not null,
									"COMPONENT_ID" number(22,0),
									"PSEUDO_COMPONENT" varchar2(35),
									"SECTION" varchar2(35),
									"NAME" varchar2(50),
									CONSTRAINT PK_PP_WEB_SECURITY_PERM_CTRL PRIMARY KEY ("OBJECTS"),
									CONSTRAINT FK_PP_WEB_SECURITY_PERM_CTRL_M FOREIGN KEY ("MODULE_ID") REFERENCES PP_WEB_MODULES ("MODULE_ID"),
									CONSTRAINT FK_PP_WEB_SECURITY_PERM_CTRL_C FOREIGN KEY ("COMPONENT_ID") REFERENCES PP_WEB_COMPONENT ("COMPONENT_ID")
								) TABLESPACE PWRPLANT';

			EXECUTE IMMEDIATE 'COMMENT ON TABLE "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL" IS ''Security keys to CONTROL, as well as information on how to group and alias them.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL"."OBJECTS" is ''The security key used in the code to identify this permission.  This will also match the OBJECTS column in PP_WEB_SECURITY_PERMISSION.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL"."MODULE_ID" is ''The module this permission belongs to.''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL"."COMPONENT_ID" is ''(Optional) The component this permission belongs to''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL"."PSEUDO_COMPONENT" is ''(Optional) If component_id is left null, the pseudo-column will determine the first level of grouping for this permission''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL"."SECTION" is ''Another level of grouping this permission will be CONTROLed under''';
			EXECUTE IMMEDIATE 'COMMENT ON COLUMN "PWRPLANT"."PP_WEB_SECURITY_PERM_CONTROL"."NAME" is ''A user-friendly name for this permission''';

		end;
	end if;
end;

/



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2849, 0, 2015, 2, 0, 0, 044681, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044681_websys_security_permissions_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;