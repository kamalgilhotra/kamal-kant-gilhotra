/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038615_reg_inc_fp_log.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 06/26/2014 Shane Ward       Log table for Incremental FP Adjustments
||============================================================================
*/

create table INCREMENTAL_FP_RUN_LOG
(
 INC_FP_RUN_ID        number(22,0) not null,
 WORK_ORDER_ID        number(22,0) not null,
 REVISION             number(22,0) not null,
 BUDGET_VERSION_ID    number(22,0) not null,
 FCST_DEPR_VERSION_ID number(22,0) not null,
 VERSION_ID           number(22,0) not null,
 DATE_RUN             date         null,
 RUN_USER_ID          varchar(18)  null,
 USER_ID              varchar(18)  null,
 TIMESTAMP            number(22,0) null
);

alter table INCREMENTAL_FP_RUN_LOG
   add constraint PK_INCREMENTAL_FP_RUN_LOG
       primary key (INC_FP_RUN_ID)
       using index tablespace PWRPLANT_IDX;

alter table INCREMENTAL_FP_RUN_LOG
   add constraint FK_INCREMENTAL_FP_RUN_LOG1
       foreign key (WORK_ORDER_ID, REVISION)
       references WORK_ORDER_APPROVAL (WORK_ORDER_ID, REVISION);

alter table INCREMENTAL_FP_RUN_LOG
   add constraint FK_INCREMENTAL_FP_RUN_LOG2
       foreign key (BUDGET_VERSION_ID)
       references BUDGET_VERSION (BUDGET_VERSION_ID);

alter table INCREMENTAL_FP_RUN_LOG
   add constraint FK_INCREMENTAL_FP_RUN_LOG3
       foreign key (FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);

alter table INCREMENTAL_FP_RUN_LOG
   add constraint FK_INCREMENTAL_FP_RUN_LOG4
       foreign key (VERSION_ID)
       references VERSION (VERSION_ID);

comment on table INCREMENTAL_FP_RUN_LOG is 'Keeps track of each funding project/revision that has incremental forecast
amounts calculated for each Capital Budget Version, Depreciation Forecast Version, and PowerTax Case.';
comment on column INCREMENTAL_FP_RUN_LOG.INC_FP_RUN_ID is 'System assigned identifier for the incremental run.';
comment on column INCREMENTAL_FP_RUN_LOG.WORK_ORDER_ID is 'System assigned identifier for the funding project.';
comment on column INCREMENTAL_FP_RUN_LOG.REVISION is 'System assigned identifier for the revision.';
comment on column INCREMENTAL_FP_RUN_LOG.BUDGET_VERSION_ID is 'System assigned identifier for the Capital Budget Version.';
comment on column INCREMENTAL_FP_RUN_LOG.FCST_DEPR_VERSION_ID is 'System assigned identifier for the Forecast Depreciation Version.';
comment on column INCREMENTAL_FP_RUN_LOG.VERSION_ID is 'System assigned identifer for the POwerTax Case.';
comment on column INCREMENTAL_FP_RUN_LOG.DATE_RUN is 'Date and time the incremental process was initiated.';
comment on column INCREMENTAL_FP_RUN_LOG.RUN_USER_ID is 'User ID that initiated the incremental process.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1171, 0, 10, 4, 2, 7, 38615, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038615_reg_inc_fp_log.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;