/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048974_lessor_03_lsr_asset_nullable_long_description_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/09/2017 Josh Sandler	  Allow NULL long_description
||============================================================================
*/

ALTER TABLE lsr_asset
	MODIFY long_description NULL
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3786, 0, 2017, 1, 0, 0, 48974, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048974_lessor_03_lsr_asset_nullable_long_description_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;