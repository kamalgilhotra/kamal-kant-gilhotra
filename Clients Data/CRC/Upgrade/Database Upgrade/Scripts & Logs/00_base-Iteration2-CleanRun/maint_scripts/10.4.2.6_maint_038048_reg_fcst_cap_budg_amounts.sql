/*
||================================================================================================
|| Application: PowerPlant
|| File Name:   maint_038048_reg_fcst_cap_budg_amounts.sql
||================================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||================================================================================================
|| Version  Date       Revised By             Reason for Change
|| -------- ---------- --------------         ----------------------------------------------------
|| 10.4.2.6 05/23/2014 Shane "B-Boy" Ward     Updated views to handle multiple reg accounts
||================================================================================================
*/

create table REG_BUDGET_SOURCE_ACTIVITY
(
 DETAIL_ID           number(22,0) not null,
 USER_ID             varchar2(18) null,
 TIME_STAMP          date         null,
 DESCRIPTION         varchar2(35) not null,
 COLUMN_NAME         varchar2(35) null,
 EXPENDITURE_TYPE_ID number(22,0) null
);

alter table REG_BUDGET_SOURCE_ACTIVITY
   add constraint PK_REG_BUDGET_SOURCE_ACTIVITY
       primary key (DETAIL_ID)
       using index tablespace PWRPLANT_IDX;

comment on table reg_budget_source_activity is	'Stores source details for Forecast Cap Budget Integration Activity';   
comment on column REG_BUDGET_SOURCE_ACTIVITY.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_BUDGET_SOURCE_ACTIVITY.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_BUDGET_SOURCE_ACTIVITY.DESCRIPTION is 'DESCRIPTION OF THE SOURCE DETAILS USED IN THE CAPITAL BUDGET INTERFACE';
comment on column REG_BUDGET_SOURCE_ACTIVITY.COLUMN_NAME is 'NAME OF THE COLUMN USED FOR THE BUDGET SOURCE';

create table REG_BUDGET_SOURCE_BALANCE
(
 DETAIL_ID           number(22,0) not null,
 USER_ID             varchar2(18) null,
 TIME_STAMP          date         null,
 DESCRIPTION         varchar2(35) not null,
 COLUMN_NAME         varchar2(35) null,
 EXPENDITURE_TYPE_ID number(22,0) null
);

alter table REG_BUDGET_SOURCE_BALANCE
   add constraint PK_REG_BUDGET_SOURCE_BALANCE
       primary key (DETAIL_ID)
       using index tablespace PWRPLANT_IDX;

comment on table reg_budget_source_activity is	'Stores source details for Forecast Cap Budget Integration Balance';   	   
comment on column REG_BUDGET_SOURCE_BALANCE.USER_ID is 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
comment on column REG_BUDGET_SOURCE_BALANCE.TIME_STAMP is 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';
comment on column REG_BUDGET_SOURCE_BALANCE.DESCRIPTION is 'DESCRIPTION OF THE SOURCE DETAILS USED IN THE CAPITAL BUDGET INTERFACE';
comment on column REG_BUDGET_SOURCE_BALANCE.COLUMN_NAME is 'NAME OF THE COLUMN USED FOR THE BUDGET SOURCE';

insert into REG_BUDGET_SOURCE_BALANCE
   (DETAIL_ID, DESCRIPTION, COLUMN_NAME, EXPENDITURE_TYPE_ID)
   select DETAIL_ID, DESCRIPTION, COLUMN_NAME, EXPENDITURE_TYPE_ID
     from REG_BUDGET_SOURCE_DETAIL
    where ACTIVITY_OR_BALANCE = 'balance';

insert into REG_BUDGET_SOURCE_ACTIVITY
   (DETAIL_ID, DESCRIPTION, COLUMN_NAME, EXPENDITURE_TYPE_ID)
   select DETAIL_ID, DESCRIPTION, COLUMN_NAME, EXPENDITURE_TYPE_ID
     from REG_BUDGET_SOURCE_DETAIL
    where ACTIVITY_OR_BALANCE = 'activity';

drop table REG_BUDGET_SOURCE_DETAIL;

update REG_BUDGET_COMPONENT_ELEMENT
   set ID_COLUMN_NAME = 'detail_id', DESCRIPTION_TABLE = 'reg_budget_source_activity'
 where REG_COMPONENT_ELEMENT_ID in (2);

update REG_BUDGET_COMPONENT_ELEMENT
   set ID_COLUMN_NAME = 'detail_id', DESCRIPTION_TABLE = 'reg_budget_source_balance'
 where REG_COMPONENT_ELEMENT_ID in (7);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1200, 0, 10, 4, 2, 6, 38048, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.6_maint_038048_reg_fcst_cap_budg_amounts.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;