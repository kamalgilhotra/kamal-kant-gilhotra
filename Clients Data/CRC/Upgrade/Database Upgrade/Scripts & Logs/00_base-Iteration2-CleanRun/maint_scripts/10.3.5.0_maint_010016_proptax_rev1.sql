/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010016_proptax_rev1.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- -------------------------------------
|| 10.3.5.0    08/27/2012 Julia Breuer   Point Release
|| 10.4.1.2    12/11/2013 Julia Breuer   Removed script
||============================================================================
*/

update PWRPLANT.PP_REPORTS set PP_REPORT_TIME_OPTION_ID = 19 where REPORT_ID = 513008;
update PWRPLANT.PP_REPORTS set PP_REPORT_TIME_OPTION_ID = 12 where REPORT_ID = 514613;
update PWRPLANT.PP_REPORTS set PP_REPORT_FILTER_ID      = 9  where REPORT_ID = 513505;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (206, 0, 10, 3, 5, 0, 10016, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010016_proptax_rev1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
