/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053024_lessee_01_update_schedules_lt_liab_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.2 1/31/2019 Crystal Yura   Fix bad schedules for PP-53024/PP-50487
||============================================================================
*/

update ls_asset_schedule lt
   set end_lt_liability = 0
 where exists (
         select 1
           from (
                  select ls_asset_id, revision, set_of_books_id, add_months(month,-12) month, 
                         residual_amount, term_penalty, bpo_price, beg_obligation, end_obligation
                    from ls_asset_schedule 
                   where (residual_amount <> 0 or term_penalty <> 0 or bpo_price <> 0)
                ) lm
          where lm.ls_asset_id = lt.ls_asset_id 
            and lm.revision = lt.revision 
            and lm.set_of_books_id = lt.set_of_books_id 
            and lm.month = lt.month)
          and end_lt_liability <> 0;
            
update ls_ilr_schedule lt
   set end_lt_liability = 0
 where exists (
         select 1
           from (
                  select ilr_id, revision, set_of_books_id, add_months(month,-12) month, 
                         residual_amount, term_penalty, bpo_price, beg_obligation, end_obligation
                    from ls_ilr_schedule 
                   where (residual_amount <> 0 or term_penalty <> 0 or bpo_price <> 0)
                ) lm
          where lm.ilr_id = lt.ilr_id 
            and lm.revision = lt.revision 
            and lm.set_of_books_id = lt.set_of_books_id 
            and lm.month = lt.month)
         and end_lt_liability <> 0;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(14442, 0, 2018, 1, 0, 2, 53024, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053024_lessee_01_update_schedules_lt_liab_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;