/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043965_reg_new_layer_review_wksp_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/15/2015 Sarah Byers    Update the wksp for Layer Review
||============================================================================
*/
-- Update the workspace
update ppbase_workspace
	set workspace_uo_name = 'uo_reg_inc_layer_review'
 where workspace_identifier = 'uo_reg_inc_ifa_fp_review';

-- Update the description for the incremental adjustment types
update reg_incremental_adjust_type
	set description = 'FP Adjustment',
		 long_description = 'FP Adjustment: Funding Project + 2 Revisions'
 where incremental_adj_type_id = 1;

update reg_incremental_adjust_type
	set description = 'Depr Version Adjustment',
		 long_description = 'Depr Version Adjustment'
 where incremental_adj_type_id = 2;

update reg_incremental_adjust_type
	set description = 'Depr Set of Books Substitution',
		 long_description = 'Depr Set of Books Substitution'
 where incremental_adj_type_id = 3;

update reg_incremental_adjust_type
	set description = 'FP Snapshot',
		 long_description = 'FP Snapshot: Funding Project + Revision'
 where incremental_adj_type_id = 4;

update reg_incremental_adjust_type
	set description = 'Existing Assets Adjustment',
		 long_description = 'Existing Assets Adjustment: Funding Project, Work Order, or Class Code Value'
 where incremental_adj_type_id = 5;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2597, 0, 2015, 2, 0, 0, 043965, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043965_reg_new_layer_review_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;