/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036571_lease_ilr_accounts_import.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/26/2014 Kyle Peterson
||============================================================================
*/

alter table LS_IMPORT_ILR
   add (RES_DEBIT_ACCOUNT_XLATE  varchar2(254),
        RES_DEBIT_ACCOUNT_ID     number(22,0),
        RES_CREDIT_ACCOUNT_XLATE varchar(254),
        RES_CREDIT_ACCOUNT_ID    number(22,0));

alter table LS_IMPORT_ILR_ARCHIVE
   add (RES_DEBIT_ACCOUNT_XLATE varchar2(254),
       RES_DEBIT_ACCOUNT_ID     number(22,0),
       RES_CREDIT_ACCOUNT_XLATE varchar(254),
       RES_CREDIT_ACCOUNT_ID    number(22,0));

comment on column LS_IMPORT_ILR.RES_DEBIT_ACCOUNT_XLATE is 'Translation field for determining residual debit account';
comment on column LS_IMPORT_ILR.RES_DEBIT_ACCOUNT_ID is 'The residual debit account.';
comment on column LS_IMPORT_ILR.RES_CREDIT_ACCOUNT_XLATE is 'Translation field for determining residual credit account';
comment on column LS_IMPORT_ILR.RES_CREDIT_ACCOUNT_ID is 'The residual credit account.';

comment on column LS_IMPORT_ILR_ARCHIVE.RES_DEBIT_ACCOUNT_XLATE is 'Translation field for determining residual debit account';
comment on column LS_IMPORT_ILR_ARCHIVE.RES_DEBIT_ACCOUNT_ID is 'The residual debit account.';
comment on column LS_IMPORT_ILR_ARCHIVE.RES_CREDIT_ACCOUNT_XLATE is 'Translation field for determining residual credit account';
comment on column LS_IMPORT_ILR_ARCHIVE.RES_CREDIT_ACCOUNT_ID is 'The residual credit account.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'res_debit_account_id',
           'Residual Value Debit Account',
           'res_debit_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
   (select PPIT.IMPORT_TYPE_ID,
           'res_credit_account_id',
           'Residual Value Credit Account',
           'res_credit_account_xlate',
           0,
           2,
           'number(22,0)',
           'gl_account',
           1,
           'gl_account_id'
      from PP_IMPORT_TYPE PPIT
     where PPIT.DESCRIPTION like 'Add: ILR%');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'res_debit_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
       and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)' or
           PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)' or
           PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   (select PPIT.IMPORT_TYPE_ID, 'res_credit_account_id', PPIL.IMPORT_LOOKUP_ID
      from PP_IMPORT_TYPE PPIT, PP_IMPORT_LOOKUP PPIL
     where PPIT.DESCRIPTION like 'Add: ILR%'
       and (PPIL.DESCRIPTION like 'GL Account.External Account Code (for given company)' or
           PPIL.DESCRIPTION like 'GL Account.Long Description (for given company)' or
           PPIL.DESCRIPTION like 'GL Account.Description (for given company)'));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (997, 0, 10, 4, 2, 0, 36571, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036571_lease_ilr_accounts_import.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;