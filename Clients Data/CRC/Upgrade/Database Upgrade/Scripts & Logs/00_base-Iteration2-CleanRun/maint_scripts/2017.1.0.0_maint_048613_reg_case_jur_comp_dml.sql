 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048613_reg_case_jur_comp_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 08/11/2017 Sarah Byers    New menu item for RMS Case to Jur Comparison
 ||============================================================================
 */ 

insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values (
	'REG', 'uo_reg_case_jur_comp_ws', 'Sync Case to Jur', 'uo_reg_case_jur_comp_ws', 'Sync Case Attributes to Jur Template', 1);

update ppbase_menu_items
	set item_order = 17
 where module = 'REG'
	and menu_identifier = 'SELECT_CASE'
	and workspace_identifier = 'uo_reg_case_definition_ws';

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values (
	'REG', 'CASE_SYNC', 2, 16, 'Sync Case to Jur', 'Sync Case Attributes to Jur Template', 'RATE_CASES', 'uo_reg_case_jur_comp_ws', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3644, 0, 2017, 1, 0, 0, 48613, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048613_reg_case_jur_comp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;