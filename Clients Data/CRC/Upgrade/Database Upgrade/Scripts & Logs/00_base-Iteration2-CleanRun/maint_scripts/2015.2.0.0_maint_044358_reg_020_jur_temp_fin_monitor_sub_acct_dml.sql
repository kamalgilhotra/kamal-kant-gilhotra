/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_044358_reg_020_jur_temp_fin_monitor_sub_acct_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- ----------------------------------------------
|| 2015.2.0.0 07/30/2015 Sarah Byers 		 	Add new Jur Template menu item and move existing menu item 
||========================================================================================
*/

-- Move the existing Analysis Key set up under the Company/Jur Setup workspace
update ppbase_menu_items
	set label = 'Analysis Key Setup',
		 minihelp = 'Analysis Key Setup',
		 workspace_identifier = 'uo_reg_fin_monitor_summary_setup_ws'
 where module = 'REG'
	and menu_identifier = 'MONITOR_SETUP';

delete from ppbase_menu_items
 where module = 'REG'
	and menu_identifier = 'FIN_MON_SETUP';

update ppbase_menu_items
	set item_order = item_order - 1
 where module = 'REG'
	and parent_menu_identifier = 'SETUP'
	and item_order > 6;

-- Create a new menu item for the new jur template workspace
insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
select module, 'uo_reg_jur_fin_mon_summ_setup_ws', 'Jur Analysis Key Setup', workspace_uo_name, 'Jur Analysis Key Setup', 1 
  from ppbase_workspace
 where module = 'REG'
	and workspace_identifier = 'uo_reg_fin_monitor_summary_setup_ws';

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values (
	'REG', 'JUR_MONITOR_SETUP', 2, 11, 'Jur Analysis Key Setup', 'Jur Analysis Key Setup', 'CASE_JUR_MAINT', 'uo_reg_jur_fin_mon_summ_setup_ws', 1);

-- Populate the new jur table for each existing template based on the company setup
insert into reg_jur_fin_monitor_sub_acct (
	reg_jur_template_id, reg_acct_type_id, sub_acct_type_id, reg_fin_monitor_summ_id)
select t.reg_jur_template_id, m.reg_acct_type_id, m.sub_acct_type_id, m.reg_fin_monitor_summ_id
  from reg_fin_monitor_sub_acct m, reg_jur_template t, reg_jurisdiction j
 where m.reg_company_id = j.reg_company_id
	and j.reg_jurisdiction_id = t.reg_jurisdiction_id;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2741, 0, 2015, 2, 0, 0, 044358, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044358_reg_020_jur_temp_fin_monitor_sub_acct_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;