/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032598_depr_retro_rate_change.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.2   11/04/2013 Brandon Beck
||============================================================================
*/

alter table DEPR_CALC_STG
   add (ORIG_DEPR_EXPENSE          number(22,2),
        ORIG_DEPR_EXP_ALLOC_ADJUST number(22,2),
        ORIG_SALV_EXPENSE          number(22,2),
        ORIG_SALV_EXP_ALLOC_ADJUST number(22,2),
        ORIG_COR_EXP_ALLOC_ADJUST  number(22,2),
        ORIG_COR_EXPENSE           number(22,2),
        RETRO_DEPR_ADJ             number(22,2),
        RETRO_COR_ADJ              number(22,2),
        RETRO_SALV_ADJ             number(22,2));

comment on column DEPR_CALC_STG.ORIG_DEPR_EXPENSE is 'The original DEPR_EXPENSE from DEPR_LEDGER. Used in retroactive rate change calculations.';
comment on column DEPR_CALC_STG.ORIG_DEPR_EXP_ALLOC_ADJUST is 'The original DEPR_EXP_ALLOC_ADJUST from DEPR_LEDGER. Used in retroactive rate change calculations.';
comment on column DEPR_CALC_STG.ORIG_SALV_EXPENSE is 'The original SALVAGE_EXPENSE from DEPR_LEDGER. Used in retroactive rate change calculations.';
comment on column DEPR_CALC_STG.ORIG_SALV_EXP_ALLOC_ADJUST is 'The original SALVAGE_EXP_ALLOC_ADJUST from DEPR_LEDGER. Used in retroactive rate change calculations.';
comment on column DEPR_CALC_STG.ORIG_COR_EXP_ALLOC_ADJUST is 'The original COR_EXP_ALLOC_ADJUST from DEPR_LEDGER. Used in retroactive rate change calculations.';
comment on column DEPR_CALC_STG.ORIG_COR_EXPENSE is 'The original COR_EXPENSE from DEPR_LEDGER. Used in retroactive rate change calculations.';
comment on column DEPR_CALC_STG.RETRO_DEPR_ADJ is 'The rolling sum of the difference between the original calculation and the retroactive calculation for depr expense.';
comment on column DEPR_CALC_STG.RETRO_COR_ADJ is 'The rolling sum of the difference between the original calculation and the retroactive calculation for COR expense.';
comment on column DEPR_CALC_STG.RETRO_SALV_ADJ is 'The rolling sum of the difference between the original calculation and the retroactive calculation for salvage expense.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (712, 0, 10, 4, 1, 2, 32598, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.2_maint_032598_depr_retro_rate_change.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
