/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029923_lease_early_ret.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   05/03/2013 Kyle Peterson  Point Release
||============================================================================
*/

create table LS_ILR_EARLY_RET
(
 ILR_ID              number(22,0) not null,
 LS_ASSET_ID         number(22,0) not null,
 FUTURE_PRIN_ACCRUAL number(22,2),
 FUTURE_INT_ACCRUAL  number(22,2),
 GL_POST_MONTH       date
);

alter table LS_ILR_EARLY_RET
   add constraint LS_ILR_EARLY_RET_PK
       primary key (ILR_ID, LS_ASSET_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ILR_EARLY_RET
   add constraint LS_ILR_EARLYRET_ILR_FK1
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);

alter table LS_ILR_EARLY_RET
   add constraint LS_ILR_EARLYRET_AST_FK1
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (376, 0, 10, 4, 0, 0, 29923, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029923_lease_early_ret.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
