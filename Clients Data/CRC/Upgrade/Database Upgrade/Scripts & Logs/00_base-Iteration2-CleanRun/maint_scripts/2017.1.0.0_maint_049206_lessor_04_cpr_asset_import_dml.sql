/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049206_lessor_04_cpr_asset_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/01/2017 Shane "C" Ward		Miscellaneous fixes for Import
||============================================================================
*/

UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    import_lookup_id = NULL
WHERE  import_lookup_id IN ( 1091, 1092 );

DELETE FROM PP_IMPORT_COLUMN_LOOKUP
WHERE  import_lookup_id IN ( 1091, 1092 );

DELETE FROM PP_IMPORT_LOOKUP
WHERE  import_lookup_id IN ( 1091, 1092 );

UPDATE PP_IMPORT_COLUMN
SET    is_required = 0
WHERE  import_type_id IN ( 503, 504 ) AND
       column_name IN ( 'economic_life', 'estimated_residual', 'estimated_residual_pct', 'expected_life',
                        'guaranteed_residual', 'revision', 'long_description', 'serial_number' );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 504,
             'carrying_cost',
             NULL,
             NULL,
             'Carrying Cost',
             null,
             0,
             1,
             'number(22,2)',
             NULL,
             NULL,
             1,
             NULL,
             NULL,
             NULL,
             NULL );
             
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 504 AND
                      description = 'Add: External Lessor Assets to ILR' ),
             12,
             504,
             'carrying_cost',
             NULL); 
						
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3862, 0, 2017, 1, 0, 0, 49206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049206_lessor_04_cpr_asset_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;						