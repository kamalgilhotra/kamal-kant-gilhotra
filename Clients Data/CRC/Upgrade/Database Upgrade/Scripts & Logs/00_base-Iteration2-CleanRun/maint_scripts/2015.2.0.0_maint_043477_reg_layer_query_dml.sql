/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043477_reg_layer_query_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| --------   ---------- ---------------- --------------------------------------
|| 2015.2.0.0 06/18/2015 Sarah Byers		
||============================================================================
*/

delete from cr_dd_sources_criteria_fields
 where id in (select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_IFA_VIEW');

delete from cr_dd_sources_criteria
 where upper(table_name) = 'REG_FORECAST_LEDGER_IFA_VIEW';

insert into cr_dd_sources_criteria (
	id, source_id, criteria_field, table_name, description, feeder_field)
select max(id) + 1 id,
		 1002, 'none', 'reg_forecast_ledger_layer_view', 'Forecast Ledger with Regulatory Layers', 'none'
  from cr_dd_sources_criteria;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'ADJ_AMOUNT', 14, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Adj Amount', 313, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'ADJ_MONTH', 15, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Adj Month', 281, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'ANNUALIZED_AMT', 13, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Annualized Amt', 418, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'ANNUALIZED_TOTAL', 21, to_date('2015-06-18 15:50:55', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Annualized Total', 454, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'ANNUALZIED_LAYER_AMOUNT', 17, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Annualzied Layer Amount', 674, 0, 'Any', null, null, 0, null, null,
 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'FCST_AMOUNT', 12, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Fcst Amount', 340, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'FORECAST_LEDGER', 2, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Forecast Ledger', 857, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'FORECAST_LEDGER_ID', 24, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Forecast Ledger ID', 509, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'GL_MONTH', 11, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'GL Month', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'INCREMENTAL_ADJ_ID', 28, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Incremental Adj ID', 491, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'INCREMENTAL_ADJ_TYPE_ID', 29, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Incremental Adj Type ID', 637, 0, 'Any', null, null, 0, null, null,
 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'ITEM', 10, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Item', 793, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'LABEL', 9, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Label', 1003, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'LAYER_AMOUNT', 16, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Layer Amount', 409, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'LAYER_DESCRIPTION', 8, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Layer Description', 710, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'LAYER_TYPE', 7, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Layer Type', 587, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'RECON_ADJ_AMOUNT', 18, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Recon Adj Amount', 500, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'RECON_ADJ_COMMENT', 19, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Recon Adj Comment', 541, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_ACCOUNT', 3, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Account', 1926, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_ACCOUNT_TYPE', 4, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Account Type', 715, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_ACCT_ID', 23, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Acct ID', 331, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_ACCT_TYPE_ID', 26, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Acct Type ID', 477, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_COMPANY', 1, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Company', 687, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_COMPANY_ID', 22, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Company ID', 445, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_SOURCE', 6, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Source', 820, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'REG_SOURCE_ID', 25, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Reg Source ID', 395, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'SUB_ACCOUNT_TYPE', 5, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Sub Account Type', 953, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'SUB_ACCT_TYPE_ID', 27, to_date('2015-06-18 15:50:19', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Sub Acct Type ID', 473, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_FORECAST_LEDGER_LAYER_VIEW'), 
'TOTAL', 20, to_date('2015-06-18 15:50:55', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Total', 409, 0, 'Any', null, null, 0, null, null, 0) ; 

delete from cr_dd_sources_criteria_fields
 where id in (select id from cr_dd_sources_criteria where upper(table_name) = 'REG_IFA_DETAILS_VIEW');

delete from cr_dd_sources_criteria
 where upper(table_name) = 'REG_IFA_DETAILS_VIEW';

insert into cr_dd_sources_criteria (
	id, source_id, criteria_field, table_name, description, feeder_field)
select max(id) + 1 id,
		 1002, 'none', 'reg_layer_fp_details_view', 'Detailed Layer View - FP Adjustment', 'none'
  from cr_dd_sources_criteria;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'DIFFERENCE', 11, to_date('2015-06-24 09:25:01', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Difference', 294, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'FUNDING_PROJECT_NUMBER', 3, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Funding Project Number', 642, 0, 'Any', null, null, 0, null, null, 
0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'ITEM', 6, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Item', 738, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'ITEM_SORT', 14, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Item Sort', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'LABEL', 5, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Label', 1003, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'LABEL_SORT', 13, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Label Sort', 285, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'LAYER', 1, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Layer', 386, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'LONG_DESCRIPTION', 2, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 459, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'MONTH_YEAR', 4, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Month Year', 317, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'ORIGINAL_AMOUNT', 8, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Original Amount', 427, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'ORIGINAL_REVISION', 7, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Original Revision', 459, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'UPDATED_AMOUNT', 10, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Updated Amount', 450, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'UPDATED_REVISION', 9, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Updated Revision', 482, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_FP_DETAILS_VIEW'),  
'WO_DESCRIPTION', 12, to_date('2015-06-24 09:24:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'FP Description', 418, 0, 'Any', null, null, 0, null, null, 0) ; 

insert into cr_dd_sources_criteria (
	id, source_id, criteria_field, table_name, description, feeder_field)
select max(id) + 1 id,
		 1002, 'none', 'reg_layer_ea_details_view', 'Detailed Layer View - Existing Assets', 'none'
  from cr_dd_sources_criteria;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'AMOUNT', 11, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Amount', 212, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'AS_OF_DATE', 5, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'As Of Date', 303, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'END_MONTH', 7, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'End Month', 299, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'ITEM', 10, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Item', 980, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'ITEM_SORT', 13, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Item Sort', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'LABEL', 9, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Label', 1003, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'LABEL_SORT', 12, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Label Sort', 285, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'LAYER', 1, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Layer', 276, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'LONG_DESCRIPTION', 2, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 459, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'MONTH_YEAR', 8, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Month Year', 317, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'SELECTED_FIELD', 3, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Selected Field', 427, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'SELECTED_VALUE', 4, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Selected Value', 1538, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_EA_DETAILS_VIEW'), 
'START_MONTH', 6, to_date('2015-06-23 11:10:37', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Start Month', 322, 0, 'Any', null, null, 0, null, null, 0) ; 

insert into cr_dd_sources_criteria (
	id, source_id, criteria_field, table_name, description, feeder_field)
select max(id) + 1 id,
		 1002, 'none', 'reg_layer_depr_details_view', 'Detailed Layer View - Depr Adjustment', 'none'
  from cr_dd_sources_criteria;

INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'DIFFERENCE', 14, to_date('2015-06-24 09:03:43', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Difference', 390, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'ITEM', 5, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Item', 527, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'ITEM_SORT', 16, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Item Sort', 253, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'LABEL', 4, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Label', 500, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'LABEL_SORT', 15, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Label Sort', 285, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'LAYER', 1, to_date('2015-06-24 09:03:26', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Layer', 587, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'LONG_DESCRIPTION', 2, to_date('2015-06-24 09:03:26', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Long Description', 1286, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'MONTH_YEAR', 3, to_date('2015-06-24 09:03:26', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Month Year', 317, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'ORIGINAL_AMOUNT', 9, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Original Amount', 427, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'ORIGINAL_FCST_DEPR_VERSION', 6, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Original Fcst Depr Version', 697, 0, 'Any', null, null, 0, null, 
null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'ORIGINAL_SET_OF_BOOKS', 7, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Original Set Of Books', 578, 0, 'Any', null, null, 0, null, null, 0) 
; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'ORIGINAL_TAX_CASE', 8, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Original Tax Case', 477, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'UPDATED_AMOUNT', 13, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 1, 1, null, 'Updated Amount', 450, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'UPDATED_FCST_DEPR_VERSION', 10, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Updated Fcst Depr Version', 719, 0, 'Any', null, null, 0, null, 
null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'UPDATED_SET_OF_BOOKS', 11, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Updated Set Of Books', 601, 0, 'Any', null, null, 0, null, null, 0) ; 
INSERT INTO "CR_DD_SOURCES_CRITERIA_FIELDS" ("ID", "DETAIL_FIELD", "COLUMN_ORDER", "TIME_STAMP", "USER_ID", "AMOUNT_FIELD", "INCLUDE_IN_SELECT_CRITERIA", "DEFAULT_VALUE", 
"COLUMN_HEADER", "COLUMN_WIDTH", "QUANTITY_FIELD", "COLUMN_CASE", "ELEMENT_ID", "FORMAT_OVERRIDE", "REQUIRED_FILTER", "REQUIRED_ONE_MULTI", "DDDW_SQL", "TABLE_LOOKUP") 
VALUES ((select id from cr_dd_sources_criteria where upper(table_name) = 'REG_LAYER_DEPR_DETAILS_VIEW'),  
'UPDATED_TAX_CASE', 12, to_date('2015-06-24 09:03:27', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 0, 1, null, 'Updated Tax Case', 500, 0, 'Any', null, null, 0, null, null, 0) ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2642, 0, 2015, 2, 0, 0, 043477, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043477_reg_layer_query_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;