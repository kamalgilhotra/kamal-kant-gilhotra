/*
||==============================================================================
|| Application: PowerPlan
|| File Name: maint_039724_lease_component_import_template.sql
||==============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ------------------------------------------
|| 10.4.3.0 08/21/2014 Daniel Motter  Add default import template for components
||==============================================================================
*/

declare
   TYPE_ID           NUMBER(22,0);
   TEMPLATE_ID       NUMBER(22,0);
   COMPANY_LOOKUP_ID NUMBER(22,0);
   ASSET_LOOKUP_ID   NUMBER(22,0);
begin
   --Fill variables

   --Import type ID for 'Add: Lease Components' is 255
   TYPE_ID := 255;

   --Just get the next value in the sequence for PP_IMPORT_TEMPLATE - it is not a fixed table
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL
     into TEMPLATE_ID
     from DUAL;

   --Data in PP_IMPORT_lOOKUP is messed up for these fields (company and asset)
   --    The id's should be between 2501 and 3000 (for Lease), but in masterDB and a few
   --    other qa and dev databases, they are not and are different in different databases.
   --       So, look up their id's by the description we want.
   select max(IMPORT_LOOKUP_ID)
     into COMPANY_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where DESCRIPTION = 'Company.Description';

   select max(IMPORT_LOOKUP_ID)
     into ASSET_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where DESCRIPTION = 'Ls Asset.Description';

   --Fix required fields
   update PP_IMPORT_COLUMN
      set IS_REQUIRED = 0
    where IMPORT_TYPE_ID = TYPE_ID;

   update PP_IMPORT_COLUMN
      set IS_REQUIRED = 1
    where IMPORT_TYPE_ID = TYPE_ID
      and COLUMN_NAME in ('company_id', 'description', 'amount');

   --Make sure translation options are present
   insert into PP_IMPORT_COLUMN_LOOKUP
      (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select TYPE_ID, 'company_id', COMPANY_LOOKUP_ID
     from DUAL
    where not exists (select 1
                        from PP_IMPORT_COLUMN_LOOKUP
                       where IMPORT_TYPE_ID = TYPE_ID
                         and COLUMN_NAME = 'company_id'
                         and IMPORT_LOOKUP_ID = COMPANY_LOOKUP_ID);

   insert into PP_IMPORT_COLUMN_LOOKUP
      (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select TYPE_ID, 'ls_asset_id', ASSET_LOOKUP_ID
     from DUAL
    where not exists (select 1
                        from PP_IMPORT_COLUMN_LOOKUP
                       where IMPORT_TYPE_ID = TYPE_ID
                         and COLUMN_NAME = 'ls_asset_id'
                         and IMPORT_LOOKUP_ID = ASSET_LOOKUP_ID);

   --Add template to PP_IMPORT_TEMPLATE table
   insert into PP_IMPORT_TEMPLATE
      (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, IS_AUTOCREATE_TEMPLATE)
   values
      (TEMPLATE_ID, TYPE_ID, 'Lease Component Add', 'Lease Component Add', 'PWRPLANT', sysdate, 1, 0);

   --Add template fields to PP_IMPORT_TEMPlATE_FIELDS
   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 1, TYPE_ID, 'component_id');

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   values
      (TEMPLATE_ID, 2, TYPE_ID, 'company_id', COMPANY_LOOKUP_ID);

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 3, TYPE_ID, 'description');

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 4, TYPE_ID, 'date_received');

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 5, TYPE_ID, 'amount');

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 6, TYPE_ID, 'interim_interest_start_date');

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 7, TYPE_ID, 'long_description');

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   values
      (TEMPLATE_ID, 8, TYPE_ID, 'ls_asset_id', ASSET_LOOKUP_ID);

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 9, TYPE_ID, 'po_number');

   insert into PP_IMPORT_TEMPLATE_FIELDS
      (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME)
   values
      (TEMPLATE_ID, 10, TYPE_ID, 'serial_number');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1366, 0, 10, 4, 3, 0, 39724, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039724_lease_component_import_template.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;