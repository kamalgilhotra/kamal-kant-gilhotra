/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_037582_sys_pp_htmlhelp.sql
|| Description: Insert statements for PP_HTMLHELP.
||              Reload the pp_htmlhelp table with the latest map_id's for
||              accessing the context sensitive help system.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 04/04/2014 Lee Quinn
||============================================================================
*/

/*
select DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID
  from PP_HTMLHELP
 order by COUNTER_ID;
*/

begin
   execute immediate 'alter table PP_HTMLHELP modify W_NAME varchar2(255)';
exception
   when others then
      null;
end;
/

delete from pp_htmlhelp;
commit;

insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'introduction', '1_1_Overview', 1, null, 1);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'introduction', '1_2_PowerPlan_Application_Design', 2, null, 2);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'introduction', '1_3_Using_the_Main_Application_ToolBar', 3, null, 3);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'introduction', 'Introduction', 4, null, 4);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '1_1_Overview1', 1000, null, 5);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '1_2_Navigation_Using_the_Asset_Management_Toolbar', 1001, 'w_asset_main', 6);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '10_1_Overview', 1002, null, 7);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '10_2_Using_the_Interface_Selection_Window', 1003, 'w_interface', 8);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '11_1_Overview', 1004, null, 9);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '11_2_Using_the_Handy_Whitman_Loading_Interface_Window', 1005, 'w_hw_interface', 10);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '12_1_Overview', 1006, null, 11);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '12_2_Running_Reports', 1007, null, 12);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '12_2_1_Using_the_PowerPlan_Reporting_Window__Asset_Summary_Level', 1008, 'w_reporting_main', 13);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '12_2_2_Using_the_PowerPlan_Reporting_Window__CPR_Detail_Level', 1009, null, 14);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '12_2_3_Using_the_PowerPlan_Reporting_Window_Subledger_Reports', 1010, null, 15);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '12_2_4_Creating_a_Batch_Report', 1011, null, 16);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '12_2_5_Reporting_Structure_and_Modification', 1012, null, 17);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_1_Overview', 1013, null, 18);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_2_Using_the_Continuing_Property_Records_Control_Window', 1014, 'w_cpr_control', 19);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_2_1_Using_the_CPR_Control_Taskbar', 1015, null, 20);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_2_2_Email_from_Closing_Processes', 1016, null, 21);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_2_3_Statistics_from_Closing_Processes', 1017, null, 22);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_2_4_Using_the_PowerPlan_CPR_Logs_Window', 1018, 'w_pp_online_logs_close', 23);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_2_5_Using_the_Verify_Alert_Results_Window__Balancing_Results', 1019, 'w_pp_verify_errors2', 24);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_2_6_Using_the_Verify_Alert_Results_Details_Window__Balancing_Results', 1020, null, 25);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_3_Transactions', 1021, null, 26);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_3_1_Using_the_Transactions_Toolbar', 1022, 'w_pend_trans_admin', 27);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_3_2_Using_the_Pending_Transaction_Administration_Window', 1023, 'w_cpr_trans_manage', 28);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_3_3_Using_the_Pending_Transaction_Detail_Window', 1024, 'w_pend_trans_detail', 29);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_3_4_Using_the_Pending_Subledger_Administration_Window', 1025, 'w_pend_subl_trans_manage', 30);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_3_5_Using_the_Addition_Basis_Account_Information_Window', 1026, 'w_cpr_add_basis', 31);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_3_6_Using_the_Pending_Transaction_Administration_Toolbar', 1027, null, 32);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_4_Transaction_History', 1028, null, 33);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '13_4_1_Using_the_Transaction_History_Window', 1029, null, 34);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_1_Overview', 1030, null, 35);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_1_1_PowerPlan_General_Flow_Diagram', 1031, null, 36);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_2_PowerPlan_Post_Program_Processing_Logic', 1032, null, 37);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_2_1_Post__Tables', 1033, null, 38);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_2_2_Post__Inputs_and_Overall_Flow', 1034, null, 39);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_2_3_Post_Processing', 1035, null, 40);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_PowerPlan_Post_Program_Technical_Information', 1036, null, 41);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_1_Post_Transaction_Approval', 1037, null, 42);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_10Post_Transaction_History', 1038, null, 43);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_11Activity_History_vs_Transaction_History', 1039, null, 44);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_12CPR_Activity_Codes', 1040, null, 45);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_13Connection_between_CPR_Activities_Pending_Trans_Archive', 1041, null, 46);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_14_Journal_Entry_Table_gl_transaction', 1042, null, 47);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_2_Post_How_to_Run_It', 1043, 'w_post_control', 48);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_3_Post_Errors', 1044, null, 49);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_4_Pending_Transaction_Post_Status', 1045, null, 50);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_5_Post_Control_Window', 1046, null, 51);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_6_Debugging_the_Post_Program', 1047, null, 52);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_7_Debugging_the_Performance_of_Post', 1048, null, 53);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_8_Post_Version_Check', 1049, null, 54);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_3_9_Post_GL_Entries', 1050, null, 55);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_4_PP_Journal_Layouts', 1051, null, 56);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_4_1_Background', 1052, null, 57);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_4_2_Setup', 1053, null, 58);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_4_3_Configuration', 1054, null, 59);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '14_4_4_GL_Account_and_the_CR', 1055, null, 60);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_Overview', 1056, null, 61);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_10_Transferring_Assets', 1057, null, 62);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_10_1_Using_the_CPR_Transfers_Window', 1058, 'w_cpr_transfer_free', 63);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_10_2_Basic_Steps_to_Transfer_an_Asset', 1059, null, 64);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_10_3_Transfers_of_Related_Assets', 1060, null, 65);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_11_Reversing_Non_Unitized_Assets', 1061, null, 66);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_12_Adjusting_Assets', 1062, null, 67);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_12_1_Using_the_CPR_Adjustments_Window', 1063, 'w_cpr_adjustment_free', 68);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_12_2_The_Basic_Steps_to_Adjust_an_Asset_s_', 1064, null, 69);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_13_Adding_Assets', 1065, null, 70);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_13_1_Using_the_Addition_Entry_Details_Window', 1066, 'w_cpr_addition_detail', 71);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_13_2_Using_the_Addition_Basis_Account_Information_Window', 1067, 'w_cpr_addition_accounts', 72);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_14_Viewing_Pending_Transactions', 1068, null, 73);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_14_1_Checking_for_Pending_Transactions', 1069, null, 74);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_15_Assigning_Classification_Codes', 1070, null, 75);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_15_1_Overview', 1071, null, 76);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_15_2_Using_the_CPR_Classification_Code_Update_Window', 1072, 'w_cpr_class_code', 77);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_15_3_Basic_Steps_to_Update_Class_Codes_on_Assets', 1073, null, 78);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_2_Navigation_Using_the_CPR_Toolbar', 1074, null, 79);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_Searching_for_Assets', 1075, null, 80);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_1_Using_the_PowerPlan_CPR_Asset_Selection_Window', 1076, 'w_cpr_select_tabs', 81);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_10_Searching_by_Class_Code', 1077, null, 82);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_11_Other_Search_Criteria', 1078, null, 83);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_12_Drill_to_Work_Order', 1079, null, 84);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_13_Saving_and_Re_running_Queries', 1080, 'w_cpr_query', 85);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_14_Using_the_CPR_Datagrid', 1081, null, 86);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_2_Searching_by_Company', 1082, null, 87);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_3_Searching_by_GL_Account', 1083, null, 88);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_4_Searching_by_Utility_Account', 1084, null, 89);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_5_Searching_by_Property_Tax_District', 1085, null, 90);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_6_Searching_by_Location', 1086, null, 91);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_7_Searching_by_Retirement_Unit', 1087, null, 92);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_8_Searching_by_Depreciation_Group', 1088, null, 93);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_3_9_Searching_by_Work_Order_Number', 1089, null, 94);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_Viewing_Editing_Asset_Information', 1090, null, 95);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_1_Using_the_CPR_Ledger_Detail_Asset_Details_Window', 1091, 'w_cpr_ledger_detail', 96);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_2_Editing_the_Asset_Description', 1092, null, 97);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_3_Editing_the_Accumulated_Quantity', 1093, null, 98);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_4_Reviewing_or_Assigning_a_Class_Code_Value', 1094, null, 99);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_5_Adding_Reviewing_Additional_Comments', 1095, null, 100);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_6_Viewing_an_Assets_Cost_Basis', 1096, 'w_cpr_ldg_basis', 101);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_4_7_Accessing_an_Assets_Supporting_Subledger', 1097, 'w_cpr_subledger_grid', 102);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_5_CPR_Depreciation_Information', 1098, null, 103);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_5_1_Accessing_an_Assets_CPR_Depreciation_Information', 1099, 'w_cpr_depr', 104);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_6_Relating_Assets', 1100, null, 105);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_6_1_Overview', 1101, null, 106);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_6_2_Relating_Multiple_Assets_or_Individual_Assets', 1102, 'w_cpr_relate', 107);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_7_Viewing_CPR_Activities', 1103, null, 108);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_7_1_Using_the_CPR_Ledger_Entry_Activity_Window', 1104, 'w_cpr_activity_grid', 109);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_7_2_Using_the_CPR_Ledger_Entry_Activity_Detail_Window', 1105, 'w_cpr_activity_detail', 110);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_8_Performing_a_Net_Book_Value_Analysis', 1106, null, 111);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_8_1_Using_the_Financial_Analysis_Window', 1107, 'w_cpr_fin_analysis', 112);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_Retiring_Assets', 1108, null, 113);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_1_Using_the_CPR_Retirements_Window', 1109, 'w_cpr_retirement_free', 114);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_2_Tax_Only_Test_Transactions', 1110, 'w_tax_test_version', 115);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_3_Mass_Retirements', 1111, 'w_cpr_mass_retirement', 116);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_4_Using_the_Partial_Retirements_Indexing_Window', 1112, 'w_cpr_trending_info', 117);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_5_Basic_Steps_to_Retire_an_Asset', 1113, null, 118);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_6_Auto_Retire_for_Assets', 1114, null, 119);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_7_Unretiring_an_Asset', 1115, null, 120);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_8_Related_Asset_Retires', 1116, null, 121);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_9_9_Allow_Post_to_Find_Asset_for_Specific_Retirement', 1117, null, 122);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_1_Overview', 1118, null, 123);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_2_Navigation_Using_the_Equipment_Registry_Taskbar', 1119, 'w_equip_main', 124);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_3_Navigation_Using_the_Equipment_Registry_Select_Tabs_Taskbar', 1120, null, 125);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_4_Using_the_Equipment_Registry_Select_Tabs_Window', 1121, 'w_equip_ledger_select_tabs', 126);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_5_Viewing_Editing_Equipment_Registry_Asset_Information', 1122, null, 127);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_6_Equipment_Retirements', 1123, null, 128);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_7_New_Equipment_Entry', 1124, 'w_equip_new_equip', 129);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_8_Configuration', 1125, null, 130);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_1_Overview', 1126, null, 131);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_1_1_Background', 1127, null, 132);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_1_2_Accounting_Summary', 1128, null, 133);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_2_Using_the_CPR_Impairment_Tool', 1129, null, 134);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_3_Impairment_Accounting_in_PowerPlan', 1130, null, 135);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_3_1_Validations', 1131, null, 136);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_3_2_Journal_Entries', 1132, null, 137);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_3_3_Reversals', 1133, null, 138);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_4_Non_Unitized_106_Impairments', 1134, null, 139);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_5_CWIP_Impairments', 1135, null, 140);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_6_Effect_of_Other_Asset_Transactions_on_Impaired_Plant', 1136, null, 141);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_1_Overview', 1137, null, 142);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_10_Adding_Subledger_Assets', 1138, 'w_cpr_subl_add_detail', 143);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_2_Adding_a_New_Subledger_using_PowerPlan_Tables_', 1139, null, 144);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_3_Navigation_Using_the_Subledger_Toolbar', 1140, null, 145);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_Searching_for_Subledger_Assets', 1141, 'w_subledger_select_tabs', 146);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_1_Searching_by_Company', 1142, null, 147);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_2_Searching_by_GL_Account', 1143, null, 148);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_3_Searching_by_Utility_Account', 1144, null, 149);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_4_Searching_by_Location', 1145, null, 150);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_5_Searching_by_Retirement_Unit', 1146, null, 151);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_6_Searching_by_Depreciation_Group', 1147, null, 152);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_7_Searching_by_Work_Order', 1148, null, 153);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_8_Searching_by_Column', 1149, null, 154);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_9_Other_Search_Criteria', 1150, null, 155);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_5_Viewing_Editing_Subledger_Asset_Information', 1151, 'w_cpr_subledger_detail', 156);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_6_Accessing_the_CPR_Ledger_Entry_window', 1152, 'w_cpr_ledger_detail_not_shared', 157);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_7_Adjusting_Subledger_Assets', 1153, 'w_cpr_subledger_adjust', 158);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_8_Retiring_Subledger_Assets', 1154, 'w_cpr_subledger_retire', 159);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_9_Transferring_Subledger_Assets', 1155, 'w_cpr_subledger_transfer', 160);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_9_1_To_Item_Transfer', 1156, null, 161);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_9_2_Entire_Balance_Transfer', 1157, null, 162);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_1_Overview', 1158, null, 163);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_1_1_Elements_of_Asset_Conversion', 1159, null, 164);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_2_Entering_Control_Batch_Information', 1160, null, 165);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_2_1_Using_the_CPR_Conversion_Batch_Control_Window', 1161, 'w_cpr_conversion_batch', 166);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_3_Viewing_Editing_CPR_Conversion_Information', 1162, null, 167);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_3_1_Using_the_CPR_Conversion_Entries_Window', 1163, 'w_cpr_conversion_add', 168);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_3_2_Using_the_CPR_Conversion_Entry_Activities_Window', 1164, 'w_cpr_conversion_activity', 169);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_3_3_Using_the_CPR_Conversion_Basis_Window', 1165, 'w_cpr_conv_act_basis', 170);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_4_Maintaining_Batches', 1166, null, 171);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_4_1_Using_the_Conversion_Batch_Control_Maintenance_Window', 1167, 'w_cpr_conversion_batch_maint', 172);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_1_Overview', 1168, null, 173);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_1_1_CPR_Activities', 1169, null, 174);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_1_2_CPR_Transaction_Archives', 1170, null, 175);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_2_Querying_CPR_Activity_and_Transaction_Archives', 1171, null, 176);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_2_1_Using_the_CPR_Activities_by_Month_Window', 1172, 'w_cpr_activity_history', 177);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_2_2_Using_the_CPR_Transaction_Archive_Window', 1173, 'w_pend_archive', 178);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_1_Overview', 1174, null, 179);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_2_Building_a_Query', 1175, null, 180);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_2_1_Using_the_CPR__User_Defined_Query_Window', 1176, 'w_cpr_ledger_dollars', 181);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_2_2_Saving_Your_Query', 1177, 'w_pp_query_dw', 182);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_2_3_CPR_Any_Query', 1178, null, 183);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_1_Overview', 1179, null, 184);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_10_Building_User_Defined_Mappings', 1180, 'w_pp_conv_mapping', 185);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_11_Mapping_Reports', 1181, null, 186);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_12_Validating_Asset_Data', 1182, null, 187);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_13_Detail_Batch_Asset_Reports', 1183, 'w_conv_report_display', 188);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_14_Committing_to_the_Ledger', 1184, null, 189);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_15_Finding_Committed_Assets', 1185, null, 190);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_16_Note_for_Mass_Assets_and_Asset_Activities', 1186, null, 191);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_2_Using_the_CPR_Loader', 1187, 'w_pp_conv_cpr', 192);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_3_Viewing_and_Sorting_Data', 1188, null, 193);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_4_Loading_or_Deleting_a_Saved_Batch', 1189, 'w_pp_conv_batch_select', 194);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_5_Creating_a_New_Batch', 1190, 'w_pp_conv_batches', 195);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_6_Editing_Batch_Properties', 1191, 'w_pp_conv_batches_edit', 196);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_7_Loading_an_Asset_File', 1192, 'w_pp_conv_import', 197);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_8_Saving_a_File_Template', 1193, null, 198);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_9_Updating_Values_for_Individual_Records', 1194, null, 199);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_1_Introduction_to_Asset_Management', 1195, null, 200);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_10_CPR_Interfaces', 1196, null, 201);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_11_Handy_Whitman_Tables', 1197, null, 202);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_12_Financial_Reports', 1198, null, 203);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_13_CPR_Closing_Process', 1199, null, 204);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_14_PowerPlan_Post_Program', 1200, null, 205);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_2_PowerPlan_CPR', 1201, null, 206);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_3_Equipment_Registry', 1202, null, 207);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_4_Asset_Impairments', 1203, null, 208);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_5_CPR_Supporting_Subledgers', 1204, null, 209);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_6_Asset_Conversion', 1205, null, 210);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_7_CPR_Activities_and_Transactions_History', 1206, null, 211);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_8_Other_CPR_Query_Tools', 1207, null, 212);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_9_CPR_Loader', 1208, null, 213);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '1_1_Introduction', 2000, 'w_budget_main', 214);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '1_2_Definitions', 2001, null, 215);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '1_3_System_Navigation', 2002, null, 216);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '1_4_Project_Module_vs_Budgeting_Module', 2003, null, 217);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '10_1_Creating_a_Substitution', 2004, null, 218);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '10_2_Substitution_Review', 2005, null, 219);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '10_3_Reviewing_a_Substitution_Accept_or_Reject_', 2006, null, 220);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '10_4_Setup_Review_Types_and_Levels', 2007, null, 221);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '11_1_Overview1', 2008, null, 222);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '11_2_Accrual_Types', 2009, null, 223);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '11_3_Attaching_an_Accrual_Type_to_a_Funding_Project_or_Work_Order', 2010, null, 224);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '11_4_Calculating_Accruals', 2011, null, 225);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '11_5_Approving_Accruals', 2012, null, 226);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_1_Introduction', 2013, null, 227);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_10_Analyzing_Actuals_vs_Budgeted_Amounts', 2014, null, 228);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_2_Closing_a_Funding_Project', 2015, null, 229);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_3_Drilling_to_Work_Orders', 2016, null, 230);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_4_Funding_Project_Balances', 2017, null, 231);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_5_Funding_Project_Charges', 2018, null, 232);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_6_Maintaining_Class_Codes', 2019, null, 233);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_7_Maintaining_Other_Funding_Project_Attributes', 2020, null, 234);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_8_Project_Dashboards', 2021, null, 235);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '12_9_Viewing_External_Documents', 2022, null, 236);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_1_Introduction', 2023, null, 237);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_2_Creating_a_new_ranking_scenario', 2024, 'w_bv_proj_rank', 238);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_3_Building_the_Ranking_Formula', 2025, null, 239);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_4_Running_Viewing_the_Ranking_Scenario', 2026, null, 240);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_5_Modifying_Project_Estimates', 2027, 'w_bv_proj_rank_results', 241);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_5_1_Sliding_Recalculating_Projects', 2028, null, 242);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_5_2_Deferring_Accelerating_Projects', 2029, null, 243);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_5_3_Scaling_Project_Annual_Amounts', 2030, null, 244);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_6_Maintaining_Ranking_Fields', 2031, null, 245);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_7_Maintaining_Additional_Attributes', 2032, null, 246);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_8_Project_Ranking_Cost_Filter_Setup', 2033, null, 247);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '13_9_Applying_a_Filter_to_the_Scenario', 2034, null, 248);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '14_1_Introduction', 2035, null, 249);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '14_2_Global_Reports', 2036, 'w_fp_report_types', 250);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '14_3_Selection_Reports', 2037, null, 251);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_1_Overview', 2038, null, 252);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_10_General_Navigation_Restoring_a_Saved_Query', 2039, null, 253);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_11_Funding_Project_Charges_Query_Tool', 2040, null, 254);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_12_Funding_Project_Estimates_Query_Tool', 2041, null, 255);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_13_Estimate_vs_Actual_Query_Tools', 2042, null, 256);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_14_Estimate_vs_Actual_Query_Tools_Computed_Field', 2043, null, 257);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_15_Estimate_vs_Actual_Query_Tools_Advanced_Computed_Field', 2044, null, 258);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_16_Estimate_vs_Actual_Query_Tools_Build_Filter_Tab', 2045, null, 259);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_17_Estimate_vs_Actual_Query_Tools_Modify_Query_Results', 2046, null, 260);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_18_Estimate_vs_Actual_Query_Tools_Work_Order', 2047, null, 261);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_19_Estimate_vs_Actual_Query_Tools_Funding_Project', 2048, null, 262);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_2_General_Navigation', 2049, 'w_choose_cwip_dollars', 263);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_3_General_Navigation_Dollar_Attributes', 2050, 'w_cwip_charge_dollars_fp', 264);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_4_General_Navigation_Header_Attributes', 2051, 'w_cwip_charge_dollars_fp_funding_project_attributes', 265);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_5_General_Navigation_Class_Code_Filter_Options', 2052, null, 266);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_6_General_Navigation_Subtotals', 2053, null, 267);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_7_General_Navigation_Build_Filters', 2054, null, 268);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_8_General_Navigation_Query_Results', 2055, null, 269);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '15_9_General_Navigation_Saving_Your_Query', 2056, null, 270);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '16_1_Introduction', 2057, null, 271);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '16_2_Definitions', 2058, null, 272);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '17_1_Introduction', 2059, null, 273);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '17_2_Viewing_Budget_Items', 2060, 'w_budget_elements', 274);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '18_1_Introduction', 2061, null, 275);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '18_2_Budget_Item_Selection_Getting_Started', 2062, 'w_budget_select_tabs', 276);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '18_3_Budget_Item_Selection__Entering_a_Budget_Number', 2063, null, 277);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '18_4_Budget_Item_Selection__Using_Other_Criteria', 2064, null, 278);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '18_5_Budget_Item_Selection__Saving_and_Re_Running_a_Query', 2065, null, 279);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '19_1_Introduction', 2066, null, 280);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '19_2_Budget_Item_Headers_Details', 2067, 'w_budget_detail', 281);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '19_3_Budget_Item_Headers__Class_Code_Tab', 2068, null, 282);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '19_4_Budget_Item_Headers__Approvals_Tab', 2069, null, 283);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '19_5_Budget_Item_Headers__Documents_Tab', 2070, null, 284);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_1_Introduction', 2071, null, 285);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_10_WO_Group_Budget_Organization__Setting_Up_the_Mapping', 2072, null, 286);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_11_Budget_Summaries__Setting_Them_Up', 2073, null, 287);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_12_WO_Type_Budget_Summary__Setting_Up_the_Mapping', 2074, null, 288);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_13_Budget_Plant_Classes_and_Depreciation_Groups', 2075, null, 289);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_14_Budget_Summary_Budget_Plant_Class_Mapping', 2076, null, 290);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_15_Expenditure_Types', 2077, null, 291);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_16_Estimate_Charge_Types', 2078, null, 292);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_17_Estimate_Charge_Types__Setting_Them_Up', 2079, null, 293);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_2_Funding_Project_Types', 2080, null, 294);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_3_Funding_Project_Types__Defining_the_Types', 2081, null, 295);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_4_Funding_Project_Types__Setting_Them_Up', 2082, null, 296);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_5_Work_Order_WO_Groups', 2083, null, 297);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_6_WO_Groups__Setting_Them_Up', 2084, null, 298);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_7_WO_Groups__Setting_Up_Defaults_based_on_WO_Type', 2085, null, 299);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_8_Types_and_Groups__How_They_Relate_to_the_Budget_Module', 2086, null, 300);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '2_9_Budget_Organizations__Setting_Them_Up', 2087, null, 301);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '20_1_Introduction', 2088, null, 302);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '20_2_Grid_Estimates', 2089, null, 303);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '21_1_Sending_for_Approval', 2090, null, 304);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '21_2_Approving_a_Budget_Item_Accept_or_Reject_', 2091, 'w_budget_approval_list', 305);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '21_3_Setup_Approval_Types_and_Levels', 2092, null, 306);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '22_1_Introduction', 2093, null, 307);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '22_2_Adding_a_Budget_Item_to_a_Version', 2094, 'w_budget_add', 308);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '22_3_Drilling_to_Funding_Projects', 2095, null, 309);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '22_4_Maintaining_Class_Codes', 2096, 'w_budget_class_code', 310);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '22_5_Analysis', 2097, 'w_budget_analysis', 311);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_1_Introduction', 2098, 'w_budget_main', 312);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_2_Tables', 2099, null, 313);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_3_Defining_and_maintaining_Overheads', 2100, 'w_wo_clear_maintenance_bdg', 314);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_3_1_Parameters', 2101, null, 315);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_3_2_Overhead_Rates', 2102, null, 316);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_3_3_Applicable_Budget_Items', 2103, null, 317);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_3_4_Overhead_Basis_Definition', 2104, null, 318);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_4_Defining_and_Maintaining_Budget_WIP_Computations', 2105, null, 319);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_4_1_Overview', 2106, null, 320);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_4_2_Using_the_WIP_Computation_Window', 2107, null, 321);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_5_Versions', 2108, 'w_budget_maint', 322);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_6_Closing_Patterns', 2109, 'w_budget_closing_pattern_edit', 323);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_7_Budget_Merge', 2110, 'w_budget_copy_years', 324);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_8_Archiving_Budget_Versions', 2111, null, 325);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '23_9_O_M', 2112, null, 326);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '24_1_Budget_Amounts', 2113, 'w_budget_amounts', 327);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '24_2_Analyzing_Actuals_vs_Budgeted_Amounts', 2114, 'w_budget_actual_vs_budget', 328);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '24_3_Budget_Analysis__Graphics', 2115, null, 329);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '25_1_Introduction', 2116, null, 330);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '25_2_Creating_a_new_ranking_scenario', 2117, null, 331);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '25_3_Building_the_Ranking_Formula', 2118, null, 332);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '25_4_Running_Viewing_the_Ranking_Scenario', 2119, null, 333);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '25_5_Maintaining_Ranking_Fields', 2120, null, 334);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '25_6_Maintaining_Additional_Attributes', 2121, null, 335);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '25_7_Applying_A_Filter_to_the_Scenario', 2122, null, 336);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '26_1_Introduction', 2123, null, 337);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '26_2_Global_Reports', 2124, null, 338);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '27_1_Global_Reports__Budgeting_Reports', 2125, 'w_reporting_main', 339);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '27_2_Global_Reports__Project_Management_Reports', 2126, null, 340);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '27_3_Selection_Reports__Budgeting_Reports', 2127, null, 341);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '27_4_Selection_Reports__Project_Management_Reports', 2128, null, 342);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '28_1_Global_Reports', 2129, null, 343);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_1_Introduction', 2130, null, 344);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_Budget_Versions__Processing', 2131, null, 345);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_1_Loading_Budget_Dollars_from_Funding_Projects', 2132, null, 346);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_2_Budget_Versions__Update_with_Actuals', 2133, 'w_budget_process_control', 347);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_3_Budget_Versions__Escalation', 2134, null, 348);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_4_Budget_Versions__Overheads', 2135, 'w_run_allocation_batch_bdg', 349);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_5_Budget_Versions__AFUDC', 2136, null, 350);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_6_Budget_Versions__WIP_Computations', 2137, null, 351);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_7_Budget_Versions__Pull_Send_Data_to_from_CR', 2138, null, 352);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_8_Budget_Versions__Run_CR_Allocations', 2139, null, 353);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_10_9_Budget_Versions__Close_for_Entry', 2140, null, 354);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_11_Budget_Versions__Integration_with_Departmental_Budgeting', 2141, null, 355);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_11_1_Introduction', 2142, null, 356);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_11_2_Configuring_the_Integration', 2143, null, 357);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_11_3_Running_the_CR_to_Budget_to_CR_process', 2144, null, 358);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_12_Budget_Versions__Viewing_Associated_Funding_Projects', 2145, 'w_budget_bv_fp_grid', 359);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_13_Budget_Versions__Comparing_Different_Budget_Versions', 2146, 'w_budget_compare_fps', 360);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_14_Budget_Versions__Forecast', 2147, 'w_wo_est_monthly_custom', 361);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_15_Budget_Versions__Archiving_Budget_Versions', 2148, 'w_budget_archive', 362);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_16_Budget_Versions__Approval_Statuses', 2149, 'w_wo_approval_status', 363);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_17_Budget_Version_Dashboards', 2150, null, 364);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_17_1_Overview', 2151, null, 365);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_17_2_Setting_Up_Budget_Version_Dashboards_for_a_User', 2152, null, 366);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_2_Budget_Versions_Getting_Started', 2153, 'w_version_select_tabs', 367);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_3_Budget_Version_Selection__Entering_a_Description', 2154, null, 368);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_4_Budget_Version_Selection__Using_Other_Criteria', 2155, 'w_budget_version_control', 369);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_5_Budget_Versions_Fields', 2156, null, 370);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_6_Budget_Versions__Making_Changes', 2157, null, 371);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_7_Budget_Versions__Creating_a_New_Version', 2158, 'w_budget_version_detail', 372);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_8_Budget_Versions__Deleting_a_Version', 2159, null, 373);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_Budget_Versions__Options', 2160, null, 374);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_1_Budget_Versions__Locking_a_budget_version', 2161, null, 375);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_10_Budget_Versions__Creating_New_Funding_Project_Revisions', 2162, null, 376);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_11_Budget_Versions__Associating_Existing_Revisions', 2163, 'w_budget_detail', 377);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_2_Budget_Versions__Maintaining_Budget_Items_on_the_Version', 2164, 'w_budget_version_budgets', 378);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_3_Budget_Versions__Viewing_Funding_Projects_on_the_Version', 2165, null, 379);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_4_Budget_Versions__Plant_Classes', 2166, null, 380);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_5_Budget_Versions__Approved', 2167, null, 381);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_6_Budget_Versions__Update_with_Actuals', 2168, null, 382);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_7_Budget_Versions__Rollfoward', 2169, null, 383);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_8_Budget_Versions__Respread', 2170, 'w_bv_respread_forecast', 384);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '3_9_9_Budget_Versions__Import_Estimates', 2171, null, 385);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '4_1_Introduction', 2172, null, 386);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '4_2_Prerequisites', 2173, null, 387);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '4_3_Using_PowerPlan_to_Initiate_a_FP__Step_1', 2174, null, 388);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '4_4_Copy_Funding_Project_Option', 2175, null, 389);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '4_5_Using_PowerPlan_to_Initiate_a_FP__Step_2', 2176, null, 390);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '4_6_Funding_Project_Initiation__Additional_Information', 2177, null, 391);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_1_Overview1', 2178, null, 392);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_10_FP_Information_Overheads', 2179, null, 393);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_11_FP_Information__Authorization', 2180, null, 394);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_12_FP_Information__User_Comments', 2181, null, 395);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_13_FP_Information__Review', 2182, 'w_wo_detail_Review', 396);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_2_Using_the_Funding_Project_Information_Window', 2183, null, 397);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_3_FP_Information_Details', 2184, null, 398);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_4_FP_Information_Accounts', 2185, null, 399);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_5_FP_Information_Departments', 2186, null, 400);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_6_FP_Information_Contacts', 2187, null, 401);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_7_FP_Information_Tasks', 2188, null, 402);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_8_FP_Information_Class_Codes', 2189, null, 403);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '5_9_FP_Information_Justification', 2190, null, 404);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '6_1_Introduction', 2191, null, 405);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '6_2_FP_Selection_Getting_Started', 2192, 'w_project_select_tabs', 406);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '6_3_FP_Selection__Entering_a_Project_Number', 2193, null, 407);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '6_4_FP_Selection__Using_Other_Criteria', 2194, null, 408);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '6_5_FP_Selection__Saving_and_Re_Running_a_Query', 2195, null, 409);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '7_1_Introduction', 2196, null, 410);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '7_2_Funding_Project_Statuses', 2197, null, 411);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '7_3_Automatic_Approvals', 2198, null, 412);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '7_4_Sending_a_Funding_Project_for_Approval', 2199, null, 413);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '7_5_Funding_Project_Approval_Delegation', 2200, null, 414);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '7_6_Email_Notifications', 2201, null, 415);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '7_7_Setting_up_lists_of_Users_for_each_Approval_Level', 2202, null, 416);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '8_1_Sending_for_Review', 2203, null, 417);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '8_2_Reviewing_a_Funding_Project_Accept_or_Reject_', 2204, null, 418);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '8_3_Setup_Review_Types_and_Levels', 2205, null, 419);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_1_Introduction', 2206, null, 420);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_10_FP_Estimates__Slide_Estimates', 2207, null, 421);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_11_FP_Estimates__Comparing_Budget_Versions', 2208, null, 422);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_12_FP_Estimates__Incremental_Forecasting', 2209, 'w_budget_inc_fcst_fp', 423);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_2_Budget_Versions_and_Funding_Project_Revisions', 2210, null, 424);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_3_FP_Estimates__Getting_Started', 2211, null, 425);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_4_FP_Estimates__Estimating_Options', 2212, null, 426);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_5_FP_Estimates__Initial_Estimate', 2213, null, 427);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_6_FP_Estimates__Grid_Estimates', 2214, null, 428);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_7_FP_Estimates__Copy_Estimates', 2215, null, 429);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_8_FP_Estimates__Monthly_Estimate_Upload_Tool', 2216, null, 430);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', '9_9_FP_Estimates__Creating_a_New_Revision', 2217, null, 431);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_1_Funding_Projects_FP_', 2218, null, 432);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_10_Funding_Project_Substitutions', 2219, null, 433);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_11_Funding_Project_Accruals', 2220, null, 434);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_12_Other_Funding_Project_Functions', 2221, null, 435);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_13_Funding_Project_Ranking', 2222, null, 436);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_14_Funding_Project_Reporting', 2223, null, 437);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_15_Project_Management_Query_Tools', 2224, null, 438);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_16_Budgeting', 2225, null, 439);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_17_Budget_Items', 2226, null, 440);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_18_Budget_Item_Selection', 2227, null, 441);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_19_Budget_Item_Headers', 2228, null, 442);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_2_Funding_Project_Table_Setup', 2229, null, 443);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_20_Budget_Item_Dollars', 2230, 'w_budget_annual', 444);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_21_Budget_Item_Approval', 2231, null, 445);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_22_Other_Budget_Item_Functions', 2232, null, 446);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_23_Budget_Item_Configuration', 2233, null, 447);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_24_Budget_Analysis', 2234, null, 448);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_25_Budget_Item_Ranking', 2235, null, 449);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_26_Budget_Item_Reporting', 2236, null, 450);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_27_FP_Report_Listing', 2237, null, 451);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_28_Budgeting_Report_Listing', 2238, null, 452);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_3_Getting_Started_With_Budget_Versions', 2239, null, 453);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_4_Funding_Project_Initiation', 2240, null, 454);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_5_Funding_Project_Header_Information', 2241, null, 455);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_6_Funding_Project_Selection', 2242, null, 456);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_7_Funding_Project_Approval', 2243, null, 457);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_8_Budget_Review_Funding_Project_Review_', 2244, null, 458);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Chapter_9_Funding_Project_Estimates', 2245, null, 459);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'budget', 'Introduction1', 2246, null, 460);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_1_Introduction1', 3000, null, 461);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_2_Definitions1', 3001, null, 462);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_3_System_Navigation1', 3002, null, 463);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_4_CR_Hierarchy', 3003, null, 464);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_5_CR_Hierarchy__Schematic', 3004, null, 465);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_1_Introduction', 3005, null, 466);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_2_Cancellation_Setup', 3006, 'w_cr_cancel_process_setup', 467);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_3_Cancellation__Table_Create_and_Reconcile', 3007, 'w_cr_cancel_process', 468);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '11_1_Introduction', 3008, null, 469);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '11_2_Billing_Type_Header_Maintenance', 3009, 'w_cr_sco_type', 470);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '11_3_Billing_Type_Rates_Maintenance', 3010, 'w_cr_sco_type_rates', 471);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '11_4_Derivations', 3011, null, 472);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_1_Introduction1', 3012, null, 473);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_10_PP_Integration_Examples', 3013, null, 474);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_10_1Outbound_WO_FP_Interface', 3014, null, 475);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_10_2CR_Transaction_Interface', 3015, null, 476);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_10_3Inbound_WO_Task_Unit_Ests', 3016, null, 477);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_11_Posting_Approval_Options', 3017, 'w_cr_approval_select', 478);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_12_Posting_Approval_Actuals', 3018, 'w_cr_posting_approval', 479);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_13_Posting_Approval_Budget', 3019, 'w_cr_posting_approval_bdg', 480);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_14_Project_Approval', 3020, 'w_cr_project_approval', 481);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_15_CWIP_Charge_Kickouts', 3021, 'w_cr_cwip_kickout_review', 482);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_2_Interface_Definitions', 3022, 'w_cr_interface', 483);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_3_Security_Options', 3023, 'w_cr_security', 484);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_4_Security_Options__Company_Security', 3024, 'w_cr_security_company', 485);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_5_Security_Options__Budget_Version_Security', 3025, 'w_cr_security_budget_version', 486);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_6_Security_Options__Structure_Based_Security', 3026, 'w_cr_security_structures', 487);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_7_Allocation_Parameter_Validation', 3027, 'w_cr_code_block_validate', 488);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_8_CR_Data_Mover', 3028, 'w_cr_data_mover', 489);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '12_9_PP_Integration', 3029, 'w_pp_integration', 490);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '13_1_Introduction1', 3030, null, 491);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '13_2_Global_Reports', 3031, 'w_cr_report_types', 492);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_1_Introduction1', 3032, 'w_cr_archiving', 493);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_10_Work_Order_Archiving__Archive_within_the_Database', 3033, null, 494);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_11_Work_Order_Archiving__Un_Archive_within_the_Database', 3034, null, 495);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_12_Work_Order_Archiving__Archive_to_Disk', 3035, null, 496);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_13_Work_Order_Archiving__Un_Archive_from_Disk', 3036, null, 497);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_14_Archive_History', 3037, null, 498);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_2_Summary_Archiving__Archive_within_the_Database', 3038, null, 499);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_3_Summary_Archiving__Un_Archive_within_the_Database', 3039, null, 500);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_4_Summary_Archiving__Archive_to_Disk', 3040, null, 501);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_5_Summary_Archiving__Un_Archive_from_Disk', 3041, null, 502);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_6_Detail_Archiving__Archive_within_the_Database', 3042, null, 503);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_7_Detail_Archiving__Un_Archive_within_the_Database', 3043, null, 504);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_8_Detail_Archiving__Archive_to_Disk', 3044, null, 505);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '14_9_Detail_Archiving__Un_Archive_from_Disk', 3045, null, 506);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '15_1_CR_Setup', 3046, null, 507);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_1_Introduction1', 3047, 'w_cr_setup', 508);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_10_Table_Report', 3048, null, 509);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_11_CR__Projects__Posting_Actuals', 3049, 'w_cr_cwip_charge_control', 510);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_12_CR__Projects__Posting__Commitments_Posting_', 3050, 'w_cr_commitments_control', 511);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_13_CR__Projects__Posting__Commitments_Entry_', 3051, 'w_cr_commitments_control_from_pp', 512);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_14_Creating_the_CR_Tables', 3052, null, 513);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_15_Creating_the_CR_Indexes', 3053, 'w_cr_build_indexes', 514);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_16_Other_Tables__Queries_against_the_GL', 3054, null, 515);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_17_Other_Tables__Queries_against_any_Table_or_View', 3055, 'w_cr_any_table_query_setup', 516);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_18_Other_Tables__Drills_to_Feeder_Systems', 3056, 'w_cr_drill_criteria', 517);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_The_Accounting_Key', 3057, 'w_cr_accounting_key', 518);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_3_Sources_of_Data', 3058, 'w_cr_sources', 519);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_4_Source_Specific_Fields', 3059, 'w_cr_sources_fields', 520);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_Master_Element_Tables', 3060, null, 521);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_6_Master_Element_Tables__Defining_the_Fields', 3061, 'w_cr_elements_fields', 522);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_7_CR__Automated_Postings', 3062, 'w_cr_post_to_gl_control', 523);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_8_Derivations__Additional_Columns', 3063, 'w_cr_deriver_additional_fields', 524);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_9_Cancellations__Additional_Columns', 3064, 'w_cr_cancellation_additional_fields', 525);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_1_Introduction1', 3065, null, 526);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_10_Projects_Based_Validations', 3066, 'w_cr_validation_rules_projects', 527);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_11_Account_Range_Validations', 3067, 'w_cr_validation_acct_range', 528);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_12_Account_Range_Validations__Exclusions', 3068, 'w_cr_validation_acct_range_excl', 529);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_13_Structures', 3069, 'w_cr_structures', 530);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_14_Structures__Apply_Ranges', 3070, null, 531);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_15_Structures__Structure_Flattening', 3071, null, 532);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_16_Structures__Structure_Audits', 3072, null, 533);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_17_Structures__Structure_Points', 3073, 'w_cr_structure_values', 534);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_18_Structures__Structures_Ranges', 3074, 'w_cr_structure_values_edit_ranges', 535);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_2_Master_Element_Tables', 3075, 'w_cr_element_values_list', 536);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_3_Combination_Validations', 3076, 'w_cr_validation_combos', 537);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_4_Combination_Validations__Defining_the_Rules', 3077, 'w_cr_validation_rules', 538);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_5_Combinations_Validations__Defining_the_Control_Data', 3078, 'w_cr_validations_combos', 539);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_6_Open_Month_Validations', 3079, 'w_cr_open_month_number', 540);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_7_Reviewing_and_Correcting_Validation_Kickouts', 3080, 'w_cr_validation_kickout_review', 541);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_8_The_Validator', 3081, 'w_cr_validator', 542);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_9_Suspense_Accounting', 3082, 'w_cr_suspense_account', 543);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_1_Introduction1', 3083, null, 544);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_10_Queries__Balances', 3084, 'w_cr_select_balances', 545);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_11_Queries_Crosstab', 3085, 'w_cr_crosstab_options', 546);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_12_Queries_Crosstab__Snapshots', 3086, null, 547);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_13_Queries__Allocation_Results', 3087, 'w_cr_select_summary_alloc_query', 548);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_14_Queries__Distribution_Administration', 3088, 'w_cr_query_distrib_maint', 549);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_2_Starting_a_Query', 3089, 'w_cr_select_options', 550);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_3_Queries__CRSummary', 3090, 'w_cr_select_summary', 551);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_4_Queries__CRDetail', 3091, 'w_cr_select_detail', 552);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_5_Queries__General_Ledger', 3092, 'w_cr_select_gl', 553);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_6_Queries__Formulas', 3093, 'w_cr_select_cr_sum', 554);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_7_Queries__Actual_vs_Budget', 3094, 'w_cr_select_cr_sum_ab', 555);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_8_Queries__All_Details', 3095, 'w_cr_select_all_details', 556);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_9_Queries__Any_Table_Queries', 3096, 'w_cr_select_any_table', 557);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_1_Introduction', 3097, 'w_cr_journals_top', 558);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_10_Manual_Journal_Entries__Journal_Batches', 3098, 'w_cr_journal_batches', 559);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_11_Manual_Journal_Entries__Recurring_Journals', 3099, 'w_cr_manual_je_recurring', 560);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_12_Manual_Journal_Entries__Admin', 3100, 'w_cr_manual_je_admin', 561);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_13_Adjusting_Journal_Entries', 3101, 'w_cr_all_details_je', 562);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_2_Manual_Journal_Entries__Getting_Started', 3102, 'w_cr_manual_je', 563);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_3_Manual_Journal_Entries__Entering_the_Journal_Entry_Lines', 3103, 'w_cr_saved_templates', 564);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_4_Manual_Journal_Entries__Validations', 3104, null, 565);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_5_Manual_Journal_Entries__Viewing_and_Editing_Existing_Journals', 3105, 'w_cr_journals', 566);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_6_Manual_Journal_Entries__Defining_Approval_Types', 3106, 'w_cr_approval_group', 567);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_7_Manual_Journal_Entries__Sending_an_Entry_for_Approvals', 3107, null, 568);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_8_Manual_Journal_Entries__Approving_a_Journal_Entry', 3108, 'w_cr_journal_approve_and_post', 569);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_9_Manual_Journal_Entries__Posting_a_Journal_Entry', 3109, null, 570);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_1_Introduction1', 3110, null, 571);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_10_Source_Criteria_Syntax', 3111, 'w_cr_alloc_where_clause', 572);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_11_Source_Grouping', 3112, 'w_cr_alloc_group_by', 573);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_12_Targets', 3113, 'w_cr_alloc_target_criteria', 574);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_13_Targets__Transposing', 3114, 'w_cr_alloc_target_criteria_trpo', 575);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_14_Targets__Using_Rate_Types', 3115, 'w_cr_rates', 576);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_15_Targets__Rate_Type_Examples', 3116, null, 577);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_16_Targets__Rate_Types_with_Rates_defined_in_CR_Structures', 3117, 'w_cr_structure_values_rates', 578);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_17_Targets__Warning_and_Error_Messages', 3118, null, 579);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_18_Credits', 3119, 'w_cr_alloc_credit_criteria', 580);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_19_Balance_Criteria', 3120, null, 581);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_2_Allocations__Getting_Started', 3121, 'w_cr_alloc_maintenance', 582);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_20_Intercompany_Criteria', 3122, 'w_cr_alloc_interco_criteria', 583);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_21_Intercompany_Criteria__Special_Setup', 3123, 'w_cr_alloc_interco_criteria2_grid', 584);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_22_Running_the_Allocations', 3124, 'w_cr_run_allocation_batch', 585);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_23_Other_Allocation_Options', 3125, 'w_cr_alloc_maintenance_other', 586);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_24_Allocation_Hints', 3126, null, 587);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_3_Allocations__How_they_will_Run', 3127, null, 588);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_4_Modifying_an_Allocation', 3128, null, 589);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_5_Adding_a_New_Allocation', 3129, null, 590);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_6_Deleting_an_Allocation', 3130, null, 591);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_7_Copying_an_Allocation_to_a_Budget_Allocation_', 3131, null, 592);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_8_Defining_Parameters_Used_in_Allocations', 3132, null, 593);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_9_Source_Criteria', 3133, null, 594);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_1_Introduction1', 3134, null, 595);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_2_Derivation_Control_Retrieval', 3135, null, 596);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_3_Derivation_Control_Maintenance', 3136, 'w_cr_derivation_maint', 597);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_3_1_Derivation_Override_Tab', 3137, null, 598);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_3_2_Derivation_Override_2_Tab', 3138, null, 599);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_3_3_Derivation_Trueup', 3139, 'w_cr_derivation_trueup_setup', 600);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_3_4_Batch_Derivations', 3140, 'w_cr_bd_setup', 601);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_4_Security_Options__Derivation_Type_Security', 3141, 'w_cr_security_derivation_type', 602);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_1_Introduction', 3142, 'w_cr_control', 603);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_10_Special_Processing_Tab', 3143, null, 604);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_11_Interface_Dates_Tab', 3144, null, 605);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_12_Audit_and_Balance_Tab', 3145, null, 606);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_13_Eliminations_Tab', 3146, null, 607);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_2_Allocations_Tab', 3147, null, 608);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_3_Allocations_Tab__Running_an_Allocation_Batch', 3148, null, 609);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_4_Allocations_Tab__Deleting_Allocation_Results', 3149, null, 610);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_5_Allocations_Tab__Reversing_Allocation_Results', 3150, null, 611);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_6_Allocations_Tab__Attaching_Documents', 3151, null, 612);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_7_Allocation_Reports_Tab', 3152, null, 613);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_8_Months_Tab', 3153, null, 614);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_9_General_Ledger_Tab', 3154, null, 615);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_1_Introduction1', 3155, 'w_cr_recon_maintenance', 616);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_2_Recon_Setup', 3156, 'w_cr_recon_reconcile', 617);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_3_Recon__Table_Create_and_Reconcile', 3157, null, 618);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_4_Recon__Reports', 3158, null, 619);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_1_Introduction_and_Definitions', 3159, null, 620);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_10_CR__Cancellation', 3160, null, 621);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_11_CR_Service_Company_Billing', 3161, null, 622);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_12_CR_Other_Functions', 3162, null, 623);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_13_CR_Reporting', 3163, null, 624);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_14_CR_Archiving', 3164, null, 625);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_15_CR_Quick_Reference', 3165, null, 626);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_2_Table_Setup', 3166, null, 627);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_3_CR_Validations_and_Structures', 3167, null, 628);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_4_CR_Queries', 3168, null, 629);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_5_CR_Online_Journal_Entries', 3169, null, 630);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_6_CR_Allocations', 3170, null, 631);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_7_CR_Derivations', 3171, null, 632);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_8_CR_Administrative_Control', 3172, null, 633);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_9_CR_Reconciliations', 3173, null, 634);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_1_Introduction_to_Depreciation', 4000, null, 635);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_1_1_PowerPlan_Depreciation_Functionality', 4001, null, 636);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_1_2_Net_Asset_Reserve_for_Individual_Assets', 4002, null, 637);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_1_3_Depreciation_Groups', 4003, null, 638);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_1_4_Reserve_for_Depreciation', 4004, null, 639);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_1_5_Depreciation_Set_of_Books', 4005, null, 640);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_1_6_Initializing_the_Depreciation_System', 4006, null, 641);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_2_Navigating_the_System', 4007, null, 642);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '1_2_1_Using_the_Depreciation_Taskbar', 4008, 'w_depr_main', 643);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '10_1_Overview1', 4009, null, 644);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '10_2_Using_the_Regulatory_Entry_Maintenance_Window', 4010, null, 645);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '10_2_Using_the_Regulatory_Entry_Maintenance_Window__Ref346698368', 4011, null, 646);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '11_1_Introduction1', 4012, null, 647);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '12_1_PowerPlan_Reporting_Window__Depreciation', 4013, null, 648);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_1_Introduction2', 4014, null, 649);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_1_1_Overview', 4015, null, 650);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_1_2_Using_the_Depreciation_Group_Taskbar', 4016, null, 651);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_10_Estimated_Net_Additions_for_Estimated_Depreciation', 4017, null, 652);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_10_1_Using_the_Allocate_Net_Adds_for_Estimated_Depreciation_Window', 4018, null, 653);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_2_Searching_for_Depreciation_Groups', 4019, null, 654);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_2_1_Using_the_Depreciation_Group_Selection_Window', 4020, 'w_depr_select_tabs', 655);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_3_Viewing_the_Depreciation_Ledger_Data', 4021, null, 656);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_3_1_Using_the_Depreciation_Ledger_Selection_Window', 4022, 'w_depr_ledger_select', 657);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_3_2_Viewing_Historical_Depreciation_Rates', 4023, 'w_depr_rates_browse', 658);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_3_3_Viewing_Depreciation_Reserve_Allocation_Factors', 4024, 'w_depr_res_allo_factors', 659);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_3_4_Depreciation_Reserve_Allocation_Methodology', 4025, null, 660);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_3_5_Depreciation_Ledger_Transactions', 4026, null, 661);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_4_Inputting_Manual_Depreciation_Activity_Entries', 4027, null, 662);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_4_1_Using_the_Depreciation_Activity_Input_Window', 4028, 'w_depr_activity_input', 663);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_4_2_Creating_a_New_Depreciation_Activity', 4029, 'w_depr_new_activity', 664);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_4_3_Prior_Period_Depreciation_Activity', 4030, null, 665);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_5_Depreciation_Transaction_Sets', 4031, null, 666);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_5_1_Using_the_Depreciation_Transaction_Set_window', 4032, 'w_depr_trans_set_open', 667);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_5_2_Creating_Depreciation_Activity_Transactions_to_Post', 4033, null, 668);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_5_3_Creating_Prior_Period_Activities_for_a_Transaction_Set', 4034, null, 669);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_6_Approving_Posting_Pending_Reserve_Activity', 4035, null, 670);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_6_1_Approving_a_Pending_Reserve_Activity', 4036, 'w_depr_activity_approve', 671);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_7_Reviewing_Posted_Depreciation_Activity', 4037, null, 672);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_7_1_Using_the_Review_Posted_Activity_Window', 4038, 'w_depr_activity_posted', 673);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_Viewing_Depreciation_Group_Rate_Details', 4039, null, 674);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_1_Using_the_Depreciation_Group_Rate_Maintenance_Window', 4040, 'w_depr_group_maint_new', 675);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_2_Adding_Depreciation_Groups', 4041, null, 676);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_3_Maintaining_Depreciation_Rate_Parameters', 4042, null, 677);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_4_Maintaining_Unit_of_Production_Parameters', 4043, 'w_depr_uop_details', 678);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_5_Using_Unit_of_Production_Depreciation', 4044, null, 679);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_6_Using_Individual_Asset_CPR_Depreciation', 4045, null, 680);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_7_Using_Subledger_Depreciation', 4046, null, 681);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_8_8_Using_Automatic_Accrual_Rate_Recalculation', 4047, null, 682);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_9_Depreciation_Calculation', 4048, null, 683);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_9_1_Using_the_PowerPlan_Depreciation_Calculation_Window', 4049, 'w_depr_ledger_calc', 684);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '2_9_2_Calculation_Methodology', 4050, null, 685);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '3_1_Introduction2', 4051, null, 686);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '3_1_1_Overview', 4052, 'w_choose_depr_dollars', 687);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '3_2_Building_a_Query', 4053, 'w_fcst_depr_ledger_dollars', 688);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '3_2_1_Using_the_Depreciation_User_Defined_Query_Window', 4054, 'w_depr_ledger_dollars', 689);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '3_2_2_Saving_Your_Query', 4055, null, 690);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '4_1_Introduction2', 4056, null, 691);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '4_1_1_Overview', 4057, null, 692);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '4_2_Maintaining_Depreciation_Groups_Controls', 4058, null, 693);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '4_2_1_Using_the_Depreciation_Group_Control_Detail_Window', 4059, 'w_depr_group_cntl_detail', 694);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '4_2_2_Depreciation_Group_Control_Hierarchy_Example', 4060, null, 695);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '4_2_3_The_Methodology_of_Setting_Up_Depreciation_Group_Control', 4061, null, 696);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '5_1_Introduction1', 4062, null, 697);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '5_1_1_Overview', 4063, null, 698);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '5_2_Depreciation_Methods', 4064, null, 699);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '5_2_1_Using_the_Depr_Method_Rates_Edit_Window', 4065, 'w_depr_method_rates_edit_full', 700);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '5_2_2_Maintaining_Depreciation_Method_Blending', 4066, null, 701);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '5_2_3_Blending_Examples', 4067, null, 702);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '6_1_Introduction2', 4068, null, 703);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '6_2_Maintaining_Combined_Depreciation_Groups', 4069, null, 704);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_1_Introduction2', 4070, null, 705);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_1_1_PowerPlan_Forecast_Depreciation_Functionality', 4071, null, 706);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_10_Forecast_Depreciation_Plant_Activity', 4072, null, 707);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_10_1_Using_the_Forecast_Depreciation_Plant_Activity_Window', 4073, 'w_fcst_depr_plant_activity', 708);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_10_2_Using_the_Forecast_CPR_Depreciation_Assets_Window', 4074, 'w_fcst_cpr_assets', 709);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_11_Forecast_Depreciation_Reserve_Activity', 4075, null, 710);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_11_1_Using_the_Forecast_Depreciation_Reserve_Activity_Window', 4076, 'w_fcst_depr_reserve_activity', 711);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_12_Forecast_Depreciation_Tax_Activity', 4077, null, 712);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_12_1_Using_the_Forecast_Depreciation_Tax_Activity_Window', 4078, 'w_fcst_tax_activity', 713);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_13_Forecast_Depreciation_Reporting', 4079, null, 714);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_13_1_PowerPlan_Reporting_Window__Forecast_Depreciation', 4080, null, 715);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_14_Forecast_Depreciation_Step_by_Step', 4081, null, 716);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_14_1_Overview', 4082, null, 717);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_14_2_Creating_Forecast_Depreciation_Groups', 4083, null, 718);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_14_3_Creating_Forecast_Depreciation_Versions', 4084, null, 719);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_14_4_Inputting_Budget_Data_into_Forecast_Depreciation_Version', 4085, null, 720);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_14_5_Interfacing_Budget_Data_into_Forecast_Depreciation_Version', 4086, null, 721);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_14_6_Calculating_Forecast_Depreciation', 4087, null, 722);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_2_Navigating_Forecast_Depreciation', 4088, null, 723);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_2_1_Using_the_Forecast_Depreciation_Taskbar', 4089, 'w_depr_forecast_main', 724);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_3_Forecast_Depreciation_Groups', 4090, null, 725);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_3_1_Using_the_Forecast_Depreciation_Group_Window', 4091, null, 726);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_3_1_Using_the_Forecast_Depreciation_Group_Window_Starting_Out_and_Auditing', 4092, null, 727);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_3_1_Using_the_Forecast_Depreciation_Group_Window_Where_Used', 4093, null, 728);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_3_2_Using_the_Forecast_Depreciation_Group_Details_Window', 4094, 'w_fcst_depr_group_maint', 729);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_3_2_Using_the_Forecast_Depreciation_Group_Details_Window__Ref89365378', 4095, 'w_fcst_cdg', 730);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_3_2_Using_the_Forecast_Depreciation_Group_Details_Window__Ref89365378', 4095, 'w_fcst_depr_group', 731);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_4_Forecast_Depreciation_Versions', 4096, null, 732);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_4_1_Forecast_Depreciation_Version_Selection', 4097, 'w_fcst_depr_version_select', 733);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_4_2_Forecast_Depreciation_Version_Control', 4098, 'w_fcst_depr_version_control', 734);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_4_3_Set_of_Books_in_a_Version', 4099, null, 735);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_4_4_Forecast_Depreciation_Groups_in_a_Version', 4100, 'w_fcst_depr_version_groups', 736);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_4_5_Forecast_Depreciation_Calculation', 4101, 'w_fcst_depr_calc_version', 737);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_5_Forecast_Depreciation_Method_Rates_Edit', 4102, null, 738);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_5_1_Using_the_Forecast_Depreciation_Method_Rates_Edit_Window', 4103, 'w_fcst_depr_rates_edit', 739);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_6_Forecast_Depreciation_Budget_Link', 4104, null, 740);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_6_1_Using_the_Budget_to_Forecast_Depreciation_Link_Window', 4105, 'w_budget2fcst', 741);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_6_2_Step_1_Stage_Budget_Activity', 4106, null, 742);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_6_3_Step_2_Verify_Budget_Activity', 4107, null, 743);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_6_4_Step_3_Load_Budget_Activity', 4108, null, 744);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_7_Forecast_Depreciation_Selection', 4109, null, 745);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_7_1_Using_the_Forecast_Depreciation_Select_Taskbar', 4110, null, 746);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_8_Forecast_Depreciation_Group__Version_Selection', 4111, null, 747);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_8_1_Using_the_Forecast_Depreciation_Group_Selection_Window', 4112, 'w_fcst_depr_select_tabs', 748);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_9_Forecast_Depreciation_Group__Version_Details', 4113, null, 749);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_9_1_Using_the_Forecast_Depreciation_Group_Rate_Maintenance_Window', 4114, 'w_fcst_depr_group_version_maint', 750);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_9_1_Using_the_Forecast_Depreciation_Group_Rate_Maintenance_Window_Forecast_Unit_of', 4115, null, 751);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '7_9_2_Using_the_Forecast_Unit_of_Production_Details_Window', 4116, null, 752);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '8_1_Using_the_Retirement_History_Window', 4117, null, 753);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', '9_1_Using_the_Depreciation_Activity_History_Window', 4118, null, 754);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_1_Introduction', 4119, null, 755);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_10_Regulatory_Accounting_Entries', 4120, 'w_regulatory_entries', 756);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_11_Run_Interfaces', 4121, null, 757);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_12_Depreciation_Reporting', 4122, null, 758);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_2_Depreciation_Group_Selection', 4123, null, 759);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_3_Depreciation_Ledger_Query', 4124, null, 760);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_4_Depreciation_Group_Control_Maintenance', 4125, null, 761);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_5_Depreciation_Methods', 4126, null, 762);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_6_Combined_Depreciation_Groups', 4127, 'w_cdg', 763);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_7_Depreciation_Forecast', 4128, null, 764);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_8_Retirement_History', 4129, 'w_wo_retirement_transactions', 765);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation', 'Chapter_9_Depreciation_Activity_History', 4130, 'w_depr_activity_history2', 766);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_1_Overview2', 5000, 'w_depr_study_main', 767);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_1_1_Relation_with_the_PowerPlan_Asset_Management_Module', 5001, null, 768);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_1_2_Depreciation_Study_Data_Integration', 5002, null, 769);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_1_3_Direct_Benefits_of_the_Depreciation_Studies_Module', 5003, null, 770);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_1_4_Converting_to_and_Maintaining_under_IFRS', 5004, null, 771);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_2_Using_the_Depreciation_Study_Toolbar', 5005, null, 772);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_3_Table_Queries_PP_Any_Query_', 5006, 'w_pp_any_query_options', 773);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_3_Table_Queries_PP_Any_Query_', 5006, 'w_pp_select_any_table', 774);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_3_Table_Queries_PP_Any_Query_', 5006, 'w_pp_any_field_search', 775);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_3_Table_Queries_PP_Any_Query_', 5006, 'w_pp_any_table', 776);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_4_Run_Interfaces', 5007, 'w_interface', 777);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_5_Reports', 5008, 'w_reporting_main', 778);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '1_5_Reports', 5008, 'w_reporting_main_detail', 779);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_1_Overview1', 5009, 'w_ds_database_main', 780);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_2_Using_the_Depreciation_Study_Database_Taskbar', 5010, null, 781);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_3_Loading_Database_Transactions_from_the_PowerPlan_CPR', 5011, 'w_ds_cpr_act_interface', 782);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_4_The_Database_Transactions_Grid', 5012, 'w_transaction_detail', 783);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_5_Data_Account_Control', 5013, 'w_compare_tree', 784);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_5_Data_Account_Control', 5013, 'w_print_tree', 785);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_5_1_Plant_Data_Account_Control_Schema_Example', 5014, null, 786);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_5_2_The_Methodology_of_Setting_up_Plant_Data_Account_Control', 5015, null, 787);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_6_Using_the_Depreciation_Studies_Setup_Window', 5016, 'w_ds_setup', 788);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_6_1_Using_the_Depreciation_Studies_Setup_Details_Window', 5017, 'w_ds_setup_load', 789);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_6_2_Customizing_the_Depreciation_Study_Setup', 5018, null, 790);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_7_Using_the_Database_Import_Window', 5019, 'w_ds_db_import', 791);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_7_1_Using_the_Historical_Data_Mapping_Window', 5020, 'w_ds_db_import_map', 792);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_7_2_Using_the_Activity_Summary_Window', 5021, 'w_ds_db_import_act_compare', 793);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '2_8_Plant_Data_Accounts', 5022, 'w_ds_account_maint', 794);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '3_1_Overview1', 5023, 'w_ds_dataset_main', 795);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '3_2_Analysis_DataSet_Selection_Window', 5024, 'w_ds_dataset_select', 796);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '3_3_Using_the_Depreciation_Study_DataSet_Taskbar', 5025, null, 797);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '3_4_DataSet_Transaction_Management', 5026, 'w_dataset_load', 798);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '3_5_DataSet_Transaction_Grid', 5027, null, 799);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '3_6_DataSet_Layout_Window', 5028, 'w_analysis_dataset_detail', 800);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '3_7_DataSet_Transaction_Import', 5029, 'w_ds_external_trans', 801);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_1_Overview1', 5030, 'w_ds_scenario_main', 802);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_10_Depreciation_Accrual_Rate_Calculation', 5031, null, 803);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_10_1_Overview', 5032, null, 804);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_10_2_Using_the_Depreciation_Accrual_Rate_Calculation_Window', 5033, 'w_accrual_calc_all', 805);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_11_Reserve_Analysis', 5034, null, 806);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_11_1_Overview', 5035, null, 807);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_11_2_Using_the_Reserve_Analysis_Window', 5036, 'w_reserve_analysis', 808);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_12_Salvage_Analysis', 5037, 'w_salvage_analysis', 809);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_12_1_Overview', 5038, null, 810);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_12_2_Using_the_Salvage_Analysis_Window', 5039, null, 811);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_13_Depreciated_Valuation_Analysis', 5040, null, 812);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_13_1_Overview', 5041, null, 813);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_13_2_Using_the_Depreciated_Valuation_Window', 5042, 'w_valuation', 814);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_14_Forecast_Retirements_Analysis', 5043, null, 815);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_14_1_Overview', 5044, null, 816);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_14_2_Using_the_Forecast_Retirements_Analysis_Window', 5045, 'w_forecast_retirements', 817);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_2_Using_the_Scenario_Selection_Window', 5046, 'w_ds_scenario_select', 818);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_3_Using_the_Depreciation_Study_Scenario_Taskbar', 5047, null, 819);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_4_Using_the_Analysis_Scenario_Detail_Window', 5048, 'w_scenario_analysis', 820);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_5_Using_the_Analysis_Scenario_Transaction_Management_Window', 5049, 'w_scenario_trans_manage', 821);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_6_Using_the_Data_Audit_Window', 5050, 'w_ds_matrix', 822);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_6_1_The_Activity_Grid', 5051, null, 823);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_6_2_The_Activity_Matrix', 5052, null, 824);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_7_Using_the_Life_Analysis_Window', 5053, 'w_analysis_account', 825);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_8_Actuarial_Life_Analysis', 5054, 'w_life_analysis', 826);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_8_1_Overview', 5055, null, 827);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_8_2_Using_the_Actuarial_Life_Analysis_Window', 5056, null, 828);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_8_3_Using_the_Curve_Fit_Window', 5057, 'w_fit_stats_display', 829);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_8_4_Using_the_Graph_Control_Window', 5058, 'w_ds_gc', 830);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_8_5_Using_the_Observed_Life_Table', 5059, null, 831);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_9_Simulated_Plant_Record_Semi_Actuarial_Life_Analysis', 5060, null, 832);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_9_1_Overview', 5061, null, 833);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_9_2_Using_the_Simulated_Plant_Records_SPR_Analysis', 5062, 'w_simulated_plant_record', 834);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', '4_9_3_Using_the_Compute_Age_Distribution_Window', 5063, 'w_compute_age_distribution', 835);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', 'Chapter_1_Introduction_to_Depreciation_Studies', 5064, null, 836);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', 'Chapter_2_The_Depreciation_Study_Database', 5065, null, 837);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', 'Chapter_3_Depreciation_Study_DataSets', 5066, null, 838);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', 'Chapter_4_Depreciation_Study_Scenarios', 5067, null, 839);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', 'Chapter_5_Analysis_Transaction_Codes', 5068, null, 840);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'depreciation study', 'Chapter_6_Transaction_Code_Mapping__CPR_Activity_to_Depreciation_Study_Database', 5069, null, 841);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '1_1_Overview3', 6000, null, 842);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '1_2_Typical_Interfaces_to_PowerPlan_Lease', 6001, null, 843);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '1_3_Leased_Assets_Glossary', 6002, null, 844);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '1_4_Hierarchy', 6003, null, 845);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '1_5_Typical_Steps_for_Setting_up_Lease_Lessor_Accounts', 6004, null, 846);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '10_1_Adding_ILR_Approvers', 6005, null, 847);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '10_2_Navigation_to_ILR_and_Schedule_Approvals_Workspace', 6006, null, 848);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '10_3_Approve_ILR', 6007, 'w_ls_center_main/approval_assets', 849);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '10_4_Approve_All_ILRs', 6008, null, 850);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '10_5_Reject_ILR', 6009, null, 851);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '10_6_ILR_Details', 6010, null, 852);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_1_Life_Cycle_of_a_Leased_Asset', 6011, null, 853);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_Viewing_Finding_Leased_Assets', 6012, 'w_ls_center_main/search_asset', 854);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_1_Navigation_to_Leased_Asset_Workspace', 6013, null, 855);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_2_Search_by_Company', 6014, null, 856);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_3_Search_by_Lease', 6015, null, 857);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_4_Search_by_ILR', 6016, null, 858);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_5_Search_by_Leased_Asset', 6017, null, 859);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_6_Search_by_Lease_Group', 6018, null, 860);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_2_7_Viewing_Leased_Asset_Details', 6019, 'w_ls_center_main/details_asset', 861);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_3_Initiate_a_New_Leased_Asset', 6020, 'w_ls_center_main/initiate_asset', 862);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_4_Copying_Cloning_Assets', 6021, null, 863);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_5_Actions_from_Leased_Asset_Workspace', 6022, null, 864);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_5_1_Drill_to_ILR', 6023, null, 865);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_5_2_Drill_to_MLA', 6024, null, 866);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_5_3_Drill_to_CPR', 6025, null, 867);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_5_4_Retire_Asset', 6026, null, 868);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_5_5_Transfer_Asset', 6027, null, 869);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '11_6_Contingent_Payment_Adjustments', 6028, null, 870);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '12_1_Navigation_to_Payment_Reconciliation_Workspace', 6029, null, 871);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '12_2_Filtering_Payments_Invoices', 6030, null, 872);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '12_3_Matching_Payments_Invoices', 6031, 'w_ls_center_main/control_payments', 873);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '12_4_Adjusting_Payment_Values_Outside_of_Tolerance', 6032, null, 874);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '12_5_Sending_Matched_Reconciled_Payments_for_Approval', 6033, null, 875);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '12_6_Unmatching_Rejected_Payments_and_Invoices', 6034, null, 876);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '13_1_Navigation_to_Payment_Reconciliation_Workspace', 6035, null, 877);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '13_2_Viewing_Editing_Detailed_Invoices', 6036, 'w_ls_center_main/control_invoices', 878);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '13_3_Filtering_Detailed_Invoices', 6037, null, 879);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_1_Adding_Payment_Approvers', 6038, null, 880);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_2_Navigation_to_Payment_Approval_Workspace', 6039, null, 881);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_3_Approve_Payments', 6040, 'w_ls_center_main/approval_payments', 882);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_4_Approve_All_Payments', 6041, null, 883);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_5_Reject_Payments', 6042, null, 884);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_6_Actions_from_Payment_Approvals', 6043, null, 885);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_6_1_Drill_to_MLA', 6044, null, 886);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_6_2_Drill_to_ILR', 6045, null, 887);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '14_6_3_Drill_to_Assets', 6046, null, 888);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '15_1_Navigation_to_Pending_Transaction_Administration_Workspace', 6047, null, 889);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '15_2_Leased_Asset_Retirement_Approvals', 6048, null, 890);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '15_3_Leased_Asset_Transfer_Approvals', 6049, null, 891);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '16_1_Navigation_to_Residual_Processing_Workspace', 6050, null, 892);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '16_2_Booking_Residual_Value_Amounts', 6051, 'w_ls_center_main/control_residual', 893);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '17_1_Navigation_to_Lessee_Month_End_Close_Workspace', 6052, null, 894);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '17_2_Filtering_on_Month_End', 6053, null, 895);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '17_3_Month_End_Processes', 6054, 'w_ls_center_main/control_monthend', 896);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '17_3_Month_End_Processes__Running_Month_End', 6055, null, 897);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '18_1_Searching_for_a_Particular_Value', 6056, 'w_ls_center_main/lessee_search', 898);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '19_1_Navigation_to_Forecast_Workspace', 6057, null, 899);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '19_2_Creating_a_Forecast', 6058, 'w_ls_center_main/forecast_tool', 900);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '19_3_Viewing_Filtering_the_Forecast', 6059, null, 901);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '19_4_Saving_Forecast_as_New_Revision', 6060, null, 902);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '2_1_Accessing_the_Lessee_Accounting_Homepage', 6061, null, 903);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '2_2_Left_side_Navigation', 6062, null, 904);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '2_3_Navigation_to_Open_Workspaces', 6063, null, 905);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '2_4_Collapsible_Fields', 6064, null, 906);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '2_5_Wizards', 6065, null, 907);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '2_6_Standard_Buttons_Actions', 6066, null, 908);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '20_1_Navigation_to_Lessee_Reports_Workspace', 6067, null, 909);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '20_2_Running_Reports', 6068, 'w_ls_center_main/menu_wksp_reports', 910);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '20_3_Saving_Printing_Emailing_Exporting_Reports', 6069, null, 911);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '21_1_Navigation_to_Lessee_Queries_Workspace', 6070, 'w_ls_center_main/reporting_query', 912);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '21_2_Query_Future_Minimum_Lease_Payments', 6071, null, 913);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '21_3_Query_ILR_by_Set_of_Books_ALL_Revisions_', 6072, null, 914);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '21_4_Query_ILR_by_Set_of_Books_CURRENT_Revision_', 6073, null, 915);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_1_Navigation_to_Import_Workspace', 6074, null, 916);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_2_Import_a_New_File', 6075, 'w_ls_center_main/menu_wksp_import', 917);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_3_Editing_an_Import_File', 6076, null, 918);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_4_Reprocess_an_Existing_Import', 6077, null, 919);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_5_Create_Spreadsheet_Template', 6078, null, 920);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_6_Import_Template_Maintenance', 6079, null, 921);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_7_Mass_Import_Tool_Build_Schedules_for_Imported_ILRs_', 6080, null, 922);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_7_1_Navigation_to_Mass_Import_Tool', 6081, null, 923);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_7_2_Build_Schedules_for_Imported_ILRs', 6082, null, 924);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '22_7_3_Attach_Assets_to_Imported_ILRs', 6083, null, 925);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '23_1_Navigation_to_Table_Maintenance_Workspace', 6084, null, 926);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '23_2_Selecting_a_Table', 6085, null, 927);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '23_3_Performing_Edits_Maintenance', 6086, 'w_ls_center_main/admin_table_maint', 928);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '3_1_Navigation_to_Lease_Groups_Workspace', 6087, null, 929);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '3_2_Viewing_Lease_Group_Detail', 6088, 'w_ls_center_main/admin_lease_groups', 930);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '3_3_Add_New_Lease_Group', 6089, null, 931);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '3_4_Copying_Cloning_Lease_Groups', 6090, null, 932);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_1_Navigation_to_Lessors_Workspace', 6091, null, 933);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_2_Viewing_Lessor_Detail', 6092, 'w_ls_center_main/uo_ls_admincntr_lessor_wksp', 934);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_3_Adding_a_New_Lessor', 6093, null, 935);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_4_Adding_Vendors', 6094, null, 936);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_5_Copying_Cloning_Lessors', 6095, null, 937);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_6_Actions_from_Lessor_Details', 6096, null, 938);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_6_1_Delete_Lessor', 6097, null, 939);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_6_2_Initiate_MLA', 6098, null, 940);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '4_6_3_Drill_to_MLA', 6099, null, 941);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '5_1_Navigation_to_Taxes_Workspace', 6100, null, 942);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '5_2_Viewing_Tax_Details', 6101, 'w_ls_center_main/admin_taxes', 943);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '5_3_Add_New_Taxes', 6102, null, 944);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '5_4_Copying_Cloning_Taxes', 6103, null, 945);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_1_Life_Cycle_of_a_Lease_MLA_', 6104, null, 946);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_Viewing_Finding_MLAs', 6105, 'w_ls_center_main/search_mla', 947);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_1_Navigation_to_Workspace', 6106, null, 948);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_2_Search_by_Company', 6107, null, 949);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_3_Search_by_Lease_MLA_', 6108, null, 950);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_4_Search_by_Lessor', 6109, null, 951);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_5_Search_by_Lease_Group', 6110, null, 952);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_6_Search_by_Lease_Type', 6111, null, 953);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_2_7_Viewing_MLA_Details', 6112, 'w_ls_center_main/details_mla', 954);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_3_Initiate_a_New_MLA', 6113, 'w_ls_center_main/initiate_mla', 955);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_4_Copying_Cloning_MLAs', 6114, null, 956);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_5_Actions_from_MLA_Workspace', 6115, null, 957);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_5_1_Approval_Route', 6116, null, 958);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_5_2_Drill_to_ILR', 6117, null, 959);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_5_3_Drill_to_Assets', 6118, null, 960);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_5_4_Drill_to_CPR', 6119, null, 961);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_5_5_Initiate_ILR', 6120, null, 962);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '6_5_6_New_Revision', 6121, null, 963);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '7_1_Adding_MLA_Approvers', 6122, null, 964);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '7_2_Navigation_to_MLA_Approvals_Workspace', 6123, null, 965);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '7_3_Approve_MLA', 6124, 'w_ls_center_main/approval_mla', 966);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '7_4_Approve_All_MLAs', 6125, null, 967);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '7_5_Reject_MLAs', 6126, null, 968);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '7_6_View_MLA_Details', 6127, null, 969);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '8_1_Navigation_to_ILR_Groups_Workspace', 6128, null, 970);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '8_2_Viewing_ILR_Group_Detail', 6129, 'w_ls_center_main/admin_ilr_groups', 971);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '8_3_Add_New_ILR_Group', 6130, null, 972);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '8_4_Copying_Cloning_ILR_Groups', 6131, null, 973);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_1_Life_Cycle_of_an_ILR', 6132, null, 974);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_Viewing_Finding_ILRs', 6133, 'w_ls_center_main/search_ilr', 975);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_1_Navigation_to_Search_ILRs_Workspace', 6134, null, 976);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_2_Search_by_Company', 6135, null, 977);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_3_Search_by_MLA_Lessor', 6136, null, 978);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_4_Search_by_ILR', 6137, null, 979);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_5_Search_by_Lease_Group', 6138, null, 980);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_6_Search_by_Lease_Type', 6139, null, 981);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_2_7_Viewing_ILR_Details', 6140, 'w_ls_center_main/details_ilr', 982);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_3_Initiate_a_New_ILR', 6141, 'w_ls_center_main/initiate_ilr', 983);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_4_Copying_Cloning_ILRs', 6142, null, 984);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_5_Actions_from_ILR_Workspace', 6143, null, 985);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_5_1_Build_Schedule', 6144, null, 986);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_5_2_Approval_Route', 6145, null, 987);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_5_3_Drill_to_MLA', 6146, null, 988);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_5_4_Drill_to_Assets', 6147, null, 989);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_5_5_Drill_to_CPR', 6148, null, 990);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_5_6_New_Revision', 6149, null, 991);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', '9_6_Contingent_Payment_Adjustments', 6150, null, 992);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_1_Introduction1', 6151, null, 993);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_10_ILR_Asset_Schedule_Approvals', 6152, null, 994);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_11_Leased_Assets', 6153, null, 995);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_12_Payments_Invoice_Reconciliation', 6154, null, 996);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_13_Detailed_Invoices', 6155, null, 997);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_14_Payment_Approvals', 6156, null, 998);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_15_CPR_Retirement_Transfer_Approvals_Leased_Assets_', 6157, null, 999);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_16_Residual_Processing', 6158, null, 1000);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_17_Month_End_Processing', 6159, null, 1001);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_18_Global_Search', 6160, null, 1002);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_19_Comparative_Forecast_Tool', 6161, null, 1003);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_2_Navigating_the_GUI', 6162, null, 1004);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_20_Reports', 6163, null, 1005);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_21_Querying', 6164, null, 1006);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_22_Imports', 6165, null, 1007);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_23_Table_Maintenance', 6166, null, 1008);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_3_Lease_Groups', 6167, null, 1009);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_4_Lessors_Vendors', 6168, null, 1010);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_5_Taxes', 6169, null, 1011);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_6_MLAs_Master_Lease_Agreements_', 6170, null, 1012);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_7_MLA_Approvals', 6171, null, 1013);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_8_ILR_Groups', 6172, null, 1014);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'leased asset', 'Chapter_9_ILRs', 6173, null, 1015);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_1_Introduction2', 7000, null, 1016);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_2_Definitions2', 7001, null, 1017);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '1_3_System_Navigation2', 7002, null, 1018);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_1_Introduction1', 7003, null, 1019);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_10_Logs_Tab', 7004, null, 1020);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_11_Factor_Budget', 7005, null, 1021);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_2_Allocations_Tab', 7006, 'w_cr_control_bdg', 1022);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_3_Allocations_Tab__Running_an_Allocation_Batch', 7007, null, 1023);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_4_Allocations_Tab__Deleting_Allocation_Results', 7008, null, 1024);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_5_Allocations_Tab__Reversing_Allocation_Results', 7009, null, 1025);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_6_Allocation_Reports_Tab', 7010, null, 1026);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_7_General_Ledger_Tab', 7011, null, 1027);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_8_Special_Processing_Tab', 7012, null, 1028);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '10_9_Interface_Dates_Tab', 7013, null, 1029);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_1_Budget_Account_Key', 7014, null, 1030);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_Budget_Templates', 7015, 'w_cr_budget_templates', 1031);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_1_Define_Templates', 7016, null, 1032);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_10_Value_Type', 7017, null, 1033);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_11_Approval_Type', 7018, null, 1034);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_12_Structure', 7019, null, 1035);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_2_Template_Fields', 7020, null, 1036);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_3_Template_Details', 7021, null, 1037);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_4_Subtotal_Fields', 7022, null, 1038);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_5_Computed_Fields', 7023, null, 1039);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_6_Allocations', 7024, null, 1040);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_7_Budget_By', 7025, null, 1041);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_8_Deriver_Type', 7026, null, 1042);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_2_9_Calc_All', 7027, null, 1043);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_3_Budget_Template_Groups', 7028, 'w_cr_budget_groups_maint', 1044);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_3_1_Budget_Groups', 7029, null, 1045);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_3_2_Users_and_Budget_Groups', 7030, null, 1046);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_3_3_Templates_and_Budget_Groups', 7031, null, 1047);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_4_Budget_User_Values', 7032, 'w_cr_budget_users_valid_values', 1048);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_4_1_Setup_Value_Types', 7033, null, 1049);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_4_2_Assign_Values', 7034, null, 1050);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_4_3_Activate_Inactivate_Values', 7035, null, 1051);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_Labor_Setup', 7036, 'w_cr_budget_labor_config', 1052);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_1_Labor_Screen', 7037, null, 1053);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_2_Labor_Screen_Monthly_Option', 7038, null, 1054);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_3_Labor_Type_to_Estimate_Charge_Type', 7039, null, 1055);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_4_Capital_to_O_M', 7040, null, 1056);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_5_Resources', 7041, null, 1057);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_6_Resource_Security', 7042, 'w_cr_budget_groups_maint_hr', 1058);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_7_Positions', 7043, null, 1059);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '2_5_8_Rates', 7044, null, 1060);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_1_Introduction3', 7045, null, 1061);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_2_Budget_Version_Copy_Types', 7046, 'w_cr_budget_version_copy', 1062);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '3_3_Budget_Version_Summarize', 7047, null, 1063);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_1_Introduction3', 7048, 'w_cr_budget_entry_alt', 1064);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_2_Budget_Entry_Criteria', 7049, null, 1065);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_3_Budget_Data_Entry', 7050, null, 1066);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_3_1_Other_Tab', 7051, null, 1067);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_3_2_Labor_Screen_Monthly_Option', 7052, 'w_cr_budget_entry_alt_months', 1068);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_3_3_Rows_Tab', 7053, null, 1069);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_3_4_Calc_Tab', 7054, null, 1070);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '4_3_5_Appr_Tab', 7055, null, 1071);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_1_Introduction2', 7056, null, 1072);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '5_2_Defining_Approval_Types', 7057, 'w_cr_approval_group_bdg', 1073);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_1_Introduction3', 7058, null, 1074);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_2_Allocation_Override', 7059, 'w_cr_budget_ent_rate_ovr', 1075);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_3_Rate_Types', 7060, null, 1076);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_4_Computed_Column_Criteria', 7061, null, 1077);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_5_Budget_Spread_Factors', 7062, null, 1078);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_5_1_Budget_Spread_Factors', 7063, 'w_cr_budget_spread_factor', 1079);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_5_2_Budget_Fixed_Factors', 7064, 'w_cr_budget_fixed_factor', 1080);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_6_Security_Options__Budget_Version_Security', 7065, null, 1081);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_7_Budget_Input', 7066, 'w_cr_budget_input', 1082);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_8_Budget_Submitted', 7067, 'w_cr_budget_review_submitted', 1083);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_9_Budget_Escalations', 7068, 'w_cr_budget_escalations', 1084);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_9_1_Header_Information_', 7069, null, 1085);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_9_2_Source_Criteria', 7070, null, 1086);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_9_3_Rates', 7071, null, 1087);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_9_4_Budget_Versions_and_Priority', 7072, null, 1088);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '6_9_5_Run_Options', 7073, null, 1089);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_1_Introduction3', 7074, null, 1090);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_2_Master_Element_Validations', 7075, null, 1091);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_3_Combination_Validations', 7076, 'w_cr_validations_combos_bdg', 1092);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_4_Combination_Validations__Defining_the_Rules', 7077, 'w_cr_validation_rules_bdg', 1093);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '7_5_Combination_Validations__Defining_the_Control_Data', 7078, 'w_cr_validation_control_bdg', 1094);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_1_Introduction1', 7079, null, 1095);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_2_Starting_a_Query', 7080, null, 1096);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '8_3_Entry_Screen_Queries', 7081, null, 1097);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_1_Introduction2', 7082, null, 1098);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_10_Source_Grouping', 7083, null, 1099);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_11_Targets', 7084, 'w_cr_alloc_target_criteria_bdg', 1100);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_12_Targets_Transposing', 7085, null, 1101);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_13_Targets__Using_Rate_Types', 7086, null, 1102);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_14_Targets__Rate_Type_Examples', 7087, null, 1103);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_15_Targets__Rate_Types_with_Rates_defined_in_CR_Structures', 7088, null, 1104);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_16_Targets__Warning_and_Error_Messages', 7089, null, 1105);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_17_Credits', 7090, 'w_cr_alloc_credit_criteria_bdg', 1106);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_18_Balance_Criteria', 7091, null, 1107);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_19_Intercompany_Criteria', 7092, null, 1108);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_2_Allocations__Getting_Started', 7093, 'w_cr_alloc_maintenance_bdg', 1109);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_20_Intercompany_Criteria__Special_Setup', 7094, null, 1110);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_21_Running_the_Allocations', 7095, null, 1111);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_22_Other_Allocation_Options', 7096, null, 1112);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_3_Allocations__How_they_will_Run', 7097, null, 1113);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_4_Modifying_an_Allocation', 7098, null, 1114);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_5_Adding_a_new_Allocation', 7099, null, 1115);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_6_Deleting_an_Allocation', 7100, null, 1116);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_7_Defining_Parameters_Used_in_Allocations', 7101, null, 1117);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_8_Source_Criteria', 7102, null, 1118);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', '9_9_Source_Criteria_Syntax', 7103, null, 1119);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_1_Introduction_and_Definitions1', 7104, null, 1120);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_10_CR_Administrative_Control', 7105, null, 1121);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_2_CR_Budget_Setup', 7106, null, 1122);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_3_CR_Budget_Version_Control', 7107, null, 1123);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_4_CR_Budget_Entry', 7108, null, 1124);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_5_CR_Approve_and_Post_Budgets', 7109, null, 1125);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_6_CR_Budget_Miscellaneous', 7110, null, 1126);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_7_CR_Budget_Validations', 7111, null, 1127);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_8_CR_Budget_Queries', 7112, null, 1128);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'cost repository', 'Chapter_9_CR_Budget_Allocations', 7113, null, 1129);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '1_1_Introduction3', 8000, null, 1130);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '1_2_Book_Integration', 8001, null, 1131);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '1_2_1_Continuing_Property_Record_CPR_Plant_Ledger', 8002, null, 1132);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '1_2_2_Locations', 8003, null, 1133);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '1_2_3_Maintenance_of_a_Property_Tax_Ledger', 8004, null, 1134);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '1_3_Property_Tax_Processing', 8005, null, 1135);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_1_Introduction_Navigation', 8006, null, 1136);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_10_Authority_District_Maintenance', 8007, 'w_ptc_center_main/admin_auth_dist', 1137);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_11_Report_Fields_Maintenance', 8008, 'w_ptc_center_main/admin_rpt_fields', 1138);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_12_Run_Interfaces', 8009, 'w_ptc_center_main/admin_interfaces', 1139);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_12_1Company_Maintenance_Interface', 8010, null, 1140);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_13_Import_Tool', 8011, 'w_ptc_center_main/admin_import', 1141);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_13_1Property_Tax_Special_Imports', 8012, null, 1142);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_14_PT_Company_Setup', 8013, 'w_ptc_center_main/admin_ptco_setup', 1143);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_15_Tax_Year_Setup', 8014, 'w_ptc_center_main/admin_tax_year_setup', 1144);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_16_Case_Setup', 8015, 'w_ptc_center_main/admin_case_setup', 1145);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_17_Process_Steps_Maintenance', 8016, 'w_ptc_center_main/admin_processes', 1146);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_18_Approvers_Maintenance', 8017, 'w_ptc_center_main/admin_approvals', 1147);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_19_System_Options', 8018, 'w_ptc_center_main/admin_systemoptions', 1148);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_2_Table_Maintenance', 8019, 'w_ptc_center_main/admin_table_maint', 1149);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_20_User_Preferences', 8020, null, 1150);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_3_Tax_Type_Maintenance', 8021, 'w_ptc_center_main/admin_tax_types', 1151);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_4_Location_Maintenance', 8022, 'w_ptc_center_main/admin_locations', 1152);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_5_Allocation_Statistics_Maintenance', 8023, 'w_ptc_center_main/admin_allocations', 1153);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_6_Reserve_Factor_Maintenance', 8024, 'w_ptc_center_main/admin_rsv_factors', 1154);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_7_Unit_Cost_Amounts_Maintenance', 8025, 'w_ptc_center_main/admin_unit_cost', 1155);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_8_Escalation_Index_Maintenance', 8026, 'w_ptc_center_main/admin_escalation_indices', 1156);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '10_9_Tax_Type_Rollup_Maintenance', 8027, 'w_ptc_center_main/admin_rollups', 1157);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '11_1_Introduction2', 8028, null, 1158);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '11_2_Navigation', 8029, null, 1159);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '11_3_Reports_Workspace', 8030, 'w_ptc_center_main/reporting_report', 1160);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '11_4_Returns_and_Electronic_Filing_Workspaces', 8031, 'w_ptc_center_main/reporting_electronic', 1161);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '11_5_Queries', 8032, 'w_ptc_center_main/reporting_query', 1162);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_1_Tax_Year_Assessment_Year', 8033, null, 1163);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_10_Payments', 8034, null, 1164);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_11_Tips_and_Tricks', 8035, null, 1165);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_2_Statement_Year', 8036, null, 1166);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_3_Case_Setup', 8037, null, 1167);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_4_Copying_Data_Forward', 8038, null, 1168);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_5_Bills_Center_Searching_for_Bills', 8039, null, 1169);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_6_Entering_New_Bills', 8040, null, 1170);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_7_Entering_Assessments', 8041, null, 1171);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_8_Entering_Levy_Rates', 8042, null, 1172);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '12_9_Process_and_Voucher_Bills', 8043, null, 1173);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '13_1_System_Options', 8044, null, 1174);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '14_1_Net_Tax_Overview', 8045, null, 1175);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '14_2_Net_Tax_Sample_Calculation', 8046, null, 1176);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '14_3_Net_Tax_Execution', 8047, null, 1177);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '15_1_Standard_Reports', 8048, null, 1178);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '16_1_Standard_Return_Reports', 8049, 'w_ptc_center_main/reporting_rendition', 1179);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '17_1_Annual_Processing__Generate_Returned_Assets', 8050, null, 1180);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '17_2_Annual_Processing_Statement_Year', 8051, null, 1181);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '17_3_Annual_Processing_Case_Setup', 8052, null, 1182);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '17_4_Annual_Processing_Copying_Data_Forward', 8053, null, 1183);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '17_5_Monthly_Accrual_Processing', 8054, null, 1184);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '2_1_Entering_Property_Tax', 8055, null, 1185);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '2_2_Property_Tax_Home', 8056, null, 1186);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_1_Introduction4', 8057, null, 1187);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_2_Navigation', 8058, null, 1188);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_3_Search_Ledger', 8059, 'w_ptc_center_main/ledger_postallo', 1189);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_3_1_Using_the_Ledger_Select_Tabs', 8060, null, 1190);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_3_2_Using_the_Ledger_Search_Results_Grid', 8061, null, 1191);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_3_3_Using_the_Ledger_Details_Window', 8062, null, 1192);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_3_4_Using_the_Ledger_Add_Like_Window', 8063, null, 1193);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_3_5_Using_the_Ledger_Multi_Adjust_Window', 8064, null, 1194);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_3_6_Using_the_Ledger_Multi_Transfer_Window', 8065, null, 1195);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_4_Search_PreAllo_Ledger', 8066, 'w_ptc_center_main/ledger_preallo', 1196);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_4_1_Using_the_PreAllo_Ledger_Select_Tabs', 8067, null, 1197);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_4_2_Using_the_PreAllo_Search_Results_Grid', 8068, null, 1198);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_4_3_Using_the_PreAllo_Ledger_Details_Window', 8069, null, 1199);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_4_4_Using_the_PreAllo_Ledger_Add_Like_Window', 8070, null, 1200);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_4_5_Using_the_PreAllo_Ledger_Multi_Adjust_Window', 8071, null, 1201);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_4_6_Using_the_PreAllo_Ledger_Multi_Transfer_Window', 8072, null, 1202);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_5_Copy_Activity', 8073, 'w_ptc_center_main/ledger_copy', 1203);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_5_1_Locating_Records_to_Copy', 8074, null, 1204);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_5_2_Copying_Ledger_Records', 8075, null, 1205);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_6_Recalculate', 8076, 'w_ptc_center_main/ledger_recalc', 1206);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_6_1_Introduction', 8077, null, 1207);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '3_7_User_Preferences', 8078, null, 1208);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_1_Introduction4', 8079, null, 1209);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_2_Navigation', 8080, null, 1210);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_3_Search_Parcels', 8081, 'w_ptc_center_main/parcel_search', 1211);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_3_1_Using_the_Parcel_Select_Tabs', 8082, null, 1212);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_3_2_Using_the_Parcel_Search_Results_Grid', 8083, null, 1213);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_3_3_Using_the_Parcel_Details_Window', 8084, null, 1214);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_3_4_Using_the_Parcel_Add_Like_Window', 8085, null, 1215);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_3_5_Using_the_Parcel_Assessments_Search_Results_Grid', 8086, null, 1216);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_4_Copying_Parcel_Assessments', 8087, null, 1217);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_5_Assessment_Group_Setup', 8088, 'w_ptc_center_main/parcel_assessment_groups', 1218);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_6_Parcel_Geography_Factors', 8089, 'w_ptc_center_main/parcel_geography_factors', 1219);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_7_Parcel_Type_Maintenance', 8090, 'w_ptc_center_main/parcel_types', 1220);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '4_8_User_Preferences', 8091, null, 1221);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_1_Introduction3', 8092, 'w_ptc_center_main/returns_home', 1222);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_10_Import_Parcel_Updates', 8093, null, 1223);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_11_Location_Audit', 8094, null, 1224);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_12_CPR_Extraction', 8095, null, 1225);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_13_CWIP_Extraction', 8096, null, 1226);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_14_CPR_Pre_allocation_Balance_Audit', 8097, null, 1227);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_15_CWIP_Pre_allocation_Balance_Audit', 8098, null, 1228);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_16_Import_Pre_Allo_Items', 8099, null, 1229);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_17_Import_Pre_Allo_Adjustments', 8100, null, 1230);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_18_Input_Pre_Allo_Activity', 8101, null, 1231);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_19_Copy_Pre_Allo_Activity', 8102, null, 1232);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_2_Tax_Year_Setup', 8103, null, 1233);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_20_Import_Full_Statistics', 8104, null, 1234);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_21_Import_Incremental_Statistics', 8105, null, 1235);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_22_Copy_Allocation_Statistics', 8106, null, 1236);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_23_Statistics_Creator', 8107, null, 1237);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_24_Edit_Allocation_Statistics', 8108, null, 1238);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_25_Net_Tax_Audit', 8109, null, 1239);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_26_Net_Tax_Setup', 8110, null, 1240);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_27_Allocation_Statistics_Audit', 8111, null, 1241);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_28_National_Type_Audit', 8112, null, 1242);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_29_Import_Escalation_Factors', 8113, null, 1243);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_3_PT_Location_County_Audit', 8114, null, 1244);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_30_Escalated_Value_Audit', 8115, null, 1245);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_31_Reserve_Factor_Audit', 8116, null, 1246);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_32_Import_Unit_Cost_Amounts', 8117, null, 1247);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_33_Unit_Cost_Audit', 8118, null, 1248);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_34_Allocation', 8119, null, 1249);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_35_CPR_Post_allocation_Balance_Audit', 8120, null, 1250);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_36_CWIP_Post_allocation_Balance_Audit', 8121, null, 1251);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_37_National_Allocation_Balance_Audit', 8122, null, 1252);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_38_National_Allocation_CWIP_Balance_Audit', 8123, null, 1253);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_39_Incremental_Audit', 8124, null, 1254);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_4_CPR_Assign_Tree', 8125, null, 1255);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_40_Net_Tax_Balance_Audit', 8126, null, 1256);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_41_Import_Ledger_Items', 8127, null, 1257);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_42_Import_Ledger_Adjustments', 8128, null, 1258);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_43_Input_Ledger_Activity', 8129, null, 1259);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_44_Copy_Ledger_Activity', 8130, null, 1260);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_45_Depreciation_Floor_Adjustments', 8131, null, 1261);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_46_Negative_Balance_Transfers', 8132, null, 1262);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_47_Copy_Auth_Dist_Relationships', 8133, null, 1263);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_48_Auth_Dist_Audit', 8134, null, 1264);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_49_Rollup_Value_Audit', 8135, null, 1265);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_5_CWIP_Assign_Tree', 8136, null, 1266);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_50_Run_Returns', 8137, null, 1267);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_51_Inherit_Case', 8138, null, 1268);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_52_Import_Levy_Rates', 8139, null, 1269);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_53_Import_Parcel_Assessments', 8140, null, 1270);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_54_Run_Assessment_Allocation', 8141, null, 1271);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_55_Lock_Tax_Year', 8142, null, 1272);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_56_User_Preferences', 8143, null, 1273);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_6_CWIP_Pseudo_Assign_Tree', 8144, null, 1274);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_7_CWIP_Work_Order_Grid', 8145, null, 1275);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_8_Import_CWIP_Assign', 8146, null, 1276);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '5_9_Import_Parcels', 8147, null, 1277);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_1_Introduction4', 8148, null, 1278);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_2_Navigation', 8149, null, 1279);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_3_Search_Edit_Bills', 8150, 'w_ptc_center_main/bills_search', 1280);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_3_1_Using_the_Bills_Select_Tabs', 8151, null, 1281);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_3_2_Using_the_Bills_Search_Results_Grid', 8152, null, 1282);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_3_3_Using_the_Bills_Add_Like_Window', 8153, null, 1283);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_3_4_Using_the_Bill_Voucher_Window', 8154, null, 1284);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_4_Copying_Bills', 8155, null, 1285);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_5_Levy_Rates', 8156, 'w_ptc_center_main/bills_levyrates', 1286);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_6_Schedules', 8157, 'w_ptc_center_main/bills_schedules', 1287);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_7_Statement_Groups', 8158, 'w_ptc_center_main/bills_groups', 1288);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '6_8_User_Preferences', 8159, null, 1289);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_1_Introduction4', 8160, null, 1290);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_2_Navigation', 8161, null, 1291);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_3_Payment_Center_Home', 8162, 'w_ptc_center_main/payment_home', 1292);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_3_1_Un_Vouchered_Bills', 8163, null, 1293);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_3_2_Un_Approved_Requests', 8164, null, 1294);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_3_3_Un_Released_Requests', 8165, null, 1295);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_3_4_Current_Payments', 8166, null, 1296);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_4_Search_Workspace', 8167, 'w_ptc_center_main/payment_search', 1297);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_5_Using_the_Payment_Details_Window', 8168, null, 1298);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_6_Payment_Documents', 8169, null, 1299);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '7_7_User_Preferences', 8170, null, 1300);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_1_Introduction2', 8171, null, 1301);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_10_User_Preferences', 8172, null, 1302);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_2_Navigation', 8173, null, 1303);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_3_Accrual_Control', 8174, 'w_ptc_center_main/accrual_control', 1304);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_4_Approvers', 8175, 'w_ptc_center_main/accrual_approval', 1305);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_5_Accrual_Search', 8176, 'w_ptc_center_main/accrual_search_accruals', 1306);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_5_1_Using_the_Select_Tab_filters', 8177, null, 1307);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_5_2_Using_the_Accrual_Search_Results_Grid', 8178, null, 1308);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_6_Calculate_Liability', 8179, 'w_ptc_center_main/accrual_liability', 1309);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_7_Estimates', 8180, 'w_ptc_center_main/accrual_search_estimates', 1310);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_7_1_Using_the_Select_Tab_filters', 8181, null, 1311);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_7_2_Using_the_Estimate_Search_Results_Grid', 8182, null, 1312);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_8_Actuals', 8183, 'w_ptc_center_main/accrual_search_actuals', 1313);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_9_Accrual_Type_Mapping', 8184, 'w_ptc_center_main/accrual_tree', 1314);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_9_1_Creating_a_New_Tree', 8185, null, 1315);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_9_2_Displaying_An_Existing_Accrual_Assign_Tree', 8186, null, 1316);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '8_9_3_Editing_the_Accrual_Assign_Tree', 8187, null, 1317);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '9_1_Introduction3', 8188, null, 1318);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '9_2_Variables', 8189, null, 1319);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '9_3_Search_Vault', 8190, 'w_ptc_center_main/data_vault', 1320);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '9_4_Templates', 8191, 'w_ptc_center_main/data_templates', 1321);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '9_5_Run_Scenarios', 8192, 'w_ptc_center_main/data_run', 1322);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', '9_6_Search_Scenarios', 8193, 'w_ptc_center_main/data_scenario', 1323);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_1_Property_Tax_Overview', 8194, null, 1324);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_10_Admin_Center', 8195, null, 1325);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_11_Reporting_Center', 8196, null, 1326);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_12_Steps_for_Processing_Yearly_Bills', 8197, null, 1327);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_13_System_Options', 8198, null, 1328);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_14_Net_Tax_Processing', 8199, null, 1329);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_15_Standard_Reports', 8200, null, 1330);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_16_Standard_Return_Reports', 8201, null, 1331);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_17_Steps_for_Processing_Accruals', 8202, null, 1332);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_2_Running_Property_Tax', 8203, null, 1333);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_3_Ledger_Center', 8204, null, 1334);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_4_Parcel_Center', 8205, null, 1335);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_5_Returns_Center', 8206, null, 1336);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_6_Bills_Center', 8207, null, 1337);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_7_Payments_Center', 8208, null, 1338);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_8_Accruals_Center', 8209, null, 1339);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'property tax', 'Chapter_9_Data_Center', 8210, null, 1340);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_PowerTax_Features_Overview', 9000, null, 1341);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_1_Tax_Accounts', 9001, null, 1342);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_10_Deferred_Taxes', 9002, null, 1343);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_11_Forecasts', 9003, null, 1344);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_12_Book_Integration', 9004, null, 1345);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_13_Tax_Provision', 9005, null, 1346);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_14_Technical_Information', 9006, null, 1347);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_15_Documentation', 9007, null, 1348);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_16_Security', 9008, null, 1349);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_17_Flexible_Naming_and_Languages', 9009, null, 1350);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_18_Audit_Trail_Internal_Controls_and_Data_Integrity', 9010, null, 1351);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_19_Query_and_Reporting', 9011, 'w_report_select', 1352);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_2_Set_of_Tax_Books', 9012, null, 1353);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_20_Archiving', 9013, null, 1354);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_21_PowerTaxs_DBA_Toolkit', 9014, null, 1355);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_3_Cases', 9015, null, 1356);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_4_Impact_Analysis', 9016, null, 1357);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_5_Book_to_Tax_Reconciliation', 9017, null, 1358);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_6_Tax_Depreciation', 9018, null, 1359);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_7_Tax_Depletion', 9019, null, 1360);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_8_Reserve_and_Gain_Loss_Maintenance', 9020, null, 1361);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '1_1_9_Alternative_Minimum_Tax_Preferences', 9021, null, 1362);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_1_Beginning_a_New_Tax_Year', 9022, null, 1363);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_1_1_Selecting_an_Existing_Case_or_Creating_a_New_Case', 9023, null, 1364);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_1_2_Creating_a_New_Tax_Year', 9024, null, 1365);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_1_2_Creating_a_New_Tax_Year__Ref94329210', 9025, null, 1366);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_Book_Activity_Processing', 9026, null, 1367);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_1_Running_the_PowerPlan_to_PowerTax_Interface', 9027, null, 1368);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_2_Running_an_Interface_to_PowerTax_from_Another_Fixed_Asset_System', 9028, null, 1369);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_3_Tax_Book_Translate', 9029, null, 1370);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_4_Additions_Interface', 9030, null, 1371);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_5_Retirements_Interface', 9031, null, 1372);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_6_Tax_Test_Retirements', 9032, null, 1373);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_7_Salvage_COR_Allocation', 9033, null, 1374);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_8_M_Item_Allocation', 9034, null, 1375);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '2_2_9_Book_Depreciation_Allocation', 9035, null, 1376);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_1_Main_Menu_Bar', 9036, null, 1377);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_Deferred_Taxes', 9037, null, 1378);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_1_Deferred_Tax_Overview', 9038, null, 1379);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_10Deferred_Tax_Windows', 9039, 'w_dfit_select', 1380);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_11Deferred_Income_Tax_Calculations', 9040, null, 1381);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_11Deferred_Income_Tax_Calculations_Basis_Difference', 9041, null, 1382);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_12Normalization_Schema', 9042, 'w_deferred_tax_schema', 1383);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_13Reports', 9043, null, 1384);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_14Run_Fast', 9044, null, 1385);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_15Cases', 9045, null, 1386);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_2_Methodology_Book_V_Tax_Asset_Recovery', 9046, null, 1387);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_3_Book_Allocation_Mechanism', 9047, null, 1388);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_4_Processing_Sequence_for_Depreciation_Allocation', 9048, null, 1389);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_4_Processing_Sequence_for_Depreciation_Allocation__Ref203964639', 9049, null, 1390);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_5_One_Step_Depreciation_Allocation_Process', 9050, null, 1391);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_6_Depreciation_Allocation_Technique', 9051, null, 1392);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_7_Deferred_Tax_Calculation_Method_Life', 9052, null, 1393);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_8_Deferred_Tax_Calculation_Basis_Difference', 9053, null, 1394);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_10_9_DFIT_Forecasting_Note', 9054, null, 1395);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_11_Forecasts', 9055, null, 1396);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_11_1_Overview', 9056, null, 1397);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_11_2_Forecast_Inputs', 9057, 'w_tax_fcst_input', 1398);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_11_3_Forecast_Reports', 9058, 'w_report_fcst_select', 1399);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_11_4_Forecast_Run_Fast', 9059, 'w_tax_forecast_dll', 1400);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_11_5_Forecast_Cases', 9060, 'w_tax_fcst_case', 1401);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_11_6_Forecast_Export', 9061, 'w_tax_fcst_export', 1402);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_Interface', 9062, null, 1403);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_1_Tax_Book_Translate_Interface', 9063, 'w_tax_book_translate_company', 1404);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_2_Additions_Interface', 9064, 'w_tax_book_additions_company', 1405);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_3_Retirements_Interface', 9065, 'w_tax_book_retirements_company', 1406);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_4_Transfer_Interface', 9066, null, 1407);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_5_Tax_Retirement_Transfers_Rules', 9067, null, 1408);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_6_Tax_Test_Retirements', 9068, null, 1409);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_7_Salvage_COR_Interface', 9069, null, 1410);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_8_Salvage_COR_Automatic_Interface', 9070, 'w_tax_salvage_input_alloc_automatic', 1411);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_12_9_M_Item_Allocation_Interface', 9071, 'w_basis_adj_allocate', 1412);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_MLP', 9072, null, 1413);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_1_Entering_the_MLP_module', 9073, null, 1414);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_2_Book_Activity_Processing__Additions_Interface', 9074, null, 1415);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_3_Book_Activity_Processing_Retirements_Interface', 9075, null, 1416);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_4_Book_Activity_Processing__Transfers', 9076, null, 1417);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_5_Tax_Asset_Grid', 9077, null, 1418);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_6_Drop_Down_Review', 9078, null, 1419);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_7_Reporting', 9079, 'w_report_select', 1420);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_13_8_System_Options', 9080, null, 1421);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_14_Archive', 9081, 'w_tax_archive', 1422);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_15_Transfers', 9082, 'w_tax_transfer_tab', 1423);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_16_Verify', 9083, 'w_pp_verify2', 1424);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_2_Case_Selection', 9084, 'w_tax_case_exist', 1425);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_3_Main_PowerTax_Toolbar', 9085, 'w_tax_main', 1426);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_Tax_Asset_Review', 9086, null, 1427);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_1_Tax_Asset_Selection', 9087, 'w_tax_select', 1428);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_2_Tax_Rollups', 9088, null, 1429);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_3_Asset_Processes', 9089, null, 1430);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_4_Data_Copy', 9090, null, 1431);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_5_Manage', 9091, 'w_tax_asset_manage', 1432);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_6_Add_New_Asset', 9092, 'w_tax_asset_new', 1433);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_7_Merge_Prior_Years_Vintages', 9093, null, 1434);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_8_Merge_Current_Years_Vintage', 9094, null, 1435);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change', 9095, 'w_tax_detail', 1436);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change__Ref160336862', 9096, null, 1437);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change__Ref203825043', 9097, null, 1438);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change_Tax_Asset_Data_Change_', 9098, null, 1439);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change_Tax_Asset_Data_Change_1', 9099, null, 1440);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change_Tax_Asset_Data_Change_2', 9100, null, 1441);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change_Tax_Asset_Data_Change_3', 9101, null, 1442);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_4_9_Tax_Asset_Data_Change_Tax_Asset_Data_Change5', 9102, null, 1443);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_Tax_Depreciation_Rates_Window', 9103, 'w_tax_rate', 1444);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_1_Tax_Depreciation_Rate_Parameters', 9104, null, 1445);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_2_Tax_Depreciation_Rates', 9105, null, 1446);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_3_Depreciation_Methods', 9106, null, 1447);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_4_PowerTax_Rate_Calculation_Logic', 9107, null, 1448);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_5_Tax_Rates_for_the_Mid_Quarter_Convention', 9108, null, 1449);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_6_Tax_Rates_for_the_Short_Tax_Year', 9109, null, 1450);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_5_7_Tax_Rate_and_Convention_Lock', 9110, null, 1451);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_6_Tax_Conventions', 9111, null, 1452);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_6_1_Overview', 9112, null, 1453);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_6_2_Tax_Convention_Options', 9113, 'w_tax_conv_detail', 1454);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_7_Reporting', 9114, null, 1455);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_7_1_Selecting_Criteria', 9115, 'w_report_select', 1456);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_7_2_Rollup_Reports_and_Filters', 9116, null, 1457);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_7_3_Overlay_Reports', 9117, null, 1458);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_7_4_Report_Window_Features', 9118, null, 1459);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_7_5_Report_4562', 9119, null, 1460);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_8_Case_Management', 9120, 'w_tax_case_main', 1461);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_8_1_Case_Copy_Window', 9121, 'w_tax_case_setup', 1462);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '3_9_Running_Cases', 9122, 'w_tax_run_dll', 1463);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_1_Other_Tax_Depreciation_Activities', 9123, null, 1464);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_1_1_New_Tax_Year', 9124, 'w_tax_run_dll', 1465);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_1_2_Conversion_Initial_Balances', 9125, null, 1466);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_1_3_Adding_Vintages_Accounts_and_Sets_of_Tax_Books', 9126, null, 1467);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_Tax_Depreciation_Topics', 9127, null, 1468);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_1_AMT_and_ACE', 9128, null, 1469);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_2_Amortizations', 9129, null, 1470);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_3_Auto_Car_Limits', 9130, null, 1471);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_4_Capitalized_Depreciation', 9131, null, 1472);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_5_Short_Tax_Years', 9132, null, 1473);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_6_Dispositions', 9133, null, 1474);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_7_E_P_Depreciation', 9134, null, 1475);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_2_8_Listed_Property', 9135, null, 1476);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '4_3_Adding_a_Report', 9136, null, 1477);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '5_1_Overview2', 9137, null, 1478);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '5_2_PowerPlan_Table_Maintenance', 9138, null, 1479);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '5_3_PowerPlan_Table_Data', 9139, null, 1480);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '5_3_1_Overview', 9140, null, 1481);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '5_3_2_Finding_Related_Tables', 9141, null, 1482);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '5_4_PowerPlan_Table_Data_Entry', 9142, null, 1483);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '5_5_Special_Note_for_Table_Maintenance', 9143, null, 1484);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_1_Standard_Table_Maintenance', 9144, null, 1485);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_10_Maintain_Company_and_Consolidation_Tree_Views', 9145, null, 1486);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_2_Add_a_Vintage', 9146, null, 1487);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_3_Add_Vintage_Records', 9147, null, 1488);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_3_1_Copy_from_an_Existing_Vintage', 9148, null, 1489);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_3_2_Use_Tax_Depreciation_Schema_Assignments', 9149, null, 1490);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_4_Add_a_Tax_Class', 9150, null, 1491);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_5_Add_a_Tax_Include_Id', 9151, null, 1492);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_6_Add_a_Tax_Include_Activity', 9152, null, 1493);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_7_Add_a_Tax_Reconcile_Item', 9153, null, 1494);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_8_Add_a_Tax_Limit', 9154, null, 1495);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '7_9_Modify_Tax_Control', 9155, null, 1496);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_1_Overview1', 9156, null, 1497);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_1_1_Oracle_Database', 9157, null, 1498);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_1_2_Security_for_PowerPlan', 9158, null, 1499);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_1_3_Table_Audits', 9159, null, 1500);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_2_Security_for_PowerPlan_PowerTax', 9160, null, 1501);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_2_1_Users_and_Groups_Menu_Option', 9161, null, 1502);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_2_2_Windows_Menu_Options', 9162, null, 1503);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_3_Audit_Trail_for_Table_Changes', 9163, null, 1504);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_3_1_Using_the_Audit_Trail_Window_Trigger_Audits_', 9164, null, 1505);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_3_2_Using_the_Table_Audit_Trail_Window_Non_Trigger_Audits_', 9165, null, 1506);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_3_3_Using_the_Audit_Detail_Window', 9166, null, 1507);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', '8_3_4_Table_Audit_Related_Tables', 9167, null, 1508);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_1_Introduction_to_PowerTax', 9168, null, 1509);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_2_Operating_Procedures', 9169, null, 1510);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_3_Running_PowerTax', 9170, null, 1511);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_4_Other_Tax_Depreciation_Topics', 9171, null, 1512);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_5_PowerTax_Table_Maintenance', 9172, null, 1513);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_6_PowerTax_Table_Maintenance', 9173, null, 1514);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_7_Additional_Table_Instructions', 9174, null, 1515);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'powertax', 'Chapter_8_PowerPlan_Security', 9175, null, 1516);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_Overview4', 10000, null, 1517);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_1_Statement_of_Purpose', 10001, null, 1518);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_10_Tax_Journal_Entry_Schemas', 10002, null, 1519);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_2_Context_Sensitive_Help', 10003, null, 1520);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_3_Cases_Versions', 10004, null, 1521);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_4_Use_of_Estimates', 10005, null, 1522);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_5_Data_Input_Sources_to_Provision', 10006, null, 1523);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_6_Current_Provision', 10007, null, 1524);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_7_Deferred_Tax_Accounting__APB_11', 10008, null, 1525);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_8_Liability_Tax_Accounting__SFAS_109', 10009, null, 1526);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_1_9_Regulatory_Treatment_Schemas', 10010, null, 1527);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_2_Application_Design', 10011, null, 1528);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_2_1_Multiple_Document_Interface', 10012, null, 1529);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '1_2_2_PowerTax_Provision_Datawindow', 10013, null, 1530);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_M_Items', 10014, null, 1531);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_1_M_Item_Overview', 10015, 'w_tax_accrual_m-items', 1532);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_2_Adding_New_M_Items', 10016, 'w_tax_accrual_m_item_add', 1533);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_3_Deferred_Taxes', 10017, 'w_tax_accrual_def_tax', 1534);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_4_Analyze_M_FAS_109_Analysis', 10018, 'w_tax_accrual_analyze_m', 1535);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_5_Case_Configuration', 10019, 'w_tax_accrual_case_configs', 1536);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_6_M_Master', 10020, 'w_tax_accrual_m_master', 1537);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_1_7_Clear_M_Item_Tool', 10021, null, 1538);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_Book_Transactions', 10022, null, 1539);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_1_Integration_of_CR_GL', 10023, 'w_tax_accrual_cr_mapping', 1540);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_1_Integration_of_CR_GL__Ref347910903', 10024, null, 1541);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_2_Allocation', 10025, 'w_tax_accrual_alloc_percents', 1542);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_3_CR_Setup', 10026, 'w_tax_accrual_technical_setup', 1543);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_4_CR_to_Provision', 10027, 'w_tax_accrual_technical_setup_cr_to_prov', 1544);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_5_Compliance_Export', 10028, 'w_tax_accrual_export', 1545);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_6_PowerTax_Activity', 10029, 'w_tax_accrual_powertax_setup', 1546);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_7_Import_File_Setup', 10030, 'w_tax_accrual_import_file_setup', 1547);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_8_Consolidation', 10031, 'w_tax_accrual_master_supermap', 1548);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_2_9_DMI48_Import', 10032, 'w_tax_accrual_dmi_setup', 1549);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_Tax_Rates', 10033, 'w_tax_accrual_rates_tab', 1550);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_1_Overview', 10034, 'w_tax_accrual_rates_tab_dit_schemas', 1551);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_2_Managing_DIT_Schemas', 10035, 'w_tax_accrual_results_rates', 1552);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_2_Managing_DIT_Schemas__Associating_Current_Rates', 10036, null, 1553);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_3_Updating_State_Apportionment_Percents', 10037, 'w_tax_accrual_rates_tab_apportionment', 1554);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_4_Maintaining_DIT_Schema_Types', 10038, 'w_tax_accrual_jur_allo', 1555);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_5_Maintaining_Statutory_Rates', 10039, 'w_tax_accrual_entity_rates_alt', 1556);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_6_Maintaining_Entity_Includes', 10040, 'w_tax_accrual_entity_include', 1557);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_3_7_Maintaining_Entity_Deductibility', 10041, 'w_tax_accrual_entity_deduct', 1558);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_Tax_Journal_Entries', 10042, null, 1559);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_1_Overview', 10043, null, 1560);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_2_Journal_Entry_Assignments', 10044, 'w_tax_accrual_je_select', 1561);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_3_Journal_Entry_Search_Window', 10045, 'w_tax_accrual_je_master', 1562);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_4_Journal_Entry_Account_Definitions', 10046, 'w_tax_accrual_case_configs_curr_tax_jes', 1563);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_4_Journal_Entry_Account_Definitions_Setting_up_GL_Accounts', 10047, null, 1564);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_5_Current_Tax_Journal_Entries', 10048, 'w_tax_accrual_je_translate', 1565);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_6_JE_Translate', 10049, 'w_tax_accrual_subledger_adjust_query', 1566);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_7_JE_Adjustment', 10050, 'w_tax_accrual_subledger_adjust', 1567);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_8_Inter_Company_Transfers', 10051, 'w_tax_accrual_je_adj_update', 1568);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_4_9_Journal_Entry_Pointer_Changes_Balance_Transfers', 10052, 'delete w_tax_accrual_tbbs, detail windows are below', 1569);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_5_Subledger_Functionality', 10053, 'delete w_tax_accrual_tbbs, detail windows are below', 1570);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_5_1_Overview', 10054, null, 1571);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_5_2_Beginning_Balances', 10055, 'w_tax_accrual_beg_balances', 1572);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_5_3_Payments', 10056, 'w_tax_accrual_other_interface', 1573);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_6_Tax_Basis_Balance_Sheet', 10057, 'w_tax_accrual_tbbs', 1574);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_7_Table_Maintenance', 10058, null, 1575);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_7_1_Overview', 10059, null, 1576);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_7_2_PowerPlan_Table_Maintenance', 10060, 'w_pp_table_select', 1577);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_7_3_PowerPlan_Table_Data', 10061, null, 1578);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_7_4_Provision_Tables', 10062, null, 1579);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_Reports', 10063, null, 1580);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_1_Report_Window', 10064, 'w_tax_accrual_reports', 1581);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_2_Report_Filters', 10065, null, 1582);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_3_Custom_Saved_Reports', 10066, null, 1583);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_4_Company_Consolidation_Tree_Setup', 10067, 'w_pp_treeview1', 1584);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_5_GL_Accounts_Rollup_Assignments', 10068, 'w_tax_accrual_account_rollups', 1585);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_6_M_Item_Rollup_Assignments', 10069, 'w_tax_m_item_rollups', 1586);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '2_8_7_Report_Identification', 10070, null, 1587);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_1_Processing_Overview', 10071, null, 1588);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_Monthly_Processing', 10072, null, 1589);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_1_Check_for_Changes', 10073, null, 1590);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_10_Alerts', 10074, null, 1591);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_11_Journal_Entry_Preparation_and_Approval', 10075, 'w_tax_accrual_approval', 1592);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_2_Importing_Data_from_a_File', 10076, null, 1593);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_3_Interfacing_with_the_Charge_Repository', 10077, null, 1594);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_4_Interfacing_PowerTax_Activity', 10078, null, 1595);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_5_User_Inputs', 10079, null, 1596);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_6_GL_Month_Manager_Months_Manager_Tab_', 10080, 'w_tax_accrual_months_manager', 1597);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_7_Running', 10081, 'w_tax_accrual_process_month', 1598);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_8_Reporting_and_Reviewing', 10082, 'w_tax_accrual, tabpage_reports', 1599);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_2_9_Audits', 10083, 'w_trig_history_display', 1600);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_3_Special_Cases', 10084, null, 1601);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_3_1_Return_to_Accrual_Adjustments', 10085, null, 1602);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_3_2_Merge_Cases', 10086, 'w_tax_accrual_merge_cases', 1603);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_3_3_Sync_Cases', 10087, 'w_sync_cases', 1604);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_3_4_Case_Management', 10088, 'w_tax_accrual_case_manager', 1605);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', '3_3_5_Budgeting_in_Provision', 10089, null, 1606);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', 'Chapter_1_Introduction_to_PowerTax_Provision', 10090, null, 1607);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', 'Chapter_2_Configuration_Setup_and_Maintenance', 10091, null, 1608);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax accrual', 'Chapter_3_Monthly_Provision_Processing', 10092, null, 1609);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_1_Overview2', 11000, 'w_mypp', 1610);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_2_My_PowerPlan_Display_User_Options', 11001, 'w_user_options', 1611);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_3_Turning_on_off_the_My_PowerPlan_display_on_demand', 11002, null, 1612);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_4_My_PowerPlan_Navigation', 11003, null, 1613);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_4_1_Accessing_a_module', 11004, null, 1614);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_5_Changing_the_My_PowerPlan_Layout', 11005, 'w_mypp_change_layout', 1615);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_Setting_up_the_My_PowerPlan_Modules', 11006, null, 1616);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_1_Shortcuts_Module', 11007, 'w_mypp_shortcut_manage', 1617);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_2_Reports_Module', 11008, 'w_mypp_report_manage', 1618);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_3_User_Defined_Queries_Module', 11009, null, 1619);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_4_Alerts_Module', 11010, null, 1620);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_5_Alerts_Dashboard_Module', 11011, 'w_mypp_alert_dash_manage2', 1621);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_6_Approvals_User_Comments_Module', 11012, null, 1622);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_7_To_Do_List', 11013, null, 1623);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_8_Using_the_To_Do_List', 11014, 'w_mypp_todo_manage', 1624);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_6_9_PowerPlan_News_Module', 11015, null, 1625);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_7_Batch_Reports_Module', 11016, null, 1626);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_7_1_Setting_up_Batch_Reports', 11017, 'w_batch_report_manage', 1627);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_8_My_PowerPlan_System_Administration', 11018, 'w_mypp_all_shortcut_manage', 1628);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_8_1_Administrator_Shortcut_Management_Tab', 11019, null, 1629);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_8_2_User_Setup_Management_Tab', 11020, null, 1630);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '10_8_3_Single_Item_Deployment_Tab', 11021, null, 1631);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_1_System_Menu', 11022, null, 1632);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_1_1_User_Window_Information', 11023, null, 1633);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_1_2_User__Save_Window_Size_Remove_Window_Size', 11024, null, 1634);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_1_3_Debug__Used_to_Start_Various_Traces', 11025, 'w_starttrace', 1635);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_1_4_Security', 11026, null, 1636);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_2_Login_Menu', 11027, null, 1637);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_Show_Environment', 11028, 'w_show_environment', 1638);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_1_Using_the_Show_Environment_Window__Oracle_Client_Tab', 11029, null, 1639);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_2_Using_the_Show_Environment_Window__PowerBuilder_Tab', 11030, null, 1640);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_3_Using_the_Show_Environment_Window__PowerPlan_Tab', 11031, null, 1641);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_4_Using_the_Show_Environment_Window__Objects_Tab', 11032, null, 1642);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_5_Using_the_Show_Environment_Window__Windows_Tab', 11033, null, 1643);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_6_Using_the_Show_Environment_Window__File_Size_Tab', 11034, null, 1644);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_3_7_Using_the_Show_Environment_Window__Regedit_Tab', 11035, null, 1645);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_System_Configuration_Window', 11036, 'w_config', 1646);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_1_Using_the_System_Configuration_Window__Connect_Information_Tab', 11037, null, 1647);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_2_Using_the_System_Configuration_Window__Help_Files_Tab', 11038, null, 1648);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_3_Using_the_System_Configuration_Window__Printer_Setup_Tab', 11039, null, 1649);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_4_Using_the_System_Configuration_Window__Mail_Setup_Tab', 11040, null, 1650);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_5_Using_the_System_Configuration_Window__Options_Tab', 11041, null, 1651);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_6_Using_the_System_Configuration_Window__Version_Tab', 11042, null, 1652);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_7_Using_the_System_Configuration_Window__PB_Trace_Tab', 11043, null, 1653);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_4_8_Using_the_System_Configuration_Window__Single_Sign_On_Tab', 11044, null, 1654);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_5_Custom_Help', 11045, null, 1655);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_6_PowerPlan_User_Preferences', 11046, null, 1656);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_6_1_Overview', 11047, null, 1657);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_6_2_Using_the_Preferences_Toolbar', 11048, null, 1658);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_6_3_Changing_Password', 11049, 'w_new_password', 1659);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_6_4_Changing_Regional_Setting', 11050, 'w_regional_setting', 1660);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_6_5_Changing_User_Options', 11051, 'w_user_options', 1661);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_7_Import_Tool', 11052, null, 1662);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_7_1_Importing_a_New_File', 11053, null, 1663);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_7_2_Reprocessing_an_Existing_Import', 11054, null, 1664);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_7_3_Editing_an_Import_File', 11055, null, 1665);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_7_4_Import_Template_Maintenance', 11056, null, 1666);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_7_5_Creating_a_Spreadsheet_Template', 11057, null, 1667);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_8_Kickout_Management_Tool', 11058, null, 1668);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_8_1_Configuration', 11059, 'w_api_errors_config', 1669);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '11_8_2_Fix_Errors', 11060, 'w_api_errors', 1670);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_1_Overview2', 11061, null, 1671);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_1_1_Defining_Property_Units', 11062, null, 1672);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_1_2_Tax_Considerations', 11063, null, 1673);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_1_3_Non_Unitized_Additions_Etc_', 11064, null, 1674);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_1_4_Overall_Use_of_the_Property_Unit_Catalog', 11065, null, 1675);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_1_5_Elements_of_the_Property_Unit_Catalog', 11066, null, 1676);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_2_Using_the_Property_Unit_Catalog_Read_only_', 11067, 'w_prop_unit_maint', 1677);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_2_1_Accessing_the_Window', 11068, null, 1678);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_3_Using_the_Property_Unit_Catalog_Edit_Mode_', 11069, null, 1679);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_3_1_Overview1', 11070, null, 1680);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_3_2_Accessing_the_Window', 11071, null, 1681);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_3_3_Searching_for_Property_Units', 11072, null, 1682);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_3_4_Using_the_Fast_Relate_Copy_button', 11073, null, 1683);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_3_5_Using_the_Property_Unit_Catalog_Toolbar', 11074, null, 1684);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_4_Editing_Property_Unit_Details', 11075, 'w_prop_unit_detail', 1685);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_4_1_Overview1', 11076, null, 1686);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_4_2_Accessing_the_Window', 11077, null, 1687);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_4_3_Description_of_the_Fields_on_this_Window', 11078, null, 1688);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_4_4_Relating_Property_Units_to_Other_Fields_i_e_Utility_Account_', 11079, null, 1689);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_4_5_Using_the_Buttons_on_the_Window', 11080, null, 1690);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_4_6_Adding_Copying_a_New_Property_Unit', 11081, null, 1691);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_5_Editing_Retirement_Unit_Details', 11082, 'w_retire_unit_detail', 1692);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_5_1_Overview1', 11083, null, 1693);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_5_2_Accessing_the_Window_', 11084, null, 1694);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_5_3_Description_of_the_Fields_on_this_Window', 11085, null, 1695);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_5_4_Relating_Retirement_Units_to_Other_Fields', 11086, null, 1696);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '2_5_5_Using_the_Buttons_on_the_Window', 11087, null, 1697);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_1_Overview2', 11088, null, 1698);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_10_Maintaining_Asset_Locations', 11089, null, 1699);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_10_1_Overview', 11090, null, 1700);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_10_2_Elements_of_PowerPlan_Locations', 11091, null, 1701);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_10_3_Using_the_PowerPlan_Location_Maintenance_Window', 11092, 'w_location_maint', 1702);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_10_4_Using_the_Major_Location_Details_Window', 11093, 'w_location_detail', 1703);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_10_5_Using_the_Asset_Location_Details_Window', 11094, 'w_asset_loc_detail', 1704);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_11_Maintaining_General_Ledger_Plant_Sub_Accounts', 11095, null, 1705);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_11_1_Overview1', 11096, null, 1706);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_11_2_Using_the_PowerPlan_Account_Maintenance_Window', 11097, 'w_account_maintenance', 1707);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_11_3_Using_the_Utility_Account_Details_Window', 11098, 'w_util_account_detail', 1708);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_11_4_Using_the_Sub_Account_Details_Window', 11099, 'w_sub_account_detail', 1709);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_12_Maintaining_Reports', 11100, null, 1710);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_12_1_Overview', 11101, null, 1711);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_12_2_Using_the_PowerPlan_Report_Maintenance_Window', 11102, 'w_report_maint', 1712);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_12_2_Using_the_PowerPlan_Report_Maintenance_Window__Ref102794202', 11103, null, 1713);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_13_PowerPlan_System_Control_Tables', 11104, null, 1714);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_13_1_Overview', 11105, null, 1715);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_13_2_Using_the_PowerPlan_System_Control_Window', 11106, 'w_system_control', 1716);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_14_Multi_Currency_Processing', 11107, null, 1717);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_14_1_Overview', 11108, null, 1718);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_14_2_Definitions', 11109, null, 1719);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_14_3_Data', 11110, null, 1720);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_15_Flexible_Naming_and_Multiple_Languages', 11111, null, 1721);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_15_1_Overall_Configuration', 11112, null, 1722);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_15_2_Using_Flex_Names_and_Languages', 11113, null, 1723);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_15_3_Changing_a_label_of_one_object', 11114, 'w_flex_names', 1724);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_15_4_Changing_labels_of_multiple_objects', 11115, 'w_customize', 1725);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_15_5_Restoring_Labels', 11116, null, 1726);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_15_6_Users_and_Language', 11117, null, 1727);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_2_Navigation_Using_the_Table_Maintenance_Toolbar', 11118, 'w_table_main', 1728);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_3_Maintaining_Standard_Tables', 11119, null, 1729);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_3_1_Using_the_PowerPlan_Table_Maintenance_Selection_Window', 11120, 'w_pp_table_select', 1730);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_3_2_Using_the_PowerPlan_Table_Data_Window', 11121, 'w_pp_table_grid', 1731);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_3_3_Using_the_PowerPlan_Table_Data_Entry_Window', 11122, 'w_pp_table_entry', 1732);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_3_4_Using_the_Excel_Edit_Functionality', 11123, null, 1733);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_4_Sorting_Rows', 11124, null, 1734);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_4_1_Using_the_Specify_Sorting_Criteria_Window', 11125, 'w_sort_dw_mult', 1735);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_5_Filtering_Rows', 11126, null, 1736);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_5_1_Using_the_Specify_Filter_Criteria_Window', 11127, 'w_filter_dw_mult', 1737);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_6_Finding_Related_Tables', 11128, null, 1738);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_6_1_Using_the_Table_References_Window', 11129, 'w_pp_table_relate', 1739);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_Datagrid_Right_Click_Menu', 11130, null, 1740);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_1_Searching', 11131, null, 1741);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_2_Sorting', 11132, null, 1742);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_3_Printing', 11133, null, 1743);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_4_Exporting', 11134, null, 1744);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_5_Moving_Columns', 11135, null, 1745);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_6_Using_the_PowerBuilder_Filter', 11136, null, 1746);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_7_Saving_Appearance_Changes_Customize_Uncustomize_', 11137, null, 1747);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_7_8_Microhelp_Sum', 11138, null, 1748);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_8_Tree_View', 11139, 'w_pp_tree_select', 1749);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_8_Tree_View', 11139, 'w_pp_treeview1', 1750);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_9_Standard_Table_Maintenance_Setup', 11140, null, 1751);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_9_1_Modifying_the_display_of_a_Standard_Table', 11141, null, 1752);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_9_2_Adding_a_custom_table_to_Standard_Table_Maintenance', 11142, null, 1753);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_9_3_Editing_the_PowerPlan_Columns_entries', 11143, null, 1754);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '3_9_4_Table_Queries_PP_Any_Query_', 11144, 'w_pp_any_query_options', 1755);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_1_Overview2', 11145, null, 1756);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_2_Navigation_Using_the_Administration_Toolbar', 11146, 'w_system_admin', 1757);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_3_Jurisdictional_Allocations', 11147, null, 1758);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_3_1_Overview', 11148, 'w_jur_allo_menu', 1759);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_3_2_Navigation', 11149, null, 1760);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_3_3_Allocation_Setup', 11150, 'w_jur_allo', 1761);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_3_3_Allocation_Setup__Ref288571875', 11151, null, 1762);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_3_4_Allocation_Calculations', 11152, null, 1763);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_3_5_Allocation_Reporting', 11153, 'w_reporting_main', 1764);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_4_Verify_Alert', 11154, 'w_pp_verify2', 1765);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_4_1_Adding_and_Modifying_Alerts', 11155, 'w_pp_verify_maint2', 1766);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_4_2_Using_the_Verify_Alert_System_Category_Setup_Window', 11156, 'w_pp_verify_category', 1767);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_4_3_Using_the_Verify_Alert_System_Batch_Setup_Window', 11157, 'w_pp_verify_batch', 1768);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_4_4_Viewing_Verify_Alert_System_Results', 11158, 'w_pp_verify_errors2', 1769);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_4_5_Using_the_Verify_Alert_Result_Details_Window', 11159, 'w_pp_verify_display2', 1770);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '4_5_PowerPlan_Tools_Window', 11160, 'w_pp_tools', 1771);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '5_1_Database_Hints', 11161, 'w_datawindow_hints', 1772);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '5_2_PowerPlan_Online_Logs', 11162, 'w_pp_online_logs', 1773);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_1_Overview1', 11163, null, 1774);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_2_Batch_Processing', 11164, 'w_batch', 1775);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_2_1_Using_the_Batch_Window__Jobs_Tab', 11165, null, 1776);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_2_1_Using_the_Batch_Window__Jobs_Tab_Submit_Batch_Job_Window', 11166, null, 1777);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_2_2_Using_the_Batch_Window__Queues_Tab', 11167, null, 1778);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_2_3_Using_the_Batch_Window__Programs_Tab', 11168, null, 1779);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_2_4_Using_the_Batch_Window__Reports_Tab', 11169, null, 1780);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_2_5_Using_the_Batch_Window__Execute_Tab', 11170, null, 1781);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '6_3_Batch_Reporting', 11171, null, 1782);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_1_Overview1', 11172, null, 1783);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_2_Security_for_Windows', 11173, null, 1784);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_2_1_Using_the_Security_for_Windows_Option', 11174, 'w_security', 1785);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_2_2_Displaying_the_Security_Objects', 11175, null, 1786);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_3_System_Security', 11176, 'w_security_system', 1787);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_3_1_Using_the_System_Security_Window__Users_and_Groups', 11177, 'w_security_system_users_and_groups', 1788);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_3_2_Using_the_System_Security_Window__Users_and_Companies', 11178, 'w_security_system_users_and_companies', 1789);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_3_3_Using_the_System_Security_Window__Tables_and_Groups', 11179, 'w_security_system_tables_and_groups', 1790);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_3_4_Using_the_System_Security_Window__Class_Codes_and_Groups', 11180, 'w_security_system_class_codes_and_groups', 1791);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_3_5_Using_the_System_Security_Window__Work_Order_Types_and_Groups', 11181, 'w_security_system_wo_types_and_groups', 1792);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_3_6_Using_the_System_Security_Window__Reports_and_Groups', 11182, 'w_security_system_reports_and_groups', 1793);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_4_Using_the_System_Security_Window__Alias', 11183, null, 1794);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_4_1_Using_the_System_Security_Window__Login_Management', 11184, null, 1795);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_4_2_Using_the_System_Security_Window__Sync_Database_IDs', 11185, null, 1796);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_4_3_Using_the_System_Security_Window__Mail_IDs', 11186, null, 1797);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_4_4_Using_the_System_Security_Window__User_Description', 11187, null, 1798);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_4_5_Using_the_System_Security_Window__Reports', 11188, null, 1799);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_4_6_Using_the_System_Security_Window__Rule_based_Security', 11189, null, 1800);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_5_Oracle_Single_Sign_On', 11190, null, 1801);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_5_1_SSO_Overview', 11191, null, 1802);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '7_5_2_SSO_Installation_details', 11192, null, 1803);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_1_Overview2', 11193, null, 1804);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_2_Audit_Trail_for_Table_Changes', 11194, 'w_audit_control', 1805);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_2_1_Using_the_Audit_Trail_Window_Trigger_Audits_', 11195, 'w_trig_audit_table_display', 1806);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_2_2_Using_the_Table_Audit_Trail_Window_Non_Trigger_Audits_', 11196, 'w_pp_audit', 1807);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_2_3_Using_the_Audit_Detail_Window', 11197, 'w_pp_audit_detail', 1808);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_2_4_Table_Audit_Related_Tables', 11198, 'w_pp_audit_relate', 1809);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_3_Using_the_Trigger_Creation_Window_to_Create_an_Audit_Trigger', 11199, 'w_trig_main', 1810);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_3_1_Steps_to_Create_an_Audit_Trigger', 11200, null, 1811);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_3_2_Additional_Functionality_of_the_Trigger_Creation_Window', 11201, null, 1812);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_4_Review_of_Audit_Records', 11202, null, 1813);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_4_1_Context_sensitive_audits', 11203, null, 1814);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_4_2_Table_or_subsystem_audits', 11204, null, 1815);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '8_5_Using_the_Audit_Trail_Window', 11205, 'w_trig_history_display', 1816);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', '9_1_Interface_and_Other_Passwords', 11206, 'w_ppc_program_opt', 1817);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_1_Introduction2', 11207, null, 1818);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_10_My_PowerPlan', 11208, null, 1819);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_11_Miscellaneous', 11209, null, 1820);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_2_Property_Unit_Catalog', 11210, null, 1821);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_3_PowerPlan_Table_Maintenance', 11211, null, 1822);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_4_Administration', 11212, null, 1823);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_5_Other_Technical_Information', 11213, null, 1824);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_6_Batch_Processing', 11214, null, 1825);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_7_Security', 11215, null, 1826);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_8_PowerPlan_Table_Audits', 11216, null, 1827);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Chapter_9_Other_Security_Issues', 11217, null, 1828);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'admin', 'Overview', 11218, null, 1829);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Account_Summary', 12000, null, 1830);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Account_Type', 12001, null, 1831);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Action_Code', 12002, null, 1832);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Action_Indicator_Xlat', 12003, null, 1833);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Activity_Code', 12004, null, 1834);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Adjust_Convention', 12005, null, 1835);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Adjusted_Plant_History', 12006, null, 1836);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Calc', 12007, null, 1837);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Calc_Test', 12008, null, 1838);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Calc_WO_List_Temp', 12009, null, 1839);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Control', 12010, null, 1840);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Data', 12011, null, 1841);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Data_Test_CPI_Retro_Rates_', 12012, null, 1842);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Input', 12013, null, 1843);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Input_Ratio', 12014, null, 1844);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_OH_Process_Control', 12015, null, 1845);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Rate_Calc', 12016, null, 1846);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Rate_Calc_Type', 12017, null, 1847);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\AFUDC_Status', 12018, null, 1848);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Aged_Transaction', 12019, null, 1849);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Amortization_Type', 12020, null, 1850);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Account', 12021, null, 1851);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Account_Class', 12022, null, 1852);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Account_Data', 12023, null, 1853);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Account_Depr_Group', 12024, null, 1854);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Depr_Group', 12025, null, 1855);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Depr_Ledger', 12026, null, 1856);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Graph_Control', 12027, null, 1857);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Rsrv_Transaction', 12028, null, 1858);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Transaction_Code', 12029, null, 1859);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Analysis_Version', 12030, null, 1860);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Apport_Case', 12031, null, 1861);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Apport_Company_Group', 12032, null, 1862);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Apport_Data', 12033, null, 1863);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Apport_Data_Load', 12034, null, 1864);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Apport_Factor_Item', 12035, null, 1865);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Apport_Results', 12036, null, 1866);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Apport_State_Weight', 12037, null, 1867);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval', 12038, null, 1868);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Auth_Level', 12039, null, 1869);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Defaults', 12040, null, 1870);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Delegation', 12041, null, 1871);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Delegation_Hist', 12042, null, 1872);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Hierarchy', 12043, null, 1873);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Notification', 12044, null, 1874);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Routing', 12045, null, 1875);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Status', 12046, null, 1876);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Steps', 12047, null, 1877);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approval_Steps_History', 12048, null, 1878);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Approvals_Pending', 12049, null, 1879);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Account_Summary', 12050, null, 1880);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_AFUDC_Input', 12051, null, 1881);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Basis_Amounts', 12052, null, 1882);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Compress_CPR_Act', 12053, null, 1883);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_CPR_Act_Basis', 12054, null, 1884);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_CPR_Activity', 12055, null, 1885);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_CPR_Memo_Activity', 12056, null, 1886);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Deferred_Income_Tax', 12057, null, 1887);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Deferred_Income_Tax_Transf', 12058, null, 1888);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Depr_Ledger', 12059, null, 1889);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Depr_Res_Allo_Factors', 12060, null, 1890);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Depr_Vintage_Summary', 12061, null, 1891);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Dfit_Forecast_Output', 12062, null, 1892);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Fcst_DFIT_Forecast_Output', 12063, null, 1893);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Fcst_Tax_Forecast_Input', 12064, null, 1894);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Fcst_Tax_Forecast_Output', 12065, null, 1895);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Funding_Justification', 12066, null, 1896);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_GL_Transaction', 12067, null, 1897);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_PP_Processes_Messages', 12068, null, 1898);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_PP_Processes_Occurrences', 12069, null, 1899);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Retire_Transaction', 12070, null, 1900);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Annotation', 12071, null, 1901);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Book_Reconcile', 12072, null, 1902);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Book_Reconcile_Transfer', 12073, null, 1903);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Depreciation', 12074, null, 1904);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Depreciation_Transfer', 12075, null, 1905);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Forecast_Input', 12076, null, 1906);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Forecast_Output', 12077, null, 1907);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Record_Control', 12078, null, 1908);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Renumber_ID', 12079, null, 1909);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Renumber_Tids', 12080, null, 1910);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Tax_Transfer_Control', 12081, null, 1911);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_WO_Available_List', 12082, null, 1912);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_WO_Bk_Available_List', 12083, null, 1913);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_WO_Bk_Charges', 12084, null, 1914);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_WO_Bk_Groups', 12085, null, 1915);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_WO_Bk_Units', 12086, null, 1916);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_WO_Problems', 12087, null, 1917);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Work_Order_Blankets_List', 12088, null, 1918);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Arc_Work_Order_List', 12089, null, 1919);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ArcCR_Archive_History', 12090, null, 1920);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Archive_Files', 12091, null, 1921);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Archive_Status', 12092, null, 1922);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Archive_System_Control', 12093, null, 1923);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Archive_System_Log', 12094, null, 1924);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Archive_Table_List', 12095, null, 1925);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Archive_Tax_List', 12096, null, 1926);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Archive_Trace', 12097, null, 1927);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO', 12098, null, 1928);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Charge', 12099, null, 1929);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Delete_Charge', 12100, null, 1930);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Depr_Activity', 12101, null, 1931);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Document', 12102, null, 1932);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Fcst_Liability', 12103, null, 1933);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Fcst_Liability_Dtl', 12104, null, 1934);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Layer', 12105, null, 1935);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Layer_Discounting', 12106, null, 1936);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Layer_Stream', 12107, null, 1937);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Layer_Stream_Downward_Adj', 12108, null, 1938);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Layer_Work_Order', 12109, null, 1939);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Liability', 12110, null, 1940);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Liability_Accr_Dtl', 12111, null, 1941);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Liability_Adj', 12112, null, 1942);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Liability_Preview', 12113, null, 1943);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Mass_Calc', 12114, null, 1944);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Mass_RU', 12115, null, 1945);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Multi_Est_Review', 12116, null, 1946);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Cost_Component', 12117, null, 1947);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Cost_Study', 12118, null, 1948);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Cost_Study_Control', 12119, null, 1949);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Cost_Study_Fcf', 12120, null, 1950);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Discount_Group', 12121, null, 1951);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Discount_Group_Rate', 12122, null, 1952);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Esc_Factor_Control', 12123, null, 1953);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Escalation_Factor', 12124, null, 1954);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Inf_Factor', 12125, null, 1955);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Inf_Factor_Data', 12126, null, 1956);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Layer', 12127, null, 1957);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Scen_Sum_Est', 12128, null, 1958);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Scenario', 12129, null, 1959);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Scenario_Estimates', 12130, null, 1960);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Scenario_Summary', 12131, null, 1961);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Scenario_Type', 12132, null, 1962);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Settlement_Adj', 12133, null, 1963);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Slide_Factor', 12134, null, 1964);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_PP_Slide_Factor_Control', 12135, null, 1965);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Related_Location', 12136, null, 1966);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Rollup', 12137, null, 1967);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Settle_Layer_Stream', 12138, null, 1968);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Settlement_Adj', 12139, null, 1969);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Status', 12140, null, 1970);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Stream', 12141, null, 1971);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Temp_Asset', 12142, null, 1972);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Temp_Class_Code', 12143, null, 1973);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Temp_Related_Asset', 12144, null, 1974);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Transition', 12145, null, 1975);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Transition_Mass', 12146, null, 1976);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Type', 12147, null, 1977);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_WAM_Rate_Calc', 12148, null, 1978);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Wam_Rate_Calc_Detailed', 12149, null, 1979);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\ARO_Work_Order', 12150, null, 1980);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Asset_Acct_Method', 12151, null, 1981);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Asset_Location', 12152, null, 1982);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Basis_Amounts', 12153, null, 1983);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Basis_Amounts_FP', 12154, null, 1984);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Bill_Material_Status', 12155, null, 1985);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Bks_Schem_Set_Bks_Not_used_in_V10_2_1_', 12156, null, 1986);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Blend_Depr_Data', 12157, null, 1987);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Book_Alloc_Assign', 12158, null, 1988);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Book_Alloc_Group', 12159, null, 1989);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Book_Alloc_Retire_Process', 12160, null, 1990);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Book_Alloc_Retirements', 12161, null, 1991);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Book_Schema_Not_used_in_V10_2_1_', 12162, null, 1992);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Book_Summary', 12163, null, 1993);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Bud_Summary_Bud_Plant_Class', 12164, null, 1994);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget', 12165, null, 1995);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Actuals_Temp_Global_Temp_Table_', 12166, null, 1996);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_AFUDC_Calc', 12167, null, 1997);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_AFUDC_Calc_Beg_Bal', 12168, null, 1998);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_AFUDC_Calc_Beg_Bal_Temp', 12169, null, 1999);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_AFUDC_Calc_Closing', 12170, null, 2000);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_AFUDC_Calc_Closing_Temp', 12171, null, 2001);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_AFUDC_Calc_Temp', 12172, null, 2002);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_AFUDC_Defaults', 12173, null, 2003);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Amounts', 12174, null, 2004);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Amounts_Arch', 12175, null, 2005);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval', 12176, null, 2006);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval_Arch', 12177, null, 2007);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval_Data', 12178, null, 2008);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval_Data_Arch', 12179, null, 2009);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval_Detail', 12180, null, 2010);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval_Detail_Arch', 12181, null, 2011);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval_Levels', 12182, null, 2012);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Approval_Levels_Arch', 12183, null, 2013);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Cap_Interest_Calc', 12184, null, 2014);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Cap_Interest_WO', 12185, null, 2015);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Class_Code', 12186, null, 2016);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Class_Code_Arch', 12187, null, 2017);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Closings_Pct', 12188, null, 2018);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Closings_Pct_Temp', 12189, null, 2019);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Closings_Stage', 12190, null, 2020);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_CWIP_In_Rate_Base', 12191, null, 2021);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Delete', 12192, null, 2022);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Delete_Custom', 12193, null, 2023);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Document', 12194, null, 2024);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Escalations', 12195, null, 2025);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Escalations_Rates', 12196, null, 2026);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Escalations_Rates_Bk', 12197, null, 2027);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Fcst_Depr_Assign', 12198, null, 2028);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Fields', 12199, null, 2029);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Load_Departments', 12200, null, 2030);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Data', 12201, null, 2031);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Data_Arch', 12202, null, 2032);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Data_Cr', 12203, null, 2033);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Data_Escalation', 12204, null, 2034);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Data_Mo_Id', 12205, null, 2035);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Data_Trans_Temp', 12206, null, 2036);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Data_Transpose', 12207, null, 2037);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Spread', 12208, null, 2038);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Monthly_Spread_Arch', 12209, null, 2039);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_One_Button', 12210, null, 2040);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Org_Clear_Dflt_Bdg', 12211, null, 2041);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Organization', 12212, null, 2042);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Plant_Class', 12213, null, 2043);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Process_Control', 12214, null, 2044);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Reimb_Rates', 12215, null, 2045);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review', 12216, null, 2046);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review_Arch', 12217, null, 2047);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review_Defaults', 12218, null, 2048);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review_Detail', 12219, null, 2049);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review_Detail_Arch', 12220, null, 2050);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review_Level', 12221, null, 2051);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review_Type', 12222, null, 2052);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Review_Users', 12223, null, 2053);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Rollup', 12224, null, 2054);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Status', 12225, null, 2055);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Subs_Default_Type', 12226, null, 2056);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Substitutions', 12227, null, 2057);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Substitutions_Arch', 12228, null, 2058);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Summary', 12229, null, 2059);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Summary_Clear_Dflt_Bdg', 12230, null, 2060);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Summary_Data', 12231, null, 2061);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Summary_Rollup', 12232, null, 2062);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Type', 12233, null, 2063);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Version', 12234, null, 2064);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Version_Arch', 12235, null, 2065);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Version_Fund_Proj', 12236, null, 2066);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Version_Fund_Proj_Arch', 12237, null, 2067);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Version_Maint', 12238, null, 2068);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Version_Status', 12239, null, 2069);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_Version_Type', 12240, null, 2070);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_WIP_Comp_Calc', 12241, null, 2071);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_WIP_Comp_Calc_Temp', 12242, null, 2072);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_WIP_Comp_Extensions', 12243, null, 2073);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_WIP_Comp_Input', 12244, null, 2074);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Budget_WIP_Comp_Rate', 12245, null, 2075);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Business_Segment', 12246, null, 2076);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_FP_Respread_Audit', 12247, null, 2077);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking', 12248, null, 2078);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking_Attrib_Control', 12249, null, 2079);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking_Attributes', 12250, null, 2080);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking_Fields', 12251, null, 2081);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking_Filter_Control', 12252, null, 2082);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking_Filters', 12253, null, 2083);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking_Results', 12254, null, 2084);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\BV_Proj_Ranking_Thresholds', 12255, null, 2085);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Cap_Gain_Convention', 12256, null, 2086);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Cap_Int_Adjust_Temp', 12257, null, 2087);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Cap_Interest_Calc', 12258, null, 2088);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Cap_Interest_WO', 12259, null, 2089);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Collect_Control', 12260, null, 2090);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Error', 12261, null, 2091);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Format', 12262, null, 2092);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Group_Control', 12263, null, 2093);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Location', 12264, null, 2094);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Location_Level', 12265, null, 2095);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Location_Type', 12266, null, 2096);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Summary', 12267, null, 2097);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Summary_Closings', 12268, null, 2098);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Type', 12269, null, 2099);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Charge_Type_Data', 12270, null, 2100);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code', 12271, null, 2101);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_CPR_Ledger', 12272, null, 2102);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_Default', 12273, null, 2103);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_Display', 12274, null, 2104);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_Pending_Trans', 12275, null, 2105);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_Pending_Trans_Arc', 12276, null, 2106);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_Values', 12277, null, 2107);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_Values_Filter', 12278, null, 2108);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Class_Code_WO_Type', 12279, null, 2109);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Clearing_WO_Control', 12280, null, 2110);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Clearing_Wo_Control_Bdg', 12281, null, 2111);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Clearing_WO_Rate', 12282, null, 2112);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Clearing_Wo_Rate_Bdg', 12283, null, 2113);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Closing_Option', 12284, null, 2114);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Closing_Pattern', 12285, null, 2115);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Closing_Pattern_Data', 12286, null, 2116);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Co_Tenancy_Agreement', 12287, null, 2117);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Co_Tenancy_Partners', 12288, null, 2118);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Co_Tenancy_WO', 12289, null, 2119);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Combined_Depr_Group', 12290, null, 2120);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Combined_Depr_Res_Allo_Factors', 12291, null, 2121);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Commitment_Type', 12292, null, 2122);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Commitments', 12293, null, 2123);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Commitments_Tmp_Global_Temp_Table_', 12294, null, 2124);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Comp_Unit_Action_Code_Std', 12295, null, 2125);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Comp_Unit_Hierarchy', 12296, null, 2126);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Comp_Unit_Stock_Keeping_Unit', 12297, null, 2127);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Comp_Unit_Template', 12298, null, 2128);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Comp_Unit_Template_Rows', 12299, null, 2129);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Comp_Unit_Work_Type', 12300, null, 2130);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Account_Curves', 12301, null, 2131);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Approval_Auth_Level', 12302, null, 2132);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Bus_Segment_Control', 12303, null, 2133);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Closing_Rollup', 12304, null, 2134);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_GL_Account', 12305, null, 2135);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Group', 12306, null, 2136);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_JE_Method_Exclude', 12307, null, 2137);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Major_Location', 12308, null, 2138);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Property_Unit', 12309, null, 2139);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Set_of_Books', 12310, null, 2140);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Setup', 12311, null, 2141);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Sub_Account_Control', 12312, null, 2142);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Company_Summary', 12313, null, 2143);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Compatible_Unit', 12314, null, 2144);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Computed_Age_Distribution', 12315, null, 2145);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Conversion_Batch_Control', 12316, null, 2146);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Cost_Element', 12317, null, 2147);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Cost_of_Removal_Convention', 12318, null, 2148);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Country', 12319, null, 2149);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\County', 12320, null, 2150);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_106_OOB_WO', 12321, null, 2151);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Alloc_Error', 12322, null, 2152);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Asset_Allocation', 12323, null, 2153);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Base_Adjust', 12324, null, 2154);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Base_Control', 12325, null, 2155);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Calc', 12326, null, 2156);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Closings', 12327, null, 2157);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Closings_Temp', 12328, null, 2158);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_Compare', 12329, null, 2159);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_CPR_Assets', 12330, null, 2160);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Retro_WO_List_Temp', 12331, null, 2161);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPI_Wo_Compare', 12332, null, 2162);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Act_Basis', 12333, null, 2163);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Act_Basis_Post_Test', 12334, null, 2164);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Act_Depr_Group', 12335, null, 2165);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Act_Month_Global_Temp_Table_', 12336, null, 2166);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Activity', 12337, null, 2167);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Activity_Post_Test', 12338, null, 2168);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Class_Code_Import_Stg', 12339, null, 2169);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Class_Code_Import_Stg_Arc', 12340, null, 2170);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Company_Global_Temp_Table_', 12341, null, 2171);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Control', 12342, null, 2172);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Depr', 12343, null, 2173);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Depr_Calc_CfnuStg', 12344, null, 2174);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Depr_Calc_Nurv_Stg', 12345, null, 2175);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Depr_Calc_Stg', 12346, null, 2176);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Depr_Calc_Stg_Arc', 12347, null, 2177);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Activity', 12348, null, 2178);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Asset_Relate', 12349, null, 2179);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Asset_Relation', 12350, null, 2180);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Attribute_Define', 12351, null, 2181);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Attribute_Values', 12352, null, 2182);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Estimate_Relate', 12353, null, 2183);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Event', 12354, null, 2184);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Event_Attribute', 12355, null, 2185);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Event_Type', 12356, null, 2186);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Ledger', 12357, null, 2187);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Ledger_API', 12358, null, 2188);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Ledger_API_Archive', 12359, null, 2189);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Ledger_API_Temp_Global_Temp_Table_', 12360, null, 2190);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Ledger_Attribute', 12361, null, 2191);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Ledger_Doc', 12362, null, 2192);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Retire_Relate', 12363, null, 2193);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Type', 12364, null, 2194);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Type_Attribute', 12365, null, 2195);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Equip_Type_Event', 12366, null, 2196);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Impair_Calc_Temp', 12367, null, 2197);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Impair_Status', 12368, null, 2198);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Impairment', 12369, null, 2199);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Impairment_Alloc_Method', 12370, null, 2200);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Impairment_Event', 12371, null, 2201);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Impairment_Method', 12372, null, 2202);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ldg_Basis', 12373, null, 2203);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ldg_Basis_Post_Test', 12374, null, 2204);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ledger', 12375, null, 2205);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ledger_Act_Imp_Astid_Stg', 12376, null, 2206);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ledger_Act_Import_Aid_Stg', 12377, null, 2207);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ledger_Act_Import_Stg', 12378, null, 2208);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ledger_Act_Import_Stg_Arc', 12379, null, 2209);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ledger_Comment', 12380, null, 2210);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Ledger_Post_Test', 12381, null, 2211);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Memo_Activity', 12382, null, 2212);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CPR_Where_Clause', 12383, null, 2213);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Introduction2', 12384, null, 2214);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PowerPlan_Tables', 12385, null, 2215);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Special_Tables_and_System_Structure', 12386, null, 2216);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Table_Definitions', 12387, null, 2217);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Ab_Id', 12388, null, 2218);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Account_Type', 12389, null, 2219);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_All_Details_Combo_Validate', 12390, null, 2220);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_All_Details_Dr_Stg', 12391, null, 2221);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_All_Details_Dr_Stg_Arch', 12392, null, 2222);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_All_Details_Orig_Stg', 12393, null, 2223);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_All_Details_Orig_Stg_Arch', 12394, null, 2224);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Credit', 12395, null, 2225);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Credit_Bdg', 12396, null, 2226);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Credit_Criteria', 12397, null, 2227);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Credit_Criteria_Bdg', 12398, null, 2228);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Credit_Criteria_T_Bdg', 12399, null, 2229);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Credit_Criteria_Trpo', 12400, null, 2230);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Custom_DW', 12401, null, 2231);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Custom_DW_Bdg', 12402, null, 2232);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Group', 12403, null, 2233);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Group_Bdg', 12404, null, 2234);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Group_By', 12405, null, 2235);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Group_By_Bdg', 12406, null, 2236);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Interco', 12407, null, 2237);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Interco_Bdg', 12408, null, 2238);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Interco_Criteria', 12409, null, 2239);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Interco_Criteria_Bdg', 12410, null, 2240);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Interco_Criteria2', 12411, null, 2241);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Interco_Criteria2_Bdg', 12412, null, 2242);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Process_Control', 12413, null, 2243);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Process_Control_Bdg', 12414, null, 2244);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Process_Control2', 12415, null, 2245);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Process_Control2_Bdg', 12416, null, 2246);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Process_Control3', 12417, null, 2247);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Process_Control3_Bdg', 12418, null, 2248);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_System_Control', 12419, null, 2249);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Target', 12420, null, 2250);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Target_Bdg', 12421, null, 2251);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Target_Criteria', 12422, null, 2252);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Target_Criteria_Bdg', 12423, null, 2253);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Target_Criteria_T_Bdg', 12424, null, 2254);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Target_Criteria_Trpo', 12425, null, 2255);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Where', 12426, null, 2256);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Where_Bdg', 12427, null, 2257);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Where_Clause', 12428, null, 2258);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Alloc_Where_Clause_Bdg', 12429, null, 2259);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocation_Attachments', 12430, null, 2260);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocation_Budget_Values', 12431, null, 2261);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocation_Control', 12432, null, 2262);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocation_Control_Bdg', 12433, null, 2263);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocations', 12434, null, 2264);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocations_Stg', 12435, null, 2265);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocations_Test', 12436, null, 2266);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Allocations_Test_Stg', 12437, null, 2267);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Amount_Type', 12438, null, 2268);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Amount_Type_Book', 12439, null, 2269);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Amount_Type_Type', 12440, null, 2270);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Approval_Auth_Level', 12441, null, 2271);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Approval_Group', 12442, null, 2272);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Approval_Group_Users', 12443, null, 2273);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Approval_Status', 12444, null, 2274);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Approval_Type', 12445, null, 2275);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Approval_Type_Bdg', 12446, null, 2276);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Balances', 12447, null, 2277);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Balances_BDG', 12448, null, 2278);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Balances_Bdg_Ye_Close', 12449, null, 2279);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Balances_DD_Keys', 12450, null, 2280);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Balances_Id', 12451, null, 2281);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Balances_YE_Close', 12452, null, 2282);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Derivation_Company', 12453, null, 2283);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Derivation_Control', 12454, null, 2284);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Derivation_Dates_BDG', 12455, null, 2285);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Derivation_EXCL_WO', 12456, null, 2286);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Derivation_Exclusion', 12457, null, 2287);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Derivation_Queue', 12458, null, 2288);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Derivation_Results', 12459, null, 2289);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Batch_Labor_Cap_Dollars', 12460, null, 2290);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Active_Entry', 12461, null, 2291);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Additional_Fields', 12462, null, 2292);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_AFUDC_Calc', 12463, null, 2293);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_AFUDC_Wotype_Exclude', 12464, null, 2294);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Approval', 12465, null, 2295);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Approval_Auth_Level', 12466, null, 2296);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Approval_Type', 12467, null, 2297);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Attachments', 12468, null, 2298);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Calc_All_Control', 12469, null, 2299);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Calc_All_Template', 12470, null, 2300);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Component', 12471, null, 2301);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data', 12472, null, 2302);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Entry', 12473, null, 2303);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Entry1', 12474, null, 2304);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Labor', 12475, null, 2305);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Labor_Labels', 12476, null, 2306);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Labor_Monthly', 12477, null, 2307);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Labor_Spread', 12478, null, 2308);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Labor2', 12479, null, 2309);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Labor2_Define', 12480, null, 2310);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Lbr_Mnth_Rows', 12481, null, 2311);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Projects', 12482, null, 2312);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Temp_Global_Temp_Table_', 12483, null, 2313);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Data_Test', 12484, null, 2314);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Element_Criteria', 12485, null, 2315);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Element_Group_By', 12486, null, 2316);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Ent_Rate_Ovr', 12487, null, 2317);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Ent_Rate_Ovr_Element', 12488, null, 2318);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Ent_Rate_Ovr_Rates', 12489, null, 2319);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Entry_Gross_Up', 12490, null, 2320);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Esc_Type', 12491, null, 2321);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Esc_Type_BV', 12492, null, 2322);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Esc_Type_Orig', 12493, null, 2323);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Esc_Type_Rate', 12494, null, 2324);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Esc_Type_Where', 12495, null, 2325);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Filter_Criteria', 12496, null, 2326);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Fixed_Factor', 12497, null, 2327);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Groups', 12498, null, 2328);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Groups_HR', 12499, null, 2329);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_HR_Groups_Security', 12500, null, 2330);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_HR_Users_Grps', 12501, null, 2331);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Labor_Lt_to_Ect', 12502, null, 2332);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Labor_Monthly_Update', 12503, null, 2333);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Labor_Position', 12504, null, 2334);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Labor_Position_Rates', 12505, null, 2335);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Labor_Resource', 12506, null, 2336);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Labor_Resource_Order', 12507, null, 2337);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Rate_Type', 12508, null, 2338);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Rate_Type_Element', 12509, null, 2339);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Rate_Type_Rates', 12510, null, 2340);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Rate_Type_Template', 12511, null, 2341);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Report_Data_Source', 12512, null, 2342);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Reversals', 12513, null, 2343);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Spread_Factor', 12514, null, 2344);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Step1', 12515, null, 2345);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Step1_Calculations', 12516, null, 2346);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Step1_Justification', 12517, null, 2347);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Step2', 12518, null, 2348);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Submitted', 12519, null, 2349);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Sup_Column_Control', 12520, null, 2350);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Sup_Data_Global_Temp_Table_', 12521, null, 2351);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Sup_Where_Clause', 12522, null, 2352);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates', 12523, null, 2353);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Alloc', 12524, null, 2354);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Appr_Type', 12525, null, 2355);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Budget_By', 12526, null, 2356);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_BV', 12527, null, 2357);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Computed', 12528, null, 2358);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Dtl_Fields', 12529, null, 2359);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Fields', 12530, null, 2360);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Group_By', 12531, null, 2361);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Groups', 12532, null, 2362);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Structure', 12533, null, 2363);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Templates_Users_Grps', 12534, null, 2364);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Users_Dept', 12535, null, 2365);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Users_Valid_Values', 12536, null, 2366);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Value_Type', 12537, null, 2367);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Value_Type_Template', 12538, null, 2368);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Version', 12539, null, 2369);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Version_Security', 12540, null, 2370);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Version_Security_All', 12541, null, 2371);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Where', 12542, null, 2372);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Where_Apply_to_Year', 12543, null, 2373);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Where_Clause', 12544, null, 2374);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Budget_Where_Copy_Fields', 12545, null, 2375);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cancel_Additional_Fields', 12546, null, 2376);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cancel_Control', 12547, null, 2377);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cancel_Detail', 12548, null, 2378);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cancel_Exceptions', 12549, null, 2379);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cancel_Header', 12550, null, 2380);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cancel_Results', 12551, null, 2381);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Commitments_Field_Map', 12552, null, 2382);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Commitments_Sum', 12553, null, 2383);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Commitments_Translate', 12554, null, 2384);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Commitments_Validations', 12555, null, 2385);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Company', 12556, null, 2386);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Company_Security', 12557, null, 2387);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Company_Summary', 12558, null, 2388);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cost_Repository', 12559, null, 2389);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_CR_ID', 12560, null, 2390);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Crosstab_Source_DW', 12561, null, 2391);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_CWIP_Charge_Drill', 12562, null, 2392);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cwip_Charge_Sum', 12563, null, 2393);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Cwip_Charge_Translate', 12564, null, 2394);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover', 12565, null, 2395);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Database', 12566, null, 2396);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_DBMS', 12567, null, 2397);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Filter', 12568, null, 2398);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Filter_Clause', 12569, null, 2399);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Map', 12570, null, 2400);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Map_Columns', 12571, null, 2401);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Module', 12572, null, 2402);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Module_Column', 12573, null, 2403);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Module_Join', 12574, null, 2404);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Data_Mover_Module_Table', 12575, null, 2405);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_DD_Query_Default', 12576, null, 2406);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_DD_Required_Filter_Global_Temp_Table_', 12577, null, 2407);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_DD_Sources_Criteria', 12578, null, 2408);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_DD_Sources_Criteria_Fields', 12579, null, 2409);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Debit_Credit', 12580, null, 2410);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Derivation_Rollup', 12581, null, 2411);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Derivation_Rollup_Priority', 12582, null, 2412);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Derivation_Trueup', 12583, null, 2413);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Derivation_Trueup_Stg', 12584, null, 2414);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Derivation_Type_Security', 12585, null, 2415);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Additional_Fields', 12586, null, 2416);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Additional_Fields_B', 12587, null, 2417);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Control', 12588, null, 2418);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Control_Virtual_Global_Temp_Table_', 12589, null, 2419);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_OR2_Sources_Alloc', 12590, null, 2420);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_OR2_Sources_AllocB', 12591, null, 2421);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Override', 12592, null, 2422);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Override2', 12593, null, 2423);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Override2_Sources', 12594, null, 2424);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Type_Sources', 12595, null, 2425);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Type_Sources_Alloc', 12596, null, 2426);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Type_Sources_AllocB', 12597, null, 2427);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_Type_Sources_Budget', 12598, null, 2428);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Deriver_WO_Link_Control', 12599, null, 2429);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Distrib_Groups', 12600, null, 2430);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Distrib_Queries_Groups', 12601, null, 2431);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Distrib_Users_Groups', 12602, null, 2432);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Drilldown_Keys', 12603, null, 2433);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Elements', 12604, null, 2434);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Elements_Fields', 12605, null, 2435);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_FERC_Allocation_Control', 12606, null, 2436);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports', 12607, null, 2437);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Build', 12608, null, 2438);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Columns', 12609, null, 2439);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Data', 12610, null, 2440);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Dol_Type', 12611, null, 2441);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Filters', 12612, null, 2442);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Formulas', 12613, null, 2443);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Formulas2', 12614, null, 2444);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Linked', 12615, null, 2445);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Run', 12616, null, 2446);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Run_ARC', 12617, null, 2447);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Run2_Global_Temp_Table_', 12618, null, 2448);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Setup', 12619, null, 2449);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Setup_Lbl', 12620, null, 2450);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Financial_Reports_Spec_Char', 12621, null, 2451);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_GL_ID', 12622, null, 2452);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_GL_Journal_Category', 12623, null, 2453);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Inter_Company', 12624, null, 2454);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Inter_Company_Test', 12625, null, 2455);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Interco_Balancing', 12626, null, 2456);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Interface_Attachments', 12627, null, 2457);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Interface_Dates', 12628, null, 2458);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Interface_Month_Period', 12629, null, 2459);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Interfaces', 12630, null, 2460);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_JE_Import_Templates', 12631, null, 2461);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_JE_Import_Templates_Data', 12632, null, 2462);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Journal_Approval', 12633, null, 2463);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Journal_Attachments', 12634, null, 2464);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Journal_Batches', 12635, null, 2465);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Journal_Lines', 12636, null, 2466);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Journal_Lines_Stg', 12637, null, 2467);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Journals', 12638, null, 2468);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Manual_Commitment_Elem_Hide', 12639, null, 2469);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Manual_JE_Element_Hide', 12640, null, 2470);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Manual_JE_Recurring_Months', 12641, null, 2471);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Manual_JE_Templates', 12642, null, 2472);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Manual_JE_Templates_Data', 12643, null, 2473);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Master_Element_Autoextend', 12644, null, 2474);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Master_Element_Format', 12645, null, 2475);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Month_Number', 12646, null, 2476);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Month_Number_Bdg', 12647, null, 2477);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Open_Month_Number', 12648, null, 2478);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Post_to_GL_Columns', 12649, null, 2479);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Post_To_GL_Columns_Bdg', 12650, null, 2480);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Post_to_GL_Control', 12651, null, 2481);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Post_To_GL_Control_Bdg', 12652, null, 2482);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Posting_Approval', 12653, null, 2483);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Posting_Approval_Bdg', 12654, null, 2484);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Posting_Approval_Ids', 12655, null, 2485);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Posting_Approval_Ids_Bdg', 12656, null, 2486);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_PowerPlant', 12657, null, 2487);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_PowerPlant_Stg', 12658, null, 2488);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_PowerTax', 12659, null, 2489);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_PowerTax_Stg', 12660, null, 2490);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_PPtoCR_DA_Mapping', 12661, null, 2491);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Project_Approval', 12662, null, 2492);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Project_Approval_Ids', 12663, null, 2493);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Query_Reporting_Control', 12664, null, 2494);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Query_Scalar_Select', 12665, null, 2495);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Rates', 12666, null, 2496);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Rates_Input', 12667, null, 2497);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Recon', 12668, null, 2498);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Recon_Filter', 12669, null, 2499);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Recon_JE_Setup', 12670, null, 2500);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Recon_Sources', 12671, null, 2501);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Recon_Sources_Fields', 12672, null, 2502);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Recon_Where', 12673, null, 2503);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Recon_Where_Clause', 12674, null, 2504);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Record_Count', 12675, null, 2505);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Reversals_Manual', 12676, null, 2506);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Reversals_Manual_Stg', 12677, null, 2507);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SAP_Posting_To_Order', 12678, null, 2508);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SAP_Trueup_Company', 12679, null, 2509);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SAP_Trueup_Control', 12680, null, 2510);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SAP_Trueup_Eligible_Accts', 12681, null, 2511);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SAP_Trueup_Exclusion', 12682, null, 2512);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SAP_Trueup_Exclusion_WO', 12683, null, 2513);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Saved_Queries', 12684, null, 2514);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Saved_Queries_Data', 12685, null, 2515);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SCO_Billing_Type', 12686, null, 2516);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_SCO_Billing_Type_Rates', 12687, null, 2517);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sources', 12688, null, 2518);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sources_Fields', 12689, null, 2519);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Special_Processing', 12690, null, 2520);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Special_Processing_Bdg', 12691, null, 2521);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Special_Processing_Dates', 12692, null, 2522);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Special_Processing_Dates_BD', 12693, null, 2523);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Special_Processing_Dates2', 12694, null, 2524);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Special_Processing_Dates2_B', 12695, null, 2525);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Structure_Security', 12696, null, 2526);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Structure_Values', 12697, null, 2527);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Structure_Values2', 12698, null, 2528);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Structure_Values2_Bdg_Temp_Global_Temp_Table_', 12699, null, 2529);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Structures', 12700, null, 2530);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Structures_Flattened', 12701, null, 2531);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Structures_Flattened2', 12702, null, 2532);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sum', 12703, null, 2533);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sum_AB', 12704, null, 2534);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sum_AB_History', 12705, null, 2535);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sum_AB_Temp', 12706, null, 2536);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sum_AB_Temp_1', 12707, null, 2537);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sum_Dollar_Columns', 12708, null, 2538);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Sum_Hist', 12709, null, 2539);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Suspense_Account', 12710, null, 2540);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_System_Control', 12711, null, 2541);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_Global_Temp_Table_', 12712, null, 2542);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_Allocations_Global_Temp_Table_', 12713, null, 2543);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_Commitment_Global_Temp_Table_', 12714, null, 2544);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_GL_Global_Temp_Table_', 12715, null, 2545);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_GL_Bdg_Global_Temp_Table_', 12716, null, 2546);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_GL_Bdg_Temp_Global_Temp_Table_', 12717, null, 2547);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_GL_IDS', 12718, null, 2548);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_GL_IDS_Bdg', 12719, null, 2549);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_GL_Temp_Global_Temp_Table_', 12720, null, 2550);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_JE_Global_Temp_Table_', 12721, null, 2551);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_JE_Temp', 12722, null, 2552);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Temp_CR_Provision', 12723, null, 2553);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Bdg_To_CR_Fcst_Amt', 12724, null, 2554);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Budget_To_CR_Audit', 12725, null, 2555);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Budget_To_CR_Columns', 12726, null, 2556);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Budget_To_CR_Control', 12727, null, 2557);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_CC_Detail_Tables', 12728, null, 2558);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Comm_Control', 12729, null, 2559);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Comm_Detail_Tables', 12730, null, 2560);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Comm_Table_List', 12731, null, 2561);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Comm_Translate', 12732, null, 2562);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Comm_Translate_Clause', 12733, null, 2563);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Cwip_Control', 12734, null, 2564);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Cwip_Table_List', 12735, null, 2565);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Cwip_Translate', 12736, null, 2566);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_To_Cwip_Translate_Clause', 12737, null, 2567);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Acct_Range', 12738, null, 2568);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Acct_Range_Excl', 12739, null, 2569);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Combos', 12740, null, 2570);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Combos_Bdg', 12741, null, 2571);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Combos_Staging', 12742, null, 2572);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Combos_STG_Bdg', 12743, null, 2573);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Control', 12744, null, 2574);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Control_Bdg', 12745, null, 2575);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Exclusion', 12746, null, 2576);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Rules', 12747, null, 2577);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Rules_Bdg', 12748, null, 2578);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Rules_Hints', 12749, null, 2579);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validation_Rules_Projects', 12750, null, 2580);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validations_Invalid_IDS', 12751, null, 2581);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validations_Invalid_IDS2', 12752, null, 2582);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validations_Invalid_IDS2A_Global_Temp_Table_', 12753, null, 2583);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_Validations_Invalid_IDS3', 12754, null, 2584);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_WO_Clear_Global_Temp_Table_', 12755, null, 2585);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CR_WO_Clear_Bdg', 12756, null, 2586);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CRB_Budget_Data', 12757, null, 2587);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Crew_Type', 12758, null, 2588);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Crew_Type_Rate_Hist', 12759, null, 2589);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Currency', 12760, null, 2590);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Currency_Rate', 12761, null, 2591);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Currency_Rate_Type', 12762, null, 2592);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Currency_Schema', 12763, null, 2593);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Currency_Type', 12764, null, 2594);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Custom_Validations_Info_Tmp_Global_Temp_Table_', 12765, null, 2595);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CWIP_Charge', 12766, null, 2596);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\CWIP_in_Rate_Base', 12767, null, 2597);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Data_Account_Class', 12768, null, 2598);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Def_Income_Tax_Rates_Version', 12769, null, 2599);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Deferred_Income_Tax', 12770, null, 2600);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Deferred_Income_Tax_FP', 12771, null, 2601);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Deferred_Income_Tax_Rates', 12772, null, 2602);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Deferred_Income_Tax_Transfer', 12773, null, 2603);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Deferred_Rates', 12774, null, 2604);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Deferred_Tax_Assign', 12775, null, 2605);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Deferred_Tax_Schema', 12776, null, 2606);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Department', 12777, null, 2607);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Activity', 12778, null, 2608);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Activity_Code', 12779, null, 2609);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Activity_Recurring', 12780, null, 2610);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Beg_Bals', 12781, null, 2611);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Blending_Type', 12782, null, 2612);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Blending_Type_Control', 12783, null, 2613);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Amort_Arc', 12784, null, 2614);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Amort_Stg', 12785, null, 2615);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Combined_Arc', 12786, null, 2616);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Combined_Stg', 12787, null, 2617);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Over_Stg', 12788, null, 2618);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Over_Stg_Arc', 12789, null, 2619);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Process_Control', 12790, null, 2620);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Stg', 12791, null, 2621);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Calc_Stg_Arc', 12792, null, 2622);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Combined_Group_Books', 12793, null, 2623);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_CPR_Depr_Import_Stg', 12794, null, 2624);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_CPR_Depr_Import_Stg_Arc', 12795, null, 2625);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Group', 12796, null, 2626);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Group_Control', 12797, null, 2627);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Group_Jur_Allo', 12798, null, 2628);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Group_UOP', 12799, null, 2629);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Ledger', 12800, null, 2630);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Ledger_Blending', 12801, null, 2631);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Ledger_Blending_Stg', 12802, null, 2632);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Ledger_Blending_Stg_Arc', 12803, null, 2633);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Ledger_Import_Stg', 12804, null, 2634);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Ledger_Import_Stg_Arc', 12805, null, 2635);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Method_Blending', 12806, null, 2636);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Method_Rates', 12807, null, 2637);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Method_Rates_Import_Stg', 12808, null, 2638);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Method_Rates_Import_Stg_Arc', 12809, null, 2639);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Method_Schedule', 12810, null, 2640);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Method_Sle', 12811, null, 2641);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Method_UOP', 12812, null, 2642);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Mid_Period_Method', 12813, null, 2643);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Net_Salvage_Amort', 12814, null, 2644);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Post_Activity_Temp', 12815, null, 2645);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Process_Temp', 12816, null, 2646);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Rates_Comment', 12817, null, 2647);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Res_Allo_Factors', 12818, null, 2648);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Subledger_Basis', 12819, null, 2649);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Subledger_Template', 12820, null, 2650);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Summary', 12821, null, 2651);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Summary2', 12822, null, 2652);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Trans_Allo_Method', 12823, null, 2653);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Trans_Set', 12824, null, 2654);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Trans_Set_Dg', 12825, null, 2655);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Trans_Type', 12826, null, 2656);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depr_Vintage_Summary', 12827, null, 2657);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Depreciation_Method', 12828, null, 2658);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Dept_Template', 12829, null, 2659);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Dept_Template_Assign', 12830, null, 2660);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DFIT_Forecast_Output', 12831, null, 2661);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Disposition_Code', 12832, null, 2662);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DIT_Rate_Version', 12833, null, 2663);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Division', 12834, null, 2664);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Document_Stage', 12835, null, 2665);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Acct_Dataset_Map', 12836, null, 2666);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Activity_Translate', 12837, null, 2667);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Analysis_Dataset', 12838, null, 2668);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Company_Control', 12839, null, 2669);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Data_Account', 12840, null, 2670);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Data_Account_Control', 12841, null, 2671);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Data_Account_Depr_Group', 12842, null, 2672);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Data_Transaction', 12843, null, 2673);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Dataset_Company', 12844, null, 2674);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Dataset_External_Trans', 12845, null, 2675);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Dataset_Rsrv_Transaction', 12846, null, 2676);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Dataset_Transaction', 12847, null, 2677);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_DB_Import_Acct_Map', 12848, null, 2678);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_DB_Import_Code_Map', 12849, null, 2679);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_DB_Import_Plant', 12850, null, 2680);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_DB_Import_Rsrv', 12851, null, 2681);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_DB_Import_Variables', 12852, null, 2682);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_DG_Dataset_Map', 12853, null, 2683);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\DS_Rsrv_Transaction', 12854, null, 2684);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Element_Table_Template', 12855, null, 2685);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Eng_Estimate_Status', 12856, null, 2686);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Est_Salvage_Convention', 12857, null, 2687);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Estimate_Charge_Type', 12858, null, 2688);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Estimate_Curve_Factors', 12859, null, 2689);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Estimate_Curves', 12860, null, 2690);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Estimate_Overhead_Base', 12861, null, 2691);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Estimate_Overhead_Rates', 12862, null, 2692);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Estimate_Upload_Customize', 12863, null, 2693);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Exp_Type_Est_Chg_Type', 12864, null, 2694);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Expenditure_Type', 12865, null, 2695);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budg_CPR_Depr_Asset_Temp', 12866, null, 2696);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budg_CPR_Depr_Clos_Fp_Tmp', 12867, null, 2697);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budg_CPR_Depr_Closing_Temp', 12868, null, 2698);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budg_Depr_Clos_Fp_Temp', 12869, null, 2699);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budg_Depr_Closing_Temp', 12870, null, 2700);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budget_Load', 12871, null, 2701);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budget_Load_Additions', 12872, null, 2702);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budget_Load_Additions_Fp', 12873, null, 2703);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Budget_Load_Arch', 12874, null, 2704);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Combined_Depr_Group', 12875, null, 2705);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Control', 12876, null, 2706);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_CPR_Depr', 12877, null, 2707);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_CPR_Depr_FP', 12878, null, 2708);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_CPR_Depr_Temp', 12879, null, 2709);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_CPR_Depr_Temp1', 12880, null, 2710);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Def_Tax_Rate', 12881, null, 2711);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Calc_Amort_Arc', 12882, null, 2712);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Calc_Combined_Arc', 12883, null, 2713);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Calc_Stg_Arc', 12884, null, 2714);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Group', 12885, null, 2715);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Group_UOP', 12886, null, 2716);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Group_Version', 12887, null, 2717);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Group_XLAT', 12888, null, 2718);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Ledger', 12889, null, 2719);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Ledger_Annual', 12890, null, 2720);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Ledger_Annual_Temp', 12891, null, 2721);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Ledger_Blend_Stg_Arc', 12892, null, 2722);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Ledger_Blending', 12893, null, 2723);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Ledger_FP', 12894, null, 2724);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Method_Blending', 12895, null, 2725);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Method_Rates', 12896, null, 2726);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Method_UOP', 12897, null, 2727);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Res_Allo_Factors', 12898, null, 2728);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Subledger_Template', 12899, null, 2729);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Version', 12900, null, 2730);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Vintage_Input', 12901, null, 2731);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depr_Vintage_Summary', 12902, null, 2732);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Depreciation_Method', 12903, null, 2733);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_DVS_Temp', 12904, null, 2734);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_GL_Transaction', 12905, null, 2735);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Method_Retire_Pct', 12906, null, 2736);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Net_Salvage_Amort', 12907, null, 2737);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Rate_Recalc_Temp', 12908, null, 2738);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Rate_Recalc2_Temp', 12909, null, 2739);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Subledger_Template', 12910, null, 2740);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Tax_Depr', 12911, null, 2741);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Tax_Depr_Rate', 12912, null, 2742);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Tax_Method', 12913, null, 2743);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Unit_of_Production', 12914, null, 2744);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fcst_Version_Set_of_Books', 12915, null, 2745);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\FERC_Activity_Code', 12916, null, 2746);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\FERC_Plant_Account', 12917, null, 2747);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\FERC_Report', 12918, null, 2748);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\FERC_Sys_of_Accts', 12919, null, 2749);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fit_Statistics', 12920, null, 2750);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Fit_Stats_All', 12921, null, 2751);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Forecasted_Retirements', 12922, null, 2752);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Func_Class', 12923, null, 2753);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Func_Class_Loc_Type', 12924, null, 2754);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Func_Class_Prop_Grp', 12925, null, 2755);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Funding_Justification', 12926, null, 2756);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Table_Definitions1', 12927, null, 2757);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Gain_Loss_Convention', 12928, null, 2758);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Generation_Arrangement', 12929, null, 2759);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Generation_Summary', 12930, null, 2760);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\GL_Account', 12931, null, 2761);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\GL_Acct_Bus_Segment', 12932, null, 2762);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\GL_JE_Control', 12933, null, 2763);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\GL_Trans_Status', 12934, null, 2764);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\GL_Transaction', 12935, null, 2765);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Handy_Whitman_Index', 12936, null, 2766);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Handy_Whitman_Rates', 12937, null, 2767);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Handy_Whitman_Region', 12938, null, 2768);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Hrs_Qty_Control', 12939, null, 2769);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Instruction', 12940, null, 2770);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\JE_Method', 12941, null, 2771);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\JE_Method_Set_of_Books', 12942, null, 2772);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\JE_Method_Trans_Type', 12943, null, 2773);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\JE_Trans_Type', 12944, null, 2774);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task', 12945, null, 2775);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Class_Code', 12946, null, 2776);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Default', 12947, null, 2777);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Document', 12948, null, 2778);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Group', 12949, null, 2779);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Interface_Staging', 12950, null, 2780);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Interface_Staging_ARC', 12951, null, 2781);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_List', 12952, null, 2782);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Mass_Update', 12953, null, 2783);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Priority_Code', 12954, null, 2784);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Setup_CC_Default', 12955, null, 2785);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Setup_Template', 12956, null, 2786);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Status', 12957, null, 2787);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_System', 12958, null, 2788);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Template', 12959, null, 2789);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Template_Control', 12960, null, 2790);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Job_Task_Template_WO_Type', 12961, null, 2791);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Activity', 12962, null, 2792);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Activity_Code', 12963, null, 2793);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Afc_Shadow_Mapping', 12964, null, 2794);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Afc_Shadow_WO', 12965, null, 2795);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Allo', 12966, null, 2796);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Allo_Book', 12967, null, 2797);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Allo_Book_Description', 12968, null, 2798);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Allo_Version', 12969, null, 2799);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Allo_Version_Control', 12970, null, 2800);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jur_Results', 12971, null, 2801);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Jurisdiction', 12972, null, 2802);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Life_Analysis_Parameters', 12973, null, 2803);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Location_Type', 12974, null, 2804);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Ls_Accrual_Type', 12975, null, 2805);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Ls_Ap_Status', 12976, null, 2806);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset', 12977, null, 2807);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Class_Code', 12978, null, 2808);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Component', 12979, null, 2809);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Document', 12980, null, 2810);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Loc_Local_Tax_District', 12981, null, 2811);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Local_Tax', 12982, null, 2812);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Schedule', 12983, null, 2813);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Status', 12984, null, 2814);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Asset_Tax_Map', 12985, null, 2815);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Cancelable_Type', 12986, null, 2816);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Column_Usage', 12987, null, 2817);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Component_Charge', 12988, null, 2818);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Component_Status', 12989, null, 2819);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_CPR_Asset_Map', 12990, null, 2820);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Extended_Rental_Type', 12991, null, 2821);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR', 12992, null, 2822);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Account', 12993, null, 2823);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Amounts_Set_of_Books', 12994, null, 2824);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Approval', 12995, null, 2825);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Asset_Map', 12996, null, 2826);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Asset_Schedule_Calc_Stg', 12997, null, 2827);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Asset_Schedule_Stg', 12998, null, 2828);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Asset_Stg', 12999, null, 2829);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Class_Code', 13000, null, 2830);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Document', 13001, null, 2831);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Group', 13002, null, 2832);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Group_Floating_Rates', 13003, null, 2833);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Options', 13004, null, 2834);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Payment_Term', 13005, null, 2835);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Schedule', 13006, null, 2836);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Schedule_Bucket_Stg', 13007, null, 2837);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Schedule_Stg', 13008, null, 2838);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Status', 13009, null, 2839);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_ILR_Stg', 13010, null, 2840);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Asset', 13011, null, 2841);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Asset_Archive', 13012, null, 2842);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_ILR', 13013, null, 2843);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_ILR_Archive', 13014, null, 2844);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Invoice', 13015, null, 2845);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Invoice_Archive', 13016, null, 2846);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Lease', 13017, null, 2847);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Lease_Archive', 13018, null, 2848);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Lessor', 13019, null, 2849);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Import_Lessor_Archive', 13020, null, 2850);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Invoice', 13021, null, 2851);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Invoice_Line', 13022, null, 2852);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Invoice_Payment_Map', 13023, null, 2853);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease', 13024, null, 2854);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Approval', 13025, null, 2855);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Company', 13026, null, 2856);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Document', 13027, null, 2857);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Group', 13028, null, 2858);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Group_User', 13029, null, 2859);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Options', 13030, null, 2860);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Status', 13031, null, 2861);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Type', 13032, null, 2862);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Type_Jes_Exclude', 13033, null, 2863);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Type_Tolerance', 13034, null, 2864);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lease_Vendor', 13035, null, 2865);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Lessor', 13036, null, 2866);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Mec_Config', 13037, null, 2867);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_MLA_Class_Code', 13038, null, 2868);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Monthly_Accrual_Stg', 13039, null, 2869);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Monthly_Depr_Stg', 13040, null, 2870);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Monthly_Tax_Stg', 13041, null, 2871);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Payment_Approval', 13042, null, 2872);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Payment_Freq', 13043, null, 2873);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Payment_HDR', 13044, null, 2874);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Payment_Line', 13045, null, 2875);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Payment_Term_Type', 13046, null, 2876);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Payment_Type', 13047, null, 2877);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Basis', 13048, null, 2878);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Basis_Arc', 13049, null, 2879);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Class_Code', 13050, null, 2880);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Class_Code_Arc', 13051, null, 2881);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Set_of_Books', 13052, null, 2882);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Set_of_Books_Arc', 13053, null, 2883);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Transaction', 13054, null, 2884);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Pend_Transaction_Arc', 13055, null, 2885);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Post_CPR_Find', 13056, null, 2886);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Process_Control', 13057, null, 2887);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Purchase_Option_Type', 13058, null, 2888);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Rebuild_Acct_Summ', 13059, null, 2889);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Renewal_Option_Type', 13060, null, 2890);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Rent_Bucket_Admin', 13061, null, 2891);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Retirement_New_Terms', 13062, null, 2892);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Retirement_Status', 13063, null, 2893);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Tax_District_Rates', 13064, null, 2894);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Table_Definitions2', 13065, null, 2895);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Tax_Local', 13066, null, 2896);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Tax_Rate_Option', 13067, null, 2897);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Tax_State_Rates', 13068, null, 2898);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Tax_Summary', 13069, null, 2899);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\LS_Vendor', 13070, null, 2900);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Major_Location', 13071, null, 2901);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Control', 13072, null, 2902);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Criteria', 13073, null, 2903);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Criteria_Lines', 13074, null, 2904);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Datadef', 13075, null, 2905);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Datadef_Sources', 13076, null, 2906);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Map_To_As_Built', 13077, null, 2907);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Max_Source_Ids', 13078, null, 2908);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Non_Table_Customize', 13079, null, 2909);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Sources', 13080, null, 2910);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Sources_Criteria', 13081, null, 2911);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Sources_Fields', 13082, null, 2912);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Trans', 13083, null, 2913);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Matlrec_Trans_Stg', 13084, null, 2914);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Minor_Location', 13085, null, 2915);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Monthly_Estimate_Mode', 13086, null, 2916);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Mortality_Curve', 13087, null, 2917);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Mortality_Curve_Points', 13088, null, 2918);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Mortality_Curve_Ret_Points', 13089, null, 2919);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Mortality_Memory', 13090, null, 2920);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Mortality_Memory_Post_Test', 13091, null, 2921);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Municipality', 13092, null, 2922);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\NARUC_Plant_Account', 13093, null, 2923);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\NARUC_Version', 13094, null, 2924);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Norm_Type', 13095, null, 2925);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Normalization_Pct', 13096, null, 2926);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Normalization_Schema', 13097, null, 2927);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\OH_Alloc_Basis', 13098, null, 2928);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\OH_Alloc_Basis_Bdg', 13099, null, 2929);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Original_Cost_Retirement', 13100, null, 2930);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Original_Cost_Retirement_Stg', 13101, null, 2931);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Overhead_Basis', 13102, null, 2932);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Overhead_Basis_Bdg', 13103, null, 2933);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_ARO_Reg_Activity', 13104, null, 2934);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_ARO_Reg_Activity_Arc', 13105, null, 2935);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Basis', 13106, null, 2936);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Basis_Archive', 13107, null, 2937);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Basis_Post_Test', 13108, null, 2938);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Basis_Temp', 13109, null, 2939);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Depr_Activity', 13110, null, 2940);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Equip', 13111, null, 2941);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Equip_Archive', 13112, null, 2942);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Related_Asset', 13113, null, 2943);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Subledger_Basis', 13114, null, 2944);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Subledger_Basis_Archive', 13115, null, 2945);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Subledger_Entry', 13116, null, 2946);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Subledger_Entry_Archive', 13117, null, 2947);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction', 13118, null, 2948);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction_Archive', 13119, null, 2949);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction_Memo', 13120, null, 2950);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction_Memo_Archive', 13121, null, 2951);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction_Post_Test', 13122, null, 2952);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction_Set_of_Books', 13123, null, 2953);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction_SOB_ARC', 13124, null, 2954);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Pend_Transaction_Temp', 13125, null, 2955);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Plan_Table', 13126, null, 2956);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Posting_Status', 13127, null, 2957);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PowerPlan_Columns', 13128, null, 2958);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PowerPlan_DDDW', 13129, null, 2959);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PowerPlan_Tables1', 13130, null, 2960);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PowerPlan_Template', 13131, null, 2961);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_ADSI_Attributes', 13132, null, 2962);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_ADSI_Directory_Path', 13133, null, 2963);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_ADSI_Map_Attributes', 13134, null, 2964);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Alert_Process_Log', 13135, null, 2965);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Alert_Processes', 13136, null, 2966);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Amt_Format', 13137, null, 2967);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Any_Query_Criteria', 13138, null, 2968);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Any_Query_Criteria_Fields', 13139, null, 2969);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Any_Required_Filter_Global_Temp_Table_', 13140, null, 2970);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Audit_DDL_Table', 13141, null, 2971);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Audit_Logon_Table', 13142, null, 2972);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Audit_Trail_Deletes', 13143, null, 2973);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Audits_Subsystem_Trail', 13144, null, 2974);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Jobs', 13145, null, 2975);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Program_Group', 13146, null, 2976);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Programs', 13147, null, 2977);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Queues', 13148, null, 2978);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Report_Args', 13149, null, 2979);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Report_Class', 13150, null, 2980);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Report_Format', 13151, null, 2981);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Report_Output', 13152, null, 2982);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Report_Printer', 13153, null, 2983);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Reports', 13154, null, 2984);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Reports_Data', 13155, null, 2985);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Reports_Log', 13156, null, 2986);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Rpts_Distr_Grps', 13157, null, 2987);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Rpts_Schedule', 13158, null, 2988);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Batch_Rpts_Viewed', 13159, null, 2989);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Calendar', 13160, null, 2990);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Charge_Split_Running', 13161, null, 2991);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Class_Code_Groups', 13162, null, 2992);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Client_Extensions', 13163, null, 2993);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Color_Lookup', 13164, null, 2994);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Company_Security', 13165, null, 2995);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Company_Security_Temp_Global_Temp_Table_', 13166, null, 2996);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Batches', 13167, null, 2997);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Batches_Class_Code', 13168, null, 2998);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_FERC_XLAT', 13169, null, 2999);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Mapping', 13170, null, 3000);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Ref_ID', 13171, null, 3001);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Stage', 13172, null, 3002);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Temp_Activity_Id', 13173, null, 3003);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Temp_Cost_Quantity', 13174, null, 3004);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Templates', 13175, null, 3005);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conv_Templates_Data', 13176, null, 3006);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Conversion_Windows', 13177, null, 3007);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Database_Object', 13178, null, 3008);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Datawindow_Excel', 13179, null, 3009);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Table_Definitions3', 13180, null, 3010);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Datawindow_Hints', 13181, null, 3011);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Drop_Constraints', 13182, null, 3012);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dyn_Filter_Saved_Values_DW', 13183, null, 3013);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Filter', 13184, null, 3014);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Filter_Mapping', 13185, null, 3015);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Filter_Restrictions', 13186, null, 3016);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Filter_Saved', 13187, null, 3017);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Filter_Saved_Values', 13188, null, 3018);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Filter_Values', 13189, null, 3019);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Filter_Values_DW', 13190, null, 3020);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Dynamic_Rpt_User_Saved', 13191, null, 3021);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Edit_Type', 13192, null, 3022);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Error_Message', 13193, null, 3023);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Flex_Names', 13194, null, 3024);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Flex_Names_MLS', 13195, null, 3025);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_GL_Trans_Temp', 13196, null, 3026);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_HtmlHelp', 13197, null, 3027);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Column', 13198, null, 3028);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Column_Lookup', 13199, null, 3029);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_File', 13200, null, 3030);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Lookup', 13201, null, 3031);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Run', 13202, null, 3032);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Subsystem', 13203, null, 3033);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Template', 13204, null, 3034);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Template_Edits', 13205, null, 3035);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Template_Fields', 13206, null, 3036);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Type', 13207, null, 3037);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Type_Subsystem', 13208, null, 3038);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Import_Type_Updates_Lookup', 13209, null, 3039);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Integration', 13210, null, 3040);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Interface', 13211, null, 3041);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Interface_Dates', 13212, null, 3042);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Interface_Groups', 13213, null, 3043);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_JL_Bind_Arg_Code', 13214, null, 3044);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Job_Request', 13215, null, 3045);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Journal_Data_SQL', 13216, null, 3046);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Journal_Keywords', 13217, null, 3047);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Journal_Layouts', 13218, null, 3048);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Journal_Layouts_Temp', 13219, null, 3049);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Journal_Types', 13220, null, 3050);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Languages', 13221, null, 3051);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_LDAP_Attributes', 13222, null, 3052);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_LDAP_Map_Attributes', 13223, null, 3053);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_LDAP_Profiles', 13224, null, 3054);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_LDAP_User', 13225, null, 3055);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_LDAP_User_Sync_Log', 13226, null, 3056);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Locale_ID', 13227, null, 3057);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mail_Data', 13228, null, 3058);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Manual_Interface', 13229, null, 3059);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Misc_Search', 13230, null, 3060);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Modules', 13231, null, 3061);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Alert_Dash_Columns', 13232, null, 3062);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Alert_Dashboard', 13233, null, 3063);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Alert_Dashboard_Page', 13234, null, 3064);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Approval_Control', 13235, null, 3065);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_MyPP_Metric_Dw', 13236, null, 3066);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Report_Type_Task', 13237, null, 3067);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Task', 13238, null, 3068);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Task_Step', 13239, null, 3069);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Tax_Report_Filter', 13240, null, 3070);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Time', 13241, null, 3071);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_Time_Report_Time', 13242, null, 3072);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Layout', 13243, null, 3073);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_MyPP_User_Metric_Dw', 13244, null, 3074);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Query', 13245, null, 3075);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Report', 13246, null, 3076);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Report_Parm', 13247, null, 3077);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Task', 13248, null, 3078);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_MyPP_User_Template', 13249, null, 3079);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Todo_Attachments', 13250, null, 3080);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Todo_List', 13251, null, 3081);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Mypp_User_Values', 13252, null, 3082);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Objects', 13253, null, 3083);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Objects_Copy', 13254, null, 3084);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Plsql_Debug_Enable', 13255, null, 3085);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Post_Lock', 13256, null, 3086);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Process_Error_Action', 13257, null, 3087);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes', 13258, null, 3088);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Documents', 13259, null, 3089);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Err_Info_Act', 13260, null, 3090);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Error_Info', 13261, null, 3091);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Error_Msg', 13262, null, 3092);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Info_Type', 13263, null, 3093);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Kickout_Columns', 13264, null, 3094);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Kickout_Control', 13265, null, 3095);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Kickout_Table', 13266, null, 3096);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Messages', 13267, null, 3097);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Occ_Attribute', 13268, null, 3098);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Occ_Error_Info', 13269, null, 3099);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Occ_Metric', 13270, null, 3100);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Occurrences', 13271, null, 3101);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Return_Values', 13272, null, 3102);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Processes_Running', 13273, null, 3103);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Query', 13274, null, 3104);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Query_DW', 13275, null, 3105);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Query_DW_CC_Values', 13276, null, 3106);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Query_DW_Cols', 13277, null, 3107);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Query_DW_Filter', 13278, null, 3108);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Query_Scalar_Keywords', 13279, null, 3109);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Query_Scalar_Select', 13280, null, 3110);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Record_Log', 13281, null, 3111);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Record_Log1', 13282, null, 3112);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Report_Filter', 13283, null, 3113);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Report_Filter_Columns', 13284, null, 3114);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Report_Fiscal_Year_Replace', 13285, null, 3115);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Report_Type', 13286, null, 3116);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports', 13287, null, 3117);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Args', 13288, null, 3118);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Company_Security', 13289, null, 3119);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Data', 13290, null, 3120);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Denomination', 13291, null, 3121);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Envir', 13292, null, 3122);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Filter', 13293, null, 3123);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Groups', 13294, null, 3124);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Link', 13295, null, 3125);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Link_Substitution', 13296, null, 3126);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Status', 13297, null, 3127);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Subsystem', 13298, null, 3128);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Reports_Time_Option', 13299, null, 3129);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Required_Table_Column', 13300, null, 3130);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Required_Table_Column1', 13301, null, 3131);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Schema_Change_Log', 13302, null, 3132);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Data', 13303, null, 3133);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Group_Types', 13304, null, 3134);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Groups', 13305, null, 3135);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Log_Record', 13306, null, 3136);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Login_Profile', 13307, null, 3137);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Object_Rules', 13308, null, 3138);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Objects', 13309, null, 3139);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Passwd_Group', 13310, null, 3140);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Rules', 13311, null, 3141);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Rules_Group', 13312, null, 3142);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_User_Status', 13313, null, 3143);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Users', 13314, null, 3144);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Users_Alias', 13315, null, 3145);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Security_Users_Groups', 13316, null, 3146);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Session_Parameters', 13317, null, 3147);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Statistics', 13318, null, 3148);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_System_Control_Company_SS1_', 13319, null, 3149);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_System_Errors', 13320, null, 3150);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Audits', 13321, null, 3151);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Audits_Obj_Actions', 13322, null, 3152);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Audits_PK_Lookup', 13323, null, 3153);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Audits_Trail', 13324, null, 3154);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Groups', 13325, null, 3155);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Log', 13326, null, 3156);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Months', 13327, null, 3157);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Type', 13328, null, 3158);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Table_Years', 13329, null, 3159);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Task_List', 13330, null, 3160);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Temp_Import_File_Lines', 13331, null, 3161);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Test_Performance', 13332, null, 3162);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Tooltip_Help', 13333, null, 3163);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Tree', 13334, null, 3164);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Tree_Category', 13335, null, 3165);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Tree_Type', 13336, null, 3166);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Treeview_Temp_Global_Temp_Table_', 13337, null, 3167);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Update_Flex', 13338, null, 3168);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_User_DW_Microhelp', 13339, null, 3169);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_User_Profile', 13340, null, 3170);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_User_Profile_Detail', 13341, null, 3171);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify', 13342, null, 3172);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_Batch', 13343, null, 3173);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_Category', 13344, null, 3174);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_Category_Description', 13345, null, 3175);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_Comments_List', 13346, null, 3176);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_Results', 13347, null, 3177);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_User_Comments', 13348, null, 3178);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_User_Key_Fields', 13349, null, 3179);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Verify_User_Std_Comments', 13350, null, 3180);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Version', 13351, null, 3181);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Version_Exes', 13352, null, 3182);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Web_Data', 13353, null, 3183);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Web_Datatypes', 13354, null, 3184);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Web_Printers', 13355, null, 3185);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Web_Sites', 13356, null, 3186);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Window_Disable_Resize', 13357, null, 3187);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Window_Size', 13358, null, 3188);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Wo_Est_Customize', 13359, null, 3189);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Wo_Est_Customize_Temp', 13360, null, 3190);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Wo_Task_Customize', 13361, null, 3191);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Work_Order_Type_Groups', 13362, null, 3192);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PP_Worksheets', 13363, null, 3193);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PPBase_Actions_Windows', 13364, null, 3194);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PPBase_Menu_Items', 13365, null, 3195);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PPBase_Query_Filters', 13366, null, 3196);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PPBase_System_Options', 13367, null, 3197);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PPBase_User_Options', 13368, null, 3198);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PPBase_Workspace_Links', 13369, null, 3199);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PPVerify_Lock', 13370, null, 3200);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Processing_Type', 13371, null, 3201);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Group_Prop_Unit', 13372, null, 3202);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Allo_Definition', 13373, null, 3203);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Distri_Pct', 13374, null, 3204);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_District', 13375, null, 3205);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Location', 13376, null, 3206);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Net_Tax_Reserve', 13377, null, 3207);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Net_Tax_Reserve_Rep', 13378, null, 3208);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Tax_Reserve_Percent', 13379, null, 3209);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Assign_Tree', 13380, null, 3210);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Assign_Tree_CWIP', 13381, null, 3211);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Code', 13382, null, 3212);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Cpr_Audit', 13383, null, 3213);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Cwip_Audit', 13384, null, 3214);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Fifth_Level', 13385, null, 3215);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Fifth_Level_Cwip', 13386, null, 3216);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Tax_Type_Prop_Tax_Adjust', 13387, null, 3217);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Prop_Unit_Type_Size', 13388, null, 3218);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Group', 13389, null, 3219);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Tax_Adjust', 13390, null, 3220);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Tax_Adjust_Type', 13391, null, 3221);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Tax_Authority', 13392, null, 3222);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Tax_Authority_Type', 13393, null, 3223);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Tax_Type_Data', 13394, null, 3224);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Tax_Year', 13395, null, 3225);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Tax_Year_Lock', 13396, null, 3226);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Unit', 13397, null, 3227);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Property_Unit_Default_Life', 13398, null, 3228);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Assign_Tree', 13399, null, 3229);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Assign_Tree_Assign', 13400, null, 3230);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Assign_Tree_Levels', 13401, null, 3231);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Charge', 13402, null, 3232);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Control', 13403, null, 3233);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Deferral_Amount', 13404, null, 3234);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Deferral_Group', 13405, null, 3235);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Pending', 13406, null, 3236);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Pending_Bak', 13407, null, 3237);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Status', 13408, null, 3238);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Type', 13409, null, 3239);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Accrual_Year_Control', 13410, null, 3240);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Actuals', 13411, null, 3241);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Actuals_Summary', 13412, null, 3242);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Allocate_Assessment_Type', 13413, null, 3243);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Allocation_Method', 13414, null, 3244);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Allocation_Type', 13415, null, 3245);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Appraiser', 13416, null, 3246);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Approval_Auth_Level_Bak', 13417, null, 3247);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessed_Value_Allocation', 13418, null, 3248);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessed_Value_County', 13419, null, 3249);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessed_Value_Tax_District', 13420, null, 3250);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessment_Group', 13421, null, 3251);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessment_Group_Case', 13422, null, 3252);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessment_Group_Types', 13423, null, 3253);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessment_Year', 13424, null, 3254);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Assessor', 13425, null, 3255);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Authority_Rate_Type', 13426, null, 3256);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Business_Segment', 13427, null, 3257);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Business_Segment_PCT', 13428, null, 3258);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Case', 13429, null, 3259);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Case_Calc', 13430, null, 3260);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Case_Calc_Authority', 13431, null, 3261);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Case_Control', 13432, null, 3262);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Case_Type', 13433, null, 3263);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Class_Code_Value_Control', 13434, null, 3264);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Column_Help', 13435, null, 3265);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Company', 13436, null, 3266);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Company_Override', 13437, null, 3267);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Company_Tax_Type', 13438, null, 3268);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Control', 13439, null, 3269);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_County_Equalization', 13440, null, 3270);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date', 13441, null, 3271);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date_Field', 13442, null, 3272);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date_Generator', 13443, null, 3273);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date_Link', 13444, null, 3274);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date_Type', 13445, null, 3275);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date_Type_Filter', 13446, null, 3276);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date_Type_Person', 13447, null, 3277);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Date_Weekend_Method', 13448, null, 3278);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Depr_Floor_Autoadj', 13449, null, 3279);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Depr_Floor_Autoadj_Types', 13450, null, 3280);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_District_Equalization', 13451, null, 3281);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Equalization_Factor_Type', 13452, null, 3282);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Escalated_Value_Index', 13453, null, 3283);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Escalated_Value_Type', 13454, null, 3284);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Assessor', 13455, null, 3285);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Assessor_Archive', 13456, null, 3286);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Asset_Loc', 13457, null, 3287);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Asset_Loc_Archive', 13458, null, 3288);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Assets', 13459, null, 3289);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Assets_Archive', 13460, null, 3290);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Auth_Dist', 13461, null, 3291);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Auth_Dist_Archive', 13462, null, 3292);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Class_Code', 13463, null, 3293);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Class_Code_Archive', 13464, null, 3294);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_CWIP_Assign', 13465, null, 3295);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_CWIP_Assign_Archive', 13466, null, 3296);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Division', 13467, null, 3297);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Division_Archive', 13468, null, 3298);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Escval_Index', 13469, null, 3299);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Escval_Index_Archive', 13470, null, 3300);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Ledger', 13471, null, 3301);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Ledger_Adj_Archive', 13472, null, 3302);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Ledger_Archive', 13473, null, 3303);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Levy_Rates', 13474, null, 3304);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Levy_Rates_Archive', 13475, null, 3305);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Loc_Type', 13476, null, 3306);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Loc_Type_Archive', 13477, null, 3307);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Major_Loc', 13478, null, 3308);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Major_Loc_Archive', 13479, null, 3309);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Minor_Loc_Archive', 13480, null, 3310);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Minor_Loc_Type', 13481, null, 3311);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Municipality', 13482, null, 3312);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Municipality_Archive', 13483, null, 3313);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Parcel', 13484, null, 3314);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Parcel_Archive', 13485, null, 3315);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Payment', 13486, null, 3316);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Payment1', 13487, null, 3317);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Payment_Archive', 13488, null, 3318);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Payment_Archive1', 13489, null, 3319);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Asmt', 13490, null, 3320);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Asmt_Archive', 13491, null, 3321);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Geo', 13492, null, 3322);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Geo_Archive', 13493, null, 3323);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Geofctr', 13494, null, 3324);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Geofctr_Archive', 13495, null, 3325);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_History', 13496, null, 3326);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_History_Archive', 13497, null, 3327);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Loc', 13498, null, 3328);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Loc_Archive', 13499, null, 3329);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Rsp', 13500, null, 3330);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Rsp_Archive', 13501, null, 3331);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Rsp_Ent', 13502, null, 3332);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Prcl_Rsp_Ent_Archive', 13503, null, 3333);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Preallo', 13504, null, 3334);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Preallo_Adj_Archive', 13505, null, 3335);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Preallo_Archive', 13506, null, 3336);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Proptax_Loc', 13507, null, 3337);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Proptax_Loc_Archive', 13508, null, 3338);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Retire_Unit', 13509, null, 3339);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Retire_Unit_Archive', 13510, null, 3340);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Rsvfctr_Pcts', 13511, null, 3341);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Rsvfctr_Pcts_Archive', 13512, null, 3342);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stats_Full', 13513, null, 3343);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stats_Full_Archive', 13514, null, 3344);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stats_Incr', 13515, null, 3345);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stats_Incr_Archive', 13516, null, 3346);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stmt', 13517, null, 3347);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stmt_Archive', 13518, null, 3348);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stmt_Line', 13519, null, 3349);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Stmt_Line_Archive', 13520, null, 3350);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Tax_Dist', 13521, null, 3351);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Tax_Dist_Archive', 13522, null, 3352);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Type_Code', 13523, null, 3353);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Type_Code_Archive', 13524, null, 3354);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Unit_Cost', 13525, null, 3355);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Unit_Cost_Archive', 13526, null, 3356);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Util_Account', 13527, null, 3357);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Util_Account_Archive', 13528, null, 3358);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Value_Vault', 13529, null, 3359);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Import_Value_Vault_Archive', 13530, null, 3360);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Incremental_Base_Year', 13531, null, 3361);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Incremental_Init_Adj', 13532, null, 3362);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Incremental_Initialization', 13533, null, 3363);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Installment_Type', 13534, null, 3364);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Interface', 13535, null, 3365);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Interface_Tax_Year', 13536, null, 3366);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger', 13537, null, 3367);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Adjustment', 13538, null, 3368);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Detail', 13539, null, 3369);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Detail_Column', 13540, null, 3370);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Detail_Map', 13541, null, 3371);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Detail_Map_Fields', 13542, null, 3372);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Detail_Source', 13543, null, 3373);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Tax_Year', 13544, null, 3374);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Ledger_Transfer', 13545, null, 3375);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Levy_Class', 13546, null, 3376);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Levy_Rate', 13547, null, 3377);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Location_Rollup', 13548, null, 3378);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Market_Value_Rate', 13549, null, 3379);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Market_Value_Rate_County', 13550, null, 3380);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Method', 13551, null, 3381);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Neg_Bal_Autotrans', 13552, null, 3382);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel', 13553, null, 3383);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Appeal', 13554, null, 3384);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Appeal_Event', 13555, null, 3385);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Appeal_Status', 13556, null, 3386);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Appeal_Type', 13557, null, 3387);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Appraisal', 13558, null, 3388);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Assessment', 13559, null, 3389);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Asset', 13560, null, 3390);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Auto_Adjust', 13561, null, 3391);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Flex_Field_Control', 13562, null, 3392);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Flex_Field_Usage', 13563, null, 3393);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Flex_Field_Values', 13564, null, 3394);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Flex_Fields', 13565, null, 3395);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Geo_Type_Factors', 13566, null, 3396);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Geography', 13567, null, 3397);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Geography_Type', 13568, null, 3398);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Geography_Type_State', 13569, null, 3399);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_History', 13570, null, 3400);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Location', 13571, null, 3401);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Responsibility', 13572, null, 3402);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Responsibility_Type', 13573, null, 3403);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Responsible_Entity', 13574, null, 3404);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Parcel_Type', 13575, null, 3405);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Payment_Status', 13576, null, 3406);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Period_Type', 13577, null, 3407);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_PowerTax_Company', 13578, null, 3408);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Preallo_Adjustment', 13579, null, 3409);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Preallo_Ledger', 13580, null, 3410);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Preallo_Transfer', 13581, null, 3411);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Process', 13582, null, 3412);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Process_Company_State', 13583, null, 3413);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Process_Company_State_Ty', 13584, null, 3414);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Process_Dependency', 13585, null, 3415);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Process_Status', 13586, null, 3416);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Process_Type', 13587, null, 3417);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Pymt_Cntr_1000_Ids', 13588, null, 3418);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Quantity_Conversion', 13589, null, 3419);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Query_Col_Save', 13590, null, 3420);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Query_Columns', 13591, null, 3421);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Query_Filter_Save', 13592, null, 3422);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Query_Main_Save', 13593, null, 3423);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Query_Type', 13594, null, 3424);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Rate_Definition', 13595, null, 3425);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Rate_Type', 13596, null, 3426);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Rates', 13597, null, 3427);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_Company_Field', 13598, null, 3428);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_County_Field', 13599, null, 3429);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_Field_Labels', 13600, null, 3430);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_Location_Field', 13601, null, 3431);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_Parcel_Field', 13602, null, 3432);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_Pt_Company_Field', 13603, null, 3433);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_State_Field', 13604, null, 3434);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_Tax_District_Field', 13605, null, 3435);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Report_Type_Code_Field', 13606, null, 3436);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Reserve_Factor_County_Pcts', 13607, null, 3437);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Reserve_Factor_Dist_Pcts', 13608, null, 3438);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Reserve_Factor_Percents', 13609, null, 3439);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Reserve_Factor_Type', 13610, null, 3440);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Reserve_Factors', 13611, null, 3441);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Reserve_Method', 13612, null, 3442);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Schedule', 13613, null, 3443);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Schedule_Installment', 13614, null, 3444);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Schedule_Period', 13615, null, 3445);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Sgroup_Rollup', 13616, null, 3446);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Sgroup_Rollup_Assign', 13617, null, 3447);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Sgroup_Rollup_Values', 13618, null, 3448);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement', 13619, null, 3449);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Group', 13620, null, 3450);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Group_Payment', 13621, null, 3451);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Group_Schedule', 13622, null, 3452);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Installment', 13623, null, 3453);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Line', 13624, null, 3454);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Line_Payment', 13625, null, 3455);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Payee', 13626, null, 3456);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Payment_App_Bak', 13627, null, 3457);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Period', 13628, null, 3458);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Statement_Year', 13629, null, 3459);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Status', 13630, null, 3460);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Verified_Status', 13631, null, 3461);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statement_Year', 13632, null, 3462);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statistics_Autocreate', 13633, null, 3463);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statistics_Full', 13634, null, 3464);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statistics_Incremental', 13635, null, 3465);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Statistics_Spread_Rules', 13636, null, 3466);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Taxable_Value_Rate', 13637, null, 3467);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Taxable_Value_Rate_County', 13638, null, 3468);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Accrual_Prior_Month_Global_Temp_Table_', 13639, null, 3469);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Actuals_Asmts_Global_Temp_Table_', 13640, null, 3470);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Actuals_Load_Pcts_Global_Temp_Table_', 13641, null, 3471);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Actuals_Total_Global_Temp_Table_', 13642, null, 3472);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Allo_Incr_Base_Pcts_Global_Temp_Table_', 13643, null, 3473);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Allo_Incr_Total_Global_Temp_Table_', 13644, null, 3474);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Allo_Percents_Global_Temp_Table_', 13645, null, 3475);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Allo_Spread_Global_Temp_Table_', 13646, null, 3476);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Bills_Copy', 13647, null, 3477);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Bills_Recalc_Terms_Global_Temp_Table_', 13648, null, 3478);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Casecalc_Rounding_Global_Temp_Table_', 13649, null, 3479);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Casecalc_Totals_Global_Temp_Table_', 13650, null, 3480);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Cprpull_Basis_Adjs', 13651, null, 3481);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_CPRpull_Deprmonth', 13652, null, 3482);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_CPRpull_Ledger_Detail', 13653, null, 3483);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_IDs', 13654, null, 3484);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_IDs_2', 13655, null, 3485);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_IDs_3', 13656, null, 3486);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_IDs_4', 13657, null, 3487);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Ledger_Adds_Global_Temp_Table_', 13658, null, 3488);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Ledger_Adjs_Global_Temp_Table_', 13659, null, 3489);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Ledger_Adjustment', 13660, null, 3490);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Ledger_Tax_Year', 13661, null, 3491);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Ledger_Transfers_Global_Temp_Table_', 13662, null, 3492);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Parcels', 13663, null, 3493);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Parcels_Asmt_Gp', 13664, null, 3494);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Preallo_Adjs', 13665, null, 3495);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Preallo_Transfers_Global_Temp_Table_', 13666, null, 3496);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Scenario_Formula', 13667, null, 3497);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Temp_Statement_Lines_Global_Temp_Table_', 13668, null, 3498);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Type_Assign_Cwip_Pseudo', 13669, null, 3499);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Type_National_Map', 13670, null, 3500);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Type_Rollup', 13671, null, 3501);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Type_Rollup_Assign', 13672, null, 3502);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Type_Rollup_Values', 13673, null, 3503);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Unit_Cost', 13674, null, 3504);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Unit_Cost_Amount', 13675, null, 3505);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Unit_Cost_Amount_Vintage', 13676, null, 3506);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Unit_Cost_Type', 13677, null, 3507);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_User_Input_Ledger', 13678, null, 3508);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Scenario', 13679, null, 3509);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Scenario_Result', 13680, null, 3510);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Scenario_Run', 13681, null, 3511);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Template', 13682, null, 3512);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Template_Eq_Formula', 13683, null, 3513);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Template_Equation', 13684, null, 3514);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Timefram', 13685, null, 3515);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Variable', 13686, null, 3516);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Val_Variable_Source', 13687, null, 3517);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Value_Vault', 13688, null, 3518);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Vintage_Option', 13689, null, 3519);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PT_Year_Override', 13690, null, 3520);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PTC_Documents', 13691, null, 3521);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PTC_Documents_Type', 13692, null, 3522);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PTC_Script_Log', 13693, null, 3523);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PTC_System_Options', 13694, null, 3524);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PTC_System_Options_Centers', 13695, null, 3525);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PTC_System_Options_Values', 13696, null, 3526);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\PTC_User_Options', 13697, null, 3527);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Rate', 13698, null, 3528);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Rate_Analysis_Control', 13699, null, 3529);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Rate_Analysis_Reserve', 13700, null, 3530);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Rate_Type', 13701, null, 3531);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reason_Code', 13702, null, 3532);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Recovery_Period', 13703, null, 3533);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Acct_Company', 13704, null, 3534);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Acct_Master', 13705, null, 3535);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Acct_Rollup', 13706, null, 3536);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Acct_Rollup_Levels', 13707, null, 3537);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Acct_Rollup_Values', 13708, null, 3538);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Acct_Tax_Entity', 13709, null, 3539);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Acct_Type', 13710, null, 3540);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Adjust_Jur_Default', 13711, null, 3541);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Adjust_Jur_Default_Acct', 13712, null, 3542);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Adjust_Types', 13713, null, 3543);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Adjustment_Disposition', 13714, null, 3544);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Adjustment_Status', 13715, null, 3545);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Alloc_Category', 13716, null, 3546);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Alloc_Result', 13717, null, 3547);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Alloc_Target', 13718, null, 3548);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Allocator', 13719, null, 3549);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Allocator_Dyn', 13720, null, 3550);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Allocator_Dyn_Ext', 13721, null, 3551);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Annualization_Type', 13722, null, 3552);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Annualization_Work', 13723, null, 3553);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Comp_Member_Map', 13724, null, 3554);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Comp_Where_Clause', 13725, null, 3555);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Expenditure_Type', 13726, null, 3556);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Family_Member_Map', 13727, null, 3557);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Member_Element', 13728, null, 3558);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Member_Rule', 13729, null, 3559);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Member_Rule_Values', 13730, null, 3560);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Source', 13731, null, 3561);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Budget_Source_Detail', 13732, null, 3562);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Calculations', 13733, null, 3563);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Capital_Struct_Indicator', 13734, null, 3564);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Capital_Structure_Control', 13735, null, 3565);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case', 13736, null, 3566);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Acct', 13737, null, 3567);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Actuals_Log', 13738, null, 3568);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adj_Attachment', 13739, null, 3569);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adj_Calc_Input_Acct', 13740, null, 3570);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjust_Acct', 13741, null, 3571);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjust_Acct_Monthly', 13742, null, 3572);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjust_Alloc_Ledger', 13743, null, 3573);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjust_Alloc_Result', 13744, null, 3574);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjust_Calc_Ext', 13745, null, 3575);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjust_Detail', 13746, null, 3576);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjust_Summary', 13747, null, 3577);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Adjustment', 13748, null, 3578);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Alloc_Account', 13749, null, 3579);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Alloc_Direct_Assign', 13750, null, 3580);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Alloc_Factor', 13751, null, 3581);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Alloc_Steps', 13752, null, 3582);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Alloc_Tax_Control', 13753, null, 3583);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Allocator_Dyn_Factor', 13754, null, 3584);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Attachment', 13755, null, 3585);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Calculation', 13756, null, 3586);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Category_Allocator', 13757, null, 3587);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Cost_of_Capital', 13758, null, 3588);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Cost_of_Capital_Rates', 13759, null, 3589);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Coverage_Versions', 13760, null, 3590);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Forecast_Log', 13761, null, 3591);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_History', 13762, null, 3592);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Interest_Sync_Control', 13763, null, 3593);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Interest_Sync_Rate', 13764, null, 3594);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Monitor_Control', 13765, null, 3595);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Multi_Calc_Cap_Struct', 13766, null, 3596);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Multi_Calc_Label', 13767, null, 3597);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Multi_Calc_Result', 13768, null, 3598);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Parameter', 13769, null, 3599);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Process', 13770, null, 3600);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Process_Status', 13771, null, 3601);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Process_Steps', 13772, null, 3602);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Results', 13773, null, 3603);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Results_Type', 13774, null, 3604);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Return_Result', 13775, null, 3605);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Return_Result_Summary', 13776, null, 3606);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Rev_Req_Results', 13777, null, 3607);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Status_Detail_Adjust', 13778, null, 3608);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Status_Detail_Alloc', 13779, null, 3609);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Status_Detail_Calc', 13780, null, 3610);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Status_Detail_Ledger', 13781, null, 3611);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Summary_Ledger', 13782, null, 3612);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Case_Target_Forward', 13783, null, 3613);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Chart', 13784, null, 3614);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Chart_Group', 13785, null, 3615);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Chart_Group_Cases', 13786, null, 3616);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Chart_Group_Labels', 13787, null, 3617);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Co_Input_Scale', 13788, null, 3618);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Column', 13789, null, 3619);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Company', 13790, null, 3620);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Company_Security', 13791, null, 3621);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos', 13792, null, 3622);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos_Elements', 13793, null, 3623);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos_Elements_Bdg', 13794, null, 3624);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos_Fields', 13795, null, 3625);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos_Fields_Bdg', 13796, null, 3626);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos_Map', 13797, null, 3627);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos_Map_Bdg', 13798, null, 3628);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Combos_Sources_Fields', 13799, null, 3629);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Pull_Type', 13800, null, 3630);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Tables', 13801, null, 3631);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Where', 13802, null, 3632);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CR_Where_Clause', 13803, null, 3633);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Comp_Member_Map', 13804, null, 3634);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Comp_Where_Clause', 13805, null, 3635);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Component_Element', 13806, null, 3636);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Component_Values', 13807, null, 3637);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Family_Components', 13808, null, 3638);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Family_Member', 13809, null, 3639);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Family_Member_Map', 13810, null, 3640);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_GL_Account_Type', 13811, null, 3641);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Member_Element', 13812, null, 3642);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Member_Rule', 13813, null, 3643);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Member_Rule_Values', 13814, null, 3644);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_CWIP_Source', 13815, null, 3645);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Depr_Component_Activity', 13816, null, 3646);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Depr_Family_Components', 13817, null, 3647);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Depr_Family_Member', 13818, null, 3648);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Depr_Family_Member_Map', 13819, null, 3649);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Depr_Ledger_Columns', 13820, null, 3650);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Depr_Ledger_Map', 13821, null, 3651);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Depr_Ledger_Map_Act', 13822, null, 3652);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Combos', 13823, null, 3653);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Combos_Fields', 13824, null, 3654);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Combos_Map', 13825, null, 3655);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Company_Map', 13826, null, 3656);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Load_Bal_Temp', 13827, null, 3657);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Load_Temp', 13828, null, 3658);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Pull_Type', 13829, null, 3659);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Source', 13830, null, 3660);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Source_Field', 13831, null, 3661);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Stage', 13832, null, 3662);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ext_Stg_Field', 13833, null, 3663);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Family', 13834, null, 3664);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fcst_Cap_Bdg_Load_Temp', 13835, null, 3665);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fcst_Cr_Load_Temp', 13836, null, 3666);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fcst_Depr_Fam_Mem_Map', 13837, null, 3667);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fcst_Depr_Ledger_Map', 13838, null, 3668);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fcst_Depr_Ledger_Map_Act', 13839, null, 3669);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_FERC_Account', 13840, null, 3670);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Cost_of_Capital', 13841, null, 3671);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Alloc', 13842, null, 3672);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Alloc_Factor', 13843, null, 3673);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Calc_Key', 13844, null, 3674);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Calc_Summary', 13845, null, 3675);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Key', 13846, null, 3676);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Process_Temp', 13847, null, 3677);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Sub_Acct', 13848, null, 3678);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Summary', 13849, null, 3679);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Fin_Monitor_Type', 13850, null, 3680);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Financial_Monitor_Res_Bak', 13851, null, 3681);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Financial_Monitor_Result', 13852, null, 3682);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Financial_Monitor_Result1', 13853, null, 3683);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Financial_Monitor_Summary', 13854, null, 3684);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Forecast_Activity', 13855, null, 3685);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Forecast_Ledger', 13856, null, 3686);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Forecast_Ledger_Adjust', 13857, null, 3687);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Forecast_Version', 13858, null, 3688);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Function', 13859, null, 3689);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_CR_Load_Temp', 13860, null, 3690);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_CWIP_Load_Temp', 13861, null, 3691);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_Depr_Set_of_Books', 13862, null, 3692);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_Recon_CR_Combos', 13863, null, 3693);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_Recon_CR_Elements', 13864, null, 3694);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_Recon_CR_Map', 13865, null, 3695);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_Recon_Items', 13866, null, 3696);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_Recon_Regacct_Map', 13867, null, 3697);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Hist_Tax_Load_Temp', 13868, null, 3698);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Historic_Recon_Ledger', 13869, null, 3699);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Historic_Version', 13870, null, 3700);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_History_Activity', 13871, null, 3701);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_History_Ledger', 13872, null, 3702);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_History_Ledger_Adjust', 13873, null, 3703);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Import_Allo_Factor_Stg', 13874, null, 3704);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Import_Allo_Factor_Stg_Arc', 13875, null, 3705);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Import_Alloc_Factor', 13876, null, 3706);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Import_Ext_Stage_Arc', 13877, null, 3707);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Import_Ext_Stage_Stg', 13878, null, 3708);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Import_History_Ldg', 13879, null, 3709);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Import_History_Ldg_Arc', 13880, null, 3710);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Input_Scale', 13881, null, 3711);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Interface', 13882, null, 3712);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Interface_Control', 13883, null, 3713);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Issue', 13884, null, 3714);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Acct_Default', 13885, null, 3715);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Adj_Attachment', 13886, null, 3716);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Adj_Calc_Input_Acct', 13887, null, 3717);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Adjust_Calc_Ext', 13888, null, 3718);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Alloc_Account', 13889, null, 3719);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Alloc_Factor', 13890, null, 3720);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Alloc_Steps', 13891, null, 3721);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Alloc_Tax_Control', 13892, null, 3722);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Category_Allocator', 13893, null, 3723);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Input_Scale', 13894, null, 3724);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Monitor_Control', 13895, null, 3725);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Target_Forward', 13896, null, 3726);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Tax_Apportionment', 13897, null, 3727);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jur_Template', 13898, null, 3728);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Jurisdiction', 13899, null, 3729);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Monitor_Control_Adj_Temp', 13900, null, 3730);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Monitor_Jur_Case', 13901, null, 3731);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Multi_Calc_Parm', 13902, null, 3732);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Parameter', 13903, null, 3733);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Query_Drillback', 13904, null, 3734);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Query_List', 13905, null, 3735);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Recovery_Class', 13906, null, 3736);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Related_Cases', 13907, null, 3737);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Source', 13908, null, 3738);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Status', 13909, null, 3739);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Status_Check', 13910, null, 3740);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Sub_Acct_Type', 13911, null, 3741);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Comp_Member_Map', 13912, null, 3742);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Component', 13913, null, 3743);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Control_Map', 13914, null, 3744);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Family_Member', 13915, null, 3745);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Family_Member_Map', 13916, null, 3746);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_M_Type_Entity_Map', 13917, null, 3747);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Split_Rule_Amount', 13918, null, 3748);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Split_Rules', 13919, null, 3749);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Ta_Version_Control', 13920, null, 3750);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Comp_Member_Map', 13921, null, 3751);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Component', 13922, null, 3752);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Control', 13923, null, 3753);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Entity', 13924, null, 3754);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Family_Member', 13925, null, 3755);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Family_Member_Map', 13926, null, 3756);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Load_History', 13927, null, 3757);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Rate', 13928, null, 3758);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Stage', 13929, null, 3759);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reg_Tax_Version_Control', 13930, null, 3760);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Regulatory_Entries_Elig', 13931, null, 3761);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Regulatory_Entry_Type', 13932, null, 3762);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Regulatory_Transactions', 13933, null, 3763);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Account', 13934, null, 3764);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Amt_Type', 13935, null, 3765);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill', 13936, null, 3766);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Cap_Adjustment', 13937, null, 3767);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Class_Code', 13938, null, 3768);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Details', 13939, null, 3769);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Details_Temp', 13940, null, 3770);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Document', 13941, null, 3771);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Dollar_Type', 13942, null, 3772);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Group', 13943, null, 3773);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Grp_Related', 13944, null, 3774);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Hold_History', 13945, null, 3775);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Interface', 13946, null, 3776);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Line', 13947, null, 3777);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Messages', 13948, null, 3778);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Premises_Rule', 13949, null, 3779);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review', 13950, null, 3780);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review_Adj_Split', 13951, null, 3781);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review_Adjs_Appr', 13952, null, 3782);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review_Adjustments', 13953, null, 3783);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review_Approval', 13954, null, 3784);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review_Detail', 13955, null, 3785);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review_Excess', 13956, null, 3786);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Review_Split', 13957, null, 3787);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Schedule', 13958, null, 3788);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Statistics', 13959, null, 3789);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Status', 13960, null, 3790);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Support_CR', 13961, null, 3791);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Support_CWIP', 13962, null, 3792);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Bill_Trans_Type', 13963, null, 3793);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Billing_Freq', 13964, null, 3794);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Billing_Type', 13965, null, 3795);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Group', 13966, null, 3796);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Group_By', 13967, null, 3797);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_OH_Elig', 13968, null, 3798);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_OH_Exclusions', 13969, null, 3799);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_OH_Rate', 13970, null, 3800);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Overhead', 13971, null, 3801);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Sources', 13972, null, 3802);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Sources_Fields', 13973, null, 3803);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Target', 13974, null, 3804);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Target_Criteria', 13975, null, 3805);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Where', 13976, null, 3806);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_CR_Where_Clause', 13977, null, 3807);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Credit_Memo', 13978, null, 3808);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Cust_Type', 13979, null, 3809);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Customer', 13980, null, 3810);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Customer_Company', 13981, null, 3811);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Customer_Split', 13982, null, 3812);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Display', 13983, null, 3813);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Display_Elements', 13984, null, 3814);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Elig', 13985, null, 3815);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Elig_Charge_Type', 13986, null, 3816);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Elig_Est', 13987, null, 3817);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Est_Bill_Hist', 13988, null, 3818);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Est_OH_Alloc', 13989, null, 3819);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Est_OH_Basis', 13990, null, 3820);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Est_OH_Elig', 13991, null, 3821);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Est_OH_Exclusions', 13992, null, 3822);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Est_OH_Rate', 13993, null, 3823);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Est_Overhead', 13994, null, 3824);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Estimate', 13995, null, 3825);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_GL_Transaction', 13996, null, 3826);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_JE_Timing', 13997, null, 3827);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_JE_Type', 13998, null, 3828);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Method', 13999, null, 3829);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Method_Company', 14000, null, 3830);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_OH_Alloc', 14001, null, 3831);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_OH_Basis', 14002, null, 3832);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_OH_Elig', 14003, null, 3833);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_OH_Exclusions', 14004, null, 3834);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_OH_Rate', 14005, null, 3835);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_OH_Type', 14006, null, 3836);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Overhead', 14007, null, 3837);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Payment', 14008, null, 3838);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Penalty', 14009, null, 3839);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Penalty_Results', 14010, null, 3840);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Premises', 14011, null, 3841);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Process_Control', 14012, null, 3842);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Queue', 14013, null, 3843);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Queue_Users', 14014, null, 3844);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund', 14015, null, 3845);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Calc', 14016, null, 3846);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Rate_Sch', 14017, null, 3847);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Rate_Type', 14018, null, 3848);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Rates', 14019, null, 3849);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Ratio', 14020, null, 3850);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Ratio_Data', 14021, null, 3851);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Special_Calc', 14022, null, 3852);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Special_Calc_Args', 14023, null, 3853);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Refund_Type', 14024, null, 3854);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Report', 14025, null, 3855);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Report_CR', 14026, null, 3856);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Report_CWIP', 14027, null, 3857);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Statistic_Fields', 14028, null, 3858);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Statistic_Type', 14029, null, 3859);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Status', 14030, null, 3860);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Status_History', 14031, null, 3861);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Super_Group', 14032, null, 3862);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_System_Control', 14033, null, 3863);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Tax_Gross_Up', 14034, null, 3864);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Type', 14035, null, 3865);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimb_Work_Order', 14036, null, 3866);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimbursable', 14037, null, 3867);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimbursable_Calculation', 14038, null, 3868);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reimbursable_Payment', 14039, null, 3869);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Related_Asset', 14040, null, 3870);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Related_Asset_Type', 14041, null, 3871);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Related_WOS', 14042, null, 3872);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Amount_Allocate', 14043, null, 3873);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Amount_Allocate_Debug', 14044, null, 3874);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Amt_Alloc_Reporting', 14045, null, 3875);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Batch_Control', 14046, null, 3876);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Blanket_Proc_Debug', 14047, null, 3877);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Blanket_Process_Report', 14048, null, 3878);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Blanket_Processing', 14049, null, 3879);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Blanket_Results', 14050, null, 3880);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Blanket_Results_Debug', 14051, null, 3881);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Blkt_Results_Reporting', 14052, null, 3882);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Calc_Asset', 14053, null, 3883);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Calc_Asset_Stg', 14054, null, 3884);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Calc_CWIP', 14055, null, 3885);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Detail_WMIS_Feed', 14056, null, 3886);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Loc_Rollup', 14057, null, 3887);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Loc_Rollup_Func_Class', 14058, null, 3888);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Loc_Rollup_Mapping', 14059, null, 3889);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Location', 14060, null, 3890);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Location1', 14061, null, 3891);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Location_Type', 14062, null, 3892);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Lookup_Qty_Cost', 14063, null, 3893);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Major_Unit_Pct', 14064, null, 3894);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Pretest_Explain', 14065, null, 3895);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Priortiy', 14066, null, 3896);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Priortiy_Type', 14067, null, 3897);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Priortiy_Type_Priority', 14068, null, 3898);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Process_Control', 14069, null, 3899);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Range_Test', 14070, null, 3900);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Range_Test_Header', 14071, null, 3901);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Schema', 14072, null, 3902);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Tables', 14073, null, 3903);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Test_Priority', 14074, null, 3904);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Thresholds', 14075, null, 3905);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Unit_Code', 14076, null, 3906);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Unit_Code_Func_Class', 14077, null, 3907);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_WO_RUC_Exception', 14078, null, 3908);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_WO_Seg_Reporting', 14079, null, 3909);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Work_Order_Debug', 14080, null, 3910);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Work_Order_Orig_Debug', 14081, null, 3911);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Work_Order_Segments', 14082, null, 3912);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Work_Order_Temp', 14083, null, 3913);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Repair_Work_Order_Temp_Orig', 14084, null, 3914);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Report_Time_Global_Temp_Table_', 14085, null, 3915);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Req_Status', 14086, null, 3916);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Reserve_Ratios', 14087, null, 3917);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Ret_Unit_Mort_History', 14088, null, 3918);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Bal_Convention', 14089, null, 3919);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Depr_Convention', 14090, null, 3920);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Method', 14091, null, 3921);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Reserve_Convention', 14092, null, 3922);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Transaction', 14093, null, 3923);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Unit_Comp_Unit', 14094, null, 3924);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Unit_SKU', 14095, null, 3925);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Unit_Std', 14096, null, 3926);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Unit_Tax_Distinction', 14097, null, 3927);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retire_Unit_Typ_Siz', 14098, null, 3928);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retirement_Unit', 14099, null, 3929);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retirement_Unit_Association', 14100, null, 3930);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Retirement_Unit_Work_Type', 14101, null, 3931);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Revision_Selection', 14102, null, 3932);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Ast_Loc', 14103, null, 3933);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Ast_Loc_Arc', 14104, null, 3934);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Roll_Fc', 14105, null, 3935);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Roll_Fc_Arc', 14106, null, 3936);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Roll_Loc', 14107, null, 3937);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Roll_Loc_Arc', 14108, null, 3938);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Rollup', 14109, null, 3939);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Loc_Rollup_Arc', 14110, null, 3940);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Location', 14111, null, 3941);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Location_Arc', 14112, null, 3942);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Pri_Test', 14113, null, 3943);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Pri_Test_Arc', 14114, null, 3944);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Range_Test', 14115, null, 3945);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Range_Test_Arc', 14116, null, 3946);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Range_Test_Rng', 14117, null, 3947);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Range_Test_Rng_Arc', 14118, null, 3948);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Rng_Tst_Unt_Cd', 14119, null, 3949);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Rng_Tst_Unt_Cd_Arc', 14120, null, 3950);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Schema', 14121, null, 3951);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Schema_Arc', 14122, null, 3952);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Tax_Status', 14123, null, 3953);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Tax_Status_Arc', 14124, null, 3954);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Test', 14125, null, 3955);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Test_Arc', 14126, null, 3956);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Threshold', 14127, null, 3957);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Threshold_Arc', 14128, null, 3958);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Tst_Wotype', 14129, null, 3959);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Tst_Wotype_Arc', 14130, null, 3960);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Fc', 14131, null, 3961);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Fc_Arc', 14132, null, 3962);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Prp_Grp', 14133, null, 3963);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Prp_Grp_Arc', 14134, null, 3964);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Uapu', 14135, null, 3965);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Uapu_Arc', 14136, null, 3966);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Wotype', 14137, null, 3967);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Uc_Wotype_Arc', 14138, null, 3968);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Unit_Code', 14139, null, 3969);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Unit_Code_Arc', 14140, null, 3970);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Wo_Tax_Repairs', 14141, null, 3971);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RPR_Import_Wo_Tax_Repairs_Arc', 14142, null, 3972);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Rus_Line_Items', 14143, null, 3973);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Allo_Errors', 14144, null, 3974);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Allo_Est', 14145, null, 3975);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Allo_Groups', 14146, null, 3976);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Allo_Results', 14147, null, 3977);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Control', 14148, null, 3978);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Exclude', 14149, null, 3979);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Pendtrans', 14150, null, 3980);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Revision', 14151, null, 3981);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Round2', 14152, null, 3982);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Rounding', 14153, null, 3983);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Stag_Al', 14154, null, 3984);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Stag_Arc', 14155, null, 3985);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Stag_Woe', 14156, null, 3986);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Closeout_Staging', 14157, null, 3987);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Sob_Id_Basis', 14158, null, 3988);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Type', 14159, null, 3989);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\RWIP_Type_Set_Of_Books', 14160, null, 3990);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Salvage', 14161, null, 3991);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Salvage_Analysis', 14162, null, 3992);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Salvage_Convention', 14163, null, 3993);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Select_Tabs_Grids', 14164, null, 3994);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Set_of_Books', 14165, null, 3995);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\SKU_Substitution', 14166, null, 3996);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\SPR_Analysis', 14167, null, 3997);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Spread_Factor', 14168, null, 3998);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Standard_Journal_Entries', 14169, null, 3999);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\State', 14170, null, 4000);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Status_Code', 14171, null, 4001);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Stck_Keep_Unit', 14172, null, 4002);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Stck_Keep_Unit_Price_Hist', 14173, null, 4003);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Stock_Type', 14174, null, 4004);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Stores_Group', 14175, null, 4005);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Sub_Account', 14176, null, 4006);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Sub_Account_List', 14177, null, 4007);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Subledger_Control', 14178, null, 4008);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Subledger_Depr_Type', 14179, null, 4009);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Subledger_Entry_Control', 14180, null, 4010);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Subledger_Template', 14181, null, 4011);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Subst_Review_Details_Arch', 14182, null, 4012);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Substitution_Review', 14183, null, 4013);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Substitution_Review_Arch', 14184, null, 4014);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Substitution_Review_Details', 14185, null, 4015);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Summary_4562', 14186, null, 4016);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Summary_Transaction_Code', 14187, null, 4017);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Suspended_Charge', 14188, null, 4018);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Account_Debug', 14189, null, 4019);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Account_Tmp_Global_Temp_Table_', 14190, null, 4020);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Account_Type_Def', 14191, null, 4021);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Acct_Rollups', 14192, null, 4022);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Activity_Type', 14193, null, 4023);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Alloc', 14194, null, 4024);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Alloc_Amounts', 14195, null, 4025);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Alloc_Drill', 14196, null, 4026);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Alloc_Percents', 14197, null, 4027);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Alloc_Type', 14198, null, 4028);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Applications', 14199, null, 4029);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Apportionment', 14200, null, 4030);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Approval_Co_Tmp', 14201, null, 4031);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Approval_Document', 14202, null, 4032);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Assign_NS_TMP_Global_Temp_Table_', 14203, null, 4033);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Balance_Diffs_Tmp', 14204, null, 4034);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Batch_Log', 14205, null, 4035);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Beg_Bal_Load_Global_Temp_Table_', 14206, null, 4036);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_BS', 14207, null, 4037);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_BS_Book_Assign', 14208, null, 4038);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_BS_Line_Item', 14209, null, 4039);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_BS_Report', 14210, null, 4040);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_BS_Schema', 14211, null, 4041);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_BS_Schema_Assign', 14212, null, 4042);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_BS_Tax_Basis_Type', 14213, null, 4043);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Co_Consol_Tmp_Global_Temp_Table_', 14214, null, 4044);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Co_Trans', 14215, null, 4045);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Company_Fields', 14216, null, 4046);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Company_Posting', 14217, null, 4047);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Company_Rollup', 14218, null, 4048);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Compliance_Export', 14219, null, 4049);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Consol_Drill', 14220, null, 4050);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Control', 14221, null, 4051);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Control_Document', 14222, null, 4052);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Budget_Data', 14223, null, 4053);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Drill', 14224, null, 4054);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Drill_Arc', 14225, null, 4055);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Drill_Explode', 14226, null, 4056);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_JE_Field_Map', 14227, null, 4057);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_JE_Updates', 14228, null, 4058);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pay_Control', 14229, null, 4059);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pay_Criteria', 14230, null, 4060);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pay_Updates', 14231, null, 4061);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pull_Categories', 14232, null, 4062);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pull_Control', 14233, null, 4063);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pull_Criteria', 14234, null, 4064);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pull_Fields', 14235, null, 4065);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pull_Type', 14236, null, 4066);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Pull_Updates', 14237, null, 4067);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Structures2_Tmp_Global_Temp_Table_', 14238, null, 4068);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_CR_Structures2_Tmp_Global_Temporary_Table_', 14239, null, 4069);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Custom_Tab', 14240, null, 4070);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Custom_Tab_Fields', 14241, null, 4071);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Dampening_Map', 14242, null, 4072);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Dampening_Option', 14243, null, 4073);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Dampening_Type', 14244, null, 4074);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Data_Source_Type', 14245, null, 4075);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Data_Sources', 14246, null, 4076);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DB_Session_Params', 14247, null, 4077);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Def_Tax', 14248, null, 4078);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Def_Tax_Control', 14249, null, 4079);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Def_Tax_Proc_Tmp_Global_Temporary_Table_', 14250, null, 4080);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Def_Tax_Tmp_Global_Temp_Table_', 14251, null, 4081);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Diff_Ind', 14252, null, 4082);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DIT_Process_Option', 14253, null, 4083);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DIT_Rate_Type', 14254, null, 4084);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DIT_Schema', 14255, null, 4085);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DIT_Schema_Checks', 14256, null, 4086);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Classification', 14257, null, 4087);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Company', 14258, null, 4088);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Data_Con_Dtls', 14259, null, 4089);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Data_Con_Tmp', 14260, null, 4090);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Data_Control', 14261, null, 4091);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Deferral_Type', 14262, null, 4092);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Drill', 14263, null, 4093);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Filter_Type', 14264, null, 4094);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_ID', 14265, null, 4095);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Issue', 14266, null, 4096);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Jurisdiction', 14267, null, 4097);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Process_Info', 14268, null, 4098);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Results', 14269, null, 4099);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Results_Raw', 14270, null, 4100);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Status', 14271, null, 4101);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DMI_Tax_Year', 14272, null, 4102);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Document', 14273, null, 4103);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_DS_Values', 14274, null, 4104);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Eff_Rate', 14275, null, 4105);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Eff_Rate_Debug', 14276, null, 4106);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Eff_Rate_Tmp_Global_Temp_Table_', 14277, null, 4107);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Eff_Rate_Tmp2_Global_Temp_Table_', 14278, null, 4108);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity', 14279, null, 4109);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Deduct', 14280, null, 4110);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Deduct1', 14281, null, 4111);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Deduct_Exc', 14282, null, 4112);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Deduct_Tmp_Global_Temp_Table_', 14283, null, 4113);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Include', 14284, null, 4114);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Include_Act', 14285, null, 4115);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Juris', 14286, null, 4116);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Entity_Rep_Include', 14287, null, 4117);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Error_Codes', 14288, null, 4118);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_FAS109', 14289, null, 4119);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_FAS109_Control', 14290, null, 4120);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_FAS109_Proc_Tmp_Global_Temp_Table_', 14291, null, 4121);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_FAS109_Tmp_Global_Temp_Table_', 14292, null, 4122);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GL_Account_Type', 14293, null, 4123);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GL_Data', 14294, null, 4124);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GL_Data_Con_Dtls', 14295, null, 4125);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GL_Data_Control', 14296, null, 4126);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GL_Errors', 14297, null, 4127);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GL_Month_Def', 14298, null, 4128);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GL_Pay_Control', 14299, null, 4129);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_GLJE_Data', 14300, null, 4130);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Import', 14301, null, 4131);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Import_Activity', 14302, null, 4132);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Import_BS', 14303, null, 4133);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Import_Control', 14304, null, 4134);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Import_Document', 14305, null, 4135);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE', 14306, null, 4136);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Detail', 14307, null, 4137);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Export', 14308, null, 4138);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Field_Map', 14309, null, 4139);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_History', 14310, null, 4140);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_History_Tmp_Global_Temporary_Table_', 14311, null, 4141);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Results', 14312, null, 4142);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Results_Con_His', 14313, null, 4143);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Results_Control', 14314, null, 4144);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Schema', 14315, null, 4145);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Translate', 14316, null, 4146);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Type', 14317, null, 4147);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_JE_Updates', 14318, null, 4148);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Jur_Allo', 14319, null, 4149);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Jur_Allo_Type', 14320, null, 4150);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Jur_Exceptions', 14321, null, 4151);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Jur_Ta_Norm', 14322, null, 4152);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Current', 14323, null, 4153);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Item', 14324, null, 4154);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Item_Debug', 14325, null, 4155);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Item_Drill_Tmp_Global_Temp_Table_', 14326, null, 4156);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Item_ETR_Calcs', 14327, null, 4157);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Item_Proc_Tmp_Global_Temp_Table_', 14328, null, 4158);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Item_Rollups', 14329, null, 4159);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Item_Tmp_Global_Temp_Table_', 14330, null, 4160);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Master', 14331, null, 4161);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Rate_Type', 14332, null, 4162);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Rollup', 14333, null, 4163);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Type', 14334, null, 4164);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_M_Type_Treatment', 14335, null, 4165);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Month_Dropdowns', 14336, null, 4166);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Month_Group', 14337, null, 4167);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Month_Type', 14338, null, 4168);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Month_Type_Trueup', 14339, null, 4169);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Monthly_Spread', 14340, null, 4170);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Norm_Schema', 14341, null, 4171);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Oper_Co', 14342, null, 4172);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Oper_Ind', 14343, null, 4173);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Pay_Field', 14344, null, 4174);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Period', 14345, null, 4175);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PLSQL_Debug', 14346, null, 4176);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Posting_Type', 14347, null, 4177);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PowerTax_Arc', 14348, null, 4178);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PowerTax_Map', 14349, null, 4179);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PowerTax_Map_NR', 14350, null, 4180);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PowerTax_Option', 14351, null, 4181);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PowerTax_Retr_NR', 14352, null, 4182);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PowerTax_Retrieve', 14353, null, 4183);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PP_Tree_Type', 14354, null, 4184);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Proc_Deduct', 14355, null, 4185);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Process_Info', 14356, null, 4186);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Process_Option', 14357, null, 4187);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Process_Tmp_Global_Temp_Table_', 14358, null, 4188);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Processing_Errors', 14359, null, 4189);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PT_Ind', 14360, null, 4190);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PT_TC_Rollup_Tmp_Global_Temp_Table_', 14361, null, 4191);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PT_TRC_Tmp_Global_Temp_Table_', 14362, null, 4192);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PT_Unused_Schema', 14363, null, 4193);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_PT_Unused_TC', 14364, null, 4194);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Reg_Ind', 14365, null, 4195);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Cons_Cols', 14366, null, 4196);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Cons_Rows', 14367, null, 4197);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Cons_Type', 14368, null, 4198);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Criteria_Debug', 14369, null, 4199);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Criteria_Tmp_Global_Temp_Table_', 14370, null, 4200);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Custom', 14371, null, 4201);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Custom_Mod', 14372, null, 4202);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Custom_Mod_Dtl', 14373, null, 4203);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Custom_Opt_Dtl', 14374, null, 4204);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Custom_Option', 14375, null, 4205);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Deferred_Type', 14376, null, 4206);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Je_Type_Global_Temp_Table_', 14377, null, 4207);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Je_Type_Debug', 14378, null, 4208);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Months_Global_Temp_Table_', 14379, null, 4209);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Months_Global_Temporary_Table_', 14380, null, 4210);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Roll_Tmp_Global_Temp_Table_', 14381, null, 4211);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Rollup', 14382, null, 4212);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Rollup_Dtl', 14383, null, 4213);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Rollup_Group', 14384, null, 4214);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Special_Crit', 14385, null, 4215);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Special_Note', 14386, null, 4216);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Tree_Debug', 14387, null, 4217);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rep_Tree_Temp_Global_Temp_Table_', 14388, null, 4218);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Report_Object', 14389, null, 4219);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Report_Option', 14390, null, 4220);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Report_Option_Dtl', 14391, null, 4221);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Report_Transaction', 14392, null, 4222);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Reports_Customize', 14393, null, 4223);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Results', 14394, null, 4224);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Results_Con_JE', 14395, null, 4225);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Results_Con_JE_Alt', 14396, null, 4226);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Results_Control', 14397, null, 4227);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Results_ETR', 14398, null, 4228);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rollup', 14399, null, 4229);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rollup_Acct', 14400, null, 4230);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rollup_Detail', 14401, null, 4231);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Rollup_Dtl_Acct', 14402, null, 4232);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_RTP_Check_Global_Temp_Table_', 14403, null, 4233);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_RTP_Proc_Tmp_Global_Temp_Table_', 14404, null, 4234);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_SchM_M3', 14405, null, 4235);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Sign_Filter', 14406, null, 4236);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Spread_Template', 14407, null, 4237);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Spread_TMP_Global_Temp_Table_', 14408, null, 4238);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_SQL_Hints', 14409, null, 4239);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_SQL_Mods', 14410, null, 4240);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_SubLedger', 14411, null, 4241);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Adj_TMP_Global_Temporary_Table_', 14412, null, 4242);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Adjust', 14413, null, 4243);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Ext', 14414, null, 4244);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Ext_Data', 14415, null, 4245);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Ext_Roll', 14416, null, 4246);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Freq', 14417, null, 4247);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Pay_Type', 14418, null, 4248);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Sub_Cat', 14419, null, 4249);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Subledger_Tax_Type', 14420, null, 4250);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_SubLedger_Tmp_Global_Temp_Table_', 14421, null, 4251);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Sys_Option_Values', 14422, null, 4252);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_System_Control', 14423, null, 4253);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_System_Options', 14424, null, 4254);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Tax_Control', 14425, null, 4255);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Tax_Year', 14426, null, 4256);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Tbs_Account_Type', 14427, null, 4257);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Tbs_Treatment', 14428, null, 4258);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Templates', 14429, null, 4259);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Templates_Data', 14430, null, 4260);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_TrueUp', 14431, null, 4261);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Trueup_TMP_Global_Temp_Table_', 14432, null, 4262);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_User_Options', 14433, null, 4263);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Validation_Msgs_Global_Temp_Table_', 14434, null, 4264);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Version', 14435, null, 4265);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Accrual_Version_Type', 14436, null, 4266);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Activity_Code', 14437, null, 4267);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Add_Audit_Trail', 14438, null, 4268);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Annotation', 14439, null, 4269);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Archive_Status', 14440, null, 4270);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Auth_Prop_Tax_Dist', 14441, null, 4271);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book', 14442, null, 4272);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Reconcile', 14443, null, 4273);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Reconcile_FP', 14444, null, 4274);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Reconcile_Transfer', 14445, null, 4275);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Schema', 14446, null, 4276);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Trans_Adds', 14447, null, 4277);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Transactions', 14448, null, 4278);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Transactions_Grp', 14449, null, 4279);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Transfers', 14450, null, 4280);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Transfers_Grp', 14451, null, 4281);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Translate', 14452, null, 4282);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Translate_Group', 14453, null, 4283);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Book_Translate_Group_Map', 14454, null, 4284);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Class', 14455, null, 4285);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Class_Rollups', 14456, null, 4286);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Co_Def_Tax_Schema', 14457, null, 4287);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Convention', 14458, null, 4288);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Credit', 14459, null, 4289);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Credit_Schema', 14460, null, 4290);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_CWIP_Class_Reconcile', 14461, null, 4291);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_CWIP_Company_Record', 14462, null, 4292);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_CWIP_Company_Rollup', 14463, null, 4293);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Depr_Schema', 14464, null, 4294);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Depr_Schema_Control', 14465, null, 4295);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Depreciation', 14466, null, 4296);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Depreciation_FP', 14467, null, 4297);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Depreciation_Transfer', 14468, null, 4298);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Event', 14469, null, 4299);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Fcst_Archive_Status', 14470, null, 4300);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Fcst_Budget_Adds', 14471, null, 4301);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Fcst_Budget_Adds_FP', 14472, null, 4302);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Fcst_Budget_Retires', 14473, null, 4303);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Forecast_Input', 14474, null, 4304);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Forecast_Output', 14475, null, 4305);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Forecast_Version', 14476, null, 4306);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Half_Year', 14477, null, 4307);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Include', 14478, null, 4308);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Include_Activity', 14479, null, 4309);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Interface_Cos', 14480, null, 4310);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Job_Log', 14481, null, 4311);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Job_Params', 14482, null, 4312);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Law', 14483, null, 4313);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Layer', 14484, null, 4314);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Limit', 14485, null, 4315);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Limitation', 14486, null, 4316);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Location', 14487, null, 4317);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Method', 14488, null, 4318);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Mx_Inflation_Index', 14489, null, 4319);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Mx_Inflation_Midpoint', 14490, null, 4320);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Package_Control', 14491, null, 4321);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Rate_Control', 14492, null, 4322);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Rate_Control_Delv', 14493, null, 4323);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Rates', 14494, null, 4324);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Rates_Delv', 14495, null, 4325);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Reconcile_Item', 14496, null, 4326);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Record_Control', 14497, null, 4327);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Record_Document', 14498, null, 4328);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Repairs_Add_Expense', 14499, null, 4329);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Ret_Audit_Trail', 14500, null, 4330);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Ret_Audit_Train_Grp', 14501, null, 4331);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Retire_Rules', 14502, null, 4332);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Reversal_Book_Summary', 14503, null, 4333);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Rollup', 14504, null, 4334);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Rollup_Detail', 14505, null, 4335);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Summary_Control', 14506, null, 4336);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Temp_Book_Transfers_Stg', 14507, null, 4337);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Temp_Incfcst_Depr_Alloc', 14508, null, 4338);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Temp_Transfer_Control', 14509, null, 4339);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Temp_Trid_Deletes', 14510, null, 4340);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Test_CPR_Activity', 14511, null, 4341);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Test_Pend_Basis', 14512, null, 4342);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Test_Pend_Transaction', 14513, null, 4343);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Test_Version', 14514, null, 4344);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Trans_Audit_Trail', 14515, null, 4345);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Trans_Audit_Trail_Grp', 14516, null, 4346);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Transfer_Control', 14517, null, 4347);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Type_of_Property', 14518, null, 4348);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Utility_Account', 14519, null, 4349);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Vintage_Convention', 14520, null, 4350);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Vintage_Translate', 14521, null, 4351);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Xfer_Traceback_Final', 14522, null, 4352);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Xfer_Traceback_Stg', 14523, null, 4353);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tax_Year_Version', 14524, null, 4354);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Te_Aggregation', 14525, null, 4355);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Accrual_Work_Order', 14526, null, 4356);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Archive_Work_Order_No', 14527, null, 4357);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_ARO_Global_Temp_Table_', 14528, null, 4358);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_ARO_Liability_Accr_Dtl_Global_Temp_Table_', 14529, null, 4359);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Asset_Global_Temp_Table_', 14530, null, 4360);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Bill_Group_Global_Temp_Table_', 14531, null, 4361);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Budget', 14532, null, 4362);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Budget_Version', 14533, null, 4363);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Charge_Summary_Closings_Global_Temp_Table_', 14534, null, 4364);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Compress_CPR_Act', 14535, null, 4365);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Cons_List', 14536, null, 4366);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Cons_List_New', 14537, null, 4367);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_CPR_Query_ID_Change', 14538, null, 4368);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Denomination', 14539, null, 4369);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Equip_Ledger_Global_Temp_Table_', 14540, null, 4370);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Exclude_Table_List', 14541, null, 4371);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_G_Def_Income_Tax_Trans_Global_Temp_Table_', 14542, null, 4372);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Global_Company_ID_Tbl_Global_Temp_Table_', 14543, null, 4373);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Global_Tax_Basis_Amount_Global_Temp_Table_', 14544, null, 4374);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Global_Tax_Book_Tbl_Global_Temp_Table_', 14545, null, 4375);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Global_Tax_Class_Tbl_Global_Temp_Table_', 14546, null, 4376);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Global_Vintage_Tbl_Global_Temp_Table_', 14547, null, 4377);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Ind_List', 14548, null, 4378);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Ind_List_New', 14549, null, 4379);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Job_Task_Global_Temp_Table_', 14550, null, 4380);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_LDAP_Attrib_List_Global_Temp_Table_', 14551, null, 4381);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_LDAP_Search_Global_Temp_Table_', 14552, null, 4382);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_MyPP_Report_ID_Change', 14553, null, 4383);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_MyPP_Todo_ID_Change', 14554, null, 4384);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Norm_ID_Tbl_Global_Temp_Table_', 14555, null, 4385);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_PP_AnyQuery_ID_Change', 14556, null, 4386);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Procedure_List', 14557, null, 4387);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Query_Dw_ID_Change', 14558, null, 4388);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Report_Filter_ID_Change', 14559, null, 4389);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Security_Rule_Global_Temp_Table_', 14560, null, 4390);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Set_of_Books_Global_Temp_Table_', 14561, null, 4391);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Table_List', 14562, null, 4392);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Table_List_New', 14563, null, 4393);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Task_ID_Change', 14564, null, 4394);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Verify_Cat_ID_Change', 14565, null, 4395);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Verify_ID_Change', 14566, null, 4396);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_View_List_New', 14567, null, 4397);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_WOCG', 14568, null, 4398);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Work_Order_Global_Temp_Table_', 14569, null, 4399);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Temp_Work_Order_Control_Global_Temp_Table_', 14570, null, 4400);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Template_Basis', 14571, null, 4401);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Template_Depr', 14572, null, 4402);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tmp_Companies_Global_Temp_Table_', 14573, null, 4403);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Tmp_Ta_Sub_Ext_Roll', 14574, null, 4404);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Town', 14575, null, 4405);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Trans_Line_Number', 14576, null, 4406);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Trans_Line_Stats', 14577, null, 4407);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Transaction_Input_Type', 14578, null, 4408);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Trepair_Funding_Proj_Charges', 14579, null, 4409);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Trepairs_Wo_Compare', 14580, null, 4410);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Trepairs_Wo_CPR', 14581, null, 4411);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Trepairs_WO_CWIP', 14582, null, 4412);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Type_Size', 14583, null, 4413);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unadjusted_Plant_History', 14584, null, 4414);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_Alloc_Basis', 14585, null, 4415);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_Alloc_Meth_Control', 14586, null, 4416);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_Alloc_Method', 14587, null, 4417);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_Allocation', 14588, null, 4418);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_Item_Class_Code', 14589, null, 4419);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_of_Measure', 14590, null, 4420);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_of_Production_Type', 14591, null, 4421);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unit_Target_Control', 14592, null, 4422);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitization_Target', 14593, null, 4423);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitization_Tolerance', 14594, null, 4424);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitization_Tolerance_Ct_Excl', 14595, null, 4425);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitization_Tolerance_Ect_Excl', 14596, null, 4426);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitize_Alloc_CGC_Inserts', 14597, null, 4427);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitize_Alloc_Errors', 14598, null, 4428);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitize_Alloc_List', 14599, null, 4429);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitize_Alloc_Stnd_Stats', 14600, null, 4430);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitize_Alloc_Unit_Item_Stats', 14601, null, 4431);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitize_Pend_Trans_Temp', 14602, null, 4432);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitize_Wo_List', 14603, null, 4433);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitized_Wo_List_Loop', 14604, null, 4434);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitized_Wo_Single', 14605, null, 4435);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitized_Wo_Units_Stg', 14606, null, 4436);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitized_Work_Order', 14607, null, 4437);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitized_Work_Order_Memo', 14608, null, 4438);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitized_Work_Order_Sl', 14609, null, 4439);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Unitized_Work_Order_SL_Basis', 14610, null, 4440);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Update_With_Actuals_Excl_ET', 14611, null, 4441);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Update_With_Actuals_Exclusion', 14612, null, 4442);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Use_Indicator', 14613, null, 4443);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Util_Acct_Prop_Unit', 14614, null, 4444);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Utility_Account', 14615, null, 4445);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Utility_Account_Depreciation', 14616, null, 4446);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Utility_Account_NARUC', 14617, null, 4447);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Utility_Acct_Depreciation_FP', 14618, null, 4448);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Valuation', 14619, null, 4449);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Version', 14620, null, 4450);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Vintage', 14621, null, 4451);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Vintage_Survivors', 14622, null, 4452);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Calc', 14623, null, 4453);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Charges', 14624, null, 4454);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Extensions', 14625, null, 4455);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Input', 14626, null, 4456);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Pend_Trans_Temp_Global_Temp_Table_', 14627, null, 4457);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Pending_Trans', 14628, null, 4458);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Rate', 14629, null, 4459);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Temp_WO_Global_Temp_Table_', 14630, null, 4460);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_Unit_Calc', 14631, null, 4461);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_WO_Override', 14632, null, 4462);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Comp_WO_Type', 14633, null, 4463);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WIP_Computation', 14634, null, 4464);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Accrual_Exc_Cost_Element', 14635, null, 4465);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Accrual_Session', 14636, null, 4466);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Accrual_Type', 14637, null, 4467);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Accrual_Type_Exp_Type', 14638, null, 4468);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Accruals', 14639, null, 4469);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Accruals_History', 14640, null, 4470);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Accrued_Gain_Loss', 14641, null, 4471);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Approval_Group', 14642, null, 4472);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Approval_Mult_Category', 14643, null, 4473);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Approval_Mult_Category_User', 14644, null, 4474);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Approval_Multiple', 14645, null, 4475);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Approval_Multiple_Arch', 14646, null, 4476);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Approval_Multiple_Default', 14647, null, 4477);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Alert_History', 14648, null, 4478);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Class', 14649, null, 4479);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Class_CL_Opt', 14650, null, 4480);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Class_WOT', 14651, null, 4481);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Results', 14652, null, 4482);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Rule_Class', 14653, null, 4483);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Rules', 14654, null, 4484);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Run_Control', 14655, null, 4485);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Run_Mode', 14656, null, 4486);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_ARC_Temp_Run_Global_Temp_Table_', 14657, null, 4487);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto101_Control', 14658, null, 4488);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Charges', 14659, null, 4489);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Charges_Sum', 14660, null, 4490);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Class_Codes', 14661, null, 4491);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Control', 14662, null, 4492);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Pending_Trans', 14663, null, 4493);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Pending_Trans_Arc', 14664, null, 4494);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Temp_Wos', 14665, null, 4495);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Auto106_Wos', 14666, null, 4496);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Bill_Material', 14667, null, 4497);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Clear_Over', 14668, null, 4498);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Clear_Over_Bdg', 14669, null, 4499);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Complete_Dates_Approve', 14670, null, 4500);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Dashboard', 14671, null, 4501);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Dashboard_Data', 14672, null, 4502);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Dashboard_Datawindow', 14673, null, 4503);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Dashboard_Defaults', 14674, null, 4504);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Dashboard_Security', 14675, null, 4505);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Dashboard_Type', 14676, null, 4506);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Delete', 14677, null, 4507);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Delete_Custom', 14678, null, 4508);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Comments', 14679, null, 4509);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Justification', 14680, null, 4510);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Justification_Bud_Sum', 14681, null, 4511);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Justification_Calc', 14682, null, 4512);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Justification_Rules', 14683, null, 4513);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Justification_Tabs', 14684, null, 4514);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Justification_Values', 14685, null, 4515);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Doc_Justification_WO_Type', 14686, null, 4516);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Documentation', 14687, null, 4517);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Eng_Est_Child', 14688, null, 4518);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Eng_Est_Detail', 14689, null, 4519);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Eng_Est_Detail1', 14690, null, 4520);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Eng_Estimate', 14691, null, 4521);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Eng_Estimate_Status', 14692, null, 4522);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Actuals_Temp_Temporary_Table_', 14693, null, 4523);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Actuals_Temp2_Temporary_Table_', 14694, null, 4524);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Closings_WOS', 14695, null, 4525);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Copy_Revision_Temp_Temporary_Table_', 14696, null, 4526);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Cotenant_Spread_Temp_Temporary_Table_', 14697, null, 4527);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Date_Changes', 14698, null, 4528);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Derivation_Pct', 14699, null, 4529);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Eng_Estimate1', 14700, null, 4530);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Forecast_Customize', 14701, null, 4531);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Forecast_Options', 14702, null, 4532);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Grid_User_Options', 14703, null, 4533);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Hierarchy', 14704, null, 4534);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Hierarchy_Map', 14705, null, 4535);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Load_Budgets_Temp', 14706, null, 4536);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly', 14707, null, 4537);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Arch', 14708, null, 4538);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_CR', 14709, null, 4539);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Default', 14710, null, 4540);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Escalation', 14711, null, 4541);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Escalation_Temp_Global_Temp_Table_', 14712, null, 4542);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_FY_Temp_Temporary_Table_', 14713, null, 4543);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Spread', 14714, null, 4544);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Spread_Arch', 14715, null, 4545);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Subs_Det', 14716, null, 4546);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Upload', 14717, null, 4547);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Monthly_Upload_Arch', 14718, null, 4548);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Processing_Mo_ID_Global_Temp_Table_', 14719, null, 4549);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Processing_Temp_Global_Temp_Table_', 14720, null, 4550);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Processing_Transpose_Global_Temp_Table_', 14721, null, 4551);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Rate_Default', 14722, null, 4552);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Rate_Filter', 14723, null, 4553);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Slide_Adjust_Temp_Temporary_Table_', 14724, null, 4554);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Slide_Results_Temp_Temporary_Table_', 14725, null, 4555);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Summary_Tbl', 14726, null, 4556);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Supplemental_Data', 14727, null, 4557);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Supplemental_Type', 14728, null, 4558);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Supplemental_Values', 14729, null, 4559);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Temp_Wem_From_We_Global_Temp_Table_', 14730, null, 4560);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Template', 14731, null, 4561);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Template_Control', 14732, null, 4562);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Template_Wo_Type', 14733, null, 4563);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Trans_Type', 14734, null, 4564);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Transpose_Temp_Temporary_Table_', 14735, null, 4565);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Update_Template', 14736, null, 4566);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Update_With_Act_Temp_Temporary_Table_', 14737, null, 4567);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Upload_New_Combos_Global_Temp_Table_', 14738, null, 4568);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Upload_New_Combos2_Global_Temp_Table_', 14739, null, 4569);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_Upload_New_Combos3_Global_Temp_Table_', 14740, null, 4570);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Est_WO_Eng_Est', 14741, null, 4571);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Estimate', 14742, null, 4572);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Estimate_Class_Code', 14743, null, 4573);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Estimate_Class_Code_Temp_Global_Temp_Table_', 14744, null, 4574);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Estimate_Import', 14745, null, 4575);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Estimate_Unit_Rollup_Temp_Global_Temp_Table_', 14746, null, 4576);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Estimated_Retire', 14747, null, 4577);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_GL_Account_Summary', 14748, null, 4578);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_GL_Account_Summary_MO', 14749, null, 4579);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_GL_Account_Summary_Preset', 14750, null, 4580);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Grp_WO_Type', 14751, null, 4581);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Image_Interface', 14752, null, 4582);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Monthly', 14753, null, 4583);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Monthly_Arc', 14754, null, 4584);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Monthly_Arc_Old', 14755, null, 4585);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Monthly_Ids', 14756, null, 4586);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Monthly_Temp', 14757, null, 4587);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Ocr_Arc', 14758, null, 4588);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Ocr_Results', 14759, null, 4589);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Ocr_Results1', 14760, null, 4590);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_OCR_Stg', 14761, null, 4591);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Revisions', 14762, null, 4592);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Staging', 14763, null, 4593);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Staging_Arc', 14764, null, 4594);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Unit', 14765, null, 4595);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Interface_Unit_Arc', 14766, null, 4596);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Material_Req', 14767, null, 4597);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Metric', 14768, null, 4598);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Metric_Company', 14769, null, 4599);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Metric_Detail', 14770, null, 4600);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Metric_Results', 14771, null, 4601);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Metric_Saved_Graph', 14772, null, 4602);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Overhead_DW_Name', 14773, null, 4603);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Overhead_Jur_Allo', 14774, null, 4604);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Overhead_Target_Type', 14775, null, 4605);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Overhead_Targets', 14776, null, 4606);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Process_Control', 14777, null, 4607);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Pseudo_Unitize_CC_Control', 14778, null, 4608);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Pseudo_Unitize_Summary', 14779, null, 4609);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Pseudo_Unitize_Summary_MO_Global_Temp_Table_', 14780, null, 4610);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Reimbursable', 14781, null, 4611);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Repair_Location_Metod', 14782, null, 4612);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Report_Dynamic_Subtotal', 14783, null, 4613);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Status_Trail', 14784, null, 4614);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Summary', 14785, null, 4615);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Tax_Expense_Test', 14786, null, 4616);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Tax_Status', 14787, null, 4617);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Type_Class_Code_Default', 14788, null, 4618);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Type_Clear_Dflt', 14789, null, 4619);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Type_Dept_Template', 14790, null, 4620);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Unit_Item_Pend_Trans', 14791, null, 4621);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Validation_Control', 14792, null, 4622);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Validation_Run', 14793, null, 4623);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\WO_Validation_Type', 14794, null, 4624);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Account', 14795, null, 4625);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Alternatives', 14796, null, 4626);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Approval', 14797, null, 4627);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Approval_Arch', 14798, null, 4628);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Cashflow', 14799, null, 4629);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Charge_Group', 14800, null, 4630);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Charge_Group_Temp', 14801, null, 4631);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Class_Code', 14802, null, 4632);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Control', 14803, null, 4633);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Department', 14804, null, 4634);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Document', 14805, null, 4635);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Funding_Proj_Type', 14806, null, 4636);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Group', 14807, null, 4637);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Group_Budget_Org', 14808, null, 4638);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Initiator', 14809, null, 4639);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Mass_Update', 14810, null, 4640);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Prereqs', 14811, null, 4641);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Status', 14812, null, 4642);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Tax_Status', 14813, null, 4643);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Type', 14814, null, 4644);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Type_Budget_Summary', 14815, null, 4645);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Type_Est_Import', 14816, null, 4646);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Order_Validation', 14817, null, 4647);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Situation', 14818, null, 4648);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Type', 14819, null, 4649);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Type_Attr_Value', 14820, null, 4650);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Work_Type_Attribute', 14821, null, 4651);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Workflow', 14822, null, 4652);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Workflow_Amount_SQL', 14823, null, 4653);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Workflow_Detail', 14824, null, 4654);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Workflow_Rule', 14825, null, 4655);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Workflow_Subsystem', 14826, null, 4656);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Workflow_Type', 14827, null, 4657);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Workflow_Type_Rule', 14828, null, 4658);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tables', 'Tables\Yes_No', 14829, null, 4659);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '1_1_Overview5', 17000, null, 4660);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '1_2_Elements_and_Definitions_of_Asset_Retirements_Obligations', 17001, null, 4661);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '1_3_Navigation_Using_the_ARO_Main_Toolbar', 17002, 'w_aro_top_main', 4662);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '1_4_ARO_Data_Requirements', 17003, null, 4663);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '10_1_ARO_Setup_under_IFRS', 17004, null, 4664);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '11_1_Overview2', 17005, null, 4665);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '11_2_ARO_with_Regulatory_Obligations', 17006, null, 4666);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '11_3_Using_the_Regulatory_Entry_Maintenance_Window', 17007, null, 4667);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_Creating_an_Asset_Retirement_Obligation', 17008, null, 4668);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_1_Step_1_Entering_ARO_Information', 17009, null, 4669);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_2_Step_2_Entering_ARO_Asset_Information', 17010, null, 4670);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_3_Negative_ARO_Asset_ARC_', 17011, null, 4671);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_4_Other_Anomalies', 17012, null, 4672);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_5_Relating_AROs_to_Assets', 17013, null, 4673);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_5_Relating_AROs_to_Assets__Ref225759028', 17014, null, 4674);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_1_6_Verifying_Depreciation_Groups', 17015, null, 4675);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_2_Using_the_ARO_Selection_Window', 17016, 'w_aro_main', 4676);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_2_1_ARO_Selection_Window', 17017, null, 4677);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_2_2_Using_the_ARO_Select_Taskbar', 17018, null, 4678);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '2_2_3_Viewing_ARO_Details', 17019, 'w_aro_details', 4679);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_1_Viewing_and_Creating_ARO_Layers', 17020, null, 4680);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_1_1_ARO_Layer_History_Window', 17021, 'w_aro_activity_history', 4681);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_1_2_Adding_a_New_Layer', 17022, null, 4682);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_1_3_Deleting_Layers', 17023, null, 4683);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_2_Estimating_and_Re_Estimating_Future_Cash_Flows_for_AROs', 17024, null, 4684);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_2_1_Layer_Information_Input_and_Calculation', 17025, 'w_aro_estimate', 4685);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_2_2_Creating_Viewing_Editing_Cash_Flow_Items', 17026, null, 4686);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_2_3_ARO_Rate_Types', 17027, null, 4687);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_2_4_Calculating_Future_Cash_Flows__Mass_Type', 17028, null, 4688);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '3_2_5_Adjusting_Stream_Probabilities', 17029, null, 4689);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_1_Overview3', 17030, null, 4690);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_2_ARO_Settlement_Window', 17031, 'w_aro_settlement', 4691);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_3_Settlement_History_and_Estimated_Future_Cash_Flows', 17032, 'w_aro_settlement_history', 4692);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_4_Assigning_Work_Orders_to_an_ARO', 17033, 'w_aro_work_order', 4693);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_5_Directing_ARO_Work_Orders_to_Specific_Layers', 17034, null, 4694);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_6_Prior_Layer_Settlement_Adjustment', 17035, null, 4695);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_7_ARC_Auto_Retirements', 17036, null, 4696);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '4_8_Mass_ARO_Year_End_Processing', 17037, null, 4697);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_1_Assigning_Documents_to_an_ARO', 17038, 'w_aro_open_doc', 4698);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_1_1_ARO_Documents_Window', 17039, null, 4699);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_2_Deleting_AROs', 17040, null, 4700);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_3_ARO_Adjustments', 17041, 'w_aro_liability_adjust', 4701);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_3_1_ARO_Liability_Adjustments_Window', 17042, null, 4702);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_3_2_ARO_Liability_Adjustment_Reports', 17043, null, 4703);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '5_4_Multi_ARO_Estimate_Tool', 17044, 'w_aro_est_multi', 4704);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_1_Overview2', 17045, 'w_aro_transition', 4705);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_2_Using_the_ARO_Transition_Taskbar', 17046, null, 4706);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_3_Accounting_Adjustment_Account', 17047, null, 4707);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_4_Transition_Dates_for_Estimating_Future_Cash_Flows', 17048, null, 4708);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_5_Processing_Transition_AROs', 17049, null, 4709);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_6_Transition_Reporting', 17050, null, 4710);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '6_7_Depreciation_Activity_Input', 17051, 'w_find_depr_group', 4711);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_1_Overview2', 17052, 'w_aro_forecast', 4712);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_2_ARO_Forecast_Window', 17053, null, 4713);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_3_Using_the_ARO_Forecast_Taskbar', 17054, null, 4714);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_4_ARO_Forecast_Details', 17055, null, 4715);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_5_Forecast_Dates_for_Estimating_Future_Cash_Flows', 17056, null, 4716);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_6_Making_a_Forecast_ARO_Real', 17057, null, 4717);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_7_Forecast_Run_Outs', 17058, null, 4718);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '7_8_Forecast_Reporting', 17059, null, 4719);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_1_Overview3', 17060, 'w_aro_preprocessor', 4720);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_2_Using_the_ARO_Pre_Processor_Taskbar', 17061, null, 4721);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_3_ARO_Pre_Processor_Configuration', 17062, null, 4722);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_4_ARO_Pre_Processor_Scenario_Summary_Details_', 17063, null, 4723);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_5_Using_the_ARO_Pre_Processor_Detail_Taskbar', 17064, null, 4724);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_6_ARO_Pre_Processor_Mapping', 17065, null, 4725);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_7_ARO_Pre_Processor_Probabilities', 17066, null, 4726);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_8_ARO_Pre_Processor_Import_Cash_Flows_', 17067, null, 4727);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_9_Auto_Generate_Scenarios', 17068, null, 4728);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_9_1_ARO_Pre_Processor_Review_Window_Results_', 17069, null, 4729);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '8_9_1_ARO_Pre_Processor_Review_Window_Results_ARO_Pre_Processor', 17070, null, 4730);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_1_ARO_Reporting', 17071, null, 4731);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_1_1_ARO_Reporting_Selection_Main_Menu_Bar_', 17072, null, 4732);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_2_ARO_Month_End_Processing', 17073, null, 4733);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', '9_2_1_Continuing_Property_Records_Control_Window', 17074, null, 4734);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_1_Introduction_to_Asset_Retirement_Obligations', 17075, null, 4735);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_10_IFRS_Considerations', 17076, null, 4736);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_11_Regulatory_Accounting_Entries', 17077, null, 4737);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_2_Setting_Up_AROs', 17078, null, 4738);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_3_ARO_Layers_and_Cash_Flows', 17079, null, 4739);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_4_Settlement_Processing_for_AROs', 17080, null, 4740);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_5_Miscellaneous_ARO_Activity', 17081, null, 4741);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_6_ARO_Transition_Process', 17082, null, 4742);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_7_ARO_Forecasting_Process', 17083, null, 4743);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_8_ARO_Pre_Processor', 17084, null, 4744);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'asset management', 'Chapter_9_ARO_Reporting_and_Month_End_Processing', 17085, null, 4745);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '1_1_Overview6', 18000, null, 4746);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '1_1_Overview6', 18000, null, 4747);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '10_1_Overview3', 18001, null, 4748);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '10_1_Overview3', 18001, null, 4749);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '10_2_Using_the_Work_Order_Balances_Window', 18002, 'w_wo_query', 4750);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '10_2_Using_the_Work_Order_Balances_Window', 18002, 'w_wo_query', 4751);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '10_2_1_Work_Order_Balances__Expenditure_Type_View', 18003, null, 4752);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '10_2_1_Work_Order_Balances__Expenditure_Type_View', 18003, null, 4753);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '10_2_2_Work_Order_Balances__Charge_Type_View', 18004, null, 4754);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '10_2_2_Work_Order_Balances__Charge_Type_View', 18004, null, 4755);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '10_2_3_Work_Order_Balances__Charges_View', 18005, null, 4756);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '10_2_3_Work_Order_Balances__Charges_View', 18005, null, 4757);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '11_1_Overview3', 18006, null, 4758);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '11_1_Overview3', 18006, null, 4759);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '11_2_Using_the_Work_Order_Charges_Window', 18007, 'w_wo_charge_select', 4760);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '11_2_Using_the_Work_Order_Charges_Window', 18007, 'w_wo_charge_select', 4761);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '11_3_Charge_Information', 18008, 'w_charge_detail', 4762);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '11_3_Charge_Information', 18008, 'w_charge_detail', 4763);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '11_4_Work_Order__Journal_Entries_Create_JE_', 18009, 'w_wo_charge_je', 4764);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '11_4_Work_Order__Journal_Entries_Create_JE_', 18009, 'w_wo_charge_je', 4765);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '11_5_Work_Order_Charge_Entry_Add_Charges_', 18010, 'w_wo_charge_add', 4766);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '11_5_Work_Order_Charge_Entry_Add_Charges_', 18010, 'w_wo_charge_add', 4767);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '11_6_Work_Order_Adjustments', 18011, 'w_wo_charge_adjust', 4768);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '11_6_Work_Order_Adjustments', 18011, 'w_wo_charge_adjust', 4769);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '12_1_Overview1', 18012, null, 4770);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '12_1_Overview1', 18012, null, 4771);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '12_2_Using_the_Work_Order_Commitments_Window', 18013, 'w_wo_commitment_select', 4772);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '12_2_Using_the_Work_Order_Commitments_Window', 18013, 'w_wo_commitment_select', 4773);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '12_2_1_Commitment_Information', 18014, null, 4774);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '12_2_1_Commitment_Information', 18014, null, 4775);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '12_2_2_Work_Order_Commitment_Entry_Add_Commitment_', 18015, null, 4776);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '12_2_2_Work_Order_Commitment_Entry_Add_Commitment_', 18015, null, 4777);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '12_2_3_Work_Order_Commitment_Adjustments', 18016, null, 4778);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '12_2_3_Work_Order_Commitment_Adjustments', 18016, null, 4779);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '12_2_4_Work_Order_Commitment_Offset_Manual_Matching_', 18017, null, 4780);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '12_2_4_Work_Order_Commitment_Offset_Manual_Matching_', 18017, null, 4781);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_1_Using_Class_Codes', 18018, null, 4782);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_1_Using_Class_Codes', 18018, null, 4783);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_1_1_Overview', 18019, null, 4784);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_1_1_Overview', 18019, null, 4785);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_1_2_Using_the_Work_Order_Class_Code_Update_Window', 18020, 'w_project_class_code', 4786);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_1_2_Using_the_Work_Order_Class_Code_Update_Window', 18020, 'w_project_class_code', 4787);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_2_Using_Work_Order_Dashboards', 18021, null, 4788);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_2_Using_Work_Order_Dashboards', 18021, null, 4789);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_2_1_Overview', 18022, 'w_dashboard', 4790);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_2_1_Overview', 18022, 'w_dashboard', 4791);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_2_2_Setting_Up_Project_Management_Dashboards_for_a_User', 18023, 'w_dashboard_setup_single_user', 4792);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_2_2_Setting_Up_Project_Management_Dashboards_for_a_User', 18023, 'w_dashboard_setup_single_user', 4793);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_3_Work_Order_Accruals', 18024, null, 4794);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_3_Work_Order_Accruals', 18024, null, 4795);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_3_1_Overview', 18025, null, 4796);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_3_1_Overview', 18025, null, 4797);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_3_2_Accrual_Types', 18026, null, 4798);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_3_2_Accrual_Types', 18026, null, 4799);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_3_3_Attaching_an_Accrual_Type_to_a_Funding_Project_or_Work_Order', 18027, 'w_accrual_setup', 4800);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_3_3_Attaching_an_Accrual_Type_to_a_Funding_Project_or_Work_Order', 18027, 'w_accrual_setup', 4801);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_3_4_Preparing_a_Work_Order_or_Funding_Project_for_the_Accrual_Calculation', 18028, null, 4802);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_3_4_Preparing_a_Work_Order_or_Funding_Project_for_the_Accrual_Calculation', 18028, null, 4803);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '13_4_Using_the_Work_Order_Mass_Update_Window', 18029, 'w_wo_mass_update', 4804);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '13_4_Using_the_Work_Order_Mass_Update_Window', 18029, 'w_wo_mass_update', 4805);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_1_Using_the_Related_Documents_Window', 18030, null, 4806);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_1_Using_the_Related_Documents_Window', 18030, null, 4807);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_2_Database_Documents', 18031, null, 4808);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_2_Database_Documents', 18031, null, 4809);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_2_1_Viewing_Attaching_External_Documents', 18032, 'w_wo_open_doc_db', 4810);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_2_1_Viewing_Attaching_External_Documents', 18032, 'w_wo_open_doc_db', 4811);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_2_2_Attaching_a_Document_to_the_Work_Order_Header', 18033, null, 4812);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_2_2_Attaching_a_Document_to_the_Work_Order_Header', 18033, null, 4813);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_2_3_Attaching_a_Document_to_a_Justification_Document', 18034, null, 4814);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_2_3_Attaching_a_Document_to_a_Justification_Document', 18034, null, 4815);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_2_4_Attaching_a_Document_to_an_Accrual', 18035, null, 4816);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_2_4_Attaching_a_Document_to_an_Accrual', 18035, null, 4817);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_3_Network_Document_Attachment', 18036, null, 4818);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_3_Network_Document_Attachment', 18036, null, 4819);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_3_1_Viewing_External_Documents', 18037, null, 4820);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_3_1_Viewing_External_Documents', 18037, null, 4821);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '14_3_2_Attaching_an_External_Document', 18038, null, 4822);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '14_3_2_Attaching_an_External_Document', 18038, null, 4823);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_1_Overview1', 18039, null, 4824);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_1_Overview1', 18039, null, 4825);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_2_Navigation', 18040, null, 4826);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_2_Navigation', 18040, null, 4827);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_2_1_Using_the_Job_Task_Taskbar', 18041, null, 4828);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_2_1_Using_the_Job_Task_Taskbar', 18041, null, 4829);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_2_2_Using_the_Job_Task_Selection_Taskbar', 18042, null, 4830);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_2_2_Using_the_Job_Task_Selection_Taskbar', 18042, null, 4831);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_3_Initiating_Job_Tasks', 18043, 'w_task_entry', 4832);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_3_Initiating_Job_Tasks', 18043, 'w_task_entry', 4833);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_Job_Task_Selection', 18044, null, 4834);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_Job_Task_Selection', 18044, null, 4835);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_1_Using_the_Job_Task_Selection_Window', 18045, 'w_task_select_tabs', 4836);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_1_Using_the_Job_Task_Selection_Window', 18045, 'w_task_select_tabs', 4837);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_2_Using_the_Job_Task_Detail_Window', 18046, 'w_task_detail', 4838);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_2_Using_the_Job_Task_Detail_Window', 18046, 'w_task_detail', 4839);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_3_Using_the_Job_Task_Estimates_Window', 18047, null, 4840);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_3_Using_the_Job_Task_Estimates_Window', 18047, null, 4841);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_4_Using_the_Job_Task_Charges_Window', 18048, 'w_task_charge_select', 4842);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_4_Using_the_Job_Task_Charges_Window', 18048, 'w_task_charge_select', 4843);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_5_Using_the_Job_Task_Commitments_Window', 18049, 'w_task_commit_select', 4844);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_5_Using_the_Job_Task_Commitments_Window', 18049, 'w_task_commit_select', 4845);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_6_Using_the_Job_Task_Forecast_Window', 18050, null, 4846);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_6_Using_the_Job_Task_Forecast_Window', 18050, null, 4847);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_7_Using_Class_Codes', 18051, null, 4848);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_7_Using_Class_Codes', 18051, null, 4849);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '15_4_8_Using_the_Job_Task_Mass_Update_Window', 18052, 'w_job_task_mass_update', 4850);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '15_4_8_Using_the_Job_Task_Mass_Update_Window', 18052, 'w_job_task_mass_update', 4851);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_1_Overview', 18053, null, 4852);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_1_Overview', 18053, null, 4853);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_1_1_ARC__Automatic_Review_for_Closing', 18054, null, 4854);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_1_1_ARC__Automatic_Review_for_Closing', 18054, null, 4855);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_1_2_Non_Unitized_Close_Processing', 18055, null, 4856);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_1_2_Non_Unitized_Close_Processing', 18055, null, 4857);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_1_3_Work_Order_Completion_and_Work_Order_Retirement_Processing', 18056, null, 4858);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_1_3_Work_Order_Completion_and_Work_Order_Retirement_Processing', 18056, null, 4859);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_1_4_Work_Order_Unitization', 18057, null, 4860);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_1_4_Work_Order_Unitization', 18057, null, 4861);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_2_Using_the_Work_Order_Completion_Window', 18058, 'w_wo_close', 4862);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_2_Using_the_Work_Order_Completion_Window', 18058, 'w_wo_close', 4863);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_Using_the_Work_Order_Unitization_Window', 18059, null, 4864);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_Using_the_Work_Order_Unitization_Window', 18059, null, 4865);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_1_Charge_Groups', 18060, null, 4866);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_1_Charge_Groups', 18060, null, 4867);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_10Other_Customized_Unitization_Audits', 18061, null, 4868);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_10Other_Customized_Unitization_Audits', 18061, null, 4869);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_11Unitization_of_Items_Maintained_on_a_Subledger', 18062, null, 4870);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_11Unitization_of_Items_Maintained_on_a_Subledger', 18062, null, 4871);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_12Late_Charge_Unitization', 18063, null, 4872);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_12Late_Charge_Unitization', 18063, null, 4873);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_2_Unitized_Charges_Unit_Items_', 18064, null, 4874);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_2_Unitized_Charges_Unit_Items_', 18064, null, 4875);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_3_Unit_Item_Details', 18065, null, 4876);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_3_Unit_Item_Details', 18065, null, 4877);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_3_Unit_Item_Details_Copying_an_Existing_Unit', 18066, null, 4878);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_3_Unit_Item_Details_Copying_an_Existing_Unit', 18066, null, 4879);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_3_Unit_Item_Details_Creating_New_Unit_Items', 18067, null, 4880);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_3_Unit_Item_Details_Creating_New_Unit_Items', 18067, null, 4881);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_3_Unit_Item_Details_Deleting_a_Unit_Item', 18068, null, 4882);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_3_Unit_Item_Details_Deleting_a_Unit_Item', 18068, null, 4883);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_4_Assigning_Charge_Groups_to_Unit_Items_Drag_Drop_', 18069, null, 4884);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_4_Assigning_Charge_Groups_to_Unit_Items_Drag_Drop_', 18069, null, 4885);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_5_Splitting_a_Charge_Group_using_Targets', 18070, null, 4886);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_5_Splitting_a_Charge_Group_using_Targets', 18070, null, 4887);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_6_Allocating_Remaining_Charge_Groups', 18071, null, 4888);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_6_Allocating_Remaining_Charge_Groups', 18071, null, 4889);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_7_Automatic_Unitization', 18072, null, 4890);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_7_Automatic_Unitization', 18072, null, 4891);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_8_Joint_Work_Order_Unitization', 18073, 'w_wo_co_tenancy_wo', 4892);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_8_Joint_Work_Order_Unitization', 18073, 'w_wo_co_tenancy_wo', 4893);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '16_3_9_Unitization_Tolerance', 18074, null, 4894);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '16_3_9_Unitization_Tolerance', 18074, null, 4895);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '17_1_Overview', 18075, null, 4896);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '17_1_Overview', 18075, null, 4897);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '17_2_Using_the_Work_Order_Retirements_Window', 18076, 'w_wo_close_retire', 4898);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '17_2_Using_the_Work_Order_Retirements_Window', 18076, 'w_wo_close_retire', 4899);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '17_2_Using_the_Work_Order_Retirements_Window_Step_2_Selecting_the', 18077, null, 4900);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '17_2_Using_the_Work_Order_Retirements_Window_Step_2_Selecting_the', 18077, null, 4901);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '17_2_Using_the_Work_Order_Retirements_Window_Step_3_Using_the_CPR', 18078, null, 4902);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '17_2_Using_the_Work_Order_Retirements_Window_Step_3_Using_the_CPR', 18078, null, 4903);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_1_Overview', 18079, null, 4904);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_1_Overview', 18079, null, 4905);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_10_Analyzing_Work_Order_Actuals_vs_Estimate_Amounts', 18080, 'w_fp_actual_vs_budget', 4906);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_10_Analyzing_Work_Order_Actuals_vs_Estimate_Amounts', 18080, 'w_wo_actual_vs_estimate', 4907);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_General_Navigation', 18081, null, 4908);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_General_Navigation', 18081, null, 4909);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_1_General_Navigation_Dollar_Attributes', 18082, null, 4910);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_1_General_Navigation_Dollar_Attributes', 18082, null, 4911);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_2_General_Navigation_Header_Attributes', 18083, null, 4912);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_2_General_Navigation_Header_Attributes', 18083, null, 4913);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_3_General_Navigation_Class_Code_Filter_Options', 18084, null, 4914);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_3_General_Navigation_Class_Code_Filter_Options', 18084, null, 4915);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_4_General_Navigation_Subtotals', 18085, null, 4916);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_4_General_Navigation_Subtotals', 18085, null, 4917);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_5_General_Navigation_Build_Filters', 18086, null, 4918);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_5_General_Navigation_Build_Filters', 18086, null, 4919);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_6_General_Navigation_Query_Results', 18087, null, 4920);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_6_General_Navigation_Query_Results', 18087, null, 4921);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_7_General_Navigation_Saving_Your_Query', 18088, null, 4922);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_7_General_Navigation_Saving_Your_Query', 18088, null, 4923);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_2_8_General_Navigation_Restoring_a_Saved_Query', 18089, null, 4924);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_2_8_General_Navigation_Restoring_a_Saved_Query', 18089, null, 4925);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_3_Work_Order_Charges_Query_Tool', 18090, 'w_cwip_charge_dollars_wo', 4926);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_3_Work_Order_Charges_Query_Tool', 18090, 'w_cwip_charge_dollars_wo', 4927);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_4_Funding_Project_Charges_Query_Tool', 18091, null, 4928);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_4_Funding_Project_Charges_Query_Tool', 18091, null, 4929);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_5_Work_Order_Estimates_Query_Tool', 18092, null, 4930);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_5_Work_Order_Estimates_Query_Tool', 18092, null, 4931);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_6_Funding_Project_Estimates_Query_Tool', 18093, null, 4932);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_6_Funding_Project_Estimates_Query_Tool', 18093, null, 4933);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_7_Estimate_vs_Actual_Query_Tools', 18094, null, 4934);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_7_Estimate_vs_Actual_Query_Tools', 18094, null, 4935);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_7_1_Estimate_vs_Actual_Query_Tools_Computed_Field', 18095, null, 4936);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_7_1_Estimate_vs_Actual_Query_Tools_Computed_Field', 18095, null, 4937);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_7_2_Estimate_vs_Actual_Query_Tools_Advanced_Computed_Field', 18096, null, 4938);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_7_2_Estimate_vs_Actual_Query_Tools_Advanced_Computed_Field', 18096, null, 4939);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_7_3_Estimate_vs_Actual_Query_Tools_Build_Filter_Tab', 18097, null, 4940);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_7_3_Estimate_vs_Actual_Query_Tools_Build_Filter_Tab', 18097, null, 4941);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_7_4_Estimate_vs_Actual_Query_Tools_Modify_Query_Results', 18098, null, 4942);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_7_4_Estimate_vs_Actual_Query_Tools_Modify_Query_Results', 18098, null, 4943);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_7_5_Estimate_vs_Actual_Query_Tools_Work_Order', 18099, null, 4944);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_7_5_Estimate_vs_Actual_Query_Tools_Work_Order', 18099, null, 4945);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_7_6_Estimate_vs_Actual_Query_Tools_Funding_Project', 18100, null, 4946);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_7_6_Estimate_vs_Actual_Query_Tools_Funding_Project', 18100, null, 4947);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_8_Work_Order_Funding_Project_Any_Query_User_Defined_Query_Window', 18101, null, 4948);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_8_Work_Order_Funding_Project_Any_Query_User_Defined_Query_Window', 18101, null, 4949);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '18_9_Analyzing_Actuals_vs_Budgeted_Amounts', 18102, null, 4950);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '18_9_Analyzing_Actuals_vs_Budgeted_Amounts', 18102, null, 4951);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '19_1_Overview', 18103, null, 4952);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '19_1_Overview', 18103, null, 4953);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '19_2_PowerPlan_Reporting_Window__Work_Order_Summary', 18104, null, 4954);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '19_2_PowerPlan_Reporting_Window__Work_Order_Summary', 18104, null, 4955);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '19_2_1_PowerPlan_Reporting_Report_Selection_Tab', 18105, null, 4956);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '19_2_1_PowerPlan_Reporting_Report_Selection_Tab', 18105, null, 4957);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '19_2_2_PowerPlan_Reporting_Filter_Tab', 18106, null, 4958);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '19_2_2_PowerPlan_Reporting_Filter_Tab', 18106, null, 4959);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '19_2_3_PowerPlan_Reporting', 18107, null, 4960);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '19_2_3_PowerPlan_Reporting', 18107, null, 4961);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '19_3_PowerPlan_Reporting_Window__Work_Order_Detail', 18108, null, 4962);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '19_3_PowerPlan_Reporting_Window__Work_Order_Detail', 18108, null, 4963);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '19_4_Reporting_Structure_and_Modification', 18109, null, 4964);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '19_4_Reporting_Structure_and_Modification', 18109, null, 4965);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '2_1_Overview3', 18110, 'w_project_main', 4966);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '2_1_Overview3', 18110, 'w_project_main', 4967);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '2_2_Using_the_Project_Management_Toolbar', 18111, 'w_project_config', 4968);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '2_2_Using_the_Project_Management_Toolbar', 18111, 'w_project_config', 4969);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '2_3_Using_the_Project_Configuration_Taskbar', 18112, null, 4970);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '2_3_Using_the_Project_Configuration_Taskbar', 18112, null, 4971);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '2_4_Using_the_Work_Order_Toolbar', 18113, 'w_work_main', 4972);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '2_4_Using_the_Work_Order_Toolbar', 18113, 'w_work_main', 4973);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '2_5_Using_the_Work_Order_Selection_Taskbar', 18114, null, 4974);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '2_5_Using_the_Work_Order_Selection_Taskbar', 18114, null, 4975);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_1_Overview', 18115, null, 4976);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_1_Overview', 18115, null, 4977);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_10_Using_the_AFUDC_Rate_Calculation_Worksheet_Window', 18116, 'w_afudc_rate_calc', 4978);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_10_Using_the_AFUDC_Rate_Calculation_Worksheet_Window', 18116, 'w_afudc_rate_calc', 4979);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_11_Guide_to_Running_CPI_Retro_Calculation', 18117, 'w_cpi_retro_calc', 4980);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_11_Guide_to_Running_CPI_Retro_Calculation', 18117, 'w_cpi_retro_calc', 4981);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_11_1Adding_Rates', 18118, null, 4982);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_11_1Adding_Rates', 18118, null, 4983);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_11_2Running_the_CPI_Retro_Calculation_Window', 18119, null, 4984);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_11_2Running_the_CPI_Retro_Calculation_Window', 18119, null, 4985);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_12_Accrual_Calculation', 18120, null, 4986);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_12_Accrual_Calculation', 18120, null, 4987);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_12_1Overview', 18121, null, 4988);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_12_1Overview', 18121, null, 4989);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_13_Auto_101_Running_options', 18122, null, 4990);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_13_Auto_101_Running_options', 18122, null, 4991);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_14_Auto_101__Automatic_Unitization_Kickouts', 18123, null, 4992);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_14_Auto_101__Automatic_Unitization_Kickouts', 18123, null, 4993);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_14_1Using_the_Automatic_Unitization__Error_Messages_Window', 18124, 'w_wo_auto101_control', 4994);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_14_1Using_the_Automatic_Unitization__Error_Messages_Window', 18124, 'w_wo_auto101_control', 4995);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_15_Auto_106__Non_Unitized_Kickouts', 18125, null, 4996);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_15_Auto_106__Non_Unitized_Kickouts', 18125, null, 4997);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_15_1Using_the_Automatic_106__Error_Messages_Window', 18126, 'w_wo_auto106_control', 4998);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_15_1Using_the_Automatic_106__Error_Messages_Window', 18126, 'w_wo_auto106_control', 4999);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_2_Using_the_Work_Order_Monthly_Closing_Window', 18127, 'w_wo_control', 5000);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_2_Using_the_Work_Order_Monthly_Closing_Window', 18127, 'w_wo_control', 5001);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_2_1_COR_SALV_Processing', 18128, null, 5002);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_2_1_COR_SALV_Processing', 18128, null, 5003);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_2_2_Using_Before_and_After_AFUDC_Overheads', 18129, null, 5004);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_2_2_Using_Before_and_After_AFUDC_Overheads', 18129, null, 5005);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_3_Using_the_Work_Order_Control_Taskbar', 18130, null, 5006);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_3_Using_the_Work_Order_Control_Taskbar', 18130, null, 5007);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_4_AFUDC_Interest_and_Construction_Period_Interest_Tax_', 18131, null, 5008);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_4_AFUDC_Interest_and_Construction_Period_Interest_Tax_', 18131, null, 5009);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_4_1_Overview', 18132, null, 5010);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_4_1_Overview', 18132, null, 5011);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_4_2_CPI_Considerations', 18133, null, 5012);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_4_2_CPI_Considerations', 18133, null, 5013);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_5_Capitalized_Interest_Work_Orders', 18134, null, 5014);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_5_Capitalized_Interest_Work_Orders', 18134, null, 5015);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_5_1_Entering_Cap_Structure_for_the_Cap_Interest_Calculation', 18135, null, 5016);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_5_1_Entering_Cap_Structure_for_the_Cap_Interest_Calculation', 18135, null, 5017);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_5_2_Creating_Parent_Work_Orders_for_the_Cap_Interest_Calculation', 18136, null, 5018);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_5_2_Creating_Parent_Work_Orders_for_the_Cap_Interest_Calculation', 18136, null, 5019);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_6_Using_the_Input_AFUDC_Window', 18137, 'w_afudc_input', 5020);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_6_Using_the_Input_AFUDC_Window', 18137, 'w_afudc_input', 5021);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_7_Using_the_Input_AFUDC_Ratios_Window', 18138, 'w_afudc_input_ratio', 5022);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_7_Using_the_Input_AFUDC_Ratios_Window', 18138, 'w_afudc_input_ratio', 5023);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_8_Using_the_CWIP_in_Base_Window', 18139, 'w_cwip_in_rate_base', 5024);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_8_Using_the_CWIP_in_Base_Window', 18139, 'w_cwip_in_rate_base', 5025);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '20_9_Using_the_Retroactive_AFUDC_Adjustments_Window', 18140, 'w_afudc_adjust', 5026);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '20_9_Using_the_Retroactive_AFUDC_Adjustments_Window', 18140, 'w_afudc_adjust', 5027);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '21_1_Exhibit_A__IRC_Section_263A', 18141, null, 5028);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '21_1_Exhibit_A__IRC_Section_263A', 18141, null, 5029);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '21_2_Exhibit_B__Regulation_Section_1_263A_9_f_1_', 18142, null, 5030);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '21_2_Exhibit_B__Regulation_Section_1_263A_9_f_1_', 18142, null, 5031);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '21_3_Exhibit_C__Regulation_Section_1_263A_9_f_2_', 18143, null, 5032);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '21_3_Exhibit_C__Regulation_Section_1_263A_9_f_2_', 18143, null, 5033);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_1_Overview3', 18144, null, 5034);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_1_Overview3', 18144, null, 5035);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_2_Work_Order_Types', 18145, 'w_pp_table_entry_wo_type', 5036);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_2_Work_Order_Types', 18145, 'w_pp_table_entry_wo_type', 5037);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_3_Work_Order_Groups', 18146, 'w_security_system_wo_types_and_groups', 5038);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_3_Work_Order_Groups', 18146, 'w_security_system_wo_types_and_groups', 5039);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_4_Expenditure_Types', 18147, null, 5040);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_4_Expenditure_Types', 18147, null, 5041);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_5_Cost_Elements', 18148, null, 5042);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_5_Cost_Elements', 18148, null, 5043);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_6_Charge_Types', 18149, null, 5044);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_6_Charge_Types', 18149, null, 5045);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_7_Charge_Type_Data', 18150, null, 5046);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_7_Charge_Type_Data', 18150, null, 5047);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_8_Estimate_Charge_Types', 18151, null, 5048);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_8_Estimate_Charge_Types', 18151, null, 5049);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '3_9_Setting_up_the_Estimate_Overhead_Calculation_CWIP_', 18152, null, 5050);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '3_9_Setting_up_the_Estimate_Overhead_Calculation_CWIP_', 18152, null, 5051);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_1_Overview4', 18153, null, 5052);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_1_Overview4', 18153, null, 5053);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_10_Material_Reconciliation', 18154, 'w_matlrec_search', 5054);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_10_Material_Reconciliation', 18154, 'w_matlrec_search', 5055);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_10_1_Overview1', 18155, 'w_matlrec_config', 5056);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_10_1_Overview1', 18155, 'w_matlrec_config', 5057);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_11_Capital_Overheads', 18156, 'w_wo_clear_maintenance', 5058);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_11_Capital_Overheads', 18156, 'w_wo_clear_maintenance', 5059);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_11_1_Overview1', 18157, null, 5060);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_11_1_Overview1', 18157, null, 5061);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_11_2_Using_the_Overhead_Maintenance_Window', 18158, null, 5062);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_11_2_Using_the_Overhead_Maintenance_Window', 18158, null, 5063);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_11_3_Overhead_Basis_Definition', 18159, null, 5064);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_11_3_Overhead_Basis_Definition', 18159, null, 5065);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_12_External_Overheads', 18160, null, 5066);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_12_External_Overheads', 18160, null, 5067);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_13_Unitization_Allocations', 18161, null, 5068);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_13_Unitization_Allocations', 18161, null, 5069);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_13_1_Overview1', 18162, null, 5070);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_13_1_Overview1', 18162, null, 5071);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_13_2_Using_the_Unitization__Allocation_Method_Types_Window', 18163, 'w_unit_alloc_method_maint', 5072);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_13_2_Using_the_Unitization__Allocation_Method_Types_Window', 18163, 'w_unit_alloc_method_maint', 5073);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_13_3_Using_the_Unitization_Allocation_Basis_Window', 18164, null, 5074);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_13_3_Using_the_Unitization_Allocation_Basis_Window', 18164, null, 5075);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_14_Project_Management_Approvals', 18165, null, 5076);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_14_Project_Management_Approvals', 18165, null, 5077);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_14_1_Overview1', 18166, null, 5078);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_14_1_Overview1', 18166, null, 5079);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_14_2_Approval_Notifications', 18167, null, 5080);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_14_2_Approval_Notifications', 18167, null, 5081);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_14_3_Workflow', 18168, null, 5082);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_14_3_Workflow', 18168, null, 5083);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_15_Project_Management_Rates', 18169, null, 5084);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_15_Project_Management_Rates', 18169, null, 5085);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_15_1_Overview', 18170, null, 5086);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_15_1_Overview', 18170, null, 5087);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_15_2_Defaults', 18171, null, 5088);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_15_2_Defaults', 18171, null, 5089);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_16_Approval_Workflow_Setup', 18172, null, 5090);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_16_Approval_Workflow_Setup', 18172, null, 5091);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_16_1_Overview', 18173, 'w_rate_edit', 5092);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_16_1_Overview', 18173, 'w_rate_edit', 5093);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_16_2_Subsystems', 18174, 'w_workflow_setup', 5094);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_16_2_Subsystems', 18174, 'w_workflow_setup', 5095);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_16_3_Approval_Types', 18175, null, 5096);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_16_3_Approval_Types', 18175, null, 5097);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_16_4_Approval_Levels', 18176, null, 5098);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_16_4_Approval_Levels', 18176, null, 5099);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_16_5_Approval_Types_Levels_Relate', 18177, null, 5100);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_16_5_Approval_Types_Levels_Relate', 18177, null, 5101);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_16_6_Approval_Levels', 18178, null, 5102);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_16_6_Approval_Levels', 18178, null, 5103);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_Compatible_Unit_Setup', 18179, 'w_cu_main', 5104);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_Compatible_Unit_Setup', 18179, 'w_cu_main', 5105);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_Compatible_Unit_Setup', 18179, 'w_wo_eng_estimates', 5106);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_Compatible_Unit_Setup', 18179, 'w_wo_eng_estimates', 5107);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_1_Compatible_Unit_Details_Tab', 18180, 'w_cu_detail', 5108);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_1_Compatible_Unit_Details_Tab', 18180, 'w_cu_detail', 5109);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_2_Stock_Keeping_Unit_Detail_Tab', 18181, 'w_sku_detail', 5110);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_2_Stock_Keeping_Unit_Detail_Tab', 18181, 'w_sku_detail', 5111);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_3_Compatible_Unit_Hierarchy_Tab', 18182, 'w_cu_hier_add', 5112);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_3_Compatible_Unit_Hierarchy_Tab', 18182, 'w_cu_hier_add', 5113);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_3_Compatible_Unit_Hierarchy_Tab', 18182, 'w_cu_hier_detail', 5114);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_3_Compatible_Unit_Hierarchy_Tab', 18182, 'w_cu_hier_detail', 5115);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_4_Compatible_Unit_Action_Code_Tab', 18183, 'w_cu_act_code_std', 5116);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_4_Compatible_Unit_Action_Code_Tab', 18183, 'w_cu_act_code_std', 5117);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_4_Compatible_Unit_Action_Code_Tab', 18183, 'w_cu_act_code_std_detail', 5118);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_4_Compatible_Unit_Action_Code_Tab', 18183, 'w_cu_act_code_std_detail', 5119);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_5_Compatible_Unit_Work_Type_Tab', 18184, 'w_cu_wt', 5120);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_5_Compatible_Unit_Work_Type_Tab', 18184, 'w_cu_wt', 5121);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_6_Action_Code_Tab', 18185, null, 5122);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_6_Action_Code_Tab', 18185, null, 5123);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_7_Crew_Type_Tab', 18186, null, 5124);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_7_Crew_Type_Tab', 18186, null, 5125);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_8_Work_Situation_Tab', 18187, null, 5126);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_8_Work_Situation_Tab', 18187, null, 5127);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_9_Work_Type', 18188, 'w_cu_work_type', 5128);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_9_Work_Type', 18188, 'w_cu_work_type', 5129);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_17_9_Work_Type', 18188, 'w_cu_work_type_detail', 5130);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_17_9_Work_Type', 18188, 'w_cu_work_type_detail', 5131);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_2_Work_Order_Type_Maintenance', 18189, null, 5132);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_2_Work_Order_Type_Maintenance', 18189, null, 5133);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_3_Automatic_Review_for_Closing', 18190, null, 5134);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_3_Automatic_Review_for_Closing', 18190, null, 5135);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_3_1_Overview1', 18191, null, 5136);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_3_1_Overview1', 18191, null, 5137);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_3_2_Setting_up_an_Automatic_Review_Class', 18192, 'w_arc_control_main', 5138);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_3_2_Setting_up_an_Automatic_Review_Class', 18192, 'w_arc_control_main', 5139);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_3_3_Viewing_ARC_Results', 18193, 'w_arc_results_select_tabs', 5140);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_3_3_Viewing_ARC_Results', 18193, 'w_arc_results_select_tabs', 5141);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_4_Project_Management_Dashboards', 18194, 'w_dashboard_setup', 5142);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_4_Project_Management_Dashboards', 18194, 'w_dashboard_setup', 5143);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_4_1_Overview', 18195, null, 5144);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_4_1_Overview', 18195, null, 5145);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_4_2_Setting_Up_Project_Management_Dashboards', 18196, null, 5146);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_4_2_Setting_Up_Project_Management_Dashboards', 18196, null, 5147);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_5_Project_Management_Dynamic_Validations', 18197, null, 5148);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_5_Project_Management_Dynamic_Validations', 18197, null, 5149);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_5_1_Overview', 18198, null, 5150);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_5_1_Overview', 18198, null, 5151);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_5_2_Setting_up_Dynamic_Validations', 18199, 'w_wo_validation_test', 5152);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_5_2_Setting_up_Dynamic_Validations', 18199, 'w_wo_validation_test', 5153);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_6_Project_Management_Metrics', 18200, null, 5154);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_6_Project_Management_Metrics', 18200, null, 5155);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_6_1_Overview', 18201, null, 5156);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_6_1_Overview', 18201, null, 5157);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_6_2_Setting_up_Metrics', 18202, 'w_wo_metric_setup', 5158);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_6_2_Setting_up_Metrics', 18202, 'w_wo_metric_setup', 5159);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_6_2_Setting_up_Metrics__Ref266860284', 18203, 'w_wo_metric_detail_maint', 5160);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_6_2_Setting_up_Metrics__Ref266860284', 18203, 'w_wo_metric_detail_maint', 5161);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_7_Project_Management_Justifications', 18204, null, 5162);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_7_Project_Management_Justifications', 18204, null, 5163);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_7_1_Overview', 18205, 'w_wo_doc_just', 5164);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_7_1_Overview', 18205, 'w_wo_doc_just', 5165);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_7_2_Setting_up_Justification_Fields', 18206, 'w_wo_doc_justification_setup', 5166);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_7_2_Setting_up_Justification_Fields', 18206, 'w_wo_doc_justification_setup', 5167);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_7_2_Setting_up_Justification_Fields__Ref266961540', 18207, null, 5168);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_7_2_Setting_up_Justification_Fields__Ref266961540', 18207, null, 5169);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_8_WIP_Computation_Maintenance', 18208, null, 5170);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_8_WIP_Computation_Maintenance', 18208, null, 5171);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_8_1_Overview1', 18209, null, 5172);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_8_1_Overview1', 18209, null, 5173);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_8_2_Using_the_WIP_Computation_Window', 18210, 'w_wip_details', 5174);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_8_2_Using_the_WIP_Computation_Window', 18210, 'w_wip_details', 5175);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_WIP_Computation_Rationale_and_Examples', 18211, null, 5176);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_WIP_Computation_Rationale_and_Examples', 18211, null, 5177);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_1_Background_Information', 18212, null, 5178);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_1_Background_Information', 18212, null, 5179);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_10_Sample_Depreciation_Calculations', 18213, null, 5180);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_10_Sample_Depreciation_Calculations', 18213, null, 5181);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_11_Wip_Computation_Compounding', 18214, null, 5182);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_11_Wip_Computation_Compounding', 18214, null, 5183);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_2_AFUDC_Computational_Methods', 18215, null, 5184);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_2_AFUDC_Computational_Methods', 18215, null, 5185);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_3_Asset_Representation_Flow', 18216, null, 5186);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_3_Asset_Representation_Flow', 18216, null, 5187);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_4_Depreciation_Computational_Methods', 18217, null, 5188);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_4_Depreciation_Computational_Methods', 18217, null, 5189);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_5_Technical_and_Processing_Consideration', 18218, null, 5190);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_5_Technical_and_Processing_Consideration', 18218, null, 5191);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_6_Budgeting_and_Forecasting_Process', 18219, null, 5192);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_6_Budgeting_and_Forecasting_Process', 18219, null, 5193);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_7_IFRS', 18220, null, 5194);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_7_IFRS', 18220, null, 5195);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_8_Other_Processing', 18221, null, 5196);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_8_Other_Processing', 18221, null, 5197);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '4_9_9_Sample_AFUDC_Calculations', 18222, null, 5198);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '4_9_9_Sample_AFUDC_Calculations', 18222, null, 5199);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '5_1_Overview3', 18223, null, 5200);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '5_1_Overview3', 18223, null, 5201);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '5_2_Using_the_Work_Order_Initiation_Window', 18224, null, 5202);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '5_2_Using_the_Work_Order_Initiation_Window', 18224, null, 5203);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '5_2_1_Using_PowerPlan_to_Initiate_a_Work_Order__Step_1', 18225, 'w_wo_entry', 5204);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '5_2_1_Using_PowerPlan_to_Initiate_a_Work_Order__Step_1', 18225, 'w_wo_entry', 5205);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '5_2_2_Copy_Work_Order_Option', 18226, null, 5206);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '5_2_2_Copy_Work_Order_Option', 18226, null, 5207);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '5_2_3_Using_PowerPlan_to_Initiate_a_Work_Order__Step_2', 18227, 'w_wo_detail', 5208);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '5_2_3_Using_PowerPlan_to_Initiate_a_Work_Order__Step_2', 18227, 'w_wo_detail', 5209);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_1_Overview3', 18228, null, 5210);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_1_Overview3', 18228, null, 5211);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_2_Automatic_Approvals', 18229, null, 5212);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_2_Automatic_Approvals', 18229, null, 5213);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_3_Sending_a_Work_Order_for_Approval', 18230, null, 5214);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_3_Sending_a_Work_Order_for_Approval', 18230, null, 5215);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_4_Approving_a_Work_Order_Using_the_Work_Order_Approval_Window_', 18231, 'w_wo_approval_list', 5216);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_4_Approving_a_Work_Order_Using_the_Work_Order_Approval_Window_', 18231, 'w_wo_approval_list', 5217);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_5_Work_Order_Approval_Form_Window', 18232, 'w_wo_approval_report', 5218);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_5_Work_Order_Approval_Form_Window', 18232, 'w_wo_approval_report', 5219);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_6_Work_Order_Approval_Delegation', 18233, 'w_wo_appr_delegation', 5220);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_6_Work_Order_Approval_Delegation', 18233, 'w_wo_appr_delegation', 5221);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_7_Email_Notifications', 18234, null, 5222);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_7_Email_Notifications', 18234, null, 5223);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '6_8_Work_Order_Approval_Justification_Approval_Status', 18235, 'w_wo_approval_status', 5224);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '6_8_Work_Order_Approval_Justification_Approval_Status', 18235, 'w_wo_approval_status', 5225);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_1_Overview3', 18236, null, 5226);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_1_Overview3', 18236, null, 5227);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_Using_the_Work_Order_Selection_Window', 18237, 'w_project_select_tabs', 5228);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_Using_the_Work_Order_Selection_Window', 18237, 'w_project_select_tabs', 5229);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_1_Work_Order_Selection__Custom_Grid_Results', 18238, null, 5230);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_1_Work_Order_Selection__Custom_Grid_Results', 18238, null, 5231);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_10_Work_Order_Selection_Searching_by_Property_Tax_Locations', 18239, 'w_project_select_tabs_Prop_Tax_Dist', 5232);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_10_Work_Order_Selection_Searching_by_Property_Tax_Locations', 18239, 'w_project_select_tabs_Prop_Tax_Dist', 5233);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_11_Work_Order_Selection_Searching_by_Class_Code', 18240, 'w_project_select_tabs_Class_Code', 5234);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_11_Work_Order_Selection_Searching_by_Class_Code', 18240, 'w_project_select_tabs_Class_Code', 5235);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_12_Work_Order_Selection_Miscellaneous_Search_Criteria', 18241, 'w_project_select_tabs_Misc', 5236);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_12_Work_Order_Selection_Miscellaneous_Search_Criteria', 18241, 'w_project_select_tabs_Misc', 5237);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_13_Work_Order_Selection_Searching_by_Justification', 18242, null, 5238);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_13_Work_Order_Selection_Searching_by_Justification', 18242, null, 5239);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_14_Work_Order_Selection_Audits', 18243, 'w_project_select_tabs_Audits', 5240);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_14_Work_Order_Selection_Audits', 18243, 'w_project_select_tabs_Audits', 5241);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_15_Work_Order_Selection_Searching_by_Alerts', 18244, 'w_project_select_tabs_Alerts', 5242);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_15_Work_Order_Selection_Searching_by_Alerts', 18244, 'w_project_select_tabs_Alerts', 5243);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_16_Work_Order_Selection_Saving_and_Re_running_Queries', 18245, 'w_cpr_query', 5244);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_16_Work_Order_Selection_Saving_and_Re_running_Queries', 18245, 'w_cpr_query', 5245);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_2_Work_Order_Selection__Searching_by_Work_Order_Number', 18246, null, 5246);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_2_Work_Order_Selection__Searching_by_Work_Order_Number', 18246, null, 5247);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_3_Work_Order_Selection_Searching_by_Funding_Project', 18247, 'w_project_select_tabs_Fund_Proj', 5248);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_3_Work_Order_Selection_Searching_by_Funding_Project', 18247, 'w_project_select_tabs_Fund_Proj', 5249);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_4_Work_Order_Selection_Searching_by_Company', 18248, 'w_project_select_tabs_Company', 5250);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_4_Work_Order_Selection_Searching_by_Company', 18248, 'w_project_select_tabs_Company', 5251);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_5_Work_Order_Selection_Searching_by_Budget_Item', 18249, 'w_project_select_tabs_Budget', 5252);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_5_Work_Order_Selection_Searching_by_Budget_Item', 18249, 'w_project_select_tabs_Budget', 5253);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_6_Work_Order_Selection_Searching_by_Location', 18250, 'w_project_select_tabs_Loc', 5254);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_6_Work_Order_Selection_Searching_by_Location', 18250, 'w_project_select_tabs_Loc', 5255);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_7_Work_Order_Selection_Searching_by_Department', 18251, 'w_project_select_tabs_Dept', 5256);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_7_Work_Order_Selection_Searching_by_Department', 18251, 'w_project_select_tabs_Dept', 5257);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_8_Work_Order_Selection_Searching_by_Work_Order_Group', 18252, 'w_project_select_tabs_WO_Group', 5258);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_8_Work_Order_Selection_Searching_by_Work_Order_Group', 18252, 'w_project_select_tabs_WO_Group', 5259);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '7_2_9_Work_Order_Selection_Searching_by_Work_Order_Type', 18253, 'w_project_select_tabs_WO_Type', 5260);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '7_2_9_Work_Order_Selection_Searching_by_Work_Order_Type', 18253, 'w_project_select_tabs_WO_Type', 5261);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_1_Overview4', 18254, null, 5262);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_1_Overview4', 18254, null, 5263);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_Using_the_Work_Order_Information_Window', 18255, 'w_wo_detail', 5264);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_Using_the_Work_Order_Information_Window', 18255, 'w_wo_detail', 5265);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_1_Work_Order_Information__Details', 18256, 'w_wo_detail_Details', 5266);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_1_Work_Order_Information__Details', 18256, 'w_wo_detail_Details', 5267);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_10_Work_Order_Information_Authorizations', 18257, 'w_wo_detail_Authorizations', 5268);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_10_Work_Order_Information_Authorizations', 18257, 'w_wo_detail_Authorizations', 5269);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_11_Work_Order_Information_Overheads', 18258, 'w_wo_detail_Overheads', 5270);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_11_Work_Order_Information_Overheads', 18258, 'w_wo_detail_Overheads', 5271);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_12_Work_Order_Information__User_Comments', 18259, 'w_wo_detail_User_Comment', 5272);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_12_Work_Order_Information__User_Comments', 18259, 'w_wo_detail_User_Comment', 5273);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_13_Work_Order_Information__Related_Work_Orders', 18260, 'w_wo_detail_Related_Wos', 5274);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_13_Work_Order_Information__Related_Work_Orders', 18260, 'w_wo_detail_Related_Wos', 5275);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_2_Work_Order_Information_Accounts', 18261, 'w_wo_detail_Accounts', 5276);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_2_Work_Order_Information_Accounts', 18261, 'w_wo_detail_Accounts', 5277);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_3_Work_Order_Information_Departments', 18262, 'w_wo_detail_Departments', 5278);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_3_Work_Order_Information_Departments', 18262, 'w_wo_detail_Departments', 5279);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_4_Work_Order_Information_Contacts', 18263, 'w_wo_detail_Contacts', 5280);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_4_Work_Order_Information_Contacts', 18263, 'w_wo_detail_Contacts', 5281);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_5_Work_Order_Information_Tasks', 18264, 'w_wo_detail_Tasks', 5282);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_5_Work_Order_Information_Tasks', 18264, 'w_wo_detail_Tasks', 5283);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_6_Work_Order_Information_Class_Codes', 18265, 'w_wo_detail_Class_Codes', 5284);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_6_Work_Order_Information_Class_Codes', 18265, 'w_wo_detail_Class_Codes', 5285);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_7_Work_Order_Information_Billings', 18266, 'w_wo_detail_Billings', 5286);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_7_Work_Order_Information_Billings', 18266, 'w_wo_detail_Billings', 5287);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_8_Project_Work_Order_Information_Justification', 18267, 'w_wo_detail_Justification', 5288);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_8_Project_Work_Order_Information_Justification', 18267, 'w_wo_detail_Justification', 5289);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '8_2_9_Work_Order_Information__Tax_Status', 18268, null, 5290);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '8_2_9_Work_Order_Information__Tax_Status', 18268, null, 5291);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_1_Overview1', 18269, null, 5292);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_1_Overview1', 18269, null, 5293);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_10_Slide', 18270, 'w_wo_est_slide', 5294);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_10_Slide', 18270, 'w_wo_est_slide', 5295);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_2_Using_the_Work_Order_Estimates__Summary_window', 18271, 'w_wo_estimates', 5296);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_2_Using_the_Work_Order_Estimates__Summary_window', 18271, 'w_wo_estimates', 5297);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_3_Using_the_Work_Order_Estimates__Grid_Window', 18272, 'w_wo_est_monthly_grid_entry_custom', 5298);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_3_Using_the_Work_Order_Estimates__Grid_Window', 18272, 'w_wo_est_monthly_grid_entry_custom', 5299);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_4_Using_the_Work_Order_Estimates__Copy_Estimate', 18273, null, 5300);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_4_Using_the_Work_Order_Estimates__Copy_Estimate', 18273, null, 5301);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_5_Monthly_Estimate_Upload_Tool', 18274, 'w_wo_est_monthly_upload', 5302);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_5_Monthly_Estimate_Upload_Tool', 18274, 'w_wo_est_monthly_upload', 5303);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_6_Using_the_Work_Order_Estimate_Details_Unit_Estimates_Window', 18275, 'w_wo_est_build', 5304);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_6_Using_the_Work_Order_Estimate_Details_Unit_Estimates_Window', 18275, 'w_wo_est_build', 5305);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_6_1_Other_Estimates_Processing_Options', 18276, null, 5306);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_6_1_Other_Estimates_Processing_Options', 18276, null, 5307);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_6_2_Work_Order_Estimate_Details_Templates', 18277, 'w_wo_est_template_select', 5308);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_6_2_Work_Order_Estimate_Details_Templates', 18277, 'w_wo_est_template_select', 5309);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_6_2_Work_Order_Estimate_Details_Templates__Ref204488490', 18278, 'w_wo_est_template', 5310);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_6_2_Work_Order_Estimate_Details_Templates__Ref204488490', 18278, 'w_wo_est_template', 5311);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_6_3_Work_Order_Estimate_Details_Capitalization_Analysis', 18279, null, 5312);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_6_3_Work_Order_Estimate_Details_Capitalization_Analysis', 18279, null, 5313);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_6_3_Work_Order_Estimate_Details_Capitalization_Analysis__Ref25980687', 18280, null, 5314);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_6_3_Work_Order_Estimate_Details_Capitalization_Analysis__Ref25980687', 18280, null, 5315);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_6_4_Work_Order_Estimate_Details_View_Customization', 18281, null, 5316);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_6_4_Work_Order_Estimate_Details_View_Customization', 18281, null, 5317);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_7_Using_the_Work_Order_As_Built_Details_Window', 18282, null, 5318);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_7_Using_the_Work_Order_As_Built_Details_Window', 18282, null, 5319);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_8_Work_Order_Engineering_Estimates', 18283, null, 5320);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_8_Work_Order_Engineering_Estimates', 18283, null, 5321);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_8_1_Engineering_Estimates_Tab', 18284, null, 5322);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_8_1_Engineering_Estimates_Tab', 18284, null, 5323);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_8_2_Detailed_Engineering_Estimate_Tab', 18285, null, 5324);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_8_2_Detailed_Engineering_Estimate_Tab', 18285, null, 5325);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_8_3_Detailed_Material_Tab', 18286, null, 5326);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_8_3_Detailed_Material_Tab', 18286, null, 5327);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', '9_9_Revision_Comments', 18287, 'w_wo_est_comments', 5328);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', '9_9_Revision_Comments', 18287, 'w_wo_est_comments', 5329);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_1_Introduction_to_Work_Order_Management', 18288, null, 5330);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_1_Introduction_to_Work_Order_Management', 18288, null, 5331);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_10_Viewing_Work_Order_Balances', 18289, null, 5332);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_10_Viewing_Work_Order_Balances', 18289, null, 5333);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_11_Viewing_Work_Order_Charges', 18290, null, 5334);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_11_Viewing_Work_Order_Charges', 18290, null, 5335);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_12_Viewing_and_Processing_Work_Order_Commitments', 18291, null, 5336);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_12_Viewing_and_Processing_Work_Order_Commitments', 18291, null, 5337);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_13_Work_Order_Other_Topics', 18292, null, 5338);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_13_Work_Order_Other_Topics', 18292, null, 5339);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_14_Relating_Documents', 18293, null, 5340);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_14_Relating_Documents', 18293, null, 5341);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_15_Job_Task_Submodule', 18294, null, 5342);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_15_Job_Task_Submodule', 18294, null, 5343);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_16_Closing_Work_Orders', 18295, null, 5344);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_16_Closing_Work_Orders', 18295, null, 5345);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_17_Booking_Retirements', 18296, null, 5346);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_17_Booking_Retirements', 18296, null, 5347);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_18_Project_Management_Query_Tools', 18297, null, 5348);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_18_Project_Management_Query_Tools', 18297, null, 5349);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_19_Running_Reports', 18298, null, 5350);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_19_Running_Reports', 18298, null, 5351);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_2_Navigating_Project_Management', 18299, null, 5352);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_2_Navigating_Project_Management', 18299, null, 5353);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_20_Monthly_Work_Order_Closing_Process', 18300, null, 5354);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_20_Monthly_Work_Order_Closing_Process', 18300, null, 5355);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_21_Exhibits', 18301, null, 5356);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_21_Exhibits', 18301, null, 5357);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_3_Table_Setup', 18302, null, 5358);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_3_Table_Setup', 18302, null, 5359);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_4_Project_Configurations', 18303, null, 5360);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_4_Project_Configurations', 18303, null, 5361);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_5_Initiating_Work_Orders', 18304, null, 5362);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_5_Initiating_Work_Orders', 18304, null, 5363);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_6_Approving_Work_Orders', 18305, null, 5364);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_6_Approving_Work_Orders', 18305, null, 5365);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_7_Finding_Work_Orders', 18306, null, 5366);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_7_Finding_Work_Orders', 18306, null, 5367);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_8_Work_Order_Header_Information', 18307, null, 5368);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_8_Work_Order_Header_Information', 18307, null, 5369);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - fp', 'Chapter_9_Work_Order_Estimates', 18308, null, 5370);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management - wo', 'Chapter_9_Work_Order_Estimates', 18308, null, 5371);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '1_1_Overview7', 19000, 'w_reimb_main', 5372);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '1_1_1_Setup', 19001, null, 5373);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '1_1_2_Ongoing', 19002, null, 5374);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '1_2_Navigating_the_Reimbursables_Module', 19003, null, 5375);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '1_2_1_Using_the_Reimbursables_Main_Taskbar', 19004, null, 5376);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_1_Overview4', 19005, null, 5377);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_2_Step_1_Initiate_the_Billing_Group', 19006, 'w_reimb_initiate', 5378);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_2_1_Searching_for_a_Work_Order', 19007, null, 5379);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_2_2_Work_Order_Error_Messages', 19008, null, 5380);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_Step_2_Billing_Group_Description', 19009, null, 5381);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_1_Billing_Group_Details__Detail_Tab', 19010, 'w_reimb_bill_group_detail', 5382);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_10_Billing_Group_Details__Statistics_Button', 19011, null, 5383);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_11_Billing_Group_Details__Reviews_Button', 19012, null, 5384);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_12_Billing_Group_Details__Reviews_Detail', 19013, null, 5385);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_13_Billing_Group_Details__Reviews_Report', 19014, null, 5386);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_2_Billing_Group_Details__Customer', 19015, null, 5387);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_3_Billing_Group_Details__Class_Codes_Tab', 19016, null, 5388);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_4_Billing_Group_Details__Work_Orders_Tab', 19017, null, 5389);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_5_Billing_Group_Details__Methods_Tab', 19018, null, 5390);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_6_Billing_Group_Details__Documents_Tab', 19019, null, 5391);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_7_Billing_Group_Details__Schedule_Tab', 19020, null, 5392);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_8_Billing_Group_Details__Premises_Tab', 19021, null, 5393);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '2_3_9_Billing_Group_Details__Related_Grps_Tab', 19022, null, 5394);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_1_Overview4', 19023, null, 5395);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_Using_the_Reimbursable_Billing_Groups_Selection_Window', 19024, 'w_reimb_select_tabs_main', 5396);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_1_Entering_a_Billing_Group_Number', 19025, null, 5397);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_10_Searching_by_Class_Codes', 19026, null, 5398);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_11_Searching_by_Miscellaneous', 19027, null, 5399);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_2_Searching_by_Company', 19028, null, 5400);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_3_Searching_by_Customer', 19029, null, 5401);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_4_Searching_by_Reimbursable_Method', 19030, null, 5402);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_5_Searching_by_Refund_Type', 19031, null, 5403);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_6_Searching_by_Rate_Schedule', 19032, null, 5404);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_7_Searching_by_Reimbursable_Status', 19033, null, 5405);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_8_Searching_by_Work_Order', 19034, null, 5406);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_2_9_Searching_by_Super_Groups', 19035, null, 5407);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_3_Reimbursable_Work_Orders', 19036, 'w_reimb_work_order', 5408);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_3_1_Reimbursable_Work_Orders_Window', 19037, null, 5409);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_3_2_Adding_Additional_Work_Orders', 19038, null, 5410);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_3_3_Removing_Work_Orders', 19039, null, 5411);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_4_Reimbursable_Estimates', 19040, 'w_reimb_est_monthly', 5412);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_4_1_Using_the_Reimbursable_Estimates_Window', 19041, null, 5413);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_4_2_Pulling_Work_Order_Estimates_for_a_Billing_Group', 19042, null, 5414);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_4_3_Applying_Reimbursable_Estimate_Overheads', 19043, null, 5415);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_4_4_Reimbursable_Adjustment_Estimates', 19044, null, 5416);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_4_5_Locking_the_Estimate_for_Billing', 19045, null, 5417);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_Reimbursable_Charges_Displays', 19046, 'w_reimb_display_test', 5418);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_1_Reimbursable_Display_Charge_Summary_Window', 19047, null, 5419);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_2_Pulling_Charges_from_the_Cost_Repository', 19048, null, 5420);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_3_Applying_Reimbursable_Overheads', 19049, null, 5421);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_4_Manually_Calculating_a_Bill_from_the_Charge_Summary', 19050, null, 5422);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_5_Printing_and_Exporting_Displays', 19051, null, 5423);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_6_Reimbursable_Detail_Charge_View', 19052, null, 5424);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_7_Using_the_Grid_Functionality', 19053, null, 5425);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_8_Creating_Adjustment_Transactions', 19054, 'w_reimb_display_adjust', 5426);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_5_9_Deleting_Adjustments', 19055, 'w_reimb_display_detail', 5427);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_6_Reimbursable_Billing', 19056, 'w_reimb_bill_summary', 5428);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_6_1_Reimbursable_Bills_and_Payments_Window', 19057, 'w_reimb_bill_payment', 5429);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_6_2_Calculating_a_Bill', 19058, null, 5430);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_6_3_Reviewing_Bill_Details', 19059, 'w_reimb_bill_detail', 5431);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_6_4_Viewing_the_Billing_Report', 19060, 'w_reimb_billing_report', 5432);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_6_5_Approving_a_Bill', 19061, 'w_reimb_bill_approve', 5433);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_6_6_Payment_Details', 19062, null, 5434);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_7_Reimbursable_Queries', 19063, null, 5435);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '3_7_1_Select_PowerPlan_Query_Window', 19064, null, 5436);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_Overview5', 19065, null, 5437);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_1_Using_the_Reimbursables_Controls_Taskbar', 19066, null, 5438);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_2_Reimbursable_System_Controls', 19067, 'w_reimb_system_control', 5439);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_3_Reimbursable_CR_Sources', 19068, 'w_reimb_cr_sources', 5440);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_4_Reimbursable_Displays', 19069, 'w_reimb_display_setup', 5441);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_5_Reimbursable_Methods', 19070, 'w_reimb_methods', 5442);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_6_Reimbursable_Overheads', 19071, 'w_reimb_cr_oh_maintenance', 5443);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '4_1_7_Reimbursables_Customers', 19072, 'w_reimb_customer', 5444);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '5_1_Overview4', 19073, 'w_reimb_control', 5445);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '5_1_1_Using_the_Reimbursables_Controls_Taskbar', 19074, null, 5446);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '5_1_2_Refund_Types', 19075, 'w_reimb_refund_type', 5447);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '5_1_3_Refund_Calc', 19076, 'w_reimb_refund_calc', 5448);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '5_1_4_Refund_Rates', 19077, 'w_reimb_refund_rates', 5449);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '5_1_5_Statistic_Types', 19078, 'w_reimb_statistics_control', 5450);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '6_1_Overview4', 19079, null, 5451);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '6_1_1_Refund_Detail_Dollars', 19080, null, 5452);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '6_1_2_Refund_Dollar_Categorizations', 19081, null, 5453);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '6_1_3_Refund_Schedules', 19082, null, 5454);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '6_1_4_Refund_Premises', 19083, null, 5455);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '6_1_5_Unassigned_Premises', 19084, null, 5456);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '6_2_Statistics', 19085, null, 5457);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '7_1_Overview4', 19086, null, 5458);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '7_1_1_Bills_Tab', 19087, null, 5459);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '7_1_2_Refunds_Tab', 19088, null, 5460);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', '7_1_3_Penalties_Tab', 19089, null, 5461);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', 'Chapter_1_Introduction3', 19090, null, 5462);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', 'Chapter_2_Initiating_a_Billing_Group', 19091, null, 5463);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', 'Chapter_3_Finding_Billing_Groups', 19092, null, 5464);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', 'Chapter_4_Reimbursable_System_Controls', 19093, null, 5465);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', 'Chapter_5_Refund_Controls', 19094, null, 5466);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', 'Chapter_6_Refund_Details', 19095, null, 5467);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'project management', 'Chapter_7_Reimbursable_Monthly_Processing', 19096, null, 5468);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '1_1_Introduction4', 20000, 'w_rpr_center_main', 5469);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '10_1_Introduction2', 20001, 'w_rpr_center_main/menu_wksp_processing', 5470);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '10_2_Navigating_the_Processing_Center', 20002, null, 5471);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '10_2_1_Processing_Errors', 20003, null, 5472);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '10_3_Navigating_the_Posting_Center', 20004, 'w_rpr_center_main/menu_wksp_posting', 5473);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '10_3_1_Posting_Errors', 20005, null, 5474);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '11_1_Introduction3', 20006, null, 5475);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '11_1_1_Manage_General_Methodology', 20007, null, 5476);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '11_1_2_Manage_Unit_of_Property_Methodology', 20008, null, 5477);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '11_1_3_Manage_Blended_Methodology', 20009, null, 5478);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '11_1_4_Manage_Allocation_Methodology', 20010, null, 5479);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '11_1_5_Manage_Unit_of_Property_Work_Task_Methodology', 20011, null, 5480);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '11_2_Tax_Expense_Reversals', 20012, null, 5481);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '12_1_Introduction2', 20013, null, 5482);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '12_2_Posting_to_Projects', 20014, null, 5483);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '12_3_Posting_to_Assets_CPR_', 20015, null, 5484);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '12_4_Posting_to_PowerTax', 20016, null, 5485);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '12_5_Posting_to_Tax_Provision', 20017, null, 5486);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '13_1_Thresholds_Estimate_Configuration', 20018, 'w_rpr_center_main/menu_wksp_threshold_estimate', 5487);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_1_Introduction3', 20019, null, 5488);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_2_Tax_Repairs_in_CWIP', 20020, null, 5489);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_2_1_Tax_Repairs_Testing_in_CWIP', 20021, null, 5490);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_2_2_Work_Order_Charges', 20022, null, 5491);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_2_3_CWIP_Tax_Repairs_Deduction', 20023, null, 5492);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_2_4_Interface_to_PowerTax_Provision', 20024, null, 5493);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_2_5_Work_Order_Closing', 20025, null, 5494);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_3_Tax_Repairs_in_the_CPR', 20026, null, 5495);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_3_1_Tax_Repairs_Testing_in_the_CPR', 20027, null, 5496);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_3_2_CPR_Tax_Repairs_Deduction', 20028, null, 5497);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_4_Tax_Repairs__Blended', 20029, null, 5498);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_5_Tax_Repairs_in_PowerTax', 20030, null, 5499);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_5_1_Additions', 20031, null, 5500);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '2_5_2_Retirements', 20032, null, 5501);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '3_1_Introduction5', 20033, null, 5502);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '3_2_Icons_and_Buttons', 20034, null, 5503);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '3_2_1_Universal_Icons', 20035, null, 5504);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '3_2_2_Report_Icons', 20036, null, 5505);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '3_2_3_One_click_Buttons', 20037, null, 5506);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '3_2_4_Query_Buttons', 20038, null, 5507);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '3_2_5_More_Options', 20039, null, 5508);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '4_1_Introduction5', 20040, 'w_rpr_center_main/menu_wksp_home', 5509);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '4_2_Repair_Review', 20041, null, 5510);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_1_Introduction4', 20042, null, 5511);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_2_Book_Summary', 20043, 'w_rpr_center_main/menu_wksp_book_summary', 5512);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_3_Manage_Repair_Schemas', 20044, 'w_rpr_center_main/menu_wksp_repair_schema', 5513);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_3_1_Adding_a_Repair_Schema', 20045, null, 5514);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_3_2_Deleting_a_Repair_Schema', 20046, null, 5515);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_4_Manage_Repair_Locations', 20047, 'w_rpr_center_main/menu_wksp_tax_locations', 5516);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_4_1_Adding_a_Repair_Location', 20048, null, 5517);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_4_2_Assigning_Unassigning_Asset_Locations', 20049, null, 5518);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_4_3_Deleting_a_Repair_Location', 20050, null, 5519);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_5_Manage_Repair_Tests', 20051, 'w_rpr_center_main/menu_wksp_tax_exp_tests', 5520);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_5_1_Adding_a_Repair_Test', 20052, null, 5521);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_5_2_Tax_Statuses', 20053, null, 5522);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_5_3_Percents', 20054, null, 5523);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_5_4_Other_Settings', 20055, null, 5524);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_5_5_Assigning_Unassigning_Other_Settings', 20056, null, 5525);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '5_5_6_Deleting_a_Repair_Test', 20057, null, 5526);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_1_Introduction5', 20058, null, 5527);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_2_Manage_Tax_Status', 20059, 'w_rpr_center_main/menu_wksp_tax_status', 5528);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_2_1_Adding_a_Tax_Status', 20060, null, 5529);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_2_2_Deleting_a_Tax_Status', 20061, null, 5530);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_2_3_Unassigning_a_Tax_Status', 20062, null, 5531);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_3_Manage_Range_Test', 20063, 'w_rpr_center_main/menu_wksp_range_test', 5532);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_3_1_Adding_a_Range_Test', 20064, null, 5533);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_3_2_Deleting_a_Range_Test', 20065, null, 5534);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_4_Manage_Tax_Units_of_Property', 20066, 'w_rpr_center_main/menu_wksp_tax_units', 5535);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_4_1_Adding_a_Tax_Unit_of_Property', 20067, null, 5536);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_4_2_Assigning_Unassigning_Key_Data_Fields_Gen_', 20068, null, 5537);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_4_3_Deleting_Tax_Units_of_Property_Gen_', 20069, null, 5538);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_5_Manage_Tax_Thresholds', 20070, 'w_rpr_center_main/menu_wksp_tax_thresholds', 5539);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_5_1_Adding_Tax_Thresholds', 20071, null, 5540);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_5_2_Copying_Tax_Thresholds', 20072, null, 5541);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '6_5_3_Deleting_Tax_Thresholds', 20073, null, 5542);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_1_Introduction5', 20074, null, 5543);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_2_Manage_Tax_Units_of_Property_UOP_', 20075, null, 5544);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_2_1_Adding_a_Tax_Units_of_Property_UOP_', 20076, null, 5545);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_2_2_Assigning_Unassigning_Key_Data_Fields_UOP_', 20077, null, 5546);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_2_3_Deleting_Tax_Units_of_Property_UOP_', 20078, null, 5547);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_3_Manage_Repair_Location_Rollups', 20079, 'w_rpr_center_main/menu_wksp_tax_loc_rollups', 5548);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_3_1_Adding_Repair_Location_Rollups', 20080, null, 5549);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_3_2_Deleting_Repair_Location_Rollups', 20081, null, 5550);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_4_Manage_Tax_Thresholds', 20082, null, 5551);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_4_1_Adding_Tax_Thresholds', 20083, null, 5552);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_4_2_Copying_Tax_Thresholds', 20084, null, 5553);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '7_4_3_Deleting_Tax_Thresholds', 20085, null, 5554);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '8_1_Introduction3', 20086, null, 5555);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '8_2_Creating_an_Import_Template', 20087, null, 5556);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '8_3_Creating_an_Add_Like_Import_Template', 20088, null, 5557);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '8_4_Deleting_an_Import_Template', 20089, null, 5558);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '8_5_Running_an_Import', 20090, null, 5559);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_1_Introduction4', 20091, null, 5560);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_2_Testing_Parameters', 20092, 'w_rpr_center_main/menu_wksp_assign_tax_test', 5561);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_2_1_Retrieving_Work_Orders', 20093, null, 5562);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_2_2_Drill_Down', 20094, null, 5563);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_2_3_Updating_Work_Orders', 20095, null, 5564);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_Set_Tax_Status', 20096, 'w_rpr_center_main/menu_wksp_set_tax_status', 5565);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_1_Retrieving_Work_Orders', 20097, null, 5566);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_2_Drilldown_to_Tax_Status', 20098, null, 5567);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_3_Drilldown_to_WO_Details', 20099, null, 5568);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_4_Drilldown_to_Charges', 20100, null, 5569);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_5_Drilldown_to_Estimates', 20101, null, 5570);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_6_Drilldown_to_CPR', 20102, null, 5571);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_3_7_Testing_and_Updating_Work_Orders', 20103, null, 5572);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', '9_4_WO_TaxUOP_Exclusions', 20104, 'w_rpr_center_main/menu_wksp_wo_ruc_exception', 5573);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_1_Tax_Repairs_Overview', 20105, null, 5574);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_10_Processing_and_Posting_Tax_Repairs', 20106, null, 5575);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_11_Calculating_Expense', 20107, null, 5576);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_12_Posting_Tax_Repairs_Deduction', 20108, null, 5577);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_13_Thresholds_Estimate_Configuration', 20109, null, 5578);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_2_Book_to_Tax_Integration', 20110, null, 5579);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_3_Tax_Repairs_Workspace_Layout', 20111, null, 5580);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_4_Home_Page', 20112, null, 5581);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_5_Configuration_Options', 20113, 'w_rpr_center_main/menu_wksp_config', 5582);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_6_General_Method', 20114, 'w_rpr_center_main/menu_wksp_work_order', 5583);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_7_Unit_of_Property_Method', 20115, 'w_rpr_center_main/menu_wksp_asset', 5584);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_8_Import', 20116, 'w_rpr_center_main/menu_wksp_import', 5585);
insert into pp_htmlhelp (DEFINE_VAR, SUBSYSTEM, ALIAS_NAME, MAP_ID, W_NAME, COUNTER_ID)
values ('#define', 'tax repairs', 'Chapter_9_Modify_and_Test_Attributes_on_Work_Orders', 20117, 'w_rpr_center_main/menu_wksp_wo_setup', 5586);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1095, 0, 10, 4, 2, 0, 37582, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037582_sys_pp_htmlhelp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
