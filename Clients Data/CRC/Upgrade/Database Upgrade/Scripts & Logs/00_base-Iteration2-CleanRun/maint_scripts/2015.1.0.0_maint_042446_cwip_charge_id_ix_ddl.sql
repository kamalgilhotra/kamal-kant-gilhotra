/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042446_cwip_charge_id_ix_ddl.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   01/26/2015 Aaron Smith    		Need Index on tables with CHARGE_ID
||========================================================================================
*/

--New Indices
CREATE INDEX ARCHARGE_CHGID_IX ON ARO_CHARGE (CHARGE_ID) TABLESPACE PWRPLANT_IDX;

CREATE INDEX WOAUTO106_CHGID_IX ON WO_AUTO106_CHARGES (CHARGE_ID) TABLESPACE PWRPLANT_IDX;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2217, 0, 2015, 1, 0, 0, 42446, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042446_cwip_charge_id_ix_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
