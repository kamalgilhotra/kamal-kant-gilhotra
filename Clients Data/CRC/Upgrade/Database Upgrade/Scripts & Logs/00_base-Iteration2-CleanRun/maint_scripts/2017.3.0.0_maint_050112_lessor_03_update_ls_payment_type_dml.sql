/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050112_lessor_03_update_ls_payment_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/09/2018 Johnny Sisouphanh Add new column Recognized Profit
||============================================================================
*/

update ls_payment_type
set description = 'Recognized Profit'
where payment_type_id = 27
  and lower(description) = 'earned profit';

UPDATE WORKFLOW_AMOUNT_SQL
set SQL = 'select nvl(sum(nvl(invoice_principal,0)+nvl(invoice_interest,0)+nvl(invoice_executory,0)+nvl(invoice_contingent,0)+nvl(invoice_recognized_profit,0)),0) appr_amount from lsr_invoice where invoice_id = <<id_field1>>'
where subsystem = 'lsr_invoice_approval';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4176, 0, 2017, 3, 0, 0, 50112, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050112_lessor_03_update_ls_payment_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
