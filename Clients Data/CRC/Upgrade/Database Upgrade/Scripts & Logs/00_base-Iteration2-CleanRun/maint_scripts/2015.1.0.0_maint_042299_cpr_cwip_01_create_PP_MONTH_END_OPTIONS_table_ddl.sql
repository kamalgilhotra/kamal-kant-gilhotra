/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042299_cpr_cwip_01_create_PP_MONTH_END_OPTIONS_table_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/03/2015 Charlie Shilling maint-42299
||============================================================================
*/

CREATE TABLE pp_month_end_options (
	company_id			NUMBER(22,0)	NOT NULL,
	process_id			NUMBER(22,0)	NOT NULL,
	option_id			NUMBER(22,0)	NOT NULL,
	description			VARCHAR2(254)	NULL,
	long_description	VARCHAR2(2000)	NULL,
	option_value		VARCHAR2(35)	NULL,
	user_id				VARCHAR2(18)	NULL,
	time_stamp			DATE 			NULL
);

ALTER TABLE pp_month_end_options
  ADD CONSTRAINT pk_pp_month_end_options PRIMARY KEY (
    company_id, process_id, option_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx
/

ALTER TABLE pp_month_end_options
  ADD CONSTRAINT r_pp_month_end_options1 FOREIGN KEY (
    process_id
  ) REFERENCES pp_processes (
    process_id
  )
/

comment on table pp_month_end_options is '(F) [01][04] This table holds the default value for user options used during the month end processes. The main purpose is to provide away for clients to provide feedback to the SSP month end processes when using the job server for prompts that were originally required user feedback.';
comment on column pp_month_end_options.company_id is 'System-assigned identifier of a particular company. These values will vary client to client.';
comment on column pp_month_end_options.process_id is 'System-assigned identifier of a particular process. These values will vary client to client.';
comment on column pp_month_end_options.option_id is 'System-assigned identifier of the specific month end option. These are hard-coded values that MUST remain consistent client-to-client.';
comment on column pp_month_end_options.description is 'A description of the option.';
comment on column pp_month_end_options.long_description is 'A detailed description of the option.';
comment on column pp_month_end_options.option_value is 'The value of the option.';
comment on column pp_month_end_options.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column pp_month_end_options.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2266, 0, 2015, 1, 0, 0, 042299, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042299_cpr_cwip_01_create_PP_MONTH_END_OPTIONS_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;