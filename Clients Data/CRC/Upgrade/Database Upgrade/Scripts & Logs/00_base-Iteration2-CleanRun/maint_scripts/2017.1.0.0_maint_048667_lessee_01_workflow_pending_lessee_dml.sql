/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048667_lessee_01_workflow_pending_lessee_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 11/01/2017 Shane "C" Ward  Update any Lessee MLA and ILR in workflow to point to new lessee versions of subsytem
||
||============================================================================
*/

UPDATE workflow SET subsystem = 'lessee_ilr_approval' WHERE Lower(subsystem) = 'ilr_approval';
UPDATE workflow SET subsystem = 'lessee_mla_approval' WHERE Lower(subsystem) = 'mla_approval';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3861, 0, 2017, 1, 0, 0, 48667, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048667_lessee_01_workflow_pending_lessee_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;