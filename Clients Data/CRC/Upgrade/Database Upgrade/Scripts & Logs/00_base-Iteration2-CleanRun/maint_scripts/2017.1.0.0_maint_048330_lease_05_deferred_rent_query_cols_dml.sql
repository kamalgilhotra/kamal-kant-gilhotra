SET SERVEROUTPUT ON
/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_05_deferred_rent_query_cols_dml.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding query columns for deferred rent
||========================================================================================
*/

declare
   query_id number;
   column_id number;
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';
   PPCORA varchar2(10) := 'PPC-ORA' || '-> ';
   lease_check number;
   -- change value of LB_QUERY_CHECK to TRUE after queries have been
   -- compared to ensure no customizations are being overwritten
   LB_QUERY_CHECK boolean := false;
begin

DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

select count(*)
into lease_check
from ls_lease;

if lease_check <> 0 then
	if not LB_QUERY_CHECK then
		DBMS_OUTPUT.PUT_LINE(PPCORA || 'ORA' ||
                              '-02000 - Contact PPC support for help.  Read message below.');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  Please edit the maint_048330_lease_05_deferred_rent_query_cols_dml.sql script once the queries involved ');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  have been compared to ensure no client customizations are being overwritten.');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  After the queries have been checked and updates made accordingly to this script, ');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  change this line: LB_QUERY_CHECK boolean := false;');
         DBMS_OUTPUT.PUT_LINE(PPCMSG || '  to:               LB_QUERY_CHECK boolean := true;');
		 raise_application_error( -20000, PPCMSG || 'Please edit the maint_048330_lease_05_deferred_rent_query_cols_dml.sql script once the queries involved have been compared to ensure no client customizations are being overwritten. After the queries have been checked and updates made accordingly to this script, change this line: LB_QUERY_CHECK boolean := false; to:               LB_QUERY_CHECK boolean := true;');
	else
		DBMS_OUTPUT.PUT_LINE(PPCMSG || ' Query check not being performed.');
	end if;
else
	DBMS_OUTPUT.PUT_LINE(PPCMSG || ' Client does not currently use the lease module, checks for query customizations being skipped.');
end if;

update pp_any_query_criteria
set sql = 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2, las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10, las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10, las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE,
       las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_ST_DEFERRED_RENT, las.END_ST_DEFERRED_RENT
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(ll.lease_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''LEASE NUMBER'')'
  where upper(trim(description)) = 'LEASE ASSET SCHEDULE BY MLA'
  ;

update pp_any_query_criteria
set sql = 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2,  las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10,  las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,  las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE,
       las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_ST_DEFERRED_RENT, las.END_ST_DEFERRED_RENT
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(ilr.ilr_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''ILR NUMBER'')'
where upper(trim(description)) = 'LEASE ASSET SCHEDULE BY ILR'
;

update pp_any_query_criteria
set sql = 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description,
       ilr.ilr_id, ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location,
       las.REVISION, las.SET_OF_BOOKS_ID, to_char(las.MONTH, ''yyyymm'') as monthnum,
       las.BEG_CAPITAL_COST, las.END_CAPITAL_COST,
       las.BEG_OBLIGATION, las.END_OBLIGATION, las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL,
       las.INTEREST_PAID, las.PRINCIPAL_PAID, las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2,  las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4,
       las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6, las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10,
       las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,  las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7,
       las.EXECUTORY_PAID8, las.EXECUTORY_PAID9, las.EXECUTORY_PAID10,  las.CONTINGENT_ACCRUAL1,  las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3,
       las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5, las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9,
       las.CONTINGENT_ACCRUAL10,  las.CONTINGENT_PAID1,  las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6,
       las.CONTINGENT_PAID7, las.CONTINGENT_PAID8, las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,  las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE,
       las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_ST_DEFERRED_RENT, las.END_ST_DEFERRED_RENT
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ilr.ilr_id = ilro.ilr_id
  and ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.asset_location_id = al.asset_location_id
  and la.company_id = co.company_id
  and las.ls_asset_id = la.ls_asset_id
  and las.revision = la.approved_revision
  and upper(la.leased_asset_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''LEASED ASSET NUMBER'')'
where upper(trim(description)) = 'LEASE ASSET SCHEDULE BY LEASED ASSET'
;

select id
into query_id
from pp_any_query_criteria
where upper(trim(description)) = 'LEASE ASSET SCHEDULE BY MLA'
;

select max(column_order) + 1
into column_id
from pp_any_query_criteria_fields
where id = query_id
;

insert into pp_any_query_criteria_fields(id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field)
select query_id, 'beg_deferred_rent', column_id, 1, 1, 'Beg Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'beg_deferred_rent' and id = query_id) union all
select query_id, 'deferred_rent', column_id + 1, 1, 1, 'Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'deferred_rent' and id = query_id) union all
select query_id, 'end_deferred_rent', column_id + 2, 1, 1, 'End Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'end_deferred_rent' and id = query_id) union all
select query_id, 'beg_st_deferred_rent', column_id + 3, 1, 1, 'Beg ST Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'beg_st_deferred_rent' and id = query_id) union all
select query_id, 'end_st_deferred_rent', column_id + 4, 1, 1, 'End ST Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'end_st_deferred_rent' and id = query_id)
;

select id
into query_id
from pp_any_query_criteria
where upper(trim(description)) = 'LEASE ASSET SCHEDULE BY ILR'
;

select max(column_order) + 1
into column_id
from pp_any_query_criteria_fields
where id = query_id
;

insert into pp_any_query_criteria_fields(id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field)
select query_id, 'beg_deferred_rent', column_id, 1, 1, 'Beg Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'beg_deferred_rent' and id = query_id) union all
select query_id, 'deferred_rent', column_id + 1, 1, 1, 'Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'deferred_rent' and id = query_id) union all
select query_id, 'end_deferred_rent', column_id + 2, 1, 1, 'End Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'end_deferred_rent' and id = query_id) union all
select query_id, 'beg_st_deferred_rent', column_id + 3, 1, 1, 'Beg ST Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'beg_st_deferred_rent' and id = query_id) union all
select query_id, 'end_st_deferred_rent', column_id + 4, 1, 1, 'End ST Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'end_st_deferred_rent' and id = query_id)
;

select id
into query_id
from pp_any_query_criteria
where upper(trim(description)) = 'LEASE ASSET SCHEDULE BY LEASED ASSET'
;

select max(column_order) + 1
into column_id
from pp_any_query_criteria_fields
where id = query_id
;

insert into pp_any_query_criteria_fields(id, detail_field, column_order, amount_field, include_in_select_criteria, column_header, column_width, column_type, quantity_field)
select query_id, 'beg_deferred_rent', column_id, 1, 1, 'Beg Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'beg_deferred_rent' and id = query_id) union all
select query_id, 'deferred_rent', column_id + 1, 1, 1, 'Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'deferred_rent' and id = query_id) union all
select query_id, 'end_deferred_rent', column_id + 2, 1, 1, 'End Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'end_deferred_rent' and id = query_id) union all
select query_id, 'beg_st_deferred_rent', column_id + 3, 1, 1, 'Beg ST Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'beg_st_deferred_rent' and id = query_id) union all
select query_id, 'end_st_deferred_rent', column_id + 4, 1, 1, 'End ST Deferred Rent', 300, 'NUMBER', 0 from dual where not exists(select 1 from pp_any_query_criteria_fields where detail_field = 'end_st_deferred_rent' and id = query_id)
;

commit;

exception when others then
    raise_application_error(-20000, 'The Above SQL block did not execute properly. Please contact PowerPlan: '||sqlcode||': '||sqlerrm);
    rollback;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3557, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_05_deferred_rent_query_cols_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;