/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007595_cr_sys_cntrl.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/16/2011 Joseph King    Point Release
||============================================================================
*/

insert into CR_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, TIME_STAMP, USER_ID, LONG_DESCRIPTION)
   select 264,
          'Derivation: Rounding Priority',
          'abs(amount) desc',
          null,
          null,
          'The order by clause used to determine the record to plug any rounding adjustments.  Leaving this value blank will use the min(id) technique.'
     from DUAL
    where not exists (select 1
             from CR_SYSTEM_CONTROL
            where UPPER(CONTROL_NAME) = 'DERIVATION: ROUNDING PRIORITY');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (50, 0, 10, 3, 3, 0, 7595, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007595_cr_sys_cntrl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
