/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043460_reg_ifa_hist_layer_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 05/26/2015 Anand R        New workspace for IFA historical layer
||============================================================================
*/

insert into ppbase_workspace ( MODULE,WORKSPACE_IDENTIFIER,LABEL,WORKSPACE_UO_NAME,MINIHELP,OBJECT_TYPE_ID)
values ('REG', 'uo_reg_inc_adj_find_assets', 'Find Assets', 'uo_reg_inc_adj_find_assets', 'Find Assets', 1);


insert into ppbase_menu_items ( MODULE,MENU_IDENTIFIER,MENU_LEVEL,ITEM_ORDER,LABEL,MINIHELP,PARENT_MENU_IDENTIFIER,WORKSPACE_IDENTIFIER,ENABLE_YN)
values ('REG', 'FIND_ASSETS', 3, 7, 'Find Assets', 'Find Assets', 'IFA_TOP', 'uo_reg_inc_adj_find_assets', 1);

INSERT INTO REG_INCREMENTAL_ADJUST_TYPE (INCREMENTAL_ADJ_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION)
VALUES ( 5,	'Historic Layer', 'Historic Layer: Funding Project, Work Order, or Class Code Value');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2567, 0, 2015, 2, 0, 0, 43460, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043460_reg_ifa_hist_layer_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;