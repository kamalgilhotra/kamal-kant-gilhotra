/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_048330_lease_07_deferred_rent_trans_types_dml.sql
||========================================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version    Date       Created By           Reason for Change
|| ---------- ---------- -------------------- ----------------------------------------------
|| 2017.1.0.0 06/26/2017 Jared Schwantz     	Adding trans types for deferred rent
||========================================================================================
*/

insert into je_trans_type(trans_type, description)
select 3053, '3053 - Short Term Deferred Rent Credit' from dual union
select 3054, '3054 - Long Term Deferred Rent Credit' from dual union
select 3055, '3055 - Deferred Rent Adjustment Debit' from dual union
select 3056, '3056 - Backdated Deferred Rent Debit' from dual;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3555, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_07_deferred_rent_trans_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  
