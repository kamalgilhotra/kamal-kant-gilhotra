/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009388_depr_blend_type.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   02/06/2012 David Liss     Point Release
||============================================================================
*/

create table DEPR_BLENDING_TYPE_CONTROL
(
 DEPR_BLENDING_TYPE_ID number(22,0) not null,
 DESCRIPTION           varchar2(35),
 LONG_DESCRIPTION      varchar2(254),
 USER_ID               varchar2(18),
 TIME_STAMP            date
);

alter table DEPR_BLENDING_TYPE_CONTROL
   add constraint PK_DEPR_BLENDING_TYPE_CONTROL
       primary key (DEPR_BLENDING_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

create table DEPR_BLENDING_TYPE
(
 DEPR_BLENDING_TYPE_ID  number(22,0) not null,
 TARGET_SET_OF_BOOKS_ID number(22,0) not null,
 EFFECTIVE_DATE         date not null,
 SOURCE_SET_OF_BOOKS_ID number(22,0) not null,
 SOURCE_PERCENT         number(22,8),
 PRIORITY               number(22,0),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table DEPR_BLENDING_TYPE
   add constraint PK_DEPR_BLENDING_TYPE
       primary key (DEPR_BLENDING_TYPE_ID, TARGET_SET_OF_BOOKS_ID, SOURCE_SET_OF_BOOKS_ID, EFFECTIVE_DATE)
       using index tablespace PWRPLANT_IDX;

alter table DEPR_BLENDING_TYPE
   add constraint R_DEPR_BLENDING_TYPE1
       foreign key (DEPR_BLENDING_TYPE_ID)
       references DEPR_BLENDING_TYPE_CONTROL;

alter table DEPR_BLENDING_TYPE
   add constraint R_DEPR_BLENDING_TYPE2
       foreign key (TARGET_SET_OF_BOOKS_ID)
       references SET_OF_BOOKS;

alter table DEPR_BLENDING_TYPE
   add constraint R_DEPR_BLENDING_TYPE3
       foreign key (SOURCE_SET_OF_BOOKS_ID)
       references SET_OF_BOOKS;

alter table DEPRECIATION_METHOD
   add DEPR_BLENDING_TYPE_ID number(22,0);

alter table DEPRECIATION_METHOD
   add constraint R_DEPRECIATION_METHOD1
       foreign key (DEPR_BLENDING_TYPE_ID)
       references DEPR_BLENDING_TYPE_CONTROL;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('depr_blending_type_id', 'depreciation_method',
    TO_DATE('02-02-2012 11:31:56', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'depr_blending_type_control', 'p', null, 'Depr Blending Type', '', 5, '', '', null, '', '', '',
    null, '');

insert into POWERPLANT_DDDW
   (DROPDOWN_NAME, TABLE_NAME, CODE_COL, DISPLAY_COL)
values
   ('depr_blending_type_control', 'depr_blending_type_control', 'depr_blending_type_id',
    'description');

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,
    SUBSYSTEM_SCREEN, SELECT_WINDOW, SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT,
    BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX,
    POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('depr_blending_type_control', TO_DATE('27-01-2012 16:54:13', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 's', 'Depr Blending Type Control', 'Depr Blending Type Control', 'work', '', '', '',
    '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('depr_blending_type_id', 'depr_blending_type_control',
    TO_DATE('27-01-2012 16:54:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', '', 'e', null,
    'depr blending type id', '', 1, '', '', null, '', '', '', null, '');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'depr_blending_type_control',
    TO_DATE('27-01-2012 16:54:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', '', 'e', null,
    'description', '', 0, '', '', null, '', '', '', null, '');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'depr_blending_type_control',
    TO_DATE('27-01-2012 16:54:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', '', 'e', null,
    'long description', '', 3, '', '', null, '', '', '', null, '');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'depr_blending_type_control',
    TO_DATE('27-01-2012 16:54:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', '', 'e', null, 'time stamp',
    '', 100, '', '', null, '', '', '', null, '');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT,
    DROPDOWN_RESTRICT, RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'depr_blending_type_control',
    TO_DATE('27-01-2012 16:54:15', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', '', 'e', null, 'user id',
    '', 101, '', '', null, '', '', '', null, '');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (389, 0, 10, 4, 1, 0, 9388, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009388_depr_blend_type.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;