/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_041658_lease_pay_lessor_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 01/15/2015 		B.Beck				Adding Payment Tax JEs for Lease
||==========================================================================================
*/

update je_trans_type
set description = 'Lease Tax Debit Accrual'
where trans_type = 3040;

update je_trans_type
set description = 'Lease Tax Credit Accrual'
where trans_type = 3041;

insert into je_trans_type
(
	trans_type, description
)
values
(
	3045, 'Lease Tax Debit Payment'
);

insert into je_trans_type
(
	trans_type, description
)
values
(
	3046, 'Lease Tax Credit Payment'
);

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2171, 0, 10, 4, 3, 2, 041658, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041658_lease_pay_lessor_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;