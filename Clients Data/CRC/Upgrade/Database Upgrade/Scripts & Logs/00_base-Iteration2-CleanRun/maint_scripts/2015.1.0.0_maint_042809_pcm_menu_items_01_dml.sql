/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042809_pcm_menu_items_01_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 		B.Beck			Modify Left Hand Navigation
||==========================================================================================
*/

delete
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier like 'prog%'
and menu_level = 4;

delete
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier like 'prog%'
and menu_level = 3;

delete
from ppbase_menu_items
where module = 'pcm'
and parent_menu_identifier like 'prog%'
and menu_level = 2;

delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier = 'programs';

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier in
(
'prog_create',
'prog_search',
'prog_maint_info',
'prog_authorize',
'prog_forecast',
'prog_close',
'prog_est_cashflows',
'prog_maint_details',
'prog_maint_funding_projects',
'prog_maint_contacts',
'prog_maint_class_codes',
'prog_maint_accounts',
'prog_maint_other',
'prog_mon_dashboards',
'prog_mon_charges',
'prog_mon_commitments'
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'fp_pend_auth', 2, 3, 'Pending Authorizations', 
	'Pending Funding Project Authorizations', 'funding_projects', '', 1
);

insert into ppbase_menu_items
(
	module, menu_identifier, menu_level, item_order, label, 
	minihelp, parent_menu_identifier, workspace_identifier, enable_yn
)
values
(
	'pcm', 'fp_pend_budg_rev', 2, 4, 'Pending Budget Reviews', 
	'Pending Budget Reviews', 'funding_projects', '', 1
);

delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier in ('fp_close', 'fp_forecast');

delete
from ppbase_menu_items
where module = 'pcm'
and menu_identifier in ('fp_close', 'fp_forecast');

update ppbase_menu_items
set item_order = item_order + 10
where module = 'pcm'
and parent_menu_identifier = 'fp_search';

update ppbase_menu_items
set label = 'Authorization', item_order = 2
where module = 'pcm'
and menu_identifier = 'fp_authorize';

update ppbase_menu_items
set label = 'Budget Review', item_order = 3
where module = 'pcm'
and menu_identifier = 'fp_review';

update ppbase_menu_items
set label = 'Justification', item_order = 4
where module = 'pcm'
and menu_identifier = 'fp_justify';

update ppbase_menu_items
set label = 'Estimation', item_order = 5
where module = 'pcm'
and menu_identifier = 'fp_estimate';

update ppbase_menu_items
set label = 'Monitoring', item_order = 6
where module = 'pcm'
and menu_identifier = 'fp_monitor';

delete
from ppbase_workspace
where module = 'pcm'
and workspace_identifier in ('fp_close', 'fp_forecast');

update ppbase_menu_items
set item_order = 1
where module = 'pcm'
and menu_identifier = 'fp_maintain';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2284, 0, 2015, 1, 0, 0, 042809, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042809_pcm_menu_items_01_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;