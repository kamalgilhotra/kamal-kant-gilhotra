/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050181_lessor_03_reenable_pp_import_template_fields_fk_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/05/2018 Andrew Hill       Re-enable PP_IMPT_TMPLT_IMPT_COL_FK
||============================================================================
*/

ALTER TABLE pp_import_template_fields ADD CONSTRAINT PP_IMPT_TMPLT_IMPT_COL_FK 
FOREIGN KEY (import_type_id, column_name) references pp_import_column (import_type_id, column_name);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4278, 0, 2017, 3, 0, 0, 50181, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050181_lessor_03_reenable_pp_import_template_fields_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 