/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046761_pwrtax_01_pr_form_remove_rates_for_jurs_not_in_form_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 10/31/2016 Charlie Shilling remove lines from tax_plant_recon_form_rates who's jurisdictions are not in the form
||============================================================================
*/
--Remove invalid rates from tax_plant_recon_form_rates
DELETE FROM tax_plant_recon_form_rates
WHERE (tax_pr_form_id, jurisdiction_id) NOT IN (
	SELECT tax_pr_form_id, jurisdiction_id
	FROM tax_plant_recon_form_jur
);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3333, 0, 2016, 1, 0, 0, 046761, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046761_pwrtax_01_pr_form_remove_rates_for_jurs_not_in_form_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;