/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042868_pcm_fp_req.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/012/2015 Alex P.         Make sure FP is required when editable
||                                        on WO Info screen
||============================================================================
*/

insert into pp_required_table_column( id, table_name, column_name, objectpath, description, required_column_expression)
values( 1019, 'work_order_control', 'funding_wo_id', 'uo_pcm_maint_info.dw_detail', 'Funding Project', 'funding_wo_indicator=0' );


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2278, 0, 2015, 1, 0, 0, 042868, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042868_pcm_fp_req.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;