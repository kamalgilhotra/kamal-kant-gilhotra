/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048905_lessor_01_add_je_and_disposition_codes_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 02/07/2018 J Sisouphanh     Create Lessor JE codes and Disposition Codes
||============================================================================
*/

--STANDARD_JOURNAL_ENTRIES
insert into STANDARD_JOURNAL_ENTRIES
   (JE_ID, GL_JE_CODE, EXTERNAL_JE_CODE, DESCRIPTION, LONG_DESCRIPTION)
   select max(A.JE_ID) + 1,
          'LSR Derecognition',
          'Lessor Derecognition',
          'Lessor Derecognition',
          'Lessor Derecognition'
     from STANDARD_JOURNAL_ENTRIES A;

insert into GL_JE_CONTROL
   (PROCESS_ID, JE_ID, DR_TABLE, DR_COLUMN, CR_TABLE, CR_COLUMN)
   select S.GL_JE_CODE, S.JE_ID, 'NONE', 'NONE', 'NONE', 'NONE'
     from STANDARD_JOURNAL_ENTRIES S
    where trim(S.GL_JE_CODE) in ('LSR Derecognition');	  

--DISPOSITION_CODE
insert into DISPOSITION_CODE
	(disposition_code, description, long_description, disposition_type)
	select max(A.DISPOSITION_CODE) + 1, 
			'Derecognize Leased Asset', 
			'Derecognize Leased Asset', 
			1
	from DISPOSITION_CODE A;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4118, 0, 2017, 3, 0, 0, 48905, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_048905_lessor_01_add_je_and_disposition_codes_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 