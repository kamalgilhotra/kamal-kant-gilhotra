/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034961_reg_REG_HIST_CWIP_RATE_BASE_VIEW.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/11/2014 Shane "C" Ward
||============================================================================
*/

--CWIP Rate Base View
create or replace VIEW REG_HIST_CWIP_RATE_BASE_VIEW(
  WORK_ORDER_ID, WORK_ORDER_DESCRIPTION, COMPANY_ID, MAJOR_LOCATION_ID,
  ASSET_LOCATION_ID, BUS_SEGMENT_ID, WORK_ORDER_TYPE_ID, FUNC_CLASS_ID,
  EFFECTIVE_DATE, CWIP_IN_BASE, RETENTIONS, REG_ACCT_DESCRIPTION) as(
   select C.WORK_ORDER_ID,
          C.DESCRIPTION WORK_ORDER_DESCRIPTION,
          C.COMPANY_ID,
          C.MAJOR_LOCATION_ID,
          C.ASSET_LOCATION_ID,
          C.BUS_SEGMENT_ID,
          C.WORK_ORDER_TYPE_ID,
          A.FUNC_CLASS_ID,
          TO_CHAR(EFFECTIVE_DATE, 'mm/yyyy') EFFECTIVE_DATE,
          B.CWIP_IN_BASE,
          B.RETENTIONS,
          F.FILTER_STRING REG_ACCT_DESCRIPTION
     from CWIP_IN_RATE_BASE         B,
          WORK_ORDER_CONTROL        C,
          CR_DD_REQUIRED_FILTER     F,
          CR_DD_REQUIRED_FILTER     MN,
          WORK_ORDER_ACCOUNT        A,
          REG_CWIP_COMPONENT_VALUES v,
          CR_DD_REQUIRED_FILTER     COMP
    where B.WORK_ORDER_ID = C.WORK_ORDER_ID
      and F.COLUMN_NAME = 'REG_ACCT_DESCRIPTION'
      and A.WORK_ORDER_ID = C.WORK_ORDER_ID
      and MN.COLUMN_NAME = 'MONTH_NUMBER'
      and MN.FILTER_STRING = TO_CHAR(EFFECTIVE_DATE, 'yyyymm')
      and B.JURISDICTION_ID = V.ID_VALUE
      and V.REG_COMPONENT_ID = To_Number(comp.filter_string)
      and COMP.COLUMN_NAME = 'REG_COMPONENT_ID'
      and V.REG_FAMILY_ID = 3);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1036, 0, 10, 4, 2, 0, 34961, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034961_reg_REG_HIST_CWIP_RATE_BASE_VIEW.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;