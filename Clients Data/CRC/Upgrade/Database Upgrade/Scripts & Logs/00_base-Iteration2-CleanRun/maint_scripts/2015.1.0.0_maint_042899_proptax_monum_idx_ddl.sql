--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042899_proptax_monum_idx_ddl.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/06/2015 B Borm, A Scott  create new index on month_number in the pt_value_vault table.
--||============================================================================
--*/

create index pt_val_vault_mo_idx
on pt_value_vault (month_number) tablespace pwrplant_idx;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2355, 0, 2015, 1, 0, 0, 42899, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042899_proptax_monum_idx_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;