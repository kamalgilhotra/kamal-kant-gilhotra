 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045176_prov_20152_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2 11/03/2015 	Jarrett Skov   Adds gl_data_control_id column to tax_accrual_cr_drill and tax_accrual_cr_drill_explode for easier verification of CR Pulls
 ||============================================================================
 */ 


-- tax_accrual_cr_drill
alter table tax_accrual_cr_drill
add gl_data_control_id number(22,0);

comment on column tax_accrual_cr_drill.gl_data_control_id
is 'System-assigned identifier for a tax_accrual_gl_data_control record.';

CREATE INDEX ta_cr_drill_gl_data_con_id
  ON tax_accrual_cr_drill (
    gl_data_control_id
  )
 TABLESPACE pwrplant_idx
;


-- tax_accrual_cr_drill_explode
alter table tax_accrual_cr_drill_explode
add gl_data_control_id number(22,0);

comment on column tax_accrual_cr_drill_explode.gl_data_control_id
is 'System-assigned identifier for a tax_accrual_gl_data_control record.';

CREATE INDEX ta_cr_drill_exp_gl_data_con_id
  ON tax_accrual_cr_drill_explode (
    gl_data_control_id
  )
 TABLESPACE pwrplant_idx
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2960, 0, 2015, 2, 0, 0, 45176, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045176_prov_20152_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;