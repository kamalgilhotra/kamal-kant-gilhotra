/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039190_pwrtax_fcst_case.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/19/2014 Alex P.
||============================================================================
*/

update PP_DYNAMIC_FILTER
   set SQLS_COLUMN_EXPRESSION = 'tax_depreciation.tax_layer_id', DW = 'dw_tax_layer_filter'
 where FILTER_ID = 100;

delete from PP_DYNAMIC_FILTER_MAPPING
 where FILTER_ID = 100
   and PP_REPORT_FILTER_ID = 75;

update PP_DYNAMIC_FILTER
   set LABEL = replace(LABEL, 'Depr-', '')
 where FILTER_ID in (select FILTER_ID from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 60);

update PP_DYNAMIC_FILTER
   set LABEL = replace(LABEL, 'DFIT-', '')
 where FILTER_ID in (select FILTER_ID from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 61);

update PP_DYNAMIC_FILTER
   set LABEL = replace(LABEL, 'FCST-', '')
 where FILTER_ID in (select FILTER_ID from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 62);

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (60, 172);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (61, 172);
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (62, 156);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, REQUIRED, EXCLUDE_FROM_WHERE, INVISIBLE)
values
   (190, 'Tax Year ( End )', 'sle', 1, 1, 0);

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID, FILTER_ID) values (62, 190);

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('powertax', 'forecast_manage', 'Forecast Management', 'uo_tax_fcst_wksp_manage', 'Forecat Management');

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where MODULE = 'powertax'
   and PARENT_MENU_IDENTIFIER = 'forecast';

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('powertax', 'forecast_manage', 2, 1, 'Manage Forecasts', 'forecast', 'forecast_manage', 1);

alter table TAX_FORECAST_VERSION modify create_date date;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1362, 0, 10, 4, 3, 0, 39190, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039190_pwrtax_fcst_case.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;