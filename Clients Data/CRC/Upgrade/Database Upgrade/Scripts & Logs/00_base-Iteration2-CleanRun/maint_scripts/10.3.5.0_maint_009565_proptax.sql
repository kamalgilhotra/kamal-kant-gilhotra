/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009565_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/09/2012 Andrew Scott   Point Release
||============================================================================
*/

update PTC_SYSTEM_OPTIONS
   set PP_DEFAULT_VALUE = 'All'
 where SYSTEM_OPTION_ID = 'Returns - CPR Tree - Creation - Assets to Use';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (178, 0, 10, 3, 5, 0, 9565, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009565_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
