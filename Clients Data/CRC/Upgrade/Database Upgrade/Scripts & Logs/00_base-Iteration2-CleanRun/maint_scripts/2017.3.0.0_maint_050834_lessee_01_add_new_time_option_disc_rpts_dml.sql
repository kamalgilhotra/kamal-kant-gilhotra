/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050834_lessee_01_add_new_time_option_disc_rpts_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 4/3/2018  Alex Healey    Adding new report time options for multicurrency disclosure reports
||============================================================================
*/

INSERT INTO PP_REPORTS_TIME_OPTION
            (pp_report_time_option_id,
             description,
             parameter_uo_name,
             dwname1,
             label1,
             keycolumn1,
             dwname2,
             label2,
             keycolumn2)
VALUES      (207,
             'Lease Curr type, SOB, Month',
             'uo_ppbase_report_parms_dddw_two_mnum',
             'dddw_ls_currency_type',
             'Currency Type',
             'ls_currency_type_id',
             'dw_ls_set_of_books',
             'Set of Books',
             'set_of_books_id');

INSERT INTO PP_REPORTS_TIME_OPTION
            (pp_report_time_option_id,
             description,
             parameter_uo_name,
             dwname1,
             label1,
             keycolumn1,
             dwname2,
             label2,
             keycolumn2)
VALUES      (208,
             'Lease Curr type, SOB, Month Span',
             'uo_ppbase_report_parms_dddw_two_mnum_spn',
             'dddw_ls_currency_type',
             'Currency Type',
             'ls_currency_type_id',
             'dw_ls_set_of_books',
             'Set of Books',
             'set_of_books_id'); 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4254, 0, 2017, 3, 0, 0, 50834, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050834_lessee_01_add_new_time_option_disc_rpts_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
