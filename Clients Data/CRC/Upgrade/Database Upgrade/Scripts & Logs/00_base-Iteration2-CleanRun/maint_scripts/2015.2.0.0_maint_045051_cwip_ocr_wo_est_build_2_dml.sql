/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045051_cwip_ocr_wo_est_build_2_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By            Reason for Change
|| -------- ---------- --------------------  ---------------------------------
|| 2015.2   10/29/2015 Anand R              Add new rows to pp_wo_est_customize
||============================================================================
*/ 


begin 
  
   -- undo the insert from maint_045051_cwip_ocr_wo_est_build_dml.sql first
   delete from pp_wo_est_customize where company_id = -1 and tab = 1 and lower(column_name) = 'ocr_api' ;
   
   -- Loop through each company and add new column 'ocr_api' for tabs 1, 2, 3
  
   for i in (
      select distinct company_id  from pp_wo_est_customize 
      )
   loop

      insert into pp_wo_est_customize (COMPANY_ID, TAB, COLUMN_NAME, DISPLAY, COLUMN_ORDER, COLUMN_WIDTH, CR_ELEMENT_ID, DDDW_SQL)
      values (i.company_id, 1, 'ocr_api', 1, 105, 250, null, null);
  
      insert into pp_wo_est_customize (COMPANY_ID, TAB, COLUMN_NAME, DISPLAY, COLUMN_ORDER, COLUMN_WIDTH, CR_ELEMENT_ID, DDDW_SQL)
      values (i.company_id, 2, 'ocr_api', 0, 305, 250, null, null);
  
      insert into pp_wo_est_customize (COMPANY_ID, TAB, COLUMN_NAME, DISPLAY, COLUMN_ORDER, COLUMN_WIDTH, CR_ELEMENT_ID, DDDW_SQL)
      values (i.company_id, 3, 'ocr_api', 0, 305, 250, null, null);
      
   end loop;
   
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2949, 0, 2015, 2, 0, 0, 45051, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045051_cwip_ocr_wo_est_build_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;