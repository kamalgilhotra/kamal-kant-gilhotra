/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042281_pcm_revision_fields_dml.sql
||========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	02/02/2015 Ryan Oliveria		Make collapsed dates required in rev details
||========================================================================================
*/

insert into PP_REQUIRED_TABLE_COLUMN
	(ID, TABLE_NAME, COLUMN_NAME, OBJECTPATH, DESCRIPTION)
select ID + 3, TABLE_NAME, COLUMN_NAME||'_1', OBJECTPATH, DESCRIPTION
  from PP_REQUIRED_TABLE_COLUMN
 where TABLE_NAME = 'work_order_approval'
   and COLUMN_NAME in ('est_start_date','est_in_service_date','est_complete_date');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2243, 0, 2015, 1, 0, 0, 042281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042281_pcm_revision_fields_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;