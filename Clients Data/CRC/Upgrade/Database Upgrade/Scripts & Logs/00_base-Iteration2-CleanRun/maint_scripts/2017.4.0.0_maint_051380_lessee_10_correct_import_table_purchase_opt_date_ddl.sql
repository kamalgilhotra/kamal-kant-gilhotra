/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051380_lessee_10_correct_import_table_purchase_opt_date_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/30/2018  Alex Healey    Correct the field definitions for the two date fields
||============================================================================
*/

ALTER TABLE ls_import_ilr_purchase_opt
MODIFY  decision_date VARCHAR2(35);

ALTER TABLE ls_import_ilr_purchase_opt
MODIFY  purchase_date VARCHAR2(35);

ALTER TABLE ls_import_ilr_purchase_opt_arc
MODIFY  decision_date VARCHAR2(35);

ALTER TABLE ls_import_ilr_purchase_opt_arc
MODIFY  purchase_date VARCHAR2(35);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6102, 0, 2017, 4, 0, 0, 51380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051380_lessee_10_correct_import_table_purchase_opt_date_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;