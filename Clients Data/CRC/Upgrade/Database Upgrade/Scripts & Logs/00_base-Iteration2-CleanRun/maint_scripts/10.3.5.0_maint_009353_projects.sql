/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009353_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/06/2012 Chris Mardis   Point Release
||============================================================================
*/

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    HARD_EDIT)
values
   (61, 'WO/FP Reject a Revision (Pre)', 'WO/FP Reject a Revision (Pre)', 'f_workflow_base',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'revision', 1);

insert into WO_VALIDATION_TYPE
   (WO_VALIDATION_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, "FUNCTION", FIND_COMPANY, COL1, COL2,
    HARD_EDIT)
values
   (62, 'WO/FP Reject a Revision (Post)', 'WO/FP Reject a Revision (Post)', 'f_workflow_base',
    'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id',
    'revision', 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (172, 0, 10, 3, 5, 0, 9353, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009353_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
