/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049655_lessor_02_add_invoice_approval_fks_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/20/2017 Charlie Shilling add foreign keys to the lsr_invoice_approval table
||============================================================================
*/

ALTER TABLE lsr_invoice_approval
	ADD CONSTRAINT lsr_invoice_appr_workflow_type FOREIGN KEY (
		approval_type_id
	) REFERENCES workflow_type (
		workflow_type_id
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3992, 0, 2017, 1, 0, 0, 49655, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049655_lessor_02_add_invoice_approval_fks_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
			 
