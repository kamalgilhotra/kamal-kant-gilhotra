 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048884_lessor_01_add_ilr_accounts_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 11/14/2017 Anand R        Add accounts to Lessor ILR 
 ||============================================================================
 */ 

create table LSR_ILR_ACCOUNT (
        ILR_ID                        number(22,0),
        INT_ACCRUAL_ACCOUNT_ID        number(22,0),
        INT_EXPENSE_ACCOUNT_ID        number(22,0),
        EXEC_ACCRUAL_ACCOUNT_ID       number(22,0),
        EXEC_EXPENSE_ACCOUNT_ID       number(22,0),
        CONT_ACCRUAL_ACCOUNT_ID       number(22,0),
        CONT_EXPENSE_ACCOUNT_ID       number(22,0),
        ST_RECEIVABLE_ACCOUNT_ID      number(22,0),
        LT_RECEIVABLE_ACCOUNT_ID      number(22,0),
        AR_ACCOUNT_ID                 number(22,0),
        UNGUARAN_RES_ACCOUNT_ID       number(22,0),
        INT_UNGUARAN_RES_ACCOUNT_ID   number(22,0),
        SELL_PROFIT_LOSS_ACCOUNT_ID   number(22,0),
        INI_DIRECT_COST_ACCOUNT_ID    number(22,0),
        PROP_PLANT_ACCOUNT_ID         number(22,0),
        ST_DEFERRED_ACCOUNT_ID        number(22,0),
        LT_DEFERRED_ACCOUNT_ID        number(22,0),
        CURR_GAIN_LOSS_ACCT_ID        number(22,0),
        CURR_GAIN_LOSS_OFFSET_ACCT_ID number(22,0),
		user_id                       varchar2(18),
        time_stamp                    date
    );

comment on table  LSR_ILR_ACCOUNT                               is '(O)  [06] This table stores accounts associated to Lessor Lease.';    
comment on column LSR_ILR_ACCOUNT.ILR_ID                        is 'The system assigned identifier or Lessor Lease';
comment on column LSR_ILR_ACCOUNT.INT_ACCRUAL_ACCOUNT_ID        is 'The interest accrual account';
comment on column LSR_ILR_ACCOUNT.INT_EXPENSE_ACCOUNT_ID        is 'The interest expense account';
comment on column LSR_ILR_ACCOUNT.EXEC_ACCRUAL_ACCOUNT_ID       is 'The executory accrual account';
comment on column LSR_ILR_ACCOUNT.EXEC_EXPENSE_ACCOUNT_ID       is 'The executory expense account';
comment on column LSR_ILR_ACCOUNT.CONT_ACCRUAL_ACCOUNT_ID       is 'The contingent accrual account';
comment on column LSR_ILR_ACCOUNT.CONT_EXPENSE_ACCOUNT_ID       is 'The contingent expense account';
comment on column LSR_ILR_ACCOUNT.ST_RECEIVABLE_ACCOUNT_ID      is 'The short term receivables account';
comment on column LSR_ILR_ACCOUNT.LT_RECEIVABLE_ACCOUNT_ID      is 'The long term receivables account';
comment on column LSR_ILR_ACCOUNT.AR_ACCOUNT_ID                 is 'The accounts receivable account';
comment on column LSR_ILR_ACCOUNT.UNGUARAN_RES_ACCOUNT_ID       is 'The unguaranteed residual account';
comment on column LSR_ILR_ACCOUNT.INT_UNGUARAN_RES_ACCOUNT_ID   is 'The interest on unguaranteed residual account';
comment on column LSR_ILR_ACCOUNT.SELL_PROFIT_LOSS_ACCOUNT_ID   is 'The selling profit/loss account';
comment on column LSR_ILR_ACCOUNT.INI_DIRECT_COST_ACCOUNT_ID    is 'The initial direct cost account';
comment on column LSR_ILR_ACCOUNT.PROP_PLANT_ACCOUNT_ID         is 'The property plant and equipment account';
comment on column LSR_ILR_ACCOUNT.ST_DEFERRED_ACCOUNT_ID        is 'The short term deferred rent account';
comment on column LSR_ILR_ACCOUNT.LT_DEFERRED_ACCOUNT_ID        is 'The long term deferred rent account';
comment on column LSR_ILR_ACCOUNT.CURR_GAIN_LOSS_ACCT_ID        is 'The currency gain/loss account';
comment on column LSR_ILR_ACCOUNT.CURR_GAIN_LOSS_OFFSET_ACCT_ID is 'The currency gain/loss offset account';
comment on column LSR_ILR_ACCOUNT.USER_ID                       is 'Standard system-assigned user id used for audit purposes.';
comment on column LSR_ILR_ACCOUNT.TIME_STAMP                    is 'Standard system-assigned timestamp used for audit purposes.';

alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_PK
       primary key (ILR_ID)
       using index TABLESPACE PWRPLANT_IDX;

alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK1
       foreign key (ILR_ID)
       references LSR_ILR (ILR_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK2
       foreign key (INT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK3
       foreign key (INT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK4
       foreign key (EXEC_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK5
       foreign key (EXEC_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK6
       foreign key (CONT_ACCRUAL_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK7
       foreign key (CONT_EXPENSE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK8
       foreign key (ST_RECEIVABLE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK9
       foreign key (LT_RECEIVABLE_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK10
       foreign key (AR_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK11
       foreign key (UNGUARAN_RES_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK12
       foreign key (INT_UNGUARAN_RES_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK13
       foreign key (SELL_PROFIT_LOSS_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK14
       foreign key (INI_DIRECT_COST_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK15
       foreign key (PROP_PLANT_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK16
       foreign key (ST_DEFERRED_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK17
       foreign key (LT_DEFERRED_ACCOUNT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK18
       foreign key (CURR_GAIN_LOSS_ACCT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);
     
alter table LSR_ILR_ACCOUNT
   add constraint LSR_ILR_ACCOUNT_FK19
       foreign key (CURR_GAIN_LOSS_OFFSET_ACCT_ID)
       references GL_ACCOUNT (GL_ACCOUNT_ID);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3975, 0, 2017, 1, 0, 0, 48884, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048884_lessor_01_add_ilr_accounts_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	   