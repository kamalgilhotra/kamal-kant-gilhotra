/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_051029_lessee_01_add_mec_usertime_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.1 04/27/2018 Crystal Yura     Add Lessee MEC Config missing user/time_stamp columns
||============================================================================
*/
alter table ls_mec_config add user_id VARCHAR2(18);

alter table ls_mec_config add TIME_STAMP DATE;

comment on column ls_mec_config.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_mec_config.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6842, 0, 2017, 3, 0, 1, 51029, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051029_lessee_01_add_mec_usertime_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;