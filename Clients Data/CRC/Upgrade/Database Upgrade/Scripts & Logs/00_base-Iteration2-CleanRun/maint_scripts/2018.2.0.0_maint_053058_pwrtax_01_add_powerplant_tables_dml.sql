/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053058_pwrtax_01_add_powerplant_tables_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 02/13/2019 David Conway   Add tables: tax_credit, tax_credit_schema.
|| 2018.2.0.0 02/18/2019 David Conway   Corrected edit type on Ordering column.
||============================================================================
*/
/* Set up TAX_CREDIT for Table Maintenance / security */

insert into POWERPLANT_TABLES
(table_name,pp_table_type_id,description,long_description,subsystem_screen,subsystem_display,subsystem)
select 'tax_credit','s','Tax Credit','Tax Credit','tax','tax','tax'
from dual
where not exists( select 1 from powerplant_tables where table_name = 'tax_credit' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'tax_credit_id','tax_credit','s','Tax Credit ID','Tax Credit ID',1,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit' and column_name = 'tax_credit_id' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'time_stamp','tax_credit','e','Time Stamp','Time Stamp',100,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit' and column_name = 'time_stamp' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'user_id','tax_credit','e','User ID','User ID',101,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit' and column_name = 'user_id' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'description','tax_credit','e','Description','Description',2,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit' and column_name = 'description' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'ext_tax_credit','tax_credit','e','Ext Tax Credit','Ext Tax Credit',3,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit' and column_name = 'ext_tax_credit' );

-- Set up TAX_CREDIT_SCHEMA for Table Maintenance / security

insert into POWERPLANT_TABLES
(table_name,pp_table_type_id,description,long_description,subsystem_screen,subsystem_display,subsystem)
select 'tax_credit_schema','s','Tax Credit Schema','Tax Credit Schema','tax','tax','tax'
from dual
where not exists( select 1 from powerplant_tables where table_name = 'tax_credit_schema' );

insert into POWERPLANT_DDDW
(dropdown_name, table_name, code_col, display_col)
select 'tax_credit', 'tax_credit', 'tax_credit_id', 'description'
from dual
where not exists( select 1 from powerplant_dddw where dropdown_name = 'tax_credit' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'tax_credit_id','tax_credit_schema','tax_credit','p','Tax Credit','Tax Credit',1,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit_schema' and column_name = 'tax_credit_id' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,DROPDOWN_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'tax_limit_id','tax_credit_schema','tax_limit','p','Tax Limit','Tax Limit',2,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit_schema' and column_name = 'tax_limit_id' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'ordering','tax_credit_schema','e','Ordering','Ordering',3,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit_schema' and column_name = 'ordering' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'time_stamp','tax_credit_schema','e','Time Stamp','Time Stamp',100,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit_schema' and column_name = 'time_stamp' );

insert into POWERPLANT_COLUMNS
(COLUMN_NAME,TABLE_NAME,PP_EDIT_TYPE_ID,DESCRIPTION,LONG_DESCRIPTION,COLUMN_RANK,READ_ONLY)
select 'user_id','tax_credit_schema','e','User ID','User ID',101,0
from dual
where not exists( select 1 from powerplant_columns where table_name = 'tax_credit_schema' and column_name = 'user_id' );

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15042, 0, 2018, 2, 0, 0, 53058, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053058_pwrtax_01_add_powerplant_tables_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;