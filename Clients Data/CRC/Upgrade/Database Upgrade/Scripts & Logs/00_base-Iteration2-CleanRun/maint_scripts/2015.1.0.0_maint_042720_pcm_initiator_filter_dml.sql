/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042720_pcm_initiator_filter_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/05/2015 Alex P.          Initiation Filter
||============================================================================
*/

update pp_dynamic_filter
set sqls_column_expression = 'lower( work_order_initiator.users )'
where label = 'Initiator'
  and sqls_column_expression = 'work_order_initiator.users';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2253, 0, 2015, 1, 0, 0, 042720, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042720_pcm_initiator_filter_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;