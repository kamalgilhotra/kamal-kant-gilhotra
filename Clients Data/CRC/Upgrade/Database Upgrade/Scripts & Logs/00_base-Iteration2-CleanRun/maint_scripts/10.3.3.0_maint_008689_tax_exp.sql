/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008689_tax_exp.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/28/2011 Elhadj Bah     Point Release
||============================================================================
*/

alter table WO_TAX_EXPENSE_TEST
   add TAX_STATUS_OVH_TO_UDG number(22, 0);

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'Tax Exp - Ovh to Udgrd Rplcmt Check',
          'yes',
          'dw_yes_no;1',
          '""Yes"" will check for cases where overhead conductor was replaced with underground conductor and if so, will assign the WO the ""Tax Status if Ovh w. Udg"" status.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (55, 0, 10, 3, 3, 0, 8689, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008689_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
