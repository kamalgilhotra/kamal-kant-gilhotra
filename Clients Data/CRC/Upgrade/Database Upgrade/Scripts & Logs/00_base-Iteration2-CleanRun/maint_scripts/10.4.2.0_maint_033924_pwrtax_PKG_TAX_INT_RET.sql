/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033924_pwrtax_PKG_TAX_INT_RET.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- ------------------------------------
|| 10.4.2.0 12/4/2013  Andrew Scott        phase two changes for the tax retirements
||                                         package.
||============================================================================
*/

create or replace package PKG_TAX_INT_RET as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_TAX_INT_RET
   || Description: Moved/Enhanced PowerTax Retirements Interface logic to PLSQL
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.2.0 10/14/2013 Andrew Scott   Original Version
   || 10.4.2.0 10/24/2013 Andrew Scott   QA changes: removed tax class join,
   ||                                      trunc-ed year on tax_book_transactions_grp,
   ||                                      audit trail grp.
   || 10.4.2.0 10/29/2013 Andrew Scott   fixed to make FIFO pick from earliest
   || 10.4.2.0 10/30/2013 Andrew Scott   removed tolerance from logic
   || 10.4.2.0 11/20/2013 Andrew Scott   non-FIFO rets issue where sum book > trid book
   ||                                    causes allocation ratios to blow up the retirement amount
   || 10.4.2.0 11/25/2013 Andrew Scott   changed decode fix from 11/20 to use sign of tbt.trans_amount
   ||                                    instead of trying to compare sum book to trid book.  also changed
   ||                                    order by to process negatives first, then by abs of book balance
   ||
   ||============================================================================
   */

   type CO_IDS is table of number(22, 0) index by binary_integer;

   -- procedure to backfill tax_book_translate with the new tbt_group_id
   procedure P_BACKFILL_TBT_GROUP;

   -- function to refresh the rows in tax_interface_cos
   function F_REFRESH_INTERFACE_COS(A_CO_IDS           in CO_IDS,
                                    A_INTERFACE_OBJECT varchar2) return varchar2;

   -- function the return the process id for retirements interface
   function F_GETPROCESS return number;

   -- function to clear out previously processed retirements
   function F_REMOVE_PRIOR_TRANS(A_VERSION_ID       number,
                                 A_TAX_YEAR         number,
                                 A_INTERFACE_OBJECT varchar2) return varchar2;

   -- function to process a batch of data (each iteration could run many batches)
   function F_RUN_BATCH(A_TAX_YEAR         number,
                        A_START_MO         number,
                        A_END_MO           number,
                        A_VERSION_ID       number,
                        A_ITERATION        number,
                        A_BATCH            number,
                        A_INTERFACE_OBJECT varchar2) return varchar2;

   -- function to process the retirements
   function F_RUN_ITERATION(A_TAX_YEAR         number,
                            A_START_MO         number,
                            A_END_MO           number,
                            A_VERSION_ID       number,
                            A_INTERFACE_OBJECT varchar2) return varchar2;

   -- function to start the log, clear out the retirements, process the retirements, end the log
   function F_POWERTAX_RETS(A_TAX_YEAR         number,
                            A_START_MO         number,
                            A_END_MO           number,
                            A_VERSION_ID       number,
                            A_INTERFACE_OBJECT varchar2) return varchar2;

end PKG_TAX_INT_RET;
/


create or replace package body PKG_TAX_INT_RET as

   --****************************************************************************************************
   --               VARIABLES
   --****************************************************************************************************
   L_PROCESS_ID number;
   L_DEBUG_CODE number;

   --****************************************************************************************************
   --               Start Body
   --****************************************************************************************************

   --****************************************************************************************************
   --               PROCEDURES
   --****************************************************************************************************

   --procedure grabs the retirements interface process id for the online logs
   procedure P_SETPROCESS is
   begin
      select PROCESS_ID
        into L_PROCESS_ID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = 'POWERTAX-RETIREMENTS';
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_PROCESS_ID := -1;
   end P_SETPROCESS;

   --procedure to backfill the tbt_group_id on tax_book_translate.
   procedure P_BACKFILL_TBT_GROUP is

      type TBT_GRP_BACKFILL_STG_REC_TYPE is record(
         TBT_GROUP_ID       TAX_BOOK_TRANSLATE_GROUP.TBT_GROUP_ID%type,
         COMPANY_ID         TAX_BOOK_TRANSLATE.COMPANY_ID%type,
         BUS_SEGMENT_ID     TAX_BOOK_TRANSLATE.BUS_SEGMENT_ID%type,
         UTILITY_ACCOUNT_ID TAX_BOOK_TRANSLATE.UTILITY_ACCOUNT_ID%type,
         SUB_ACCOUNT_ID     TAX_BOOK_TRANSLATE.SUB_ACCOUNT_ID%type,
         TAX_LOCATION_ID    TAX_BOOK_TRANSLATE.TAX_LOCATION_ID%type,
         CLASS_CODE_ID      TAX_BOOK_TRANSLATE.CLASS_CODE_ID%type,
         CLASS_CODE_VALUE   TAX_BOOK_TRANSLATE.CLASS_CODE_VALUE%type,
         GL_ACCOUNT_ID      TAX_BOOK_TRANSLATE.GL_ACCOUNT_ID%type,
         TAX_DISTINCTION_ID TAX_BOOK_TRANSLATE.TAX_DISTINCTION_ID%type);

      type TBT_GRP_BACKFILL_STG_TBL_TYPE is table of TBT_GRP_BACKFILL_STG_REC_TYPE;

      TBT_GRP_BACKFILL_STG_TBL TBT_GRP_BACKFILL_STG_TBL_TYPE;

   begin

      select TBT_GROUP_ID,
             NVL(COMPANY_ID, 0),
             NVL(BUS_SEGMENT_ID, 0),
             NVL(UTILITY_ACCOUNT_ID, 0),
             NVL(SUB_ACCOUNT_ID, 0),
             NVL(TAX_LOCATION_ID, 0),
             NVL(CLASS_CODE_ID, 0),
             NVL(CLASS_CODE_VALUE, 'xxxxx'),
             NVL(GL_ACCOUNT_ID, 0),
             NVL(TAX_DISTINCTION_ID, 0) bulk collect
        into TBT_GRP_BACKFILL_STG_TBL
        from (select distinct TAX_BOOK_TRANSLATE_GROUP.TBT_GROUP_ID, TBT.*
                from TAX_BOOK_TRANSLATE_GROUP,
                     (select INSIDE.COMPANY_ID,
                             INSIDE.BUS_SEGMENT_ID,
                             INSIDE.UTILITY_ACCOUNT_ID,
                             INSIDE.SUB_ACCOUNT_ID,
                             INSIDE.TAX_LOCATION_ID,
                             INSIDE.CLASS_CODE_ID,
                             INSIDE.CLASS_CODE_VALUE,
                             INSIDE.GL_ACCOUNT_ID,
                             INSIDE.TAX_DISTINCTION_ID,
                             LISTAGG(TAX_CLASS_ID, ':') WITHIN group(order by TAX_CLASS_ID) TAX_CLASS_ID_AGG,
                             LISTAGG(START_EFF_BOOK_VINTAGE, ':') WITHIN group(order by TAX_CLASS_ID) START_EFF_BOOK_VINTAGE_AGG,
                             LISTAGG(END_EFF_BOOK_VINTAGE, ':') WITHIN group(order by TAX_CLASS_ID) END_EFF_BOOK_VINTAGE_AGG,
                             LISTAGG(ACTIVE, ':') WITHIN group(order by TAX_CLASS_ID) ACTIVE_AGG
                        from (select TBT.*,
                                     TBT_TAX_CLASS.TAX_CLASS_ID,
                                     TBT_TAX_CLASS.START_EFF_BOOK_VINTAGE,
                                     TBT_TAX_CLASS.END_EFF_BOOK_VINTAGE,
                                     TBT_TAX_CLASS.ACTIVE
                                from (select ROWNUM TBT_GROUP_ID, INSIDE.*
                                        from (select distinct COMPANY_ID,
                                                              BUS_SEGMENT_ID,
                                                              UTILITY_ACCOUNT_ID,
                                                              SUB_ACCOUNT_ID,
                                                              TAX_LOCATION_ID,
                                                              CLASS_CODE_ID,
                                                              CLASS_CODE_VALUE,
                                                              GL_ACCOUNT_ID,
                                                              TAX_DISTINCTION_ID
                                                from TAX_BOOK_TRANSLATE) INSIDE) TBT,
                                     (select distinct COMPANY_ID,
                                                      BUS_SEGMENT_ID,
                                                      UTILITY_ACCOUNT_ID,
                                                      SUB_ACCOUNT_ID,
                                                      TAX_LOCATION_ID,
                                                      CLASS_CODE_ID,
                                                      CLASS_CODE_VALUE,
                                                      GL_ACCOUNT_ID,
                                                      TAX_DISTINCTION_ID,
                                                      TAX_CLASS_ID,
                                                      START_EFF_BOOK_VINTAGE,
                                                      END_EFF_BOOK_VINTAGE,
                                                      ACTIVE
                                        from TAX_BOOK_TRANSLATE) TBT_TAX_CLASS
                               where TBT.COMPANY_ID = TBT_TAX_CLASS.COMPANY_ID
                                 and TBT.BUS_SEGMENT_ID = TBT_TAX_CLASS.BUS_SEGMENT_ID
                                 and TBT.UTILITY_ACCOUNT_ID = TBT_TAX_CLASS.UTILITY_ACCOUNT_ID
                                 and TBT.SUB_ACCOUNT_ID = TBT_TAX_CLASS.SUB_ACCOUNT_ID
                                 and TBT.TAX_LOCATION_ID = TBT_TAX_CLASS.TAX_LOCATION_ID
                                 and TBT.CLASS_CODE_ID = TBT_TAX_CLASS.CLASS_CODE_ID
                                 and TBT.CLASS_CODE_VALUE = TBT_TAX_CLASS.CLASS_CODE_VALUE
                                 and TBT.GL_ACCOUNT_ID = TBT_TAX_CLASS.GL_ACCOUNT_ID
                                 and TBT.TAX_DISTINCTION_ID = TBT_TAX_CLASS.TAX_DISTINCTION_ID) INSIDE
                       group by INSIDE.COMPANY_ID,
                                INSIDE.BUS_SEGMENT_ID,
                                INSIDE.UTILITY_ACCOUNT_ID,
                                INSIDE.SUB_ACCOUNT_ID,
                                INSIDE.TAX_LOCATION_ID,
                                INSIDE.CLASS_CODE_ID,
                                INSIDE.CLASS_CODE_VALUE,
                                INSIDE.GL_ACCOUNT_ID,
                                INSIDE.TAX_DISTINCTION_ID) TBT
               where TAX_BOOK_TRANSLATE_GROUP.TAX_CLASS_ID_AGG = TBT.TAX_CLASS_ID_AGG
                 and TAX_BOOK_TRANSLATE_GROUP.START_EFF_BOOK_VINTAGE_AGG =
                     TBT.START_EFF_BOOK_VINTAGE_AGG
                 and TAX_BOOK_TRANSLATE_GROUP.END_EFF_BOOK_VINTAGE_AGG =
                     TBT.END_EFF_BOOK_VINTAGE_AGG
                 and TAX_BOOK_TRANSLATE_GROUP.ACTIVE_AGG = TBT.ACTIVE_AGG);

      forall I in TBT_GRP_BACKFILL_STG_TBL.FIRST .. TBT_GRP_BACKFILL_STG_TBL.LAST
      -----only update if the tbt_group_ids have changed
         update TAX_BOOK_TRANSLATE
            set TBT_GROUP_ID = TBT_GRP_BACKFILL_STG_TBL(I).TBT_GROUP_ID
          where NVL(COMPANY_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).COMPANY_ID
            and NVL(BUS_SEGMENT_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).BUS_SEGMENT_ID
            and NVL(UTILITY_ACCOUNT_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).UTILITY_ACCOUNT_ID
            and NVL(SUB_ACCOUNT_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).SUB_ACCOUNT_ID
            and NVL(TAX_LOCATION_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).TAX_LOCATION_ID
            and NVL(CLASS_CODE_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).CLASS_CODE_ID
            and NVL(CLASS_CODE_VALUE, 'xxxxx') = TBT_GRP_BACKFILL_STG_TBL(I).CLASS_CODE_VALUE
            and NVL(GL_ACCOUNT_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).GL_ACCOUNT_ID
            and NVL(TAX_DISTINCTION_ID, 0) = TBT_GRP_BACKFILL_STG_TBL(I).TAX_DISTINCTION_ID
            and NVL(TBT_GROUP_ID, 0) <> NVL(TBT_GRP_BACKFILL_STG_TBL(I).TBT_GROUP_ID, 0);

   end P_BACKFILL_TBT_GROUP;

   --****************************************************************************************************
   --                FUNCTIONS
   --****************************************************************************************************

   -- function to refresh the rows in tax_interface_cos
   function F_REFRESH_INTERFACE_COS(A_CO_IDS           in CO_IDS,
                                    A_INTERFACE_OBJECT varchar2) return varchar2 is
      L_MSG    varchar2(2000);
      NUM_ROWS number;
   begin

      L_MSG := 'Clearing out tax_interface_cos for interface ' || LOWER(A_INTERFACE_OBJECT) ||
               '...';
      delete from TAX_INTERFACE_COS
       where UPPER(trim(INTERFACE_OBJECT)) = UPPER(trim(A_INTERFACE_OBJECT))
       and USER_ID = USER ;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows deleted.';

      NUM_ROWS := A_CO_IDS.COUNT;

      L_MSG := 'Inserting into tax_interface_cos for interface ' || LOWER(A_INTERFACE_OBJECT) ||
               '...';

      for I in 1 .. NUM_ROWS
      loop
         L_MSG := 'Insert on row:' || I || ', company:' || A_CO_IDS(I) || ', interface object:' ||
                  A_INTERFACE_OBJECT;
         insert into TAX_INTERFACE_COS
            (COMPANY_ID, INTERFACE_OBJECT, USER_ID)
         values
            (A_CO_IDS(I), A_INTERFACE_OBJECT, USER);
      end loop;

      return 'OK';
   exception
      when others then
         return 'ERROR on ' || L_MSG;
   end F_REFRESH_INTERFACE_COS;

   -- function to return the process id for retirements interface
   function F_GETPROCESS return number is
   begin
      return L_PROCESS_ID;
   end F_GETPROCESS;

   -- function to clear out previously processed retirements
   function F_REMOVE_PRIOR_TRANS(A_VERSION_ID       number,
                                 A_TAX_YEAR         number,
                                 A_INTERFACE_OBJECT varchar2) return varchar2 is
      L_MSG    varchar2(2000);
      NUM_ROWS number;
   begin

      L_MSG := 'Deleting from basis_amounts...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      delete from BASIS_AMOUNTS BA
       where TAX_YEAR = A_TAX_YEAR
         and TAX_ACTIVITY_CODE_ID in (2, 4)
         and exists (select 1
                from TAX_RECORD_CONTROL TRC, TAX_INTERFACE_COS TIC
               where TRC.VERSION_ID = A_VERSION_ID
                 and TRC.COMPANY_ID = TIC.COMPANY_ID
                 and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                 and TIC.USER_ID = USER
                 and TRC.TAX_RECORD_ID = BA.TAX_RECORD_ID);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows deleted.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Deleting from tax_ret_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      delete from TAX_RET_AUDIT_TRAIL_GRP
       where TAX_YEAR = A_TAX_YEAR
         and TAX_ACTIVITY_CODE_ID in (2, 4)
         and COMPANY_ID in
             (select COMPANY_ID from TAX_INTERFACE_COS where INTERFACE_OBJECT = A_INTERFACE_OBJECT AND USER_ID = USER );
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows deleted.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating tax_book_transactions_grp (setting ret status to -99 where applicable)';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_BOOK_TRANSACTIONS_GRP
         set RETIREMENT_STATUS = -99, FOUND_IND = 0, AMOUNT_UNPROCESSED = AMOUNT
       where TAX_YEAR = A_TAX_YEAR
         and exists (select 1
                from TAX_INTERFACE_COS TIC
               where TAX_BOOK_TRANSACTIONS_GRP.COMPANY_ID = TIC.COMPANY_ID
                 and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                 and TIC.USER_ID = USER);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating tax_book_transactions_grp (setting ret status to 0 where applicable)';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_BOOK_TRANSACTIONS_GRP
         set RETIREMENT_STATUS = 0
       where AMOUNT = 0
         and TAX_YEAR = A_TAX_YEAR
         and exists (select 1
                from TAX_INTERFACE_COS TIC
               where TAX_BOOK_TRANSACTIONS_GRP.COMPANY_ID = TIC.COMPANY_ID
                 and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                 and TIC.USER_ID = USER);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_REMOVE_PRIOR_TRANS;

   -- function to process a batch of data (each iteration could run many batches)
   function F_RUN_BATCH(A_TAX_YEAR         number,
                        A_START_MO         number,
                        A_END_MO           number,
                        A_VERSION_ID       number,
                        A_ITERATION        number,
                        A_BATCH            number,
                        A_INTERFACE_OBJECT varchar2) return varchar2 is
      L_MSG        varchar2(2000);
      NUM_ROWS     number;
      ANALYZE_CODE number(22, 0);
      type BA_REC_TYPE is record(
         TAX_RECORD_ID        BASIS_AMOUNTS.TAX_RECORD_ID%type,
         TAX_YEAR             BASIS_AMOUNTS.TAX_YEAR%type,
         TAX_ACTIVITY_CODE_ID BASIS_AMOUNTS.TAX_ACTIVITY_CODE_ID%type,
         AMOUNT               BASIS_AMOUNTS.AMOUNT%type);
      type BA_TBL_TYPE is table of BA_REC_TYPE;
      BA_TBL BA_TBL_TYPE;

   begin

      L_MSG := 'Regathering stats for tax_book_transactions_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('tax_book_transactions_grp');

      L_MSG := 'Regathering stats for basis_amounts...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ANALYZE_CODE := ANALYZE_TABLE('basis_amounts');

      L_MSG := 'Inserting into tax_ret_audit_trail_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into TAX_RET_AUDIT_TRAIL_GRP
         (COMPANY_ID, TAX_ACTIVITY_CODE_ID, IN_SERVICE_YEAR, TAX_YEAR, START_MONTH, END_MONTH,
          TRANS_GRP_ID, START_EFF_BOOK_VINTAGE, END_EFF_BOOK_VINTAGE, BOOK_BALANCE,
          HOLD_SUM_BOOK_BALANCE, FINAL_AMOUNT, TRANS_AMOUNT, VINTAGE_YEAR, TAX_RECORD_ID,
          RETIREMENT_STATUS, TRID_VINTAGE_YEAR, BATCH, ITERATION, TBT_GROUP_ID)
         select COMPANY_ID,
                TAX_ACTIVITY_CODE_ID,
                IN_SERVICE_YEAR,
                TAX_YEAR,
                START_MONTH,
                END_MONTH,
                TRANS_GRP_ID,
                START_EFF_BOOK_VINTAGE,
                END_EFF_BOOK_VINTAGE,
                BOOK_BALANCE,
                BALANCE,
                NEW_RETIREMENT,
                TRANS_AMOUNT,
                BOOK_VINTAGE_YEAR,
                TAX_RECORD_ID,
                RETIREMENT_STATUS,
                VINTAGE_YEAR,
                BATCH,
                ITERATION,
                TBT_GROUP_ID
           from (select COMPANY_ID,
                        TAX_ACTIVITY_CODE_ID,
                        IN_SERVICE_YEAR,
                        TAX_YEAR,
                        START_MONTH,
                        END_MONTH,
                        TRANS_GRP_ID,
                        START_EFF_BOOK_VINTAGE,
                        END_EFF_BOOK_VINTAGE,
                        BOOK_BALANCE,
                        BALANCE,
                        DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               NEW_RETIREMENT,
                               DECODE(SIGN(BALANCE - NEW_RETIREMENT),
                                      -1,
                                      BALANCE,
                                      NEW_RETIREMENT)) NEW_RETIREMENT,
                        TRANS_AMOUNT,
                        BOOK_VINTAGE_YEAR,
                        TAX_RECORD_ID,
                        DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               ITERATION,
                               DECODE(SIGN(BALANCE - NEW_RETIREMENT),
                                      -1,
                                      -98,
                                      ITERATION))  RETIREMENT_STATUS,
                        VINTAGE_YEAR,
                        (A_BATCH) BATCH,
                        ITERATION,
                        TBT_GROUP_ID,
                        ALLOC_SUBTOTAL,
                        FIFO,
                        max(TRID_RANK) OVER(partition by TRANS_GRP_ID) TRANS_RANK
                   from (select distinct TBT.COMPANY_ID COMPANY_ID,
                                         TBT.TAX_ACTIVITY_CODE_ID TAX_ACTIVITY_CODE_ID,
                                         TBT.IN_SERVICE_YEAR IN_SERVICE_YEAR,
                                         TBT.TAX_YEAR,
                                         (A_START_MO) START_MONTH,
                                         (A_END_MO) END_MONTH,
                                         TBT.TRANS_GRP_ID TRANS_GRP_ID,
                                         TBX.START_EFF_BOOK_VINTAGE,
                                         TBX.END_EFF_BOOK_VINTAGE,
                                         TD.BOOK_BALANCE,
                                         (NVL(TD.BOOK_BALANCE, 0) + NVL(BA.AMOUNT, 0) + NVL(TDT.BOOK_BALANCE, 0)) BALANCE,
                                         DECODE(( sum( NVL(TD.BOOK_BALANCE, 0) + NVL(BA.AMOUNT, 0) + NVL(TDT.BOOK_BALANCE, 0) )
                                                    OVER(partition by TRANS_GRP_ID) ) ,
                                                  0,
                                                  0,
                                                  DECODE(TRR.FIFO,
                                                       0,
                                                       decode(
                                                          sign(TBT.AMOUNT),
                                                          -1,
                                                          DECODE(DENSE_RANK()
                                                                 OVER(partition by TBT.TRANS_GRP_ID
                                                                    order by
                                                                     decode(SIGN(TD.BOOK_BALANCE),-1,1,2) ASC,
                                                                     abs(TD.BOOK_BALANCE) desc,
                                                                     TRC.TAX_RECORD_ID),
                                                                 1,
                                                                 1,
                                                                 0) * TBT.AMOUNT_UNPROCESSED,
                                                          ( NVL(TD.BOOK_BALANCE, 0) + NVL(BA.AMOUNT, 0) + NVL(TDT.BOOK_BALANCE, 0) ) /
                                                                ( sum( NVL(TD.BOOK_BALANCE, 0) + NVL(BA.AMOUNT, 0) + NVL(TDT.BOOK_BALANCE, 0) )
                                                                   OVER(partition by TRANS_GRP_ID) ) * TBT.AMOUNT_UNPROCESSED
                                                        ) ,
                                                        DECODE(DENSE_RANK()
                                                               OVER(partition by TBT.TRANS_GRP_ID
                                                                    order by
                                                                    V.YEAR,
                                                                    ABS(TD.BOOK_BALANCE) desc,
                                                                    TRC.TAX_RECORD_ID),
                                                               1,
                                                               1,
                                                               0) * TBT.AMOUNT_UNPROCESSED)) NEW_RETIREMENT,
                                         TBT.AMOUNT TRANS_AMOUNT,
                                         TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY')) BOOK_VINTAGE_YEAR,
                                         TRC.TAX_RECORD_ID TAX_RECORD_ID,
                                         A_ITERATION ITERATION,
                                         TBT.TBT_GROUP_ID,
                                         V.YEAR VINTAGE_YEAR,
                                         TBT.AMOUNT_UNPROCESSED,
                                         sum(NVL(TD.BOOK_BALANCE, 0) + NVL(BA.AMOUNT, 0) +
                                             NVL(TDT.BOOK_BALANCE, 0)) OVER(partition by TRANS_GRP_ID) ALLOC_SUBTOTAL,
                                         TRR.FIFO,
                                         RANK() OVER(partition by TRC.TAX_RECORD_ID order by DECODE(SIGN(TBT.AMOUNT),-1,1,2) ASC, TBT.AMOUNT desc) TRID_RANK
                           from TAX_BOOK_TRANSACTIONS_GRP TBT,
                                TAX_BOOK_TRANSLATE_GROUP_MAP TBX,
                                COMPANY C,
                                TAX_INTERFACE_COS TIC,
                                (select COMPANY_ID,
                                        ITERATION,
                                        VINTAGE_FORWARD,
                                        VINTAGE_BACK,
                                        SORT_ORDER,
                                        GFORMAT_STRING,
                                        FFORMAT_STRING,
                                        FIFO,
                                        MIN_VINTAGE
                                   from TAX_RETIRE_RULES TRR
                                 union
                                 select distinct COMPANY_ID,
                                                 1 ITERATION,
                                                 0 VINTAGE_FORWARD,
                                                 0 VINTAGE_BACK,
                                                 'book_balance D' SORT_ORDER,
                                                 null GFORMAT_STRING,
                                                 null FFORMAT_STRING,
                                                 0 FIFO,
                                                 1969 MIN_VINTAGE
                                   from (select distinct COMPANY_ID
                                           from TAX_BOOK_TRANSACTIONS
                                         minus
                                         select COMPANY_ID
                                           from TAX_RETIRE_RULES)) TRR,
                                TAX_DEPRECIATION TD,
                                TAX_CONTROL TC,
                                TAX_RECORD_CONTROL TRC,
                                VINTAGE V,
                                (select BA2.TAX_RECORD_ID,
                                        BA2.TAX_YEAR,
                                        sum(NVL(BA2.AMOUNT, 0) * NVL(TAC2.SIGN, 1)) AMOUNT
                                   from BASIS_AMOUNTS      BA2,
                                        TAX_ACTIVITY_CODE  TAC2,
                                        TAX_RECORD_CONTROL TRC2
                                  where BA2.TAX_ACTIVITY_CODE_ID = TAC2.TAX_ACTIVITY_CODE_ID(+)
                                    and BA2.TAX_ACTIVITY_CODE_ID <> 12
                                    and BA2.TAX_RECORD_ID = TRC2.TAX_RECORD_ID
                                    and TRC2.VERSION_ID = A_VERSION_ID
                                    and BA2.TAX_YEAR = A_TAX_YEAR
                                  group by BA2.TAX_RECORD_ID, BA2.TAX_YEAR) BA,
                                (select TDT2.TAX_RECORD_ID,
                                        TDT2.TAX_YEAR,
                                        TDT2.TAX_BOOK_ID,
                                        sum(NVL(TDT2.BOOK_BALANCE, 0)) BOOK_BALANCE
                                   from TAX_DEPRECIATION_TRANSFER TDT2, TAX_RECORD_CONTROL TRC2
                                  where TDT2.TAX_RECORD_ID = TRC2.TAX_RECORD_ID
                                    and TRC2.VERSION_ID = A_VERSION_ID
                                    and TDT2.TAX_YEAR = A_TAX_YEAR
                                  group by TDT2.TAX_RECORD_ID, TDT2.TAX_YEAR, TDT2.TAX_BOOK_ID) TDT
                          where TBT.TBT_GROUP_ID = TBX.TBT_GROUP_ID(+)
                            and TBT.TAX_YEAR = A_TAX_YEAR
                            and TBT.COMPANY_ID = C.COMPANY_ID
                            and TBT.COMPANY_ID = TRR.COMPANY_ID
                            and TBT.COMPANY_ID = TIC.COMPANY_ID
                            and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                            and TIC.USER_ID = USER
                            and TBT.AMOUNT <> 0
                            and TBT.TAX_ACTIVITY_CODE_ID in (2, 4)
                            and TBT.RETIREMENT_STATUS in (-99, -98)
                            and TRR.ITERATION = A_ITERATION
                            and TD.TAX_RECORD_ID = TRC.TAX_RECORD_ID
                            and TC.TAX_RECORD_ID = TRC.TAX_RECORD_ID
                            and TD.TAX_BOOK_ID = TC.TAX_BOOK_ID
                            and TRC.COMPANY_ID = C.COMPANY_ID
                            and TRC.VINTAGE_ID = V.VINTAGE_ID
                            and TD.TAX_RECORD_ID = BA.TAX_RECORD_ID(+)
                            and TD.TAX_YEAR = BA.TAX_YEAR(+)
                            and TD.TAX_RECORD_ID = TDT.TAX_RECORD_ID(+)
                            and TD.TAX_BOOK_ID = TDT.TAX_BOOK_ID(+)
                            and TD.TAX_YEAR = TDT.TAX_YEAR(+)
                            and TBT.COMPANY_ID = TRC.COMPANY_ID
                            and TBX.TAX_CLASS_ID = TRC.TAX_CLASS_ID
                            and TBT.TAX_YEAR = TD.TAX_YEAR
                            and V.YEAR <= DECODE(SIGN(TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY')) -
                                                      MIN_VINTAGE),
                                                 -1,
                                                 MIN_VINTAGE,
                                                 TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY'))) +
                                TRR.VINTAGE_FORWARD
                            and V.YEAR >= DECODE(SIGN(TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY')) -
                                                      MIN_VINTAGE),
                                                 -1,
                                                 MIN_VINTAGE,
                                                 TO_NUMBER(TO_CHAR(TBT.IN_SERVICE_YEAR, 'YYYY'))) -
                                TRR.VINTAGE_FORWARD
                            and 1 = DECODE(SIGN(TBT.AMOUNT),
                                           -1,
                                           1,
                                           SIGN(NVL(TD.BOOK_BALANCE, 0) + NVL(BA.AMOUNT, 0) +
                                                NVL(TDT.BOOK_BALANCE, 0)))
                            and TD.TAX_BOOK_ID = 10
                            and TRC.VERSION_ID = A_VERSION_ID)
                  where DECODE(SIGN(TRANS_AMOUNT),
                               -1,
                               NEW_RETIREMENT,
                                  DECODE(SIGN(BALANCE - NEW_RETIREMENT),
                                      -1,
                                      BALANCE,
                                      NEW_RETIREMENT)) <> 0)
          where TRANS_RANK = 1;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows inserted into tax_ret_audit_trail_grp.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      if NUM_ROWS = 0 then
         L_MSG := 'Exiting function on 0 rows inserts';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return 'ZERO ROWS';
      end if;

      L_MSG := 'Updating tax_book_transactions_grp...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      update TAX_BOOK_TRANSACTIONS_GRP TBT
         set RETIREMENT_STATUS = NVL((select max(RETIREMENT_STATUS)
                                        from TAX_RET_AUDIT_TRAIL_GRP TRAT, TAX_INTERFACE_COS TIC
                                       where TRAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                                         and TRAT.COMPANY_ID = TBT.COMPANY_ID
                                         and TRAT.TAX_YEAR = TBT.TAX_YEAR
                                         and TBT.TAX_YEAR = A_TAX_YEAR
                                         and TBT.COMPANY_ID = TIC.COMPANY_ID
                                         and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                                         and TIC.USER_ID = USER),
                                      -99),
             AMOUNT_UNPROCESSED = AMOUNT_UNPROCESSED -
                                   NVL((select sum(FINAL_AMOUNT)
                                         from TAX_RET_AUDIT_TRAIL_GRP TRAT, TAX_INTERFACE_COS TIC
                                        where TRAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                                          and TRAT.COMPANY_ID = TBT.COMPANY_ID
                                          and TRAT.TAX_YEAR = TBT.TAX_YEAR
                                          and TRAT.ITERATION = A_ITERATION
                                          and TRAT.BATCH = A_BATCH
                                          and TBT.TAX_YEAR = A_TAX_YEAR
                                          and TRAT.COMPANY_ID = TIC.COMPANY_ID
                                          and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                                          and TIC.USER_ID = USER),
                                       0)
       where exists (select 1
                from TAX_INTERFACE_COS TIC
               where TBT.COMPANY_ID = TIC.COMPANY_ID
                 and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                 and TIC.USER_ID = USER)
         and TAX_YEAR = A_TAX_YEAR
         and RETIREMENT_STATUS in (-99, -98)
         and exists (select 1
                from TAX_RET_AUDIT_TRAIL_GRP TRAT, TAX_INTERFACE_COS TIC
               where TRAT.TRANS_GRP_ID = TBT.TRANS_GRP_ID
                 and TRAT.COMPANY_ID = TBT.COMPANY_ID
                 and TRAT.TAX_YEAR = TBT.TAX_YEAR
                 and TRAT.ITERATION = A_ITERATION
                 and TBT.TAX_YEAR = A_TAX_YEAR
                 and TRAT.COMPANY_ID = TIC.COMPANY_ID
                 and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
                 and TIC.USER_ID = USER);
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows updated on tax_book_transactions_grp.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Updating basis_amounts...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TRAT.TAX_RECORD_ID,
             TRAT.TAX_YEAR,
             TRAT.TAX_ACTIVITY_CODE_ID,
             sum(TRAT.FINAL_AMOUNT) bulk collect
        into BA_TBL
        from TAX_RET_AUDIT_TRAIL_GRP TRAT, TAX_INTERFACE_COS TIC
       where TRAT.COMPANY_ID = TIC.COMPANY_ID
         and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
         and TIC.USER_ID = USER
         and TRAT.TAX_YEAR = A_TAX_YEAR
         and TRAT.ITERATION = A_ITERATION
         and TRAT.BATCH = A_BATCH
       group by TRAT.TAX_RECORD_ID, TRAT.TAX_YEAR, TRAT.TAX_ACTIVITY_CODE_ID;

      forall I in BA_TBL.FIRST .. BA_TBL.LAST
         update BASIS_AMOUNTS
            set AMOUNT = AMOUNT + BA_TBL(I).AMOUNT
          where TAX_RECORD_ID = BA_TBL(I).TAX_RECORD_ID
            and TAX_YEAR = BA_TBL(I).TAX_YEAR
            and TAX_ACTIVITY_CODE_ID = BA_TBL(I).TAX_ACTIVITY_CODE_ID
            and TAX_YEAR = A_TAX_YEAR;
      NUM_ROWS := BA_TBL.COUNT;
      L_MSG    := NUM_ROWS || ' rows updated on basis_amounts.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      L_MSG := 'Inserting into basis_amounts...';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into BASIS_AMOUNTS BA
         (TAX_RECORD_ID, TAX_ACTIVITY_CODE_ID, TAX_YEAR, TAX_INCLUDE_ID, AMOUNT)
         select TRAT.TAX_RECORD_ID,
                TRAT.TAX_ACTIVITY_CODE_ID,
                A_TAX_YEAR,
                1 TAX_INCLUDE_ID,
                sum(TRAT.FINAL_AMOUNT) AMOUNT
           from TAX_RET_AUDIT_TRAIL_GRP TRAT, TAX_INTERFACE_COS TIC
          where TRAT.TAX_YEAR = A_TAX_YEAR
            and TRAT.COMPANY_ID = TIC.COMPANY_ID
            and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
            and TIC.USER_ID = USER
            and TRAT.ITERATION = A_ITERATION
            and TRAT.BATCH = A_BATCH
            and not exists (select 1
                   from BASIS_AMOUNTS BA
                  where BA.TAX_RECORD_ID = TRAT.TAX_RECORD_ID
                    and BA.TAX_YEAR = TRAT.TAX_YEAR
                    and BA.TAX_ACTIVITY_CODE_ID = TRAT.TAX_ACTIVITY_CODE_ID
                    and BA.TAX_YEAR = A_TAX_YEAR)
          group by TRAT.TAX_RECORD_ID, TRAT.TAX_ACTIVITY_CODE_ID, A_TAX_YEAR;
      NUM_ROWS := sql%rowcount;
      L_MSG    := NUM_ROWS || ' rows inserted into basis_amounts.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_RUN_BATCH;

   -- function to process the retirements
   function F_RUN_ITERATION(A_TAX_YEAR         number,
                            A_START_MO         number,
                            A_END_MO           number,
                            A_VERSION_ID       number,
                            A_INTERFACE_OBJECT varchar2) return varchar2 is
      L_MSG     varchar2(2000);
      NUM_ROWS  number;
      MAX_ITER  number;
      BATCH_NO  number;
      ZERO_ROWS boolean;
   begin

      L_MSG := 'Retrieving max number of iterations to run.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

      select max(NVL(ITERATION, 0))
        into MAX_ITER
        from TAX_RETIRE_RULES TRR, TAX_INTERFACE_COS TIC
       where TRR.COMPANY_ID = TIC.COMPANY_ID
         and TIC.INTERFACE_OBJECT = A_INTERFACE_OBJECT
         and TIC.USER_ID = USER;

      if MAX_ITER < 1 or MAX_ITER is null then
         L_MSG := 'Error on selecting the max iteration from tax retire rules for applicable companies';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
      end if;

      for I in 1 .. MAX_ITER
      loop
         L_MSG := 'Entering iteration loop ' || I || ' of ' || MAX_ITER;
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);

         BATCH_NO  := 1;
         ZERO_ROWS := false;

         while ZERO_ROWS = false
         loop

            if BATCH_NO > 100000 then
               L_MSG := 'Infinite Loop Error Encountered! Exiting!';
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
            end if;

            L_MSG := ' ';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := 'Entering batch function. Iteration ' || I || ', Batch ' || BATCH_NO || '.';
            PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
            L_MSG := F_RUN_BATCH(A_TAX_YEAR,
                                 A_START_MO,
                                 A_END_MO,
                                 A_VERSION_ID,
                                 I,
                                 BATCH_NO,
                                 A_INTERFACE_OBJECT);
            if L_MSG = 'ZERO ROWS' then
               ZERO_ROWS := true;
            elsif L_MSG <> 'OK' then
               PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
               return L_MSG;
            end if;

            BATCH_NO := BATCH_NO + 1;

         end loop;
      end loop;

      return 'OK';

   exception
      when others then
         return L_MSG;
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         return L_MSG;
   end F_RUN_ITERATION;

   -- function to start the log, clear out the retirements, process the retirements, end the log
   function F_POWERTAX_RETS(A_TAX_YEAR         number,
                            A_START_MO         number,
                            A_END_MO           number,
                            A_VERSION_ID       number,
                            A_INTERFACE_OBJECT varchar2) return varchar2 is
      L_MSG          varchar2(2000);
      NUM_ROWS       number;
      MAX_ITER       number;
      BATCH_NO       number;
      ZERO_ROWS      boolean;
      CO_PARSE_VAR   number;
      CO_ID_COUNT    number;
      I              number;
      CUR_CO_ID      number;
      CO_PARSE_COUNT number;
      CO_IDS_STR     varchar2(2000);

      cursor CO_IDS is
         select COMPANY_ID
           from TAX_INTERFACE_COS
          where INTERFACE_OBJECT = A_INTERFACE_OBJECT
          and USER_ID = USER
          order by COMPANY_ID;

   begin

      if L_PROCESS_ID = -1 or L_PROCESS_ID is null then
         P_SETPROCESS();
      end if;

      PKG_PP_LOG.P_START_LOG(L_PROCESS_ID);

      PKG_PP_LOG.P_WRITE_MESSAGE('Starting...');
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing with the following variable definitions:');
      PKG_PP_LOG.P_WRITE_MESSAGE('Tax Year : ' || A_TAX_YEAR);
      PKG_PP_LOG.P_WRITE_MESSAGE('Start Mo : ' || A_START_MO);
      PKG_PP_LOG.P_WRITE_MESSAGE('End Mo : ' || A_END_MO);
      PKG_PP_LOG.P_WRITE_MESSAGE('Interface Object : ' || A_INTERFACE_OBJECT);

      -- routine to log the company ids being used.  Hard coding the logs to do 10
      -- per line of the logs.
      CO_PARSE_VAR   := 10;
      CO_PARSE_COUNT := 1;
      I              := 1;
      open CO_IDS;
      loop
         fetch CO_IDS
            into CUR_CO_ID;
         exit when CO_IDS %notfound;

         if CO_PARSE_COUNT <> 1 then
            CO_IDS_STR := CO_IDS_STR || ',' || TO_CHAR(CUR_CO_ID);
         else
            CO_IDS_STR := TO_CHAR(CUR_CO_ID);
         end if;
         if I = 1 then
            CO_IDS_STR := 'Company IDs : ' || CO_IDS_STR;
         end if;

         if CO_PARSE_COUNT = CO_PARSE_VAR then
            PKG_PP_LOG.P_WRITE_MESSAGE(CO_IDS_STR);
            CO_PARSE_COUNT := 0;
         end if;
         CO_PARSE_COUNT := CO_PARSE_COUNT + 1;
         I              := I + 1;
      end loop;
      close CO_IDS;
      if CO_PARSE_COUNT <> CO_PARSE_VAR + 1 then
         ----after looping through and logging sets of company ids, there may be
         ----an orphaned set of company ids that needs to be logged (the last chunck).
         PKG_PP_LOG.P_WRITE_MESSAGE(CO_IDS_STR);
      end if;

      PKG_PP_LOG.P_WRITE_MESSAGE('Version ID : ' || A_VERSION_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE('Entering resetting function...');
      L_MSG := F_REMOVE_PRIOR_TRANS(A_VERSION_ID, A_TAX_YEAR, A_INTERFACE_OBJECT);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);
      PKG_PP_LOG.P_WRITE_MESSAGE('Entering processing function...');
      L_MSG := F_RUN_ITERATION(A_TAX_YEAR, A_START_MO, A_END_MO, A_VERSION_ID, A_INTERFACE_OBJECT);
      if L_MSG <> 'OK' then
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
      end if;
      PKG_PP_LOG.P_WRITE_MESSAGE('Function Result : ' || L_MSG);
      PKG_PP_LOG.P_WRITE_MESSAGE('Finished...');
      PKG_PP_LOG.P_END_LOG();
      return 'OK';

   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
   end F_POWERTAX_RETS;

end PKG_TAX_INT_RET;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (787, 0, 10, 4, 2, 0, 33924, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033924_pwrtax_PKG_TAX_INT_RET.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;