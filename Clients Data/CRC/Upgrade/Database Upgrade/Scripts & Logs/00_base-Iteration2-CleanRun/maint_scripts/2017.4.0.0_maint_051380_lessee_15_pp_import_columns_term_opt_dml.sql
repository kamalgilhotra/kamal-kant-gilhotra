/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051380_lessee_15_pp_import_columns_term_opt_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/31/2018  Alex Healey    Create new import columns and template setup for Termination options
||============================================================================
*/


insert into pp_import_type
(import_type_id,
description,
long_description,
import_table_name,
archive_table_name,
allow_updates_on_add,
delegate_object_name)
values
(269,
 'Add: ILR Termination Options',
 'Lessee ILR Termination Options',
 'ls_import_ilr_term_opt',
 'ls_import_ilr_term_opt_arc',
 1,
 'nvo_ls_logic_import'
);


insert into pp_import_type_subsystem
(import_type_id,
 import_subsystem_id
)
values
(269,
 8
 );
 
 INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                         ,
                'ilr_id'                    ,
                SYSDATE                     ,
                USER                        ,
                'ILR ID'                    ,
                'ilr_id_xlate'              ,
                1                           ,
                1                           ,
                'number(22,0)'              ,
                'ls_ilr',
                ' '                          ,
                1                           ,
                NULL                        ,
                ' '                         ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269           ,
                'revision'    ,
                SYSDATE       ,
                USER          ,
                'Revision'    ,
                NULL           ,
                0             ,
                1             ,
                'number(22,0)',
                NULL           ,
                ' '            ,
                1             ,
                NULL          ,
                ' '           ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                     ,
                'ilr_term_option_id',
                SYSDATE                 ,
                USER                    ,
                'Termination Option ID'       ,
                NULL                     ,
                0                       ,
                1                       ,
                'number(22,0)'          ,
                NULL                     ,
                ' '                      ,
                1                       ,
                NULL                    ,
                ' '                     ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                          ,
                'ilr_term_probability_id',
                SYSDATE                      ,
                USER                         ,
                'Probability of Termination'    ,
                'ilr_term_prob_id_xlate' ,
                1                            ,
                1                            ,
                'number(22,0)'               ,
                'ls_ilr_renewal_probability' ,
                ' '                           ,
                1                            ,
                NULL                         ,
                ' '                          ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                          ,
                'decision_notice'            ,
                SYSDATE                      ,
                USER                         ,
                'Decision Notice (in Months)',
                NULL                          ,
                0                            ,
                1                            ,
                'number(22,0)'               ,
                NULL                           ,
                ' '                           ,
                1                            ,
                NULL                         ,
                ' '                          ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                    ,
                'decision_date'        ,
                SYSDATE                ,
                USER                   ,
                'Actual Date to Decide',
                NULL                     ,
                0                      ,
                1                      ,
                'date mmddyyyy format' ,
                NULL                    ,
                ' '                     ,
                1                      ,
                NULL                   ,
                ' '                    ,
                ' '
        );

INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                   ,
                'termination_date'       ,
                SYSDATE               ,
                USER                  ,
                'Date Termination Occurs',
                NULL                  ,
                0                     ,
                1                     ,
                'date mmddyyyy format',
                NULL                   ,
                ' '                    ,
                1                     ,
                NULL                  ,
                ' '                   ,
                ' '
        );


INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                 ,
                'termination_amt'      ,
                SYSDATE             ,
                USER                ,
                'Amount to Terminate',
                NULL                  ,
                1                   ,
                1                   ,
                'number(22,2)'      ,
                NULL                  ,
                ' '                  ,
                1                   ,
                NULL                ,
                ' '                 ,
                '0'
        );
INSERT INTO
        pp_import_column
        (
                import_type_id           ,
                column_name              ,
                time_stamp               ,
                user_id                  ,
                description              ,
                import_column_name       ,
                is_required              ,
                processing_order         ,
                column_type              ,
                parent_table             ,
                parent_table_pk_column2  ,
                is_on_table              ,
                autocreate_import_type_id,
                parent_table_pk_column   ,
                default_value
        )
        VALUES
        (
                269                 ,
                'is_modified'      ,
                SYSDATE             ,
                USER                ,
                'Is modified',
                NULL                  ,
                0                   ,
                1                   ,
                'number(22,0)'      ,
                NULL                 ,
                ' '                  ,
                1                   ,
                NULL                ,
                ' '                 ,
                '0'
        );

		
--Now that we've inserted the new import columns, we can now add lookups for the common ones
INSERT INTO
        pp_import_column_lookup
        (
                import_type_id  ,
                column_name     ,
                import_lookup_id,
                time_stamp      ,
                user_id
        )
        VALUES
        (
                269                   ,
                'ilr_id',
                1096                  ,
                SYSDATE               ,
                USER
        );

INSERT INTO
        pp_import_column_lookup
        (
                import_type_id  ,
                column_name     ,
                import_lookup_id,
                time_stamp      ,
                user_id
        )
        VALUES
        (
                269                          ,
                'ilr_term_probability_id',
                1086                         ,
                SYSDATE                      ,
                USER
        );


--add the new template for termination options

INSERT INTO
        pp_import_template
        (
                import_template_id,
                import_type_id    ,
                description       ,
                long_description  ,
                created_by        ,
                created_date      ,
                do_update_with_add,
                is_autocreate_template
        )
        VALUES
        (
                pp_import_template_seq.nextval                                               ,
                269                                               ,
                'ILR Terminate Options Add'                        ,
                'Default template for adding ILR Termination options',
                'PWRPLANT'                                        ,
                SYSDATE                                           ,
                1                                                 ,
                0
        );
		
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 269 and lower(description) = 'ilr terminate options add')      ,
                1       ,
                269     ,
                'ilr_id',
                1096
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 269 and lower(description) = 'ilr terminate options add')        ,
                2         ,
                269       ,
                'revision',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 269 and lower(description) = 'ilr terminate options add')                           ,
                3                            ,
                269                          ,
                'ilr_term_probability_id',
                1086
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 269 and lower(description) = 'ilr terminate options add')            ,
                4             ,
                269           ,
                'termination_amt',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 269 and lower(description) = 'ilr terminate options add')             ,
                5            ,
                269            ,
                'decision_date',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 269 and lower(description) = 'ilr terminate options add')             ,
                6              ,
                269            ,
                'termination_date',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 269 and lower(description) = 'ilr terminate options add')               ,
               7               ,
                269              ,
                'decision_notice',
                NULL
        );

--Corrections to the purchase option import template/columns, 
--since import uses the next sequence value to determine the option id
 		
delete from pp_import_template_fields
where import_type_id = 268
and lower(column_name) = 'ilr_purchase_option_id';


update pp_import_template_fields
set field_id = 6
where import_type_id = 268
and field_id = 7;


update pp_import_template_fields
set field_id = 7
where import_type_id = 268
and field_id = 8;


update pp_import_template_fields
set field_id = 8
where import_type_id = 268
and field_id = 9;


update pp_import_column
set is_required = 0
where import_type_id = 268
and lower(column_name) = 'is_modified';

update pp_import_column
set import_column_name = NULL
where import_type_id = 268
and lower(column_name) not in('ilr_id', 'ilr_purchase_probability_id', 'purchase_option_type' );


update pp_import_column
set parent_table = NULL
where import_type_id = 268
and lower(column_name) not in('ilr_id', 'ilr_purchase_probability_id', 'purchase_option_type' );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6283, 0, 2017, 4, 0, 0, 51380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051380_lessee_15_pp_import_columns_term_opt_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
		