/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045368_lease_create_fasb_cap_type_sob_map_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2016.1.0  02/03/2016  Anand R        Create new table for FASB cap type - set of books mapping
|| 2015.2.2.0 04/14/2016 David Haupt		 Backporting to 2015.2.2
||============================================================================
*/

ALTER TABLE ls_lease_cap_type
    DROP COLUMN fasb_cap_type_id;

CREATE TABLE ls_fasb_cap_type_sob_map  (
  set_of_books_id      NUMBER(22,0)  NOT NULL,
  lease_cap_type_id    NUMBER(22,0)  NOT NULL,
  fasb_cap_type_id     NUMBER(22,0) NOT NULL,
  user_id              VARCHAR2(18)  NULL,
  time_stamp           DATE          NULL
);

ALTER TABLE ls_fasb_cap_type_sob_map
  ADD CONSTRAINT pk_ls_fasb_cap_type_sob_map PRIMARY KEY (
    set_of_books_id,
    lease_cap_type_id,
    fasb_cap_type_id
  )
  using index tablespace PWRPLANT_IDX ;

ALTER TABLE ls_fasb_cap_type_sob_map
  ADD CONSTRAINT fk_ls_fasb_cap_sob_map_sob 
  FOREIGN KEY ( set_of_books_id  )
  REFERENCES set_of_books (set_of_books_id) ;

ALTER TABLE ls_fasb_cap_type_sob_map
  ADD CONSTRAINT fk_ls_fasb_cap_sob_map_lcp 
  FOREIGN KEY ( lease_cap_type_id  )
  REFERENCES ls_lease_cap_type (ls_lease_cap_type_id) ;
  
ALTER TABLE ls_fasb_cap_type_sob_map
  ADD CONSTRAINT fk_ls_fasb_cap_sob_map_fasbcp 
  FOREIGN KEY ( fasb_cap_type_id  )
  REFERENCES ls_fasb_cap_type (fasb_cap_type_id ) ;

comment on table ls_fasb_cap_type_sob_map is 'FASB cap type to set of books mapping table';
comment on column ls_fasb_cap_type_sob_map.set_of_books_id is 'System assigned identifier of Set of books';
comment on column ls_fasb_cap_type_sob_map.lease_cap_type_id is 'System assigned identifier of lease cap type';
comment on column ls_fasb_cap_type_sob_map.fasb_cap_type_id  is 'System assigned identifier of FASB cap type';
comment on column ls_fasb_cap_type_sob_map.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_fasb_cap_type_sob_map.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3042, 0, 2015, 2, 2, 0, 045368, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045368_lease_create_fasb_cap_type_sob_map_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;