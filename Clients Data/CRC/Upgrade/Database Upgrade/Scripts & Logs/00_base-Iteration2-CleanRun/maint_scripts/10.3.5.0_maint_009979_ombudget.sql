/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009979_ombudget.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   04/19/2012 Marc Zawko     Human Resources Functionality Won't Work Without It
||============================================================================
*/

alter table CR_BUDGET_LABOR_RESOURCE
   add FTE_RATE number(22,8);

alter table CR_BUDGET_LABOR_RESOURCE
   add LOST_TIME_DOLLARS number(22,2);

insert into CR_BUDGET_LABOR_RESOURCE_ORDER
   (COLUMN_NAME, COLUMN_ORDER, TIME_STAMP, USER_ID)
values
   ('fte_rate', 23, TO_DATE('10-04-2012 19:10:34', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT');

insert into CR_BUDGET_LABOR_RESOURCE_ORDER
   (COLUMN_NAME, COLUMN_ORDER, TIME_STAMP, USER_ID)
values
   ('lost_time_dollars', 24, TO_DATE('10-04-2012 19:10:34', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (146, 0, 10, 3, 5, 0, 9979, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009979_ombudget.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
