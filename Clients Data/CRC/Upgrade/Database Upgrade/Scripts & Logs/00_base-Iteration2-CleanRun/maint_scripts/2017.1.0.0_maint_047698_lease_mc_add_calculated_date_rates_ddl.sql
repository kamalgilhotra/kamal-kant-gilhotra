/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047698_lease_mc_add_calculated_date_rates_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.1.0.0 04/27/2017 Johnny Sisouphanh add table to store calculated date/rate
||============================================================================
*/

CREATE TABLE ls_lease_calculated_date_rates (
  company_id            NUMBER(22,0) NOT NULL,
  contract_currency_id  NUMBER(22,0) NOT NULL,
  company_currency_id   NUMBER(22,0) NOT NULL,  
  exchange_rate_type_id NUMBER(22,0) NOT NULL,  
  accounting_month      DATE         NOT NULL,
  exchange_date         DATE         NOT NULL,
  rate                  NUMBER(22,8) NOT NULL,
  user_id               VARCHAR2(18) NULL,
  time_stamp            DATE         NULL
  );

ALTER TABLE ls_lease_calculated_date_rates 
  ADD CONSTRAINT ls_lease_calc_date_rates_pk 
  PRIMARY KEY (company_id, contract_currency_id, accounting_month)
  USING INDEX TABLESPACE pwrplant_idx
;

ALTER TABLE ls_lease_calculated_date_rates
  ADD CONSTRAINT ls_lease_calc_date_rates_fk1 
  FOREIGN KEY (company_id, accounting_month) 
  REFERENCES LS_PROCESS_CONTROL (company_id, gl_posting_mo_yr)
;

ALTER TABLE ls_lease_calculated_date_rates
  ADD CONSTRAINT ls_lease_calc_date_rates_fk2
  FOREIGN KEY (exchange_date, contract_currency_id, company_currency_id, exchange_rate_type_id)
  REFERENCES currency_rate_default (exchange_date, currency_from, currency_to, exchange_rate_type_id)
;

comment on table  ls_lease_calculated_date_rates is '(S) [11] This table maintains the calculated date rates to be used in the translation in Lease Multicurrency.';
comment on column ls_lease_calculated_date_rates.company_id IS 'The identifier of the company.';
comment on column ls_lease_calculated_date_rates.contract_currency_id IS 'The contract currency used by this MLA and its underlying ILRs and Assets.';
comment on column ls_lease_calculated_date_rates.company_currency_id IS 'The company currency added solely for the purpose as part of FK to currency_rate_default table.';
comment on column ls_lease_calculated_date_rates.exchange_rate_type_id IS 'The exchange rate type id added solely for the purpose as part of FK to currency_rate_default table.';
comment on column ls_lease_calculated_date_rates.accounting_month IS 'The accounting month we are storing the calculated date/rate for.';
comment on column ls_lease_calculated_date_rates.exchange_date IS 'The date of the exchange rate.';
comment on column ls_lease_calculated_date_rates.rate IS 'The actual exchange rate.';
comment on column ls_lease_calculated_date_rates.user_id IS 'Standard system-assigned user id used for audit purposes.';
comment on column ls_lease_calculated_date_rates.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3466, 0, 2017, 1, 0, 0, 47698, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047698_lease_mc_add_calculated_date_rates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;