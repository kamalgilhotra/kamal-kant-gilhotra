/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052420_lessee_02_add_unaccrued_int_je_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.3  10/09/2018 Sarah Byers      Add new trans types for Purchase option gain/loss
||============================================================================
*/

insert into je_trans_type (trans_type, description)
select 3076, '3076 - Unaccrued Interest Debit'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3076);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3076
   and not exists (select 1 from je_method_trans_type where trans_type = 3076 and je_method_id = 1);

insert into je_trans_type (trans_type, description)
select 3077, '3077 - Unaccrued Interest Credit'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3077);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3077
   and not exists (select 1 from je_method_trans_type where trans_type = 3077 and je_method_id = 1);
   
insert into je_trans_type (trans_type, description)
select 3078, '3078 - Accrued Interest Debit'
  from dual
 where not exists (select 1 from je_trans_type where trans_type = 3078);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
 where trans_type = 3078
   and not exists (select 1 from je_method_trans_type where trans_type = 3078 and je_method_id = 1);

   

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(10223, 0, 2017, 4, 0, 3, 52420, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.3_maint_052420_lessee_02_add_unaccrued_int_je_trans_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;