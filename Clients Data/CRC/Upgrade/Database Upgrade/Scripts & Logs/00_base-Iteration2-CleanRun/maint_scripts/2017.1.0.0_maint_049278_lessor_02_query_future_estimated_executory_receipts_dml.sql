/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049278_lessor_02_query_future_estimated_executory_receipts_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/06/2017 Josh Sandler     Filter out $0 rows
||============================================================================
*/

UPDATE pp_any_query_criteria
SET SQL = 'WITH amounts AS (  SELECT ilr_id, revision, set_of_books_id, Nvl(time_period, ''Total'') time_period, ls_cur_type, currency, currency_symbol, executory_bucket, Sum(future_executory_receipts) future_executory_receipts  FROM (  SELECT  ilr_id,  revision,  set_of_books_id,  CASE   WHEN To_Char(MONTH,''yyyymm'') BETWEEN year_filter.filter_value AND SubStr(year_filter.filter_value,1,4) || 12 THEN year_filter.filter_value || '' - '' || SubStr(year_filter.filter_value,1,4) || ''12''   WHEN To_Char(MONTH,''yyyy'') >= SubStr(year_filter.filter_value,1,4) + 5 THEN ''Thereafter''   ELSE To_Char(MONTH,''yyyy'')  END time_period,  ls_cur_type,  iso_code AS currency,  currency_display_symbol AS currency_symbol,  buckets.bucket_name executory_bucket,  Sum(Decode(buckets.bucket_number,        1,executory_paid1,        2,executory_paid2,        3,executory_paid3,        4,executory_paid4,        5,executory_paid5,        6,executory_paid6,        7,executory_paid7,        8,executory_paid8,        9,executory_paid9,        10,executory_paid10,        0)) future_executory_receipts  FROM v_lsr_ilr_mc_schedule sch   CROSS JOIN pp_any_required_filter year_filter   CROSS JOIN lsr_receivable_bucket_admin buckets  WHERE Upper(year_filter.column_name) = ''AS OF MONTHNUM''  AND sch.MONTH >= To_Date(year_filter.filter_value,''yyyymm'')  AND buckets.receivable_type = ''Executory''  GROUP BY  ilr_id,  revision,  set_of_books_id,  CASE   WHEN To_Char(MONTH,''yyyymm'') BETWEEN year_filter.filter_value AND SubStr(year_filter.filter_value,1,4) || 12 THEN year_filter.filter_value || '' - '' || SubStr(year_filter.filter_value,1,4) || ''12''   WHEN To_Char(MONTH,''yyyy'') >= SubStr(year_filter.filter_value,1,4) + 5 THEN ''Thereafter''   ELSE To_Char(MONTH,''yyyy'')  END,  ls_cur_type,  iso_code,  currency_display_symbol,  buckets.bucket_name  ) schedule  GROUP BY rollup(' ||
'ilr_id, revision, set_of_books_id, ls_cur_type, currency, currency_symbol, executory_bucket, time_period)  HAVING ilr_id IS NOT NULL AND revision IS NOT NULL AND set_of_books_id IS NOT NULL AND ls_cur_type IS NOT NULL AND currency IS NOT NULL AND currency_symbol IS NOT NULL AND executory_bucket IS NOT NULL  )  SELECT  (SELECT filter_value as_of_monthnum FROM pp_any_required_filter WHERE Upper(column_name) = ''AS OF MONTHNUM'') as_of_monthnum,  lct.description currency_type,  c.description company,  ls.lease_number lease_number,  ilr.ilr_number,  st.description ilr_status,  lg.description lease_group,  ig.description ilr_group,  lse.description lessee,  ct.description capitalization_type,  sob.description set_of_books,  amounts.time_period,  amounts.currency,  amounts.executory_bucket,  amounts.future_executory_receipts,  amounts.currency_symbol  FROM amounts   JOIN lsr_ilr ilr ON (amounts.ilr_id = ilr.ilr_id AND amounts.revision = ilr.current_revision)   JOIN ls_lease_currency_type lct ON (amounts.ls_cur_type = lct.ls_currency_type_id)   JOIN lsr_lease ls ON (ilr.lease_id = ls.lease_id)   JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id)   JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id)   JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id)   JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)   JOIN lsr_cap_type ct ON (o.lease_cap_type_id = ct.cap_type_id)   JOIN company c ON (ilr.company_id = c.company_id)   JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id)   JOIN set_of_books sob ON (amounts.set_of_books_id = sob.set_of_books_id) WHERE amounts.future_executory_receipts <> 0 ORDER BY ilr_number, currency_type, set_of_books, executory_bucket, time_period'
WHERE description = 'Non Disclosure: Future Estimated Executory Receipts';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3889, 0, 2017, 1, 0, 0, 49278, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049278_lessor_02_query_future_estimated_executory_receipts_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;