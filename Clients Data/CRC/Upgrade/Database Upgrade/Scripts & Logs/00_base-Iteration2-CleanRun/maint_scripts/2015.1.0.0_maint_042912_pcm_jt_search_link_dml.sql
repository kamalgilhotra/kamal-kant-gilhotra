/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042912_pcm_jt_search_link_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/16/2015 Sarah Byers      Open JT Search via Links
||============================================================================
*/
-- Populate all existing records with 0
update ppbase_workspace_links
	set linked_window = 'w_task_select_tabs',
		 linked_window_label = 'Search (Tasks)',
		 linked_window_opensheet = 1,
		 linked_workspace_identifier = null
 where module = 'pcm'
	and linked_workspace_identifier in ('fp_maint_job_tasks', 'wo_maint_job_tasks');
	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2301, 0, 2015, 1, 0, 0, 042912, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042912_pcm_jt_search_link_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;