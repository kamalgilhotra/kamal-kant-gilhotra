SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035213_aro_reg_table2.sql
|| Description: Updates to Regulated ARO schema
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/15/2014 Ryan Oliveria
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE, PP_REPORT_NUMBER,
    OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          'ARO Pending Regulated Activity',
          'ARO Pending Regulated Activity',
          null,
          'dw_aro_pend_reg_activity_rpt',
          'already has report time',
          null,
          null,
          'ARO - 1017',
          'dw_company_select',
          null,
          null,
          9,
          37,
          1,
          17,
          1,
          1,
          null,
          null,
          null,
          null,
          null,
          null
     from PP_REPORTS;

create table DEPR_TRANS_TYPE
(
 DEPR_TRANS_TYPE_ID number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(35)
);

comment on table DEPR_TRANS_TYPE is 'A list of types for Depreciation Transaction Sets.';
comment on column DEPR_TRANS_TYPE.DEPR_TRANS_TYPE_ID is 'System-assigned identifier of a particular Depr Trans Type.';
comment on column DEPR_TRANS_TYPE.TIME_STAMP is 'Standard system-assigned timestamp for audit purposes.';
comment on column DEPR_TRANS_TYPE.USER_ID is 'Standard system-assigned user id for audit purposes.';
comment on column DEPR_TRANS_TYPE.DESCRIPTION is 'Description of this particular trans type.';

alter table DEPR_TRANS_TYPE
   add constraint PK_DEPR_TRANS_TYPE
       primary key (DEPR_TRANS_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

begin
   execute immediate 'alter table DEPR_TRANS_SET drop column REGULATED_ARO';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Column REGULATED_ARO did not exist to drop from the DEPR_TRANS_SET table.');
end;
/

alter table DEPR_TRANS_SET add DEPR_TRANS_TYPE_ID number(22,0);
comment on column DEPR_TRANS_SET.DEPR_TRANS_TYPE_ID is 'System-assigned identifier for a particular depr trans type. 1=Input Adj.  2=Reg ARO.';

alter table DEPR_TRANS_SET add USED_IN_ARO_CALC number(1,0);
comment on column DEPR_TRANS_SET.USED_IN_ARO_CALC is 'A number to identify whether or not this trans set has been used in Regulated ARO caculation.  1=Yes.  2=No.';

alter table DEPR_TRANS_SET
   add constraint FK_DEPR_TRANS_SET_TYPE
       foreign key (DEPR_TRANS_TYPE_ID)
       references DEPR_TRANS_TYPE (DEPR_TRANS_TYPE_ID);

insert into DEPR_TRANS_TYPE (DEPR_TRANS_TYPE_ID, DESCRIPTION) values (1, 'Input Adj');

insert into DEPR_TRANS_TYPE (DEPR_TRANS_TYPE_ID, DESCRIPTION) values (2, 'Reg ARO');

update DEPR_TRANS_SET set DEPR_TRANS_TYPE_ID = 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (860, 0, 10, 4, 2, 0, 35213, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035213_aro_reg_table2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
