/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031233_sys_pp_reports.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/16/2013 Shane Ward
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   ((select max(REPORT_ID) + 1 from PP_REPORTS), 'PWRPLANT', sysdate,
    'FERC Activity / Asset Loc/Account',
    'Lists the FERC plant account activity  for  FERC accounts 101 and 106 (together) by  asset location for a given time span and the selected set of books.',
    null, 'dw_ferc_cpr_assetloc_report', null, null, null, 'Asset - 1014                     ',
    'dw_ferc_report_select,dw_set_of_books_select', null, null, 9, 5, 2, 3, 1, 1, null, null,
    sysdate, null, null, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (494, 0, 10, 4, 1, 0, 31233, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031233_sys_pp_reports.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
