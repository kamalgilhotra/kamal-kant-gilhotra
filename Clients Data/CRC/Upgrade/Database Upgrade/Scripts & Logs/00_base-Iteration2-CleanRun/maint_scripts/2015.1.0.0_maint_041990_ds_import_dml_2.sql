/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_041990_ds_import_dml_1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- --------------   ----------------------------------------
|| 2015.1   01/11/2015 A Scott          Import tool setup for depr studies
||============================================================================
*/

SET SERVEROUTPUT ON

delete from pp_import_template_edits where import_template_id in (
   select import_template_id from pp_import_template where IMPORT_TYPE_ID between 450 and 453
  );
delete from pp_import_template_fields where IMPORT_TYPE_ID between 450 and 453;
delete from pp_import_template where IMPORT_TYPE_ID between 450 and 453;
delete from pp_import_type_updates_lookup where IMPORT_TYPE_ID between 450 and 453;
delete from pp_import_column_lookup where IMPORT_TYPE_ID between 450 and 453;
delete from pp_import_lookup where import_lookup_id between 4501 and 4999;
delete from pp_import_column where IMPORT_TYPE_ID between 450 and 453;
delete from PP_IMPORT_TYPE_SUBSYSTEM
 where IMPORT_TYPE_ID between 450 and 453
   and IMPORT_SUBSYSTEM_ID = 13;
delete from PP_IMPORT_TYPE where IMPORT_TYPE_ID between 450 and 453;
delete from PP_IMPORT_SUBSYSTEM
 where IMPORT_SUBSYSTEM_ID = 13
   and DESCRIPTION = 'Depr Studies';


begin
	insert into data_account_class (data_account_class_id, description)
	values (-1, 'Default');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Default value already exists in data_account_class.');
end;
/

insert into PP_IMPORT_SUBSYSTEM
   (IMPORT_SUBSYSTEM_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (13, 'Depr Studies', 'Depr Studies');

insert into pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 450, sysdate, user, 'DS Plant Transactions', 'Import Depr Studies Plant Transactions', 'ds_import_plant', 'ds_import_plant_arc', null, 0, 'nvo_ds_logic_import', 'ds_trans_id', 'DS Plant', '' );
insert into pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 451, sysdate, user, 'DS Reserve Transactions', 'Import Depr Studies Reserve Transactions', 'ds_import_reserve', 'ds_import_reserve_arc', null, 0, 'nvo_ds_logic_import', 'ds_trans_id', 'DS Reserve', '' );
insert into pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 452, sysdate, user, 'DS Data Accounts', 'Import Depr Studies Data Accounts', 'ds_import_data_account', 'ds_import_data_account_arc', null, 1, 'nvo_ds_logic_import', 'ds_data_account_id, ds_data_account_xlate, data_account_class_id', 'DS Data Accounts', '' );
insert into pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 453, sysdate, user, 'DS Depr Groups', 'Import Depr Studies Depr Groups', 'ds_import_depr_group', 'ds_import_depr_group_arc', null, 1, 'nvo_ds_logic_import', 'depr_group_id, depr_group_xlate, subledger_type_id, mid_period_conv, mid_period_method', 'DS Depr Groups', '' );

insert into pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 450, 13, sysdate, user );
insert into pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 451, 13, sysdate, user );
insert into pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 452, 13, sysdate, user );
insert into pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 453, 13, sysdate, user );

insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'ds_data_account_id', sysdate, user, 'Data Account', 'ds_data_account_xlate', 1, 1, 'number(22,0)', 'ds_data_account', '', 1, 452, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'analysis_trans_id', sysdate, user, 'Analysis Transaction Code', 'analysis_trans_xlate', 1, 1, 'number(22,0)', 'analysis_transaction_code', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'vintage', sysdate, user, 'Vintage', '', 1, 1, 'number(22,0)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'activity_year', sysdate, user, 'Activity Year', '', 1, 1, 'number(22,0)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'adjustment_year', sysdate, user, 'Adjustment Year', '', 0, 1, 'number(22,0)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'amount', sysdate, user, 'Amount', '', 1, 1, 'number(22,2)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'quantity', sysdate, user, 'Quantity', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'description', sysdate, user, 'Description', '', 0, 1, 'varchar2(254)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 450, 'set_of_books_id', sysdate, user, 'Set of Books', 'set_of_books_id_xlate', 0, 1, 'number(22,0)', 'set_of_books', '', 1, null, '', '1' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'depr_group_id', sysdate, user, 'Depreciation Group', 'depr_group_xlate', 1, 1, 'number(22,0)', 'depr_group', '', 1, 453, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'analysis_trans_id', sysdate, user, 'Analysis Transaction Code', 'analysis_trans_xlate', 1, 1, 'number(22,0)', 'analysis_transaction_code', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'activity_year', sysdate, user, 'Activity Year', '', 1, 1, 'number(22,0)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'amount', sysdate, user, 'Amount', '', 1, 1, 'number(22,2)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'quantity', sysdate, user, 'Quantity', '', 0, 1, 'number(22,2)', '', '', 1, null, '', '0' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'description', sysdate, user, 'Description', '', 0, 1, 'varchar2(254)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 451, 'set_of_books_id', sysdate, user, 'Set of Books', 'set_of_books_id_xlate', 0, 1, 'number(22,0)', 'set_of_books', '', 1, null, '', '1' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 452, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 452, 'description', sysdate, user, 'Description', '', 0, 1, 'varchar2(35)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 452, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(255)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 452, 'status_code_id', sysdate, user, 'Status Code', 'status_code_xlate', 1, 1, 'number(22,0)', 'status_code', '', 1, null, '', '1' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 453, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 453, 'description', sysdate, user, 'Description', '', 0, 1, 'varchar2(35)', '', '', 1, null, '', '' );
insert into pp_import_column ( import_type_id, column_name, time_stamp, user_id, description, import_column_name, is_required, processing_order, column_type, parent_table, parent_table_pk_column2, is_on_table, autocreate_import_type_id, parent_table_pk_column, default_value ) values ( 453, 'external_depr_code', sysdate, user, 'External Depr Code', '', 0, 1, 'varchar2(35)', '', '', 1, null, '', '' );

insert into pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 4501, sysdate, user, 'DS Data Account.Description', 'The passed in value corresponds to the DS Data Account: Description field.  Translate to the DS Data Account ID using the Description column on the DS Data Account table.', 'ds_data_account_id', '( select ds.ds_data_account_id from ds_data_account ds where upper( trim( <importfield> ) ) = upper( trim( ds.description ) ) )', 0, 'ds_data_account', 'description', '', '', null );
insert into pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 4502, sysdate, user, 'DS Data Account.Long Description', 'The passed in value corresponds to the DS Data Account: Long Description field.  Translate to the DS Data Account ID using the Long Description column on the DS Data Account table.', 'ds_data_account_id', '( select ds.ds_data_account_id from ds_data_account ds where upper( trim( <importfield> ) ) = upper( trim( ds.long_description ) ) )', 0, 'ds_data_account', 'long_description', '', '', null );
insert into pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 4503, sysdate, user, 'DS Data Account.Description (for given Company)', 'The passed in value corresponds to the DS Data Account: Description field.  Translate to the DS Data Account ID using the Description column on the DS Data Account table.', 'ds_data_account_id', '( select ds.ds_data_account_id from ds_data_account ds where upper( trim( <importfield> ) ) = upper( trim( ds.description ) ) and <importtable>.company_id = ds.company_id )', 0, 'ds_data_account', 'description', 'company_id', 'select ds_data_account.description, company.<company_id> from ds_data_account, company where ds_data_account.company_id = company.company_id and 5=5', null );
insert into pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 4504, sysdate, user, 'DS Data Account.Long Description (for given Company)', 'The passed in value corresponds to the DS Data Account: Long Description field.  Translate to the DS Data Account ID using the Long Description column on the DS Data Account table.', 'ds_data_account_id', '( select ds.ds_data_account_id from ds_data_account ds where upper( trim( <importfield> ) ) = upper( trim( ds.long_description ) ) and <importtable>.company_id = ds.company_id )', 0, 'ds_data_account', 'long_description', 'company_id', 'select ds_data_account.long_description, company.<company_id> from ds_data_account, company where ds_data_account.company_id = company.company_id and 5=5', null );
insert into pp_import_lookup ( import_lookup_id, time_stamp, user_id, description, long_description, column_name, lookup_sql, is_derived, lookup_table_name, lookup_column_name, lookup_constraining_columns, lookup_values_alternate_sql, derived_autocreate_yn ) values ( 4505, sysdate, user, 'Analysis Transaction Code.Description', 'The passed in value corresponds to the DS Data Account: Description field.  Translate to the DS Data Account ID using the Description column on the DS Data Account table.', 'ds_data_account_id', '( select atc.analysis_trans_id from analysis_transaction_code atc where upper( trim( <importfield> ) ) = upper( trim( atc.description ) ) )', 0, 'analysis_transaction_code', 'description', '', '', null );

insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'ds_data_account_id', 4501, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'ds_data_account_id', 4502, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'ds_data_account_id', 4503, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'ds_data_account_id', 4504, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'company_id', 19, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'company_id', 20, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'company_id', 21, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'analysis_trans_id', 4505, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 450, 'set_of_books_id', 501, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'depr_group_id', 41, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'depr_group_id', 42, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'depr_group_id', 502, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'depr_group_id', 503, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'company_id', 19, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'company_id', 20, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'company_id', 21, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'analysis_trans_id', 4505, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 451, 'set_of_books_id', 501, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 452, 'company_id', 19, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 452, 'company_id', 20, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 452, 'company_id', 21, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 452, 'status_code_id', 138, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 452, 'status_code_id', 139, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 453, 'company_id', 19, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 453, 'company_id', 20, sysdate, user );
insert into pp_import_column_lookup ( import_type_id, column_name, import_lookup_id, time_stamp, user_id ) values ( 453, 'company_id', 21, sysdate, user );

insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 452, 4501, sysdate, user );
insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 452, 4502, sysdate, user );
insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 452, 4503, sysdate, user );
insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 452, 4504, sysdate, user );
insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 453, 41, sysdate, user );
insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 453, 42, sysdate, user );
insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 453, 502, sysdate, user );
insert into pp_import_type_updates_lookup ( import_type_id, import_lookup_id, time_stamp, user_id ) values ( 453, 503, sysdate, user );

declare

   new_plt_template_id number(22,0);
   new_res_template_id number(22,0);
   auto_da_template_id number(22,0);
   auto_dg_template_id number(22,0);
   max_template_id number(22,0);
   err_msg varchar2(2000);

begin

   err_msg := 'Populating next template id for plant transactions.';
   SELECT pp_import_template_seq.nextval INTO new_plt_template_id FROM DUAL;

   err_msg := 'Populating next template id for reserve transactions.';
   SELECT pp_import_template_seq.nextval INTO new_res_template_id FROM DUAL;

--   err_msg := 'Populating next template id for auto-creating data accounts.';
--   SELECT pp_import_template_seq.nextval INTO auto_da_template_id FROM DUAL;
--
--   err_msg := 'Populating next template id for auto-creating depr groups.';
--   SELECT pp_import_template_seq.nextval INTO auto_dg_template_id FROM DUAL;

   ----delivered plant template, fields, edits
   err_msg := 'new_plt_template_id : '||new_plt_template_id||' Inserting into pp_import_template.';
   insert into pp_import_template (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   values (new_plt_template_id, to_date('12-01-2015 15:16:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'DS Plant Transactions Template 1', 'DS Plant Transactions Template 1', 'PWRPLANT', to_date('12-01-2015 15:16:40', 'dd-mm-yyyy hh24:mi:ss'), null, null, null, 0);

   err_msg := 'new_plt_template_id : '||new_plt_template_id||' Inserting into pp_import_template_fields.';
   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 1, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'company_id', 19, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 2, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'ds_data_account_id', 4503, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 3, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'analysis_trans_id', 4505, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 4, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'activity_year', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 5, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'vintage', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 6, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'adjustment_year', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 7, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'amount', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 8, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'quantity', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 9, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'description', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_plt_template_id, 10, to_date('12-01-2015 15:18:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 450, 'set_of_books_id', 501, null, null, null);

   err_msg := 'new_plt_template_id : '||new_plt_template_id||' Inserting into pp_import_template_edits.';
   ---- this allows the old mappings to be used in the import correction logic.
   insert into pp_import_template_edits
   (import_template_id, sequence_number, column_name, original_value, new_value, made_by, made_at)
   select 
      new_plt_template_id,
      pp_import_template_edits_seq.nextval,
      column_id,
      original_value,
      new_value,
      user,
      sysdate
   from 
   (
      select distinct 
         'ds_data_account_xlate' column_id,
         a.hist_acct original_value,
         b.description new_value
      from ds_db_import_acct_map a,
         ds_data_account b
      where a.ds_data_account_id = b.ds_data_account_id
      and a.hist_acct <> b.description
      union 
      select distinct
         'analysis_trans_xlate' column_id,
         a.tran_code original_value,
         b.description new_value
      from ds_db_import_code_map a,
         analysis_transaction_code b
      where a.analysis_trans_id = b.analysis_trans_id
      and a.tran_code <> b.description
   );

   ----delivered reserve template and fields
   err_msg := 'new_plt_template_id : '||new_plt_template_id||' Inserting into pp_import_template.';
   insert into pp_import_template (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   values (new_res_template_id, to_date('12-01-2015 15:20:39', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'DS Reserve Transactions Template 1', 'DS Reserve Transactions Template 1', 'PWRPLANT', to_date('12-01-2015 15:20:39', 'dd-mm-yyyy hh24:mi:ss'), null, null, null, 0);

   err_msg := 'new_plt_template_id : '||new_plt_template_id||' Inserting into pp_import_template_fields.';
   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 1, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'company_id', 19, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 2, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'depr_group_id', 502, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 3, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'analysis_trans_id', 4505, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 4, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'activity_year', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 5, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'amount', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 6, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'quantity', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 7, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'description', null, null, null, null);

   insert into pp_import_template_fields (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   values (new_res_template_id, 8, to_date('12-01-2015 15:22:25', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 451, 'set_of_books_id', 501, null, null, null);

   err_msg := 'new_plt_template_id : '||new_plt_template_id||' Inserting into pp_import_template_fields.';
   ---- this allows the old mappings to be used in the import correction logic.
   insert into pp_import_template_edits
   (import_template_id, sequence_number, column_name, original_value, new_value, made_by, made_at)
   select 
      new_res_template_id,
      pp_import_template_edits_seq.nextval,
      column_id,
      original_value,
      new_value,
      user,
      sysdate
   from 
   (
      select distinct 
         'depr_group_xlate' column_id,
         a.hist_acct original_value,
         b.description new_value
      from ds_db_import_acct_map a,
         depr_group b
      where a.depr_group_id = b.depr_group_id
      and a.hist_acct <> b.description
      union
      select distinct
         'analysis_trans_xlate' column_id,
         a.tran_code original_value,
         b.description new_value
      from ds_db_import_code_map a,
         analysis_transaction_code b
      where a.analysis_trans_id = b.analysis_trans_id
      and a.tran_code <> b.description
   );

   ----  NOT auto creating data accounts, depr groups.  


   DBMS_OUTPUT.PUT_LINE('Successfully populated delivered ds import templates.');

exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Error in populating delivered templates.  Last message: '||err_msg);

end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2163, 0, 2015, 1, 0, 0, 41990, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041990_ds_import_dml_2.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;