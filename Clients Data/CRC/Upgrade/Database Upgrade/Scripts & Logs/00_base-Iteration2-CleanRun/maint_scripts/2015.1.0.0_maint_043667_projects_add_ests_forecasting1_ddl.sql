/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043667_projects_add_ests_forecasting1_ddl.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     04/25/2015 Chris Mardis   Additional estimate attributes requires flexible number of detail levels
||============================================================================
*/

create table d1_wo_est_forecast_options_bak as select * from wo_est_forecast_options;

alter table wo_est_forecast_options rename column detail_level1 to detail_levels;
alter table wo_est_forecast_options modify detail_levels varchar2(4000);

alter table wo_est_forecast_options add subtotal_details varchar2(4000);

comment on column wo_est_forecast_options.subtotal_details is 'Holds the saved template values for the Detail Level subtotal checkboxes.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2527, 0, 2015, 1, 0, 0, 43667, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043667_projects_add_ests_forecasting1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;