/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_32_ls_cr_deriver_dddws_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

--execute p_drop_table('ls_cr_deriver_dropdown_config');

create table ls_cr_deriver_dropdown_config
(column_name varchar2(35) not null,
 dddw_name varchar2(35) not null,
 display_column varchar2(35) not null,
 data_column varchar2(35) not null,
 time_stamp date,
 user_id varchar2(24));

ALTER TABLE ls_cr_deriver_dropdown_config
  ADD CONSTRAINT ls_cr_deriv_dropdown_config_pk PRIMARY KEY (
    column_name
  )
  USING INDEX
     TABLESPACE pwrplant_idx
/
 
comment on table ls_cr_deriver_dropdown_config is 'Table used to store the custom dropdown configurations for lease CR deriver control line input';
comment on column ls_cr_deriver_dropdown_config.column_name is 'The column name on the CR deriver control dynamic datawindow. Usually associated with a CR element';
comment on column ls_cr_deriver_dropdown_config.dddw_name is 'Drodown DataWindow name for the given column. Custom dddw''s may need to be made on a client to client basis';
comment on column ls_cr_deriver_dropdown_config.display_column is 'The column that will be displayed to the user from the dropdown datawindow';
comment on column ls_cr_deriver_dropdown_config.data_column is 'The column that will be input into the database from the dropdown datawindow';
COMMENT ON COLUMN ls_cr_deriver_dropdown_config.user_id IS 'Standard system-assigned user id user for audit purposes.';
COMMENT ON COLUMN ls_cr_deriver_dropdown_config.time_stamp IS 'Standard system-assigned timestamp user for audit purposes.';






--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2418, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_32_ls_cr_deriver_dddws_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;