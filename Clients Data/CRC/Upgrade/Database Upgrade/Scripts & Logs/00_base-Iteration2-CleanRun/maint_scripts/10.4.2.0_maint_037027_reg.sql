/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037027_reg.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 03/11/2014 Sarah Byers
||============================================================================
*/

alter table PP_COMPANY_SECURITY_TEMP add REG_COMPANY_ID number(22,0);

comment on column PWRPLANT.PP_COMPANY_SECURITY_TEMP.REG_COMPANY_ID is 'System assigned identifier of a Regulatory Company.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1023, 0, 10, 4, 2, 0, 37027, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037027_reg.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;