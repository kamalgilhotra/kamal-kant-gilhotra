/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045046_lease_new_forecast_wksp_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/13/2015 Sarah Byers    New Fcst Version workspace
||============================================================================
*/

update ppbase_menu_items
	set workspace_identifier = null,
		 menu_identifier = 'forecast'
 where module = 'LESSEE'
	and menu_identifier = 'forecast_tool';

insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, object_type_id)
values (
	'LESSEE', 'create_forecast', 'Create Forecast Version', 'uo_ls_create_fcst_wksp', 1);

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
values (
	'LESSEE', 'create_forecast', 2, 1, 'Create Forecast Version', 'forecast', 'create_forecast', 1);
	
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2919, 0, 2015, 2, 0, 0, 45046, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045046_lease_new_forecast_wksp_dml.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
