/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009531_project_cgc_indexes.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   02/21/2012 Joseph King    Point Release
||============================================================================
*/

SET SERVEROUTPUT ON

-- Base index name = CGCTWOIDIX
-- Look up index name incase it is different.
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';

   V_TABLE_NAME    varchar2(30) := 'CHARGE_GROUP_CONTROL';
   V_INDEX_COLUMNS varchar2(2000) := 'WORK_ORDER_ID';
   V_INDEX_COUNT   number := 0;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   for CSR_SELECT in (select 'drop index ' || UIC.INDEX_NAME DROP_STATEMENT, UIC.INDEX_NAME
                        from (select INDEX_NAME, STRING_AGG(COLUMN_NAME) INDEX_COLUMNS
                                from USER_IND_COLUMNS
                               where TABLE_NAME = V_TABLE_NAME
                               group by INDEX_NAME) V1,
                             USER_IND_COLUMNS UIC
                       where V1.INDEX_COLUMNS = V_INDEX_COLUMNS
                         and V1.INDEX_NAME = UIC.INDEX_NAME
                         and TABLE_NAME = V_TABLE_NAME
                       group by UIC.INDEX_NAME)
   loop
      execute immediate CSR_SELECT.DROP_STATEMENT;
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Index ' || CSR_SELECT.INDEX_NAME || ' dropped.');
      V_INDEX_COUNT := V_INDEX_COUNT + 1;
   end loop;
   if V_INDEX_COUNT = 0 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Index for Table Name: ' || V_TABLE_NAME || ' Columns: ' ||
                           V_INDEX_COLUMNS || ' not found.');
   end if;
end;
/

-- Base index name = CGCONCHTYIDIX
-- Look up index name incase it is different.
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';

   V_TABLE_NAME    varchar2(30) := 'CHARGE_GROUP_CONTROL';
   V_INDEX_COLUMNS varchar2(2000) := 'CHARGE_TYPE_ID';
   V_INDEX_COUNT   number := 0;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   for CSR_SELECT in (select 'drop index ' || UIC.INDEX_NAME DROP_STATEMENT, UIC.INDEX_NAME
                        from (select INDEX_NAME, STRING_AGG(COLUMN_NAME) INDEX_COLUMNS
                                from USER_IND_COLUMNS
                               where TABLE_NAME = V_TABLE_NAME
                               group by INDEX_NAME) V1,
                             USER_IND_COLUMNS UIC
                       where V1.INDEX_COLUMNS = V_INDEX_COLUMNS
                         and V1.INDEX_NAME = UIC.INDEX_NAME
                         and TABLE_NAME = V_TABLE_NAME
                       group by UIC.INDEX_NAME)
   loop
      execute immediate CSR_SELECT.DROP_STATEMENT;
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Index ' || CSR_SELECT.INDEX_NAME || ' dropped.');
      V_INDEX_COUNT := V_INDEX_COUNT + 1;
   end loop;
   if V_INDEX_COUNT = 0 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Index for Table Name: ' || V_TABLE_NAME || ' Columns: ' ||
                           V_INDEX_COLUMNS || ' not found.');
   end if;
end;
/

-- Base index name = CHARGEBATCH
-- Look up index name incase it is different.
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';

   V_TABLE_NAME    varchar2(30) := 'CHARGE_GROUP_CONTROL';
   V_INDEX_COLUMNS varchar2(2000) := 'BATCH_UNIT_ITEM_ID';
   V_INDEX_COUNT   number := 0;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   for CSR_SELECT in (select 'drop index ' || UIC.INDEX_NAME DROP_STATEMENT, UIC.INDEX_NAME
                        from (select INDEX_NAME, STRING_AGG(COLUMN_NAME) INDEX_COLUMNS
                                from USER_IND_COLUMNS
                               where TABLE_NAME = V_TABLE_NAME
                               group by INDEX_NAME) V1,
                             USER_IND_COLUMNS UIC
                       where V1.INDEX_COLUMNS = V_INDEX_COLUMNS
                         and V1.INDEX_NAME = UIC.INDEX_NAME
                         and TABLE_NAME = V_TABLE_NAME
                       group by UIC.INDEX_NAME)
   loop
      execute immediate CSR_SELECT.DROP_STATEMENT;
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Index ' || CSR_SELECT.INDEX_NAME || ' dropped.');
      V_INDEX_COUNT := V_INDEX_COUNT + 1;
   end loop;
   if V_INDEX_COUNT = 0 then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Index for Table Name: ' || V_TABLE_NAME || ' Columns: ' ||
                           V_INDEX_COLUMNS || ' not found.');
   end if;
end;
/


create index CGCONCHTYIDIX
   on CHARGE_GROUP_CONTROL (CHARGE_TYPE_ID, WORK_ORDER_ID, BATCH_UNIT_ITEM_ID, PEND_TRANSACTION)
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (97, 0, 10, 3, 4, 0, 9531, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009531_project_cgc_indexes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
