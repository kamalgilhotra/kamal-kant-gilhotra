/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040645_pcm_fp_init_dml1.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/04/2015 Alex P.          WO/FP Cotenancy Workspaces
||============================================================================
*/

insert into ppbase_workspace( module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id )
values( 'pcm', 'fp_create_cotenancy', 'Create (FP) - Cotenancy', 'uo_pcm_create_wksp_cotenancy', 'Create Cotenancy (FP)', 1 );

insert into ppbase_workspace( module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id )
values( 'pcm', 'wo_create_cotenancy', 'Create (WO) - Cotenancy', 'uo_pcm_create_wksp_cotenancy', 'Create Cotenancy (WO)', 1 );

update ppbase_menu_items
set workspace_identifier = 'wo_create_detail'
where workspace_identifier = 'wo_create'
 and  module = 'pcm';
 
update ppbase_menu_items
set workspace_identifier = 'fp_create_detail'
where workspace_identifier = 'fp_create'
 and  module = 'pcm';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2307, 0, 2015, 1, 0, 0, 040645, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040645_pcm_fp_init_dml1.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;