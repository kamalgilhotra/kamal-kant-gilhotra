/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048886_lessor_01_add_lessor_cap_type_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/21/2017 Jared Watkins    Create the tables for Lessor cap types and FASB/SOB association
||============================================================================
*/

--create the Lessor Cap Types table
create table lsr_cap_type (
  cap_type_id       number(22,0)  not null,
  user_id           varchar2(18)  null,
  time_stamp        date          null,
  description       varchar2(35)  not null,
  long_description  varchar2(254) null,
  book_summary_id   number(22,0)  not null,
  active            number(1)     default 1 not null
);

alter table lsr_cap_type
add constraint pk_lsr_cap_type
primary key ( cap_type_id )
using index tablespace pwrplant_idx;

alter table lsr_cap_type
add constraint fk1_lsr_cap_type_book_summary
foreign key ( book_summary_id )
references book_summary( book_summary_id );

COMMENT ON TABLE lsr_cap_type IS '(S)[06] The Lessor Cap Types table is a configuration table that holds data related to the type of Lease selected (Operating, Sales Type, or Direct Finance). Also maps to the specific Book Summary to select which bucket holds the related amounts.';

COMMENT ON COLUMN lsr_cap_type.cap_type_id IS 'System-assigned ID of the user-defined Lessor Cap Type';
COMMENT ON COLUMN lsr_cap_type.description IS 'Name of the user-defined Lessor Cap Type.';
COMMENT ON COLUMN lsr_cap_type.long_description IS 'Longer description of the Cap Type.';
COMMENT ON COLUMN lsr_cap_type.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN lsr_cap_type.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_cap_type.book_summary_id IS 'The basis bucket that stores this Lease''s dollars.';
COMMENT ON COLUMN lsr_cap_type.active IS 'Active = 1, Inactive = 0.';

--create the FASB cap type master data table for Lessor
create table lsr_fasb_cap_type (
  fasb_cap_type_id  number(22,0) not null,
  user_id           varchar2(18) null,
  time_stamp        date         null,
  description       varchar2(35) not null
);

alter table lsr_fasb_cap_type
add constraint pk_lsr_fasb_cap_type
primary key ( fasb_cap_type_id )
using index tablespace pwrplant_idx;

COMMENT ON TABLE lsr_fasb_cap_type IS '(S)[06] The Lessor FASB cap type.';

COMMENT ON COLUMN lsr_fasb_cap_type.fasb_cap_type_id IS 'System-assigned identifier that specifies the Lessor FASB cap type.';
COMMENT ON COLUMN lsr_fasb_cap_type.description IS 'Description of the FASB cap type.';
COMMENT ON COLUMN lsr_fasb_cap_type.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_fasb_cap_type.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--create the table to associate FASB cap types with SOBs
create table lsr_fasb_type_sob (
  cap_type_id       number(22,0) not null,
  fasb_cap_type_id  number(22,0) not null,
  set_of_books_id   number(22,0) not null,
  user_id           varchar2(18) null,
  time_stamp        date         null
);

alter table lsr_fasb_type_sob
add constraint pk_lsr_fasb_type_sob
primary key ( cap_type_id, fasb_cap_type_id, set_of_books_id )
using index tablespace pwrplant_idx;

alter table lsr_fasb_type_sob
add constraint fk1_fasb_sob_lsr_cap_type
foreign key ( cap_type_id )
references lsr_cap_type ( cap_type_id );

alter table lsr_fasb_type_sob
add constraint fk2_fasb_sob_fasb_cap_type
foreign key ( fasb_cap_type_id )
references lsr_fasb_cap_type ( fasb_cap_type_id );

alter table lsr_fasb_type_sob
add constraint fk3_fasb_sob_set_of_books
foreign key ( set_of_books_id )
references set_of_books ( set_of_books_id );

COMMENT ON TABLE lsr_fasb_type_sob IS '(S)[06] Lessor FASB cap type to set of books mapping table';

COMMENT ON COLUMN lsr_fasb_type_sob.cap_type_id IS 'System assigned identifier of lease cap type';
COMMENT ON COLUMN lsr_fasb_type_sob.fasb_cap_type_id IS 'System assigned identifier of FASB cap type';
COMMENT ON COLUMN lsr_fasb_type_sob.set_of_books_id IS 'System assigned identifier of set of books';
COMMENT ON COLUMN lsr_fasb_type_sob.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN lsr_fasb_type_sob.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3730, 0, 2017, 1, 0, 0, 48886, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048886_lessor_01_add_lessor_cap_type_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;