/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039578_pwrtax_company_consol.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/12/2014 Alex P.
||============================================================================
*/

drop table COMPANY_CONSOL_VIEW;

-- Create TAX_COMPANY_CONSOL table
create global temporary table TAX_COMPANY_CONSOL
(
 PP_TREE_CATEGORY_ID number(22) not null,
 CONSOLIDATION_ID    number(22) not null,
 PARENT_ID           number(22) not null,
 COMPANY_ID          number(22) not null,
 DESCRIPTION         varchar2(254),
 SORT_ORDER          number(22) not null
) on commit preserve rows;

alter table TAX_COMPANY_CONSOL
  add constraint PK_TCC_CAT_CONSOL_PRNT_CO
      primary key (PP_TREE_CATEGORY_ID, CONSOLIDATION_ID, PARENT_ID, COMPANY_ID);

comment on table TAX_COMPANY_CONSOL is '(T)  [09] The Tax Company Consol table is a global temp table that holds user-defined company consolidations using company security whose primary use is to consolidate PowerTax reports.';
comment on column TAX_COMPANY_CONSOL.PP_TREE_CATEGORY_ID is 'System-assigned identifier of a tree category.';
comment on column TAX_COMPANY_CONSOL.CONSOLIDATION_ID is 'System-assigned identifier of a tree type.';
comment on column TAX_COMPANY_CONSOL.PARENT_ID is 'On the tree, the parent identifier of the company node.';
comment on column TAX_COMPANY_CONSOL.COMPANY_ID is 'On the tree, the child identifier of the company node.';
comment on column TAX_COMPANY_CONSOL.DESCRIPTION is 'The description is what will be shown on the PowerTax report.  By default the description will be the company description of the parent however this may be overwritten by the user.';
comment on column TAX_COMPANY_CONSOL.SORT_ORDER is 'By default the tree will sort alphabetically, but if the user desires to sort the tree in a different order the user can specify the sort using this column.';

-- Recreate COMPANY_CONSOL_VIEW view
create or replace view COMPANY_CONSOL_VIEW as
select B.PP_TREE_CATEGORY_ID,
       B.PP_TREE_TYPE_ID CONSOLIDATION_ID,
       B.MAIN_CHILD PARENT_ID,
       B.CHILD_ID COMPANY_ID,
       NVL(A.OVERRIDE_PARENT_DESCRIPTION, NVL(COMPANY.DESCRIPTION, 'NO DESCRIPTION')) DESCRIPTION,
       B.SORT_ORDER
  from (select distinct PP_TREE_TYPE_ID, PARENT_ID, OVERRIDE_PARENT_DESCRIPTION
          from (select PP_TREE_CATEGORY_ID,
                       PP_TREE_TYPE_ID,
                       PARENT_ID,
                       CHILD_ID,
                       EMPTY_NODE,
                       OVERRIDE_PARENT_DESCRIPTION,
                       NVL(SORT_ORDER, 0) SORT_ORDER
                  from PP_TREE
                 where PP_TREE_CATEGORY_ID = 1)
         where OVERRIDE_PARENT_DESCRIPTION is not null) A,
       COMPANY COMPANY,
       (select A.PP_TREE_CATEGORY_ID,
               A.PP_TREE_TYPE_ID,
               A.PARENT_ID,
               A.CHILD_ID,
               level                 LEVEL_NUM,
               CONNECT_BY_ROOT       A.CHILD_ID MAIN_CHILD,
               A.EMPTY_NODE,
               CONNECT_BY_ROOT       A.SORT_ORDER SORT_ORDER
          from (select PP_TREE_CATEGORY_ID,
                       PP_TREE_TYPE_ID,
                       PARENT_ID,
                       CHILD_ID,
                       EMPTY_NODE,
                       OVERRIDE_PARENT_DESCRIPTION,
                       NVL(SORT_ORDER, 0) SORT_ORDER
                  from PP_TREE
                 where PP_TREE_CATEGORY_ID = 1) A
         start with A.CHILD_ID is not null
        connect by NOCYCLE prior A.CHILD_ID = A.PARENT_ID
               and prior A.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID) B,
       (select A.PP_TREE_CATEGORY_ID, A.PP_TREE_TYPE_ID, A.PARENT_ID, A.CHILD_ID, level LEVEL_NUM
          from (select PP_TREE_CATEGORY_ID,
                       PP_TREE_TYPE_ID,
                       PARENT_ID,
                       CHILD_ID,
                       EMPTY_NODE,
                       OVERRIDE_PARENT_DESCRIPTION,
                       NVL(SORT_ORDER, 0) SORT_ORDER
                  from PP_TREE
                 where PP_TREE_CATEGORY_ID = 1) A
         start with A.CHILD_ID = A.PARENT_ID
        connect by NOCYCLE prior A.CHILD_ID = A.PARENT_ID
               and prior A.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID) C
 where B.MAIN_CHILD = A.PARENT_ID(+)
   and B.PP_TREE_TYPE_ID = A.PP_TREE_TYPE_ID(+)
   and B.MAIN_CHILD = COMPANY.COMPANY_ID(+)
   and B.MAIN_CHILD = C.CHILD_ID
   and B.PP_TREE_TYPE_ID = C.PP_TREE_TYPE_ID
   and B.MAIN_CHILD in (select COMPANY_ID
                          from COMPANY
                        union
                        select CHILD_ID
                          from (select PP_TREE_CATEGORY_ID,
                                       PP_TREE_TYPE_ID,
                                       PARENT_ID,
                                       CHILD_ID,
                                       EMPTY_NODE,
                                       OVERRIDE_PARENT_DESCRIPTION,
                                       NVL(SORT_ORDER, 0) SORT_ORDER
                                  from PP_TREE
                                 where PP_TREE_CATEGORY_ID = 1) INSIDE
                         where INSIDE.EMPTY_NODE = 1
                           and B.PP_TREE_TYPE_ID = INSIDE.PP_TREE_TYPE_ID)
union
select 1           PP_TREE_CATEGORY_ID,
       -1          CONSOLIDATION_ID,
       COMPANY_ID  PARENT_ID,
       COMPANY_ID  COMPANY_ID,
       DESCRIPTION,
       0           SORT_ORDER
  from COMPANY COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1419, 0, 10, 4, 3, 0, 39578, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039578_pwrtax_company_consol.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
