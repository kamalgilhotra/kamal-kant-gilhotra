/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051379_lessee_06_add_import_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/13/2018 Sarah Byers       add columns to import and archive table
||============================================================================
*/

-- Change gl_posting_mo_yr to character field
alter table ls_import_asset_uop modify gl_posting_mo_yr varchar2(254);

-- Add company fields
alter table ls_import_asset_uop add (
  company_id number(22,0) NULL,
  company_xlate varchar2(254) NULL);

COMMENT ON COLUMN ls_import_asset_uop.company_id IS 'System-assigned identifier of a particular company.';
COMMENT ON COLUMN ls_import_asset_uop.company_xlate IS 'Translation field for determining the company using the company description.';
COMMENT ON COLUMN ls_import_asset_uop.ls_asset_xlate IS 'Translation field for determining the leased asset using the leased asset number and company.';

-- Change gl_posting_mo_yr to character field
alter table ls_import_asset_uop_archive modify gl_posting_mo_yr varchar2(254);

-- Add company fields
alter table ls_import_asset_uop_archive add (
  company_id number(22,0) NULL,
  company_xlate varchar2(254) NULL);

COMMENT ON COLUMN ls_import_asset_uop_archive.company_id IS 'System-assigned identifier of a particular company.';
COMMENT ON COLUMN ls_import_asset_uop_archive.company_xlate IS 'Translation field for determining the company using the company description.';
COMMENT ON COLUMN ls_import_asset_uop_archive.ls_asset_xlate IS 'Translation field for determining the leased asset using the leased asset number and company.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6704, 0, 2017, 4, 0, 0, 51379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051379_lessee_06_add_import_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;