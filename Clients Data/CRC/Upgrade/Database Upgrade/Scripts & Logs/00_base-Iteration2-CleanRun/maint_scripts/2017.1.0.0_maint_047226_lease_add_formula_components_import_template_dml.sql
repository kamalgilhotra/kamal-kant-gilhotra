/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047226_lease_add_formula_components_import_template_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/08/2017 Jared Watkins    populate all of the rows needed for the new import
||============================================================================
*/

--this will insert the new import type we need
insert into pp_import_type(import_type_id, description, long_description, 
  import_table_name, archive_table_name, allow_updates_on_add, delegate_object_name)
values(263, 'Add: Var Payment Component Amounts', 'Add: Variable Payment Formula Component Amounts', 
  'ls_import_vp_comp_amts', 'ls_import_vp_comp_amts_archive', 1, 'nvo_ls_logic_import')
;

--relate the import type to the lease subsystem
insert into pp_import_type_subsystem(import_type_id, import_subsystem_id)
values(263, 8)
;

--insert the columns we will need to use in the import
insert into pp_import_column(import_type_id, column_name, description, import_column_name, 
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(263, 'formula_component_id', 'Formula Component Identifier', 'formula_component_xlate',
  1, 1, 'number(22,0)', 'ls_formula_component', 'formula_component_id')
;

insert into pp_import_column(import_type_id, column_name, description, import_column_name, 
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(263, 'effective_date', 'Effective Date', NULL,
  0, 1, 'varchar2(254)', NULL, NULL)
;

insert into pp_import_column(import_type_id, column_name, description, import_column_name, 
  is_required, processing_order, column_type, parent_table, parent_table_pk_column)
values(263, 'amount', 'Amount', NULL,
  0, 1, 'varchar2(254)', NULL, NULL)
;

--add the new lookup for the Formula Component ID into the database
insert into pp_import_lookup(import_lookup_id, description, long_description, 
  column_name, lookup_sql, 
  is_derived, lookup_table_name, lookup_column_name)
values(2504, 'Formula Component.Description', 'The passed in value corresponds to the LS Formula Component: Description field.  Translate to the Formula Component ID using the Description column on the LS Formula Component table.',
  'formula_component_id', '( select f.formula_component_id from ls_formula_component f where upper( trim( <importfield> ) ) = upper( trim( f.description ) ) )',
  0, 'ls_formula_component', 'description')
;

--associate the column in the import with the lookup translation
insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
values(263, 'formula_component_id', 2504);

--add the import template (based on max ID + 1) and the fields for the template
declare
  templateID number := pp_import_template_seq.nextval;
begin
  --add the specific template into the DB
  insert into pp_import_template(import_template_id, import_type_id, description, 
    long_description, created_by, created_date, do_update_with_add)
  select distinct templateID, 263, 'Index/Rate VP Component Amounts Add', 
    'Index/Rates Formula Component Amounts Add', 'PWRPLANT', SYSDATE, 1
  from pp_import_template
  where not exists (select 1 from pp_import_template where description = 'Index/Rate VP Component Amounts Add');
  
  --add the fields for the template into the DB
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 1, 263, 'formula_component_id', 2504)
  ;
  
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 2, 263, 'effective_date', NULL)
  ;
  
  insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  values(templateID, 3, 263, 'amount', NULL)
  ;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3486, 0, 2017, 1, 0, 0, 47226, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047226_lease_add_formula_components_import_template_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;