/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048266_taxrpr_report_fixes_uop_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 06/15/2017 Eric Berger    Updating the datawindow values in
||                                      reporting tables to reflect the new
||                                      tax repairs standard.
||============================================================================
*/

--update report UOP - 2270

UPDATE pp_reports
SET datawindow = 'dw_rpr_2270'
WHERE report_number = 'UOP - 2270';

--update report UOP - 2271

UPDATE pp_reports
SET datawindow = 'dw_rpr_2271'
WHERE report_number = 'UOP - 2271';

--report UOP - 2280

UPDATE pp_reports
SET datawindow = 'dw_rpr_2280'
WHERE report_number = 'UOP - 2280';

--report UOP - 2290

UPDATE pp_reports
SET datawindow = 'dw_rpr_2290'
WHERE report_number = 'UOP - 2290';

--report UOP - 2300

UPDATE pp_reports
SET datawindow = 'dw_rpr_2300'
WHERE report_number = 'UOP - 2300';

--report UOP - 2310

UPDATE pp_reports
SET datawindow = 'dw_rpr_2310'
WHERE report_number = 'UOP - 2310';

--report UOP - 2320

UPDATE pp_reports
SET datawindow = 'dw_rpr_2320'
WHERE report_number = 'UOP - 2320';


--report UOP - 2340

UPDATE pp_reports
SET datawindow = 'dw_rpr_2340'
WHERE report_number = 'UOP - 2340';


--report UOP - 2360

UPDATE pp_reports
SET datawindow = 'dw_rpr_2360'
WHERE report_number = 'UOP - 2360';


--report UOP - 2380

UPDATE pp_reports
SET datawindow = 'dw_rpr_2380'
WHERE report_number = 'UOP - 2380';


--report UOP - 2390

UPDATE pp_reports
SET datawindow = 'dw_rpr_2390'
WHERE report_number = 'UOP - 2390';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13724, 0, 2018, 2, 0, 0, 48266, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048266_taxrpr_report_fixes_uop_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;