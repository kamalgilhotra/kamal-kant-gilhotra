/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_009562_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    06/08/2012 Andrew Scott   Point Release
||============================================================================
*/

insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (100, '01%',
    '1% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (300, '03%',
    '3% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (600, '06%',
    '6% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (625, '06.25%',
    '6.25% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (950, '09.5%',
    '9.5% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (1000, '10%',
    '10% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (1050, '10.5%',
    '10.5% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (1350, '13.5%',
    '13.5% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (1500, '15%',
    '15% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (2000, '20%',
    '20% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (2300, '23%',
    '23% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (2500, '25%',
    '25% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (2900, '29%',
    '29% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (3000, '30%',
    '30% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (3333, '33.333333%',
    '33.333333% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (3500, '35%',
    '10% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (4000, '40%',
    '40% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (5000, '50%',
    '50% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (6000, '60%',
    '60% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (6666, '66.666666%',
    '66.666666% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (7000, '70%',
    '70% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (7500, '75%',
    '75% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (8000, '80%',
    '80% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);
insert into PT_RATE_DEFINITION
   (PT_RATE_ID, DESCRIPTION, LONG_DESCRIPTION, RATE_TYPE_ID)
values
   (9000, '90%',
    '90% to be applied to the "cost approach value" during the allocation step to populate the "market value" on the ledger.',
    1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (150, 0, 10, 3, 5, 0, 9562, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_009562_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
