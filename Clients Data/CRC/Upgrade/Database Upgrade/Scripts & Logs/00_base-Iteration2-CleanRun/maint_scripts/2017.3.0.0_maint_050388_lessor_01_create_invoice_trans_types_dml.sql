/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050388_lessor_01_create_invoice_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 03/16/2018 Jared Watkins     Create the new Transaction types we need for Lessor Invoices
||============================================================================
*/

/* Add new trans type 4018 - Accounts Receivable Debit */
insert into je_trans_type (trans_type, description)
values (4018, 'Lessor Accounts Receivable Debit');

/* Add new trans type 4019 - Interest Invoice Credit */
insert into je_trans_type (trans_type, description)
values (4019, 'Lessor Interest Invoice Credit');

/* Add new trans type 4020 - Executory Invoice Credit */
insert into je_trans_type (trans_type, description)
values (4020, 'Lessor Executory Invoice Credit');

/* Add new trans type 4021 - Contingent Invoice Credit */
insert into je_trans_type (trans_type, description)
values (4021, 'Lessor Contingent Invoice Credit');

/* Add new trans type 4022 - Lessor ST Receivable Credit */
insert into je_trans_type (trans_type, description)
values (4022, 'Lessor ST Receivable Credit');

/* Relate the new trans types to the delivered JE Method */
insert into je_method_trans_type(je_method_id, trans_type)
select 1, trans_type from je_trans_type 
where trans_type between 4018 and 4022;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4210, 0, 2017, 3, 0, 0, 50388, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050388_lessor_01_create_invoice_trans_types_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
