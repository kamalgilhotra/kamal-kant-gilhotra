/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031231_proptax_extension_asmt_sprd.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 08/27/2013 Andrew Scott        Allow client specific assessment spread window to be opened
||============================================================================
*/

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, IS_BASE_OPTION)
values
   ('Assessments - Spread Assessments Window Object',
    'If multiple parcel assessments are selected in the parcel search or bills search, and then right-clicked, this option specifies the window that can open.  Most will use the default value, but this system option allows client extension windows to be used.',
    0, 'w_ptc_parcel_center_assessments_spread', 1);

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Assessments - Spread Assessments Window Object', 'proptax');

insert into PPBASE_SYSTEM_OPTIONS_WKSP
   (SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER)
values
   ('Assessments - Spread Assessments Window Object', 'parcel_search');

insert into PPBASE_SYSTEM_OPTIONS_WKSP
   (SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER)
values
   ('Assessments - Spread Assessments Window Object', 'bills_search');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (546, 0, 10, 4, 1, 0, 31231, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031231_proptax_extension_asmt_sprd.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;