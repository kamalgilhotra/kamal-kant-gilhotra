/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010284_version-updates.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.1   05/14/2012 AYP            10.3.4.1 version
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.3.4.0',
       PP_PATCH   = 'Patch: 10.3.4.1',
       POWERTAX_VERSION = '10.3.4.0',
       DATE_INSTALLED = sysdate,
       POST_VERSION = 'Version 10.3.4.0, April 11, 2012'
where PP_VERSION_ID = 1;


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (143, 0, 10, 3, 4, 1, 10284, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.1_maint_010284_version-updates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
