/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052436_lessee_01_import_ilr_rev_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 3/5/2019 	 Shane "C" Ward   Import tables for ILR Revisions
||============================================================================
*/

CREATE TABLE ls_import_ilr_rev
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     ilr_id                     NUMBER(22, 0),
     ilr_xlate                  VARCHAR2(254),
     discount_rate              NUMBER(22, 8),
     payment_term_id            NUMBER(22, 0),
     payment_term_date          VARCHAR2(254),
	 payment_term_type_id       NUMBER(22, 0),
     payment_term_xlate         VARCHAR2(254),
     payment_freq_id            NUMBER(22, 0),
     payment_freq_xlate         VARCHAR2(254),
     number_of_terms            NUMBER(22, 0),
     paid_amount                NUMBER(22, 2),
	 escalation					NUMBER(22, 0),
	 escalation_xlate			VARCHAR2(254),
	 escalation_freq_id			NUMBER(22, 0),
	 escalation_freq_xlate		VARCHAR2(254),
	 escalation_pct				NUMBER(22, 12),
     remeasurement_date         VARCHAR2(254),
     lease_cap_type_id          NUMBER(22, 0),
     lease_cap_type_xlate       VARCHAR2(254),
     itc_sw                     NUMBER(1, 0),
     itc_xlate                  VARCHAR2(254),
     cancelable_type_id         NUMBER(22, 0),
     cancelable_type_xlate      VARCHAR2(254),
     specialized_asset          NUMBER(1, 0),
     specialized_asset_xlate    VARCHAR2(254),
     intent_to_purch            NUMBER(1, 0),
     intent_to_purch_xlate      VARCHAR2(254),
     interco_company_id         NUMBER(22, 0),
     interco_company_xlate      VARCHAR2(254),
     interco_lease              NUMBER(1, 0),
     interco_lease_xlate        VARCHAR2(254),
     sublease_flag              NUMBER(1, 0),
     sublease_flag_xlate        VARCHAR2(254),
     sublease_id                NUMBER(22, 0),
     sublease_xlate             VARCHAR2(254),
     depr_calc_method           NUMBER(1, 0),
     depr_calc_method_xlate     VARCHAR2(254),
     remeasure_calc_gain_loss   NUMBER(1, 0),
     remeasure_calc_gain_xlate  VARCHAR2(254),
     default_rate_flag          NUMBER(1, 0),
     default_rate_flag_xlate    VARCHAR2(254),
     borrowing_curr_id          NUMBER(1, 0),
     borrowing_curr_xlate       VARCHAR2(254),
	 e_bucket_1                  NUMBER(22, 2),
     e_bucket_2                  NUMBER(22, 2),
     e_bucket_3                  NUMBER(22, 2),
     e_bucket_4                  NUMBER(22, 2),
     e_bucket_5                  NUMBER(22, 2),
     e_bucket_6                  NUMBER(22, 2),
     e_bucket_7                  NUMBER(22, 2),
     e_bucket_8                  NUMBER(22, 2),
     e_bucket_9                  NUMBER(22, 2),
     e_bucket_10                 NUMBER(22, 2),
     c_bucket_1                  NUMBER(22, 2),
     c_bucket_2                  NUMBER(22, 2),
     c_bucket_3                  NUMBER(22, 2),
     c_bucket_4                  NUMBER(22, 2),
     c_bucket_5                  NUMBER(22, 2),
     c_bucket_6                  NUMBER(22, 2),
     c_bucket_7                  NUMBER(22, 2),
     c_bucket_8                  NUMBER(22, 2),
     c_bucket_9                  NUMBER(22, 2),
     c_bucket_10                 NUMBER(22, 2),
     c_1_incl_initial_measure   NUMBER(1, 0),
     c_1_incl_xlate             VARCHAR2(254),
     c_2_incl_initial_measure   NUMBER(1, 0),
     c_2_incl_xlate             VARCHAR2(254),
     c_3_incl_initial_measure   NUMBER(1, 0),
     c_3_incl_xlate             VARCHAR2(254),
     c_4_incl_initial_measure   NUMBER(1, 0),
     c_4_incl_xlate             VARCHAR2(254),
     c_5_incl_initial_measure   NUMBER(1, 0),
     c_5_incl_xlate             VARCHAR2(254),
     c_6_incl_initial_measure   NUMBER(1, 0),
     c_6_incl_xlate             VARCHAR2(254),
     c_7_incl_initial_measure   NUMBER(1, 0),
     c_7_incl_xlate             VARCHAR2(254),
     c_8_incl_initial_measure   NUMBER(1, 0),
     c_8_incl_xlate             VARCHAR2(254),
     c_9_incl_initial_measure   NUMBER(1, 0),
     c_9_incl_xlate             VARCHAR2(254),
     c_10_incl_initial_measure  NUMBER(1, 0),
     c_10_incl_xlate            VARCHAR2(254),
     error_message              VARCHAR2(4000),
     time_stamp                 DATE NULL,
     user_id                    VARCHAR2(18) NULL
  );


COMMENT ON TABLE ls_import_ilr_rev IS '(S)  [06]
The LS Import ILR Revision table is a staging table used to import ILR Revisions.';
COMMENT ON COLUMN ls_import_ilr_rev.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_ilr_rev.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_ilr_rev.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_ilr_rev.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_ilr_rev.error_message IS 'System generated error message from validating line displaying what is wrong';
COMMENT ON COLUMN ls_import_ilr_rev.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN ls_import_ilr_rev.ilr_xlate IS 'Translation field for Internal ILR ID.';
COMMENT ON COLUMN ls_import_ilr_rev.discount_rate IS  'The interest rate used to determine interest payments.  Also used to determine net present value.';
COMMENT ON COLUMN ls_import_ilr_rev.payment_term_id IS 'ID for this ILR payment term.';
COMMENT ON COLUMN ls_import_ilr_rev.payment_term_date IS 'The payment term date for this ILR payment term.';
COMMENT ON COLUMN ls_import_ilr_rev.payment_term_type_id IS 'The payment term date for this ILR payment term. lookup on ls_payment_term_type';
COMMENT ON COLUMN ls_import_ilr_rev.payment_term_xlate IS 'Translation field for Payment Term Type.';
COMMENT ON COLUMN ls_import_ilr_rev.payment_freq_id IS 'The payment frequency on this ILR payment term. lookup on ls_payment_freq.';
COMMENT ON COLUMN ls_import_ilr_rev.payment_freq_xlate IS 'Translation field for Payment Frequency.';
COMMENT ON COLUMN ls_import_ilr_rev.number_of_terms IS 'Number of Payments within the Payment Term.';
COMMENT ON COLUMN ls_import_ilr_rev.paid_amount IS 'Payment amount for each Payment within the Payment Term.';
COMMENT ON COLUMN ls_import_ilr_rev.escalation IS 'Variable Escalation ID to be leveraged with the Payment.';
COMMENT ON COLUMN ls_import_ilr_rev.escalation_xlate IS 'Translation field for Escalation.';
COMMENT ON COLUMN ls_import_ilr_rev.escalation_freq_id IS 'Frequency of Escalation. lookup on ls_payment_freq';
COMMENT ON COLUMN ls_import_ilr_rev.escalation_freq_xlate IS 'Translation field for Escalation Frequency.';
COMMENT ON COLUMN ls_import_ilr_rev.escalation_pct IS 'Fixed percentage escalation for Payment Term.';
COMMENT ON COLUMN ls_import_ilr_rev.remeasurement_date IS 'Remeasurement Date for new Revision of ILR.';
COMMENT ON COLUMN ls_import_ilr_rev.lease_cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';
COMMENT ON COLUMN ls_import_ilr_rev.lease_cap_type_xlate IS 'Translation field for Lease Cap Type';
COMMENT ON COLUMN ls_import_ilr_rev.itc_sw IS 'A flag identifying whether this ILR has investment tax credits associate with it.';
COMMENT ON COLUMN ls_import_ilr_rev.itc_xlate IS 'Translation field for ITC';
COMMENT ON COLUMN ls_import_ilr_rev.cancelable_type_id IS 'ID representing whether or not a lease can be cancelled before the lease terms are up.';
COMMENT ON COLUMN ls_import_ilr_rev.cancelable_type_xlate IS 'Translation field for Cancelable Type.';
COMMENT ON COLUMN ls_import_ilr_rev.specialized_asset IS 'Flag for Specialized Asset for this ILR Revision.';
COMMENT ON COLUMN ls_import_ilr_rev.specialized_asset_xlate IS 'Translation field for Specialized Asset.';
COMMENT ON COLUMN ls_import_ilr_rev.intent_to_purch IS 'ID for Yes/No for Intent to Purchase';
COMMENT ON COLUMN ls_import_ilr_rev.intent_to_purch_xlate IS 'Translation field for Intent to Purchase.';
COMMENT ON COLUMN ls_import_ilr_rev.interco_company_id IS 'ID of Company ILR is associated with for Intercompany Lease.';
COMMENT ON COLUMN ls_import_ilr_rev.interco_company_xlate IS 'Translation field for Intercompany Company (lookup against company view).';
COMMENT ON COLUMN ls_import_ilr_rev.interco_lease IS 'ID for Yes/No for whether ILR is an Intercompany Lease.';
COMMENT ON COLUMN ls_import_ilr_rev.interco_lease_xlate IS 'Translation field for Intercompany Lease (yes/no)';
COMMENT ON COLUMN ls_import_ilr_rev.sublease_flag IS 'ID for Yes/No on whether ILR has sublease. ';
COMMENT ON COLUMN ls_import_ilr_rev.sublease_flag_xlate IS 'Translation field for Sublease Flag (yes/no).';
COMMENT ON COLUMN ls_import_ilr_rev.sublease_id IS 'ID of Sublease for ILR (lsr_lease)';
COMMENT ON COLUMN ls_import_ilr_rev.sublease_xlate IS 'Translation field for Sublease Lease ID from lsr_lease.';
COMMENT ON COLUMN ls_import_ilr_rev.depr_calc_method IS 'Yes/No for leveraging Units of Production for Depreciation Calc (0: Normal Lease Depreciation, 1: Units of Production.)';
COMMENT ON COLUMN ls_import_ilr_rev.depr_calc_method_xlate IS 'Translation field for Depreciation Calculation Method (yes/no)';
COMMENT ON COLUMN ls_import_ilr_rev.remeasure_calc_gain_loss IS 'Indicates whether Gain/Loss should be calculated upon remeasurement. 1 = yes 0 = no (default is yes)';
COMMENT ON COLUMN ls_import_ilr_rev.remeasure_calc_gain_xlate IS 'Translation field for Remeasure Calc Gain Loss (yes/no).';
COMMENT ON COLUMN ls_import_ilr_rev.default_rate_flag IS 'Indicates whether the ILR leverages Default Discount Rate logic.';
COMMENT ON COLUMN ls_import_ilr_rev.default_rate_flag_xlate IS 'Translation field for Default Rate Flag (yes/no).';
COMMENT ON COLUMN ls_import_ilr_rev.borrowing_curr_id IS 'ID of a currency marked as the Borrowing Currency for the Default Discount Rate to be leveraged';
COMMENT ON COLUMN ls_import_ilr_rev.borrowing_curr_xlate IS 'Translation field for Borrowing Currency.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_1 IS  'Payment Amount for Executory Bucket 1.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_2 IS  'Payment Amount for Executory Bucket 2.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_3 IS  'Payment Amount for Executory Bucket 3.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_4 IS  'Payment Amount for Executory Bucket 4.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_5 IS  'Payment Amount for Executory Bucket 5.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_6 IS  'Payment Amount for Executory Bucket 6.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_7 IS  'Payment Amount for Executory Bucket 7.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_8 IS  'Payment Amount for Executory Bucket 8.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_9 IS  'Payment Amount for Executory Bucket 9.';
COMMENT ON COLUMN ls_import_ilr_rev.e_bucket_10 IS 'Payment Amount for Executory Bucket 10.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_1 IS 'Payment Amount for Contingent Bucket 1.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_2 IS 'Payment Amount for Contingent Bucket 2.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_3 IS 'Payment Amount for Contingent Bucket 3.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_4 IS 'Payment Amount for Contingent Bucket 4.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_5 IS 'Payment Amount for Contingent Bucket 5.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_6 IS 'Payment Amount for Contingent Bucket 6.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_7 IS 'Payment Amount for Contingent Bucket 7.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_8 IS 'Payment Amount for Contingent Bucket 8.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_9 IS 'Payment Amount for Contingent Bucket 9.';
COMMENT ON COLUMN ls_import_ilr_rev.c_bucket_10 IS 'Payment Amount for Contingent Bucket 10.';
COMMENT ON COLUMN ls_import_ilr_rev.c_1_incl_initial_measure IS 'Include Contingent 1 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_2_incl_initial_measure IS 'Include Contingent 2 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_3_incl_initial_measure IS 'Include Contingent 3 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_4_incl_initial_measure IS 'Include Contingent 4 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_5_incl_initial_measure IS 'Include Contingent 5 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_6_incl_initial_measure IS 'Include Contingent 6 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_7_incl_initial_measure IS 'Include Contingent 7 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_8_incl_initial_measure IS 'Include Contingent 8 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_9_incl_initial_measure IS 'Include Contingent 9 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_10_incl_initial_measure IS'Include Contingent 10 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev.c_1_incl_xlate IS 'Translation field for Contingent 1 Include in Initial Measure.';
COMMENT ON COLUMN ls_import_ilr_rev.c_2_incl_xlate IS 'Translation field for Contingent 2 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_3_incl_xlate IS 'Translation field for Contingent 3 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_4_incl_xlate IS 'Translation field for Contingent 4 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_5_incl_xlate IS 'Translation field for Contingent 5 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_6_incl_xlate IS 'Translation field for Contingent 6 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_7_incl_xlate IS 'Translation field for Contingent 7 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_8_incl_xlate IS 'Translation field for Contingent 8 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_9_incl_xlate IS 'Translation field for Contingent 9 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev.c_10_incl_xlate IS 'Translation field for Contingent 10 Include in Initial Measure.';

CREATE TABLE ls_import_ilr_rev_arc
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     ilr_id                     NUMBER(22, 0),
     ilr_xlate                  VARCHAR2(254),
     discount_rate              NUMBER(22, 8),
     payment_term_id            NUMBER(22, 0),
     payment_term_date          VARCHAR2(254),
	 payment_term_type_id       NUMBER(22, 0),
     payment_term_xlate         VARCHAR2(254),
     payment_freq_id            NUMBER(22, 0),
     payment_freq_xlate         VARCHAR2(254),
     number_of_terms            NUMBER(22, 0),
     paid_amount                NUMBER(22, 2),
	 escalation					NUMBER(22, 0),
	 escalation_xlate			VARCHAR2(254),
	 escalation_freq_id			NUMBER(22, 0),
	 escalation_freq_xlate		VARCHAR2(254),
	 escalation_pct				NUMBER(22, 12),
     remeasurement_date         VARCHAR2(254),
     lease_cap_type_id          NUMBER(22, 0),
     lease_cap_type_xlate       VARCHAR2(254),
     itc_sw                     NUMBER(1, 0),
     itc_xlate                  VARCHAR2(254),
     cancelable_type_id         NUMBER(22, 0),
     cancelable_type_xlate      VARCHAR2(254),
     specialized_asset          NUMBER(1, 0),
     specialized_asset_xlate    VARCHAR2(254),
     intent_to_purch            NUMBER(1, 0),
     intent_to_purch_xlate      VARCHAR2(254),
     interco_company_id         NUMBER(22, 0),
     interco_company_xlate      VARCHAR2(254),
     interco_lease              NUMBER(1, 0),
     interco_lease_xlate        VARCHAR2(254),
     sublease_flag              NUMBER(1, 0),
     sublease_flag_xlate        VARCHAR2(254),
     sublease_id                NUMBER(22, 0),
     sublease_xlate             VARCHAR2(254),
     depr_calc_method           NUMBER(1, 0),
     depr_calc_method_xlate     VARCHAR2(254),
     remeasure_calc_gain_loss   NUMBER(1, 0),
     remeasure_calc_gain_xlate  VARCHAR2(254),
     default_rate_flag          NUMBER(1, 0),
     default_rate_flag_xlate    VARCHAR2(254),
     borrowing_curr_id          NUMBER(1, 0),
     borrowing_curr_xlate       VARCHAR2(254),
     e_bucket_1                  NUMBER(22, 2),
     e_bucket_2                  NUMBER(22, 2),
     e_bucket_3                  NUMBER(22, 2),
     e_bucket_4                  NUMBER(22, 2),
     e_bucket_5                  NUMBER(22, 2),
     e_bucket_6                  NUMBER(22, 2),
     e_bucket_7                  NUMBER(22, 2),
     e_bucket_8                  NUMBER(22, 2),
     e_bucket_9                  NUMBER(22, 2),
     e_bucket_10                 NUMBER(22, 2),
     c_bucket_1                  NUMBER(22, 2),
     c_bucket_2                  NUMBER(22, 2),
     c_bucket_3                  NUMBER(22, 2),
     c_bucket_4                  NUMBER(22, 2),
     c_bucket_5                  NUMBER(22, 2),
     c_bucket_6                  NUMBER(22, 2),
     c_bucket_7                  NUMBER(22, 2),
     c_bucket_8                  NUMBER(22, 2),
     c_bucket_9                  NUMBER(22, 2),
     c_bucket_10                 NUMBER(22, 2),
     c_1_incl_initial_measure   NUMBER(1, 0),
     c_1_incl_xlate             VARCHAR2(254),
     c_2_incl_initial_measure   NUMBER(1, 0),
     c_2_incl_xlate             VARCHAR2(254),
     c_3_incl_initial_measure   NUMBER(1, 0),
     c_3_incl_xlate             VARCHAR2(254),
     c_4_incl_initial_measure   NUMBER(1, 0),
     c_4_incl_xlate             VARCHAR2(254),
     c_5_incl_initial_measure   NUMBER(1, 0),
     c_5_incl_xlate             VARCHAR2(254),
     c_6_incl_initial_measure   NUMBER(1, 0),
     c_6_incl_xlate             VARCHAR2(254),
     c_7_incl_initial_measure   NUMBER(1, 0),
     c_7_incl_xlate             VARCHAR2(254),
     c_8_incl_initial_measure   NUMBER(1, 0),
     c_8_incl_xlate             VARCHAR2(254),
     c_9_incl_initial_measure   NUMBER(1, 0),
     c_9_incl_xlate             VARCHAR2(254),
     c_10_incl_initial_measure  NUMBER(1, 0),
     c_10_incl_xlate            VARCHAR2(254),
     error_message              VARCHAR2(4000),
     time_stamp                 DATE NULL,
     user_id                    VARCHAR2(18) NULL
  );
  
  
COMMENT ON TABLE ls_import_ilr_rev_arc IS '(S)  [06]
The LS Import ILR Revision Archive table holds prior successfull import ILR Revision runs.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.error_message IS 'System generated error message from validating line displaying what is wrong';
COMMENT ON COLUMN ls_import_ilr_rev_arc.ilr_id IS 'The internal ILR id within PowerPlant .';
COMMENT ON COLUMN ls_import_ilr_rev_arc.ilr_xlate IS 'Translation field for Internal ILR ID.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.discount_rate IS  'The interest rate used to determine interest payments.  Also used to determine net present value.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.payment_term_id IS 'ID for this ILR payment term.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.payment_term_date IS 'The payment term date for this ILR payment term.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.payment_term_type_id IS 'The payment term date for this ILR payment term. lookup on ls_payment_term_type';
COMMENT ON COLUMN ls_import_ilr_rev_arc.payment_term_xlate IS 'Translation field for Payment Term Type.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.payment_freq_id IS 'The payment frequency on this ILR payment term. lookup on ls_payment_freq.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.payment_freq_xlate IS 'Translation field for Payment Frequency.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.number_of_terms IS 'Number of Payments within the Payment Term.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.paid_amount IS 'Payment amount for each Payment within the Payment Term.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.escalation IS 'Variable Escalation ID to be leveraged with the Payment.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.escalation_xlate IS 'Translation field for Escalation.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.escalation_freq_id IS 'Frequency of Escalation. lookup on ls_payment_freq';
COMMENT ON COLUMN ls_import_ilr_rev_arc.escalation_freq_xlate IS 'Translation field for Escalation Frequency.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.escalation_pct IS 'Fixed percentage escalation for Payment Term.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.remeasurement_date IS 'Remeasurement Date for new Revision of ILR.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.lease_cap_type_id IS 'Identifies how a lease is mapped to cpr basis buckets';
COMMENT ON COLUMN ls_import_ilr_rev_arc.lease_cap_type_xlate IS 'Translation field for Lease Cap Type';
COMMENT ON COLUMN ls_import_ilr_rev_arc.itc_sw IS 'A flag identifying whether this ILR has investment tax credits associate with it.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.itc_xlate IS 'Translation field for ITC';
COMMENT ON COLUMN ls_import_ilr_rev_arc.cancelable_type_id IS 'ID representing whether or not a lease can be cancelled before the lease terms are up.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.cancelable_type_xlate IS 'Translation field for Cancelable Type.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.specialized_asset IS 'Flag for Specialized Asset for this ILR Revision.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.specialized_asset_xlate IS 'Translation field for Specialized Asset.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.intent_to_purch IS 'ID for Yes/No for Intent to Purchase';
COMMENT ON COLUMN ls_import_ilr_rev_arc.intent_to_purch_xlate IS 'Translation field for Intent to Purchase.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.interco_company_id IS 'ID of Company ILR is associated with for Intercompany Lease.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.interco_company_xlate IS 'Translation field for Intercompany Company (lookup against company view).';
COMMENT ON COLUMN ls_import_ilr_rev_arc.interco_lease IS 'ID for Yes/No for whether ILR is an Intercompany Lease.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.interco_lease_xlate IS 'Translation field for Intercompany Lease (yes/no)';
COMMENT ON COLUMN ls_import_ilr_rev_arc.sublease_flag IS 'ID for Yes/No on whether ILR has sublease. ';
COMMENT ON COLUMN ls_import_ilr_rev_arc.sublease_flag_xlate IS 'Translation field for Sublease Flag (yes/no).';
COMMENT ON COLUMN ls_import_ilr_rev_arc.sublease_id IS 'ID of Sublease for ILR (lsr_lease)';
COMMENT ON COLUMN ls_import_ilr_rev_arc.sublease_xlate IS 'Translation field for Sublease Lease ID from lsr_lease.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.depr_calc_method IS 'Yes/No for leveraging Units of Production for Depreciation Calc (0: Normal Lease Depreciation, 1: Units of Production.)';
COMMENT ON COLUMN ls_import_ilr_rev_arc.depr_calc_method_xlate IS 'Translation field for Depreciation Calculation Method (yes/no)';
COMMENT ON COLUMN ls_import_ilr_rev_arc.remeasure_calc_gain_loss IS 'Indicates whether Gain/Loss should be calculated upon remeasurement. 1 = yes 0 = no (default is yes)';
COMMENT ON COLUMN ls_import_ilr_rev_arc.remeasure_calc_gain_xlate IS 'Translation field for Remeasure Calc Gain Loss (yes/no).';
COMMENT ON COLUMN ls_import_ilr_rev_arc.default_rate_flag IS 'Indicates whether the ILR leverages Default Discount Rate logic.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.default_rate_flag_xlate IS 'Translation field for Default Rate Flag (yes/no).';
COMMENT ON COLUMN ls_import_ilr_rev_arc.borrowing_curr_id IS 'ID of a currency marked as the Borrowing Currency for the Default Discount Rate to be leveraged';
COMMENT ON COLUMN ls_import_ilr_rev_arc.borrowing_curr_xlate IS 'Translation field for Borrowing Currency.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_1 IS  'Payment Amount for Executory Bucket 1.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_2 IS  'Payment Amount for Executory Bucket 2.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_3 IS  'Payment Amount for Executory Bucket 3.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_4 IS  'Payment Amount for Executory Bucket 4.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_5 IS  'Payment Amount for Executory Bucket 5.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_6 IS  'Payment Amount for Executory Bucket 6.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_7 IS  'Payment Amount for Executory Bucket 7.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_8 IS  'Payment Amount for Executory Bucket 8.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_9 IS  'Payment Amount for Executory Bucket 9.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.e_bucket_10 IS 'Payment Amount for Executory Bucket 10.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_1 IS 'Payment Amount for Contingent Bucket 1.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_2 IS 'Payment Amount for Contingent Bucket 2.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_3 IS 'Payment Amount for Contingent Bucket 3.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_4 IS 'Payment Amount for Contingent Bucket 4.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_5 IS 'Payment Amount for Contingent Bucket 5.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_6 IS 'Payment Amount for Contingent Bucket 6.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_7 IS 'Payment Amount for Contingent Bucket 7.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_8 IS 'Payment Amount for Contingent Bucket 8.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_9 IS 'Payment Amount for Contingent Bucket 9.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_bucket_10 IS 'Payment Amount for Contingent Bucket 10.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_1_incl_initial_measure IS 'Include Contingent 1 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_2_incl_initial_measure IS 'Include Contingent 2 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_3_incl_initial_measure IS 'Include Contingent 3 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_4_incl_initial_measure IS 'Include Contingent 4 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_5_incl_initial_measure IS 'Include Contingent 5 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_6_incl_initial_measure IS 'Include Contingent 6 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_7_incl_initial_measure IS 'Include Contingent 7 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_8_incl_initial_measure IS 'Include Contingent 8 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_9_incl_initial_measure IS 'Include Contingent 9 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_10_incl_initial_measure IS'Include Contingent 10 amount in initial schedule measurement for all Set of Books';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_1_incl_xlate IS 'Translation field for Contingent 1 Include in Initial Measure.';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_2_incl_xlate IS 'Translation field for Contingent 2 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_3_incl_xlate IS 'Translation field for Contingent 3 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_4_incl_xlate IS 'Translation field for Contingent 4 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_5_incl_xlate IS 'Translation field for Contingent 5 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_6_incl_xlate IS 'Translation field for Contingent 6 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_7_incl_xlate IS 'Translation field for Contingent 7 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_8_incl_xlate IS 'Translation field for Contingent 8 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_9_incl_xlate IS 'Translation field for Contingent 9 Include in Initial Measure';
COMMENT ON COLUMN ls_import_ilr_rev_arc.c_10_incl_xlate IS 'Translation field for Contingent 10 Include in Initial Measure.';

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15803, 0, 2018, 2, 0, 0, 52346, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052346_lessee_01_import_ilr_rev_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;