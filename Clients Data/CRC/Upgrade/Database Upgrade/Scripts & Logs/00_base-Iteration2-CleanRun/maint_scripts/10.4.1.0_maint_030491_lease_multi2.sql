/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi2.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/13/2013 Brandon Beck   Point release
||============================================================================
*/

-- alerts
insert into PP_VERIFY_CATEGORY_DESCRIPTION
   (VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID, DESCRIPTION, PRIORITY_ID, SUBSYSTEM)
   select max(VERIFY_CATEGORY_ID) + 1, sysdate, user, 'Lessee Alerts', 1, null /*11*/
     from PP_VERIFY_CATEGORY_DESCRIPTION;

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Leased assets retiring in the next 3 months', 'Leased assets retiring in the next 3 months',
  'Review', 'Critical', null, 1, 11, 'select la.leased_asset_number
from ls_asset la, work_order_control woc
where woc.work_order_id = la.work_order_id
and trunc(woc.out_of_service_date) = trunc(add_months(sysdate, 3))',
  null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Leased asset number <col1> is retiring in 3 months.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Leased assets retiring in the next 3 months'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Leased assets retiring next month', 'Leased assets retiring next month',
  'Review', 'Critical', null, 1, 11, 'select la.leased_asset_number
from ls_asset la, work_order_control woc
where woc.work_order_id = la.work_order_id
and trunc(woc.out_of_service_date) = trunc(add_months(sysdate, 1))',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Leased asset number <col1> is retiring in 1 month.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Leased assets retiring next month'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Leases awaiting approval (over 1 week old)', 'Leases awaiting approval (over 1 week old)',
  'Review', 'Critical', null, 1, 11, 'select distinct wfd.users, mla.lease_number, wf.id_field1 lease_id
from workflow_detail wfd, workflow wf, ls_lease mla
where wfd.workflow_id = wf.workflow_id
and wf.id_field1 = to_char(mla.lease_id)
and wf.approval_status_id = 2
and wfd.approval_status_id = 2
and wf.date_sent < sysdate - 7
and wf.subsystem = ''mla_approval''',
   null, 1, 'select <col1> from dual', 'Lease number <col2> has been awaiting your approval for over a week.  Please approve or reject lease <col2>.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Leases awaiting approval (over 1 week old)'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'ILRs awaiting approval (over 1 week old)', 'ILRs awaiting approval (over 1 week old)',
  'Review', 'Critical', null, 1, 11, 'select distinct wfd.users, ilr.ilr_number, wf.id_field1 ilr_id
from workflow_detail wfd, workflow wf, ls_ilr ilr
where wfd.workflow_id = wf.workflow_id
and wf.id_field1 = to_char(ilr.ilr_id)
and wf.approval_status_id = 2
and wfd.approval_status_id = 2
and wf.date_sent < sysdate - 7
and wf.subsystem = ''ilr_approval''',
   null, 1, 'select <col1> from dual', 'ILR number <col2> has been awaiting your approval for over a week.  Please approve or reject ILR <col2>.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'ILRs awaiting approval (over 1 week old)'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'ILR payment term ending soon (3 months)', 'ILR payment term ending soon (3 months)',
  'Review', 'Critical', null, 1, 11, 'select li.ilr_id, li.ilr_number, pt.revision, pt.payment_term_date start_date,
  add_months(pt.payment_term_date, pt.number_of_terms * decode(pt.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1, 5, 1) - 1) end_date,
  to_char(add_months(pt.payment_term_date, pt.number_of_terms * decode(pt.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1, 5, 1) - 1), ''Month'') end_month
from ls_ilr li, ls_ilr_payment_term pt, ls_ilr_approval appr
where li.ilr_id = pt.ilr_id
and appr.ilr_id = pt.ilr_id
and appr.revision = pt.revision
and appr.approval_status_id = 3
and add_months(pt.payment_term_date, pt.number_of_terms * decode(pt.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1, 5, 1) - 1) <= add_months(sysdate, 3)
and add_months(pt.payment_term_date, pt.number_of_terms * decode(pt.payment_freq_id, 1, 12, 2, 6, 3, 3, 4, 1, 5, 1) - 1) > sysdate',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'ILR <col2> is set to finish term payments in 3 months in <col6>.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'ILR payment term ending soon (3 months)'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Payment amount does not equal Invoice amount', 'Payment amount does not equal Invoice amount',
  'Review', 'Critical', null, 1, 11, 'select mla.lease_number, mla.description lease_description, i.invoice_id, p.payment_id, i.invoice_amount,
  p.amount payment_amount, to_char(i.gl_posting_mo_yr, ''Month'') invoice_month, to_char(p.gl_posting_mo_yr, ''Month'') payment_month
from ls_lease mla, ls_invoice i, ls_invoice_payment_map map, ls_payment_hdr p
where mla.lease_id = i.lease_id
and i.invoice_id = map.invoice_id
and p.payment_id = map.payment_id
and i.invoice_amount <> p.amount',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Lease <col1> had a payment of <col6> which does not match the invoice amount of <col5> in <col7>.  Please review payment and invoice for correctness.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Payment amount does not equal Invoice amount'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Payment header amount not equal to sum of payment line amounts', 'Payment header amount not equal to sum of payment line amounts',
  'Review', 'Critical', null, 1, 11, 'select ll.lease_id, ll.lease_number, ll.description lease_description, p.payment_id, p.description payment_description,
  to_char(p.gl_posting_mo_yr, ''Month'') payment_month, p.amount header_amount, sum(pl.amount) sum_line_amounts
from ls_lease ll, ls_payment_hdr p, ls_payment_line pl
where ll.lease_id = p.lease_id
and pl.payment_id = p.payment_id
group by ll.lease_id, ll.lease_number, ll.description, p.payment_id, p.description, p.gl_posting_mo_yr, p.amount
having p.amount <> sum(pl.amount);',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Payment <col5> made in <col6> on MLA <col2> has a payment amount $<col6> that does not equal the sum of its constituent payments $<col7>.  Please review the payments line by line and compare to the header.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Payment header amount not equal to sum of payment line amounts'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Invoice header amount not equal to sum of invoice line amounts', 'Invoice header amount not equal to sum of invoice line amounts',
  'Review', 'Critical', null, 1, 11, 'select ll.lease_id, ll.lease_number, ll.description lease_description, i.invoice_id,
  to_char(i.gl_posting_mo_yr, ''Month'') invoice_month, i.invoice_amount, sum(il.amount) sum_line_amounts
from ls_lease ll, ls_invoice i, ls_invoice_line il
where ll.lease_id = i.lease_id
and il.invoice_id = i.invoice_id
group by ll.lease_id, ll.lease_number, ll.description, i.invoice_id, i.gl_posting_mo_yr, i.invoice_amount
having i.invoice_amount <> sum(il.amount)',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Invoice ID <col4> for <col5> on MLA <col2> has a total amount $<col5> that does not equal the sum of its constituent invoices $<col6>.  Please review the invoices line by line and compare to the header.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Invoice header amount not equal to sum of invoice line amounts'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Payment amount different from same payment last month', 'Payment amount different from same payment last month',
  'Review', 'Critical', null, 1, 11, 'select mla.lease_number, to_char(curr.gl_posting_mo_yr, ''MONTH YYYY'') month, curr.payment_id,
  curr.amount current_amount, prev.amount previous_amount, curr.lease_id
from ls_payment_hdr curr, ls_payment_hdr prev, ls_lease mla
where curr.lease_id = prev.lease_id
and mla.lease_id = curr.lease_id
and prev.gl_posting_mo_yr = add_months(curr.gl_posting_mo_yr, -1)
and trunc(curr.gl_posting_mo_yr, ''MM'') = trunc(sysdate, ''DD'')
and curr.amount <> prev.amount',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'The payment on lease number <col1> for <col2> has a different amount than the same payment for the previous month. Please review the payments for lease number <col1>.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Payment amount different from same payment last month'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'MLAs in the inititated state (over 1 month old)', 'MLAs in the inititated state (over 1 month old)',
  'Review', 'Critical', null, 1, 11, 'select lease_number
from ls_lease
where lease_status_id = 1
and initiation_date < add_months(sysdate, -1)',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Lease number <col1> has been in the "Initiated" state for over a month.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'MLAs in the inititated state (over 1 month old)'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'ILRs in the initiated state (over 1 month old)', 'ILRs in the initiated state (over 1 month old)',
  'Review', 'Critical', null, 1, 11, 'select ilr_number
from ls_ilr
where ilr_status_id = 1
and time_stamp < add_months(sysdate, -1)',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'ILR number <col1> has been in the "Initiated" state for over a month.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'ILRs in the initiated state (over 1 month old)'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'MLAs with multiple revisions in initiated state', 'MLAs with multiple revisions in initiated state',
  'Review', 'Critical', null, 1, 11, 'select mla.lease_number, mla.lease_id, count(1) init_revision_count
from ls_lease_approval appr, ls_lease mla
where appr.lease_id = mla.lease_id
and appr.approval_status_id = 1
group by mla.lease_number, mla.lease_id
having count(1) > 1',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'MLA number <col1> has <col3> revisions in the "Initiated" state.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'MLAs with multiple revisions in initiated state'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'ILRs with multiple revisions in initiated state', 'ILRs with multiple revisions in initiated state',
  'Review', 'Critical', null, 1, 11, 'select ilr.ilr_number, ilr.ilr_id, count(1) init_revision_count
from ls_ilr_approval appr, ls_ilr ilr
where appr.ilr_id = ilr.ilr_id
and appr.approval_status_id = 1
group by ilr.ilr_number, ilr.ilr_id
having count(1) > 1',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'ILR number <col1> has <col3> revisions in the "Initiated" state.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'ILRs with multiple revisions in initiated state'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'ILRs with negative payment terms', 'ILRs with negative payment terms',
  'Review', 'Critical', null, 1, 11, 'select ilr.ilr_number
from ls_ilr_payment_term term, ls_ilr ilr
where term.ilr_id = ilr.ilr_id
and (
  term.est_executory_cost < 0
  or term.paid_amount < 0
  or term.contingent_amount < 0
  or term.c_bucket_1 < 0
  or term.c_bucket_2 < 0
  or term.c_bucket_3 < 0
  or term.c_bucket_4 < 0
  or term.c_bucket_5 < 0
  or term.c_bucket_6 < 0
  or term.c_bucket_7 < 0
  or term.c_bucket_8 < 0
  or term.c_bucket_9 < 0
  or term.c_bucket_10 < 0
  or term.c_bucket_11 < 0
  or term.c_bucket_12 < 0
  or term.c_bucket_13 < 0
  or term.c_bucket_14 < 0
  or term.c_bucket_15 < 0
  or term.c_bucket_16 < 0
  or term.c_bucket_17 < 0
  or term.c_bucket_18 < 0
  or term.c_bucket_19 < 0
  or term.c_bucket_20 < 0
)
group by ilr.ilr_number',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'ILR number <col1> has payment terms with negative dollar values.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'ILRs with negative payment terms'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'MLAs with multiple required approvers that have not been approved by all parties (1 or more has approved)', 'MLAs with multiple required approvers that have not been approved by all parties (1 or more has approved)',
  'Review', 'Critical', null, 1, 11, 'select mla.lease_number, wd.num_required, count(1) approved_count
from workflow_detail wd, workflow wf, ls_lease mla
where wf.workflow_id = wd.workflow_id
and to_char(mla.lease_id) = wf.id_field1
and to_char(mla.current_revision) = wf.id_field2
and wd.approval_status_id = 3
group by mla.lease_number, wf.workflow_id, wd.num_required
having wd.num_required > count(1)',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Lease number <col1> has been approved by <col3> out of <col2> required approvers.  Please notify remaining approvers of pending MLA approval.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'MLAs with multiple required approvers that have not been approved by all parties (1 or more has approved)'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Ending value of obligation is not $0 or the guaranteed residual value', 'Ending value of obligation is not $0 or the guaranteed residual value',
  'Review', 'Critical', null, 1, 11, 'select la.leased_asset_number, sch.end_obligation, (sch.residual_amount + sch.term_penalty + sch.bpo_price), sch.residual_amount, sch.term_penalty, sch.bpo_price
from ls_asset_schedule sch, ls_asset la, ls_ilr ilr
where la.ls_asset_id = sch.ls_asset_id
and ilr.ilr_id = la.ilr_id
and sch.revision = ilr.current_revision
and sch.month = (select max(b.month) from ls_asset_schedule b where b.ls_asset_id = sch.ls_asset_id and revision = sch.revision)
and sch.end_obligation <> sch.residual_amount + sch.term_penalty + sch.bpo_price',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Leased asset <col1> has an ending obligation $<col2> that does not equal the sum of the residual amount, termination penalty, and bargain purchase option price of $<col3>. Please review the ending obligation, residual amount, termination penalty, and bargain purchase option price for correctness.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Ending value of obligation is not $0 or the guaranteed residual value'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Leased assets still in existence past estimated completion date', 'Leased assets still in existence past estimated completion date',
  'Review', 'Critical', null, 1, 11, 'select la.leased_asset_number, to_char(woc.est_complete_date, ''DD-Mon-YYYY'') est_complete_date
from cpr_ledger cpr, ls_cpr_asset_map map, ls_asset la, work_order_control woc
where map.asset_id = cpr.asset_id
and la.ls_asset_id = map.ls_asset_id
and woc.work_order_id = la.work_order_id
and cpr.accum_cost <> 0
and cpr.accum_quantity <> 0
and woc.est_complete_date < sysdate',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Leased asset <col1> still exists though it is past its estimated completion date of <col2>', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Leased assets still in existence past estimated completion date'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'In-service ILRs with no attached leased assets', 'In-service ILRs with no attached leased assets',
  'Review', 'Critical', null, 1, 11, 'select ilr.ilr_number
from ls_ilr ilr
where ilr.ilr_status_id in (4,5)
and not exists (
  select 1
  from ls_ilr_asset_map imap
  where imap.ilr_id = ilr.ilr_id
)',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'ILR <col1> is in service, but has no attached leased assets.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'In-service ILRs with no attached leased assets'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'In-service MLAs with no associated ILRs', 'In-service MLAs with no associated ILRs',
  'Review', 'Critical', null, 1, 11, 'select mla.lease_number
from ls_lease mla
where mla.lease_status_id in (3,4)
and not exists (
  select 1
  from ls_ilr ilr
  where ilr.lease_id = mla.lease_id
)',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'MLA <col1> is in service, but has no associated ILRs.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'In-service MLAs with no associated ILRs'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'In-service leased assets associated to old revision of their ILR', 'In-service leased assets associated to old revision of their ILR',
  'Review', 'Critical', null, 1, 11, 'select la.leased_asset_number, la.description asset_description, liam.revision asset_revision, li.ilr_number ilr_number, li.current_revision ilr_revision, ll.lease_number, ll.description lease_description
from ls_asset la, (select ls_asset_id, ilr_id, max(revision) revision from ls_ilr_asset_map group by ls_asset_id, ilr_id) liam, ls_ilr li, ls_lease ll
where la.ls_asset_id = liam.ls_asset_id
  and liam.ilr_id = li.ilr_id
  and li.lease_id = ll.lease_id
  and exists (select 1 from ls_cpr_asset_map lcam where lcam.ls_asset_id = la.ls_asset_id)
  and liam.revision < li.current_revision',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Leased Asset <col1>: <col2> does not have the same revision as Individual Lease Record <col4>.  Leased Asset <col1> is associated with revision <col3>, ILR <col4> is in revision <col5>', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'In-service leased assets associated to old revision of their ILR'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Monthly CPR Capital leased asset obligation is the same as previous months', 'Monthly CPR Capital leased asset obligation is the same as previous months',
  'Review', 'Critical', null, 1, 11, 'select ll.lease_number, ll.description mla_description, li.ilr_number, la.leased_asset_number, la.description asset_description, to_char(las.month, ''MM/DD/YYYY'') obligation_month, las.beg_obligation beginning_obligation
from ls_asset la, ls_asset_schedule las, ls_ilr_asset_map liam, ls_ilr li, ls_ilr_payment_term lipt, ls_lease ll, ls_cpr_asset_map lcam
where lcam.ls_asset_id = la.ls_asset_id
  and las.ls_asset_id = la.ls_asset_id
  and liam.ls_asset_id = la.ls_asset_id
  and liam.revision = las.revision
  and li.ilr_id = liam.ilr_id
  and li.current_revision = liam.revision
  and lipt.ilr_id = li.ilr_id
  and lipt.revision = las.revision
  and ll.lease_id = li.lease_id
  and ll.lease_cap_type_id = 1
  and lipt.payment_freq_id = 4
  and las.beg_obligation = (
    select las2.beg_obligation
    from ls_asset_schedule las2
    where las.ls_asset_id = las2.ls_asset_id
      and las.revision = las2.revision
      and las2.month = add_months(las.month,-1))',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Monthly CPR capital lease asset <col4>: <col5> has the same beginning obligation of <col7> in <col6> as the previous month.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Monthly CPR Capital leased asset obligation is the same as previous months'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Quarterly CPR Capital leased asset obligation is the same as previous quarters', 'Quarterly CPR Capital leased asset obligation is the same as previous quarters',
  'Review', 'Critical', null, 1, 11, 'select ll.lease_number, ll.description mla_description, li.ilr_number, la.leased_asset_number, la.description asset_description, to_char(las.month, ''MM/DD/YYYY'') obligation_month, las.beg_obligation beginning_obligation
from ls_asset la, ls_asset_schedule las, ls_ilr_asset_map liam, ls_ilr li, ls_ilr_payment_term lipt, ls_lease ll, ls_cpr_asset_map lcam
where lcam.ls_asset_id = la.ls_asset_id
  and las.ls_asset_id = la.ls_asset_id
  and liam.ls_asset_id = la.ls_asset_id
  and liam.revision = las.revision
  and li.ilr_id = liam.ilr_id
  and li.current_revision = liam.revision
  and lipt.ilr_id = li.ilr_id
  and lipt.revision = las.revision
  and ll.lease_id = li.lease_id
  and ll.lease_cap_type_id = 1
  and lipt.payment_freq_id = 3
  and las.beg_obligation = (
    select las2.beg_obligation
    from ls_asset_schedule las2
    where las.ls_asset_id = las2.ls_asset_id
      and las.revision = las2.revision
      and las2.month = add_months(las.month,-3))',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Quarterly CPR capital lease asset <col4>: <col5> has the same beginning obligation of <col7> in <col6> as the previous quarter.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Quarterly CPR Capital leased asset obligation is the same as previous quarters'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);

insert into PP_VERIFY (VERIFY_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION,
  ACTION_TO_TAKE, SEVERITY, DATAWINDOW, ENABLE_DISABLE, SUBSYSTEM, SQL,
  TOLERANCE, USER_ALERT, USER_SQL, ALERT_MESSAGE, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP,
  COMMENT_ALERT, COMMENT_REQUIRED, PRIMARY_KEY_FIELD, DISPLAY_ON_SELECT_TABS, SQL2,
  SQL3, SQL4, SQL5, ONE_COMMENT)
values ((select max(verify_id)+1 from pp_verify), sysdate, user, 'Yearly CPR Capital leased asset Obligation is the same as previous years', 'Yearly CPR Capital leased asset Obligation is the same as previous years',
  'Review', 'Critical', null, 1, 11, 'select ll.lease_number, ll.description mla_description, li.ilr_number, la.leased_asset_number, la.description asset_description, to_char(las.month, ''MM/DD/YYYY'') obligation_month, las.beg_obligation beginning_obligation
from ls_asset la, ls_asset_schedule las, ls_ilr_asset_map liam, ls_ilr li, ls_ilr_payment_term lipt, ls_lease ll, ls_cpr_asset_map lcam
where lcam.ls_asset_id = la.ls_asset_id
  and las.ls_asset_id = la.ls_asset_id
  and liam.ls_asset_id = la.ls_asset_id
  and liam.revision = las.revision
  and li.ilr_id = liam.ilr_id
  and li.current_revision = liam.revision
  and lipt.ilr_id = li.ilr_id
  and lipt.revision = las.revision
  and ll.lease_id = li.lease_id
  and ll.lease_cap_type_id = 1
  and lipt.payment_freq_id = 1
  and las.beg_obligation = (
    select las2.beg_obligation
    from ls_asset_schedule las2
    where las.ls_asset_id = las2.ls_asset_id
      and las.revision = las2.revision
      and las2.month = add_months(las.month,-12))',
   null, 1, 'select "USERS" from pp_security_users where upper("USERS") in ()', 'Yearly CPR capital lease asset <col4>: <col5> has the same beginning obligation of <col7> in <col6> as the previous year.', null, 0, 1,
  0, 0, null, null, null,
  null, null, null, 0);

insert into PP_VERIFY_CATEGORY (VERIFY_ID, VERIFY_CATEGORY_ID, TIME_STAMP, USER_ID)
values (
  (select VERIFY_ID from PP_VERIFY where DESCRIPTION = 'Yearly CPR Capital leased asset Obligation is the same as previous years'),
  (select VERIFY_CATEGORY_ID from PP_VERIFY_CATEGORY_DESCRIPTION where DESCRIPTION = 'Lessee Alerts'),
  sysdate, user
);


/*
update pp_verify set user_sql = 'select USERS from pp_security_users where upper(USERS) in (''PWRPLANT'')'
where user_sql = 'select "USERS" from pp_security_users where upper("USERS") in ()'
and exists (
  select 1
  from PP_VERIFY_CATEGORY
  where VERIFY_ID = PP_VERIFY.VERIFY_ID
  and VERIFY_CATEGORY_ID = (
    select VERIFY_CATEGORY_ID
    from PP_VERIFY_CATEGORY_DESCRIPTION
    where DESCRIPTION = 'Lessee Alerts'
  )
);
*/

/*
insert into PP_VERIFY_BATCH (COMPANY_ID, VERIFY_ID, MANUAL_BATCH, FREQUENCY, FREQUENCY_INFO, NEXT_NOTIFICATION, NEXT_RUN, LAST_RUN, TIME_STAMP, USER_ID, VERSION)
select 1, v.verify_id, 0, 1, null, null, sysdate, sysdate-1, sysdate, user, 0
from pp_verify v, pp_verify_category vc, pp_verify_category_description vcd
where vc.verify_id = v.verify_id
and vcd.verify_category_id = vc.verify_category_id
and vcd.description = 'Lessee Alerts';
*/

insert into workflow_type (WORKFLOW_TYPE_ID, DESCRIPTION, SUBSYSTEM, ACTIVE, USE_LIMITS, SQL_APPROVAL_AMOUNT, EXTERNAL_WORKFLOW_TYPE, BASE_SQL)
  values ((select max(workflow_type_id)+1 from workflow_type), 'Lease Payment Approval', 'ls_payment_approval', 1, 1,
  'select nvl(sum(amount),0) from LS_PAYMENT_HDR where payment_id = <<id_field1>>', null, 1);


alter table LS_PAYMENT_LINE drop constraint FK_LS_PAYMENT_LINE1;

alter table LS_PAYMENT_LINE
   add constraint FK_LS_PAYMENT_LINE1
       foreign key (PAYMENT_ID)
       references LS_PAYMENT_HDR(PAYMENT_ID)
       on delete cascade;

alter table LS_INVOICE_PAYMENT_MAP drop constraint FK_LS_INVOICE_PAYMENT_MAP2;

alter table LS_INVOICE_PAYMENT_MAP
   add constraint FK_LS_INVOICE_PAYMENT_MAP2
       foreign key (PAYMENT_ID)
       references LS_PAYMENT_HDR(PAYMENT_ID)
       on delete cascade;

insert into WORKFLOW_SUBSYSTEM
   (SUBSYSTEM, FIELD1_DESC, FIELD2_DESC, FIELD3_DESC, FIELD4_DESC, FIELD5_DESC, FIELD1_SQL,
    FIELD2_SQL, FIELD3_SQL, FIELD4_SQL, FIELD5_SQL, WORKFLOW_TYPE_FILTER, PENDING_NOTIFICATION_TYPE,
    APPROVED_NOTIFICATION_TYPE, REJECTED_NOTIFICATION_TYPE, SEND_SQL, APPROVE_SQL, REJECT_SQL,
    UNREJECT_SQL, UNSEND_SQL, UPDATE_WORKFLOW_TYPE_SQL, SEND_EMAILS, CASCADE_APPROVALS, USER_ID,
    TIME_STAMP)
values
   ('mla_approval',
    'MLA Number',
    'Revision',
    null,
    null,
    null,
    'select lease_number from ls_lease where lease_id = <<id_field1>>', '<<id_field2>>',
    null,
    null,
    null,
    null,
    'MLA APPROVAL MESSAGE', 'MLA APPROVED NOTICE MESSAGE', 'MLA REJECTION MESSAGE',
    'select PKG_LEASE_CALC.F_SEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
    'select PKG_LEASE_CALC.F_APPROVE_MLA(<<id_field1>>, <<id_field2>>) from dual',
    'select PKG_LEASE_CALC.F_REJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
    'select PKG_LEASE_CALC.F_UNREJECT_MLA(<<id_field1>>, <<id_field2>>) from dual',
    'select PKG_LEASE_CALC.F_UNSEND_MLA(<<id_field1>>, <<id_field2>>) from dual',
    'select PKG_LEASE_CALC.F_UPDATE_WORKFLOW_MLA(<<id_field1>>, <<id_field2>>) from dual', 1,
    null,
    null,
    null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (509, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
