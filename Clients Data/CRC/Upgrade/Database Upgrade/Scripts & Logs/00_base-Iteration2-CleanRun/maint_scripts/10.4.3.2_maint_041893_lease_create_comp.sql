/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Lessee
|| File Name:   maint_041893_lease_create_comp
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3 12/15/2014 		B.Beck				Add MLA statused
||==========================================================================================
*/

insert into ppbase_actions_windows
( id, module, action_identifier, action_text, action_order, action_event )
values
(	22, 'LESSEE', 'create_comp', 'Init Component', 8, 'ue_createComp' );

commit;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2152, 0, 10, 4, 3, 2, 041893, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041893_lease_create_comp.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;