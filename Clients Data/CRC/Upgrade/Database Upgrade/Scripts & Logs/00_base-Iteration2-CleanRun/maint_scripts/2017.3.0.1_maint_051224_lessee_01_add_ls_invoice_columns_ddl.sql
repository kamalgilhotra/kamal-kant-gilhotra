/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051224_lessee_01_add_ls_invoice_columns_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.1 5/10/2018 Crystal Yura       Add columns to use for payment recon interfaces
||============================================================================
*/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_invoice add paid_date date default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_invoice add external_value1 varchar2(254) default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_invoice add external_value2 varchar2(254) default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/



BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_invoice add paid_date date default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_invoice add external_value1 varchar2(254) default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_invoice add external_value2 varchar2(254) default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/



BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_invoice_archive add paid_date date default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_invoice_archive  add external_value1 varchar2(254) default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'alter table ls_import_invoice_archive  add external_value2 varchar2(254) default NULL';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -1430 THEN
         RAISE;
      END IF;
END;
/



COMMENT ON COLUMN ls_invoice.paid_date is 'Paid date of invoice, able to be used for integration purposes.';
COMMENT ON COLUMN ls_invoice.external_value1 is 'An external value to be used for integration purposes.';
COMMENT ON COLUMN ls_invoice.external_value2 is 'An external value to be used for integration purposes.';
COMMENT ON COLUMN ls_import_invoice.paid_date is 'Paid date of invoice, able to be used for integration purposes.';
COMMENT ON COLUMN ls_import_invoice.external_value1 is 'An external value to be used for integration purposes.';
COMMENT ON COLUMN ls_import_invoice.external_value2 is 'An external value to be used for integration purposes.';
COMMENT ON COLUMN ls_import_invoice_archive.paid_date is 'Paid date of invoice, able to be used for integration purposes.';
COMMENT ON COLUMN ls_import_invoice_archive.external_value1 is 'An external value to be used for integration purposes.';
COMMENT ON COLUMN ls_import_invoice_archive.external_value2 is 'An external value to be used for integration purposes.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5302, 0, 2017, 3, 0, 1, 51224, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051224_lessee_01_add_ls_invoice_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;