/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030751_lease_PKG_LEASE_SCHEDULE.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   06/21/2013 Brandon Beck   Point Release
|| 10.4.1.0   07/05/2013 Brandon Beck   Load schedule tables from the calc tables
||============================================================================
*/

create or replace package PKG_LEASE_SCHEDULE as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 06/21/2013 Brandon Beck   Added BPO and Term Penalty.
   ||                                    Also correct the end obligation for guaranteed residual
   ||============================================================================
   */

   -- Function to process the asset (allocate, find NBV)
   function F_PROCESS_ASSETS return varchar2;

   -- Loads the ilr_scheduling table for an entire LEASE
   function F_PROCESS_ILRS(A_LEASE_ID number) return varchar2;

   -- Loads the ilr_scheduling table for a single ILR
   function F_PROCESS_ILR(A_ILR_ID   number,
                          A_REVISION number) return varchar2;

   /*
   *  Loads from the calc staging table to the "real" tables
   */
   function F_SAVE_SCHEDULES return varchar2;
end PKG_LEASE_SCHEDULE;
/


create or replace package body PKG_LEASE_SCHEDULE as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: LEASE_SCHEDULE_PKG
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck    Original Version
   ||============================================================================
   */

   --**************************************************************************
   --                            Start Body
   --**************************************************************************
   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************
   --**************************************************************************
   --                            P_CHECK_RECALC_NPV
   -- Recalc NPV after determing IRR for any ILRs that have an IRR
   -- set the process_npv to be 0 where irr is null
   --**************************************************************************
   procedure P_CHECK_RECALC_NPV is
   begin
      -- do not recalc NPV if IRR was not found
      update ls_ilr_stg
      set process_npv = 0
      where irr is null
      ;

      -- do not recalc NPV if ANNUAL IRR not between 0 and 100
      update ls_ilr_stg
      set process_npv = 0, validation_message = 'IRR Calculation not between 0 and 100%'
      where process_npv = 1
      and 12 * irr not between 0 and 1
      ;

      update ls_ilr_schedule_stg s
      set process_npv = 0
      where exists
      (
         select 1
         from ls_ilr_stg l
         where l.ilr_id = s.ilr_id
         and l.process_npv = 0
      );

      -- set the rate to be the irr for the schedule
      update ls_ilr_schedule_stg s
      set rate =
      (
         select l.irr
         from ls_ilr_stg l
         where l.ilr_id = s.ilr_id
         and l.process_npv = 1
      )
      where exists
      (
         select 1
         from ls_ilr_stg l
         where l.ilr_id = s.ilr_id
         and l.process_npv = 1
      );
   end P_CHECK_RECALC_NPV;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   --**************************************************************************
   --                            F_ASSET_ALLOC_NPV
   -- This function will allocate the NPV of the schedule down to the assets
   --**************************************************************************
   function F_ASSET_ALLOC_NPV
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      insert into ls_ilr_asset_stg
      (
         id, ilr_id, revision, ls_asset_id,
         current_lease_cost, alloc_npv, residual_amount,
         residual_npv, npv_minus_residual_npv,
         term_penalty, bpo_price
      )
      with ls_asset_view as
      (
         select la.ilr_id, la.ls_asset_id, la.fmv as current_lease_cost, nvl(la.guaranteed_residual_amount, 0) as residual_amount,
            nvl(ratio_to_report(la.fmv) over(partition by la.ilr_id), 1) as pct_spread
         from ls_asset la, ls_ilr_stg ls
         where la.ilr_id = ls.ilr_id
      ),
      ls_months_view as
      (
         select z.ilr_id, z.the_length + 1 as the_length, z.rate
         from
         (
            select MONTHS_BETWEEN(max(month) over (partition by ilr_id), month) as the_length, id, month, ilr_id, rate
            from ls_ilr_schedule_stg
         ) z
         where z.id = 1
      )
      select row_number() over(partition by s.ilr_id order by l.current_lease_cost desc, l.ls_asset_id) as id,
         s.ilr_id, s.revision, l.ls_asset_id,
         l.current_lease_cost,
         round(s.npv * l.pct_spread, 2) +
            decode(row_number() over(partition by s.ilr_id order by l.current_lease_cost desc, l.ls_asset_id),
               1, s.npv - sum(round(s.npv * l.pct_spread, 2)) over(partition by s.ilr_id),
               0) as alloc_payment_with_plug,
         l.residual_amount, round(l.residual_amount / POWER(1 + m.rate, m.the_length - s.prepay_switch ), 2),
         round(s.npv * l.pct_spread, 2) +
            decode(row_number() over(partition by s.ilr_id order by l.current_lease_cost desc, l.ls_asset_id),
               1, s.npv - sum(round(s.npv * l.pct_spread, 2)) over(partition by s.ilr_id),
               0) - round(l.residual_amount / POWER(1 + m.rate, m.the_length - s.prepay_switch ), 2),
         round(s.term_penalty * l.pct_spread, 2) +
            decode(row_number() over(partition by s.ilr_id order by l.current_lease_cost desc, l.ls_asset_id),
               1, s.term_penalty - sum(round(s.term_penalty * l.pct_spread, 2)) over(partition by s.ilr_id),
               0),
         round(s.bpo_price * l.pct_spread, 2) +
            decode(row_number() over(partition by s.ilr_id order by l.current_lease_cost desc, l.ls_asset_id),
               1, s.bpo_price - sum(round(s.bpo_price * l.pct_spread, 2)) over(partition by s.ilr_id),
               0)
      from ls_ilr_stg s, ls_asset_view l, ls_months_view m
      where s.ilr_id = l.ilr_id
      and s.ilr_id = m.ilr_id;

      if sqlCode <> 0 then
         l_msg := substr('ERROR allocating NPV to assets: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;
      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_ASSET_ALLOC_NPV;

   --**************************************************************************
   --                            F_ALLOCATE_ASSET_PAYMENTS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease obligation
   --**************************************************************************
   function F_ALLOCATE_ASSET_PAYMENTS
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'Starting Allocation of payment to Assets';
      insert into ls_ilr_asset_schedule_stg
      (
         id,
         ilr_id, revision, ls_asset_id, month, prepay_switch, rate,
         payment_month, months_to_accrue, current_lease_cost,
         amount, residual_amount, beg_capital_cost, beg_obligation,
         term_penalty, bpo_price
      )
      with ls_asset_view as
      (
         select la.ilr_id, la.ls_asset_id, la.current_lease_cost, la.residual_amount as residual_amount, la.alloc_npv,
            ratio_to_report(la.npv_minus_residual_npv) over(partition by la.ilr_id) as pct_spread,
            la.term_penalty, la.bpo_price
         from ls_ilr_asset_stg la
      )
      select row_number() over(partition by s.ilr_id order by s.month, l.ls_asset_id) as id,
         s.ilr_id, s.revision, l.ls_asset_id, s.month, s.prepay_switch, s.rate,
         s.payment_month, s.months_to_accrue, l.current_lease_cost,
         round(s.amount * l.pct_spread, 2) +
            decode(row_number() over(partition by s.ilr_id, s.month order by s.rate desc, l.ls_asset_id),
               1, s.amount - sum(round(s.amount * l.pct_spread, 2)) over(partition by s.ilr_id, s.month),
               0) as alloc_payment_with_plug,
         decode(s.residual_amount, 0, 0, l.residual_amount) as residual, l.alloc_npv, l.alloc_npv,
         decode(s.term_penalty, 0, 0, l.term_penalty), decode(s.bpo_price, 0, 0, l.bpo_price)
      from ls_ilr_schedule_stg s, ls_asset_view l
      where s.ilr_id = l.ilr_id;

      if sqlCode <> 0 then
         l_msg := substr('ERROR allocating to assets: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

            -- make sure month with residual is set to 1
      l_status := 'Setting the payment month for residual amounts';
      update ls_ilr_asset_schedule_stg s
      set s.payment_month = 1
      where residual_amount <> 0
      ;

      if sqlCode <> 0 then
         l_msg := substr('ERROR setting residual month: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      -- update the last payment month to be a 2
      update ls_ilr_asset_schedule_stg s
      set s.payment_month = 2
      where (s.ls_asset_id, s.month) in
      (
         select a.ls_asset_id, max(a.month)
         from ls_ilr_asset_schedule_stg a
         where a.payment_month = 1
         and a.ilr_id = s.ilr_id
         group by a.ls_asset_id
      )
      ;

      if sqlCode <> 0 then
         l_msg := substr('ERROR setting last payment month: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_ALLOCATE_ASSET_PAYMENTS;

   --**************************************************************************
   --                            F_ALLOCATE_TO_ASSETS
   -- This function will allocate the payments to the assets
   -- Spreads the payments based on pct current lease cost
   --**************************************************************************
   function F_ALLOCATE_TO_ASSETS
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'Starting Allocation to Assets';
      -- allocate the NPV of the ILR to the ASSETS underneath
      l_msg := F_ASSET_ALLOC_NPV;
      if l_msg = 'OK' then

         -- Allocate the payments to the assets
         return F_ALLOCATE_ASSET_PAYMENTS;
      end if;

      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_ALLOCATE_TO_ASSETS;

   --**************************************************************************
   --                            F_CALC_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_CALC_ASSET_SCHEDULE
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'Undo prior asset schedule calculations';
      delete from ls_ilr_asset_schedule_calc_stg
      where ilr_id in
      (
         select a.ilr_id
         from ls_ilr_asset_schedule_stg a
      );
      l_status := 'Starting to build asset schedule';
      insert into ls_ilr_asset_schedule_calc_stg s
      (
         id, ilr_id, revision, ls_asset_id, month,
         amount, residual_amount,
         prepay_switch, payment_month,
         months_to_accrue, rate, npv,
         beg_capital_cost,
         end_capital_cost, beg_obligation,
         end_obligation,
         interest_accrual, principal_accrual,
         interest_paid, principal_paid,
         penny_rounder, penny_prin_rounder,
         penny_int_rounder, penny_end_rounder,
         principal_accrued, interest_accrued,
         prin_round_accrued, int_round_accrued,
         term_penalty, bpo_price
      )
      select
         id, ilr_id, revision, ls_asset_id, month,
         amt, res_amt,
         prepay_switch, payment_month,
         months_to_accrue, rate, npv,
         beg_capital_cost,
         end_capital_cost, beg_obligation,
         end_obligation,
         int_accrual, prin_accrual,
         int_paid, prin_paid,
         add_a_penny, add_a_penny_prin_accrual,
         add_a_penny_int_accrual, add_a_penny_end,
         prin_accrued, int_accrued,
         prin_accrued_round, int_accrued_round,
         term_penalty, bpo_price
      from ls_ilr_asset_schedule_stg
      model
      partition by (ilr_id, ls_asset_id)
      dimension by (row_number() over(partition by ilr_id, ls_asset_id order by month) as id)
      measures
      (
         beg_obligation, revision,
         amount as amt, residual_amount as res_amt,
         prepay_switch, payment_month, months_to_accrue,
         0 as add_a_penny, 0 as add_a_penny_end, 0 as add_a_penny_prin_accrual, 0 as add_a_penny_int_accrual,
         rate, beg_capital_cost, amount as amount,
         interest_accrual as int_accrual, 0 as int_accrued, interest_paid as int_paid,
         principal_accrual as prin_accrual, 0 as prin_accrued, principal_paid as prin_paid,
         end_capital_cost, end_obligation, month, npv, 0 as prin_accrued_round, 0 as int_accrued_round,
         MONTHS_BETWEEN(max(month) over (partition by ilr_id, ls_asset_id), month) as month_id,
         term_penalty, bpo_price
      )
      ignore nav
      rules upsert iterate(100000) until iteration_number = month_id[1]
      (
         -- if prepaid, the first months amount goes into the period zero accrual
         payment_month[0] = 1,
         prin_accrual[0] = decode(prepay_switch[ 1 ], 1, amount[1], 0),
         prin_accrued[0] = decode(prepay_switch[ 1 ], 1, amount[1], 0),
         int_accrual[0] = 0,
         int_accrued[0] = 0,
         prin_accrued_round[0] = decode(prepay_switch[ 1 ], 1, amount[1], 0),
         int_accrued_round[0] = 0,
   -- set the rate to be based on my number of months accrued (to account for monthly interest accrued during periods
   rate[ iteration_number + 1 ] = ( power( 1 + rate[ CV(id) ],  months_to_accrue[ CV(id) ] ) - 1 ) /  months_to_accrue[ CV(id) ],
         -- If this is the month after the payment, then accrued amount is equal to 0 if arrears or accrual amount of prior period if prepaid.
         --  All other months are equal to prior accrued amount + prior accrual
         int_accrued[ iteration_number + 1 ] = decode( payment_month[ CV(id) - 1 ], 0, int_accrued[ CV(id) - 1 ] + int_accrual[ CV(id) - 1 ],
                                             prepay_switch[ CV(id) ] * int_accrual[ CV(id) - 1 ]),
         prin_accrued[ iteration_number + 1 ] = decode( payment_month[ CV(id) - 1 ], 0, prin_accrued[ CV(id) - 1 ] + prin_accrual[ CV(id) - 1 ],
                                             prepay_switch[ CV(id) ] * prin_accrual[ CV(id) - 1 ]),
         int_accrued_round[ iteration_number + 1 ] = decode( payment_month[ CV(id) - 1 ], 0, int_accrued_round[ CV(id) - 1 ]
                                                                        + round( int_accrual[ CV(id) - 1 ], 2 ),
                                             prepay_switch[ CV(id) ] * round(int_accrual[ CV(id) - 1 ], 2) ),
         prin_accrued_round[ iteration_number + 1 ] = decode( payment_month[ CV(id) - 1 ], 0, prin_accrued_round[ CV(id) - 1 ]
                                                                        + round( prin_accrual[ CV(id) - 1 ], 2 ),
                                             prepay_switch[ CV(id) ] * round(prin_accrual[ CV(id) - 1 ], 2) ),

         -- if prepaid, the end balance is used to determine the accrual
         int_accrual[ iteration_number + 1 ] = rate[ CV(id) ]
                  * (beg_obligation[ CV(id) ] -
                     decode( payment_month[ CV(id) ], 0, 0, prepay_switch[ CV(id) ] * prin_accrued[ CV(id) ] ) ),
         prin_accrual[ iteration_number + 1 ] = amount[ CV(id) ] - int_accrual[ CV(id) ],
         -- for prepaid, the accrual from the prior period is used for the payment amount
         -- for prin, the first period will include the full first payment amount
         int_paid[ iteration_number + 1 ] =
            decode( payment_month[ CV(id) ], 0, 0, ( 1 - prepay_switch[ CV(id) ] ) * int_accrual[ CV(id) ] + int_accrued[ CV(id) ] ),
         prin_paid[ iteration_number + 1 ] =
            decode( payment_month[ CV(id) ], 0, 0, ( 1 - prepay_switch[ CV(id) ] ) * prin_accrual[ CV(id) ] + prin_accrued[ CV(id) ] ),

         -- after setting the paid amount, set the accrued amounts to be zero
         prin_accrued[ iteration_number + 1 ] = decode( payment_month[ CV(id) ], 0, prin_accrued[ CV(id) ], 0),
         int_accrued[ iteration_number + 1 ] = decode( payment_month[ CV(id) ], 0, int_accrued[ CV(id) ], 0),

         -- TRUEUP rounding that may come into play around accrued balanced <> paid balances
         -- take the prior months accrued rounding.  And add the accrual (if arrears).  Else do not add anything
         add_a_penny_prin_accrual[ iteration_number + 1 ] = decode( payment_month[ CV(id) ], 0, 0,
                                                decode( CV(id), 1, 0,
                                                   round(prin_paid[ CV(id) ], 2) - prin_accrued_round[ CV(id) ]
                                                                           - (1-prepay_switch[ CV(id) ] )
                                                                           * round(prin_accrual[ CV(id) ], 2) ) ),
         add_a_penny_int_accrual[ iteration_number + 1 ] = decode( payment_month[ CV(id) ], 0, 0,
                                                decode( CV(id), 1, 0,
                                                   round(int_paid[ CV(id) ], 2) - int_accrued_round[ CV(id) ] - (1-prepay_switch[ CV(id) ] )
                                                                           * round(int_accrual[ CV(id) ], 2) ) ),

         -- trueup principal accrual for rounding between periods for arrears
         prin_accrual[ iteration_number + 1 ] = prin_accrual[ CV(id) ] + ( ( 1 - prepay_switch[ CV(id) ] ) * add_a_penny_prin_accrual[ CV(id) ] ),
         int_accrual[ iteration_number + 1 ] = int_accrual[ CV(id) ] + ( ( 1 - prepay_switch[ CV(id) ] ) * add_a_penny_int_accrual[ CV(id) ] ),

         -- prepaids update prior month accrual.  And current month accrued
         prin_accrual[ iteration_number ] = prin_accrual[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny_prin_accrual[ CV(id) + 1 ] ),
         prin_accrued[ iteration_number + 1 ] = prin_accrued[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny_prin_accrual[ CV(id) ] ),
         int_accrual[ iteration_number ] = int_accrual[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny_int_accrual[ CV(id) + 1 ] ),
         int_accrued[ iteration_number + 1 ] = int_accrued[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny_int_accrual[ CV(id) ] ),

         -- end capital cost and obligation
         end_capital_cost[ iteration_number + 1 ] = beg_capital_cost[ CV(id) ],
         end_obligation[ iteration_number + 1 ] = beg_obligation[ CV(id) ] - prin_paid[ CV(id) ],

         -- check if a penny needs to be added to principal and subtracted from interest
         -- if the last month.  Make sure end obligation is zero
         add_a_penny[ iteration_number + 1 ] = round(beg_obligation[ CV(id) ], 2) - round(prin_paid[ CV(id) ], 2) - round(end_obligation[ CV(id) ], 2),

         -- apply from current month accrual if arrears
         int_accrual[ iteration_number + 1 ] = int_accrual[ CV(id) ] - ( ( 1 - prepay_switch[ CV(id) ] ) * add_a_penny[ CV(id) ] ),
         prin_accrual[ iteration_number + 1 ] = prin_accrual[ CV(id) ] + ( ( 1 - prepay_switch[ CV(id) ] ) * add_a_penny[ CV(id) ] ),

         -- for prepaid, apply to prior period accrual and current month accrued
         int_accrual[ iteration_number ] = int_accrual[ CV(id) ] - ( prepay_switch[ CV(id) ] * add_a_penny[ CV(id) + 1 ] ),
         prin_accrual[ iteration_number ] = prin_accrual[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny[ CV(id) + 1 ] ),
         int_accrued[ iteration_number + 1 ] = int_accrued[ CV(id) ] - ( prepay_switch[ CV(id) ] * add_a_penny[ CV(id) ] ),
         prin_accrued[ iteration_number + 1 ] = prin_accrued[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny[ CV(id) ] ),

         -- Apply to current period payment
         int_paid[ iteration_number + 1 ] = int_paid[ CV(id) ] - add_a_penny[ CV(id) ],
         prin_paid[ iteration_number + 1 ] = prin_paid[ CV(id) ] + add_a_penny[ CV(id) ],

         -- in the month where end obligation should go to zero (payment_month = 2).
         -- MAKE sure this happens by applying rounding
         -- check beginning balance minus payment minus res - bpo - termination
         add_a_penny_end[ iteration_number + 1 ] =
            decode(payment_month[ CV(id) ],
               2, round( beg_obligation[ CV(id) ], 2) - round(prin_paid[ CV(id) ], 2) - res_amt[ CV(id) ] - bpo_price[ CV(id) ] - term_penalty[ CV(id) ],
               0),

         -- if arrears, then odify accrual for current month
         int_accrual[ iteration_number + 1 ] = int_accrual[ CV(id) ] - ( ( 1 - prepay_switch[ CV(id) ] ) * add_a_penny_end[ CV(id) ] ),
         prin_accrual[ iteration_number + 1 ] = prin_accrual[ CV(id) ] + ( ( 1 - prepay_switch[ CV(id) ] ) * add_a_penny_end[ CV(id) ] ),

         -- for prepaid, apply to prior period accrual and current month accrued
         int_accrual[ iteration_number ] = int_accrual[ CV(id) ] - ( prepay_switch[ CV(id) ] * add_a_penny_end[ CV(id) + 1 ] ),
         prin_accrual[ iteration_number ] = prin_accrual[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny_end[ CV(id) + 1 ] ),
         int_accrued[ iteration_number + 1 ] = int_accrued[ CV(id) ] - ( prepay_switch[ CV(id) ] * add_a_penny_end[ CV(id) ] ),
         prin_accrued[ iteration_number + 1 ] = prin_accrued[ CV(id) ] + ( prepay_switch[ CV(id) ] * add_a_penny_end[ CV(id) ] ),

         -- Apply to current period payment
         int_paid[ iteration_number + 1 ] = int_paid[ CV(id) ] - add_a_penny_end[ CV(id) ],
         prin_paid[ iteration_number + 1 ] = prin_paid[ CV(id) ] + add_a_penny_end[ CV(id) ],

         -- set end obligation to be residual + bpo + termination if last month.
         end_obligation[ iteration_number + 1 ] = decode( payment_month[ CV(id) ], 2, res_amt[ CV(id) ] + bpo_price[ CV(id) ] + term_penalty[ CV(id) ], end_obligation[ CV(id) ]),

         beg_obligation[ iteration_number + 2 ] = end_obligation[ CV(id) - 1 ],

         -- if last payment month and it is a prepaid, then set the accrual to be the prepaid payment amount (stored in prin_accrued for period 0)
         prin_accrual[ iteration_number + 1 ] = decode(payment_month[ CV(id) ], 2,
                                             decode( prepay_switch[ CV(id) ], 1, prin_accrued[0], prin_accrual[ CV(id) ] ),
                                             prin_accrual[ CV(id) ] ),
         int_accrual[ iteration_number + 1 ] = decode(payment_month[ CV(id) ], 2,
                                             decode( prepay_switch[ CV(id) ], 1, 0, int_accrual[ CV(id) ] ),
                                             int_accrual[ CV(id) ] )
      )
      ;

      if sqlCode <> 0 then
         l_msg := substr('ERROR building asset schedule: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_CALC_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_LOAD_ASSET_SCHEDULE
   -- This function will Build the asset schedule out
   --**************************************************************************
   function F_LOAD_ASSET_SCHEDULE
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'Starting to load asset schedule';

      merge into ls_ilr_asset_schedule_stg s using
      (
         select *
         from ls_ilr_asset_schedule_calc_stg
      ) b
      on (b.ilr_id = s.ilr_id and b.ls_asset_id = s.ls_asset_id and b.month = s.month)
      when matched then update
      set s.end_capital_cost = round(b.end_capital_cost, 2),
         s.beg_obligation = round(b.beg_obligation, 2),
         s.end_obligation = round(b.end_obligation, 2),
         s.interest_accrual = round(b.interest_accrual, 2),
         s.principal_accrual = round(b.principal_accrual, 2),
         s.interest_paid = round(b.interest_paid, 2),
         s.principal_paid = round(b.principal_paid, 2)
      ;

      if sqlCode <> 0 then
         l_msg := substr('ERROR loading asset schedule: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_LOAD_ASSET_SCHEDULE;

   --**************************************************************************
   --                            F_PROCESS_ASSETS
   -- This function will process the assets under the ILRS
   -- IT will allocate the payments to the assets
   -- then calculate NBV for the asset
   --**************************************************************************
   function F_PROCESS_ASSETS
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'CALLING F_ALLOCATE_TO_ASSETS';
      l_msg := F_ALLOCATE_TO_ASSETS;
      if l_msg = 'OK' then
         l_status := 'CALLING F_CALC_ASSET_SCHEDULE';
         l_msg := F_CALC_ASSET_SCHEDULE;
         if l_msg = 'OK' then
            l_status := 'CALLING F_LOAD_ASSET_SCHEDULE';
            l_msg := F_LOAD_ASSET_SCHEDULE;
            return l_msg;
         end if;
      end if;
      return l_msg;
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_PROCESS_ASSETS;

   --**************************************************************************
   --                            F_CALC_IRR
   -- This function will calculate the internal rate of return
   -- For ILRs where NPV > FMV (net present value is greater than fair market value)
   --**************************************************************************
   function F_CALC_IRR
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'Starting IRR Calculation';
      merge into ls_ilr_stg s using
      (
         select ilr_id, 1 / x - 1 as the_irr
         from
         (
            select *
            from
            (
               select ilr_id, month, sum(amount) as amount
               from
               (
                  select l.ilr_id, s.month, s.amount + s.residual_amount + s.bpo_price + s.term_penalty as amount
                  from ls_ilr_schedule_stg s, ls_ilr_stg l
                  where l.ilr_id = s.ilr_id
                  and l.npv > l.fmv
                  union all
                  select l.ilr_id, min(add_months(s.month, l.prepay_switch - 1)), -1 * l.fmv
                  from ls_ilr_schedule_stg s, ls_ilr_stg l
                  where l.ilr_id = s.ilr_id
                  and l.npv > l.fmv
                  group by l.ilr_id, l.fmv
               )
               group by ilr_id, month
               order by 1, 2
            )
            model
            partition by (ilr_id)
            dimension by (row_number() over (partition by ilr_id order by month) as the_row)
            measures(MONTHS_BETWEEN(month, first_value(month) over (partition by ilr_id order by month)) as month, amount s,
                     0 ss, 0 f_a, 0 f_b, 0 f_x, 0 a, 1 b, 0 x, 0 iter)
            rules iterate(10000) until (abs(f_x[1]) < power(10, -20))
            (
               ss[any] = s[CV()] * power(a[1], month[CV()]),
               f_a[1] = sum(ss)[any],
               ss[any] = s[CV()] * power(b[1], month[CV()]),
               f_b[1] = sum(ss)[any],
               x[1] = a[1] - f_a[1] * ( b[1] - a[1] ) / ( f_b[1] - f_a[1] ),
               ss[any] = s[CV()] * power(x[1], month[CV()]),
               f_x[1] = sum(ss)[any],
               a[1] = decode( sign( f_a[1] * f_x[1] ), 1, x[1], a[1] ),
               b[1] = decode( sign( f_a[1] * f_x[1]), 1, b[1], x[1] ),
               iter[1] = iteration_number + 1
            )
         ) b
         where b.the_row = 1
         and b.x <> 0
      ) i
      on (i.ilr_id = s.ilr_id)
      when matched then update
      set s.irr = i.the_irr;

      if sqlCode <> 0 then
         l_msg := substr('ERROR finding IRR: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_CALC_IRR;

   --**************************************************************************
   --                            F_NET_PRESENT_VALUE
   -- Determine the NET PRESENT VALUE of the future cash flow
   -- Based on the rate (either the discount rate entered or the IRR)
   --**************************************************************************
   function F_NET_PRESENT_VALUE
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'Starting NBV by payment';
      update ls_ilr_schedule_stg s
      set npv =
      (
         select b.npv
         from
         (
            SELECT id, ilr_id, month, npv
            FROM ls_ilr_schedule_stg
            WHERE process_npv = 1
            MODEL
            PARTITION BY (ilr_id)
            DIMENSION BY (row_number() over(partition by ilr_id order by month) as id)
            MEASURES (amount, 0 npv, rate, month, prepay_switch, residual_amount, bpo_price, term_penalty)
            RULES (
               npv[ ANY ] = (amount[ CV(id) ] + residual_amount[ CV(id) ] + bpo_price[ CV(id) ] + term_penalty[ CV(id) ] ) /
                           POWER(1 + rate[ CV(id) ], CV(id) - prepay_switch[ CV(id) ] )
            )
         ) b
         where b.ilr_id = s.ilr_id
         and b.month = s.month
      )
      where process_npv = 1;
      if sqlCode <> 0 then
         l_msg := substr('ERROR finding NBV1: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      l_status := 'Updating NBV for ILR';
      update ls_ilr_stg s
      set npv =
      (
         select round(sum(b.npv), 2)
         from ls_ilr_schedule_stg b
         where b.ilr_id = s.ilr_id
      )
      where process_npv = 1;
      if sqlCode <> 0 then
         l_msg := substr('ERROR finding NBV2: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_NET_PRESENT_VALUE;

   --**************************************************************************
   --                            F_LOAD_ILR_STG
   -- Loads the ILR_STG table for a single ILR ID
   --**************************************************************************
   function F_LOAD_ILR_STG (a_ilr_id number, a_revision number)
   return varchar2
   is
      l_msg varchar2(2000);
   begin
      insert into ls_ilr_stg
      (ilr_id, revision, prepay_switch, discount_rate, fmv, residual_amount, process_npv, bpo_price, term_penalty)
      select l.ilr_id, lo.revision, ll.pre_payment_sw, lo.inception_air / 12,
         sum(nvl(la.fmv, 0)), sum(nvl(la.guaranteed_residual_amount, 0)), 1, nvl(lo.purchase_option_amt, 0), nvl(lo.termination_amt, 0)
      from ls_ilr l, ls_lease ll, ls_asset la, ls_ilr_options lo
      where l.lease_id = ll.lease_id
      and l.ilr_id = la.ilr_id (+)
      and la.ls_asset_status_id <> 4
      and l.ilr_id = a_ilr_id
    and lo.ilr_id = l.ilr_id
      and lo.revision = a_revision
      group by l.ilr_id, ll.pre_payment_sw, lo.inception_air, lo.purchase_option_amt, lo.termination_amt, lo.revision
      ;

      if sqlCode <> 0 then
         l_msg := substr('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      return 'OK';
   exception
   when others then
      l_msg := substr('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
      return l_msg;
   end F_LOAD_ILR_STG;

   --**************************************************************************
   --                            F_LOAD_ILRS_STG
   -- Loads the ILR_STG table for a single LEASE ID
   --**************************************************************************
   function F_LOAD_ILRS_STG (a_lease_id number)
   return varchar2
   is
      l_msg varchar2(2000);
   begin
--      insert into ls_ilr_stg
--      (ilr_id, revision, prepay_switch, discount_rate, fmv, residual_amount, process_npv, bpo_price, term_penalty)
--      select l.ilr_id, l.revision, ll.pre_payment_sw, l.inception_air / 12,
--         sum(nvl(la.fmv, 0)), sum(nvl(la.guaranteed_residual_amount, 0)), 1, 0, 0
--      from ls_ilr l, ls_lease ll, ls_asset la
--      where l.lease_id = ll.lease_id
--      and l.ilr_id = la.ilr_id (+)
--      and la.ls_asset_status_id <> 4
--      and ll.lease_id = a_lease_id
--      and l.ilr_status_id = 2
--      group by l.ilr_id, ll.pre_payment_sw, l.inception_air, l.revision
--      ;
--
--      if sqlCode <> 0 then
--         l_msg := substr('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
--         return l_msg;
--      end if;

      return 'OK';
   exception
   when others then
      l_msg := substr('ERROR loading ls_ilr_stg: ' || sqlerrm, 1, 2000);
      return l_msg;
   end F_LOAD_ILRS_STG;

   --**************************************************************************
   --                            F_LOAD_ILR_SCHEDULE_STG
   -- Builds out the cash flow based on payment terms
   -- Take into consideration whether or not the payment is
   -- Paid at the beginning of the period or at the end of the period
   --**************************************************************************
   function F_LOAD_ILR_SCHEDULE_STG
   return varchar2 is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'Loading ls_ilr_schedule_stg';
      insert into ls_ilr_schedule_stg
      (
         id, ilr_id, revision, month, amount, residual_amount, rate, prepay_switch, process_npv,
         payment_month, months_to_accrue, bpo_price, term_penalty
      )
      with n as
      (
         -- create a "table" with 10000 rows
         select rownum as the_row
         from dual
         connect by level <= 10000
      ),
      lp as
      (
         -- find the payment_term_id with the latest payment_term_date
         select l2.ilr_id, l2.payment_term_id, l2.revision,
            row_number() over(partition by l2.ilr_id, l2.revision order by l2.payment_term_date desc) as the_max
         from ls_ilr_payment_term l2, ls_ilr_stg l1
         where l1.ilr_id = l2.ilr_id
      and l1.revision = l2.revision
      )
      select row_number() over(partition by p.ilr_id order by add_months(p.payment_term_date, n.the_row - 1)),
         p.ilr_id, l.revision, add_months(p.payment_term_date, the_row - 1),
         case when mod(n.the_row, decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1)) =
            decode(p.payment_freq_id, 4, 0, l.prepay_switch)
         then p.paid_amount else 0 end as paid_amount,
         case when n.the_row = p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1)
            and p.payment_term_id = lp.payment_term_id
         then l.residual_amount else 0 end as residual_amount,
         l.discount_rate, l.prepay_switch, 1,
         case when mod(n.the_row, decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1)) =
            decode(p.payment_freq_id, 4, 0, l.prepay_switch)
         then 1 else 0 end as payment_month,
         decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1) as months_to_accrue,
         case when n.the_row = p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1)
            and p.payment_term_id = lp.payment_term_id
         then l.bpo_price else 0 end as bpo_price,
         case when n.the_row = p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1)
            and p.payment_term_id = lp.payment_term_id
         then l.term_penalty else 0 end as term_penalty
      from ls_ilr_payment_term p, ls_ilr_stg l, lp, n
      where p.ilr_id = l.ilr_id
    and p.revision = l.revision
      and lp.ilr_id = l.ilr_id
   and lp.revision = l.revision
      and lp.the_max = 1
      and n.the_row <=  p.number_of_terms * decode(p.payment_freq_id, 1, 12, 2, 6, 3, 3, 1)
      order by 2;

      if sqlCode <> 0 then
         l_msg := substr('ERROR loading ls_ilr_schedule_stg: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      return 'OK';
   exception
   when others then
      l_msg := substr(l_status || ': ' || sqlerrm, 1, 2000);
      return l_msg;
   end F_LOAD_ILR_SCHEDULE_STG;

   --**************************************************************************
   --                            F_CALC_SCHEDULES
   --**************************************************************************
   function F_CALC_SCHEDULES
   return varchar2 is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'CALLING F_LOAD_ILR_SCHEDULE_STG';
      l_msg := F_LOAD_ILR_SCHEDULE_STG;
      if l_msg = 'OK' then
         -- Determine the Net Present Value based on the
         -- entered discount rate and the cash flow
         l_status := 'CALLING F_NET_PRESENT_VALUE';
         l_msg := F_NET_PRESENT_VALUE;
         if l_msg = 'OK' then
            -- Call the function to calculate internal rate of return
            -- the function will only update ILRs where NPV > FMV
            l_status := 'CALLING F_CALC_IRR';
            l_msg := F_CALC_IRR;
            if l_msg = 'OK' then
               l_status := 'DO NOT REPROCESS NPV FOR SOME ILRs';
               P_CHECK_RECALC_NPV;

               l_status := 'CALLING F_NET_PRESENT_VALUE AFTER IRR';
               l_msg := F_NET_PRESENT_VALUE;
               return l_msg;
            end if;
         end if;
      end if;

      return l_msg;
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_CALC_SCHEDULES;

   --**************************************************************************
   --                            F_PROCESS_ILR
   --**************************************************************************
   function F_PROCESS_ILR(a_ilr_id number, a_revision number)
   return varchar2 is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'CALLING F_LOAD_ILR_STG';
      l_msg := F_LOAD_ILR_STG(a_ilr_id, a_revision);
      if l_msg = 'OK' then
         l_status := 'CALLING F_CALC_SCHEDULES';
         l_msg := F_CALC_SCHEDULES;
         return l_msg;
      end if;
      return l_msg;
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_PROCESS_ILR;

   --**************************************************************************
   --                            F_PROCESS_ILRS
   --**************************************************************************
   function F_PROCESS_ILRS(a_lease_id number)
   return varchar2 is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'CALLING F_LOAD_ILRS_STG';
      l_msg := F_LOAD_ILRS_STG(a_lease_id);
      if l_msg = 'OK' then
         l_status := 'CALLING F_CALC_SCHEDULES';
         l_msg := F_CALC_SCHEDULES;
         return l_msg;
      end if;
      return l_msg;
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_PROCESS_ILRS;


   --**************************************************************************
   --                            F_SAVE_SCHEDULES
   -- This function saves the scehdules frm the calc table to the stg tables
   --**************************************************************************
   function F_SAVE_SCHEDULES
   return varchar2
   is
      l_msg varchar2(2000);
      l_status varchar2(2000);
   begin
      l_status := 'LOADING ls_ilr_schedule';
   delete from ls_ilr_schedule
   where (ilr_id, revision) in
   (
      select a.ilr_id, a.revision
      from LS_ILR_ASSET_SCHEDULE_STG a
   );

    if sqlCode <> 0 then
         l_msg := substr('ERROR deleting from ls_ilr_schedule: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      insert into ls_ilr_schedule
      (
         ILR_ID, REVISION,
         MONTH, RESIDUAL_AMOUNT,
         TERM_PENALTY, BPO_PRICE,
         BEG_CAPITAL_COST, END_CAPITAL_COST,
         BEG_OBLIGATION, END_OBLIGATION,
         BEG_LT_OBLIGATION, END_LT_OBLIGATION,
         INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL,
         INTEREST_PAID, PRINCIPAL_PAID
      )
      select ILR_ID, REVISION,
         MONTH, sum(RESIDUAL_AMOUNT),
         sum(TERM_PENALTY), sum(BPO_PRICE),
         sum(BEG_CAPITAL_COST), sum(END_CAPITAL_COST),
         sum(BEG_OBLIGATION), sum(END_OBLIGATION),
         sum(BEG_LT_OBLIGATION), sum(END_LT_OBLIGATION),
         sum(INTEREST_ACCRUAL), sum(PRINCIPAL_ACCRUAL),
         sum(INTEREST_PAID), sum(PRINCIPAL_PAID)
      from LS_ILR_ASSET_SCHEDULE_STG
      group by ilr_id, revision, month;

      if sqlCode <> 0 then
         l_msg := substr('ERROR loading ls_ilr_schedule: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      l_status := 'LOADING ls_asset_schedule';
   delete from ls_asset_schedule
   where (ls_asset_id, revision) in
   (
      select a.ls_asset_id, a.revision
      from LS_ILR_ASSET_SCHEDULE_STG a
   );

 if sqlCode <> 0 then
         l_msg := substr('ERROR deleting from ls_asset_schedule: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

      insert into ls_asset_schedule
      (
         LS_ASSET_ID, REVISION,
         MONTH, RESIDUAL_AMOUNT,
         TERM_PENALTY, BPO_PRICE,
         BEG_CAPITAL_COST, END_CAPITAL_COST,
         BEG_OBLIGATION, END_OBLIGATION,
         BEG_LT_OBLIGATION, END_LT_OBLIGATION,
         INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL,
         INTEREST_PAID, PRINCIPAL_PAID
      )
      select LS_ASSET_ID, REVISION,
         MONTH, RESIDUAL_AMOUNT,
         TERM_PENALTY, BPO_PRICE,
         BEG_CAPITAL_COST, END_CAPITAL_COST,
         BEG_OBLIGATION, END_OBLIGATION,
         BEG_LT_OBLIGATION, END_LT_OBLIGATION,
         INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL,
         INTEREST_PAID, PRINCIPAL_PAID
      from LS_ILR_ASSET_SCHEDULE_STG;

      if sqlCode <> 0 then
         l_msg := substr('ERROR loading ls_asset_schedule: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

   delete from LS_ILR_ASSET_SCHEDULE_STG;
      if sqlCode <> 0 then
         l_msg := substr('ERROR loading LS_ILR_ASSET_SCHEDULE_STG: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

   delete from LS_ILR_ASSET_STG;
      if sqlCode <> 0 then
         l_msg := substr('ERROR loading LS_ILR_ASSET_STG: ' || sqlerrm, 1, 2000);
         return l_msg;
      end if;

   delete from LS_ILR_SCHEDULE_STG;
      if sqlCode <> 0 then
         l_msg := substr('ERROR loading LS_ILR_SCHEDULE_STG: ' || sqlerrm, 1, 2000);
         return l_msg;
   end if;

   delete from LS_ILR_STG;
      if sqlCode <> 0 then
         l_msg := substr('ERROR loading LS_ILR_STG: ' || sqlerrm, 1, 2000);
         return l_msg;
   end if;

      return 'OK';
   exception
   when others then
      l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
      return l_status;
   end F_SAVE_SCHEDULES;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (471, 0, 10, 4, 1, 0, 30751, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030751_lease_PKG_LEASE_SCHEDULE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
