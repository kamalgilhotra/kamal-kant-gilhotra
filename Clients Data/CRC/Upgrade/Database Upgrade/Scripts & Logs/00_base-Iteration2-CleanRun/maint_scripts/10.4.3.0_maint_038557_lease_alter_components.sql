/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038557_lease_alter_components.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 10.4.3.0 06/12/2014 Shane "C" Ward       Changing column names to make easier to query
||========================================================================================
*/

alter table LS_LEASE_INTERIM_RATES rename column LS_LEASE_ID to LEASE_ID;
alter table LS_COMPONENT rename column LS_COMPONENT_ID to COMPONENT_ID;

alter table LS_ILR drop column INTERIM_INTEREST_BEGIN_DATE;
alter table LS_ILR drop column FINAL_EOT_DATE;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1199, 0, 10, 4, 3, 0, 38557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038557_lease_alter_components.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
