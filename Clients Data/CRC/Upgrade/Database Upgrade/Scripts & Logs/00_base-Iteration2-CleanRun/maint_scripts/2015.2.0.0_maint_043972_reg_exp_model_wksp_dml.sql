/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043972_reg_exp_model_wksp_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/01/2015 Alex P.        Add export working case model wksp and menu
||============================================================================
*/

insert into ppbase_workspace( module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values( 'REG', 'uo_reg_exp_work_model_ws', 'Case Working Model', 'uo_reg_exp_work_model_ws', 'Case Working Model', 1);

insert into ppbase_menu_items( module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values( 'REG', 'EXPORT', 1, 8, 'Export', 'Export', null, null, 1);

insert into ppbase_menu_items( module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values( 'REG', 'EXP_MODEL', 2, 1, 'Case Working Model', 'Case Working Model', 'EXPORT', 'uo_reg_exp_work_model_ws', 1);




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2572, 0, 2015, 2, 0, 0, 043972, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043972_reg_exp_model_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;