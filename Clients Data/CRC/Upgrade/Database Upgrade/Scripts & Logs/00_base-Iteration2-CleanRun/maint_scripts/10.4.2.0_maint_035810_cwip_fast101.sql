/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_035810_cwip_fast101.sql  (part of epic 9415)
||=================================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date     Revised By     Reason for Change
|| -------- -------- -------------- ------------------------------------------
|| 10.4.2.0 3/7/2014 Sunjin Cone    New Auto101 code for performance improvement
||=================================================================================
*/

create table UNITIZE_WO_LIST
(
 WORK_ORDER_ID           number(22,0) not null,
 WORK_ORDER_NUMBER       varchar2(35) not null,
 COMPANY_ID              number(22,0) not null,
 BUS_SEGMENT_ID          number(22,0) not null,
 MAJOR_LOCATION_ID       number(22,0) not null,
 ASSET_LOCATION_ID       number(22,0),
 WO_STATUS_ID            number(22,0) not null,
 IN_SERVICE_DATE         date,
 COMPLETION_DATE         date,
 LATE_CHG_WAIT_PERIOD    number(22,0),
 FUNDING_WO_ID           number(22,0),
 DESCRIPTION             varchar2(35),
 LONG_DESCRIPTION        varchar2(254),
 CLOSING_OPTION_ID       number(22,0) not null,
 CWIP_GL_ACCOUNT         number(22,0) not null,
 NON_UNITIZED_GL_ACCOUNT number(22,0) not null,
 UNITIZED_GL_ACCOUNT     number(22,0) not null,
 ALLOC_METHOD_TYPE_ID    number(22,0) not null,
 ACCRUAL_TYPE_ID         number(22,0),
 RWIP_YES_NO             varchar2(3),
 EST_UNIT_ITEM_OPTION    number(22,0),
 UNIT_ITEM_FROM_ESTIMATE number(22,0),
 UNIT_ITEM_SOURCE        varchar2(35),
 UNITIZE_BY_ACCOUNT      number(22,0),
 TOLERANCE_ID            number(22,0),
 TOLERANCE_REVISION      number(22,0),
 TOLERANCE_THRESHOLD     number(22,2),
 TOLERANCE_PCT           number(22,4),
 TOLERANCE_ESTIMATE_AMT  number(22,2),
 TOLERANCE_ACTUAL_AMT    number(22,2),
 TOLERANCE_EST_ACT_DIFF  number(22,2),
 MAX_CHARGE_GROUP_ID     number(22,0),
 MAX_REVISION            number(22,0),
 MAX_UNIT_ITEM_ID        number(22,0),
 RTN_CODE                number(22,0),
 ERROR_MSG               varchar2(2000),
 TIME_STAMP              date,
 USER_ID                 varchar2(18)
);

alter table UNITIZE_WO_LIST
   add constraint PK_UNITIZE_WO_LIST
       primary key (WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_WO_LIST is '(C) [04] Stores the list of work order(s) being unitized.';
comment on column UNITIZE_WO_LIST.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_LIST.WORK_ORDER_NUMBER is 'User-defined number associated with a Work Order.  This is usually a reference to an external numbering system.';
comment on column UNITIZE_WO_LIST.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_LIST.BUS_SEGMENT_ID is 'System-assigned identifier of a particular business segment.';
comment on column UNITIZE_WO_LIST.MAJOR_LOCATION_ID is 'System-assigned identifier of a particular major location.';
comment on column UNITIZE_WO_LIST.ASSET_LOCATION_ID is 'System-assigned identifier of a particular asset location.';
comment on column UNITIZE_WO_LIST.WO_STATUS_ID is 'System-assigned identifier of a particular work order status.';
comment on column UNITIZE_WO_LIST.IN_SERVICE_DATE is 'Actual (day, month, year) date in which facility constructed is in service; AFUDC should cease and the accounting may now be in 106 or 101.';
comment on column UNITIZE_WO_LIST.COMPLETION_DATE is 'Month/year/date that the job is complete and charges are in; at this point it is waiting unitization and closing.';
comment on column UNITIZE_WO_LIST.LATE_CHG_WAIT_PERIOD is 'Number of months that a Work Order remains after the completion date before it is available for automatic unitization.';
comment on column UNITIZE_WO_LIST.FUNDING_WO_ID is 'Refers to another work order identifier that is the funding work order to which this work order has been assigned.  Funding work  orders, if used, can be set up using a special Work Order Type on the Work Order Type table.';
comment on column UNITIZE_WO_LIST.DESCRIPTION is 'Brief description of the Work Order.';
comment on column UNITIZE_WO_LIST.LONG_DESCRIPTION is 'Detailed description of the Work Order.';
comment on column UNITIZE_WO_LIST.CLOSING_OPTION_ID is 'System-assigned identifier of a particular closing option.';
comment on column UNITIZE_WO_LIST.CWIP_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account for relieving the CWIP side of the work order (e.g., 107, 183 when posting to 106 or 101.';
comment on column UNITIZE_WO_LIST.NON_UNITIZED_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account used when posting in a non-unitized manner (e.g., 106).';
comment on column UNITIZE_WO_LIST.UNITIZED_GL_ACCOUNT is 'System-assigned identifier of a unique General Ledger account to which unitized entries are.';
comment on column UNITIZE_WO_LIST.ALLOC_METHOD_TYPE_ID is 'System-assigned identifier of a particular Allocation Method Type.';
comment on column UNITIZE_WO_LIST.ACCRUAL_TYPE_ID is 'This field indicates which accrual type is assigned to the work order/funding project.';
comment on column UNITIZE_WO_LIST.RWIP_YES_NO is 'Yes = Unitization includes RWIP charges.  No = Unitization excludes RWIP Charges.';
comment on column UNITIZE_WO_LIST.EST_UNIT_ITEM_OPTION is 'Summarize (=0) � Force unitization to create unit items from the summarized estimates (default). One for One (=1) = Allow unitization to create unit items from non-summarized estimate.';
comment on column UNITIZE_WO_LIST.UNIT_ITEM_FROM_ESTIMATE is 'Yes (=1) � Force unitization to create unit items from the estimates only and ignore actuals. No (=0) = Allow unitization to decide how to create unit items based on the presence of actuals first and then estimate (default).';
comment on column UNITIZE_WO_LIST.UNIT_ITEM_SOURCE is 'This field indicates if the source of the unit item creation is Actuals, Estimates, or CPR.';
comment on column UNITIZE_WO_LIST.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_WO_LIST.TOLERANCE_ID is 'System-assigned identifier of a tolerance test used in unitization and set on the unitization tolerance table.';
comment on column UNITIZE_WO_LIST.TOLERANCE_REVISION is 'Estimate revision associated with the unitization tolerance validation.';
comment on column UNITIZE_WO_LIST.TOLERANCE_THRESHOLD is 'Estimate dollar VS Actual dollar threshold for the unitization tolerance.';
comment on column UNITIZE_WO_LIST.TOLERANCE_PCT is 'Estimate vs Actual dollar difference vs Estimate dollar percent threshold for the unitization tolerance.';
comment on column UNITIZE_WO_LIST.TOLERANCE_ESTIMATE_AMT is 'Estimate dollar amount for the work order.';
comment on column UNITIZE_WO_LIST.TOLERANCE_ACTUAL_AMT is 'Actual dollar amount for the work order.';
comment on column UNITIZE_WO_LIST.TOLERANCE_EST_ACT_DIFF is 'Absoulte value of the difference of Estimate vs Actual dollar amounts for the work order.';
comment on column UNITIZE_WO_LIST.MAX_CHARGE_GROUP_ID is 'The maximum charge_group_id from CHARGE_GROUP_CONTROL table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST.MAX_REVISION is 'The maximum revision from WO_ESTIMATE table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST.MAX_UNIT_ITEM_ID is 'The maximum unit_item_id from UNITIZED_WORK_ORDER table at the time of unitization processing.';
comment on column UNITIZE_WO_LIST.RTN_CODE is 'System-assigned identifier of a particular return code saved to Auto101 errors log(WO_AUTO101_CONTROL).';
comment on column UNITIZE_WO_LIST.ERROR_MSG is 'Error messages saved to Auto101 errors log(WO_AUTO101_CONTROL).';
comment on column UNITIZE_WO_LIST.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_LIST.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_WO_LIST_LOOP
(
 WORK_ORDER_ID number(22,0) not null,
 COMPANY_ID    number(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

alter table UNITIZE_WO_LIST_LOOP
   add constraint PK_UNITIZE_WO_LIST_LOOP
       primary key (WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_WO_LIST_LOOP is '(C) [04] Stores the list of work order(s) that need to be unitized using the old unitization code that unitizes one work order at a time.';
comment on column UNITIZE_WO_LIST_LOOP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_LIST_LOOP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_LIST_LOOP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_LIST_LOOP.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_WO_SINGLE
(
 WORK_ORDER_ID number(22,0) not null,
 COMPANY_ID    number(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

alter table UNITIZE_WO_SINGLE
   add constraint PK_UNITIZE_WO_SINGLE
       primary key (WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_WO_SINGLE is '(C) [04] The list of work orders currently being single Unitized via Manual or Single WO Auto Unitization.';
comment on column UNITIZE_WO_SINGLE.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_SINGLE.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_SINGLE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_SINGLE.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_WO_UNITS_STG
(
 ID                    number(22,0),
 COMPANY_ID            number(22,0),
 WORK_ORDER_ID         number(22,0),
 NEW_UNIT_ITEM_ID      number(22,0),
 UNIT_ITEM_SOURCE      varchar2(35),
 RETIREMENT_UNIT_ID    number(22,0),
 UTILITY_ACCOUNT_ID    number(22,0),
 EST_BUS_SEGMENT_ID    number(22,0),
 BUS_SEGMENT_ID        number(22,0),
 SUB_ACCOUNT_ID        number(22,0),
 EST_PROPERTY_GROUP_ID number(22,0),
 PROPERTY_GROUP_ID     number(22,0),
 EST_ASSET_LOCATION_ID number(22,0),
 ASSET_LOCATION_ID     number(22,0),
 MAJOR_LOCATION_ID     number(22,0),
 WO_MAJOR_LOCATION_ID  number(22,0),
 UA_FUNC_CLASS_ID      number(22,0),
 PROPERTY_UNIT_ID      number(22,0),
 LOCATION_TYPE_ID      number(22,0),
 SERIAL_NUMBER         varchar2(35),
 GL_ACCOUNT_ID         number(22,0),
 ASSET_ACCT_METH_ID    number(22,0),
 ACTIVITY_CODE         varchar2(8),
 DESCRIPTION           varchar2(254),
 SHORT_DESCRIPTION     varchar2(35),
 AMOUNT                number(22,2),
 QUANTITY              number(22,2),
 LDG_ASSET_ID          number(22,0),
 STATUS                number(22,0),
 EST_REVISION          number(22,0),
 UNIT_ESTIMATE_ID      number(22,0),
 MINOR_UNIT_ITEM_ID    number(22,0),
 IN_SERVICE_DATE       date,
 REPLACEMENT_AMOUNT    number(22,2),
 ERROR_MSG             varchar2(2000),
 TIME_STAMP            date,
 USER_ID               varchar2(18)
);

comment on table  UNITIZE_WO_UNITS_STG is '(C) [04] The list of new unit items to add during unitization. No primary key.';
comment on column UNITIZE_WO_UNITS_STG.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_WO_UNITS_STG.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_WO_UNITS_STG.NEW_UNIT_ITEM_ID is 'System-assigned identifier of the new unit item.';
comment on column UNITIZE_WO_UNITS_STG.UNIT_ITEM_SOURCE is 'This field indicates if the source of the unit item creation is Actuals, Estimates, or CPR.';
comment on column UNITIZE_WO_UNITS_STG.RETIREMENT_UNIT_ID is 'System-assigned identifier of a particular retirement unit.';
comment on column UNITIZE_WO_UNITS_STG.UTILITY_ACCOUNT_ID is 'User-designated identifier of a unique utility plant account.  It can be a FERC utility plant account such as 314 or the Companys own account number structure.';
comment on column UNITIZE_WO_UNITS_STG.EST_BUS_SEGMENT_ID  is 'System-assigned identifier of a unique business segment from wo estimate.';
comment on column UNITIZE_WO_UNITS_STG.BUS_SEGMENT_ID is 'System-assigned identifier of a unique business segment.';
comment on column UNITIZE_WO_UNITS_STG.SUB_ACCOUNT_ID is 'User-designated value to further detail a particular utility account.';
comment on column UNITIZE_WO_UNITS_STG.EST_PROPERTY_GROUP_ID is  'System-assigned identifier of a particular property group from wo estimate';
comment on column UNITIZE_WO_UNITS_STG.PROPERTY_GROUP_ID is 'System-assigned identifier of a particular property group.';
comment on column UNITIZE_WO_UNITS_STG.EST_ASSET_LOCATION_ID is 'System-assigned identifier of an asset location from wo estimate.';
comment on column UNITIZE_WO_UNITS_STG.ASSET_LOCATION_ID is 'System-assigned identifier of an asset location.';
comment on column UNITIZE_WO_UNITS_STG.MAJOR_LOCATION_ID is 'System-assigned identifier of a major location.';
comment on column UNITIZE_WO_UNITS_STG.WO_MAJOR_LOCATION_ID is 'System-assigned identifier of a major location on work order control.';
comment on column UNITIZE_WO_UNITS_STG.UA_FUNC_CLASS_ID is 'System-assigned identifier of a particular functional class on the utility account.';
comment on column UNITIZE_WO_UNITS_STG.PROPERTY_UNIT_ID is 'System-assigned identifier of a particular property unit.';
comment on column UNITIZE_WO_UNITS_STG.LOCATION_TYPE_ID is 'System-assigned identifier of a particular location type.';
comment on column UNITIZE_WO_UNITS_STG.SERIAL_NUMBER is 'Records the serial number associated with the asset component by the manufacturer.';
comment on column UNITIZE_WO_UNITS_STG.GL_ACCOUNT_ID is 'System-assigned identifier of a particular gl account.';
comment on column UNITIZE_WO_UNITS_STG.ASSET_ACCT_METH_ID is 'System-assigned identifier of a particular asset accounting method.';
comment on column UNITIZE_WO_UNITS_STG.ACTIVITY_CODE is 'An activity code from the Activity Code table.';
comment on column UNITIZE_WO_UNITS_STG.DESCRIPTION is 'Description of the particular asset (retirement unit).';
comment on column UNITIZE_WO_UNITS_STG.SHORT_DESCRIPTION is 'Short description filled in by user during unitization.';
comment on column UNITIZE_WO_UNITS_STG.AMOUNT is 'Amount in dollars for the CPR entry.';
comment on column UNITIZE_WO_UNITS_STG.QUANTITY is 'Quantity for the CPR entry.';
comment on column UNITIZE_WO_UNITS_STG.LDG_ASSET_ID is 'System-assigned identifier of the CPR asset identifier for a retirement or transfer.';
comment on column UNITIZE_WO_UNITS_STG.STATUS is 'Internally set processing indicated used in unitization allocation: Null =  From charges; 10 = Minor addition;  11 = From wo estimates';
comment on column UNITIZE_WO_UNITS_STG.EST_REVISION is 'WO Estimate revision used for unit item creation';
comment on column UNITIZE_WO_UNITS_STG.UNIT_ESTIMATE_ID is 'System-assigned identifier of a particular wo estimate.';
comment on column UNITIZE_WO_UNITS_STG.MINOR_UNIT_ITEM_ID is 'Item id from this table for a previously unitized item.  Used for minor items to allow copying of additional details.';
comment on column UNITIZE_WO_UNITS_STG.IN_SERVICE_DATE is 'Engineering in-service date, when using multiple in-service dates for a single work order.  (See PP System Control.)';
comment on column UNITIZE_WO_UNITS_STG.REPLACEMENT_AMOUNT is 'Dollar amount in current dollars to be (Handy Whitman) indexed to get original dollars when the retirement unit is using a retirement method of HW_Fifo or HW_Curve.';
comment on column UNITIZE_WO_UNITS_STG.ERROR_MSG is 'Error message';
comment on column UNITIZE_WO_UNITS_STG.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_WO_UNITS_STG.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ALLOC_LIST
(
 COMPANY_ID     number(22,0) not null,
 ALLOC_ID       number(22,0) not null,
 ALLOC_PRIORITY number(22,0) not null,
 ALLOC_TYPE     varchar2(35) not null,
 ALLOC_BASIS    varchar2(35) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18)
);

alter table UNITIZE_ALLOC_LIST
   add constraint PK_UNITIZE_ALLOC_LIST
       primary key (COMPANY_ID, ALLOC_ID, ALLOC_PRIORITY, ALLOC_TYPE, ALLOC_BASIS)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_LIST is '(C) [04] The list of distinct allocation methods for the work orders being unitized.';
comment on column UNITIZE_ALLOC_LIST.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_LIST.ALLOC_ID is 'System-assigned identifier of the unit allocation type.';
comment on column UNITIZE_ALLOC_LIST.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_LIST.ALLOC_TYPE is 'Indicates whether the unitization basis will be on Actual, Estimate, or Standards.';
comment on column UNITIZE_ALLOC_LIST.ALLOC_BASIS is 'Defines the allocation methodology given by allocation type, ie dollars, quantity, material cost, etc.';
comment on column UNITIZE_ALLOC_LIST.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_LIST.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_ALLOC_ACT_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 ALLOC_ID               number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 UTILITY_ACCOUNT_ID     number(22,0),
 UNITIZE_BY_ACCOUNT     number(22,0),
 ALLOC_PRIORITY         number(22,8) not null,
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_ACCOUNT number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_ACT_STATS
   add constraint PK_UNITIZE_ALLOC_ACT_STATS
       primary key (COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, ALLOC_ID, ALLOC_PRIORITY)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_ACT_STATS is '(C) [04] Allocation information used to unitization charge types configured for Actuals based allocation.';
comment on column UNITIZE_ALLOC_ACT_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_ACT_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_ACT_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_ACT_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_ACT_STATS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_ACT_STATS.ALLOC_TYPE is 'Allocation type: actuals.';
comment on column UNITIZE_ALLOC_ACT_STATS.ALLOC_BASIS is 'Allocation basis: dollars, quantity.';
comment on column UNITIZE_ALLOC_ACT_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_ACT_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_ACT_STATS.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_ACT_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_ACT_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_ACT_STATS.ALLOC_RATIO_BY_ACCOUNT is 'Allocation percentage assigned to the unit item that has utility account information over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_ACT_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_ACT_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ALLOC_EST_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 ALLOC_ID               number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 UTILITY_ACCOUNT_ID     number(22,0),
 UNITIZE_BY_ACCOUNT     number(22,0),
 ALLOC_PRIORITY         number(22,0) not null,
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_ACCOUNT number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_EST_STATS
   add constraint PK_UNITIZE_ALLOC_EST_STATS
       primary key (COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, ALLOC_ID, ALLOC_PRIORITY)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_EST_STATS is '(C) [04] Allocation information used to unitization charge types configured for Estimates based allocation.';
comment on column UNITIZE_ALLOC_EST_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_EST_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_EST_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_EST_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_TYPE is 'Allocation type: estimates.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_BASIS is 'Allocation basis: dollars, quantity.';
comment on column UNITIZE_ALLOC_EST_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_EST_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_EST_STATS.ALLOC_RATIO_BY_ACCOUNT is 'Allocation percentage assigned to the unit item that has utility account information over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_EST_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_EST_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_ALLOC_STND_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 ALLOC_ID               number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 UTILITY_ACCOUNT_ID     number(22,0),
 UNITIZE_BY_ACCOUNT     number(22,0),
 ALLOC_PRIORITY         number(22,0) not null,
 ALLOC_STATISTIC        number(22,2),
 UNIT_QUANTITY          number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_ACCOUNT number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_STND_STATS
   add constraint PK_UNITIZE_ALLOC_STND_STATS
       primary key (COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, ALLOC_ID, ALLOC_PRIORITY)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_STND_STATS is '(C) [04] Allocation information used to unitization charge types configured for Standards based allocation.';
comment on column UNITIZE_ALLOC_STND_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_STND_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_STND_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_STND_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_TYPE is 'Allocation type: standards.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_BASIS is 'Allocation basis: standard material cost, standard labor_cost, standard labor hours, standard cor cost, standard cor hours.';
comment on column UNITIZE_ALLOC_STND_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_STND_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_RATIO_BY_ACCOUNT is 'Allocation percentage assigned to the unit item that has utility account information over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_STND_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_STND_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_ALLOC_UNIT_ITEM_STATS
(
 COMPANY_ID             number(22,0) not null,
 WORK_ORDER_ID          number(22,0) not null,
 UNIT_ITEM_ID           number(22,0) not null,
 EXPENDITURE_TYPE_ID    number(22,0) not null,
 UTILITY_ACCOUNT_ID     number(22,0),
 UNITIZE_BY_ACCOUNT     number(22,0),
 ALLOC_ID               number(22,0) not null,
 ALLOC_TYPE             varchar2(35),
 ALLOC_BASIS            varchar2(35),
 ALLOC_PRIORITY         number(22,0) not null,
 ALLOC_STATISTIC        number(22,2),
 ALLOC_RATIO            number(22,8),
 ALLOC_RATIO_BY_ACCOUNT number(22,8),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

alter table UNITIZE_ALLOC_UNIT_ITEM_STATS
   add constraint PK_UNITIZE_ALLOC_UNIT_ITEM_STA
       primary key (COMPANY_ID, WORK_ORDER_ID, UNIT_ITEM_ID, EXPENDITURE_TYPE_ID, ALLOC_ID, ALLOC_PRIORITY)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_UNIT_ITEM_STATS is '(C) [04] Allocation information used to unitization charge types configured for Actuals, Estimate, and Standards based allocation.  This table holds the collection of data from the three tables, UNITIZE_ALLOC_ACT_STATS, UNITIZE_ALLOC_EST_STATS, and UNITIZE_ALLOC_STND_STATS.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.UNIT_ITEM_ID  is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_TYPE is 'Allocation type: actuals, estimates, standards.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_BASIS is 'Allocation basis: dollars, quantity, standard material cost, standard labor_cost, standard labor hours, standard cor cost, standard cor hours.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.UNITIZE_BY_ACCOUNT is '1=Yes, which means adhere to utility account during unitization.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_PRIORITY is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_STATISTIC is 'Numeric value used to determine the allocation percent ratio.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.ALLOC_RATIO is 'Allocation percentage assigned to the unit item over all unit items.';
comment on column UNITIZE_ALLOC_STND_STATS.ALLOC_RATIO_BY_ACCOUNT is 'Allocation percentage assigned to the unit item that has utility account information over all unit items that have utility account information.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_UNIT_ITEM_STATS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create table UNITIZE_ALLOC_ERRORS
(
 COMPANY_ID           number(22,0) not null,
 WORK_ORDER_ID        number(22,0) not null,
 EXPENDITURE_TYPE_ID  number(22,0) not null,
 UTILITY_ACCOUNT_ID   number(22,0) not null,
 ALLOC_ID             number(22,0) not null,
 ALLOC_DESCRIPTION    varchar2(35),
 PRIORITY             number(22,0) not null,
 ERROR_MSG            varchar2(2000),
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

alter table UNITIZE_ALLOC_ERRORS
   add constraint PK_UNITIZE_ALLOC_ERRORS
       primary key (COMPANY_ID, WORK_ORDER_ID, UTILITY_ACCOUNT_ID, ALLOC_ID, EXPENDITURE_TYPE_ID, PRIORITY)
       using index tablespace PWRPLANT_IDX;

comment on table  UNITIZE_ALLOC_ERRORS is '(C) [04] Unitization allocation errors saved for work orders being unitized.';
comment on column UNITIZE_ALLOC_ERRORS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_ERRORS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_ERRORS.EXPENDITURE_TYPE_ID is 'System-assigned identifier of the expenditure type.';
comment on column UNITIZE_ALLOC_ERRORS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.  Used for the Unitize by Account option.';
comment on column UNITIZE_ALLOC_ERRORS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_ERRORS.ALLOC_DESCRIPTION is 'Unit Allocation Type and Basis description.';
comment on column UNITIZE_ALLOC_ERRORS.PRIORITY  is 'User-defined order (from 1 to n) that determines the order in which charge types are allocated.';
comment on column UNITIZE_ALLOC_ERRORS.ERROR_MSG is 'Description of the error message.';
comment on column UNITIZE_ALLOC_ERRORS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_ERRORS.USER_ID is 'Standard system-assigned user id used for audit purposes.';


create table UNITIZE_ALLOC_CGC_INSERTS
(
 ID                   number(22,0),
 COMPANY_ID           number(22,0) not null,
 ORIG_CHARGE_GROUP_ID number(22,0) not null,
 NEW_CHARGE_GROUP_ID  number(22,0),
 WORK_ORDER_ID        number(22,0) not null,
 ALLOC_ID             number(22,0),
 UNIT_ITEM_ID         number(22,0),
 UTILITY_ACCOUNT_ID   number(22,0),
 BUS_SEGMENT_ID       number(22,0),
 SUB_ACCOUNT_ID       number(22,0),
 CHARGE_TYPE_ID       number(22,0),
 EXPENDITURE_TYPE_ID  number(22,0),
 RETIREMENT_UNIT_ID   number(22,0),
 PROPERTY_GROUP_ID    number(22,0),
 ASSET_LOCATION_ID    number(22,0),
 SERIAL_NUMBER        varchar2(35),
 DESCRIPTION          varchar2(35),
 GROUP_INDICATOR      number(22,0),
 ALLOCATION_PRIORITY  number(22,0),
 UNITIZATION_SUMMARY  varchar2(35),
 BOOK_SUMMARY_NAME    varchar2(35),
 AMOUNT               number(22,2) default 0,
 QUANTITY             number(22,2) default 0,
 TIME_STAMP           date,
 USER_ID              varchar2(18)
);

comment on table  UNITIZE_ALLOC_CGC_INSERTS is '(C) [04] Unitization allocation results to be inserted into the CHARGE_GROUP_CONTROL table.  No primary key.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.ID is 'System-assigned identifier of a particular record in the table.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.ORIG_CHARGE_GROUP_ID is 'System-assigned identifier of the charge_group_id being allocated.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.NEW_CHARGE_GROUP_ID is 'System-assigned identifier of the new charge_group_id created for the allocation results.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.ALLOC_ID is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.UNIT_ITEM_ID is 'System-assigned identifier of the unit item.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account of the unit item.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment associated with the utility account of the unit item.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account associated with the utilty account of the unit item.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.PROPERTY_GROUP_ID is 'System-assigned identifier of the property group of the unit item.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.ASSET_LOCATION_ID is 'System-assigned identifier of the asset location from CWIP_CHARGE.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.SERIAL_NUMBER is 'Serial Number from CWIP_CHARGE.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.DESCRIPTION is 'Records a short description of the charge group (or charge).';
comment on column UNITIZE_ALLOC_CGC_INSERTS.GROUP_INDICATOR is 'Use null value for these records to indicate they are from allocation results, not from CWIP_CHARGE.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.ALLOCATION_PRIORITY is 'User-defined order (from 1 to n) that was used when the charge types were allocated.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.UNITIZATION_SUMMARY is 'Summary Name from BOOK_SUMMARY associated with the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.BOOK_SUMMARY_NAME is 'Summary Name from BOOK_SUMMARY associated with the charge type.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.AMOUNT is 'Dollar amount of allocated records.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.QUANTITY is 'Use 0 for quantity of the allocated records.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column UNITIZE_ALLOC_CGC_INSERTS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

create global temporary table UNITIZE_PEND_TRANS_TEMP
(
 COMPANY_ID          number(22,0),
 GL_POSTING_MO_YR    date,
 IN_SERVICE_YEAR     date,
 WORK_ORDER_ID       number(22,0),
 WORK_ORDER_NUMBER   varchar2(35),
 UNIT_ITEM_ID        number(22,0),
 PEND_TRANS_ID       number(22,0),
 LDG_ASSET_ID        number(22,0),
 LDG_DEPR_GROUP_ID   number(22,0),
 GL_ACCOUNT_ID       number(22,0),
 BUS_SEGMENT_ID      number(22,0),
 UTILITY_ACCOUNT_ID  number(22,0),
 SUB_ACCOUNT_ID      number(22,0),
 FUNC_CLASS_ID       number(22,0),
 RETIREMENT_UNIT_ID  number(22,0),
 PROPERTY_GROUP_ID   number(22,0),
 ASSET_LOCATION_ID   number(22,0),
 SUBLEDGER_INDICATOR number(22,0),
 POSTING_QUANTITY    number(22,2),
 POSTING_AMOUNT      number(22,2),
 GL_JE_CODE          char(18),
 SERIAL_NUMBER       varchar2(35),
 FERC_ACTIVITY_CODE  number(22,2),
 ACTIVITY_CODE       char(5),
 DESCRIPTION         varchar2(35),
 LONG_DESCRIPTION    varchar2(254),
 RETIRE_METHOD_ID    number(22,2),
 BOOKS_SCHEMA_ID     number(22,0),
 RESERVE_CREDITS     number(22,2),
 COST_OF_REMOVAL     number(22,2),
 SALVAGE_CASH        number(22,2),
 SALVAGE_RETURNS     number(22,2),
 GAIN_LOSS           number(22,2),
 REPLACEMENT_AMOUNT  number(22,2),
 USER_ID1            varchar2(18),
 BASIS_1             number(22,2),
 BASIS_2             number(22,2),
 BASIS_3             number(22,2),
 BASIS_4             number(22,2),
 BASIS_5             number(22,2),
 BASIS_6             number(22,2),
 BASIS_7             number(22,2),
 BASIS_8             number(22,2),
 BASIS_9             number(22,2),
 BASIS_10            number(22,2),
 BASIS_11            number(22,2),
 BASIS_12            number(22,2),
 BASIS_13            number(22,2),
 BASIS_14            number(22,2),
 BASIS_15            number(22,2),
 BASIS_16            number(22,2),
 BASIS_17            number(22,2),
 BASIS_18            number(22,2),
 BASIS_19            number(22,2),
 BASIS_20            number(22,2),
 BASIS_21            number(22,2),
 BASIS_22            number(22,2),
 BASIS_23            number(22,2),
 BASIS_24            number(22,2),
 BASIS_25            number(22,2),
 BASIS_26            number(22,2),
 BASIS_27            number(22,2),
 BASIS_28            number(22,2),
 BASIS_29            number(22,2),
 BASIS_30            number(22,2),
 BASIS_31            number(22,2),
 BASIS_32            number(22,2),
 BASIS_33            number(22,2),
 BASIS_34            number(22,2),
 BASIS_35            number(22,2),
 BASIS_36            number(22,2),
 BASIS_37            number(22,2),
 BASIS_38            number(22,2),
 BASIS_39            number(22,2),
 BASIS_40            number(22,2),
 BASIS_41            number(22,2),
 BASIS_42            number(22,2),
 BASIS_43            number(22,2),
 BASIS_44            number(22,2),
 BASIS_45            number(22,2),
 BASIS_46            number(22,2),
 BASIS_47            number(22,2),
 BASIS_48            number(22,2),
 BASIS_49            number(22,2),
 BASIS_50            number(22,2),
 BASIS_51            number(22,2),
 BASIS_52            number(22,2),
 BASIS_53            number(22,2),
 BASIS_54            number(22,2),
 BASIS_55            number(22,2),
 BASIS_56            number(22,2),
 BASIS_57            number(22,2),
 BASIS_58            number(22,2),
 BASIS_59            number(22,2),
 BASIS_60            number(22,2),
 BASIS_61            number(22,2),
 BASIS_62            number(22,2),
 BASIS_63            number(22,2),
 BASIS_64            number(22,2),
 BASIS_65            number(22,2),
 BASIS_66            number(22,2),
 BASIS_67            number(22,2),
 BASIS_68            number(22,2),
 BASIS_69            number(22,2),
 BASIS_70            number(22,2),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
) on commit preserve rows;

comment on table  UNITIZE_PEND_TRANS_TEMP is '(C) [04] Global temp table to hold the unitization pending transactions to be inserted into the real pending tables.  No primary key.';
comment on column UNITIZE_PEND_TRANS_TEMP.COMPANY_ID is 'System-assigned identifier of a particular company.';
comment on column UNITIZE_PEND_TRANS_TEMP.GL_POSTING_MO_YR is 'Accounting month of the transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.IN_SERVICE_YEAR is 'Records the month and year in which the asset represented by the pending transaction was placed in service.  (This is the engineering in service year of the work order.)  For a retirement transaction this is the out-of-service year.';
comment on column UNITIZE_PEND_TRANS_TEMP.WORK_ORDER_ID is 'System-assigned identifier of the work order number.';
comment on column UNITIZE_PEND_TRANS_TEMP.WORK_ORDER_NUMBER is 'System-assigned identifier of the unit allocation type and basis.';
comment on column UNITIZE_PEND_TRANS_TEMP.UNIT_ITEM_ID is 'System-assigned identifier of a particular record in the table.';
comment on column UNITIZE_PEND_TRANS_TEMP.PEND_TRANS_ID is 'System-assigned identifier of a particular pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.LDG_ASSET_ID is 'Records the existing CPR ledger entry, if any, which is associated with the pending transaction. For example, retirements, transfers out, and adjustment transactions would all affect existing entries.';
comment on column UNITIZE_PEND_TRANS_TEMP.LDG_DEPR_GROUP_ID is 'This field is used to communicate between the unitization routines and the post routine.';
comment on column UNITIZE_PEND_TRANS_TEMP.GL_ACCOUNT_ID is 'System-assigned identifier of a unique General Ledger account.';
comment on column UNITIZE_PEND_TRANS_TEMP.BUS_SEGMENT_ID is 'System-assigned identifier of the business segment.';
comment on column UNITIZE_PEND_TRANS_TEMP.UTILITY_ACCOUNT_ID is 'System-assigned identifier of the utility account.';
comment on column UNITIZE_PEND_TRANS_TEMP.SUB_ACCOUNT_ID is 'System-assigned identifier of the sub account.';
comment on column UNITIZE_PEND_TRANS_TEMP.FUNC_CLASS_ID is 'System-assigned identifier of the functional class.';
comment on column UNITIZE_PEND_TRANS_TEMP.RETIREMENT_UNIT_ID is  'System-assigned identifier of the retirement unit.';
comment on column UNITIZE_PEND_TRANS_TEMP.PROPERTY_GROUP_ID is  'System-assigned identifier of the property group.';
comment on column UNITIZE_PEND_TRANS_TEMP.ASSET_LOCATION_ID is  'System-assigned identifier of the asset location.';
comment on column UNITIZE_PEND_TRANS_TEMP.SUBLEDGER_INDICATOR is 'System-assigned identifier of the subledger.  Always 0 for unitization transactions';
comment on column UNITIZE_PEND_TRANS_TEMP.POSTING_QUANTITY is 'Records the quantity of the transaction being posted on the pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.POSTING_AMOUNT is 'Records the dollar amount (financial set of books) of the transaction being posted on the pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.GL_JE_CODE is 'Records the General Ledger journal entry number code used to generate a General ledger journal entry.';
comment on column UNITIZE_PEND_TRANS_TEMP.SERIAL_NUMBER is 'Records the serial number associated with the asset component by the manufacturer.';
comment on column UNITIZE_PEND_TRANS_TEMP.FERC_ACTIVITY_CODE is 'Identifier of FERC transaction activity type (1:Addition  2:Retirement)';
comment on column UNITIZE_PEND_TRANS_TEMP.ACTIVITY_CODE is 'Records the type of activity the entry represents.  Possible types for unitization transactions would include UADD, MADD, URET.';
comment on column UNITIZE_PEND_TRANS_TEMP.DESCRIPTION is 'Records a short description of the transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.LONG_DESCRIPTION is 'Records a long description of the transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.RETIRE_METHOD_ID is 'System-assigned id associated with a retirement method.  It is set to null for unitization transactions with no asset_id.';
comment on column UNITIZE_PEND_TRANS_TEMP.BOOKS_SCHEMA_ID is 'System-assigned identifier of the book schema.  The default book schema is 1(all).';
comment on column UNITIZE_PEND_TRANS_TEMP.RESERVE_CREDITS is 'Reserve_credits records the dollar amount of any other credits such as insurance proceeds associated with a retirement.';
comment on column UNITIZE_PEND_TRANS_TEMP.COST_OF_REMOVAL is 'Records the dollar amount of removal cost associated with a retirement transaction';
comment on column UNITIZE_PEND_TRANS_TEMP.SALVAGE_CASH is 'Records the dollar amount of cash salvage proceeds associated with a retirement transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.SALVAGE_RETURNS is 'Records the dollar amount of salvage returns to stock associated with a retirement transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.GAIN_LOSS is 'Records the dollar amount (financial set of books) of gain (positive) or loss (negative), if any, recognized with a retirement transaction.  For transactions where the full asset amount is removed from the accumulated reserve (i.e., the gain/loss is netted in the reserve) no amount appears in this field.';
comment on column UNITIZE_PEND_TRANS_TEMP.REPLACEMENT_AMOUNT is 'Dollar amount of a corresponding retirement of an addition.  This is in current dollars, and will be indexed back to fund the actual amount of the retirements.  The Post program creates the retirement transaction automatically when the addition posts.  The retire_method_id can be HW-FIFO or HW-CURVE. If neither is given on the pending transaction, HW-FIFO will be used.';
comment on column UNITIZE_PEND_TRANS_TEMP.USER_ID1 is 'System-assigned id associated with the pending transaction creation.  An example would be the user id associated with the analyst or field engineer who generated the pending transaction.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_1 is 'Records an amount associated with book summary level 1 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_2 is 'Records an amount associated with book summary level 2 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_3 is 'Records an amount associated with book summary level 3 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_4 is 'Records an amount associated with book summary level 4 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_5 is 'Records an amount associated with book summary level 5 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_6 is 'Records an amount associated with book summary level 6 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_7 is 'Records an amount associated with book summary level 7 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_8 is 'Records an amount associated with book summary level 8 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_9 is 'Records an amount associated with book summary level 9 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_10 is 'Records an amount associated with book summary level 10 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_11 is 'Records an amount associated with book summary level 11 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_12 is 'Records an amount associated with book summary level 12 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_13 is 'Records an amount associated with book summary level 13 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_14 is 'Records an amount associated with book summary level 14 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_15 is 'Records an amount associated with book summary level 15 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_16 is 'Records an amount associated with book summary level 16 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_17 is 'Records an amount associated with book summary level 17 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_18 is 'Records an amount associated with book summary level 18 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_19 is 'Records an amount associated with book summary level 19 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_20 is 'Records an amount associated with book summary level 20 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_21 is 'Records an amount associated with book summary level 21 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_22 is 'Records an amount associated with book summary level 22 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_23 is 'Records an amount associated with book summary level 23 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_24 is 'Records an amount associated with book summary level 24 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_25 is 'Records an amount associated with book summary level 25 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_26 is 'Records an amount associated with book summary level 26 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_27 is 'Records an amount associated with book summary level 27 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_28 is 'Records an amount associated with book summary level 28 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_29 is 'Records an amount associated with book summary level 29 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_30 is 'Records an amount associated with book summary level 30 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_31 is 'Records an amount associated with book summary level 31 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_32 is 'Records an amount associated with book summary level 32 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_33 is 'Records an amount associated with book summary level 33 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_34 is 'Records an amount associated with book summary level 34 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_35 is 'Records an amount associated with book summary level 35 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_36 is 'Records an amount associated with book summary level 36 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_37 is 'Records an amount associated with book summary level 37 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_38 is 'Records an amount associated with book summary level 38 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_39 is 'Records an amount associated with book summary level 39 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_40 is 'Records an amount associated with book summary level 40 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_41 is 'Records an amount associated with book summary level 41 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_42 is 'Records an amount associated with book summary level 42 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_43 is 'Records an amount associated with book summary level 43 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_44 is 'Records an amount associated with book summary level 44 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_45 is 'Records an amount associated with book summary level 45 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_46 is 'Records an amount associated with book summary level 46 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_47 is 'Records an amount associated with book summary level 47 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_48 is 'Records an amount associated with book summary level 48 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_49 is 'Records an amount associated with book summary level 49 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_50 is 'Records an amount associated with book summary level 50 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_51 is 'Records an amount associated with book summary level 51 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_52 is 'Records an amount associated with book summary level 52 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_53 is 'Records an amount associated with book summary level 53 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_54 is 'Records an amount associated with book summary level 54 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_55 is 'Records an amount associated with book summary level 55 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_56 is 'Records an amount associated with book summary level 56 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_57 is 'Records an amount associated with book summary level 57 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_58 is 'Records an amount associated with book summary level 58 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_59 is 'Records an amount associated with book summary level 59 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_60 is 'Records an amount associated with book summary level 60 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_61 is 'Records an amount associated with book summary level 61 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_62 is 'Records an amount associated with book summary level 62 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_63 is 'Records an amount associated with book summary level 63 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_64 is 'Records an amount associated with book summary level 64 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_65 is 'Records an amount associated with book summary level 65 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_66 is 'Records an amount associated with book summary level 66 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_67 is 'Records an amount associated with book summary level 67 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_68 is 'Records an amount associated with book summary level 68 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_69 is 'Records an amount associated with book summary level 69 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.BASIS_70 is 'Records an amount associated with book summary level 70 in dollars.';
comment on column UNITIZE_PEND_TRANS_TEMP.TIME_STAMP is 'System-assigned timestamp used for audit purposes.';
comment on column UNITIZE_PEND_TRANS_TEMP.USER_ID is 'System-assigned user id used for audit purposes.';

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'AUTO101 - FAST NO LOOP',
          'No',
          'dw_yes_no;1',
          '"Yes" will cause auto unitization processing to avoid looping by individual work orders and is faster. "No" will cause auto unitization processing to loop by individual work orders.  "No" is the default.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1019, 0, 10, 4, 2, 0, 35810, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035810_cwip_fast101.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;