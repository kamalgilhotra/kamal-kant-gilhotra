/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010765_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/10/2012 Julia Breuer   Point Release
||============================================================================
*/

insert into PWRPLANT.PT_PARCEL_FLEX_FIELDS
   (PARCEL_ID)
   select PARCEL_ID
     from PWRPLANT.PT_PARCEL
    where PARCEL_ID not in (select PARCEL_ID from PWRPLANT.PT_PARCEL_FLEX_FIELDS);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (218, 0, 10, 4, 1, 0, 10765, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010765_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
