/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030536_lease_ilr_init_wizard.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Kyle Peterson  Point release
||============================================================================
*/

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_ilrcntr_wksp_init_lease'
 where WORKSPACE_IDENTIFIER = 'initiate_ilr_new'
 and MODULE = 'LESSEE';

alter table LS_ILR
   drop (CAPITAL_YN_SW, TAX_CAPITAL_YN_SW, LEASE_AIR_SW);

alter table LS_ILR modify FINAL_EOT_DATE date null;

alter table LS_ILR modify LEASE_GROUP_ID null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (440, 0, 10, 4, 1, 0, 30536, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030536_lease_ilr_init_wizard.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
