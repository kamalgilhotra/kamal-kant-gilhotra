--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_043117_proptax_bills_search_system_control_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/06/2015 Anand R          System control for bills search workspace grid
--||============================================================================
--*/

insert into PPBASE_SYSTEM_OPTIONS (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values ('Bills Center - Search - Display Type Code', 'Whether type code is retrieved in the grid for the Bills Center Search workspace. This is done to increase the grid retrieval performance.', 0, 'No', null, 1, 0);

insert into PPBASE_SYSTEM_OPTIONS_MODULE ( SYSTEM_OPTION_ID, MODULE)
values ('Bills Center - Search - Display Type Code', 'proptax');

insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE)
values ('Bills Center - Search - Display Type Code', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE)
values ('Bills Center - Search - Display Type Code', 'No');

insert into PPBASE_SYSTEM_OPTIONS_WKSP( SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER)
values ( 'Bills Center - Search - Display Type Code', 'bills_search');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2361, 0, 2015, 1, 0, 0, 43117, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043117_proptax_bills_search_system_control_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;