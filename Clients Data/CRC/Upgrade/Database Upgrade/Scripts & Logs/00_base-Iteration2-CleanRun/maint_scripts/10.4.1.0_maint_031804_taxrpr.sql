/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031804_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 08/26/2013 Andrew Scott        New Tax Repair Report
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, SPECIAL_NOTE, REPORT_NUMBER,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW)
   select MAX_ID + 1,
          'General/Blended Results by Batch',
          'General Method: Displays the tax repair amount calculated for a specific batch for work orders tested using the General or Blended methods',
          'dw_rpr_rpt_cwip_gen_blend',
          'REPAIRS',
          'GEN - 1310',
          13,
          101,
          40,
          34,
          1,
          3,
          0
     from DUAL, (select max(REPORT_ID) MAX_ID from PP_REPORTS);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (544, 0, 10, 4, 1, 0, 31804, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031804_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
