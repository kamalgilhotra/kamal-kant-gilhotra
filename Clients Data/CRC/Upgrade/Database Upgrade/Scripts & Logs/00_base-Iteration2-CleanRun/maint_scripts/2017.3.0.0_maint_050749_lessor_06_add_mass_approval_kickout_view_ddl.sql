/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050749_lessor_06_add_mass_approval_kickout_view_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.3.0.0 04/04/2018 Andrew Hill       Add view to see all errors from mass approval
||============================================================================
*/

CREATE OR REPLACE VIEW v_lsr_ilr_mass_app_kickouts AS
SELECT DISTINCT ilr_id, revision, message
FROM lsr_ilr_schedule_kickouts
UNION ALL
SELECT DISTINCT ilr_id, revision, message
FROM lsr_ilr_approval_kickouts;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4267, 0, 2017, 3, 0, 0, 50749, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050749_lessor_06_add_mass_approval_kickout_view_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;