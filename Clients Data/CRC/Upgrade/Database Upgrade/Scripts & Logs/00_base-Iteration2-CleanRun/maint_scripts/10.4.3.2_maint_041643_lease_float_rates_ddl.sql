/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_041643_lease_float_rates_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 01/18/2015 		B.Beck			Added lease id relationship to the floating rates
||==========================================================================================
*/

alter table ls_ilr_group_floating_rates
add ( lease_id number(22,0) );

comment on column ls_ilr_group_floating_rates.lease_id is 'The lease for the ilr groups rate';

alter table ls_ilr_group_floating_rates drop primary key drop index;

truncate table ls_ilr_group_floating_rates;

alter table ls_ilr_group_floating_rates
add constraint pk_ls_ilr_group_floating_rates
primary key (ilr_group_id, lease_id, effective_date);



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2223, 0, 10, 4, 3, 2, 041643, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041643_lease_float_rates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;