/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036074_reg_PKG_REG_COMMON.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/05/2014 Shane "C" Ward
||============================================================================
*/

create or replace package PKG_REG_COMMON
/*
||============================================================================
|| Application: PowerPlant (Rates and Regulatory)
|| Object Name: PKG_REG_COMMON
|| Description: Reg Annualization Functions.
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version Date        Revised By         Reason for Change
|| ------- ----------  ---------------    ------------------------------------
|| 1.0     02/05/2014  Shane "C" Ward     Create
||============================================================================
*/

 as
   function f_RoundNumber(a_case_Id       number,
                          a_company_id    NUMBER,
                          a_lookup_value  varchar2,
                          a_input_number  NUMBER) RETURN number;

end PKG_REG_COMMON;
/


Create or Replace package body PKG_REG_COMMON as

   --**************************************************************
   --       TYPE DECLARATIONS
   --**************************************************************


   --**************************************************************
   --       VARIABLES
   --**************************************************************


   --**************************************************************
   --       FUNCTIONS
   --**************************************************************
   function F_ROUNDNUMBER(A_CASE_ID      number,
                          a_company_id NUMBER,
                          A_LOOKUP_VALUE varchar2,
                          A_INPUT_NUMBER NUMBER
                          ) return number is

      ROUND_NUMBER number;
      SCALE        number;
      percent_flag NUMBER;
   begin
      --Check for nulls
      if A_CASE_ID is null then
         RAISE_APPLICATION_ERROR(-20000, 'Case ID is null');
      end if;

      IF a_company_id IS NULL THEN
         Raise_Application_Error(-20001, 'Company ID is null');
      END IF;

      if A_LOOKUP_VALUE is null then
         RAISE_APPLICATION_ERROR(-20002, 'Lookup Value is null');
      end if;

      IF a_input_number IS NULL THEN
          RETURN NULL;
      END IF;

      IF a_company_id = 0 THEN

        BEGIN
        select INPUT_SCALE, percentage_flag
          into SCALE, percent_flag
          from REG_JUR_INPUT_SCALE S, REG_CASE C, reg_input_scale i
          where C.REG_CASE_ID = A_CASE_ID
            and i.scale_id = s.scale_id
            and C.REG_JUR_TEMPLATE_ID = S.REG_JUR_TEMPLATE_ID
            and Upper(i.DESCRIPTION) = Upper(A_LOOKUP_VALUE);

        EXCEPTION
          WHEN No_Data_Found THEN
            RETURN a_input_number;
        END;

      ELSIF a_case_id = 0 THEN

        BEGIN
        select INPUT_SCALE, percentage_flag
          into SCALE, percent_flag
          from REG_CO_INPUT_SCALE S, reg_input_scale i
          where s.reg_company_id = A_company_id
            and i.scale_id = s.scale_id
            and Upper(i.DESCRIPTION) = Upper(A_LOOKUP_VALUE);

        EXCEPTION
          WHEN No_Data_Found THEN
            RETURN a_input_number;
        END;
      --Else both are 0
      ELSE
        RETURN a_input_number;
      END IF;

      IF percent_flag = 1 THEN
        scale := scale+2;
      END IF;

      ROUND_NUMBER := ROUND(A_INPUT_NUMBER, SCALE);

      return ROUND_NUMBER;

   end F_ROUNDNUMBER;


end PKG_REG_COMMON;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (949, 0, 10, 4, 2, 0, 36074, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036074_reg_PKG_REG_COMMON.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;