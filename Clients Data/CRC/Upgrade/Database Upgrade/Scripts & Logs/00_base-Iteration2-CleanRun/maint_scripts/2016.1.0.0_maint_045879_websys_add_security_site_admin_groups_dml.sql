/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045879_websys_add_security_site_admin_groups_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 08/08/2016 Jared Watkins			 Insert default admin groups into web security tables; also insert
||                                            an invisible security key for the security site and grant it to admin groups
||============================================================================
*/

insert into pp_web_security_groups(groups, description, custom_web)
select 'system', '', 0 from dual
where not exists (
  select 1 from pp_web_security_groups
  where lower(groups) = 'system'
);

insert into pp_web_security_groups(groups, description, custom_web)
select 'system_admin', '', 0 from dual
where not exists (
  select 1 from pp_web_security_groups
  where lower(groups) = 'system_admin'
)
and exists (
  select 1 from pp_security_groups
  where lower(groups) = 'system_admin'
);

insert into pp_web_security_groups(groups, description, custom_web)
select 'web_admin', 'Users with full administrative access to web security', 1 from dual
where not exists (
  select 1 from pp_web_security_groups
  where lower(groups) = 'web_admin'
);

insert into pp_web_security_users_groups(users, groups, mail_id)
select users, groups, mail_id from pp_security_users_groups a
where lower(groups) = 'system'
and not exists (
  select 1 from pp_web_security_users_groups b
  where lower(a.users) = lower(b.users)
  and lower(a.groups) = lower(b.groups)
);

insert into pp_web_security_users_groups(users, groups, mail_id)
select users, groups, mail_id from pp_security_users_groups a
where exists (
  select 1 from pp_security_groups c
  where lower(groups) = 'system_admin'
  and a.groups = c.groups
)
and lower(groups) = 'system_admin'
and not exists (
  select 1 from pp_web_security_users_groups b
  where lower(a.users) = lower(b.users)
  and lower(a.groups) = lower(b.groups)
);

insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'Security.Admin.All', 8, -1, '', '', '' from dual
where not exists (
  select 1 from pp_web_security_perm_control
  where objects = 'Security.Admin.All'
);

insert into pp_web_security_permission(objects, principal_type, principal, permission)
select 'Security.Admin.All', 'G', 'system', 'G' from dual
where not exists (
  select 1 from pp_web_security_permission
  where objects = 'Security.Admin.All'
  and lower(principal) = 'system'
  and principal_type = 'G'
)
and exists (
  select 1 from pp_web_security_groups
  where lower(groups) = 'system'
);

insert into pp_web_security_permission(objects, principal_type, principal, permission)
select 'Security.Admin.All', 'G', 'system_admin', 'G' from dual
where not exists (
  select 1 from pp_web_security_permission
  where objects = 'Security.Admin.All'
  and lower(principal) = 'system_admin'
  and principal_type = 'G'
)
and exists (
  select 1 from pp_security_groups
  where lower(groups) = 'system_admin'
)
and exists (
  select 1 from pp_web_security_groups
  where lower(groups) = 'system_admin'
);

insert into pp_web_security_permission(objects, principal_type, principal, permission)
select 'Security.Admin.All', 'G', 'web_admin', 'G' from dual
where not exists (
  select 1 from pp_web_security_permission
  where objects = 'Security.Admin.All'
  and lower(principal) = 'web_admin'
  and principal_type = 'G'
)
and exists (
  select 1 from pp_web_security_groups
  where lower(groups) = 'web_admin'
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3251, 0, 2016, 1, 0, 0, 045879, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045879_websys_add_security_site_admin_groups_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;

