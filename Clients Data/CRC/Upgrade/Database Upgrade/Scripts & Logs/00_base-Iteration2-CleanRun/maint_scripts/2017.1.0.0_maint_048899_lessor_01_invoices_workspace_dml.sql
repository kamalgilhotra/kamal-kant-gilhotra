/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048899_lessor_01_invoices_workspace_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/06/2017 Sarah Byers      Update detailed invoices workspace
||============================================================================
*/
update ppbase_workspace
  set workspace_uo_name = 'uo_lsr_invoice_review_wksp'
 where module = 'LESSOR'
  and workspace_identifier = 'control_invoices';

update ppbase_menu_items
  set enable_yn = 1
 where module = 'LESSOR'
  and workspace_identifier = 'control_invoices';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3883, 0, 2017, 1, 0, 0, 48899, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048899_lessor_01_invoices_workspace_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;