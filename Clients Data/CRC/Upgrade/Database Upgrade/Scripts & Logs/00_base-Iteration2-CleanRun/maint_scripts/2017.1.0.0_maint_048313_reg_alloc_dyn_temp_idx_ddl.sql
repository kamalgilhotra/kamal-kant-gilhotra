 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048313_reg_alloc_dyn_temp_idx_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 06/28/2017 Sarah Byers    New temp tables for RMS dynamic allocation performance rewrite
 ||============================================================================
 */

CREATE INDEX reg_case_alloc_dyn_rat_id_idx ON reg_case_allocator_dyn (Nvl(reg_acct_type_id,0));
CREATE INDEX reg_case_alloc_dyn_sat_id_idx ON reg_case_allocator_dyn (Nvl(sub_acct_type_id,0));
CREATE INDEX reg_case_alloc_dyn_targ_id_idx ON reg_case_allocator_dyn (Nvl(reg_alloc_target_id,0));

CREATE INDEX reg_case_adyn_list_temp_idx1 ON reg_case_alloc_dyn_list_temp (processed);
create index reg_case_alloc_acct_praa_idx ON REG_CASE_ALLOC_ACCOUNT("REG_CASE_ID","REG_ACCT_ID","PARENT_REG_ALLOC_ACCT_ID");


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3549, 0, 2017, 1, 0, 0, 48313, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048313_reg_alloc_dyn_temp_idx_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
