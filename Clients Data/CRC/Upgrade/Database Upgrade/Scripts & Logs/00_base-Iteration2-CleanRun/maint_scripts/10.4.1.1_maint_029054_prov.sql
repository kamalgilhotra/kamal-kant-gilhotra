/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029054_prov.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Nathan Hollis  Point Release
||============================================================================
*/

alter table TAX_ACCRUAL_REP_CRITERIA_TMP modify DESCRIPTION varchar2(2000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (664, 0, 10, 4, 1, 1, 29054, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_029054_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;