/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/13/2013 Brandon Beck   Point release
||============================================================================
*/

---30762
create sequence LS_VENDOR_SEQ
   minvalue 1
   start with 1
   increment by 1
   cache 20;

--* Bring sequence up to the max vendor id to avoid overlapping ID's
select LS_VENDOR_SEQ.NEXTVAL
  from ALL_OBJECTS
 where ROWNUM <= (select max(VENDOR_ID) from LS_VENDOR);

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_admincntr_lessor_wksp'
 where WORKSPACE_IDENTIFIER = 'admin_lessors'
 and MODULE = 'LESSEE';

alter table LS_VENDOR rename column EXTERNAL_VENDOR_ID to EXTERNAL_VENDOR_NUMBER;

alter table LS_LESSOR drop column EXTERNAL_VENDOR_ID;


-- 30772
--CREATE PENDING TRANSACTION ARCHIVE TABLE WITH KEYS
create table LS_PEND_TRANSACTION_ARC
(
 LS_PEND_TRANS_ID    number(22,0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 LS_ASSET_ID         number(22,0),
 POSTING_AMOUNT      number(22,2),
 POSTING_QUANTITY    number(22,2),
 ACTIVITY_CODE       char(5),
 GL_JE_CODE          char(18),
 RETIREMENT_UNIT_ID  number(22),
 UTILITY_ACCOUNT_ID  number(22),
 BUS_SEGMENT_ID      number(22),
 FUNC_CLASS_ID       number(22),
 ILR_ID              number(22),
 COMPANY_ID          number(22),
 ASSET_LOCATION_ID   number(22),
 PROPERTY_GROUP_ID   number(22),
 LEASED_ASSET_NUMBER varchar2(35),
 SUB_ACCOUNT_ID      number(22),
 WORK_ORDER_ID       number(22),
 SERIAL_NUMBER       varchar2(35)
);

alter table LS_PEND_TRANSACTION_ARC
   add constraint PK_LS_PEND_TRANSACTION_ARC
       primary key (LS_PEND_TRANS_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PEND_TRANSACTION_ARC
   add constraint FK1_LS_PEND_TRANSACTION_ARC
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

--CREATE PENDING BASIS ARCHIVE TABLE WITH KEYS

create table LS_PEND_BASIS_ARC
(
 LS_PEND_TRANS_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 BASIS_1          number(22,2) not null,
 BASIS_2          number(22,2) not null,
 BASIS_3          number(22,2) not null,
 BASIS_4          number(22,2) not null,
 BASIS_5          number(22,2) not null,
 BASIS_6          number(22,2) not null,
 BASIS_7          number(22,2) not null,
 BASIS_8          number(22,2) not null,
 BASIS_9          number(22,2) not null,
 BASIS_10         number(22,2) not null,
 BASIS_11         number(22,2) not null,
 BASIS_12         number(22,2) not null,
 BASIS_13         number(22,2) not null,
 BASIS_14         number(22,2) not null,
 BASIS_15         number(22,2) not null,
 BASIS_16         number(22,2) not null,
 BASIS_17         number(22,2) not null,
 BASIS_18         number(22,2) not null,
 BASIS_19         number(22,2) not null,
 BASIS_20         number(22,2) not null,
 BASIS_21         number(22,2) not null,
 BASIS_22         number(22,2) not null,
 BASIS_23         number(22,2) not null,
 BASIS_24         number(22,2) not null,
 BASIS_25         number(22,2) not null,
 BASIS_26         number(22,2) not null,
 BASIS_27         number(22,2) not null,
 BASIS_28         number(22,2) not null,
 BASIS_29         number(22,2) not null,
 BASIS_30         number(22,2) not null,
 BASIS_31         number(22,2) not null,
 BASIS_32         number(22,2) not null,
 BASIS_33         number(22,2) not null,
 BASIS_34         number(22,2) not null,
 BASIS_35         number(22,2) not null,
 BASIS_36         number(22,2) not null,
 BASIS_37         number(22,2) not null,
 BASIS_38         number(22,2) not null,
 BASIS_39         number(22,2) not null,
 BASIS_40         number(22,2) not null,
 BASIS_41         number(22,2) not null,
 BASIS_42         number(22,2) not null,
 BASIS_43         number(22,2) not null,
 BASIS_44         number(22,2) not null,
 BASIS_45         number(22,2) not null,
 BASIS_46         number(22,2) not null,
 BASIS_47         number(22,2) not null,
 BASIS_48         number(22,2) not null,
 BASIS_49         number(22,2) not null,
 BASIS_50         number(22,2) not null,
 BASIS_51         number(22,2) not null,
 BASIS_52         number(22,2) not null,
 BASIS_53         number(22,2) not null,
 BASIS_54         number(22,2) not null,
 BASIS_55         number(22,2) not null,
 BASIS_56         number(22,2) not null,
 BASIS_57         number(22,2) not null,
 BASIS_58         number(22,2) not null,
 BASIS_59         number(22,2) not null,
 BASIS_60         number(22,2) not null,
 BASIS_61         number(22,2) not null,
 BASIS_62         number(22,2) not null,
 BASIS_63         number(22,2) not null,
 BASIS_64         number(22,2) not null,
 BASIS_65         number(22,2) not null,
 BASIS_66         number(22,2) not null,
 BASIS_67         number(22,2) not null,
 BASIS_68         number(22,2) not null,
 BASIS_69         number(22,2) not null,
 BASIS_70         number(22,2) not null
);

alter table LS_PEND_BASIS_ARC
   add constraint PK_LS_PEND_BASIS_ARC
       primary key (LS_PEND_TRANS_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PEND_BASIS_ARC
   add constraint FK1_LS_PEND_BASIS_ARC
       foreign key (LS_PEND_TRANS_ID)
       references LS_PEND_TRANSACTION (LS_PEND_TRANS_ID);

--CREATE PENDING CLASS CODE ARCHIVE TABLE WITH KEYS

create table LS_PEND_CLASS_CODE_ARC
(
 CLASS_CODE_ID    number(22,0) not null,
 LS_PEND_TRANS_ID number(22,0) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 VALUE            varchar2(254) not null
);

alter table LS_PEND_CLASS_CODE_ARC
   add constraint PK_LS_PEND_CLASS_CODE_ARC
   primary key (CLASS_CODE_ID, LS_PEND_TRANS_ID)
   using index tablespace PWRPLANT_IDX;

alter table LS_PEND_CLASS_CODE_ARC
   add constraint FK1_LS_PEND_CLASS_CODE_ARC
       foreign key (CLASS_CODE_ID)
       references CLASS_CODE (CLASS_CODE_ID);

alter table LS_PEND_CLASS_CODE_ARC
   add constraint FK2_LS_PEND_CLASS_CODE_ARC
       foreign key (LS_PEND_TRANS_ID)
       references LS_PEND_TRANSACTION (LS_PEND_TRANS_ID);

--Add columns to LS_PEND_TRANSACTION tables

alter table LS_PEND_TRANSACTION
   add (RETIREMENT_UNIT_ID  number(22),
        UTILITY_ACCOUNT_ID  number(22),
        BUS_SEGMENT_ID      number(22),
        FUNC_CLASS_ID       number(22),
        ILR_ID              number(22),
        COMPANY_ID          number(22),
        ASSET_LOCATION_ID   number(22),
        PROPERTY_GROUP_ID   number(22),
        LEASED_ASSET_NUMBER varchar2(35),
        SUB_ACCOUNT_ID      number(22),
        WORK_ORDER_ID       number(22),
        SERIAL_NUMBER       varchar2(35));

-- 30773

create table LS_CPR_ASSET_MAP
(
 LS_ASSET_ID    number(22,0) not null,
 ASSET_ID       number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 EFFECTIVE_DATE date
);

alter table LS_CPR_ASSET_MAP
   add constraint PK_LS_CPR_ASSET_MAP
       primary key (LS_ASSET_ID, ASSET_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_CPR_ASSET_MAP
   add constraint FK1_LS_CPR_ASSET_MAP
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

alter table LS_CPR_ASSET_MAP
   add constraint FK2_LS_CPR_ASSET_MAP
       foreign key (ASSET_ID)
       references CPR_LEDGER (ASSET_ID);

insert into LS_CPR_ASSET_MAP
   (LS_ASSET_ID, ASSET_ID, TIME_STAMP, USER_ID, EFFECTIVE_DATE)
   select LS_ASSET_ID, LA.ASSET_ID, sysdate, user, CPR_LEDGER.IN_SERVICE_YEAR
     from LS_ASSET LA, CPR_LEDGER
    where not exists (select 1 from LS_CPR_ASSET_MAP where LS_ASSET_ID = LA.LS_ASSET_ID)
      and LA.ASSET_ID = CPR_LEDGER.ASSET_ID;

alter table LS_ASSET drop constraint FK_ASSET_CPR_ASSET;

alter table LS_ASSET drop column ASSET_ID;


-- 30841
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3001, 'Leased Asset Addition Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3002, 'Leased Asset Addition ST Obligation Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3003, 'Leased Asset Addition LT Obligation Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3010, 'Lease Monthly Interest Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3011, 'Lease Monthly Interest Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3012, 'Lease Monthly Executory Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3013, 'Lease Monthly Executory Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3014, 'Lease Monthly Contingent Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3015, 'Lease Monthly Contingent Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3018, 'Lease Payment ST Obligation Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3019, 'Lease Payment Interest Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3020, 'Lease Payment Executory Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3021, 'Lease Payment Contingent Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3022, 'Lease Payment Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3023, 'Lease Obligation Reclass Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3024, 'Lease Obligation Reclass Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3025, 'Lease Retirement Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3026, 'Lease Retirement Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3027, 'Lease Gain/Loss Accum Reserve Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3028, 'Lease Gain/Loss ST Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3029, 'Lease Gain/Loss LT Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3030, 'Lease Gain/Loss Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3031, 'Lease Gain/Loss Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3032, 'Lease Depreciation Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3033, 'Lease Depreciation Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3034, 'Lease Transfer to Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3035, 'Lease Transfer from Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3036, 'Lease Termination Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3037, 'Lease Termination Credit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3038, 'Lease Residual Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3039, 'Lease Residual Credit');


-- 31030
--
-- Create ls_ild_asset_map
-- holds the assets included in a given revision for an ilr

create table LS_ILR_ASSET_MAP
(
 ILR_ID      number(22,0) not null,
 LS_ASSET_ID number(22,0) not null,
 REVISION    number(22,0) not null,
 USER_ID     varchar2(18),
 TIME_STAMP  date
);

alter table LS_ILR_ASSET_MAP
   add constraint LS_ILR_ASSET_MAP_PK
       primary key (ILR_ID, LS_ASSET_ID, REVISION)
       using index tablespace PWRPLANT_IDX;

alter table LS_ILR_ASSET_MAP
   add constraint R_ILR_ASSET_MAP_ILR_ID
       foreign key (ILR_ID, REVISION)
       references LS_ILR_APPROVAL (ILR_ID, REVISION);

alter table LS_ILR_ASSET_MAP
   add constraint R_ILR_ASSET_MAP_ASSET_ID
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

--* Populate ls_ilr_asset_map with all existing ls_assets
insert into LS_ILR_ASSET_MAP
   (ILR_ID, LS_ASSET_ID, REVISION, USER_ID, TIME_STAMP)
   select ILR.ILR_ID, A.LS_ASSET_ID, APPR.REVISION, user, sysdate
     from LS_ILR ILR, LS_ASSET A, LS_ILR_APPROVAL APPR
    where A.ILR_ID = ILR.ILR_ID
      and APPR.ILR_ID = ILR.ILR_ID;


-- 31036
alter table LS_PROCESS_CONTROL rename column PROCESS_1  to DEPR_CALC;
alter table LS_PROCESS_CONTROL rename column PROCESS_2  to DEPR_APPROVED;
alter table LS_PROCESS_CONTROL rename column PROCESS_3  to ACCR_CALC;
alter table LS_PROCESS_CONTROL rename column PROCESS_4  to ACCR_APPROVED;
alter table LS_PROCESS_CONTROL rename column PROCESS_5  to PAYMENT_CALC;
alter table LS_PROCESS_CONTROL rename column PROCESS_6  to PAYMENT_APPROVED;
alter table LS_PROCESS_CONTROL rename column PROCESS_7  to IMPORT_INVOICES;
alter table LS_PROCESS_CONTROL rename column PROCESS_8  to PAYMENT_INVOICE_RECON;
alter table LS_PROCESS_CONTROL rename column PROCESS_9  to PAYMENT_SENT_APPROVAL;
alter table LS_PROCESS_CONTROL rename column PROCESS_10 to OPEN_NEXT;

alter table LS_LEASE_GROUP add PAYMENT_FLG number(1,0);

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_mecntr_wksp'
 where WORKSPACE_IDENTIFIER = 'control_monthend'
 and MODULE = 'LESSEE';


-- 31265
alter table LS_LESSOR add EXTERNAL_LESSOR_NUMBER varchar2(35);

create unique index EXT_LESSOR_NUM_IDX
   on LS_LESSOR (EXTERNAL_LESSOR_NUMBER)
   tablespace PWRPLANT_IDX;


-- 31235
create table LS_PAYMENT_TYPE
(
 PAYMENT_TYPE_ID number(22,0) not null,
 DESCRIPTION     varchar2(35)
);

alter table LS_PAYMENT_TYPE
   add constraint PK_LS_PAYMENT_TYPE
       primary key (PAYMENT_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

create table LS_PAYMENT_HDR
(
 PAYMENT_ID       number(22,0) not null,
 LEASE_ID         number(22,0),
 VENDOR_ID        number(22,0),
 COMPANY_ID       number(22,0),
 AMOUNT           number(22,2),
 DESCRIPTION      varchar2(35),
 GL_POSTING_MO_YR date
);

alter table LS_PAYMENT_HDR
   add constraint PK_LS_PAYMENT_HEADER
       primary key (PAYMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PAYMENT_HDR
   add constraint FK_LS_PAYMENT_HEADER1
       foreign key (LEASE_ID)
       references LS_LEASE(LEASE_ID);

alter table LS_PAYMENT_HDR
   add constraint FK_LS_PAYMENT_HEADER2
       foreign key (VENDOR_ID)
       references LS_VENDOR(VENDOR_ID);

alter table LS_PAYMENT_HDR
   add constraint FK_LS_PAYMENT_HEADER3
       foreign key (COMPANY_ID)
       references COMPANY_SETUP(COMPANY_ID);

create table LS_PAYMENT_LINE
(
 PAYMENT_ID          number(22,0) not null,
 PAYMENT_LINE_NUMBER number(22,0) not null,
 PAYMENT_TYPE_ID     number(22,0) not null,
 LS_ASSET_ID         number(22,0),
 AMOUNT              number(22,2),
 GL_POSTING_MO_YR    date,
 DESCRIPTION         varchar2(35)
);

alter table LS_PAYMENT_LINE
   add constraint PK_LS_PAYMENT_LINE
       primary key (PAYMENT_ID, PAYMENT_LINE_NUMBER, PAYMENT_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PAYMENT_LINE
   add constraint FK_LS_PAYMENT_LINE1
       foreign key (PAYMENT_ID)
       references LS_PAYMENT_HDR(PAYMENT_ID);

alter table LS_PAYMENT_LINE
   add constraint FK_LS_PAYMENT_LINE2
       foreign key (LS_ASSET_ID)
       references LS_ASSET(LS_ASSET_ID);

alter table LS_PAYMENT_LINE
   add constraint FK_LS_PAYMENT_LINE3
       foreign key (PAYMENT_TYPE_ID)
       references LS_PAYMENT_TYPE(PAYMENT_TYPE_ID);

create table LS_INVOICE
(
 INVOICE_ID         number(22,0) not null,
 COMPANY_ID         number(22,0),
 LEASE_ID           number(22,0),
 GL_POSTING_MO_YR   date,
 VENDOR_ID          number(22,0),
 INVOICE_STATUS     number(22,0),
 INVOICE_NUMBER     varchar2(35),
 INVOICE_AMOUNT     number(22,2),
 INVOICE_INTEREST   number(22,2),
 INVOICE_EXECUTORY  number(22,2),
 INVOICE_CONTINGENT number(22,2)
);

alter table LS_INVOICE
   add constraint PK_LS_INVOICE
       primary key (INVOICE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_INVOICE
   add constraint FK_LS_INVOICE1
       foreign key (COMPANY_ID)
       references COMPANY_SETUP(COMPANY_ID);

alter table LS_INVOICE
   add constraint FK_LS_INVOICE2
       foreign key (LEASE_ID)
       references LS_LEASE(LEASE_ID);

alter table LS_INVOICE
   add constraint FK_LS_INVOICE3
       foreign key (VENDOR_ID)
       references LS_VENDOR(VENDOR_ID);

create table LS_INVOICE_LINE
(
 INVOICE_ID          number(22,0) not null,
 INVOICE_LINE_NUMBER number(22,0) not null,
 PAYMENT_TYPE_ID     number(22,0) not null,
 LS_ASSET_ID         number(22,0) not null,
 GL_POSTING_MO_YR    date,
 AMOUNT              number(22,2)
);

alter table LS_INVOICE_LINE
   add constraint PK_LS_INVOICE_LINE
       primary key (INVOICE_ID, INVOICE_LINE_NUMBER, PAYMENT_TYPE_ID, LS_ASSET_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_INVOICE_LINE
   add constraint FK_LS_INVOICE_LINE1
       foreign key (INVOICE_ID)
       references LS_INVOICE(INVOICE_ID);

alter table LS_INVOICE_LINE
   add constraint FK_LS_INVOICE_LINE2
       foreign key (PAYMENT_TYPE_ID)
       references LS_PAYMENT_TYPE(PAYMENT_TYPE_ID);

alter table LS_INVOICE_LINE
   add constraint FK_LS_INVOICE_LINE3
       foreign key (LS_ASSET_ID)
       references LS_ASSET(LS_ASSET_ID);

create table LS_INVOICE_PAYMENT_MAP
(
 INVOICE_ID number(22,0) not null,
 PAYMENT_ID number(22,0) not null
);

alter table LS_INVOICE_PAYMENT_MAP
   add constraint PK_LS_INVOICE_PAYMENT_MAP
       primary key (INVOICE_ID, PAYMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_INVOICE_PAYMENT_MAP
   add constraint FK_LS_INVOICE_PAYMENT_MAP1
       foreign key (INVOICE_ID)
       references LS_INVOICE(INVOICE_ID);

alter table LS_INVOICE_PAYMENT_MAP
   add constraint FK_LS_INVOICE_PAYMENT_MAP2
       foreign key (PAYMENT_ID)
       references LS_PAYMENT_HDR(PAYMENT_ID);

insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (1, 'Principal Payment');
insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (2, 'Interest Payment');
insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (3, 'Executory Payment');
insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (4, 'Contingent Payment');
insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (5, 'Interest Adjustment');
insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (6, 'Executory Adjustment');
insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (7, 'Contingent Adjustment');

alter table LS_LEASE_GROUP add TOLERANCE_PCT number(22,2);

alter table LS_INVOICE_PAYMENT_MAP add IN_TOLERANCE number(1,0);

create sequence LS_INVOICE_SEQ;

delete from PPBASE_MENU_ITEMS
 where MODULE = 'LESSEE'
   and MENU_IDENTIFIER = 'search_schedules';
delete from PPBASE_WORKSPACE
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'search_schedules';
delete from PPBASE_WORKSPACE
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'details_schedules';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = 4
 where MODULE = 'LESSEE'
   and MENU_IDENTIFIER = 'lessee_search';


-- 31258
alter table LS_LEASE_GROUP add WORKFLOW_TYPE_ID number(22);

--
-- START HERE
--

alter table LS_ILR_OPTIONS
   add (NET_PRESENT_VALUE    number(22,2),
        INTERNAL_RATE_RETURN number(22,8),
        CURRENT_LEASE_COST   number(22,2));

update LS_ILR_OPTIONS LO
   set (NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CURRENT_LEASE_COST) =
        (select A.NPV, A.IRR, A.CURRENT_LEASE_COST from LS_ILR A where A.ILR_ID = LO.ILR_ID);

alter table LS_ILR drop column NPV2;
alter table LS_ILR drop column AIR2;
alter table LS_ILR drop column IRR;
alter table LS_ILR drop column NPV;
alter table LS_ILR drop column BPO_PRICE;
alter table LS_ILR drop column TERM_PENALTY;
alter table LS_ILR drop column TOTAL_LEASE_TERMS;
alter table LS_ILR drop column REVISION;
alter table LS_ILR drop column CURRENT_LEASE_COST;
alter table LS_ILR drop column PYMNT_MONTH_INDICATOR;

-- pend transations should have revision
alter table LS_PEND_TRANSACTION add REVISION number(22,0);

alter table LS_LESSOR add STATUS_CODE_ID number(22,0);

alter table LS_LESSOR add MULTI_VENDOR_YN_SW number(22,0);

alter table LS_LESSOR add LEASE_GROUP_ID number(22,0);

alter table LS_PEND_BASIS_ARC drop constraint FK1_LS_PEND_BASIS_ARC;

alter table LS_PEND_CLASS_CODE_ARC drop constraint FK1_LS_PEND_CLASS_CODE_ARC;

alter table LS_PEND_CLASS_CODE_ARC drop constraint FK2_LS_PEND_CLASS_CODE_ARC;

drop view LS_DIST_DEF_CONTROL_VIEW;
drop view LS_ASSET_CPR_FIELDS_VIEW;

alter table LS_PEND_TRANSACTION add ERROR_MESSAGE varchar2(2000);

alter table LS_PEND_TRANSACTION add IN_SERVICE_DATE date;

update JE_TRANS_TYPE set DESCRIPTION = 'Leased Asset Addition Debit' where TRANS_TYPE = 3001;
update JE_TRANS_TYPE set DESCRIPTION = 'Leased Asset Addition ST Obligation Credit' where TRANS_TYPE = 3002;
update JE_TRANS_TYPE set DESCRIPTION = 'Leased Asset Addition LT Obligation Credit' where TRANS_TYPE = 3003;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Monthly Interest Debit' where TRANS_TYPE = 3010;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Monthly Interest Credit' where TRANS_TYPE = 3011;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Monthly Executory Debit' where TRANS_TYPE = 3012;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Monthly Executory Credit' where TRANS_TYPE = 3013;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Monthly Contingent Debit' where TRANS_TYPE = 3014;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Monthly Contingent Credit' where TRANS_TYPE = 3015;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Payment ST Obligation Debit' where TRANS_TYPE = 3018;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Payment Interest Debit' where TRANS_TYPE = 3019;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Payment Executory Debit' where TRANS_TYPE = 3020;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Payment Contingent Debit' where TRANS_TYPE = 3021;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Payment Credit' where TRANS_TYPE = 3022;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Obligation Reclass Debit' where TRANS_TYPE = 3023;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Obligation Reclass Credit' where TRANS_TYPE = 3024;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Retirement Debit (POST)' where TRANS_TYPE = 3025;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Retirement Credit (POST)' where TRANS_TYPE = 3026;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Gain/Loss Credit' where TRANS_TYPE = 3027;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Gain/Loss ST Debit' where TRANS_TYPE = 3028;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Gain/Loss LT Debit' where TRANS_TYPE = 3029;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Gain/Loss Debit (POST)' where TRANS_TYPE = 3030;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Gain/Loss Credit (POST)' where TRANS_TYPE = 3031;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Depreciation Debit' where TRANS_TYPE = 3032;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Depreciation Credit' where TRANS_TYPE = 3033;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Transfer to Debit (POST)' where TRANS_TYPE = 3034;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Transfer from Credit (POST)' where TRANS_TYPE = 3035;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Termination Debit' where TRANS_TYPE = 3036;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Termination Credit' where TRANS_TYPE = 3037;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Residual Debit' where TRANS_TYPE = 3038;
update JE_TRANS_TYPE set DESCRIPTION = 'Lease Residual Credit' where TRANS_TYPE = 3039;

create table LS_MONTHLY_ACCRUALS_STG
(
 ID              number(22,0) not null,
 LS_ASSET_ID     number(22,0),
 AMOUNT_CATEGORY varchar2(35),
 VENDOR_ID       number(22,0),
 AMOUNT          number(22,2),
 USER_ID         varchar2(18),
 TIME_STAMP      date
);

alter table LS_MONTHLY_ACCRUALS_STG
   add constraint PK_LS_MONTHLY_ACCRUALS
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_MONTHLY_ACCRUALS_STG
   add constraint R_LS_MONTHLY_ACCRUALS
       foreign key (LS_ASSET_ID)
       references LS_ASSET (LS_ASSET_ID);

alter table LS_LEASE rename column LS_LEASE_CAP_TYPE_ID to LEASE_CAP_TYPE_ID;

alter table LS_LEASE drop column LEASE_RULE_ID;
alter table LS_LEASE drop column CALC_APPROVAL_RULE_ID;
alter table LS_LEASE drop column PREV_MONTH_PAY_YN_SW;

drop table LS_LEASE_RULE;

alter table LS_ASSET add APPROVED_REVISION number(4, 0);

alter table LS_ASSET add IS_EARLY_RET number(1, 0);

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values
   (1, 'LESSEE', 'send_approval', 'Approval Route', 5, 'ue_sendApproval', null, null);
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values
   (2, 'LESSEE', 'new_revision', 'New Revision', 4, 'ue_newRevision', null, null);
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values
   (3, 'LESSEE', 'drill_ilrs', 'Drill to ILRs', 1, 'ue_drillILRs', null, null);
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values
   (4, 'LESSEE', 'drill_assets', 'Drill to Assets', 2, 'ue_drillAssets', null, null);
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values
   (5, 'LESSEE', 'drill_cpr', 'Drill to CPR', 3, 'ue_drillCPR', null, null);
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
values
   (6, 'LESSEE', 'new_ilr', 'New ILR', 6, 'ue_newILR', 'PWRPLANT',
    TO_DATE('30-07-2013 08:32:02', 'DD-MM-YYYY HH24:MI:SS'));

alter table LS_PEND_TRANSACTION_ARC add REVISION number(22,0);

alter table LS_LEASE add WORKFLOW_TYPE_ID number(22,0);


--31328

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_ilrcntr_wksp_approvals'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'approval_assets';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_pymntcntr_wksp'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'control_payments';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_ilrcntr_details'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'details_ilr';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_lscntr_wskp'
 where MODULE = 'LESSEE'
   and WORKSPACE_IDENTIFIER = 'details_mla';

update LS_ASSET set QUANTITY = 1 where QUANTITY is null;

alter table LS_ASSET modify QUANTITY number(22,2) not null;

--TO SET UP LESSEE SHORTCUTS
--Insert into PP_MYPP_TASK
insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Lessee', 'Lessee', null, 0, null,
    null);

insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Lessee Initiation',
    'Lessee > Lessee Initiation', (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee'), 0,
    null, null);

insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Initiate MLA',
    'Lessee > Lessee Initiation > Initiate MLA',
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Initiation'), 1, null, null);

insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Initiate ILR',
    'Lessee > Lessee Initiation > Initiate ILR',
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Initiation'), 1, null, null);

insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Initiate Asset',
    'Lessee > Lessee Initiation > Initiate Asset',
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Initiation'), 1, null, null);

--Insert into PP_MYPP_TASK_STEP

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee'), 1, null, null, 'w_top_frame',
    'm_top_frame.m_application.m_cpr', 'clicked', null);

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lessee Initiation'), 1, null, null,
    'w_asset_main', 'm_asset_main.m_application.m_lease', 'clicked', null);

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Initiate MLA'), 1, null, null,
    'w_ls_center_main', 'dw_mla_init', 'clicked', null);

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Initiate ILR'), 1, null, null,
    'w_ls_center_main', 'dw_ilr_init', 'clicked', null);

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Initiate Asset'), 1, null, null,
    'w_ls_center_main', 'dw_asset_init', 'clicked', null);


insert into WORKFLOW_TYPE
   (WORKFLOW_TYPE_ID, DESCRIPTION, SUBSYSTEM, ACTIVE, USE_LIMITS, SQL_APPROVAL_AMOUNT,
    EXTERNAL_WORKFLOW_TYPE, BASE_SQL, USER_ID, TIME_STAMP)
   select nvl(max(WORKFLOW_TYPE_ID),0)+1, 'Auto approve', 'ilr_approval, mla_approval', 1, 0, null, 'AUTO', 1, null, null
   from WORKFLOW_TYPE;


-- 31322
-- PAYMENTS

create sequence LS_PAYMENT_HDR_SEQ nocache;

create table LS_ACCRUAL_TYPE
(
 ACCRUAL_TYPE_ID number(22,0) not null,
 DESCRIPTION     varchar2(35)
);

alter table LS_ACCRUAL_TYPE
   add constraint PK_LS_ACCRUAL_TYPE
       primary key (ACCRUAL_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

insert into LS_ACCRUAL_TYPE (ACCRUAL_TYPE_ID, DESCRIPTION) values (1, 'Interest Accrual');
insert into LS_ACCRUAL_TYPE (ACCRUAL_TYPE_ID, DESCRIPTION) values (2, 'Executory Accrual');
insert into LS_ACCRUAL_TYPE (ACCRUAL_TYPE_ID, DESCRIPTION) values (3, 'Contingent Accrual');

create table LS_MONTHLY_ACCRUAL_STG
(
 ACCRUAL_ID       number(22,0) not null,
 ACCRUAL_TYPE_ID  number(22,0),
 LS_ASSET_ID      number(22,0),
 AMOUNT           number(22,2),
 GL_POSTING_MO_YR date
);

alter table LS_MONTHLY_ACCRUAL_STG
   add constraint PK_LS_MONTHLY_ACCRUAL_STG
       primary key (ACCRUAL_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_MONTHLY_ACCRUAL_STG
   add constraint FK_LS_MONTHLY_ACCRUAL_STG1
       foreign key (LS_ASSET_ID)
       references LS_ASSET(LS_ASSET_ID);

alter table LS_MONTHLY_ACCRUAL_STG
   add constraint FK_LS_MONTHLY_ACCRUAL_STG2
       foreign key (ACCRUAL_TYPE_ID)
       references LS_ACCRUAL_TYPE(ACCRUAL_TYPE_ID);

create sequence LS_MONTHLY_ACCRUAL_STG_SEQ nocache;

alter table LS_ILR       add WORKFLOW_TYPE_ID number(22,0);

alter table LS_ILR_GROUP add WORKFLOW_TYPE_ID number(22,0);

--Add Approvals to MYPOWERPLANT
--Insert MLA Approvals into pp_mypp_approval_control
insert into PP_MYPP_APPROVAL_CONTROL
   (MYPP_APPRV_ID, TIME_STAMP, USER_ID, LABEL_ONE, LABEL_MULTI, SORT_ORDER, TASK_ID, DW,
    ADDITION_ARG, sql, STATUS, TASK_WINDOW_PATH)
values
   ((select max(MYPP_APPRV_ID) + 1 from PP_MYPP_APPROVAL_CONTROL), sysdate, 'PWRPLANT',
    'MLA to Approve', 'MLA to Approve', (select max(SORT_ORDER) + 1 from PP_MYPP_APPROVAL_CONTROL),
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'MLA Approval'),
    'dw_ls_lease_approval_grid', null, null, 1, null);

--Insert ILR Approvals into pp_mypp_approval_control
insert into PP_MYPP_APPROVAL_CONTROL
   (MYPP_APPRV_ID, TIME_STAMP, USER_ID, LABEL_ONE, LABEL_MULTI, SORT_ORDER, TASK_ID, DW,
    ADDITION_ARG, sql, STATUS, TASK_WINDOW_PATH)
values
   ((select max(MYPP_APPRV_ID) + 1 from PP_MYPP_APPROVAL_CONTROL), sysdate, 'PWRPLANT',
    'ILR to Approve', 'ILR to Approve', (select max(SORT_ORDER) + 1 from PP_MYPP_APPROVAL_CONTROL),
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'ILR Approval'), 'dw_ls_ilr_approval_grid',
    null, null, 1, null);

alter table LS_ILR_GROUP drop column LEASE_ID;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (508, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
