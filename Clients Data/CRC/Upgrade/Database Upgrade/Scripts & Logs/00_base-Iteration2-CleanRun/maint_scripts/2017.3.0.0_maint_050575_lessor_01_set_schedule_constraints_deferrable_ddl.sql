/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050575_lessor_01_set_schedule_constraints_deferrable_ddl.sql
|| Description:	Make inter-schedule table constraints deferrable
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 02/16/2018 Andrew Hill    Inter-schedule table constrains need to be deferrable
||============================================================================
*/

ALTER TABLE lsr_ilr_schedule_sales_direct DROP CONSTRAINT r_lsr_ilr_sched_sales_direct3;
ALTER TABLE lsr_ilr_schedule_sales_direct ADD CONSTRAINT r_lsr_ilr_sched_sales_direct3 FOREIGN KEY (ilr_id, revision, set_of_books_id, MONTH) REFERENCES lsr_ilr_schedule(ilr_id, revision, set_of_books_id, MONTH) DEFERRABLE INITIALLY IMMEDIATE;
ALTER TABLE Lsr_Ilr_Schedule_Direct_Fin DROP CONSTRAINT lsr_ilr_sch_direct_fin_sch_fk;
ALTER TABLE lsr_ilr_schedule_direct_fin ADD CONSTRAINT lsr_ilr_sch_direct_fin_sch_fk FOREIGN KEY (ilr_id, revision, set_of_books_id, MONTH) REFERENCES lsr_ilr_schedule_sales_direct(ilr_id, revision, set_of_books_id, MONTH) DEFERRABLE INITIALLY IMMEDIATE;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4144, 0, 2017, 3, 0, 0, 50575, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050575_lessor_01_set_schedule_constraints_deferrable_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 