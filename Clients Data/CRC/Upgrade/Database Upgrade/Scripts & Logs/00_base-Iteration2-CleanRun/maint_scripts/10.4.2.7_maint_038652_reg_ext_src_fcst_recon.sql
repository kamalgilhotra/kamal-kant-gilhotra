/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038652_reg_ext_src_fcst_recon.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 06/26/2014 Shane Ward       Allow user to load to forecast recon
||                                      ledger
||============================================================================
*/

alter table REG_EXT_STAGE add VERSION varchar2(35);
comment on column REG_EXT_STAGE.VERSION is 'Allows the user to specify the different versions of the external data; this field is required when the external source is used for Forecast Reconciliation.';

alter table REG_EXT_SOURCE add FCST_RECON_USE number(22, 0);
comment on column REG_EXT_SOURCE.FCST_RECON_USE is 'Indicates if this external source data is loaded to Forecast Recon Ledger: 1 = yes/true; 0 = no/false. If ''True'' then Historic Use and Forecast Use columns must be ''False.''';

alter table REG_IMPORT_EXT_STAGE_ARC add VERSION varchar2(35);
comment on column REG_IMPORT_EXT_STAGE_ARC.VERSION is 'Allows the user to specify the different versions of the external data; this field is required when the external source is used for Forecast Reconciliation.';

alter table REG_IMPORT_EXT_STAGE_STG add VERSION varchar2(35);
comment on column REG_IMPORT_EXT_STAGE_STG.VERSION is 'Allows the user to specify the different versions of the external data; this field is required when the external source is used for Forecast Reconciliation.';

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, IS_ON_TABLE)
values
   (154, 'version', 'Version', 0, 1, 'varchar2(35)', 1);

insert into REG_EXT_STG_FIELD values ('VERSION');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1169, 0, 10, 4, 2, 7, 38652, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038652_reg_ext_src_fcst_recon.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;