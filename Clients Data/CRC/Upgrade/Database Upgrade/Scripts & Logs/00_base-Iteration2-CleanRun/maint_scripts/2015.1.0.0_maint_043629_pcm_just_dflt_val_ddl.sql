 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_043629_pcm_just_dflt_val_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/14/2015 	D. Mendel  Increasing default value field size to 4000
 ||============================================================================
 */ 

ALTER TABLE wo_doc_justification_rules MODIFY default_value VARCHAR2(4000);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2497, 0, 2015, 1, 0, 0, 43629, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043629_pcm_just_dflt_val_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;