/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038294_taxrpr_rpr_loc_import.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.4 07/21/2014 Alex P.
||========================================================================================
*/

---
--- Recreate Repair Location Import tables
---

--
-- REPAIR LOCATIONS
--
drop table RPR_IMPORT_LOCATION;

create table RPR_IMPORT_LOCATION
(
 IMPORT_RUN_ID                 number(22,0)   not null,
 LINE_ID                       number(22,0)   not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 DESCRIPTION                   varchar2(254),
 LONG_DESCRIPTION              varchar2(254),
 REPAIR_LOCATION_TYPE_ID_XLATE varchar2(254),
 REPAIR_LOCATION_TYPE_ID       number(22,0),
 REPAIR_LOCATION_ID            number(22,0),
 IS_MODIFIED                   number(22,0),
 ERROR_MESSAGE                 varchar2(4000)
);

alter table RPR_IMPORT_LOCATION
   add constraint RPR_IMP_LC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOCATION
   add constraint RPR_IMP_LC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

drop table RPR_IMPORT_LOCATION_ARC;

create table RPR_IMPORT_LOCATION_ARC
(
 IMPORT_RUN_ID                 number(22,0)   not null,
 LINE_ID                       number(22,0)   not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 DESCRIPTION                   varchar2(254),
 LONG_DESCRIPTION              varchar2(254),
 REPAIR_LOCATION_TYPE_ID_XLATE varchar2(254),
 REPAIR_LOCATION_TYPE_ID       number(22,0),
 REPAIR_LOCATION_ID            number(22,0)
);

alter table RPR_IMPORT_LOCATION_ARC
   add constraint RPR_IMP_LC_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOCATION_ARC
   add constraint RPR_IMP_LC_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;


insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE,
    PARENT_TABLE, IS_ON_TABLE, PARENT_TABLE_PK_COLUMN)
values
   (107, 'repair_location_type_id', 'Repair Location Type', 'repair_location_type_id_xlate', 1, 1, 'number(22,0)',
    'repair_location_type', 1, 'repair_location_type_id');

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS)
values
   (1071, sysdate, user, 'Repair Location Type Description',
    'The passed in value corresponds to the Repair Location Type flag.  Translate to the ID using the Repair Location Type table.',
    'repair_location_type_id',
    '( select r.repair_location_type_id from repair_location_type r where upper( trim( <importfield> ) ) = upper( trim( r.description ) ) )',
    0, 'repair_location_type', 'description', '');

delete PP_IMPORT_COLUMN_LOOKUP
 where COLUMN_NAME = 'is_circuit'
   and IMPORT_LOOKUP_ID = 78;

update PP_IMPORT_COLUMN_LOOKUP
   set COLUMN_NAME = 'repair_location_type_id', IMPORT_LOOKUP_ID = 1071
 where COLUMN_NAME = 'is_circuit';

update PP_IMPORT_TEMPLATE_FIELDS set COLUMN_NAME = 'repair_location_type_id' where COLUMN_NAME = 'is_circuit';

delete PP_IMPORT_COLUMN where COLUMN_NAME = 'is_circuit';

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID = 1071
 where IMPORT_TYPE_ID = 107
   and COLUMN_NAME = 'repair_location_type_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1264, 0, 10, 4, 2, 4, 38294, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.4_maint_038294_taxrpr_rpr_loc_import.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;