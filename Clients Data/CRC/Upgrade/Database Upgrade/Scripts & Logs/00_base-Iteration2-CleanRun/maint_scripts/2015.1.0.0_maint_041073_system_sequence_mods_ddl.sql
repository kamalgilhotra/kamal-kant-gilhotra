/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041073_system_sequence_mods_ddl.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1	  12/08/2014 Anand R 		   Modify the increment_by of all sequences to 1
||========================================================================================
*/

SET SERVEROUTPUT ON 

DECLARE 
CURSOR C1 IS SELECT SEQUENCE_NAME, MIN_VALUE, MAX_VALUE, INCREMENT_BY, CYCLE_FLAG, ORDER_FLAG,CACHE_SIZE, LAST_NUMBER FROM ALL_SEQUENCES  WHERE SEQUENCE_OWNER = 'PWRPLANT' ; 

SEQ_ROW    C1%ROWTYPE ;
RESET_INCR_BY_FLAG BOOLEAN;
RESET_CACHE_SIZE_FLAG BOOLEAN;
RESET_MAX_VALUE_FLAG BOOLEAN;
SEQUENCE_MAXVALUE CONSTANT NUMBER := 9999999999999999999999999999;
SQLSTRING  VARCHAR2(2000);
ERROR_CD NUMBER;
ERROR_MSG VARCHAR(2000);

BEGIN
DBMS_OUTPUT.ENABLE(buffer_size => NULL);
OPEN C1;

LOOP
  SQLSTRING := '';
  RESET_INCR_BY_FLAG := FALSE;
  RESET_CACHE_SIZE_FLAG := FALSE;
  RESET_MAX_VALUE_FLAG := FALSE;
  
  FETCH C1 INTO SEQ_ROW;
  EXIT WHEN C1%NOTFOUND ;
  
  DBMS_OUTPUT.PUT_LINE('Checking Sequence ' || SEQ_ROW.SEQUENCE_NAME );
  
  IF SEQ_ROW.INCREMENT_BY <> 1 THEN 
    RESET_INCR_BY_FLAG := TRUE;
  END IF;
  
  IF SEQ_ROW.CACHE_SIZE <> 0 THEN 
    RESET_CACHE_SIZE_FLAG := TRUE;
  END IF;
  
  IF SEQ_ROW.MAX_VALUE <> SEQUENCE_MAXVALUE THEN
    RESET_MAX_VALUE_FLAG := TRUE;
  END IF;
  
  IF RESET_INCR_BY_FLAG = FALSE AND RESET_CACHE_SIZE_FLAG = FALSE AND RESET_MAX_VALUE_FLAG = FALSE THEN
    DBMS_OUTPUT.PUT_LINE('No Changes Required.' );
    DBMS_OUTPUT.PUT_LINE(' ');
    CONTINUE;
  END IF;
  
  SQLSTRING := 'DROP SEQUENCE ' || SEQ_ROW.SEQUENCE_NAME ;
  
  DBMS_OUTPUT.PUT_LINE('Dropping Sequence ' || SEQ_ROW.SEQUENCE_NAME || ' - ' || SQLSTRING );
  
  EXECUTE IMMEDIATE SQLSTRING;
   
  SQLSTRING := 'CREATE SEQUENCE ' || SEQ_ROW.SEQUENCE_NAME ;
  SQLSTRING := SQLSTRING || ' MINVALUE ' || TO_CHAR(SEQ_ROW.MIN_VALUE) ;

  SQLSTRING := SQLSTRING || ' INCREMENT BY 1' ;

  SQLSTRING := SQLSTRING || ' NOCACHE ' ;
  
  IF RESET_INCR_BY_FLAG = TRUE THEN
    SQLSTRING := SQLSTRING || 'START WITH 2147483647' ;
  ELSE
    SQLSTRING := SQLSTRING || 'START WITH ' || TO_CHAR( SEQ_ROW.LAST_NUMBER + 1 ) ; 
  END IF;
  
  DBMS_OUTPUT.PUT_LINE('Creating Sequence ' || SEQ_ROW.SEQUENCE_NAME || ' - ' || SQLSTRING );
  DBMS_OUTPUT.PUT_LINE(' ');

  EXECUTE IMMEDIATE SQLSTRING;
  
END LOOP ;
CLOSE C1;

DBMS_OUTPUT.PUT_LINE('Checking and Rebuilding Sequences Complete.');

EXCEPTION
  WHEN OTHERS THEN
    ERROR_CD := SQLCODE;
    ERROR_MSG := SUBSTR(SQLERRM, 1, 2000);
    DBMS_OUTPUT.PUT_LINE('THERE WERE ERROR DURING EXECUTION. ' || ERROR_CD || ' : ' || ERROR_MSG);

END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2319, 0, 2015, 1, 0, 0, 41073, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041073_system_sequence_mods_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;