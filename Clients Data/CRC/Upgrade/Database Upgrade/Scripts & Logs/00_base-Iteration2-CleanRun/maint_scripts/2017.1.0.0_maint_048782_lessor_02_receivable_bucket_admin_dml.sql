/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048782_lessor_02_receivable_bucket_admin_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/11/2017 Shane Ward       Create Receivable Bucket Maint workspace for Lessor
||============================================================================
*/

--Remove old Table Maint from Lessor
DELETE FROM ppbase_menu_items WHERE Upper(MODULE) = 'LESSOR' AND menu_identifier = 'admin_table_maint';

--New Workspace
INSERT INTO ppbase_workspace (MODULE, workspace_identifier, label, workspace_uo_name, object_type_id)
VALUES ('LESSOR', 'admin_receivables_bucket', 'Receivables Buckets', 'uo_lsr_admincntr_receivables_bucket', 1);

--Hijack the odl spot for Receivables Buckets
INSERT INTO ppbase_menu_items (MODULE, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
VALUES ('LESSOR', 'admin_receive_bucket', 2, 4, 'Receivables Buckets', 'menu_wksp_admin', 'admin_receivables_bucket', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3695, 0, 2017, 1, 0, 0, 48782, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048782_lessor_02_receivable_bucket_admin_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;