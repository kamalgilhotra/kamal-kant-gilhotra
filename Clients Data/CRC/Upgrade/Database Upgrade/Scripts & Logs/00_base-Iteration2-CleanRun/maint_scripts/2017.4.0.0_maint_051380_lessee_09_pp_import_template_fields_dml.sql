/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051380_lessee_09_pp_import_template_fields_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 5/30/2018  Alex Healey    Create new purchase option import template and fields for the template
||============================================================================
*/

INSERT INTO
        pp_import_template
        (
                import_template_id,
                import_type_id    ,
                description       ,
                long_description  ,
                created_by        ,
                created_date      ,
                do_update_with_add,
                is_autocreate_template
        )
        VALUES
        (
                pp_import_template_seq.nextval                    ,
                268                                               ,
                'ILR Purchase Options Add'                        ,
                'Default template for adding ILR purchase options',
                'PWRPLANT'                                        ,
                SYSDATE                                           ,
                1                                                 ,
                0
        );
		
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')    ,
                1       ,
                268     ,
                'ilr_id',
                1096
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')       ,
                2         ,
                268       ,
                'revision',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')                          ,
                3                            ,
                268                          ,
                'ilr_purchase_probability_id',
                1086
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')                   ,
                4                     ,
                268                   ,
                'purchase_option_type',
                1117
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')           ,
                5             ,
                268           ,
                'purchase_amt',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')                     ,
                6                       ,
                268                     ,
                'ilr_purchase_option_id',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')            ,
                7              ,
                268            ,
                'decision_date',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')            ,
                8              ,
                268            ,
                'purchase_date',
                NULL
        );
INSERT INTO
        pp_import_template_fields
        (
                import_template_id,
                field_id          ,
                import_type_id    ,
                column_name       ,
                import_lookup_id
        )
        VALUES
        (
                (select distinct import_template_id from pp_import_template where import_type_id = 268 and lower(description) = 'ilr purchase options add')              ,
                9                ,
                268              ,
                'decision_notice',
                NULL
        );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6085, 0, 2017, 4, 0, 0, 51380, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051380_lessee_09_pp_import_template_fields_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;