/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039508_reg_ifa_relate.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/19/2014 Ryan Oliveria    Allow IFA Relationships
||============================================================================
*/

create table REG_INCREMENTAL_RELATE_TYPE
(
 RELATE_TYPE_ID number(22,0) not null,
 USER_ID        varchar2(18),
 TIME_STAMP     date,
 DESCRIPTION    varchar2(35) not null
);

alter table REG_INCREMENTAL_RELATE_TYPE
   add constraint PK_REG_INCREMENTAL_RELATE_TYPE
       primary key (RELATE_TYPE_ID)
       using index tablespace PWRPLANT_IDX;


create table REG_INCREMENTAL_ADJ_RELATE
(
 PARENT_ADJ_ID  number(22,0) not null,
 CHILD_ADJ_ID   number(22,0) not null,
 USER_ID        varchar2(18),
 TIME_STAMP     date,
 RELATE_TYPE_ID number(22,0) not null
);

alter table REG_INCREMENTAL_ADJ_RELATE
   add constraint PK_REG_INCREMENTAL_ADJ_RELATE
       primary key (PARENT_ADJ_ID, CHILD_ADJ_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_ADJ_RELATE
   add constraint R_REG_INCREMENTAL_ADJ_RELATE1
       foreign key (PARENT_ADJ_ID)
       references REG_INCREMENTAL_ADJUSTMENT (INCREMENTAL_ADJ_ID);

alter table REG_INCREMENTAL_ADJ_RELATE
   add constraint R_REG_INCREMENTAL_ADJ_RELATE2
       foreign key (CHILD_ADJ_ID)
       references REG_INCREMENTAL_ADJUSTMENT (INCREMENTAL_ADJ_ID);

alter table REG_INCREMENTAL_ADJ_RELATE
   add constraint R_REG_INCREMENTAL_ADJ_RELATE3
       foreign key (RELATE_TYPE_ID)
       references REG_INCREMENTAL_RELATE_TYPE (RELATE_TYPE_ID);

-- COMMENTS
comment on table REG_INCREMENTAL_RELATE_TYPE is 'Stores the different relationship types. Originally just Parent/Child and Exclusive.';
comment on column REG_INCREMENTAL_RELATE_TYPE.RELATE_TYPE_ID is 'The ID of the relationship type.';
comment on column REG_INCREMENTAL_RELATE_TYPE.USER_ID is 'Standard system-assigned user ID used for audit purposes';
comment on column REG_INCREMENTAL_RELATE_TYPE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_INCREMENTAL_RELATE_TYPE.DESCRIPTION is 'A short description of the relationship type';

comment on table REG_INCREMENTAL_ADJ_RELATE is 'Stores the relationships between IFA''s.';
comment on column REG_INCREMENTAL_ADJ_RELATE.PARENT_ADJ_ID is 'The ID of the parent IFA.';
comment on column REG_INCREMENTAL_ADJ_RELATE.CHILD_ADJ_ID is 'The ID of the child IFA.';
comment on column REG_INCREMENTAL_ADJ_RELATE.USER_ID is 'Standard system-assigned user ID used for audit purposes';
comment on column REG_INCREMENTAL_ADJ_RELATE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_INCREMENTAL_ADJ_RELATE.RELATE_TYPE_ID is 'Specifies the type of relationship. 1 = Parent/Child relationship. 0 = Exclusive relationship.';

-- CONFIG
insert into REG_INCREMENTAL_RELATE_TYPE (RELATE_TYPE_ID, DESCRIPTION) values (1, 'Parent/Child');
insert into REG_INCREMENTAL_RELATE_TYPE (RELATE_TYPE_ID, DESCRIPTION) values (2, 'Exclusive');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1338, 0, 10, 4, 2, 7, 39508, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039508_reg_ifa_relate.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
