/*
 ||============================================================================
 || Application: acqaider_app
 || File Name: maint_043663_sys_acqa_04_depr_imp_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2015.1.0.0 03/20/2015 Sherri Ramson  <Inserts and Updates for depr import tool in acqaider app>
 ||============================================================================
 */

--�	No inserts from pp_import_subsystem required

--�	Need pp_import_type insert for �Depreciation Configuration Loader� in yellow

insert into pp_import_type ( import_type_id, time_stamp, user_id, description, long_description, import_table_name, archive_table_name, pp_report_filter_id, allow_updates_on_add, delegate_object_name, archive_additional_columns, autocreate_description, autocreate_restrict_sql ) values ( 56, sysdate, user, 'Depreciation Configuration Loader', 'Import depreciation methods, depreciation groups, and depr group control', 'depr_config_import_stg', 'depr_config_import_stg_arc', null, 0, 'uo_ppdepr_config_logic_import', '', 'Depr Methods/Groups/Control', '' );

--�	Need pp_import_type_subsystem insert for pp_import_type �Depreciation Configuration Loader� in yellow

insert into pp_import_type_subsystem ( import_type_id, import_subsystem_id, time_stamp, user_id ) values ( 56, 3, sysdate, user );

--�	Pp_import_column � everything for import_type_id = 56
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'auto_retire', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Auto Retire', null, 0, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'company_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'depr_blending_type_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'Depr Blending Type', 'depr_blending_type_XLATE', 0, 1, 'number(22,0)',
    'depr_blending_type', null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'description', TO_DATE('23-02-2015 23:28:05', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Description', null, 1, 1, 'varchar2(35)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'exclude_from_rwip', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Exclude from RWIP', null, 0, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'rate_recalc_option', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Rate Recalc Option', null, 0, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'ARO_ID', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'ARO',
    'ARO_XLATE', 0, 1, 'number(22,0)', 'aro', null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'BUS_SEGMENT_ID', TO_DATE('09-04-2015 15:05:45', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Business Segment', 'BUS_SEGMENT_XLATE', 1, 2, 'number(22,0)', 'business_segment', null, 1, null,
    null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'COMBINED_DEPR_GROUP_ID', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'Combined Depr Group', 'COMBINED_DEPR_GROUP_XLATE', 0, 1, 'number(22,0)',
    'combined_depr_group', null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'COR_EXPENSE_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Cost of Removal Expense Account', 'COR_EXPENSE_ACCT_XLATE', 1, 4, 'number(22,0)', 'gl_account',
    null, 0, null, 'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'COR_RESERVE_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Cost of Removal Reserve Account', 'COR_RESERVE_ACCT_XLATE', 1, 4, 'number(22,0)', 'gl_account',
    null, 1, null, 'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'COR_TREATMENT', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Cost of Removal Treatment', null, 0, 1, 'varchar2(5)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'DEPR_GROUP_ID', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Depr Group', null, 0, 1, 'number(22,0)', null, null, 0, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'DEPR_METHOD_ID', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Depr Method', null, 0, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'DEPR_SUMMARY2_ID', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Depr Summary2', 'DEPR_SUMMARY2_XLATE', 0, 1, 'number(22,0)', 'depr_summary2', null, 1, null,
    null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'DEPR_SUMMARY_ID', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Depr Summary', 'DEPR_SUMMARY_XLATE', 0, 1, 'number(22,0)', 'depr_summary', null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'EST_ANN_NET_ADDS', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Estimated Annual Net Adds', null, 0, 1, 'NUMBER(22,8)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'EXPENSE_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Expense Account', 'EXPENSE_ACCT_XLATE', 1, 4, 'number(22,0)', 'gl_account', null, 1, null,
    'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'EXTERNAL_DEPR_CODE', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'External Depr Code', null, 0, 1, 'VARCHAR2(254)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'FACTOR_ID', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Factor',
    'FACTOR_XLATE', 0, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'FUNC_CLASS_ID', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Functional Class', 'FUNC_CLASS_XLATE', 0, 1, 'number(22,0)', 'func_class', null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'GAIN_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Gain Account', 'GAIN_ACCT_XLATE', 1, 4, 'number(22,0)', 'gl_account', null, 1, null,
    'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'GAIN_LOSS_DEFAULT', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Gain Loss Default', null, 0, 1, 'NUMBER(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'JE_BY_ASSET', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'JE By Asset', null, 0, 1, 'NUMBER(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'LOSS_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Loss Account', 'LOSS_ACCT_XLATE', 1, 4, 'number(22,0)', 'gl_account', null, 1, null,
    'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'MID_PERIOD_CONV', TO_DATE('23-02-2015 23:20:44', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Mid Period Convention', null, 1, 1, 'NUMBER(22,8)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'MID_PERIOD_METHOD', TO_DATE('23-02-2015 23:21:53', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Mid Period Method', null, 1, 1, 'varchar2(35)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'NET_SALVAGE_AMORT_LIFE', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'Net Salvage Amort Life (In Months)', null, 0, 1, 'number(22,0)', null, null, 1,
    null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'RESERVE_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Reserve Account', 'RESERVE_ACCT_XLATE', 1, 4, 'number(22,0)', 'gl_account', null, 1, null,
    'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'SALVAGE_EXPENSE_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'Salvage Expense Account', 'SALVAGE_EXPENSE_ACCT_XLATE', 1, 4, 'number(22,0)',
    'gl_account', null, 1, null, 'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'SALVAGE_RESERVE_ACCT_ID', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'Salvage Reserve Account', 'SALVAGE_RESERVE_ACCT_XLATE', 1, 4, 'number(22,0)',
    'gl_account', null, 1, null, 'gl_account_id', null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'SALVAGE_TREATMENT', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Salvage Treatment (No/Amort/Month)', null, 0, 1, 'varchar2(35)', null, null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'STATUS_ID', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Status',
    'STATUS_XLATE', 0, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'SUBLEDGER_TYPE_ID', TO_DATE('23-02-2015 23:26:30', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Subledger Type', 'SUBLEDGER_TYPE_XLATE', 1, 1, 'number(22,0)', 'subledger_control', null, 1,
    null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'TRUE_UP_CPR_DEPR', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'True Up Cpr Depr', null, 0, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'asset_location_id', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Asset Location', 'asset_location_xlate', 0, 5, 'number(22,0)', 'asset_location', null, 1, null,
    null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'book_vintage', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Book Vintage', null, 0, 1, 'date', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'class_code_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Class Code', 'class_code_xlate', 0, 1, 'number(22,0)', 'class_code', null, 1, null, null, null,
    null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'cc_value', TO_DATE('24-02-2015 14:30:29', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Class Code Value', null, 0, 1, 'number(22,0)', 'class_code_values', null, 1, null, null, null,
    null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'gl_account_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'GL Account', 'gl_account_xlate', 0, 1, 'number(22,0)', 'gl_account', null, 1, null, null, null,
    null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'location_type_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Location Type', 'location_type_xlate', 0, 1, 'number(22,0)', 'location_type', null, 1, null,
    null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'major_location_id', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Major Location', 'major_location_xlate', 0, 4, 'number(22,0)', 'major_location', null, 1, null,
    null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'property_unit_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Property Unit', 'property_unit_xlate', 0, 1, 'number(22,0)', 'property_unit', null, 1, null,
    null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'retirement_unit_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Retirement Unit', 'retirement_unit_xlate', 0, 1, 'number(22,0)', 'retirement_unit', null, 1,
    null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'sub_account_id', TO_DATE('12-02-2015 20:24:48', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Sub Account', 'sub_account_xlate', 0, 1, 'number(22,0)', 'sub_account', null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'utility_account_id', TO_DATE('09-04-2015 15:05:45', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'Utility Account', 'utility_account_xlate', 1, 3, 'number(22,0)', 'utility_account',
    'bus_segment_id', 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'LOCATION_TYPE_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'LOCATION TYPE INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'PROP_UNIT_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'PROP UNIT INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'CLASS_CODE_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'CLASS CODE INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'RET_UNIT_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'RET UNIT INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'ASSET_LOC_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'ASSET LOC INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'BOOK_VINTAGE_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'BOOK VINTAGE INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'SUBLEDGER_TYPE_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'SUBLEDGER TYPE INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'SUB_ACCT_INDICATOR', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'SUB ACCT INDICATOR', null, 1, 4, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'MAJOR_LOC_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'MAJOR LOC INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'GL_ACCT_INDICATOR', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'GL ACCT INDICATOR', null, 1, 4, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'UTILITY_ACCT_INDICATOR', TO_DATE('09-04-2015 15:08:16', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT', 'UTILITY ACCT INDICATOR', null, 1, 4, 'number(22,0)', null, null, 1, null, null,
    null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'BUS_SEG_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'BUS SEG INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null);

insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED,
    PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE,
    AUTOCREATE_IMPORT_TYPE_ID, PARENT_TABLE_PK_COLUMN, DEFAULT_VALUE, PARENT_TABLE_PK_COLUMN3)
values
   (56, 'COMPANY_INDICATOR', TO_DATE('23-02-2015 20:26:31', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT',
    'COMPANY INDICATOR', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null);

--�	Pp_import_lookup � at least the rows highlited in yellow (may need to check on the other 500 records (501-515), I think these already exist, but worth checking
--514 and 515 Did not exist, the rest are update statements,
insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (516, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type DPR Expense)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'COR_EXPENSE_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 6 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 6 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (517, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type DPR)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'COR_RESERVE_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 3 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 3 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (518, to_date('08-03-2015 19:31:11', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Depr Summary.Description', 'The passed in value corresponds to the Depr Summary: Description', 'DEPR_SUMMARY_ID', '( select depr_summary_id from depr_summary where upper( trim( <importfield> ) ) = upper( trim( description) ) )', 0, 'depr_summary', 'description', null, null, null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (519, to_date('08-03-2015 19:31:11', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Depr Summary2.Description', 'The passed in value corresponds to the Depr Summary2: Description', 'DEPR_SUMMARY2_ID', '( select depr_summary2_id from depr_summary where upper( trim( <importfield> ) ) = upper( trim( description) ) )', 0, 'depr_summary2', 'description', null, null, null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (520, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type DPR Expense)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'EXPENSE_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 6 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 6 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (521, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type Gain/Loss)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'GAIN_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 7 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 7 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (522, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type Gain/Loss)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'LOSS_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 7 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 7 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (523, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type DPR)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'RESERVE_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 3 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 3 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (524, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type DPR Expense)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'SALVAGE_EXPENSE_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 6 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 6 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (525, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company, account type DPR)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'SALVAGE_RESERVE_ACCT_ID', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where account_type_id = 3 and upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and account_type_id = 3 and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (526, to_date('01-04-2015 20:12:12', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'GL Account.Description (for given company)', 'The passed in value corresponds to the GL Account: Description field.  Translate to the GL Account ID using the Description column on the GL Account table.', 'gl_account_id', '( select distinct ga.gl_account_id from gl_account ga, company_gl_account cga where upper( trim( <importfield> ) ) = upper( trim( ga.description ) )  and <importtable>.company_id = cga.company_id )  ', 0, 'gl_account', 'description', 'company_id', 'select gl_account.description, company.<company_id> from gl_account, company_gl_account , company where gl_account.gl_account_id = company_gl_account.gl_account_id and company.company_id = company_gl_account.company_id and 5=5', null);

insert into Pp_import_lookup (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN)
values (527, to_date('01-04-2015 19:03:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT', 'Major Location.Description (for given company)', 'The passed in value corresponds to the Major Location: Description field.  Translate to the Major Location ID using the Description column on the Major Location table.', 'major_location_id', '( select ml.major_location_id from major_location ml, company_major_location cml where cml.major_location_id = ml.major_location_id and upper( trim( <importfield> ) ) = upper( trim( ml.description ) ) and <importtable>.company_id = cml.company_id )', 0, 'major_location', 'description', 'company_id', 'select major_location.description, company.<company_id> from major_location, company_major_location, company where major_location.major_location_id = company_major_location.major_location_id and company_major_location.company_id = company.company_id and 5=5', null);



--�	Pp_import_column_lookup � all rows for import_Type_id = 56
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'BUS_SEGMENT_ID', 25, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'BUS_SEGMENT_ID', 26, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'BUS_SEGMENT_ID', 604, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'BUS_SEGMENT_ID', 605, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'COR_EXPENSE_ACCT_ID', 516, TO_DATE('01-04-2015 20:18:20', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'COR_RESERVE_ACCT_ID', 517, TO_DATE('01-04-2015 20:18:20', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'DEPR_SUMMARY2_ID', 519, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'DEPR_SUMMARY_ID', 518, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'EXPENSE_ACCT_ID', 520, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'GAIN_ACCT_ID', 521, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'LOSS_ACCT_ID', 522, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'RESERVE_ACCT_ID', 523, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'SALVAGE_EXPENSE_ACCT_ID', 524, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'SALVAGE_RESERVE_ACCT_ID', 525, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'asset_location_id', 609, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'class_code_id', 199, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'class_code_id', 200, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'company_id', 19, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'company_id', 20, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'company_id', 21, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'gl_account_id', 526, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'), 'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'major_location_id', 527, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'property_unit_id', 194, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'retirement_unit_id', 607, TO_DATE('12-02-2015 21:12:40', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'utility_account_id', 27, TO_DATE('12-02-2015 21:15:03', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'utility_account_id', 28, TO_DATE('12-02-2015 21:15:03', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'utility_account_id', 29, TO_DATE('12-02-2015 21:15:03', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID)
values
   (56, 'utility_account_id', 30, TO_DATE('12-02-2015 21:15:03', 'dd-mm-yyyy hh24:mi:ss'),
    'PWRPLANT');


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2511, 0, 2015, 1, 0, 0, 43663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043663_sys_acqa_04_depr_imp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
