/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052974_MO_ONLY_2018.1.0.1_MO_pp_version_update.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.1 1/21/2019  CYura            2018.1.0.1 MaintOps Release - Apply to 2018.1.0.1 only
||============================================================================
*/
update PP_VERSION
   set PP_PATCH   = 'Patch: 2018.1.0.1',
       POWERTAX_VERSION = '2018.1.0.1',
       PROVISION_VERSION = '2018.1.0.1',
       PROP_TAX_VERSION = '2018.1.0.1',
       LEASE_VERSION = '2018.1.0.1',
       BUDGET_VERSION = '2018.1.0.1',
       DATE_INSTALLED = sysdate
where PP_VERSION_ID = 1;


insert into PP_VERSION_EXES 
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '2018.1.0.1', executable_file, '2018.1.0.0' from PP_VERSION_EXES A where pp_version = '2018.1.0.0'
and not exists (select 1 from PP_VERSION_EXES B where A.executable_file = B.executable_file and b.pp_version = '2018.1.0.1');

update pp_version_exes 
set executable_version = '2018.1.0.1'
where lower(executable_file) like 'ssp_lease_control.exe%'
and pp_version = '2018.1.0.1';

update pp_processes 
set version = '2018.1.0.1'
where lower(executable_file) like 'ssp_lease_control.exe%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14104, 0, 2018, 1, 0, 1, 52974, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052974_MO_ONLY_2018.1.0.1_MO_pp_version_update.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;