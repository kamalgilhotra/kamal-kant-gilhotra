/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010638_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date             Revised By           Reason for Change
|| ----------  ---------------  -------------------  -------------------------
|| 10.4.2.0    12/18/2013       Andrew Scott         Add system option and change calc
||                                                   to include individually assessed
||                                                   parcels in assessment distribution
||============================================================================
*/

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE,
    OPTION_VALUE, IS_BASE_OPTION)
values
   ('Assessments - Include Individually Assessed Parcels in Distribution', sysdate, user,
    'Whether the system should include individually assessed parcels in the distribution. This should only be used to recalculate the estimates for an entire assessment group, usually needed for accrual processing. The default is set to ''No''.',
    0, 'No', null, 1);

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID)
values
   ('Assessments - Include Individually Assessed Parcels in Distribution', 'No', sysdate,
    user);

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID)
values
   ('Assessments - Include Individually Assessed Parcels in Distribution', 'Yes',
    sysdate, user);

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE, TIME_STAMP, USER_ID)
values
   ('Assessments - Include Individually Assessed Parcels in Distribution', 'proptax',
    sysdate, user);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (819, 0, 10, 4, 2, 0, 10638, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_010638_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;