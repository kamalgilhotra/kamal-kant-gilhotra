/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051700_sys_05_add_tools_window_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/05/2018 Tech Prod Mgmt   Add new Translate Rules Setup window to Admin>Tools list
||============================================================================
*/

insert into PP_CONVERSION_WINDOWS( WINDOW_ID ,
          DESCRIPTION ,
          LONG_DESCRIPTION ,
          WINDOW_NAME     )
select w_id, 'PowerPlan Translate Rule Setup', 'PowerPlan Translate Rule Setup', 'w_pp_translate_setup'
from (select max(window_id) + 1 w_id from PP_CONVERSION_WINDOWS)
where not exists (select 1 from pp_conversion_windows where window_Name='w_pp_translate_setup')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7595, 0, 2017, 4, 0, 0, 51700, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051700_sys_05_add_tools_window_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;