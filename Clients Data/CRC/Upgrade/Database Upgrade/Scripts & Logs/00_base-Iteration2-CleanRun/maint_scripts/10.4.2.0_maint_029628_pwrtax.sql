/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029628_pwrtax.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/24/2014 Nathan Hollis
||============================================================================
*/

insert into PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, REPORT_TYPE, REPORT_NUMBER,
    FILTER_OPTION, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID)
values
   ((select max(REPORT_ID) + 1 from PP_REPORTS), 'Tax Dashboard Report', 'Tax Dashboard Report',
    'PowerTax', 'dw_tax_dashboard', 'PowerTax_Rollup', '300', 'DETAIL', 1, 29, 0, 3, 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1071, 0, 10, 4, 2, 0, 29628, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_029628_pwrtax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
