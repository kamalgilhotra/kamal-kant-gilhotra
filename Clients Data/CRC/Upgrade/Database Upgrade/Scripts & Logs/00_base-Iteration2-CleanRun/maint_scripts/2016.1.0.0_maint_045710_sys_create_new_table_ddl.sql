/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045710_sys_create_new_table_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2016.1.0  06/07/2016 Anand R       Create new table to store actions drop down for Alerts
||============================================================================
*/

create table PPBASE_ACTIONS_LIST (
  ID              NUMBER(22,0) NOT NULL,
  ID_TYPE         VARCHAR2(254)  NOT NULL,
  DRILL_TO_WINDOW VARCHAR2(254) NOT NULL,
  TASK_ID         NUMBER(22,0)  NULL,
  USER_ID         VARCHAR2(18) NULL,
  TIME_STAMP      DATE NULL
);

comment on table PPBASE_ACTIONS_LIST is 'Table to store Actions dropdowns for Alerts';

comment on column PPBASE_ACTIONS_LIST.ID is 'unique identifier for the record';
comment on column PPBASE_ACTIONS_LIST.ID_TYPE is 'Alert type';
comment on column PPBASE_ACTIONS_LIST.DRILL_TO_WINDOW is 'This column stored the action performed for each dropdown';
comment on column PPBASE_ACTIONS_LIST.TASK_ID is 'system defined task id';
comment on column PPBASE_ACTIONS_LIST.USER_ID is 'Standard system-assigned user id user for audit purposes.';
comment on column PPBASE_ACTIONS_LIST.TIME_STAMP is 'Standard system-assigned timestamp user for audit purposes.';

alter table PPBASE_ACTIONS_LIST
  add constraint PPBASE_ACTIONS_LIST_PK
  primary key (ID)
  using index tablespace PWRPLANT_IDX;
  
alter table PPBASE_ACTIONS_LIST 
   add constraint PPBASE_ACTIONS_LIST_FK1
   foreign key (TASK_ID)
   references PP_MYPP_TASK(TASK_ID);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3213, 0, 2016, 1, 0, 0, 045710, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045710_sys_create_new_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;