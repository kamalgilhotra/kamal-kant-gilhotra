/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048559_lessee_01_add_new_disc_rpt_wtd_avg_discnt_rt_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.3.0.0 4/6/2018  Alex Healey    Add new disclosure report for calculating the weighted average of the discount rate
||============================================================================
*/

INSERT INTO PP_REPORTS
		(REPORT_ID,
		DESCRIPTION,
		LONG_DESCRIPTION,
		SUBSYSTEM,
		DATAWINDOW,
		REPORT_NUMBER,
		PP_REPORT_SUBSYSTEM_ID,
		REPORT_TYPE_ID,
		PP_REPORT_TIME_OPTION_ID,
		PP_REPORT_FILTER_ID,
		PP_REPORT_STATUS_ID,
		PP_REPORT_ENVIR_ID)
VALUES
		((SELECT NVL(MAX(REPORT_ID), 0)+1 from pp_reports),
		'Weighted avg of Discount rate',
		'Disclosure: The weighted average of discount rates by company, lease cap type, lease group',
		'Lessee',
		'dw_ls_rpt_disc_wtd_discnt_rt',
		'Lessee - 2003',
		11,
		311,
		206,
		103,
		1,
		3);
		
		
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4285, 0, 2017, 3, 0, 0, 48559, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_048559_lessee_01_add_new_disc_rpt_wtd_avg_discnt_rt_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;