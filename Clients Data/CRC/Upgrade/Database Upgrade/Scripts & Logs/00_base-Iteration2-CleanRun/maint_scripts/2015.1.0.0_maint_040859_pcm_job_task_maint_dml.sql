/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040859_pcm_job_task_maint_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 01/08/2015 Sarah Byers      Porting the Job Task Maint tab from Work Order Details
||============================================================================
*/
-- Update the workspace identifier for the job task maint workspace
update ppbase_workspace
	set workspace_uo_name = 'uo_pcm_maint_wksp_job_tasks'
 where workspace_identifier = 'fp_maint_job_tasks';

update ppbase_workspace
	set workspace_uo_name = 'uo_pcm_maint_wksp_job_tasks'
 where workspace_identifier = 'wo_maint_job_tasks';

-- Set up new System Options based on System Controls
-- WODETAIL - JOB TASK DETAIL WINDOW
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select control_name, 
		 '"No" allows users to add new job tasks directly to the datawindow. "Yes" opens the job task initiation window when adding new job tasks', 
		 0, 'No', control_value, 1, 0
  from pp_system_control
 where control_name = 'WODETAIL - JOB TASK DETAIL WINDOW';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODETAIL - JOB TASK DETAIL WINDOW', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODETAIL - JOB TASK DETAIL WINDOW', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODETAIL - JOB TASK DETAIL WINDOW', 'pcm');

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select control_name, company_id, control_value
  from pp_system_control_company
 where control_name = 'WODETAIL - JOB TASK DETAIL WINDOW'
	and company_id <> -1;

-- FPDETAIL - JOB TASK DETAIL WINDOW
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select control_name, 
		 '"No" allows users to add new job tasks directly to the datawindow. "Yes" opens the job task initiation window when adding new job tasks', 
		 0, 'No', control_value, 1, 0
  from pp_system_control
 where control_name = 'FPDETAIL - JOB TASK DETAIL WINDOW';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPDETAIL - JOB TASK DETAIL WINDOW', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPDETAIL - JOB TASK DETAIL WINDOW', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'FPDETAIL - JOB TASK DETAIL WINDOW', 'pcm');

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select control_name, company_id, control_value
  from pp_system_control_company
 where control_name = 'FPDETAIL - JOB TASK DETAIL WINDOW'
	and company_id <> -1;

-- WODETAIL - JOB TASK EDIT
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select control_name, long_description, 0, 'Yes', control_value, 1, 0
  from pp_system_control
 where control_name = 'WODETAIL - JOB TASK EDIT';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODETAIL - JOB TASK EDIT', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'WODETAIL - JOB TASK EDIT', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'WODETAIL - JOB TASK EDIT', 'pcm');

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select control_name, company_id, control_value
  from pp_system_control_company
 where control_name = 'WODETAIL - JOB TASK EDIT'
	and company_id <> -1;

-- FPDETAIL - JOB TASK EDIT
insert into ppbase_system_options (
	system_option_id, long_description, system_only, pp_default_value, option_value, is_base_option, allow_company_override)
select 'FPDETAIL - JOB TASK EDIT', long_description, 0, 'Yes', control_value, 1, 0
  from pp_system_control
 where control_name = 'WODETAIL - JOB TASK EDIT';

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPDETAIL - JOB TASK EDIT', 'Yes');

insert into ppbase_system_options_values (
	system_option_id, option_value)
values (
	'FPDETAIL - JOB TASK EDIT', 'No');

insert into ppbase_system_options_module (
	system_option_id, module)
values (
	'FPDETAIL - JOB TASK EDIT', 'pcm');

insert into ppbase_system_options_company (
	system_option_id, company_id, option_value)
select control_name, company_id, control_value
  from pp_system_control_company
 where control_name = 'FPDETAIL - JOB TASK DETAIL WINDOW'
	and company_id <> -1;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2151, 0, 2015, 1, 0, 0, 040859, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040859_pcm_job_task_maint_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;