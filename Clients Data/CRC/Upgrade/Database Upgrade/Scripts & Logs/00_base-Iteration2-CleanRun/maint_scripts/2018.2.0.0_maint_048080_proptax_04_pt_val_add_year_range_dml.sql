 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048080_proptax_04_pt_val_add_year_range_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 06/06/2017 Josh Sandler   Add Year Range option for Data Center
 ||============================================================================
 */

INSERT INTO pt_val_timeframe
  (value_timeframe_id, description, long_description)
VALUES
  (4,'Year Range','Year Range');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14885, 0, 2018, 2, 0, 0, 48080, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_048080_proptax_04_pt_val_add_year_range_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;