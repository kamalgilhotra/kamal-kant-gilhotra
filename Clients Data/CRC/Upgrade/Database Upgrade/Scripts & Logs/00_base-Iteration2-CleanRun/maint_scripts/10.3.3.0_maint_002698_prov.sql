SET DEFINE OFF
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_002698_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/03/2011 Blake Andrews  Point Release
||============================================================================
*/

--###PATCH(2968)
create table PWRPLANT.TAX_ACCRUAL_APPLICATIONS
(
 APPLICATION_ID number(22) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(35)
);

alter table PWRPLANT.TAX_ACCRUAL_APPLICATIONS
   add constraint TA_APP_PK
       primary key (APPLICATION_ID)
       using index tablespace PWRPLANT_IDX;

create table PWRPLANT.TAX_ACCRUAL_ERROR_CODES
(
 APPLICATION_ID number(22) not null,
 ERROR_CODE     number(22) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(2000)
);

alter table PWRPLANT.TAX_ACCRUAL_ERROR_CODES
   add constraint TA_ERROR_CODES_PK
       primary key (APPLICATION_ID, ERROR_CODE)
       using index tablespace PWRPLANT_IDX;

alter table PWRPLANT.TAX_ACCRUAL_ERROR_CODES
   add ERROR_TYPE varchar2(1);

alter table PWRPLANT.TAX_ACCRUAL_ERROR_CODES
   add SUGGESTED_ACTION varchar2(2000);

create or replace trigger PWRPLANT.TAX_ACCRUAL_APPLICATIONS
before update or insert on PWRPLANT.TAX_ACCRUAL_APPLICATIONS
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create or replace trigger PWRPLANT.TAX_ACCRUAL_ERROR_CODES
before update or insert on PWRPLANT.TAX_ACCRUAL_ERROR_CODES
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

insert into TAX_ACCRUAL_APPLICATIONS (APPLICATION_ID, DESCRIPTION) values (48, 'DMI48');

insert into TAX_ACCRUAL_ERROR_CODES
(APPLICATION_ID, ERROR_CODE, DESCRIPTION, ERROR_TYPE, SUGGESTED_ACTION)
select 48, '1','File Not Found','E','DMI file not found.  Please specify another file path and name' from dual union all
select 48, '2','Invalid File Format','E', 'DMI file is in the incorrect format.  Please ensure that the DMI file is valid' from dual union all
select 48, '3','Internal Data Error','E', 'Please contact DMI.' from dual union all
select 48, '4','Data Processing Error','E', 'Please contact DMI.' from dual union all
select 48, '5','Internal Data Error Jurisdictions','E', 'Please contact DMI.' from dual union all
select 48, '7','Internal Data Error Entities','E', 'Please contact DMI.' from dual union all
select 48, '8','Internal Data Error Issues','E', 'Please contact DMI.' from dual union all
select 48, '14','Internal Data Error','E', 'Please contact DMI.' from dual union all
select 48, '20','File needs to be calculated in DMI48','E','Please calculate the file in DMI48 and reload' from dual union all
select 48, '98','Connection Error','E', 'Please ensure that DMI48 is appropriately installed.' from dual union all
select 48, '99','Error reading from Data File','E', 'Please contact DMI.' from dual union all
select 48, '2000','Invalid DMI48 Version - Open file in latest DMI48 to upgrade version','E', null from dual union all
select 48, '2001','Invalid DMI48 assembly - Update DMI48','E', null from dual union all
select 48, '2002','Invalid DMI48 assembly Data','E', 'Please contact DMI.' from dual union all
select 48, '3000','Invalid file version (must be loaded and calculated in the current version of DMI48)','E', null from dual union all
select 48, '3001','Error parsing version','E', 'Please contact DMI.' from dual union all
select 48, '4001','Environment Variable invalid (problem with DMI48 installation)','E', 'Please contact DMI.' from dual union all
select 48, '4002','DMI48 root invalid','E', 'Please contact DMI.' from dual union all
select 48, '7001','File not found - System Data Error','E', 'Please contact DMI.' from dual union all
select 48, '7002','Invalid File Format - System Data Error','E', 'Please contact DMI.' from dual union all
select 48, '7099','Data file locked by another process - System Data Error','E', 'Please contact DMI.' from dual union all
select 48, '8002','No data exists for selection','W', 'No data exists in the selected DMI file.' from dual union all
select 48, '8003','Provision End date = 0 (indicates file was computed with no previous file attached)','W', null from dual union all
select 48, '8004','Reserve date not equal to Run To Date','W',null from dual union all
select 48, '8840','Currency Data missing (if user has foreign currency data and missing exchange rates)','W', null from dual union all
select 48, '0','Success','S', null from dual;

alter table PWRPLANT.TAX_ACCRUAL_ERROR_CODES
   add constraint TA_ERROR_CODES_APP_ID_FK
       foreign key (APPLICATION_ID)
       references PWRPLANT.TAX_ACCRUAL_APPLICATIONS(APPLICATION_ID);

insert into TAX_ACCRUAL_APPLICATIONS
   (APPLICATION_ID, DESCRIPTION)
values
   (1, 'PBConnectToNewObject');

insert into TAX_ACCRUAL_ERROR_CODES
(APPLICATION_ID, ERROR_CODE, DESCRIPTION, ERROR_TYPE, SUGGESTED_ACTION)
select 1, '-1','Invalid Call: the argument is the Object property of a control','E', 'Please contact provisionsupport@pwrplan.com' from dual union all
select 1, '-2','Class name not found','E', 'Please ensure that you are running the latest version of DMI48.' from dual union all
select 1, '-3','Object could not be created','E', 'Please ensure that you are running the latest version of DMI48.' from dual union all
select 1, '-4','Could not connect to object','E', 'Please contact provisionsupport@pwrplan.com' from dual union all
select 1, '-9','Other error','E', 'Please contact provisionsupport@pwrplan.com' from dual union all
select 1, '-15','MTS is not loaded on this computer','E', 'Please contact provisionsupport@pwrplan.com.' from dual union all
select 1, '-16','Invalid Call: this function not applicable','E', 'Please contact provisionsupport@pwrplan.com.' from dual union all
select 1, '0','Success','S', null from dual;

insert into TAX_ACCRUAL_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
values
   (48, 'DMI48 File Location', null, null, 'Path and filename of DMI48 file to load', -1);

create table PWRPLANT.TAX_ACCRUAL_DMI_RESULTS
(
 TA_VERSION_ID    number(22) not null,
 GL_MONTH         number(22,2) not null,
 DMI_ID           number(22) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 ENDBALUTP        number(22,2),
 ENDBALTAX        number(22,2),
 ENDBALPEN        number(22,2),
 ENDBALDEFINT     number(22,2),
 ENDBALOVRINT     number(22,2),
 SETTAX           number(22,2),
 SETPEN           number(22,2),
 SETDEFINT        number(22,2),
 SETOVRINT        number(22,2),
 ENDBALTAXNET     number(22,2),
 ENDBALPENNET     number(22,2),
 ENDBALDEFINTNET  number(22,2),
 ENDBALOVRINTNET  number(22,2),
 SETTAXNET        number(22,2),
 SETPENNET        number(22,2),
 SETDEFINTNET     number(22,2),
 SETOVRINTNET     number(22,2)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_RESULTS
   add constraint TA_DMI_RESULTS_PK
       primary key (TA_VERSION_ID, GL_MONTH, DMI_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_RESULTS
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_RESULTS
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create table PWRPLANT.TAX_ACCRUAL_DMI_ISSUE
(
 DMI_ISSUE_ID      number(22) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(50),
 SHORT_DESCRIPTION varchar2(15)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_ISSUE
   add constraint TA_DMI_ISSUES_PK
       primary key (DMI_ISSUE_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_ISSUE
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_ISSUE
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create table PWRPLANT.TAX_ACCRUAL_DMI_COMPANY
(
 DMI_ENTITY_ID     number(22) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(50),
 SHORT_DESCRIPTION varchar2(15),
 TIN               varchar2(20),
 COMPANY_ID        number(22)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_COMPANY
   add constraint TA_DMI_COMPANY_PK
       primary key (DMI_ENTITY_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_COMPANY
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_COMPANY
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create table PWRPLANT.TAX_ACCRUAL_DMI_JURISDICTION
(
 DMI_JURISDICTION varchar2(7) not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(25)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_JURISDICTION
   add constraint TA_DMI_JURISDICTION_PK
       primary key (DMI_JURISDICTION)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_JURISDICTION
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_JURISDICTION
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create table PWRPLANT.TAX_ACCRUAL_DMI_PROCESS_INFO
(
 TA_VERSION_ID     number(22) not null,
 COMPANY_ID        number(22) not null,
 GL_MONTH          number(22,2) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DMI_FILE_NAME     varchar2(2000),
 RESERVEYEAR       number(4),
 RESERVEMONTH      number(2),
 PROVISIONENDYEAR  number(4),
 PROVISIONENDMONTH number(1),
 RUNTODATE         number(8)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_PROCESS_INFO
   add constraint TA_DMI_PROCESS_INFO_PK
       primary key (TA_VERSION_ID, COMPANY_ID, GL_MONTH)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_PROCESS_INFO
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_PROCESS_INFO
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create table PWRPLANT.TAX_ACCRUAL_DMI_ID
(
 DMI_ID             number(22) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DMI_JURISDICTION   varchar2(25),
 DMI_ENTITY_ID      number(22),
 DMI_ISSUE_ID       number(22),
 TAX_YEAR           number(4),
 DEFERRAL           varchar2(35),
 STATUS             varchar2(35),
 DMI_CLASSIFICATION varchar2(2)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_ID
   add constraint TA_DMI_ID_PK
       primary key (DMI_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_ID
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_ID
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create table PWRPLANT.TAX_ACCRUAL_DMI_DRILL
(
 M_ID                number(22) not null,
 GL_MONTH            number(22,2) not null,
 DMI_ID              number(22) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 MULT                number(22,8),
 AMOUNT              number(22,2),
 ENDBALUTP_IND       number(1) default 0,
 ENDBALTAX_IND       number(1) default 0,
 ENDBALPEN_IND       number(1) default 0,
 ENDBALDEFINT_IND    number(1) default 0,
 ENDBALOVRINT_IND    number(1) default 0,
 SETTAX_IND          number(1) default 0,
 SETPEN_IND          number(1) default 0,
 SETDEFINT_IND       number(1) default 0,
 SETOVRINT_IND       number(1) default 0,
 ENDBALTAXNET_IND    number(1) default 0,
 ENDBALPENNET_IND    number(1) default 0,
 ENDBALDEFINTNET_IND number(1) default 0,
 ENDBALOVRINTNET_IND number(1) default 0,
 SETTAXNET_IND       number(1) default 0,
 SETPENNET_IND       number(1) default 0,
 SETDEFINTNET_IND    number(1) default 0,
 SETOVRINTNET_IND    number(1) default 0
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_DRILL
   add constraint TA_DMI_DRILL_PK
       primary key (M_ID, GL_MONTH, DMI_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_DRILL
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_DRILL
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create sequence PWRPLANT.TAX_ACCRUAL_DMI_SEQ;

create table PWRPLANT.TAX_ACCRUAL_DMI_DATA_CONTROL
(
 DMI_DATA_CONTROL_ID number(22) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 COMPANY_ID          number(22),
 EXTERNAL_KEY        varchar2(35),
 DMI_ID              number(22) default 0,
 MULT                number(22,8) default 1,
 ENDBALUTP_IND       number(1) default 0,
 ENDBALTAX_IND       number(1) default 0,
 ENDBALPEN_IND       number(1) default 0,
 ENDBALDEFINT_IND    number(1) default 0,
 ENDBALOVRINT_IND    number(1) default 0,
 SETTAX_IND          number(1) default 0,
 SETPEN_IND          number(1) default 0,
 SETDEFINT_IND       number(1) default 0,
 SETOVRINT_IND       number(1) default 0,
 ENDBALTAXNET_IND    number(1) default 0,
 ENDBALPENNET_IND    number(1) default 0,
 ENDBALDEFINTNET_IND number(1) default 0,
 ENDBALOVRINTNET_IND number(1) default 0,
 SETTAXNET_IND       number(1) default 0,
 SETPENNET_IND       number(1) default 0,
 SETDEFINTNET_IND    number(1) default 0,
 SETOVRINTNET_IND    number(1) default 0,
 MANUAL_SELECT       number(1) default 0
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_DATA_CONTROL
   add constraint TA_DMI_DATA_CONTROL_PK
       primary key (DMI_DATA_CONTROL_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_DATA_CONTROL
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_DATA_CONTROL
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

create table PWRPLANT.TAX_ACCRUAL_DMI_DATA_CON_DTLS
(
 DMI_DATA_CONTROL_ID number(22) not null,
 FILTER_TYPE_ID      number(22) not null,
 FILTER_VALUE_CHAR   varchar2(35) not null,
 FILTER_VALUE_ID     number(22) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_DATA_CON_DTLS
   add constraint TA_DMI_DATA_CON_DTL_PK
       primary key (DMI_DATA_CONTROL_ID, FILTER_TYPE_ID, FILTER_VALUE_CHAR, FILTER_VALUE_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_DATA_CON_DTLS
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_DATA_CON_DTLS
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

alter table PWRPLANT.TAX_ACCRUAL_DMI_DATA_CON_DTLS
   add constraint TA_DMI_DATA_CON_DTLS_ID_FK
       foreign key (DMI_DATA_CONTROL_ID)
       references PWRPLANT.TAX_ACCRUAL_DMI_DATA_CONTROL(DMI_DATA_CONTROL_ID);

create table PWRPLANT.TAX_ACCRUAL_DMI_CLASSIFICATION
(
 DMI_CLASSIFICATION varchar2(2) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(35)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_CLASSIFICATION
   add constraint TA_DMI_CLASS_PK
       primary key (DMI_CLASSIFICATION)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_DATA_CONTROL
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_DATA_CONTROL
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

insert into TAX_ACCRUAL_DMI_CLASSIFICATION
   (DMI_CLASSIFICATION, DESCRIPTION)
values
   ('ST', 'Short-Term');

insert into TAX_ACCRUAL_DMI_CLASSIFICATION
   (DMI_CLASSIFICATION, DESCRIPTION)
values
   ('LT', 'Long-Term');

create global temporary table PWRPLANT.TAX_ACCRUAL_DMI_RESULTS_RAW
(
 JURISDICTION      varchar2(25),
 ENTITY            number(22),
 ISSUE             number(22),
 TAX_YEAR          number(4),
 CLASSIFICATION    varchar2(2),
 DEFERRAL          varchar2(35),
 STATUS            varchar2(35),
 ENDBALUTP         number(22,2),
 ENDBALTAX         number(22,2),
 ENDBALPEN         number(22,2),
 ENDBALDEFINT      number(22,2),
 ENDBALOVRINT      number(22,2),
 SETTAX            number(22,2),
 SETPEN            number(22,2),
 SETDEFINT         number(22,2),
 SETOVRINT         number(22,2),
 ENDBALTAXNET      number(22,2),
 ENDBALPENNET      number(22,2),
 ENDBALDEFINTNET   number(22,2),
 ENDBALOVRINTNET   number(22,2),
 SETTAXNET         number(22,2),
 SETPENNET         number(22,2),
 SETDEFINTNET      number(22,2),
 SETOVRINTNET      number(22,2)
) on commit preserve rows;

alter table PWRPLANT.TAX_ACCRUAL_DMI_ID
   add constraint TA_DMI_ID_JUR_FK
       foreign key (DMI_JURISDICTION)
       references PWRPLANT.TAX_ACCRUAL_DMI_JURISDICTION(DMI_JURISDICTION);

alter table PWRPLANT.TAX_ACCRUAL_DMI_ID
   add constraint TA_DMI_ID_ENTITY_FK
       foreign key (DMI_ENTITY_ID)
       references PWRPLANT.TAX_ACCRUAL_DMI_COMPANY(DMI_ENTITY_ID);

alter table PWRPLANT.TAX_ACCRUAL_DMI_ID
   add constraint TA_DMI_ID_ISSUE_FK
       foreign key (DMI_ISSUE_ID)
       references PWRPLANT.TAX_ACCRUAL_DMI_ISSUE (DMI_ISSUE_ID);

alter table PWRPLANT.TAX_ACCRUAL_DMI_PROCESS_INFO
   add constraint TA_DMI_PROC_INFO_VERID
       foreign key (TA_VERSION_ID)
       references PWRPLANT.TAX_ACCRUAL_VERSION(TA_VERSION_ID);

alter table PWRPLANT.TAX_ACCRUAL_DMI_PROCESS_INFO
   add constraint TA_DMI_PROC_INFO_COID
       foreign key (COMPANY_ID)
       references PWRPLANT.COMPANY_SETUP(COMPANY_ID);

alter table PWRPLANT.TAX_ACCRUAL_DMI_RESULTS
   add constraint TA_DMI_RESULTS_VERID
       foreign key (TA_VERSION_ID)
       references PWRPLANT.TAX_ACCRUAL_VERSION(TA_VERSION_ID);

alter table PWRPLANT.TAX_ACCRUAL_DMI_RESULTS
   add constraint TA_DMI_RESULTS_DMIID
       foreign key (DMI_ID)
       references PWRPLANT.TAX_ACCRUAL_DMI_ID(DMI_ID);

create global temporary table PWRPLANT.TAX_ACCRUAL_DMI_DATA_CON_TMP
(
 COMPANY_ID          number(22),
 EXTERNAL_KEY        varchar2(35),
 DMI_ID              number(22),
 MULT                number(22,8),
 ENDBALUTP_IND       number(1),
 ENDBALTAX_IND       number(1),
 ENDBALPEN_IND       number(1),
 ENDBALDEFINT_IND    number(1),
 ENDBALOVRINT_IND    number(1),
 SETTAX_IND          number(1),
 SETPEN_IND          number(1),
 SETDEFINT_IND       number(1),
 SETOVRINT_IND       number(1),
 ENDBALTAXNET_IND    number(1),
 ENDBALPENNET_IND    number(1),
 ENDBALDEFINTNET_IND number(1),
 ENDBALOVRINTNET_IND number(1),
 SETTAXNET_IND       number(1),
 SETPENNET_IND       number(1),
 SETDEFINTNET_IND    number(1),
 SETOVRINTNET_IND    number(1)
) on commit preserve rows;

alter table PWRPLANT.TAX_ACCRUAL_DMI_COMPANY
   add constraint TA_DMI_CO_CO_ID
       foreign key (COMPANY_ID)
       references PWRPLANT.COMPANY_SETUP(COMPANY_ID);

insert into TAX_ACCRUAL_PT_IND (PT_IND, DESCRIPTION) values (6, 'DMI');

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID,
          TRUEUP_ID,
          6,
          CURRENT_MONTH_TRUEUP,
          OUT_MONTH_TRUEUP,
          CURRENT_MONTH_TRUEUP_DT,
          OUT_MONTH_TRUEUP_DT,
          PULL_ACTUALS
     from TAX_ACCRUAL_MONTH_TYPE_TRUEUP
    where FROM_PT_IND = 4;

alter table PWRPLANT.TAX_ACCRUAL_DMI_DRILL
   add constraint TA_DMI_DRILL_FK
       foreign key (M_ID)
       references PWRPLANT.TAX_ACCRUAL_CONTROL(M_ID);

delete from TAX_ACCRUAL_SYSTEM_CONTROL where CONTROL_ID = 74;

alter table PWRPLANT.TAX_ACCRUAL_PROCESS_INFO
   add PROCESS_DMI number(22) default 0;

delete from TAX_ACCRUAL_TAX_CONTROL where T_ID = 10;

insert into TAX_ACCRUAL_TAX_CONTROL (T_ID, ELECTION, value) values (10, 'Process DMI', 0);

insert into TAX_ACCRUAL_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
values
   (49, 'DMI Results Character Length', 2, null,
    'In some instances, each character in the DMI results string is actually read as 2 characters in PB.  The default is ' ||
     '2 for PB11, and should be 1 for PB9.', -1);

create table PWRPLANT.TAX_ACCRUAL_DMI_DEFERRAL_TYPE
(
 DEFERRAL_TYPE varchar2(35) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_DEFERRAL_TYPE
   add constraint TA_DMI_DEF_TYPE
       primary key (DEFERRAL_TYPE)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_DEFERRAL_TYPE
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_DEFERRAL_TYPE
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

insert into TAX_ACCRUAL_DMI_DEFERRAL_TYPE (DEFERRAL_TYPE) values ('Temporary');
insert into TAX_ACCRUAL_DMI_DEFERRAL_TYPE (DEFERRAL_TYPE) values ('Permanent');

create table PWRPLANT.TAX_ACCRUAL_DMI_STATUS
(
 status      varchar2(35) not null,
 time_stamp  date,
 user_id     varchar2(18)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_STATUS
   add constraint TA_DMI_STATUS_PK
       primary key (STATUS)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_STATUS
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_STATUS
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

insert into TAX_ACCRUAL_DMI_STATUS (STATUS) values ('Open');
insert into TAX_ACCRUAL_DMI_STATUS (STATUS) values ('Settled');
insert into TAX_ACCRUAL_DMI_STATUS (STATUS) values ('Agreed');

create table PWRPLANT.TAX_ACCRUAL_DMI_FILTER_TYPE
(
 FILTER_TYPE_ID    number(22) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 DESCRIPTION       varchar2(35)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_FILTER_TYPE
   add constraint TA_DMI_FILTER_TYPE_PK
       primary key (FILTER_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_FILTER_TYPE
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_FILTER_TYPE
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

insert into TAX_ACCRUAL_DMI_FILTER_TYPE
(FILTER_TYPE_ID, DESCRIPTION)
select 1, 'Status' from dual union all
select 2, 'Deferral Type' from dual union all
select 3, 'Classification' from dual union all
select 4, 'Jurisdiction' from dual union all
select 5, 'Issue' from dual union all
select 6, 'Tax Year' from dual;

alter table PWRPLANT.TAX_ACCRUAL_DMI_DATA_CON_DTLS
   add constraint TA_DMI_DATA_CON_DTLS_TYPE_FK
       foreign key (FILTER_TYPE_ID)
       references PWRPLANT.TAX_ACCRUAL_DMI_FILTER_TYPE(FILTER_TYPE_ID);

create table PWRPLANT.TAX_ACCRUAL_DMI_TAX_YEAR
(
 tax_year   number(22) not null,
 time_stamp date,
 user_id    varchar2(18)
);

alter table PWRPLANT.TAX_ACCRUAL_DMI_TAX_YEAR
   add constraint TA_DMI_TAX_YEAR_PK
       primary key (TAX_YEAR)
       using index tablespace PWRPLANT_IDX;

create or replace trigger PWRPLANT.TAX_ACCRUAL_DMI_TAX_YEAR
before update or insert on PWRPLANT.TAX_ACCRUAL_DMI_TAX_YEAR
FOR EACH ROW
BEGIN
   :NEW.USER_ID := USER;
   :NEW.TIME_STAMP := SYSDATE;
END;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (4, 0, 10, 3, 3, 0, 2698, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_002698_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON