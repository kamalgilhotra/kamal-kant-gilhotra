/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009187_sys_2.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/12/2012 Joseph King    Table Maintenance
||============================================================================
*/

update POWERPLANT_COLUMNS
   set DROPDOWN_NAME = 'yes_no'
 where TABLE_NAME = 'cr_dd_sources_criteria_fields'
   and LOWER(trim(DROPDOWN_NAME)) in ('qf_yes_no', 'rf_yes_no', 'rom_yes_no', 'tl_yes_no');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (118, 0, 10, 3, 4, 0, 9187, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009187_sys_2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
