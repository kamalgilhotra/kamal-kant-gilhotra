 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048160_3_reg_alloc_dyn_convert_dml.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 08/30/2017 Sarah Byers    Convert reg_acct_id to reg_alloc_acct_id for dynamic allocator basis
 ||============================================================================
 */ 
 

 update reg_jur_allocator_dyn d
    set reg_alloc_acct_id = (
      select a.reg_alloc_acct_id from reg_jur_alloc_account a
       where d.reg_jur_template_id = a.reg_jur_template_id
         and d.reg_acct_id = a.reg_acct_id
         and d.reg_alloc_target_id = a.reg_alloc_target_id)
 where d.reg_alloc_target_id is not null
   and d.reg_alloc_acct_id = d.reg_acct_id
   and exists (
      select 1 from reg_jur_alloc_account a
       where d.reg_jur_template_id = a.reg_jur_template_id
         and d.reg_acct_id = a.reg_acct_id
         and d.reg_alloc_target_id = a.reg_alloc_target_id);

 
 update reg_case_allocator_dyn d
    set reg_alloc_acct_id = (
      select a.reg_alloc_acct_id from reg_case_alloc_account a
       where d.reg_case_id = a.reg_case_id
         and d.reg_acct_id = a.reg_acct_id
         and d.reg_alloc_target_id = a.reg_alloc_target_id)
 where d.reg_alloc_target_id is not null
   and d.reg_alloc_acct_id = d.reg_acct_id
   and exists (
      select 1 from reg_case_alloc_account a
       where d.reg_case_id = a.reg_case_id
         and d.reg_acct_id = a.reg_acct_id
         and d.reg_alloc_target_id = a.reg_alloc_target_id);
		 
		 
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3681, 0, 2017, 1, 0, 0, 48160, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048160_3_reg_alloc_dyn_convert_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;