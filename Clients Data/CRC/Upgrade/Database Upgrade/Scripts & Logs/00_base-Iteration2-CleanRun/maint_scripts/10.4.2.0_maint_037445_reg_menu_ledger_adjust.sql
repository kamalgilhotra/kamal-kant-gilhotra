/*
||=================================================================================
|| Application: PowerPlant
|| File Name:   maint_037445_reg_menu_ledger_adjust.sql  (part of epic 9415)
||=================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||=================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ---------------------------------------------
|| 10.4.2.0 03/31/2014 Shane "C" Ward Update menu item names in regulatory module
||=================================================================================
*/

--Update reg menu names
update PPBASE_WORKSPACE
   set LABEL = 'History Ledger Adj'
 where LABEL like 'History Ledger Adjustment';

update PPBASE_WORKSPACE
   set LABEL = 'Forecast Ledger Adj'
 where LABEL like 'Forecast Ledger Adjustment%';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1087, 0, 10, 4, 2, 0, 37445, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037445_reg_menu_ledger_adjust.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;