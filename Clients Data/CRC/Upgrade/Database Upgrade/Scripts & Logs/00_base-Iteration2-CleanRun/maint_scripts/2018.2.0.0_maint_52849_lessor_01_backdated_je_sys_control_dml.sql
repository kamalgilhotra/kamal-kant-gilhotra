/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_52849_lessor_01_backdated_je_sys_control_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.2.0.0 01/28/2019 Sarah Byers    New system control to specify how to handle backdated commencement JEs
||============================================================================
*/

insert into pp_system_control_company (
  control_id, control_name, control_value, description, long_description, company_id)
select max(control_id) + 1, 'Lessor: Allow Backdated JEs', 'Yes', 'dw_yes_no;1', 
       'Allows the user to perform a backdated commencement of a Lessor ILR in which the system will generate true-up amounts during commencement.',
       -1
  from pp_system_control_company;


--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14402, 0, 2018, 2, 0, 0, 52849, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_52849_lessor_01_backdated_je_sys_control_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;