/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030279_taxrpr.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   08/26/2013 Nathan Hollis   Point Release
||============================================================================
*/

--* UPDATE SCRIPT FOR UOP-2390(R)

update PP_REPORTS
   set DESCRIPTION = 'Repairs (R): Retirmnt Reversal Amt', REPORT_NUMBER = 'UOP - 2390',
       PP_REPORT_NUMBER = 'NETWK - 0039'
 where DATAWINDOW = 'dw_rpr_rpt_netwk_process_rets_related';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (533, 0, 10, 4, 1, 0, 30279, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030279_taxrpr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;