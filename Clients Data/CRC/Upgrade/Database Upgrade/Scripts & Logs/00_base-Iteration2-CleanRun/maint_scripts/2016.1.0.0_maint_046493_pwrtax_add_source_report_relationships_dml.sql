/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046493_pwrtax_add_source_report_relationships_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 10/06/2016 Jared Watkins	Insert the relationships between tax plant
||                                      recon line items and their source reports
||============================================================================
*/
delete from tax_plant_recon_source_report;

insert all
--PowerPlan CPR Asset Actuals
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(3, 1, 3, 'Asset - 1020')
--PowerTax Book Basis
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(4, 1, 2, 'PwrTax - 030')
--Total Book to Tax Basis Difference
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(4, 2, 2, 'PwrTax - 030')
--Adds: Ordinary Retirements
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(4, 3, 2, 'PwrTax - 030')
--PowerTax <JUR> Tax Basis
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(4, 4, 2, 'PwrTax - 030')
--PowerTax <JUR> Tax Reserve
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(6, 2, 2, 'PwrTax - 020')
--PowerPlan CPR Depr Reserve Balance
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(5, 1, 3, 'Depr - 1033')
--PowerTax Reserve Balance
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(5, 4, 2, 'PwrTax - 248 ' || chr(38) || ' 249')
--PowerTax Temp Diff - <JUR>
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(6, 8, 2, 'PwrTax - 257')
--Deferred Tax Book Basis Balance
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(7, 8, 2, 'PwrTax - 249')
--PowerTax Book Basis Balance - 2 lines
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(3, 4, 2, 'PwrTax - 030')
  into tax_plant_recon_source_report(tax_pr_section_id, sort_order, recon_source, source_report)
  values(7, 9, 2, 'PwrTax - 030')
select * from dual;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3307, 0, 2016, 1, 0, 0, 46493, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046493_pwrtax_add_source_report_relationships_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
