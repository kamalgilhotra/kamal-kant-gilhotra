 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044593_sys_email_mobile_link_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version     Date       Revised By     Reason for Change
 || --------    ---------- -------------- ----------------------------------------
 || 2015.2.0.0  08/21/2015 David Haupt    Adding a system control for Mobile Approvals IP
 ||============================================================================
 */

INSERT INTO pp_system_control_company
(control_id, control_name, control_value, description, long_description, company_id)
SELECT * FROM(select Max(control_id)+1, 'Mobile Approvals IP', '127.0.0.1', 'Mobile Approvals address', 'IP where Mobile Approvals login is located', -1 FROM pp_system_control)
WHERE NOT EXISTS (SELECT 1 FROM pp_system_control_company WHERE Lower(control_name) = 'Mobile Approvals IP')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2815, 0, 2015, 2, 0, 0, 044593, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044593_sys_email_mobile_link_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;