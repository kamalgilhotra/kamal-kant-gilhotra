/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011573_version-updates.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/12/2013 Lee Quinn      10.4.1.0 version
|| 10.4.1.0   09/19/2013 Lee Quinn      Updated Post version
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.4.1.0',
       PP_PATCH   = 'Patch: None',
       POWERTAX_VERSION = '10.4.1.0',
       LEASE_VERSION = '10.4.1.0',
       BUDGET_VERSION = '10.4.1.0',
       DATE_INSTALLED = sysdate,
       POST_VERSION = 'Version 10.4.1.0, Sept. 18, 2013'
where PP_VERSION_ID = 1;

update PP_PROCESSES
   set VERSION = '10.4.1.0'
 where trim(LOWER(EXECUTABLE_FILE)) in ('automatedreview.exe',
                                        'batreporter.exe',
                                        'budget_allocations.exe',
                                        'cr_allocations.exe',
                                        'cr_balances.exe',
                                        'cr_balances_bdg.exe',
                                        'cr_batch_derivation.exe',
                                        'cr_batch_derivation_bdg.exe',
                                        'cr_budget_entry_calc_all.exe',
                                        'cr_build_combos.exe',
                                        'cr_build_deriver.exe',
                                        'cr_delete_budget_allocations.exe',
                                        'cr_financial_reports_build.exe',
                                        'cr_flatten_structures.exe',
                                        'cr_man_jrnl_reversals.exe',
                                        'cr_posting.exe',
                                        'cr_posting_bdg.exe',
                                        'cr_sap_trueup.exe',
                                        'cr_sum.exe',
                                        'cr_sum_ab.exe',
                                        'cr_to_commitments.exe',
                                        'cwip_charge.exe',
                                        'populate_matlrec.exe',
                                        'ppmetricbatch.exe',
                                        'pptocr.exe',
                                        'ppverify.exe',
                                        'pp_archive.exe',
                                        'pp_integration.exe',
                                        'cr_derivation_trueup.exe');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '10.4.1.0', 'automatedreview.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'batreporter.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'budget_allocations.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_allocations.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_balances.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_balances_bdg.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_batch_derivation.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_batch_derivation_bdg.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_budget_entry_calc_all.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_build_combos.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_build_deriver.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_delete_budget_allocations.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_financial_reports_build.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_flatten_structures.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_man_jrnl_reversals.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_posting.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_posting_bdg.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_sap_trueup.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_sum.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_sum_ab.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_to_commitments.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cwip_charge.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'populate_matlrec.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'ppmetricbatch.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'pptocr.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'ppverify.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'pp_integration.exe', '10.4.1.0' from dual
union all select '10.4.1.0', 'cr_derivation_trueup.exe', '10.4.1.0' from dual
minus select PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION from PP_VERSION_EXES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (502, 0, 10, 4, 1, 0, 31640, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031640_version-updates.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'),
    SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
