/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045110_lease_edit_forecast_wksp_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/22/2015 Ryan Oliveria  Edit Fcst Version workspace
||============================================================================
*/

insert into ppbase_workspace (
	module, workspace_identifier, label, workspace_uo_name, object_type_id)
values (
	'LESSEE', 'edit_forecast', 'Build / Modify Forecast', 'uo_ls_edit_fcst_wksp', 1);

insert into ppbase_menu_items (
	module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
values (
	'LESSEE', 'edit_forecast', 2, 2, 'Build / Modify Forecast', 'forecast', 'edit_forecast', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2936, 0, 2015, 2, 0, 0, 45110, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045110_lease_edit_forecast_wksp_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;