/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044019_lease_cr_deriver_add_sort_ddl.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2   06/09/2015 William Davis  Add sorting to the CR deriver dropdown config table in lease
 ||============================================================================
 */ 

alter table ls_cr_deriver_dropdown_config
add sort_order number(22,0) null;

comment on column ls_cr_deriver_dropdown_config.sort_order is 'The sort order for the dynamic CR deriver control window in the leased asset window';

alter table ls_cr_deriver_dropdown_config
modify dddw_name null;

alter table ls_cr_deriver_dropdown_config
modify display_column null;

alter table ls_cr_deriver_dropdown_config
modify data_column null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2592, 0, 2015, 2, 0, 0, 044019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044019_lease_cr_deriver_add_sort_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;