/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045022_prov_nr_powertax_pull_report_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2     9/29/2015 Julio Clavijo  Adds report to provision report dw
||============================================================================
*/

insert into pp_reports 
(report_id, 
description, 
long_description, 
subsystem, 
datawindow, 
special_note, 
report_number)
values
(54561, 
'PowerTax Pull Report NR', 
'Reports detailed information regarding the activity making up NonReg items coming from Powertax',
'Tax Accrual',
'dw_tax_accrual_powertax_drill_report_nr',
'OPER_M_ITEM_NO_MONTH',
'Tax Accrual - 54561');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2886, 0, 2015, 2, 0, 0, 045022, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045022_prov_nr_powertax_pull_report_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
