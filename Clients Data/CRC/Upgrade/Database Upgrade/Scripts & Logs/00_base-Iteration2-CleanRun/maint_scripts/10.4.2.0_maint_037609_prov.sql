/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037609_prov.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/03/2014 Nathan Hollis
||============================================================================
*/

update PP_REPORTS set SUBSYSTEM = 'Tax Accrual' where REPORT_NUMBER in ('Tax Accrual - 51060');

update PP_REPORTS
   set SUBSYSTEM = 'Tax Accruala'
 where REPORT_NUMBER in ('Tax Accrual - 51002',
                         'Tax Accrual - 51015',
                         'Tax Accrual - 51030',
                         'Tax Accrual - 51037',
                         'Tax Accrual - 51054',
                         'Tax Accrual - 54517',
                         'Tax Accrual - 54521',
                         'Tax Accrual - 58004',
                         'Tax Accrual - 59001',
                         'Tax Accrual - 54518');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1098, 0, 10, 4, 2, 0, 37609, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037609_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
