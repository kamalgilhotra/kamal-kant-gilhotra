/*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_052288_lessee_lessor_01_add_lock_process_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- --------------------------------------
 || 2017.4.0.2 9/11/2018  C.Yura   	  Combine Lessee/Lessor Lock process
 ||============================================================================
 */ 
 
update pp_processes 
set description = 'Lease - Lock', 
long_description = 'Lease - Lock',
executable_file = 'ssp_lease_control.exe LEASE_LOCK'
where lower(executable_file) = 'ssp_lease_control.exe lsr_close_month';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (9742, 0, 2017, 4, 0, 2, 52288, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.2_maint_052288_lessee_lessor_01_add_lock_process_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
