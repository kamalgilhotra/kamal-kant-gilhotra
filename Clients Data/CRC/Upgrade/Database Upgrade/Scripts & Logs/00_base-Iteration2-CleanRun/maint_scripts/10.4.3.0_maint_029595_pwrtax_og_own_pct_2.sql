/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_029595_pwrtax_og_own_pct_2.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By        Reason for Change
|| -------- ---------- --------------    -------------------------------------
|| 10.4.3.0 10/03/2014 Anand Rajashekar  Add table company_setup to tables window
||============================================================================
*/

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY, CWIP_ACCOUNTING,
    DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM, UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT,
    WHERE_CLAUSE)
values
   ('company_percent', TO_DATE('03-OCT-14', 'DD-MON-RR'), 'PWRPLANT', 's', 'Company Percent', 'Company Percent',
    'always', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE,
    RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('company_id', 'company_percent', TO_DATE('03-OCT-14', 'DD-MON-RR'), 'PWRPLANT', 'company', 'p', null, 'Company',
    EMPTY_CLOB(), 1, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE,
    RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('percent', 'company_percent', TO_DATE('03-OCT-14', 'DD-MON-RR'), 'PWRPLANT', null, 'e', null, 'Percent',
    EMPTY_CLOB(), 2, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE,
    RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'company_percent', TO_DATE('03-OCT-14', 'DD-MON-RR'), 'PWRPLANT', null, 'e', null, 'user id',
    EMPTY_CLOB(), 3, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT, RELATED_TYPE,
    RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'company_percent', TO_DATE('03-OCT-14', 'DD-MON-RR'), 'PWRPLANT', null, 'e', null, 'time stamp',
    EMPTY_CLOB(), 4, null, null, null, null, null, null, null, null);

alter table COMPANY_PERCENT
   add constraint CP_COMPANY_ID_FK
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;
	   
insert into pp_reports ( report_id, time_stamp, user_id, description, long_description, subsystem, datawindow, special_note, report_type, time_option, report_number, input_window, filter_option, status, pp_report_subsystem_id, report_type_id, pp_report_time_option_id, pp_report_filter_id, pp_report_status_id, pp_report_envir_id, documentation, user_comment, last_approved_date, pp_report_number, old_report_number, dynamic_dw ) 
values (  403009, sysdate, user, 'Rollforward Report - Own Pct', 'Depreciation Rollforward Report taking into account company ownership percent.', '', 'dw_tax_rpt_roll_forward_pct', '', '', '', 'PwrTax - 031', '', '', '', 1, 130, 50, 53, 1, 3, '', '', null, '', '', 0 );
	   

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1494, 0, 10, 4, 3, 0, 29595, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_029595_pwrtax_og_own_pct_2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;