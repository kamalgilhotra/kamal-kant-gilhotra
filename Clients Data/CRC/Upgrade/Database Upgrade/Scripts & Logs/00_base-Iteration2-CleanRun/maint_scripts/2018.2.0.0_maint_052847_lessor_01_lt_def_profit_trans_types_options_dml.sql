/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052847_lessor_01_lt_def_profit_trans_types_options_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 02/11/2019  Sarah Byers      New trans types to support LT Deferred Profit
||============================================================================
*/

insert into je_trans_type (trans_type, description)
select 4078, '4078 - Lessor LT Deferred Selling Profit Debit'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4078);

insert into je_trans_type (trans_type, description)
select 4079, '4079 - Lessor LT Deferred Selling Profit Credit'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4079);

insert into je_trans_type (trans_type, description)
select 4080, '4080 - Lessor LT Deferred Profit Debit'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4080);

insert into je_trans_type (trans_type, description)
select 4081, '4081 - Lessor ST Deferred Profit Credit'
from dual
where not exists (select 1 from je_trans_type where trans_type = 4081);

insert into je_method_trans_type (je_method_id, trans_type)
select 1, trans_type from je_trans_type
where trans_type IN (4078, 4079, 4080, 4081)
and not exists (select 1 from je_method_trans_type where trans_type IN (4078, 4079, 4080, 4081) and je_method_id = 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14946, 0, 2018, 2, 0, 0, 52847, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052847_lessor_01_lt_def_profit_trans_types_options_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;