/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_045042_cwip_add_cols_to_wo_interface_ocr_stg_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 10/15/2015 Anand R        Add description and long description to the OCR API tables
||============================================================================
*/

alter table wo_interface_ocr_stg
add description VARCHAR2(35)  NULL;

alter table wo_interface_ocr_arc
add description VARCHAR2(35)  NULL;

alter table wo_interface_ocr_stg
add long_description VARCHAR2(254) NULL;

alter table wo_interface_ocr_arc
add long_description VARCHAR2(254) NULL;

comment on column wo_interface_ocr_stg.description is 'Records a short description of a particular OCR.';
comment on column wo_interface_ocr_stg.long_description is 'Records a more detailed description of a particular OCR.';

comment on column wo_interface_ocr_arc.description is 'Records a short description of a particular OCR.';
comment on column wo_interface_ocr_arc.long_description is 'Records a more detailed description of a particular OCR.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2928, 0, 2015, 2, 0, 0, 45042, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045042_cwip_add_cols_to_wo_interface_ocr_stg_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;