/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_044530_proptax_pt_import_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2015.2     07/27/2015 Kevin Dettbarn   Table names incorrect on previous script.
||============================================================================
*/

alter table PT_IMPORT_STMT drop primary key drop index;

alter table PT_IMPORT_STMT_ARCHIVE drop primary key drop index;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2718, 0, 2015, 2, 0, 0, 044530, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044530_proptax_pt_import_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;