/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs6.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   04/02/2013 ALex P.
||============================================================================
*/

--
-- Create a new import type for Work Order Tax Repairs.
--
insert into PP_IMPORT_TYPE ( IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION ) values ( 120, sysdate, user, 'Work Order Tax Repairs', 'Import Work Order Tax Repairs Data', 'rpr_import_wo_tax_repairs', 'rpr_import_wo_tax_repairs_arc', null, 1, 'nvo_rpr_logic_import', '', 'WO Tax Repairs' );

insert into PP_IMPORT_TYPE_SUBSYSTEM ( IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID ) values ( 120, 5, sysdate, user );

insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 120, 'work_order_id', sysdate, user, 'Work Order', 'work_order_xlate', 1, 2, 'number(22,0)', 'work_order_control', '', 1, null );
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 120, 'tax_expense_test_id', sysdate, user, 'Repair Test', 'tax_expense_test_xlate', 0, 1, 'number(22,0)', 'wo_tax_expense_test', '', 1, null );
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 120, 'repair_location_method_id', sysdate, user, 'Repair Location Method', 'repair_location_method_xlate', 0, 1, 'number(22,0)', 'wo_repair_location_method', '', 1, null );
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 120, 'repair_location_id', sysdate, user, 'Repair Location', 'repair_location_xlate', 0, 1, 'number(22,0)', 'repair_location', '', 1, null );
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 120, 'te_aggregation_id', sysdate, user, 'Aggregation', 'te_aggregation_xlate', 0, 1, 'number(22,0)', 'te_aggregation', '', 1, null );
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 120, 'repair_loc_rollup_id', sysdate, user, 'Repair Location Rollup', 'repair_loc_rollup_xlate', 0, 1, 'number(22,0)', 'repair_loc_rollup', '', 1, null );
insert into PP_IMPORT_COLUMN ( IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID ) values ( 120, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null );

insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN ) values ( 1021, sysdate, user, 'Repair Location Method.Description', 'The passed in value corresponds to the Repair Location Method: Description field.  Translate to the Repair Location Method ID using the Description column on the Repair Location Method table.', 'repair_location_method_id', '( select rlm.repair_location_method_id from wo_repair_location_method rlm where upper( trim( <importfield> ) ) = upper( trim( rlm.description ) ) )', 0, 'wo_repair_location_method', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN ) values ( 1022, sysdate, user, 'Aggregation.Description', 'The passed in value corresponds to the TE Aggregation: Description field.  Translate to the TE Aggregation ID using the Description column on the TE Aggregation table.', 'te_aggregation_id', '( select ta.te_aggregation_id from te_aggregation ta where upper( trim( <importfield> ) ) = upper( trim( ta.description ) ) )', 0, 'te_aggregation', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP ( IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN ) values ( 1023, sysdate, user, 'Aggregation.Long Description', 'The passed in value corresponds to the TE Aggregation: Long Description field.  Translate to the TE Aggregation ID using the Long Description column on the TE Aggregation table.', 'te_aggregation_id', '( select ta.te_aggregation_id from te_aggregation ta where upper( trim( <importfield> ) ) = upper( trim( ta.long_description ) ) )', 0, 'te_aggregation', 'long_description', '', '', null );

insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'work_order_id', 110, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'work_order_id', 111, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'tax_expense_test_id', 1009, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'repair_location_method_id', 1021, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'repair_location_id', 1004, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'repair_location_id', 1003, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'te_aggregation_id', 1022, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'te_aggregation_id', 1023, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'repair_loc_rollup_id', 1010, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'repair_loc_rollup_id', 1011, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'company_id', 19, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'company_id', 21, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP ( IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID ) values ( 120, 'company_id', 20, sysdate, user );

commit;

create table RPR_IMPORT_WO_TAX_REPAIRS
(
 IMPORT_RUN_ID                 number(22,0)   not null,
 LINE_ID                       number(22,0)   not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 COMPANY_XLATE                 varchar2(254),
 COMPANY_ID                    number(22,0),
 WORK_ORDER_XLATE              varchar2(254),
 WORK_ORDER_ID                 number(22,0),
 TAX_EXPENSE_TEST_XLATE        varchar2(254),
 TAX_EXPENSE_TEST_ID           number(22,0),
 REPAIR_LOCATION_METHOD_XLATE  varchar2(254),
 REPAIR_LOCATION_METHOD_ID     number(22,0),
 REPAIR_LOCATION_XLATE         varchar2(254),
 REPAIR_LOCATION_ID            number(22,0),
 TE_AGGREGATION_XLATE          varchar2(254),
 TE_AGGREGATION_ID             number(22,0),
 REPAIR_LOC_ROLLUP_XLATE       varchar2(254),
 REPAIR_LOC_ROLLUP_ID          number(22,0),
 IS_MODIFIED                   number(22,0),
 ERROR_MESSAGE                 varchar2(4000)
);

alter table RPR_IMPORT_WO_TAX_REPAIRS
   add constraint RPR_IMP_WOTR_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_WO_TAX_REPAIRS
   add constraint RPR_IMP_WOTR_IMP_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PP_IMPORT_RUN;

create table RPR_IMPORT_WO_TAX_REPAIRS_ARC
(
 IMPORT_RUN_ID                 number(22,0)   not null,
 LINE_ID                       number(22,0)   not null,
 TIME_STAMP                    date,
 USER_ID                       varchar2(18),
 COMPANY_XLATE                 varchar2(254),
 COMPANY_ID                    number(22,0),
 WORK_ORDER_XLATE              varchar2(254),
 WORK_ORDER_ID                 number(22,0),
 TAX_EXPENSE_TEST_XLATE        varchar2(254),
 TAX_EXPENSE_TEST_ID           number(22,0),
 REPAIR_LOCATION_METHOD_XLATE  varchar2(254),
 REPAIR_LOCATION_METHOD_ID     number(22,0),
 REPAIR_LOCATION_XLATE            varchar2(254),
 REPAIR_LOCATION_ID               number(22,0),
 TE_AGGREGATION_XLATE          varchar2(254),
 TE_AGGREGATION_ID             number(22,0),
 REPAIR_LOC_ROLLUP_XLATE       varchar2(254),
 REPAIR_LOC_ROLLUP_ID          number(22,0)
);

alter table RPR_IMPORT_WO_TAX_REPAIRS_ARC
   add constraint RPR_IMP_WOTR_ARC_PK
       primary key ( IMPORT_RUN_ID, LINE_ID )
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_WO_TAX_REPAIRS_ARC
   add constraint RPR_IMP_WOTR_ARC_IMP_RUN_FK
       foreign key ( IMPORT_RUN_ID )
       references PP_IMPORT_RUN;

--
-- Remove add_test from the repair_thresholds import.
--
update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID + 100
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101)
   and exists (select 'x'
          from PP_IMPORT_TEMPLATE_FIELDS INFLDS
         where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
           and COLUMN_NAME = 'add_test')
   and FIELD_ID > (select FIELD_ID
                     from PP_IMPORT_TEMPLATE_FIELDS INFLDS
                    where INFLDS.IMPORT_TEMPLATE_ID = FLDS.IMPORT_TEMPLATE_ID
                      and COLUMN_NAME = 'add_test');

delete from PP_IMPORT_TEMPLATE_FIELDS
 where COLUMN_NAME = 'add_test'
   and IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101);

update PP_IMPORT_TEMPLATE_FIELDS FLDS
   set FIELD_ID = FIELD_ID - 101
 where IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where IMPORT_TYPE_ID = 101)
   and FIELD_ID > 100;

delete from PP_IMPORT_COLUMN
 where IMPORT_TYPE_ID = 101
   and COLUMN_NAME = 'add_test';

alter table RPR_IMPORT_THRESHOLD     drop column ADD_TEST;
alter table RPR_IMPORT_THRESHOLD_ARC drop column ADD_TEST;

begin
   execute immediate ('alter table REPAIR_THRESHOLDS DROP COLUMN RETIRE_TEST');
   DBMS_OUTPUT.PUT_LINE('RETIRE_TEST column dropped from REPAIR_THRESHOLDS table.');
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('RETIRE_TEST column didn''t need to be dropped from REPAIR_THRESHOLDS table.');
end;
/

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID,
    DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP,
    VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE)
values
   (54, 'Work Order Number', 'dw', 'cpr_ledger.work_order_number', null,
    'dw_rpr_work_order_number_filter', 'work_order_number', 'work_order_number', 'C', null, null,
    null, null, null, null, null, 1);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (37, 54, null, null);

update WO_TAX_EXPENSE_TEST
   set TAX_STATUS_DEFAULT_CAPITAL =
        (select min(TAX_STATUS_ID)
           from WO_TAX_STATUS
          where DESCRIPTION = 'Capital - Exclude from Test')
 where TAX_STATUS_DEFAULT_CAPITAL is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (341, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs6.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;