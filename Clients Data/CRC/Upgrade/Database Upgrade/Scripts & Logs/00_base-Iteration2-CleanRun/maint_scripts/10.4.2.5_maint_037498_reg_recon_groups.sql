/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037498_reg_recon_groups.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.5 04/14/2014 Shane "C" Ward Create Recon Groups
||========================================================================================
*/

create table REG_HIST_RECON_GROUP
(
 RECON_GROUP_ID number(22,0) not null,
 DESCRIPTION    varchar2(50) not null
);

comment on table REG_HIST_RECON_GROUP is 'Controls grouping for recon items.';
comment on column REG_HIST_RECON_GROUP.RECON_GROUP_ID is 'ID for recon group.';
comment on column REG_HIST_RECON_GROUP.DESCRIPTION is 'Name of recon group';

alter table REG_HIST_RECON_GROUP
   add constraint PK_REG_HIST_RECON_GROUP
       primary key (RECON_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_HIST_RECON_ITEMS add RECON_GROUP_ID number(22,0);

alter table REG_HIST_RECON_ITEMS
   add constraint FK1_REG_HIST_RECON_ITEMS
       foreign key (RECON_GROUP_ID)
       references REG_HIST_RECON_GROUP (RECON_GROUP_ID);

comment on column REG_HIST_RECON_ITEMS.RECON_GROUP_ID is 'ID for recon group.';	   
--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1130, 0, 10, 4, 2, 5, 37498, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.5_maint_037498_reg_recon_groups.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
