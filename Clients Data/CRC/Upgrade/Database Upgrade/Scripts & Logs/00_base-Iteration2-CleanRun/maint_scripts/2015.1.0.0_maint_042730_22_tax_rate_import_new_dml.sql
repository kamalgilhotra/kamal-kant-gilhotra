/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_22_tax_rate_import_new_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

update pp_import_column
set import_column_name = 'ls_tax_district_xlate'
where column_name = 'ls_tax_district_id'
;

update pp_import_template_fields
set import_lookup_id = (select import_lookup_id from pp_import_lookup where column_name = 'tax_local_id')
where import_type_id in (258,259)
and column_name = 'tax_local_id';

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql, is_derived,
   lookup_table_name, lookup_column_name)
select
   (select max(import_lookup_id)+1 from pp_import_lookup) AS import_lookup_id,
   'Lease Tax District.Long Description' AS description,
   'The passed in value corresponds to the Tax District: Long Description field.  Translate to the Tax District ID using the Long Description column on the Tax District table.' AS long_description,
   'tax_district_id' AS column_name,
   '( select td.tax_district_id from tax_district td where upper( trim( <importfield> ) ) = upper( trim( td.long_description ) ) )' AS lookup_sql,
   0 AS is_derived,
   'tax_district' AS lookup_table_name,
   'long_description' AS lookup_column_name
from dual
where not exists(
   select 1
   from pp_import_lookup
   where lookup_table_name = 'tax_district'
   and lookup_column_name = 'long_description')
;

insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
select 258, 'ls_tax_district_id', import_lookup_id
from pp_import_lookup
where lookup_table_name = 'tax_district'
and lookup_column_name = 'long_description'
and not exists(
   select 1
   from pp_import_column_lookup
   where import_type_id = 258
   and column_name = 'ls_tax_district_id')
;

update pp_import_template_fields
set import_lookup_id = (select import_lookup_id from pp_import_lookup where lookup_table_name = 'tax_district' and lookup_column_name = 'long_description')
where column_name = 'ls_tax_district_id'
;

update pp_import_template_fields
set import_lookup_id = 1
where import_lookup_id = 2
and import_type_id between 250 and 262
;


commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2396, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_22_tax_rate_import_new_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;