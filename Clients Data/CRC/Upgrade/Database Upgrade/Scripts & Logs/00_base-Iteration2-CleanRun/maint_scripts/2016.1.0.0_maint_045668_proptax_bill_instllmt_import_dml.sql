/*
||==========================================================================================
|| Application: PowerPlan
|| Module: Property Tax
|| File Name:   maint_045668_proptax_bill_instllmt_import_dml.sql
||==========================================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2016.1    04/27/2016  Graham Miller    Add import type to import to Bills Center by installment
||==========================================================================================
*/


INSERT INTO "PP_IMPORT_TYPE" ("IMPORT_TYPE_ID", "TIME_STAMP", "USER_ID", "DESCRIPTION", "LONG_DESCRIPTION", "IMPORT_TABLE_NAME", "ARCHIVE_TABLE_NAME", "PP_REPORT_FILTER_ID",
"ALLOW_UPDATES_ON_ADD", "DELEGATE_OBJECT_NAME", "ARCHIVE_ADDITIONAL_COLUMNS", "AUTOCREATE_DESCRIPTION", "AUTOCREATE_RESTRICT_SQL") VALUES (57, to_date('2013-09-26 11:03:08',
'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Update : Bills by Installment', 'Import Updates to Bills by Installment', 'pt_import_stmt_installment', 'pt_import_stmt_installment_arc', null, 0, 'nvo_ptc_logic_import', null, 'Bill', null)
;


INSERT INTO "PP_IMPORT_TYPE_SUBSYSTEM" ("IMPORT_TYPE_ID", "IMPORT_SUBSYSTEM_ID", "TIME_STAMP", "USER_ID") VALUES (57, 1, to_date('2013-05-07 15:19:35', 'yyyy-mm-dd hh24:mi:ss'),
'PWRPLANT') ;




INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (57,
'credit_amount', to_date('2013-05-07 15:19:21', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Credit Amount', null, 0, 1, 'number(22,2)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (57,
'interest_amount', to_date('2013-05-07 15:19:21', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Interest Amount', null, 0, 1, 'number(22,2)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (57,
'penalty_amount', to_date('2013-05-07 15:19:21', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Penalty Amount', null, 0, 1, 'number(22,2)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (57,
'statement_id', to_date('2013-05-07 15:19:21', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Statement', 'statement_xlate', 1, 3, 'number(22,0)', 'pt_statement', null, 1, null, null,
null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (57,
'statement_year_id', to_date('2013-05-07 15:19:21', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Statement Year', 'statement_year_xlate', 1, 1, 'number(22,0)', 'pt_statement_year', null,
 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (57,
'tax_amount', to_date('2013-05-07 15:19:21', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Tax Amount', null, 0, 1, 'number(22,2)', null, null, 1, null, null, null, null) ;
INSERT INTO "PP_IMPORT_COLUMN" ("IMPORT_TYPE_ID", "COLUMN_NAME", "TIME_STAMP", "USER_ID", "DESCRIPTION", "IMPORT_COLUMN_NAME", "IS_REQUIRED", "PROCESSING_ORDER", "COLUMN_TYPE",
"PARENT_TABLE", "PARENT_TABLE_PK_COLUMN2", "IS_ON_TABLE", "AUTOCREATE_IMPORT_TYPE_ID", "PARENT_TABLE_PK_COLUMN", "DEFAULT_VALUE", "PARENT_TABLE_PK_COLUMN3") VALUES (57,
'installment_id', to_date('2013-05-07 15:19:21', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 'Installment', null, 1, 1, 'number(22,0)', null, null, 1, null, null, null, null) ;



INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 170, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 171, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 172, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 173, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 174, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 175, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 176, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 177, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_id', 178, to_date('2013-05-07 15:19:29',
 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_year_id', 179,
to_date('2013-05-07 15:19:29', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_year_id', 180,
to_date('2013-05-07 15:19:29', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;
INSERT INTO "PP_IMPORT_COLUMN_LOOKUP" ("IMPORT_TYPE_ID", "COLUMN_NAME", "IMPORT_LOOKUP_ID", "TIME_STAMP", "USER_ID") VALUES (57, 'statement_year_id', 181,
to_date('2013-05-07 15:19:29', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT') ;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3172, 0, 2016, 1, 0, 0, 045668, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045668_proptax_bill_instllmt_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;