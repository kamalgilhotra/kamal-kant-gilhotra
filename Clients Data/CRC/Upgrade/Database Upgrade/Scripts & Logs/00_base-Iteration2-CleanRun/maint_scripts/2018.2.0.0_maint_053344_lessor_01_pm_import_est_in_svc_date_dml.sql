/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053344_lessor_01_pm_import_est_in_svc_date_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 04/05/2019  Sarah Byers      Add Partial Month Payment Term Type functionality to Lessor
||============================================================================
*/

update pp_import_column
   set description = 'Est In Svc Date (yyyymmdd)'
 where import_type_id = (select import_type_id from pp_import_type where upper(import_table_name) = 'LSR_IMPORT_ILR')
   and column_name = 'est_in_svc_date'; 


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16622, 0, 2018, 2, 0, 0, 53344, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053344_lessor_01_pm_import_est_in_svc_date_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
