/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052029_lessee_ilr_header_fx_vw_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  ------------------------------------
|| 2017.4.0.0 07/19/2018 Sarah Byers       Restrict to Actual rates
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_ILR_HEADER_FX_VW AS
WITH cur 
as (select  ls_currency_type_id as ls_cur_type, 
            currency_id, 
            currency_display_symbol, 
            iso_code, 
            CASE ls_currency_type_id
              WHEN 1 THEN 1
              ELSE NULL
            END as contract_approval_rate
    FROM currency
    CROSS JOIN ls_lease_currency_type),
calc_rate 
AS (SELECT  company_id, 
            contract_currency_id, 
            company_currency_id,
            accounting_month, 
            exchange_date, rate
    FROM ls_lease_calculated_date_rates a
    WHERE accounting_month = (select max(accounting_month)
                              from ls_lease_calculated_date_rates b
                              where a.company_id = b.company_id
                              and a.contract_currency_id = b.contract_currency_id
                              and a.company_currency_id = b.company_currency_id
                              GROUP BY company_id, contract_currency_id, company_currency_id)
      and exchange_rate_type_id = 1),
rate_now 
AS (SELECT  currency_from,
            currency_to,
            exchange_rate_type_id,
            exchange_date,
            rate
    FROM (SELECT  currency_from,
                  currency_to,
                  rate,
                  exchange_rate_type_id,
                  exchange_date,
                  ROW_NUMBER() OVER(PARTITION BY currency_from, currency_to ORDER BY exchange_date DESC) AS rn
          FROM currency_rate_default_dense 
          WHERE trunc(exchange_date,'MONTH') <= trunc(SYSDATE,'MONTH')
          AND exchange_rate_type_id = 1)
    WHERE rn = 1) 
SELECT
  ilr.ilr_id,
  ilr.ilr_number,
  ilr.current_revision,
  liasob.revision,
  liasob.set_of_books_id,
  cur.ls_cur_type,
  lio.in_service_exchange_rate,
  cr.rate,
  calc_rate.rate calc_rate,
  cur.currency_display_symbol,
  cur.iso_code,
  lease.contract_currency_id,
  cur.currency_id,
  cr.exchange_date,
  calc_rate.exchange_date calc_date,
  ilr.lease_id,
  ilr.company_id,
  liasob.internal_rate_return,
  liasob.is_om,
  lio.purchase_option_amt * nvl(calc_rate.rate, cr.rate) purchase_option_amt,
  lio.termination_amt * nvl(calc_rate.rate, cr.rate) termination_amt,
  liasob.net_present_value * nvl(calc_rate.rate, cr.rate) net_present_value,
  liasob.capital_cost * nvl(nvl(cur.contract_approval_rate, lio.in_service_exchange_rate), cr.rate) capital_cost,
  liasob.current_lease_cost * nvl(nvl(cur.contract_approval_rate, lio.in_service_exchange_rate), cr.rate) current_lease_cost,
  lease.lease_number,
  po.description purchase_option_type,
  lio.lease_cap_type_id,
  lio.inception_air,
  ilr.est_in_svc_date
FROM ls_ilr ilr
INNER JOIN ls_ilr_amounts_set_of_books liasob
ON ilr.ilr_id = liasob.ilr_id
INNER JOIN ls_ilr_options lio
ON lio.ilr_id = liasob.ilr_id
AND lio.revision = liasob.revision
INNER JOIN ls_lease lease
ON ilr.lease_id = lease.lease_id
INNER JOIN currency_schema cs
  ON ilr.company_id = cs.company_id
inner join cur on cur.currency_id = case cur.ls_cur_type
                                      when 1 then lease.contract_currency_id
                                      when 2 then cs.currency_id
                                      else null
                                    end
INNER JOIN rate_now cr ON cur.currency_id = cr.currency_to AND lease.contract_currency_id = cr.currency_from
LEFT OUTER JOIN calc_rate
  ON calc_rate.company_id = ilr.company_id
  AND calc_rate.contract_currency_id = lease.contract_currency_id
  AND calc_rate.company_currency_id = cur.currency_id
INNER JOIN ls_purchase_option_type po
  on lio.purchase_option_type_id = po.purchase_option_type_id
where cs.currency_type_id = 1
and cr.exchange_rate_type_id = 1;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8323, 0, 2017, 4, 0, 0, 52029, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052029_lessee_ilr_header_fx_vw_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
