/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052064_lessor_01_add_missing_je_trans_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 07/24/2018 Anand R          Add missing rows into je_method trans types for lessee and lessor
||============================================================================
*/

insert into je_method_trans_type(je_method_id, trans_type)    
select 1 je_method_id, trans_type 
from (    
  select trans_type 
  from je_trans_type    
  where trans_type between 3000 and 5000
   and trans_type not in ( 
        select distinct trans_type 
		from je_method_trans_type 
		where trans_type between 3000 and 5000 )
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (8602, 0, 2017, 4, 0, 0, 52064, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_052064_lessor_01_add_missing_je_trans_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
  
