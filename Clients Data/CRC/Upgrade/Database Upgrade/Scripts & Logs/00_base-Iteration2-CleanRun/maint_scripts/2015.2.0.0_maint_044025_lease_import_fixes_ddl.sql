/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044025_lease_import_fixes_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/15/2015 Will Davis     Fixed various import issues
||============================================================================
*/

declare
counter number;
begin
select count(1) into counter from all_tab_columns where table_name = 'LS_IMPORT_COMPONENT' and column_name = 'ILR_ID';
if counter>0 then
  execute immediate 'alter table ls_import_component drop column ilr_id';
end if;
select count(1) into counter from all_tab_columns where table_name = 'LS_IMPORT_COMPONENT_ARCHIVE' and column_name = 'ILR_ID';
if counter>0 then
  execute immediate 'alter table ls_import_component_archive drop column ilr_id';
end if;
end;
/

alter table ls_import_component add ls_asset_override varchar2(254);
alter table ls_import_component_archive add ls_asset_override varchar2(254);
alter table ls_import_asset_taxes add ls_asset_override varchar2(254);
alter table ls_import_asset_taxes_archive add ls_asset_override varchar2(254);
alter table ls_import_alloc_rows add ls_asset_override varchar2(254);
alter table ls_import_alloc_rows_archive add ls_asset_override varchar2(254);

Comment on column ls_import_component.ls_asset_override is 'Additional column to map asset ID during import process';
Comment on column ls_import_component_archive.ls_asset_override is 'Additional column to map asset ID during import process';
Comment on column ls_import_asset_taxes.ls_asset_override is 'Additional column to map asset ID during import process';
Comment on column ls_import_asset_taxes_archive.ls_asset_override is 'Additional column to map asset ID during import process';
Comment on column ls_import_alloc_rows.ls_asset_override is 'Additional column to map asset ID during import process';
Comment on column ls_import_alloc_rows_archive.ls_asset_override is 'Additional column to map asset ID during import process';

alter table ls_import_asset add estimated_residual_tmp number(22,8);
alter table ls_import_asset_archive add estimated_residual_tmp number(22,8);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2612, 0, 2015, 2, 0, 0, 044025, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044025_lease_import_fixes_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;