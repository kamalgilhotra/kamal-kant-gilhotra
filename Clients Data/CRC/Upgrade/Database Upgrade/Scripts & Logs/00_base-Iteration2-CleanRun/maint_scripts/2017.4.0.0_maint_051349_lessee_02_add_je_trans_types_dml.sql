/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051349_lessee_02_add_je_trans_types_dml.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/24/2018 David Conway     Add new JE Trans Types
|| 2017.4.0.0  06/07/2018 David Conway     Add JE Method Trans Type entries,
||                                         corrected typo in 3067 description.
|| 2017.4.0.0  06/14/2018 David Conway     Added update for new payment_month.
||============================================================================
*/

/* add 3066 - Prepayment Debit, if not already exists */
insert into je_trans_type ( trans_type, description)
select 3066, '3066 - Prepayment Debit'
from dual
where not exists( select 1 from je_trans_type where trans_type = 3066) ;

/* add 3067 - Prepayment Credit, if not already exists */
insert into je_trans_type ( trans_type, description)
select 3067, '3067 - Prepayment Credit'
from dual
where not exists( select 1 from je_trans_type where trans_type = 3067) ;

/* insert the above types into je_method_trans_type, if not already exist */
insert into je_method_trans_type(je_method_id, trans_type)
select 1 AS je_method_id, t.trans_type
from je_trans_type t
  left join je_method_trans_type m on m.trans_type = t.trans_type
where t.trans_type in (3066, 3067)
  and m.trans_type is null ;

/* update new payment_month column with gl_posting_mo_yr */
update ls_payment_hdr
set payment_month = gl_posting_mo_yr
where payment_month is null;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6710, 0, 2017, 4, 0, 0, 51349, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051349_lessee_02_add_je_trans_types_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;