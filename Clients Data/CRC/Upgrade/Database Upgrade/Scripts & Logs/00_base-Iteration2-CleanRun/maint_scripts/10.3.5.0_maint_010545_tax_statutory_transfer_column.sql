/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010545_tax_statutory_transfer_column.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    08/28/2012 Jennifer Butts Point Release
||============================================================================
*/

alter table JURISDICTION add STATUTORY_TRANSFER number (22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (208, 0, 10, 3, 5, 0, 10545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010545_tax_statutory_transfer_column.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
