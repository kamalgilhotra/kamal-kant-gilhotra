/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_039097_pwrtax_TAX_MLP.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| PP Version Version   Date       Revised By    Reason for Change
|| ---------- --------- ---------- ------------- --------------------------
|| 10.4.3.0   1.01      08/14/2014 Andrew Scott  Original Version : initially needed for
||                                               faster K1 export generation and backfilling.
||============================================================================
*/


create or replace package TAX_MLP as
   /*
   ||============================================================================
   || Application: PowerPlan
   || Object Name: TAX_MLP
   || Description: General PLSQL Logic needed for MLP processes
   ||============================================================================
   || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
   ||============================================================================
   ||
   || PP Version  Version    Date           Revised By     Reason for Change
   || ----------  ---------  -------------  -------------  -------------------------
   || 10.4.3.0    1.01       08/14/2014     Andrew Scott   Original Version : initially needed for
   ||                                                      faster K1 export generation and backfilling.
   ||============================================================================
   */

   -- function to get the version of this package.
   function F_GET_VERSION return varchar2;

   -- function to populate k1 export
   function F_TAX_MLP_K1_GEN(A_VERSION_ID             number,
                             A_K1_EXPORT_RUN_ID       number,
                             A_K1_EXPORT_DEFINTION_ID number) return varchar2;

   -- function to backfill k1 export id onto tax record control
   function F_TAX_MLP_K1_TRC_BACKFILL(A_VERSION_ID       number,
                                      A_K1_EXPORT_RUN_ID number) return varchar2;

end TAX_MLP;
/

create or replace package body TAX_MLP as

   --****************************************************************************************************
   --               VARIABLES
   --****************************************************************************************************
   L_PROCESS_ID number;

   --****************************************************************************************************
   --               Start Body
   --****************************************************************************************************

   -- function to get the version of this package.
   function F_GET_VERSION return varchar2 is
   begin
      return '1.01';
   end F_GET_VERSION;

   --****************************************************************************************************
   --               PROCEDURES
   --****************************************************************************************************

   --procedure grabs the process id for the online logs
   procedure P_SETPROCESS is
   begin
      select PROCESS_ID
        into L_PROCESS_ID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = UPPER('PowerTax - MLP K1 Export Generation');
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_PROCESS_ID := -1;
   end P_SETPROCESS;

   --****************************************************************************************************
   --                FUNCTIONS
   --****************************************************************************************************

   --
   -- F_TAX_MLP_K1_GEN : function to populate k1 export
   --
   function F_TAX_MLP_K1_GEN(A_VERSION_ID             number,
                             A_K1_EXPORT_RUN_ID       number,
                             A_K1_EXPORT_DEFINTION_ID number) return varchar2 is
      L_MSG               varchar2(2000);
      L_PKG_VERSION       varchar2(2000);
      L_PREFIX            varchar2(10);
      SQLS                varchar2(2000);
      L_LAST_TAX_EVENT_ID pls_integer;
      L_NUM_ROWS          pls_integer;
      L_FIELD_VALUE       varchar2(2000);
      L_RTN_CODE          number(22, 0);
   
      L_COMPANY_FLD_VALUE   varchar2(2000);
      L_VINTAGE_FLD_VALUE   varchar2(2000);
      L_TAX_RECD_FLD_VALUE  varchar2(2000);
      L_TAX_LAYER_FLD_VALUE varchar2(2000);
      L_TAX_CRDT_FLD_VALUE  varchar2(2000);
      L_TAX_RATE_FLD_VALUE  varchar2(2000);
   
      L_LAST_COMPANY_ID       number;
      L_LAST_VINTAGE_ID       number;
      L_LAST_IN_SERVICE_MONTH date;
      L_LAST_TAX_LAYER_ID     number;
   
      type K1_EXP_TBL_TYPE is table of TAX_K1_EXPORT_RUN_RESULTS%rowtype;
      K1_EXP_TBL K1_EXP_TBL_TYPE;
   
      type FIELD_MAPS_TYPE_REF_CUR is ref cursor;
      FIELDS_MAP_REF_CUR FIELD_MAPS_TYPE_REF_CUR;
      type FIELDS_REC_TYPE is record(
         K1_FIELD_ID TAX_K1_EXPORT_FIELDS.K1_FIELD_ID%type,
         DESCRIPTION TAX_K1_EXPORT_FIELDS.DESCRIPTION%type,
         FIELD_SQL   TAX_K1_EXPORT_FIELDS.FIELD_SQL%type,
         WHERE_SQL   TAX_K1_EXPORT_FIELDS.WHERE_SQL%type,
         FROM_SQL    TAX_K1_EXPORT_FIELDS.FROM_SQL%type,
         SEPARATOR   TAX_K1_EXPORT_DEF_FIELDS.SEPARATOR%type);
      type FIELDS_TBL_TYPE is table of FIELDS_REC_TYPE;
      FIELDS_TBL       FIELDS_TBL_TYPE;
      EMPTY_FIELDS_TBL FIELDS_TBL_TYPE;
   
      type K1_EXP_RES_REC_TYPE is record(
         TAX_RECORD_ID        TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID%type,
         AUTOGEN_K1_EXPORT_ID TAX_K1_EXPORT_RUN_RESULTS.AUTOGEN_K1_EXPORT_ID%type,
         FINAL_K1_EXPORT_ID   TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID%type);
      type K1_EXP_RES_TBL_TYPE is table of K1_EXP_RES_REC_TYPE;
      K1_EXP_RES_TBL K1_EXP_RES_TBL_TYPE;
   
   begin
   
      if L_PROCESS_ID = -1 or L_PROCESS_ID is null then
         P_SETPROCESS();
      end if;
   
      PKG_PP_LOG.P_START_LOG(L_PROCESS_ID);
   
      PKG_PP_LOG.P_WRITE_MESSAGE('Process started (PLSQL DB processing) ');
   
      L_PKG_VERSION := F_GET_VERSION();
      PKG_PP_LOG.P_WRITE_MESSAGE('MLP Package version : ' || L_PKG_VERSION);
      PKG_PP_LOG.P_WRITE_MESSAGE('Processing with the following variable definitions:');
      PKG_PP_LOG.P_WRITE_MESSAGE('K1 Export Run ID : ' || A_K1_EXPORT_RUN_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE('K1 Export Definition ID : ' || A_K1_EXPORT_DEFINTION_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE('Version ID : ' || A_VERSION_ID);
      PKG_PP_LOG.P_WRITE_MESSAGE('Beginning backfilling process...');
   
      ----START OF LOGIC
   
      ----we will not write to the log during the massive looping logic.
      ----but, as actions are occuring, L_MSG will be updated.  This value is passed to the 
      ----exception which is displayed/logged, so it will help to pinpoint where an error is
      ----encountered.
      L_MSG := '';
   
      ---- turn off auditing.
      L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      L_MSG      := 'Turn off auditing return code : ' || L_RTN_CODE;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
   
      ----grab what we need from the K1 definition (used for all records being processed.)
      L_MSG := 'Grabbing K1 definition parameters.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select DEF.PREFIX
        into L_PREFIX
        from TAX_K1_EXPORT_DEFINITION DEF
       where DEF.K1_EXPORT_DEFINITION_ID = A_K1_EXPORT_DEFINTION_ID;
   
      ----populate the table collection with the current table contents
      L_MSG := 'Selecting run results into table variable.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select * bulk collect
        into K1_EXP_TBL
        from TAX_K1_EXPORT_RUN_RESULTS
       where K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
       order by NVL(TAX_EVENT_ID, 999);
   
      ----loop through the result rows
      L_MSG := 'Entering looping logic.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      for ROW_IDX in 1 .. K1_EXP_TBL.COUNT
      loop
      
         ---- if the definition says the k1 export should be:
         ---- vintage.year;tax_layer.description;company.description
         ---- then the logic needs to go to update a the example record:
         ---- field_value_1 = 2007
         ---- field_value_2 = TechTerm XYZ blah blah
         ---- field_value_3 = Yellow Jacket Power
      
         ---- the easiest way to do this will be to go into *another* loop for each of the fields
         ---- make a reference cursor that gathers the fields needed.  ref cursors are a little slow,
         ---- so if this row's tax event id is the same as the last row's tax event id, then it's safe
         ---- to use the existing cursor's fields.
      
         if NVL(K1_EXP_TBL(ROW_IDX).TAX_EVENT_ID, 0) <> NVL(L_LAST_TAX_EVENT_ID, 0) or ROW_IDX = 1 then
            ----populate the reference cursor of fields we build out.
            L_MSG := 'Populating refence cursor of fields to use. Row: ' || ROW_IDX ||
                     '. Expecting tax event.';
            open FIELDS_MAP_REF_CUR for
               select FIELDS.K1_FIELD_ID,
                      FIELDS.DESCRIPTION,
                      FIELDS.FIELD_SQL,
                      FIELDS.WHERE_SQL,
                      FIELDS.FROM_SQL,
                      DEF_FIELDS.SEPARATOR
                 from TAX_K1_EXPORT_DEF_FIELDS DEF_FIELDS, TAX_K1_EXPORT_FIELDS FIELDS
                where DEF_FIELDS.K1_FIELD_ID = FIELDS.K1_FIELD_ID
                  and DEF_FIELDS.K1_EXPORT_DEFINITION_ID = A_K1_EXPORT_DEFINTION_ID
                  and NVL(DEF_FIELDS.TAX_EVENT_ID, 99999) =
                      NVL(K1_EXP_TBL(ROW_IDX).TAX_EVENT_ID, 99999)
                order by DEF_FIELDS.FIELD_ORDER;
         
            L_MSG      := 'Resetting fields table variable. Row: ' || ROW_IDX ||
                          '. Expecting tax event.';
            FIELDS_TBL := EMPTY_FIELDS_TBL;
            L_MSG      := 'Fetching fields from cursor into table variable. Row: ' || ROW_IDX ||
                          '. Expecting tax event.';
            fetch FIELDS_MAP_REF_CUR bulk collect
               into FIELDS_TBL;
            close FIELDS_MAP_REF_CUR;
         
            ----is there anything in this collection?  If not, try the default (no tax event overrides were set up for this
            ----tax event)
            if FIELDS_TBL.COUNT = 0 then
            
               L_MSG := 'Populating refence cursor of fields to use. Row: ' || ROW_IDX ||
                        '. No tax event.';
               open FIELDS_MAP_REF_CUR for
                  select FIELDS.K1_FIELD_ID,
                         FIELDS.DESCRIPTION,
                         FIELDS.FIELD_SQL,
                         FIELDS.WHERE_SQL,
                         FIELDS.FROM_SQL,
                         DEF_FIELDS.SEPARATOR
                    from TAX_K1_EXPORT_DEF_FIELDS DEF_FIELDS, TAX_K1_EXPORT_FIELDS FIELDS
                   where DEF_FIELDS.K1_FIELD_ID = FIELDS.K1_FIELD_ID
                     and DEF_FIELDS.K1_EXPORT_DEFINITION_ID = A_K1_EXPORT_DEFINTION_ID
                     and DEF_FIELDS.TAX_EVENT_ID is null
                   order by DEF_FIELDS.FIELD_ORDER;
            
               L_MSG      := 'Resetting fields table variable. Row: ' || ROW_IDX ||
                             '. No tax event.';
               FIELDS_TBL := EMPTY_FIELDS_TBL;
               L_MSG      := 'Fetching fields from cursor into table variable. Row: ' || ROW_IDX ||
                             '. No tax event.';
               fetch FIELDS_MAP_REF_CUR bulk collect
                  into FIELDS_TBL;
               close FIELDS_MAP_REF_CUR;
            
            end if; --if original field count = 0
         
         end if; --current tax event does not match last tax event

         ----if the fields_tbl is still empty, then there are no fields at all linked to this definition.
         if FIELDS_TBL.COUNT = 0 then
            L_MSG := SUBSTR('Error! There are no fields assigned to this K1 Export Definition.'||
                  ' Please do this in the K1 Export Definition Maintenance window and try again.'||
                  ' Exiting. ',1,2000);
            return L_MSG;
         end if;

         ----loop through fields
         for FIELD_IDX in 1 .. FIELDS_TBL.COUNT
         loop
         
            ---- what is the field's "from sql"?  This tells us what table we need to be pulling
            ---- from.  current available values are: 
            ----     company_setup
            ----     vintage
            ----     tax_record_control
            ----     tax_layer
            ----     tax_credit
            ----     tax_rate_control
         
            case UPPER(trim(FIELDS_TBL(FIELD_IDX).FROM_SQL))
               when 'COMPANY_SETUP' then

                  begin
                     
                     L_MSG := 'Building company value. Row: ' || ROW_IDX || '. Field:' ||
                              FIELD_IDX || '.';
                     SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
                              'FROM COMPANY_SETUP ' || 'WHERE COMPANY_SETUP.COMPANY_ID = ' || K1_EXP_TBL(ROW_IDX)
                             .COMPANY_ID;
                     L_MSG := 'Executing company value SQL. SQLS: ' || SQLS || '. Row: ' ||
                              ROW_IDX || '. Field:' || FIELD_IDX || '.';
                     execute immediate SQLS
                        into L_COMPANY_FLD_VALUE;
                     
                  exception
                     when NO_DATA_FOUND then
                        L_MSG               := 'No company value found. This is okay. Row: ' ||
                                               ROW_IDX || '. Field:' || FIELD_IDX || '.';
                        L_COMPANY_FLD_VALUE := '';
                     when others then
                        L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
                        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                        return L_MSG;
                  end;
               
                  L_FIELD_VALUE     := L_COMPANY_FLD_VALUE;
                  L_LAST_COMPANY_ID := K1_EXP_TBL(ROW_IDX).COMPANY_ID;
               
               when 'VINTAGE' then

                  begin
                     
                     L_MSG := 'Building vintage value. Row: ' || ROW_IDX || '. Field:' ||
                              FIELD_IDX || '.';
                     SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
                              'FROM VINTAGE ' || 'WHERE VINTAGE.VINTAGE_ID = ' || K1_EXP_TBL(ROW_IDX)
                             .VINTAGE_ID;
                     
                     L_MSG := 'Executing vintage value SQL. SQLS: ' || SQLS || '. Row: ' ||
                              ROW_IDX || '. Field:' || FIELD_IDX || '.';
                     execute immediate SQLS
                        into L_VINTAGE_FLD_VALUE;
                     
                  exception
                     when NO_DATA_FOUND then
                        L_MSG               := 'No vintage value found. This is okay. Row: ' ||
                                               ROW_IDX || '. Field:' || FIELD_IDX || '.';
                        L_VINTAGE_FLD_VALUE := '';
                     when others then
                        L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
                        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                        return L_MSG;
                  end;
               
                  L_FIELD_VALUE     := L_VINTAGE_FLD_VALUE;
                  L_LAST_VINTAGE_ID := K1_EXP_TBL(ROW_IDX).VINTAGE_ID;
               
               when 'TAX_RECORD_CONTROL' then

                  begin
                     
                     L_MSG := 'Building tax record control value. Row: ' || ROW_IDX ||
                              '. Field:' || FIELD_IDX || '.';
                     SQLS  := 'SELECT TRIM(' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ') ' ||
                              'FROM TAX_RECORD_CONTROL ' ||
                              'WHERE TAX_RECORD_CONTROL.TAX_RECORD_ID = ' || K1_EXP_TBL(ROW_IDX)
                             .TAX_RECORD_ID;
                     
                     L_MSG := 'Executing tax record control value SQL. SQLS: ' || SQLS ||
                              '. Row: ' || ROW_IDX || '. Field:' || FIELD_IDX || '.';
                     execute immediate SQLS
                        into L_TAX_RECD_FLD_VALUE;
                     
                  exception
                     when NO_DATA_FOUND then
                        L_MSG                := 'No tax record control value found. This is okay. Row: ' ||
                                                ROW_IDX || '. Field:' || FIELD_IDX || '.';
                        L_TAX_RECD_FLD_VALUE := '';
                     when others then
                        L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
                        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                        return L_MSG;
                  end;
               
                  L_FIELD_VALUE           := L_TAX_RECD_FLD_VALUE;
                  L_LAST_IN_SERVICE_MONTH := K1_EXP_TBL(ROW_IDX).IN_SERVICE_MONTH;
               
               when 'TAX_LAYER' then

                  begin
                     
                     L_MSG := 'Building tax layer value. Row: ' || ROW_IDX || '. Field:' ||
                              FIELD_IDX || '.';
                     SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
                              'FROM TAX_LAYER ' ||
                              'WHERE NVL(TAX_LAYER.TAX_LAYER_ID,-999) = ' || NVL(K1_EXP_TBL(ROW_IDX)
                             .TAX_LAYER_ID,-999);
                     
                     L_MSG := 'Executing tax layer value SQL. SQLS: ' || SQLS || '. Row: ' ||
                              ROW_IDX || '. Field:' || FIELD_IDX || '.';
                     execute immediate SQLS
                        into L_TAX_LAYER_FLD_VALUE;
                     
                  exception
                     when NO_DATA_FOUND then
                        L_MSG                 := 'No tax layer value found. This is okay. Row: ' ||
                                                 ROW_IDX || '. Field:' || FIELD_IDX || '.';
                        L_TAX_LAYER_FLD_VALUE := '';
                     when others then
                        L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
                        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                        return L_MSG;
                  end;
               
                  L_FIELD_VALUE       := L_TAX_LAYER_FLD_VALUE;
                  L_LAST_TAX_LAYER_ID := K1_EXP_TBL(ROW_IDX).TAX_LAYER_ID;
               
               when 'TAX_CREDIT' then

                  begin
                  
                     L_MSG := 'Building tax credit value. Row: ' || ROW_IDX || '. Field:' ||
                              FIELD_IDX || '.';
                     SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
                              'FROM TAX_DEPRECIATION, TAX_BOOK, TAX_CREDIT ' ||
                              'WHERE TAX_DEPRECIATION.TAX_RECORD_ID = ' || K1_EXP_TBL(ROW_IDX)
                             .TAX_RECORD_ID || ' ' || 'AND TAX_DEPRECIATION.TAX_YEAR = ' || K1_EXP_TBL(ROW_IDX)
                             .TAX_YEAR || ' ' ||
                              'AND TAX_DEPRECIATION.TAX_BOOK_ID = TAX_BOOK.TAX_BOOK_ID ';
                  
                     if not (trim(FIELDS_TBL(FIELD_IDX).WHERE_SQL) is null) then
                        SQLS := SQLS || ' AND ' || FIELDS_TBL(FIELD_IDX).WHERE_SQL || ' ';
                     end if;
                  
                     SQLS := SQLS ||
                             ' AND TAX_DEPRECIATION.TAX_CREDIT_ID = TAX_CREDIT.TAX_CREDIT_ID';
                  
                     L_MSG := 'Executing tax credit value SQL. SQLS: ' || SQLS || '. Row: ' ||
                              ROW_IDX || '. Field:' || FIELD_IDX || '.';
                     execute immediate SQLS
                        into L_TAX_CRDT_FLD_VALUE;
                  
                  exception
                     when NO_DATA_FOUND then
                        L_MSG                := 'No tax credit value found. This is okay. Row: ' ||
                                                ROW_IDX || '. Field:' || FIELD_IDX || '.';
                        L_TAX_CRDT_FLD_VALUE := '';
                     when others then
                        L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
                        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                        return L_MSG;
                  end;
               
                  L_FIELD_VALUE := L_TAX_CRDT_FLD_VALUE;
               
               when 'TAX_RATE_CONTROL' then

                  begin
                  
                     L_MSG := 'Building tax rate value. Row: ' || ROW_IDX || '. Field:' ||
                              FIELD_IDX || '.';
                     SQLS  := 'SELECT ' || trim(FIELDS_TBL(FIELD_IDX).FIELD_SQL) || ' ' ||
                              'FROM TAX_DEPRECIATION, TAX_BOOK, TAX_RATE_CONTROL ' ||
                              'WHERE TAX_DEPRECIATION.TAX_RECORD_ID = ' || K1_EXP_TBL(ROW_IDX)
                             .TAX_RECORD_ID || ' ' || 'AND TAX_DEPRECIATION.TAX_YEAR = ' || K1_EXP_TBL(ROW_IDX)
                             .TAX_YEAR || ' ' ||
                              'AND TAX_DEPRECIATION.TAX_BOOK_ID = TAX_BOOK.TAX_BOOK_ID ';
                  
                     if not (trim(FIELDS_TBL(FIELD_IDX).WHERE_SQL) is null) then
                        SQLS := SQLS || ' AND ' || FIELDS_TBL(FIELD_IDX).WHERE_SQL || ' ';
                     end if;
                  
                     SQLS := SQLS ||
                             ' AND TAX_DEPRECIATION.TAX_RATE_ID = TAX_RATE_CONTROL.TAX_RATE_ID';
                  
                     L_MSG := 'Executing tax rate value SQL. SQLS: ' || SQLS || '. Row: ' ||
                              ROW_IDX || '. Field:' || FIELD_IDX || '.';
                     execute immediate SQLS
                        into L_TAX_RATE_FLD_VALUE;
                  
                  exception
                     when NO_DATA_FOUND then
                        L_MSG                := 'No tax rate value found. This is okay. Row: ' ||
                                                ROW_IDX || '. Field:' || FIELD_IDX || '.';
                        L_TAX_RATE_FLD_VALUE := '';
                     when others then
                        L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm, 1, 2000);
                        PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                        return L_MSG;
                  end;
               
                  L_FIELD_VALUE := L_TAX_RATE_FLD_VALUE;
               
            end case;
         
            ----append the separator value to the field if it's populated
            if FIELDS_TBL(FIELD_IDX).SEPARATOR is not null then
               L_FIELD_VALUE := L_FIELD_VALUE || FIELDS_TBL(FIELD_IDX).SEPARATOR;
            end if;
         
            ----tried to do this dynamically, but it does not seem possible.
            ----This case statement is fine.
            L_MSG := 'Setting table variable Field Value: ' || L_FIELD_VALUE || '. Row: ' ||
                     ROW_IDX || '. Field:' || FIELD_IDX || '.';
            case FIELD_IDX
               when 1 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_1 := L_PREFIX || L_FIELD_VALUE;
               when 2 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_2 := L_FIELD_VALUE;
               when 3 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_3 := L_FIELD_VALUE;
               when 4 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_4 := L_FIELD_VALUE;
               when 5 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_5 := L_FIELD_VALUE;
               when 6 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_6 := L_FIELD_VALUE;
               when 7 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_7 := L_FIELD_VALUE;
               when 8 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_8 := L_FIELD_VALUE;
               when 9 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_9 := L_FIELD_VALUE;
               when 10 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_10 := L_FIELD_VALUE;
               when 11 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_11 := L_FIELD_VALUE;
               when 12 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_12 := L_FIELD_VALUE;
               when 13 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_13 := L_FIELD_VALUE;
               when 14 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_14 := L_FIELD_VALUE;
               when 15 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_15 := L_FIELD_VALUE;
               when 16 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_16 := L_FIELD_VALUE;
               when 17 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_17 := L_FIELD_VALUE;
               when 18 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_18 := L_FIELD_VALUE;
               when 19 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_19 := L_FIELD_VALUE;
               when 20 then
                  K1_EXP_TBL(ROW_IDX).FIELD_VALUE_20 := L_FIELD_VALUE;
               else
                  L_MSG := SUBSTR('Error! The k1 export definition '||
                        'has more than 20 fields, which the process does not allow. '||
                        'Please reduce the number of fields for this definition and try again. '||
                        'Exiting. ',1,2000);
                  return L_MSG;
            end case;
         
            ----build out the final value
            L_MSG := 'Setting table variable Final Value: ' || K1_EXP_TBL(ROW_IDX).FINAL_VALUE ||
                     L_FIELD_VALUE || '. Row: ' || ROW_IDX || '. Field:' || FIELD_IDX || '.';
            if FIELD_IDX = 1 then
               K1_EXP_TBL(ROW_IDX).FINAL_VALUE := L_PREFIX || L_FIELD_VALUE;
            else
               K1_EXP_TBL(ROW_IDX).FINAL_VALUE := K1_EXP_TBL(ROW_IDX).FINAL_VALUE || L_FIELD_VALUE;
            end if;

            ----if we are on the last field in the row, and the field value is still null
            ----then something is wrong.  bad template config most likely.  error out.
            if FIELD_IDX = FIELDS_TBL.COUNT and K1_EXP_TBL(ROW_IDX).FINAL_VALUE IS NULL then
               L_MSG := SUBSTR('Error! The final value for tax record id '||K1_EXP_TBL(ROW_IDX).TAX_RECORD_ID||
                     ' is entirely empty.  The k1 export definition '||
                     'is likely insufficient and needs to be reconfigured.  Exiting. ',1,2000);
               return L_MSG;
            end if;
         
         end loop; --fields within the row loop
      
         ----set the last tax event to be this row's tax event
         L_MSG               := 'Setting last tax event id. Row: ' || ROW_IDX || '.';
         L_LAST_TAX_EVENT_ID := K1_EXP_TBL(ROW_IDX).TAX_EVENT_ID;
      
      end loop; --row loop
   
      L_MSG := 'Updating tax_k1_export_results with field values and final k1 export value. count :' ||
               K1_EXP_TBL.COUNT;
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      ----update the actual table with everything populated into the table variable.
      forall I in 1 .. K1_EXP_TBL.COUNT
         update TAX_K1_EXPORT_RUN_RESULTS
            set TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_1 = K1_EXP_TBL(I).FIELD_VALUE_1,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_2 = K1_EXP_TBL(I).FIELD_VALUE_2,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_3 = K1_EXP_TBL(I).FIELD_VALUE_3,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_4 = K1_EXP_TBL(I).FIELD_VALUE_4,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_5 = K1_EXP_TBL(I).FIELD_VALUE_5,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_6 = K1_EXP_TBL(I).FIELD_VALUE_6,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_7 = K1_EXP_TBL(I).FIELD_VALUE_7,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_8 = K1_EXP_TBL(I).FIELD_VALUE_8,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_9 = K1_EXP_TBL(I).FIELD_VALUE_9,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_10 = K1_EXP_TBL(I).FIELD_VALUE_10,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_11 = K1_EXP_TBL(I).FIELD_VALUE_11,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_12 = K1_EXP_TBL(I).FIELD_VALUE_12,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_13 = K1_EXP_TBL(I).FIELD_VALUE_13,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_14 = K1_EXP_TBL(I).FIELD_VALUE_14,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_15 = K1_EXP_TBL(I).FIELD_VALUE_15,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_16 = K1_EXP_TBL(I).FIELD_VALUE_16,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_17 = K1_EXP_TBL(I).FIELD_VALUE_17,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_18 = K1_EXP_TBL(I).FIELD_VALUE_18,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_19 = K1_EXP_TBL(I).FIELD_VALUE_19,
                TAX_K1_EXPORT_RUN_RESULTS.FIELD_VALUE_20 = K1_EXP_TBL(I).FIELD_VALUE_20,
                TAX_K1_EXPORT_RUN_RESULTS.FINAL_VALUE = K1_EXP_TBL(I).FINAL_VALUE
          where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
            and TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID = K1_EXP_TBL(I).TAX_RECORD_ID;
   
      L_NUM_ROWS := K1_EXP_TBL.COUNT;
      L_MSG      := L_NUM_ROWS || ' rows updated on tax_k1_export_results.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
   
      ----insert new rows into tax_k1_export
      L_MSG := 'Inserting new rows into tax_k1_export.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      insert into TAX_K1_EXPORT
         (K1_EXPORT_ID, DESCRIPTION)
         select MAX_ID + ROWNUM, FINAL_VALUE
           from (select distinct FINAL_VALUE
                   from TAX_K1_EXPORT_RUN_RESULTS
                  where K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
                    and FINAL_VALUE not in (select DESCRIPTION from TAX_K1_EXPORT)),
                (select NVL(max(K1_EXPORT_ID), 0) MAX_ID from TAX_K1_EXPORT);
   
      ----backfill k1 export ids to table.
      L_MSG := 'Backfilling K1 export id onto tax_k1_export_results (pt1).';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      select TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID,
             TAX_K1_EXPORT.K1_EXPORT_ID AUTOGEN_K1_EXPORT_ID,
             NVL(TAX_K1_EXPORT_RUN_RESULTS.EXISTING_K1_EXPORT_ID, TAX_K1_EXPORT.K1_EXPORT_ID) FINAL_K1_EXPORT_ID bulk collect
        into K1_EXP_RES_TBL
        from TAX_K1_EXPORT_RUN_RESULTS, TAX_K1_EXPORT
       where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
         and TAX_K1_EXPORT_RUN_RESULTS.FINAL_VALUE = TAX_K1_EXPORT.DESCRIPTION;
   
      L_MSG := 'Backfilling K1 export id onto tax_k1_export_results (pt2).';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
      forall I in K1_EXP_RES_TBL.FIRST .. K1_EXP_RES_TBL.LAST
         update TAX_K1_EXPORT_RUN_RESULTS
            set AUTOGEN_K1_EXPORT_ID = K1_EXP_RES_TBL(I).AUTOGEN_K1_EXPORT_ID,
                FINAL_K1_EXPORT_ID = K1_EXP_RES_TBL(I).FINAL_K1_EXPORT_ID
          where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
            and TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID = K1_EXP_RES_TBL(I).TAX_RECORD_ID;
      L_NUM_ROWS := K1_EXP_RES_TBL.COUNT;
      L_MSG      := L_NUM_ROWS || ' rows updated on tax_k1_export_results.';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
   
      ----END OF LOGIC
      PKG_PP_LOG.P_WRITE_MESSAGE('Finished...');
      PKG_PP_LOG.P_END_LOG();
      return 'OK';
   
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
                         1,
                         2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
         PKG_PP_LOG.P_END_LOG();
         return L_MSG;
   end F_TAX_MLP_K1_GEN;

   --
   -- F_TAX_MLP_K1_TRC_BACKFILL : simple function to update k1_export_id on tax record control to match
   --    what was saved onto tax_k1_export_run_results
   --
   function F_TAX_MLP_K1_TRC_BACKFILL(A_VERSION_ID       number,
                                      A_K1_EXPORT_RUN_ID number) return varchar2 is
      L_MSG      varchar2(2000);
      L_RTN_CODE number(22, 0);
   
      type K1_EXP_RES_REC_TYPE is record(
         TAX_RECORD_ID      TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID%type,
         FINAL_K1_EXPORT_ID TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID%type);
      type K1_EXP_RES_TBL_TYPE is table of K1_EXP_RES_REC_TYPE;
      K1_EXP_RES_TBL K1_EXP_RES_TBL_TYPE;
   
   begin
   
      L_MSG := '';
   
      ---- turn off auditing.
      L_RTN_CODE := AUDIT_TABLE_PKG.SETCONTEXT('audit', 'no');
      L_MSG      := 'Turn off auditing return code : ' || L_RTN_CODE;
   
      ----backfill final k1 export ids to tax record control.
      L_MSG := 'Backfilling K1 export id onto tax record control (pt1).';
      select TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID,
             TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID bulk collect
        into K1_EXP_RES_TBL
        from TAX_K1_EXPORT_RUN_RESULTS, TAX_RECORD_CONTROL
       where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
         and TAX_K1_EXPORT_RUN_RESULTS.TAX_RECORD_ID = TAX_RECORD_CONTROL.TAX_RECORD_ID
         and TAX_RECORD_CONTROL.VERSION_ID = A_VERSION_ID
         and NVL(TAX_K1_EXPORT_RUN_RESULTS.FINAL_K1_EXPORT_ID, 0) <>
             NVL(TAX_RECORD_CONTROL.K1_EXPORT_ID, 0);
   
      L_MSG := 'Backfilling K1 export id onto tax_k1_export_results (pt2).';
      forall I in K1_EXP_RES_TBL.FIRST .. K1_EXP_RES_TBL.LAST
         update TAX_RECORD_CONTROL
            set K1_EXPORT_ID = K1_EXP_RES_TBL(I).FINAL_K1_EXPORT_ID
          where TAX_RECORD_CONTROL.VERSION_ID = A_VERSION_ID
            and TAX_RECORD_CONTROL.TAX_RECORD_ID = K1_EXP_RES_TBL(I).TAX_RECORD_ID;
   
      ----refresh the "existing" k1 export to be what was set as the final export id
      L_MSG := 'Refreshing existing k1 export on run table to match final export.';
      update TAX_K1_EXPORT_RUN_RESULTS
         set EXISTING_K1_EXPORT_ID = FINAL_K1_EXPORT_ID
       where TAX_K1_EXPORT_RUN_RESULTS.K1_EXPORT_RUN_ID = A_K1_EXPORT_RUN_ID
         and NVL(EXISTING_K1_EXPORT_ID, 0) <> NVL(FINAL_K1_EXPORT_ID, 0);
   
      return 'OK';
   
   exception
      when others then
         L_MSG := SUBSTR(L_MSG || ': ' || sqlerrm || ' ' || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
                         1,
                         2000);
         return L_MSG;
   end F_TAX_MLP_K1_TRC_BACKFILL;

end TAX_MLP;
/


--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1358, 0, 10, 4, 3, 0, 39097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039097_pwrtax_TAX_MLP.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
