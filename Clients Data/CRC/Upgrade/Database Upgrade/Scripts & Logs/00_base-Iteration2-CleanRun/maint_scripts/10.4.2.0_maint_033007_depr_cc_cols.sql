/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033007_depr_cc_cols.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   10/24/2013 Ryan Oliveria
||============================================================================
*/

alter table DS_DATA_ACCOUNT_CONTROL
   add (CLASS_CODE_ID number(22,0),
        CC_VALUE      varchar2(35));

comment on column DS_DATA_ACCOUNT_CONTROL.CLASS_CODE_ID is 'The class code.';
comment on column DS_DATA_ACCOUNT_CONTROL.CC_VALUE is 'The class code value.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (704, 0, 10, 4, 2, 0, 33007, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033007_depr_cc_cols.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
