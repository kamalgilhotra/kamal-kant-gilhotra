/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049479_taxrpr_configure_pre_hw_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.0.0 10/18/2017 Eric Berger    Loads in the new pretest explain
||                                      values for the Handy Whitman pretest
||                                      bug fix.
||============================================================================
*/

INSERT INTO repair_pretest_explain (id,description, long_description)
SELECT 14,'Handy Wtmn(R)', 'No retirements on repair segment or missing handy whitman rates.'
FROM dual
WHERE NOT EXISTS
(
  SELECT 1
  FROM repair_pretest_explain
  WHERE id = 14
);

UPDATE repair_pretest_explain
SET long_description = 'Repairs - Allow Non-Indexed Replace is set to NO and Handy Whitman rates could not be determined. Or asset location is missing from work order'
WHERE id = 13;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(13732, 0, 2018, 2, 0, 0, 49479, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_049479_taxrpr_configure_pre_hw_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;