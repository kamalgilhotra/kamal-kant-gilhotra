--/*
--||============================================================================
--|| Application: PowerPlant
--|| File Name:   maint_042893_proptax_rtn_steps_dml.sql
--||============================================================================
--|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
--||============================================================================
--|| Version    Date       Revised By       Reason for Change
--|| ---------- ---------- ---------------- ------------------------------------
--|| 2015.1     03/05/2015 B Borm, A Scott  new return processes
--||============================================================================
--*/

--  Import Variable Values
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 60, sysdate, user, 'Import Variable Values', 'Import Data Center variable values to the value vault.', 'Import Variable Vals',
    'import_value_vault', 1, 1, 60, 'uo_ptc_rtncntr_wksp_import_value_vault'
  );

--  Import Parcel Geography
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 61, sysdate, user, 'Import Parcel Geography', 'Import Parcel Geography Type assignments.', 'Import Parcel Geo',
    'import_parcel_geo', 1, 1, 61, 'uo_ptc_rtncntr_wksp_import_parcel_geo'
  );

--  Import Parcel Geography Factors
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 62, sysdate, user, 'Import Geography Type Factors', 'Import Parcel Geography Type Factors.', 'Import Geo Factors',
    'import_parcel_geofctrs', 1, 1, 62, 'uo_ptc_rtncntr_wksp_import_parcel_geofctrs'
  );

--  Import Parcel Responsibile Entities
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 63, sysdate, user, 'Import Responsibile Entities', 'Import Parcel Responsible Entities.', 'Import Resp Entities',
    'import_parcel_resp_ent', 1, 1, 63, 'uo_ptc_rtncntr_wksp_import_parcel_resp_ent'
  );

--  Import Parcel Responsibilities
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 64, sysdate, user, 'Import Parcel Responsibilites', 'Import Parcel Responsibility assignments.', 'Import Parcel Resp',
    'import_parcel_resp', 1, 1, 64, 'uo_ptc_rtncntr_wksp_import_parcel_resp'
  );

--  Import Parcel History
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 65, sysdate, user, 'Import Parcel History', 'Import Parcel History.', 'Import Parcel History',
    'import_parcel_history', 1, 1, 65, 'uo_ptc_rtncntr_wksp_import_parcel_history'
  );

--  Edit Value Variables
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 66, sysdate, user, 'Edit Variable Values', 'Manually edit Data Center variable values in the value vault.', 'Edit Variable Vals',
    'edit_value_vault', 1, 1, 66, 'uo_ptc_rtncntr_wksp_edit_value_vault'
  );

--  Run Scenario
insert into pt_process
( pt_process_id, time_stamp, user_id, description, long_description, short_description,
  identifier, pt_process_type_id, is_base_process, default_step_number, object_name
)
values
  ( 67, sysdate, user, 'Run Value Scenario', 'Run a Scenario from the Data/Valuation Center.', 'Run Value Scenario',
    'scenario', 1, 1, 67, 'uo_ptc_rtncntr_wksp_run_scenario'
  );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2350, 0, 2015, 1, 0, 0, 42893, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042893_proptax_rtn_steps_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;