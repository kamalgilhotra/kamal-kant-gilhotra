/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053041_lessee_01_import_ilr_disc_rate_setup_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/05/2019 Shane "C" Ward   Updates to ILR Import Definition to include Default Rates
||============================================================================
*/
--New Columns and Lookups for ILR Import
INSERT INTO pp_import_column (import_type_id, column_name, column_type, description, import_column_name, is_required, processing_order, parent_table, is_on_table)
VALUES (252, 'default_rate_flag', 'number(1,0)', 'Default Rate Flag', 'default_rate_flag_xlate', 0, 1, null, 0);

INSERT INTO pp_import_column (import_type_id, column_name, column_type, description, import_column_name, is_required, processing_order, parent_table, is_on_table)
VALUES (252, 'borrowing_curr_id', 'number(22,0)', 'Borrowing Currency', 'borrowing_curr_xlate', 0, 1, null, 0);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
(IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
VALUES (252, 'borrowing_curr_id', 2509);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
(IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
VALUES (252, 'default_rate_flag', 77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
(IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
VALUES (252, 'default_rate_flag', 78);

--Setup the ILR All Fields Template
DECLARE
    templateid NUMBER;
    max_field_id NUMBER;
BEGIN

--Identify All Fields Template
  SELECT import_template_id
  INTO templateid
  FROM pp_import_template
  WHERE description = 'ILR Add - All Fields';

  --Get Current Max of the template since users can technically modify it
  IF templateid IS NOT NULL AND templateid > 0 THEN
      SELECT Max(field_id)
      INTO max_field_id
      FROM pp_import_template_fields
      WHERE import_template_id = templateid;

      --Insert the new fields
          INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                        (import_template_id,
                        field_id,
                        import_type_id,
                        column_name,
                        import_lookup_id)
            VALUES      (templateid,
                        max_field_id + 1,
                        252,
                        'borrowing_curr_id',
                        2509);

          INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
                        (import_template_id,
                        field_id,
                        import_type_id,
                        column_name,
                        import_lookup_id)
            VALUES      (templateid,
                        max_field_id + 2,
                        252,
                        'default_rate_flag',
                        77);

  END IF;

END;
/

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14622, 0, 2018, 2, 0, 0, 53041, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053041_lessee_01_import_ilr_disc_rate_setup_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;