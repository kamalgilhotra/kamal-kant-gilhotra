/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036607_projects_work_est_monthly_cr.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/28/2014 Brandon Beck
||============================================================================
*/

alter table WO_EST_MONTHLY_CR add WO_WORK_ORDER_ID number(22,0);
alter table WO_EST_MONTHLY_CR add UWA_INCLUDE_WO number(22,0);

comment on column WO_EST_MONTHLY_CR.WO_WORK_ORDER_ID is 'Internal ID linking to the work order';
comment on column WO_EST_MONTHLY_CR.UWA_INCLUDE_WO is 'Internal field for processing work order';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1003, 0, 10, 4, 2, 0, 36607, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036607_projects_work_est_monthly_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;