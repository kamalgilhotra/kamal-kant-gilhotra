/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_005496_allcomp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/17/2012 Sunjin Cone    Point Release
||============================================================================
*/

-- ** Remove FK that reference COMPANY_SETUP **
SET SERVEROUTPUT ON

declare
   PPCMSG   varchar2(10) := 'PPC-MSG> ';
   PPCERR   varchar2(10) := 'PPC-ERR> ';
   PPCSQL   varchar2(10) := 'PPC-SQL> ';
   SQL_LINE varchar2(2000);

begin
   select 'ALTER TABLE ' || TABLE_NAME || ' DROP CONSTRAINT ' || CONSTRAINT_NAME ALTER_SQL
     into SQL_LINE
     from ALL_CONSTRAINTS
    where OWNER = 'PWRPLANT'
      and TABLE_NAME = 'RETIREMENT_UNIT_ASSOCIATION'
      and R_CONSTRAINT_NAME in (select CONSTRAINT_NAME
                                  from ALL_CONSTRAINTS
                                 where OWNER = 'PWRPLANT'
                                   and TABLE_NAME = 'COMPANY_SETUP'
                                   and CONSTRAINT_TYPE = 'P');
   begin
      execute immediate SQL_LINE;
      DBMS_OUTPUT.PUT_LINE(PPCSQL || SQL_LINE || ';');
   exception
      when others then
         DBMS_OUTPUT.PUT_LINE(PPCSQL || SQL_LINE || ';');
         DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
   end;
exception
   when NO_DATA_FOUND then
      DBMS_OUTPUT.PUT_LINE(PPCERR ||
                           'There is not a Foreign Key on RETIREMENT_UNIT_ASSOCIATION to drop.');
   when others then
      DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);
end;
/

-- Cleanup data if constraint errors are encountered.
delete from COMPANY_SETUP where COMPANY_ID = -1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (185, 0, 10, 3, 5, 0, 5496, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_005496_allcomp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
