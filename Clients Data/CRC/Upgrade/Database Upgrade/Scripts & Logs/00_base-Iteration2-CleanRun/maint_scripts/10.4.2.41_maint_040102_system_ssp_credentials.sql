/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040102_system_ssp_credentials.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/25/2014 Joseph King
||============================================================================
*/

alter table PP_JOB_REQUEST modify PROCESS       varchar2(1000);
alter table PP_JOB_REQUEST modify PROCESS_DESCR varchar2(1000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1462, 0, 10, 4, 2, 41, 40102, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.41_maint_040102_system_ssp_credentials.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;