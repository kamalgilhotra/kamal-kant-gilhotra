/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031360_sys_basegui_create_report_links.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   08/16/2013 Stephen Wicks     Point Release
||============================================================================
*/

-- pp_reports_link
create table PP_REPORTS_LINK
(
 REPORT_ID         number(22,0)   not null,
 OBJECT_NAME       varchar2(50)   not null,
 LINK_SEQUENCE     number(22,0)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 LINKED_REPORT_ID  number(22,0)   not null,
 LINK_EXPRESSION   varchar2(254),
 FILTER_EXPRESSION varchar2(254)
);

alter table PP_REPORTS_LINK
   add constraint PP_REPORTS_LINK_PK
       primary key ( REPORT_ID, OBJECT_NAME, LINK_SEQUENCE )
       using index tablespace PWRPLANT_IDX;

alter table PP_REPORTS_LINK
   add constraint PP_REPORTS_LINK_REPORTS_FK
       foreign key ( REPORT_ID )
       references PP_REPORTS;

alter table PP_REPORTS_LINK
   add constraint PP_REPORTS_LINK_LNK_REPORTS_FK
       foreign key ( LINKED_REPORT_ID )
       references PP_REPORTS;

comment on table PP_REPORTS_LINK is '(F) [16] The PP Reports Link table is used to control reporting linkage functionality.  Reports containing objects which can be clicked to run other reports are listed in this table along with the parameters and expressions the system needs to determine the correct report to run.';
comment on column PP_REPORTS_LINK.REPORT_ID is 'Standard system-assigned identifier of a report.';
comment on column PP_REPORTS_LINK.OBJECT_NAME is 'Name of the object on the datawindow which, when clicked, links to another report.';
comment on column PP_REPORTS_LINK.LINK_SEQUENCE is 'Specifies processing order used to determine the correct report to link for a given report and object on that report.';
comment on column PP_REPORTS_LINK.TIME_STAMP is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_REPORTS_LINK.USER_ID is 'Standarad system-assigned timestamp used for audit purposes.';
comment on column PP_REPORTS_LINK.LINKED_REPORT_ID is 'Standard system-assigned identifier of the report to which this report (defined in report_id) links.';
comment on column PP_REPORTS_LINK.LINK_EXPRESSION is 'Expression which can be evaluated to a 1 or 0 using the datawindow evaluate function to determine whether the linked report is the appropriate report to run given the current data condition.';
comment on column PP_REPORTS_LINK.FILTER_EXPRESSION is 'Expression which can be evaluated using the datawindow evaluate function to produce an additional where clause to append to the linked report when running that report.';

-- pp_reports_link_substitution
create table PP_REPORTS_LINK_SUBSTITUTION
(
 REPORT_ID         number(22,0)   not null,
 BASE_VALUE        varchar2(50)   not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 SUBSTITUTE_VALUE  varchar2(50)
);

alter table PP_REPORTS_LINK_SUBSTITUTION
   add constraint PP_REPORTS_LINK_SUBSTITUTIO_PK
       primary key ( REPORT_ID, BASE_VALUE )
       using index tablespace PWRPLANT_IDX;

alter table PP_REPORTS_LINK_SUBSTITUTION
   add constraint PP_RPT_LNK_SUB_REPORT_FK
       foreign key ( REPORT_ID )
       references PP_REPORTS;

comment on table PP_REPORTS_LINK_SUBSTITUTION is '(C) [16] The PP Reports Link Substitution table allows clients to define installation-specific substition values to replace base values in expresssions used by the report linking logic.';
comment on column PP_REPORTS_LINK_SUBSTITUTION.REPORT_ID is 'Standard system-assigned identifier of a report.';
comment on column PP_REPORTS_LINK_SUBSTITUTION.BASE_VALUE is 'Standard value used in linked report expresssion which can be replaced by the specified substitute value.';
comment on column PP_REPORTS_LINK_SUBSTITUTION.TIME_STAMP is 'Standard system-assigned user id used for audit purposes.';
comment on column PP_REPORTS_LINK_SUBSTITUTION.USER_ID is 'Standarad system-assigned timestamp used for audit purposes.';
comment on column PP_REPORTS_LINK_SUBSTITUTION.SUBSTITUTE_VALUE is 'Value which should be substituted for the BASE_VALUE each time the BASE_VALUE is used in an expression within the linked report logic.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (515, 0, 10, 4, 1, 0, 31360, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031360_sys_basegui_create_report_links.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;