/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_005592_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   07/05/2012 Elizabeth C.   Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select (select NVL(max(CONTROL_ID), 0) from PP_SYSTEM_CONTROL_COMPANY) + 1 CONTROL_ID,
          'FUNDPROJ - EDIT APPROVED UNIT_EST' CONTROL_NAME,
          CONTROL_VALUE,
          DESCRIPTION,
          '"Yes" will indicate unit estimates can be edited regardless of revision status,  "No" will only allow unit estimates to be modified on the selected revision' LONG_DESCRIPTION,
          COMPANY_ID
     from PP_SYSTEM_CONTROL_COMPANY
    where UPPER(trim(CONTROL_NAME)) = UPPER('FUNDPROJ - EDIT APPROVED FP');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (168, 0, 10, 3, 5, 0, 5592, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_005592_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
