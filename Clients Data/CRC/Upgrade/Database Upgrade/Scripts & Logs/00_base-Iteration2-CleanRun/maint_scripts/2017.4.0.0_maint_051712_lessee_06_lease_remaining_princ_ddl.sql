 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051712_lessee_06_lease_remaining_princ_ddl.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.4.0.0 07/06/2018 Shane "C" Ward  New columns for holding remaining principal
 ||============================================================================
 */

alter table ls_ilr_schedule_stg add remaining_principal number(22,2);

comment on column ls_ilr_schedule_stg.remaining_principal is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc';

alter table ls_ilr_schedule add remaining_principal number(22,2);

comment on column ls_ilr_schedule.remaining_principal is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc';

alter table ls_asset_schedule add remaining_principal number(22,2);

comment on column ls_asset_schedule.remaining_principal is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc';

alter table ls_ilr_asset_schedule_stg add remaining_principal number(22,2);

comment on column ls_ilr_asset_schedule_stg.remaining_principal is 'Holds remaining principal amount for remainder of Payment Terms, for use in Fixed Principal w Interest NPV calc';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7664, 0, 2017, 4, 0, 0, 51712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051712_lessee_06_lease_remaining_princ_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;