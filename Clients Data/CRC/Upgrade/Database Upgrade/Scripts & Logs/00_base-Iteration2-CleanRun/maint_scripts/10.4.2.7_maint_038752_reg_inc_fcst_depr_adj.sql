/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038752_reg_inc_fcst_depr_adj.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/04/2014 Shane Ward
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_fcst_depr_inc_adj_ws', 'IFA - Depr Select', 'uo_reg_fcst_depr_inc_adj_ws',
    'IFA Forecast Depreciation Select');
insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'ifa_depr_select', 3, 4, 'IFA - Depr Select', 'IFA - Depr Select', 'IFA_TOP',
    'uo_reg_fcst_depr_inc_adj_ws', 1);


create table INCREMENTAL_DEPR_ADJ_DATA
(
 INC_DATA_ID          number(22,0) not null,
 LABEL_ID             number(22,0) not null,
 ITEM_ID              number(22,0) not null,
 FCST_DEPR_VERSION_ID number(22,0) not null,
 VERSION_ID           number(22,0) null,
 MONTH_YEAR           number(22,0) not null,
 AMOUNT               number(22,2) not null,
 FCST_DEPR_GROUP_ID   number(22,0) null,
 SET_OF_BOOKS_ID      number(22,0) null,
 TAX_CLASS_ID         number(22,0) null,
 NORMALIZATION_ID     number(22,0) null,
 JURISDICTION_ID      number(22,0) null,
 USER_ID              varchar2(18) null,
 TIMESTAMP            date null
);


alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint PK_INCREMENTAL_DEPR_ADJ_DATA
       primary key (INC_DATA_ID)
       using index tablespace PWRPLANT_IDX;

alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA1
       foreign key (LABEL_ID, ITEM_ID)
       references INCREMENTAL_FP_ADJ_ITEM (LABEL_ID, ITEM_ID);

alter table incremental_depr_adj_data
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA2
       foreign key (NORMALIZATION_ID)
       references NORMALIZATION_SCHEMA (NORMALIZATION_ID);

alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA3
       foreign key (JURISDICTION_ID)
       references JURISDICTION (JURISDICTION_ID);

alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA4
       foreign key (VERSION_ID)
       references VERSION (VERSION_ID);

alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA5
       foreign key (FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);

alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA6
       foreign key (FCST_DEPR_GROUP_ID)
       references FCST_DEPR_GROUP (FCST_DEPR_GROUP_ID);

alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA7
       foreign key (SET_OF_BOOKS_ID)
       references SET_OF_BOOKS (SET_OF_BOOKS_ID);

alter table INCREMENTAL_DEPR_ADJ_DATA
   add constraint FK_INCREMENTAL_DEPR_ADJ_DATA8
       foreign key (TAX_CLASS_ID)
       references TAX_CLASS (TAX_CLASS_ID);

comment on table INCREMENTAL_DEPR_ADJ_DATA is 'The Incremental Depr Adj Data table stores incremental forecast depreciation data by incremental label and item when doing comparison calculations between two forecast depreciation versions/set of book combinations.';
comment on column INCREMENTAL_DEPR_ADJ_DATA.INC_DATA_ID is 'System assigned identifier for the incremental depreciation adjustment data.';
comment on column INCREMENTAL_DEPR_ADJ_DATA.LABEL_ID is 'System assigned identifier for the incremental data label';
comment on column INCREMENTAL_DEPR_ADJ_DATA.ITEM_ID is 'System assigned identifier for the incremental data item';
comment on column INCREMENTAL_DEPR_ADJ_DATA.FCST_DEPR_VERSION_ID is 'System assigned identifier for the depreciation forecast version';
comment on column INCREMENTAL_DEPR_ADJ_DATA.VERSION_ID is 'System assigned identifier for the PowerTax case';
comment on column INCREMENTAL_DEPR_ADJ_DATA.MONTH_YEAR is 'Month in YYYYMM format';
comment on column INCREMENTAL_DEPR_ADJ_DATA.AMOUNT is 'Monthly incremental financial amount in dollars';
comment on column INCREMENTAL_DEPR_ADJ_DATA.FCST_DEPR_GROUP_ID is 'System assigned identifier for the forecast depreciation group for labels/items that involve depreciation';
comment on column INCREMENTAL_DEPR_ADJ_DATA.SET_OF_BOOKS_ID is 'System assigned identifier for the Set of Books for labels/items that involve depreciation';
comment on column INCREMENTAL_DEPR_ADJ_DATA.TAX_CLASS_ID is 'System assigned identifier for the tax class for labels/items that involve deferred taxes';
comment on column INCREMENTAL_DEPR_ADJ_DATA.NORMALIZATION_ID is 'System assigned identifier for the normalization schema for labels/items that involve deferred taxes';
comment on column INCREMENTAL_DEPR_ADJ_DATA.JURISDICTION_ID is 'System assigned identifier for the jurisdiction for labels/items that involve deferred taxes';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1330, 0, 10, 4, 2, 7, 38752, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038752_reg_inc_fcst_depr_adj.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;