/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_038454_lease_ilr_rates_import.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.3.0 07/09/2014 Kyle Peterson
||============================================================================
*/

create table LS_IMPORT_FLOAT_RATES
(
 IMPORT_RUN_ID   number(22,0) not null,
 LINE_ID         number(22,0) not null,
 TIME_STAMP      date,
 USER_ID         varchar2(18),
 ILR_GROUP_XLATE varchar2(254),
 ILR_GROUP_ID    number(22,0),
 RATE            number(22,8),
 EFFECTIVE_DATE  varchar2(254),
 LOADED          number(22,0),
 IS_MODIFIED     number(22,0),
 ERROR_MESSAGE   varchar2(4000)
);

alter table LS_IMPORT_FLOAT_RATES
   add constraint LS_IMPORT_FLOAT_RATES_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_IMPORT_FLOAT_RATES
   add constraint LS_IMPORT_FLOAT_RATES_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN (IMPORT_RUN_ID);

create table LS_IMPORT_FLOAT_RATES_ARCHIVE as (select * from LS_IMPORT_FLOAT_RATES where 1=0);

insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS,
    AUTOCREATE_DESCRIPTION, AUTOCREATE_RESTRICT_SQL)
   select 257,
          'Add: ILR Group Rates',
          'ILR Group Rates',
          'LS_IMPORT_FLOAT_RATES',
          'LS_IMPORT_FLOAT_RATES_archive',
          null,
          1,
          'nvo_ls_logic_import',
          null,
          null,
          null
     from DUAL;

insert into PP_IMPORT_TYPE_SUBSYSTEM
   (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID)
   select IMPORT_TYPE_ID, 8 from PP_IMPORT_TYPE where IMPORT_TABLE_NAME like 'LS_IMPORT_FLOAT_RATES';

--Insert Columns
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID,
    PARENT_TABLE_PK_COLUMN)
   select TYPE.IMPORT_TYPE_ID,
          LOWER(COL.COLUMN_NAME),
          INITCAP(replace(COL.COLUMN_NAME, '_', ' ')),
          null,
          0,
          1,
          LOWER(COL.DATA_TYPE) || DECODE(COL.DATA_TYPE,
                                         'DATE',
                                         '',
                                         'NUMBER',
                                         '(' || COL.DATA_PRECISION || ',' || COL.DATA_SCALE || ')',
                                         '(' || COL.DATA_LENGTH || ')'),
          null,
          null,
          1,
          null,
          null
     from PP_IMPORT_TYPE type, ALL_TAB_COLUMNS COL
    where TYPE.IMPORT_TABLE_NAME = 'LS_IMPORT_FLOAT_RATES'
      and COL.TABLE_NAME = 'LS_IMPORT_FLOAT_RATES'
      and COL.COLUMN_NAME not like '%_XLATE%'
      and LOWER(COL.COLUMN_NAME) not in ('import_run_id', 'line_id', 'time_stamp', 'user_id', 'error_message','is_modified','loaded');

--Set import_column_name
update PP_IMPORT_COLUMN A
   set A.IMPORT_COLUMN_NAME = LOWER(replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'))
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'LS_IMPORT_FLOAT_RATES')
   and UPPER(A.COLUMN_NAME) like '%_ID'
   and exists (select 1
          from ALL_TAB_COLUMNS
         where TABLE_NAME = 'LS_IMPORT_FLOAT_RATES'
           and COLUMN_NAME = replace(UPPER(A.COLUMN_NAME), '_ID', '_XLATE'));

--Set is_required
update PP_IMPORT_COLUMN A
   set A.IS_REQUIRED = 1
 where A.IMPORT_TYPE_ID = (select TYPE.IMPORT_TYPE_ID
                             from PP_IMPORT_TYPE type
                            where TYPE.IMPORT_TABLE_NAME = 'LS_IMPORT_FLOAT_RATES')
   and UPPER(A.COLUMN_NAME) in ('ILR_GROUP_ID',
                        'EFFECTIVE_DATE',
                        'RATE');

insert into PP_IMPORT_TEMPLATE
   (IMPORT_TEMPLATE_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE,
    DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE)
   select PP_IMPORT_TEMPLATE_SEQ.NEXTVAL,
          TYPE.IMPORT_TYPE_ID,
          'ILR Floating Rates Add',
          'ILR Floating Rates Add',
          user,
          sysdate,
          1,
          null,
          null,
          0
     from PP_IMPORT_TYPE type
    where TYPE.IMPORT_TABLE_NAME = 'LS_IMPORT_FLOAT_RATES';

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select TYPE.IMPORT_TYPE_ID, PPCOL.COLUMN_NAME, LOOK.IMPORT_LOOKUP_ID
     from PP_IMPORT_TYPE type, PP_IMPORT_COLUMN PPCOL, PP_IMPORT_LOOKUP LOOK
    where PPCOL.IMPORT_TYPE_ID = TYPE.IMPORT_TYPE_ID
      and (LOOK.COLUMN_NAME = PPCOL.COLUMN_NAME or
          (LOOK.COLUMN_NAME = 'class_code_id' and PPCOL.COLUMN_NAME like 'class_code_id%'))
      and TYPE.IMPORT_TABLE_NAME = 'LS_IMPORT_FLOAT_RATES'
      and PPCOL.COLUMN_NAME like '%_id%'
      and LOOK.LOOKUP_VALUES_ALTERNATE_SQL is null
      and LOOK.LOOKUP_COLUMN_NAME like '%description%'
      and not exists (select 1
             from PP_IMPORT_COLUMN_LOOKUP
            where IMPORT_TYPE_ID = TYPE.IMPORT_TYPE_ID
              and COLUMN_NAME = PPCOL.COLUMN_NAME
              and IMPORT_LOOKUP_ID = LOOK.IMPORT_LOOKUP_ID)
    order by TYPE.IMPORT_TABLE_NAME;

 insert into PP_IMPORT_TEMPLATE_FIELDS
   (IMPORT_TEMPLATE_ID, FIELD_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID,
    AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID)
   select TEMP.IMPORT_TEMPLATE_ID,
          COL.COLUMN_ID,
          TYPE.IMPORT_TYPE_ID,
          PPCOL.COLUMN_NAME,
          max(CL.IMPORT_LOOKUP_ID),
          null,
          null,
          null
     from PP_IMPORT_TEMPLATE      TEMP,
          PP_IMPORT_TYPE          type,
          PP_IMPORT_COLUMN        PPCOL,
          PP_IMPORT_COLUMN_LOOKUP CL,
          ALL_TAB_COLUMNS         COL
    where (TEMP.DESCRIPTION = 'ILR Floating Rates Add' and TYPE.IMPORT_TABLE_NAME = 'LS_IMPORT_FLOAT_RATES')
      and PPCOL.IMPORT_TYPE_ID = TYPE.IMPORT_TYPE_ID
      and CL.COLUMN_NAME(+) = PPCOL.COLUMN_NAME
      and UPPER(COL.TABLE_NAME) = UPPER(TYPE.IMPORT_TABLE_NAME)
      and UPPER(COL.COLUMN_NAME) = UPPER(PPCOL.COLUMN_NAME)
    group by TEMP.IMPORT_TEMPLATE_ID, COL.COLUMN_ID, TYPE.IMPORT_TYPE_ID, PPCOL.COLUMN_NAME
    order by COL.COLUMN_ID;

update PP_IMPORT_TEMPLATE_FIELDS TF
   set TF.FIELD_ID =
        (select count(1)
           from PP_IMPORT_TEMPLATE_FIELDS B
          where B.IMPORT_TEMPLATE_ID = TF.IMPORT_TEMPLATE_ID
            and B.FIELD_ID <= TF.FIELD_ID)
 where TF.IMPORT_TEMPLATE_ID in
       (select IMPORT_TEMPLATE_ID
          from PP_IMPORT_TEMPLATE
         where DESCRIPTION in
               ('ILR Floating Rates Add'));

comment on table LS_IMPORT_FLOAT_RATES is '(S)  [06] The LS Import Float Rates table is an API table used to import floating interest rates for ILRs.';
comment on table LS_IMPORT_FLOAT_RATES_ARCHIVE is '(S)  [06] The LS Import Float Rates Archive table holds records of previous floating interest rate imports.';

comment on column LS_IMPORT_FLOAT_RATES.IMPORT_RUN_ID is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column LS_IMPORT_FLOAT_RATES.LINE_ID is 'System-assigned line number for this import run.';
comment on column LS_IMPORT_FLOAT_RATES.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LS_IMPORT_FLOAT_RATES.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column LS_IMPORT_FLOAT_RATES.ILR_GROUP_XLATE is 'Translation field for ILR group id';
comment on column LS_IMPORT_FLOAT_RATES.ILR_GROUP_ID is 'An internal PowerPlant id representing the ILR Group for the ILR.';
comment on column LS_IMPORT_FLOAT_RATES.EFFECTIVE_DATE is 'Effective date for the rate or amount.';
comment on column LS_IMPORT_FLOAT_RATES.RATE is 'The monthly rate for the tax';
comment on column LS_IMPORT_FLOAT_RATES.LOADED is 'System-assigned number to specify if this row has been loaded into its table.';
comment on column LS_IMPORT_FLOAT_RATES.IS_MODIFIED is 'System-assigned number to specify if this row has been modified from its original values.';
comment on column LS_IMPORT_FLOAT_RATES.ERROR_MESSAGE is 'Error messages resulting from data valdiation in the import process.';

comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.IMPORT_RUN_ID is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.LINE_ID is 'System-assigned line number for this import run.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.ILR_GROUP_XLATE is 'Translation field for ILR group id';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.ILR_GROUP_ID is 'An internal PowerPlant id representing the ILR Group for the ILR.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.EFFECTIVE_DATE is 'Effective date for the rate or amount.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.RATE is 'The monthly rate for the tax';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.LOADED is 'System-assigned number to specify if this row has been loaded into its table.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.IS_MODIFIED is 'System-assigned number to specify if this row has been modified from its original values.';
comment on column LS_IMPORT_FLOAT_RATES_ARCHIVE.ERROR_MESSAGE is 'Error messages resulting from data valdiation in the import process.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1243, 0, 10, 4, 3, 0, 38454, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038454_lease_ilr_rates_import.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
