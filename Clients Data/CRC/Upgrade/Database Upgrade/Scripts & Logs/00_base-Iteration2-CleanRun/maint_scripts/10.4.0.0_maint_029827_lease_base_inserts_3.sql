SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029827_lease_base_inserts_3.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/15/2013 Lee Quinn      Point Release
||============================================================================
*/

declare
   TABLE_DOES_NOT_EXIST exception;
   pragma exception_init(TABLE_DOES_NOT_EXIST, -00942);

   LOOPING_CHAIN_OF_SYNONYMS exception;
   pragma exception_init(LOOPING_CHAIN_OF_SYNONYMS, -01775);

   type CV_TYP is ref cursor;
   CV CV_TYP;

   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   L_COUNT             number := 1;
   LB_LEASE_BEING_USED boolean := true;

begin
   DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);

   begin
      open CV for 'select count(*) from LS_ASSET where ROWNUM < 10';
      fetch CV
         into L_COUNT;
      close CV;

   if L_COUNT = 0 then
      LB_LEASE_BEING_USED := false;
   end if;

   exception
      when TABLE_DOES_NOT_EXIST then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'LS_ASSET table Doesn''t exist so Lease Module can be refreshed.');
         LB_LEASE_BEING_USED := false;
      when LOOPING_CHAIN_OF_SYNONYMS then
         LB_LEASE_BEING_USED := false;
         DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Looping Chain of Synonyms - LS_ASSET table Doesn''t exist so the Lease Module can be refreshed.');
         LB_LEASE_BEING_USED := false;
   end;

   if LB_LEASE_BEING_USED then
      DBMS_OUTPUT.PUT_LINE('Lease is being used so no Data inserts will be made.');
   else

      --LS_RETIREMENT_STATUS
      insert into LS_RETIREMENT_STATUS
         (RETIREMENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (0, sysdate, user, 'Pending');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_RETIREMENT_STATUS.');

      insert into LS_RETIREMENT_STATUS
         (RETIREMENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (-1, sysdate, user, 'Failed -  Error');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_RETIREMENT_STATUS.');

      insert into LS_RETIREMENT_STATUS
         (RETIREMENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (1, sysdate, user, 'Successful');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_RETIREMENT_STATUS.');

      insert into LS_RETIREMENT_STATUS
         (RETIREMENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (-2, sysdate, user, 'Failed - Not Approved');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_RETIREMENT_STATUS.');

      --LS_ACTIVITY_TYPE
      insert into LS_ACTIVITY_TYPE
         (LS_ACTIVITY_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, AM_CALC_FLAG,
          FERC_ACTIVITY_CODE)
      values
         (1, sysdate, user, 'ADD', 'Addition', 'AM', null);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ACTIVITY_TYPE.');

      insert into LS_ACTIVITY_TYPE
         (LS_ACTIVITY_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, AM_CALC_FLAG,
          FERC_ACTIVITY_CODE)
      values
         (2, sysdate, user, 'ADJ', 'Adjustment', 'AM', null);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ACTIVITY_TYPE.');

      insert into LS_ACTIVITY_TYPE
         (LS_ACTIVITY_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, AM_CALC_FLAG,
          FERC_ACTIVITY_CODE)
      values
         (3, sysdate, user, 'RET', 'Retirement', 'AM', null);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ACTIVITY_TYPE.');

      insert into LS_ACTIVITY_TYPE
         (LS_ACTIVITY_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, AM_CALC_FLAG,
          FERC_ACTIVITY_CODE)
      values
         (4, sysdate, user, 'TRT', 'Transfer To', 'AM', null);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ACTIVITY_TYPE.');

      insert into LS_ACTIVITY_TYPE
         (LS_ACTIVITY_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, AM_CALC_FLAG,
          FERC_ACTIVITY_CODE)
      values
         (5, sysdate, user, 'TRF', 'Transfer From', 'AM', null);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ACTIVITY_TYPE.');

      insert into LS_ACTIVITY_TYPE
         (LS_ACTIVITY_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, AM_CALC_FLAG,
          FERC_ACTIVITY_CODE)
      values
         (99, sysdate, user, 'LsCalc-Rev', 'Lease Calc Reversal', 'CALC', null);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ACTIVITY_TYPE.');

      insert into LS_ACTIVITY_TYPE
         (LS_ACTIVITY_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, AM_CALC_FLAG,
          FERC_ACTIVITY_CODE)
      values
         (100, sysdate, user, 'LsCalc', 'Lease Calc', 'CALC', null);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ACTIVITY_TYPE.');

      --LS_API_BATCH_STATUS
      insert into LS_API_BATCH_STATUS
         (API_BATCH_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, '01-Not Committed', '01-Not Committed');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_API_BATCH_STATUS.');

      insert into LS_API_BATCH_STATUS
         (API_BATCH_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, '02-Fatal Error', '02-Fatal Error');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_API_BATCH_STATUS.');

      insert into LS_API_BATCH_STATUS
         (API_BATCH_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, '03-In Process', '03-In Process');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_API_BATCH_STATUS.');

      insert into LS_API_BATCH_STATUS
         (API_BATCH_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, '04-Committed', '04-Committed');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_API_BATCH_STATUS.');

      --LS_ASSET_STATUS
      insert into LS_ASSET_STATUS
         (LS_ASSET_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Committed', 'Committed');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ASSET_STATUS.');

      insert into LS_ASSET_STATUS
         (LS_ASSET_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Received', 'Received');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ASSET_STATUS.');

      insert into LS_ASSET_STATUS
         (LS_ASSET_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'In Service', 'In Service');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ASSET_STATUS.');

      insert into LS_ASSET_STATUS
         (LS_ASSET_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Transferred', 'Transferred');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ASSET_STATUS.');

      insert into LS_ASSET_STATUS
         (LS_ASSET_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Retired', 'Retired');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ASSET_STATUS.');

      --LS_CALC_APPROVAL_RULE
      insert into LS_CALC_APPROVAL_RULE
         (CALC_APPROVAL_RULE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'On-Line Approval', 'On-Line Approval');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_CALC_APPROVAL_RULE.');

      insert into LS_CALC_APPROVAL_RULE
         (CALC_APPROVAL_RULE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Auto Approval vs Tolerance', 'Auto Approve vs Tolerance');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_CALC_APPROVAL_RULE.');

      insert into LS_CALC_APPROVAL_RULE
         (CALC_APPROVAL_RULE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Auto Approval (no reconciliation)',
          'Auto Approval (no reconciliation)');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_CALC_APPROVAL_RULE.');

      --LS_CANCELABLE_TYPE
      insert into LS_CANCELABLE_TYPE
         (CANCELABLE_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Cancelable with Penalty', 'Cancelable with Penalty');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_CANCELABLE_TYPE.');

      insert into LS_CANCELABLE_TYPE
         (CANCELABLE_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Cancelable without Penalty', 'Cancelable without Penalty');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_CANCELABLE_TYPE.');

      insert into LS_CANCELABLE_TYPE
         (CANCELABLE_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Not Cancelable', 'Not Cancelable');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_CANCELABLE_TYPE.');

      --LS_COLUMN_USAGE
      insert into LS_COLUMN_USAGE
         (COLUMN_USAGE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Not Used', 'Not Used');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COLUMN_USAGE.');

      insert into LS_COLUMN_USAGE
         (COLUMN_USAGE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Required', 'Required');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COLUMN_USAGE.');

      insert into LS_COLUMN_USAGE
         (COLUMN_USAGE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Optional', 'Optional');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COLUMN_USAGE.');

      --LS_COMPONENT_STATUS
      insert into LS_COMPONENT_STATUS
         (LS_COMPONENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Budgeted', 'Budgeted');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COMPONENT_STATUS.');

      insert into LS_COMPONENT_STATUS
         (LS_COMPONENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Committed', 'Committed');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COMPONENT_STATUS.');

      insert into LS_COMPONENT_STATUS
         (LS_COMPONENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Received', 'Received');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COMPONENT_STATUS.');

      insert into LS_COMPONENT_STATUS
         (LS_COMPONENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Actual', 'Actual');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COMPONENT_STATUS.');

      insert into LS_COMPONENT_STATUS
         (LS_COMPONENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Transferred', 'Transferred');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COMPONENT_STATUS.');

      insert into LS_COMPONENT_STATUS
         (LS_COMPONENT_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (6, sysdate, user, 'Retired', 'Retired');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_COMPONENT_STATUS.');

      --LS_DIST_DEF_TYPE
      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (1, sysdate, user, 'All', 'All', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (2, sysdate, user, 'Principal', 'Principal', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (3, sysdate, user, 'Interest', 'Interest', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (4, sysdate, user, 'Depreciation / Reg Amort', 'Depreciation / Reg Amort', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (5, sysdate, user, 'Loc Tax- Lessor', 'Local Taxes - Lessor', 0);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (6, sysdate, user, 'Loc Tax- Lessee', 'Local Taxes - Lessee', 0);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (7, sysdate, user, 'Executory', 'Executory', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (8, sysdate, user, 'Contingent', 'Contingent', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (9, sysdate, user, 'Payment', 'Payment', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (10, sysdate, user, 'Short Term Obligation', 'Short Term Obligation', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      insert into LS_DIST_DEF_TYPE
         (DIST_DEF_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_USAGE_ID)
      values
         (11, sysdate, user, 'Long Term Obligation', 'Long Term Obligation', 1);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DIST_DEF_TYPE.');

      --LS_DISTRIBUTION_TYPE
      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (1, sysdate, user, 'Pre Tax Lease Expense', 'Pre Tax Lease Expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (2, sysdate, user, 'Sales Tax Lease Expense', 'Sales Tax Lease Expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (3, sysdate, user, 'Use Tax Lease Expense', 'Use Tax Lease Expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (4, sysdate, user, 'Personal Property Tax Lease Ex', 'Personal Property Tax Lease Ex', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (5, sysdate, user, 'Real Property Lease Expense', 'Real Property Lease Expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (6, sysdate, user, 'Muni Tax Lease expense', 'Muni Tax Lease expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (28, sysdate, user, 'Liability Recategorization', 'Liability Recategorization', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (29, sysdate, user, 'Retirement', 'Retirement', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (30, sysdate, user, 'Transfer', 'Transfer', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (31, sysdate, user, 'Reinstatement', 'Reinstatement', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (11, sysdate, user, 'GAAP Depr Expense', 'GAAP Depr Expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (12, sysdate, user, 'Additions', 'Additions', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (13, sysdate, user, 'Gain', 'Gain', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (14, sysdate, user, 'Loss', 'Loss', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (32, sysdate, user, 'GAAP Interest', 'GAAP Interest Expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (33, sysdate, user, 'GAAP Principal', 'GAAP Principal', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (34, sysdate, user, 'Opening Adjustments', 'Opening Adjustments (corrections)', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (19, sysdate, user, 'Lease Obligation', 'Lease Obligation', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (20, sysdate, user, 'INACTIVE : Lease Obligation Contra',
          'INACTIVE : Lease Obligation Contra', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (21, sysdate, user, 'INACTIVE : Long Term Liability', 'INACTIVE : Long Term Liability',
          'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (25, sysdate, user, 'INACTIVE : Short Term Liability', 'INACTIVE : Short Term Liability',
          'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (26, sysdate, user, 'INACTIVE : Trade In', 'INACTIVE : Trade In', 'B');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      insert into LS_DISTRIBUTION_TYPE
         (DISTRIBUTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXP_BAL_FLAG)
      values
         (27, sysdate, user, 'Vehicle Tax Lease Expense', 'Vehicle Tax Lease Expense', 'E');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_DISTRIBUTION_TYPE.');

      --LS_EXTENDED_RENTAL_TYPE
      insert into LS_EXTENDED_RENTAL_TYPE
         (EXTENDED_RENTAL_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Bargain Amount', 'Bargain Amount');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_EXTENDED_RENTAL_TYPE.');

      insert into LS_EXTENDED_RENTAL_TYPE
         (EXTENDED_RENTAL_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Amount', 'Amount');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_EXTENDED_RENTAL_TYPE.');

      insert into LS_EXTENDED_RENTAL_TYPE
         (EXTENDED_RENTAL_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Bargain Percent', 'Bargain Percent');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_EXTENDED_RENTAL_TYPE.');

      insert into LS_EXTENDED_RENTAL_TYPE
         (EXTENDED_RENTAL_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'None', 'None');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_EXTENDED_RENTAL_TYPE.');

      --LS_ILR_STATUS
      insert into LS_ILR_STATUS
         (ILR_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Interim Submit', 'Interim Submit');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ILR_STATUS.');

      insert into LS_ILR_STATUS
         (ILR_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Interim Paid', 'Interim Paid');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ILR_STATUS.');

      insert into LS_ILR_STATUS
         (ILR_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Final Submit', 'Final Submit');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ILR_STATUS.');

      insert into LS_ILR_STATUS
         (ILR_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Final Paid', 'Final Paid');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ILR_STATUS.');

      insert into LS_ILR_STATUS
         (ILR_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Retired', 'Retired');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_ILR_STATUS.');

      --LS_JE_METHOD
      insert into LS_JE_METHOD
         (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Operating', 'Operating');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_JE_METHOD.');

      insert into LS_JE_METHOD
         (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'GAAP Capital', 'GAAP Capital');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_JE_METHOD.');

      insert into LS_JE_METHOD
         (JE_METHOD_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'FERC Capital', 'FERC Capital');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_JE_METHOD.');

      --LS_LEASE_APPROVAL
      insert into LS_LEASE_APPROVAL
         (LEASE_APPROVAL_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Pending', 'Pending');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_APPROVAL.');

      insert into LS_LEASE_APPROVAL
         (LEASE_APPROVAL_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Approved', 'Approved');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_APPROVAL.');

      insert into LS_LEASE_APPROVAL
         (LEASE_APPROVAL_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Denied', 'Denied');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_APPROVAL.');

      --LS_LEASE_CALC_ROLE
      insert into LS_LEASE_CALC_ROLE
         (LEASE_CALC_ROLE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Lease Calc', 'Lease Calc');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_ROLE.');

      insert into LS_LEASE_CALC_ROLE
         (LEASE_CALC_ROLE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Lease Calc Approval', 'Lease Calc Approval');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_ROLE.');

      --LS_LEASE_CALC_ROLE_USER
      insert into LS_LEASE_CALC_ROLE_USER
         (USERS, LEASE_CALC_ROLE_ID, TIME_STAMP, USER_ID)
      values
         (user, 1, sysdate, user);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_ROLE_USER.');

      insert into LS_LEASE_CALC_ROLE_USER
         (USERS, LEASE_CALC_ROLE_ID, TIME_STAMP, USER_ID)
      values
         (user, 2, sysdate, user);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_ROLE_USER.');

      --LS_LEASE_CALC_STATUS
      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Deferred', 'Deferred');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Staged', 'Staged');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'In Process', 'In Process');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Processed', 'Processed');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Submitted', 'Submitted');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (6, sysdate, user, 'Pending', 'Pending');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (7, sysdate, user, 'Approved', 'Approved');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (8, sysdate, user, 'Denied', 'Denied');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (9, sysdate, user, 'Posted', 'Posted');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      insert into LS_LEASE_CALC_STATUS
         (LEASE_CALC_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (10, sysdate, user, 'Rejected', 'Rejected');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_STATUS.');

      --LS_LEASE_CALC_TYPE
      insert into LS_LEASE_CALC_TYPE
         (LEASE_CALC_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Batch : Calc', 'Batch : Calc');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_TYPE.');

      insert into LS_LEASE_CALC_TYPE
         (LEASE_CALC_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Batch : Reversal', 'Batch : Reversal');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_TYPE.');

      insert into LS_LEASE_CALC_TYPE
         (LEASE_CALC_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'On Line : Calc', 'On Line : Calc');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_TYPE.');

      insert into LS_LEASE_CALC_TYPE
         (LEASE_CALC_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'On Line : Reversal', 'On Line : Reversal');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_CALC_TYPE.');

      --LS_LEASE_STATUS
      insert into LS_LEASE_STATUS
         (LEASE_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Closed', 'Closed');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_STATUS.');

      insert into LS_LEASE_STATUS
         (LEASE_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Denied', 'Denied');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_STATUS.');

      insert into LS_LEASE_STATUS
         (LEASE_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Open', 'Open');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_STATUS.');

      insert into LS_LEASE_STATUS
         (LEASE_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Pending', 'Pending');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_STATUS.');

      insert into LS_LEASE_STATUS
         (LEASE_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Retired', 'Retired');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_STATUS.');

      --LS_LEASE_TYPE
      insert into LS_LEASE_TYPE
         (LEASE_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Fixed', 'Fixed');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_TYPE.');

      --LS_LESSOR_INVOICE_STATUS
      insert into LS_LESSOR_INVOICE_STATUS
         (STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Invoice Created', 'Invoice Created, Ready to be sent to AP');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LESSOR_INVOICE_STATUS.');

      insert into LS_LESSOR_INVOICE_STATUS
         (STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Hold Invoice', 'Do not send this invoice to AP');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LESSOR_INVOICE_STATUS.');

      insert into LS_LESSOR_INVOICE_STATUS
         (STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Invoice Submitted', 'Invoice Submitted to AP without Error');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LESSOR_INVOICE_STATUS.');

      insert into LS_LESSOR_INVOICE_STATUS
         (STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Error Sending',
          'There was an error in the interface sending the request for payment for this invoice');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LESSOR_INVOICE_STATUS.');

      insert into LS_LESSOR_INVOICE_STATUS
         (STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Invoice Paid', 'Invoice Paid');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LESSOR_INVOICE_STATUS.');

      insert into LS_LESSOR_INVOICE_STATUS
         (STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (6, sysdate, user, 'Invoice Staged',
          'Invoice is staged in base export tables and will be picked up on next run of executable');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LESSOR_INVOICE_STATUS.');

      --LS_PAYMENT_FREQ
      insert into LS_PAYMENT_FREQ
         (PAYMENT_FREQ_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Annual', 'Annual');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_FREQ.');

      insert into LS_PAYMENT_FREQ
         (PAYMENT_FREQ_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Semi-Annual', 'Semi-Annual');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_FREQ.');

      insert into LS_PAYMENT_FREQ
         (PAYMENT_FREQ_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Quarterly', 'Quarterly');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_FREQ.');

      insert into LS_PAYMENT_FREQ
         (PAYMENT_FREQ_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Monthly', 'Monthly');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_FREQ.');

      insert into LS_PAYMENT_FREQ
         (PAYMENT_FREQ_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Actual', 'Actual');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_FREQ.');

      --LS_PAYMENT_TERM_TYPE
      insert into LS_PAYMENT_TERM_TYPE
         (PAYMENT_TERM_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Interim', 'Interim');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_TERM_TYPE.');

      insert into LS_PAYMENT_TERM_TYPE
         (PAYMENT_TERM_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Normal', 'Normal');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_TERM_TYPE.');

      insert into LS_PAYMENT_TERM_TYPE
         (PAYMENT_TERM_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Extended', 'Extended');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PAYMENT_TERM_TYPE.');

      --LS_PURCHASE_OPTION_TYPE
      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'None', 'None');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Flat Amount Asset', 'Flat Amount Asset');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Flat Amount ILR', 'Flat Amount ILR');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Percent ILR or Asset', 'Percent ILR or Asset');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Bargain Flat Amount Asset', 'Bargain Flat Amount Asset');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (6, sysdate, user, 'Bargain Flat Amount ILR', 'Bargain Flat Amount ILR');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (7, sysdate, user, 'Bargain Percent ILR or Asset', 'Bargain Percent ILR or Asset');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      insert into LS_PURCHASE_OPTION_TYPE
         (PURCHASE_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (8, sysdate, user, 'Rollover', 'Rollover');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_PURCHASE_OPTION_TYPE.');

      --LS_RENEWAL_OPTION_TYPE
      insert into LS_RENEWAL_OPTION_TYPE
         (RENEWAL_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Bargain', 'Bargain');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_RENEWAL_OPTION_TYPE.');

      insert into LS_RENEWAL_OPTION_TYPE
         (RENEWAL_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Fair Market Value', 'Fair Market Value');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_RENEWAL_OPTION_TYPE.');

      insert into LS_RENEWAL_OPTION_TYPE
         (RENEWAL_OPTION_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'None', 'None');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_RENEWAL_OPTION_TYPE.');

      --LS_GRANDFATHER_LEVEL
      insert into LS_GRANDFATHER_LEVEL
         (GRANDFATHER_LEVEL_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Master Lease', 'Use Master Lease Date of the lease id');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_GRANDFATHER_LEVEL.');

      insert into LS_GRANDFATHER_LEVEL
         (GRANDFATHER_LEVEL_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Rider Date', 'Use Rider Date of the lease id');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_GRANDFATHER_LEVEL.');

      insert into LS_GRANDFATHER_LEVEL
         (GRANDFATHER_LEVEL_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'ILR Date', 'Use the 1st Term Date of the ILR');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_GRANDFATHER_LEVEL.');

      --LS_LEASE_RULE
      insert into LS_LEASE_RULE
         (LEASE_RULE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, RENT_HOLIDAY_SW,
          CURR_MO_INT_OVERRIDE_SW, PERIOD_OVERRIDE, RESET_INTEREST_SW, RESET_EXTENDED_RENTAL_SW,
          RESET_PRETAX_PAYMENT_SW, ALLOW_NEG_PRETAX_SW, INTEREST_ROUND_SW, INTEREST_ROUND_POSITION,
          PRINCIPLE_ROUND_SW, PRINCIPLE_ROUND, FACTOR_ROUND_SW, FACTOR_ROUND, USE_AGE_SW)
      values
         (1, sysdate, user, 'Default', 'Default', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LEASE_RULE.');

      --LS_LOCAL_TAX_DEFAULT
      insert into LS_LOCAL_TAX_DEFAULT
         (LOCAL_TAX_DEFAULT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Always', 'Always');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LOCAL_TAX_DEFAULT.');

      insert into LS_LOCAL_TAX_DEFAULT
         (LOCAL_TAX_DEFAULT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'Never', 'Never');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LOCAL_TAX_DEFAULT.');

      insert into LS_LOCAL_TAX_DEFAULT
         (LOCAL_TAX_DEFAULT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Sometimes', 'Sometimes');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_LOCAL_TAX_DEFAULT.');

      --LS_SOB_TYPE
      insert into LS_SOB_TYPE
         (SOB_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (1, sysdate, user, 'GAAP');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_SOB_TYPE.');

      insert into LS_SOB_TYPE
         (SOB_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (2, sysdate, user, 'FERC');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_SOB_TYPE.');

      insert into LS_SOB_TYPE
         (SOB_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (3, sysdate, user, 'IFRS');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_SOB_TYPE.');

      insert into LS_SOB_TYPE
         (SOB_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION)
      values
         (4, sysdate, user, 'NOT USED');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_SOB_TYPE.');

      --LS_VOUCHER_STATUS
      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (1, sysdate, user, 'Created', 'Created');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (2, sysdate, user, 'In Approval Process', 'In Approval Process');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (3, sysdate, user, 'Approved', 'Approved');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (4, sysdate, user, 'Rejected', 'Rejected by approver');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (5, sysdate, user, 'Sent to AP', 'Sent to AP');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (6, sysdate, user, 'Error Sending', 'Error Sending');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (7, sysdate, user, 'Denied', 'Denied by AP');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

      insert into LS_VOUCHER_STATUS
         (VOUCHER_STATUS_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
      values
         (8, sysdate, user, 'Paid', 'Paid');
      DBMS_OUTPUT.PUT_LINE(PPCMSG || '1 row inserted into LS_VOUCHER_STATUS.');

   end if;
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (359, 0, 10, 4, 0, 0, 29827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029827_lease_base_inserts_3.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
