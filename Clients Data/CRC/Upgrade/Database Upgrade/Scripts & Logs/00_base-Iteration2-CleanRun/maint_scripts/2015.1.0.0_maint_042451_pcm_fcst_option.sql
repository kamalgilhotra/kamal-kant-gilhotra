/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042451_pcm_fcst_option.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/19/2015 Alex P.          Change Forecast Option Label
||============================================================================
*/

update ppbase_workspace 
set label = 'Forecasting Options'
where module = 'pcm' and workspace_identifier = 'config_estimate';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2314, 0, 2015, 1, 0, 0, 042451, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042451_pcm_fcst_option.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;