/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042804_pcm_remove_sys_options_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/09/2015 Sarah Byers      Removing PCM System Options
||============================================================================
*/
declare
	cursor c1 is
		select system_option_id
		  from ppbase_system_options_module
		 where lower(trim(module)) = 'pcm';
begin
	for record in c1
	loop
		delete from ppbase_system_options_company
		 where system_option_id = record.system_option_id;

		delete from ppbase_system_options_values
		 where system_option_id = record.system_option_id;

		delete from ppbase_system_options_module
		 where system_option_id = record.system_option_id;

		delete from ppbase_system_options_wksp
		 where system_option_id = record.system_option_id;

		delete from ppbase_system_options
		 where system_option_id = record.system_option_id;
	end loop;
exception
   when others then
      RAISE_APPLICATION_ERROR(-20000, 'ERROR: Deleting PCM system options: ' || sqlerrm);
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2269, 0, 2015, 1, 0, 0, 042804, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042804_pcm_remove_sys_options_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;