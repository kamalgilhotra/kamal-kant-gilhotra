 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048355_reg_allocator_linking_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 7/07/2017 Shane "C" Ward	Add linked allocator logic to RMS
 ||============================================================================
 */ 

--Jur Template Tables
 CREATE TABLE reg_jur_allocator_link (
 reg_jur_template_Id  NUMBER(22,0),
 reg_alloc_category_id NUMBER(22,0),
 reg_alloc_target_id NUMBER(22,0),
 start_allocator_id NUMBER(22,0),
 reg_allocator_id NUMBER(22,0),
 user_id VARCHAR(18),
 time_stamp DATE);
 
 comment on table reg_jur_allocator_link is ' (S) [19] Table holds set up of Linked Allocators for a Jur Template';
 comment on column reg_jur_allocator_link.reg_jur_template_id is 'System assigned ID of Jurisidictional Template';
 comment on column reg_jur_allocator_link.reg_alloc_category_id is 'System assigned ID of Allocation Category';
 comment on column reg_jur_allocator_link.reg_alloc_target_id is 'System assigned ID of Allocation Target';
 comment on column reg_jur_allocator_link.start_allocator_id is 'ID of allocator that is being used as beginning of LInked Allocator. Beginning of Linked allocator is where start_allocator_id = reg_allocator_Id';
 comment on column reg_jur_allocator_link.reg_allocator_id is 'System assigned ID of Allocation Factor';
 comment on column reg_jur_allocator_link.user_id is 'Standard system-assigned user id used for audit purposes.';
 comment on column reg_jur_allocator_link.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

 ALTER TABLE reg_jur_allocator_link
  ADD CONSTRAINT pk_reg_jur_allocator_link PRIMARY KEY (
    reg_jur_template_id,
    reg_alloc_category_id,
    reg_alloc_target_id,
    start_allocator_id
  );

  ALTER TABLE reg_jur_allocator_link
  ADD CONSTRAINT r_reg_jur_allocator_link1 FOREIGN KEY (
    reg_jur_template_id
  ) REFERENCES reg_jur_template (
    reg_jur_template_id
  );

  ALTER TABLE reg_jur_allocator_link
  ADD CONSTRAINT r_reg_jur_allocator_link2 FOREIGN KEY (
    reg_alloc_category_id
  ) REFERENCES reg_alloc_category (
    reg_alloc_category_id
  );

  ALTER TABLE reg_jur_allocator_link
  ADD CONSTRAINT r_reg_jur_allocator_link3 FOREIGN KEY (
    reg_allocator_id
  ) REFERENCES reg_allocator (
    reg_allocator_id
  );

  ALTER TABLE reg_jur_allocator_link
  ADD CONSTRAINT r_reg_jur_allocator_link4 FOREIGN KEY (
    start_allocator_id
  ) REFERENCES reg_allocator (
    reg_allocator_id
  );


 --Case Tables
 CREATE TABLE reg_case_allocator_link (
 reg_case_id  NUMBER(22,0),
 reg_alloc_category_id NUMBER(22,0),
 reg_alloc_target_id NUMBER(22,0),
 start_allocator_id NUMBER(22,0),
 reg_allocator_id NUMBER(22,0),
 user_id VARCHAR(18),
 time_stamp DATE);
 
 comment on table reg_case_allocator_link is ' (S) [19] Table holds set up of Linked Allocators for a Case';
 comment on column reg_case_allocator_link.reg_case_id is 'System assigned ID of Case';
 comment on column reg_case_allocator_link.reg_alloc_category_id is 'System assigned ID of Allocation Category';
 comment on column reg_case_allocator_link.reg_alloc_target_id is 'System assigned ID of Allocation Target';
 comment on column reg_case_allocator_link.start_allocator_id is 'ID of allocator that is being used as beginning of LInked Allocator. Beginning of Linked allocator is where start_allocator_id = reg_allocator_Id';
 comment on column reg_case_allocator_link.reg_allocator_id is 'System assigned ID of Allocation Factor';
 comment on column reg_case_allocator_link.user_id is 'Standard system-assigned user id used for audit purposes.';
 comment on column reg_case_allocator_link.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';

 ALTER TABLE reg_case_allocator_link
  ADD CONSTRAINT pk_reg_case_allocator_link PRIMARY KEY (
    reg_case_id,
    reg_alloc_category_id,
    reg_alloc_target_id,
    start_allocator_id
  );

  ALTER TABLE reg_case_allocator_link
  ADD CONSTRAINT r_reg_case_allocator_link1 FOREIGN KEY (
    reg_case_id
  ) REFERENCES reg_case (
    reg_case_id
  );

  ALTER TABLE reg_case_allocator_link
  ADD CONSTRAINT r_reg_case_allocator_link2 FOREIGN KEY (
    reg_alloc_category_id
  ) REFERENCES reg_alloc_category (
    reg_alloc_category_id
  );


  ALTER TABLE reg_case_allocator_link
  ADD CONSTRAINT r_reg_case_allocator_link3 FOREIGN KEY (
    reg_allocator_id
  ) REFERENCES reg_allocator (
    reg_allocator_id
  );

  ALTER TABLE reg_case_allocator_link
  ADD CONSTRAINT r_reg_case_allocator_link4 FOREIGN KEY (
    start_allocator_id
  ) REFERENCES reg_allocator (
    reg_allocator_id
  );


  ALTER TABLE reg_jur_alloc_account ADD linked_allocator_id NUMBER(22,0);
  comment on column reg_jur_alloc_account.linked_allocator_id is 'ID of Linked Allocator that is used for the Allocation Account. If not using a linked allocator, this column is null';

  ALTER TABLE reg_case_alloc_account ADD linked_allocator_id NUMBER(22,0);
  comment on column reg_case_alloc_account.linked_allocator_id is 'ID of Linked Allocator that is used for the Allocation Account. If not using a linked allocator, this column is null';
  
  
  --****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3570, 0, 2017, 1, 0, 0, 48355, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048355_reg_allocator_linking_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;