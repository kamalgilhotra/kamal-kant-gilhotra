/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050691_lessee_05_fk_ls_accrual_type_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/30/2018 Josh Sandler 	  Add Lessee Accrual types
||============================================================================
*/

ALTER TABLE LS_MONTHLY_ACCRUAL_STG
  ADD CONSTRAINT fk_accrual_type_id FOREIGN KEY (
    accrual_type_id
  ) REFERENCES ls_accrual_type (
    accrual_type_id
  )
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
(4301, 0, 2017, 3, 0, 0, 50691, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050691_lessee_05_fk_ls_accrual_type_ddl.sql', 1,
SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
