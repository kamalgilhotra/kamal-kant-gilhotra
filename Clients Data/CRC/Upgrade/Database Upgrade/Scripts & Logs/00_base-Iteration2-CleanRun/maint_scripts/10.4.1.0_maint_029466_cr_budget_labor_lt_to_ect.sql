/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029466_cr_budget_labor_lt_to_ect.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/19/2013 Marc Zawko       Point Release
||============================================================================
*/

SET SERVEROUTPUT ON

declare
	OBJECT_EXISTS exception;
	pragma exception_init(OBJECT_EXISTS, -00955);
	PRIMARY_KEY_EXISTS exception;
	pragma exception_init(PRIMARY_KEY_EXISTS, -02260);
	COLUMN_EXISTS exception;
	pragma exception_init(COLUMN_EXISTS, -01430);

begin
	begin

		execute immediate 'create table CR_BUDGET_LABOR_VERSION_MAP
			(
			 CR_BUDGET_VERSION_ID number(22,0) not null,
			 BUDGET_VERSION_ID    number(22,0),
			 TIME_STAMP           date,
			 USER_ID              varchar2(18)
			)';

		exception
			when OBJECT_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Table (cr_budget_labor_version_map) already exists.');
	end;

	begin

		execute immediate 'alter table CR_BUDGET_LABOR_VERSION_MAP
			add constraint CR_BUDGET_LABOR_VERSION_MAP_PK
				 primary key (CR_BUDGET_VERSION_ID)
				 using index tablespace PWRPLANT_IDX';
		
		exception
			when PRIMARY_KEY_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Primary key (cr_budget_labor_version_map) already exists.');
	end;

	begin

		execute immediate 'create table CR_BUDGET_LABOR_LT_TO_ECT
			(
			 estimate_charge_type number(22,0) not null,
			 labor_type           varchar2(35) not null,
			 time_stamp           date,
			 user_id              varchar2(18)
			)';

		exception
			when OBJECT_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Table (cr_budget_labor_lt_to_ect) already exists.');
	end;

	begin

		execute immediate 'alter table CR_BUDGET_LABOR_LT_TO_ECT
			add constraint CR_BUDGET_LABOR_LT_TO_ECT_PK
				 primary key (ESTIMATE_CHARGE_TYPE)
				 using index tablespace PWRPLANT_IDX';

		exception
			when PRIMARY_KEY_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Primary key (cr_budget_labor_lt_to_ect) already exists.');
	end;
 
	begin
		
		execute immediate 'alter table CR_BUDGET_DATA_LABOR2_DEFINE add INCLUDE_AS_LABOR_TYPE number(22,0)';

		exception
			when COLUMN_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Column include_as_labor_type (cr_budget_data_labor2_define) already exists.');

	end;

	begin

		execute immediate 'create table CR_BUDGET_LABOR_REPORT_GRAPHS
									(
									 DATAWINDOW varchar2(50) not null,
									 TITLE      varchar2(50),
									 AGGREGATE  number(22,0) not null,
									 TIME_STAMP date,
									 USER_ID     varchar2(18)
									)';

		exception
			when OBJECT_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Table (cr_budget_labor_report_graphs) already exists.');
	end;

	begin

		execute immediate 'alter table CR_BUDGET_LABOR_REPORT_GRAPHS
			add constraint CR_BUDGET_LABOR_RPT_GRAPHS_PK
				 primary key (DATAWINDOW, AGGREGATE)
				 using index tablespace PWRPLANT_IDX';

		exception
			when PRIMARY_KEY_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Primary key (cr_budget_labor_report_graphs) already exists.');
	end;

	begin

		execute immediate 'create table CR_BUDGET_LABOR_CAP_DOLLARS
			(
			 department           varchar2(35) not null,
			 funding_project      varchar2(35) not null,
			 cr_budget_version_id number(22,0) not null,
			 year                 number(22,0) not null,
			 jan_amt              number(22,2),
			 feb_amt              number(22,2),
			 mar_amt              number(22,2),
			 apr_amt              number(22,2),
			 may_amt              number(22,2),
			 jun_amt              number(22,2),
			 jul_amt              number(22,2),
			 aug_amt              number(22,2),
			 sep_amt              number(22,2),
			 oct_amt              number(22,2),
			 nov_amt              number(22,2),
			 dec_amt              number(22,2),
			 jan_qty              number(22,4),
			 feb_qty              number(22,4),
			 mar_qty              number(22,4),
			 apr_qty              number(22,4),
			 may_qty              number(22,4),
			 jun_qty              number(22,4),
			 jul_qty              number(22,4),
			 aug_qty              number(22,4),
			 sep_qty              number(22,4),
			 oct_qty              number(22,4),
			 nov_qty              number(22,4),
			 dec_qty              number(22,4),
			 jan_hrs              number(22,4),
			 feb_hrs              number(22,4),
			 mar_hrs              number(22,4),
			 apr_hrs              number(22,4),
			 may_hrs              number(22,4),
			 jun_hrs              number(22,4),
			 jul_hrs              number(22,4),
			 aug_hrs              number(22,4),
			 sep_hrs              number(22,4),
			 oct_hrs              number(22,4),
			 nov_hrs              number(22,4),
			 dec_hrs              number(22,4),
			 labor_type           varchar2(35) not null
			)';

		exception
			when OBJECT_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Table (cr_budget_labor_cap_dollars) already exists.');
	end;

	begin

		execute immediate 'alter table CR_BUDGET_LABOR_CAP_DOLLARS
			add constraint CR_BUDGET_LABOR_CAP_DOLLARS_PK
				 primary key (DEPARTMENT, FUNDING_PROJECT, CR_BUDGET_VERSION_ID, YEAR, LABOR_TYPE)
				 using index tablespace PWRPLANT_IDX';
		
		exception
			when PRIMARY_KEY_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Primary key (cr_budget_labor_cap_dollars) already exists.');
	end;

		
	begin

		execute immediate 'alter table ESTIMATE_CHARGE_TYPE add LABOR_TYPE_INDICATOR number(22,0)';

		exception
			when COLUMN_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Column labor_type_indicator (estimate_charge_type) already exists.');

	end;

	begin

		execute immediate 'alter table CR_TO_BUDGET_TO_CR_CONTROL add USES_FUNDING_PROJ number(22,0)';

		exception
			when COLUMN_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Column uses_funding_proj (cr_to_budget_to_cr_control) already exists.');

	end;

	begin
		execute immediate 'alter table CR_TO_BUDGET_TO_CR_CONTROL add CR_TABLE varchar2(30)';

		exception
			when COLUMN_EXISTS then
				DBMS_OUTPUT.PUT_LINE('Column cr_table (cr_to_budget_to_cr_control) already exists.');

	end;


end;
/

alter table CR_BUDGET_LABOR_LT_TO_ECT add QTY_AMT_HRS varchar2(35);

update CR_BUDGET_LABOR_LT_TO_ECT set QTY_AMT_HRS = 'amount' where QTY_AMT_HRS is null;

alter table CR_BUDGET_LABOR_LT_TO_ECT modify QTY_AMT_HRS not null;

alter table CR_BUDGET_LABOR_LT_TO_ECT drop primary key drop index;

alter table CR_BUDGET_LABOR_LT_TO_ECT
   add constraint CR_BUDGET_LABOR_LT_TO_ECT_PK
       primary key (ESTIMATE_CHARGE_TYPE, QTY_AMT_HRS)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (327, 0, 10, 4, 1, 0, 29466, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029466_cr_budget_labor_lt_to_ect.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;