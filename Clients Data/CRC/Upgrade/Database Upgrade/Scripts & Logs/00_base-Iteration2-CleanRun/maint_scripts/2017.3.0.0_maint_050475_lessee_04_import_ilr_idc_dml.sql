/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050475_lessee_04_import_ilr_idc_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 03/25/2018 Shane "C" Ward    Create import setup for Initial Direct Cost Import
||============================================================================
*/
--New Import Type
INSERT INTO PP_IMPORT_TYPE
            (import_type_id,
             description,
             long_description,
             import_table_name,
             archive_table_name,
             allow_updates_on_add,
             delegate_object_name,
             archive_additional_columns)
VALUES      (266,
             'Add: ILR Initial Direct Cost',
             'Lessee ILR Initial Direct Cost',
             'LS_IMPORT_ILR_INIT_DIRECT',
             'LS_IMPORT_ILR_INIT_DIRECT_ARC',
             1,
             'nvo_ls_logic_import',
             NULL);

--Associate new Import Type
INSERT INTO PP_IMPORT_TYPE_SUBSYSTEM
            (import_type_id,
             import_subsystem_id)
VALUES      (266,
             8);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 266,
             'ilr_id',
             'ILR ID',
             'ilr_id_xlate',
             1,
             1,
             'number(22,0)',
             'ls_ilr',
             1,
             'ilr_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 266,
             'revision',
             'Revision',
             NULL,
             0,
             1,
             'number(22,0)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 266,
             'description',
             'description',
             NULL,
             0,
             1,
             'varchar2(254)',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 266,
             'idc_group_id',
             'Initial Direct Cost Group ID',
             'idc_group_id_xlate',
             1,
             1,
             'number(22,0)',
             'ls_initial_direct_cost_group',
             1,
             'idc_group_id');

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 266,
             'date_incurred',
             'Date Incurred',
             NULL,
             0,
             1,
             'date mm/dd/yyyy format',
             NULL,
             1,
             NULL);

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             is_on_table,
             parent_table_pk_column)
VALUES      ( 266,
             'amount',
             'Amount',
             NULL,
             1,
             1,
             'number(22,2)',
             NULL,
             1,
             NULL);

--New Lookups IDCG
INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1110,
             'Ls IDC Group.Description',
             'Lessee Initial Direct Cost Group Description',
             'idc_group_id',
             '( select distinct b.idc_group_id from ls_initial_direct_cost_group b where upper( trim( <importfield> ) ) = upper( trim( b.description ) ) )',
             0,
             'ls_initial_direct_cost_group',
             'description');

INSERT INTO PP_IMPORT_LOOKUP
            (import_lookup_id,
             description,
             long_description,
             column_name,
             lookup_sql,
             is_derived,
             lookup_table_name,
             lookup_column_name)
VALUES      (1111,
             'Ls IDC Group.Long Description',
             'Lessee Initial Direct Cost Group Long Description',
             'idc_group_id',
             '( select distinct b.idc_group_id from ls_initial_direct_cost_group b where upper( trim( <importfield> ) ) = upper( trim( b.long_description ) ) )',
             0,
             'ls_initial_direct_cost_group',
             'long_description');

--Associate Lookups
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (266,
             'idc_group_id',
             1110);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (266,
             'idc_group_id',
             1111);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             import_lookup_id,
             column_name)
VALUES      (266,
             1096,
             'ilr_id');

--Default Template
INSERT INTO PP_IMPORT_TEMPLATE
            (import_template_id,
             import_type_id,
             description,
             long_description,
             do_update_with_add,
             is_autocreate_template)
VALUES      (( SELECT Max(import_template_id) + 1
               FROM   PP_IMPORT_TEMPLATE ),
             266,
             'Add: ILR Initial Direct Cost',
             'Default Template for Adding Initial Direct Costs to ILRs',
             1,
             0);

--Default Template Fields
INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 266 AND
                      description = 'Add: ILR Initial Direct Cost' ),
             1,
             266,
             'ilr_id',
             1096);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 266 AND
                      description = 'Add: ILR Initial Direct Cost' ),
             2,
             266,
             'revision',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 266 AND
                      description = 'Add: ILR Initial Direct Cost' ),
             3,
             266,
             'idc_group_id',
             1110);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 266 AND
                      description = 'Add: ILR Initial Direct Cost' ),
             4,
             266,
             'description',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 266 AND
                      description = 'Add: ILR Initial Direct Cost' ),
             5,
             266,
             'date_incurred',
             NULL);

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
VALUES      ( ( SELECT import_template_id
               FROM   PP_IMPORT_TEMPLATE
               WHERE  import_type_id = 266 AND
                      description = 'Add: ILR Initial Direct Cost' ),
             6,
             266,
             'amount',
             NULL);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4237, 0, 2017, 3, 0, 0, 50475, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050475_lessee_04_import_ilr_idc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;