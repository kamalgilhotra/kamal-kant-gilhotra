/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034105_lease_report_payment.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/07/2014 Ryan Oliveria  Adding a report
||============================================================================
*/

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
select 43, 'Lessee - Payments', 'ls_payment_hdr', 'uo_ls_selecttabs_payment'
   from dual;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(PP_REPORTS.REPORT_ID) + 1,
          user,
          sysdate,
          'Payments and Invoices',
          'Payments and Invoices',
          'Lessee',
          'dw_ls_rpt_payment_invoice',
          '',
          'Lessee - Schedules',
          '',
          'Lessee - 1018',
          null,
          null,
          null,
          11,
          308, --Report Type
          2, --Time Option
          43, --Filter ID
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1018',
          null,
          0
     from PP_REPORTS, PP_REPORTS_FILTER
	 where PP_REPORTS_FILTER.FILTER_UO_NAME = 'uo_ls_selecttabs_payment';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (951, 0, 10, 4, 2, 0, 34105, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034105_lease_report_payment.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;