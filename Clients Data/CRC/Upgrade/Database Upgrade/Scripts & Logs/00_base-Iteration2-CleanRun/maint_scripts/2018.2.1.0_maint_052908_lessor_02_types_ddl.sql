/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_052908_lessor_02_types_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 03/25/2019 B. Beck    	Adding fields to the LSR_ILR_SALES_SCH_INFO
||										and rebuild the dependent Types.
||============================================================================
*/

CREATE OR REPLACE TYPE LSR_NPV_VALUES AUTHID CURRENT_USER AS OBJECT
(
	payments NUMBER,
	guaranteed_residual NUMBER,
	unguaranteed_residual NUMBER
);
/

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO FORCE AUTHID CURRENT_USER AS OBJECT
(
	carrying_cost NUMBER,
	carrying_cost_company_curr NUMBER,
	fair_market_value NUMBER,
	fair_market_value_company_curr NUMBER,
	guaranteed_residual NUMBER,
	estimated_residual NUMBER,
	days_in_year NUMBER,
	purchase_option_amount NUMBER,
	termination_amount NUMBER,
	remeasurement_date DATE,
	investment_amount NUMBER,
	original_profit_loss NUMBER,
	new_beg_receivable NUMBER,
	prior_end_unguaran_residual NUMBER,
	remeasure_month_fixed_payment NUMBER,
	include_idc_sw NUMBER,
	accrued_deferred_rent NUMBER,
	npv LSR_NPV_VALUES,
	selling_profit_loss NUMBER,
	set_of_books_id NUMBER
);
/

CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO_TAB as table of LSR_ILR_SALES_SCH_INFO;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16383, 0, 2018, 2, 1, 0, 52908, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.1.0_maint_052908_lessor_02_types_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;