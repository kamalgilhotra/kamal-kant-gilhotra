 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_041913_pcm_suspend_dv_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/13/2015 D. Mendel     Create suspend/unsuspend dynamic validation
 ||============================================================================
 */ 

INSERT INTO wo_validation_type (wo_validation_type_id, description, long_description, FUNCTION, find_company, col1, col2, hard_edit)
SELECT 69, 'WO/FP Suspend/Unsuspend (Pre)', 'WO/FP Suspend/Unsuspend (Pre)', 'w_wo_detail.cb_suspend', 'select company_id from work_order_control where work_order_id = <arg1>', 'work_order_id', 'SUSPEND/UNSUSPEND', 1
FROM dual
where not exists (select 1 from wo_validation_type where wo_validation_type_id = 69);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2492, 0, 2015, 1, 0, 0, 41913, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041913_pcm_suspend_dv_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;