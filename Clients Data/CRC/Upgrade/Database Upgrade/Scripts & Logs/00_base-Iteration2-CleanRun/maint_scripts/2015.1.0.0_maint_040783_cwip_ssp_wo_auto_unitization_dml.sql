/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_040783_cwip_ssp_wo_auto_unitization_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 2/16/2015   Luke Warren     creation of ssp process - Auto Unitization
||============================================================================
*/

-- Use merge on the off chance that they already have a record called Depreciation Approval 
MERGE INTO pp_processes p1
USING (
	SELECT Nvl(Max(process_id),0) + 1 AS process_id, 'WO Auto Unitization' AS description,
		'WO Auto Unitization' AS long_description, 'ssp_wo_auto_unitization.exe' AS executable_file,
		'2015.1.0.0' AS version, 0 AS allow_concurrent, 1 AS wo_month_end_flag
	FROM pp_processes) p2
ON (Lower(Trim(p1.description)) = Lower(Trim(p2.description)))
WHEN MATCHED THEN
	UPDATE SET p1.long_description = p2.long_description,
		p1.executable_file = p2.executable_file,
		p1.version = p2.version,
		p1.allow_concurrent = p2.allow_concurrent,
		p1.wo_month_end_flag = p2.wo_month_end_flag
WHEN NOT MATCHED THEN
	INSERT (process_id, description, long_description, executable_file, version, allow_concurrent, wo_month_end_flag)
	VALUES (p2.process_id, p2.description, p2.long_description, p2.executable_file, p2.version, p2.allow_concurrent, p2.wo_month_end_flag)
;

-- new entry in pp_month_end_options for the Auto Unitization Late Months Job Server Default Value
-- Work Orders to Unitize default to ALL
INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
 process_id,
 1 AS option_id,
 'Work Orders to Unitize',
 'Auto Unitization Work Orders to Unitize',
 'ALL',
 Sys_Context('USERENV','SESSION_USER'),
 SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_auto_unitization.exe';

-- Previous Months to Process default to 1
INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
 process_id,
 2 AS option_id,
 'Late Unitization Options',
 'Auto Unitization Late Unitization Options',
 'Unitization and Late Charge Unitization',
 Sys_Context('USERENV','SESSION_USER'),
 SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_auto_unitization.exe';

-- Previous Months to Process default to 1
INSERT INTO pp_month_end_options (company_id, process_id, option_id, description, long_description, option_value, user_id, time_stamp)
SELECT -1 AS company_id,
 process_id,
 3 AS option_id,
 'Previous Months to Process',
 'Auto Unitization Previous Months to Process',
 '1',
 Sys_Context('USERENV','SESSION_USER'),
 SYSDATE
FROM pp_processes
WHERE executable_file = 'ssp_wo_auto_unitization.exe';

-- Create new SQL Dynamic Validation type for the Work Order Control Automatic Unitization
insert into wo_validation_type (
	wo_validation_type_id, description, long_description, "FUNCTION", find_company, col1, col2, hard_edit)
values (
	1056, 'WO Control - Auto Unitization', 'WO Control - Auto Unitization', 'f_unitization_audit', 'select work_order_id, company_id from unitize_wo_list_temp where work_order_id = <arg1> and company_id = <arg2>', 'work_order_id', 'company_id', 1);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2316, 0, 2015, 1, 0, 0, 040783, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_040783_cwip_ssp_wo_auto_unitization_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;