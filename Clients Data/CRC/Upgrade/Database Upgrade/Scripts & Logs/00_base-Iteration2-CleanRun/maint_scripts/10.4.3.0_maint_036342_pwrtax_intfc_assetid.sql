/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036342_pwrtax_intfc_assetid.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 05/29/2014 Andrew Scott        Alter adds, rets, xfer intfcs
||                                         to use asset id if allowed for the
||                                         company.
||============================================================================
*/

----change the interface tables to have asset id:

----Retirements
alter table TAX_BOOK_TRANSACTIONS add ASSET_ID number(22, 0);
comment on column TAX_BOOK_TRANSACTIONS.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Optional and populated depending on the company system option settings.';

alter table TAX_BOOK_TRANSACTIONS_GRP add ASSET_ID number(22, 0);
comment on column TAX_BOOK_TRANSACTIONS_GRP.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Optional and populated depending on the company system option settings.';

----Transfers
alter table TAX_TEMP_BOOK_TRANSFERS_STG add ASSET_ID_FROM number(22, 0);
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.ASSET_ID_FROM is 'System-assigned identifier of a particular asset recorded on the CPR Ledger (for the "from" side of the transfer).  Optional and populated depending on the company system option settings.';

alter table TAX_TEMP_BOOK_TRANSFERS_STG add ASSET_ID_TO number(22, 0);
comment on column TAX_TEMP_BOOK_TRANSFERS_STG.ASSET_ID_TO is 'System-assigned identifier of a particular asset recorded on the CPR Ledger (for the "to" side of the transfer).  Optional and populated depending on the company system option settings.';

--------asset id already exists on this table...
--------alter table TAX_BOOK_TRANSFERS add ASSET_ID number(22, 0);
comment on column TAX_BOOK_TRANSFERS.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Optional and populated depending on the company system option settings.';


alter table TAX_TRANS_AUDIT_TRAIL_GRP add ASSET_ID_TO number(22, 0);
comment on column TAX_TRANS_AUDIT_TRAIL_GRP.ASSET_ID_TO is 'System-assigned identifier of a particular asset recorded on the CPR Ledger (for the "to" side of the transfer).  Optional and populated depending on the company system option settings.';

--------asset id already exists on this table...
--------alter table TAX_TRANSFER_CONTROL add ASSET_ID number(22, 0);
comment on column TAX_TRANSFER_CONTROL.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Used for creating new tax records for the transfer-tos.  Optional and populated depending on the company system option settings.';

--------asset id already exists on this table...
--------alter table TAX_TEMP_TRANSFER_CONTROL add ASSET_ID number(22, 0);
comment on column TAX_TEMP_TRANSFER_CONTROL.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Used for creating new tax records for the transfer-tos.  Optional and populated depending on the company system option settings.';

alter table TAX_BOOK_TRANSFERS_GRP add ASSET_ID number(22, 0);
comment on column TAX_BOOK_TRANSFERS_GRP.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger (for the "from" side of the transfer).  Optional and populated depending on the company system option settings.';

alter table TAX_BOOK_TRANSFERS_GRP add ASSET_ID_TO number(22, 0);
comment on column TAX_BOOK_TRANSFERS_GRP.ASSET_ID_TO is 'System-assigned identifier of a particular asset recorded on the CPR Ledger (for the "to" side of the transfer).  Optional and populated depending on the company system option settings.';

----Additions
alter table TAX_BOOK_TRANS_ADDS add ASSET_ID number(22, 0);
comment on column TAX_BOOK_TRANS_ADDS.ASSET_ID is 'System-assigned identifier of a particular asset recorded on the CPR Ledger.  Optional and populated depending on the company system option settings.';

----previous index has NVLs around the fields.
----per Julia, these will always have default values
----so these NVLs should be removed.
drop index TBT_FUNC_GROUP_IDX;

create index TBT_FUNC_GROUP_IDX
   on TAX_BOOK_TRANSLATE (COMPANY_ID,
                          BUS_SEGMENT_ID,
                          UTILITY_ACCOUNT_ID,
                          SUB_ACCOUNT_ID,
                          TAX_LOCATION_ID,
                          CLASS_CODE_ID,
                          CLASS_CODE_VALUE,
                          GL_ACCOUNT_ID,
                          TAX_DISTINCTION_ID)
     tablespace PWRPLANT_IDX;

----adds interface insert into tax_depreciation and audit tables need this hint to run in a reasonable amount of time.
insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('nvo_tax_logic_additions.of_initializeadditions.td', 1, '/*+ USE_HASH(tax_book_trans_adds, tax_record_control) */');

insert into PP_DATAWINDOW_HINTS
   (DATAWINDOW, SELECT_NUMBER, HINT)
values
   ('nvo_tax_logic_additions.of_initializeadditions.taat', 2,
    '/*+ USE_HASH(tax_book_trans_adds, tax_record_control) */');

---- drop the unique index here and just build an index.
drop index TEMP_TAX_XFER_CTRL_UDX1_FTRID;

create index TEMP_TAX_XFER_CTRL_FTRID_IDX on TAX_TEMP_TRANSFER_CONTROL (FROM_TRID);

---- add the company ppbase system option to allow asset id to be used in the tax interfaces
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values
   ('Extract Asset IDs with Interfaces',
    'Whether or not to pull use asset id with the interfaces.  Only set to "Yes" if the Individual Asset Depreciation system option is set to "Yes".',
    0, 'No', 1, 1);

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Extract Asset IDs with Interfaces', 'powertax');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Extract Asset IDs with Interfaces', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Extract Asset IDs with Interfaces', 'No');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1117, 0, 10, 4, 3, 0, 36342, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036342_pwrtax_intfc_assetid.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
