 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_045840_pwrtax_tax_plant_recon_section_name_change_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 07/07/2016 Anand R        Modify description for tax plant section names
 ||============================================================================
 */ 

update tax_plant_recon_section set description = 'Book Balance Reconciliation' where tax_pr_section_id = 3;

update tax_plant_recon_section set description = 'PowerTax Book Basis to Tax Basis Federal and State' where tax_pr_section_id = 4;

update tax_plant_recon_section set description = 'Book Reserve Reconciliation' where tax_pr_section_id = 5;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3235, 0, 2016, 1, 0, 0, 045840, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045840_pwrtax_tax_plant_recon_section_name_change_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;