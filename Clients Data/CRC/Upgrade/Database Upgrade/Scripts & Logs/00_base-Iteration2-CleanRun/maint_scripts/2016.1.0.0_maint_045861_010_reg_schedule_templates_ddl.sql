/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045861_010_reg_schedule_templates_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 08/18/2016 Shane Ward		 Schedule Builder Template Functionality
||============================================================================
*/

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE reg_schedule_template_map';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE reg_schedule_template_sheet';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

BEGIN
   EXECUTE IMMEDIATE 'DROP TABLE reg_schedule_template';
EXCEPTION
   WHEN OTHERS THEN
      IF SQLCODE != -942 THEN
         RAISE;
      END IF;
END;
/

create table reg_schedule_template (
main_form varchar2(75) not null,
file_name varchar2(254) not null,
file_path varchar2(4000),
file_data blob,
user_id varchar2(18),
time_stamp date);

alter table reg_schedule_template add constraint pk_reg_schedule_template primary key (main_form, file_name);

comment on table reg_schedule_template is 'Table for holding Excel Template Files to be leveraged by schedule builder';
COMMENT ON COLUMN REG_SCHEDULE_TEMPLATE.main_form IS 'Identifier of Form Template belongs to.';
COMMENT ON COLUMN REG_SCHEDULE_TEMPLATE.FILE_NAME IS 'Name of Excel Workbook used as Template.';
COMMENT ON COLUMN REG_SCHEDULE_TEMPLATE.FILE_DATA IS 'Binary data of the attached template.';
COMMENT ON COLUMN REG_SCHEDULE_TEMPLATE.USER_ID IS 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
COMMENT ON COLUMN REG_SCHEDULE_TEMPLATE.TIME_STAMP IS 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';



create table reg_schedule_template_sheet (
main_form VARCHAR2(75) NOT null,
file_name varchar2(254) not null,
sheet_name varchar2(254) not null,
page_number number(22,0) not null,
schedule_id NUMBER(22,0),
start_row NUMBER(22,0),
user_id varchar2(18),
time_stamp date);

alter table reg_schedule_template_sheet add constraint pk_reg_schedule_template_sheet primary key (file_name, sheet_name);
ALTER TABLE reg_schedule_template_sheet ADD CONSTRAINT fk_reg_sched_template_sheet_m1 FOREIGN KEY (main_form, file_name) REFERENCES reg_schedule_template (main_form, file_name);

comment on table reg_schedule_template_sheet is 'List of Sheets within an Excel Workbook that are used with a Form';
comment on column reg_schedule_template_sheet.file_name is 'Name of Excel Workbook used as Template';
comment on column reg_schedule_template_sheet.sheet_name is 'Name of Excel Worksheet within the Workbook';
comment on column reg_schedule_template_sheet.page_number is 'Page number/order of the sheet';
COMMENT ON COLUMN reg_schedule_template_sheet.USER_ID IS 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
COMMENT ON COLUMN reg_schedule_template_sheet.TIME_STAMP IS 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

ALTER TABLE REG_SCHEDULE_LINE_NUMBER ADD col_specific_formulas NUMBER(22,0);

comment on column reg_schedule_line_number.col_specific_formulas is 'Flag to indicate whether the line leverages Column Specific Formulas/Values 0/null = no 1 = yes';


ALTER TABLE reg_schedule_sql_args ADD reg_case_id NUMBER(22,0);

comment on column reg_schedule_sql_args.reg_case_id is 'Case ID assigned to the Column and SQL Arg being retrieved';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3274, 0, 2016, 1, 0, 0, 045861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045861_010_reg_schedule_templates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;