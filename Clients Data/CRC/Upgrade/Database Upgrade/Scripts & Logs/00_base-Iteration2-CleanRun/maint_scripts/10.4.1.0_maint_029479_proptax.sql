SET DEFINE OFF

/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029479_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/14/2013 Andrew Scott   Point Release
|| 10.4.1.2   12/11/2013 Julia Breuer   Script was removed
||============================================================================
*/
/*
----KY schedule J, K for public service corporations.  Developed at LGE KU using AEP's & Duke's schedules as a starting point.
----website for KY forms: http://revenue.ky.gov/forms/12ptf.htm

----J return
insert into PP_REPORTS (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW) values ( 513703, sysdate, user, 'KY-61A200J-Public Serv-Summary', 'Kentucky-61A200J-Public Service Companies-Property Summary by Taxing District', 'dw_ptr_rtn_ky_61a200_sch_j', 'KY-61A200(J)', 10, 200, 9, 7, 4, 3, 0 );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513703, 'pt_report_pt_company_field', 'field1', 'Revision Date' );

----K return
insert into PP_REPORTS (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW) values ( 513704, sysdate, user, 'KY-61A200K-Public Service-Op Prop', 'Kentucky-61A200K-Public Service Companies-Operating Property by Taxing District', 'dw_ptr_rtn_ky_61a200_sch_k', 'KY-61A200(K)', 10, 200, 9, 7, 4, 3, 0 );
----there was no revision date on the K state form, so not report field labels needed (leave the join in in case it's needed later).

----J-K Category rollup
----notes: not submitting tax types or rollup assignments---this would be too difficult.  better to let the public
----service companies define their own tax types
----(by their ferc accounts, for example)
----and let users apply all the rollups needed for the A, J, and K
insert into PT_TYPE_ROLLUP (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID) values ( 28002, sysdate, user, 'KY-61A200J-K-Filing Category', 'KY-61A200J-K-Filing Category', 2 );

insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 1, 'Manufacturers Raw Materials','Manufacturers Raw Materials');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 2, 'Manufacturing Machinery','Manufacturing Machinery');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 3, 'Radio-Television-Telephonic Equip','Radio-Television-Telephonic Equipment');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 4, 'Pollution Control Equipment','Pollution Control Equipment');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 5, 'Foreign Trade Zone','Foreign Trade Zone');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 6, 'Recycling Equipment','Recycling Equipment');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 7, 'IRB Property','IRB Property');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 8, 'Inventory In - Transit','Inventory In - Transit');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 9, 'Business Inventory for Resale','Business Inventory for Resale');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 10, 'Real Estate Owned and Leased','Real Estate Owned and Leased');
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, DESCRIPTION, LONG_DESCRIPTION) values (28002, 11, 'Tangible Personalty','Tangible Personalty Owned and Leased');


----K Property Description rollup
----notes: not inserting type rollup values for base.  At LGE-KU, users provided the descriptions they
----want on the external code of the tax type.
----These will have to be populated manually be client or on-site consultant.
----not inserting tax type assignments since types are not being standardized
insert into PT_TYPE_ROLLUP (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID) values ( 28003, sysdate, user, 'KY-61A200K-Property Description', 'KY-61A200K-Property Description', 2 );


----K OP vs NON-OP rollup
----notes:not inserting tax type assignments since K types are not being made
insert into PT_TYPE_ROLLUP (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID) values ( 28004, sysdate, user, 'KY-61A200K-Op vs Non-Op', 'KY-61A200K-Op vs Non-Op', 2 );

insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28004, 1, sysdate, user, 'Operating', 'Operating', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28004, 2, sysdate, user, 'Non-Operating', 'Non-Operating', '' );

----A return
insert into PP_REPORTS (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW) values ( 513705, sysdate, user, 'KY-61A200A-Public Service-Total Sys', 'Kentucky-61A200A-Public Service Companies-Total Unit System', 'dw_ptr_rtn_ky_61a200_sch_a', 'KY-61A200(A)', 10, 200, 9, 7, 4, 3, 0 );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513705, 'pt_report_pt_company_field', 'field1', 'Revision Date' );

----A Line Number rollup
----notes:not inserting tax type assignments since types are not being standardized
insert into PT_TYPE_ROLLUP (TYPE_ROLLUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, STATUS_CODE_ID) values ( 28005, sysdate, user, 'KY-Schedule A Line Number', 'KY-Schedule A Line Number', 2 );

insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 1, sysdate, user, 'Line 1: General Plant---Real', 'Line 1: General Plant---Real', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 2, sysdate, user, 'Line 2: Land', 'Line 2: Land', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 3, sysdate, user, 'Line 3: Buildings and Leasehold Imp', 'Line 3: Buildings and Leasehold Improvements', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 4, sysdate, user, 'Line 4: Rights of Way', 'Line 4: Rights of Way', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 5, sysdate, user, 'Line 5: Pipelines---T/D/I', 'Line 5: Pipelines---Transmission / Distribution / Interconnect', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 6, sysdate, user, 'Line 6: Stored Gas, Oil and Coal', 'Line 6: Stored Gas, Oil and Coal (Noncurrent)', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 7, sysdate, user, 'Line 7: CWIP---Real', 'Line 7: CWIP---Real', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 8, sysdate, user, 'Line 8: Storage Fields', 'Line 8: Storage Fields', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 9, sysdate, user, 'Line 9: Underground Conduits', 'Line 9: Underground Conduits', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 10, sysdate, user, 'Line 10: Shelters & Huts', 'Line 10: Shelters & Huts', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 11, sysdate, user, 'Line 11: Services (Buried Pipe', 'Line 11: Services (Buried Pipe)', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 12, sysdate, user, 'Line 12: Real Property Held Foreign', 'Line 12: Real Property Held in Foreign Trade Zone', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 13, sysdate, user, 'Line 13: Operating Leased Real', 'Line 13: Operating Leased Property---Real', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 14, sysdate, user, 'Line 14: Capital Leased Real', 'Line 14: Capital Leased Property---Real', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 15, sysdate, user, 'Line 15: Noncarrier Real', 'Line 15: Noncarrier Property---Real', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 16, sysdate, user, 'Line 16: Industrial Revenue Bonds', 'Line 16: Industrial Revenue Bonds---Real', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 17, sysdate, user, 'Line 17: Other', 'Line 17: Other', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 19, sysdate, user, 'Line 19: General Plant---Personal', 'Line 19: General Plant---Personal', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 20, sysdate, user, 'Line 20: Furniture and Fixtures', 'Line 20: Furniture and Fixtures', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 21, sysdate, user, 'Line 21: Computers and Software', 'Line 21: Computers and Software', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 22, sysdate, user, 'Line 22: Materials and Supplies', 'Line 22: Materials and Supplies', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 23, sysdate, user, 'Line 23: CWIP---Personal', 'Line 23: CWIP---Personal', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 24, sysdate, user, 'Line 24: Stores and Fuel Stock Expe', 'Line 24: Stores and Fuel Stock Expenses Undistributed', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 25, sysdate, user, 'Line 25: Fuel Stock Stored---Curren', 'Line 25: Fuel Stock Stored---Current', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 26, sysdate, user, 'Line 26: Business Inventory Held Fo', 'Line 26: Business Inventory Held For Resale', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 27, sysdate, user, 'Line 27: Motor Vehicles-Owned Lease', 'Line 27: Motor Vehicles---Owned & Capital Leased', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 28, sysdate, user, 'Line 28: Motor Vehicles---Unlicense', 'Line 28: Motor Vehicles---Unlicensed', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 29, sysdate, user, 'Line 29: Motor Vehicles---Op Leased', 'Line 29: Motor Vehicles---Operating Leased', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 30, sysdate, user, 'Line 30: Railroad Cars', 'Line 30: Railroad Cars', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 31, sysdate, user, 'Line 31: Gathering Pipe Lines', 'Line 31: Gathering Pipe Lines', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 32, sysdate, user, 'Line 32: General Inventory', 'Line 32: General Inventory', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 33, sysdate, user, 'Line 33: Office Equipment', 'Line 33: Office Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 34, sysdate, user, 'Line 34: Communication Equipment', 'Line 34: Communication Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 35, sysdate, user, 'Line 35: Heavy Machinery & Equip', 'Line 35: Heavy Machinery & Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 36, sysdate, user, 'Line 36: Watercraft', 'Line 36: Watercraft', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 37, sysdate, user, 'Line 37: Residuals and Extracted', 'Line 37: Residuals and Extracted Products', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 38, sysdate, user, 'Line 38: Spare Parts & Equipment', 'Line 38: Spare Parts & Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 39, sysdate, user, 'Line 39: Drilling Equipment', 'Line 39: Drilling Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 40, sysdate, user, 'Line 40: Compressors & Odorizers', 'Line 40: Compressors & Odorizers', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 41, sysdate, user, 'Line 41: Meters & Regulators', 'Line 41: Meters & Regulators', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 42, sysdate, user, 'Line 42: Services (Ele Above Grnd)', 'Line 42: Services (Electric Above Ground)', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 43, sysdate, user, 'Line 43: Towers', 'Line 43: Towers', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 44, sysdate, user, 'Line 44: CCNC', 'Line 44: Completed Construction Not Classified', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 45, sysdate, user, 'Line 45: Miscellaneous Personal Pro', 'Line 45: Miscellaneous Personal Property', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 46, sysdate, user, 'Line 46: Capital Leased Property', 'Line 46: Capital Leased Property---Personal', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 47, sysdate, user, 'Line 47: Operating Leased Property', 'Line 47: Operating Leased Property---Personal', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 48, sysdate, user, 'Line 48: Noncarrier Property', 'Line 48: Noncarrier Property---Personal', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 49, sysdate, user, 'Line 49: Industrial Revenue Bonds', 'Line 49: Industrial Revenue Bonds---Personal', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 50, sysdate, user, 'Line 50: Personal Property Held in ', 'Line 50: Personal Property Held in Foreign Trade Zone', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 51, sysdate, user, 'Line 51: Manufacturing Machinery', 'Line 51: Manufacturing Machinery', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 52, sysdate, user, 'Line 52: Business Inventory', 'Line 52: Business Inventory / Raw Materials---Manuf. Mach', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 53, sysdate, user, 'Line 53: CWIP---Manufacturing Mach', 'Line 53: CWIP---Manufacturing Machinery', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 54, sysdate, user, 'Line 54: Operating Leased Property', 'Line 54: Operating Leased Property---Manuf. Mach', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 55, sysdate, user, 'Line 55: Industrial Revenue Bonds', 'Line 55: Industrial Revenue Bonds---Manuf. Mach', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 56, sysdate, user, 'Line 56: Cert Pollution Control', 'Line 56: Certified Pollution Control Facility Machinery', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 57, sysdate, user, 'Line 57: Recyling Equipment', 'Line 57: Recyling Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 58, sysdate, user, 'Line 58: Radio, Television and Tele', 'Line 58: Radio, Television and Telephonic Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 59, sysdate, user, 'Line 59: Water Treatment Equipment', 'Line 59: Water Treatment Equipment', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 60, sysdate, user, 'Line 60: Other', 'Line 60: Other', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 63, sysdate, user, 'Line 63: Real---Owned', 'Line 63: Real---Owned', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 64, sysdate, user, 'Line 64: Real---Operating Leased', 'Line 64: Real---Operating Leased', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 65, sysdate, user, 'Line 65: Personal---Owned', 'Line 65: Personal---Owned', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 66, sysdate, user, 'Line 66: Personal---Operating Lease', 'Line 66: Personal---Operating Leased', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 67, sysdate, user, 'Line 67: Manufacturing Machinery', 'Line 67: Manufacturing Machinery', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 68, sysdate, user, 'Line 68: Other', 'Line 68: Other', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 71, sysdate, user, 'Line 71: Goodwill', 'Line 71: Goodwill', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 72, sysdate, user, 'Line 72: Permits, Licenses and Cust', 'Line 72: Permits, Licenses and Customer Lists', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 73, sysdate, user, 'Line 73: Allowances', 'Line 73: Allowances', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 74, sysdate, user, 'Line 74: Derivative Instrument Asse', 'Line 74: Derivative Instrument Assets', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 75, sysdate, user, 'Line 75: Organizational Expenses an', 'Line 75: Organizational Expenses and Franchises', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 76, sysdate, user, 'Line 76: Other', 'Line 76: Other', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 78, sysdate, user, 'Line 78: .25 Intangibles', 'Line 78: .25 Intangibles', '' );
insert into PT_TYPE_ROLLUP_VALUES (TYPE_ROLLUP_ID, TYPE_ROLLUP_VALUE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, EXTERNAL_CODE) values ( 28005, 79, sysdate, user, 'Line 79: .015 Intangibles', 'Line 79: .015 Intangibles', '' );

----L return:

insert into PP_REPORTS (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DYNAMIC_DW) values ( 513706, sysdate, user, 'KY-61A200L-Public Service-Allo Fact', 'Kentucky-61A200L-Public Service Companies-Allocation Factors', 'dw_ptr_rtn_ky_61a200_sch_l', 'KY-61A200(L)', 10, 200, 9, 7, 4, 3, 0 );

insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field1', 'Revision Date' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field2', 'Total Gross Revenue' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field3', 'KY Gross Revenue' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field4', 'Total Net Revenue' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field5', 'KY Net Revenue' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field6', 'Total Customers' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field7', 'KY Customers' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field8', 'Total Wire Miles' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field9', 'KY Wire Miles' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field10', 'Total Diameter In. Pipe' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field11', 'KY Diameter In. Pipe' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field12', 'Total Through Put Pipe' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field13', 'KY Through Put Pipe' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field14', 'Total Other' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field15', 'KY Other' );
insert into PT_REPORT_FIELD_LABELS (REPORT_ID, TABLE_NAME, COLUMN_NAME, LABEL) values ( 513706, 'pt_report_pt_company_field', 'field16', 'Other Text' );

----L rollups:
----uses the same rollups as the A
*/
--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (324, 0, 10, 4, 1, 0, 29479, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029479_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

SET DEFINE ON