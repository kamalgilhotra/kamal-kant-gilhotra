 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_043533_reg_recon_setup_dml.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/03/2015 	Shane Ward      Initialize reg_hist_recon_setup for
 ||                                     clients who upgrade to 2015.1
 ||============================================================================
 */

INSERT INTO reg_hist_recon_setup
	(historic_version_id, recon_source)
	SELECT historic_version_id, 0
		FROM reg_historic_version
	 WHERE historic_version_id <> 0
	 and historic_version_id not in 
		(select historic_version_id from reg_hist_recon_setup);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2479, 0, 2015, 1, 0, 0, 43533, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043533_reg_recon_setup_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;