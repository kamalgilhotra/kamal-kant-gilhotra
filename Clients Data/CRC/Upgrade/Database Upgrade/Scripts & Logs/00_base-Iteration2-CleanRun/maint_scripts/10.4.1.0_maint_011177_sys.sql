/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011177_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/20/2013 Lee Quinn      Fix USER_ID column datatypes
||============================================================================
*/

--alter table TAX_ACCRUAL_REP_CUSTOM_OPTION modify USER_ID varchar2(18);
alter table PP_MYPP_USER_TEMPLATE         modify USER_ID varchar2(18);
alter table HRS_QTY_CONTROL               modify USER_ID varchar2(18);
alter table MATLREC_MAP_TO_AS_BUILT       modify USER_ID varchar2(18);
alter table FCST_BUDGET_LOAD_ADDITIONS    modify USER_ID varchar2(18);
--alter table TAX_ACCRUAL_CONSOL_DRILL      modify USER_ID varchar2(18);
alter table WO_EST_HIERARCHY_MAP          modify USER_ID varchar2(18);
alter table PP_INTERFACE_GROUPS           modify USER_ID varchar2(18);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (526, 0, 10, 4, 1, 0, 11177, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_011177_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
