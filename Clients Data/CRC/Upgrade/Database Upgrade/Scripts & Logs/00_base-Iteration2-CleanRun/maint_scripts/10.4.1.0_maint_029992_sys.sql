/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_029992_sys.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   05/13/2013 Stephen Bynoe  Point Release
||============================================================================
*/

--Increasing alert_message column length to 4000

alter table PP_VERIFY_USER_COMMENTS modify ALERT_MESSAGE varchar2(4000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (383, 0, 10, 4, 1, 0, 29992, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029992_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
