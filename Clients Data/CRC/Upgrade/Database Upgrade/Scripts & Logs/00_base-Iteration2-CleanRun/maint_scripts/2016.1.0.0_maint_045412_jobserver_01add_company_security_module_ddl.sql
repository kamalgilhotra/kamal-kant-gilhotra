/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045412_jobserver_01add_company_security_module_dd.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 05/27/2016 Jared Watkins		 Add column to pp_job_executable that identifies 
||											 where to check for company security access
||============================================================================
*/ 
alter table pp_job_executable add company_security_module varchar(35);

COMMENT ON COLUMN PWRPLANT.PP_JOB_EXECUTABLE.company_security_module IS 'Name identifying which PP module should be used to check company security for the user';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3200, 0, 2016, 1, 0, 0, 045412, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045412_jobserver_01add_company_security_module_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;