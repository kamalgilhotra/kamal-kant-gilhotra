/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029581_pwrtax_PP_JOB.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   03/14/2013 Roger Roach      Changed the Fast Tax to use Oracle Packages
||============================================================================
*/

create or replace package PP_JOB is
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: PP_JOB
   --|| Description: Summit Jobs to Oracle Job Queue
   --||============================================================================
   --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 1.0      03/20/2013 Roger Roach    Original Version
   --|| 1.1      05/24/2013 Roger Roach    Added second submit for tax archiving
   --||============================================================================

   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2,
                   A_ID    pls_integer) return integer;

   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2) return integer;

   function REMOVE(A_ID integer) return integer;
end PP_JOB;
/

create or replace package body PP_JOB is
   -- =============================================================================
   --  Function REMOVE
   -- =============================================================================
   function REMOVE(A_ID integer) return integer is

      L_CODE pls_integer;

   begin
      DBMS_JOB.REMOVE(A_ID);
      commit;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Remove Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end REMOVE;

   -- =============================================================================
   --  Function SUBMIT
   -- =============================================================================
   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2,
                   A_ID    pls_integer) return integer is

      L_SQL  varchar2(200);
      L_NUM  pls_integer;
      L_CODE pls_integer;

   begin
      DBMS_OUTPUT.ENABLE(10000);

      case A_TYPE
         when 'function' then
            L_SQL := 'declare code number; begin code := pwrplant.' || A_FUNCT || '(' ||
                     TO_CHAR(A_ID) || '); end;';
         when 'procedure' then
            L_SQL := 'pwrplant.' || A_FUNCT || '(' || TO_CHAR(A_ID) || ');';
         else
            return -1;
      end case;

      DBMS_JOB.SUBMIT(L_NUM, L_SQL, sysdate, null);
      DBMS_OUTPUT.PUT_LINE('Submit Tax Job ' || TO_CHAR(L_NUM) || ' ');
      commit;
      return(L_NUM);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Submit Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end SUBMIT;

   -- =============================================================================
   --  Function SUBMIT
   -- =============================================================================
   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2) return integer is

      L_SQL  varchar2(200);
      L_NUM  pls_integer;
      L_CODE pls_integer;

   begin
      DBMS_OUTPUT.ENABLE(10000);

      case A_TYPE
         when 'function' then
            L_SQL := 'declare code number; begin code := pwrplant.' || A_FUNCT || ' ; end;';
         when 'procedure' then
            L_SQL := 'pwrplant.' || A_FUNCT;
         else
            return - 1;
      end case;

      DBMS_JOB.SUBMIT(L_NUM, L_SQL, sysdate, null);
      DBMS_OUTPUT.PUT_LINE('Submit Tax Job ' || TO_CHAR(L_NUM) || ' ');
      commit;
      return(L_NUM);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Submit Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end SUBMIT;
end PP_JOB;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (407, 0, 10, 4, 1, 0, 29581, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029581_pwrtax_PP_JOB.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
