/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_046232_prov_tbbs_column_close_descriptions_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version	  Date	     Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 09/12/2016 Jared Watkins  Add missing descriptions to tbbs_column_close
||============================================================================
*/

update tbbs_column_close set description =
  decode(column_name, 'book_amount', 'Book Amount',
  'book_rc', 'Book Reclass',
  'book_adj', 'Book Adjustments',
  'm_beg_bal_py', 'Prior Year M-Item Beginning Balance',
  'm_activity_py', 'Prior Year M-Item Activity',
  'm_adjust_py', 'Prior Year M-Item Adjustments',
  'm_variance_py', 'Prior Year Activity Variance',
  'm_beg_bal_cy', 'Current Year M-Item Beginning Balance',
  'm_activity_cy', 'Current Year M-Item Activity',
  'm_adjust_cy', 'Current Year M-Item Adjustments',
  'm_rta_cy', 'RTA Booked in Current Year',
  'm_variance_cy', 'Current Year Activity Variance',
  'm_m13_py', 'Prior Year M-Item Month 13',
  'm_unbooked_rta', 'Unbooked Prior Year RTA',
  'tax_m_rc', 'M-Item Reclasses',
  'tax_m_adj', 'Manual M-Item Adjustments',
  'm_def_tax_bal', 'M-Item Deferred Tax Balance',
  'm_def_tax_bal_reg', 'M-Item Deferred Tax Balance (Regulatory)',
  'tax_def_rc', 'Deferred Tax Balance Reclasses', '')
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3289, 0, 2016, 1, 0, 0, 46232, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046232_prov_tbbs_column_close_descriptions_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
