SET SERVEROUTPUT ON

/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039733_prov_various_1043_pt2.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 10/06/2014 Jarrett Skov        drop an bad constraint if it exists
||============================================================================
*/

-- Provision fk_ta_line_item drop

begin
   execute immediate 'alter table TAX_ACCRUAL_BS drop constraint FK_TA_LINE_ITEM';
exception
   when others then
      DBMS_OUTPUT.PUT_LINE('Foreign Key FK_TA_LINE_ITEM does not exist on table TAX_ACCRUAL_BS');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1501, 0, 10, 4, 3, 0, 39733, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039733_prov_various_1043_pt2.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

