/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008019_sys.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/28/2011 Luis Kramarz   Point Release
||============================================================================
*/

-- Maint 8019

update POWERPLANT_COLUMNS
   set LONG_DESCRIPTION = 'C-Corp~tC-Corp/S-Corp~tS-Corp/LLC~tLLC/Partnership~tPartnership/Exempt Org~tExempt Org/Disregarded Entity~tDisregarded Entity/None~tNone'
 where COLUMN_NAME = 'company_type'
   and TABLE_NAME = 'company_setup';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (21, 0, 10, 3, 3, 0, 8019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008019_sys.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
