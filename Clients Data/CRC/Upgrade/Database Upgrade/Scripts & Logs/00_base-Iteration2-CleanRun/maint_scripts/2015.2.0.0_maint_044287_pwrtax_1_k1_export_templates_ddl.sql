/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044287_pwrtax_1_k1_export_templates_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.0.0 07/08/2015 Sarah Byers  	 Add template and formula concept to K1 export
||============================================================================
*/

-- TAX_K1_EXPORT_CONFIG_TEMPLATE
create table tax_k1_export_config_template (
k1_exp_config_temp_id number(22,0) not null,
user_id varchar2(18) null,
time_stamp date null,
description varchar2(50) not null,
long_description varchar2(254) null);

alter table tax_k1_export_config_template add (
constraint tax_k1_export_config_temp_pk primary key (k1_exp_config_temp_id) using index tablespace pwrplant_idx);

comment on table tax_k1_export_config_template is 'The Tax K1 Export Config Template table holds the templates set up for the final fields used in the summarization and saving of the MLP K1 export processing results.';
comment on column tax_k1_export_config_template.k1_exp_config_temp_id is 'System assigned identifier for a Tax K1 Export Configuration Template.';
comment on column tax_k1_export_config_template.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column tax_k1_export_config_template.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column tax_k1_export_config_template.description is 'Description of the Tax K1 Export Configuration Template.';
comment on column tax_k1_export_config_template.long_description is 'Long description of the Tax K1 Export Configuration Template.';

-- TAX_K1_EXPORT_CONFIG_SAVE
alter table tax_k1_export_config_save add (
k1_exp_config_save_id number(22,0),
k1_exp_config_temp_id number(22,0));

comment on column tax_k1_export_config_save.k1_exp_config_save_id is 'System assigned identifier for a row in the Tax K1 Export Configuration Save table.';
comment on column tax_k1_export_config_save.k1_exp_config_temp_id is 'System assigned identifier for a Tax K1 Export Configuration Template.';

-- drop the primary key, the next script will populate the new columns, and then third script will create the new primary key
alter table tax_k1_export_config_save drop primary key drop index;

alter table tax_k1_export_config_save
add constraint fk_tax_k1_export_config_save1
foreign key (k1_exp_config_temp_id)
references tax_k1_export_config_template (k1_exp_config_temp_id);

-- TAX_K1_EXPORT_CONFIG_FORMULA
create table tax_k1_export_config_formula (
k1_exp_config_form_id number(22,0) not null,
user_id varchar2(18) null,
time_stamp date null,
description varchar2(50) not null,
formula varchar2(2000) not null);

alter table tax_k1_export_config_formula add (
constraint tax_k1_export_config_form_pk primary key (k1_exp_config_form_id) using index tablespace pwrplant_idx);

comment on table tax_k1_export_config_formula is 'The Tax K1 Export Config Formula table holds formulas available to use in the MLP K1 export process.';
comment on column tax_k1_export_config_formula.k1_exp_config_form_id is 'System assigned identifier for a Tax K1 Export Configuration Formula.';
comment on column tax_k1_export_config_formula.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column tax_k1_export_config_formula.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column tax_k1_export_config_formula.description is 'Description of the Tax K1 Export Configuration Formula.';
comment on column tax_k1_export_config_formula.formula is 'The formula definition, e.g. "(field_1 + field_2) / field_3". Available operators for formula creation are "+", "-", "/", "*", "(", and ")".';

-- TAX_K1_EXPORT_CONFIG_AVAIL
alter table tax_k1_export_config_avail add (
k1_exp_config_form_id number(22,0) null);

comment on column tax_k1_export_config_avail.k1_exp_config_form_id is 'System assigned identifier for a Tax K1 Export Configuration Formula.';

alter table tax_k1_export_config_avail
add constraint fk_tax_k1_export_config_avail1
foreign key (k1_exp_config_form_id)
references tax_k1_export_config_formula (k1_exp_config_form_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2686, 0, 2015, 2, 0, 0, 044287, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044287_pwrtax_1_k1_export_templates_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;