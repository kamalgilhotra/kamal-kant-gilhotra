/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051214_lessor_01_add_dynamic_validations_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.3.0.1  06/07/2018 Crystal Yura     Add Lessor Dynamic Validation Types
||============================================================================
*/

insert into wo_validation_type (wo_validation_type_id, description, long_description, function, find_company, col1, col2, hard_edit)
values
(5003,'Lessor MLAs','Lessor MLAs Save','uo_lsr_lscntr_wksp','select -1 from dual','lease_id','revision',1);

insert into wo_validation_type (wo_validation_type_id, description, long_description, function, find_company, col1, col2, hard_edit)
values
(5004,'Lessor ILRs','Lessor ILRs Save','uo_lsr_ilrcntr_wksp_details','select company_id from lsr_ilr where ilr_id = <arg1>','ilr_id','revision',1);

update wo_validation_type set description = 'Lessee MLAs', long_description = 'Lessee MLAs Save', function = 'uo_ls_lscntr_wksp' where wo_validation_type_id = 5002;

update wo_validation_type set description = 'Lessee ILRs', long_description = 'Lessee ILRs Save'  where wo_validation_type_id = 5001;

update wo_validation_type set description = 'Lessee Assets', long_description = 'Lessee Assets Save'  where wo_validation_type_id = 5000;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6542, 0, 2017, 3, 0, 1, 51214, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051214_lessor_01_add_dynamic_validations_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;