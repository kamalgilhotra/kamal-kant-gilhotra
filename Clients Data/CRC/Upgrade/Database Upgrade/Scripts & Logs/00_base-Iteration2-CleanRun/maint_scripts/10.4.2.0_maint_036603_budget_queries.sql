/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036603_budget_queries.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/28/2014 Brandon Beck
||============================================================================
*/

update PP_QUERY_DW_COLS set COLUMNS = 'budget_item_number' where COLUMNS in ('budget_item', 'budget_number');
update PP_QUERY_DW_COLS set COLUMNS = 'budget_item_description' where COLUMNS in ('budget_id');
update PP_QUERY_DW_FILTER set COLUMNS = 'budget_item_number' where COLUMNS in ('budget_item', 'budget_number');
update PP_QUERY_DW_FILTER set COLUMNS = 'budget_item_description' where COLUMNS in ('budget_id');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1002, 0, 10, 4, 2, 0, 36603, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036603_budget_queries.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;