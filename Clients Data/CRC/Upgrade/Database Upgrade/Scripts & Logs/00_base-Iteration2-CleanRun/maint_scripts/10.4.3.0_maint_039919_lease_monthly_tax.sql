/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039919_lease_monthly_tax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/11/2014 Daniel Motter    Adding table and column comments
||============================================================================
*/

alter table LS_MONTHLY_TAX add VENDOR_ID number(22,0);

comment on column LS_MONTHLY_TAX.VENDOR_ID is 'An internal value linking to the vendor being paid the tax amount';

update LS_MONTHLY_TAX set VENDOR_ID = NVL(VENDOR_ID, -1);

alter table LS_MONTHLY_TAX drop primary key drop index;

alter table LS_MONTHLY_TAX
   add constraint PK_LS_MONTHLY_TAX
       primary key (LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID, ACCRUAL, VENDOR_ID)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1426, 0, 10, 4, 3, 0, 39919, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039919_lease_monthly_tax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
