/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_029760_budgetom_labor_post_recalc.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   05/18/2013 Marc Zawko     Point Release
||============================================================================
*/

alter table CR_BUDGET_TEMPLATES add LABOR_POST_RECALC number(22,0);

update CR_BUDGET_TEMPLATES set LABOR_POST_RECALC = 1 where LABOR_POST_RECALC is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (385, 0, 10, 4, 1, 0, 29760, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029760_budgetom_labor_post_recalc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;