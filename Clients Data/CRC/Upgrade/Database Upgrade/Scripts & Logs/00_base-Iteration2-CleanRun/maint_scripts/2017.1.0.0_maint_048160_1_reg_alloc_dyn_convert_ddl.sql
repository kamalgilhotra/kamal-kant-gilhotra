 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_048160_1_reg_alloc_dyn_convert_ddl.sql
 ||============================================================================
 || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 06/27/2017 Sarah Byers    Convert reg_acct_id to reg_alloc_acct_id for dynamic allocator basis
 ||============================================================================
 */ 
 
alter table reg_jur_allocator_dyn add (reg_alloc_acct_id number(22,0));

comment on column reg_jur_allocator_dyn.reg_alloc_acct_id is 'System assigned identifier of a regulatory allocation account.';

alter table reg_case_allocator_dyn add (reg_alloc_acct_id number(22,0));

comment on column reg_case_allocator_dyn.reg_alloc_acct_id is 'System assigned identifier of a regulatory allocation account.';

-- Add an index on the reg_alloc_acct_id column
CREATE INDEX reg_jur_alloc_dyn_aacct_id_idx ON reg_jur_allocator_dyn (Nvl(reg_alloc_acct_id,0));
CREATE INDEX reg_case_adyn_aacct_id_idx ON reg_case_allocator_dyn (Nvl(reg_alloc_acct_id,0));


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3547, 0, 2017, 1, 0, 0, 48160, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048160_1_reg_alloc_dyn_convert_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;