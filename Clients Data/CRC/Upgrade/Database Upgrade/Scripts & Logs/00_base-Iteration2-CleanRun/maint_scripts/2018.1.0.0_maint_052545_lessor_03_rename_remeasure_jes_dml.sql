/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052545_lessor_03_rename_remeasure_jes_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/16/2018 Anand R        PP-52545 Rename description for re measurement JEs
||============================================================================
*/

UPDATE JE_TRANS_TYPE
SET DESCRIPTION = '4070 - Lessor Receivable Remeasurement Debit'
WHERE TRANS_TYPE = 4070 ;

UPDATE JE_TRANS_TYPE
SET DESCRIPTION = '4071 - Lessor Receivable Remeasurement Credit'
WHERE TRANS_TYPE = 4071 ;

UPDATE JE_TRANS_TYPE
SET DESCRIPTION = '4072 - Lessor LT Receivable Remeasurement Debit'
WHERE TRANS_TYPE = 4072 ;

UPDATE JE_TRANS_TYPE
SET DESCRIPTION = '4073 - Lessor Unguaranteed Residual Remeasurement Debit'
WHERE TRANS_TYPE = 4073 ;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12002, 0, 2018, 1, 0, 0, 52545, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052545_lessor_03_rename_remeasure_jes_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;