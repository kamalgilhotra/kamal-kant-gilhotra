/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043159_pcm.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.1     03/16/2015 Mallory Smyth  Point Release
||============================================================================
*/
update wo_validation_type
set find_company= 'select case
         when lower(<arg2>) = ''budget_monthly_data'' then
          (select nvl(min(company_id), -1)
             from budget, wo_est_processing_temp
            where budget.budget_id = wo_est_processing_temp.work_order_id)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id =
                  wo_est_processing_temp.work_order_id)
       end case
  from dual'
where wo_validation_type_id = 81;
update wo_validation_type
set find_company = 'select case
         when lower(<arg2>) = ''budget_monthly_data'' then
          (select nvl(min(company_id), -1)
             from budget, wo_est_processing_temp
            where budget.budget_id = wo_est_processing_temp.work_order_id)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id =
                  wo_est_processing_temp.work_order_id)
       end case
  from dual'
where wo_validation_type_id = 82;
update wo_validation_type
set find_company = 'select case
         when lower(<arg2>) = ''bi'' then
          (select nvl(company_id, -1)
             from budget_version
            where budget_version_id = <arg3>)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id = wo_est_processing_temp.work_order_id)
       end case
  from dual'
where wo_validation_type_id = 83;
update wo_validation_type
set find_company = 'select case
         when lower(<arg2>) = ''bi'' then
          (select nvl(min(company_id), -1)
             from budget, wo_est_processing_temp
            where budget.budget_id = wo_est_processing_temp.work_order_id)
         else
          (select nvl(min(company_id), -1)
             from work_order_control, wo_est_processing_temp
            where work_order_control.work_order_id =
                  wo_est_processing_temp.work_order_id)
       end case
  from dual'
where wo_validation_type_id = 87;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2389, 0, 2015, 1, 0, 0, 43159, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043159_pcm_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;