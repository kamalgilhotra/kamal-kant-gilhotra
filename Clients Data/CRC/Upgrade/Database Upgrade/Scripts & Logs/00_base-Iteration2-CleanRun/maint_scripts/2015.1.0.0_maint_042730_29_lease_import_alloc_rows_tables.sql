/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_29_lease_import_alloc_rows_tables.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

-- Need an insert into CR Deriver Control
-- Dynamic import as possible

-- Need Asset Number, trans type, company, at least 15 fields (30 fields?), percentage

create table ls_import_alloc_rows (
import_run_id         NUMBER(22,0)   NOT NULL,
line_id               NUMBER(22,0)   NOT NULL,
time_stamp            DATE           NULL,
user_id               VARCHAR2(18)   NULL,
ls_asset_xlate        VARCHAR2(254)  NULL,
ls_asset_id           NUMBER(22,0)   NULL,
ilr_xlate             VARCHAR2(254)  NULL,
ilr_id                NUMBER(22,0)   NULL,
company_xlate         VARCHAR2(254)  NULL,
company_id            NUMBER(22,0)   NULL,
trans_type_xlate      VARCHAR2(254)  NULL,
trans_type            NUMBER(22,0)   NULL,
tax_local_xlate       VARCHAR2(254)  NULL,
tax_local_id          VARCHAR2(254)  NULL,
percent               NUMBER(22,8)   NULL,
element_1             VARCHAR2(254)  NULL,
element_2             VARCHAR2(254)  NULL,
element_3             VARCHAR2(254)  NULL,
element_4             VARCHAR2(254)  NULL,
element_5             VARCHAR2(254)  NULL,
element_6             VARCHAR2(254)  NULL,
element_7             VARCHAR2(254)  NULL,
element_8             VARCHAR2(254)  NULL,
element_9             VARCHAR2(254)  NULL,
element_10            VARCHAR2(254)  NULL,
element_11            VARCHAR2(254)  NULL,
element_12            VARCHAR2(254)  NULL,
element_13            VARCHAR2(254)  NULL,
element_14            VARCHAR2(254)  NULL,
element_15            VARCHAR2(254)  NULL,
element_16            VARCHAR2(254)  NULL,
element_17            VARCHAR2(254)  NULL,
element_18            VARCHAR2(254)  NULL,
element_19            VARCHAR2(254)  NULL,
element_20            VARCHAR2(254)  NULL,
element_21            VARCHAR2(254)  NULL,
element_22            VARCHAR2(254)  NULL,
element_23            VARCHAR2(254)  NULL,
element_24            VARCHAR2(254)  NULL,
element_25            VARCHAR2(254)  NULL,
element_26            VARCHAR2(254)  NULL,
element_27            VARCHAR2(254)  NULL,
element_28            VARCHAR2(254)  NULL,
element_29            VARCHAR2(254)  NULL,
element_30            VARCHAR2(254)  NULL,
loaded                NUMBER(22,0)   NULL,
is_modified           NUMBER(22,0)   NULL,
error_message         VARCHAR2(4000) NULL
)
/

ALTER TABLE ls_import_alloc_rows
  ADD CONSTRAINT ls_import_alloc_rows_pk PRIMARY KEY (
    import_run_id,
	line_id
  )
  USING INDEX
     TABLESPACE pwrplant_idx
/

ALTER TABLE ls_import_alloc_rows
  ADD CONSTRAINT ls_import_alloc_rows_fk FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  )
/

COMMENT ON TABLE ls_import_alloc_rows IS '(S) [06] The LS Import Alloc Rows table is an API table used to import leased asset JE allocation rows.';

COMMENT ON COLUMN ls_import_alloc_rows.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_alloc_rows.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_alloc_rows.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_alloc_rows.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_alloc_rows.ls_asset_xlate IS 'Translation field used to determine the internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows.ls_asset_id IS 'The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows.ilr_xlate IS 'Translation field for the ILR identifier';
COMMENT ON COLUMN ls_import_alloc_rows.ilr_id IS 'Identifier for which ILR is associated with the invoice';
COMMENT ON COLUMN ls_import_alloc_rows.company_xlate IS 'Translation field for determining the company.';
COMMENT ON COLUMN ls_import_alloc_rows.company_id IS 'The company.';
COMMENT ON COLUMN ls_import_alloc_rows.trans_type_xlate IS 'Translation field for determining the Trans Type.';
COMMENT ON COLUMN ls_import_alloc_rows.trans_type IS 'The internal trans type within PowerPlan.';
COMMENT ON COLUMN ls_import_alloc_rows.tax_local_xlate IS 'Translation field for determining the internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows.tax_local_id IS 'The internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows.percent IS 'Percentage split for the JE allocation row for given asset/trans type combination.';
COMMENT ON COLUMN ls_import_alloc_rows.element_1 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_2 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_3 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_4 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_5 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_6 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_7 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_8 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_9 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_10 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_11 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_12 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_13 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_14 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_15 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_16 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_17 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_18 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_19 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_20 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_21 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_22 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_23 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_24 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_25 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_26 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_27 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_28 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_29 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows.element_30 IS 'Element to be used for client CR Deriver Control record.';


create table ls_import_alloc_rows_archive (
import_run_id         NUMBER(22,0)   NOT NULL,
line_id               NUMBER(22,0)   NOT NULL,
time_stamp            DATE           NULL,
user_id               VARCHAR2(18)   NULL,
ls_asset_xlate        VARCHAR2(254)  NULL,
ls_asset_id           NUMBER(22,0)   NULL,
ilr_xlate             VARCHAR2(254)  NULL,
ilr_id                NUMBER(22,0)   NULL,
company_xlate         VARCHAR2(254)  NULL,
company_id            NUMBER(22,0)   NULL,
trans_type_xlate      VARCHAR2(254)  NULL,
trans_type            NUMBER(22,0)   NULL,
tax_local_xlate       VARCHAR2(254)  NULL,
tax_local_id          VARCHAR2(254)  NULL,
percent               NUMBER(22,8)   NULL,
element_1             VARCHAR2(254)  NULL,
element_2             VARCHAR2(254)  NULL,
element_3             VARCHAR2(254)  NULL,
element_4             VARCHAR2(254)  NULL,
element_5             VARCHAR2(254)  NULL,
element_6             VARCHAR2(254)  NULL,
element_7             VARCHAR2(254)  NULL,
element_8             VARCHAR2(254)  NULL,
element_9             VARCHAR2(254)  NULL,
element_10            VARCHAR2(254)  NULL,
element_11            VARCHAR2(254)  NULL,
element_12            VARCHAR2(254)  NULL,
element_13            VARCHAR2(254)  NULL,
element_14            VARCHAR2(254)  NULL,
element_15            VARCHAR2(254)  NULL,
element_16            VARCHAR2(254)  NULL,
element_17            VARCHAR2(254)  NULL,
element_18            VARCHAR2(254)  NULL,
element_19            VARCHAR2(254)  NULL,
element_20            VARCHAR2(254)  NULL,
element_21            VARCHAR2(254)  NULL,
element_22            VARCHAR2(254)  NULL,
element_23            VARCHAR2(254)  NULL,
element_24            VARCHAR2(254)  NULL,
element_25            VARCHAR2(254)  NULL,
element_26            VARCHAR2(254)  NULL,
element_27            VARCHAR2(254)  NULL,
element_28            VARCHAR2(254)  NULL,
element_29            VARCHAR2(254)  NULL,
element_30            VARCHAR2(254)  NULL,
loaded                NUMBER(22,0)   NULL,
is_modified           NUMBER(22,0)   NULL,
error_message         VARCHAR2(4000) NULL
)
/

ALTER TABLE ls_import_alloc_rows_archive
  ADD CONSTRAINT ls_import_alloc_rows_archive_p PRIMARY KEY (
    import_run_id,
	line_id
  )
  USING INDEX
     TABLESPACE pwrplant_idx
/

ALTER TABLE ls_import_alloc_rows_archive
  ADD CONSTRAINT ls_import_alloc_rows_archive_f FOREIGN KEY (
    import_run_id
  ) REFERENCES pp_import_run (
    import_run_id
  )
/

COMMENT ON TABLE ls_import_alloc_rows_archive IS '(S) [06] The LS Import Alloc Rows Archive table is an API table used to archive the imported leased asset JE allocation rows.';

COMMENT ON COLUMN ls_import_alloc_rows_archive.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.line_id IS 'System-assigned line number for this import run.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.user_id IS 'Standard system-assigned user id used for audit purposes.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.ls_asset_xlate IS 'Translation field used to determine the internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.ls_asset_id IS 'The internal leased asset id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.ilr_xlate IS 'Translation field for the ILR identifier';
COMMENT ON COLUMN ls_import_alloc_rows_archive.ilr_id IS 'Identifier for which ILR is associated with the invoice';
COMMENT ON COLUMN ls_import_alloc_rows_archive.company_xlate IS 'Translation field for determining the company.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.company_id IS 'The company.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.trans_type_xlate IS 'Translation field for determining the Trans Type.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.trans_type IS 'The internal trans type within PowerPlan.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.tax_local_xlate IS 'Translation field for determining the internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.tax_local_id IS 'The internal local tax id within PowerPlant.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.percent IS 'Percentage split for the JE allocation row for given asset/trans type combination.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_1 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_2 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_3 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_4 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_5 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_6 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_7 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_8 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_9 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_10 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_11 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_12 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_13 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_14 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_15 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_16 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_17 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_18 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_19 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_20 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_21 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_22 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_23 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_24 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_25 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_26 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_27 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_28 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_29 IS 'Element to be used for client CR Deriver Control record.';
COMMENT ON COLUMN ls_import_alloc_rows_archive.element_30 IS 'Element to be used for client CR Deriver Control record.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2421, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_29_lease_import_alloc_rows_tables.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;