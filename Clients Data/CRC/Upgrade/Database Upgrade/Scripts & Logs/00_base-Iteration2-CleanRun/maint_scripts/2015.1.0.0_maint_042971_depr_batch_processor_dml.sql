/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042971_depr_batch_processor_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 02/20/2015 Charlie Shilling create individual process_ids for depr calcs
||============================================================================
*/

begin
	--it is expected that there is already one process in pp_processes for ssp_depr_calc
	--	update the description on that process.
	UPDATE pp_processes
	SET description = 'Depr Calc-Launcher',
		long_description = 'Process to launch the depreciation calculations for CPR Month End',
		version = '2015.1.0.0',
		allow_concurrent = 0
	WHERE lower(trim(executable_file)) = 'ssp_depr_calc.exe';

	--Now we need to create new PP_PROCESSES records for each individual version of the calc
	--Actuals Group
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent, cpr_month_end_flag)
	SELECT nvl(max(process_id),0)+1, 'Depr Calc-Group', 'Depreciation Calculation for Group Depreciation', 'ssp_depr_calc.exe GROUP', '2015.1.0.0', 0, 1
	FROM pp_processes;

	--loop over subledgers to create processes for actuals individuals
	for rec in (select distinct subledger_type_id, subledger_name FROM subledger_control WHERE depreciation_indicator = 3) loop
		INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent, cpr_month_end_flag)
		SELECT nvl(max(process_id),0)+1, SubStr('Depr Calc-' || rec.subledger_name, 1, 35), 'Depreciation Calculation for individual depreciation subledger ' || rec.subledger_name, 'ssp_depr_calc.exe IND:' || to_char(rec.subledger_type_id), '2015.1.0.0', 0, 1
		FROM pp_processes;
	end loop;

	--Forecast Group
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent, cpr_month_end_flag)
	SELECT nvl(max(process_id),0)+1, 'Depr Calc-Forecast Group', 'Depreciation Calculation for Forecast Group Depreciation', 'ssp_depr_calc.exe FCST:GRP', '2015.1.0.0', 0, 1
	FROM pp_processes;

	--Forecast Individual
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent, cpr_month_end_flag)
	SELECT nvl(max(process_id),0)+1, 'Depr Calc-Forecast Individual', 'Depreciation Calculation for Forecast Individual Depreciation', 'ssp_depr_calc.exe FCST:IND', '2015.1.0.0', 0, 1
	FROM pp_processes;

	--Forecast Incremental
	INSERT INTO pp_processes (process_id, description, long_description, executable_file, version, allow_concurrent, cpr_month_end_flag)
	SELECT nvl(max(process_id),0)+1, 'Depr Calc-Forecast Incremental', 'Depreciation Calculation for Forecast Incremental Depreciation', 'ssp_depr_calc.exe FCST:INCR', '2015.1.0.0', 0, 1
	FROM pp_processes;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2320, 0, 2015, 1, 0, 0, 042971, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042971_depr_batch_processor_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;