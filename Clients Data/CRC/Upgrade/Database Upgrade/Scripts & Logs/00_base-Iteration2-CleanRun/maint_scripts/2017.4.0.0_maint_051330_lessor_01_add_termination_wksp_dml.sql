/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051330_lessor_01_add_termination_wksp_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  05/29/2018 Anand R          Add new workspace for Termination review
||============================================================================
*/

insert into ppbase_workspace 
   (module, workspace_identifier, label, workspace_uo_name, minihelp, object_type_id)
values 
   ('LESSOR', 'control_terminations', 'Terminations', 'uo_lsr_mecntr_terminations', null, 1);
   

insert into ppbase_menu_items 
   (module, menu_identifier, menu_level, item_order, label, minihelp, parent_menu_identifier, workspace_identifier, enable_yn)
values 
   ('LESSOR', 'control_terminations', 2, 4, 'Terminations', null, 'menu_wksp_control', 'control_terminations', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6022, 0, 2017, 4, 0, 0, 51330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051330_lessor_01_add_termination_wksp_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;