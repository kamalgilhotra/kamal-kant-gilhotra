/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050735_lessee_07_query_fcst_deprec_dml.sql
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2017.4.0.0 06/06/2018 K Powers  Add multi-currency fields to Lease Forecast 
||                                 queries
||============================================================================
*/

DECLARE
  V_ID NUMBER;
BEGIN

  SELECT NVL(MAX(ID), 0) + 1 INTO V_ID FROM PP_ANY_QUERY_CRITERIA;

  delete from pp_any_query_criteria_fields
   where id in (select id
                  from pp_any_query_criteria
                 where description = 'Forecast Lease Depreciation');

  delete from pp_any_query_criteria
   where description = 'Forecast Lease Depreciation';

  INSERT INTO PP_ANY_QUERY_CRITERIA
    (ID,
     SOURCE_ID,
     CRITERIA_FIELD,
     TABLE_NAME,
     DESCRIPTION,
     FEEDER_FIELD,
     SUBSYSTEM,
     QUERY_TYPE)
  VALUES
    (V_ID,
     1001,
     'none',
     'Dynamic View',
     'Forecast Lease Depreciation',
     'none',
     'lessee',
     'user');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'forecast_version',
     1,
     0,
     1,
     '',
     'Forecast Version',
     300,
     'description',
     'ls_forecast_version',
     'VARCHAR2',
     0,
     'description',
     1,
     2,
     'description');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'sob',
     2,
     0,
     1,
     '',
     'Set Of Books',
     300,
     'description',
     '(select * from set_of_books where set_of_books_id in (select set_of_books_id from ls_fasb_cap_type_sob_map))',
     'VARCHAR2',
     0,
     'description',
     0,
     0,
     'description');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'company',
     3,
     0,
     1,
     '',
     'Company',
     300,
     'description',
     '(select * from company where is_lease_company = 1)',
     'VARCHAR2',
     0,
     'description',
     0,
     0,
     'description');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'lease_number',
     4,
     0,
     1,
     '',
     'Lease Number',
     300,
     'lease_number',
     'ls_lease',
     'VARCHAR2',
     0,
     'lease_number',
     0,
     0,
     'lease_number');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'ilr_number',
     5,
     0,
     1,
     '',
     'ILR Number',
     300,
     'ilr_number',
     'ls_ilr',
     'VARCHAR2',
     0,
     'ilr_number',
     0,
     0,
     'ilr_number');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'leased_asset_number',
     6,
     0,
     1,
     '',
     'Leased Asset Number',
     300,
     'leased_asset_number',
     'ls_asset',
     'VARCHAR2',
     0,
     'leased_asset_number',
     0,
     0,
     'leased_asset_number');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'month',
     7,
     0,
     1,
     '',
     'Month',
     300,
     'month_number',
     'pp_calendar',
     'number',
     0,
     'month_number',
     0,
     0,
     'month_number');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'depr_expense',
     8,
     1,
     1,
     '',
     'Depr Expense',
     300,
     null,
     null,
     'NUMBER',
     0,
     null,
     0,
     0,
     null);

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'begin_reserve',
     9,
     1,
     1,
     '',
     'Begin Reserve',
     300,
     null,
     null,
     'NUMBER',
     0,
     null,
     0,
     0,
     null);

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'end_reserve',
     10,
     1,
     1,
     '',
     'End Reserve',
     300,
     null,
     null,
     'NUMBER',
     0,
     null,
     0,
     0,
     null);

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'end_capital_cost',
     11,
     1,
     1,
     '',
     'End Capital Cost',
     300,
     null,
     null,
     'NUMBER',
     0,
     null,
     0,
     0,
     null);

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (V_ID,
     'rou',
     12,
     1,
     1,
     '',
     'ROU',
     300,
     null,
     null,
     'NUMBER',
     0,
     null,
     0,
     0,
     null);
     
   INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     TIME_STAMP,
     USER_ID,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL)
  VALUES
    (v_id,
     'iso_code',
     73,
     to_date(sysdate, 'yyyy-mm-dd hh24:mi:ss'),
     'PWRPLANT',
     0,
     1,
     null,
     'Currency',
     300,
     'description',
     'currency',
     'VARCHAR2',
     0,
     'iso_code',
     null,
     null,
     'iso_code');

  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     TIME_STAMP,
     USER_ID,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL,
     HIDE_FROM_RESULTS,
     HIDE_FROM_FILTERS)
  VALUES
    (v_id,
     'currency_display_symbol',
     72,
     to_date(sysdate, 'yyyy-mm-dd hh24:mi:ss'),
     'PWRPLANT',
     0,
     0,
     null,
     'Currency Symbol',
     300,
     null,
     null,
     'VARCHAR2',
     0,
     null,
     null,
     null,
     'currency_display_symbol',
     1,
     1);
     
  INSERT INTO PP_ANY_QUERY_CRITERIA_FIELDS
    (ID,
     DETAIL_FIELD,
     COLUMN_ORDER,
     TIME_STAMP,
     USER_ID,
     AMOUNT_FIELD,
     INCLUDE_IN_SELECT_CRITERIA,
     DEFAULT_VALUE,
     COLUMN_HEADER,
     COLUMN_WIDTH,
     DISPLAY_FIELD,
     DISPLAY_TABLE,
     COLUMN_TYPE,
     QUANTITY_FIELD,
     DATA_FIELD,
     REQUIRED_FILTER,
     REQUIRED_ONE_MULT,
     SORT_COL,
     HIDE_FROM_RESULTS,
     HIDE_FROM_FILTERS)
  VALUES
    (v_id,
     'currency_type',
     71,
     to_date(sysdate, 'yyyy-mm-dd hh24:mi:ss'),
     'PWRPLANT',
     0,
     1,
     null,
     'Currency Type',
     300,
     'description',
     'ls_lease_currency_type',
     'VARCHAR2',
     0,
     'description',
     1,
     2,
     'description',
     0,
     0);
     
  update pp_any_query_criteria
     set SQL = 'sELECT fct.description as forecast_version, la.leased_asset_number, ilr.ilr_number, mla.lease_number, cs.description as company, sob.description as sob,
                to_number(to_char(ldf.MONTH,''yyyymm'')) as month, ldf.depr_expense, ldf.begin_reserve, ldf.end_reserve, sch.end_capital_cost, 
                (sch.end_capital_cost-ldf.end_reserve) as ROU,llct.description as CURRENCY_TYPE,iso_code,currency_display_symbol 
                FROM ls_lease mla,
                     ls_ilr ilr,
                     ls_asset la,
                     ls_depr_forecast ldf,
                     company cs,
                     set_of_books sob,
                     v_ls_asset_schedule_fx_vw sch,
                     ls_forecast_version fct,
                     ls_lease_currency_type llct 
                WHERE la.ilr_id = ilr.ilr_id
                AND   ilr.lease_id = mla.lease_id
                AND   fct.revision = ldf.revision
                AND   abs(fct.set_of_books_id) = abs(sob.set_of_books_id)
                AND   la.ls_asset_id = ldf.ls_asset_id
                AND   la.company_id = cs.company_id 
                and   llct.ls_currency_type_id = sch.ls_cur_type 
                and   ldf.set_of_books_id = sob.set_of_books_id
                AND   la.ls_asset_status_id = 3
                and   sch.ls_asset_id = la.ls_asset_id
                and   sch.revision = fct.revision
                and   sch.month = ldf.month
                and   sch.set_of_books_id = sob.set_of_books_id
                ORDER BY la.leased_asset_number, ldf.MONTH, sob.description',is_multicurrency = 1 
   where description = 'Forecast Lease Depreciation';

END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6529, 0, 2017, 4, 0, 0, 50735, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050735_lessee_07_query_fcst_deprec_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
