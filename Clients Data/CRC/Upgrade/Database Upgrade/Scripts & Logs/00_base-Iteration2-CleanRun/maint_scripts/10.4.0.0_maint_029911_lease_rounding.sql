/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_029911_lease_rounding.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   05/01/2013 Travis Nemes      Point Release
||============================================================================
*/

drop table LS_JE_ROUNDING;

CREATE TABLE LS_JE_ROUNDING
(
 LS_ASSET_ID         number(22,0) not null,
 GL_POSTING_MONTHNUM number(22,0) not null,
 DC_INDICATOR        number(22,0) not null,
 TARGET_AMOUNT       number(22,2),
 ALLOC_AMOUNT        number(22,2),
 DELTA               number(22,2),
 DIST_LINE_ID        number(22,0)
);

create index IX_LSJE_ROUND_ASSET
   on LS_JE_ROUNDING (LS_ASSET_ID)
      tablespace PWRPLANT_IDX;

create index IX_LSJE_ROUND_DC_MONTH
   on LS_JE_ROUNDING (GL_POSTING_MONTHNUM, DC_INDICATOR)
      tablespace PWRPLANT_IDX;

create index IX_LSJE_ROUND_DIST_LINE
   on LS_JE_ROUNDING (DIST_LINE_ID)
      tablespace PWRPLANT_IDX;

alter table LS_JE_ROUNDING
   add constraint PK_LS_ROUND
       primary key (LS_ASSET_ID, GL_POSTING_MONTHNUM, DC_INDICATOR)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (374, 0, 10, 4, 0, 0, 29911, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029911_lease_rounding.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
