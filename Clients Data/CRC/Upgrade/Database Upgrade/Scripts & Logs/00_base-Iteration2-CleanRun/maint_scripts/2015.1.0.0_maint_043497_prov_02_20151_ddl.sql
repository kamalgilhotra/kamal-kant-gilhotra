

 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_043497_prov_02_20151_ddl.sql
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.1 04/08/2015 	Jarrett Skov   Adding a primary key to tax_accrual_rep_cons_rows to prevent line duplication issues
 ||============================================================================
 */ 


-- PK for rep_cons_rows

ALTER TABLE tax_accrual_rep_cons_rows
  ADD CONSTRAINT ta_rep_cons_rows_pk PRIMARY KEY (
    rep_cons_type_id,
    row_type,
    row_id
  )
using index tablespace PWRPLANT_IDX;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2484, 0, 2015, 1, 0, 0, 43497, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043497_prov_02_20151_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
