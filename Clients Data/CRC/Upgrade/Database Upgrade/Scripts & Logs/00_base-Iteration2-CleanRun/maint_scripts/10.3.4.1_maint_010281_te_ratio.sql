/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010281_te_ratio.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.1   05/11/2012 Sunjin Cone    Point Release
||============================================================================
*/

-- PL/SQL block to raise error and stop processing if any of the statements fail.
begin
   execute immediate 'alter table REPAIR_WORK_ORDER_SEGMENTS add ADD_RETIRE_RATIO_TEMP number(22, 8)';
   execute immediate 'update REPAIR_WORK_ORDER_SEGMENTS set ADD_RETIRE_RATIO_TEMP = ADD_RETIRE_RATIO';
   execute immediate 'commit';

   execute immediate 'update REPAIR_WORK_ORDER_SEGMENTS set ADD_RETIRE_RATIO = null';
   execute immediate 'commit';

   execute immediate 'alter table REPAIR_WORK_ORDER_SEGMENTS modify ADD_RETIRE_RATIO number(22, 8)';
   execute immediate 'update REPAIR_WORK_ORDER_SEGMENTS set ADD_RETIRE_RATIO = ADD_RETIRE_RATIO_TEMP';

   execute immediate 'alter table REPAIR_WORK_ORDER_SEGMENTS drop column ADD_RETIRE_RATIO_TEMP';

   execute immediate 'alter table REPAIR_WO_SEG_REPORTING add ADD_RETIRE_RATIO_TEMP number(22, 8)';
   execute immediate 'update REPAIR_WO_SEG_REPORTING set ADD_RETIRE_RATIO_TEMP = ADD_RETIRE_RATIO';
   execute immediate 'commit';

   execute immediate 'update REPAIR_WO_SEG_REPORTING set ADD_RETIRE_RATIO = null';
   execute immediate 'commit';

   execute immediate 'alter table REPAIR_WO_SEG_REPORTING MODIFY ADD_RETIRE_RATIO number(22, 8)';
   execute immediate 'update REPAIR_WO_SEG_REPORTING set ADD_RETIRE_RATIO = ADD_RETIRE_RATIO_TEMP';
   execute immediate 'commit';

   execute immediate 'alter table REPAIR_WO_SEG_REPORTING drop column ADD_RETIRE_RATIO_TEMP';
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (137, 0, 10, 3, 4, 1, 10281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.1_maint_010281_te_ratio.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
