/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048808_lessor_01_add_payment_month_freq_col_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/21/2017 Charlie Shilling add column to database to avoid having to manually calc same values in many places in code/SQL
||============================================================================
*/
DECLARE
   PROCEDURE add_column(v_table_name VARCHAR2,
                        v_col_name   VARCHAR2,
                        v_datatype   VARCHAR2,
                        v_comment    VARCHAR2) IS
   BEGIN
      BEGIN
         EXECUTE IMMEDIATE 'alter table ' || v_table_name || ' add ' || v_col_name || ' ' ||
                           v_datatype;
         Dbms_Output.put_line('Sucessfully added column ' || v_col_name || ' to table ' ||
                              v_table_name || '.');
      EXCEPTION
         WHEN OTHERS THEN
            IF SQLCODE = -1430 THEN
               --1430 is "column being added already exists in table", so we are good here
               Dbms_Output.put_line('Column ' || v_col_name || ' already exists on table ' ||
                                    v_table_name || '. No action necessasry.');
            ELSE
               Raise_Application_Error(-20001,
                                       'Could not add column ' || v_col_name || ' to ' ||
                                       v_table_name || '. SQL Error: ' || SQLERRM);
            END IF;
      END;

      BEGIN
         EXECUTE IMMEDIATE 'Comment on column ' || v_table_name || '.' || v_col_name || ' is ''' ||
                           v_comment || '''';
         Dbms_Output.put_line('	Sucessfully added the comment to column ' || v_col_name ||
                              ' to table ' || v_table_name || '.');
      EXCEPTION
         WHEN OTHERS THEN
            Raise_Application_Error(-20002,
                                    'Could not add comment to column ' || v_col_name || ' to ' ||
                                    v_table_name || '. SQL Error: ' || SQLERRM);
      END;
   END add_column;
BEGIN
	add_column('ls_payment_freq',
              'payment_month_freq',
              'number(22,0)',
              'The number of months between payments. The column helps in the schedule calculations.');
END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3735, 0, 2017, 1, 0, 0, 48808, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048808_lessor_01_add_payment_month_freq_col_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
