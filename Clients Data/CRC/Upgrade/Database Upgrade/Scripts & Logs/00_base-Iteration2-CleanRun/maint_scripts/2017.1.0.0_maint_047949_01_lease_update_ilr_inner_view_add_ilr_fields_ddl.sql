/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047949_01_lease_update_ilr_inner_view_add_ilr_fields_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/11/2017 Jared Watkins    update ILR schedule inner view to add more fields in the retrieve
||============================================================================
*/
CREATE OR REPLACE VIEW V_MULTICURRENCY_LIS_INNER AS
with depr AS (
  SELECT  /*+ materialize */ ilr_id, revision, set_of_books_id,
         month, sum(depr_expense) as depr_expense,
         sum(begin_reserve) as begin_reserve,
         sum(end_reserve) as end_reserve,
         sum(depr_exp_alloc_adjust) as depr_exp_alloc_adjust
  FROM ls_asset la, ls_depr_forecast ldf
  WHERE la.ls_asset_id = ldf.ls_asset_id
  GROUP BY ilr_id, revision, set_of_books_id, month
)
select /*+ NO_MERGE */
  lis.ilr_id,
  ilr.ilr_number,
  ilr.current_revision,
  lis.revision,
  lis.set_of_books_id,
  lis.MONTH,
  lis.beg_capital_cost,
  lis.end_capital_cost,
  lis.beg_obligation,
  lis.end_obligation,
  lis.beg_lt_obligation,
  lis.end_lt_obligation,
  lis.interest_accrual,
  lis.principal_accrual,
  lis.interest_paid,
  lis.principal_paid,
  lis.executory_accrual1,
  lis.executory_accrual2,
  lis.executory_accrual3,
  lis.executory_accrual4,
  lis.executory_accrual5,
  lis.executory_accrual6,
  lis.executory_accrual7,
  lis.executory_accrual8,
  lis.executory_accrual9,
  lis.executory_accrual10,
  lis.executory_paid1,
  lis.executory_paid2,
  lis.executory_paid3,
  lis.executory_paid4,
  lis.executory_paid5,
  lis.executory_paid6,
  lis.executory_paid7,
  lis.executory_paid8,
  lis.executory_paid9,
  lis.executory_paid10,
  lis.contingent_accrual1,
  lis.contingent_accrual2,
  lis.contingent_accrual3,
  lis.contingent_accrual4,
  lis.contingent_accrual5,
  lis.contingent_accrual6,
  lis.contingent_accrual7,
  lis.contingent_accrual8,
  lis.contingent_accrual9,
  lis.contingent_accrual10,
  lis.contingent_paid1,
  lis.contingent_paid2,
  lis.contingent_paid3,
  lis.contingent_paid4,
  lis.contingent_paid5,
  lis.contingent_paid6,
  lis.contingent_paid7,
  lis.contingent_paid8,
  lis.contingent_paid9,
  lis.contingent_paid10,
  lis.current_lease_cost,
  depr.depr_expense,
  depr.begin_reserve,
  depr.end_reserve,
  depr.depr_exp_alloc_adjust,
  ilr.lease_id,
  ilr.company_id,
  opt.in_service_exchange_rate,
  opt.purchase_option_amt,
  opt.termination_amt,
  liasob.net_present_value,
  liasob.capital_cost,
  lis.is_om
FROM ls_ilr_schedule lis
INNER JOIN ls_ilr_options opt
  ON lis.ilr_id = opt.ilr_id
  AND lis.revision = opt.revision
INNER JOIN ls_ilr_amounts_set_of_books liasob
  ON lis.ilr_id = liasob.ilr_id
  AND lis.revision = liasob.revision
  AND lis.set_of_books_id = liasob.set_of_books_id
INNER JOIN ls_ilr ilr
  ON lis.ilr_id = ilr.ilr_id
LEFT OUTER JOIN depr
  ON lis.ilr_id = depr.ilr_id
  AND lis.revision = depr.revision
  AND lis.set_of_books_id = depr.set_of_books_id
  AND lis.month = depr.month
INNER JOIN ls_lease lease
  ON ilr.lease_id = lease.lease_id;
  
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3491, 0, 2017, 1, 0, 0, 47949, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047949_01_lease_update_ilr_inner_view_add_ilr_fields_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
