/*
||============================================================================
|| Application: PowerPlan, Inc.
|| Object Name: maint_044641_reg_tax_control_desc_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0   08/18/2015 Alex P.        Correct a typo in reg_tax_control
||============================================================================
*/

update reg_tax_control
set description = 'FAS109 Incremental Activity State'
where description = 'FAS109 Incremental Acitivity State';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2796, 0, 2015, 2, 0, 0, 044641, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044641_reg_tax_control_desc_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;