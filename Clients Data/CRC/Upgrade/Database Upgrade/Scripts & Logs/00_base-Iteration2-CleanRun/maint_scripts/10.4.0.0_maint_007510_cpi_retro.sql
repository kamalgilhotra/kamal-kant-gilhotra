/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007510_cpi_retro.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   02/12/2013 Sunjin Cone    Point Release
||============================================================================
*/

create table CPI_RETRO_BATCH_CONTROL
(
 BATCH_ID number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 COMPANY_ID         number(22,0),
 START_MONTH_NUMBER number(22,0),
 END_MONTH_NUMBER   number(22,0),
 CALC_DATE          date,
 POST_DATE          date
);

alter table CPI_RETRO_BATCH_CONTROL
   add constraint PK_CPI_BATCH_CONTROL
       primary key (BATCH_ID)
       using index tablespace PWRPLANT_IDX;

alter table CPI_RETRO_BATCH_CONTROL
   add constraint FK_CPI_BATCH_CONTROL
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

create table CPI_RETRO_BASE_ADJUST
(
 BATCH_ID          number(22,0) not null,
 COMPANY_ID        number(22,0) not null,
 EFFECTIVE_MONTH   date not null,
 WORK_ORDER_NUMBER varchar2(35) not null,
 TIME_STAMP        date,
 USER_ID           varchar2(18),
 CPI_BASE_ADJUST   number(22,2),
 WORK_ORDER_ID     number(22,0),
 NOTES             varchar2(2000)
);

alter table CPI_RETRO_BASE_ADJUST
   add constraint PK_CPI_RETRO_BASE_ADJUST
       primary key (BATCH_ID, COMPANY_ID, WORK_ORDER_NUMBER, EFFECTIVE_MONTH)
       using index tablespace PWRPLANT_IDX;

alter table CPI_RETRO_BASE_ADJUST
   add constraint R_CPI_RETRO_BASE_ADJUST1
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;

alter table CPI_RETRO_BASE_ADJUST
   add constraint R_CPI_RETRO_BASE_ADJUST2
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

alter table CPI_RETRO_BASE_ADJUST
   add constraint R_CPI_RETRO_BASE_ADJUST3
       foreign key (BATCH_ID)
       references CPI_RETRO_BATCH_CONTROL;

create table CPI_RETRO_106_OOB_WO
(
 WORK_ORDER_ID     number(22,0),
 COMPANY_ID        number(22,0),
 BATCH_ID          number(22,0),
 WORK_ORDER_NUMBER varchar2(35),
 CWIP_AMOUNT       number(22,2),
 CPR_AMOUNT        number(22,2),
 TIME_STAMP        date,
 USER_ID           varchar2(18)
);

ALTER table CPI_RETRO_106_OOB_WO
   add constraint PK_CPI_RETRO_106_OOB_WO
       primary key (BATCH_ID, COMPANY_ID, WORK_ORDER_ID)
       using index tablespace PWRPLANT_IDX;

ALTER table CPI_RETRO_106_OOB_WO
   add constraint R_CPI_RETRO_106_OOB_WO1
       foreign key (BATCH_ID)
       references CPI_RETRO_BATCH_CONTROL;

ALTER table CPI_RETRO_106_OOB_WO
   add constraint R_CPI_RETRO_106_OOB_WO2
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

ALTER table CPI_RETRO_106_OOB_WO
   add constraint R_CPI_RETRO_106_OOB_WO3
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;

CREATE GLOBAL TEMPORARY table CPI_RETRO_WO_LIST_TEMP
(
 WORK_ORDER_ID            number(22,0),
 COMPANY_ID               number(22,0),
 AFUDC_TYPE_ID            number(22,0),
 CPI_ELIG_AMOUNT          number(22,2),
 AFUDC_START_DATE         date,
 AFUDC_STOP_DATE          date,
 IN_SERVICE_DATE          date,
 WO_STATUS_ID             number(22,0),
 DEPARTMENT_ID            number(22,0),
 BUS_SEGMENT_ID           number(22,0),
 CWIP_GL_ACCOUNT          number(22,0),
 ELIGIBLE_FOR_CPI         number(22,0),
 ORIGINAL_IN_SERVICE_DATE date,
 ORIG_ISD_MONTH_NUMBER    number(22,0),
 EST_START_DATE           date,
 EST_COMPLETE_DATE        date,
 EST_IN_SERVICE_DATE      date,
 FUNDING_WO_ID            number(22,0),
 CURRENT_REVISION         number(22,0),
 CPI_COST_ELEMENT         number(22,0),
 MIN_EST_AMT_CPI          number(22,2),
 MIN_EST_MOS_CPI          number(22,0),
 ALLOW_NEG_CPI            number(22,0),
 PERCENT_EST_FOR_CPI      number(22,8),
 IGNORE_CURRENT_MONTH_CPI number(22,2),
 IN_SERVICE_OPTION        varchar2(35),
 EST_REQUIRED             varchar2(35),
 CPI_SKIP_TRIGGER         number(22,0),
 IDLE_CPI_MONTHS          number(22,0),
 IDLE_EXP_TYPE_ADDITION   number(22,0),
 CURRENT_MONTH_ONLY       number(22,0),
 CPI_DE_MINIMIS_YN        number(1,0) default 0,
 CPI_DE_MINIMIS_WO_OR_FP  varchar2(35),
 CPI_DE_MINIMIS_AMOUNT    number(22,2),
 CPI_DE_MINIMIS_MAX_DAYS  number(22,0),
 CPI_CHARGE_TYPE          number(22,0),
 MAX_REVISION             number(22,0),
 HAS_PRIOR_CPI            number(22,0),
 MIN_CHARGE_MONUM         number(22,0),
 CPI_STATUS_ID            number(22,0),
 WO_EST_AMOUNT            number(22,2),
 CPI_ACT_AMOUNT           number(22,2),
 CPI_DEMIN_EST_DAYS       number(22,0),
 CPI_DEMIN_BUD_AMOUNT     number(22,2),
 STOP_MONTH_RATE_ADJ      number(22,8),
 CPI_RATE                 number(22,8),
 CPI_PAST_ISD_AMOUNT      number(22,2),
 CPI_ISD_AMOUNT           number(22,2),
 CPI_BASE_ADJ             number(22,2),
 CALC_MISSING             number(22,0),
 LAST_COMP_CPI_MN         number(22,0)
) on commit preserve rows;


create table CPI_RETRO_CALC
(
 WORK_ORDER_ID          number(22,0) not null,
 MONTH                  date not null,
 BATCH_ID               number(22,0),
 COMPANY_ID             number(22,0),
 BEG_CPI_BASE           number(22,2),
 CPI_BASE               number(22,2),
 HALF_CURRENT_MONTH_CPI number(22,2),
 CPI                    number(22,2),
 CPI_COMPOUND_AMT       number(22,2),
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 INPUT_CPI              number(22,2),
 CPI_BASE_USED_IN_CALC  number(22,2),
 INPUT_CPI_RATIO        number(22,12),
 CPI_STATUS_ID          number(22,0),
 CPI_RATE               number(22,8),
 CPI_JE                 number(22,2),
 CPI_ISD                number(22,2),
 CM_CHARGES_IN_CALC_CPI number(22,2),
 CPI_BASE_ADJUST        number(22,2)
);

ALTER table CPI_RETRO_CALC
   add constraint PK_CPI_RETRO_CALC
       primary key (BATCH_ID, COMPANY_ID, WORK_ORDER_ID, MONTH)
       using index tablespace PWRPLANT_IDX;

ALTER table CPI_RETRO_CALC
   add constraint R_CPI_RETRO_CALC1
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;

ALTER table CPI_RETRO_CALC
   add constraint R_CPI_RETRO_CALC2
       foreign key (CPI_STATUS_ID)
       references AFUDC_STATUS;

ALTER table CPI_RETRO_CALC
   add constraint R_CPI_RETRO_CALC3
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

ALTER table CPI_RETRO_CALC
   add constraint R_CPI_RETRO_CALC4
       foreign key (BATCH_ID)
       references CPI_RETRO_BATCH_CONTROL;


create table CPI_RETRO_COMPARE
(
 WORK_ORDER_ID            number(22,0),
 COMPANY_ID               number(22,0),
 BATCH_ID                 number(22,0),
 MONTH_NUMBER             number(22,0),
 WORK_ORDER_NUMBER        varchar2(35),
 AFUDC_TYPE_ID            number(22,0),
 AFUDC_STOP_DATE          date,
 IN_SERVICE_DATE          date,
 CURRENT_AMOUNT           number(22,2),
 NEW_AMOUNT               number(22,2),
 DIFF_AMOUNT              number(22,2),
 CWIP_CHARGE_FOUND        number(22,0),
 GL_ACCOUNT_ID            number(22,0),
 CLOSED_MONTH_NUMBER      number(22,0),
 UNIT_CLOSED_MONTH_NUMBER number(22,0),
 NEW_CHARGE_ID            number(22,0),
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

ALTER table CPI_RETRO_COMPARE
   add constraint PK_CPI_RETRO_COMPARE
       primary key (BATCH_ID, COMPANY_ID, WORK_ORDER_ID, MONTH_NUMBER);

ALTER table CPI_RETRO_COMPARE
   add constraint R_CPI_RETRO_COMPARE1
       foreign key (BATCH_ID)
       references CPI_RETRO_BATCH_CONTROL;

ALTER table CPI_RETRO_COMPARE
   add constraint R_CPI_RETRO_COMPARE2
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

ALTER table CPI_RETRO_COMPARE
   add constraint R_CPI_RETRO_COMPARE3
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;

ALTER table CPI_RETRO_COMPARE
   add constraint R_CPI_RETRO_COMPARE4
       foreign key (AFUDC_TYPE_ID)
       references AFUDC_CONTROL;

create table CPI_RETRO_CLOSINGS
(
 WORK_ORDER_ID            number(22,0) not null,
 COMPANY_ID               number(22,0) not null,
 BATCH_ID                 number(22,0) not null,
 WORK_ORDER_NUMBER        varchar2(35),
 CLOSED_MONTH_NUMBER  number(22,0),
 UNIT_CLOSED_MONTH_NUMBER  number(22,0),
 ADJUSTED_WO_CLOSE_AMOUNT number(22,2),
 ASSETS_FOUND             number(22,0),
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

ALTER table CPI_RETRO_CLOSINGS
   add constraint PK_CPI_RETRO_CLOSINGS
       primary key (BATCH_ID, COMPANY_ID, WORK_ORDER_ID, CLOSED_MONTH_NUMBER);

ALTER table CPI_RETRO_CLOSINGS
   add constraint R_CPI_RETRO_CLOSINGS1
       foreign key (BATCH_ID)
       references CPI_RETRO_BATCH_CONTROL;

ALTER table CPI_RETRO_CLOSINGS
   add constraint R_CPI_RETRO_CLOSINGS2
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

ALTER table CPI_RETRO_CLOSINGS
   add constraint R_CPI_RETRO_CLOSINGS3
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;


CREATE GLOBAL TEMPORARY table CPI_RETRO_CLOSINGS_TEMP
(
 WORK_ORDER_ID            number(22,0) not null,
 COMPANY_ID               number(22,0) not null,
 BATCH_ID                 number(22,0) not null,
 WORK_ORDER_NUMBER        varchar2(35),
 CPR_MONTH_NUMBER  number(22,0),
 WO_CLOSE_AMOUNT number(22,2)
) on commit preserve rows;


create table CPI_RETRO_CPR_ASSETS
(
 WORK_ORDER_ID         number(22,0),
 COMPANY_ID            number(22,0),
 BATCH_ID              number(22,0),
 WORK_ORDER_NUMBER     varchar2(35),
 ASSET_ID              number(22,0),
 ASSET_ACTIVITY_ID     number(22,0),
 RETIREMENT_UNIT_ID    number(22,0),
 GL_ACCOUNT_ID         number(22,0),
 MONTH_NUMBER          number(22,0),
 ACTIVITY_CODE         char(8),
 ACTIVITY_COST         number(22,2),
 CPI_BASIS_COST        number(22,2),
 ALLOC_STATISTIC       number(22,2),
 ALLOC_RATIO           number(22,8),
 ALLOCATED_AMOUNT      number(22,2),
 NEW_ASSET_ACTIVITY_ID number(22,0)
);

ALTER table CPI_RETRO_CPR_ASSETS
   add constraint PK_CPI_RETRO_CPR_ASSETS
       primary key (BATCH_ID, ASSET_ID, ASSET_ACTIVITY_ID)
       using index tablespace PWRPLANT_IDX;

ALTER table CPI_RETRO_CPR_ASSETS
   add constraint R_CPI_RETRO_CPR_ASSETS1
       foreign key (BATCH_ID)
       references CPI_RETRO_BATCH_CONTROL;

ALTER table CPI_RETRO_CPR_ASSETS
   add constraint R_CPI_RETRO_CPR_ASSETS2
       foreign key (COMPANY_ID)
       references COMPANY_SETUP;

ALTER table CPI_RETRO_CPR_ASSETS
   add constraint R_CPI_RETRO_CPR_ASSETS3
       foreign key (WORK_ORDER_ID)
       references WORK_ORDER_CONTROL;

update PP_REPORTS
   set INPUT_WINDOW = 'dw_cpi_retro_batch_select',
       LONG_DESCRIPTION = 'This report shows the retro calculations of Construction Period Interest capitalized for income tax purposes (CPI) charges.  The report is sorted by month, company, business segment, and by work order. '
 where DATAWINDOW = 'dw_cpi_report_retro_test';

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, ALLOW_CONCURRENT)
   select max(PROCESS_ID) + 1, 'CPI Retro Calculation', 'CPI Retro Calculation', 0
     from PP_PROCESSES;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (240, 0, 10, 4, 0, 0, 7510, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_007510_cpi_retro.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;