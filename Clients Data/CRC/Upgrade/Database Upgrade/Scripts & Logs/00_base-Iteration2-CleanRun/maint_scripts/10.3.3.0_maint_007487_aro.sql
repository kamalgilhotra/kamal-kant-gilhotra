/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007487_aro.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/31/2011 Lee Quinn      Point Release
||============================================================================
*/

-- Drop index on table ARO_WORK_ORDER(WORK_ORDER_ID) if it exists.
declare
   PPCMSG varchar2(10) := 'PPC-MSG> ';
   PPCERR varchar2(10) := 'PPC-ERR> ';
   PPCSQL varchar2(10) := 'PPC-SQL> ';

   L_DROP_INDEX_SQL varchar2(128);
   L_INDEX_NAME     varchar2(30);

begin
   select 'drop index ' || AI.INDEX_NAME, AI.INDEX_NAME
     into L_DROP_INDEX_SQL, L_INDEX_NAME
     from ALL_INDEXES AI, ALL_IND_COLUMNS AIC
    where AI.TABLE_OWNER = 'PWRPLANT'
      and AI.TABLE_OWNER = AIC.TABLE_OWNER
      and AI.TABLE_NAME = 'ARO_WORK_ORDER'
      and AI.TABLE_NAME = AIC.TABLE_NAME
      and AI.INDEX_NAME = AIC.INDEX_NAME
      and AIC.COLUMN_NAME = 'WORK_ORDER_ID'
      and AI.INDEX_NAME = (select INDEX_NAME
                             from ALL_IND_COLUMNS AIC
                            where AIC.TABLE_NAME = 'ARO_WORK_ORDER'
                              and AIC.TABLE_OWNER = 'PWRPLANT'
                            group by INDEX_NAME
                           having count(*) = 1);

   execute immediate L_DROP_INDEX_SQL;
   DBMS_OUTPUT.PUT_LINE(PPCSQL || L_DROP_INDEX_SQL || ';');
   DBMS_OUTPUT.PUT_LINE(PPCMSG || 'Index: ' || L_INDEX_NAME || ' DROPPED.');
exception
   when NO_DATA_FOUND then
      DBMS_OUTPUT.PUT_LINE(PPCMSG || 'No index to drop.');
end;
/

create unique index ARO_WORK_ORDER_WO_ID_IDX
   on ARO_WORK_ORDER (WORK_ORDER_ID)
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (23, 0, 10, 3, 3, 0, 7487, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_007487_aro.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
