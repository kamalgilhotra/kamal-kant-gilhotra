/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051379_lessee_03_make_new_column_not_null_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/29/2018 Charlie Shilling Convert the new column to NOT NULL now that it's been populated.
||============================================================================
*/

ALTER TABLE ls_ilr_options
MODIFY depr_calc_method NOT NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6004, 0, 2017, 4, 0, 0, 51379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051379_lessee_03_make_new_column_not_null_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;