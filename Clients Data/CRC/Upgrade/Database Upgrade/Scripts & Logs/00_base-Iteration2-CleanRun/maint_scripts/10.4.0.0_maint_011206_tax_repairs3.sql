/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs3.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   02/21/2013 Blake Andrews
|| 10.4.1.2   12/11/2013 Alex P.         Modified after release
||============================================================================
*/

--Added 08/19/2013 - Alex P.
alter table REPAIR_SCHEMA disable constraint FK_CLASS_CODE_ID;

--Added 12/11/2013 - Alex P.
delete from CLASS_CODE_VALUES
 where CLASS_CODE_ID in (select distinct REPAIR_LOC_OVRRD_CLASS_CODE_ID from REPAIR_SCHEMA);
 
delete from PP_CLASS_CODE_GROUPS
 where CLASS_CODE_ID in (select distinct REPAIR_LOC_OVRRD_CLASS_CODE_ID from REPAIR_SCHEMA);

delete from WORK_ORDER_CLASS_CODE
 where CLASS_CODE_ID in (select distinct REPAIR_LOC_OVRRD_CLASS_CODE_ID from REPAIR_SCHEMA);
-- End 12/11/2013

-- Delete repair location class code
delete from CLASS_CODE_CPR_LEDGER
 where CLASS_CODE_ID in (select distinct REPAIR_LOC_OVRRD_CLASS_CODE_ID from REPAIR_SCHEMA);

-- 09082014 - Alex Pivoshenko
delete from UNIT_ITEM_CLASS_CODE
 where CLASS_CODE_ID in (select distinct REPAIR_LOC_OVRRD_CLASS_CODE_ID from REPAIR_SCHEMA);

delete from CLASS_CODE_PENDING_TRANS
 where CLASS_CODE_ID in (select distinct REPAIR_LOC_OVRRD_CLASS_CODE_ID from REPAIR_SCHEMA);
--END Fix

delete from CLASS_CODE
 where CLASS_CODE_ID in (select distinct REPAIR_LOC_OVRRD_CLASS_CODE_ID from REPAIR_SCHEMA);

-- Drop unused columns and tables
alter table REPAIR_LOC_ROLLUP drop column WORK_ORDER_ID;

alter table REPAIR_SCHEMA drop column REPAIR_LOC_OVRRD_CLASS_CODE_ID;
alter table REPAIR_SCHEMA drop column CPR_ONLY_NO_JE;

alter table REPAIR_AMOUNT_ALLOCATE     drop column REPAIR_METHOD_ID;
alter table REPAIR_AMT_ALLOC_REPORTING drop column REPAIR_METHOD_ID;
alter table REPAIR_DETAIL_WMIS_FEED    drop column REPAIR_METHOD_ID;

alter table REPAIR_DETAIL_WMIS_FEED drop column VINTAGE;

--alter table REPAIR_DETAIL_WMIS_FEED_TEMP drop column REPAIR_METHOD_ID;
--alter table REPAIR_DETAIL_WMIS_FEED_TEMP drop column VINTAGE;

alter table REPAIR_WORK_ORDER_TEMP drop column REPAIR_METHOD_ID;
alter table REPAIR_WORK_ORDER_TEMP drop column VINTAGE;

alter table REPAIR_WORK_ORDER_TEMP_ORIG drop column REPAIR_METHOD_ID;
alter table REPAIR_WORK_ORDER_TEMP_ORIG drop column VINTAGE;

alter table REPAIR_CALC_ASSET drop column REPAIR_METHOD_ID;
alter table REPAIR_CALC_ASSET drop column PRE81_QUANTITY;
alter table REPAIR_CALC_ASSET drop column POST80_QUANTITY;
alter table REPAIR_CALC_ASSET drop column PRE81_RETIREMENT_RATIO;
alter table REPAIR_CALC_ASSET drop column VINTAGE_MIN;
alter table REPAIR_CALC_ASSET drop column VINTAGE_MAX;

alter table REPAIR_CALC_PEND_TRANS drop column REPAIR_METHOD_ID;
alter table REPAIR_CALC_PEND_TRANS drop column PRE81_QUANTITY;
alter table REPAIR_CALC_PEND_TRANS drop column POST80_QUANTITY;
alter table REPAIR_CALC_PEND_TRANS drop column PRE81_RETIREMENT_RATIO;
alter table REPAIR_CALC_PEND_TRANS drop column VINTAGE_MIN;
alter table REPAIR_CALC_PEND_TRANS drop column VINTAGE_MAX;

alter table REPAIR_WO_SEG_REPORTING drop constraint PK_REPAIR_WO_SEG_REPORTING drop index;

alter table REPAIR_WO_SEG_REPORTING
   add constraint PK_REPAIR_WO_SEG_REPORTING
       primary key (BATCH_ID, REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER,
                    WORK_REQUEST, REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID, RETIREMENT_UNIT_ID,
                    REPAIR_QTY_INCLUDE)
       using index tablespace PWRPLANT_IDX;

alter table REPAIR_WO_SEG_REPORTING drop column REPAIR_METHOD_ID;
alter table REPAIR_WO_SEG_REPORTING drop column PRE81_QUANTITY;
alter table REPAIR_WO_SEG_REPORTING drop column POST80_QUANTITY;
alter table REPAIR_WO_SEG_REPORTING drop column MIN_VINTAGE;
alter table REPAIR_WO_SEG_REPORTING drop column MAX_VINTAGE;

alter table REPAIR_WORK_ORDER_SEGMENTS drop constraint PK_REPAIR_WORK_ORDER_SEGMENTS drop index;

alter table repair_work_order_segments
   add constraint PK_REPAIR_WORK_ORDER_SEGMENTS
       primary key (REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER, WORK_REQUEST,
                    REPAIR_UNIT_CODE_ID, REPAIR_LOCATION_ID, RETIREMENT_UNIT_ID,
                    REPAIR_QTY_INCLUDE);

alter table REPAIR_WORK_ORDER_SEGMENTS drop column REPAIR_METHOD_ID;
alter table REPAIR_WORK_ORDER_SEGMENTS drop column PRE81_QUANTITY;
alter table REPAIR_WORK_ORDER_SEGMENTS drop column POST80_QUANTITY;
alter table REPAIR_WORK_ORDER_SEGMENTS drop column MIN_VINTAGE;
alter table REPAIR_WORK_ORDER_SEGMENTS drop column MAX_VINTAGE;

drop table REPAIR_LIMIT;
drop table REPAIR_METHOD_KEY;
drop table REPAIR_METHOD_PARAMETERS;
drop table REPAIR_METHOD;
drop table REPAIR_CALC_ASSET2;

alter table WORK_ORDER_ACCOUNT drop column REPAIR_EXCLUDE;
alter table WORK_ORDER_ACCOUNT drop column TE_AGGREGATION_ID;
alter table WORK_ORDER_ACCOUNT drop column TAX_EXPENSE_TEST_ID;

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION)
   select max(PROCESS_ID) + 1,
          'Tax Repairs Processing',
          'Tax Repairs Processing for CPR/CWIP/CWIP_CPR/BLANKET/WMIS methods.'
     from PP_PROCESSES;

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION)
   select max(PROCESS_ID) + 1, 'Tax Repairs Status Update', 'Tax Repairs Status Update'
     from PP_PROCESSES;

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION)
   select max(PROCESS_ID) + 1, 'Tax Repairs Posting', 'Tax Repairs Posting' from PP_PROCESSES;

update PP_DYNAMIC_FILTER
   set SINGLE_SELECT_ONLY = 1
 where FILTER_ID = 37
   and LABEL = 'Funding Project Indicator';

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID,
    DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP,
    VALID_OPERATORS, EXCLUDE_FROM_WHERE)
values
   (52, 'Repair Test', 'dw', 'nvl(work_order_tax_repairs.tax_expense_test_id, -1)', null,
    'dw_rpr_tax_expense_test_none_filter', 'tax_expense_test_id', 'description', 'N', null, null,
    null, null, null, null, null);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (36, 52, null, null);

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, DESCRIPTION, TABLE_NAME)
values
   (35, 'Repairs - WO - Assign Test', 'work_order_control');

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID)
   select 35, FILTER_ID from PP_DYNAMIC_FILTER_MAPPING where PP_REPORT_FILTER_ID = 34;

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (35, 52, null, null);

alter table REPAIR_WORK_ORDER_TEMP_ORIG add BLANKET_METHOD varchar2(35);
alter table REPAIR_WORK_ORDER_TEMP      add BLANKET_METHOD varchar2(35);

alter table REPAIR_WORK_ORDER_TEMP_ORIG add WORK_ORDER_ID number(22);
alter table REPAIR_WORK_ORDER_TEMP      add WORK_ORDER_ID number(22);


alter table REPAIR_BLANKET_PROCESSING add REPAIR_SCHEMA_ID number(22);

drop table REPAIR_PER_SE_CAPITAL_PCT;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (305, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs3.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;