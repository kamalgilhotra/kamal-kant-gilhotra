/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049557_lessee_01_ilr_renewal_import_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/10/2017 Shane "C" Ward   New Lessee Renewal Option Import
||============================================================================
*/
CREATE TABLE LS_IMPORT_ILR_RENEWAL
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     time_stamp                 DATE NULL,
     error_message              VARCHAR2(4000) NULL,
     user_id                    VARCHAR2(18) NULL,
     ilr_id                     NUMBER(22, 0) NULL,
     ilr_id_xlate               VARCHAR2(254) NULL,
     revision                   NUMBER(22, 0) NULL,
     ilr_renewal_prob_id        NUMBER(22, 0) NULL,
     ilr_renewal_prob_id_xlate  VARCHAR2(35) NULL,
     renewal_notice             NUMBER(22, 0) NULL,
     renewal_frequency_id       NUMBER(22, 0) NULL,
     renewal_frequency_id_xlate VARCHAR2(35) NULL,
     renewal_number_of_terms    NUMBER(22, 0) NULL,
     renewal_amount_per_term    NUMBER(22, 2) NULL,
     ilr_renewal_id             NUMBER(22, 0) NULL,
     ilr_renewal_option_id      NUMBER(22, 0) NULL,
     renewal_start_date         VARCHAR2(35) NULL
  );

COMMENT ON TABLE LS_IMPORT_ILR_RENEWAL IS '(S)  [06]
The Lessee Import ILR Renewal table is an API table used to import Lessee ILRs Renewal Options.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.ilr_id IS 'The internal ILR id within PowerPlant .';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.ilr_id_xlate IS 'Translation field for internal ILR id within PowerPlant .';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.revision IS 'Revision of ILR.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.ilr_renewal_prob_id IS 'ID of Renewal probability of ILR';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.ilr_renewal_prob_id_xlate IS 'Translation of ID of Renewal probability of ILR';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.renewal_notice IS 'Number of Months needed for notice prior to end of regular lease term for renewal option';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.renewal_frequency_id IS 'Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.renewal_frequency_id_xlate IS 'Translation for Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.renewal_number_of_terms IS 'Number of terms associated with renewal option';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.renewal_amount_per_term IS 'Amount associated with Renewal Option';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.renewal_start_date IS 'Date of Renewal Start in ''MM/DD/YYYY'' format';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.ilr_renewal_id IS 'System-assigned ID of Renewal for ILR and Revision combination';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL.ilr_renewal_option_id IS 'User defined value to group renewal options together';

CREATE TABLE LS_IMPORT_ILR_RENEWAL_ARCHIVE
  (
     import_run_id              NUMBER(22, 0) NOT NULL,
     line_id                    NUMBER(22, 0) NOT NULL,
     time_stamp                 DATE NULL,
     error_message              VARCHAR2(4000) NULL,
     user_id                    VARCHAR2(18) NULL,
     ilr_id                     NUMBER(22, 0) NULL,
     ilr_id_xlate               VARCHAR2(254) NULL,
     revision                   NUMBER(22, 0) NULL,
     ilr_renewal_prob_id        NUMBER(22, 0) NULL,
     ilr_renewal_prob_id_xlate  VARCHAR2(35) NULL,
     renewal_notice             NUMBER(22, 0) NULL,
     renewal_frequency_id       NUMBER(22, 0) NULL,
     renewal_frequency_id_xlate VARCHAR2(35) NULL,
     renewal_number_of_terms    NUMBER(22, 0) NULL,
     renewal_amount_per_term    NUMBER(22, 2) NULL,
     ilr_renewal_id             NUMBER(22, 0) NULL,
     ilr_renewal_option_id      NUMBER(22, 0) NULL,
     renewal_start_date         VARCHAR2(35) NULL
  );

COMMENT ON TABLE LS_IMPORT_ILR_RENEWAL_ARCHIVE IS '(S)  [06]
The Lessee Import ILR Renewal Archive table is an API table used to Archive imported Lessee ILRs Renewal Options.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.import_run_id IS 'System-assigned ID that specifies the import run that this record was imported in.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.line_id IS 'System-assigned line number for this import run.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.time_stamp IS 'Standard system-assigned timestamp used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.user_id IS 'Standard system-assigned user id used for audit purposes.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_id IS 'The internal ILR id within PowerPlant .';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_id_xlate IS 'Translation field for internal ILR id within PowerPlant .';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.revision IS 'Revision of ILR.';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_prob_id IS 'ID of Renewal probability of ILR';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_prob_id_xlate IS 'Translation of ID of Renewal probability of ILR';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_notice IS 'Number of Months needed for notice prior to end of regular lease term for renewal option';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_frequency_id IS 'Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_frequency_id_xlate IS 'Translation for Frequency of renewal payments, e.g., Monthly, Quarterly, Semi-Annually, Annually';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_number_of_terms IS 'Number of terms associated with renewal option';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_amount_per_term IS 'Amount associated with Renewal Option';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.renewal_start_date IS 'Date of Renewal Start in ''MM/DD/YYYY'' format';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_id IS 'System-assigned ID of Renewal for ILR and Revision combination';

COMMENT ON COLUMN LS_IMPORT_ILR_RENEWAL_ARCHIVE.ilr_renewal_option_id IS 'User defined value to group renewal options together'; 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3923, 0, 2017, 1, 0, 0, 49557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049557_lessee_01_ilr_renewal_import_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
