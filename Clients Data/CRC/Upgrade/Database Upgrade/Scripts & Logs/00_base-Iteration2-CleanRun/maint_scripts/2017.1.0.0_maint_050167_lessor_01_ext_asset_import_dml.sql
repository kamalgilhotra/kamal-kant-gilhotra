/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050167_lessor_01_ext_asset_import_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 12/15/2017 Josh Sandler		Fix external asset import
||============================================================================
*/

UPDATE pp_import_column
SET import_column_name = NULL,
processing_order = 1,
is_on_table = 1
WHERE import_type_id IN (503,504)
AND import_column_name = 'estimated_residual_xlate';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4072, 0, 2017, 1, 0, 0, 50167, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_050167_lessor_01_ext_asset_import_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;