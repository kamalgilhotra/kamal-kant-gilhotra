/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033713_reg_03_alter_company_setup.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014  Sarah Byers
||============================================================================
*/

alter table COMPANY_SETUP add REG_COMPANY_ID number(22,0);

alter table COMPANY_SETUP
   add constraint R_COMPANY_SETUP5
       foreign key (REG_COMPANY_ID)
       REFERENCES REG_COMPANY (REG_COMPANY_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (746, 0, 10, 4, 2, 0, 33713, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_033713_reg_03_alter_company_setup.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
