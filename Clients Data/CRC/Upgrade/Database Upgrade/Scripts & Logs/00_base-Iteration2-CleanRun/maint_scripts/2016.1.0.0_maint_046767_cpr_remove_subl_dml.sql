 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_046767_cpr_remove_subl_dml.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2016.1.0.0 11/08/2016 AJ Smith		 Remove (or rename) subledger related entries
 ||============================================================================
 */ 

delete from subledger_depr_type
where depreciation_type_id not in (0, 3)
and depreciation_type_id not in
(select distinct s.depreciation_indicator from subledger_control s) 
;

update subledger_depr_type
set description = substr('(Obsolete) ' || description, 1, 35)
where depreciation_type_id not in (0, 3)
and depreciation_type_id in
(select distinct s.depreciation_indicator from subledger_control s) 
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3336, 0, 2016, 1, 0, 0, 046767, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_046767_cpr_remove_subl_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;