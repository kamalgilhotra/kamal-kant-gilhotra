/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041334_reg_import_ledg_adj_setup.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   11/18/2014 Shane Ward    		Add Import for Ledger Adjustments
||========================================================================================
*/

/****************************
**	Deletes
****************************/
delete from pp_import_template_fields where import_type_id in (152, 153);
drop table reg_import_history_ldg;
drop table reg_import_history_ldg_arc;
delete from pp_import_run
 where import_template_id in
			 (select import_template_id
					from pp_import_template
				 where import_type_id in (152, 153));
delete from pp_import_template where import_type_id in (152, 153);
delete from pp_import_column_lookup where import_type_id in (152, 153);
delete from pp_import_column where import_type_id in (152, 153);
delete from pp_import_type_subsystem where import_type_id in (152, 153);
delete from pp_import_type where import_type_id in (152, 153);

/****************************
**  Import Type
****************************/
insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME)
values
   (152, 'History Ledger Adjustment Import', 'Import History Ledger Adjustments',
    'reg_import_ledger_adj_stg', 'reg_import_ledger_adj_stg_arc', 0, 'nvo_reg_logic_import');
insert into PP_IMPORT_TYPE
   (IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME,
    ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME)
values
   (153, 'Forecast Ledger Adjustment Import', 'Import Forecast Ledger Adjustments',
    'reg_import_ledger_adj_stg', 'reg_import_ledger_adj_stg_arc', 0, 'nvo_reg_logic_import');

/****************************
**  Import Type Subsytem
****************************/
INSERT INTO pp_import_type_subsystem (import_Type_id, import_subsystem_id)
VALUES (152, 6);

INSERT INTO pp_import_type_subsystem (import_Type_id, import_subsystem_id)
VALUES (153, 6);

/****************************
**  Import Column
****************************/
-- Historic
--Historic Version
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, 'version_id', 'Historic Version', 'version_id_xlate', 1, 1, 'varchar2(254)',
    'reg_hist_version_lookup', 1);
--Company - Update to Longest Description
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, 'reg_company_id', 'Company', 'company_id_xlate', 1, 1, 'varchar2(100)', 'reg_company', 1);
--Reg Acct - Update to long description length
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, 'reg_acct_id', 'Reg Account', 'reg_acct_id_xlate', 1, 1, 'varchar2(254)',
    'reg_acct_master', 1);
--GL Month
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, 'gl_month', 'GL Month (YYYYMM)', null, 1, 1, 'number(8,0)', null, null);
--Adjustment Month
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, 'adjust_month', 'Adjustment Month (YYYYMM)', null, 1, 1, 'number(8,0)', null,
    null);
--COMMENT - Update to COMMENT length
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, '"comment"', 'comment', null, 1, 1, 'varchar2(2000)', null, null);
--Adjust Amount
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, 'adjust_amount', 'Adjustment Amount', null, 1, 1, 'number(22,2)', null, null);
--Reverse Month
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (152, 'reverse_month', 'Reverse Month (YYYYMM)', null, 0, 1, 'number(8,0)', null,
    null);

-- Forecast
--Forecast Version
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, 'version_id', 'Forecast Version', 'version_id_xlate', 1, 1, 'varchar2(254)',
    'reg_fcst_version_lookup', 1);
--Company
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, 'reg_company_id', 'Company', 'company_id_xlate', 1, 1, 'varchar2(100)', 'reg_company', 1);
--Reg Acct
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, 'reg_acct_id', 'Reg Account', 'reg_acct_id_xlate', 1, 1, 'varchar2(254)',
    'reg_acct_master', 1);
--GL Month
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, 'gl_month', 'GL Month (YYYYMM)', null, 1, 1, 'number(8,0)', null, null);
--Adjustment Month
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, 'adjust_month', 'Adjustment Month (YYYYMM)', null, 1, 1, 'number(8,0)', null,
    null);
--COMMENT - Update to COMMENT length
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, '"comment"', 'comment', null, 1, 1, 'varchar2(2000)', null, null);
--Adjust Amount
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, 'adjust_amount', 'Adjustment Amount', null, 1, 1, 'number(22,2)', null, null);
--Reverse Month
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
values
   (153, 'reverse_month', 'Reverse Month (YYYYMM)', null, 0, 1, 'number(8,0)', null,
    null);

/****************************
**  Translates
****************************/
--Reg Company Description to Reg Company ID
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   select max(NVL(IMPORT_LOOKUP_ID, 0)) + 1,
          'Reg Company.Description',
          'Translates Reg Company Description to Reg Company ID',
          'company_id_xlate',
          '(Select reg_company_id from reg_company where upper(trim(description)) = upper(trim(<importfield>)))',
          0,
          'reg_company',
          'description'
     from PP_IMPORT_LOOKUP;

--PP Company Description to Reg Company ID
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   select max(NVL(IMPORT_LOOKUP_ID, 0)) + 1,
          'Reg-PP Company.Description',
          'Translates PP Company Description to Reg Company ID',
          'company_id_xlate',
          '(Select reg_company_id from company_setup where upper(trim(description)) = upper(trim(<importfield>)))',
          0,
          'company_setup',
          'description'
     from PP_IMPORT_LOOKUP;

--Reg Acct Description to Reg Acct ID
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   select max(NVL(IMPORT_LOOKUP_ID, 0)) + 1,
          'Reg Acct.Description',
          'Translates Reg Acct Description to Reg Acct ID',
          'reg_acct_id_xlate',
          '(Select reg_acct_id from reg_acct_master where upper(trim(description)) = upper(trim(<importfield>)))',
          0,
          'reg_acct_master',
          'description'
     from PP_IMPORT_LOOKUP;

--Reg Acct Long Description to Reg Acct ID
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   select max(NVL(IMPORT_LOOKUP_ID, 0)) + 1,
          'Reg Acct.Long Description',
          'Translates Reg Acct Long Description to Reg Acct ID',
          'reg_acct_id_xlate',
          '(Select reg_acct_id from reg_acct_master where upper(trim(long_description)) = upper(trim(<importfield>)))',
          0,
          'reg_acct_master',
          'long_description'
     from PP_IMPORT_LOOKUP;

--Historic Version Long Description to Historic Version ID
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   select max(NVL(IMPORT_LOOKUP_ID, 0)) + 1,
          'Reg Hist Version.Long Description',
          'Translates Historic Version Long Description to Historic Version ID',
          'version_id_xlate',
          '(Select historic_version_id from reg_historic_version where upper(trim(long_description)) = upper(trim(<importfield>)))',
          0,
          'reg_historic_version',
          'long_description'
     from PP_IMPORT_LOOKUP;

--Forecast Version Long Description to Forecast Version ID
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   select max(NVL(IMPORT_LOOKUP_ID, 0)) + 1,
          'Reg Fcst Version.Long Description',
          'Translates Forecast Version Long Description to Forecast Version ID',
          'version_id_xlate',
          '(Select forecast_version_id from reg_forecast_version where upper(trim(long_description)) = upper(trim(<importfield>)))',
          0,
          'reg_forecast_version',
          'long_description'
     from PP_IMPORT_LOOKUP;

--Forecast Version Description to Forecast Version ID
insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
   select max(NVL(IMPORT_LOOKUP_ID, 0)) + 1,
          'Reg Fcst Version.Description',
          'Translates Forecast Version Description to Forecast Version ID',
          'version_id_xlate',
          '(Select forecast_version_id from reg_forecast_version where upper(trim(description)) = upper(trim(<importfield>)))',
          0,
          'reg_forecast_version',
          'description'
     from PP_IMPORT_LOOKUP;

/****************************
**  Translate Mappings
****************************/
-- Historic
--Reg Company Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG COMPANY.DESCRIPTION' ;
--PP Company Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG-PP COMPANY.DESCRIPTION';
--Reg Account Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.DESCRIPTION';
--Reg Account Long Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.LONG DESCRIPTION';
--Historic Version Long Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 152, 'version_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG HIST VERSION.LONG DESCRIPTION';

-- Forecast
--Reg Company Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG COMPANY.DESCRIPTION';
--PP Company Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_company_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG-PP COMPANY.DESCRIPTION';
--Reg Account Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.DESCRIPTION';
--Reg Account Long Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'reg_acct_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG ACCT.LONG DESCRIPTION';
--Forecast Version Long Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'version_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG FCST VERSION.LONG DESCRIPTION';
--Forecast Version Description
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 153, 'version_id', IMPORT_LOOKUP_ID
     from PP_IMPORT_LOOKUP
    where UPPER(DESCRIPTION) = 'REG FCST VERSION.DESCRIPTION';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2052, 0, 2015, 1, 0, 0, 41334, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041334_reg_import_ledg_adj_setup.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;