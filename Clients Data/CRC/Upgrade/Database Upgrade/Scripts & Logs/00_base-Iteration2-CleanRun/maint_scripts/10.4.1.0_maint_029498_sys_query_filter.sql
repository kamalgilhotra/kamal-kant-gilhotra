/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029498_sys_query_filter.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 10.4.1.0   08/20/2013 Stephen Motter   Point Release
||============================================================================
*/

create table PPBASE_QUERY_FILTERS
(
 DW_SYNTAX     varchar2(4000) not null,
 SAVE_NAME     varchar2(255) not null,
 FILTER_VALUES varchar2(4000)
);

comment on table PPBASE_QUERY_FILTERS is '(S) [10] The Query Filters table holds saved filter settings for the ppbasequery tool.';
comment on column PPBASE_QUERY_FILTERS.DW_SYNTAX is 'The sql for each table is used as the look up key.';
comment on column PPBASE_QUERY_FILTERS.SAVE_NAME is 'The save name distinguishes different saves for the same table.';
comment on column PPBASE_QUERY_FILTERS.FILTER_VALUES is 'The filter values are stored using :: as a delimiter.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (522, 0, 10, 4, 1, 0, 29498, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029498_sys_query_filter.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;