/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_045276_lease_fix_asset_import_util_acct_dml.sql
||========================================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 2015.2.1.0 12/17/2015 Will Davis	        Make lease asset import include business segment in
||                                          utility account lookup
||========================================================================================
*/

update pp_import_column
set processing_order = 4
where import_type_id = (select import_type_id from pp_import_type where upper(trim(description)) = 'ADD: LEASED ASSETS')
  and column_name = 'sub_account_id';

update pp_import_column
set processing_order = 3
where import_type_id = (select import_type_id from pp_import_type where upper(trim(description)) = 'ADD: LEASED ASSETS')
  and column_name = 'utility_account_id';

update pp_import_column
set processing_order = 2
where import_type_id = (select import_type_id from pp_import_type where upper(trim(description)) = 'ADD: LEASED ASSETS')
  and column_name = 'bus_segment_id';

update pp_import_template_fields
set import_lookup_id = (select import_lookup_id from pp_import_lookup where upper(description) = 'UTILITY ACCOUNT.DESCRIPTION (FOR GIVEN BUSINESS SEGMENT)')
where import_type_id = (select import_type_id from pp_import_type where upper(trim(description)) = 'ADD: LEASED ASSETS')
and column_name = 'utility_account_id';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3018, 0, 2015, 2, 1, 0, 045276, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045276_lease_fix_asset_import_util_acct_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;