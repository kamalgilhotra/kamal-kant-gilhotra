/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_042191_version-updates_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.2 1/21/2015  Luke Warren      Patch Release
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.4.3.0',
       PP_PATCH   = 'Patch: 10.4.3.2',
       POST_VERSION = 'Version 10.4.3.a October 29, 2014', -- NO CHANGE FROM 10.4.3.1
       POWERTAX_VERSION = '10.4.3.2',
       PROVISION_VERSION = '10.4.3.2',
       PROP_TAX_VERSION = '10.4.3.2',
       LEASE_VERSION = '10.4.3.2',
       BUDGET_VERSION = '10.4.3.2',
       DATE_INSTALLED = sysdate
where PP_VERSION_ID = 1;

update PP_PROCESSES
   set VERSION = '10.4.3.2'
 where trim(LOWER(EXECUTABLE_FILE)) in ('automatedreview.exe',
                                        'batreporter.exe',
                                        'budget_allocations.exe',
                                        'cpm.exe',
                                        'cr_allocations.exe',
                                        'cr_balances.exe',
                                        'cr_balances_bdg.exe',
                                        'cr_batch_derivation.exe',
                                        'cr_batch_derivation_bdg.exe',
                                        'cr_budget_entry_calc_all.exe',
                                        'cr_build_combos.exe',
                                        'cr_build_deriver.exe',
                                        'cr_delete_budget_allocations.exe',
                                        'cr_financial_reports_build.exe',
                                        'cr_flatten_structures.exe',
                                        'cr_man_jrnl_reversals.exe',
                                        'cr_posting.exe',
                                        'cr_posting_bdg.exe',
                                        'cr_sap_trueup.exe', -- Next version cr_sap_trueup.exe will be renamed to cr_derivation_trueup.exe
                                        'cr_sum.exe',
                                        'cr_sum_ab.exe',
                                        'cr_to_commitments.exe',
                                        'cwip_charge.exe',
                                        'populate_matlrec.exe',
                                        'ppmetricbatch.exe',
                                        'pptocr.exe',
                                        'ppverify.exe',
                                        'pp_integration.exe',
                                        'ssp_aro_approve.exe',
                                        'ssp_aro_calc.exe',
                                        'ssp_budget.exe',
                                        'ssp_cpr_close_month.exe',
                                        'ssp_cpr_new_month.exe',
                                        'ssp_depr_calc.exe'
                                        );


update PP_MODULES
   set VERSION = '10.4.3.2'
 where trim(LOWER(MODULE_ID)) in (1, -- Depreciation Studies
                          2, -- Cost Repository
                          3, -- Property Tax
                          4, -- Asset Management
                          5, -- Project Management
                          6, -- Budget
                          7, -- Property Unit
                          8, -- Administration
                          9, -- Depreciation
                          10, -- PowerTax
                          11, -- Tables
                          12, -- PowerTax Provision
                          13, -- OM Budget
                          14, -- Tax Repairs
                          15 -- Regulatory
                                  );


insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
          select '10.4.3.2', 'automatedreview.exe',               '10.4.3.2' from dual
union all select '10.4.3.2', 'batreporter.exe',                   '10.4.3.2' from dual
union all select '10.4.3.2', 'budget_allocations.exe',            '10.4.3.2' from dual
union all select '10.4.3.2', 'cpm.exe',                           '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_allocations.exe',                '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_balances.exe',                   '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_balances_bdg.exe',               '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_batch_derivation.exe',           '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_batch_derivation_bdg.exe',       '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_budget_entry_calc_all.exe',      '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_build_combos.exe',               '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_build_deriver.exe',              '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_delete_budget_allocations.exe',  '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_financial_reports_build.exe',    '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_flatten_structures.exe',         '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_man_jrnl_reversals.exe',         '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_posting.exe',                    '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_posting_bdg.exe',                '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_sap_trueup.exe',                 '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_sum.exe',                        '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_sum_ab.exe',                     '10.4.3.2' from dual
union all select '10.4.3.2', 'cr_to_commitments.exe',             '10.4.3.2' from dual
union all select '10.4.3.2', 'cwip_charge.exe',                   '10.4.3.2' from dual
union all select '10.4.3.2', 'populate_matlrec.exe',              '10.4.3.2' from dual
union all select '10.4.3.2', 'ppmetricbatch.exe',                 '10.4.3.2' from dual
union all select '10.4.3.2', 'pptocr.exe',                        '10.4.3.2' from dual
union all select '10.4.3.2', 'ppverify.exe',                      '10.4.3.2' from dual
union all select '10.4.3.2', 'pp_integration.exe',                '10.4.3.2' from dual
union all select '10.4.3.2', 'ssp_aro_approve.exe',               '10.4.3.2' from dual
union all select '10.4.3.2', 'ssp_aro_calc.exe',                  '10.4.3.2' from dual
union all select '10.4.3.2', 'ssp_budget.exe',                    '10.4.3.2' from dual
union all select '10.4.3.2', 'ssp_cpr_close_month.exe',           '10.4.3.2' from dual
union all select '10.4.3.2', 'ssp_cpr_new_month.exe',             '10.4.3.2' from dual
union all select '10.4.3.2', 'ssp_depr_calc.exe',                 '10.4.3.2' from dual
minus select PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION from PP_VERSION_EXES;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2209, 0, 10, 4, 3, 2, 042191, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_042191_version-updates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;