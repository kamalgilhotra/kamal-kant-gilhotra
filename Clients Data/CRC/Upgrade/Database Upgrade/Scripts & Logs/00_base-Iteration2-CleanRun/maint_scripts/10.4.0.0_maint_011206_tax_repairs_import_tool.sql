/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs_import_tool.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   02/11/2013 Alex Pivoshenko
||============================================================================
*/

--
-- Create the import control data.
--
insert into PP_IMPORT_SUBSYSTEM (IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION) values ( 5, sysdate, user, 'Tax Repairs', 'Tax Repairs' );

insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 101, sysdate, user, 'Repair Thresholds', 'Import Tax Repair Thresholds', 'rpr_import_threshold', 'rpr_import_threshold_arc', null, 1, 'nvo_rpr_logic_import', '', 'Threshold' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 102, sysdate, user, 'Repair Range Tests', 'Import Tax Repair Range Tests', 'rpr_import_range_test', 'rpr_import_range_test_arc', null, 0, 'nvo_rpr_logic_import', 'range_test_id', 'Range Test' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 103, sysdate, user, 'Repair Range Test Ranges', 'Import Tax Repair Range Test Ranges', 'rpr_import_range_test_rng', 'rpr_import_range_test_rng_arc', null, 1, 'nvo_rpr_logic_import', '', 'Range' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 104, sysdate, user, 'Repair Schemas', 'Import Tax Repair Schemas', 'rpr_import_schema', 'rpr_import_schema_arc', null, 1, 'nvo_rpr_logic_import', 'repair_schema_id', 'Schema' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 105, sysdate, user, 'Repair Tests', 'Import Tax Repair Tests', 'rpr_import_test', 'rpr_import_test_arc', null, 1, 'nvo_rpr_logic_import', 'tax_expense_test_id', 'Test' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 106, sysdate, user, 'Repair Location Rollups', 'Import Tax Repair Location Rollups', 'rpr_import_loc_rollup', 'rpr_import_loc_rollup_arc', null, 1, 'nvo_rpr_logic_import', 'repair_loc_rollup_id', 'Location Rollup' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 107, sysdate, user, 'Repair Locations', 'Import Tax Repair Locations', 'rpr_import_location', 'rpr_import_location_arc', null, 1, 'nvo_rpr_logic_import', 'repair_location_id', 'Location' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 108, sysdate, user, 'Tax Statuses', 'Import Tax Statuses', 'rpr_import_tax_status', 'rpr_import_tax_status_arc', null, 1, 'nvo_rpr_logic_import', 'tax_status_id', 'Tax Status' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 109, sysdate, user, 'Repair Unit Codes', 'Import Tax Repair Unit Codes', 'rpr_import_unit_code', 'rpr_import_unit_code_arc', null, 1, 'nvo_rpr_logic_import', 'repair_unit_code_id', 'Unit Code' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 110, sysdate, user, 'Assign : Range Test to Unit Code', 'Import Range Test to Unit Code Assignments', 'rpr_import_rng_tst_unt_cd', 'rpr_import_rng_tst_unt_cd_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 111, sysdate, user, 'Assign : Repair Test to WO Type', 'Import Repair Test to Work Order Type Assignments', 'rpr_import_tst_wotype', 'rpr_import_tst_wotype_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 112, sysdate, user, 'Assign : Loc Rollup to Location', 'Import Location Rollup to Location Assignments', 'rpr_import_loc_roll_loc', 'rpr_import_loc_roll_loc_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 113, sysdate, user, 'Assign : Loc Rollup to Func Class', 'Import Location Rollup to Functional Class Assignments', 'rpr_import_loc_roll_fc', 'rpr_import_loc_roll_fc_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 114, sysdate, user, 'Assign : Repair Loc to Asset Loc', 'Import Repair Location to Asset Location Assignments', 'rpr_import_loc_ast_loc', 'rpr_import_loc_ast_loc_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 115, sysdate, user, 'Assign : Unit Code to Prop Group', 'Import Unit Code to Property Group Assignments', 'rpr_import_uc_prp_grp', 'rpr_import_uc_prp_grp_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 116, sysdate, user, 'Assign : Unit Code to WO Type', 'Import Unit Code to Work Order Type Assignments', 'rpr_import_uc_wotype', 'rpr_import_uc_wotype_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 117, sysdate, user, 'Assign : Unit Code to Func Class', 'Import Unit Code to Functional Class Assignments', 'rpr_import_uc_fc', 'rpr_import_uc_fc_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 118, sysdate, user, 'Assign : Unit Code to Utl Act Pr Ut', 'Import Unit Code to Utility Account Property Unit Assigments', 'rpr_import_uc_uapu', 'rpr_import_uc_uapu_arc', null, 0, 'nvo_rpr_logic_import', '', '' );
insert into PP_IMPORT_TYPE (IMPORT_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, IMPORT_TABLE_NAME, ARCHIVE_TABLE_NAME, PP_REPORT_FILTER_ID, ALLOW_UPDATES_ON_ADD, DELEGATE_OBJECT_NAME, ARCHIVE_ADDITIONAL_COLUMNS, AUTOCREATE_DESCRIPTION) values ( 119, sysdate, user, 'Assign : Priority to Repair Test', 'Import Priority to Repair Test Assignments', 'rpr_import_pri_test', 'rpr_import_pri_test_arc', null, 0, 'nvo_rpr_logic_import', '', '' );

insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 101, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 102, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 103, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 104, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 105, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 106, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 107, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 108, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 109, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 110, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 111, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 112, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 113, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 114, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 115, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 116, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 117, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 118, 5, sysdate, user );
insert into PP_IMPORT_TYPE_SUBSYSTEM (IMPORT_TYPE_ID, IMPORT_SUBSYSTEM_ID, TIME_STAMP, USER_ID) values ( 119, 5, sysdate, user );

insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'add_test', sysdate, user, 'Add Test', '', 0, 1, 'number(22,0)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'base_year', sysdate, user, 'Base Year', '', 0, 1, 'number(22,0)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'company_id', sysdate, user, 'Company', 'company_xlate', 1, 1, 'number(22,0)', 'company', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'effective_date', sysdate, user, 'Effective Date', '', 1, 1, 'number(22,0)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'max_proj_amt', sysdate, user, 'Max Project Amount', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'repair_location_id', sysdate, user, 'Repair Location', 'repair_location_xlate', 1, 1, 'number(22,0)', 'repair_location', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'repair_unit_code_id', sysdate, user, 'Repair Unit Code', 'repair_unit_code_xlate', 1, 1, 'number(22,0)', 'repair_unit_code', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'replacement_cost', sysdate, user, 'Replacement Cost', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'replacement_quantity', sysdate, user, 'Replacement Quantity', '', 0, 1, 'number(22,0)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'retire_test', sysdate, user, 'Retire Test', '', 0, 1, 'number(22,0)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'unit_code_ratio', sysdate, user, 'Unit Code Ratio', '', 0, 1, 'number(22,8)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 101, 'unit_replace_cost', sysdate, user, 'Unit Replace Cost', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 102, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 103, 'high_range', sysdate, user, 'High Range', '', 1, 1, 'number(22,8)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 103, 'low_range', sysdate, user, 'Low Range', '', 1, 1, 'number(22,8)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 103, 'range_test_id', sysdate, user, 'Range Test', 'range_test_xlate', 1, 1, 'number(22,0)', 'repair_range_test_header', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 103, 'tax_status_id', sysdate, user, 'Tax Status', 'tax_status_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 104, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 104, 'long_description', sysdate, user, 'Long Description', '', 0, 1, 'varchar2(254)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 104, 'status', sysdate, user, 'Status', 'status_xlate', 1, 1, 'number(22,0)', 'status_code', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 104, 'usage_flag', sysdate, user, 'Usage Flag', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'add_retire_circuit_pct', sysdate, user, 'Add Retire Circuit Percent', '', 0, 1, 'number(22,8)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'add_retire_tolerance_pct', sysdate, user, 'Add Retire Tolerance Percent', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'blanket_method', sysdate, user, 'Blanket Method', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'est_revision', sysdate, user, 'Est Revision', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'estimate_dollars_vs_units', sysdate, user, 'Estimate Dollars vs Units', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'expense_percent', sysdate, user, 'Expense Percent', '', 0, 1, 'number(22,2)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'quantity_vs_dollars', sysdate, user, 'Quantity vs Dollars', '', 0, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'repair_schema_id', sysdate, user, 'Repair Schema', 'repair_schema_xlate', 1, 1, 'number(22,0)', 'repair_schema', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_default_capital', sysdate, user, 'Tax Status Default Capital', 'tax_status_default_cap_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_fp_fully_qualifying', sysdate, user, 'Tax Status FP Fully Qualifying', 'tax_status_fp_fully_qual_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_fp_non_qualifying', sysdate, user, 'Tax Status FP Non  Qualifying', 'tax_status_fp_non_qual_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_from_fp', sysdate, user, 'Tax Status from FP', 'tax_status_from_fp_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_minor_uop', sysdate, user, 'Tax Status Minor Unit of Property', 'tax_status_minor_uop_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_no_replacement', sysdate, user, 'Tax Status No Replacement', 'tax_status_no_replace_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_over_max_amount', sysdate, user, 'Tax Status Over Max Amount', 'tax_status_over_max_amt_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_over_tolerance', sysdate, user, 'Tax Status Over Tolerance', 'tax_status_over_toler_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 105, 'tax_status_ovh_to_udg', sysdate, user, 'Tax Status Overhead to Underground', 'tax_status_ovh_to_udg_xlate', 0, 1, 'number(22,0)', 'wo_tax_status', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 106, 'company_id', sysdate, user, 'Company', 'company_xlate', 0, 1, 'number(22,0)', 'company', '', 0, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 106, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 106, 'long_description', sysdate, user, 'Long Description', '', 1, 1, 'varchar2(254)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 106, 'work_order_id', sysdate, user, 'Work Order', 'work_order_xlate', 0, 2, 'number(22,0)', 'work_order_control', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 107, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 107, 'is_circuit', sysdate, user, 'Is Circuit', 'is_circuit_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 107, 'long_description', sysdate, user, 'Long Description', '', 1, 1, 'varchar2(254)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 108, 'alert_review_flag', sysdate, user, 'Alert Review Flag', 'alert_review_flag_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 108, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 108, 'priority', sysdate, user, 'Priority', '', 1, 1, 'number(22,0)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 108, 'repair_schema_id', sysdate, user, 'Repair Schema', 'repair_schema_xlate', 0, 1, 'number(22,0)', 'repair_schema', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 108, 'require_review', sysdate, user, 'Require Review', 'require_review_xlate', 0, 1, 'number(22,0)', 'yes_no', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 109, 'description', sysdate, user, 'Description', '', 1, 1, 'varchar2(35)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 109, 'hw_table_line_id', sysdate, user, 'HW Table Line', 'hw_table_line_xlate', 0, 1, 'number(22,0)', 'handy_whitman_index', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 109, 'long_description', sysdate, user, 'Long Description', '', 1, 1, 'varchar2(254)', '', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 109, 'range_test_id', sysdate, user, 'Range Test', 'range_test_xlate', 0, 1, 'number(22,0)', 'repair_range_test_header', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 109, 'unit_of_measure_id', sysdate, user, 'Unit of Measure', 'unit_of_measure_xlate', 0, 1, 'number(22,0)', 'unit_of_measure', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 110, 'range_test_id', sysdate, user, 'Range Test', 'range_test_xlate', 1, 1, 'number(22,0)', 'repair_range_test_header', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 110, 'repair_unit_code_id', sysdate, user, 'Repair Unit Code', 'repair_unit_code_xlate', 1, 1, 'number(22,0)', 'repair_unit_code', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 111, 'tax_expense_test_id', sysdate, user, 'Repair Test', 'tax_expense_test_xlate', 1, 1, 'number(22,0)', 'wo_tax_expense_test', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 111, 'work_order_type_id', sysdate, user, 'Work Order Type', 'work_order_type_xlate', 1, 1, 'number(22,0)', 'work_order_type', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 112, 'repair_loc_rollup_id', sysdate, user, 'Repair Location Rollup', 'repair_loc_rollup_xlate', 1, 1, 'number(22,0)', 'repair_loc_rollup', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 112, 'repair_location_id', sysdate, user, 'Repair Location', 'repair_location_xlate', 1, 1, 'number(22,0)', 'repair_location', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 113, 'func_class_id', sysdate, user, 'Functional Class', 'func_class_xlate', 1, 1, 'number(22,0)', 'func_class', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 113, 'repair_loc_rollup_id', sysdate, user, 'Repair Location Rollup', 'repair_loc_rollup_xlate', 1, 1, 'number(22,0)', 'repair_loc_rollup', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 114, 'asset_location_id', sysdate, user, 'Asset Location', 'asset_location_xlate', 1, 2, 'number(22,0)', 'asset_location', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 114, 'repair_location_id', sysdate, user, 'Repair Location', 'repair_location_xlate', 1, 1, 'number(22,0)', 'repair_location', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 114, 'state_id', sysdate, user, 'State', 'state_xlate', 0, 1, 'char(18)', 'state', '', 0, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 115, 'property_group_id', sysdate, user, 'Property Group', 'property_group_xlate', 1, 1, 'number(22,0)', 'property_group', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 115, 'repair_unit_code_id', sysdate, user, 'Repair Unit Code', 'repair_unit_code_xlate', 1, 1, 'number(22,0)', 'repair_unit_code', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 116, 'repair_unit_code_id', sysdate, user, 'Repair Unit Code', 'repair_unit_code_xlate', 1, 1, 'number(22,0)', 'repair_unit_code', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 116, 'work_order_type_id', sysdate, user, 'Work Order Type', 'work_order_type_xlate', 1, 1, 'number(22,0)', 'work_order_type', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 117, 'func_class_id', sysdate, user, 'Functional Class', 'func_class_xlate', 1, 1, 'number(22,0)', 'func_class', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 117, 'repair_unit_code_id', sysdate, user, 'Repair Unit Code', 'repair_unit_code_xlate', 1, 1, 'number(22,0)', 'repair_unit_code', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 118, 'bus_segment_id', sysdate, user, 'Business Segment', 'bus_segment_xlate', 1, 1, 'number(22,0)', 'business_segment', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 118, 'property_unit_id', sysdate, user, 'Property Unit', 'property_unit_xlate', 1, 1, 'number(22,0)', 'property_unit', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 118, 'repair_unit_code_id', sysdate, user, 'Repair Unit Code', 'repair_unit_code_xlate', 1, 1, 'number(22,0)', 'repair_unit_code', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 118, 'utility_account_id', sysdate, user, 'Utility Account', 'utility_account_xlate', 1, 2, 'number(22,0)', 'utility_account', 'bus_segment_id', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 119, 'tax_expense_test_id', sysdate, user, 'Repair Test', 'tax_expense_test_xlate', 1, 1, 'number(22,0)', 'wo_tax_expense_test', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 119, 'repair_priority_type_id', sysdate, user, 'Repair Priority Type', 'repair_priority_type_xlate', 1, 1, 'number(22,0)', 'repair_priority_type', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 119, 'repair_priority_id', sysdate, user, 'Repair Priority', 'repair_priority_xlate', 1, 2, 'number(22,0)', 'repair_priority', '', 1, null );
insert into PP_IMPORT_COLUMN (IMPORT_TYPE_ID, COLUMN_NAME, TIME_STAMP, USER_ID, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER, COLUMN_TYPE, PARENT_TABLE, PARENT_TABLE_PK_COLUMN2, IS_ON_TABLE, AUTOCREATE_IMPORT_TYPE_ID) values ( 119, 'sequence_number', sysdate, user, 'Sequence Number', '', 1, 1, 'number(22,0)', '', '', 1, null );

insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1001, sysdate, user, 'Repair Unit Code.Description', 'The passed in value corresponds to the Repair Unit Code: Description field.  Translate to the Repair Unit Code ID using the Description column on the Repair Unit Code table.', 'repair_unit_code_id', '( select ruc.repair_unit_code_id from repair_unit_code ruc where upper( trim( <importfield> ) ) = upper( trim( ruc.description ) ) )', 0, 'repair_unit_code', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1002, sysdate, user, 'Repair Unit Code.Long Description', 'The passed in value corresponds to the Repair Unit Code: Long Description field.  Translate to the Repair Unit Code ID using the Long Description column on the Repair Unit Code table.', 'repair_unit_code_id', '( select ruc.repair_unit_code_id from repair_unit_code ruc where upper( trim( <importfield> ) ) = upper( trim( ruc.long_description ) ) )', 0, 'repair_unit_code', 'long_description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1003, sysdate, user, 'Repair Location.Description', 'The passed in value corresponds to the Repair Location: Description field.  Translate to the Repair Location ID using the Description column on the Repair Location table.', 'repair_location_id', '( select rl.repair_location_id from repair_location rl where upper( trim( <importfield> ) ) = upper( trim( rl.description ) ) )', 0, 'repair_location', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1004, sysdate, user, 'Repair Location.Long Description', 'The passed in value corresponds to the Repair Location: Long Description field.  Translate to the Repair Location ID using the Long Description column on the Repair Location table.', 'repair_location_id', '( select rl.repair_location_id from repair_location rl where upper( trim( <importfield> ) ) = upper( trim( rl.long_description ) ) )', 0, 'repair_location', 'long_description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1005, sysdate, user, 'Repair Range Test.Description', 'The passed in value corresponds to the Repair Range Test: Description field.  Translate to the Range Test ID using the Description column on the Repair Range Test Header table.', 'range_test_id', '( select rt.range_test_id from repair_range_test_header rt where upper( trim( <importfield> ) ) = upper( trim( rt.description ) ) )', 0, 'repair_range_test_header', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1006, sysdate, user, 'Tax Status.Description', 'The passed in value corresponds to the Tax Status: Description field.  Translate to the Tax Status ID using the Description column on the WO Tax Status table.', 'tax_status_id', '( select ts.tax_status_id from wo_tax_status ts where upper( trim( <importfield> ) ) = upper( trim( ts.description ) ) )', 0, 'wo_tax_status', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1007, sysdate, user, 'Repair Schema.Description', 'The passed in value corresponds to the Repair Schema: Description field.  Translate to the Repair Schema ID using the Description column on the Repair Schema table.', 'repair_schema_id', '( select rs.repair_schema_id from repair_schema rs where upper( trim( <importfield> ) ) = upper( trim( rs.description ) ) )', 0, 'repair_schema', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1008, sysdate, user, 'Repair Schema.Long Description', 'The passed in value corresponds to the Repair Schema: Long Description field.  Translate to the Repair Schema ID using the Long Description column on the Repair Schema table.', 'repair_schema_id', '( select rs.repair_schema_id from repair_schema rs where upper( trim( <importfield> ) ) = upper( trim( rs.long_description ) ) )', 0, 'repair_schema', 'long_description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1009, sysdate, user, 'Tax Repair Test.Description', 'The passed in value corresponds to the Tax Repair Test: Description field.  Translate to the Tax Repair Test ID using the Description column on the Tax Repair Test table.', 'tax_expense_test_id', '( select tet.tax_expense_test_id from wo_tax_expense_test tet where upper( trim( <importfield> ) ) = upper( trim( tet.description ) ) )', 0, 'wo_tax_expense_test', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1010, sysdate, user, 'Repair Location Rollup.Description', 'The passed in value corresponds to the Repair Location Rollup: Description field.  Translate to the Repair Location Rollup ID using the Description column on the Repair Location Rollup table.', 'repair_loc_rollup_id', '( select rlr.repair_loc_rollup_id from repair_loc_rollup rlr where upper( trim( <importfield> ) ) = upper( trim( rlr.description ) ) )', 0, 'repair_loc_rollup', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1011, sysdate, user, 'Repair Location Rollup.Long Description', 'The passed in value corresponds to the Repair Location Rollup: Long Description field.  Translate to the Repair Location Rollup ID using the Long Description column on the Repair Location Rollup table.', 'repair_loc_rollup_id', '( select rlr.repair_loc_rollup_id from repair_loc_rollup rlr where upper( trim( <importfield> ) ) = upper( trim( rlr.long_description ) ) )', 0, 'repair_loc_rollup', 'long_description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1012, sysdate, user, 'Handy Whitman Index.Description', 'The passed in value corresponds to the Handy Whitman Index: Description field.  Translate to the HW Table Line ID using the Description column on the Handy Whitman Index table.', 'hw_table_line_id', '( select hw.hw_table_line_id from handy_whitman_index hw where upper( trim( <importfield> ) ) = upper( trim( hw.description ) ) )', 0, 'handy_whitman_index', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1013, sysdate, user, 'Unit of Measure.Description', 'The passed in value corresponds to the Unit of Measure: Description field.  Translate to the Unit of Measure ID using the Description column on the Unit of Measure table.', 'unit_of_measure_id', '( select um.unit_of_measure_id from unit_of_measure um where upper( trim( <importfield> ) ) = upper( trim( um.description ) ) )', 0, 'unit_of_measure', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1014, sysdate, user, 'Unit of Measure.Long Description', 'The passed in value corresponds to the Unit of Measure: Long Description field.  Translate to the Unit of Measure ID using the Long Description column on the Unit of Measure table.', 'unit_of_measure_id', '( select um.unit_of_measure_id from unit_of_measure um where upper( trim( <importfield> ) ) = upper( trim( um.long_description ) ) )', 0, 'unit_of_measure', 'long_description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1015, sysdate, user, 'Work Order Type.Description', 'The passed in value corresponds to the Work Order Type: Description field.  Translate to the Work Order Type ID using the Description column on the Work Order Type table.', 'work_order_type_id', '( select wot.work_order_type_id from work_order_type wot where upper( trim( <importfield> ) ) = upper( trim( wot.description ) ) )', 0, 'work_order_type', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1016, sysdate, user, 'Work Order Type.Long Description', 'The passed in value corresponds to the Work Order Type: Long Description field.  Translate to the Work Order Type ID using the Long Description column on the Work Order Type table.', 'work_order_type_id', '( select wot.work_order_type_id from work_order_type wot where upper( trim( <importfield> ) ) = upper( trim( wot.long_description ) ) )', 0, 'work_order_type', 'long_description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1017, sysdate, user, 'Work Order Type.External Work Order Type', 'The passed in value corresponds to the Work Order Type: External Work Order Type field.  Translate to the Work Order Type ID using the External Work Order Type column on the Work Order Type table.', 'work_order_type_id', '( select wot.work_order_type_id from work_order_type wot where upper( trim( <importfield> ) ) = upper( trim( wot.external_work_order_type ) ) )', 0, 'work_order_type', 'external_work_order_type', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1018, sysdate, user, 'Repair Priority Type.Description', 'The passed in value corresponds to the Repair Priority Type: Description field.  Translate to the Repair Priority Type ID using the Description column on the Repair Priority Type table.', 'repair_priority_type_id', '( select rpt.repair_priority_type_id from repair_priority_type rpt where upper( trim( <importfield> ) ) = upper( trim( rpt.description ) ) )', 0, 'repair_priority_type', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1019, sysdate, user, 'Repair Priority.Description', 'The passed in value corresponds to the Repair Priority: Description field.  Translate to the Repair Priority ID using the Description column on the Repair Priority table.', 'repair_priority_id', '( select rp.repair_priority_id from repair_priority rp where upper( trim( <importfield> ) ) = upper( trim( rp.description ) ) )', 0, 'repair_priority', 'description', '', '', null );
insert into PP_IMPORT_LOOKUP (IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED, LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME, LOOKUP_CONSTRAINING_COLUMNS, LOOKUP_VALUES_ALTERNATE_SQL, DERIVED_AUTOCREATE_YN) values ( 1020, sysdate, user, 'Repair Priority.Repair Priority Code', 'The passed in value corresponds to the Repair Priority: Repair Priority Code field.  Translate to the Repair Priority ID using the Repair Priority Code column on the Repair Priority table.', 'repair_priority_id', '( select rp.repair_priority_id from repair_priority rp where upper( trim( <importfield> ) ) = upper( trim( rp.repair_priority_code ) ) )', 0, 'repair_priority', 'repair_priority_code', '', '', null );

insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 101, 'company_id', 19, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 101, 'company_id', 20, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 101, 'company_id', 21, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 101, 'repair_location_id', 1003, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 101, 'repair_location_id', 1004, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 101, 'repair_unit_code_id', 1001, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 101, 'repair_unit_code_id', 1002, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 103, 'range_test_id', 1005, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 103, 'tax_status_id', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 104, 'status', 138, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 104, 'status', 139, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'repair_schema_id', 1007, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'repair_schema_id', 1008, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_default_capital', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_fp_fully_qualifying', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_fp_non_qualifying', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_from_fp', 77, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_from_fp', 78, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_minor_uop', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_no_replacement', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_over_max_amount', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_over_tolerance', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 'tax_status_ovh_to_udg', 1006, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 106, 'company_id', 19, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 106, 'company_id', 20, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 106, 'company_id', 21, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 106, 'work_order_id', 110, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 106, 'work_order_id', 111, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 107, 'is_circuit', 77, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 107, 'is_circuit', 78, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 108, 'alert_review_flag', 77, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 108, 'alert_review_flag', 78, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 108, 'repair_schema_id', 1007, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 108, 'repair_schema_id', 1008, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 108, 'require_review', 77, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 108, 'require_review', 78, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 109, 'hw_table_line_id', 1012, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 109, 'range_test_id', 1005, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 109, 'unit_of_measure_id', 1013, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 109, 'unit_of_measure_id', 1014, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 110, 'range_test_id', 1005, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 110, 'repair_unit_code_id', 1001, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 110, 'repair_unit_code_id', 1002, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 111, 'tax_expense_test_id', 1009, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 111, 'work_order_type_id', 1015, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 111, 'work_order_type_id', 1016, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 111, 'work_order_type_id', 1017, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 112, 'repair_loc_rollup_id', 1010, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 112, 'repair_loc_rollup_id', 1011, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 112, 'repair_location_id', 1003, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 112, 'repair_location_id', 1004, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 113, 'func_class_id', 190, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 113, 'repair_loc_rollup_id', 1010, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 113, 'repair_loc_rollup_id', 1011, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 88, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 89, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 90, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 91, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 92, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 93, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 94, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 95, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 96, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 97, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 98, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'asset_location_id', 99, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'repair_location_id', 1003, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'repair_location_id', 1004, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'state_id', 1, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 114, 'state_id', 2, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 115, 'property_group_id', 618, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 115, 'property_group_id', 619, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 115, 'repair_unit_code_id', 1001, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 115, 'repair_unit_code_id', 1002, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 116, 'repair_unit_code_id', 1001, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 116, 'repair_unit_code_id', 1002, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 116, 'work_order_type_id', 1015, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 116, 'work_order_type_id', 1016, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 116, 'work_order_type_id', 1017, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 117, 'func_class_id', 190, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 117, 'repair_unit_code_id', 1001, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 117, 'repair_unit_code_id', 1002, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'bus_segment_id', 25, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'bus_segment_id', 26, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'property_unit_id', 193, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'property_unit_id', 194, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'property_unit_id', 195, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'repair_unit_code_id', 1001, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'repair_unit_code_id', 1002, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'utility_account_id', 27, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'utility_account_id', 28, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'utility_account_id', 29, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 118, 'utility_account_id', 30, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 119, 'tax_expense_test_id', 1009, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 119, 'repair_priority_type_id', 1018, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 119, 'repair_priority_id', 1019, sysdate, user );
insert into PP_IMPORT_COLUMN_LOOKUP (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 119, 'repair_priority_id', 1020, sysdate, user );

insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 104, 1007, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 104, 1008, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 105, 1009, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 106, 1010, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 106, 1011, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 107, 1003, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 107, 1004, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 108, 1006, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 109, 1001, sysdate, user );
insert into PP_IMPORT_TYPE_UPDATES_LOOKUP (IMPORT_TYPE_ID, IMPORT_LOOKUP_ID, TIME_STAMP, USER_ID) values ( 109, 1002, sysdate, user );

--
-- Create the tables.
--

--
-- THRESHOLDS
--
create table RPR_IMPORT_THRESHOLD
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 REPAIR_LOCATION_XLATE  varchar2(254),
 REPAIR_LOCATION_ID     number(22,0),
 COMPANY_XLATE          varchar2(254),
 COMPANY_ID             number(22,0),
 EFFECTIVE_DATE         varchar2(35),
 ADD_TEST               varchar2(35),
 RETIRE_TEST            varchar2(35),
 UNIT_CODE_RATIO        varchar2(35),
 REPLACEMENT_COST       varchar2(35),
 BASE_YEAR              varchar2(35),
 REPLACEMENT_QUANTITY   varchar2(35),
 MAX_PROJ_AMT           varchar2(35),
 UNIT_REPLACE_COST      varchar2(35),
 IS_MODIFIED            number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_THRESHOLD
   add constraint RPR_IMP_THRSHLD_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_THRESHOLD
   add constraint RPR_IMP_THRSHLD_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_THRESHOLD_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 REPAIR_LOCATION_XLATE  varchar2(254),
 REPAIR_LOCATION_ID     number(22,0),
 COMPANY_XLATE          varchar2(254),
 COMPANY_ID             number(22,0),
 EFFECTIVE_DATE         varchar2(35),
 ADD_TEST               varchar2(35),
 RETIRE_TEST            varchar2(35),
 UNIT_CODE_RATIO        varchar2(35),
 REPLACEMENT_COST       varchar2(35),
 BASE_YEAR              varchar2(35),
 REPLACEMENT_QUANTITY   varchar2(35),
 MAX_PROJ_AMT           varchar2(35),
 UNIT_REPLACE_COST      varchar2(35)
);

alter table RPR_IMPORT_THRESHOLD_ARC
   add constraint RPR_IMP_THRSHLD_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_THRESHOLD_ARC
   add constraint RPR_IMP_THRSHLD_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references pp_import_run;

--
-- RANGE TESTS
--
create table RPR_IMPORT_RANGE_TEST
(
 IMPORT_RUN_ID number(22,0)   not null,
 LINE_ID       number(22,0)   not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 DESCRIPTION   varchar2(254),
 RANGE_TEST_ID number(22,0),
 ERROR_MESSAGE varchar2(4000)
);

alter table RPR_IMPORT_RANGE_TEST
   add constraint RPR_IMP_RNG_TST_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RANGE_TEST
   add constraint RPR_IMP_RNG_TST_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_RANGE_TEST_ARC
(
 IMPORT_RUN_ID number(22,0)   not null,
 LINE_ID       number(22,0)   not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 DESCRIPTION   varchar2(254),
 RANGE_TEST_ID number(22,0)
);

alter table RPR_IMPORT_RANGE_TEST_ARC
   add constraint RPR_IMP_RNG_TST_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RANGE_TEST_ARC
   add constraint RPR_IMP_RNG_TST_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- RANGE TEST RANGES
--
create table RPR_IMPORT_RANGE_TEST_RNG
(
 IMPORT_RUN_ID    number(22,0)   not null,
 LINE_ID          number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 RANGE_TEST_XLATE varchar2(254),
 RANGE_TEST_ID    number(22,0),
 LOW_RANGE        varchar2(35),
 HIGH_RANGE       varchar2(35),
 TAX_STATUS_XLATE varchar2(254),
 TAX_STATUS_ID    number(22,0),
 ERROR_MESSAGE    varchar2(4000)
);

alter table RPR_IMPORT_RANGE_TEST_RNG
   add constraint RPR_IMP_RT_RNG_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RANGE_TEST_RNG
   add constraint RPR_IMP_RT_RNG_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_RANGE_TEST_RNG_ARC
(
 IMPORT_RUN_ID    number(22,0)   not null,
 LINE_ID          number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 RANGE_TEST_XLATE varchar2(254),
 RANGE_TEST_ID    number(22,0),
 LOW_RANGE        varchar2(35),
 HIGH_RANGE       varchar2(35),
 TAX_STATUS_XLATE varchar2(254),
 TAX_STATUS_ID    number(22,0)
);

alter table RPR_IMPORT_RANGE_TEST_RNG_ARC
   add constraint RPR_IMP_RT_RNG_RG_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RANGE_TEST_RNG_ARC
   add constraint RPR_IMP_RT_RNG_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- REPAIR SCHEMAS
--
create table RPR_IMPORT_SCHEMA
(
 IMPORT_RUN_ID    number(22,0)   not null,
 LINE_ID          number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(254),
 LONG_DESCRIPTION varchar2(254),
 STATUS_XLATE     varchar2(254),
 STATUS           number(22,0),
 USAGE_FLAG       varchar2(254),
 REPAIR_SCHEMA_ID number(22,0),
 IS_MODIFIED      number(22,0),
 ERROR_MESSAGE    varchar2(4000)
);

alter table RPR_IMPORT_SCHEMA
   add constraint RPR_IMP_SCHM_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_SCHEMA
   add constraint RPR_IMP_SCHM_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_SCHEMA_ARC
(
 IMPORT_RUN_ID    number(22,0)   not null,
 LINE_ID          number(22,0)   not null,
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 DESCRIPTION      varchar2(254),
 LONG_DESCRIPTION varchar2(254),
 STATUS_XLATE     varchar2(254),
 STATUS           number(22,0),
 USAGE_FLAG       varchar2(254),
 REPAIR_SCHEMA_ID number(22,0)
);

alter table RPR_IMPORT_SCHEMA_ARC
   add constraint RPR_IMP_SCHM_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_SCHEMA_ARC
   add constraint RPR_IMP_SCHM_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- REPAIR TESTS
--
create table RPR_IMPORT_TEST
(
 IMPORT_RUN_ID                  number(22,0)   not null,
 LINE_ID                        number(22,0)   not null,
 TIME_STAMP                     date,
 USER_ID                        varchar2(18),
 DESCRIPTION                    varchar2(254),
 TAX_STATUS_FROM_FP_XLATE       varchar2(254),
 TAX_STATUS_FROM_FP             number(22,0),
 TAX_STATUS_NO_REPLACE_XLATE    varchar2(254),
 TAX_STATUS_NO_REPLACEMENT      number(22,0),
 REPAIR_SCHEMA_XLATE            varchar2(254),
 REPAIR_SCHEMA_ID               number(22,0),
 TAX_STATUS_OVER_MAX_AMT_XLATE  varchar2(254),
 TAX_STATUS_OVER_MAX_AMOUNT     number(22,0),
 TAX_STATUS_OVH_TO_UDG_XLATE    varchar2(254),
 TAX_STATUS_OVH_TO_UDG          number(22,0),
 TAX_STATUS_MINOR_UOP_XLATE     varchar2(254),
 TAX_STATUS_MINOR_UOP           number(22,0),
 TAX_STATUS_OVER_TOLER_XLATE    varchar2(254),
 TAX_STATUS_OVER_TOLERANCE      number(22,0),
 TAX_STATUS_FP_FULLY_QUAL_XLATE varchar2(254),
 TAX_STATUS_FP_FULLY_QUALIFYING number(22,0),
 TAX_STATUS_FP_NON_QUAL_XLATE   varchar2(254),
 TAX_STATUS_FP_NON_QUALIFYING   number(22,0),
 TAX_STATUS_DEFAULT_CAP_XLATE   varchar2(254),
 TAX_STATUS_DEFAULT_CAPITAL     number(22,0),
 ESTIMATE_DOLLARS_VS_UNITS      varchar2(254),
 EST_REVISION                   varchar2(254),
 QUANTITY_VS_DOLLARS            varchar2(254),
 ADD_RETIRE_TOLERANCE_PCT       varchar2(35),
 BLANKET_METHOD                 varchar2(254),
 EXPENSE_PERCENT                varchar2(35),
 ADD_RETIRE_CIRCUIT_PCT         varchar2(35),
 TAX_EXPENSE_TEST_ID            number(22,0),
 IS_MODIFIED                    number(22,0),
 ERROR_MESSAGE                  varchar2(4000)
);

alter table RPR_IMPORT_TEST
   add constraint rpr_imp_tst_pk
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_TEST
   add constraint RPR_IMP_TST_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_TEST_ARC
(
 IMPORT_RUN_ID                  number(22,0)   not null,
 LINE_ID                        number(22,0)   not null,
 TIME_STAMP                     date,
 USER_ID                        varchar2(18),
 DESCRIPTION                    varchar2(254),
 TAX_STATUS_FROM_FP_XLATE       varchar2(254),
 TAX_STATUS_FROM_FP             number(22,0),
 TAX_STATUS_NO_REPLACE_XLATE    varchar2(254),
 TAX_STATUS_NO_REPLACEMENT      number(22,0),
 REPAIR_SCHEMA_XLATE            varchar2(254),
 REPAIR_SCHEMA_ID               number(22,0),
 TAX_STATUS_OVER_MAX_AMT_XLATE  varchar2(254),
 TAX_STATUS_OVER_MAX_AMOUNT     number(22,0),
 TAX_STATUS_OVH_TO_UDG_XLATE    varchar2(254),
 TAX_STATUS_OVH_TO_UDG          number(22,0),
 TAX_STATUS_MINOR_UOP_XLATE     varchar2(254),
 TAX_STATUS_MINOR_UOP           number(22,0),
 TAX_STATUS_OVER_TOLER_XLATE    varchar2(254),
 TAX_STATUS_OVER_TOLERANCE      number(22,0),
 TAX_STATUS_FP_FULLY_QUAL_XLATE varchar2(254),
 TAX_STATUS_FP_FULLY_QUALIFYING number(22,0),
 TAX_STATUS_FP_NON_QUAL_XLATE   varchar2(254),
 TAX_STATUS_FP_NON_QUALIFYING   number(22,0),
 TAX_STATUS_DEFAULT_CAP_XLATE   varchar2(254),
 TAX_STATUS_DEFAULT_CAPITAL     number(22,0),
 ESTIMATE_DOLLARS_VS_UNITS      varchar2(254),
 EST_REVISION                   varchar2(254),
 QUANTITY_VS_DOLLARS            varchar2(254),
 ADD_RETIRE_TOLERANCE_PCT       varchar2(35),
 BLANKET_METHOD                 varchar2(254),
 EXPENSE_PERCENT                varchar2(35),
 ADD_RETIRE_CIRCUIT_PCT         varchar2(35),
 TAX_EXPENSE_TEST_ID            number(22,0)
);

alter table RPR_IMPORT_TEST_ARC
   add constraint RPR_IMP_TST_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_TEST_ARC
   add constraint RPR_IMP_TST_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- REPAIR LOCATION ROLLUPS
--
create table RPR_IMPORT_LOC_ROLLUP
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(254),
 LONG_DESCRIPTION     varchar2(254),
 COMPANY_XLATE        varchar2(254),
 COMPANY_ID           number(22,0),
 WORK_ORDER_XLATE     varchar2(254),
 WORK_ORDER_ID        number(22,0),
 REPAIR_LOC_ROLLUP_ID number(22,0),
 IS_MODIFIED          number(22,0),
 ERROR_MESSAGE        varchar2(4000)
);

alter table RPR_IMPORT_LOC_ROLLUP
   add constraint RPR_IMP_LC_RLL_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_ROLLUP
   add constraint RPR_IMP_LC_RLL_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_LOC_ROLLUP_ARC
(
 IMPORT_RUN_ID        number(22,0)   not null,
 LINE_ID              number(22,0)   not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(254),
 LONG_DESCRIPTION     varchar2(254),
 COMPANY_XLATE        varchar2(254),
 COMPANY_ID           number(22,0),
 WORK_ORDER_XLATE     varchar2(254),
 WORK_ORDER_ID        number(22,0),
 REPAIR_LOC_ROLLUP_ID number(22,0)
);

alter table RPR_IMPORT_LOC_ROLLUP_ARC
   add constraint RPR_IMP_LC_RLL_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_ROLLUP_ARC
   add constraint RPR_IMP_LC_RLL_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- REPAIR LOCATIONS
--
create table RPR_IMPORT_LOCATION
(
 IMPORT_RUN_ID      number(22,0)   not null,
 LINE_ID            number(22,0)   not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(254),
 LONG_DESCRIPTION   varchar2(254),
 IS_CIRCUIT_XLATE   varchar2(254),
 IS_CIRCUIT         number(22,0),
 REPAIR_LOCATION_ID number(22,0),
 IS_MODIFIED        number(22,0),
 ERROR_MESSAGE      varchar2(4000)
);

alter table RPR_IMPORT_LOCATION
   add constraint RPR_IMP_LC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOCATION
   add constraint RPR_IMP_LC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_LOCATION_ARC
(
 IMPORT_RUN_ID      number(22,0)   not null,
 LINE_ID            number(22,0)   not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(254),
 LONG_DESCRIPTION   varchar2(254),
 IS_CIRCUIT_XLATE   varchar2(254),
 IS_CIRCUIT         number(22,0),
 REPAIR_LOCATION_ID number(22,0)
);

alter table RPR_IMPORT_LOCATION_ARC
   add constraint RPR_IMP_LC_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOCATION_ARC
   add constraint RPR_IMP_LC_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- TAX STATUSES
--
create table RPR_IMPORT_TAX_STATUS
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DESCRIPTION             varchar2(254),
 REPAIR_SCHEMA_XLATE     varchar2(254),
 REPAIR_SCHEMA_ID        number(22,0),
 PRIORITY                varchar2(35),
 ALERT_REVIEW_FLAG_XLATE varchar2(254),
 ALERT_REVIEW_FLAG       number(22,0),
 REQUIRE_REVIEW_XLATE    varchar2(254),
 REQUIRE_REVIEW          number(22,0),
 TAX_STATUS_ID           number(22,0),
 IS_MODIFIED             number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table RPR_IMPORT_TAX_STATUS
   add constraint RPR_IMP_TX_STAT_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_TAX_STATUS
   add constraint RPR_IMP_TX_STAT_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_TAX_STATUS_ARC
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 DESCRIPTION             varchar2(254),
 REPAIR_SCHEMA_XLATE     varchar2(254),
 REPAIR_SCHEMA_ID        number(22,0),
 PRIORITY                varchar2(35),
 ALERT_REVIEW_FLAG_XLATE varchar2(254),
 ALERT_REVIEW_FLAG       number(22,0),
 REQUIRE_REVIEW_XLATE    varchar2(254),
 REQUIRE_REVIEW          number(22,0),
 TAX_STATUS_ID           number(22,0)
);

alter table RPR_IMPORT_TAX_STATUS_ARC
   add constraint RPR_IMP_TX_STAT_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_TAX_STATUS_ARC
   add constraint RPR_IMP_TX_STAT_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- REPAIR UNIT CODES
--
create table RPR_IMPORT_UNIT_CODE
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(254),
 LONG_DESCRIPTION      varchar2(254),
 RANGE_TEST_XLATE      varchar2(254),
 RANGE_TEST_ID         number(22,0),
 HW_TABLE_LINE_XLATE   varchar2(254),
 HW_TABLE_LINE_ID      number(22,0),
 UNIT_OF_MEASURE_XLATE varchar2(254),
 UNIT_OF_MEASURE_ID    number(22,0),
 REPAIR_UNIT_CODE_ID   number(22,0),
 IS_MODIFIED           number(22,0),
 ERROR_MESSAGE         varchar2(4000)
);

alter table RPR_IMPORT_UNIT_CODE
   add constraint RPR_IMP_UNT_CD_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UNIT_CODE
   add constraint RPR_IMP_UNT_CD_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_UNIT_CODE_ARC
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 DESCRIPTION           varchar2(254),
 LONG_DESCRIPTION      varchar2(254),
 RANGE_TEST_XLATE      varchar2(254),
 RANGE_TEST_ID         number(22,0),
 HW_TABLE_LINE_XLATE   varchar2(254),
 HW_TABLE_LINE_ID      number(22,0),
 UNIT_OF_MEASURE_XLATE varchar2(254),
 UNIT_OF_MEASURE_ID    number(22,0),
 REPAIR_UNIT_CODE_ID   number(22,0)
);

alter table RPR_IMPORT_UNIT_CODE_ARC
   add constraint RPR_IMP_UNT_CD_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UNIT_CODE_ARC
   add constraint RPR_IMP_UNT_CD_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Range Test/Unit Code
--
create table RPR_IMPORT_RNG_TST_UNT_CD
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 RANGE_TEST_XLATE       varchar2(254),
 RANGE_TEST_ID          number(22,0),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_RNG_TST_UNT_CD
   add constraint RPR_IMP_RT_UC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RNG_TST_UNT_CD
   add constraint RPR_IMP_RTT_UC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_RNG_TST_UNT_CD_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 RANGE_TEST_XLATE       varchar2(254),
 RANGE_TEST_ID          number(22,0),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0)
);

alter table RPR_IMPORT_RNG_TST_UNT_CD_ARC
   add constraint RPR_IMP_RT_UC_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_RNG_TST_UNT_CD_ARC
   add constraint RPR_IMP_RTT_UC_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Repair Test/Work Order Type
--
create table RPR_IMPORT_TST_WOTYPE
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 TAX_EXPENSE_TEST_XLATE varchar2(254),
 TAX_EXPENSE_TEST_ID    number(22,0),
 WORK_ORDER_TYPE_XLATE  varchar2(254),
 WORK_ORDER_TYPE_ID     number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_TST_WOTYPE
   add constraint RPR_IMP_TST_WOT_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_TST_WOTYPE
   add constraint RPR_IMP_TST_WOT_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_TST_WOTYPE_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 TAX_EXPENSE_TEST_XLATE varchar2(254),
 TAX_EXPENSE_TEST_ID    number(22,0),
 WORK_ORDER_TYPE_XLATE  varchar2(254),
 WORK_ORDER_TYPE_ID     number(22,0)
);

alter table RPR_IMPORT_TST_WOTYPE_ARC
   add constraint RPR_IMP_TST_WOT_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_TST_WOTYPE_ARC
   add constraint RPR_IMP_TST_WOT_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Location Rollup/Location
--
create table RPR_IMPORT_LOC_ROLL_LOC
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 REPAIR_LOC_ROLLUP_XLATE varchar2(254),
 REPAIR_LOC_ROLLUP_ID    number(22,0),
 REPAIR_LOCATION_XLATE   varchar2(254),
 REPAIR_LOCATION_ID      number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table RPR_IMPORT_LOC_ROLL_LOC
   add constraint RPR_IMP_RLR_LOC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_ROLL_LOC
   add constraint RPR_IMP_RLR_LOC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_LOC_ROLL_LOC_ARC
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 REPAIR_LOC_ROLLUP_XLATE varchar2(254),
 REPAIR_LOC_ROLLUP_ID    number(22,0),
 REPAIR_LOCATION_XLATE   varchar2(254),
 REPAIR_LOCATION_ID      number(22,0)
);

alter table RPR_IMPORT_LOC_ROLL_LOC_ARC
   add constraint RPR_IMP_RLR_LOC_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_ROLL_LOC_ARC
   add constraint RPR_IMP_RLR_LOC_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Location Rollup/Functional Class
--
create table RPR_IMPORT_LOC_ROLL_FC
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 REPAIR_LOC_ROLLUP_XLATE varchar2(254),
 REPAIR_LOC_ROLLUP_ID    number(22,0),
 FUNC_CLASS_XLATE        varchar2(254),
 FUNC_CLASS_ID           number(22,0),
 ERROR_MESSAGE           varchar2(4000)
);

alter table RPR_IMPORT_LOC_ROLL_FC
   add constraint RPR_IMP_RLR_FC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_ROLL_FC
   add constraint RPR_IMP_RLR_FC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_LOC_ROLL_FC_ARC
(
 IMPORT_RUN_ID           number(22,0)   not null,
 LINE_ID                 number(22,0)   not null,
 TIME_STAMP              date,
 USER_ID                 varchar2(18),
 REPAIR_LOC_ROLLUP_XLATE varchar2(254),
 REPAIR_LOC_ROLLUP_ID    number(22,0),
 FUNC_CLASS_XLATE        varchar2(254),
 FUNC_CLASS_ID           number(22,0)
);

alter table RPR_IMPORT_LOC_ROLL_FC_ARC
   add constraint RPR_IMP_RLR_FC_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_ROLL_FC_ARC
   add constraint RPR_IMP_RLR_FC_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Location/Asset Location
--
create table RPR_IMPORT_LOC_AST_LOC
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 REPAIR_LOCATION_XLATE varchar2(254),
 REPAIR_LOCATION_ID    number(22,0),
 ASSET_LOCATION_XLATE  varchar2(254),
 ASSET_LOCATION_ID     number(22,0),
 STATE_XLATE           varchar2(254),
 STATE_ID              char(18),
 ERROR_MESSAGE         varchar2(4000)
);

alter table RPR_IMPORT_LOC_AST_LOC
   add constraint RPR_IMP_LOC_AL_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_AST_LOC
   add constraint RPR_IMP_LOC_AL_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_LOC_AST_LOC_ARC
(
 IMPORT_RUN_ID         number(22,0)   not null,
 LINE_ID               number(22,0)   not null,
 TIME_STAMP            date,
 USER_ID               varchar2(18),
 REPAIR_LOCATION_XLATE varchar2(254),
 REPAIR_LOCATION_ID    number(22,0),
 ASSET_LOCATION_XLATE  varchar2(254),
 ASSET_LOCATION_ID     number(22,0),
 STATE_XLATE           varchar2(254),
 STATE_ID              char(18)
);

alter table RPR_IMPORT_LOC_AST_LOC_ARC
   add constraint RPR_IMP_LOC_AL_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_LOC_AST_LOC_ARC
   add constraint RPR_IMP_LOC_AL_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Unit Code/Property Group
--
create table RPR_IMPORT_UC_PRP_GRP
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 PROPERTY_GROUP_XLATE   varchar2(254),
 PROPERTY_GROUP_ID      number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_UC_PRP_GRP
   add constraint RPR_IMP_UC_PG_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_PRP_GRP
   add constraint RPR_IMP_UC_PG_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_UC_PRP_GRP_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 PROPERTY_GROUP_XLATE   varchar2(254),
 PROPERTY_GROUP_ID      number(22,0)
);

alter table RPR_IMPORT_UC_PRP_GRP_ARC
   add constraint RPR_IMP_UC_PG_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_PRP_GRP_ARC
   add constraint RPR_IMP_UC_PG_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Unit Code/Work Order Type
--
create table RPR_IMPORT_UC_WOTYPE
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 WORK_ORDER_TYPE_XLATE  varchar2(254),
 WORK_ORDER_TYPE_ID     number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_UC_WOTYPE
   add constraint RPR_IMP_UC_WOT_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_WOTYPE
   add constraint RPR_IMP_UC_WOT_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_UC_WOTYPE_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 WORK_ORDER_TYPE_XLATE  varchar2(254),
 WORK_ORDER_TYPE_ID     number(22,0)
);

alter table RPR_IMPORT_UC_WOTYPE_ARC
   add constraint RPR_IMP_UC_WOT_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_WOTYPE_ARC
   add constraint RPR_IMP_UC_WOT_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Unit Code/Functional Class
--
create table RPR_IMPORT_UC_FC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 FUNC_CLASS_XLATE       varchar2(254),
 FUNC_CLASS_ID          number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_UC_FC
   add constraint RPR_IMP_UC_FC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_FC
   add constraint RPR_IMP_UC_FC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_UC_FC_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 FUNC_CLASS_XLATE       varchar2(254),
 FUNC_CLASS_ID          number(22,0)
);

alter table RPR_IMPORT_UC_FC_ARC
   add constraint RPR_IMP_UC_FC_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_FC_ARC
   add constraint RPR_IMP_UC_FC_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Unit Code/Utility Account Property Unit
--
create table RPR_IMPORT_UC_UAPU
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 BUS_SEGMENT_XLATE      varchar2(254),
 BUS_SEGMENT_ID         number(22,0),
 UTILITY_ACCOUNT_XLATE  varchar2(254),
 UTILITY_ACCOUNT_ID     number(22,0),
 PROPERTY_UNIT_XLATE    varchar2(254),
 PROPERTY_UNIT_ID       number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_UC_UAPU
   add constraint RPR_IMP_UC_UAPU_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_UAPU
   add constraint RPR_IMP_UC_UAPU_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_UC_UAPU_ARC
(
 IMPORT_RUN_ID          number(22,0)   not null,
 LINE_ID                number(22,0)   not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 REPAIR_UNIT_CODE_XLATE varchar2(254),
 REPAIR_UNIT_CODE_ID    number(22,0),
 BUS_SEGMENT_XLATE      varchar2(254),
 BUS_SEGMENT_ID         number(22,0),
 UTILITY_ACCOUNT_XLATE  varchar2(254),
 UTILITY_ACCOUNT_ID     number(22,0),
 PROPERTY_UNIT_XLATE    varchar2(254),
 PROPERTY_UNIT_ID       number(22,0),
 ERROR_MESSAGE          varchar2(4000)
);

alter table RPR_IMPORT_UC_UAPU_ARC
   add constraint RPR_IMP_UC_UAPU_ARC_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_UC_UAPU_ARC
   add constraint RPR_IMP_UC_UAPU_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Repair Test/Priority
--
create table RPR_IMPORT_PRI_TEST
(
 IMPORT_RUN_ID              number(22,0)   not null,
 LINE_ID                    number(22,0)   not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 TAX_EXPENSE_TEST_XLATE     varchar2(254),
 TAX_EXPENSE_TEST_ID        number(22,0),
 REPAIR_PRIORITY_TYPE_XLATE varchar2(254),
 REPAIR_PRIORITY_TYPE_ID    number(22,0),
 REPAIR_PRIORITY_XLATE      varchar2(254),
 REPAIR_PRIORITY_ID         number(22,0),
 SEQUENCE_NUMBER            varchar2(35),
 ERROR_MESSAGE              varchar2(4000)
);

alter table RPR_IMPORT_PRI_TEST
   add constraint RPR_IMP_PRI_TST_PK
       primary key (IMPORT_RUN_ID, LINE_ID)
       using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_PRI_TEST
   add constraint RPR_IMP_PRI_TST_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

create table RPR_IMPORT_PRI_TEST_ARC
(
 IMPORT_RUN_ID              number(22,0)   not null,
 LINE_ID                    number(22,0)   not null,
 TIME_STAMP                 date,
 USER_ID                    varchar2(18),
 TAX_EXPENSE_TEST_XLATE     varchar2(254),
 TAX_EXPENSE_TEST_ID        number(22,0),
 REPAIR_PRIORITY_TYPE_XLATE varchar2(254),
 REPAIR_PRIORITY_TYPE_ID    number(22,0),
 REPAIR_PRIORITY_XLATE      varchar2(254),
 REPAIR_PRIORITY_ID         number(22,0),
 SEQUENCE_NUMBER            varchar2(35),
 ERROR_MESSAGE              varchar2(4000)
);

alter table RPR_IMPORT_PRI_TEST_ARC
   add constraint RPR_IMP_PRI_TST_ARC_PK
      primary key (IMPORT_RUN_ID, LINE_ID)
      using index tablespace PWRPLANT_IDX;

alter table RPR_IMPORT_PRI_TEST_ARC
   add constraint RPR_IMP_PRI_TST_ARC_IMP_RUN_FK
       foreign key (IMPORT_RUN_ID)
       references PP_IMPORT_RUN;

--
-- Create standard templates for Tax Repairs imports.  Be sure to use the sequence to get the template ID.
--
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 112, 'Assign Loc Rollup to Location', 'Assign Loc Rollup to Location', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 110, 'Assign Range Test to Unit Code', 'Assign Range Test to Unit Code', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 114, 'Assign Repair Loc to Asset Loc', 'Assign Repair Loc to Asset Loc', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 111, 'Assign Repair Test to WO Type', 'Assign Repair Test to WO Type', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 117, 'Assign Unit Code to Func Class', 'Assign Unit Code to Func Class', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 115, 'Assign Unit Code to Prop Group', 'Assign Unit Code to Prop Group', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 118, 'Assign Unit Code to Utl Act Pr Ut', 'Assign Unit Code to Utl Act Pr Ut', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 116, 'Assign Unit Code to WO Type', 'Assign Unit Code to WO Type', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 113, 'Assign Loc Rollup to Func Class', 'Assign Loc Rollup to Func Class', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 104, 'Schema Add', 'Schema Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 104, 'Schema Add/Update', 'Schema Add/Update - Add new records and update existing records', user, sysdate, 1, 1008, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 102, 'Range Test Add', 'Range Test Add - Only add new records', user, sysdate, null, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 103, 'Range Test Ranges Add', 'Range Test Ranges Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 103, 'Range Test Ranges Add/Update', 'Range Test Ranges Add/Update - Add new records and update existing records', user, sysdate, 1, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 106, 'Location Rollup Add', 'Location Rollup Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 106, 'Location Rollup Add/Update', 'Location Rollup Add/Update - Add new records and update existing records', user, sysdate, 1, 1011, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 107, 'Location Add', 'Location Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 107, 'Location Add/Update', 'Location Add/Update - Add new records and update existing records', user, sysdate, 1, 1004, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 108, 'Tax Status Add', 'Tax Status Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 108, 'Tax Status Add/Update', 'Tax Status Add/Update - Add new records and update existing records', user, sysdate, 1, 1006, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 109, 'Unit Code Add', 'Unit Code Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 109, 'Unit Code Add/Update', 'Unit Code Add/Update - Add new records and update existing records', user, sysdate, 1, 1002, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 101, 'Thresholds Add', 'Thresholds Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 101, 'Thresholds Add/Update', 'Thresholds Add/Update - Add new records and update existing records', user, sysdate, 1, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 105, 'Repair Test Add', 'Repair Test Add - Only add new records', user, sysdate, 0, null, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 105, 'Repair Test Add/Update', 'Repair Test Add/Update - Add new records and update existing records', user, sysdate, 1, 1009, null, 0 from dual;
insert into PP_IMPORT_TEMPLATE (IMPORT_TEMPLATE_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, CREATED_BY, CREATED_DATE, DO_UPDATE_WITH_ADD, UPDATES_IMPORT_LOOKUP_ID, FILTER_CLAUSE, IS_AUTOCREATE_TEMPLATE) select pp_import_template_seq.nextval, sysdate, user, 119, 'Assign Priority to Repair Test', 'Assign Priority to Repair Test', user, sysdate, null, null, null, 0 from dual;

insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 112, 'repair_loc_rollup_id', 1010 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 112 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Loc Rollup to Location';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 112, 'repair_location_id', 1003 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 112 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Loc Rollup to Location';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 110, 'range_test_id', 1005 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 110 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Range Test to Unit Code';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 110, 'repair_unit_code_id', 1001 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 110 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Range Test to Unit Code';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 114, 'repair_location_id', 1003 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 114 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Repair Loc to Asset Loc';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 114, 'asset_location_id', 94 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 114 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Repair Loc to Asset Loc';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 114, 'state_id', 1 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 114 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Repair Loc to Asset Loc';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 111, 'tax_expense_test_id', 1009 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 111 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Repair Test to WO Type';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 111, 'work_order_type_id', 1015 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 111 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Repair Test to WO Type';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 117, 'repair_unit_code_id', 1001 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 117 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Func Class';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 117, 'func_class_id', 190 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 117 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Func Class';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 115, 'repair_unit_code_id', 1001 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 115 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Prop Group';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 115, 'property_group_id', 618 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 115 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Prop Group';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 118, 'repair_unit_code_id', 1001 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 118 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Utl Act Pr Ut';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 118, 'bus_segment_id', 25 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 118 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Utl Act Pr Ut';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 118, 'utility_account_id', 28 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 118 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Utl Act Pr Ut';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 118, 'property_unit_id', 193 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 118 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to Utl Act Pr Ut';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 116, 'repair_unit_code_id', 1001 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 116 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to WO Type';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 116, 'work_order_type_id', 1015 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 116 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Unit Code to WO Type';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 106, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 106 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Rollup Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 106, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 106 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Rollup Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 106, 'work_order_id', 110 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 106 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Rollup Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 113, 'repair_loc_rollup_id', 1010 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 113 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Loc Rollup to Func Class';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 113, 'func_class_id', 190 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 113 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Loc Rollup to Func Class';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 103, 'range_test_id', 1005 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 103, 'high_range', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 103, 'low_range', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 104, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 104, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 104, 'status', 138 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 104, 'usage_flag', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 104, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 104, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 104, 'status', 138 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 104, 'usage_flag', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 104 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Schema Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 102, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 102 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 103, 'tax_status_id', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 103, 'range_test_id', 1005 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 103, 'high_range', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 103, 'low_range', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 103, 'tax_status_id', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 103 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Range Test Ranges Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 106, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 106 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Rollup Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 106, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 106 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Rollup Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 106, 'company_id', 19 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 106 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Rollup Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 106, 'work_order_id', 111 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 106 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Rollup Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 107, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 107 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 107, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 107 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 107, 'is_circuit', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 107 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 107, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 107 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 107, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 107 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 107, 'is_circuit', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 107 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Location Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 108, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 108, 'priority', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 108, 'repair_schema_id', 1007 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 108, 'alert_review_flag', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 108, 'require_review', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 108, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 108, 'priority', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 108, 'repair_schema_id', 1007 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 108, 'alert_review_flag', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 108, 'require_review', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 108 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Tax Status Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 109, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 109, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 109, 'hw_table_line_id', 1012 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 109, 'range_test_id', 1005 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 109, 'unit_of_measure_id', 1013 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 109, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 109, 'long_description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 109, 'hw_table_line_id', 1012 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 109, 'range_test_id', 1005 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 109, 'unit_of_measure_id', 1013 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 109 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Unit Code Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 101, 'company_id', 19 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 101, 'effective_date', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 101, 'repair_location_id', 1003 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 101, 'repair_unit_code_id', 1001 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 101, 'base_year', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 6, sysdate, user, 101, 'add_test', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 7, sysdate, user, 101, 'retire_test', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 8, sysdate, user, 101, 'max_proj_amt', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 9, sysdate, user, 101, 'replacement_cost', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 10, sysdate, user, 101, 'replacement_quantity', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 11, sysdate, user, 101, 'unit_code_ratio', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 12, sysdate, user, 101, 'unit_replace_cost', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 101, 'company_id', 19 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 101, 'effective_date', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 101, 'repair_location_id', 1003 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 101, 'repair_unit_code_id', 1001 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 101, 'base_year', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 6, sysdate, user, 101, 'add_test', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 7, sysdate, user, 101, 'retire_test', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 8, sysdate, user, 101, 'max_proj_amt', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 9, sysdate, user, 101, 'replacement_cost', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 10, sysdate, user, 101, 'replacement_quantity', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 11, sysdate, user, 101, 'unit_code_ratio', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 12, sysdate, user, 101, 'unit_replace_cost', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 101 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Thresholds Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 105, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 105, 'repair_schema_id', 1007 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 105, 'blanket_method', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 105, 'tax_status_from_fp', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 105, 'est_revision', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 6, sysdate, user, 105, 'estimate_dollars_vs_units', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 7, sysdate, user, 105, 'quantity_vs_dollars', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 8, sysdate, user, 105, 'tax_status_no_replacement', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 9, sysdate, user, 105, 'tax_status_over_max_amount', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 10, sysdate, user, 105, 'tax_status_fp_fully_qualifying', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 11, sysdate, user, 105, 'tax_status_fp_non_qualifying', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 12, sysdate, user, 105, 'tax_status_minor_uop', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 13, sysdate, user, 105, 'tax_status_ovh_to_udg', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 14, sysdate, user, 105, 'tax_status_default_capital', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 15, sysdate, user, 105, 'tax_status_over_tolerance', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 16, sysdate, user, 105, 'expense_percent', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 17, sysdate, user, 105, 'add_retire_tolerance_pct', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 18, sysdate, user, 105, 'add_retire_circuit_pct', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 105, 'description', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 105, 'repair_schema_id', 1007 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 105, 'blanket_method', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 105, 'tax_status_from_fp', 77 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 5, sysdate, user, 105, 'est_revision', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 6, sysdate, user, 105, 'estimate_dollars_vs_units', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 7, sysdate, user, 105, 'quantity_vs_dollars', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 8, sysdate, user, 105, 'tax_status_no_replacement', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 9, sysdate, user, 105, 'tax_status_over_max_amount', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 10, sysdate, user, 105, 'tax_status_fp_fully_qualifying', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 11, sysdate, user, 105, 'tax_status_fp_non_qualifying', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 12, sysdate, user, 105, 'tax_status_minor_uop', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 13, sysdate, user, 105, 'tax_status_ovh_to_udg', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 14, sysdate, user, 105, 'tax_status_default_capital', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 15, sysdate, user, 105, 'tax_status_over_tolerance', 1006 from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 16, sysdate, user, 105, 'expense_percent', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 17, sysdate, user, 105, 'add_retire_tolerance_pct', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 18, sysdate, user, 105, 'add_retire_circuit_pct', null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 105 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Repair Test Add/Update';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 1, sysdate, user, 119, 'tax_expense_test_id', 1009, null, null, null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 119 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Priority to Repair Test';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 2, sysdate, user, 119, 'repair_priority_type_id', 1018, null, null, null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 119 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Priority to Repair Test';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 3, sysdate, user, 119, 'repair_priority_id', 1020, null, null, null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 119 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Priority to Repair Test';
insert into PP_IMPORT_TEMPLATE_FIELDS (IMPORT_TEMPLATE_ID, FIELD_ID, TIME_STAMP, USER_ID, IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID, AUTOCREATE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_TEMPLATE_ID, DERIVE_IMPORT_FIELD_ID) select PP_IMPORT_TEMPLATE.IMPORT_TEMPLATE_ID, 4, sysdate, user, 119, 'sequence_number', null, null, null, null from PP_IMPORT_TEMPLATE where PP_IMPORT_TEMPLATE.IMPORT_TYPE_ID = 119 and PP_IMPORT_TEMPLATE.DESCRIPTION = 'Assign Priority to Repair Test';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (297, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs_import_tool.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;