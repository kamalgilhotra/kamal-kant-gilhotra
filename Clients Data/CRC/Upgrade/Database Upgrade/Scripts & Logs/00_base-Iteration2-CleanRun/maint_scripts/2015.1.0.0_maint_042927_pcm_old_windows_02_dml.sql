/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		PCM
|| File Name:   maint_042927_pcm_old_windows_02_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1 02/13/2015 		B.Beck			Open old windows from left hand navigation 
||==========================================================================================
*/
insert into ppbase_workspace_object_type
(object_type_id, description)
values
(3, 'A Powerbuilder window object that takes parameters.  Should be programatically handled in of_openWindowWithParm')
;

update ppbase_workspace
set object_type_id = 3
where module = 'pcm'
and object_type_id = 2
;

update ppbase_workspace
set object_type_id = 2
where object_type_id = 3
and workspace_identifier like 'config%'
;

update ppbase_workspace
set workspace_uo_name = 'w_worksheet'
where module = 'pcm'
and workspace_identifier = 'config_wo_worksheets'
;

update ppbase_workspace
set workspace_uo_name = 'w_worksheet'
where module = 'pcm'
and workspace_identifier = 'config_fp_worksheets'
;

update ppbase_workspace
set object_type_id = 3
where module = 'pcm'
and workspace_identifier = 'config_maintain'
;

update ppbase_workspace
set object_type_id = 3
where module = 'pcm'
and workspace_identifier = 'config_rates'
;

update ppbase_workspace
set workspace_uo_name = 'w_wo_commitment_select'
where module = 'pcm'
and workspace_identifier = 'wo_mon_commitments'
;

update ppbase_workspace
set workspace_uo_name = 'w_fp_commitment_select'
where module = 'pcm'
and workspace_identifier = 'fp_mon_commitments'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2295, 0, 2015, 1, 0, 0, 042927, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042927_pcm_old_windows_02_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;