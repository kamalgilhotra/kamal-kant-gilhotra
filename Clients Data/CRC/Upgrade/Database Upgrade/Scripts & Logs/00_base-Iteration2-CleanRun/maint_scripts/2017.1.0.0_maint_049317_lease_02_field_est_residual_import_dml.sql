/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049317_lease_02_field_est_residual_import_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

update pp_import_column 
set import_column_name = 'estimated_residual_xlate',
    processing_order = 2,
    is_on_table = 1
where column_name = 'estimated_residual';

insert into pp_import_lookup(import_lookup_id, description, long_description, column_name, lookup_sql, is_derived, lookup_constraining_columns)
select 
   (select max(import_lookup_id) + 1 from pp_import_lookup) AS import_lookup_id, 
   'Derive Estimated Residual Percent from Estimated Residual Amount and FMV' AS description, 
   'Derive Estimated Residual Percent from Estimated Residual Amount and FMV' AS long_description, 
   'estimated_residual' AS column_name,
   'round(nvl(<importfield>,0)/decode(nvl(fmv,0), 0, 1, fmv),8)' lookup_sql,
   0 AS is_derived, 
   'fmv' AS lookup_constraining_columns
from dual
where not exists(
   select 1
   from pp_import_lookup
   where column_name = 'estimated_residual');

insert into pp_import_column_lookup(import_type_id, column_name, import_lookup_id)
select 
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_asset') import_type_id, 
   'estimated_residual', 
   (select import_lookup_id from pp_import_lookup where column_name = 'estimated_residual') import_lookup_id
from dual
where not exists(
   select 1
   from pp_import_column_lookup
   where column_name = 'estimated_residual');
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4015, 0, 2017, 1, 0, 0, 49317, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049317_lease_02_field_est_residual_import_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;