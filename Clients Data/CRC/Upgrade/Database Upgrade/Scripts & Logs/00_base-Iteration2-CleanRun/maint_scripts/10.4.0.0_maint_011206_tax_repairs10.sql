/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011206_tax_repairs10.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.0.0   04/25/2013 Alex P.         Point Release
||============================================================================
*/

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID,
    DW_DESCRIPTION, DW_ID_DATATYPE, REQUIRED, SINGLE_SELECT_ONLY, DATA, USER_ID, TIME_STAMP,
    VALID_OPERATORS, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE)
values
   (55, 'Work Order Number (Activity)', 'dw', 'cpr_activity.work_order_number', null,
    'dw_rpr_work_order_number_filter', 'work_order_number', 'work_order_number', 'C', null, null,
    null, null, null, null, null, 1);

insert into PP_DYNAMIC_FILTER_MAPPING
   (PP_REPORT_FILTER_ID, FILTER_ID, USER_ID, TIME_STAMP)
values
   (37, 55, null, null);

update PP_REPORTS
   set DESCRIPTION = 'Tax Repairs CWIP/InService Analysis',
       LONG_DESCRIPTION = 'Tax Repairs CWIP and In-Service Analysis for all work orders grouped by Company for a span of time. Dates correpond to a point of time when charges were incurred.'
 where REPORT_NUMBER = 'RPR - 0090';

alter table PP_DYNAMIC_FILTER_VALUES modify START_VALUE varchar2(2000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (375, 0, 10, 4, 0, 0, 11206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011206_tax_repairs10.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;