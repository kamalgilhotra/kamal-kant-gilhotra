/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_04_lease_populate_currency_rate_default_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/21/2017 Jared Watkins    populate currency_rate_default with all of the exchange rates we will
||                                        need for multicurrency, including default 0/1 rates
||============================================================================
*/

insert into currency_rate_default(exchange_date, currency_from, currency_to, exchange_rate_type_id, rate)
(SELECT exchange_date, currency_from, currency_to, 1, rate
  FROM currency_rate
  WHERE exchange_rate_type_id = 1  --only look at 'Actual' exchange rates
  AND currency_from <> currency_to --eliminate translations from one currency to the same currency since that's taken care of below
    UNION
  SELECT To_Date('190001','yyyymm'), currency_id, currency_id, 1, 1
  FROM currency                    --add exchange rates with a default rate of 1 from every currency to itself
    UNION
  SELECT To_Date('190001', 'yyyymm'), a.currency_id, b.currency_id, 1, 0
  FROM currency a, currency b      --add default 0 exchange rates for every combination of currencies in the system in case there are no real rates
  WHERE a.currency_id <> b.currency_id
);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3445, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_04_lease_populate_currency_rate_default_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;