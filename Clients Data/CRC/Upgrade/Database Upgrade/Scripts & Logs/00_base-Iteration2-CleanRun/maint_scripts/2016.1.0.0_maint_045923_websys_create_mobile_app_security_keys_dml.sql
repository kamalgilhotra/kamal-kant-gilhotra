/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045923_websys_create_mobile_app_security_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 08/12/2016 Jared Watkins			 Add the necessary security keys in order to add security to
||                                          Mobile Approvals similar to the Job Scheduler security
||============================================================================
*/

delete from pp_web_security_perm_control where objects like 'MobileApprovals%';

delete from pp_web_security_perm_control
where objects in ('MobileApprovals.MobileApprovals.PropertyTax.Details',
                  'MobileApprovals.MobileApprovals.PropTaxPayment.ApproveReject',
                  'MobileApprovals.MobileApprovals.PropTaxJE.ApproveReject',
                  'MobileApprovals.MobileApprovals.Projects.Details',
                  'MobileApprovals.MobileApprovals.WorkOrder.ApproveReject',
                  'MobileApprovals.MobileApprovals.WorkOrder.Attachments',
                  'MobileApprovals.MobileApprovals.FundingProject.ApproveReject',
                  'MobileApprovals.MobileApprovals.FundingProject.Attachments',
                  'MobileApprovals.MobileApprovals.Lease.Details',
                  'MobileApprovals.MobileApprovals.LeasePayment.ApproveReject',
                  'MobileApprovals.MobileApprovals.LeaseILR.ApproveReject',
                  'MobileApprovals.MobileApprovals.LeaseMLA.ApproveReject',
                  'MobileApprovals.MobileApprovals.Alerts.Details',
                  'MobileApprovals.MobileApprovals.Alerts.ActionsDropDown',
                  'MobileApprovals.MobileApprovals.Alerts.Attachments',
                  'MobileApprovals.MobileApprovals.Updates.Details',
                  'MobileApprovals.MobileApprovals.Updates.ActionsDropDown',
                  'MobileApprovals.MobileApprovals.Updates.Attachments');

--PROJECTS
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Projects.Details', 2, 1, '', 'Projects', 'View Work Orders and Funding Projects' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Projects.Details');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.WorkOrder.ApproveReject', 2, 1, '', 'Work Orders', 'Approve / Reject' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.WorkOrder.ApproveReject');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.WorkOrder.Attachments', 2, 1, '', 'Work Orders', 'View Attachments' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.WorkOrder.Attachments');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.FundingProject.ApproveReject', 2, 1, '', 'Funding Projects', 'Approve / Reject' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.FundingProject.ApproveReject');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.FundingProject.Attachments', 2, 1, '', 'Funding Projects', 'View Attachments' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.FundingProject.Attachments');


--PROPERTY TAX
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.PropertyTax.Details', 2, 2, '', 'Property Tax', 'View Payments and Journal Entries' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.PropertyTax.Details');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.PropTaxPayment.ApproveReject', 2, 2, '', 'Payments', 'Approve / Reject' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.PropTaxPayment.ApproveReject');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.PropTaxJE.ApproveReject', 2, 2, '', 'Journal Entries', 'Approve / Reject' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.PropTaxJE.ApproveReject');


--LEASE
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Lease.Details', 2, 3, '', 'Lease', 'View ILRs, MLAs, and Payments' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Lease.Details');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.LeasePayment.ApproveReject', 2, 3, '', 'Payments', 'Approve / Reject' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.LeasePayment.ApproveReject');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.LeaseILR.ApproveReject', 2, 3, '', 'Individual Lease Records', 'Approve / Reject' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.LeaseILR.ApproveReject');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.LeaseMLA.ApproveReject', 2, 3, '', 'Master Lease Agreements', 'Approve / Reject' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.LeaseMLA.ApproveReject');


--ALERTS
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Alerts.Details', 2, null, 'Alerts', 'Alerts', 'View Alerts' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Alerts.Details');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Alerts.ActionsDropDown', 2, null, 'Alerts', 'Alerts', 'Enter Project Dates' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Alerts.ActionsDropDown');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Alerts.Attachments', 2, null, 'Alerts', 'Alerts', 'View Attachments' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Alerts.Attachments');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)

--MOBILE UPDATES
select 'MobileApprovals.MobileApprovals.Updates.Details', 2, null, 'Mobile Updates', 'Mobile Updates', 'View Updates' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Updates.Details');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Updates.ActionsDropDown', 2, null, 'Mobile Updates', 'Mobile Updates', 'Enter Project Dates' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Updates.ActionsDropDown');
insert into pp_web_security_perm_control(objects, module_id, component_id, pseudo_component, section, name)
select 'MobileApprovals.MobileApprovals.Updates.Attachments', 2, null, 'Mobile Updates', 'Mobile Updates', 'View Attachments' from dual
where not exists (select 1 from pp_web_security_perm_control where objects = 'MobileApprovals.MobileApprovals.Updates.Attachments');

--remove existing Mobile Approvals security keys from the permissions table
delete from pp_web_security_permission where objects like 'MobileApprovals%';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3258, 0, 2016, 1, 0, 0, 45923, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045923_websys_create_mobile_app_security_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;