/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_034103_lease_detailed_invoices.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   01/20/2014 Kyle Peterson
||============================================================================
*/

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_invoicecntr_wksp',
       LABEL = 'Detailed Invoices'
 where WORKSPACE_IDENTIFIER = 'control_invoices'
   and MODULE = 'LESSEE';

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
   (select 'LESSEE',
           'control_invoices',
           2,
           3,
           'Detailed Invoices',
           'menu_wksp_control',
           'control_invoices',
           1
      from DUAL);

alter table LS_INVOICE_LINE drop primary key drop index;

alter table LS_INVOICE_LINE
   add constraint PK_LS_INVOICE_LINE
       primary key (INVOICE_ID, INVOICE_LINE_NUMBER, PAYMENT_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_INVOICE_LINE modify LS_ASSET_ID number(22,0) null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (883, 0, 10, 4, 2, 0, 34103, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034103_lease_detailed_invoices.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;

