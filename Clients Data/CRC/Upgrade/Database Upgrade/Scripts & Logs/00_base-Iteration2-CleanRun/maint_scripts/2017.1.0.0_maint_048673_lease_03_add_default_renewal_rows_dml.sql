/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048673_lease_03_add_default_renewal_rows_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/18/2017 Andrew Hill      Add default rows for renewal probability
||============================================================================
*/

INSERT INTO ls_ilr_renewal(ilr_renewal_id, ilr_id, revision, ilr_renewal_probability_id)
SELECT ls_ilr_renewal_seq.NEXTVAL, APP.ilr_id, APP.revision, prob.ilr_renewal_probability_id
FROM ls_ilr_approval APP
CROSS JOIN ls_ilr_renewal_probability prob
where upper(trim(prob.description)) = 'NONE';

INSERT INTO lsr_ilr_renewal(ilr_renewal_id, ilr_id, revision, ilr_renewal_probability_id)
SELECT lsr_ilr_renewal_seq.NEXTVAL, APP.ilr_id, APP.revision, prob.ilr_renewal_probability_id
FROM lsr_ilr_approval APP
CROSS JOIN ls_ilr_renewal_probability prob
where upper(trim(prob.description)) = 'NONE';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3729, 0, 2017, 1, 0, 0, 48673, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048673_lease_03_add_default_renewal_rows_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;