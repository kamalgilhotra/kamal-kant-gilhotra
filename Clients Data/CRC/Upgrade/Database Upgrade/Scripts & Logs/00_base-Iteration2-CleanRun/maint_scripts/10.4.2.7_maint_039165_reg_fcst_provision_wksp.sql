/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039165_reg_fcst_provision_wksp.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.2.7 07/22/2014 Ryan Oliveria       Forecast Provision Integration Workspace
||========================================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_fcst_prov_int', 'Tax Provision - Other Tax Items', 'uo_reg_fcst_prov_int',
    'Tax provision - Other Tax Items Integration');

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_fcst_ta_def_int', 'Tax Provision - Timing Diff/Def', 'uo_reg_fcst_ta_def_int',
    'Tax Provision - Timing Differences/Deferreds Integration');

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_fcst_ta_setup', 'Tax Provision - Setup', 'uo_reg_fcst_ta_setup', 'Tax Provision - Integration Setup');

delete from PPBASE_MENU_ITEMS
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'FORE_TAX_PROV';

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 2
 where MODULE = 'REG'
   and PARENT_MENU_IDENTIFIER = 'FORE_INTEGRATION'
   and ITEM_ORDER > 5;

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'FCST_TAX_PROV_SETUP', 3, 5, 'Tax Provision - Setup', 'Tax Provision - Integration Setup', 'FORE_INTEGRATION', 'uo_reg_fcst_ta_setup', 1);

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'FCST_DEF_TAX', 3, 6, 'Tax Provision - Timing Diff/Def', 'Tax Provision - Timing Differences/Deferreds Integration', 'FORE_INTEGRATION', 'uo_reg_fcst_ta_def_int', 1);

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'FCST_TAX_PROV', 3, 7, 'Tax Provision - Other Tax Items', 'Tax Provision - Other Tax Items Integration', 'FORE_INTEGRATION', 'uo_reg_fcst_prov_int', 1);


--Add nullable column
alter table REG_TA_COMP_MEMBER_MAP     add FORECAST_VERSION_ID number(22,0);
alter table REG_TA_CONTROL_MAP         add FORECAST_VERSION_ID number(22,0);
alter table REG_TA_FAMILY_MEMBER_MAP   add FORECAST_VERSION_ID number(22,0);
alter table REG_TA_M_TYPE_ENTITY_MAP   add FORECAST_VERSION_ID number(22,0);
alter table REG_TA_VERSION_CONTROL     add FORECAST_VERSION_ID number(22,0);

--Foreign Keys
alter table REG_TA_COMP_MEMBER_MAP
   add constraint R_REG_TA_COMP_MEMBER_MAP4
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TA_CONTROL_MAP
   add constraint R_REG_TA_CONTROL_MAP6
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TA_FAMILY_MEMBER_MAP
   add constraint R_REG_TA_FAMILY_MEMBER_MAP6
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TA_M_TYPE_ENTITY_MAP
   add constraint R_REG_TA_M_TYPE_ENTITY_MAP5
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_TA_VERSION_CONTROL
   add constraint R_REG_TA_VERSION_CONTROL3
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);


--Default to zero
update REG_TA_COMP_MEMBER_MAP    set FORECAST_VERSION_ID = 0 where FORECAST_VERSION_ID is null;
update REG_TA_CONTROL_MAP        set FORECAST_VERSION_ID = 0 where FORECAST_VERSION_ID is null;
update REG_TA_FAMILY_MEMBER_MAP  set FORECAST_VERSION_ID = 0 where FORECAST_VERSION_ID is null;
update REG_TA_M_TYPE_ENTITY_MAP  set FORECAST_VERSION_ID = 0 where FORECAST_VERSION_ID is null;
update REG_TA_VERSION_CONTROL    set FORECAST_VERSION_ID = 0 where FORECAST_VERSION_ID is null;


--Make column not nullable
alter table REG_TA_COMP_MEMBER_MAP     modify FORECAST_VERSION_ID not null;
alter table REG_TA_CONTROL_MAP         modify FORECAST_VERSION_ID not null;
alter table REG_TA_FAMILY_MEMBER_MAP   modify FORECAST_VERSION_ID not null;
alter table REG_TA_M_TYPE_ENTITY_MAP   modify FORECAST_VERSION_ID not null;
alter table REG_TA_VERSION_CONTROL     modify FORECAST_VERSION_ID not null;


--Primary Keys
alter table REG_TA_COMP_MEMBER_MAP drop primary key drop index;

alter table REG_TA_COMP_MEMBER_MAP
   add constraint PK_REG_TA_COMP_MEMBER_MAP
       primary key (HISTORIC_VERSION_ID, FORECAST_VERSION_ID, REG_COMPONENT_ID, REG_FAMILY_ID, REG_MEMBER_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_TA_M_TYPE_ENTITY_MAP drop primary key drop index;

alter table REG_TA_M_TYPE_ENTITY_MAP
   add constraint PK_REG_TA_M_TYPE_ENTITY_MAP
       primary key (HISTORIC_VERSION_ID, FORECAST_VERSION_ID, M_TYPE_ID, ENTITY_ID, TAX_CONTROL_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_TA_VERSION_CONTROL drop primary key drop index;

alter table REG_TA_VERSION_CONTROL
   add constraint PK_REG_TA_VERSION_CONTROL
       primary key (HISTORIC_VERSION_ID, FORECAST_VERSION_ID, TA_VERSION_ID)
       using index tablespace PWRPLANT_IDX;


--Comments
comment on column REG_TA_COMP_MEMBER_MAP.FORECAST_VERSION_ID is 'System assigned identifier of the forecast ledger.';
comment on column REG_TA_CONTROL_MAP.FORECAST_VERSION_ID is 'System assigned identifier of the forecast ledger.';
comment on column REG_TA_FAMILY_MEMBER_MAP.FORECAST_VERSION_ID is 'System assigned identifier of the forecast ledger.';
comment on column REG_TA_M_TYPE_ENTITY_MAP.FORECAST_VERSION_ID is 'System assigned identifier of the forecast ledger.';
comment on column REG_TA_VERSION_CONTROL.FORECAST_VERSION_ID is 'System assigned identifier of the forecast ledger.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1290, 0, 10, 4, 2, 7, 39165, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039165_reg_fcst_provision_wksp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;