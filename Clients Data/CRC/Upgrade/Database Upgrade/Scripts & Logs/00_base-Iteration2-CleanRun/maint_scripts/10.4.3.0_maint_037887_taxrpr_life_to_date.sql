/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_037887_taxrpr_life_to_date.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.3.0 06/04/2014 Alex P.
||========================================================================================
*/

create table REPAIR_PROCESSING_PERIOD
(
 PROCESSING_PERIOD_ID number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 DESCRIPTION          varchar2(35)
);

alter table REPAIR_PROCESSING_PERIOD
   add constraint PK_RPP_PROCESSING_PERIOD_ID
       primary key (PROCESSING_PERIOD_ID)
       using index tablespace PWRPLANT_IDX;

comment on table REPAIR_PROCESSING_PERIOD is '(F) [18] The Repair Processing Period table maintains values for tax repair processing methods, such as Life-to-Date and Time Period.';
comment on column REPAIR_PROCESSING_PERIOD.PROCESSING_PERIOD_ID is 'System-assigned identifier for the processing period value.';
comment on column REPAIR_PROCESSING_PERIOD.DESCRIPTION is 'Description of the processing period value.';
comment on column REPAIR_PROCESSING_PERIOD.TIME_STAMP is 'Standard System-assigned timestamp used for audit purposes.';
comment on column REPAIR_PROCESSING_PERIOD.USER_ID is 'Standard System-assigned user id used for audit purposes.';

insert into REPAIR_PROCESSING_PERIOD
   (PROCESSING_PERIOD_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (1, null, null, 'Date Range');

insert into REPAIR_PROCESSING_PERIOD
   (PROCESSING_PERIOD_ID, TIME_STAMP, USER_ID, DESCRIPTION)
values
   (2, null, null, 'Life-to-Date');

--
-- Add processing_period_id to repair_schema
--
alter table REPAIR_SCHEMA add PROCESSING_PERIOD_ID number(22,0);
comment on column REPAIR_SCHEMA.PROCESSING_PERIOD_ID is 'System-assigned identifier for the processing period value.';

update REPAIR_SCHEMA set PROCESSING_PERIOD_ID = 1;

alter table REPAIR_SCHEMA modify PROCESSING_PERIOD_ID not null;

alter table REPAIR_SCHEMA
   add constraint FK_RS_PROCESSING_PERIOD
       foreign key (PROCESSING_PERIOD_ID)
       references REPAIR_PROCESSING_PERIOD;

--
-- Add processing_period_id to repair_batch_control
--
alter table REPAIR_BATCH_CONTROL add PROCESSING_PERIOD_ID number(22,0);
comment on column REPAIR_BATCH_CONTROL.PROCESSING_PERIOD_ID is 'System-assigned identifier for the processing period value that was used with a given batch.';

update REPAIR_BATCH_CONTROL set PROCESSING_PERIOD_ID = 1;

alter table REPAIR_BATCH_CONTROL modify PROCESSING_PERIOD_ID not null;

alter table REPAIR_BATCH_CONTROL
   add constraint FK_RBC_PROCESSING_PERIOD
       foreign key (PROCESSING_PERIOD_ID)
       references REPAIR_PROCESSING_PERIOD;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1177, 0, 10, 4, 3, 0, 37887, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_037887_taxrpr_life_to_date.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;