/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_lease_add_ux_to_currency_schema_table_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/21/2017 Charlie Shilling add unique constraint for (company_id, currency_type_id) to currency_schema table.
||============================================================================
*/

ALTER TABLE currency_schema
ADD CONSTRAINT company_currency_uix UNIQUE (company_id, currency_type_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3441, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_lease_add_ux_to_currency_schema_table_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;