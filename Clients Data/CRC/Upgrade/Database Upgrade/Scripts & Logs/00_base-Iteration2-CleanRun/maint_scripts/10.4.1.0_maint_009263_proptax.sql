/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_009263_proptax.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.4.1.0    09/10/2012 Julia Breuer   Point Release
||============================================================================
*/

delete from PWRPLANT.PP_TABLE_GROUPS    where lower(TABLE_NAME) = 'pt_escalated_value_index';
delete from PWRPLANT.POWERPLANT_COLUMNS where lower(TABLE_NAME) = 'pt_escalated_value_index';
delete from PWRPLANT.POWERPLANT_TABLES  where lower(TABLE_NAME) = 'pt_escalated_value_index';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (220, 0, 10, 4, 1, 0, 9263, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_009263_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
