/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032465_lease_PKG_LEASE_ILR.sql
|| Description: Handles calculations around the ILR
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date        Revised By        Reason for Change
|| ----------  ----------  -------------- ----------------------------------
|| 10.4.1.0    09/18/13    Brandon Beck   Point Release
||============================================================================
*/

create or replace package PKG_LEASE_ILR as
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *    number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYASSET(A_LS_ASSET_ID number,
                        A_PCT         number,
                        A_QTY         number) return number;
end PKG_LEASE_ILR;
/


create or replace package body PKG_LEASE_ILR as
   --**************************************************************************
   --                            VARIABLES
   --**************************************************************************

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: the revision added for success
   *            -1 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number is
      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Revision for: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   COPY Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      L_STATUS := 'Get new revision';
      select max(REVISION) + 1 into L_REVISION from LS_ILR_APPROVAL where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   NEW Revision: ' || TO_CHAR(L_REVISION));

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select A.ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select A.ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_asset_map';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID, L_REVISION, A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      PKG_PP_LOG.P_END_LOG();
      return L_REVISION;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         PKG_PP_LOG.P_END_LOG();
         return -1;
   end F_NEWREVISION;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: the ls_ilr_id for success
   *            -1 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number is
      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;
      L_ILR_ID           number;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW ILR from: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      select LS_ILR_SEQ.NEXTVAL into L_ILR_ID from DUAL;
      PKG_PP_LOG.P_WRITE_MESSAGE('TO ILR: ' || TO_CHAR(L_ILR_ID));

      L_STATUS   := 'Get new revision';
      L_REVISION := 1;

      L_STATUS := 'LOAD ilr';
      insert into LS_ILR
         (ILR_ID, ILR_NUMBER, LEASE_ID, COMPANY_ID, EST_IN_SVC_DATE, FINAL_EOT_DATE,
          INTERIM_INTEREST_BEGIN_DATE, EXTERNAL_ILR, ILR_STATUS_ID, ILR_GROUP_ID, NOTES,
          CURRENT_REVISION, WORKFLOW_TYPE_ID)
         select L_ILR_ID,
                '(TRF' || (select 1 + count(1)
                             from LS_ILR LL
                            where LL.ILR_NUMBER like '(TRF%) ' || A.ILR_NUMBER) || ') ' ||
                A.ILR_NUMBER,
                A.LEASE_ID,
                A.COMPANY_ID,
                A.EST_IN_SVC_DATE,
                A.FINAL_EOT_DATE,
                A.INTERIM_INTEREST_BEGIN_DATE,
                A.EXTERNAL_ILR,
                1,
                A.ILR_GROUP_ID,
                A.NOTES,
                L_REVISION,
                A.WORKFLOW_TYPE_ID
           from LS_ILR A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD class codes';
      insert into LS_ILR_CLASS_CODE
         (CLASS_CODE_ID, ILR_ID, "VALUE")
         select A.CLASS_CODE_ID, L_ILR_ID, a."VALUE"
           from LS_ILR_CLASS_CODE A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD ilr_account';
      insert into LS_ILR_ACCOUNT
         (ILR_ID, INT_ACCRUAL_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID,
          EXEC_EXPENSE_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID)
         select L_ILR_ID,
                A.INT_ACCRUAL_ACCOUNT_ID,
                A.INT_EXPENSE_ACCOUNT_ID,
                A.EXEC_ACCRUAL_ACCOUNT_ID,
                A.EXEC_EXPENSE_ACCOUNT_ID,
                A.CONT_ACCRUAL_ACCOUNT_ID,
                A.CONT_EXPENSE_ACCOUNT_ID
           from LS_ILR_ACCOUNT A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select L_ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          --, payment_shift
          )
         select L_ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                ROUND(A_PCT * A.PURCHASE_OPTION_AMT, 2),
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                ROUND(A_PCT * A.TERMINATION_AMT, 2)
         --, payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select L_ILR_ID,
                L_REVISION,
                A.SET_OF_BOOKS_ID,
                ROUND(A_PCT * NET_PRESENT_VALUE, 2),
                INTERNAL_RATE_RETURN,
                ROUND(A_PCT * CAPITAL_COST, 2),
                ROUND(A_PCT * CURRENT_LEASE_COST, 2),
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10)
         select L_ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                ROUND(A_PCT * EST_EXECUTORY_COST, 2),
                ROUND(A_PCT * PAID_AMOUNT, 2),
                ROUND(A_PCT * CONTINGENT_AMOUNT, 2),
                CURRENCY_TYPE_ID,
                ROUND(A_PCT * C_BUCKET_1, 2),
                ROUND(A_PCT * C_BUCKET_2, 2),
                ROUND(A_PCT * C_BUCKET_3, 2),
                ROUND(A_PCT * C_BUCKET_4, 2),
                ROUND(A_PCT * C_BUCKET_5, 2),
                ROUND(A_PCT * C_BUCKET_6, 2),
                ROUND(A_PCT * C_BUCKET_7, 2),
                ROUND(A_PCT * C_BUCKET_8, 2),
                ROUND(A_PCT * C_BUCKET_9, 2),
                ROUND(A_PCT * C_BUCKET_10, 2),
                ROUND(A_PCT * E_BUCKET_1, 2),
                ROUND(A_PCT * E_BUCKET_2, 2),
                ROUND(A_PCT * E_BUCKET_3, 2),
                ROUND(A_PCT * E_BUCKET_4, 2),
                ROUND(A_PCT * E_BUCKET_5, 2),
                ROUND(A_PCT * E_BUCKET_6, 2),
                ROUND(A_PCT * E_BUCKET_7, 2),
                ROUND(A_PCT * E_BUCKET_8, 2),
                ROUND(A_PCT * E_BUCKET_9, 2),
                ROUND(A_PCT * E_BUCKET_10, 2)
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      PKG_PP_LOG.P_END_LOG();
      return L_ILR_ID;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         PKG_PP_LOG.P_END_LOG();
         return -1;
   end F_COPYILR;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: The ls_asset_id added for success
   *            -1 for failure
   */
   function F_COPYASSET(A_LS_ASSET_ID number,
                        A_PCT         number,
                        A_QTY         number) return number is
      L_STATUS   varchar2(2000);
      L_ASSET_ID number;
   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Asset from: ' || TO_CHAR(A_LS_ASSET_ID));

      select LS_ASSET_SEQ.NEXTVAL into L_ASSET_ID from DUAL;
      PKG_PP_LOG.P_WRITE_MESSAGE('TO Asset: ' || TO_CHAR(L_ASSET_ID));

      L_STATUS := 'LOAD asset';
      insert into LS_ASSET
         (LS_ASSET_ID, LEASED_ASSET_NUMBER, LS_ASSET_STATUS_ID, DESCRIPTION, LONG_DESCRIPTION,
          CURRENT_LEASE_DATE, INTERIM_INTEREST_BEGIN_DATE, GL_POSTING_BEGIN_DATE,
          TERMINATION_PENALTY_AMOUNT, EXPECTED_LIFE, ECONOMIC_LIFE, FMV, PROPERTY_GROUP_ID,
          UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, SUB_ACCOUNT_ID, RETIREMENT_UNIT_ID, WORK_ORDER_ID,
          FUNC_CLASS_ID, ASSET_LOCATION_ID, IN_SERVICE_DATE, SERIAL_NUMBER, ACTUAL_RESIDUAL_AMOUNT,
          GUARANTEED_RESIDUAL_AMOUNT, NOTES, COMPANY_ID, QUANTITY, IS_EARLY_RET)
         select L_ASSET_ID,
                '(TRF' ||
                (select 1 + count(1)
                   from LS_ASSET LL
                  where LL.LEASED_ASSET_NUMBER like '(TRF%) ' || A.LEASED_ASSET_NUMBER) || ') ' ||
                A.LEASED_ASSET_NUMBER,
                1,
                '(TRF) ' || DESCRIPTION,
                '(TRF) ' || LONG_DESCRIPTION,
                CURRENT_LEASE_DATE,
                INTERIM_INTEREST_BEGIN_DATE,
                GL_POSTING_BEGIN_DATE,
                ROUND(A_PCT * TERMINATION_PENALTY_AMOUNT, 2),
                EXPECTED_LIFE,
                ECONOMIC_LIFE,
                ROUND(A_PCT * FMV, 2),
                PROPERTY_GROUP_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                SUB_ACCOUNT_ID,
                RETIREMENT_UNIT_ID,
                WORK_ORDER_ID,
                FUNC_CLASS_ID,
                ASSET_LOCATION_ID,
                IN_SERVICE_DATE,
                SERIAL_NUMBER,
                ROUND(A_PCT * ACTUAL_RESIDUAL_AMOUNT, 2),
                ROUND(A_PCT * GUARANTEED_RESIDUAL_AMOUNT, 2),
                NOTES,
                COMPANY_ID,
                A_QTY,
                0
           from LS_ASSET A
          where A.LS_ASSET_ID = A_LS_ASSET_ID;

      L_STATUS := 'LOAD class codes';
      insert into LS_ASSET_CLASS_CODE
         (CLASS_CODE_ID, LS_ASSET_ID, "VALUE")
         select A.CLASS_CODE_ID, L_ASSET_ID, a."VALUE"
           from LS_ASSET_CLASS_CODE A
          where A.LS_ASSET_ID = A_LS_ASSET_ID;

      PKG_PP_LOG.P_END_LOG();
      return L_ASSET_ID;
   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         PKG_PP_LOG.P_END_LOG();
         return -1;
   end F_COPYASSET;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

end PKG_LEASE_ILR;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (624, 0, 10, 4, 1, 0, 32465, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_032465_lease_PKG_LEASE_ILR.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
