/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029849_lease_subledger_control.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/29/2013 Brandon Beck   Point Release
||============================================================================
*/

insert into SUBLEDGER_CONTROL
   (SUBLEDGER_TYPE_ID, SUBLEDGER_NAME, DEPRECIATION_INDICATOR, DESCRIPTION, LONG_DESCRIPTION,
    COL1_USE_INDICATOR, BASIS_1_INDICATOR)
values
   (-100, 'Leases', 0, 'Leases', 'Leases', 1, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (370, 0, 10, 4, 0, 0, 29849, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029849_lease_subledger_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
