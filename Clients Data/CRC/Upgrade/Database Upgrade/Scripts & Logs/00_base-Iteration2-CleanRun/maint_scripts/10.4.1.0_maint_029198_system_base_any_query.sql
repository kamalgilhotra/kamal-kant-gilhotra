/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029198_system_base_any_query.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   03/14/2013 Brandon Beck    Point Release
||============================================================================
*/

--
-- BSB: 20120529: Create a table to hold the process step types
--    1 = insert (inserting from one table to another)
--    2 = validate (perform a validation.  Update int_status and validation_message)
--    3 = update data (perform a backfill or update against a table)
--    4 = delete (perform a delete against a table)
--    5 = start main transaction processing (Trigger transaction interfaces to call uo_transaction_control.uf_read)
--    6 = cr source processing
--       MUST EXIST AFTER a step 5  This is a SUB STEP
--    7 = build derivation control (Triggers an interface to call process to build cr deriver control)
--    8 = Process Retirements (Triggers an interface to call the process to load retirements)
--
create table CLIENT_PROCESS_STEP_TYPE
(
 STEP_TYPE_ID     number(22,0) not null,
 DESCRIPTION      varchar2(35),
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 EXTERNAL_PROCESS number(1,0),
 IS_SUB_STEP      number(1,0)
);

alter table CLIENT_PROCESS_STEP_TYPE
   add constraint CLIENT_PROCESS_STEP_TYPE_PK
       primary key (STEP_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

create table CLIENT_PROCESS_FLOW
(
 PROCESS_ID       number(22,0) not null,
 STEP_ID          number(22,0) not null,
 DESCRIPTION      varchar2(2000),
 STEP_TYPE_ID     number(22,0),
 SOURCE_ID        number(22,0),
 NEXT_STEP_ID     number(10,0),
 TIME_STAMP       date,
 USER_ID          varchar2(18),
 EXTRA_ATTRIBUTE1 varchar2(254),
 EXTRA_ATTRIBUTE2 varchar2(254),
 DISPLAY_MESSAGE  number(1,0),
 PERFORM_COMMIT   number(1,0)
);

alter table CLIENT_PROCESS_FLOW
   add constraint CLIENT_PROCESS_FLOW_PK
       primary key (PROCESS_ID, STEP_ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_FLOW
   add constraint CLIENT_PROCESS_FLOW_FK1
       foreign key (PROCESS_ID)
       references PP_PROCESSES (PROCESS_ID);

alter table CLIENT_PROCESS_FLOW
   add constraint CLIENT_PROCESS_FLOW_FK2
       foreign key (STEP_TYPE_ID)
       references CLIENT_PROCESS_STEP_TYPE (STEP_TYPE_ID);

CREATE TABLE CLIENT_PROCESS_ARGUMENTS
(
 PROCESS_ID number(22,0) not null,
 ARG_NAME   varchar2(35) not null,
 TIME_STAMP date,
 USER_ID    varchar2(18)
);

alter table CLIENT_PROCESS_ARGUMENTS
   add constraint CLIENT_PROCESS_ARGUMENTS_PK
       primary key (PROCESS_ID, ARG_NAME)
       using index tablespace PWRPLANT_IDX;

create table CLIENT_PROCESS_DECISIONS
(
 ID                 number(22,0) not null,
 DECISION_ID        number(22,0),
 DECISION_CONDITION varchar2(4000),
 NEXT_STEP_ID       number(22,0),
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

alter table CLIENT_PROCESS_DECISIONS
   add constraint CLIENT_PROCESS_DECISIONS_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

create table CLIENT_PROCESS_FILE_LOAD
(
 FILE_ID                number(22,0) not null,
 PROCESS_ID             number(22,0),
 STEP_ID                number(22,0),
 FILE_NAME              varchar2(254),
 THE_DELIMITER          varchar2(5),
 DESTINATION_TABLE_NAME varchar2(50),
 TIME_STAMP             date,
 USER_ID                varchar2(18),
 CONTAINS_HEADER        number(1,0),
 USE_SQL_LOADER         number(1,0),
 USE_DIRECT_LOAD        number(1,0),
 TIME_STAMP_AT_END      number(1,0)
);

alter table CLIENT_PROCESS_FILE_LOAD
   add constraint CLIENT_PROCESS_FILE_LOAD_PK
       primary key (FILE_ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_FILE_LOAD
   add constraint CLIENT_PROCESS_FILE_LOAD_FK
       foreign key (PROCESS_ID, STEP_ID)
       references CLIENT_PROCESS_FLOW (PROCESS_ID, STEP_ID);

create table CLIENT_PROCESS_FILE_FORMAT
(
 FILE_ID        number(22,0) not null,
 COLUMN_NAME    varchar2(50) not null,
 ORDER_IN_FILE  number(3,0),
 START_POS      number(3,0),
 END_POS        number(3,0),
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DATA_TYPE      varchar2(254),
 CONSTANT_VALUE varchar2(254)
);

alter table CLIENT_PROCESS_FILE_FORMAT
   add constraint CLIENT_PROCESS_FILE_FORMAT_PK
       primary key (FILE_ID, COLUMN_NAME)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_FILE_FORMAT
   add constraint CLIENT_PROCESS_FILE_FORMAT_FK
       foreign key (FILE_ID)
       references CLIENT_PROCESS_FILE_LOAD (FILE_ID);

CREATE TABLE CLIENT_PROCESS_FILE_LOCATIONS
(
 FILE_ID       number(22,0) not null,
 DATABASE_NAME varchar2(50) not null,
 PATH_OF_FILES varchar2(254),
 PATH_OF_ARC   varchar2(254),
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

alter table CLIENT_PROCESS_FILE_LOCATIONS
   add constraint CLIENT_PROCESS_FILE_LOCS_PK
       primary key (FILE_ID, DATABASE_NAME)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_FILE_LOCATIONS
   add constraint CLIENT_PROCESS_FILE_LOCS_FK
       foreign key (FILE_ID)
       references CLIENT_PROCESS_FILE_LOAD (FILE_ID);

create table CLIENT_PROCESS_LOADS
(
 LOAD_ID         number(22,0) not null,
 PROCESS_ID      number(22,0),
 STEP_ID         number(22,0),
 DESCRIPTION     varchar2(100),
 TABLE_TO_INSERT varchar2(35),
 TIME_STAMP      date,
 USER_ID         varchar2(18)
);

alter table CLIENT_PROCESS_LOADS
   add constraint CLIENT_PROCESS_LOADS_PK
       primary key (LOAD_ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_LOADS
   add constraint CLIENT_PROCESS_LOADS_FK1
       foreign key (PROCESS_ID, STEP_ID)
       references CLIENT_PROCESS_FLOW (PROCESS_ID, STEP_ID);

create table CLIENT_PROCESS_LOADS_GROUP
(
 LOAD_ID         number(22,0) not null,
 GROUP_BY_CLAUSE varchar2(2000),
 HAVING_CLAUSE   varchar2(2000),
 TIME_STAMP      date,
 USER_ID         varchar2(18)
);

alter table CLIENT_PROCESS_LOADS_GROUP
   add constraint CLIENT_PROCESS_LOADS_GROUP_PK
       primary key (LOAD_ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_LOADS_GROUP
   add constraint CLIENT_PROCESS_LOADS_GROUP_FK
       foreign key (LOAD_ID)
       references CLIENT_PROCESS_LOADS_GROUP (LOAD_ID);

create table CLIENT_PROCESS_LOADS_TABLES
(
 TABLE_ID    number(22,0) not null,
 LOAD_ID     number(22,0),
 TABLE_NAME  varchar2(35),
 TABLE_ALIAS varchar2(35),
 TIME_STAMP  date,
 USER_ID     varchar2(18)
);

alter table CLIENT_PROCESS_LOADS_TABLES
   add constraint CLIENT_PROCESS_TABLES_PK
       primary key (TABLE_ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_LOADS_TABLES
   add constraint CLIENT_PROCESS_TABLES_FK
       foreign key (LOAD_ID)
       references CLIENT_PROCESS_LOADS (LOAD_ID);

create table CLIENT_PROCESS_LOADS_MAP
(
 MAP_ID      number(22,0) not null,
 COLUMN_NAME varchar2(35),
 LOAD_ID     number(22,0),
 TABLE_ID    number(22,0),
 VALUE1      varchar2(2000),
 TIME_STAMP  date,
 USER_ID     varchar2(18)
);

alter table CLIENT_PROCESS_LOADS_MAP
   add constraint CLIENT_PROCESS_LOADS_MAP_PK
       primary key (MAP_ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_LOADS_MAP
   add constraint CLIENT_PROCESS_LOADS_MAP_FK
       foreign key (LOAD_ID)
       references CLIENT_PROCESS_LOADS (LOAD_ID);

alter table CLIENT_PROCESS_LOADS_MAP
   add constraint CLIENT_PROCESS_LOADS_MAP_FK2
       foreign key (TABLE_ID)
       references CLIENT_PROCESS_LOADS_TABLES (TABLE_ID);

CREATE TABLE CLIENT_PROCESS_LOADS_WHERE
(
 WHERE_ID   number(22,0) not null,
 LOAD_ID    number(22,0),
 ROW_ID     number(22,0),
 LPAREN     varchar2(1),
 TABLE_ID1  number(22,0),
 FIELD1     varchar2(254),
 OPERATOR   varchar2(35),
 TABLE_ID2  number(22,0),
 VALUE1     varchar2(2000),
 VALUE2     varchar2(2000),
 RPAREN     varchar2(1),
 AND_OR     varchar2(3),
 TIME_STAMP date,
 USER_ID    varchar2(18),
 GROUPBY    varchar2(254)
);

alter table CLIENT_PROCESS_LOADS_WHERE
   add constraint CLIENT_PROCESS_LOADS_WHERE_PK
       primary key (WHERE_ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_LOADS_WHERE
   add constraint CLIENT_PROCESS_LOADS_WHERE_FK
       foreign key (LOAD_ID)
       references CLIENT_PROCESS_LOADS (LOAD_ID);

alter table CLIENT_PROCESS_LOADS_WHERE
   add constraint CLIENT_PROCESS_LOADS_WHERE_FK2
       foreign key (TABLE_ID1)
       references CLIENT_PROCESS_LOADS_TABLES (TABLE_ID);

alter table CLIENT_PROCESS_LOADS_WHERE
   add constraint CLIENT_PROCESS_LOADS_WHERE_FK3
       foreign key (TABLE_ID2)
       references CLIENT_PROCESS_LOADS_TABLES (TABLE_ID);

--
-- With any interface using the processing for validations,
--    int_status of -1 means validation kickout
--    validation_message should be varchar2(2000) on the table being validated
-- BSB: 20120529: Create a table to hold the SETUP to perform validations, updates, deletes
--    STEP_TYPE_IDs: 2, 3, or 4 only
--    IF STEP_TYPE_ID = 2 then outputting sql is:
--       update <table_name> x
--       set int_status = -1,
--          validation_message = <failed_message>
--       where <where_clause>;
--    IF STEP_TYPE_ID = 3 then outputting sql is:
--       update <table_name> x
--       set <set_clause>
--       where <where_clause>;
--    IF STEP_TYPE_ID = 4 then outputting sql is:
--       delete from <table_name> x
--       where <where_clause>;
--
create table CLIENT_PROCESS_SQL_STEP
(
 ID                number(22,0) not null,
 PROCESS_ID        number(22,0),
 STEP_ID           number(22,0),
 TABLE_NAME        varchar2(2000),
 WHERE_CLAUSE1     varchar2(2000),
 WHERE_CLAUSE2     varchar2(2000),
 WHERE_CLAUSE3     varchar2(2000),
 WHERE_CLAUSE4     varchar2(2000),
 WHERE_CLAUSE5     varchar2(2000),
 HARD_STOP         number(1,0),
 PROCESSING_ORDER  number(2,0),
 LOG_MESSAGE_PRE   varchar2(2000),
 LOG_MESSAGE_POST  varchar2(2000),
 LOG_MESSAGE_DEBUG varchar2(2000),
 SET_CLAUSE1       varchar2(4000),
 SET_CLAUSE2       varchar2(4000),
 SET_CLAUSE3       varchar2(4000),
 SET_CLAUSE4       varchar2(4000),
 SET_CLAUSE5       varchar2(4000),
 FAILED_MESSAGE    varchar2(2000),
 TIME_STAMP        date,
 USER_ID           varchar2(18)
);

alter table CLIENT_PROCESS_SQL_STEP
   add constraint CLIENT_PROCESS_SQL_STEP_PK
       primary key (ID)
       using index tablespace PWRPLANT_IDX;

alter table CLIENT_PROCESS_SQL_STEP
   add constraint CLIENT_PROCESS_SQL_STEP_FK1
       foreign key (PROCESS_ID, STEP_ID)
       references CLIENT_PROCESS_FLOW (PROCESS_ID, STEP_ID);

-- Load CLIENT_PROCESS_STEP_TYPE
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (20, 'Oracle Procedure', TO_TIMESTAMP('9-10-2012 15:42:01', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT',
    0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (21, 'Oracle Function', TO_TIMESTAMP('9-10-2012 15:42:01', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT',
    0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (1, 'Insert', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (2, 'Validate', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (3, 'Update', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (4, 'Delete', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (5, 'Start Main Transaction Processing',
    TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (6, 'Start CR Source Processing', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 0, 1);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (7, 'Build Deriver Control', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 1, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (8, 'Process Retirements', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 1, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (9, 'Process Funding Projects', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (10, 'Process Work Orders', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (11, 'Process Job Tasks', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (12, 'Process Unit Estimates', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (13, 'Load File', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (14, 'START', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (15, 'END', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (16, 'Load Argument', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT',
    0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (17, 'Decision', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT', 0, 0);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (18, 'Start Sub Process', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'),
    'PWRPLANT', 0, 1);
insert into CLIENT_PROCESS_STEP_TYPE
   (STEP_TYPE_ID, DESCRIPTION, TIME_STAMP, USER_ID, EXTERNAL_PROCESS, IS_SUB_STEP)
values
   (19, 'End Sub Process', TO_TIMESTAMP('27-09-2012 10:15:26', 'DD-MM-YYYY HH24:MI:SS'), 'PWRPLANT',
    0, 1);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (320, 0, 10, 4, 1, 0, 29198, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_029198_system_base_any_query.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;