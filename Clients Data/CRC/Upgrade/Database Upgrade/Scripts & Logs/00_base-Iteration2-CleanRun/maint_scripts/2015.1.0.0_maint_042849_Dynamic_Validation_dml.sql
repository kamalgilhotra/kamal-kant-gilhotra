/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		new  2
|| File Name:   [MODULE NAME].sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 3/16/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

update WO_VALIDATION_TYPE
   set COL20 = 'workspace_identifier'
 where WO_VALIDATION_TYPE_ID = 21; //UPDATE_PRE_VALIDATION
 
 update WO_VALIDATION_TYPE 
   set COL4 = 'workspace_identifier'
 where WO_VALIDATION_TYPE_ID = 7; //OPEN_POST_VALIDATION
 
   update WO_VALIDATION_TYPE 
   set COL2 = 'workspace_identifier'
 where WO_VALIDATION_TYPE_ID = 66; //CREATE_POST_VALIDATION
 
  update WO_VALIDATION_TYPE 
   set COL4 = 'workspace_identifier'
 where WO_VALIDATION_TYPE_ID = 67; //CREATE_OPEN_VALIDATION

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2386, 0, 2015, 1, 0, 0, 042849, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042849_Dynamic_Validation_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;