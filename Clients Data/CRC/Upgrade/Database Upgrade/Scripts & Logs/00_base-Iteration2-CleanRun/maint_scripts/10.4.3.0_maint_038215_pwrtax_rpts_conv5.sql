/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_038215_pwrtax_rpts_conv5.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 07/24/2014 Anand Rajashekar    script changes to for report conversion
||============================================================================
*/

--New report filter

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
values
   (74, TO_DATE('24-JUL-14', 'DD-MON-RR'), 'PWRPLANT', 'PowerTax - DfitFcst - Normalization', null,
    'uo_tax_selecttabs_rpts_rollup_cons');

insert into PP_REPORTS
   (REPORT_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE,
    TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
    PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT,
    LAST_APPROVED_DATE, PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
values
   (401007, sysdate, user, 'DFIT Fcst Class Report', 'DFIT Fcst Class Report. By Tax Class and Year', 'PowerTaxFcst',
    'dw_tax_rpt_dfit_fcst_class_report7', 'dfit', ' ', '', 'PwrTax - 707', '', '', '', 2, 30, 0, 74, 1, 3, '', '', null,
    '', '', 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (180, 'Normalization Schema', 'dw', 'normalization_schema.normalization_id', null, 'dw_normalization_schema_filter',
    'normalization_id', 'description', 'N', 0, 0, null, null, 'PWRPLANT', TO_DATE('25-JUL-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (181, 'FCST-Forecast Version', 'dw', 'dfit_forecast_output.tax_forecast_version_id', null,
    'dw_tax_fcst_version_by_version_filter', 'tax_forecast_version_id', 'description', 'N', 1, 0, null, null, 'PWRPLANT',
    TO_DATE('28-JUL-14', 'DD-MON-RR'), 0, 0, 0);

insert into PP_DYNAMIC_FILTER
   (FILTER_ID, LABEL, INPUT_TYPE, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, DW, DW_ID, DW_DESCRIPTION, DW_ID_DATATYPE,
    REQUIRED, SINGLE_SELECT_ONLY, VALID_OPERATORS, DATA, USER_ID, TIME_STAMP, EXCLUDE_FROM_WHERE, NOT_RETRIEVE_IMMEDIATE,
    INVISIBLE)
values
   (182, 'FCST-Tax Year', 'dw', 'dfit_forecast_output.tax_year', null, 'dw_tax_year_by_fcst_version_filter',
    'tax_forecast_output_tax_year', 'tax_year_description', 'N', 1, 0, null, null, 'PWRPLANT',
    TO_DATE('28-JUL-14', 'DD-MON-RR'), 0, 0, 0);

--Report mapping

insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (74,157,'PWRPLANT',to_date('28-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (74,176,'PWRPLANT',to_date('25-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (74,177,'PWRPLANT',to_date('25-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (74,180,'PWRPLANT',to_date('25-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (74,181,'PWRPLANT',to_date('25-JUL-14','DD-MON-RR'));
insert into PP_DYNAMIC_FILTER_MAPPING (PP_REPORT_FILTER_ID,FILTER_ID,USER_ID,TIME_STAMP) values (74,182,'PWRPLANT',to_date('25-JUL-14','DD-MON-RR'));

--report restriction

insert into PP_DYNAMIC_FILTER_RESTRICTIONS
   (RESTRICTION_ID, FILTER_ID, RESTRICT_BY_FILTER_ID, SQLS_COLUMN_EXPRESSION, SQLS_WHERE_CLAUSE, USER_ID, TIME_STAMP)
values
   (57, 182, 181, 'tax_forecast_output.tax_forecast_version_id', null, 'PWRPLANT', TO_DATE('28-JUL-14', 'DD-MON-RR'));

--updates for filters 176, 177 and 170

update PP_DYNAMIC_FILTER set LABEL = 'Vintage' where FILTER_ID = 176;

update PP_DYNAMIC_FILTER set LABEL = 'Tax Class' where FILTER_ID = 177;

update PP_DYNAMIC_FILTER set DW_DESCRIPTION = 'tax_year_description' where FILTER_ID = 170;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1305, 0, 10, 4, 3, 0, 38215, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038215_pwrtax_rpts_conv5.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
