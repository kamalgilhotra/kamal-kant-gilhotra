/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008827_JE_BY_ASSET_SPEED.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/13/2012 David Liss     Maint 8827
||============================================================================
*/

create global temporary table PP_JOURNAL_LAYOUTS_TEMP
(
 COMPANY_ID   number(22, 0) not null,
 TRANS_TYPE   number(22, 0) not null,
 JE_METHOD_ID number(22, 0) not null,
 TIME_STAMP   date,
 USER_ID      varchar2(18)
) on commit preserve rows;

alter table PP_JOURNAL_LAYOUTS_TEMP
   add constraint PK_PP_JOURNAL_LAYOUTS_TEMP
       primary key (COMPANY_ID, TRANS_TYPE, JE_METHOD_ID);

CREATE TABLE PP_JOURNAL_DATA_SQL
(
 TRANS_TYPE NUMBER(22,0) not null,
 KEY_TYPE   VARCHAR2(35),
 user_id    VARCHAR2(18),
 time_stamp DATE,
 SQLS_1     VARCHAR2(4000),
 SQLS_2     VARCHAR2(4000)
);

ALTER TABLE PP_JOURNAL_DATA_SQL
   add constraint PK_PP_JOURNAL_DATA_SQL
       primary key (TRANS_TYPE)
       USING INDEX tablespace PWRPLANT_IDX;

alter table PP_JOURNAL_DATA_SQL
   add constraint FK_PP_JDS_TRANS_TYPE
       foreign key (TRANS_TYPE)
       references JE_TRANS_TYPE;

create global temporary table PP_GL_TRANS_TEMP
(
 KEY_TYPE VARCHAR2(35),
 KEY_ID   NUMBER(22,0) not null,
 AMOUNT   NUMBER(22,2),
 COLUMN1  VARCHAR2(35),
 COLUMN2  VARCHAR2(35),
 COLUMN3  VARCHAR2(35),
 COLUMN4  VARCHAR2(35),
 COLUMN5  VARCHAR2(35),
 COLUMN6  VARCHAR2(35),
 COLUMN7  VARCHAR2(35),
 COLUMN8  VARCHAR2(35),
 COLUMN9  VARCHAR2(35),
 COLUMN10 VARCHAR2(35),
 COLUMN11 VARCHAR2(35),
 COLUMN12 VARCHAR2(35),
 COLUMN13 VARCHAR2(35),
 COLUMN14 VARCHAR2(35),
 COLUMN15 VARCHAR2(35),
 COLUMN16 VARCHAR2(35),
 COLUMN17 VARCHAR2(35),
 COLUMN18 VARCHAR2(35),
 COLUMN19 VARCHAR2(35),
 COLUMN20 VARCHAR2(35)
) on commit preserve rows;

alter table PP_GL_TRANS_TEMP
   add constraint PK_PP_GL_TRANS_TEMP
      primary key (KEY_ID);

insert into PP_JOURNAL_DATA_SQL (TRANS_TYPE, USER_ID, TIME_STAMP, SQLS_1, SQLS_2, KEY_TYPE)
values (39, 'PWRPLANT', to_date('19-11-2011 15:26:53', 'dd-mm-yyyy hh24:mi:ss'), 'select rownum, alloc_expense amount, company_id, 39 trans_type,
       1 je_method_id, gl_account_id, asset_id, total_expense, accum_cost,
       alloc_expense, to_number(substr(alloc_expense_ratio, 1, 35)) alloc_expense_ratio, total_cost, depr_group_id
  from (select asset_id, total_expense, accum_cost,
                case
                  when last_is_null is null then
                   total_expense - sum(alloc_expense) over() + alloc_expense
                  else
                   alloc_expense
                end alloc_expense, alloc_expense_ratio, total_cost, company_id,
                depr_group_id, expense_acct_id gl_account_id
           from (select cl.asset_id,
                         depreciation_expense + depr_exp_adjust +
                          depr_exp_alloc_adjust -
                          nvl(current_net_salvage_amort, 0) + salvage_expense +
                          salvage_exp_adjust + salvage_exp_alloc_adjust +
                          cor_expense + cor_exp_adjust + cor_exp_alloc_adjust total_expense,
                         sum(accum_cost) accum_cost,
                         round((RATIO_TO_REPORT(SUM(accum_cost)) OVER()) *
                                (depreciation_expense + depr_exp_adjust +
                                depr_exp_alloc_adjust -
                                nvl(current_net_salvage_amort, 0) +
                                salvage_expense + salvage_exp_adjust +
                                salvage_exp_alloc_adjust + cor_expense +
                                cor_exp_adjust + cor_exp_alloc_adjust),
                                2) alloc_expense,
                         RATIO_TO_REPORT(SUM(accum_cost)) OVER() alloc_expense_ratio,
                         sum(sum(accum_cost)) over() total_cost, dg.company_id,
                         lead(accum_cost) over(partition by dg.depr_group_id order by accum_cost) last_is_null,
                         cl.depr_group_id, dg.expense_acct_id
                    from cpr_ledger cl, depr_ledger dl, depr_group dg,
                         subledger_control sc
                   where cl.depr_group_id = dg.depr_group_id
                     and sc.subledger_type_id = dg.subledger_type_id
                     and sc.depreciation_indicator = 0
                     and dl.depr_group_id = cl.depr_group_id
                     and to_number(to_char(gl_post_mo_yr, ''yyyymm'')) = <arg1>
                     and dg.company_id = <arg2>
                     and dg.depr_group_id = <arg3>
                     and dl.set_of_books_id = <arg4>
                   group by cl.asset_id,
                            depreciation_expense + depr_exp_adjust +
                             depr_exp_alloc_adjust -
                             nvl(current_net_salvage_amort, 0) + salvage_expense +
                             salvage_exp_adjust + salvage_exp_alloc_adjust +
                             cor_expense + cor_exp_adjust + cor_exp_alloc_adjust,
                            dg.company_id, dg.depr_group_id, accum_cost,
                            cl.depr_group_id, dg.expense_acct_id)', 'union
         select cd.asset_id, 0, asset_dollars,
                (nvl(curr_depr_expense, 0) + nvl(depr_exp_alloc_adjust, 0) +
                 nvl(depr_exp_adjust, 0) + nvl(salvage_expense, 0) +
                 nvl(salvage_exp_adjust, 0) +
                 nvl(salvage_exp_alloc_adjust, 0)) depr_expense, 0, 0,
                cl.company_id, cl.depr_group_id, dg.expense_acct_id
           from cpr_depr cd, cpr_ledger cl, subledger_control st, depr_group dg
          where to_number(to_char(gl_posting_mo_yr, ''yyyymm'')) = <arg1>
            and cd.depr_group_id = <arg3>
            and set_of_books_id = <arg4>
            and cl.company_id = <arg2>
            and cd.asset_Id = cl.asset_id
            and cl.subledger_indicator = st.subledger_type_id
            and st.depreciation_indicator = 3
            and dg.depr_group_id = cl.depr_group_id)
 where alloc_expense <> 0', 'ASSET_ID');

insert into PP_JOURNAL_DATA_SQL (TRANS_TYPE, USER_ID, TIME_STAMP, SQLS_1, SQLS_2, KEY_TYPE)
values (40, 'PWRPLANT', to_date('19-11-2011 15:26:53', 'dd-mm-yyyy hh24:mi:ss'), 'select rownum, alloc_expense amount, company_id, 40 trans_type,
       1 je_method_id, gl_account_id, asset_id, total_expense, accum_cost,
       alloc_expense, to_number(substr(alloc_expense_ratio, 1, 35)) alloc_expense_ratio, total_cost, depr_group_id
  from (select asset_id, total_expense, accum_cost,
                case
                  when last_is_null is null then
                   total_expense - sum(alloc_expense) over() + alloc_expense
                  else
                   alloc_expense
                end alloc_expense, alloc_expense_ratio, total_cost, company_id,
                depr_group_id, reserve_acct_id gl_account_id
           from (select cl.asset_id,
                         depreciation_expense + depr_exp_adjust +
                          depr_exp_alloc_adjust -
                          nvl(current_net_salvage_amort, 0) + salvage_expense +
                          salvage_exp_adjust + salvage_exp_alloc_adjust +
                          cor_expense + cor_exp_adjust + cor_exp_alloc_adjust total_expense,
                         sum(accum_cost) accum_cost,
                         round((RATIO_TO_REPORT(SUM(accum_cost)) OVER()) *
                                (depreciation_expense + depr_exp_adjust +
                                depr_exp_alloc_adjust -
                                nvl(current_net_salvage_amort, 0) +
                                salvage_expense + salvage_exp_adjust +
                                salvage_exp_alloc_adjust + cor_expense +
                                cor_exp_adjust + cor_exp_alloc_adjust),
                                2) alloc_expense,
                         RATIO_TO_REPORT(SUM(accum_cost)) OVER() alloc_expense_ratio,
                         sum(sum(accum_cost)) over() total_cost, dg.company_id,
                         lead(accum_cost) over(partition by dg.depr_group_id order by accum_cost) last_is_null,
                         cl.depr_group_id, dg.reserve_acct_id
                    from cpr_ledger cl, depr_ledger dl, depr_group dg,
                         subledger_control sc
                   where cl.depr_group_id = dg.depr_group_id
                     and sc.subledger_type_id = dg.subledger_type_id
                     and sc.depreciation_indicator = 0
                     and dl.depr_group_id = cl.depr_group_id
                     and to_number(to_char(gl_post_mo_yr, ''yyyymm'')) = <arg1>
                     and dg.company_id = <arg2>
                     and dg.depr_group_id = <arg3>
                     and dl.set_of_books_id = <arg4>
                   group by cl.asset_id,
                            depreciation_expense + depr_exp_adjust +
                             depr_exp_alloc_adjust -
                             nvl(current_net_salvage_amort, 0) + salvage_expense +
                             salvage_exp_adjust + salvage_exp_alloc_adjust +
                             cor_expense + cor_exp_adjust + cor_exp_alloc_adjust,
                            dg.company_id, dg.depr_group_id, accum_cost,
                            cl.depr_group_id, dg.reserve_acct_id)', 'union
         select cd.asset_id, 0, asset_dollars,
                (nvl(curr_depr_expense, 0) + nvl(depr_exp_alloc_adjust, 0) +
                 nvl(depr_exp_adjust, 0) + nvl(salvage_expense, 0) +
                 nvl(salvage_exp_adjust, 0) +
                 nvl(salvage_exp_alloc_adjust, 0)) depr_expense, 0, 0,
                cl.company_id, cl.depr_group_id, dg.reserve_acct_id
           from cpr_depr cd, cpr_ledger cl, subledger_control st, depr_group dg
          where to_number(to_char(gl_posting_mo_yr, ''yyyymm'')) = <arg1>
            and cd.depr_group_id = <arg3>
            and set_of_books_id = <arg4>
            and cl.company_id = <arg2>
            and cd.asset_Id = cl.asset_id
            and cl.subledger_indicator = st.subledger_type_id
            and st.depreciation_indicator = 3
            and dg.depr_group_id = cl.depr_group_id)
 where alloc_expense <> 0', 'ASSET_ID');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (71, 0, 10, 3, 4, 0, 8827, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_008827_JE_BY_ASSET_SPEED.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
