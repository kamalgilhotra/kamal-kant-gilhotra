/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_043057_jobserver_inc_20150227_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     03/03/2014 Paul Cordero    	 Creating missing trigger for 'PP_WEB_LOG'
||==========================================================================================
*/

CREATE OR REPLACE TRIGGER PWRPLANT.PP_WEB_LOG
  BEFORE INSERT ON PP_WEB_LOG
  FOR EACH ROW
BEGIN
    :new.WEB_LOG_ID := PP_WEB_LOG_SEQ.nextval;
    :new.TIME_STAMP := SYSDATE;
END; 
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2341, 0, 2015, 1, 0, 0, 043057, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043057_jobserver_inc_20150227_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;