/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050379_lessor_04_add_termination_filter_table_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 05/31/2018 Jared Watkins    Add new table to allow us to filter by ILRs in the termination logic
||============================================================================
*/

create global temporary table lsr_term_ilr_filter(
  ilr_id number(22,0) not null
);

alter table lsr_term_ilr_filter
ADD CONSTRAINT lsr_term_ilr_filter_pk
primary key (ilr_id);
  
comment on table lsr_term_ilr_filter is 'Table to store ILRs selected for processing terminations';

comment on column lsr_term_ilr_filter.ilr_id is 'System generated id of the ILR';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6224, 0, 2017, 4, 0, 0, 50379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050379_lessor_04_add_termination_filter_table_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;