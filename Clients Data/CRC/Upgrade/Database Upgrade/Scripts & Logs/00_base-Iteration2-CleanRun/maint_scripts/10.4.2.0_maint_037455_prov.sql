/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_037455_prov.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 04/03/2014 Nathan Hollis
||============================================================================
*/

------------------------------------------------------------------------------------
----FIX 9888
------------------------------------------------------------------------------------

alter table TAX_ACCRUAL_DMI_PROCESS_INFO modify PROVISIONENDMONTH number(22,2);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1094, 0, 10, 4, 2, 0, 37455, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_037455_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
