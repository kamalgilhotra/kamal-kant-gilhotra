/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041850_prov_etr_grid_fix_dml.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 12/16/2014 Blake Andrews    Fix Missing Amount on ETR Report
||============================================================================
*/

update tax_accrual_rep_cons_rows 
set display_ind_field = 'display_no_adjustments_item'
where rep_cons_type_id in (30,31,32,33)
and label_value = 'detail_description';

update tax_accrual_rep_cons_rows 
set display_ind_field = trim(display_ind_field) || ' * display_no_adjustments_item'
where rep_cons_type_id in (30,31,32,33)
and trim(display_ind_field) in ('format_011_subtotal_visible','format_001_rollup_description_visible');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2118, 0, 2015, 1, 0, 0, 041850, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041850_prov_etr_grid_fix_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;