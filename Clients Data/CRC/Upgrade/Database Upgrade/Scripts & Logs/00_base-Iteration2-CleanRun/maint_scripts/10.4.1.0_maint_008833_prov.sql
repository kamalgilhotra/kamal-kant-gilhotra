/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008833_prov.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   03/27/2013 Blake Andrews  Point Release
||============================================================================
*/

create global temporary table TAX_ACCRUAL_APPROVAL_CO_TMP
(
 COMPANY_ID        number(22,0),
 GL_MONTH          number(22,2),
 RUN_NUMBER        number(22,0),
 CR_COMPANY_ID     number(22,0),
 NON_CR_COMPANY_ID number(22,0)
) on commit preserve rows;

alter table TAX_ACCRUAL_APPROVAL_CO_TMP
   add constraint TA_APPROVAL_CO_TMP_PK
       primary key (COMPANY_ID, GL_MONTH);

comment on table TAX_ACCRUAL_APPROVAL_CO_TMP is '(T) [08] holds the Tax accrual approval company_id and gl_month combination the user would like to review/approve.  The run_number column will be populated if journal entries exist in tax_accrual_je_export, else it will be null.';
comment on column TAX_ACCRUAL_APPROVAL_CO_TMP.COMPANY_ID is 'The company ID';
comment on column TAX_ACCRUAL_APPROVAL_CO_TMP.GL_MONTH is 'The GL month';
comment on column TAX_ACCRUAL_APPROVAL_CO_TMP.RUN_NUMBER is 'The run number';
comment on column TAX_ACCRUAL_APPROVAL_CO_TMP.CR_COMPANY_ID is 'The CR company ID (nullable)';
comment on column TAX_ACCRUAL_APPROVAL_CO_TMP.NON_CR_COMPANY_ID is 'The non-CR company ID';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (335, 0, 10, 4, 1, 0, 8833, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_008833_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;