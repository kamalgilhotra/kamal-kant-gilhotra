/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_048330_lease_11_mview_def_rent_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.1.0.0  08/25/2017 build script   2017.1.0.0 Release
||============================================================================
*/

drop materialized view mv_multicurr_ls_asset_inner;
create materialized view mv_multicurr_ls_asset_inner refresh fast on commit as
select
  la.ilr_id,
  las.ls_asset_id,
  las.revision,
  las.set_of_books_id,
  las.MONTH,
  la.contract_currency_id,
  las.residual_amount,
  las.term_penalty,
  las.bpo_price,
  las.beg_capital_cost,
  las.end_capital_cost,
  las.beg_obligation,
  las.end_obligation,
  las.beg_lt_obligation,
  las.end_lt_obligation,
  las.interest_accrual,
  las.principal_accrual,
  las.interest_paid,
  las.principal_paid,
  las.executory_accrual1,
  las.executory_accrual2,
  las.executory_accrual3,
  las.executory_accrual4,
  las.executory_accrual5,
  las.executory_accrual6,
  las.executory_accrual7,
  las.executory_accrual8,
  las.executory_accrual9,
  las.executory_accrual10,
  las.executory_paid1,
  las.executory_paid2,
  las.executory_paid3,
  las.executory_paid4,
  las.executory_paid5,
  las.executory_paid6,
  las.executory_paid7,
  las.executory_paid8,
  las.executory_paid9,
  las.executory_paid10,
  las.contingent_accrual1,
  las.contingent_accrual2,
  las.contingent_accrual3,
  las.contingent_accrual4,
  las.contingent_accrual5,
  las.contingent_accrual6,
  las.contingent_accrual7,
  las.contingent_accrual8,
  las.contingent_accrual9,
  las.contingent_accrual10,
  las.contingent_paid1,
  las.contingent_paid2,
  las.contingent_paid3,
  las.contingent_paid4,
  las.contingent_paid5,
  las.contingent_paid6,
  las.contingent_paid7,
  las.contingent_paid8,
  las.contingent_paid9,
  las.contingent_paid10,
  las.current_lease_cost,
  las.beg_deferred_rent,
  las.deferred_rent,
  las.end_deferred_rent,
  las.beg_st_deferred_rent,
  las.end_st_deferred_rent,
  ldf.depr_expense,
  ldf.begin_reserve,
  ldf.end_reserve,
  ldf.depr_exp_alloc_adjust,
  la.company_id,
  opt.in_service_exchange_rate,
  la.description as asset_description,
  la.leased_asset_number,
  la.fmv,
  las.is_om,
  la.approved_revision,
  opt.lease_cap_type_id,
  la.ls_asset_status_id,
  la.retirement_date,
  las.ROWID rowidlas,
  la.rowid rowidla,
  opt.ROWID rowidopt,
  ldf.rowid rowidldf
FROM ls_asset_schedule las,
     ls_asset la,
     ls_ilr_options opt,
     ls_depr_forecast ldf
WHERE las.ls_asset_id = la.ls_asset_id
AND la.ilr_id = opt.ilr_id
AND las.revision = opt.revision
AND las.ls_asset_id = ldf.ls_asset_id (+)
AND las.revision = ldf.revision (+)
AND las.set_of_books_id = ldf.set_of_books_id (+)
AND las.month = ldf.month (+);

CREATE INDEX mv_mc_ls_asset_in_ctc_cur_idx ON mv_multicurr_ls_asset_inner (contract_currency_id) TABLESPACE pwrplant_idx compute statistics;
CREATE INDEX mv_mc_ls_asset_in_cmpy_cur_idx on mv_multicurr_ls_asset_inner (company_id, contract_currency_id) tablespace pwrplant_idx compute statistics;
CREATE INDEX mv_mc_ls_asset_in_sob_idx ON mv_multicurr_ls_asset_inner (set_of_books_id) TABLESPACE pwrplant_idx compute statistics;
CREATE INDEX mv_mc_ls_asset_in_msob_idx ON mv_multicurr_ls_asset_inner(MONTH, set_of_books_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_ls_asset_in_many_idx ON mv_multicurr_ls_asset_inner (contract_currency_id, MONTH, company_id, set_of_books_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_ls_asset_in_astid_idx ON mv_multicurr_ls_asset_inner (ls_asset_id) TABLESPACE pwrplant_idx compute statistics;

drop materialized view mv_multicurr_lis_inner_amounts;
create materialized view mv_multicurr_lis_inner_amounts refresh fast on commit as
SELECT  LIS.ilr_id,
        ilr.ilr_number,
        ilr.current_revision,
        lis.revision,
        lis.set_of_books_id,
        lis.MONTH,
        lis.beg_capital_cost,
        lis.end_capital_cost,
        lis.beg_obligation,
        lis.end_obligation,
        lis.beg_lt_obligation,
        lis.end_lt_obligation,
        lis.interest_accrual,
        lis.principal_accrual,
        lis.interest_paid,
        lis.principal_paid,
        lis.executory_accrual1,
        lis.executory_accrual2,
        lis.executory_accrual3,
        lis.executory_accrual4,
        lis.executory_accrual5,
        lis.executory_accrual6,
        lis.executory_accrual7,
        lis.executory_accrual8,
        lis.executory_accrual9,
        lis.executory_accrual10,
        lis.executory_paid1,
        lis.executory_paid2,
        lis.executory_paid3,
        lis.executory_paid4,
        lis.executory_paid5,
        lis.executory_paid6,
        lis.executory_paid7,
        lis.executory_paid8,
        lis.executory_paid9,
        lis.executory_paid10,
        lis.contingent_accrual1,
        lis.contingent_accrual2,
        lis.contingent_accrual3,
        lis.contingent_accrual4,
        lis.contingent_accrual5,
        lis.contingent_accrual6,
        lis.contingent_accrual7,
        lis.contingent_accrual8,
        lis.contingent_accrual9,
        lis.contingent_accrual10,
        lis.contingent_paid1,
        lis.contingent_paid2,
        lis.contingent_paid3,
        lis.contingent_paid4,
        lis.contingent_paid5,
        lis.contingent_paid6,
        lis.contingent_paid7,
        lis.contingent_paid8,
        lis.contingent_paid9,
        lis.contingent_paid10,
        lis.current_lease_cost,
		lis.beg_deferred_rent,
		lis.deferred_rent,
		lis.end_deferred_rent,
		lis.beg_st_deferred_rent,
		lis.end_st_deferred_rent,
        ilr.lease_id,
        ilr.company_id,
        opt.in_service_exchange_rate,
        opt.purchase_option_amt,
        opt.termination_amt,
        liasob.net_present_value,
        liasob.capital_cost,
        LIS.is_om,
        LIS.ROWID AS lisrowid,
        opt.ROWID AS optrowid,
        liasob.ROWID AS liasobrowid,
        ilr.ROWID AS ilrrowid,
        lease.rowid as leaserowid
FROM  ls_ilr_schedule LIS,
      ls_ilr_options opt,
      ls_ilr_amounts_set_of_books liasob,
      ls_ilr ilr,
      ls_lease lease
WHERE LIS.ilr_id = opt.ilr_id
      AND lis.revision = opt.revision
AND LIS.ilr_id = liasob.ilr_id
    AND lis.revision = liasob.revision
    AND LIS.set_of_books_id = liasob.set_of_books_id
AND LIS.ilr_id = ilr.ilr_id
AND ilr.lease_id = lease.lease_id;

CREATE INDEX mv_mc_lis_inner_amounts_idx ON mv_multicurr_lis_inner_amounts(ilr_id, revision, MONTH, set_of_books_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amounts_rev_id ON mv_multicurr_lis_inner_amounts(revision) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amts_month_idx ON mv_multicurr_lis_inner_amounts(MONTH) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amts_idx2 ON mv_multicurr_lis_inner_amounts(company_id, lease_id) TABLESPACE pwrplant_idx COMPUTE STATISTICS;
CREATE INDEX mv_mc_lis_inner_amts_lease_idx ON mv_multicurr_lis_inner_amounts(lease_id) TABLESPACE pwrplant_idx compute statistics;
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4027, 0, 2017, 1, 0, 0, 48330, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048330_lease_11_mview_def_rent_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;