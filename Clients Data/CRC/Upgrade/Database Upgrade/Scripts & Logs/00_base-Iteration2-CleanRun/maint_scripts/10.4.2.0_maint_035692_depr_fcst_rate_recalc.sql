/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035692_depr_fcst_rate_recalc.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 02/11/2014  Kyle Peterson
||============================================================================
*/

declare
   MAX_ID number;
begin

   begin
      select NVL(max(RESERVE_RATIO_ID), 0) + 1 into MAX_ID from RESERVE_RATIOS;
   exception
      when NO_DATA_FOUND then
         MAX_ID := 1;
   end;

   execute immediate 'create sequence RESERVE_RATIOS_SEQ start with ' || TO_CHAR(MAX_ID) || ' ';
end;
/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION)
   (select max(PROCESS_ID) + 1, 'PL/SQL Debugger', 'PL/SQL Debugger' from PP_PROCESSES);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (955, 0, 10, 4, 2, 0, 35692, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035692_depr_fcst_rate_recalc.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
