/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_035668_proptax.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/30/2014 Andrew Scott        Approval workspace bug.  new workflow
||                                         objects being used need approval auth
||                                         level.default_value = 1 for prop tax.
||============================================================================
*/

update APPROVAL_AUTH_LEVEL
   set DEFAULT_USER = 1
 where APPROVAL_TYPE_ID in
       (select WORKFLOW_TYPE_ID from WORKFLOW_TYPE where LOWER(SUBSYSTEM) like '%proptax%')
   and DEFAULT_USER is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (932, 0, 10, 4, 2, 0, 35668, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035668_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;