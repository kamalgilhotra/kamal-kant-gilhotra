/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038663_budget_wo_work_order_id.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.2 07/10/2014 Kyle Peterson
||============================================================================
*/

alter table clearing_wo_control_bdg add load_by_wo number(22,0);
alter table wo_est_processing_mo_id add wo_work_order_id number(22,0);
alter table wo_est_processing_transpose add wo_work_order_id number(22,0);

comment on column CLEARING_WO_CONTROL_BDG.LOAD_BY_WO is 'Yes (1), No (0, null).  If ‘Yes’ apply the loading by work order.  I.e. two different estimated work orders will create two loading amounts.  ‘No’-combine into a single loading.';
comment on column WO_EST_PROCESSING_MO_ID.WO_WORK_ORDER_ID is 'System assigned identifier for a work order';
comment on column WO_EST_PROCESSING_TRANSPOSE.WO_WORK_ORDER_ID is 'System assigned identifier for a work order';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1253, 0, 10, 4, 2, 2, 38663, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038663_budget_wo_work_order_id.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;