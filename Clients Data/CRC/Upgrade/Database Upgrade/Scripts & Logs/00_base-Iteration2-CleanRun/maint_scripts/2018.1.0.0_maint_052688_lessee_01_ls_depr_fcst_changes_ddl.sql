/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_052688_lessee_01_ls_depr_fcst_changes_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 11/13/2018 Sarah Byers    Add new fields to ls_depr_forecast for audit purposes
||============================================================================
*/

-- Add new fields for audit purposes
alter table ls_depr_forecast add (
depr_group_id number(22,0),
mid_period_method varchar2(35),
mid_period_conv number(22,8),
partial_month_percent number(22,8) default 1,
depr_exp_comp_curr number(22,2),
depr_exp_aa_comp_curr number(22,2),
exchange_rate number(22,8));

comment on column ls_depr_forecast.depr_group_id is 'Depreciation group associated with the asset at the time; saved for audit purposes.';
comment on column ls_depr_forecast.mid_period_method is 'Depreciation Method used at the time (e.g. SL, SLE, FERC); saved for audit purposes.';
comment on column ls_depr_forecast.mid_period_conv is 'Mid-month convention used at the time (0 = Beginning of Month, .5 = Mid-Month, 1 = End of Month); saved for audit purposes.';
comment on column ls_depr_forecast.partial_month_percent is 'The percentage of the expense considered for the month when using Partial Month payment type; default is 1.';
comment on column ls_depr_forecast.depr_exp_comp_curr is 'The depreciation expense for the month converted to company currency.';
comment on column ls_depr_forecast.depr_exp_aa_comp_curr is 'The adjustment to the depreciation expense for the month converted to company currency';
comment on column ls_depr_forecast.exchange_rate is 'The exchange rate used to convert depreciation expense to company currency.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (12082, 0, 2018, 1, 0, 0, 52688, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_052688_lessee_01_ls_depr_fcst_changes_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;