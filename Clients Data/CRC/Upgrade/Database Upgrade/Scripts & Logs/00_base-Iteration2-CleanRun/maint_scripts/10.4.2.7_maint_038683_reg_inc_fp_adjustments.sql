/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038683_reg_inc_fp_adjustments.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/01/2014 Shane Ward       Data tables for REG Incremental FP Adjustments
||============================================================================
*/

--First things first
alter table INCREMENTAL_FP_RUN_LOG modify TIMESTAMP DATE;

--REG_INCREMENTAL_LABEL_MAP
create table REG_INCREMENTAL_LABEL_MAP
(
 LABEL_ID        number(22,0) not null,
 REG_FAMILY_ID   number(22,0) not null,
 USER_ID         varchar(18)  null,
 TIMESTAMP       date         null
);

alter table REG_INCREMENTAL_LABEL_MAP
   add constraint PK_REG_INCREMENTAL_LABEL_MAP
       primary key (LABEL_ID, REG_FAMILY_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_LABEL_MAP
   add constraint fk_reg_incremental_label_map1
       foreign key (LABEL_ID)
       references INCREMENTAL_FP_ADJ_LABEL (LABEL_ID);

alter table REG_INCREMENTAL_LABEL_MAP
   add constraint FK_REG_INCREMENTAL_LABEL_MAP2
       foreign key (REG_FAMILY_ID)
       references REG_FAMILY (REG_FAMILY_ID);

--REG_INCREMENTAL_ITEM_MAP
create table REG_INCREMENTAL_ITEM_MAP
(
 LABEL_ID              number(22,0) not null,
 ITEM_ID               number(22,0) not null,
 STATUS                number(22,0) not null,
 REG_FAMILY_ID         number(22,0) not null,
 REG_CWIP_COMPONENT_ID number(22,0) null,
 REG_DEPR_COMPONENT_ID number(22,0) null,
 REG_TAX_COMPONENT_ID  number(22,0) null,
 USER_ID               varchar(18)  null,
 TIMESTAMP             date         null
);

alter table REG_INCREMENTAL_ITEM_MAP
   add constraint PK_REG_INCREMENTAL_ITEM_MAP
       primary key (LABEL_ID, ITEM_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_ITEM_MAP
   add constraint FK_REG_INCREMENTAL_ITEM_MAP1
       foreign key (LABEL_ID, ITEM_ID)
       references INCREMENTAL_FP_ADJ_ITEM (LABEL_ID, ITEM_ID);

alter table REG_INCREMENTAL_ITEM_MAP
   add constraint FK_REG_INCREMENTAL_ITEM_MAP3
       foreign key (REG_FAMILY_ID)
       references REG_FAMILY (REG_FAMILY_ID);

alter table REG_INCREMENTAL_ITEM_MAP
   add constraint FK_REG_INCREMENTAL_ITEM_MAP4
       foreign key (REG_CWIP_COMPONENT_ID, REG_FAMILY_ID)
       references REG_CWIP_FAMILY_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

alter table REG_INCREMENTAL_ITEM_MAP
   add constraint FK_REG_INCREMENTAL_ITEM_MAP5
       foreign key (REG_DEPR_COMPONENT_ID)
       references REG_DEPR_FAMILY_COMPONENTS (REG_COMPONENT_ID);

alter table REG_INCREMENTAL_ITEM_MAP
   add constraint FK_REG_INCREMENTAL_ITEM_MAP6
       foreign key (REG_TAX_COMPONENT_ID, REG_FAMILY_ID)
       references REG_TAX_COMPONENT (REG_COMPONENT_ID, REG_FAMILY_ID);

--REG_INCREMENTAL_ADJUSTMENT_TYPE
create table REG_INCREMENTAL_ADJUST_TYPE
(
 INCREMENTAL_ADJ_TYPE_ID   number(22,0)  not null,
 DESCRIPTION               varchar2(35)  not null,
 LONG_DESCRIPTION          varchar2(254) not null,
 USER_ID                   varchar(18)   null,
 TIMESTAMP                 date          null
);

alter table REG_INCREMENTAL_ADJUST_TYPE
   add constraint PK_REG_INC_ADJUST_TYPE
       primary key (INCREMENTAL_ADJ_TYPE_ID)
       using index tablespace PWRPLANT_IDX;

--REG_INCREMENTAL_ADJUSTMENT
create table REG_INCREMENTAL_ADJUSTMENT
(
 INCREMENTAL_ADJ_ID            number(22,0)    not null,
 INCREMENTAL_ADJ_TYPE_ID       number(22,0)    not null,
 DESCRIPTION                   varchar2(35)    not null,
 LONG_DESCRIPTION              varchar2(254)   null,
 ORIG_WORK_ORDER_ID            number(22,0)    null,
 ORIG_REVISION                 number(22,0)    null,
 ORIG_BUDGET_VERSION_ID        number(22,0)    not null,
 ORIG_FCST_DEPR_VERSION_ID     number(22,0)    not null,
 ORIG_SET_OF_BOOKS_ID          number(22,0)    not null,
 ORIG_VERSION_ID               number(22,0)    not null,
 UPDATED_WORK_ORDER_ID         number(22,0)    null,
 UPDATED_REVISION              number(22,0)    null,
 UPDATED_BUDGET_VERSION_ID     number(22,0)    not null,
 UPDATED_FCST_DEPR_VERSION_ID  number(22,0)    not null,
 UPDATED_SET_OF_BOOKS_ID       number(22,0)    not null,
 UPDATED_VERSION_ID            number(22,0)    not null,
 USER_ID                       varchar(18)     null,
 TIMESTAMP                     date            null
);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint PK_REG_INCREMENTAL_ADJUSTMENT
       primary key (INCREMENTAL_ADJ_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT1
       foreign key (INCREMENTAL_ADJ_TYPE_ID)
       references REG_INCREMENTAL_ADJUST_TYPE (INCREMENTAL_ADJ_TYPE_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT2
       foreign key (ORIG_WORK_ORDER_ID, ORIG_REVISION)
       references WORK_ORDER_APPROVAL (WORK_ORDER_ID, REVISION);

alter table reg_incremental_adjustment
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT3
       foreign key (UPDATED_WORK_ORDER_ID, UPDATED_REVISION)
       references WORK_ORDER_APPROVAL (WORK_ORDER_ID, REVISION);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT4
       foreign key (ORIG_BUDGET_VERSION_ID)
       references BUDGET_VERSION (BUDGET_VERSION_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT5
       foreign key (UPDATED_BUDGET_VERSION_ID)
       references BUDGET_VERSION (BUDGET_VERSION_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT6
       foreign key (ORIG_FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT7
       foreign key (UPDATED_FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT8
       foreign key (ORIG_SET_OF_BOOKS_ID)
       references SET_OF_BOOKS (SET_OF_BOOKS_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMENT9
       foreign key (UPDATED_SET_OF_BOOKS_ID)
       references SET_OF_BOOKS (SET_OF_BOOKS_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMEN10
       foreign key (ORIG_VERSION_ID)
       references VERSION (VERSION_ID);

alter table REG_INCREMENTAL_ADJUSTMENT
   add constraint FK_REG_INCREMENTAL_ADJUSTMEN11
       foreign key (UPDATED_VERSION_ID)
       references version (version_id);

--REG_INCREMENTAL_ADJ_VERSION
create table REG_INCREMENTAL_ADJ_VERSION
(
 HISTORIC_VERSION_ID   number(22,0) not null,
 FORECAST_VERSION_ID   number(22,0) not null,
 INCREMENTAL_ADJ_ID    number(22,0) not null,
 STATUS_CODE_ID        number(22,0) not null,
 USER_ID               varchar(18)  null,
 TIMESTAMP             date         null
);

alter table REG_INCREMENTAL_ADJ_VERSION
   add constraint PK_REG_INCREMENTAL_ADJ_VERSION
       primary key (HISTORIC_VERSION_ID, FORECAST_VERSION_ID, INCREMENTAL_ADJ_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_ADJ_VERSION
   add constraint fk_reg_incremental_adj_vers1
       foreign key (HISTORIC_VERSION_ID)
       references REG_HISTORIC_VERSION (HISTORIC_VERSION_ID);

alter table REG_INCREMENTAL_ADJ_VERSION
   add constraint FK_REG_INCREMENTAL_ADJ_VERS2
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_INCREMENTAL_ADJ_VERSION
   add constraint FK_REG_INCREMENTAL_ADJ_VERS3
       foreign key (INCREMENTAL_ADJ_ID)
       references REG_INCREMENTAL_ADJUSTMENT (INCREMENTAL_ADJ_ID);

--REG_INCREMENTAL_FP_ADJ_TRANS
create table REG_INCREMENTAL_FP_ADJ_TRANS
(
 FORECAST_VERSION_ID   number(22,0) not null,
 INCREMENTAL_ADJ_ID    number(22,0) not null,
 LABEL_ID              number(22,0) not null,
 ITEM_ID               number(22,0) not null,
 WORK_ORDER_ID         number(22,0) not null,
 REVISION              number(22,0) not null,
 BUDGET_VERSION_ID     number(22,0) not null,
 FCST_DEPR_VERSION_ID  number(22,0) not null,
 VERSION_ID            number(22,0) not null,
 REG_COMPANY_ID        number(22,0) null,
 REG_ACCT_ID           number(22,2) null,
 OVERRIDE_TRANSLATE    number(22,0) null,
 OVERRIDE_REG_ACCT_ID  number(22,0) null,
 FCST_DEPR_GROUP_ID    number(22,0) null,
 SET_OF_BOOKS_ID       number(22,0) null,
 EXPENDITURE_TYPE_ID   number(22,0) null,
 EST_CHG_TYPE_ID       number(22,0) null,
 WIP_COMPUTATION_ID    number(22,0) null,
 WIP_ACCOUNT_ID        number(22,0) null,
 TAX_CLASS_ID          number(22,0) null,
 NORMALIZATION_ID      number(22,0) null,
 JURISDICTION_ID       number(22,0) null,
 USER_ID               varchar(18)  null,
 TIMESTAMP             date         null
);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint PK_REG_INCREMENTAL_FP_ADJ_TRA
       primary key (FORECAST_VERSION_ID, INCREMENTAL_ADJ_ID, LABEL_ID,
                    ITEM_ID, WORK_ORDER_ID, REVISION, BUDGET_VERSION_ID,
                    FCST_DEPR_VERSION_ID, VERSION_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA1
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA2
       foreign key (INCREMENTAL_ADJ_ID)
       references REG_INCREMENTAL_ADJUSTMENT (INCREMENTAL_ADJ_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA3
       foreign key (LABEL_ID, ITEM_ID)
       references INCREMENTAL_FP_ADJ_ITEM (LABEL_ID, ITEM_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA4
       foreign key (WORK_ORDER_ID, REVISION)
       references WORK_ORDER_APPROVAL (WORK_ORDER_ID, REVISION);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA5
       foreign key (BUDGET_VERSION_ID)
       references BUDGET_VERSION (BUDGET_VERSION_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA6
       foreign key (FCST_DEPR_VERSION_ID)
       references FCST_DEPR_VERSION (FCST_DEPR_VERSION_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA7
       foreign key (SET_OF_BOOKS_ID)
       references SET_OF_BOOKS (SET_OF_BOOKS_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA8
       foreign key (VERSION_ID)
       references VERSION (VERSION_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TRA9
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR10
       foreign key (REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR11
       foreign key (OVERRIDE_REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR12
       foreign key (FCST_DEPR_GROUP_ID)
       references FCST_DEPR_GROUP (FCST_DEPR_GROUP_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR13
       foreign key (EXPENDITURE_TYPE_ID)
       references EXPENDITURE_TYPE (EXPENDITURE_TYPE_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR14
       foreign key (EST_CHG_TYPE_ID)
       references ESTIMATE_CHARGE_TYPE (EST_CHG_TYPE_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR15
       foreign key (TAX_CLASS_ID)
       references TAX_CLASS (TAX_CLASS_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR16
       foreign key (NORMALIZATION_ID)
       references NORMALIZATION_SCHEMA (NORMALIZATION_ID);

alter table REG_INCREMENTAL_FP_ADJ_TRANS
   add constraint FK_REG_INCREMENTAL_FP_ADJ_TR17
       foreign key (JURISDICTION_ID)
       references JURISDICTION (JURISDICTION_ID);

--REG_FCST_INC_ADJ_LEDGER
create table REG_FCST_INC_ADJ_LEDGER
(
 FORECAST_VERSION_ID    number(22,0) not null,
 INCREMENTAL_ADJ_ID     number(22,0) not null,
 REG_COMPANY_ID         number(22,0) not null,
 REG_ACCT_ID            number(22,0) not null,
 GL_MONTH               number(6,0)  not null,
 ORIG_AMOUNT            number(22,2) not null,
 ANNUALIZED_ORIG_AMT    number(22,2) null,
 UPDATED_AMOUNT         number(22,2) not null,
 ANNUALIZED_UPDATED_AMT number(22,2) null,
 ADJ_AMOUNT             number(22,2) not null,
 ANNUALIZED_ADJ_AMT     number(22,2) null,
 REG_SOURCE_ID          number(22,0) not null,
 USER_ID                varchar(18)  null,
 TIMESTAMP              date         null
);

alter table REG_FCST_INC_ADJ_LEDGER
   add constraint PK_REG_FCST_INC_ADJ_LEDGER
       primary key (FORECAST_VERSION_ID, INCREMENTAL_ADJ_ID, REG_COMPANY_ID, REG_ACCT_ID, GL_MONTH)
       using index tablespace PWRPLANT_IDX;

alter table REG_FCST_INC_ADJ_LEDGER
   add constraint FK_REG_FCST_INC_ADJ_LEDGER1
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_FCST_INC_ADJ_LEDGER
   add constraint FK_REG_FCST_INC_ADJ_LEDGER2
       foreign key (INCREMENTAL_ADJ_ID)
       references REG_INCREMENTAL_ADJUSTMENT (INCREMENTAL_ADJ_ID);

alter table REG_FCST_INC_ADJ_LEDGER
   add constraint FK_REG_FCST_INC_ADJ_LEDGER3
       foreign key (REG_COMPANY_ID)
       references REG_COMPANY (REG_COMPANY_ID);

alter table REG_FCST_INC_ADJ_LEDGER
   add constraint FK_REG_FCST_INC_ADJ_LEDGER4
       foreign key (REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

alter table REG_FCST_INC_ADJ_LEDGER
   add constraint FK_REG_FCST_INC_ADJ_LEDGER5
       foreign key (REG_SOURCE_ID)
       references REG_SOURCE (REG_SOURCE_ID);

--Inserts
insert into REG_INCREMENTAL_LABEL_MAP (LABEL_ID, REG_FAMILY_ID) values (1, 3);
insert into REG_INCREMENTAL_LABEL_MAP (LABEL_ID, REG_FAMILY_ID) values (2, 3);
insert into REG_INCREMENTAL_LABEL_MAP (LABEL_ID, REG_FAMILY_ID) values (3, 1);
insert into REG_INCREMENTAL_LABEL_MAP (LABEL_ID, REG_FAMILY_ID) values (4, 1);
insert into REG_INCREMENTAL_LABEL_MAP (LABEL_ID, REG_FAMILY_ID) values (6, 5);
insert into REG_INCREMENTAL_LABEL_MAP (LABEL_ID, REG_FAMILY_ID) values (7, 5);

insert into REG_INCREMENTAL_ADJUST_TYPE
   (INCREMENTAL_ADJ_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (1, 'Project Revision', 'Funding Project + Revision');
insert into REG_INCREMENTAL_ADJUST_TYPE
   (INCREMENTAL_ADJ_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (2, 'Depr Rate Change', 'Depreciation Rate Change');
insert into REG_INCREMENTAL_ADJUST_TYPE
   (INCREMENTAL_ADJ_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (3, 'Depr SOB Substitution', 'Deprecation Set of Books Substitution');

update PPBASE_MENU_ITEMS set ITEM_ORDER = 2 where WORKSPACE_IDENTIFIER = 'uo_reg_inc_adj_ws';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_inc_create_ifa', 'IFA - Create Adjustment', 'uo_reg_inc_create_ifa', 'IFA - Create Adjustment');

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_inc_setup', 'IFA - Setup', 'uo_reg_inc_setup', 'IFA - Setup');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'IFA_CREATE', 3, 3, 'IFA - Create Adjustment', 'IFA - Create Adjustment', 'IFA_TOP', 'uo_reg_inc_create_ifa',
    1);

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER,
    ENABLE_YN)
values
   ('REG', 'IFA_SETUP', 3, 1, 'IFA - Setup', 'IFA - Setup', 'IFA_TOP', 'uo_reg_inc_setup', 1);

--Did you want comments? Cause we got Comments
comment on table REG_INCREMENTAL_LABEL_MAP is 'Relates Incremental Data Labels to Reg Families to facilitate translating Incremental Data to Regulatory Accounts';
comment on table REG_INCREMENTAL_ITEM_MAP is 'Relates Incremental Data Items to Regulatory Components to facilitat translating incremental data to Regulatory Accounts';
comment on table REG_INCREMENTAL_ADJUST_TYPE is 'Identifies adjustment type for Incremental Adjustment Data';
comment on table REG_INCREMENTAL_ADJUSTMENT is 'Stores the original and revised funding project and revision combinations witha a description of the adjustment.';
comment on table REG_INCREMENTAL_ADJ_VERSION is 'Relates Incremental Adjustments to Forecast and Historic Ledgers.';
comment on table REG_INCREMENTAL_FP_ADJ_TRANS is 'Stores incremental forecast data by label, item, funding project, and revision with the translation to regulatory company and account.';

--* REG_INCREMENTAL_LABEL_MAP
comment on column  REG_INCREMENTAL_LABEL_MAP.LABEL_ID is 'System assigned identifier for the incremental data label';
comment on column  REG_INCREMENTAL_LABEL_MAP.REG_FAMILY_ID is 'System assigned identifier for the regulatory family';

--* REG_INCREMENTAL_ITEM_MAP
comment on column  REG_INCREMENTAL_ITEM_MAP.LABEL_ID is 'System assigned identifier for the incremental data label';
comment on column  REG_INCREMENTAL_ITEM_MAP.ITEM_ID is 'System assigned identifier for the incremental data item';
comment on column  REG_INCREMENTAL_ITEM_MAP.STATUS is 'Indicates whether the label and item are used in the translate: 0 = Not Used, 1 = Used';
comment on column  REG_INCREMENTAL_ITEM_MAP.REG_FAMILY_ID is 'System assigned identifier for the regulatory family';
comment on column  REG_INCREMENTAL_ITEM_MAP.REG_CWIP_COMPONENT_ID is 'System assigned identifier for the Capital Budget Component';
comment on column  REG_INCREMENTAL_ITEM_MAP.REG_DEPR_COMPONENT_ID is 'System assigned identifier for the Depr Forecast Component';
comment on column  REG_INCREMENTAL_ITEM_MAP.REG_TAX_COMPONENT_ID is 'System assigned identifier for the PowerTax Component';

--* REG_INCREMENTAL_ADJUSTMENT_TYPE
comment on column  reg_incremental_adjust_type.INCREMENTAL_ADJ_TYPE_ID is 'System assigned identifier for the incremental adjustment type';
comment on column  reg_incremental_adjust_type.DESCRIPTION is 'Description of the incremental adjustment type';
comment on column  reg_incremental_adjust_type.LONG_DESCRIPTION is 'Long description of the incremental adjustment type';

--* REG_INCREMENTAL_ADJUSTMENT
comment on column  REG_INCREMENTAL_ADJUSTMENT.INCREMENTAL_ADJ_ID is 'System assigned identifier for the incremental adjustment';
comment on column  REG_INCREMENTAL_ADJUSTMENT.INCREMENTAL_ADJ_TYPE_ID is 'System assigned identifier for the incremental adjustment type';
comment on column  REG_INCREMENTAL_ADJUSTMENT.DESCRIPTION is 'Description of the incremental adjustment';
comment on column  REG_INCREMENTAL_ADJUSTMENT.LONG_DESCRIPTION is 'Long description of the incremental adjustment';
comment on column  REG_INCREMENTAL_ADJUSTMENT.ORIG_WORK_ORDER_ID is 'System assigned identifier of the funding project selected as the Original';
comment on column  REG_INCREMENTAL_ADJUSTMENT.ORIG_REVISION is 'System assigned identifier of the funding project revision selected as the Original';
comment on column  REG_INCREMENTAL_ADJUSTMENT.ORIG_BUDGET_VERSION_ID is 'System assigned identifier of the capital budget version used for the Original';
comment on column  REG_INCREMENTAL_ADJUSTMENT.ORIG_FCST_DEPR_VERSION_ID is 'System assigned identifier of the depreciation forecast version used for the Original';
comment on column  REG_INCREMENTAL_ADJUSTMENT.ORIG_SET_OF_BOOKS_ID is 'System assigned identifier of the Set of Books used for the Original';
comment on column  REG_INCREMENTAL_ADJUSTMENT.ORIG_VERSION_ID is 'System assigned identifier of the PowerTax case used for the Original';
comment on column  REG_INCREMENTAL_ADJUSTMENT.UPDATED_WORK_ORDER_ID is 'System assigned identifier of the funding project selected as Updated';
comment on column  REG_INCREMENTAL_ADJUSTMENT.UPDATED_REVISION is 'System assigned identifier of the funding project revision selected as the Updated';
comment on column  REG_INCREMENTAL_ADJUSTMENT.UPDATED_BUDGET_VERSION_ID is 'System assigned identifier of the capital budget version used for the Updated';
comment on column  REG_INCREMENTAL_ADJUSTMENT.UPDATED_FCST_DEPR_VERSION_ID is 'System assigned identifier of the depreciation forecast version used for the Updated';
comment on column  REG_INCREMENTAL_ADJUSTMENT.UPDATED_SET_OF_BOOKS_ID is 'System assigned identifier of the Set of Books used for the Updated';
comment on column  REG_INCREMENTAL_ADJUSTMENT.UPDATED_VERSION_ID is 'System assigned identifier of the PowerTax case used for the Updated';

--* REG_INCREMENTAL_ADJ_VERSION
comment on column  REG_INCREMENTAL_ADJ_VERSION.HISTORIC_VERSION_ID is 'System assigned identifier for the historic ledger, 0 is assigned when a Forecast ledger is mapped';
comment on column  REG_INCREMENTAL_ADJ_VERSION.FORECAST_VERSION_ID is 'System assigned identifier for the forecast ledger, 0 is assigned when a Historic ledger is mapped';
comment on column  REG_INCREMENTAL_ADJ_VERSION.INCREMENTAL_ADJ_ID is 'System assigned identifier for the incremental adjustment';
comment on column  REG_INCREMENTAL_ADJ_VERSION.STATUS_CODE_ID is 'Indicates the status of the adjustment for the forecast version: 0 = Inactive, 1 = Active';

--* REG_INCREMENTAL_FP_ADJ_TRANS
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.FORECAST_VERSION_ID is 'System assigned identifier for regulatory forecast ledger';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.INCREMENTAL_ADJ_ID is 'System assigned identifier for the incremental adjustment';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.LABEL_ID is 'System assigned identifier for the incremental data label';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.ITEM_ID is 'System assigned identifier for the incremental data item';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.WORK_ORDER_ID is 'System assigned identifier for the funding project';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.REVISION is 'System assigned identifier for the funding project revision';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.BUDGET_VERSION_ID is 'System assigned identifier for the capital budget version';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.FCST_DEPR_VERSION_ID is 'System assigned identifier for the depreciation forecast version';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.VERSION_ID is 'System assigned identifier for the PowerTax case';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.REG_COMPANY_ID is 'System assigned identifier for the Regulatory Company';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.REG_ACCT_ID is 'System assigned identifier for the Regulatory Account assigned by the system';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.OVERRIDE_TRANSLATE is '1 = User selects account to override system translated account, 0 = use system translated account';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.OVERRIDE_REG_ACCT_ID is 'System assigned identifier for the Regulatory Account selected by the user';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.FCST_DEPR_GROUP_ID is 'System assigned identifier for the forecast depreciation group for labels/items that involve depreciation';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.SET_OF_BOOKS_ID is 'System assigned identifier for the Set of Books for labels/items that involve depreciation';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.EXPENDITURE_TYPE_ID is 'System assigned identifier for the expenditure type for the funding project';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.EST_CHG_TYPE_ID is 'System assigned identifier for the estimate charge type for the funding project';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.WIP_COMPUTATION_ID is 'System assigned identifier for the WIP computation for labels/items that involve WIP Computations';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.WIP_ACCOUNT_ID is 'System assigned identifier for the GL account other than CWIP for labels/items that involve WIP Computations';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.TAX_CLASS_ID is 'System assigned identifier for the tax class for labels/items that involve deferred taxes';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.NORMALIZATION_ID is 'System assigned identifier for the normalization schema for labels/items that involve deferred taxes';
comment on column  REG_INCREMENTAL_FP_ADJ_TRANS.JURISDICTION_ID is 'System assigned identifier for the jurisdiction for labels/items that involve deferred taxes';

--* REG_FCST_INC_ADJ_LEDGER
comment on column  REG_FCST_INC_ADJ_LEDGER.FORECAST_VERSION_ID is 'System assigned identifier for the forecast ledger';
comment on column  REG_FCST_INC_ADJ_LEDGER.INCREMENTAL_ADJ_ID is 'System assigned identifier for the incremental adjustment';
comment on column  REG_FCST_INC_ADJ_LEDGER.REG_COMPANY_ID is 'System assigned identifier for the regulatory company';
comment on column  REG_FCST_INC_ADJ_LEDGER.REG_ACCT_ID is 'System assigned identifier for the regulatory account';
comment on column  REG_FCST_INC_ADJ_LEDGER.GL_MONTH is 'Month in YYYYMM format';
comment on column  REG_FCST_INC_ADJ_LEDGER.ORIG_AMOUNT is 'The original amount for the incremental adjustment';
comment on column  REG_FCST_INC_ADJ_LEDGER.ANNUALIZED_ORIG_AMT is '';
comment on column  REG_FCST_INC_ADJ_LEDGER.UPDATED_AMOUNT is 'The updated amount for the incremental adjustment';
comment on column  REG_FCST_INC_ADJ_LEDGER.ANNUALIZED_UPDATED_AMT is '';
comment on column  REG_FCST_INC_ADJ_LEDGER.ADJ_AMOUNT is 'The incremental adjustment amount = Updated Amount - Original Amount';
comment on column  REG_FCST_INC_ADJ_LEDGER.ANNUALIZED_ADJ_AMT is '';
comment on column  REG_FCST_INC_ADJ_LEDGER.REG_SOURCE_ID is 'System assigned identifier for the regulatory source';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1327, 0, 10, 4, 2, 7, 38683, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038683_reg_inc_fp_adjustments.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;