/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051542_lessee_01_ls_fasb_cap_type_updates_dml.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/15/2018 David Conway     Off Balance Sheet Expense by Set of Books
|| 2017.4.0.0  06/28/2018 David Conway     Corrected update logic
||============================================================================
*/
DECLARE
    config_min  pp_system_control_company.control_value%type;
    config_max  pp_system_control_company.control_value%type;
    
BEGIN

    --change Type A and Type B to Finance and Operating, respectively
    update ls_fasb_cap_type
    set description =
        case fasb_cap_type_id
            when 1 then 'Finance'
            when 2 then 'Operating'
            else description
        end
    where fasb_cap_type_id between 1 and 2;
  
    --add new FASB Cap Type records 4 and 5 (3 will be added in a separate ticket)
    insert into ls_fasb_cap_type ( fasb_cap_type_id, description )
    select 4, 'Off Balance Sheet Straight Line' from dual
    where not exists(select 1 from ls_fasb_cap_type where fasb_cap_type_id = 4);
  
    insert into ls_fasb_cap_type ( fasb_cap_type_id, description )
    select 5, 'Off Balance Sheet As Incurred' from dual
    where not exists(select 1 from ls_fasb_cap_type where fasb_cap_type_id = 5);

    --see if configuration differs across companies for 'Lease Operating Expense as Incurred'
    SELECT MIN(lower(control_value)), MAX(lower(control_value))
    INTO config_min, config_max
    FROM pp_system_control_company
    WHERE control_name = 'Lease Operating Expense as Incurred';

    IF config_min in ('yes','no') and config_max = config_min THEN
      --only one config, or all configs are same across companies, proceed

      --update the set of books mappings
      update ls_fasb_cap_type_sob_map
      set fasb_cap_type_id = CASE config_min WHEN 'yes' THEN 5 ELSE 4 END
      where lease_cap_type_id in (select ls_lease_cap_type_id from ls_lease_cap_type where is_om = 1);

      --delete the now unused system control
      delete from pp_system_control_company
      where control_name = 'Lease Operating Expense as Incurred';

    ELSE

      DBMS_OUTPUT.PUT_LINE('*******************************************************************');
      DBMS_OUTPUT.PUT_LINE('*** "Lease Operating Expense as Incurred" varies between companies.');
      DBMS_OUTPUT.PUT_LINE('*** Lessee Set of Books FASB Cap Types must be manually updated');
      DBMS_OUTPUT.PUT_LINE('*** before removing this system control.');
      DBMS_OUTPUT.PUT_LINE('***************************************************************');

    END IF;
    
EXCEPTION
  WHEN Others THEN
      DBMS_OUTPUT.PUT_LINE('SQLCODE = ' || sqlcode || ' SQLERRM = ' || sqlerrm);

END;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7462, 0, 2017, 4, 0, 0, 51542, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051542_lessee_01_ls_fasb_cap_type_updates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;