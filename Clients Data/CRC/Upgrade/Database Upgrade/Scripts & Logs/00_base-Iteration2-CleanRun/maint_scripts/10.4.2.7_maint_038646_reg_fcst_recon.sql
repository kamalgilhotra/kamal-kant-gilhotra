/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038646_reg_fcst_recon.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------  ----------------------------------------------
|| 10.4.2.7 06/26/2014 Ryan Oliveria        Reg Forecast Recon Maintenance
||========================================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_fcst_recon_setup', 'Fcst Recon Maintenance', 'uo_reg_fcst_recon_setup',
    'Forecast Reconciliation Maintenance');

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'uo_reg_fcst_recon_setup', LABEL = 'Fcst Recon Maintenance'
 where MODULE = 'REG'
   and MENU_IDENTIFIER = 'FORE_RECON_MAINT';

-- Recon Group
create table REG_FCST_RECON_GROUP
(
 RECON_GROUP_ID number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(50) not null
);

alter table REG_FCST_RECON_GROUP
   add constraint PK_REG_FCST_RECON_GROUP
       primary key (RECON_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

-- Recon Items
create table REG_FCST_RECON_ITEMS
(
 RECON_ID       number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(35) not null,
 STATUS         number(22,0) not null,
 RECON_GROUP_ID number(22,0)
);

alter table REG_FCST_RECON_ITEMS
   add constraint PK_REG_FCST_RECON_ITEMS
       primary key (RECON_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_FCST_RECON_ITEMS
   add constraint FK_REG_FCST_RECON_ITEMS_GROUP
       foreign key (RECON_GROUP_ID)
       references REG_FCST_RECON_GROUP (RECON_GROUP_ID);

-- Reg Account Mapping
create table REG_FCST_RECON_REGACCT_MAP
(
 FORECAST_VERSION_ID number(22,0) not null,
 RECON_ID            number(22,0) not null,
 REG_ACCT_ID         number(22,0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table REG_FCST_RECON_REGACCT_MAP
   add constraint PK_REG_FCST_RECON_REGACCT_MAP
       primary key (FORECAST_VERSION_ID, RECON_ID, REG_ACCT_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_FCST_RECON_REGACCT_MAP
   add constraint FK_REG_FCST_RECON_MAP_VERSION
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_FCST_RECON_REGACCT_MAP
   add constraint FK_REG_FCST_RECON_MAP_ITEM
       foreign key (RECON_ID)
       references REG_FCST_RECON_ITEMS (RECON_ID);

alter table REG_FCST_RECON_REGACCT_MAP
   add constraint FK_REG_FCST_RECON_MAP_ACCT
       foreign key (REG_ACCT_ID)
       references REG_ACCT_MASTER (REG_ACCT_ID);

-- Recon Trial Balance Setup
create table REG_FCST_RECON_TB_SETUP
(
 FORECAST_VERSION_ID  number(22,0) not null,
 RECON_SOURCE         number(22,0) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 CR_BUDGET_VERSION_ID number(22,0),
 EXT_SOURCE_ID        number(22,0),
 EXT_SOURCE_VERSION   varchar2(35)
);

alter table REG_FCST_RECON_TB_SETUP
   add constraint PK_REG_FCST_RECON_TB_SETUP
       primary key (FORECAST_VERSION_ID, RECON_SOURCE)
       using index tablespace PWRPLANT_IDX;

alter table REG_FCST_RECON_TB_SETUP
   add constraint FK_REG_FCST_RECON_TB_FCST_VERS
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_FCST_RECON_TB_SETUP
   add constraint FK_REG_FCST_RECON_TB_VERSION
       foreign key (CR_BUDGET_VERSION_ID)
       references CR_BUDGET_VERSION (CR_BUDGET_VERSION_ID);

alter table REG_FCST_RECON_TB_SETUP
   add constraint FK_REG_FCST_RECON_TB_SOURCE
       foreign key (EXT_SOURCE_ID)
       references REG_EXT_SOURCE (EXT_SOURCE_ID);

-- Recon TB Combos
create table REG_FCST_RECON_TB_COMBOS
(
 TB_COMBO_ID   number(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18),
 ACCOUNT_VALUE varchar2(100) not null,
 COMPANY_VALUE varchar2(100),
 PULL_TYPE_ID  number(22,0)
);

alter table REG_FCST_RECON_TB_COMBOS
   add constraint PK_REG_FCST_RECON_TB_COMBOS
       primary key (TB_COMBO_ID)
       using index tablespace PWRPLANT_IDX;

--Hold off on this for now
--alter table REG_FCST_RECON_TB_COMBOS
-- add constraint FK_REG_FCST_TB_COMBOS_PULL
-- foreign key (PULL_TYPE_ID)
--    references REG_CR_PULL_TYPE (PULL_TYPE_ID);

-- Recon TB Map
create table REG_FCST_RECON_TB_MAP
(
 FORECAST_VERSION_ID number(22,0) not null,
 RECON_ID            number(22,0) not null,
 TB_COMBO_ID         number(22,0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

alter table REG_FCST_RECON_TB_MAP
   add constraint PK_REG_FCST_RECON_TB_MAP
       primary key (FORECAST_VERSION_ID, RECON_ID, TB_COMBO_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_FCST_RECON_TB_MAP
   add constraint FK_REG_FCST_TB_MAP_VERSION
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);

alter table REG_FCST_RECON_TB_MAP
   add constraint FK_REG_FCST_TB_MAP_ITEM
       foreign key (RECON_ID)
       references REG_FCST_RECON_ITEMS (RECON_ID);

alter table REG_FCST_RECON_TB_MAP
   add constraint FK_REG_FCST_TB_MAP_ACCT
       foreign key (TB_COMBO_ID)
       references REG_FCST_RECON_TB_COMBOS (TB_COMBO_ID);

-- Config
insert into REG_FCST_RECON_ITEMS (RECON_ID, DESCRIPTION, STATUS) values (0, 'Not Used', 1);
insert into REG_FCST_RECON_ITEMS (RECON_ID, DESCRIPTION, STATUS) values (-1, 'Not Mapped', 1);

-- Comments
comment on table REG_FCST_RECON_ITEMS is 'The Reg Fcst Recon Items table stores unique reconciliation items used to identify discrepancies between the forecast ledger and the forecast trial balance.';
comment on table REG_FCST_RECON_GROUP is 'The Reg Fcst Recon Group table stores unique reconciliation groups.';
comment on table REG_FCST_RECON_REGACCT_MAP is 'The Reg Fcst Recon Reg Acct Map table stores the mapping of reg accounts to recon items by forecast ledger.';
comment on table REG_FCST_RECON_TB_SETUP is 'The Reg Fcst Recon TB Setup table identifies the source of data that is used as the Forecast Trial Balance; the options are the Departmental Budgeting Module or a Regulatory Forecast External Source.';
comment on table REG_FCST_RECON_TB_COMBOS is 'The Reg Fcst Recon TB Combos table stores the unique Account and Company values and identifies whether they are monthly balance or activity amounts.';
comment on table REG_FCST_RECON_TB_MAP is 'The Reg Fcst Recon TB Map table stores the mapping of Trial Balance Combos to Recon Items by Forecast Version used for reconciling Reg Accounts to the Forecast Trial Balance.';

comment on column REG_FCST_RECON_ITEMS.RECON_ID is 'System assigned identifier for the reconciliation item. Two special values are used: 0 = Not Used, -1 = Not Mapped.';
comment on column REG_FCST_RECON_ITEMS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_FCST_RECON_ITEMS.USER_ID is 'Standard system-assigned user id used for audit purposes';
comment on column REG_FCST_RECON_ITEMS.DESCRIPTION is 'Description of the reconciliation item.';
comment on column REG_FCST_RECON_ITEMS.STATUS is '1 = Active; 0 = Inactive';
comment on column REG_FCST_RECON_ITEMS.RECON_GROUP_ID is 'System assigned identifier for the reconciliation group.';

comment on column REG_FCST_RECON_GROUP.RECON_GROUP_ID is 'System assigned identifier for the reconciliation group';
comment on column REG_FCST_RECON_GROUP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_FCST_RECON_GROUP.USER_ID is 'Standard system-assigned user id used for audit purposes';
comment on column REG_FCST_RECON_GROUP.DESCRIPTION is 'Description of the reconciliation group';

comment on column REG_FCST_RECON_REGACCT_MAP.FORECAST_VERSION_ID is 'System assigned identifier for the forecast ledger';
comment on column REG_FCST_RECON_REGACCT_MAP.RECON_ID is 'System assigned identifier for the reconciliation item';
comment on column REG_FCST_RECON_REGACCT_MAP.REG_ACCT_ID is 'System assigned identifier for the reg account';
comment on column REG_FCST_RECON_REGACCT_MAP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_FCST_RECON_REGACCT_MAP.USER_ID is 'Standard system-assigned user id used for audit purposes';

comment on column REG_FCST_RECON_TB_SETUP.FORECAST_VERSION_ID is 'System assigned identifier for the forecast ledger (version)';
comment on column REG_FCST_RECON_TB_SETUP.RECON_SOURCE is 'Indicates which source of data should be used for the Forecast Reconciliation Trial Balance; 0 = Dept Budgeting; 1 = External Source';
comment on column REG_FCST_RECON_TB_SETUP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_FCST_RECON_TB_SETUP.USER_ID is 'Standard system-assigned user id used for audit purposes';
comment on column REG_FCST_RECON_TB_SETUP.CR_BUDGET_VERSION_ID is 'System assigned identifier for the cr budget version';
comment on column REG_FCST_RECON_TB_SETUP.EXT_SOURCE_ID is 'System assigned identifier for the external source';
comment on column REG_FCST_RECON_TB_SETUP.EXT_SOURCE_VERSION is 'Specifies the version value to use as the Forecast Reconciliation Trial Balance source';

comment on column REG_FCST_RECON_TB_COMBOS.TB_COMBO_ID is 'System assigned identifier of the Trial Balance combo';
comment on column REG_FCST_RECON_TB_COMBOS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_FCST_RECON_TB_COMBOS.USER_ID is 'Standard system-assigned user id used for audit purposes';
comment on column REG_FCST_RECON_TB_COMBOS.ACCOUNT_VALUE is 'Identifies the account value';
comment on column REG_FCST_RECON_TB_COMBOS.COMPANY_VALUE is 'Identifies the company value';
comment on column REG_FCST_RECON_TB_COMBOS.PULL_TYPE_ID is 'Identifies whether the amount is an ''Activity'' or ''Balance'' amount';

comment on column REG_FCST_RECON_TB_MAP.FORECAST_VERSION_ID is 'System assigned identifier for the forecast ledger';
comment on column REG_FCST_RECON_TB_MAP.RECON_ID is 'System assigned identifier for the reconciliation item';
comment on column REG_FCST_RECON_TB_MAP.TB_COMBO_ID is 'System assigned identifier for the trial balance combo item';
comment on column REG_FCST_RECON_TB_MAP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_FCST_RECON_TB_MAP.USER_ID is 'Standard system-assigned user id used for audit purposes';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1172, 0, 10, 4, 2, 7, 38646, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_038646_reg_fcst_recon.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;