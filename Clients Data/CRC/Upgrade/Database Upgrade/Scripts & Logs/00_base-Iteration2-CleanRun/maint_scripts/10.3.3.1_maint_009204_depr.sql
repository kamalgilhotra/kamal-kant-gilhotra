/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009204_depr.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.1   01/12/2012 Aaron Smith    Point Release
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'CPR DEPR: Allow Exp in Month Added',
          'Yes',
          'dw_yes_no;1',
          '"Yes" means expense will be allowed for all mid-period conventions in the month an asset is added if eng in-service date is a prior month. "No" means expense will never be allowed for groups with mid-period convention = 0 (begin balance) even if eng in-service date is a prior month.  The Default is "Yes" ',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (79, 0, 10, 3, 3, 1, 9204, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.1_maint_009204_depr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
