/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_033057_lease_unsend.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/08/2013 Ryan Oliveria  Option to Unsend ILR/MLA for Approval
||============================================================================
*/

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
   select max(ID) + 1, 'LESSEE', 'unsend_approval', 'Unsend Approval', 6, 'ue_unsendApproval'
     from PPBASE_ACTIONS_WINDOWS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (663, 0, 10, 4, 1, 1, 33057, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033057_lease_unsend.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
