/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_053082_lessor_01_trueup_rent_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.2.0.0 02/25/2019  Sarah Byers      Fix deferred/accrued rent on approved ILR schedules
||============================================================================
*/

set serveroutput on;

declare
  l_payment_terms lsr_ilr_op_sch_pay_term_tab;
  l_ilr_tab t_lsr_ilr_id_revision_tab;
begin 
  DBMS_OUTPUT.ENABLE(BUFFER_SIZE => null);
  
  -- Loop over the companies in lsr_process_control
  for COMP in (
                select company_id, min(gl_posting_mo_yr) min_open_month
                  from lsr_process_control
                 where open_next is null
                 group by company_id
              )
  loop
      DBMS_OUTPUT.PUT_LINE(' Processing Company ID: '||to_char(COMP.COMPANY_ID)||', Min Open Month:'||to_char(COMP.MIN_OPEN_MONTH)); 
      -- Loop over the ILRs that have approval status 2, 3, or 6
      for ILR in (
                    select i.ilr_id, a.revision, i.lease_id, i.company_id
                      from lsr_ilr i
                      join lsr_ilr_approval a on a.ilr_id = i.ilr_id
                     where a.approval_status_id in (2, 3, 6)
                       and i.company_id = COMP.COMPANY_ID
                       and (i.ilr_id, a.revision) in (
                             select ilr_id, revision from lsr_ilr_schedule
                              group by ilr_id, revision
                             having sum(sign(beg_deferred_rent)) < 0 or (sum(deferred_rent) = 0 and sum(sign(end_accrued_rent)) > 0))
                     order by i.ilr_id, a.revision
                 )
      loop
        DBMS_OUTPUT.PUT_LINE(' Processing ILR ID: '||to_char(ILR.ILR_ID)||', Revision:'||to_char(ILR.REVISION)); 
        -- Populate the ILR/Revision table
        l_ilr_tab := t_lsr_ilr_id_revision_tab(t_lsr_ilr_id_revision(ILR.ILR_ID, ILR.REVISION));
        
        -- Get the payment terms
        l_payment_terms := pkg_lessor_var_payments.f_get_initial_pay_terms(ILR.ILR_ID, ILR.REVISION);
        
        -- Load the prior schedule temp tables
        delete from lsr_ilr_prior_schedule_temp;
        delete from lsr_ilr_prior_amounts_temp;
        
        -- Multiply the existing deferred rent amounts by -1
        insert into lsr_ilr_prior_schedule_temp (ilr_id, revision, set_of_books_id, month,
          interest_income_received, interest_income_accrued, interest_rental_recvd_spread,
          beg_deferred_rev, deferred_rev_activity, end_deferred_rev,
          beg_receivable, end_receivable, beg_lt_receivable, end_lt_receivable, initial_direct_cost,
          executory_accrual1, executory_accrual2, executory_accrual3, executory_accrual4, executory_accrual5,
          executory_accrual6, executory_accrual7, executory_accrual8, executory_accrual9, executory_accrual10,
          executory_paid1, executory_paid2, executory_paid3, executory_paid4, executory_paid5,
          executory_paid6, executory_paid7, executory_paid8, executory_paid9, executory_paid10,
          contingent_accrual1, contingent_accrual2, contingent_accrual3, contingent_accrual4, contingent_accrual5,
          contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10,
          contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4, contingent_paid5,
          contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10,
          beg_deferred_rent, deferred_rent, end_deferred_rent,
          beg_accrued_rent, accrued_rent, end_accrued_rent,
          principal_received, principal_accrued,
          beg_unguaranteed_residual, interest_unguaranteed_residual, ending_unguaranteed_residual,
          beg_net_investment, interest_net_investment, ending_net_investment,
          begin_deferred_profit, recognized_profit, end_deferred_profit,
          begin_lt_deferred_profit, end_lt_deferred_profit, lt_deferred_profit_remeasure
          )
        select sch.ilr_id, sch.revision, sch.set_of_books_id, sch.month,
               interest_income_received, interest_income_accrued, interest_rental_recvd_spread,
               beg_deferred_rev, deferred_rev_activity, end_deferred_rev,
               beg_receivable, end_receivable, beg_lt_receivable, end_lt_receivable, initial_direct_cost,
               executory_accrual1, executory_accrual2, executory_accrual3, executory_accrual4, executory_accrual5,
               executory_accrual6, executory_accrual7, executory_accrual8, executory_accrual9, executory_accrual10,
               executory_paid1, executory_paid2, executory_paid3, executory_paid4, executory_paid5,
               executory_paid6, executory_paid7, executory_paid8, executory_paid9, executory_paid10,
               contingent_accrual1, contingent_accrual2, contingent_accrual3, contingent_accrual4, contingent_accrual5,
               contingent_accrual6, contingent_accrual7, contingent_accrual8, contingent_accrual9, contingent_accrual10,
               contingent_paid1, contingent_paid2, contingent_paid3, contingent_paid4, contingent_paid5,
               contingent_paid6, contingent_paid7, contingent_paid8, contingent_paid9, contingent_paid10,
               -1 * beg_deferred_rent, 
               -1 * deferred_rent, 
               -1 * end_deferred_rent,
               beg_accrued_rent, accrued_rent, end_accrued_rent,
               principal_received, principal_accrued,
               beg_unguaranteed_residual, interest_unguaranteed_residual, ending_unguaranteed_residual,
               beg_net_investment, interest_net_investment, ending_net_investment,
               begin_deferred_profit, recognized_profit, end_deferred_profit,
               begin_lt_deferred_profit, end_lt_deferred_profit, lt_deferred_profit_remeasure
          from lsr_ilr_schedule sch
          join table(l_ilr_tab) ids on ids.ilr_id = sch.ilr_id
                                   and ids.revision = sch.revision
          join lsr_ilr ilr on ilr.ilr_id = ids.ilr_id 
                          and ilr.revision = ids.revision
          join lsr_ilr_options options on ids.ilr_id = options.ilr_id 
                                      and ids.revision = options.revision
          left outer join lsr_ilr_schedule_sales_direct st_sch on sch.ilr_id = st_sch.ilr_id 
                                                              and sch.revision = st_sch.revision
                                                              and sch.month = st_sch.month 
                                                              and sch.set_of_books_id = st_sch.set_of_books_id
          left outer join lsr_ilr_schedule_direct_fin df_sch on sch.ilr_id = df_sch.ilr_id 
                                                            and sch.revision = df_sch.revision
                                                            and sch.month = df_sch.month 
                                                            and sch.set_of_books_id = df_sch.set_of_books_id
         where sch.month < COMP.MIN_OPEN_MONTH;

        insert into lsr_ilr_prior_amounts_temp(ilr_id, revision, set_of_books_id,
          npv_lease_payments, npv_guaranteed_residual, npv_unguaranteed_residual, selling_profit_loss,
          beginning_lease_receivable, beginning_net_investment, cost_of_goods_sold, schedule_rates)
        select amt.ilr_id, amt.revision, amt.set_of_books_id,
               npv_lease_payments, npv_guaranteed_residual, npv_unguaranteed_residual, selling_profit_loss,
               beginning_lease_receivable, beginning_net_investment, cost_of_goods_sold, schedule_rates
          from lsr_ilr_amounts amt
          join table(l_ilr_tab) ids on ids.ilr_id = amt.ilr_id
                                   and ids.revision = amt.revision
          join lsr_ilr ilr on ilr.ilr_id = ids.ilr_id 
                          and ilr.revision = ids.revision
          join lsr_ilr_options options on ids.ilr_id = options.ilr_id 
                                      and ids.revision = options.revision;
                                   
        -- Loop over each operating set of books
        for SOB in (
                     select f.set_of_books_id
                       from lsr_fasb_type_sob f
                       join lsr_ilr_options o on o.lease_cap_type_id = f.cap_type_id
                      where o.ilr_id = ILR.ILR_ID
                        and o.revision = ILR.REVISION
                   )
        loop
          DBMS_OUTPUT.PUT_LINE(' Processing Set of Books ID: '||to_char(SOB.SET_OF_BOOKS_ID)); 
          -- Calculate the new interest income accrued and deferred/accrued rent amounts
          merge into lsr_ilr_schedule s
          using (
                  WITH payments
                       AS (SELECT payment_month_frequency,
                                  payment_group, --Group months based on date payment takes place
                                  month,
                                  number_of_terms,
                                  is_prepay,
                                  iter,
                                  payment_amount
                             FROM TABLE(PKG_LESSOR_SCHEDULE.f_get_payments_from_info(PKG_LESSOR_SCHEDULE.f_get_payment_info_from_terms(pkg_lessor_var_payments.f_get_initial_pay_terms(ILR.ILR_ID, ILR.REVISION))))),
                       --Build values of income section of schedule
                       income_info
                       AS (SELECT P.MONTH,
                                  P.payment_month_frequency,
                                  P.number_of_terms,
                                  p.is_prepay,
                                  P.iter,
                                  P.payment_amount AS interest_income_received,
                                  sum(p.payment_amount) over (partition by null) as initial_receivable,
                                  --Add all amounts and divide by number of months to get the accrued income
                                  CASE P.is_prepay
                                    WHEN 0 THEN LAST_VALUE(P.payment_amount) OVER (PARTITION BY P.payment_group ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                                    WHEN 1 THEN FIRST_VALUE(P.payment_amount) OVER (PARTITION BY P.payment_group ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                                  END AS group_fixed_payment
                             FROM payments p),
                       prior_schedule
                       AS (SELECT prior_sch.ilr_id,
                                  prior_sch.revision,
                                  prior_sch.set_of_books_id,
                                  month,
                                  interest_income_received,
                                  interest_income_accrued,
                                  interest_rental_recvd_spread,
                                  beg_deferred_rent,
                                  deferred_rent,
                                  end_deferred_rent,
                                  beg_accrued_rent,
                                  accrued_rent,
                                  end_accrued_rent
                             FROM lsr_ilr_prior_schedule_temp prior_sch
                            inner join lsr_ilr_options options
                                    on prior_sch.ilr_id = options.ilr_id and prior_sch.revision = options.revision
                            inner join (select cap_type_id, fasb_cap_type_id,
                                               set_of_books_id
                                          from lsr_fasb_type_sob
                                         where fasb_cap_type_id = 1
                                           and set_of_books_id = SOB.SET_OF_BOOKS_ID
                                       ) fasb
                                    on options.lease_cap_type_id = fasb.cap_type_id and prior_sch.set_of_books_id = fasb.set_of_books_id
                            WHERE prior_sch.ilr_id = ILR.ILR_ID
                              and prior_sch.set_of_books_id = SOB.SET_OF_BOOKS_ID),
                       trueup_amounts
                       AS (select --need to return values even when there is nothing in the prior_schedule
                                  sum(end_deferred_rent) new_beg_def_rent,
                                  sum(end_accrued_rent) new_beg_acc_rent
                             from (select distinct initial_receivable from income_info) a,
                                  (select end_deferred_rent, end_accrued_rent
                                     from prior_schedule
                                    where month = (select max(month) from prior_schedule
                                                    where month < COMP.MIN_OPEN_MONTH)
                                  )
                           ),
                       prepay_pending_income
                       AS (select --want to return a 0 if this returns a null value so the accrual can still calculate
                                  coalesce((sum(prior_schedule.interest_income_received) - sum(prior_schedule.interest_rental_recvd_spread)),0) prepay_pending
                             from prior_schedule
                             join (select max(month) month from prior_schedule
                                      where interest_income_received <> 0
                                        and month < COMP.MIN_OPEN_MONTH
                                  ) b
                               on prior_schedule.month >= b.month
                            where prior_schedule.set_of_books_id = SOB.SET_OF_BOOKS_ID
                              and prior_schedule.month < COMP.MIN_OPEN_MONTH
                          ),
                       arrears_received_income
                       AS (select --want to return a 0 if this returns a null value so the accrual can still calculate
                                  coalesce(sum(prior_schedule.interest_rental_recvd_spread),0) arrears_received
                             from prior_schedule
                             join (select max(month) month from prior_schedule
                                    where interest_income_received <> 0
                                      and month < COMP.MIN_OPEN_MONTH
                                  ) b
                               on prior_schedule.month > b.month
                            where set_of_books_id = SOB.SET_OF_BOOKS_ID
                              and prior_schedule.month < COMP.MIN_OPEN_MONTH
                          ),
                       trueup_accrual
                       AS (select initial_receivable + new_beg_def_rent - new_beg_acc_rent trueup_accrual,
                                    decode(is_prepay, 1, (initial_receivable + prepay_pending),
                                                      0, (initial_receivable - arrears_received),
                                                      0) trueup_income_spread
                               from (select distinct initial_receivable, is_prepay from income_info) a
                              cross join trueup_amounts
                              cross join arrears_received_income
                              cross join prepay_pending_income
                          ),
                       prior_spread_dates
                       AS (select decode(is_prepay, 1, (select max(month) from prior_schedule where interest_income_received = 0),
                                                    0, (select max(month) from prior_schedule),
                                                    NULL) end_prior_spread,
                                  decode(is_prepay, 1, (select max(month) month from prior_schedule
                                                         where interest_income_received <> 0
                                                           and month < COMP.MIN_OPEN_MONTH
                                                       ),
                                                    0, (select min(month) from payments where payment_amount <> 0)
                                        ) last_payment_month
                             from (select distinct is_prepay from income_info)
                          ),
                      sched 
                      AS (SELECT MONTH,
                                 interest_income_received,
                                 initial_receivable,
                                 interest_income_accrued,
                                 beg_rent_balance,
                                 rent_activity,
                                 end_rent_balance,
                                 begin_deferred_rent,
                                 deferred_rent,
                                 end_deferred_rent,
                                 begin_accrued_rent,
                                 accrued_rent,
                                 end_accrued_rent,
                                 prior_beg_def_rent,
                                 prior_def_rent,
                                 prior_end_def_rent,
                                 prior_beg_acc_rent,
                                 prior_acc_rent,
                                 prior_end_acc_rent
                            FROM income_info ii
                           CROSS JOIN trueup_amounts amt
                           CROSS JOIN trueup_accrual acc
                           CROSS JOIN prior_spread_dates d
                            left JOIN prior_schedule ps on ps.month = ii.month
                               MODEL
                               DIMENSION BY (row_number() OVER (ORDER BY ii.MONTH) AS month_num)
                               MEASURES (ii.MONTH as MONTH,
                                         COMP.MIN_OPEN_MONTH trueup_month,
                                         trueup_accrual,
                                         new_beg_def_rent,
                                         new_beg_acc_rent,
                                         end_prior_spread,
                                         trueup_income_spread,
                                         payment_month_frequency,
                                         ii.interest_income_received,
                                         group_fixed_payment,
                                         initial_receivable,
                                         0 AS interest_income_accrued,
                                         0 as interest_income_spread,
                                         0 AS begin_deferred_rent,
                                         0 AS deferred_rent,
                                         0 AS end_deferred_rent,
                                         0 AS begin_accrued_rent,
                                         0 AS accrued_rent,
                                         0 AS end_accrued_rent,
                                         0 AS rent_activity,
                                         0 AS beg_rent_balance,
                                         0 AS end_rent_balance,
                                         ps.beg_deferred_rent prior_beg_def_rent,
                                         ps.deferred_rent prior_def_rent,
                                         ps.end_deferred_rent prior_end_def_rent,
                                         ps.beg_accrued_rent prior_beg_acc_rent,
                                         ps.accrued_rent prior_acc_rent,
                                         ps.end_accrued_rent prior_end_acc_rent,
                                         count(1) over (partition by null) as cnt,
                                         last_value(ii.MONTH) over() last_month)
                               RULES AUTOMATIC ORDER ( 
                                 -- INITIALIZE BEGINNING BALANCES AND REMEASUREMENT AMOUNTS
                                 begin_deferred_rent[1] = 0,
                                 begin_accrued_rent[1] = 0,

                                 -- Initialize the Rent Balance used for calculating deferred/accrued rent amounts
                                 beg_rent_balance[1] = 0,
                                           
                                 -- INTEREST INCOME ACCRUED                                
                                 interest_income_accrued[month_num] = CASE WHEN cv(month_num) = cnt[cv()] THEN 
                                                                        initial_receivable[cv()] - COALESCE(SUM(interest_income_accrued)[month_num BETWEEN 1 AND cv() - 1], 0)
                                                                      ELSE 
                                                                        round(initial_receivable[cv()] / cnt[cv()], 2)
                                                                      END,
                                           
                                 -- RENT ACTIVITY = INCOME RECEIVED - INCOME ACCRUED
                                 -- Used for deferred and accrued rent based on the sign of the amount
                                 beg_rent_balance[month_num > 1] ORDER BY month_num = end_rent_balance[cv() - 1],
                                 rent_activity[month_num] = interest_income_received[cv()] - interest_income_accrued[cv()],
                                 end_rent_balance[month_num] = beg_rent_balance[cv()] + rent_activity[cv()],
                                           
                                 -- DEFERRED RENT
                                 -- BEG_DEFERRED_RENT = prior month END_DEFERRED_RENT
                                 -- END_DEFERRED_RENT = END_RENT_BALANCE when END_RENT_BALANCE > 0
                                 -- DEFERRED_RENT = END_DEFERRED_RENT - BEG_DEFERRED_RENT
                                 begin_deferred_rent[month_num > 1] ORDER BY month_num = end_deferred_rent[cv() - 1],
                                 end_deferred_rent[month_num] = case when month[cv()] < trueup_month[cv()] then
                                                                  prior_end_def_rent[cv()]
                                                                else
                                                                  CASE WHEN end_rent_balance[cv()] >= 0 THEN 
                                                                    end_rent_balance[cv()]
                                                                  ELSE 
                                                                    0
                                                                  END
                                                                end,
                                 deferred_rent[month_num] = end_deferred_rent[cv()] - begin_deferred_rent[cv()],
                                           
                                 -- ACCRUED RENT 
                                 -- BEG_ACCRUED_RENT = prior month END_ACCRUED_RENT
                                 -- END_ACCRUED_RENT = -1 * END_RENT_BALANCE when END_RENT_BALANCE < 0
                                 -- ACCRUED_RENT = END_ACCRUED_RENT - BEG_ACCRUED_RENT
                                 begin_accrued_rent[month_num > 1] ORDER BY month_num = end_accrued_rent[cv() - 1],
                                 end_accrued_rent[month_num] = case when month[cv()] < trueup_month[cv()] then
                                                                  prior_end_acc_rent[cv()]
                                                                else
                                                                  CASE WHEN end_rent_balance[cv()] <= 0 THEN 
                                                                     -1 * end_rent_balance[cv()]
                                                                   ELSE 
                                                                     0
                                                                   END
                                                                 end,
                                 accrued_rent[month_num] = end_accrued_rent[cv()] - begin_accrued_rent[cv()]
                                           
                               )
                  )
                  select ILR.ILR_ID ilr_id, ILR.REVISION revision, SOB.SET_OF_BOOKS_ID set_of_books_id,
                         MONTH,
                         interest_income_accrued,
                         begin_deferred_rent,
                         deferred_rent,
                         end_deferred_rent,
                         begin_accrued_rent,
                         accrued_rent,
                         end_accrued_rent
                    from sched
                ) u
          on (    s.ilr_id = u.ilr_id
              and s.revision = u.revision
              and s.set_of_books_id = u.set_of_books_id
              and s.month = u.month)
          when matched then
            update set s.interest_income_accrued = nvl(u.interest_income_accrued,0),
                       s.beg_deferred_rent = nvl(u.begin_deferred_rent,0),
                       s.deferred_rent = nvl(u.deferred_rent,0),
                       s.end_deferred_rent = nvl(u.end_deferred_rent,0),
                       s.beg_accrued_rent = nvl(u.begin_accrued_rent,0),
                       s.accrued_rent = nvl(u.accrued_rent,0),
                       s.end_accrued_rent = nvl(u.end_accrued_rent,0);
                       
        DBMS_OUTPUT.PUT_LINE('  - Records updated: '||SQL%ROWCOUNT); 
          
        end loop;
      end loop;
  end loop;
exception
  when others then
    DBMS_OUTPUT.PUT_LINE('ERROR: '||sqlerrm);
    RAISE_APPLICATION_ERROR(-20000, 'ERROR: '||sqlerrm); 
    rollback; 
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15324, 0, 2018, 2, 0, 0, 53082, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053082_lessor_01_trueup_rent_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;