/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_046913_reg_add_case_processing_hints_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2017.1.0.0 01/19/2017 Sarah Byers		 Add hints for RMS case processing
||============================================================================
*/
--Indexes created for REG_CASE_ALLOC_ACCT table
CREATE INDEX case_alloc_acct_idx_10
  ON reg_case_alloc_account (
    reg_case_id,
    reg_alloc_acct_id,
    reg_alloc_category_id,
    reg_alloc_target_id,
    reg_acct_id
  )
  TABLESPACE pwrplant_idx
;

CREATE INDEX case_alloc_acct_idx_4
  ON reg_case_alloc_account (
    parent_reg_alloc_acct_id
  )
  TABLESPACE pwrplant_idx
;

CREATE INDEX case_alloc_acct_idx_6
  ON reg_case_alloc_account (
    reg_alloc_acct_id,
    reg_acct_id,
    reg_case_id
  )
  TABLESPACE pwrplant_idx
;

--Indexes created for REG_ALLOC_RESULT table
CREATE INDEX reg_alloc_result_idx_5
  ON reg_alloc_result (
    reg_acct_id,
    reg_alloc_category_id
  )
  TABLESPACE pwrplant_idx
 ;

CREATE INDEX reg_alloc_result_idx_6
  ON reg_alloc_result (
    reg_case_id,
    reg_alloc_category_id
  )
  TABLESPACE pwrplant_idx
;

CREATE INDEX reg_alloc_result_idx_7
  ON reg_alloc_result (
    reg_acct_id,
    reg_case_id
  )
  TABLESPACE pwrplant_idx
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3358, 0, 2017, 1, 0, 0, 46913, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_046913_reg_add_case_processing_hints_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;