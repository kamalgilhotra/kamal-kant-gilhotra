/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_042984_pcm_search_user_options_dml.sql
|| Description:
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2015.1.0.0 02/27/2015 Ryan Oliveria  Add user options to FP/WO Search Grid
||============================================================================
*/

--* Convert existing grids to new PCM datawindows
update SELECT_TABS_GRIDS set DW_NAME = 'dw_pcm_fp_search_grid' where DW_NAME = 'dw_fp_grid';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_pcm_fp_search_grid_sob' where DW_NAME = 'dw_fp_grid_second_sob';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_pcm_fp_search_grid_simple' where DW_NAME = 'dw_fp_grid_alt';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_pcm_wo_search_grid' where DW_NAME = 'dw_work_order_grid';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_pcm_wo_search_grid_sob' where DW_NAME = 'dw_work_order_grid_second_sob';
update SELECT_TABS_GRIDS set DW_NAME = 'dw_pcm_wo_search_grid_simple' where DW_NAME = 'dw_work_order_grid_alt';


--* FUNDING PROJECTS
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values
	('Funding Project Search - Grid View', 'The default way to display funding projects in a search grid.', 0, ' ', null, 1, 0);

insert into PPBASE_SYSTEM_OPTIONS_MODULE (SYSTEM_OPTION_ID, MODULE) values ('Funding Project Search - Grid View', 'pcm');

insert into PPBASE_SYSTEM_OPTIONS_WKSP (SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER) values ('Funding Project Search - Grid View', 'fp_search');

insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE)
select 'Funding Project Search - Grid View', DESCRIPTION
  from SELECT_TABS_GRIDS
 where subsystem = 'funding_project';

insert into PPBASE_USER_OPTIONS (OBJECT, OPTION_IDENTIFIER, USERS, USER_OPTION)
select 'System Option', 'Funding Project Search - Grid View', p.USERS, s.DESCRIPTION
  from PP_MYPP_USER_VALUES p, SELECT_TABS_GRIDS s
 where p.RECORD_DATA = s.DW_NAME
   and p.RECORD_TYPE = 'FUNDING PROJECT DEFAULT GRID'
   and s.SUBSYSTEM = 'funding_project'
   and not exists (select 1 from PPBASE_USER_OPTIONS where lower(USERS) = lower(p.USERS) and OPTION_IDENTIFIER = 'Funding Project Search - Grid View');

--* WORK ORDERS
insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION, ALLOW_COMPANY_OVERRIDE)
values
	('Work Order Search - Grid View', 'The default way to display work orders in a search grid.', 0, ' ', null, 1, 0);

insert into PPBASE_SYSTEM_OPTIONS_MODULE (SYSTEM_OPTION_ID, MODULE) values ('Work Order Search - Grid View', 'pcm');

insert into PPBASE_SYSTEM_OPTIONS_WKSP (SYSTEM_OPTION_ID, WORKSPACE_IDENTIFIER) values ('Work Order Search - Grid View', 'wo_search');

insert into PPBASE_SYSTEM_OPTIONS_VALUES (SYSTEM_OPTION_ID, OPTION_VALUE)
select 'Work Order Search - Grid View', DESCRIPTION
  from SELECT_TABS_GRIDS
 where subsystem = 'work_order';

insert into PPBASE_USER_OPTIONS (OBJECT, OPTION_IDENTIFIER, USERS, USER_OPTION)
select 'System Option', 'Work Order Search - Grid View', p.USERS, s.DESCRIPTION
  from PP_MYPP_USER_VALUES p, SELECT_TABS_GRIDS s
 where p.RECORD_DATA = s.DW_NAME
   and p.RECORD_TYPE = 'WORK ORDER DEFAULT GRID'
   and s.SUBSYSTEM = 'work_order'
   and not exists (select 1 from PPBASE_USER_OPTIONS where lower(USERS) = lower(p.USERS) and OPTION_IDENTIFIER = 'Work Order Search - Grid View');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2333, 0, 2015, 1, 0, 0, 042984, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042984_pcm_search_user_options_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;