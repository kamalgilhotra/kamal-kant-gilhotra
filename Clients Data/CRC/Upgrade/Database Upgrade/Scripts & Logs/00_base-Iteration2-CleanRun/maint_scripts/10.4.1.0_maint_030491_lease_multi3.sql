/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030491_lease_multi3.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/13/2013 Brandon Beck   Point release
||============================================================================
*/

-- the third maint script for 30491

insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Search Leases',
    'Search Leases, ILRs, Assets', null, 1, 'uo_mypp_pop_up_ls', null);

insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Search Leases'), 1, null, null,
    'w_ls_center_main', 'w_ls_center_main', 'clicked', null);

insert into WORKFLOW_SUBSYSTEM
   (SUBSYSTEM, FIELD1_DESC, FIELD2_DESC, FIELD3_DESC, FIELD4_DESC, FIELD5_DESC, FIELD1_SQL,
    FIELD2_SQL, FIELD3_SQL, FIELD4_SQL, FIELD5_SQL, WORKFLOW_TYPE_FILTER, PENDING_NOTIFICATION_TYPE,
    APPROVED_NOTIFICATION_TYPE, REJECTED_NOTIFICATION_TYPE, SEND_SQL, APPROVE_SQL, REJECT_SQL,
    UNREJECT_SQL, UNSEND_SQL, UPDATE_WORKFLOW_TYPE_SQL, SEND_EMAILS, CASCADE_APPROVALS)
values
   ('ls_payment_approval', 'Payment ID', null, null, null, null, '<<id_field1>>', null, null, null,
    null, null, 'PAYMENT APPROVAL MESSAGE', 'PAYMENT APPROVED NOTICE MESSAGE',
    'PAYMENT REJECTION MESSAGE', 'select PKG_LEASE_CALC.F_SEND_PAYMENT (<<id_field1>>) from dual',
    'select PKG_LEASE_CALC.F_APPROVE_PAYMENT(<<id_field1>>) from dual',
    'select PKG_LEASE_CALC.F_REJECT_PAYMENT(<<id_field1>>) from dual',
    'select PKG_LEASE_CALC.F_UNREJECT_PAYMENT(<<id_field1>>) from dual',
    'select PKG_LEASE_CALC.F_UNSEND_PAYMENT(<<id_field1>>) from dual',
    'select PKG_LEASE_CALC.F_UPDATE_WORKFLOW_PAYMENT(<<id_field1>>) from dual', 1, null);

create unique index LS_LEASE_LEASE_NUM_NDX
   on LS_LEASE (LEASE_NUMBER)
      tablespace PWRPLANT_IDX;

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
   select nvl(max(ID),0)+1, 'LESSEE', 'build_schedule', 'Build Schedule', 1, 'ue_buildSchedule', null, null
   from PPBASE_ACTIONS_WINDOWS
   where not exists (
    select 1 from PPBASE_ACTIONS_WINDOWS
    where module = 'LESSEE' and action_identifier = 'build_schedule'
   );

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
   select nvl(max(ID),0)+1, 'LESSEE', 'drill_mla', 'Drill to MLA', 2, 'ue_drillMLA', null, null
   from PPBASE_ACTIONS_WINDOWS
   where not exists (
    select 1 from PPBASE_ACTIONS_WINDOWS
    where module = 'LESSEE' and action_identifier = 'drill_mla'
   );

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT, USER_ID, TIME_STAMP)
   select nvl(max(ID),0)+1, 'LESSEE', 'new_asset', 'Initiate Asset', 7, 'ue_newAsset', null, null
   from PPBASE_ACTIONS_WINDOWS
   where not exists (
    select 1 from PPBASE_ACTIONS_WINDOWS
    where module = 'LESSEE' and action_identifier = 'new_asset'
   );

update PPBASE_ACTIONS_WINDOWS set ACTION_TEXT = 'Initiate ILR'
  where MODULE = 'LESSEE'
  and action_identifier = 'new_ilr';


create unique index LS_ILR_NUM_NDX
   on LS_ILR (ILR_NUMBER)
      tablespace PWRPLANT_IDX;

delete from LS_ILR_STATUS where ILR_STATUS_ID > 3;

update LS_ILR_STATUS
   set DESCRIPTION = 'In Service', LONG_DESCRIPTION = 'In Service'
 where ILR_STATUS_ID = 2;

update LS_ILR_STATUS
   set DESCRIPTION = 'Retired', LONG_DESCRIPTION = 'Retired'
 where ILR_STATUS_ID = 3;


--Add Payment Status ID to ls_payment_hdr
alter table LS_PAYMENT_HDR add PAYMENT_STATUS_ID number(22);

--Create ls_payment_approval
create table LS_PAYMENT_APPROVAL
(
 PAYMENT_ID         number(22) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(35),
 APPROVAL_TYPE_ID   number(22),
 APPROVAL_STATUS_ID number(22),
 APPROVER           varchar2(35),
 APPROVAL_DATE      date,
 REJECTED           number(22)
);

--Add payment approval flag to ls_lease_group
alter table LS_LEASE_GROUP add PYMT_APPROVAL_FLAG number(22);

DECLARE
  DEFAULT_WF_RULE number(22,0);
  AUTO_WF_RULE number(22,0);
  AUTO_WF_TYPE number(22,0);
  MLA_WF_TYPE number(22,0);
  ILR_WF_TYPE number(22,0);
  PAY_WF_TYPE number(22,0);
BEGIN
  -- Set variables
  select nvl(max(WORKFLOW_RULE_ID),0)+1 into DEFAULT_WF_RULE
  from WORKFLOW_RULE;

  select nvl(max(WORKFLOW_RULE_ID),0)+2 into AUTO_WF_RULE
  from WORKFLOW_RULE;

  select WORKFLOW_TYPE_ID into AUTO_WF_TYPE
  from WORKFLOW_TYPE
  where EXTERNAL_WORKFLOW_TYPE = 'AUTO';

  select WORKFLOW_TYPE_ID into MLA_WF_TYPE
  from WORKFLOW_TYPE
  where SUBSYSTEM = 'mla_approval';

  select WORKFLOW_TYPE_ID into ILR_WF_TYPE
  from WORKFLOW_TYPE
  where SUBSYSTEM = 'ilr_approval';

  select WORKFLOW_TYPE_ID into PAY_WF_TYPE
  from WORKFLOW_TYPE
  where SUBSYSTEM = 'ls_payment_approval';


  --Adding auto approve type and rule
  insert into WORKFLOW_RULE
    (WORKFLOW_RULE_ID, DESCRIPTION, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED, SQL, SQL_USERS,
      GROUP_APPROVAL)
  values
    (DEFAULT_WF_RULE, 'Default Lease Workflow Rule', 0, '', 1, '', '', '');

  insert into WORKFLOW_RULE
    (WORKFLOW_RULE_ID, DESCRIPTION, AUTHORITY_LIMIT, NUM_APPROVERS, NUM_REQUIRED, SQL, SQL_USERS,
      GROUP_APPROVAL)
  values
    (AUTO_WF_RULE, 'Auto', 1000000000000, '', 0, '', '', '');

  insert into WORKFLOW_TYPE_RULE
    (WORKFLOW_TYPE_ID, WORKFLOW_RULE_ID, RULE_ORDER, sql)
  values
    (AUTO_WF_TYPE, AUTO_WF_RULE, 1, '');



  insert into WORKFLOW_TYPE_RULE
    (WORKFLOW_TYPE_ID, WORKFLOW_RULE_ID, RULE_ORDER, SQL)
  values
    (MLA_WF_TYPE, DEFAULT_WF_RULE, 1, '');

  insert into WORKFLOW_TYPE_RULE
    (WORKFLOW_TYPE_ID, WORKFLOW_RULE_ID, RULE_ORDER, SQL)
  values
    (ILR_WF_TYPE, DEFAULT_WF_RULE, 1, '');

  insert into WORKFLOW_TYPE_RULE
    (WORKFLOW_TYPE_ID, WORKFLOW_RULE_ID, RULE_ORDER, SQL)
  values
    (PAY_WF_TYPE, DEFAULT_WF_RULE, 1, '');

END;
/


alter table LS_PAYMENT_APPROVAL
   add constraint PK_LS_PAYMENT_APPROVAL
       primary key (PAYMENT_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_PAYMENT_APPROVAL
   add constraint FK1_LS_PAYMENT_APPROVAL
       foreign key (PAYMENT_ID)
       references LS_PAYMENT_HDR (PAYMENT_ID);

--Add payment approvals to MYPP

--Insert into PP_MYPP_TASK
insert into PP_MYPP_TASK
   (TASK_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION, PRECEDENT_TASK_ID,
    ALLOW_AS_SHORTCUT_YN, POP_UP_OBJECT, HELP_WINDOW_NAME)
values
   ((select max(TASK_ID) + 1 from PP_MYPP_TASK), null, null, 'Lease Payment Approval',
    'Lease Payment Approval', (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Approvals'), 1,
    null, null);

--Insert into PP_MYPP_TASK_STEP
insert into PP_MYPP_TASK_STEP
   (TASK_ID, STEP_ID, TIME_STAMP, USER_ID, WINDOW, PATH, EVENT, INTERFACE_ID)
values
   ((select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lease Payment Approval'), 1, null, null,
    'w_ls_center_main', 'dw_approve_payments', 'clicked', null);

--Insert into PP_MYPP_APPROVAL_CONTROL
insert into PP_MYPP_APPROVAL_CONTROL
   (MYPP_APPRV_ID, TIME_STAMP, USER_ID, LABEL_ONE, LABEL_MULTI, SORT_ORDER, TASK_ID, DW,
    ADDITION_ARG, sql, STATUS, TASK_WINDOW_PATH)
values
   ((select max(MYPP_APPRV_ID) + 1 from PP_MYPP_APPROVAL_CONTROL), sysdate, 'PWRPLANT',
    'Payment to Approve', 'Payment to Approve',
    (select max(SORT_ORDER) + 1 from PP_MYPP_APPROVAL_CONTROL),
    (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'MLA Approval'),
    'dw_ls_pymnt_approval_grid', null, null, 1, null);

alter table LS_ILR_DOCUMENT drop constraint R_LS_ILR_DOCUMENT1;

alter table LS_ILR_DOCUMENT
   add constraint R_LS_ILR_DOCUMENT1B
       foreign key (ILR_ID)
       references LS_ILR (ILR_ID);

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_pymntcntr_wksp_approvals'
 where WORKSPACE_IDENTIFIER = 'approval_payments'
 and MODULE = 'LESSEE';

create sequence LS_ILR_GROUP_SEQ;

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, TIME_STAMP, USER_ID, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('LESSEE', 'admin_ilr_groups', sysdate, user, 'ILR Groups', 'uo_ls_igcntr_wksp_details', null);

update PPBASE_MENU_ITEMS
   set ITEM_ORDER = ITEM_ORDER + 1
 where PARENT_MENU_IDENTIFIER = 'menu_wksp_admin'
 and MODULE = 'LESSEE'
   and ITEM_ORDER > 1;

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, TIME_STAMP, USER_ID, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP,
    PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('LESSEE', 'admin_ilr_groups', sysdate, user, 2, 2, 'ILR Groups', null, 'menu_wksp_admin',
    'admin_ilr_groups', 1);

--* Make TOLERANCE_PCT on LS_LEASE_GROUP a 22,8 number

alter table LS_LEASE_GROUP add TOLERANCE_PCT_ARC number(22,2);

update LS_LEASE_GROUP set TOLERANCE_PCT_ARC = TOLERANCE_PCT;

update LS_LEASE_GROUP set TOLERANCE_PCT = null;

alter table LS_LEASE_GROUP modify TOLERANCE_PCT number(22,8);

update LS_LEASE_GROUP set TOLERANCE_PCT = TOLERANCE_PCT_ARC;

alter table LS_LEASE_GROUP drop column TOLERANCE_PCT_ARC;

alter table LS_VENDOR add STATUS_CODE_ID number(22,0);

update LS_VENDOR
   set STATUS_CODE_ID =
        (select STATUS_CODE_ID from LS_LESSOR where LESSOR_ID = LS_VENDOR.LESSOR_ID);

alter table LS_VENDOR add PRIMARY number(1,0);

update LS_VENDOR A
   set PRIMARY = 1
 where not exists (select 1
          from LS_VENDOR B
         where B.LESSOR_ID = A.LESSOR_ID
           and B.PRIMARY = 1)
   and not exists (select 1
          from LS_VENDOR B
         where B.LESSOR_ID = A.LESSOR_ID
           and B.DESCRIPTION < A.DESCRIPTION);

update WORKFLOW_SUBSYSTEM
   set PENDING_NOTIFICATION_TYPE = 'ILR APPROVAL MESSAGE'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set APPROVED_NOTIFICATION_TYPE = 'ILR APPROVED NOTICE MESSAGE'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set REJECTED_NOTIFICATION_TYPE = 'ILR REJECTION MESSAGE'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set SEND_SQL = 'select PKG_LEASE_CALC.F_SEND_ILR(<<id_field1>>, <<id_field2>>) from dual'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set APPROVE_SQL = 'select PKG_LEASE_CALC.F_APPROVE_ILR(<<id_field1>>, <<id_field2>>) from dual'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set REJECT_SQL = 'select PKG_LEASE_CALC.F_REJECT_ILR(<<id_field1>>, <<id_field2>>) from dual'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set UNREJECT_SQL = 'select PKG_LEASE_CALC.F_UNREJECT_ILR(<<id_field1>>, <<id_field2>>) from dual'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set UNSEND_SQL = 'select PKG_LEASE_CALC.F_UNSEND_ILR(<<id_field1>>, <<id_field2>>) from dual'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM
   set UPDATE_WORKFLOW_TYPE_SQL = 'select PKG_LEASE_CALC.F_UPDATE_WORKFLOW_ILR(<<id_field1>>, <<id_field2>>) from dual'
 where SUBSYSTEM = 'ilr_approval';

update WORKFLOW_SUBSYSTEM set SEND_EMAILS = 1 where SUBSYSTEM = 'ilr_approval';


update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_pymntcntr_approvals_open'
 where WORKSPACE_IDENTIFIER = 'approval_payments'
 and MODULE = 'LESSEE';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_assetcntr_wksp_wizard'
 where WORKSPACE_IDENTIFIER = 'initiate_asset'
 and MODULE = 'LESSEE';

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
   select max(ID) + 1, 'LESSEE', 'retire', 'Retire Asset', 1, 'ue_retire'
     from PPBASE_ACTIONS_WINDOWS;
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
   select max(ID) + 1, 'LESSEE', 'transfer', 'Transfer Asset', 3, 'ue_transfer'
     from PPBASE_ACTIONS_WINDOWS;
insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
   select max(ID) + 1, 'LESSEE', 'drill_ilr', 'Drill to ILR', 4, 'ue_drillilr'
     from PPBASE_ACTIONS_WINDOWS;

update PP_MYPP_APPROVAL_CONTROL
   set TASK_ID =
        (select TASK_ID from PP_MYPP_TASK where DESCRIPTION = 'Lease Payment Approval')
 where LABEL_ONE = 'Payment to Approve';

alter table LS_ILR_ASSET_SCHEDULE_CALC_STG
   add (EXECUTORY_ACCRUAL1   number(22,2),
        EXECUTORY_ACCRUAL2   number(22,2),
        EXECUTORY_ACCRUAL3   number(22,2),
        EXECUTORY_ACCRUAL4   number(22,2),
        EXECUTORY_ACCRUAL5   number(22,2),
        EXECUTORY_ACCRUAL6   number(22,2),
        EXECUTORY_ACCRUAL7   number(22,2),
        EXECUTORY_ACCRUAL8   number(22,2),
        EXECUTORY_ACCRUAL9   number(22,2),
        EXECUTORY_ACCRUAL10  number(22,2),
        EXECUTORY_PAID1      number(22,2),
        EXECUTORY_PAID2      number(22,2),
        EXECUTORY_PAID3      number(22,2),
        EXECUTORY_PAID4      number(22,2),
        EXECUTORY_PAID5      number(22,2),
        EXECUTORY_PAID6      number(22,2),
        EXECUTORY_PAID7      number(22,2),
        EXECUTORY_PAID8      number(22,2),
        EXECUTORY_PAID9      number(22,2),
        EXECUTORY_PAID10     number(22,2),
        CONTINGENT_ACCRUAL1  number(22,2),
        CONTINGENT_ACCRUAL2  number(22,2),
        CONTINGENT_ACCRUAL3  number(22,2),
        CONTINGENT_ACCRUAL4  number(22,2),
        CONTINGENT_ACCRUAL5  number(22,2),
        CONTINGENT_ACCRUAL6  number(22,2),
        CONTINGENT_ACCRUAL7  number(22,2),
        CONTINGENT_ACCRUAL8  number(22,2),
        CONTINGENT_ACCRUAL9  number(22,2),
        CONTINGENT_ACCRUAL10 number(22,2),
        CONTINGENT_PAID1     number(22,2),
        CONTINGENT_PAID2     number(22,2),
        CONTINGENT_PAID3     number(22,2),
        CONTINGENT_PAID4     number(22,2),
        CONTINGENT_PAID5     number(22,2),
        CONTINGENT_PAID6     number(22,2),
        CONTINGENT_PAID7     number(22,2),
        CONTINGENT_PAID8     number(22,2),
        CONTINGENT_PAID9     number(22,2),
        CONTINGENT_PAID10    number(22,2));

alter table LS_ILR_ASSET_SCHEDULE_STG
   add (EXECUTORY_ACCRUAL1   number(22,2),
        EXECUTORY_ACCRUAL2   number(22,2),
        EXECUTORY_ACCRUAL3   number(22,2),
        EXECUTORY_ACCRUAL4   number(22,2),
        EXECUTORY_ACCRUAL5   number(22,2),
        EXECUTORY_ACCRUAL6   number(22,2),
        EXECUTORY_ACCRUAL7   number(22,2),
        EXECUTORY_ACCRUAL8   number(22,2),
        EXECUTORY_ACCRUAL9   number(22,2),
        EXECUTORY_ACCRUAL10  number(22,2),
        EXECUTORY_PAID1      number(22,2),
        EXECUTORY_PAID2      number(22,2),
        EXECUTORY_PAID3      number(22,2),
        EXECUTORY_PAID4      number(22,2),
        EXECUTORY_PAID5      number(22,2),
        EXECUTORY_PAID6      number(22,2),
        EXECUTORY_PAID7      number(22,2),
        EXECUTORY_PAID8      number(22,2),
        EXECUTORY_PAID9      number(22,2),
        EXECUTORY_PAID10     number(22,2),
        CONTINGENT_ACCRUAL1  number(22,2),
        CONTINGENT_ACCRUAL2  number(22,2),
        CONTINGENT_ACCRUAL3  number(22,2),
        CONTINGENT_ACCRUAL4  number(22,2),
        CONTINGENT_ACCRUAL5  number(22,2),
        CONTINGENT_ACCRUAL6  number(22,2),
        CONTINGENT_ACCRUAL7  number(22,2),
        CONTINGENT_ACCRUAL8  number(22,2),
        CONTINGENT_ACCRUAL9  number(22,2),
        CONTINGENT_ACCRUAL10 number(22,2),
        CONTINGENT_PAID1     number(22,2),
        CONTINGENT_PAID2     number(22,2),
        CONTINGENT_PAID3     number(22,2),
        CONTINGENT_PAID4     number(22,2),
        CONTINGENT_PAID5     number(22,2),
        CONTINGENT_PAID6     number(22,2),
        CONTINGENT_PAID7     number(22,2),
        CONTINGENT_PAID8     number(22,2),
        CONTINGENT_PAID9     number(22,2),
        CONTINGENT_PAID10    number(22,2));

alter table LS_ILR_SCHEDULE_STG
   add (EXECUTORY_ACCRUAL1   number(22,2),
        EXECUTORY_ACCRUAL2   number(22,2),
        EXECUTORY_ACCRUAL3   number(22,2),
        EXECUTORY_ACCRUAL4   number(22,2),
        EXECUTORY_ACCRUAL5   number(22,2),
        EXECUTORY_ACCRUAL6   number(22,2),
        EXECUTORY_ACCRUAL7   number(22,2),
        EXECUTORY_ACCRUAL8   number(22,2),
        EXECUTORY_ACCRUAL9   number(22,2),
        EXECUTORY_ACCRUAL10  number(22,2),
        EXECUTORY_PAID1      number(22,2),
        EXECUTORY_PAID2      number(22,2),
        EXECUTORY_PAID3      number(22,2),
        EXECUTORY_PAID4      number(22,2),
        EXECUTORY_PAID5      number(22,2),
        EXECUTORY_PAID6      number(22,2),
        EXECUTORY_PAID7      number(22,2),
        EXECUTORY_PAID8      number(22,2),
        EXECUTORY_PAID9      number(22,2),
        EXECUTORY_PAID10     number(22,2),
        CONTINGENT_ACCRUAL1  number(22,2),
        CONTINGENT_ACCRUAL2  number(22,2),
        CONTINGENT_ACCRUAL3  number(22,2),
        CONTINGENT_ACCRUAL4  number(22,2),
        CONTINGENT_ACCRUAL5  number(22,2),
        CONTINGENT_ACCRUAL6  number(22,2),
        CONTINGENT_ACCRUAL7  number(22,2),
        CONTINGENT_ACCRUAL8  number(22,2),
        CONTINGENT_ACCRUAL9  number(22,2),
        CONTINGENT_ACCRUAL10 number(22,2),
        CONTINGENT_PAID1     number(22,2),
        CONTINGENT_PAID2     number(22,2),
        CONTINGENT_PAID3     number(22,2),
        CONTINGENT_PAID4     number(22,2),
        CONTINGENT_PAID5     number(22,2),
        CONTINGENT_PAID6     number(22,2),
        CONTINGENT_PAID7     number(22,2),
        CONTINGENT_PAID8     number(22,2),
        CONTINGENT_PAID9     number(22,2),
        CONTINGENT_PAID10    number(22,2));

alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_11 to E_BUCKET_1;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_12 to E_BUCKET_2;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_13 to E_BUCKET_3;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_14 to E_BUCKET_4;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_15 to E_BUCKET_5;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_16 to E_BUCKET_6;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_17 to E_BUCKET_7;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_18 to E_BUCKET_8;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_19 to E_BUCKET_9;
alter table LS_ILR_PAYMENT_TERM rename column C_BUCKET_20 to E_BUCKET_10;

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_pymntcntr_approvals_open'
 where WORKSPACE_IDENTIFIER = 'approval_payments'
 and MODULE = 'LESSEE';

update PPBASE_MENU_ITEMS
   set WORKSPACE_IDENTIFIER = 'menu_wksp_reports'
 where MENU_IDENTIFIER = 'menu_wksp_reports'
 and MODULE = 'LESSEE';

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_rptcntr_wksp'
 where WORKSPACE_IDENTIFIER = 'menu_wksp_reports'
 and MODULE = 'LESSEE';

insert into PP_REPORTS_FILTER
   (PP_REPORT_FILTER_ID, TIME_STAMP, USER_ID, DESCRIPTION, TABLE_NAME, FILTER_UO_NAME)
   select 40, sysdate, user, 'Lessee - Company', 'company_setup', 'uo_ls_selecttabs_company'
     from DUAL;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'MLA''s Without ILR''s',
          'MLA''s Without ILR''s',
          'Lessee',
          'dw_ls_rpt_mla_without_ilr',
          null,
          'Main - Lease',
          null,
          'Lessee - 1000',
          null,
          null,
          null,
          11,
          6,
          0,
          1,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1000',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Leased Assets Without ILR''s',
          'Leased Assets Without ILR''s',
          'Lessee',
          'dw_ls_rpt_asset_without_ilr',
          null,
          'Main - Lease',
          null,
          'Lessee - 1001',
          null,
          null,
          null,
          11,
          6,
          0,
          1,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1001',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Leased Asset Retirements',
          'Leased Asset Retirements',
          'Lessee',
          'dw_ls_rpt_asset_retirement',
          'already has report time',
          'Main - Lease',
          'Span',
          'Lessee - 1002',
          null,
          null,
          null,
          11,
          6,
          2,
          1,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1002',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Expiring ILR''s',
          'Expiring ILR''s',
          'Lessee',
          'dw_ls_rpt_expiring_ilrs',
          'already has report time',
          'Main - Lease',
          'Span',
          'Lessee - 1003',
          null,
          null,
          null,
          11,
          6,
          2,
          1,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1003',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Future Estimated Executory Cost',
          'Future Estimated Executory Cost',
          'Lessee',
          'dw_ls_rpt_future_est_exec_cost',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1004',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1004',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Future Estimated Minimum Payment',
          'Future Estimated Minimum Payment',
          'Lessee',
          'dw_ls_rpt_future_est_min_payment',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1005',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1005',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Lease Calc Adjustment',
          'Lease Calc Adjustment',
          'Lessee',
          'dw_ls_rpt_lease_calc_adjustment',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1006',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1006',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Lease Calc Overview',
          'Lease Calc Overview',
          'Lessee',
          'dw_ls_rpt_lease_calc_overview',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1007',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1007',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Lease Calc Payment',
          'Lease Calc Payment',
          'Lessee',
          'dw_ls_rpt_lease_calc_payment',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1008',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1008',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'GL Account Balances',
          'GL Account Balances',
          'Lessee',
          'dw_ls_rpt_gl_account_balances',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1009',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1009',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'ILR''s by ILR Group',
          'ILR''s by ILR Group',
          'Lessee',
          'dw_ls_rpt_ilrs_by_ilr_group',
          'already has report time',
          'Main - Lease',
          null,
          'Lessee - 1010',
          null,
          null,
          null,
          11,
          6,
          0,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1010',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Lease Calc Accrual',
          'Lease Calc Accrual',
          'Lessee',
          'dw_ls_rpt_lease_calc_accrual',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1011',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1011',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Principle Interest Breakdown',
          'Principle Interest Breakdown',
          'Lessee',
          'dw_ls_rpt_principle_interest_breakdown',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1012',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1012',
          null,
          0
     from PP_REPORTS;

insert into PP_REPORTS
   (REPORT_ID, USER_ID, TIME_STAMP, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW,
    SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, INPUT_WINDOW, FILTER_OPTION, STATUS,
    PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID,
    PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, DOCUMENTATION, USER_COMMENT, LAST_APPROVED_DATE,
    PP_REPORT_NUMBER, OLD_REPORT_NUMBER, DYNAMIC_DW)
   select max(REPORT_ID) + 1,
          user,
          sysdate,
          'Short/Long Term Obligation',
          'Short/Long Term Obligation',
          'Lessee',
          'dw_ls_rpt_short_long_term_ob',
          'already has report time',
          'Main - Lease',
          'Single Month',
          'Lessee - 1013',
          null,
          null,
          null,
          11,
          6,
          1,
          40,
          1,
          1,
          null,
          null,
          null,
          'Lessee - 1013',
          null,
          0
     from PP_REPORTS;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (510, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_multi3.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
