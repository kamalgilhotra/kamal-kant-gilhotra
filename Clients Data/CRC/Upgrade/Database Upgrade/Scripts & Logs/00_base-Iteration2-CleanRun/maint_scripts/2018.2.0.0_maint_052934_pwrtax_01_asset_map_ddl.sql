/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_052934_pwrtax_01_asset_map_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2018.2.0.0 02/05/2019 Shane "C" Ward   Updates to include Asset ID on PowerTax Imports
||============================================================================
*/

create table tax_erp_asset_map (
company_id            number(22,0) not null,
erp_asset_id          varchar2(35) not null,
powertax_asset_id     number(22,0) not null,
user_id               varchar2(18),
time_stamp            date);


ALTER TABLE tax_erp_asset_map
  ADD CONSTRAINT tax_erp_asset_map_co_fk FOREIGN KEY (
    company_id
  ) REFERENCES company_setup (
    company_id
  );

COMMENT ON TABLE tax_erp_asset_map IS 'The erp_asset_map table holds the mapping of external ERP asset id to PowerTax Asset Id. This is necessary for companies that can have non-numeric asset id values';

COMMENT ON COLUMN tax_erp_asset_map.company_id IS 'Company Id of the asset loaded.';
COMMENT ON COLUMN tax_erp_asset_map.erp_asset_id IS 'Asset Id from the ERP system';
COMMENT ON COLUMN tax_erp_asset_map.powertax_asset_id IS 'System-assigned identifier of an asset within PowerTax';

--add work order number to retirements staging table so we can translate id values
alter table tax_book_transactions add work_order_number varchar2(35);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15685, 0, 2018, 2, 0, 0, 52934, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_052934_pwrtax_01_asset_map_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;