/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052997_lessee_01_fix_asset_quantity_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.2 01/25/2019  C Yura            Fix incorrect asset quantities caused by import of multiple assets per ILR
||============================================================================
*/

MERGE INTO ls_ilr_options o 
USING (SELECT SUM(Nvl(la.quantity, 0)) sum_asset_quantity, la.ilr_id, m.revision, opt.asset_quantity
        FROM ls_asset la, ls_ilr_asset_map m, ls_asset ia, ls_ilr_options opt
        WHERE la.ls_asset_id = m.ls_asset_id 
        AND la.ls_asset_id = ia.ls_asset_id
        AND la.ilr_id = m.ilr_id 
        AND m.ilr_id = opt.ilr_id
        and m.revision = opt.revision
        AND m.ilr_id = ia.ilr_id
        AND m.revision > 0 						  
        GROUP BY la.ilr_id, m.revision, opt.asset_quantity
      having SUM(Nvl(la.quantity, 0)) <> opt.asset_quantity  ) a 
ON (a.ilr_id = o.ilr_id AND a.revision = o.revision)
WHEN MATCHED THEN 
UPDATE SET o.asset_quantity = a.sum_asset_quantity;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (14302, 0, 2018, 1, 0, 2, 52997, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_052997_lessee_01_fix_asset_quantity_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;