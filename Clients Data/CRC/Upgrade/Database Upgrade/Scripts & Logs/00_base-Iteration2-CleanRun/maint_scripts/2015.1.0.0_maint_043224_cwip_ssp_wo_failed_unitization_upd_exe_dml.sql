/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043224_cwip_ssp_wo_failed_unitization_upd_exe_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By           Reason for Change
|| ---------- ---------- ----------------     ------------------------------------
|| 2015.1.0.0 03/10/2015 Khamchanh Sisouphanh Update executable name in pp_processes
||============================================================================
*/

update pp_processes
set executable_file = 'ssp_wo_auto_nonunitize.exe non'
where description = 'WO Auto Non-Unitize'
;
update pp_processes
set executable_file = 'ssp_wo_auto_nonunitize.exe failed'
where description = 'WO Failed Unitization'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2360, 0, 2015, 1, 0, 0, 043224, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043224_cwip_ssp_wo_failed_unitization_upd_exe_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;