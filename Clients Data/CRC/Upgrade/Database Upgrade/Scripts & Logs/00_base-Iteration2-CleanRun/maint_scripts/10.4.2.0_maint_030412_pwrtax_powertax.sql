/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030412_pwrtax_powertax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 11/05/2013 Ron Ferentini
||          04/03/2014 Lee Quinn      Renamed from maint_030412_prov_powertax.sql
||                                    to maint_030412_pwrtax_powertax.sql
||============================================================================
*/

create table TAX_REPAIRS_ADD_EXPENSE
(
 TAX_YEAR                 number(22) not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18),
 TAX_EXPENSE_PRIOR_MONTH  number(22,0),
 TAX_EXPENSE_FUTURE_MONTH number(22,0)
);

alter table TAX_REPAIRS_ADD_EXPENSE
   add constraint TAX_REPAIRS_ADD_EXPENSE_PK
       primary key (TAX_YEAR)
       using index tablespace PWRPLANT_IDX;

comment on table TAX_REPAIRS_ADD_EXPENSE is 'The table holds by tax years how many months forward to look and how many months backwards for Tax Repairs expensing.';
comment on column TAX_REPAIRS_ADD_EXPENSE.TAX_YEAR is 'The tax year.';
comment on column TAX_REPAIRS_ADD_EXPENSE.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column TAX_REPAIRS_ADD_EXPENSE.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_REPAIRS_ADD_EXPENSE.TAX_EXPENSE_PRIOR_MONTH is 'The last prior-year month that should be searched for tax expensing entries.';
comment on column TAX_REPAIRS_ADD_EXPENSE.TAX_EXPENSE_FUTURE_MONTH is 'The last future-year month_number that should be searched for tax expensing entries associated with the current tax year (or prior tax years)';

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION,SUBSYSTEM_DISPLAY,SUBSYSTEM_SCREEN,SUBSYSTEM)
   select 'tax_repairs_add_expense',
          's',
          'Tax Repairs Add Expense',
          'The Tax Repairs Add Expense data table holds by tax years how many months forward to look and how many months backwards.',
      'tax,accrual',
      'tax',
      'tax'
     from DUAL;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
   select 'tax_year', 'tax_repairs_add_expense', 'e', 'Tax Year', 100, 0 from DUAL;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
   select 'time_stamp', 'tax_repairs_add_expense', 'e', 'Time Stamp', 130, 0 from DUAL;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
   select 'user_id', 'tax_repairs_add_expense', 'e', 'User Id', 140, 0 from DUAL;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
   select 'tax_expense_prior_month',
          'tax_repairs_add_expense',
          'e',
          'Tax Expense Prior Month',
          110,
          0
     from DUAL;

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, PP_EDIT_TYPE_ID, DESCRIPTION, COLUMN_RANK, READ_ONLY)
   select 'tax_expense_future_month',
          'tax_repairs_add_expense',
          'e',
          'Tax Expense Future Month',
          120,
          0
     from DUAL;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (713, 0, 10, 4, 2, 0, 30412, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_030412_pwrtax_powertax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
