/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053339_lessee_01_ilr_impair_stg_ddl.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 04/14/2019 Shane "C" Ward Add staging table for impairment calculations
||============================================================================
*/

CREATE GLOBAL temporary TABLE ls_ilr_impair_stg (
  ilr_id               NUMBER(22,0) NULL,
  revision             NUMBER(22,0) NULL,
  set_of_books_id      NUMBER(22,0) NULL,
  month                DATE         NULL,
  impairment_activity  NUMBER(22,2) NULL,
  beginning_impairment NUMBER(22,2) NULL,
  ending_impairment    NUMBER(22,2) NULL
)
ON COMMIT DELETE rows;

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16922, 0, 2018, 2, 1, 0, 53339 , 'C:\PlasticWks\powerplant\sql\maint_scripts','2018.2.1.0_maint_053339_lessee_01_ilr_impair_stg_ddl.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
