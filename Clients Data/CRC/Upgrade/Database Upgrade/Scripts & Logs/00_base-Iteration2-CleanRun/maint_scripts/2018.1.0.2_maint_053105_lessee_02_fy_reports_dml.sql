/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_053105_lessee_02_fy_reports_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.2 2/22/2019  C.Yura          Add new fixed year maturity analysis reports
||============================================================================
*/

INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Maturity Analysis - Fixed Year',
             'Disclosure: Displaying the discounted and undiscounted cash flows by company and type for each year after a given date',
             'Lessee',
             'dw_ls_rpt_disclosure_maturity_det_fy',
             'Lessee - 2004a',
             11,
             311,
             207,
             103,
             1,
             3,
	           1);
       
    INSERT INTO PP_REPORTS
            (report_id,
             description,
             long_description,
             subsystem,
             datawindow,
             report_number,
             pp_report_subsystem_id,
             report_type_id,
             pp_report_time_option_id,
             pp_report_filter_id,
             pp_report_status_id,
             pp_report_envir_id,
			 turn_off_multi_thread)
VALUES      (( SELECT Nvl(Max(report_id), 0) + 1
               FROM   PP_REPORTS ),
             'Maturity Analysis - Fixed Year',
             'Company Consolidation - Disclosure: Displaying the discounted and undiscounted cash flows by company and type for each year after a given date',
             'Lessee',
             'dw_ls_rpt_disc_maturity_det_consol_fy',
             'Lessee - 2004a - Consolidated',
             11,
             311,
             207,
             103,
             1,
             3,
	           1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (15303, 0, 2018, 1, 0, 2, 53105, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.2_maint_053105_lessee_02_fy_reports_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
