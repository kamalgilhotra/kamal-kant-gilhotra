/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_050970_lessor_01_update_lessor_sched_sob_queries_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2018.1.0.0 10/23/2018  Alex Healey    Alter Lessor ILR by SOB and ILR by schedule queries to display new fields
||============================================================================
*/

update pp_any_query_criteria
set sql = 'SELECT lct.ls_currency_type_id,
				   lct.description currency_type,
				   c.description company,
				   ls.lease_number lease_number,
				   ilr.ilr_number,
				   st.description ilr_status,
				   lg.description lease_group,
				   ig.description ilr_group,
				   lse.description lessee,
				   ct.description capitalization_type,
				   sob.description set_of_books,
				   amt.iso_code currency,
				   amt.currency_display_symbol AS currency_symbol,
				   amt.revision as revision,
				   ilr.current_revision,
				   nvl(amt.beginning_lease_receivable,0) beginning_lease_receivable,
				   nvl(amt.npv_lease_payments, 0) npv_lease_payments,
				   nvl(amt.receivable_remeasure, 0) receivable_remeasure,
				   nvl(amt.npv_guaranteed_residual, 0) npv_guaranteed_residual,
				   nvl(amt.npv_unguaranteed_residual, 0) npv_unguaranteed_residual,
				   nvl(amt.unguaran_resid_remeasure, 0) Unguaran_resid_remeasure,
				   nvl(amt.beginning_net_investment, 0) beginning_net_investment,
				   nvl(amt.cost_of_goods_sold, 0) cost_of_goods_sold,
				   nvl(SUM(decode(lct.ls_currency_type_id,
							  1,
							  la.fair_market_value,
							  la.fair_market_value_comp_curr)),0) fair_market_value,
				  COUNT(la.lsr_asset_id) as number_of_assets
			  FROM v_lsr_ilr_mc_calc_amounts amt
			  JOIN lsr_ilr ilr
				ON (ilr.ilr_id = amt.ilr_id AND ilr.current_revision = amt.revision)
			  JOIN company c
				ON (ilr.company_id = c.company_id)
			  JOIN ls_lease_currency_type lct
				ON (amt.ls_cur_type = lct.ls_currency_type_id)
			  JOIN lsr_lease ls
				ON (ilr.lease_id = ls.lease_id)
			  JOIN lsr_lessee lse
				ON (ls.lessee_id = lse.lessee_id)
			  JOIN lsr_lease_group lg
				ON (ls.lease_group_id = lg.lease_group_id)
			  JOIN lsr_ilr_group ig
				ON (ilr.ilr_group_id = ig.ilr_group_id)
			  JOIN lsr_ilr_options o
				ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)
			  JOIN lsr_cap_type ct
				ON (o.lease_cap_type_id = ct.cap_type_id)
			  JOIN ls_ilr_status st
				ON (ilr.ilr_status_id = st.ilr_status_id)
			  JOIN set_of_books sob
				ON (amt.set_of_books_id = sob.set_of_books_id)
			  LEFT OUTER JOIN lsr_asset la
				ON (amt.ilr_id = la.ilr_id AND amt.revision = la.revision)
			 GROUP BY lct.ls_currency_type_id,
					  lct.description,
					  c.description,
					  ls.lease_number,
					  ilr.ilr_number,
					  st.description,
					  lg.description,
					  ig.description,
					  lse.description,
					  ct.description,
					  sob.description,
					  amt.iso_code,
					  amt.currency_display_symbol,
					  amt.revision,
					  ilr.current_revision,
					  amt.beginning_lease_receivable,
					  amt.npv_lease_payments,
            amt.receivable_remeasure,
					  amt.npv_guaranteed_residual,
					  amt.npv_unguaranteed_residual,
            amt.unguaran_resid_remeasure,
					  amt.beginning_net_investment,
					  amt.cost_of_goods_sold
			 ORDER BY ilr_number, currency_type, set_of_books'
WHERE subsystem = 'lessor'
and description = 'Lessor ILR by Set of Books (Current Revision)';



 UPDATE   pp_any_query_criteria_fields  a
  SET column_order = column_order + 1
  WHERE column_order > (SELECT column_order FROM pp_any_query_criteria_fields 
                          WHERE Lower(detail_field) = 'npv_lease_payments' 
                           AND id = a.id)  
  AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor ILR by Set of Books (Current Revision)');
			
 UPDATE   pp_any_query_criteria_fields  a
  SET column_order = column_order + 1
  WHERE column_order > (SELECT column_order FROM pp_any_query_criteria_fields 
                          WHERE Lower(detail_field) = 'npv_unguaranteed_residual' 
                           AND id = a.id)  
  AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor ILR by Set of Books (Current Revision)');
			
insert into pp_any_query_criteria_fields
(id, detail_field, column_order, amount_field, include_in_select_criteria, default_value, column_header, column_width, display_field, display_table, column_type, quantity_field, data_field, required_filter, required_one_mult, sort_col, hide_from_results, hide_from_filters)
(select id, 'receivable_remeasure', (column_order+1), 1, 1, null, 'Receivable Remeasurement', 300, null, null, 'NUMBER', 0, null, null, null, null, 0, 0 from pp_any_query_criteria_fields  a
	WHERE column_order = (SELECT column_order FROM pp_any_query_criteria_fields 
                          WHERE Lower(detail_field) = 'npv_lease_payments' 
                           AND id = a.id)  
	AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor ILR by Set of Books (Current Revision)'));

insert into pp_any_query_criteria_fields
(id, detail_field, column_order, amount_field, include_in_select_criteria, default_value, column_header, column_width, display_field, display_table, column_type, quantity_field, data_field, required_filter, required_one_mult, sort_col, hide_from_results, hide_from_filters)
(select id, 'Unguaran_resid_remeasure', (column_order+1), 1, 1, null, 'Unguaran Residual Remeasure', 300, null, null, 'NUMBER', 0, null, null, null, null, 0, 0 from pp_any_query_criteria_fields  a
	WHERE column_order = (SELECT column_order FROM pp_any_query_criteria_fields 
                          WHERE Lower(detail_field) = 'npv_unguaranteed_residual' 
                           AND id = a.id)  
	AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor ILR by Set of Books (Current Revision)'));
			

			
update pp_any_query_criteria
set sql = 'SELECT  lct.description currency_type,  c.description company,  ls.lease_number lease_number,  ilr.ilr_number,  st.description ilr_status,  
lg.description lease_group,  ig.description ilr_group,  lse.description lessee,  ct.description capitalization_type,  sob.description set_of_books, 
 To_Char(schedule.MONTH, ''yyyymm'') month_number,  schedule.iso_code currency,  Nvl(schedule.principal_received,0) principal_received, 
 schedule.interest_income_received,  Nvl(schedule.principal_accrued,0) principal_accrued,  schedule.interest_income_accrued, schedule.deferred_rent, schedule.accrued_rent,
 schedule.contingent_paid1+schedule.contingent_paid2+schedule.contingent_paid3+schedule.contingent_paid4+schedule.contingent_paid5+  schedule.contingent_paid6+schedule.contingent_paid7+schedule.contingent_paid8+schedule.contingent_paid9+schedule.contingent_paid10 contingent_received, 
 schedule.executory_paid1+schedule.executory_paid2+schedule.executory_paid3+schedule.executory_paid4+schedule.executory_paid5+  schedule.executory_paid6+schedule.executory_paid7+schedule.executory_paid8+schedule.executory_paid9+schedule.executory_paid10 executory_received,  currency_display_symbol AS currency_symbol  
 FROM v_lsr_ilr_mc_schedule schedule  
 JOIN company c ON (schedule.company_id = c.company_id)   
 JOIN lsr_ilr ilr ON (schedule.ilr_id = ilr.ilr_id AND schedule.revision = ilr.current_revision)   
 JOIN ls_lease_currency_type lct ON (schedule.ls_cur_type = lct.ls_currency_type_id)   JOIN lsr_lease ls ON (ilr.lease_id = ls.lease_id)   
 JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id)   JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id)   
 JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id)   JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)  
 JOIN lsr_cap_type ct ON (o.lease_cap_type_id = ct.cap_type_id)   JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id)   
JOIN set_of_books sob ON (schedule.set_of_books_id = sob.set_of_books_id)  
ORDER BY ilr_number, currency_type, set_of_books, MONTH'
WHERE subsystem = 'lessor'
and description = 'Lessor Schedule by ILR';			



 UPDATE   pp_any_query_criteria_fields  a
  SET column_order = column_order + 2
  WHERE column_order > (SELECT column_order FROM pp_any_query_criteria_fields 
                          WHERE Lower(detail_field) = 'interest_income_accrued' 
                           AND id = a.id)  
  AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor Schedule by ILR');

insert into pp_any_query_criteria_fields
(id, detail_field, column_order, amount_field, include_in_select_criteria, default_value, column_header, column_width, display_field, display_table, column_type, quantity_field, data_field, required_filter, required_one_mult, sort_col, hide_from_results, hide_from_filters)
(select id, 'deferred_rent', (column_order+1), 1, 1, null, 'Deferred Rent', 300, null, null, 'NUMBER', 0, null, null, null, null, 0, 0 from pp_any_query_criteria_fields  a
	WHERE column_order = (SELECT column_order FROM pp_any_query_criteria_fields 
                          WHERE Lower(detail_field) = 'interest_income_accrued' 
                           AND id = a.id)  
	AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor Schedule by ILR'));
			
insert into pp_any_query_criteria_fields
(id, detail_field, column_order, amount_field, include_in_select_criteria, default_value, column_header, column_width, display_field, display_table, column_type, quantity_field, data_field, required_filter, required_one_mult, sort_col, hide_from_results, hide_from_filters)
(select id, 'accrued_rent', (column_order+2), 1, 1, null, 'Accrued Rent', 300, null, null, 'NUMBER', 0, null, null, null, null, 0, 0 from pp_any_query_criteria_fields  a
	WHERE column_order = (SELECT column_order FROM pp_any_query_criteria_fields 
                          WHERE Lower(detail_field) = 'interest_income_accrued' 
                           AND id = a.id)  
	AND id = (SELECT id FROM pp_any_query_criteria
            WHERE subsystem = 'lessor'
            and description = 'Lessor Schedule by ILR'));


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(10662, 0, 2018, 1, 0, 0, 50970, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.0_maint_050970_lessor_01_update_lessor_sched_sob_queries_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;