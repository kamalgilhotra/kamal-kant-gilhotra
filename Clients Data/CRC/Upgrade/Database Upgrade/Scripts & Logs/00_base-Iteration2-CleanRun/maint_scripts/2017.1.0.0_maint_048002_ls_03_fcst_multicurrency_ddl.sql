/*
||============================================================================
|| Application: PowerPlant
|| File Name: maint_48002_02_ls_fcst_multicurrency_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By        Reason for Change
|| ---------- ---------- ----------------  --------------------------------------
|| 2017.1.0.0 2017-07-06 Andrew Hill       Set multicurrency options as non-nullable
||============================================================================
*/

ALTER TABLE ls_forecast_version MODIFY ls_currency_type_id NOT NULL;
ALTER TABLE ls_forecast_version MODIFY currency_id NOT NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3562, 0, 2017, 1, 0, 0, 48002, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048002_ls_03_fcst_multicurrency_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
