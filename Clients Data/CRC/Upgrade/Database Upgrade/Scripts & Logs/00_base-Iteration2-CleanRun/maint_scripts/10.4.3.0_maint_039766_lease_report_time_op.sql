/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039766_lease_report_time_op.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 08/25/2014 Ryan Oliveria
||============================================================================
*/

update PP_REPORTS_TIME_OPTION
   set DESCRIPTION = 'Lease SOB + Mnum Span',
       PARAMETER_UO_NAME = 'uo_ppbase_report_parms_dddw_mnum_span',
       DWNAME1 = 'dw_ls_set_of_books',
       LABEL1 = 'Set of Books',
       KEYCOLUMN1 = 'set_of_books_id',
       DWNAME2 = null,
       LABEL2 = null,
       KEYCOLUMN2 = null,
       DWNAME3 = null,
       LABEL3 = null,
       KEYCOLUMN3 = null
 where PP_REPORT_TIME_OPTION_ID = 201;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1367, 0, 10, 4, 3, 0, 39766, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039766_lease_report_time_op.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
