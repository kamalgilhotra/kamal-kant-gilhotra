/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_052955_lessee_01_gl_query_updates_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.1 01/11/2019  C Yura            Add query display settings for new fields
||============================================================================
*/

update pp_any_query_criteria_fields
set display_field = 'ilr_number',
display_table = 'ls_ilr',
data_field = 'ilr_number',
sort_col = 'ilr_number'
where id in (
select id From pp_any_query_criteria
where lower(trim(description)) =
'lease gl account obligation balances by asset (company currency)')
and lower(detail_field) = 'ilr_number';

update pp_any_query_criteria_fields
set display_field = 'ilr_number',
display_table = 'ls_ilr',
data_field = 'ilr_number',
sort_col = 'ilr_number'
where id in (
select id From pp_any_query_criteria
where lower(trim(description)) =
'lease gl account liability balances by asset (company currency)')
and lower(detail_field) = 'ilr_number';

update pp_any_query_criteria_fields
set display_field = 'leased_asset_number',
display_table = 'ls_asset',
data_field = 'leased_asset_number',
sort_col = 'leased_asset_number'
where id in (
select id From pp_any_query_criteria
where lower(trim(description)) =
'lease gl account liability balances by asset (company currency)')
and lower(detail_field) = 'leased_asset_number';

update pp_any_query_criteria_fields
set display_field = 'leased_asset_number',
display_table = 'ls_asset',
data_field = 'leased_asset_number',
sort_col = 'leased_asset_number'
where id in (
select id From pp_any_query_criteria
where lower(trim(description)) =
'lease gl account obligation balances by asset (company currency)')
and lower(detail_field) = 'leased_asset_number';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13882, 0, 2018, 1, 0, 1, 52955, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_052955_lessee_01_gl_query_updates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;


