/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_036854_cpr_new_month_process.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 09/11/2014 Kyle Peterson
||============================================================================
*/

insert into PP_PROCESSES
   (PROCESS_ID, DESCRIPTION, LONG_DESCRIPTION, EXECUTABLE_FILE, VERSION, ALLOW_CONCURRENT, SYSTEM_LOCK_ENABLED,
    SYSTEM_LOCK)
   (select max(PROCESS_ID) + 1,
           'CPR NEW MONTH',
           'Process on CPR Monthly Close screen to add a new month',
           'ssp_cpr_new_month.exe',
           '10.4.3',
           0,
           0,
           0
      from PP_PROCESSES);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1412, 0, 10, 4, 3, 0, 36854, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_036854_cpr_new_month_process.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
