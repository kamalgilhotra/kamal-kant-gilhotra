/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045587_reg_3_prov_drillback_views_ddl.sql
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.2.0 04/13/2016 Sarah Byers  	 Updated Tax Provision Pull
||============================================================================
*/

create or replace view reg_fcst_ta_def_drill_inc_view as (
select m.m_id m_id,
		 t.description m_item,
		 v.description ta_version,
		 cs.description company,
		 o.description oper_ind,
		 e.description taxing_entity,
		 ram.description reg_account,
		 v.year year,
		 m.gl_month gl_month,
		 m.beg_balance beg_balance,
		 m.end_balance end_balance,
		 m.amount_est amount_est, 
		 m.amount_act amount_act,
		 m.amount_calc amount_calc,
		 m.amount_manual amount_manual,
		 m.amount_adj amount_adj,
		 m.amount_alloc amount_alloc
  from company_setup cs, 
		 tax_accrual_m_item m, tax_accrual_entity_include_act i, 
		 tax_accrual_control c, tax_accrual_version v, tax_accrual_entity e, tax_accrual_m_master t, tax_accrual_oper_ind o,
		 reg_ta_component k, reg_acct_master ram, 
		 reg_ta_comp_member_map w, reg_ta_family_member_map x, reg_ta_version_control y,
		 tax_accrual_gl_month_def g,
		 cr_dd_required_filter hv, cr_dd_required_filter fc, cr_dd_required_filter mn
 where c.company_id = cs.company_id 
	and c.company_id = x.company_id 
	and c.oper_ind = x.oper_ind 
	and c.m_item_id = x.m_item_id 
	and x.historic_version_id = y.historic_version_id 
	and x.forecast_version_id = y.forecast_version_id 
	and c.ta_version_id = y.ta_version_id 
	and c.ta_version_id = v.ta_version_id 
	and x.historic_version_id = w.historic_version_id 
	and x.forecast_version_id = w.forecast_version_id 
	and w.reg_component_id = nvl(ram.reg_component_id,0) 
	and w.reg_family_id = nvl(ram.reg_family_id,0) 
	and w.reg_member_id = nvl(ram.reg_member_id,0) 
	and w.reg_member_id = x.reg_member_id 
	and w.reg_component_id = k.reg_component_id 
	and k.reg_component_id in (5, 6)
	and decode(k.federal, 1, 10, 20) = e.entity_juris_id 
	and e.entity_id = x.entity_id 
	and ram.reg_source_id = 6 
	and m.m_id = c.m_id 
	and c.entity_include_id = i.entity_include_id 
	and i.entity_id = x.entity_id
	and c.m_item_id = t.m_item_id
	and c.oper_ind = o.oper_ind 
	and c.ta_version_id = g.ta_version_id
	and m.gl_month = g.gl_month
	and to_char(v.year) = substr(mn.filter_string, 1, 4)
	and lpad(to_char(g.period), 2, '0') = substr(mn.filter_string, 5, 2)
	and upper(mn.column_name) = 'GL_MONTH'
	and cs.reg_company_id = to_number(fc.filter_string)
	and upper(fc.column_name) = 'REG_COMPANY_ID'
	and x.forecast_version_id = to_number(hv.filter_string)
	and upper(hv.column_name) = 'FORECAST_VERSION_ID'
	and x.historic_version_id = 0
	and ram.reg_acct_id in (
		select to_number(filter_string) from cr_dd_required_filter 
		 where upper(column_name) = 'REG_ACCT_ID')
);

create or replace view reg_fcst_ta_defer_drill_view as (
select v.description ta_version,
		 cs.description company,
		 m.description m_item,
		 n.description ta_norm_schema,
		 j.description je_type,
		 o.description oper_ind,
		 e.description taxing_entity,
		 l.gl_account gl_account,
		 ram.description reg_account,
		 v.year year,
		 l.gl_month gl_month,
		 l.beg_balance,
		 l.provision,
		 l.reversal,
		 l.transfer_in,
		 l.transfer_out,
		 l.adjustment,
		 l.end_balance,
		 l.other,
		 l.payments
  from company_setup cs, 
		 tax_accrual_subledger l, 
		 tax_accrual_control c, tax_accrual_version v, tax_accrual_entity e, tax_accrual_m_master m, tax_accrual_norm_schema n,
		 tax_accrual_je_type j, tax_accrual_oper_ind o,
		 reg_ta_component k, reg_acct_master ram, 
		 reg_ta_comp_member_map w, reg_ta_family_member_map x, reg_ta_version_control y,
		 tax_accrual_gl_month_def g,
		 cr_dd_required_filter hv, cr_dd_required_filter fc, cr_dd_required_filter mn
 where c.company_id = cs.company_id 
	and c.company_id = x.company_id 
	and c.oper_ind = x.oper_ind 
	and c.m_item_id = x.m_item_id 
	and x.historic_version_id = y.historic_version_id 
	and x.forecast_version_id = y.forecast_version_id 
	and c.ta_version_id = y.ta_version_id 
	and c.ta_version_id = v.ta_version_id 
	and x.historic_version_id = w.historic_version_id 
	and x.forecast_version_id = w.forecast_version_id 
	and w.reg_component_id = nvl(ram.reg_component_id,0) 
	and w.reg_family_id = nvl(ram.reg_family_id,0) 
	and w.reg_member_id = nvl(ram.reg_member_id,0) 
	and w.reg_member_id = x.reg_member_id 
	and w.reg_component_id = k.reg_component_id 
	and decode(k.federal, 1, 10, 20) = e.entity_juris_id 
	and (l.je_type_id = decode(k.reg_component_id,1,2,2,2,3,2,4,2,7,3,8,3,9,3,10,3,-1)
		  or
		  l.je_type_id = decode(k.load_offsets,1,4,-1))
	and e.entity_id = x.entity_id 
	and ram.reg_source_id = 6 
	and l.company_id = x.company_id
	and l.oper_ind = x.oper_ind 
	and l.m_item_id = x.m_item_id 
	and l.entity_id = x.entity_id 
	and l.ta_version_id = y.ta_version_id 
	and l.gl_account = x.gl_account 
	and l.je_type_id = j.je_type_id
	and l.m_item_id = m.m_item_id
	and l.ta_norm_id = n.ta_norm_id
	and l.oper_ind = o.oper_ind
	and l.ta_version_id = g.ta_version_id
	and l.gl_month = g.gl_month
	and to_char(v.year) = substr(mn.filter_string, 1, 4)
	and lpad(to_char(g.period), 2, '0') = substr(mn.filter_string, 5, 2)
	and upper(mn.column_name) = 'GL_MONTH'
	and cs.reg_company_id = to_number(fc.filter_string)
	and upper(fc.column_name) = 'REG_COMPANY_ID'
	and x.forecast_version_id = to_number(hv.filter_string)
	and upper(hv.column_name) = 'FORECAST_VERSION_ID'
	and x.historic_version_id = 0
	and ram.reg_acct_id in (
		select to_number(filter_string) from cr_dd_required_filter 
		 where upper(column_name) = 'REG_ACCT_ID')
);

create or replace view reg_hist_ta_def_drill_inc_view as (
select m.m_id m_id,
		 t.description m_item,
		 v.description ta_version,
		 cs.description company,
		 o.description oper_ind,
		 e.description taxing_entity,
		 ram.description reg_account,
		 v.year year,
		 m.gl_month gl_month,
		 m.beg_balance beg_balance,
		 m.end_balance end_balance,
		 m.amount_est amount_est, 
		 m.amount_act amount_act,
		 m.amount_calc amount_calc,
		 m.amount_manual amount_manual,
		 m.amount_adj amount_adj,
		 m.amount_alloc amount_alloc
  from company_setup cs, 
		 tax_accrual_m_item m, tax_accrual_entity_include_act i, 
		 tax_accrual_control c, tax_accrual_version v, tax_accrual_entity e, tax_accrual_m_master t, tax_accrual_oper_ind o,
		 reg_ta_component k, reg_acct_master ram, 
		 reg_ta_comp_member_map w, reg_ta_family_member_map x, reg_ta_version_control y,
		 tax_accrual_gl_month_def g,
		 cr_dd_required_filter hv, cr_dd_required_filter fc, cr_dd_required_filter mn
 where c.company_id = cs.company_id 
	and c.company_id = x.company_id 
	and c.oper_ind = x.oper_ind 
	and c.m_item_id = x.m_item_id 
	and x.historic_version_id = y.historic_version_id 
	and x.forecast_version_id = y.forecast_version_id 
	and c.ta_version_id = y.ta_version_id 
	and c.ta_version_id = v.ta_version_id 
	and x.historic_version_id = w.historic_version_id 
	and x.forecast_version_id = w.forecast_version_id 
	and w.reg_component_id = nvl(ram.reg_component_id,0) 
	and w.reg_family_id = nvl(ram.reg_family_id,0) 
	and w.reg_member_id = nvl(ram.reg_member_id,0) 
	and w.reg_member_id = x.reg_member_id 
	and w.reg_component_id = k.reg_component_id 
	and k.reg_component_id in (5, 6)
	and decode(k.federal, 1, 10, 20) = e.entity_juris_id 
	and e.entity_id = x.entity_id 
	and ram.reg_source_id = 6 
	and m.m_id = c.m_id 
	and c.entity_include_id = i.entity_include_id 
	and i.entity_id = x.entity_id
	and c.m_item_id = t.m_item_id
	and c.oper_ind = o.oper_ind 
	and c.ta_version_id = g.ta_version_id
	and m.gl_month = g.gl_month
	and to_char(v.year) = substr(mn.filter_string, 1, 4)
	and lpad(to_char(g.period), 2, '0') = substr(mn.filter_string, 5, 2)
	and upper(mn.column_name) = 'GL_MONTH'
	and cs.reg_company_id = to_number(fc.filter_string)
	and upper(fc.column_name) = 'REG_COMPANY_ID'
	and x.historic_version_id = to_number(hv.filter_string)
	and upper(hv.column_name) = 'HISTORIC_VERSION_ID'
	and x.forecast_version_id = 0
	and ram.reg_acct_id in (
		select to_number(filter_string) from cr_dd_required_filter 
		 where upper(column_name) = 'REG_ACCT_ID')
);

create or replace view reg_hist_ta_defer_drill_view as (
select v.description ta_version,
		 cs.description company,
		 m.description m_item,
		 n.description ta_norm_schema,
		 j.description je_type,
		 o.description oper_ind,
		 e.description taxing_entity,
		 l.gl_account gl_account,
		 ram.description reg_account,
		 v.year year,
		 l.gl_month gl_month,
		 l.beg_balance,
		 l.provision,
		 l.reversal,
		 l.transfer_in,
		 l.transfer_out,
		 l.adjustment,
		 l.end_balance,
		 l.other,
		 l.payments
  from company_setup cs, 
		 tax_accrual_subledger l, 
		 tax_accrual_control c, tax_accrual_version v, tax_accrual_entity e, tax_accrual_m_master m, tax_accrual_norm_schema n,
		 tax_accrual_je_type j, tax_accrual_oper_ind o,
		 reg_ta_component k, reg_acct_master ram, 
		 reg_ta_comp_member_map w, reg_ta_family_member_map x, reg_ta_version_control y,
		 tax_accrual_gl_month_def g,
		 cr_dd_required_filter hv, cr_dd_required_filter fc, cr_dd_required_filter mn
 where c.company_id = cs.company_id 
	and c.company_id = x.company_id 
	and c.oper_ind = x.oper_ind 
	and c.m_item_id = x.m_item_id 
	and x.historic_version_id = y.historic_version_id 
	and x.forecast_version_id = y.forecast_version_id 
	and c.ta_version_id = y.ta_version_id 
	and c.ta_version_id = v.ta_version_id 
	and x.historic_version_id = w.historic_version_id 
	and x.forecast_version_id = w.forecast_version_id 
	and w.reg_component_id = nvl(ram.reg_component_id,0) 
	and w.reg_family_id = nvl(ram.reg_family_id,0) 
	and w.reg_member_id = nvl(ram.reg_member_id,0) 
	and w.reg_member_id = x.reg_member_id 
	and w.reg_component_id = k.reg_component_id 
	and decode(k.federal, 1, 10, 20) = e.entity_juris_id 
	and (l.je_type_id = decode(k.reg_component_id,1,2,2,2,3,2,4,2,7,3,8,3,9,3,10,3,-1)
		  or
		  l.je_type_id = decode(k.load_offsets,1,4,-1))
	and e.entity_id = x.entity_id 
	and ram.reg_source_id = 6 
	and l.company_id = x.company_id
	and l.oper_ind = x.oper_ind 
	and l.m_item_id = x.m_item_id 
	and l.entity_id = x.entity_id 
	and l.ta_version_id = y.ta_version_id 
	and l.gl_account = x.gl_account 
	and l.je_type_id = j.je_type_id
	and l.m_item_id = m.m_item_id
	and l.ta_norm_id = n.ta_norm_id
	and l.oper_ind = o.oper_ind
	and l.ta_version_id = g.ta_version_id
	and l.gl_month = g.gl_month
	and to_char(v.year) = substr(mn.filter_string, 1, 4)
	and lpad(to_char(g.period), 2, '0') = substr(mn.filter_string, 5, 2)
	and upper(mn.column_name) = 'GL_MONTH'
	and cs.reg_company_id = to_number(fc.filter_string)
	and upper(fc.column_name) = 'REG_COMPANY_ID'
	and x.historic_version_id = to_number(hv.filter_string)
	and upper(hv.column_name) = 'HISTORIC_VERSION_ID'
	and x.forecast_version_id = 0
	and ram.reg_acct_id in (
		select to_number(filter_string) from cr_dd_required_filter 
		 where upper(column_name) = 'REG_ACCT_ID')
);

create or replace view REG_FCST_TA_OTHER_DRILL_VIEW
(
  M_ID,
  M_ITEM,
  TA_VERSION,
  COMPANY,
  OPER_IND,
  REG_ACCOUNT,
  REG_ACCT_ID,
  TAXING_ENTITY,
  ENTITY_ID,
  YEAR,
  GL_MONTH,
  BEG_BALANCE,
  END_BALANCE,
  AMOUNT_EST,
  AMOUNT_ACT,
  AMOUNT_CALC,
  AMOUNT_MANUAL,
  AMOUNT_ADJ,
  AMOUNT_ALLOC
) as
(
select M.M_ID          M_ID,
       T.DESCRIPTION   M_ITEM,
       V.DESCRIPTION   TA_VERSION,
       CS.DESCRIPTION  COMPANY,
       I.DESCRIPTION   OPER_IND,
       RAM.DESCRIPTION REG_ACCOUNT,
       RAM.REG_ACCT_ID REG_ACCT_ID,
       A.DESCRIPTION   TAXING_ENTITY,
       E.ENTITY_ID     ENTITY_ID,
       V.YEAR          year,
       M.GL_MONTH      GL_MONTH,
       M.BEG_BALANCE   BEG_BALANCE,
       M.END_BALANCE   END_BALANCE,
       M.AMOUNT_EST    AMOUNT_EST,
       M.AMOUNT_ACT    AMOUNT_ACT,
       M.AMOUNT_CALC   AMOUNT_CALC,
       M.AMOUNT_MANUAL AMOUNT_MANUAL,
       M.AMOUNT_ADJ    AMOUNT_ADJ,
       M.AMOUNT_ALLOC  AMOUNT_ALLOC
  from TAX_ACCRUAL_M_ITEM             M,
       TAX_ACCRUAL_CONTROL            C,
       TAX_ACCRUAL_VERSION            V,
       TAX_ACCRUAL_M_MASTER           T,
       TAX_ACCRUAL_ENTITY_INCLUDE_ACT E,
       TAX_ACCRUAL_OPER_IND           I,
       COMPANY_SETUP                  CS,
       REG_TA_CONTROL_MAP             X,
       REG_TA_VERSION_CONTROL         Y,
       REG_TA_M_TYPE_ENTITY_MAP       Z,
       REG_ACCT_MASTER                RAM,
       REG_COMPANY                    RC,
       TAX_ACCRUAL_ENTITY             A,
       CR_DD_REQUIRED_FILTER          FV,
       CR_DD_REQUIRED_FILTER          FC,
       CR_DD_REQUIRED_FILTER          MN,
		 tax_accrual_gl_month_def g
 where CS.COMPANY_ID = C.COMPANY_ID
   and M.M_ID = C.M_ID
   and C.TA_VERSION_ID = V.TA_VERSION_ID
   and X.COMPANY_ID = C.COMPANY_ID
   and X.OPER_IND = C.OPER_IND
   and X.M_ITEM_ID = C.M_ITEM_ID
   and X.FORECAST_VERSION_ID = Y.FORECAST_VERSION_ID
   and Y.TA_VERSION_ID = C.TA_VERSION_ID
   and X.FORECAST_VERSION_ID = Z.FORECAST_VERSION_ID
   and NVL(Z.TAX_CONTROL_ID, 0) <> 99
   and X.TAX_CONTROL_ID = Z.TAX_CONTROL_ID
   and E.ENTITY_ID = Z.ENTITY_ID
   and E.ENTITY_INCLUDE_ID = C.ENTITY_INCLUDE_ID
   and T.M_TYPE_ID = Z.M_TYPE_ID
   and T.M_ITEM_ID = C.M_ITEM_ID
   and C.OPER_IND = I.OPER_IND
   and E.ENTITY_ID = A.ENTITY_ID
   and X.REG_ACCT_ID = RAM.REG_ACCT_ID
	and c.ta_version_id = g.ta_version_id
	and m.gl_month = g.gl_month
   and TO_NUMBER(TO_CHAR(V.YEAR) || LPAD(TO_CHAR(g.period), 2, '0')) = TO_NUMBER(MN.FILTER_STRING)
   and UPPER(MN.COLUMN_NAME) = 'GL_MONTH'
   and RC.REG_COMPANY_ID = TO_NUMBER(FC.FILTER_STRING)
   and UPPER(FC.COLUMN_NAME) = 'REG_COMPANY_ID'
   and RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
   and X.FORECAST_VERSION_ID = TO_NUMBER(FV.FILTER_STRING)
   and UPPER(FV.COLUMN_NAME) = 'FORECAST_VERSION_ID'
   and RAM.REG_ACCT_ID in
       (select TO_NUMBER(FILTER_STRING) from CR_DD_REQUIRED_FILTER where UPPER(COLUMN_NAME) = 'REG_ACCT_ID')
);

create or replace view REG_HIST_TA_OTHER_DRILL_VIEW as (
select M.M_ID          M_ID,
       T.DESCRIPTION   M_ITEM,
       V.DESCRIPTION   TA_VERSION,
       CS.DESCRIPTION  COMPANY,
       I.DESCRIPTION   OPER_IND,
       RAM.DESCRIPTION REG_ACCOUNT,
       RAM.REG_ACCT_ID REG_ACCT_ID,
       A.DESCRIPTION   TAXING_ENTITY,
       E.ENTITY_ID     ENTITY_ID,
       V.YEAR          year,
       M.GL_MONTH      GL_MONTH,
       M.BEG_BALANCE   BEG_BALANCE,
       M.END_BALANCE   END_BALANCE,
       M.AMOUNT_EST    AMOUNT_EST,
       M.AMOUNT_ACT    AMOUNT_ACT,
       M.AMOUNT_CALC   AMOUNT_CALC,
       M.AMOUNT_MANUAL AMOUNT_MANUAL,
       M.AMOUNT_ADJ    AMOUNT_ADJ,
       M.AMOUNT_ALLOC  AMOUNT_ALLOC
  from TAX_ACCRUAL_M_ITEM             M,
       TAX_ACCRUAL_CONTROL            C,
       TAX_ACCRUAL_VERSION            V,
       TAX_ACCRUAL_M_MASTER           T,
       TAX_ACCRUAL_ENTITY_INCLUDE_ACT E,
       TAX_ACCRUAL_OPER_IND           I,
       COMPANY_SETUP                  CS,
       REG_TA_CONTROL_MAP             X,
       REG_TA_VERSION_CONTROL         Y,
       REG_TA_M_TYPE_ENTITY_MAP       Z,
       REG_ACCT_MASTER                RAM,
       REG_COMPANY                    RC,
       TAX_ACCRUAL_ENTITY             A,
       CR_DD_REQUIRED_FILTER          HV,
       CR_DD_REQUIRED_FILTER          FC,
       CR_DD_REQUIRED_FILTER          MN,
		 tax_accrual_gl_month_def g
 where CS.COMPANY_ID = C.COMPANY_ID
   and M.M_ID = C.M_ID
   and C.TA_VERSION_ID = V.TA_VERSION_ID
   and X.COMPANY_ID = C.COMPANY_ID
   and X.OPER_IND = C.OPER_IND
   and X.M_ITEM_ID = C.M_ITEM_ID
   and X.HISTORIC_VERSION_ID = Y.HISTORIC_VERSION_ID
   and Y.TA_VERSION_ID = C.TA_VERSION_ID
   and X.HISTORIC_VERSION_ID = Z.HISTORIC_VERSION_ID
   and NVL(Z.TAX_CONTROL_ID, 0) <> 99
   and X.TAX_CONTROL_ID = Z.TAX_CONTROL_ID
   and E.ENTITY_ID = Z.ENTITY_ID
   and E.ENTITY_INCLUDE_ID = C.ENTITY_INCLUDE_ID
   and T.M_TYPE_ID = Z.M_TYPE_ID
   and T.M_ITEM_ID = C.M_ITEM_ID
   and C.OPER_IND = I.OPER_IND
   and E.ENTITY_ID = A.ENTITY_ID
   and X.REG_ACCT_ID = RAM.REG_ACCT_ID
	and c.ta_version_id = g.ta_version_id
	and m.gl_month = g.gl_month
   and TO_NUMBER(TO_CHAR(V.YEAR) ||
                 LPAD(TO_CHAR(g.period), 2, '0')) =
       TO_NUMBER(MN.FILTER_STRING)
   and UPPER(MN.COLUMN_NAME) = 'GL_MONTH'
   and RC.REG_COMPANY_ID = TO_NUMBER(FC.FILTER_STRING)
   and UPPER(FC.COLUMN_NAME) = 'REG_COMPANY_ID'
   and RC.REG_COMPANY_ID = CS.REG_COMPANY_ID
   and X.HISTORIC_VERSION_ID = TO_NUMBER(HV.FILTER_STRING)
   and UPPER(HV.COLUMN_NAME) = 'HISTORIC_VERSION_ID'
   and RAM.REG_ACCT_ID in (select TO_NUMBER(FILTER_STRING)
                             from CR_DD_REQUIRED_FILTER
                            where UPPER(COLUMN_NAME) = 'REG_ACCT_ID'));
							
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3149, 0, 2015, 2, 2, 0, 45587, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045587_reg_3_prov_drillback_views_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;