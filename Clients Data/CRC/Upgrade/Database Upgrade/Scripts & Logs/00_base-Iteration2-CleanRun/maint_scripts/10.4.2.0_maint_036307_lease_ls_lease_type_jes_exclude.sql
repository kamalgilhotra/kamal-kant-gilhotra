/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036307_lease_ls_lease_type_jes_exclude.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 02/12/2014 Brandon Beck
||============================================================================
*/

create table LS_LEASE_TYPE_JES_EXCLUDE
(
 lease_cap_type_id number(22,0),
 trans_type number(22,0),
 company_id number(22,0),
 user_id varchar2(18),
 time_stamp date
);

alter table LS_LEASE_TYPE_JES_EXCLUDE
   add constraint LS_LEASE_TYPE_JES_EX_PK
       primary key (LEASE_CAP_TYPE_ID, TRANS_TYPE, COMPANY_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_LEASE_TYPE_JES_EXCLUDE
   add constraint LS_LEASE_TYPE_JES_EX_FK1
       foreign key (LEASE_CAP_TYPE_ID)
       references LS_LEASE_CAP_TYPE (LS_LEASE_CAP_TYPE_ID);

alter table LS_LEASE_TYPE_JES_EXCLUDE
   add constraint LS_LEASE_TYPE_JES_EX_FK2
       foreign key (TRANS_TYPE)
       references JE_TRANS_TYPE (TRANS_TYPE);

alter table LS_LEASE_TYPE_JES_EXCLUDE
   add constraint LS_LEASE_TYPE_JES_EX_FK3
       foreign key (COMPANY_ID)
       references COMPANY_SETUP (COMPANY_ID);

comment on table LS_LEASE_TYPE_JES_EXCLUDE is 'A table to hold what JEs to ignore for certain lease cap types.  If a record is found in this table, then it will flag the account as IGNORE when loading gl_transaction';
comment on column LS_LEASE_TYPE_JES_EXCLUDE.LEASE_CAP_TYPE_ID is 'A field referencing the Lease Cap Type to exclude from JE creation';
comment on column LS_LEASE_TYPE_JES_EXCLUDE.TRANS_TYPE is 'A field referencing the Trans Type to exclude from JE creation';
comment on column LS_LEASE_TYPE_JES_EXCLUDE.LEASE_CAP_TYPE_ID is 'A field referencing the Company to exclude from JE creation';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (959, 0, 10, 4, 2, 0, 36307, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036307_lease_ls_lease_type_jes_exclude.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;