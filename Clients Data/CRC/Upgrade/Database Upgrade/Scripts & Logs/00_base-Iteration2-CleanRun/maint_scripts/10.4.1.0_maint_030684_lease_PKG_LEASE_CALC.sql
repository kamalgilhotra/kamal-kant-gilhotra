/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030684_lease_PKG_LEASE_CALC.sql
|| Description: Updates to allow revisions on MLA's
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 Ryan Oliveria   Point Release
||============================================================================
*/

create or replace package PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   || 10.4.1.0 07/10/2013 Ryan Oliveria  MLA functions (workflows and revisions)
   ||============================================================================
   */

   procedure P_SET_ILR_ID(A_ILR_ID number);

   function F_APPROVE_MLA(A_LEASE_ID number,
                          A_REVISION number) return number;

   function F_REJECT_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_SEND_MLA(A_LEASE_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_MLA(A_LEASE_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_MLA(A_LEASE_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID number,
                                  A_REVISION number) return number;

   function F_APPROVE_ILR(A_ILR_ID number,
                          A_REVISION number) return number;

   function F_REJECT_ILR(A_ILR_ID number,
                         A_REVISION number) return number;

   function F_SEND_ILR(A_ILR_ID number,
                       A_REVISION number) return number;

   function F_UNREJECT_ILR(A_ILR_ID number,
                           A_REVISION number) return number;

   function F_UNSEND_ILR(A_ILR_ID number,
                         A_REVISION number) return number;

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID number,
                                  A_REVISION number) return number;

   function F_GET_ILR_ID return number;

end PKG_LEASE_CALC;
/

create or replace package body PKG_LEASE_CALC as
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_CALC
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By     Reason for Change
   || -------- ---------- -------------- ----------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck         Original Version
   ||============================================================================
   */

   L_ILR_ID number;

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            P_SET_ILR_ID
   --**************************************************************************

   procedure P_SET_ILR_ID(A_ILR_ID number) is

   begin
      L_ILR_ID := A_ILR_ID;
   end P_SET_ILR_ID;

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************

   --**************************************************************************
   --                            F_APPROVE_MLA
   --**************************************************************************

   function F_APPROVE_MLA(A_LEASE_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      --Reject other revisions
      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 4,
        APPROVAL_DATE = sysdate,
        APPROVER = user
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      update LS_LEASE L
         set LEASE_STATUS_ID = 3,
        APPROVAL_DATE = sysdate
       where LEASE_ID = A_LEASE_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_APPROVE_MLA;



   --**************************************************************************
   --                            F_REJECT_MLA
   --**************************************************************************

   function F_REJECT_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 4
       where LEASE_ID = A_LEASE_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_REJECT_MLA;



   --**************************************************************************
   --                            F_SEND_MLA
   --**************************************************************************

   function F_SEND_MLA(A_LEASE_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LS_LEASE_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 2
       where L.LEASE_ID = A_LEASE_ID
      --and nvl(l.revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_SEND_MLA;



   --**************************************************************************
   --                            F_UNREJECT_MLA
   --**************************************************************************

   function F_UNREJECT_MLA(A_LEASE_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2,
        APPROVAL_STATUS_ID = 7
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 2
       where L.LEASE_ID = A_LEASE_ID
      --and nvl(l.revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_UNREJECT_MLA;



   --**************************************************************************
   --                            F_UNSEND_MLA
   --**************************************************************************

   function F_UNSEND_MLA(A_LEASE_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_LEASE_APPROVAL
         set REJECTED = 2,
        APPROVAL_STATUS_ID = 1
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_LEASE L
         set LEASE_STATUS_ID = 1,
        APPROVAL_DATE = null
       where LEASE_ID = A_LEASE_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_UNSEND_MLA;



   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_MLA
   --**************************************************************************

   function F_UPDATE_WORKFLOW_MLA(A_LEASE_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_LEASE_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_LEASE_ID)
                                        and NVL(ID_FIELD2, '0') = TO_CHAR(NVL(A_REVISION, '0'))),
                                     0)
       where LEASE_ID = A_LEASE_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   end F_UPDATE_WORKFLOW_MLA;

   --**************************************************************************
   --                            F_APPROVE_ILR
   --**************************************************************************

   function F_APPROVE_ILR(A_ILR_ID in number,
                          A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      --Approve this revision
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 3, APPROVAL_DATE = sysdate, APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      --Reject other revisions
      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 4,
        APPROVAL_DATE = sysdate,
        APPROVER = user
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) <> NVL(A_REVISION, 0)
         and APPROVAL_STATUS_ID in (1, 2);

      update LS_ILR L
         set ILR_STATUS_ID = 3
       where ILR_ID = A_ILR_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_APPROVE_ILR;

   --**************************************************************************
   --                            F_REJECT_ILR
   --**************************************************************************

   function F_REJECT_ILR(A_ILR_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_ILR_APPROVAL
         set REJECTED = 1, APPROVAL_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 4
       where ILR_ID = A_ILR_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_REJECT_ILR;


   --**************************************************************************
   --                            F_SEND_ILR
   --**************************************************************************

   function F_SEND_ILR(A_ILR_ID in number,
                       A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin

      update LS_ILR_APPROVAL
         set APPROVAL_STATUS_ID = 2
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 2
       where L.ILR_ID = A_ILR_ID
      --and nvl(l.revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_SEND_ILR;

   --**************************************************************************
   --                            F_UNREJECT_ILR
   --**************************************************************************

   function F_UNREJECT_ILR(A_ILR_ID in number,
                           A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_ILR_APPROVAL
         set REJECTED = 2,
        APPROVAL_STATUS_ID = 7
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 2
       where L.ILR_ID = A_ILR_ID
      --and nvl(l.revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_UNREJECT_ILR;

   --**************************************************************************
   --                            F_UNSEND_ILR
   --**************************************************************************

   function F_UNSEND_ILR(A_ILR_ID in number,
                         A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

      RTN number;

   begin
      update LS_ILR_APPROVAL
         set REJECTED = 2,
        APPROVAL_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      update LS_ILR L
         set ILR_STATUS_ID = 1
       where ILR_ID = A_ILR_ID
      --and nvl(revision,0) = nvl(A_REVISION,0)
      ;

      commit;

      return 1;
   end F_UNSEND_ILR;

   --**************************************************************************
   --                            F_UPDATE_WORKFLOW_ILR
   --**************************************************************************

   function F_UPDATE_WORKFLOW_ILR(A_ILR_ID in number,
                                  A_REVISION in number) return number is
      pragma autonomous_transaction; --Needed to be called from SQL

   begin
      update LS_ILR_APPROVAL
         set APPROVAL_TYPE_ID = NVL((select WORKFLOW_TYPE_ID
                                       from WORKFLOW
                                      where ID_FIELD1 = TO_CHAR(A_ILR_ID)
                                        and NVL(ID_FIELD2, '0') = TO_CHAR(NVL(A_REVISION, '0'))),
                                     0)
       where ILR_ID = A_ILR_ID
         and NVL(REVISION, 0) = NVL(A_REVISION, 0);

      commit;

      return 1;
   end F_UPDATE_WORKFLOW_ILR;

   --**************************************************************************
   --                            F_GET_ILR_ID
   --**************************************************************************

   function F_GET_ILR_ID return number is

   begin
      return L_ILR_ID;
   end F_GET_ILR_ID;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

begin
   L_ILR_ID := 0;

end PKG_LEASE_CALC;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (467, 0, 10, 4, 1, 0, 30684, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030684_lease_PKG_LEASE_CALC.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
