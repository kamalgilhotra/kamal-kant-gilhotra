/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045559_reg_schedule_enhance_010_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 04/04/2016  Shane Ward		Improve Reg Schedule Builder
||============================================================================
*/

-- $$$ KKG, 20190710 | Commenting drop statement, as the trigger does not exist.
-- drop trigger reg_schedule_Sql;

--Rename old Reg_schedule_Sql to reg_schedule_definition
ALTER TABLE reg_schedule_sql RENAME TO reg_schedule_definition;

--Create new reg_schedule_Sql
CREATE TABLE reg_schedule_sql (
sql_id NUMBER(22,0) NOT NULL,
description VARCHAR2(70) NOT NULL,
sql_string VARCHAR2(4000) null,
sql_type NUMBER(22,0) NOT NULL,
user_id               VARCHAR2(18)  NULL,
time_stamp            DATE          NULL);

ALTER TABLE reg_schedule_sql
  ADD CONSTRAINT pk_reg_sched_sql PRIMARY KEY (
    sql_id
  )
  USING INDEX
    TABLESPACE pwrplant_idx;


COMMENT ON TABLE reg_schedule_sql IS 'Houses Regulatory Schedule Builder SQL for creating reports';
COMMENT ON COLUMN reg_schedule_sql.sql_id IS 'System Assigned Identifier of Schedule SQL';
COMMENT ON COLUMN reg_schedule_sql.description IS 'Name or descriptin of Query';
COMMENT ON COLUMN reg_schedule_sql.sql_type IS 'Type of Sql query being run. 1 = Year End, 2 = Monthly, 3 = Allocations Results, 4 = Adjustments, 5 = Statistical';
COMMENT ON COLUMN reg_schedule_sql.user_id IS 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
COMMENT ON COLUMN reg_schedule_sql.time_stamp IS 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

--Sql Args
create table reg_schedule_sql_args (
sql_id number(22,0) not NULL,
description VARCHAR2(35) NOT NULL,
arg VARCHAR2(15) null,
user_id               VARCHAR2(18)  NULL,
time_stamp            DATE          NULL);

ALTER TABLE reg_schedule_sql_args
  ADD CONSTRAINT pk_reg_sched_sql_args PRIMARY KEY (
    sql_id,
    description
  )
  USING INDEX
    TABLESPACE pwrplant_idx;

COMMENT ON TABLE reg_schedule_sql_args IS 'Holds arguments for Schedule SQL to be populated at run time or during Column definition.';
COMMENT ON COLUMN reg_schedule_sql_args.sql_id IS 'SQL ID the arg belongs to';
COMMENT ON COLUMN reg_schedule_sql_args.description IS 'Name or description of the Variable needed to process query (ex. Month Number, Allocation Target)';
COMMENT ON COLUMN reg_schedule_sql_args.arg IS 'Gator enclosed string of the arg (ex. <month_number>, <alloc_target>) should correspond to arguments in reg_schedule_sql.sql_string)';
COMMENT ON COLUMN reg_schedule_sql_args.user_id IS 'STANDARD SYSTEM-ASSIGNED USER ID USED FOR AUDIT PURPOSES.';
COMMENT ON COLUMN reg_schedule_sql_args.time_stamp IS 'STANDARD SYSTEM-ASSIGNED TIMESTAMP USED FOR AUDIT PURPOSES.';

--Clean up from initial implementation
ALTER TABLE reg_schedule_column DROP COLUMN col_sql;

ALTER TABLE reg_schedule_column ADD sql_id NUMBER(22,0);
COMMENT ON COLUMN reg_schedule_column.sql_id IS 'System assigned Identifier of Pre-canned SQL.';

ALTER TABLE reg_schedule_column RENAME COLUMN col_filter_string TO col_string;

COMMENT ON COLUMN reg_schedule_definition.schedule_type_id IS 'Schedule Type of Schedule (ex. 1=standard leverages the reg schedule configuration, 2=Any Query leverages saved any queries)';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3130, 0, 2016, 1, 0, 0, 45559, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045559_reg_schedule_enhance_010_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;