/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_038706_system_ALTER_TABLE.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------------------
|| 10.4.2.2 06/20/2014 Roger Roach    The SQL statement to add foreign keys is misspelled
||========================================================================================
*/

create or replace function ALTER_TABLE(TABLE_NAME in varchar2,
                                       ALTER_TYPE in varchar2,
                                       CONST      varchar2) return number as
   /* Vers 1.3 6/22/05 */
   /* Vers 1.3.7 10/10/2012 constraint spelling incorrect and foreign key format was wrong */
   /* Vers 1.3.8 6/20/2014 The SQL statement to add foreign keys is misspelled */
   SQL_STMT varchar2(4000);
begin
   if ALTER_TYPE = 'drop constraint' then
      SQL_STMT := 'alter table ' || TABLE_NAME || ' drop constraint ' || CONST;
   elsif ALTER_TYPE = 'add constraint' then
      SQL_STMT := 'alter table ' || TABLE_NAME || ' add foreign key ( ' || CONST;
   elsif ALTER_TYPE = 'add primary key' then
      SQL_STMT := 'alter table ' || TABLE_NAME || ' add ( primary key  ' || CONST || ')';
   else
      return -1;
   end if;
   execute immediate SQL_STMT;
   return 0;
if alter_type = 'drop constraint' then
   sql_stmt  := 'alter table ' || table_name || ' drop constraint ' || const;
elsif alter_type = 'add constraint' then
   sql_stmt  := 'alter table ' || table_name || ' add foreign key ( ' || const;
elsif alter_type = 'add primary key' then
   sql_stmt  := 'alter table ' || table_name || ' add ( primary key  ' || const || ')';
else
   return -1;
end if;
execute immediate sql_stmt;
return 0;
if alter_type = 'drop constraint' then
   sql_stmt  := 'alter table ' || table_name || ' drop constraint ' || const;
elsif alter_type = 'add constraint' then
   sql_stmt  := 'alter table ' || table_name || ' add foreign key ( ' || const || ')';
elsif alter_type = 'add primary key' then
   sql_stmt  := 'alter table ' || table_name || ' add ( primary key  ' || const || ')';
else
   return -1;
end if;
execute immediate sql_stmt;
return 0;
exception
   when others then
      RAISE_APPLICATION_ERROR(-20000,
                              'Failed Alter Table ' || TABLE_NAME || ' Type ' || ALTER_TYPE || 'Constraint ' || CONST ||
                              ' Error: ' || sqlerrm);
      return -1;

end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1225, 0, 10, 4, 2, 2, 38706, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038706_system_ALTER_TABLE.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
