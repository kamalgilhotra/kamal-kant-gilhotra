/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030547_system_PKG_PP_COMMON.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Daniel Motter  Point release
||============================================================================
*/

create or replace package PKG_PP_COMMON as

   type NUM_TABTYPE is table of pls_integer index by binary_integer;
   type DATE_TABTYPE is table of date index by binary_integer;

   TYPE pp_system_control_arr IS TABLE OF pp_system_control.control_value%TYPE 
                                 INDEX BY pp_system_control.control_name%TYPE;
                                  
   system_controls  pp_system_control_arr;

   G_USER_ID varchar2(30) := user;

   subtype G_YES_NO_TYPE is char(1) not null;
   G_YES constant G_YES_NO_TYPE := 'Y';
   G_NO  constant G_YES_NO_TYPE := 'N';

   subtype G_TRUE_FALSE_TYPE is pls_integer range 0 .. 1 not null;
   G_TRUE  constant G_TRUE_FALSE_TYPE := 1;
   G_FALSE constant G_TRUE_FALSE_TYPE := 0;

   subtype G_RESULT_TYPE is pls_integer range 1 .. 3 not null;
   G_SUCCESS constant G_RESULT_TYPE := 1;
   G_FAIL    constant G_RESULT_TYPE := 2;
   G_WARNING constant G_RESULT_TYPE := 3;

   procedure P_INITIALIZE_SYSTEM_CONTROLS;

   function F_CLEAN_STRING(P_IN_STRING in varchar2) return varchar2;

end PKG_PP_COMMON;
/

create or replace package body PKG_PP_COMMON as

   /*******************Initialize system controils ***************************/
   procedure P_INITIALIZE_SYSTEM_CONTROLS is

   begin
      if SYSTEM_CONTROLS.COUNT > 0 then
         SYSTEM_CONTROLS.DELETE;
      end if;

      for REC in (select CONTROL_NAME, CONTROL_VALUE from PP_SYSTEM_CONTROL)
      loop
         SYSTEM_CONTROLS(trim(UPPER(REC.CONTROL_NAME))) := REC.CONTROL_VALUE;
      end loop;
   end P_INITIALIZE_SYSTEM_CONTROLS;

   /********** Clean_String ***************************************************/
   function F_CLEAN_STRING(P_IN_STRING in varchar2) return varchar2 as

      V_BAD_CHARS varchar2(200) := '/ -';

   begin

      return TRANSLATE(P_IN_STRING, V_BAD_CHARS, LPAD('_', LENGTH(V_BAD_CHARS), '_'));

   end F_CLEAN_STRING;

/****************** Initialization ********************************************/
begin
   G_USER_ID := user;

   P_INITIALIZE_SYSTEM_CONTROLS;

end PKG_PP_COMMON;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (444, 0, 10, 4, 1, 0, 30547, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030547_system_PKG_PP_COMMON.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
