/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048886_lessor_03_add_lessor_cap_types_menu_item_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/21/2017 Jared Watkins    Create the menu item and workspace for Lessor Cap Types
||============================================================================
*/

--remove the records for the old workspace
delete from ppbase_menu_items 
where module = 'LESSOR'
and menu_identifier = 'admin_set_of_books_fasb_type';

delete from ppbase_workspace
where module = 'LESSOR'
and workspace_identifier = 'admin_set_of_books_fasb_type';

--add the new workspace entry
insert into ppbase_workspace(module, workspace_identifier, label, workspace_uo_name, object_type_id)
values('LESSOR', 'admin_cap_types', 'Lease Cap Types', 'uo_lsr_admincntr_fasb_cap_types_wksp', 1);

--push down all the menu items below Receivable Buckets to make room for the new one
update ppbase_menu_items set item_order = item_order + 1
where module = 'LESSOR'
and menu_level = 2
and parent_menu_identifier = 'menu_wksp_admin'
and item_order > 4; 

--add the new Lease Cap Types menu item
insert into ppbase_menu_items(module, menu_identifier, menu_level, item_order, label, parent_menu_identifier, workspace_identifier, enable_yn)
values ('LESSOR', 'admin_cap_types', 2, 5, 'Lease Cap Types', 'menu_wksp_admin', 'admin_cap_types', 1);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3732, 0, 2017, 1, 0, 0, 48886, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048886_lessor_03_add_lessor_cap_types_menu_item_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;