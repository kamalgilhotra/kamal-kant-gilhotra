/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_041258_cwip_fast101replaceamt_ddl.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2   5/7/2015   Sunjin Cone         Add functionality for Replacement Amount
||============================================================================
*/

alter table UNITIZE_ALLOC_EST_STG_TEMP add REPLACEMENT_AMOUNT number (22,2);
alter table UNITIZE_ELIG_UNITS_TEMP add REPLACEMENT_AMOUNT number (22,2);
alter table UNITIZE_ELIG_UNITS_ARC add REPLACEMENT_AMOUNT number (22,2);

comment on column UNITIZE_ALLOC_EST_STG_TEMP.REPLACEMENT_AMOUNT is 'The Replacement Amount from WO_ESTIMATE.';
comment on column UNITIZE_ELIG_UNITS_TEMP.REPLACEMENT_AMOUNT is 'The Replacement Amount from UNITIZED_WORK_ORDER (from WO_ESTIMATE).';
comment on column UNITIZE_ELIG_UNITS_ARC.REPLACEMENT_AMOUNT is 'The Replacement Amount saved from UNITIZE_ELIG_UNITS_TEMP.'; 

alter table UNITIZE_WO_LIST_TEMP add REPLACEMENT_PERCENTAGE number (22,0);
alter table UNITIZE_WO_LIST_ARC add REPLACEMENT_PERCENTAGE number (22,0);

comment on column UNITIZE_WO_LIST_TEMP.REPLACEMENT_PERCENTAGE is '1 = Yes, the Replacement Percentage found on WORK_ORDER_TYPE makes the WO_ESTIMATE.REPLACEMENT_AMOUNT be treated as a percentage value, not a dollar amount';
comment on column UNITIZE_WO_LIST_ARC.REPLACEMENT_PERCENTAGE is '1 = Yes, the Replacement Percentage saved from UNITIZE_WO_LIST_TEMP, which indicates the WO_ESTIMATE.REPLACEMENT_AMOUNT is treated as a percentage value, not a dollar amount';

alter table UNITIZE_WO_LIST_TEMP add REPLACE_AMT_AS_UNIT number (22,0);
alter table UNITIZE_WO_LIST_ARC add REPLACE_AMT_AS_UNIT number (22,0);

comment on column UNITIZE_WO_LIST_TEMP.REPLACE_AMT_AS_UNIT is '1 = Yes, WO Estimates exist with Replacement Amount treated as a Percentage value to be included for Unit Item creation as a unique attribute like SERIAL_NUMBER.';
comment on column UNITIZE_WO_LIST_ARC.REPLACE_AMT_AS_UNIT is '1 = Yes, the Replace Amt As Unit saved from UNITIZE_WO_LIST_TEMP, which indicates WO Estimates exist with Replacement Amount treated as a Percentage value to be included for Unit Item creation as a unique attribute like SERIAL_NUMBER.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2559, 0, 2015, 2, 0, 0, 41258, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_041258_cwip_fast101replaceamt_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;