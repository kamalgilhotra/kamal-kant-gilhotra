/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032953_import_buckets.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Kyle Peterson  Patch Release
||============================================================================
*/

alter table LS_IMPORT_ILR
   add (C_BUCKET_1  number(22,2),
        C_BUCKET_2  number(22,2),
        C_BUCKET_3  number(22,2),
        C_BUCKET_4  number(22,2),
        C_BUCKET_5  number(22,2),
        C_BUCKET_6  number(22,2),
        C_BUCKET_7  number(22,2),
        C_BUCKET_8  number(22,2),
        C_BUCKET_9  number(22,2),
        C_BUCKET_10 number(22,2),
        E_BUCKET_1  number(22,2),
        E_BUCKET_2  number(22,2),
        E_BUCKET_3  number(22,2),
        E_BUCKET_4  number(22,2),
        E_BUCKET_5  number(22,2),
        E_BUCKET_6  number(22,2),
        E_BUCKET_7  number(22,2),
        E_BUCKET_8  number(22,2),
        E_BUCKET_9  number(22,2),
        E_BUCKET_10 number(22,2));

comment on column ls_import_ilr.c_bucket_1 is 'A column holding the contingent amount for bucket 1';
comment on column ls_import_ilr.c_bucket_2 is 'A column holding the contingent amount for bucket 2';
comment on column ls_import_ilr.c_bucket_3 is 'A column holding the contingent amount for bucket 3';
comment on column ls_import_ilr.c_bucket_4 is 'A column holding the contingent amount for bucket 4';
comment on column ls_import_ilr.c_bucket_5 is 'A column holding the contingent amount for bucket 5';
comment on column ls_import_ilr.c_bucket_6 is 'A column holding the contingent amount for bucket 6';
comment on column ls_import_ilr.c_bucket_7 is 'A column holding the contingent amount for bucket 7';
comment on column ls_import_ilr.c_bucket_8 is 'A column holding the contingent amount for bucket 8';
comment on column ls_import_ilr.c_bucket_9 is 'A column holding the contingent amount for bucket 9';
comment on column ls_import_ilr.c_bucket_10 is 'A column holding the contingent amount for bucket 10';

comment on column ls_import_ilr.e_bucket_1 is 'A column holding the executory amount for bucket 1';
comment on column ls_import_ilr.e_bucket_2 is 'A column holding the executory amount for bucket 2';
comment on column ls_import_ilr.e_bucket_3 is 'A column holding the executory amount for bucket 3';
comment on column ls_import_ilr.e_bucket_4 is 'A column holding the executory amount for bucket 4';
comment on column ls_import_ilr.e_bucket_5 is 'A column holding the executory amount for bucket 5';
comment on column ls_import_ilr.e_bucket_6 is 'A column holding the executory amount for bucket 6';
comment on column ls_import_ilr.e_bucket_7 is 'A column holding the executory amount for bucket 7';
comment on column ls_import_ilr.e_bucket_8 is 'A column holding the executory amount for bucket 8';
comment on column ls_import_ilr.e_bucket_9 is 'A column holding the executory amount for bucket 9';
comment on column ls_import_ilr.e_bucket_10 is 'A column holding the executory amount for bucket 10';



insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_1',
           'Contingent bucket 1',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_2',
           'Contingent bucket 2',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_3',
           'Contingent bucket 3',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_4',
           'Contingent bucket 4',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_5',
           'Contingent bucket 5',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_6',
           'Contingent bucket 6',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_7',
           'Contingent bucket 7',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_8',
           'Contingent bucket 8',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_9',
           'Contingent bucket 9',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'c_bucket_10',
           'Contingent bucket 10',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_1',
           'Executory bucket 1',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_2',
           'Executory bucket 2',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_3',
           'Executory bucket 3',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_4',
           'Executory bucket 4',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_5',
           'Executory bucket 5',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_6',
           'Executory bucket 6',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_7',
           'Executory bucket 7',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_8',
           'Executory bucket 8',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_9',
           'Executory bucket 9',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);
insert into PP_IMPORT_COLUMN
   (IMPORT_TYPE_ID, COLUMN_NAME, DESCRIPTION, IMPORT_COLUMN_NAME, IS_REQUIRED, PROCESSING_ORDER,
    COLUMN_TYPE, PARENT_TABLE, IS_ON_TABLE)
   (select (select IMPORT_TYPE_ID from PP_IMPORT_TYPE where DESCRIPTION like 'Add: ILR%'),
           'e_bucket_10',
           'Executory bucket 10',
           null,
           0,
           1,
           'number(22,2)',
           null,
           1
      from DUAL);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (646, 0, 10, 4, 1, 1, 32953, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032953_import_buckets.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
