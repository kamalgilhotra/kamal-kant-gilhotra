/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032536_lease_tax_front_end.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/09/2013 Kyle Peterson  Point Release
||============================================================================
*/

create table LS_TAX_SUMMARY
(
 TAX_SUMMARY_ID number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(35),
 PAY_LESSOR     number(1,0) default 0
);

alter table LS_TAX_SUMMARY
   add constraint PK_LS_TAX_SUMMARY
       primary key (TAX_SUMMARY_ID)
       using index tablespace PWRPLANT_IDX;

create sequence LS_TAX_SUMMARY_SEQ;

comment on table LS_TAX_SUMMARY is '(S) [06] The ls tax summary table contains summary level information for taxes for leased assets.';
comment on column LS_TAX_SUMMARY."TAX_SUMMARY_ID" is 'The internal tax summary id within PowerPlant.';
comment on column LS_TAX_SUMMARY."DESCRIPTION" is 'A description for the tax summary.';
comment on column LS_TAX_SUMMARY."PAY_LESSOR" is 'An indicator for whether or not taxes under this summary tax are paid to the lessor or directly to the government.';

create table LS_TAX_LOCAL
(
 TAX_LOCAL_ID   number(22,0) not null,
 TAX_SUMMARY_ID number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 DESCRIPTION    varchar2(35)
);

alter table LS_TAX_LOCAL
   add constraint PK_LS_TAX_LOCAL
       primary key (TAX_LOCAL_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_TAX_LOCAL
   add constraint FK_LS_TAX_LOCAL
       foreign key (TAX_SUMMARY_ID)
       references LS_TAX_SUMMARY(TAX_SUMMARY_ID);

create sequence LS_TAX_LOCAL_SEQ;

comment on table LS_TAX_LOCAL is '(S) [06] The ls tax local table contains local tax level information.';
comment on column LS_TAX_LOCAL."TAX_LOCAL_ID" is 'The internal local tax id within PowerPlant.';
comment on column LS_TAX_LOCAL."TAX_SUMMARY_ID" is 'The internal summary tax id within PowerPlant.';
comment on column LS_TAX_LOCAL."DESCRIPTION" is 'A description of the local tax.';

create table LS_TAX_STATE_RATES
(
 TAX_LOCAL_ID   number(22,0) not null,
 STATE_ID       char(18) not null,
 COMPANY_ID     number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 RATE           number(22,8),
 EFFECTIVE_DATE date not null
);

alter table LS_TAX_STATE_RATES
   add constraint PK_LS_TAX_STATE_RATES
       primary key (TAX_LOCAL_ID, STATE_ID, COMPANY_ID, EFFECTIVE_DATE)
       using index tablespace PWRPLANT_IDX;

alter table LS_TAX_STATE_RATES
   add constraint FK_LS_TAX_STATE_RATES1
       foreign key (TAX_LOCAL_ID)
       references LS_TAX_LOCAL(TAX_LOCAL_ID);

alter table LS_TAX_STATE_RATES
   add constraint FK_LS_TAX_STATE_RATES2
       foreign key (STATE_ID)
       references STATE(STATE_ID);

alter table LS_TAX_STATE_RATES
   add constraint FK_LS_TAX_STATE_RATES3
       foreign key (COMPANY_ID)
       references COMPANY_SETUP(COMPANY_ID);

comment on table LS_TAX_STATE_RATES is '(S) [06] The ls tax state rates table contains the tax rates for states.';
comment on column LS_TAX_STATE_RATES."TAX_LOCAL_ID" is 'The internal local tax id within PowerPlant.';
comment on column LS_TAX_STATE_RATES."STATE_ID" is 'The internal state id within PowerPlant.';
comment on column LS_TAX_STATE_RATES."COMPANY_ID" is 'The internal company id within PowerPlant.';
comment on column LS_TAX_STATE_RATES."RATE" is 'The rate for this local tax on the effective date for this company-state combination.';
comment on column LS_TAX_STATE_RATES."EFFECTIVE_DATE" is 'The date on which the rate for this tax becomes active.';

create table TAX_DISTRICT
(
 TAX_DISTRICT_ID      number(22,0) not null,
 COUNTY_ID            char(18),
 STATE_ID             char(18),
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 ASSIGNMENT_INDICATOR number(22,0),
 DESCRIPTION          varchar2(100) not null,
 LONG_DESCRIPTION     varchar2(254),
 NOTES                varchar2(4000)
);

alter table TAX_DISTRICT
   add constraint PK_TAX_DISTRICT
       primary key (TAX_DISTRICT_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_DISTRICT
   add constraint FK_TAX_DISTRICT1
       foreign key (COUNTY_ID, STATE_ID)
       references COUNTY(COUNTY_ID, STATE_ID);

comment on table TAX_DISTRICT is '(S) [06] The tax district table contains information defining PowerPlant tax districts.';
comment on column TAX_DISTRICT."TAX_DISTRICT_ID" is 'The internal tax district id within PowerPlant.';
comment on column TAX_DISTRICT."COUNTY_ID" is 'The county of this particular tax district.';
comment on column TAX_DISTRICT."STATE_ID" is 'The state in which this tax district resides.';
comment on column TAX_DISTRICT."DESCRIPTION" is 'A description of the tax district.';
comment on column TAX_DISTRICT."LONG_DESCRIPTION" is 'A longer description of the tax district.';
comment on column TAX_DISTRICT."NOTES" is 'A 4000 character long field for collecting notes about the tax district.';

create table LS_TAX_DISTRICT_RATES
(
 TAX_LOCAL_ID       number(22,0) not null,
 LS_TAX_DISTRICT_ID number(22,0) not null,
 COMPANY_ID         number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 RATE               number(22,8),
 EFFECTIVE_DATE     date not null
);

alter table LS_TAX_DISTRICT_RATES
   add constraint PK_LS_TAX_DISTRICT_RATES
       primary key (TAX_LOCAL_ID, LS_TAX_DISTRICT_ID, COMPANY_ID, EFFECTIVE_DATE)
       using index tablespace PWRPLANT_IDX;

alter table LS_TAX_DISTRICT_RATES
   add constraint FK_LS_TAX_DISTRICT_RATES1
       foreign key (TAX_LOCAL_ID)
       references LS_TAX_LOCAL(TAX_LOCAL_ID);

alter table LS_TAX_DISTRICT_RATES
   add constraint FK_LS_TAX_DISTRICT_RATES2
       foreign key (LS_TAX_DISTRICT_ID)
       references TAX_DISTRICT(TAX_DISTRICT_ID);

alter table LS_TAX_DISTRICT_RATES
   add constraint FK_LS_TAX_DISTRICT_RATES3
       foreign key (COMPANY_ID)
       references COMPANY_SETUP(COMPANY_ID);

comment on table LS_TAX_DISTRICT_RATES is '(S) [06] The ls tax district rates table contains the tax rates for tax districts.';
comment on column LS_TAX_DISTRICT_RATES."TAX_LOCAL_ID" is 'The internal local tax id within PowerPlant.';
comment on column LS_TAX_DISTRICT_RATES."LS_TAX_DISTRICT_ID" is 'The internal tax district id within PowerPlant.';
comment on column LS_TAX_DISTRICT_RATES."COMPANY_ID" is 'The internal company id within PowerPlant.';
comment on column LS_TAX_DISTRICT_RATES."RATE" is 'The rate for this local tax on the effective date for this company-state combination.';
comment on column LS_TAX_DISTRICT_RATES."EFFECTIVE_DATE" is 'The date on which the rate for this tax becomes active.';

create table LS_TAX_RATE_OPTION
(
 TAX_RATE_OPTION_ID number(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18),
 DESCRIPTION        varchar2(254)
);

alter table LS_TAX_RATE_OPTION
   add constraint PK_TAX_RATE_OPTION
       primary key (TAX_RATE_OPTION_ID)
       using index tablespace PWRPLANT_IDX;

comment on column LS_TAX_RATE_OPTION."TAX_RATE_OPTION_ID" is 'The internal tax rate option id within PowerPlant.';
comment on column LS_TAX_RATE_OPTION."DESCRIPTION" is 'A description of the tax rate option.';

insert into LS_TAX_RATE_OPTION (TAX_RATE_OPTION_ID, DESCRIPTION) values (1, 'Current date');
insert into LS_TAX_RATE_OPTION (TAX_RATE_OPTION_ID, DESCRIPTION) values (2, 'MLA agreement date');
insert into LS_TAX_SUMMARY (TAX_SUMMARY_ID, DESCRIPTION, PAY_LESSOR) values (0, 'None', 1);

alter table LS_LEASE_OPTIONS
   add (TAX_SUMMARY_ID     number(22,0),
        TAX_RATE_OPTION_ID number(22,0));

create table LS_ASSET_TAX_MAP
(
 LS_ASSET_ID    number(22,0) not null,
 TAX_LOCAL_ID   number(22,0) not null,
 TIME_STAMP     date,
 USER_ID        varchar2(18),
 STATUS_CODE_ID number(22,0)
);

alter table LS_ASSET_TAX_MAP
   add constraint PK_LS_ASSET_TAX_MAP
       primary key (LS_ASSET_ID, TAX_LOCAL_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_ASSET_TAX_MAP
   add constraint FK_LS_ASSET_TAX_MAP1
       foreign key (LS_ASSET_ID)
       references LS_ASSET(LS_ASSET_ID);

alter table LS_ASSET_TAX_MAP
   add constraint FK_LS_ASSET_TAX_MAP2
       foreign key (TAX_LOCAL_ID)
       references LS_TAX_LOCAL(TAX_LOCAL_ID);

comment on table LS_ASSET_TAX_MAP is '(S) [06] The ls asset tax map table maps local taxes to assets.';
comment on column LS_ASSET_TAX_MAP."LS_ASSET_ID" is 'The internal leased asset id within PowerPlant.';
comment on column LS_ASSET_TAX_MAP."TAX_LOCAL_ID" is 'The internal local tax id within PowerPlant.';
comment on column LS_ASSET_TAX_MAP."STATUS_CODE_ID" is 'A status code indicating whether or not the tax is active for this leased asset.';

alter table ASSET_LOCATION add LS_TAX_DISTRICT_ID number(22,0);

comment on column ASSET_LOCATION."LS_TAX_DISTRICT_ID" is 'The internal leased asset tax district with PowerPlant.';

insert into POWERPLANT_TABLES
   (TABLE_NAME, PP_TABLE_TYPE_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM)
values
   ('ls_tax_summary', 's', 'Lease Summary Tax Admin', null, 'lessee', null, 'lessee', 'lessee');

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('tax_summary_id', 'ls_tax_summary', null, 's', null, 'Tax Summary ID', null, 0, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('description', 'ls_tax_summary', null, 'e', null, 'Description', null, 1, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('pay_lessor', 'ls_tax_summary', 'yes_no', 'e', null, 'Pay Lessor?', null, 2, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('user_id', 'ls_tax_summary', null, 'e', null, 'User', null, 3, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('time_stamp', 'ls_tax_summary', null, 'e', null, 'Time Stamp', null, 4, null, 0);

update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = 'uo_ls_admincntr_local_tax_wksp'
 where WORKSPACE_IDENTIFIER = 'admin_taxes'
   and MODULE = 'LESSEE';

alter table LS_RENT_BUCKET_ADMIN
   add (INCLUDE_IN_TAX_BASIS number(1,0) default 2,
        TAX_EXPENSE_BUCKET number(1,0) default 2);

comment on column LS_RENT_BUCKET_ADMIN."INCLUDE_IN_TAX_BASIS" is 'A boolean value indicating whether or not a bucket should be included when calculating the total rental expense for tax purposes.';
comment on column LS_RENT_BUCKET_ADMIN."TAX_EXPENSE_BUCKET" is 'A boolean value indicating whether or not a bucket is the tax expense bucket. Only one bucket can be defined as the tax bucket.';

update POWERPLANT_COLUMNS
   set COLUMN_RANK = 6
 where TABLE_NAME = 'ls_rent_bucket_admin'
   and COLUMN_NAME = 'time_stamp';

update POWERPLANT_COLUMNS
   set COLUMN_RANK = 7
 where TABLE_NAME = 'ls_rent_bucket_admin'
   and COLUMN_NAME = 'user_id';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('abs(INCLUDE_IN_TAX_BASIS - 2)', 'ls_rent_bucket_admin', 'status_code', 'p', null,
    'Include in Expense Tax Basis', null, 4, null, 0);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
values
   ('TAX_EXPENSE_BUCKET', 'ls_rent_bucket_admin', 'status_code', 'p', null,
    'Leased Asset Tax Bucket', null, 5, null, 0);

create table LS_MONTHLY_TAX_STG
(
 LS_ASSET_ID      number(22,0) not null,
 TAX_LOCAL_ID     number(22,0) not null,
 GL_POSTING_MO_YR date not null,
 SET_OF_BOOKS_ID  number(22,0) not null,
 AMOUNT           number(22,2)
);

alter table LS_MONTHLY_TAX_STG
   add constraint PK_MONTHLY_TAX_STG
       primary key (LS_ASSET_ID, TAX_LOCAL_ID, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
       using index tablespace PWRPLANT_IDX;

alter table LS_MONTHLY_TAX_STG
   add constraint FK_MONTHLY_TAX_STG1
       foreign key (LS_ASSET_ID)
       references LS_ASSET(LS_ASSET_ID);

alter table LS_MONTHLY_TAX_STG
   add constraint FK_MONTHLY_TAX_STG2
       foreign key (TAX_LOCAL_ID)
       references LS_TAX_LOCAL(TAX_LOCAL_ID);

alter table LS_MONTHLY_TAX_STG
   add constraint FK_MONTHLY_TAX_STG3
       foreign key (SET_OF_BOOKS_ID)
       references SET_OF_BOOKS(SET_OF_BOOKS_ID);

comment on table LS_MONTHLY_TAX_STG is '(S) [06] The ls monthly tax stage table contains tax amounts for an asset for a given set of books and month.';
comment on column LS_MONTHLY_TAX_STG."LS_ASSET_ID" is 'The internal leased asset id within PowerPlant.';
comment on column LS_MONTHLY_TAX_STG."TAX_LOCAL_ID" is 'The internal local tax id within PowerPlant.';
comment on column LS_MONTHLY_TAX_STG."GL_POSTING_MO_YR" is 'The accounting month in which the tax was generated.';
comment on column LS_MONTHLY_TAX_STG."SET_OF_BOOKS_ID" is 'The internal set of books id within PowerPlant.';
comment on column LS_MONTHLY_TAX_STG."AMOUNT" is 'The dollar amount of the tax.';

alter table LS_PROCESS_CONTROL add TAX_CALC date;

comment on column LS_PROCESS_CONTROL."TAX_CALC" is 'A date indicating the day on which taxes were calculated.';

create or replace view LS_TAX_BASIS_BUCKETS as
select sum(CONT1)  as CONT1,
       sum(CONT2)  as CONT2,
       sum(CONT3)  as CONT3,
       sum(CONT4)  as CONT4,
       sum(CONT5)  as CONT5,
       sum(CONT6)  as CONT6,
       sum(CONT7)  as CONT7,
       sum(CONT8)  as CONT8,
       sum(CONT9)  as CONT9,
       sum(CONT10) as CONT10,
       sum(EXEC1)  as EXEC1,
       sum(EXEC2)  as EXEC2,
       sum(EXEC3)  as EXEC3,
       sum(EXEC4)  as EXEC4,
       sum(EXEC5)  as EXEC5,
       sum(EXEC6)  as EXEC6,
       sum(EXEC7)  as EXEC7,
       sum(EXEC8)  as EXEC8,
       sum(EXEC9)  as EXEC9,
       sum(EXEC10) as EXEC10
  from (select 2 - INCLUDE_IN_TAX_BASIS as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 1
        union
        select 0 as CONT1,
               2 - INCLUDE_IN_TAX_BASIS as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 2
        union
        select 0 as CONT1,
               0 as CONT2,
               2 - INCLUDE_IN_TAX_BASIS as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 3
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               2 - INCLUDE_IN_TAX_BASIS as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 4
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               2 - INCLUDE_IN_TAX_BASIS as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 5
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               2 - INCLUDE_IN_TAX_BASIS as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 6
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               2 - INCLUDE_IN_TAX_BASIS as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 7
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               2 - INCLUDE_IN_TAX_BASIS as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 8
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               2 - INCLUDE_IN_TAX_BASIS as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 9
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               2 - INCLUDE_IN_TAX_BASIS as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 10
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               2 - INCLUDE_IN_TAX_BASIS as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 1
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               2 - INCLUDE_IN_TAX_BASIS as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 2
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               2 - INCLUDE_IN_TAX_BASIS as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 3
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               2 - INCLUDE_IN_TAX_BASIS as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 4
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               2 - INCLUDE_IN_TAX_BASIS as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 5
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               2 - INCLUDE_IN_TAX_BASIS as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 6
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               2 - INCLUDE_IN_TAX_BASIS as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 7
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               2 - INCLUDE_IN_TAX_BASIS as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 8
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               2 - INCLUDE_IN_TAX_BASIS as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 9
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               2 - INCLUDE_IN_TAX_BASIS as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 10);

insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3040, 'Lease Tax Debit');
insert into JE_TRANS_TYPE (TRANS_TYPE, DESCRIPTION) values (3041, 'Lease Tax Credit');

create or replace view LS_TAX_EXPENSE_BUCKET as
select sum(CONT1)  as CONT1,
       sum(CONT2)  as CONT2,
       sum(CONT3)  as CONT3,
       sum(CONT4)  as CONT4,
       sum(CONT5)  as CONT5,
       sum(CONT6)  as CONT6,
       sum(CONT7)  as CONT7,
       sum(CONT8)  as CONT8,
       sum(CONT9)  as CONT9,
       sum(CONT10) as CONT10,
       sum(EXEC1)  as EXEC1,
       sum(EXEC2)  as EXEC2,
       sum(EXEC3)  as EXEC3,
       sum(EXEC4)  as EXEC4,
       sum(EXEC5)  as EXEC5,
       sum(EXEC6)  as EXEC6,
       sum(EXEC7)  as EXEC7,
       sum(EXEC8)  as EXEC8,
       sum(EXEC9)  as EXEC9,
       sum(EXEC10) as EXEC10
  from (select 2 - TAX_EXPENSE_BUCKET as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 1
        union
        select 0 as CONT1,
               2 - TAX_EXPENSE_BUCKET as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 2
        union
        select 0 as CONT1,
               0 as CONT2,
               2 - TAX_EXPENSE_BUCKET as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 3
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               2 - TAX_EXPENSE_BUCKET as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 4
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               2 - TAX_EXPENSE_BUCKET as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 5
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               2 - TAX_EXPENSE_BUCKET as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 6
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               2 - TAX_EXPENSE_BUCKET as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 7
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               2 - TAX_EXPENSE_BUCKET as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 8
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               2 - TAX_EXPENSE_BUCKET as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 9
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               2 - TAX_EXPENSE_BUCKET as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'contingent'
           and BUCKET_NUMBER = 10
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               2 - TAX_EXPENSE_BUCKET as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 1
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               2 - TAX_EXPENSE_BUCKET as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 2
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               2 - TAX_EXPENSE_BUCKET as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 3
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               2 - TAX_EXPENSE_BUCKET as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 4
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               2 - TAX_EXPENSE_BUCKET as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 5
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               2 - TAX_EXPENSE_BUCKET as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 6
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               2 - TAX_EXPENSE_BUCKET as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 7
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               2 - TAX_EXPENSE_BUCKET as EXEC8,
               0 as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 8
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               2 - TAX_EXPENSE_BUCKET as EXEC9,
               0 as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 9
        union
        select 0 as CONT1,
               0 as CONT2,
               0 as CONT3,
               0 as CONT4,
               0 as CONT5,
               0 as CONT6,
               0 as CONT7,
               0 as CONT8,
               0 as CONT9,
               0 as CONT10,
               0 as EXEC1,
               0 as EXEC2,
               0 as EXEC3,
               0 as EXEC4,
               0 as EXEC5,
               0 as EXEC6,
               0 as EXEC7,
               0 as EXEC8,
               0 as EXEC9,
               2 - TAX_EXPENSE_BUCKET as EXEC10
          from LS_RENT_BUCKET_ADMIN
         where LOWER(RENT_TYPE) = 'executory'
           and BUCKET_NUMBER = 10);

insert into LS_PAYMENT_TYPE (PAYMENT_TYPE_ID, DESCRIPTION) values (8, 'Tax Payment');
insert into JE_METHOD_TRANS_TYPE
   (JE_METHOD_ID, TRANS_TYPE)
   (select JE_METHOD_ID, 3040 from JE_METHOD);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (665, 0, 10, 4, 1, 1, 32536, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032536_lease_tax_front_end.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
