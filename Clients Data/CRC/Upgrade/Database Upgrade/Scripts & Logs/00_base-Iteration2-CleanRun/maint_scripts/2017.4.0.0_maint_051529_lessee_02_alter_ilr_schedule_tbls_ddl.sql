/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051529_lessee_02_alter_ilr_schedule_tbls_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/13/2018 David Levine     Add column to track partial month percentage
||============================================================================
*/

alter table LS_ILR_SCHEDULE_STG add partial_month_percent number(22,8);

comment on column ls_ilr_schedule_stg.partial_month_percent is 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';

alter table LS_ILR_ASSET_SCHEDULE_CALC_STG add partial_month_percent number(22,8);

comment on column ls_ilr_asset_schedule_calc_stg.partial_month_percent is 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';

alter table LS_ILR_ASSET_SCHEDULE_STG add partial_month_percent number(22,8);

comment on column ls_ilr_asset_schedule_stg.partial_month_percent is 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';

alter table LS_ASSET_SCHEDULE add partial_month_percent number(22,8);

comment on column ls_asset_schedule.partial_month_percent is 'The percentage of the first month in the payment term to pay when the payment type is Partial Month.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6986, 0, 2017, 4, 0, 0, 51529, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051529_lessee_02_alter_ilr_schedule_tbls_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
