/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048366_lease_add_pend_trans_exchange_cols_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/26/2017 Charlie Shilling add pend transaction columns to store currency exchange information for lessee multicurrency
||============================================================================
*/
ALTER TABLE pend_transaction
ADD currency_from NUMBER(22,0);

ALTER TABLE pend_transaction
ADD currency_to NUMBER(22,0);

ALTER TABLE pend_transaction
ADD exchange_rate NUMBER(22,8);

COMMENT ON COLUMN pend_transaction.currency_from IS 'The currency_id that we will use to translate certain JEs from. Currently only used for lease retirements and transfers, so this is the contract currency.';
COMMENT ON COLUMN pend_transaction.currency_to IS 'The currency_id that we will use to translate certain JEs to. Currently only used for lease retirements and transfers, so this is the company currency.';
COMMENT ON COLUMN pend_transaction.exchange_rate IS 'The rate used to translate certain JEs. Currently only used for lease retirements and transfers.';


--archive
ALTER TABLE pend_transaction_archive
ADD currency_from NUMBER(22,0);

ALTER TABLE pend_transaction_archive
ADD currency_to NUMBER(22,0);

ALTER TABLE pend_transaction_archive
ADD exchange_rate NUMBER(22,8);

COMMENT ON COLUMN pend_transaction_archive.currency_from IS 'The currency_id that we will use to translate certain JEs from. Currently only used for lease retirements and transfers, so this is the contract currency.';
COMMENT ON COLUMN pend_transaction_archive.currency_to IS 'The currency_id that we will use to translate certain JEs to. Currently only used for lease retirements and transfers, so this is the company currency.';
COMMENT ON COLUMN pend_transaction_archive.exchange_rate IS 'The rate used to translate certain JEs. Currently only used for lease retirements and transfers.';
  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3608, 0, 2017, 1, 0, 0, 48366, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048366_lease_add_pend_trans_exchange_cols_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;  
  