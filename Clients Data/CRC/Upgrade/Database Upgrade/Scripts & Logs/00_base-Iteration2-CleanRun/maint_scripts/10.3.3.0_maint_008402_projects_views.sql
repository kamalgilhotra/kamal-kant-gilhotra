/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008402_projects_views.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/31/2011 Chris Mardis   Point Release
||============================================================================
*/

create or replace view WO_EST_MONTHLY_DEESCALATION_VW as
select A.EST_MONTHLY_ID,
       A.TIME_STAMP,
       A.USER_ID,
       A.WORK_ORDER_ID,
       A.REVISION,
       A.YEAR,
       EXPENDITURE_TYPE_ID,
       A.EST_CHG_TYPE_ID,
       A.DEPARTMENT_ID,
       NVL(A.JANUARY, 0) - NVL(V.JANUARY, 0) JANUARY,
       NVL(A.FEBRUARY, 0) - NVL(V.FEBRUARY, 0) FEBRUARY,
       NVL(A.MARCH, 0) - NVL(V.MARCH, 0) MARCH,
       NVL(A.APRIL, 0) - NVL(V.APRIL, 0) APRIL,
       NVL(A.MAY, 0) - NVL(V.MAY, 0) MAY,
       NVL(A.JUNE, 0) - NVL(V.JUNE, 0) JUNE,
       NVL(A.JULY, 0) - NVL(V.JULY, 0) JULY,
       NVL(A.AUGUST, 0) - NVL(V.AUGUST, 0) AUGUST,
       NVL(A.SEPTEMBER, 0) - NVL(V.SEPTEMBER, 0) SEPTEMBER,
       NVL(A.OCTOBER, 0) - NVL(V.OCTOBER, 0) OCTOBER,
       NVL(A.NOVEMBER, 0) - NVL(V.NOVEMBER, 0) NOVEMBER,
       NVL(A.DECEMBER, 0) - NVL(V.DECEMBER, 0) DECEMBER,
       NVL(A.TOTAL, 0) - NVL(V.TOTAL, 0) TOTAL,
       A.UTILITY_ACCOUNT_ID,
       A.LONG_DESCRIPTION,
       NVL(A.FUTURE_DOLLARS, 0) FUTURE_DOLLARS,
       NVL(A.HIST_ACTUALS, 0) HIST_ACTUALS,
       A.WO_WORK_ORDER_ID,
       A.JOB_TASK_ID,
       A.SUBSTITUTION_ID,
       B.FACTOR_ID,
       B.CURVE_ID,
       B.RATE_TYPE_ID,
       NVL(A.HRS_JAN, 0) HRS_JAN,
       NVL(A.HRS_FEB, 0) HRS_FEB,
       NVL(A.HRS_MAR, 0) HRS_MAR,
       NVL(A.HRS_APR, 0) HRS_APR,
       NVL(A.HRS_MAY, 0) HRS_MAY,
       NVL(A.HRS_JUN, 0) HRS_JUN,
       NVL(A.HRS_JUL, 0) HRS_JUL,
       NVL(A.HRS_AUG, 0) HRS_AUG,
       NVL(A.HRS_SEP, 0) HRS_SEP,
       NVL(A.HRS_OCT, 0) HRS_OCT,
       NVL(A.HRS_NOV, 0) HRS_NOV,
       NVL(A.HRS_DEC, 0) HRS_DEC,
       NVL(A.HRS_TOTAL, 0) HRS_TOTAL,
       NVL(A.QTY_JAN, 0) QTY_JAN,
       NVL(A.QTY_FEB, 0) QTY_FEB,
       NVL(A.QTY_MAR, 0) QTY_MAR,
       NVL(A.QTY_APR, 0) QTY_APR,
       NVL(A.QTY_MAY, 0) QTY_MAY,
       NVL(A.QTY_JUN, 0) QTY_JUN,
       NVL(A.QTY_JUL, 0) QTY_JUL,
       NVL(A.QTY_AUG, 0) QTY_AUG,
       NVL(A.QTY_SEP, 0) QTY_SEP,
       NVL(A.QTY_OCT, 0) QTY_OCT,
       NVL(A.QTY_NOV, 0) QTY_NOV,
       NVL(A.QTY_DEC, 0) QTY_DEC,
       NVL(A.QTY_TOTAL, 0) QTY_TOTAL
  from WO_EST_MONTHLY A, WO_EST_MONTHLY_ESCALATION V, WO_EST_MONTHLY_SPREAD B
 where A.EST_MONTHLY_ID = V.EST_MONTHLY_ID(+)
   and A.YEAR = V.YEAR(+)
   and A.EST_MONTHLY_ID = B.EST_MONTHLY_ID(+);


create or replace view WO_EST_MONTHLY_DEESC_VW_FY as
select w.est_monthly_id est_monthly_id, max(w.time_stamp) time_stamp, max(w.user_id) user_id, w.work_order_id, w.revision, p.fiscal_year year,
       w.expenditure_type_id, w.est_chg_type_id, w.department_id,
       sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) january,
       sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) february,
       sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) march,
       sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) april,
       sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) may,
       sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) june,
       sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) july,
       sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) august,
       sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) september,
       sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) october,
       sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) november,
       sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)) december,
       sum(
          nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0) +
          nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.january,'02',w.february,'03',w.march,'04',w.april,'05',w.may,'06',w.june,'07',w.july,'08',w.august,'09',w.september,'10',w.october,'11',w.november,'12',w.december),0),0)
          ) total,
       w.utility_account_id, w.long_description, 0 future_dollars, 0 hist_actuals, w.wo_work_order_id, w.job_task_id, w.substitution_id,
       sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jan,
       sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_feb,
       sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_mar,
       sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_apr,
       sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_may,
       sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jun,
       sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_jul,
       sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_aug,
       sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_sep,
       sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_oct,
       sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_nov,
       sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)) hrs_dec,
       sum(
          nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0) +
          nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.hrs_jan,'02',w.hrs_feb,'03',w.hrs_mar,'04',w.hrs_apr,'05',w.hrs_may,'06',w.hrs_jun,'07',w.hrs_jul,'08',w.hrs_aug,'09',w.hrs_sep,'10',w.hrs_oct,'11',w.hrs_nov,'12',w.hrs_dec),0),0)
          ) hrs_total,
       sum(nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jan,
       sum(nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_feb,
       sum(nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_mar,
       sum(nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_apr,
       sum(nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_may,
       sum(nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jun,
       sum(nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_jul,
       sum(nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_aug,
       sum(nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_sep,
       sum(nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_oct,
       sum(nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_nov,
       sum(nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_dec,
       sum(
          nvl(decode(p.fiscal_month,1,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,2,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,3,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,4,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,5,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,6,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,7,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,8,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,9,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,10,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,11,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0) +
          nvl(decode(p.fiscal_month,12,decode(substr(p.month_number,5,2),'01',w.qty_jan,'02',w.qty_feb,'03',w.qty_mar,'04',w.qty_apr,'05',w.qty_may,'06',w.qty_jun,'07',w.qty_jul,'08',w.qty_aug,'09',w.qty_sep,'10',w.qty_oct,'11',w.qty_nov,'12',w.qty_dec),0),0)) qty_total
  from PP_CALENDAR P, WO_EST_MONTHLY_DEESCALATION_VW W
 where P.YEAR = W.YEAR
 group by W.EST_MONTHLY_ID, W.WORK_ORDER_ID, W.REVISION, P.FISCAL_YEAR,
          W.EXPENDITURE_TYPE_ID, W.EST_CHG_TYPE_ID, W.DEPARTMENT_ID,
          W.UTILITY_ACCOUNT_ID, W.LONG_DESCRIPTION, W.WO_WORK_ORDER_ID,
          W.JOB_TASK_ID, W.SUBSTITUTION_ID;


create or replace view WO_EST_MONTHLY_DEESCALATED_VW as
select W.WORK_ORDER_ID,
       W.REVISION,
       W.EST_CHG_TYPE_ID,
       TO_NUMBER(W.YEAR || LPAD(B.MONTH_NUM, 2, '0')) MONTH_NUMBER,
       W.EXPENDITURE_TYPE_ID,
       W.DEPARTMENT_ID,
       NVL(sum(DECODE(TO_NUMBER(B.MONTH_NUM),
                      1,
                      W.JANUARY,
                      2,
                      W.FEBRUARY,
                      3,
                      W.MARCH,
                      4,
                      W.APRIL,
                      5,
                      W.MAY,
                      6,
                      W.JUNE,
                      7,
                      W.JULY,
                      8,
                      W.AUGUST,
                      9,
                      W.SEPTEMBER,
                      10,
                      W.OCTOBER,
                      11,
                      W.NOVEMBER,
                      12,
                      W.DECEMBER)),
           0) AMOUNT,
       NVL(sum(DECODE(TO_NUMBER(B.MONTH_NUM),
                      1,
                      W.HRS_JAN,
                      2,
                      W.HRS_FEB,
                      3,
                      W.HRS_MAR,
                      4,
                      W.HRS_APR,
                      5,
                      W.HRS_MAY,
                      6,
                      W.HRS_JUN,
                      7,
                      W.HRS_JUL,
                      8,
                      W.HRS_AUG,
                      9,
                      W.HRS_SEP,
                      10,
                      W.HRS_OCT,
                      11,
                      W.HRS_NOV,
                      12,
                      W.HRS_DEC)),
           0) HRS,
       NVL(sum(DECODE(TO_NUMBER(B.MONTH_NUM),
                      1,
                      W.QTY_JAN,
                      2,
                      W.QTY_FEB,
                      3,
                      W.QTY_MAR,
                      4,
                      W.QTY_APR,
                      5,
                      W.QTY_MAY,
                      6,
                      W.QTY_JUN,
                      7,
                      W.QTY_JUL,
                      8,
                      W.QTY_AUG,
                      9,
                      W.QTY_SEP,
                      10,
                      W.QTY_OCT,
                      11,
                      W.QTY_NOV,
                      12,
                      W.QTY_DEC)),
           0) QTY,
       0 HIST_ACTUALS,
       W.UTILITY_ACCOUNT_ID,
       W.WO_WORK_ORDER_ID,
       W.JOB_TASK_ID,
       W.SUBSTITUTION_ID,
       W.FACTOR_ID,
       W.CURVE_ID,
       W.RATE_TYPE_ID
  from WO_EST_MONTHLY_DEESCALATION_VW W, PP_TABLE_MONTHS B
 group by W.WORK_ORDER_ID,
          W.REVISION,
          W.EST_CHG_TYPE_ID,
          TO_NUMBER(W.YEAR || LPAD(B.MONTH_NUM, 2, '0')),
          W.EXPENDITURE_TYPE_ID,
          W.DEPARTMENT_ID,
          0,
          W.UTILITY_ACCOUNT_ID,
          W.WO_WORK_ORDER_ID,
          W.JOB_TASK_ID,
          W.SUBSTITUTION_ID,
          W.FACTOR_ID,
          W.CURVE_ID,
          W.RATE_TYPE_ID;


create or replace view WO_EST_MONTHLY_FY
as
select EST_MONTHLY_ID,
       TIME_STAMP,
       USER_ID,
       WORK_ORDER_ID,
       REVISION,
       year,
       EXPENDITURE_TYPE_ID,
       EST_CHG_TYPE_ID,
       DEPARTMENT_ID,
       JANUARY,
       FEBRUARY,
       MARCH,
       APRIL,
       MAY,
       JUNE,
       JULY,
       AUGUST,
       SEPTEMBER,
       OCTOBER,
       NOVEMBER,
       DECEMBER,
       JANUARY + FEBRUARY + MARCH + APRIL + MAY + JUNE + JULY + AUGUST + SEPTEMBER + OCTOBER + NOVEMBER + DECEMBER TOTAL,
       UTILITY_ACCOUNT_ID,
       LONG_DESCRIPTION,
       FUTURE_DOLLARS,
       HIST_ACTUALS,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID,
       HRS_JAN,
       HRS_FEB,
       HRS_MAR,
       HRS_APR,
       HRS_MAY,
       HRS_JUN,
       HRS_JUL,
       HRS_AUG,
       HRS_SEP,
       HRS_OCT,
       HRS_NOV,
       HRS_DEC,
       HRS_JAN + HRS_FEB + HRS_MAR + HRS_APR + HRS_MAY + HRS_JUN + HRS_JUL + HRS_AUG + HRS_SEP + HRS_OCT + HRS_NOV + HRS_DEC HRS_TOTAL,
       QTY_JAN,
       QTY_FEB,
       QTY_MAR,
       QTY_APR,
       QTY_MAY,
       QTY_JUN,
       QTY_JUL,
       QTY_AUG,
       QTY_SEP,
       QTY_OCT,
       QTY_NOV,
       QTY_DEC,
       QTY_JAN + QTY_FEB + QTY_MAR + QTY_APR + QTY_MAY + QTY_JUN + QTY_JUL + QTY_AUG + QTY_SEP + QTY_OCT + QTY_NOV + QTY_DEC QTY_TOTAL
    from (select W.EST_MONTHLY_ID EST_MONTHLY_ID,
                 max(W.TIME_STAMP) TIME_STAMP,
                 max(W.USER_ID) USER_ID,
                 W.WORK_ORDER_ID WORK_ORDER_ID,
                 W.REVISION REVISION,
                 P.FISCAL_YEAR year,
                 max(W.EXPENDITURE_TYPE_ID) EXPENDITURE_TYPE_ID,
                 max(W.EST_CHG_TYPE_ID) EST_CHG_TYPE_ID,
                 max(W.DEPARTMENT_ID) DEPARTMENT_ID,
                 sum(nvl(decode(P.FISCAL_MONTH,1, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) JANUARY,
                 sum(nvl(decode(P.FISCAL_MONTH,2, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) FEBRUARY,
                 sum(nvl(decode(P.FISCAL_MONTH,3, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) MARCH,
                 sum(nvl(decode(P.FISCAL_MONTH,4, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) APRIL,
                 sum(nvl(decode(P.FISCAL_MONTH,5, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) MAY,
                 sum(nvl(decode(P.FISCAL_MONTH,6, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) JUNE,
                 sum(nvl(decode(P.FISCAL_MONTH,7, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) JULY,
                 sum(nvl(decode(P.FISCAL_MONTH,8, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) AUGUST,
                 sum(nvl(decode(P.FISCAL_MONTH,9, decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) SEPTEMBER,
                 sum(nvl(decode(P.FISCAL_MONTH,10,decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) OCTOBER,
                 sum(nvl(decode(P.FISCAL_MONTH,11,decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) NOVEMBER,
                 sum(nvl(decode(P.FISCAL_MONTH,12,decode(substr(P.MONTH_NUMBER,5,2),'01',W.JANUARY,'02',W.FEBRUARY,'03',W.MARCH,'04',W.APRIL,'05',W.MAY,'06',W.JUNE,'07',W.JULY,'08',W.AUGUST,'09',W.SEPTEMBER,'10',W.OCTOBER,'11',W.NOVEMBER,'12',W.DECEMBER),0),0)) DECEMBER,
                 max(W.UTILITY_ACCOUNT_ID) UTILITY_ACCOUNT_ID,
                 max(W.LONG_DESCRIPTION) LONG_DESCRIPTION,
                 0 FUTURE_DOLLARS,
                 0 HIST_ACTUALS,
                 max(W.WO_WORK_ORDER_ID) WO_WORK_ORDER_ID,
                 max(W.JOB_TASK_ID) JOB_TASK_ID,
                 max(W.SUBSTITUTION_ID) SUBSTITUTION_ID,
                 sum(nvl(decode(P.FISCAL_MONTH,1, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_JAN,
                 sum(nvl(decode(P.FISCAL_MONTH,2, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_FEB,
                 sum(nvl(decode(P.FISCAL_MONTH,3, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_MAR,
                 sum(nvl(decode(P.FISCAL_MONTH,4, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_APR,
                 sum(nvl(decode(P.FISCAL_MONTH,5, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_MAY,
                 sum(nvl(decode(P.FISCAL_MONTH,6, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_JUN,
                 sum(nvl(decode(P.FISCAL_MONTH,7, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_JUL,
                 sum(nvl(decode(P.FISCAL_MONTH,8, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_AUG,
                 sum(nvl(decode(P.FISCAL_MONTH,9, decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_SEP,
                 sum(nvl(decode(P.FISCAL_MONTH,10,decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_OCT,
                 sum(nvl(decode(P.FISCAL_MONTH,11,decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_NOV,
                 sum(nvl(decode(P.FISCAL_MONTH,12,decode(substr(P.MONTH_NUMBER,5,2),'01',W.HRS_JAN,'02',W.HRS_FEB,'03',W.HRS_MAR,'04',W.HRS_APR,'05',W.HRS_MAY,'06',W.HRS_JUN,'07',W.HRS_JUL,'08',W.HRS_AUG,'09',W.HRS_SEP,'10',W.HRS_OCT,'11',W.HRS_NOV,'12',W.HRS_DEC),0),0)) HRS_DEC,
                 sum(nvl(decode(P.FISCAL_MONTH,1, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_JAN,
                 sum(nvl(decode(P.FISCAL_MONTH,2, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_FEB,
                 sum(nvl(decode(P.FISCAL_MONTH,3, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_MAR,
                 sum(nvl(decode(P.FISCAL_MONTH,4, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_APR,
                 sum(nvl(decode(P.FISCAL_MONTH,5, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_MAY,
                 sum(nvl(decode(P.FISCAL_MONTH,6, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_JUN,
                 sum(nvl(decode(P.FISCAL_MONTH,7, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_JUL,
                 sum(nvl(decode(P.FISCAL_MONTH,8, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_AUG,
                 sum(nvl(decode(P.FISCAL_MONTH,9, decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_SEP,
                 sum(nvl(decode(P.FISCAL_MONTH,10,decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_OCT,
                 sum(nvl(decode(P.FISCAL_MONTH,11,decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_NOV,
                 sum(nvl(decode(P.FISCAL_MONTH,12,decode(substr(P.MONTH_NUMBER,5,2),'01',W.QTY_JAN,'02',W.QTY_FEB,'03',W.QTY_MAR,'04',W.QTY_APR,'05',W.QTY_MAY,'06',W.QTY_JUN,'07',W.QTY_JUL,'08',W.QTY_AUG,'09',W.QTY_SEP,'10',W.QTY_OCT,'11',W.QTY_NOV,'12',W.QTY_DEC),0),0)) QTY_DEC
            from PP_CALENDAR P, WO_EST_MONTHLY W
           where P.YEAR = W.YEAR
           group by W.EST_MONTHLY_ID, P.FISCAL_YEAR, W.WORK_ORDER_ID, W.REVISION
         );


create or replace view WO_EST_MONTHLY_MONTH_NUMBER as
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '01') MONTH_NUMBER,
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JANUARY) AMOUNT,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where "TEMP_WORK_ORDER"."SESSION_ID" = USERENV('SESSIONID')
   and UPPER("TEMP_WORK_ORDER"."USER_ID") = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and JANUARY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '02'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(FEBRUARY),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and FEBRUARY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '03'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(MARCH),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and MARCH <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '04'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(APRIL),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and APRIL <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '05'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(MAY),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and MAY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '06'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JUNE),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and JUNE <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '07'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JULY),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and JULY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '08'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(AUGUST),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and AUGUST <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '09'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(SEPTEMBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and SEPTEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '10'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(OCTOBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and OCTOBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '11'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(NOVEMBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and NOVEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '12'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(DECEMBER),
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       EST_MONTHLY_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER
 where TEMP_WORK_ORDER.SESSION_ID = USERENV('SESSIONID')
   and UPPER(TEMP_WORK_ORDER.USER_ID) = UPPER(user)
   and TEMP_WORK_ORDER.WORK_ORDER_ID = A.WORK_ORDER_ID
   and TEMP_WORK_ORDER.BATCH_REPORT_ID = 0
   and DECEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          EST_MONTHLY_ID;


create or replace view WO_EST_MONTHLY_VIEW as
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '01') MONTH_NUMBER,
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(JANUARY), 0) AMOUNT,
       NVL(sum(HRS_JAN), 0) HRS,
       NVL(sum(QTY_JAN), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where JANUARY <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '02'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(FEBRUARY), 0) AMOUNT,
       NVL(sum(HRS_FEB), 0) HRS,
       NVL(sum(QTY_FEB), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where FEBRUARY <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '03'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(MARCH), 0) AMOUNT,
       NVL(sum(HRS_MAR), 0) HRS,
       NVL(sum(QTY_MAR), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where MARCH <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '04'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(APRIL), 0) AMOUNT,
       NVL(sum(HRS_APR), 0) HRS,
       NVL(sum(QTY_APR), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where APRIL <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '05'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(MAY), 0) AMOUNT,
       NVL(sum(HRS_MAY), 0) HRS,
       NVL(sum(QTY_MAY), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where MAY <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '06'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(JUNE), 0) AMOUNT,
       NVL(sum(HRS_JUN), 0) HRS,
       NVL(sum(QTY_JUN), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where JUNE <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '07'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(JULY), 0) AMOUNT,
       NVL(sum(HRS_JUL), 0) HRS,
       NVL(sum(QTY_JUL), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where JULY <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '08'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(AUGUST), 0) AMOUNT,
       NVL(sum(HRS_AUG), 0) HRS,
       NVL(sum(QTY_AUG), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where AUGUST <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '09'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(SEPTEMBER), 0) AMOUNT,
       NVL(sum(HRS_SEP), 0) HRS,
       NVL(sum(QTY_SEP), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where SEPTEMBER <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '10'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(OCTOBER), 0) AMOUNT,
       NVL(sum(HRS_OCT), 0) HRS,
       NVL(sum(QTY_OCT), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where OCTOBER <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '11'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(NOVEMBER), 0) AMOUNT,
       NVL(sum(HRS_NOV), 0) HRS,
       NVL(sum(QTY_NOV), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where NOVEMBER <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '12'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       NVL(sum(DECEMBER), 0) AMOUNT,
       NVL(sum(HRS_DEC), 0) HRS,
       NVL(sum(QTY_DEC), 0) QTY,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 where DECEMBER <> 0
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID
union
select WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '01'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       0 AMOUNT,
       0 HRS,
       0 QTY,
       sum(NVL(HIST_ACTUALS, 0)) HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID,
       SUBSTITUTION_ID
  from WO_EST_MONTHLY
 group by WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID,
          SUBSTITUTION_ID;


create or replace view WO_EST_MONTHLY_VIEW_TEMP as
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '01') MONTH_NUMBER,
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JANUARY) AMOUNT,
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and JANUARY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '02'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(FEBRUARY),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and FEBRUARY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '03'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(MARCH),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and MARCH <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '04'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(APRIL),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and APRIL <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '05'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(MAY),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and MAY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '06'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JUNE),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and JUNE <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '07'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(JULY),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and JULY <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '08'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(AUGUST),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and AUGUST <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '09'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(SEPTEMBER),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and SEPTEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '10'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(OCTOBER),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and OCTOBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '11'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(NOVEMBER),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and NOVEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '12'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       sum(DECEMBER),
       0 HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
   and DECEMBER <> 0
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID
union
select A.WORK_ORDER_ID,
       REVISION,
       EST_CHG_TYPE_ID,
       TO_NUMBER(TO_CHAR(year) || '01'),
       EXPENDITURE_TYPE_ID,
       DEPARTMENT_ID,
       0 AMOUNT,
       sum(NVL(HIST_ACTUALS, 0)) HIST_ACTUALS,
       UTILITY_ACCOUNT_ID,
       WO_WORK_ORDER_ID,
       JOB_TASK_ID
  from WO_EST_MONTHLY A, TEMP_WORK_ORDER B
 where B.USER_ID = user
   and B.SESSION_ID = USERENV('sessionid')
   and B.BATCH_REPORT_ID = 0
   and B.WORK_ORDER_ID = A.WORK_ORDER_ID
 group by A.WORK_ORDER_ID,
          REVISION,
          EST_CHG_TYPE_ID,
          year,
          EXPENDITURE_TYPE_ID,
          DEPARTMENT_ID,
          UTILITY_ACCOUNT_ID,
          WO_WORK_ORDER_ID,
          JOB_TASK_ID;


create or replace view WO_EST_SUMMARY_VIEW as
select WO_EST_MONTHLY.EST_MONTHLY_ID EST_SUMMARY_ID,
       WO_EST_MONTHLY.EST_CHG_TYPE_ID,
       WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
       WO_EST_MONTHLY.WORK_ORDER_ID,
       WO_EST_MONTHLY.REVISION,
       WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
       WO_EST_MONTHLY.DEPARTMENT_ID,
       WO_EST_MONTHLY.HIST_ACTUALS,
       WO_EST_MONTHLY.WO_WORK_ORDER_ID,
       WO_EST_MONTHLY.JOB_TASK_ID,
       WO_EST_MONTHLY.LONG_DESCRIPTION,
       sum(WO_EST_MONTHLY.TOTAL) APPROVED_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR, TOTAL, 0)) CURRENT_YEAR_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 1, TOTAL, 0)) YEAR2_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 2, TOTAL, 0)) YEAR3_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 3, TOTAL, 0)) YEAR4_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 4, TOTAL, 0)) YEAR5_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 5, TOTAL, 0)) YEAR6_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 6, TOTAL, 0)) YEAR7_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 7, TOTAL, 0)) YEAR8_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 8, TOTAL, 0)) YEAR9_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 9, TOTAL, 0)) YEAR10_DOLLARS,
       sum(DECODE(WO_EST_MONTHLY.YEAR, BUDGET_VERSION.START_YEAR + 10, TOTAL, 0)) YEAR11_DOLLARS,
       sum(DECODE(SIGN(WO_EST_MONTHLY.YEAR - (BUDGET_VERSION.START_YEAR + 10)), 1, TOTAL, 0)) FUTURE_DOLLARS
  from WO_EST_MONTHLY, BUDGET_VERSION, BUDGET_VERSION_FUND_PROJ
 where WO_EST_MONTHLY.REVISION = BUDGET_VERSION_FUND_PROJ.REVISION
   and WO_EST_MONTHLY.WORK_ORDER_ID = BUDGET_VERSION_FUND_PROJ.WORK_ORDER_ID
   and BUDGET_VERSION_FUND_PROJ.ACTIVE = 1
   and BUDGET_VERSION_FUND_PROJ.BUDGET_VERSION_ID =
       (select max(BUDGET_VERSION_ID)
          from BUDGET_VERSION_FUND_PROJ A
         where A.WORK_ORDER_ID = BUDGET_VERSION_FUND_PROJ.WORK_ORDER_ID
           and A.REVISION = BUDGET_VERSION_FUND_PROJ.REVISION
           and A.ACTIVE = 1)
   and BUDGET_VERSION.BUDGET_VERSION_ID = BUDGET_VERSION_FUND_PROJ.BUDGET_VERSION_ID
   and exists (select 1
          from WORK_ORDER_CONTROL B
         where B.WORK_ORDER_ID = WO_EST_MONTHLY.WORK_ORDER_ID
           and B.FUNDING_WO_INDICATOR = 1)
 group by WO_EST_MONTHLY.EST_MONTHLY_ID,
          WO_EST_MONTHLY.EST_CHG_TYPE_ID,
          WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
          WO_EST_MONTHLY.WORK_ORDER_ID,
          WO_EST_MONTHLY.REVISION,
          WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
          WO_EST_MONTHLY.DEPARTMENT_ID,
          WO_EST_MONTHLY.HIST_ACTUALS,
          WO_EST_MONTHLY.WO_WORK_ORDER_ID,
          WO_EST_MONTHLY.JOB_TASK_ID,
          WO_EST_MONTHLY.LONG_DESCRIPTION
union all
select WO_EST_MONTHLY.EST_MONTHLY_ID EST_SUMMARY_ID,
       WO_EST_MONTHLY.EST_CHG_TYPE_ID,
       WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
       WO_EST_MONTHLY.WORK_ORDER_ID,
       WO_EST_MONTHLY.REVISION,
       WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
       WO_EST_MONTHLY.DEPARTMENT_ID,
       WO_EST_MONTHLY.HIST_ACTUALS,
       WO_EST_MONTHLY.WO_WORK_ORDER_ID,
       WO_EST_MONTHLY.JOB_TASK_ID,
       WO_EST_MONTHLY.LONG_DESCRIPTION,
       sum(WO_EST_MONTHLY.TOTAL) APPROVED_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')), WO_EST_MONTHLY.YEAR, TOTAL, 0)) CURRENT_YEAR_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 1, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR2_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 2, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR3_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 3, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR4_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 4, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR5_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 5, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR6_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 6, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR7_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 7, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR8_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 8, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR9_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 9, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR10_DOLLARS,
       sum(DECODE(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) + 10, WO_EST_MONTHLY.YEAR, TOTAL, 0)) YEAR11_DOLLARS,
       sum(DECODE(SIGN(TO_NUMBER(TO_CHAR(EST_START_DATE, 'YYYY')) - (WO_EST_MONTHLY.YEAR + 10)),
                  1,
                  TOTAL,
                  0)) FUTURE_DOLLARS
  from WO_EST_MONTHLY, WORK_ORDER_APPROVAL WOA
 where WO_EST_MONTHLY.WORK_ORDER_ID = WOA.WORK_ORDER_ID
   and WO_EST_MONTHLY.REVISION = WOA.REVISION
   and exists (select 1
          from WORK_ORDER_CONTROL B
         where B.WORK_ORDER_ID = WO_EST_MONTHLY.WORK_ORDER_ID
           and B.FUNDING_WO_INDICATOR = 0)
 group by WO_EST_MONTHLY.EST_MONTHLY_ID,
          WO_EST_MONTHLY.EST_CHG_TYPE_ID,
          WO_EST_MONTHLY.EXPENDITURE_TYPE_ID,
          WO_EST_MONTHLY.WORK_ORDER_ID,
          WO_EST_MONTHLY.REVISION,
          WO_EST_MONTHLY.UTILITY_ACCOUNT_ID,
          WO_EST_MONTHLY.DEPARTMENT_ID,
          WO_EST_MONTHLY.HIST_ACTUALS,
          WO_EST_MONTHLY.WO_WORK_ORDER_ID,
          WO_EST_MONTHLY.JOB_TASK_ID,
          WO_EST_MONTHLY.LONG_DESCRIPTION;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (34, 0, 10, 3, 3, 0, 8402, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008402_projects_views.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
