/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010209_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   09/19/2012 Julia Breuer   Point Release
||============================================================================
*/

insert into PWRPLANT.PTC_SYSTEM_OPTIONS ( SYSTEM_OPTION_ID, TIME_STAMP, USER_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, OPTION_VALUE, IS_BASE_OPTION ) values ( 'Parcel Center - Details - Assessments - Show Only Cases with Values', sysdate, user, 'The default for whether the Assessments grid should be filtered to show only cases with non-zero values.  The Show Only Cases with Values checkbox will be set accordingly.', 0, 'No', null, 1 );

insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Details - Assessments - Show Only Cases with Values', 'No', sysdate, user );
insert into PWRPLANT.PTC_SYSTEM_OPTIONS_VALUES ( SYSTEM_OPTION_ID, OPTION_VALUE, TIME_STAMP, USER_ID ) values ( 'Parcel Center - Details - Assessments - Show Only Cases with Values', 'Yes', sysdate, user );

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (227, 0, 10, 4, 1, 0, 10209, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_010209_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
