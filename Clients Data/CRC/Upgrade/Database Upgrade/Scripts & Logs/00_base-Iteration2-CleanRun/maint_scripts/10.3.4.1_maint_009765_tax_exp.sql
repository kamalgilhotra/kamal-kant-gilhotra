/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009765_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.1   05/08/2012 Sunjin Cone    Point Release
||============================================================================
*/

-- * Maint Req 9765 - Add table to calc and store the % of the major items that will expense.
--   This will be used to expense the similar portion of the minor items

create table REPAIR_MAJOR_UNIT_PCT
(
 BATCH_ID             number(22,0) not null,
 REPAIR_SCHEMA_ID     number(22,0) not null,
 COMPANY_ID           number(22,0) not null,
 WORK_ORDER_NUMBER    varchar2(35) not null,
 REPAIR_UNIT_CODE_ID  number(22,0) not null,
 MAJOR_EXPENSE_TAKEN  number(22,2),
 MAJOR_TOTAL_ACTIVITY number(22,2),
 MAJOR_EXPENSE_PCT    number(22,20)
);

alter table REPAIR_MAJOR_UNIT_PCT
   add constraint PK_REPAIR_MAJOR_UNIT_PCT
       primary key (BATCH_ID, REPAIR_SCHEMA_ID, COMPANY_ID, WORK_ORDER_NUMBER, REPAIR_UNIT_CODE_ID)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (132, 0, 10, 3, 4, 1, 9765, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.1_maint_009765_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
