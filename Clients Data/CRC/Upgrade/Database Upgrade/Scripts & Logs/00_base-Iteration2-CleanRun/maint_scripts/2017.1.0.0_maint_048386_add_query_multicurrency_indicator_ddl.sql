/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048386_add_query_multicurrency_indicator_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 07/28/2017 Josh Sandler     Add multicurrency indicator to Any Query table
||============================================================================
*/

ALTER TABLE pp_any_query_criteria
  ADD is_multicurrency NUMBER(1);

COMMENT ON COLUMN pp_any_query_criteria.is_multicurrency IS 'Indicator for whether query returns results in multiple currencies.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3617, 0, 2017, 1, 0, 0, 48386, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048386_add_query_multicurrency_indicator_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
