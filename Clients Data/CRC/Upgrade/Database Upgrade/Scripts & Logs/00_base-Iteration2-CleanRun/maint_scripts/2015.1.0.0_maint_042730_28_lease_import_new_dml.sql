/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		LESSEE
|| File Name:   maint_042730_28_lease_import_new_dml.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| [FROMPREF] 03/23/2015 [YOUR NAME]    	 [DESCRIPTION]
||==========================================================================================
*/

insert into pp_import_column(import_type_id, column_name, description, import_column_name,
   is_required, processing_order, column_type, is_on_table)
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_lease') AS import_type_id,
   'lease_end_date' AS column_name,
   'Lease End Date' AS description,
   null AS import_column_name,
   0 AS is_required,
   1 AS processing_order,
   'varchar2(254)' AS column_type,
   1 AS is_on_table
from dual
union
select
   (select import_type_id from pp_import_type where import_table_name = 'ls_import_lease') AS import_type_id,
   'days_in_month_sw' AS column_name,
   'Days in Month SW' AS description,
   null AS import_column_name,
   0 AS is_required,
   1 AS processing_order,
   'varchar2(35)' AS column_type,
   1 AS is_on_table
from dual
;

declare 
	max_field_id pp_import_template_fields.field_id%type;
begin 
	select nvl(max(field_id),0) 
	into max_field_id
	from pp_import_template_fields pitf
	where pitf.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_lease');

	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select pit.import_template_id, max_field_id + 1, pit.import_type_id, 'days_in_year', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_lease');

	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select pit.import_template_id, max_field_id + 2, pit.import_type_id, 'cut_off_day', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_lease');

	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select pit.import_template_id, max_field_id + 3, pit.import_type_id, 'lease_end_date', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_lease');

	insert into pp_import_template_fields(import_template_id, field_id, import_type_id, column_name, import_lookup_id)
	select pit.import_template_id, max_field_id + 4, pit.import_type_id, 'days_in_month_sw', null
	from pp_import_template pit
	where pit.import_type_id = (select import_type_id from pp_import_type where import_table_name = 'ls_import_lease');
end; 
/


commit;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2422, 0, 2015, 1, 0, 0, 042730, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042730_28_lease_import_new_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;