/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_archive_columns.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/19/2012 Stephen Wicks  Point Release
||============================================================================
*/

-- add column to pp_import_type for auto-archiving additional columns not included in pp_import_columns
alter table PP_IMPORT_TYPE add ARCHIVE_ADDITIONAL_COLUMNS varchar2(2000);

-- Update the Property Tax imports with additional columns to archive
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'assessor_id, assessor_xlate' where IMPORT_TYPE_ID = 29;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'asset_location_id, asset_location_xlate' where IMPORT_TYPE_ID = 37;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'division_id, division_xlate' where IMPORT_TYPE_ID = 34;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'property_tax_ledger_id' where IMPORT_TYPE_ID = 1;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'assessor_line_id, asset_location_id, division_line_id, location_type_line_id, major_loc_line_id, minor_loc2_line_id, minor_loc_line_id, muni_line_id, parcel_line_id, prop_tax_loc_line_id, tax_dist_line_id, type_code_line_id' where IMPORT_TYPE_ID = 40;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'location_type_id, location_type_xlate' where IMPORT_TYPE_ID = 33;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'major_location_id, major_location_xlate' where IMPORT_TYPE_ID = 35;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'minor_location_id, minor_location_xlate' where IMPORT_TYPE_ID = 32;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'event_id' where IMPORT_TYPE_ID = 16;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'responsible_entity_id, responsible_entity_xlate' where IMPORT_TYPE_ID = 14;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'preallo_ledger_id' where IMPORT_TYPE_ID = 9;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'prop_tax_location_id, prop_tax_location_xlate' where IMPORT_TYPE_ID = 36;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'retirement_unit_id, retirement_unit_xlate' where IMPORT_TYPE_ID = 23;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'statistics_full_id' where IMPORT_TYPE_ID = 2;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'statistics_incr_id' where IMPORT_TYPE_ID = 3;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'tax_district_id, tax_district_xlate' where IMPORT_TYPE_ID = 31;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'type_code_id, type_code_xlate' where IMPORT_TYPE_ID = 30;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'utility_account_id, utility_account_xlate' where IMPORT_TYPE_ID = 24;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'pk_string' where IMPORT_TYPE_ID = 17;
update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'statement_id, statement_xlate' where IMPORT_TYPE_ID = 22;

update PWRPLANT.PP_IMPORT_TYPE set ARCHIVE_ADDITIONAL_COLUMNS = 'act_activity_code, act_activity_cost, act_activity_quantity, act_activity_status, act_asset_activity_id, act_cpr_posting_mo_yr, act_description, act_ferc_activity_code, '||
            'act_gl_je_code, act_gl_posting_mo_yr, act_long_description, act_month_number, asset_id, books_schema_id, compute_act_status, depr_group_id, disp_activity_code, disp_activity_cost, disp_activity_quantity, disp_activity_status, '||
            'disp_asset_activity_id, disp_cpr_posting_mo_yr, disp_description, disp_disposition_code, disp_ferc_activity_code, disp_gl_je_code, disp_gl_posting_mo_yr, disp_long_description, disp_month_number, disp_out_of_service_mo_yr, '||
            'ledger_status, loaded, property_group_id, ru_line_id, subledger_indicator, sub_account_id, trans_activity_status, trans_cpr_posting_mo_yr, trans_description, trans_from_activity_code, trans_from_activity_cost, '||
            'trans_from_activity_quantity, trans_from_asset_activity_id, trans_from_asset_id, trans_from_ferc_activity_code, trans_gl_je_code, trans_gl_posting_mo_yr, trans_long_description, trans_month_number, trans_to_activity_code1, '||
            'trans_to_activity_code2, trans_to_activity_cost1, trans_to_activity_cost2, trans_to_activity_quantity1, trans_to_activity_quantity2, trans_to_asset_activity_id1, trans_to_asset_activity_id2, trans_to_asset_id, '||
            'trans_to_ferc_activity_code1, trans_to_ferc_activity_code2, ua_line_id' where IMPORT_TYPE_ID = 25;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (268, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_archive_columns.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
