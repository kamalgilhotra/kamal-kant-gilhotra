/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051019_lessee_03_drop_ls_cap_type_book_summary_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 04/20/2018 Josh Sandler     Remove Book Summary from LS_LEASE_CAP_TYPE
||============================================================================
*/

ALTER TABLE LS_LEASE_CAP_TYPE
  DROP COLUMN book_summary_id;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (5024, 0, 2017, 4, 0, 0, 51019, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051019_lessee_03_drop_ls_cap_type_book_summary_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT; 