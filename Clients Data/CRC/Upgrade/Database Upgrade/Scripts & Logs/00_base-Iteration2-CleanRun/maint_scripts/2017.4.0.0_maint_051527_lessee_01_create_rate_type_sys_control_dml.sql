/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_051527_lessee_01_create_rate_type_sys_control_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2017.4.0.0 06/07/2018 Sisouphanh       Add new currency rate type and system control
||                                        Add new trans type 3068 Lease Currency Gain/Loss ST Debit
||============================================================================
*/

/* Insert new exchange rate type or update description for 'Average' Rate */
merge into currency_rate_type a
using (select 4 exchange_rate_type_id, 'Average' description from dual) x
      on (a.exchange_rate_type_id = x.exchange_rate_type_id)
when matched then update set a.description = x.description
when not matched then insert (a.exchange_rate_type_id, a.description) values (x.exchange_rate_type_id, x.description);  

/* Add new system control for Lease MC: Use Average Rates' */
insert into pp_system_control_company(control_id, control_name, control_value, description, long_description, company_id)
select *
from (select
   		max(control_id)+1 AS control_id,
   		'Lease MC: Use Average Rates',
   		'no' AS control_value,
   		'dw_yes_no;1' AS description,
   		'Yes: Use Average Period Rates, No: Use End of Period Rates in Multi-Currency Schedule conversion from contract currency to company currency' AS long_description,
   		-1 AS company_id
	from pp_system_control_company
	)
where not exists(
   select 1
   from pp_system_control_company
   where control_name = 'Lease MC: Use Average Rates');

/* Add the new trans type 3068 - Lease Currency Gain/Loss ST Debit */   
merge into je_trans_type a
using (select 3068 trans_type, '3068 - Lease Currency Gain/Loss ST Debit' description from dual) x 
on (a.trans_type = x.trans_type)
when matched then update set a.description = x.description
when not matched then insert (a.trans_type, a.description) values (x.trans_type, x.description);   

/* Relate the new trans type to the delivered JE Method */
insert into je_method_trans_type(je_method_id, trans_type)
select * 
from (select 1 AS je_method_id,	trans_type 
	  from je_trans_type 
	  where trans_type = 3068
	 )
where not exists(
	select 1
	from je_method_trans_type
	where trans_type = 3068);
	
/* Update the description on 3051 to be '3051 - Lease Currency Gain/Loss LT Debit'	*/
update je_trans_type 
set description = '3051 - Lease Currency Gain/Loss LT Debit'
where trans_type = 3051;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7222, 0, 2017, 4, 0, 0, 51527, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051527_lessee_01_create_rate_type_sys_control_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;