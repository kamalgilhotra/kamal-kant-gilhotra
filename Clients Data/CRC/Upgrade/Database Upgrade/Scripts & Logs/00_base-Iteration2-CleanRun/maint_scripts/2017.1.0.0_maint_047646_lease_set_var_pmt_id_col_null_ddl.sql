 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_047646_lease_set_var_pmt_id_col_null_ddl.sql
 ||============================================================================
 || Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.1.0.0 05/18/2017 D. Haupt       Script for necessary change that was made manually
 ||============================================================================
 */ 
 
 alter table LS_ILR_PAYMENT_TERM_VAR_PAYMNT
       modify VARIABLE_PAYMENT_ID null;
	   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3505, 0, 2017, 1, 0, 0, 47646, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047646_lease_set_var_pmt_id_col_null_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;