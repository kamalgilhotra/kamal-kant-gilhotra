/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043464_reg_hist_lyr_5_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/09/2015 Andrew Scott   backfill archive tables with new incremental
||                                      process ids.  Likely just going to be -1 though.
||============================================================================
*/

update DEPR_CALC_STG_ARC x
set x.INCREMENTAL_PROCESS_ID = (
   select y.INCREMENTAL_PROCESS_ID
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update DEPR_CALC_STG_ARC
set INCREMENTAL_PROCESS_ID = -1
where INCREMENTAL_PROCESS_ID is null;

update FCST_DEPR_CALC_STG_ARC x
set x.INCREMENTAL_PROCESS_ID = (
   select y.INCREMENTAL_PROCESS_ID
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update FCST_DEPR_CALC_STG_ARC
set INCREMENTAL_PROCESS_ID = -1
where INCREMENTAL_PROCESS_ID is null;

update FCST_DEPR_LEDGER_BLEND_STG_ARC x
set x.INCREMENTAL_PROCESS_ID = (
   select y.INCREMENTAL_PROCESS_ID
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
)
where exists (
   select 1
   from INCREMENTAL_PROCESS y
   where x.FUNDING_WO_ID = y.FUNDING_WO_ID
   and x.REVISION = y.REVISION
);

update FCST_DEPR_LEDGER_BLEND_STG_ARC
set INCREMENTAL_PROCESS_ID = -1
where INCREMENTAL_PROCESS_ID is null;

commit;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2586, 0, 2015, 2, 0, 0, 043464, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043464_reg_hist_lyr_5_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;