/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010548_projects.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   09/13/2012 Chris Mardis   Point Release
||============================================================================
*/

create table WO_EST_DERIVATION_PCT
(
 WORK_ORDER_ID        number(22, 0) not null,
 REVISION             number(22, 0) not null,
 EXPENDITURE_TYPE_ID  number(22, 0) not null,
 CR_DERIVATION_ROLLUP varchar2(35) not null,
 TIME_STAMP           date,
 USER_ID              varchar2(18),
 PCT                  number(22, 8)
);

alter table WO_EST_DERIVATION_PCT
   add constraint WO_EST_DERIV_PCT_PK
       primary key (WORK_ORDER_ID, REVISION, EXPENDITURE_TYPE_ID, CR_DERIVATION_ROLLUP)
       using index tablespace PWRPLANT_IDX;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1 CONTROL_ID,
          'WOEST - WO - DERIVATION SPREAD',
          'no',
          'dw_yes_no;1',
          'For Work Order Monthly Estimates, this turns on the ability to split estimates across expenditure types based on estimate charge type cr_derivation_rollup percentages entered on the revision.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, COMPANY_ID)
   select max(CONTROL_ID) + 1 CONTROL_ID,
          'WOEST - FP - DERIVATION SPREAD',
          'no',
          'dw_yes_no;1',
          'For Funding Project Monthly Estimates, this turns on the ability to split estimates across expenditure types based on estimate charge type cr_derivation_rollup percentages entered on the revision.',
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (214, 0, 10, 4, 0, 0, 10548, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_010548_projects.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
