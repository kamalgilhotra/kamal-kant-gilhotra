/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Lessee
|| File Name:   maint_041536_lessee_comp_pt_ex_ddl.sql
||==========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.2 12/10/2014 [B.Beck    	 Add a column to Lease Components to track if a component 
||									is exempt for Property Tax 
||==========================================================================================
*/

alter table ls_component
add prop_tax_exempt number(1,0) default 0;

comment on column ls_component.prop_tax_exempt
is 'A flag specifying if the leased component is exempt from property tax purposes';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2110, 0, 10, 4, 3, 2, 041536, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.2_maint_041536_lessee_comp_pt_ex_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;