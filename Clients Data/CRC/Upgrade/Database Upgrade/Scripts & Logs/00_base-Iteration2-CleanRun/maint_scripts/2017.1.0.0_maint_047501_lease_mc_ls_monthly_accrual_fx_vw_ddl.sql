/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047501_lease_mc_ls_monthly_accrual_fx_vw_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 06/16/2017 Anand R        create view for ls_monthly_accrual_stg for multi currency
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_MONTHLY_ACCRUAL_STG_FX AS
WITH
cur AS (
    SELECT 1 ls_cur_type, contract_cur.currency_id AS currency_id, contract_cur.currency_display_symbol currency_display_symbol, contract_cur.iso_code iso_code
    FROM currency contract_cur
    UNION
    SELECT 2, company_cur.currency_id, company_cur.currency_display_symbol, company_cur.iso_code
    FROM currency company_cur
    )
SELECT lmas.accrual_id,
       lmas.accrual_type_id,
       lmas.ls_asset_id,
       Round(lmas.amount * decode(ls_cur_type, 2, rate, 1) ,2) amount,
       lmas.gl_posting_mo_yr,
       lmas.set_of_books_id,
       la.contract_currency_id,
       cs.currency_id company_currency_id,
       cur.ls_cur_type AS ls_cur_type,
       cr.exchange_date,
       decode(ls_cur_type, 2, rate, 1) rate,
       cur.iso_code,
       cur.currency_display_symbol
FROM   ls_monthly_accrual_stg lmas
INNER JOIN LS_ASSET la
      ON lmas.ls_asset_id = la.ls_asset_id
INNER JOIN currency_schema cs
      ON la.company_id = cs.company_id
INNER JOIN cur
      ON (
          (cur.ls_cur_type = 1 AND cur.currency_id = la.contract_currency_id)
          OR
          (cur.ls_cur_type = 2 AND cur.currency_id = cs.currency_id)
         )
INNER JOIN ls_lease_calculated_date_rates cr
      ON (cr.company_id = la.company_id
        AND cr.contract_currency_id =  la.contract_currency_id
        AND cr.company_currency_id =  cs.currency_id
        AND cr.accounting_month = lmas.gl_posting_mo_yr )
where cr.exchange_rate_type_id = 1
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3533, 0, 2017, 1, 0, 0, 47501, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047501_lease_mc_ls_monthly_accrual_fx_vw_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;