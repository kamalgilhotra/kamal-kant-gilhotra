/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045417_jobserver_update_jobflow_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 05/23/2016 Jared Watkins    change security keys for job flows page in integration manager
||============================================================================
*/
update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.BudgetProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneBudgetProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.BudgetProcesses' where objects = 'IntegrationManager.JobFlows.DeleteBudgetProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.BudgetProcesses' where objects = 'IntegrationManager.JobFlows.EditBudgetProcesses';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.CRSystemProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneCRSystemProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.CRSystemProcesses' where objects = 'IntegrationManager.JobFlows.DeleteCRSystemProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.CRSystemProcesses' where objects = 'IntegrationManager.JobFlows.EditCRSystemProcesses';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.CustomProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneCustomProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.CustomProcesses' where objects = 'IntegrationManager.JobFlows.DeleteCustomProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.CustomProcesses' where objects = 'IntegrationManager.JobFlows.EditCustomProcesses';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.Input' where objects = 'IntegrationManager.JobFlows.AddCloneInputJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.Input' where objects = 'IntegrationManager.JobFlows.DeleteInputJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.Input' where objects = 'IntegrationManager.JobFlows.EditInputJobs';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.Link' where objects = 'IntegrationManager.JobFlows.AddCloneJobFlowJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.Link' where objects = 'IntegrationManager.JobFlows.DeleteJobFlowJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.Link' where objects = 'IntegrationManager.JobFlows.EditJobFlowJobs';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.MonthEndProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneMonthEndProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.MonthEndProcesses' where objects = 'IntegrationManager.JobFlows.DeleteMonthEndProcesses';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.MonthEndProcesses' where objects = 'IntegrationManager.JobFlows.EditMonthEndProcesses';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.Notification' where objects = 'IntegrationManager.JobFlows.AddCloneNotification';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.Notification' where objects = 'IntegrationManager.JobFlows.DeleteNotification';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.Notification' where objects = 'IntegrationManager.JobFlows.EditNotification';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.SQL' where objects = 'IntegrationManager.JobFlows.AddCloneSQLJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.SQL' where objects = 'IntegrationManager.JobFlows.DeleteSQLJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.SQL' where objects = 'IntegrationManager.JobFlows.EditSQLJobs';

update pp_web_security_perm_control set objects = 'JobService.Workflow.Addworkflow.Service' where objects = 'IntegrationManager.JobFlows.AddCloneServiceJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Deleteworkflow.Service' where objects = 'IntegrationManager.JobFlows.DeleteServiceJobs';
update pp_web_security_perm_control set objects = 'JobService.Workflow.Editworkflow.Service' where objects = 'IntegrationManager.JobFlows.EditServiceJobs';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.BudgetProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneBudgetProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.BudgetProcesses' where objects = 'IntegrationManager.JobFlows.DeleteBudgetProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.BudgetProcesses' where objects = 'IntegrationManager.JobFlows.EditBudgetProcesses';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.CRSystemProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneCRSystemProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.CRSystemProcesses' where objects = 'IntegrationManager.JobFlows.DeleteCRSystemProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.CRSystemProcesses' where objects = 'IntegrationManager.JobFlows.EditCRSystemProcesses';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.CustomProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneCustomProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.CustomProcesses' where objects = 'IntegrationManager.JobFlows.DeleteCustomProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.CustomProcesses' where objects = 'IntegrationManager.JobFlows.EditCustomProcesses';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.Input' where objects = 'IntegrationManager.JobFlows.AddCloneInputJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.Input' where objects = 'IntegrationManager.JobFlows.DeleteInputJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.Input' where objects = 'IntegrationManager.JobFlows.EditInputJobs';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.Link' where objects = 'IntegrationManager.JobFlows.AddCloneJobFlowJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.Link' where objects = 'IntegrationManager.JobFlows.DeleteJobFlowJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.Link' where objects = 'IntegrationManager.JobFlows.EditJobFlowJobs';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.MonthEndProcesses' where objects = 'IntegrationManager.JobFlows.AddCloneMonthEndProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.MonthEndProcesses' where objects = 'IntegrationManager.JobFlows.DeleteMonthEndProcesses';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.MonthEndProcesses' where objects = 'IntegrationManager.JobFlows.EditMonthEndProcesses';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.Notification' where objects = 'IntegrationManager.JobFlows.AddCloneNotification';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.Notification' where objects = 'IntegrationManager.JobFlows.DeleteNotification';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.Notification' where objects = 'IntegrationManager.JobFlows.EditNotification';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.SQL' where objects = 'IntegrationManager.JobFlows.AddCloneSQLJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.SQL' where objects = 'IntegrationManager.JobFlows.DeleteSQLJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.SQL' where objects = 'IntegrationManager.JobFlows.EditSQLJobs';

update pp_web_security_permission set objects = 'JobService.Workflow.Addworkflow.Service' where objects = 'IntegrationManager.JobFlows.AddCloneServiceJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Deleteworkflow.Service' where objects = 'IntegrationManager.JobFlows.DeleteServiceJobs';
update pp_web_security_permission set objects = 'JobService.Workflow.Editworkflow.Service' where objects = 'IntegrationManager.JobFlows.EditServiceJobs';

insert into pp_web_security_perm_control(objects, module_id, component_id, section, name)
values ('JobService.Workflow.ImportWorkflow', 9, 7, 'Import/Export', 'Import Job Flows');
insert into pp_web_security_perm_control(objects, module_id, component_id, section, name)
values ('JobService.Workflow.ExportWorkflow', 9, 7, 'Import/Export', 'Export Job Flows');

update pp_web_security_perm_control set objects = 'JobService.Schedule.Delete.Service' where objects = 'JobService.Workflow.Delete.Service';
update pp_web_security_permission set objects = 'JobService.Schedule.Delete.Service' where objects = 'JobService.Workflow.Delete.Service';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3198, 0, 2016, 1, 0, 0, 45417, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045417_jobserver_update_jobflow_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
