/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045417_jobserver_remove_old_filewatcher_keys_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 05/24/2016 Jared Watkins    remove old security keys for file watcher page in integration manager
||============================================================================
*/
delete from pp_web_security_perm_control where objects = 'IntegrationManager.FileWatcher.AddClone';
delete from pp_web_security_perm_control where objects = 'IntegrationManager.FileWatcher.Edit';
delete from pp_web_security_perm_control where objects = 'IntegrationManager.FileWatcher.Delete';

update pp_web_security_permission set objects = 'JobService.FileWatcher.AddClone' where objects = 'IntegrationManager.FileWatcher.AddClone';
update pp_web_security_permission set objects = 'JobService.FileWatcher.Edit' where objects = 'IntegrationManager.FileWatcher.Edit';
update pp_web_security_permission set objects = 'JobService.FileWatcher.Delete' where objects = 'IntegrationManager.FileWatcher.Delete';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3199, 0, 2016, 1, 0, 0, 045417, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045417_jobserver_remove_old_filewatcher_keys_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;