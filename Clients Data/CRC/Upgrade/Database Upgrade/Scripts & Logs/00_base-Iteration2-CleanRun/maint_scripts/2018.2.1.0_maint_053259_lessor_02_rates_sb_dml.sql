/*
||============================================================================
|| Application: PowerPlan
|| File Name: 2018.2.1.0_maint_053259_lessor_02_rates_sb_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2018.2.1.0 03/22/2019 B. Beck    	Populate the lsr_ilr_rates conversion table
||============================================================================
*/

insert into lsr_ilr_rates_conversion
(
	ilr_id, revision, set_of_books_id, rate_type_id, rate, time_stamp, user_id
)
select rates.ilr_id, rates.revision, comp_books.set_of_books_id,
	rates.rate_type_id, rates.rate, rates.time_stamp, rates.user_id
from lsr_ilr_rates rates
join lsr_ilr ilr on ilr.ilr_id = rates.ilr_id
join company_set_of_books comp_books on comp_books.company_id = ilr.company_id
order by 1, 2, 4, 3;

--***********************************************
--Log the run of the script PP_SCHEMA_CHANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (16165, 0, 2018, 2, 1, 0, 53259 , 'C:\PlasticWks\powerplant\sql\maint_scripts','2018.2.1.0_maint_053259_lessor_02_rates_sb_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), 
	SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;