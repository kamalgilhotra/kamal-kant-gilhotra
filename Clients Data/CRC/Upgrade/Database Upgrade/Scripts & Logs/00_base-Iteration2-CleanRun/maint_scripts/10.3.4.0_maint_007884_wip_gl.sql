/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007884_wip_gl.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   03/01/2012 Sunjin Cone    Point Release
||============================================================================
*/

insert into PP_JOURNAL_KEYWORDS
   (KEYWORD, DESCRIPTION, SQLS, KEYWORD_TYPE, VALID_ELEMENTS, BIND_ARG_CODE_ID1, BIND_ARG_CODE_ID2,
    BIND_ARG_CODE_ID3, BIND_ARG_CODE_ID4, TIME_STAMP, USER_ID)
values
   ('p_wip', 'External CWIP or WIP Account based on pend_trans_id for Credit JE',
    'SELECT gl.external_account_code FROM work_order_account woa, gl_account gl, wip_computation wc, (SELECT nvl(decode(wip_computation_id, null, (select wip_computation_id from pend_transaction where pend_trans_id = <arg3>), wip_computation_id), -888)  wip_comp_id from cpr_ledger where asset_id = <arg1>) wip_comp WHERE  woa.work_order_id =  <arg2> and wc.wip_computation_id (+) = wip_comp.wip_comp_id and decode(wip_comp.wip_comp_id, -888, woa.CWIP_GL_ACCOUNT,  wc.WIP_GL_ACCOUNT) = gl.gl_account_id ',
    1, 'all', 2, 5, 8, null, sysdate, user);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (102, 0, 10, 3, 4, 0, 7884, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_007884_wip_gl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
