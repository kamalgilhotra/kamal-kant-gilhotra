/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043822_reg_calc_int_exp_accts_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 05/08/2015 Sarah Byers    Add data needed for interest sync calc accts
||============================================================================
*/

-- Add calculated sub account types for expense cap structure items
merge into reg_sub_acct_type r 
using ( 
		  select 11 reg_acct_type_id, 
					capital_structure_id sub_acct_type_id, 
					substr(trim(description),1, 28)||' (Calc)' description, 
					description||' (Calculated)' long_description, 
					capital_structure_id capital_structure_id, 
					capital_structure_id sort_order 
			  from reg_capital_structure_control
			 where capital_struct_indicator_id = 6
		) d 
on ( 
		    r.reg_acct_type_id = d.reg_acct_type_id 
		and r.sub_acct_type_id = d.sub_acct_type_id 
	) 
when matched then 
	update set r.description = d.description, 
				  r.long_description = d.long_description 
when not matched then 
	insert (reg_acct_type_id, sub_acct_type_id, description, long_description, capital_structure_id, sort_order) 
	values (d.reg_acct_type_id, d.sub_acct_type_id, d.description, d.long_description, d.capital_structure_id, d.sort_order); 

-- Add calculated accounts for expense cap structure items 
merge into reg_acct_master r 
using ( 
		  select 11 reg_acct_type_default, 
					capital_structure_id sub_acct_type_id, 
					description||' (Calculated)' description, 
					description||' (Calculated)' long_description, 
					0 reg_source_id, 
					1 reg_annualization_id, 
					4 acct_good_for 
			  from reg_capital_structure_control
			 where capital_struct_indicator_id = 6
		) d 
on ( 
		    r.reg_acct_type_default = d.reg_acct_type_default 
		and r.sub_acct_type_id = d.sub_acct_type_id 
	) 
when matched then 
	update set r.description = d.description, 
				  r.long_description = d.long_description 
when not matched then 
	insert (reg_acct_id, description, long_description, reg_source_id, reg_acct_type_default, sub_acct_type_id, reg_annualization_id, acct_good_for) 
	values (ppseq_reg_acct_id.nextval, d.description, d.long_description, d.reg_source_id, d.reg_acct_type_default, d.sub_acct_type_id, 
			  d.reg_annualization_id, d.acct_good_for);

-- Add dynamic allocators for expense cap structure items 
merge into reg_allocator r 
using ( 
		  select (select max(nvl(reg_allocator_id,0)) from reg_allocator) + rownum reg_allocator_id, 
					substr(trim(description),1, 28)||' (Calc)' description, 
					'Cap Structure' alloc_factor_code, 
					3 factor_type, 
					capital_structure_id capital_structure_id 
			  from reg_capital_structure_control
			 where capital_struct_indicator_id = 6
		) d 
on ( 
		    r.capital_structure_id = d.capital_structure_id 
	) 
when matched then 
	update set r.description = d.description 
when not matched then 
	insert (reg_allocator_id, description, alloc_factor_code, factor_type, capital_structure_id) 
	values (d.reg_allocator_id, d.description, d.alloc_factor_code, d.factor_type, d.capital_structure_id);

-- Add interest expense sub account types related to expense cap structure items to 
-- the dynamic allocators for expense cap structure items
insert into reg_allocator_dyn (
	reg_allocator_id, row_number, reg_acct_type_id, sub_acct_type_id, sign)
SELECT reg_allocator_id, Row_Number+ROWNUM, reg_acct_type_id, sub_acct_type_id, Sign
FROM (select a.reg_allocator_id, 
		 max(nvl(d.row_number,0)) row_number,
		 s.reg_acct_type_id,
		 s.sub_acct_type_id,
		 '+' sign
  from reg_allocator a, reg_allocator_dyn d, reg_sub_acct_type s
 where a.capital_structure_id = s.capital_structure_id
	and a.factor_type = 3
	and s.reg_acct_type_id <> 11
	and a.reg_allocator_id = d.reg_allocator_id (+)
	and s.capital_structure_id in (
		select capital_structure_id from reg_capital_structure_control
		 where capital_struct_indicator_id = 6)
 group by a.reg_allocator_id, s.reg_acct_type_id, s.sub_acct_type_id);

-- Map calculated accounts for expense cap structure items to jur templates
insert into reg_jur_acct_default (
	reg_jur_template_id, reg_acct_id, reg_acct_type_id, sub_acct_type_id, reg_annualization_id, relation_id)
select j.reg_jur_template_id, m.reg_acct_id, m.reg_acct_type_default, m.sub_acct_type_id, m.reg_annualization_id, null
  from reg_acct_master m, reg_jur_template j
 where m.reg_acct_type_default = 11
minus
select reg_jur_template_id, reg_acct_id, reg_acct_type_id, sub_acct_type_id, reg_annualization_id, null
  from reg_jur_acct_default
 where reg_acct_type_id = 11;

-- Map calculated accounts for expense cap structure items to cases
insert into reg_case_acct (
	reg_case_id, reg_acct_id, reg_acct_type_id, sub_acct_type_id, status_code_id, reg_annualization_id, note)
select c.reg_case_id, m.reg_acct_id, m.reg_acct_type_default, m.sub_acct_type_id, 1, m.reg_annualization_id, m.description
  from reg_acct_master m, reg_case c
 where m.reg_acct_type_default = 11
minus
select reg_case_id, reg_acct_id, reg_acct_type_id, sub_acct_type_id, status_code_id, reg_annualization_id, note
  from reg_case_acct
 where reg_acct_type_id = 11;

-- Add the new adjustment type
insert into reg_adjust_types (
	adjustment_type_id, description)
values (
	13, 'Interest Synchronization');



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2558, 0, 2015, 2, 0, 0, 43822, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043822_reg_calc_int_exp_accts_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;