/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032548_lease_interim_interest.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Kyle Peterson  Point Release
||============================================================================
*/

insert into LS_PAYMENT_TERM_TYPE
   (PAYMENT_TERM_TYPE_ID, TIME_STAMP, USER_ID, DESCRIPTION, LONG_DESCRIPTION)
values
   (1, null, null, 'Interim Interest', 'Interim Interest');

alter table LS_ILR_PAYMENT_TERM add MAKE_II_PAYMENT number(1,0) default 0;

alter table LS_ILR_PAYMENT_TERM add INTERIM_INTEREST_BEGIN_DATE date;

comment on column LS_ILR_PAYMENT_TERM.MAKE_II_PAYMENT is 'Indicates whether an interim interest term makes a payment in the same month. Defaults to 0';
comment on column LS_ILR_PAYMENT_TERM.INTERIM_INTEREST_BEGIN_DATE is 'The date on which interim interest begins for an interim interest payment term.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (650, 0, 10, 4, 1, 1, 32548, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032548_lease_interim_interest.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
