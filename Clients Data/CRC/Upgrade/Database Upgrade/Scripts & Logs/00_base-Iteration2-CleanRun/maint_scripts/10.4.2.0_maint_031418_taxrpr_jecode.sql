/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031418_taxrpr_jecode.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.0 08/26/2013 Alex Pivoshenko  Point Release
||============================================================================
*/

update STANDARD_JOURNAL_ENTRIES
   set GL_JE_CODE = 'TAX REPAIRS RETS', EXTERNAL_JE_CODE = 'TAX REPAIRS RETS'
 where LOWER(GL_JE_CODE) = 'tax expense retirements';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (680, 0, 10, 4, 2, 0, 31418, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031418_taxrpr_jecode.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
