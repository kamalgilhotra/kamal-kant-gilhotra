/*
||============================================================================
|| Application: PowerPlan
|| File Name:  maint_050396_lessor_01_add_lessor_retire_gain_loss_trans_types_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/25/2018 Charlie Shilling Need new retirement and gain_loss trans types for lessor derecognition and rerecognition
||============================================================================
*/
INSERT INTO je_trans_type (trans_type, description)
VALUES(4025, '4025 - Lessor Retirement Debit (POST)')
;

INSERT INTO je_trans_type (trans_type, description)
VALUES(4026, '4026 - Lessor Retirement Credit (POST)')
;

INSERT INTO je_trans_type (trans_type, description)
VALUES(4030, '4030 - Lessor Gain/Loss Debit (POST)')
;

INSERT INTO je_trans_type (trans_type, description)
VALUES(4031, '4031 - Lessor Gain/Loss Credit (POST)')
;

--***********************************************
--Log the run of the script PP_SCHEMA_CGANGE_LOG
--***********************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7164, 0, 2017, 4, 0, 0, 50396, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_050396_lessor_01_add_lessor_retire_gain_loss_trans_types_dml.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;