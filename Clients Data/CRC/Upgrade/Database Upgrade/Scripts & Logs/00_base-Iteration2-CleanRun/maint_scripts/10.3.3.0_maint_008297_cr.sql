/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008297_cr.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/31/2011 Joseph King    Point Release
||============================================================================
*/

insert into CR_ALLOC_SYSTEM_CONTROL
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, LONG_DESCRIPTION)
   select NVL((select max(CONTROL_ID) from CR_ALLOC_SYSTEM_CONTROL), 0) + 1,
          'CR to CWIP Charge End Month',
          'Current Open Month',
          'Max month to process from CR to CWIP Charge.  Either a month number in ''yyyymm'' format or ''Current Open Month'' to default to the current open month.'
     from DUAL A
    where not exists (select 1
             from CR_ALLOC_SYSTEM_CONTROL
            where UPPER(trim(CONTROL_NAME)) = 'CR TO CWIP CHARGE END MONTH');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (25, 0, 10, 3, 3, 0, 8297, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008297_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
