/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_049358_lease_01_field_userid_ts_cols_pymtinv_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2016.1.2.0  06/26/2017 build script   2016.1.2.0 Patch Release
||============================================================================
*/

ALTER TABLE ls_invoice ADD user_id VARCHAR2(18);
ALTER TABLE ls_invoice ADD time_stamp DATE;

ALTER TABLE ls_invoice_line ADD user_id VARCHAR2(18);
ALTER TABLE ls_invoice_line ADD time_stamp DATE;

ALTER TABLE ls_invoice_payment_map ADD user_id VARCHAR2(18);
ALTER TABLE ls_invoice_payment_map ADD time_stamp DATE;

CREATE OR REPLACE TRIGGER ls_invoice
   before update or insert on ls_invoice
   for each row
begin    :new.user_id := SYS_CONTEXT ('USERENV', 'SESSION_USER');    :new.time_stamp := SYSDATE; end;
/

CREATE OR REPLACE TRIGGER ls_invoice_line
   before update or insert on ls_invoice_line
   for each row
begin    :new.user_id := SYS_CONTEXT ('USERENV', 'SESSION_USER');    :new.time_stamp := SYSDATE; end;
/

CREATE OR REPLACE TRIGGER ls_invoice_payment_map
   before update or insert on ls_invoice_payment_map
   for each row
begin    :new.user_id := SYS_CONTEXT ('USERENV', 'SESSION_USER');    :new.time_stamp := SYSDATE; end;
/

ALTER TABLE ls_payment_hdr ADD user_id VARCHAR2(18);
ALTER TABLE ls_payment_hdr ADD time_stamp DATE;

ALTER TABLE ls_payment_line ADD user_id VARCHAR2(18);
ALTER TABLE ls_payment_line ADD time_stamp DATE;

CREATE OR REPLACE TRIGGER ls_payment_line
   before update or insert on ls_payment_line
   for each row
begin    :new.user_id := SYS_CONTEXT ('USERENV', 'SESSION_USER');    :new.time_stamp := SYSDATE; end;
/

CREATE OR REPLACE TRIGGER ls_payment_hdr
   before update or insert on ls_payment_hdr
   for each row
begin    :new.user_id := SYS_CONTEXT ('USERENV', 'SESSION_USER');    :new.time_stamp := SYSDATE; end;
/
   
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4024, 0, 2017, 1, 0, 0, 49358, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049358_lease_01_field_userid_ts_cols_pymtinv_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;