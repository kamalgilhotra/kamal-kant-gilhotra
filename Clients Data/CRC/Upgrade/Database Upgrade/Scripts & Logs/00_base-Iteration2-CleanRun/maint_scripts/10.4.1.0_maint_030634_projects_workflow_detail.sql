/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030634_projects_workflow_detail.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/04/2013 David Nichols  Point release
||============================================================================
*/

alter table WORKFLOW_DETAIL modify NOTES varchar2(4000);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (578, 0, 10, 4, 1, 0, 30634, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030634_projects_workflow_detail.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
