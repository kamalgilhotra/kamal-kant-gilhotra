/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008350_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   10/03/2011 Blake Andrews  Point Release
||============================================================================
*/

--###PATCH(8350)

insert into TAX_ACCRUAL_MONTH_TYPE
   (MONTH_TYPE_ID, DESCRIPTION, ACTIVITY_TYPE_ID, BEG_BAL_IND, LONG_DESCRIPTION, DISABLED)
   select 200, 'Quarter', 2, 1, 'Use this for quarter-closing months.', 0
     from DUAL
   union all
   select 201, 'Off-Month', 2, 1, 'Use this for non-quarter-closing months.', 0
     from DUAL
   union all
   select 202, 'December', 2, 1, 'Use this for year-ending months.', 0 from DUAL;

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID, TRUEUP_ID, PT_IND, 0, 1, 1, 1, 1
     from TAX_ACCRUAL_PT_IND, TAX_ACCRUAL_TRUEUP, TAX_ACCRUAL_MONTH_TYPE
    where TRUEUP_ID in (1, 2, 3)
      and MONTH_TYPE_ID in (200, 201, 202);

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID, TRUEUP_ID, PT_IND, 1, 1, 1, 1, 0
     from TAX_ACCRUAL_PT_IND, TAX_ACCRUAL_TRUEUP, TAX_ACCRUAL_MONTH_TYPE
    where TRUEUP_ID in (5, 6, 7, 8, 101, 102, 103)
      and MONTH_TYPE_ID in (200, 201);

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID, TRUEUP_ID, PT_IND, 0, 1, 1, 1, 1
     from TAX_ACCRUAL_PT_IND, TAX_ACCRUAL_TRUEUP, TAX_ACCRUAL_MONTH_TYPE
    where TRUEUP_ID in (5, 6, 7, 8)
      and MONTH_TYPE_ID in (202);

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID, TRUEUP_ID, PT_IND, 0, 1, 1, 1, 0
     from TAX_ACCRUAL_PT_IND, TAX_ACCRUAL_TRUEUP, TAX_ACCRUAL_MONTH_TYPE
    where TRUEUP_ID in (101, 102, 103)
      and MONTH_TYPE_ID in (202);

insert into TAX_ACCRUAL_TRUEUP
   (TRUEUP_ID, DESCRIPTION, M_ID, BY_OPER_IND, SPREAD_REMAINING_ESTIMATE, TRUEUP_INCLUDES_INPUT,
    TRUEUP_INCLUDES_INPUT_DT)
values
   (9, 'PTBI% - Quarter', -99, 0, 0, 0, 0);

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID,
          9,
          FROM_PT_IND,
          CURRENT_MONTH_TRUEUP,
          OUT_MONTH_TRUEUP,
          CURRENT_MONTH_TRUEUP_DT,
          OUT_MONTH_TRUEUP_DT,
          PULL_ACTUALS
     from TAX_ACCRUAL_MONTH_TYPE_TRUEUP
    where TRUEUP_ID = 5;

update TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   set CURRENT_MONTH_TRUEUP = 0, OUT_MONTH_TRUEUP = 0, CURRENT_MONTH_TRUEUP_DT = 0,
       OUT_MONTH_TRUEUP_DT = 0
 where MONTH_TYPE_ID = 201;

insert into TAX_ACCRUAL_TRUEUP
   (TRUEUP_ID, DESCRIPTION, M_ID, BY_OPER_IND, SPREAD_REMAINING_ESTIMATE, TRUEUP_INCLUDES_INPUT,
    TRUEUP_INCLUDES_INPUT_DT)
values
   (20, 'Pull Off-Months', -99, 0, 0, 0, 0);

insert into TAX_ACCRUAL_TRUEUP
   (TRUEUP_ID, DESCRIPTION, M_ID, BY_OPER_IND, SPREAD_REMAINING_ESTIMATE, TRUEUP_INCLUDES_INPUT,
    TRUEUP_INCLUDES_INPUT_DT)
values
   (21, 'Pull Quarters', -99, 0, 0, 0, 0);

insert into TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   (MONTH_TYPE_ID, TRUEUP_ID, FROM_PT_IND, CURRENT_MONTH_TRUEUP, OUT_MONTH_TRUEUP,
    CURRENT_MONTH_TRUEUP_DT, OUT_MONTH_TRUEUP_DT, PULL_ACTUALS)
   select MONTH_TYPE_ID, TRUEUP_ID, PT_IND, 0, 0, 0, 0, 1
     from TAX_ACCRUAL_MONTH_TYPE, TAX_ACCRUAL_TRUEUP, TAX_ACCRUAL_PT_IND
    where TRUEUP_ID in (20, 21);

update TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   set PULL_ACTUALS = 0
 where MONTH_TYPE_ID = 201
   and TRUEUP_ID = 21;

update TAX_ACCRUAL_MONTH_TYPE_TRUEUP
   set PULL_ACTUALS = 0
 where MONTH_TYPE_ID in (200, 202)
   and TRUEUP_ID = 20;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (7, 0, 10, 3, 3, 0, 8350, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008350_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
