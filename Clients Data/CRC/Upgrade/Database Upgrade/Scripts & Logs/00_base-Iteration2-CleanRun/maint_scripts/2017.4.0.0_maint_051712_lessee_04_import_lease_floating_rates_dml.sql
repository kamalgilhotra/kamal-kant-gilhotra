/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_051712_lessee_04_import_lease_floating_rates_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.4.0.0 06/26/2018 Shane "C" Ward  Updates to Import Type and Template for new Floating Rates table
||============================================================================
*/

--Wipe Templates for ILR Group Floating Rates
DELETE FROM pp_import_template_fields WHERE import_template_id IN (SELECT import_template_id FROM pp_import_template WHERE import_type_id = 257);

INSERT INTO pp_Import_template_fields
  (import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  SELECT import_template_id, 1, 257, 'lease_id', 1042 FROM pp_import_template WHERE import_type_id = 257;

INSERT INTO pp_Import_template_fields
  (import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  SELECT import_template_id, 2, 257, 'effective_date', NULL FROM pp_import_template WHERE import_type_id = 257;

INSERT INTO pp_Import_template_fields
  (import_template_id, field_id, import_type_id, column_name, import_lookup_id)
  SELECT import_template_id, 3, 257, 'rate', NULL FROM pp_import_template WHERE import_type_id = 257;

--Get ILR Group off the Import columns

DELETE FROM pp_import_column_lookup
 WHERE import_type_id = 257
   AND Lower(column_name) = 'ilr_group_id';

DELETE FROM pp_import_column
 WHERE import_type_id = 257
   AND Lower(column_name) = 'ilr_group_id';

--Add missing Lease ID Lookup
INSERT INTO pp_import_column_lookup (import_type_id, column_name, import_lookup_id) VALUES (257, 'lease_id', 1042);

--Rename Import since ILR Group Floating Rates doesnt make sense
UPDATE pp_import_type
   SET description      = 'Add: Lease Floating Rates',
       long_description = 'Lease Floating Rates'
 WHERE import_type_id = 257;

--Set effective date column type to mmddyyyy to help users with templates
UPDATE pp_import_column
   SET column_type = 'date mmddyyyy format'
 WHERE column_name = 'effective_date'
   AND import_type_id = 257;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7625, 0, 2017, 4, 0, 0, 51712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051712_lessee_04_import_lease_floating_rates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;