/*
||========================================================================================
|| Application: PowerPlant
|| File Name:   maint_039285_lease_stage_pay.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 10.4.3.0 07/30/2014 Brandon Beck
||========================================================================================
*/

insert into PPBASE_ACTIONS_WINDOWS
   (ID, MODULE, ACTION_IDENTIFIER, ACTION_TEXT, ACTION_ORDER, ACTION_EVENT)
values
   (19, 'LESSEE', 'stage_payments', 'Stage Payments', 8, 'ue_stagepayments');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1301, 0, 10, 4, 3, 0, 39285, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039285_lease_stage_pay.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;