/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_053017_taxrpr_add_sytem_option_values_dml.sql
||============================================================================
|| Copyright (C) 2019 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2018.2.0.0 01/31/2019 Anand R          Add Yes, No options to tax repairs system option
||============================================================================
*/

insert into ppbase_system_options_values (SYSTEM_OPTION_ID, OPTION_VALUE)
values ('Repairs - Bulk Evaluate Tax Status', 'No');

insert into ppbase_system_options_values (SYSTEM_OPTION_ID, OPTION_VALUE)
values ('Repairs - Bulk Evaluate Tax Status', 'Yes');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(14462, 0, 2018, 2, 0, 0, 53017, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.2.0.0_maint_053017_taxrpr_add_sytem_option_values_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
