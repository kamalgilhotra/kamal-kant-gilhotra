/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049461_lessee_02_mla_import_update_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/12/2017 Shane "C" Ward    Updates to Lessee ILR Import
||============================================================================
*/

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 251,
             'specialized_asset',
             NULL,
             NULL,
             'Specialized Asset',
             'specialized_asset_xlate',
             1,
             1,
             'number(22,0)',
             'yes_no',
             NULL,
             1,
             NULL,
             'yes_no_id',
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 251,
             'intent_to_purchase',
             NULL,
             NULL,
             'Intent to Purchase',
             'intent_to_purchase_xlate',
             1,
             1,
             'number(22,0)',
             'yes_no',
             NULL,
             1,
             NULL,
             'yes_no_id',
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 251,
             'sublease',
             NULL,
             NULL,
             'Sublease',
             'sublease_xlate',
             1,
             1,
             'number(22,0)',
             'yes_no',
             NULL,
             1,
             NULL,
             'yes_no_id',
             NULL,
             NULL );

INSERT INTO PP_IMPORT_COLUMN
            (import_type_id,
             column_name,
             time_stamp,
             user_id,
             description,
             import_column_name,
             is_required,
             processing_order,
             column_type,
             parent_table,
             parent_table_pk_column2,
             is_on_table,
             autocreate_import_type_id,
             parent_table_pk_column,
             default_value,
             parent_table_pk_column3)
VALUES      ( 251,
             'sublease_lease_id',
             NULL,
             NULL,
             'Sublease - Lessor Lease Number',
             'sublease_lease_xlate',
             0,
             1,
             'number(22,0)',
             'lsr_lease',
             NULL,
             1,
             NULL,
             'lease_id',
             NULL,
             NULL );

--Sublease Lookups
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'sublease_lease_id',
             1083);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'sublease_lease_id',
             1084);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'sublease_lease_id',
             1085);

--Specialized Asset Lookup
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'specialized_asset',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'specialized_asset',
             78);

--Intent to Purchase Lookup
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'intent_to_purchase',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'intent_to_purchase',
             78);

--Sublease Flag Lookup
INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'sublease',
             77);

INSERT INTO PP_IMPORT_COLUMN_LOOKUP
            (import_type_id,
             column_name,
             import_lookup_id)
VALUES      (251,
             'sublease',
             78);

--PP_IMPORT_TEMPLATE_FIELDS
UPDATE PP_IMPORT_TEMPLATE_FIELDS
SET    field_id = field_id + 4
WHERE  import_type_id = 251 AND
       import_template_id IN
       ( SELECT import_template_id
         FROM   PP_IMPORT_TEMPLATE
         WHERE  description = 'MLA Add' ) AND
       field_id > 21;

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       22,
       251,
       'specialized_asset',
       77
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 251 AND
       description = 'MLA Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       23,
       251,
       'intent_to_purchase',
       77
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 251 AND
       description = 'MLA Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       24,
       251,
       'sublease',
       77
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 251 AND
       description = 'MLA Add';

INSERT INTO PP_IMPORT_TEMPLATE_FIELDS
            (import_template_id,
             field_id,
             import_type_id,
             column_name,
             import_lookup_id)
SELECT import_template_id,
       25,
       251,
       'sublease_lease_id',
       1083
FROM   PP_IMPORT_TEMPLATE
WHERE  import_type_id = 251 AND
       description = 'MLA Add';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3937, 0, 2017, 1, 0, 0, 49461, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049461_lessee_02_mla_import_update_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;