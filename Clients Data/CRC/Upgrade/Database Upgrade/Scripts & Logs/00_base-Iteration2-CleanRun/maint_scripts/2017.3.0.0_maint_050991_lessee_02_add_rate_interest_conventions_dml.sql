/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_050991_lessee_02_add_rate_interest_conventions_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/23/2018 Josh Sandler 	  Add rate convention and interst convention
||============================================================================
*/

INSERT INTO ls_rate_convention (rate_convention_id, description)
SELECT 1, 'Simple Rate' FROM dual
UNION ALL
SELECT 2, 'Effective Period Interest' FROM dual;

INSERT INTO ls_interest_convention (interest_convention_id, description)
SELECT 1, 'Interest Method' FROM dual
UNION ALL
SELECT 2, 'Compounding Interest' FROM dual;


-- Prior to PP-50858 this was the system methodology. Default to these options.
UPDATE ls_lease_cap_type
SET rate_convention_id = 1,
interest_convention_id = 1;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4623, 0, 2017, 3, 0, 0, 50991, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_050991_lessee_02_add_rate_interest_conventions_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;