/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_009387_cr_data_mover.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.0   04/12/2012 Joseph King    Point Release
||============================================================================
*/

create table CR_DATA_MOVER_MAP
(
 CR_DATA_MOVER_MAP_ID number(22,0) not null,
 DESCRIPTION          varchar2(254) not null,
 TO_TABLE_NAME        varchar2(30) not null,
 FROM_TABLE_NAME      varchar2(30) not null,
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table CR_DATA_MOVER_MAP
   add constraint CR_DATA_MOVER_MAP_PK
       primary key (CR_DATA_MOVER_MAP_ID)
       using index tablespace PWRPLANT_IDX;

create unique index CR_DATA_MOVER_MAP_DESCR
   on CR_DATA_MOVER_MAP (DESCRIPTION)
      tablespace PWRPLANT_IDX;

alter table CR_DATA_MOVER add CR_DATA_MOVER_MAP_ID number(22,0);

alter table CR_DATA_MOVER
   add constraint CR_DATA_MOVER_MAP_FK
       foreign key (CR_DATA_MOVER_MAP_ID)
       references CR_DATA_MOVER_MAP (CR_DATA_MOVER_MAP_ID);

create table CR_DATA_MOVER_MAP_COLUMNS
(
 CR_DATA_MOVER_MAP_ID number(22,0) not null,
 TO_COLUMN_NAME       varchar2(30) not null,
 FROM_COLUMN_NAME     varchar2(2000),
 USER_ID              varchar2(18),
 TIME_STAMP           date,
 TYPE                 varchar2(35) not null,
 WIDTH                number(22,0) not null,
 "DECIMAL"            number(22,0),
 "ORDER"              number(22,0) not null,
 PRIMARY_KEY          number(22,0)
);

alter table CR_DATA_MOVER_MAP_COLUMNS
   add constraint CR_DATA_MOVER_MAP_COLUMNS_PK
       primary key (CR_DATA_MOVER_MAP_ID, TO_COLUMN_NAME)
       using index tablespace PWRPLANT_IDX;

alter table CR_DATA_MOVER_MAP_COLUMNS
   add constraint CR_DATA_MOVER_MAP_COLUMNS_FK1
       foreign key (CR_DATA_MOVER_MAP_ID)
       references CR_DATA_MOVER_MAP (CR_DATA_MOVER_MAP_ID);

create table CR_DATA_MOVER_FILTER
(
 CR_DATA_MOVER_FILTER_ID number(22,0) not null,
 DESCRIPTION             varchar2(254) not null,
 USER_ID                 varchar2(18),
 TIME_STAMP              date
);

alter table CR_DATA_MOVER_FILTER
   add constraint CR_DATA_MOVER_FILTER_PK
       primary key (CR_DATA_MOVER_FILTER_ID)
       using index tablespace PWRPLANT_IDX;

create unique index CR_DATA_MOVER_FILTER_DESCR
   on CR_DATA_MOVER_FILTER (DESCRIPTION)
      tablespace PWRPLANT_IDX;

alter table CR_DATA_MOVER add CR_DATA_MOVER_FILTER_ID number(22,0);

alter table CR_DATA_MOVER
   add constraint CR_DATA_MOVER_MAP_FK2
       foreign key (CR_DATA_MOVER_FILTER_ID)
       references CR_DATA_MOVER_FILTER (CR_DATA_MOVER_FILTER_ID);

create table CR_DATA_MOVER_FILTER_CLAUSE
(
 CR_DATA_MOVER_FILTER_ID number(22,0) not null,
 ROW_ID                  number(22,0) not null,
 USER_ID                 varchar2(18),
 TIME_STAMP              date,
 LEFT_PAREN              varchar2(1),
 COLUMN_NAME             varchar2(1000),
 OPERATOR                varchar2(35),
 VALUE1                  varchar2(1000),
 BETWEEN_AND             varchar2(3),
 VALUE2                  varchar2(1000),
 AND_OR                  varchar2(3),
 RIGHT_PAREN             varchar2(1)
);

alter table CR_DATA_MOVER_FILTER_CLAUSE
   add constraint CR_DATA_MOVER_FILTER_CLAUSE_PK
       primary key (CR_DATA_MOVER_FILTER_ID, ROW_ID)
       using index tablespace PWRPLANT_IDX;

alter table CR_DATA_MOVER_FILTER_CLAUSE
   add constraint CR_DATA_MOVER_FILTER_CLAUSE_FK
       foreign key (CR_DATA_MOVER_FILTER_ID)
       references CR_DATA_MOVER_FILTER (CR_DATA_MOVER_FILTER_ID);

alter table CR_DATA_MOVER add DESCRIPTION varchar2(254);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (114, 0, 10, 3, 4, 0, 9387, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.0_maint_009387_cr_data_mover.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
