/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030732_lease_ls_ilr_approval.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/12/2013 Kyle Peterson  Point Release
||============================================================================
*/

-- LS_ILR_APPROVAL

CREATE TABLE LS_ILR_APPROVAL
(
 ILR_ID              number(22,0) not null,
 REVISION            number(22,0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18),
 APPROVAL_TYPE_ID    number(22,0),
 APPROVAL_STATUS_ID  number(22,0),
 APPROVER            varchar2(18),
 APPROVAL_DATE       date,
 REJECTED            number(1,0),
 REVISION_DESC       varchar2(35),
 REVISION_LONG_DESC  varchar2(254)
);

alter table LS_ILR_APPROVAL
   add constraint PK_LS_ILR_APPROVAL
       primary key (ILR_ID, REVISION)
       using index tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (468, 0, 10, 4, 1, 0, 30732, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030732_lease_ls_ilr_approval.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
