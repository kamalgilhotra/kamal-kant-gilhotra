/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048626_lessor_01_delete_actual_payment_freq_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/15/2017 Anand R         Delete payment frequency 'Actual' from ls_payment_freq table
||============================================================================
*/

update ls_ilr_payment_term set payment_freq_id = 4 where payment_freq_id = 5;

delete from ls_payment_freq where payment_freq_id = 5;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3716, 0, 2017, 1, 0, 0, 48626, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048626_lessor_01_delete_actual_payment_freq_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
