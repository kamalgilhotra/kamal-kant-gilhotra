/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		maint_043637_job_executable_update_arguments.sql
|| File Name:   JOB Server.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1	  4/14/2015  MSmyth			     Job Executable Arguments Need Updated ;)
||==========================================================================================
*/

SET DEFINE OFF

ALTER TABLE "PWRPLANT"."PP_JOB_HISTORY" MODIFY ( "SERVER" NULL )

TRUNCATE TABLE "PWRPLANT"."PP_JOB_EXECUTABLE"; 

INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	15,
	'PowerBuilder',
	'CR Allocations',
	'cr_allocations.exe',
	'{"parms":[{"type":"long","value":"","label":"Start Priority","required":"true"},{"type":"long","value":"","label":"End Priority","required":"true"},{"type":"string","value":"Not Used for Actuals","label":"Budget Version","required":"true","hidden":"true"},{"type":"string","value":"ACTUAL","label":"Test or Actual","required":"true"},{"type":"long","value":"","label":"Start Month (yyyymm) ","required":"true"},{"type":"long","value":"","label":"End Month (yyyymm)","required":"true"},{"type":"string","value":"No","label":"Run As Commitments","required":"true"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	16,
	'PowerBuilder',
	'Automated Review',
	'automatedreview.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable
(job_executable_id, job_type, job_name, executable,arguments,category)
 VALUES (
	17,
	'PowerBuilder',
	'Batch Reporter',
	'batreporter.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable
(job_executable_id, job_type, job_name, executable,arguments,category)
 VALUES (
	18,
	'PowerBuilder',
	'CR Budget Allocations',
	'budget_allocations.exe',
	'{"parms":[{"type":"long","value":"","label":"Start Priority","required":"true"},{"type":"long","value":"","label":"End Priority","required":"true"},{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"ACTUAL","label":"Test or Actual","required":"true","hidden":"true"},{"type":"long","value":"","label":"Start Month (yyyymm)","required":"true"},{"type":"long","value":"","label":"End Month (yyyymm)","required":"true"}]}',
	'Budget Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	19,
	'PowerBuilder',
	'CR Balances',
	'cr_balances.exe',
	'{"parms":[{"type":"string","value":"","label":"Month Number(s)","required":"false"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	20,
	'PowerBuilder',
	'CR Balances Budget',
	'cr_balances_bdg.exe',
	NULL,
	'Budget Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	21,
	'PowerBuilder',
	'CR Batch Derivations',
	'cr_batch_derivation.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	22,
	'PowerBuilder',
	'CR Batch Derivation Budget',
	'cr_batch_derivation_bdg.exe',
	'{"parms":[{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"","label":"Derivation Type","required":"true"}, {"type":"long","value":"","label":"Start Month Number (yyyymm) ","required":"false"},{"type":"long","value":"","label":"End Month Number (yyyymm)","required":"false"},{"type":"string","value":"","label":"Company","required":"false"}]}',
	'Budget Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	23,
	'PowerBuilder',
	'CR Budget Entry Calc All',
	'cr_budget_entry_calc_all.exe',
	NULL,
	'Budget Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	24,
	'PowerBuilder',
	'CR Build Combos',
	'cr_build_combos.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	25,
	'PowerBuilder',
	'CR Build Deriver',
	'cr_build_deriver.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	26,
	'PowerBuilder',
	'CR Delete Budget Allocations',
	'cr_delete_budget_allocations.exe',
	'{"parms":[{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"","label":"Company ID","required":"true"}]}',
	'Budget Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	27,
	'PowerBuilder',
	'CR Financial Reports Build',
	'cr_financial_reports_build.exe',
	'{"parms":[{"type":"string","value":"","label":"User ID","required":"true"},{"type":"long","value":"","label":"Session ID","required":"true"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	28,
	'PowerBuilder',
	'CR Flatten Structures',
	'cr_flatten_structures.exe',
	'{"parms":[{"type":"long","value":"","label":"Structure IDs","required":"false"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable
(job_executable_id, job_type, job_name, executable,arguments,category)
 VALUES (
	29,
	'PowerBuilder',
	'CR Manual Journal Reversals',
	'cr_man_jrnl_reversals.exe',
	'{"parms":[{"type":"string","value":"","label":"Company","required":"false"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	30,
	'PowerBuilder',
	'CR Posting',
	'cr_posting.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	31,
	'PowerBuilder',
	'CR Posting Budget',
	'cr_posting_bdg.exe',
	'{"parms":[{"type":"string","value":"","label":"Budget Version","required":"true"},{"type":"string","value":"","label":"Posting IDs","required":"false"}]}',
	'Budget Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	32,
	'PowerBuilder',
	'CR SAP Trueup',
	'cr_sap_trueup.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	33,
	'PowerBuilder',
	'CR Sum',
	'cr_sum.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	34,
	'PowerBuilder',
	'CR Sum AB',
	'cr_sum_ab.exe',
	NULL,
	'Budget Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	35,
	'PowerBuilder',
	'CR To Commitments',
	'cr_to_commitments.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	36,
	'PowerBuilder',
	'CWIP Charge',
	'cwip_charge.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	37,
	'PowerBuilder',
	'Populate Material Reconciliation',
	'populate_matlrec.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	38,
	'PowerBuilder',
	'PP Integration',
	'pp_integration.exe',
	'{"parms":[{"type":"string","value":"","label":"PP Integration Command Line","required":"true"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	39,
	'PowerBuilder',
	'PP to CR',
	'pptocr.exe',
	'{"parms":[{"type":"long","value":"","label":"Company ID","required":"true"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	40,
	'PowerBuilder',
	'PP Verify',
	'ppverify.exe',
	NULL,
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	42,
	'SSP',
	'AFUDC & Overheads Approval',
	'ssp_wo_appr_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<true>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<true>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<true>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	43,
	'SSP',
	'AFUDC & Overheads Calculation',
	'ssp_wo_calc_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<true>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<true>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<true>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	44,
	'SSP',
	'ARO Approval',
	'ssp_aro_approve.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	45,
	'SSP',
	'ARO Calculation',
	'ssp_aro_calc.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	46,
	'SSP',
	'Accrual Approval',
	'ssp_wo_approve_accruals.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	47,
	'SSP',
	'Auto Non-Unitization',
	'ssp_wo_auto_nonunitize.exe non',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"true","label":"Non Unitization","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	48,
	'SSP',
	'Auto Unitization',
	'ssp_wo_auto_unitization.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type": "date","value": "","label": "Unitization Month","required": "true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	49,
	'SSP',
	'CPR Balance PowerPlan',
	'ssp_cpr_balance_pp.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month to Balance","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	57,
	'SSP',
	'Close CPR',
	'ssp_cpr_close_month.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"string","value":"<CLOSE>","label":"Task Type","hidden":"true"},{"type":"date","value":"","label":"Month to Close","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	58,
	'SSP',
	'WO Close Charge Collection',
	'ssp_wo_close_charge_collection.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month to Close","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	59,
	'SSP',
	'CPR Close PowerPlan',
	'ssp_close_powerplant.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"long2","value":"<2>","label":"Month Column","hidden":"true"},{"type":"date","value":"","label":"Month to Close","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	60,
	'SSP',
	'WO Close Month',
	'ssp_wo_close_month.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month to Close","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	61,
	'SSP',
	'Depreciation Approval',
	'ssp_depr_approval.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month to Approve","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	62,
	'SSP',
	'Depreciation Calculation',
	'ssp_depr_calc.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"string","value":"<LAUNCH>","label":"Depreciation Type","hidden":"true"},{"type":"date","value":"","label":"Month to Calculate","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	63,
	'SSP',
	'Depreciation Calculation: Lease',
	'ssp_depr_calc.exe IND:-100',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"long2","value":"<-100>","label":"Subledger Type","hidden":"true"},{"type":"string","value":"<INDIVIDUAL>","label":"Depreciation Type","hidden":"true"},{"type":"date","value":"","label":"Month to Calculate","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	66,
	'SSP',
	'CPR GLReconciliation',
	'ssp_gl_recon.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"long2","value":"<2>","label":"Month Column","required":"true","hidden":"true"},{"type":"date","value":"","label":"Month to Reconcile","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	67,
	'SSP',
	'WO GL Reconciliation',
	'ssp_wo_gl_reconciliation.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month to Reconcile ","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	70,
	'SSP',
	'WO New Month',
	'ssp_wo_new_month.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month to Close","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	71,
	'SSP',
	'CPR New Month',
	'ssp_cpr_new_month.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Previous Open Month","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	72,
	'SSP',
	'Re-Open CPR',
	'ssp_cpr_close_month.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"string","value":"<OPEN>","label":"Task Type","required":"true", "hidden":"true"},{"type":"date","value":"","label":"Month to Re-open","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	73,
	'SSP',
	'CPR Release Journal Entries',
	'ssp_release_je.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"long2","value":"<2>","label":"Month Column","hidden":"true"},{"type":"date","value":"","label":"Month to Release Entries","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	74,
	'SSP',
	'WO Release Journal Entries',
	'ssp_release_je_wo.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	75,
	'SSP',
	'Data Mover Batch',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Data Mover Batch", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Program" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	76,
	'SSP',
	'Data Mover Where',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Data Mover Where", "label": "Component","required": "yes","hidden": "yes"},"type": "string", "value": "", "label": "Where Clause (where batch like %) " }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	77,
	'SSP',
	'Create MX Locations',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Create MX Locations", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Batch / Instance" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	78,
	'SSP',
	'Create MX CU',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Create MX CU", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Batch / Instance" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	79,
	'SSP',
	'Create Work Order',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Create Work Order", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company Id (-1 for All Companies)" , "type": "string", "value": "", "label": "Batch / Instance" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	80,
	'SSP',
	'Create Job Task',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Create Job Task", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company Id (-1 for All Companies)" , "type": "string", "value": "", "label": "Batch / Instance" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	81,
	'SSP',
	'Load Unit Est',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Load Unit Est", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company Id (-1 for All Companies)" , "type": "string", "value": "", "label": "Replace Estimates (true / false)","type": "string", "value": "", "label": "Process OCR (true / false)","type": "string", "value": "", "label": "Batch / Instance"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	82,
	'SSP',
	'OCR Process',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "OCR Process", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Batch / Instance" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	83,
	'SSP',
	'PP Translate',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "PP Translate", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "PP Translate ID", "label": "Component","required": "yes","hidden": "no"}]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	84,
	'SSP',
	'CR Transactions',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "CR Transactions", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "API Data Set/Instance" ,"type": "string", "value": "", "label": "CR Source ID" , "type": "string", "value": "", "label": "Req. Balanced Entries (true / false)","type": "string", "value": "", "label": "Post to CWIP (true / false)","type": "string", "value": "", "label": "Post to GL (true / false)" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	85,
	'SSP',
	'Create Funding Project',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Create Funding Project", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company Id (-1 for All Companies)" , "type": "string", "value": "", "label": "Batch / Instance" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	86,
	'SSP',
	'Load Monthly Estimates',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Load Monthly Estimates", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Company Id (-1 for All Companies)" },{"type": "string", "value": "", "label": "Replace Estimates (true/false)"},{"type": "string", "value": "", "label": "Batch / Instance"},{"type": "string", "value": "", "label": "Replace Year (true / false)"},{"type": "string", "value": "", "label": "Funding WO Indicator" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	87,
	'SSP',
	'Reg External Source',
	'pp_integration.exe',
	'{"parms": [{"type": "string", "value": "Reg External Source", "label": "Component","required": "yes","hidden": "yes"},{"type": "string", "value": "", "label": "Historic Version" ,"type": "string", "value": "", "label": "Forecast Version" , "type": "string", "value": "", "label": "Start Month","type": "string", "value": "", "label": "End Month" }]}',
	'CR/System Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	88,
	'SSP',
	'Accrual Calculation',
	'ssp_wo_calculate_accruals.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month to Calculate","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	89,
	'SSP',
	'WO Retirements',
	'ssp_wo_retirements.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	90,
	'SSP',
	'Failed Unitization',
	'ssp_wo_auto_nonunitize.exe failed',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"false","label":"Failed Unitization","required":"true","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	91,
	'SSP',
	'AFUDC Calculation Only - No Overheads',
	'ssp_wo_calc_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<false>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<true>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<false>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	92,
	'SSP',
	'Overhead Calculation Before AFUDC',
	'ssp_wo_calc_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<true>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<false>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<false>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	93,
	'SSP',
	'Overhead Calculation After AFUDC',
	'ssp_wo_calc_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<false>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<false>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<true>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	94,
	'SSP',
	'AFUDC Approval Only - No Overheads',
	'ssp_wo_appr_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<false>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<true>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<false>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	95,
	'SSP',
	'Overhead Approval Before AFUDC',
	'ssp_wo_appr_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<true>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<false>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<false>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');
INSERT INTO pp_job_executable 
(job_executable_id, job_type, job_name, executable,arguments,category)
VALUES (
	96,
	'SSP',
	'Overhead Approval After AFUDC',
	'ssp_wo_appr_oh_afudc_wip.exe',
	'{"parms":[{"type":"long","value":"","label":"Company IDs","required":"true"},{"type":"date","value":"","label":"Month","required":"true"},{"type":"boolean","value":"<false>","label":"Calc Overhead Before","hidden":"true"},{"type":"boolean2","value":"<false>","label":"Calc AFUDC","hidden":"true"},{"type":"boolean3","value":"<true>","label":"Calc Overhead After","hidden":"true"},{"type":"boolean4","value":"<true>","label":"Calc Overhead Before Enabled","hidden":"true"},{"type":"boolean5","value":"<true>","label":"Calc AFUDC Enabled","hidden":"true"}]}',
	'Month End Processes');

SET DEFINE ON

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2499, 0, 2015, 1, 0, 0, 043637, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043637_job_executable_update_arguments.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;