/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045664_reg_schedule_precan_sql_dml_020.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.0.0 05/11/2016  Shane Ward		Improve Reg Schedule Builder
||============================================================================
*/

--Define Precanned SQL's for Schedule Builder

--Summary Ledger Queries
INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(1,
'Summary Ledger - Total Company Unadjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.total_co_included_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);

 INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(2,
'Summary Ledger - Total Company Adjustment',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.total_co_adjust_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);

INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(3,
'Summary Ledger - Total Company Adjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.total_co_final_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);

 INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(4,
'Summary Ledger - Jurisdiction Unadjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_alloc_results_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);


  INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(5,
'Summary Ledger - Jurisdiction Adjustment',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_adjust_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);

   INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(6,
'Summary Ledger - Jurisdiction Adjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_final_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);

   INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(7,
'Summary Ledger - Recovery Class',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_recovery_class_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);

INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(8,
'Summary Ledger - Revenue Requirement',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_base_rev_req_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 1);


  INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(9,
'Ledger (Unadjusted) by Month',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.unadjusted * sign) total
          FROM reg_case_composition  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND coverage_yr_ended = <case_year>
           AND a.schedule_id = <schedule_id>
           AND s.gl_month = <gl_month>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 2);

  INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(10,
'Ledger (Adjustmets) by Month',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.adjustments * sign) total
          FROM reg_case_composition  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND coverage_yr_ended = <case_year>
           AND a.schedule_id = <schedule_id>
           AND s.gl_month = <gl_month>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 2);

   INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(11,
'Ledger (Adjusted) by Month',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.per_books_adjusted * sign) total
          FROM reg_case_composition  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND coverage_yr_ended = <case_year>
           AND a.schedule_id = <schedule_id>
           AND s.gl_month = <gl_month>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order',
 2);

 --Allocation Results
INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(12,
'Allocation Results by Target - Unadjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(per_books * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_target_id = <alloc_target>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order',
 3);

 INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(13,
'Allocation Results by Target - Adjustments',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(adj_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_target_id = <alloc_target>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order',
 3);

 INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(14,
'Allocation Results by Target - Adjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(alloc_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_target_id = <alloc_target>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order',
 3);

INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(15,
'Allocation Results by Category - Unadjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(per_books * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_category_id = <alloc_category>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order',
 4);

 INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(16,
'Allocation Results by Category - Adjustments',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(adj_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_category_id = <alloc_category>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order',
 4);

 INSERT INTO reg_schedule_sql
(sql_id,
description,
sql_string,
sql_type)
VALUES
(17,
'Allocation Results by Category - Adjusted',
'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(alloc_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_category_id = <alloc_category>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.line_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order',
 4);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3189, 0, 2016, 1, 0, 0, 045664, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045664_reg_schedule_precan_sql_dml_020.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;