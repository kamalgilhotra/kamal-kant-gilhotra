/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_040299_lease_backfill_mla.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By           Reason for Change
|| -------- ---------- -------------------- ----------------------------------------------
|| 10.4.3.0 10/20/2014 Daniel Motter        Backfill default values for tax_rate_option_id
||                                          and tax_summary_id
||========================================================================================
*/

update LS_LEASE_OPTIONS
   set TAX_RATE_OPTION_ID = 1
 where TAX_RATE_OPTION_ID is null;

update LS_LEASE_OPTIONS
   set TAX_SUMMARY_ID = 0
 where TAX_SUMMARY_ID is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1550, 0, 10, 4, 3, 0, 40299, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_040299_lease_backfill_mla.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;