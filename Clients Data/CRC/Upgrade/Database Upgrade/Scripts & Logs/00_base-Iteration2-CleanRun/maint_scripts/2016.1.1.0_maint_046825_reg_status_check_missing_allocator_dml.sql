/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_046825_reg_status_check_missing_allocator_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2016.1.1.0 12/18/2016 Sarah Byers		Add status checks
||============================================================================
*/

insert into reg_status_check (
	reg_status_check_id, reg_case_process_id, reg_process_step_id, process_status, check_sql, detail_field, description, status_code_id)
select nvl(max(reg_status_check_id),0) + 1, 
		 4,
		 4,
		 3,
		 'select ''Allocator ''||ra.description||'' in case ''||c.case_name||'' is missing child dynamic target allocators.'', s.reg_alloc_category_id, cast(null as int), cast(null as int) from reg_allocator ra, reg_case c, reg_case_alloc_steps s, (select a.related_reg_alloc_id, ca.reg_alloc_category_id, count(*) child_count from reg_case_category_allocator ca, reg_allocator a where ca.reg_case_id = <case_id> and ca.reg_allocator_id = a.reg_allocator_id and a.related_reg_alloc_id is not null group by a.related_reg_alloc_id, ca.reg_alloc_category_id ) ac, (select s.category_id, count(*) target_count from reg_case_target_forward tf, reg_alloc_target t, (select c.step_order step_order, c.reg_alloc_category_id category_id, ac.description category_descr, p.reg_alloc_category_id parent_category_id, ap.description parent_category_descr from reg_case_alloc_steps c, reg_case_alloc_steps p, reg_alloc_category ac, reg_alloc_category ap where c.reg_case_id = p.reg_case_id and c.reg_case_id = <case_id> and c.step_order = p.step_order + 1 and c.step_order >= 2 and c.reg_alloc_category_id = ac.reg_alloc_category_id and p.reg_alloc_category_id = ap.reg_alloc_category_id) s where tf.reg_case_id = <case_id> and tf.reg_alloc_category_id = s.parent_category_id and tf.reg_alloc_target_id = t.reg_alloc_target_id and s.step_order <= (select step_order from reg_case_alloc_steps where reg_case_id = <case_id> and jur_result_flag = 1) group by s.category_id) tc where ac.reg_alloc_category_id = tc.category_id and ra.reg_allocator_id = ac.related_reg_alloc_id and ac.child_count <> tc.target_count and c.reg_case_id = <case_id> and c.reg_case_id = s.reg_case_id and s.reg_alloc_category_id = ac.reg_alloc_category_id',
		 'reg_case_id',
		 'Child target allocators missing for dynamic parent allocator',
		 1
  from reg_status_check;

insert into reg_status_check (
	reg_status_check_id, reg_case_process_id, reg_process_step_id, process_status, check_sql, detail_field, description, status_code_id)
select nvl(max(reg_status_check_id),0) + 1, 
		 7,
		 4,
		 3,
		 'select ''Allocator ''||ra.description||'' in case ''||c.case_name||'' is missing child dynamic target allocators.'', s.reg_alloc_category_id, cast(null as int), cast(null as int) from reg_allocator ra, reg_case c, reg_case_alloc_steps s, (select a.related_reg_alloc_id, ca.reg_alloc_category_id, count(*) child_count from reg_case_category_allocator ca, reg_allocator a where ca.reg_case_id = <case_id> and ca.reg_allocator_id = a.reg_allocator_id and a.related_reg_alloc_id is not null group by a.related_reg_alloc_id, ca.reg_alloc_category_id ) ac, (select s.category_id, count(*) target_count from reg_case_target_forward tf, reg_alloc_target t, (select c.step_order step_order, c.reg_alloc_category_id category_id, ac.description category_descr, p.reg_alloc_category_id parent_category_id, ap.description parent_category_descr from reg_case_alloc_steps c, reg_case_alloc_steps p, reg_alloc_category ac, reg_alloc_category ap where c.reg_case_id = p.reg_case_id and c.reg_case_id = <case_id> and c.step_order = p.step_order + 1 and c.step_order >= 2 and c.reg_alloc_category_id = ac.reg_alloc_category_id and p.reg_alloc_category_id = ap.reg_alloc_category_id) s where tf.reg_case_id = <case_id> and tf.reg_alloc_category_id = s.parent_category_id and tf.reg_alloc_target_id = t.reg_alloc_target_id and s.step_order > (select step_order from reg_case_alloc_steps where reg_case_id = <case_id> and jur_result_flag = 1) and s.step_order <= (select step_order from reg_case_alloc_steps where reg_case_id = <case_id> and recovery_class_flag = 1) group by s.category_id) tc where ac.reg_alloc_category_id = tc.category_id and ra.reg_allocator_id = ac.related_reg_alloc_id and ac.child_count <> tc.target_count and c.reg_case_id = <case_id> and c.reg_case_id = s.reg_case_id and s.reg_alloc_category_id = ac.reg_alloc_category_id',
		 'reg_case_id',
		 'Child target allocators missing for dynamic parent allocator',
		 1
  from reg_status_check;

insert into reg_status_check (
	reg_status_check_id, reg_case_process_id, reg_process_step_id, process_status, check_sql, detail_field, description, status_code_id)
select nvl(max(reg_status_check_id),0) + 1, 
		 9,
		 4,
		 3,
		 'select ''Allocator ''||ra.description||'' in case ''||c.case_name||'' is missing child dynamic target allocators.'', s.reg_alloc_category_id, cast(null as int), cast(null as int) from reg_allocator ra, reg_case c, reg_case_alloc_steps s, (select a.related_reg_alloc_id, ca.reg_alloc_category_id, count(*) child_count from reg_case_category_allocator ca, reg_allocator a where ca.reg_case_id = <case_id> and ca.reg_allocator_id = a.reg_allocator_id and a.related_reg_alloc_id is not null group by a.related_reg_alloc_id, ca.reg_alloc_category_id ) ac, (select s.category_id, count(*) target_count from reg_case_target_forward tf, reg_alloc_target t, (select c.step_order step_order, c.reg_alloc_category_id category_id, ac.description category_descr, p.reg_alloc_category_id parent_category_id, ap.description parent_category_descr from reg_case_alloc_steps c, reg_case_alloc_steps p, reg_alloc_category ac, reg_alloc_category ap where c.reg_case_id = p.reg_case_id and c.reg_case_id = <case_id> and c.step_order = p.step_order + 1 and c.step_order >= 2 and c.reg_alloc_category_id = ac.reg_alloc_category_id and p.reg_alloc_category_id = ap.reg_alloc_category_id) s where tf.reg_case_id = <case_id> and tf.reg_alloc_category_id = s.parent_category_id and tf.reg_alloc_target_id = t.reg_alloc_target_id and s.step_order > (select step_order from reg_case_alloc_steps where reg_case_id = <case_id> and recovery_class_flag = 1) group by s.category_id) tc where ac.reg_alloc_category_id = tc.category_id and ra.reg_allocator_id = ac.related_reg_alloc_id and ac.child_count <> tc.target_count and c.reg_case_id = <case_id> and c.reg_case_id = s.reg_case_id and s.reg_alloc_category_id = ac.reg_alloc_category_id',
		 'reg_case_id',
		 'Child target allocators missing for dynamic parent allocator',
		 1
  from reg_status_check;
  

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3351, 0, 2016, 1, 1, 0, 046825, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.1.0_maint_046825_reg_status_check_missing_allocator_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;