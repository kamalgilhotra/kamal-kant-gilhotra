/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031640_version-updates_proptax.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   08/12/2013 Andrew Scott   10.4.1.0 version
||============================================================================
*/

update PP_VERSION
   set PROP_TAX_VERSION = 'Version 10.4.1.0'
where PP_VERSION_ID = 1;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (512, 0, 10, 4, 1, 0, 31640, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_031640_version-updates_proptax.sql', 1, SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'),
    SYS_CONTEXT('USERENV', 'TERMINAL'), SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
