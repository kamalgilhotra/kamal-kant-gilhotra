/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048768_lessor_fix_menu_for_view_ex_rates_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 09/15/2017 Charlie Shilling Lessee and Lessor 
||============================================================================
*/
UPDATE ppbase_workspace
SET workspace_uo_name = 'uo_ls_admincntr_view_exchange_rate_wksp'
WHERE MODULE = 'LESSOR'
AND workspace_identifier = 'admin_view_exchange_rates'
;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3713, 0, 2017, 1, 0, 0, 48768, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048768_lessor_fix_menu_for_view_ex_rates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;