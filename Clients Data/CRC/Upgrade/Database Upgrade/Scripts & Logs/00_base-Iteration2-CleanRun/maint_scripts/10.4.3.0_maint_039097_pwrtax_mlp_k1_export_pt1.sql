/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039097_pwrtax_mlp_k1_export_pt1.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/08/2014 Andrew Scott        MLP K1 Export Workspace Pt 1
||                                         This script creates the k1 export table
||                                         that keys back to the field Alex P.
||                                         created on tax_record_control.
||============================================================================
*/

create table TAX_K1_EXPORT
(
 K1_EXPORT_ID number(22,0) not null,
 USER_ID      varchar2(18),
 TIME_STAMP   date,
 DESCRIPTION  varchar2(254) not null
);

comment on table TAX_K1_EXPORT is '(C)[09] The Tax K1 Export table holds the K1 export values used by the MLP K1 export process.';
comment on column TAX_K1_EXPORT.K1_EXPORT_ID is 'The system generated key of the K1 export value.';
comment on column TAX_K1_EXPORT.USER_ID is 'Standard system-assigned user id used for audit purposes.';
comment on column TAX_K1_EXPORT.TIME_STAMP is 'Standard system assigned timestamp used for audit purposes.';
comment on column TAX_K1_EXPORT.DESCRIPTION is 'The K1 export value.';

alter table TAX_K1_EXPORT
   add constraint TAX_K1_EXPORT_PK
       primary key (K1_EXPORT_ID)
       using index tablespace PWRPLANT_IDX;

alter table TAX_RECORD_CONTROL
   add constraint TAX_RECORD_CONTROL_K1_EXP_FK
       foreign key (K1_EXPORT_ID)
       references TAX_K1_EXPORT;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1309, 0, 10, 4, 3, 0, 39097, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039097_pwrtax_mlp_k1_export_pt1.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;