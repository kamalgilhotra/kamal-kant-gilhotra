/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045557_lease_add_col_ls_forecast_version_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 03/16/2016 Anand R			 Add new column revision_built  to table 
||											 ls_forecast_version
|| 2015.2.2.0 04/14/2016 David Haupt		 Backport to 2015.2.2
||============================================================================
*/ 

alter table LS_FORECAST_VERSION 
add REVISION_BUILT  number(1,0) default 0 ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3108, 0, 2015, 2, 2, 0, 045557, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.2.0_maint_045557_lease_add_col_ls_forecast_version_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;