/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043471_reg_hist_lyr_tax1_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/18/2015 Andrew Scott   add inc process id to tax tables.
||============================================================================
*/

----add incremental process id

ALTER TABLE TAX_DEPRECIATION_FP
 add  INCREMENTAL_PROCESS_ID NUMBER(22,0);

ALTER TABLE BASIS_AMOUNTS_FP
 add  INCREMENTAL_PROCESS_ID NUMBER(22,0);

ALTER TABLE TAX_BOOK_RECONCILE_FP
 add  INCREMENTAL_PROCESS_ID NUMBER(22,0);

ALTER TABLE DEFERRED_INCOME_TAX_FP
 add  INCREMENTAL_PROCESS_ID NUMBER(22,0);

ALTER TABLE UTILITY_ACCT_DEPRECIATION_FP
 add  INCREMENTAL_PROCESS_ID NUMBER(22,0);

ALTER TABLE TAX_FCST_BUDGET_ADDS_FP
 add  INCREMENTAL_PROCESS_ID NUMBER(22,0);

ALTER TABLE TAX_JOB_PARAMS
 add  INCREMENTAL_PROCESS_ID NUMBER(22,0);


COMMENT ON COLUMN TAX_DEPRECIATION_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN BASIS_AMOUNTS_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN TAX_BOOK_RECONCILE_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN DEFERRED_INCOME_TAX_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN UTILITY_ACCT_DEPRECIATION_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN TAX_FCST_BUDGET_ADDS_FP.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';
COMMENT ON COLUMN TAX_JOB_PARAMS.INCREMENTAL_PROCESS_ID IS 'System assigned identifier for an incremental process.';



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2637, 0, 2015, 2, 0, 0, 043471, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043471_reg_hist_lyr_tax1_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;