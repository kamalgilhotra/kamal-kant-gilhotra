/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_045678_reg_cr_combos_lists.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- --------------------------------------
|| 2016.1.0.0 05/09/2016 Sarah Byers
||============================================================================
*/

-- Add CR_LIST_ID to REG_CR_COMBOS_FIELDS
alter table REG_CR_COMBOS_FIELDS_BDG add CR_LIST_ID number(22,0) null;

comment on column REG_CR_COMBOS_FIELDS_BDG.CR_LIST_ID is 'System assigned identifier of a CR Combo List.';

-- Convert existing list uses
insert into REG_CR_COMBOS_LISTS (
	CR_LIST_ID, DESCRIPTION, CR_ELEMENT_ID, STRUCTURE_ID, ELEMENT_LIST)
select distinct F.CR_COMBO_ID||F.CR_ELEMENT_ID, C.DESCRIPTION, F.CR_ELEMENT_ID, F.STRUCTURE_ID, F.LOWER_VALUE
  from REG_CR_COMBOS C, REG_CR_COMBOS_FIELDS_BDG F
 where nvl(F.STRUCTURE_ID,0) <> 0
	and C.CR_COMBO_ID = F.CR_COMBO_ID;

update REG_CR_COMBOS_FIELDS_BDG set CR_LIST_ID = CR_COMBO_ID||CR_ELEMENT_ID where nvl(STRUCTURE_ID,0) <> 0;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3186, 0, 2016, 1, 0, 0, 045678, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045678_reg_cr_combos_lists.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;