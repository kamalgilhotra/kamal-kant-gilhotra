/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_031778_taxrpr_wmis_gui.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 11/25/2013 Andrew Scott        Tax Repairs : GUI Related Changes for WMIS Method
||============================================================================
*/

delete from PPBASE_MENU_ITEMS
 where MODULE = 'REPAIRS'
   and MENU_IDENTIFIER = 'menu_wksp_system_options';

delete from PPBASE_WORKSPACE
 where MODULE = 'REPAIRS'
   and WORKSPACE_IDENTIFIER = 'menu_wksp_system_options';

delete from PPBASE_SYSTEM_OPTIONS_MODULE
 where SYSTEM_OPTION_ID = 'Repairs - WMIS - Allow Feed Data Changes';

delete from PPBASE_SYSTEM_OPTIONS_VALUES
 where SYSTEM_OPTION_ID = 'Repairs - WMIS - Allow Feed Data Changes';

delete from PPBASE_SYSTEM_OPTIONS
 where SYSTEM_OPTION_ID = 'Repairs - WMIS - Allow Feed Data Changes';

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REPAIRS', 'menu_wksp_system_options', 'System Options', 'uo_rpr_config_wksp_sysoptions',
    'System Options');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER,
    WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REPAIRS', 'menu_wksp_system_options', 2, 8, 'System Options', 'System Options',
    'menu_wksp_config', 'menu_wksp_system_options', 1);

insert into PPBASE_SYSTEM_OPTIONS
   (SYSTEM_OPTION_ID, LONG_DESCRIPTION, SYSTEM_ONLY, PP_DEFAULT_VALUE, IS_BASE_OPTION)
values
   ('Repairs - WMIS - Allow Feed Data Changes',
    'Whether or not changes to the Repair Detail WMIS Feed data are allowed in the Processing Workspace.',
    0, 'No', 1);

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - WMIS - Allow Feed Data Changes', 'Yes');

insert into PPBASE_SYSTEM_OPTIONS_VALUES
   (SYSTEM_OPTION_ID, OPTION_VALUE)
values
   ('Repairs - WMIS - Allow Feed Data Changes', 'No');

insert into PPBASE_SYSTEM_OPTIONS_MODULE
   (SYSTEM_OPTION_ID, MODULE)
values
   ('Repairs - WMIS - Allow Feed Data Changes', 'REPAIRS');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (773, 0, 10, 4, 2, 0, 31778, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_031778_taxrpr_wmis_gui.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
