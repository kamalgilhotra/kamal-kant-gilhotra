/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_036582_reg_ta_drillback_fix.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 02/26/2014 Sarah Byers
||============================================================================
*/

merge into REG_QUERY_DRILLBACK E
using (select 6 REG_SOURCE_ID,
              1 HIST_FCST_FLAG,
              (select ID
                 from CR_DD_SOURCES_CRITERIA
                where TABLE_NAME = 'reg_hist_ta_other_drill_view') REG_QUERY_ID,
              'Tax Provision Other Drillback' DESCRIPTION,
              0 DEFAULT_FLAG,
              'TAX_ACCRUAL_M_ITEM' TABLE_NAME
         from DUAL) Q
on (   E.REG_SOURCE_ID = Q.REG_SOURCE_ID
   and E.HIST_FCST_FLAG = Q.HIST_FCST_FLAG
   and E.DESCRIPTION = 'Tax Other Drillback')
when matched then
   update set E.REG_QUERY_ID = Q.REG_QUERY_ID,
              E.TABLE_NAME = Q.TABLE_NAME
when not matched then
   insert
      (REG_SOURCE_ID, HIST_FCST_FLAG, REG_QUERY_ID, DESCRIPTION, DEFAULT_FLAG, TABLE_NAME)
   values
      (Q.REG_SOURCE_ID, Q.HIST_FCST_FLAG, Q.REG_QUERY_ID, Q.DESCRIPTION, Q.DEFAULT_FLAG, Q.TABLE_NAME);

merge into REG_QUERY_DRILLBACK E
using (select 6 REG_SOURCE_ID,
              1 HIST_FCST_FLAG,
              (select ID
                 from CR_DD_SOURCES_CRITERIA
                where TABLE_NAME = 'reg_hist_ta_defer_drill_view') REG_QUERY_ID,
              'Tax Provision Deferred Drillback' DESCRIPTION,
              0 DEFAULT_FLAG,
              'TAX_ACCRUAL_FAS109' TABLE_NAME
         from DUAL) Q
on (   E.REG_SOURCE_ID = Q.REG_SOURCE_ID
   and E.HIST_FCST_FLAG = Q.HIST_FCST_FLAG
   and E.DESCRIPTION = 'Tax Deferred Drillback')
when matched then
   update set E.REG_QUERY_ID = Q.REG_QUERY_ID, E.TABLE_NAME = Q.TABLE_NAME
when not matched then
   insert
      (REG_SOURCE_ID, HIST_FCST_FLAG, REG_QUERY_ID, DESCRIPTION, DEFAULT_FLAG, TABLE_NAME)
   values
      (Q.REG_SOURCE_ID, Q.HIST_FCST_FLAG, Q.REG_QUERY_ID, Q.DESCRIPTION, Q.DEFAULT_FLAG, Q.TABLE_NAME);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (994, 0, 10, 4, 2, 0, 36582, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036582_reg_ta_drillback_fix.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;