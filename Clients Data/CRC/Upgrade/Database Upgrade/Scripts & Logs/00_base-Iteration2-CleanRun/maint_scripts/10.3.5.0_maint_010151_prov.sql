/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010151_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   08/20/2012 Blake Andrews  Point Release
||============================================================================
*/

update TAX_ACCRUAL_DEF_TAX_CONTROL
   set SPREAD_EST_IND = 1
 where M_ID in (select M_ID
                  from TAX_ACCRUAL_CONTROL TAC
                 where FROM_PT_IND = 1
                   and not exists (select 1
                          from TAX_ACCRUAL_POWERTAX_MAP PT_MAP
                         where PT_MAP.COMPANY_ID = TAC.COMPANY_ID
                           and PT_MAP.MID_CWIP = TAC.M_ITEM_ID
                           and PT_MAP.OPER_IND = TAC.OPER_IND));

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (201, 0, 10, 3, 5, 0, 10151, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010151_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;