/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030228_cr_obs_sys_ctrl.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By      Reason for Change
|| ---------- ---------- --------------- -------------------------------------
|| 10.4.1.0   06/29/2013 Stephen Motter  Point Release
||============================================================================
*/

-- These system controls are obsolete
delete from CR_SYSTEM_CONTROL
 where UPPER(CONTROL_NAME) = 'ENABLE VALIDATIONS - JE - BATCH'
    or UPPER(CONTROL_NAME) = 'ENABLE MANUAL VALIDATIONS - COMBO';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (425, 0, 10, 4, 1, 0, 30228, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030228_cr_obs_sys_ctrl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;