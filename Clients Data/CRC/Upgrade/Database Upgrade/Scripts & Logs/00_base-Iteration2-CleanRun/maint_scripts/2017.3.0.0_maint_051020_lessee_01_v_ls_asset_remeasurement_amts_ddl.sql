/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051020_lessee_01_v_ls_asset_remeasurement_amts_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.3.0.0 04/30/2018 Josh Sandler 	  Rebuild V_LS_ASSET_REMEASUREMENT_AMTS with om_to_cap_indicator
||============================================================================
*/

CREATE OR REPLACE VIEW V_LS_ASSET_REMEASUREMENT_AMTS AS
SELECT stg.ilr_id, stg.revision, sch.ls_asset_id, sch.set_of_books_id, o.remeasurement_date, ilr.current_revision,
CASE
  WHEN sch.is_om = 1 AND llct.is_om = 0 THEN
    1
  ELSE
    0
END om_to_cap_indicator,
sch.end_obligation AS prior_month_end_obligation, sch.end_liability AS prior_month_end_liability,
sch.end_lt_obligation AS prior_month_end_lt_obligation, sch.end_lt_liability AS prior_month_end_lt_liability,
sch.end_deferred_rent AS prior_month_end_deferred_rent,
sch.end_prepaid_rent AS prior_month_end_prepaid_rent,
depr.end_reserve AS prior_month_end_depr_reserve,
sch.end_capital_cost - depr.end_reserve AS prior_month_end_nbv,
sch.end_capital_cost AS prior_month_end_capital_cost,
1 is_remeasurement
FROM ls_ilr_stg stg
  JOIN ls_ilr ilr ON stg.ilr_id = ilr.ilr_id
  JOIN ls_ilr_options o ON stg.ilr_id = o.ilr_id AND stg.revision = o.revision
  JOIN ls_lease_cap_type llct ON llct.ls_lease_cap_type_id = o.lease_cap_type_id
  JOIN ls_ilr_asset_map map ON map.ilr_id = stg.ilr_id AND map.revision = ilr.current_revision
  JOIN ls_asset_schedule sch ON map.ls_asset_id = sch.ls_asset_id AND sch.revision = ilr.current_revision AND sch.set_of_books_id = stg.set_of_books_id
  left OUTER JOIN ls_depr_forecast depr ON depr.ls_asset_id = map.ls_asset_id AND depr.revision = ilr.current_revision AND depr.set_of_books_id = stg.set_of_books_id
WHERE ilr.current_revision <> stg.revision
AND o.remeasurement_date IS NOT NULL
AND o.remeasurement_date = Add_Months(sch.MONTH,1)
AND (o.remeasurement_date = Add_Months(depr.MONTH,1) OR depr.MONTH IS NULL)
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4882, 0, 2017, 3, 0, 0, 51020, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.0_maint_051020_lessee_01_v_ls_asset_remeasurement_amts_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;