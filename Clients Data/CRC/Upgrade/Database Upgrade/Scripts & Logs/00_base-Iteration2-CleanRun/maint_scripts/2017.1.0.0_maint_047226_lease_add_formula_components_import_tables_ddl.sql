/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047226_lease_add_formula_components_import_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 05/08/2017 Jared Watkins    create the staging table(s) needed for the Index/Rate import
||============================================================================
*/

--create the staging table to hold the imported values
create table ls_import_vp_comp_amts(
  import_run_id number(22,0) not null,
  line_id number(22,0) not null,
  error_message varchar2(4000),
  time_stamp date,
  user_id varchar2(18),
  formula_component_id number(22,0),
  formula_component_xlate varchar2(254),
  effective_date varchar2(254),
  amount varchar2(254)
);

alter table ls_import_vp_comp_amts
add constraint ls_import_vp_comp_amt_pk
primary key (import_run_id, line_id)
using index tablespace PWRPLANT_IDX;

alter table ls_import_vp_comp_amts
add constraint ls_import_vp_comp_amt_fk1
foreign key (import_run_id)
references pp_import_run (import_run_id);

comment on table ls_import_vp_comp_amts is '(S) [06] The LS Import VP Comp Amts table is an API table used to import Formula Component Amounts.';
comment on column ls_import_vp_comp_amts.import_run_id is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column ls_import_vp_comp_amts.line_id is 'System-assigned line number for this import run.';
comment on column ls_import_vp_comp_amts.error_message is 'Error messages resulting from data validation in the import process.';
comment on column ls_import_vp_comp_amts.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_import_vp_comp_amts.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_import_vp_comp_amts.formula_component_id is 'The internal Formula Component id within PowerPlant.';
comment on column ls_import_vp_comp_amts.formula_component_xlate is 'Translation field for determining the Formula Component.';
comment on column ls_import_vp_comp_amts.effective_date is 'The date upon which the amount for the Index/Rate becomes effective.';
comment on column ls_import_vp_comp_amts.amount is 'The amount we are loading for the specific Formula Component.';

--create the archive staging table
create table ls_import_vp_comp_amts_archive(
  import_run_id number(22,0) not null,
  line_id number(22,0) not null,
  time_stamp date,
  user_id varchar2(18),
  formula_component_id number(22,0),
  formula_component_xlate varchar2(254),
  effective_date varchar2(254),
  amount varchar2(254)
);

comment on table ls_import_vp_comp_amts_archive is '(S) [06] The LS Import VP Comp Amts Archive table holds records of previous Formula Component Amount imports.';
comment on column ls_import_vp_comp_amts_archive.import_run_id is 'System-assigned ID that specifies the import run that this record was imported in.';
comment on column ls_import_vp_comp_amts_archive.line_id is 'System-assigned line number for this import run.';
comment on column ls_import_vp_comp_amts_archive.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_import_vp_comp_amts_archive.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_import_vp_comp_amts_archive.formula_component_id is 'The internal Formula Component id within PowerPlant.';
comment on column ls_import_vp_comp_amts_archive.formula_component_xlate is 'Translation field for determining the Formula Component.';
comment on column ls_import_vp_comp_amts_archive.effective_date is 'The date upon which the amount for the Index/Rate becomes effective.';
comment on column ls_import_vp_comp_amts_archive.amount is 'The amount we are loading for the specific Formula Component.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3485, 0, 2017, 1, 0, 0, 47226, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047226_lease_add_formula_components_import_tables_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;