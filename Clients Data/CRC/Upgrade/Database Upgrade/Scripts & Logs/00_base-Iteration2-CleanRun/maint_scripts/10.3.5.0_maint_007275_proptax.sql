/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_007275_proptax.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.5.0   05/17/2012 Julia Breuer   Point Release
||============================================================================
*/

-- Create new reports that summarize company-prop tax company assignments
insert into PWRPLANT.PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DYNAMIC_DW)
values
   (509925, 'PT Company Summary',
    'Summary of companies assigned to each prop tax company, both by default assignment and state overrides.',
    'dw_ptr_ptco_co_summary', 'PropTax - 9925', 10, 9, 6, 1, 1, 3, 0);

insert into PWRPLANT.PP_REPORTS
   (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, DATAWINDOW, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID,
    REPORT_TYPE_ID, PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID,
    PP_REPORT_ENVIR_ID, DYNAMIC_DW)
values
   (509926, 'Company Summary',
    'Summary of prop tax companies for each company, both by default assignment and state overrides.',
    'dw_ptr_co_ptco_summary', 'PropTax - 9926', 10, 9, 6, 1, 1, 3, 0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (139, 0, 10, 3, 5, 0, 7275, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_007275_proptax.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
