 /*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_041379_pcm_maint_detail.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   11/26/2014 Alex P.             Detail Info workspace
||========================================================================================
*/
 
 update ppbase_workspace
 set workspace_uo_name = 'uo_pcm_maint_wksp_detail'
 where module = 'pcm'
   and workspace_identifier in ('fp_maint_details', 'wo_maint_details' );

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2072, 0, 2015, 1, 0, 0, 041379, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_041379_pcm_maint_detail.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;