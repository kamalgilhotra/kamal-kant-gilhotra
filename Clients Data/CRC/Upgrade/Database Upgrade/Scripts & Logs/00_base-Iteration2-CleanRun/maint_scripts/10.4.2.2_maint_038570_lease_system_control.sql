/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_038570_lease_system_control.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.2 06/10/2014 Brandon Beck
||============================================================================
*/

insert into PP_SYSTEM_CONTROL_COMPANY
   (CONTROL_ID, CONTROL_NAME, CONTROL_VALUE, DESCRIPTION, LONG_DESCRIPTION, USER_DISABLED, COMPANY_ID)
   select max(CONTROL_ID) + 1,
          'Lease Operating Expense as Incurred',
          'no',
          'dw_yes_no;1',
          'If yes, then operating leases will be accrued as incurred.  No means straight line',
          null,
          -1
     from PP_SYSTEM_CONTROL_COMPANY;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1246, 0, 10, 4, 2, 2, 38570, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.2_maint_038570_lease_system_control.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
