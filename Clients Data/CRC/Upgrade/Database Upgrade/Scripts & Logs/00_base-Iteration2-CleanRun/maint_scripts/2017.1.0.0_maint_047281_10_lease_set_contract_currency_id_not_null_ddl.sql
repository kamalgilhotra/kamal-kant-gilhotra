/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_047281_10_lease_set_contract_currency_id_not_null_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 04/24/2017 Jared Watkins    change the contract_currency_id columns to be non-nullable
||                                        once we have defaulted all of the values for existing MLAs/Assets
||============================================================================
*/

ALTER TABLE ls_lease
MODIFY contract_currency_id  number(22) NOT NULL;

ALTER TABLE ls_asset
MODIFY contract_currency_id  number(22) NOT NULL;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3456, 0, 2017, 1, 0, 0, 47281, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047281_10_lease_set_contract_currency_id_not_null_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;