/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045519_pwrtax_plant_recon_add_tables_ddl.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By          Reason for Change
|| --------   ---------- ------------------  ---------------------------------
|| 2016.1.0.0 03/03/2016 Anand R			 Add Tax Plant reconcilliation tables  
||
||============================================================================
*/ 

--Create table TAX_PLANT_RECON_FORM
--=================================

create table PWRPLANT.TAX_PLANT_RECON_FORM 
(
 TAX_PR_FORM_ID NUMBER(22,0) not null,
 VERSION_ID     NUMBER(22,0) not null,
 COMPANY_ID     NUMBER(22,0) not null,
 TAX_YEAR_VERSION_ID NUMBER(22,0) not null,
 TIME_STAMP    date,
 USER_ID       varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM  is 'The Tax Plant Recon Form Table stores the company, version, and tax year for a Plant Reconciliation Form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.VERSION_ID is 'System assigned identifier for a tax case.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.COMPANY_ID is 'System assigned identifier for a company.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.TAX_YEAR_VERSION_ID is 'System assigned identifier for a tax year related to a tax case.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM 
   add constraint TAX_PLANT_RECON_FORM_PK
       primary key (TAX_PR_FORM_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM 
   add constraint TAX_PLANT_RECON_FORM_FK1
   foreign key (VERSION_ID)
   references VERSION(VERSION_ID);
   
alter table PWRPLANT.TAX_PLANT_RECON_FORM 
   add constraint TAX_PLANT_RECON_FORM_FK2
   foreign key (COMPANY_ID)
   references COMPANY_SETUP(COMPANY_ID);
   
alter table PWRPLANT.TAX_PLANT_RECON_FORM 
   add constraint TAX_PLANT_RECON_FORM_FK3
   foreign key (TAX_YEAR_VERSION_ID)
   references TAX_YEAR_VERSION(TAX_YEAR_VERSION_ID);
   
--Create table TAX_PLANT_RECON_FORM_JUR 
--=====================================
 
create table PWRPLANT.TAX_PLANT_RECON_FORM_JUR  
(
 TAX_PR_FORM_ID      NUMBER(22,0) not null,
 JURISDICTION_ID     NUMBER(22,0) not null,
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_JUR  is 'The Tax Plant Recon Form Jur Table stores the jurisdictions for a Plant Reconciliation Form.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_JUR.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_JUR.JURISDICTION_ID is 'System assigned identifier for a tax jurisdiction.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_JUR
   add constraint TAX_PLANT_RECON_FORM_JUR_PK
       primary key (TAX_PR_FORM_ID, JURISDICTION_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_JUR 
   add constraint TAX_PLANT_RECON_FORM_JUR_FK1
   foreign key (TAX_PR_FORM_ID)
   references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID);
   
alter table PWRPLANT.TAX_PLANT_RECON_FORM_JUR
   add constraint TAX_PLANT_RECON_FORM_JUR_FK2
   foreign key (JURISDICTION_ID)
   references JURISDICTION(JURISDICTION_ID);
   
--Create table TAX_PLANT_RECON_SECTION  
--====================================

create table PWRPLANT.TAX_PLANT_RECON_SECTION   
(
 TAX_PR_SECTION_ID      NUMBER(22,0) not null,
 DESCRIPTION            VARCHAR2(100) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_SECTION   is 'The Tax Plant Recon Section Table stores the description of each section of the Plant Reconciliation Form.';

comment on column PWRPLANT.TAX_PLANT_RECON_SECTION.TAX_PR_SECTION_ID is 'System assigned identifier for a tax plant reconciliation section.';
comment on column PWRPLANT.TAX_PLANT_RECON_SECTION.DESCRIPTION is 'Description of a tax plant teconciliation section.';
comment on column PWRPLANT.TAX_PLANT_RECON_SECTION.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_SECTION.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_SECTION 
   add constraint TAX_PLANT_RECON_SECTION_PK
       primary key (TAX_PR_SECTION_ID)
       using index tablespace PWRPLANT_IDX;

--Create table TAX_PLANT_RECON_FORM_STATUS   
--========================================


create table PWRPLANT.TAX_PLANT_RECON_FORM_STATUS    
(
 TAX_PR_FORM_ID      NUMBER(22,0) not null,
 TAX_PR_SECTION_ID   NUMBER(22,0) not null,
 STATUS_ID           NUMBER(22,0) not null,
 MESSAGE             VARCHAR(254),
 TIME_STAMP          date,
 USER_ID             varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_STATUS is 'The Tax Plant Recon Form Status Table stores the calculation status for a Plant Reconciliation form.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_STATUS.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_STATUS.TAX_PR_SECTION_ID is 'System assigned identifier for a tax plant reconciliation section.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_STATUS.STATUS_ID is '0 = No out of balance; 1 = out of balance condition exists.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_STATUS.MESSAGE is 'Message for the user related to the status.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_STATUS.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_STATUS.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_STATUS  
   add constraint TAX_PLANT_RECON_FORM_STATUS_PK
       primary key (TAX_PR_FORM_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_STATUS  
   add constraint TAX_PLANT_RECON_FORM_STAT_FK1
       foreign key (TAX_PR_FORM_ID)
       references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID) ;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_STATUS  
   add constraint TAX_PLANT_RECON_FORM_STAT_FK2
       foreign key (TAX_PR_SECTION_ID)
       references TAX_PLANT_RECON_SECTION(TAX_PR_SECTION_ID) ;       
       
--Create table TAX_PLANT_RECON_FORM_RATES   
--=======================================
create table PWRPLANT.TAX_PLANT_RECON_FORM_RATES     
(
 TAX_PR_FORM_ID           NUMBER(22,0) not null,
 DEF_INCOME_TAX_RATE_ID   NUMBER(22,0) not null,
 BEG_EFFECTIVE_DATE       date not null,
 END_EFFECTIVE_DATE       date not null,
 TIME_STAMP               date,
 USER_ID                  varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_RATES    is 'The Tax Plant Recon Form Rates Table stores the beginning and ending deferred income tax rates used for a Plant Reconciliation form.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RATES.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RATES.DEF_INCOME_TAX_RATE_ID is 'System assigned identifier for a deferred tax rate.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RATES.BEG_EFFECTIVE_DATE is 'Effective date used to identify the beginning of year rate.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RATES.END_EFFECTIVE_DATE is 'Effective date used to identify the end of year rate.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RATES.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RATES.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_RATES  
   add constraint TAX_PLANT_RECON_FORM_RATES_PK
       primary key (TAX_PR_FORM_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_RATES  
   add constraint TAX_PLANT_RECON_FORM_RATES_FK1
       foreign key (TAX_PR_FORM_ID)
       references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID) ;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_RATES  
   add constraint TAX_PLANT_RECON_FORM_RATES_FK2
       foreign key (DEF_INCOME_TAX_RATE_ID)
       references DEFERRED_RATES(DEF_INCOME_TAX_RATE_ID) ;      

alter table PWRPLANT.TAX_PLANT_RECON_FORM_RATES  
   add constraint TAX_PLANT_RECON_FORM_RATES_FK3
       foreign key (DEF_INCOME_TAX_RATE_ID, BEG_EFFECTIVE_DATE)
       references DEFERRED_INCOME_TAX_RATES (DEF_INCOME_TAX_RATE_ID, EFFECTIVE_DATE) ;             
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_RATES  
   add constraint TAX_PLANT_RECON_FORM_RATES_FK4
       foreign key (DEF_INCOME_TAX_RATE_ID, END_EFFECTIVE_DATE)
       references DEFERRED_INCOME_TAX_RATES (DEF_INCOME_TAX_RATE_ID, EFFECTIVE_DATE) ;    
       
--Create table TAX_PLANT_RECON_GL_ACCOUNT    
--=======================================

create table PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT      
(
 TAX_PR_GL_ACCOUNT_ID   NUMBER(22,0) not null,
 DESCRIPTION            varchar2(100) not null,
 TAX_ACCOUNT_TYPE       NUMBER(22,0) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT is 'The Tax Plant Recon GL Account Table stores the GL Accounts considered for Plant Reconciliation process.';

comment on column PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT.TAX_PR_GL_ACCOUNT_ID is 'System assigned identifier for a tax plant reconciliation GL Account.';
comment on column PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT.DESCRIPTION is 'Description of the tax plant reconciliation GL Account.';
comment on column PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT.TAX_ACCOUNT_TYPE is '1 = Asset; 2 = Reserve';
comment on column PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_GL_ACCOUNT  
   add constraint TAX_PLANT_RECON_GL_ACCOUNT_PK
       primary key (TAX_PR_GL_ACCOUNT_ID)
       using index tablespace PWRPLANT_IDX;

--Create table TAX_PLANT_RECON_FORM_GL_ACCT     
--=========================================

create table PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT       
(
 TAX_PR_FORM_ID         NUMBER(22,0) not null,
 TAX_PR_GL_ACCOUNT_ID   NUMBER(22,0) not null,
 BEG_BALANCE            NUMBER(22,2) not null,
 END_BALANCE            NUMBER(22,2) not null,
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT  is 'The Tax Plant Recon Form GL Acct Table stores the beginning and ending balances by GL Account for a Plant Reconciliation form; the values are manually entered by the user.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT.TAX_PR_GL_ACCOUNT_ID is 'System assigned identifier for a tax plant reconciliation GL Account.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT.BEG_BALANCE is 'Beginning balance for the tax plant reconciliation GL Account.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT.END_BALANCE is 'Ending balance for the tax plant reconciliation GL Account.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT   
   add constraint TAX_PLANT_RECON_FORM_GL_ACC_PK
       primary key (TAX_PR_FORM_ID, TAX_PR_GL_ACCOUNT_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT   
   add constraint TX_PLNT_RECON_FORM_GL_ACC_FK1
       foreign key (TAX_PR_FORM_ID)
       references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID) ;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_GL_ACCT   
   add constraint TX_PLNT_RECON_FORM_GL_ACC_FK2
       foreign key (TAX_PR_GL_ACCOUNT_ID)
       references TAX_PLANT_RECON_GL_ACCOUNT(TAX_PR_GL_ACCOUNT_ID) ;      
          
--Create table TAX_PLANT_RECON_FORM_SUMM_ID      
--=========================================

create table PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID        
(
 TAX_PR_FORM_ID         NUMBER(22,0) not null,
 TAX_PR_SUMMARY_ID      NUMBER(22,0) not null,
 DESCRIPTION            varchar2(100) not null,
 TAX_PR_SECTION_ID      NUMBER(22,0) not null,
 JURISDICTION_ID        NUMBER(22,2),
 RECON_SOURCE           NUMBER(22,0),
 TAX_ACCOUNT_TYPE       NUMBER(22,0),
 SUBTOTAL               NUMBER(22,0),
 SORT_ORDER             NUMBER(22,0),
 TIME_STAMP             date,
 USER_ID                varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID  is 'The Tax Plant Recon Form Summ ID Table stores the description and relevant source information for a summary amount line on a Plant Reconciliation Form.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.TAX_PR_SUMMARY_ID is 'System assigned identifier for a tax plant reconciliation summary.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.DESCRIPTION is 'Description of a tax plant reconciliation summary.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.TAX_PR_SECTION_ID is 'System assigned identifier for a tax plant reconciliation section.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.JURISDICTION_ID is 'System assigned identifier for a jurisdiction.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.RECON_SOURCE is '1 = GL; 2 = PowerTax; 3 = PowerPlan';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.TAX_ACCOUNT_TYPE is '1 = Asset; 2 = Reserve';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.SUBTOTAL is '0 = Not a subtotal; 1 = Subtotal';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.SORT_ORDER is 'Display order for the summary items';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID   
   add constraint TX_PLNT_RECON_FORM_SUMM_ID_PK
       primary key (TAX_PR_FORM_ID, TAX_PR_SUMMARY_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID   
   add constraint TX_PLNT_RECON_FORM_SUMM_ID_FK1
       foreign key (TAX_PR_FORM_ID)
       references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID) ;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID   
   add constraint TX_PLNT_RECON_FORM_SUMM_ID_FK2
       foreign key (TAX_PR_SECTION_ID)
       references TAX_PLANT_RECON_SECTION(TAX_PR_SECTION_ID) ;      
   
alter table PWRPLANT.TAX_PLANT_RECON_FORM_SUMM_ID   
   add constraint TX_PLNT_RECON_FORM_SUMM_ID_FK3
       foreign key (JURISDICTION_ID)
       references JURISDICTION (JURISDICTION_ID) ;

--Create table TAX_PLANT_RECON_FORM_SUMMARY       
--=========================================

create table PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY         
(
 TAX_PR_FORM_ID     NUMBER(22,0) not null,
 TAX_PR_SUMMARY_ID  NUMBER(22,0) not null,
 BEG_BALANCE        NUMBER(22,2) not null,
 ADDITIONS          NUMBER(22,2) not null,
 ADJUSTMENTS        NUMBER(22,2) not null,
 TRANSFERS          NUMBER(22,2) not null,
 RETIREMENTS        NUMBER(22,2) not null,
 BOOK_PROVISION     NUMBER(22,2) not null,
 ACTUAL_COR         NUMBER(22,2) not null,
 ACTUAL_SALVAGE     NUMBER(22,2) not null,
 OTHER              NUMBER(22,2) not null,
 END_BALANCE        NUMBER(22,2) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY  is 'The Tax Plant Recon Form Summary Table stores the beginning and ending balances as well as current year activity from Plant Asset, Depreciation, and PowerTax.  Summarized and calculated Form amounts are also stored.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.TAX_PR_SUMMARY_ID is 'System assigned identifier for a tax plant reconciliation summary.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.BEG_BALANCE is 'Beginning balance';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.ADDITIONS is 'Current year additions';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.ADJUSTMENTS is 'Current year adjustments';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.TRANSFERS is 'Current year transfers';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.RETIREMENTS is 'Current year retirements';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.BOOK_PROVISION is 'Current year book provision';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.ACTUAL_COR is 'Current year cost of removal';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.ACTUAL_SALVAGE is 'Current year salvage';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.OTHER is 'Current year transfers and gain/loss';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.END_BALANCE is 'Ending balance';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY   
   add constraint TX_PLNT_RECON_FORM_SUMMARY_PK
       primary key (TAX_PR_FORM_ID, TAX_PR_SUMMARY_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY   
   add constraint TX_PLNT_RECON_FORM_SUMMARY_FK1
       foreign key (TAX_PR_FORM_ID)
       references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID) ;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_SUMMARY   
   add constraint TX_PLNT_RECON_FORM_SUMMARY_FK2
       foreign key (TAX_PR_FORM_ID, TAX_PR_SUMMARY_ID)
       references TAX_PLANT_RECON_FORM_SUMM_ID(TAX_PR_FORM_ID, TAX_PR_SUMMARY_ID) ;      
       
--Create table TAX_PLANT_RECON_FORM_RES_SUMM        
--==========================================

create table PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM          
(
 TAX_PR_FORM_ID     NUMBER(22,0) not null,
 JURISDICTION_ID    NUMBER(22,0) not null,
 BEG_BALANCE        NUMBER(22,2) not null,
 DEPR_EXPENSE       NUMBER(22,2) not null,
 TRANSFERS          NUMBER(22,2) not null,
 RESERVE_RETIRE     NUMBER(22,2) not null,
 SALVAGE_IMPACTS    NUMBER(22,2) not null,
 COR_IMPACTS        NUMBER(22,2) not null,
 OTHER_BONUS        NUMBER(22,2) not null,
 END_BALANCE        NUMBER(22,2) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM   is 'The Tax Plant Recon Form Res Summ Table stores the Depreciation Reserve Summary amounts for a Plant Reconciliation Form.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.JURISDICTION_ID is 'System assigned identifier for a jurisdiction.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.BEG_BALANCE is 'Beginning balance';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.DEPR_EXPENSE is 'Current year depreciation expense';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.TRANSFERS is 'Current year transfers';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.RESERVE_RETIRE is 'Current year reserve retires';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.SALVAGE_IMPACTS is 'Current year salvage impacts';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.COR_IMPACTS is 'Current year COR impacts';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.OTHER_BONUS is 'Current year other bonus';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.END_BALANCE is 'Ending balance';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM    
   add constraint TX_PLNT_RECON_FORM_RES_SUMM_PK
       primary key (TAX_PR_FORM_ID, JURISDICTION_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM    
   add constraint TX_PLNT_RECON_FORM_RES_SUM_FK1
       foreign key (TAX_PR_FORM_ID)
       references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID) ;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_RES_SUMM    
   add constraint TX_PLNT_RECON_FORM_RES_SUM_FK2
       foreign key (JURISDICTION_ID)
       references JURISDICTION(JURISDICTION_ID) ;      

--Create table TAX_PLANT_RECON_ADJ         
--================================

create table PWRPLANT.TAX_PLANT_RECON_ADJ           
(
 TAX_PR_ADJ_ID      NUMBER(22,0) not null,
 DESCRITION         varchar2(100) not null,
 TAX_PR_SECTION_ID  NUMBER(22,0) not null,
 RECON_SOURCE       NUMBER(22,0) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_ADJ    is 'The Tax Plant Recon Adj Table stores the manual reconciling adjustments available to a Plant Reconciliation Form.';

comment on column PWRPLANT.TAX_PLANT_RECON_ADJ.TAX_PR_ADJ_ID is 'System assigned identifier for a tax plant reconciliation adjustment.';
comment on column PWRPLANT.TAX_PLANT_RECON_ADJ.DESCRITION is 'Description of a tax plant reconciliation adjustment.';
comment on column PWRPLANT.TAX_PLANT_RECON_ADJ.TAX_PR_SECTION_ID is 'System assigned identifier for a tax plant reconciliation section.';
comment on column PWRPLANT.TAX_PLANT_RECON_ADJ.RECON_SOURCE is '1 = GL; 2 = PowerTax';
comment on column PWRPLANT.TAX_PLANT_RECON_ADJ.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_ADJ.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_ADJ     
   add constraint TAX_PLANT_RECON_ADJ_PK
       primary key (TAX_PR_ADJ_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_ADJ     
   add constraint TAX_PLANT_RECON_ADJ_FK1
       foreign key (TAX_PR_SECTION_ID)
       references TAX_PLANT_RECON_SECTION(TAX_PR_SECTION_ID) ;
       
--Create table TAX_PLANT_RECON_FORM_ADJ         
--=====================================

create table PWRPLANT.TAX_PLANT_RECON_FORM_ADJ           
(
 TAX_PR_FORM_ID     NUMBER(22,0) not null,
 TAX_PR_ADJ_ID      NUMBER(22,0) not null,
 BEG_BALANCE        NUMBER(22,2) not null,
 ADDITIONS          NUMBER(22,2) not null,
 ADJUSTMENTS        NUMBER(22,2) not null,
 TRANSFERS          NUMBER(22,2) not null,
 RETIREMENTS        NUMBER(22,2) not null,
 BOOK_PROVISION     NUMBER(22,2) not null,
 ACTUAL_COR         NUMBER(22,2) not null,
 ACTUAL_SALVAGE     NUMBER(22,2) not null,
 OTHER              NUMBER(22,2) not null,
 END_BALANCE        NUMBER(22,2) not null,
 TIME_STAMP         date,
 USER_ID            varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_ADJ    is 'The Tax Plant Recon Form Adj Table stores the beginning and ending balances as well as current year activity by adjustment for a Plant Reconciliation form; the values are manually entered by the user.';

comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.TAX_PR_ADJ_ID is 'System assigned identifier for a tax plant reconciliation adjustment.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.BEG_BALANCE is 'Beginning balance';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.ADDITIONS is 'Current year additions for the tax plant reconciliation adjustment.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.ADJUSTMENTS is 'Current year adjustments for the tax plant reconciliation adjustment.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.TRANSFERS is 'Current year transfers for the tax plant reconciliation adjustment.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.RETIREMENTS is 'Current year retirements for the tax plant reconciliation adjustment.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.BOOK_PROVISION is 'Current year book provision';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.ACTUAL_COR is 'Current year cost of removal';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.ACTUAL_SALVAGE is 'Current year salvage';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.OTHER is 'Current year transfers and gain/loss';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.END_BALANCE is 'Ending balance';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_ADJ.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_ADJ     
   add constraint TAX_PLANT_RECON_FORM_ADJ_PK
       primary key (TAX_PR_FORM_ID, TAX_PR_ADJ_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_ADJ     
   add constraint TAX_PLANT_RECON_FORM_ADJ_FK1
       foreign key (TAX_PR_FORM_ID)
       references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID) ;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_ADJ     
   add constraint TAX_PLANT_RECON_FORM_ADJ_FK2
       foreign key (TAX_PR_ADJ_ID  )
       references TAX_PLANT_RECON_ADJ(TAX_PR_ADJ_ID  ) ;      
       
       
--Create table TAX_PLANT_RECON_FORM_REFRESH
--=========================================

create table PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH 
(
 TAX_PR_FORM_ID NUMBER(22,0) not null,
 FORM_SOURCE_ID NUMBER(22,0) not null,
 REFRESH_DATE   date,
 TIME_STAMP     date,
 USER_ID        varchar2(18)
);

comment on table PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH  is 'The Tax Plant Recon Form Refresh Table stores the date of last refresh for each source of data.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH.TAX_PR_FORM_ID is 'System assigned identifier for a tax plant reconciliation form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH.FORM_SOURCE_ID is '1 = Plant Asset Data; 2 = Plant Depreciation Data; 3 = PowerTax Data';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH.REFRESH_DATE is 'Date of last refresh for the source of data for the form.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes.';
comment on column PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH.USER_ID is 'Standard system-assigned user id used for audit purposes.';

alter table PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH 
   add constraint TX_PLNT_RECON_FORM_REFRESH_PK
       primary key (TAX_PR_FORM_ID, FORM_SOURCE_ID)
       using index tablespace PWRPLANT_IDX;
       
alter table PWRPLANT.TAX_PLANT_RECON_FORM_REFRESH 
   add constraint TX_PLNT_RECON_FORM_REFRESH_FK1
   foreign key (TAX_PR_FORM_ID)
   references TAX_PLANT_RECON_FORM(TAX_PR_FORM_ID);


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3089, 0, 2016, 1, 0, 0, 045519, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045519_pwrtax_plant_reconciliation_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;