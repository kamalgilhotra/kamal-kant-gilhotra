/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045060_lease_ii_calc_changes_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2.0.0 10/07/2015 Will Davis Fix interim interest calculations
||============================================================================
*/

create table ls_calc_ii_stg
(id number(22,0),
 month date,
 time_stamp date,
 user_id varchar2(35),
 component_id number(22,0),
 interim_interest_start_date date,
 current_lease_cost number(22,8),
 est_in_svc_date date,
 cut_off_day number(22,0),
 ii_rate number(22,8),
 days_in_month_basis number(22,0),
 days_in_year_basis number(22,0),
 days_of_interest number(22,0),
 roll_forward_amount number(22,8),
 amount number(22,8));

ALTER TABLE ls_calc_ii_stg
  ADD CONSTRAINT ls_calc_ii_stg_pk PRIMARY KEY (
    id,
    month
	) using index tablespace pwrplant_idx
/

comment on table ls_calc_ii_stg is 'A calculation staging table for interim interest';
comment on column ls_calc_ii_stg.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_calc_ii_stg.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_calc_ii_stg.id is 'The ID associated with the invoice on the ls_component_charge table';
comment on column ls_calc_ii_stg.month is 'The month in which the calculation is taking place';
comment on column ls_calc_ii_stg.component_id is 'The internal ID associated with the component';
comment on column ls_calc_ii_stg.interim_interest_start_date is 'The day in which interim interest calculations beging';
comment on column ls_calc_ii_stg.current_lease_cost is 'The amount associated with the component invoice';
comment on column ls_calc_ii_stg.est_in_svc_date is 'The estimated in service date of the ILR associed with the component';
comment on column ls_calc_ii_stg.cut_off_day is 'The day after which interim interest calculations are paid in the following month';
comment on column ls_calc_ii_stg.ii_rate is 'The effective dated rate that is used for interim interest calculations';
comment on column ls_calc_ii_stg.days_in_month_basis is 'The number of days in the month that are used for interest calculations';
comment on column ls_calc_ii_stg.days_in_year_basis is 'The denominator used for interest calculations. Indicates the number of days in the year';
comment on column ls_calc_ii_stg.days_of_interest is 'The actual number of days of interest paid in the month for this invoice';
comment on column ls_calc_ii_stg.roll_forward_amount is 'The amount of interest for this month that is paid in the following month due to the interim interest start date being after the cut off day';
comment on column ls_calc_ii_stg.amount is 'The total interim interest paid for the month for this invoice';

create table ls_calc_ii_stg_arc
(id number(22,0),
 month date,
 time_stamp date,
 user_id varchar2(35),
 component_id number(22,0),
 interim_interest_start_date date,
 current_lease_cost number(22,8),
 est_in_svc_date date,
 cut_off_day number(22,0),
 ii_rate number(22,8),
 days_in_month_basis number(22,0),
 days_in_year_basis number(22,0),
 days_of_interest number(22,0),
 roll_forward_amount number(22,8),
 amount number(22,8));

ALTER TABLE ls_calc_ii_stg_arc
  ADD CONSTRAINT ls_calc_ii_stg_arc_pk PRIMARY KEY (
    id,
    month
	) using index tablespace pwrplant_idx
/

comment on table ls_calc_ii_stg_arc is 'An archive table for the calculation staging table for interim interest';
comment on column ls_calc_ii_stg_arc.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_calc_ii_stg_arc.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column ls_calc_ii_stg_arc.id is 'The ID associated with the invoice on the ls_component_charge table';
comment on column ls_calc_ii_stg_arc.month is 'The month in which the calculation is taking place';
comment on column ls_calc_ii_stg_arc.component_id is 'The internal ID associated with the component';
comment on column ls_calc_ii_stg_arc.interim_interest_start_date is 'The day in which interim interest calculations beging';
comment on column ls_calc_ii_stg_arc.current_lease_cost is 'The amount associated with the component invoice';
comment on column ls_calc_ii_stg_arc.est_in_svc_date is 'The estimated in service date of the ILR associed with the component';
comment on column ls_calc_ii_stg_arc.cut_off_day is 'The day after which interim interest calculations are paid in the following month';
comment on column ls_calc_ii_stg_arc.ii_rate is 'The effective dated rate that is used for interim interest calculations';
comment on column ls_calc_ii_stg_arc.days_in_month_basis is 'The number of days in the month that are used for interest calculations';
comment on column ls_calc_ii_stg_arc.days_in_year_basis is 'The denominator used for interest calculations. Indicates the number of days in the year';
comment on column ls_calc_ii_stg_arc.days_of_interest is 'The actual number of days of interest paid in the month for this invoice';
comment on column ls_calc_ii_stg_arc.roll_forward_amount is 'The amount of interest for this month that is paid in the following month due to the interim interest start date being after the cut off day';
comment on column ls_calc_ii_stg_arc.amount is 'The total interim interest paid for the month for this invoice';

alter table ls_calc_ii_stg_arc add principal_for_month number(22,8);
comment on column ls_calc_ii_stg_arc.principal_for_month is 'The principal on the schedule for this asset for the given month'; 
alter table ls_calc_ii_stg_arc add payment_for_month number(22,8);
comment on column ls_calc_ii_stg_arc.payment_for_month is 'The payment amount on the ILR payment term for this asset for the given month';
alter table ls_calc_ii_stg add principal_for_month number(22,8);
comment on column ls_calc_ii_stg.principal_for_month is 'The principal on the schedule for this asset for the given month'; 
alter table ls_calc_ii_stg add payment_for_month number(22,8);
comment on column ls_calc_ii_stg.payment_for_month is 'The payment amount on the ILR payment term for this asset for the given month';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
   SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
   (2903, 0, 2015, 2, 0, 0, 45060, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_045060_lease_ii_calc_changes_ddl.sql', 1,
   SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;