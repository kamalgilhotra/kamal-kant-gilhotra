/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039927_reg_ifa_indexes.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 09/10/2014 Shane Ward
||============================================================================
*/

--INCREMENTAL INDEXES
create index INCREMENTAL_DEPR_ADJ_DATA_IDX
   on INCREMENTAL_DEPR_ADJ_DATA (VERSION_ID,
                                 TAX_CLASS_ID,
                                 NORMALIZATION_ID,
                                 JURISDICTION_ID)
      tablespace PWRPLANT_IDX;

create index INCREMENTAL_FP_ADJ_DATA_IDX
   on INCREMENTAL_FP_ADJ_DATA (VERSION_ID,
                               TAX_CLASS_ID,
                               NORMALIZATION_ID,
                               JURISDICTION_ID)
      tablespace PWRPLANT_IDX;

create index REG_INCREMENTAL_DEPR_TR_IDX
   on REG_INCREMENTAL_DEPR_ADJ_TRANS (VERSION_ID,
                                      TAX_CLASS_ID,
                                      NORMALIZATION_ID,
                                      JURISDICTION_ID)
      tablespace PWRPLANT_IDX;

create index REG_INCREMENTAL_FP_TR_IDX
   on REG_INCREMENTAL_FP_ADJ_TRANS (VERSION_ID,
                                    TAX_CLASS_ID,
                                    NORMALIZATION_ID,
                                    JURISDICTION_ID)
      tablespace PWRPLANT_IDX;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1400, 0, 10, 4, 2, 7, 39927, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039927_reg_ifa_indexes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;