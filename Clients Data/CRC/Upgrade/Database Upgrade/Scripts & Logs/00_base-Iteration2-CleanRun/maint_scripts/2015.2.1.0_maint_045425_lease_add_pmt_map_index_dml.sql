/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_045425_add_pmt_map_index_dml.sql
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| --------   ---------- -------------- --------------------------------------
|| 2015.2.1.0 02/12/2016 Will Davis  Add index to payment map table
||============================================================================
*/

declare
v_count number;
begin
SELECT count(1)
into v_count
fROM ALL_IND_COLUMNS
WHERE INDEX_NAME IN (SELECT INDEX_NAME FROM DBA_INDEXES WHERE TABLE_NAME= 'LS_INVOICE_PAYMENT_MAP' AND INDEX_NAME <> 'PK_LS_INVOICE_PAYMENT_MAP');
if v_count = 0 then
execute immediate '
CREATE INDEX ix_inv_pmt_map
  ON ls_invoice_payment_map (
  payment_id
  )
  TABLESPACE pwrplant_idx';
end if;
end;
/

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3060, 0, 2015, 2, 1, 0, 045425, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.1.0_maint_045425_lease_add_pmt_map_index_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;