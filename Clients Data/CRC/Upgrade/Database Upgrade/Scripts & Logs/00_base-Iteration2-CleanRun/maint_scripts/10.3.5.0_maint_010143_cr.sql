/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_010143_cr.sql
|| Description:
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date       Revised By     Reason for Change
|| ----------  ---------- -------------- ----------------------------------------
|| 10.3.5.0    07/03/2012 Joseph King    Point Release
||============================================================================
*/

alter table CR_BATCH_DERIVATION_CONTROL add OVERRIDE2_DERIVATIONS number(22,0);

update PWRPLANT.CR_BATCH_DERIVATION_CONTROL
   set OVERRIDE2_DERIVATIONS = 0
 where OVERRIDE2_DERIVATIONS is null;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (162, 0, 10, 3, 5, 0, 10143, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.5.0_maint_010143_cr.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
