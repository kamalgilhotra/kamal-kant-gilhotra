/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_051529_lessee_01_add_new_payment_type_dml.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.4.0.0 06/12/2018 David Levine     Insert new payment type
||============================================================================
*/

insert into LS_PAYMENT_TERM_TYPE (payment_term_type_id, description, long_description)
values (4,'Partial Month','Beginning and ending of the Lease Term only include a fraction of the month rather than the full month.');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6985, 0, 2017, 4, 0, 0, 51529, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051529_lessee_01_add_new_payment_type_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
