/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_047561_lease_pymt_hdr_invoice_multi_curr_2_dml.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 05/30/2017 Shane Ward   Update Lease Payment Header Invoice Query for Multi Curr
||============================================================================
*/

UPDATE pp_any_query_criteria SET SQL =
'select to_char(lph.gl_posting_mo_yr, ''yyyymm'') as monthnum,
       lph.company_id,
       lv.description as vendor,
       aps.description as payment_status,
       lpl.amount as calculated_amount, 
       lpl.adjustment_amount as adjustments, 
       lph.amount as total_payment, 
       inv.invoice_number,
       inv.invoice_amount, 
       decode(map.in_tolerance, 1, ''Yes'', null, null, ''No'') as in_tolerance,
       ll.lease_id,
       ll.lease_number,
       lct.description as lease_cap_type,
       nvl(ilr.ilr_id, -1) as ilr_id,
       nvl(ilr.ilr_number, ''N/A'') as ilr_number,
       nvl(lfs.description, ''N/A'') as funding_status,
       nvl(la.ls_asset_id, -1) as ls_asset_id,
       nvl(la.leased_asset_number, ''N/A'') as leased_asset_number,
       co.description as company_description,
       (select initcap(filter_value)
          from pp_any_required_filter
         where upper(trim(column_name)) = ''CURRENCY TYPE'') currency_type,
       lph.iso_code currency,
       lph.currency_display_symbol currency_symbol
  from v_ls_payment_hdr_fx lph, -- Multi Curr View
       ls_asset la,
       ls_ilr ilr,
       ls_lease ll,
       company co,
       ls_lease_cap_type lct,
       ls_funding_status lfs,
       ls_vendor lv,
       approval_status aps,
       ls_invoice_payment_map map,
       v_ls_invoice_fx_vw inv,
       (select payment_id,
               sum(nvl(amount, 0)) as amount,
               sum(nvl(adjustment_amount, 0)) as adjustment_amount,
               ls_cur_type
          from v_ls_payment_line_fx -- Multi Curr View
         where to_char(gl_posting_mo_yr, ''yyyymm'') in
               (select filter_value
                  from pp_any_required_filter
                 where upper(trim(column_name)) = ''MONTHNUM'')',
  sql2 ='       group by payment_id, ls_cur_type) lpl
 where lph.lease_id = ll.lease_id
   and lph.ls_asset_id = la.ls_asset_id(+)
   and lph.ilr_id = ilr.ilr_id(+)
   and ilr.funding_status_id = lfs.funding_status_id(+)
   and co.company_id = lph.company_id
   and ll.lease_cap_type_id = lct.ls_lease_cap_type_id
   and lph.vendor_id = lv.vendor_id
   and lph.payment_status_id = aps.approval_status_id
   and to_char(lph.gl_posting_mo_yr, ''yyyymm'') in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''MONTHNUM'')
   and lph.company_id in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''COMPANY ID'')
   AND lpl.ls_cur_type = lph.ls_cur_type
   AND lpl.ls_cur_type =
       (SELECT ls_currency_type_id
          FROM ls_lease_currency_type
         WHERE Upper(description) =
               Upper((select filter_value
                       from pp_any_required_filter
                      where upper(trim(column_name)) = ''CURRENCY TYPE'')))
   and lpl.payment_id = lph.payment_id
   AND inv.ls_cur_type =
       (SELECT ls_currency_type_id
          FROM ls_lease_currency_type
         WHERE Upper(description) =
               Upper((select filter_value
                       from pp_any_required_filter
                      where upper(trim(column_name)) = ''CURRENCY TYPE'')))
   and lph.payment_id = map.payment_id(+)
   and map.invoice_id = inv.invoice_id(+)'
   WHERE description = 'Lease Payment Header and Invoice Detail';


--Currency Type
INSERT INTO pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, column_Header,
column_width, display_field, display_table, column_type,
quantity_field, data_field, required_filter, required_one_mult, hide_from_results, hide_from_filters)
SELECT id, 'currency_type', 20,  0,1, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'ls_currency_type_id', 1, 2, 1, 0
FROM pp_any_query_criteria WHERE description = 'Lease Payment Header and Invoice Detail'
;

--Currency Symbol
INSERT INTO pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, column_Header,
column_width, display_field, display_table, column_type,
quantity_field, data_field, required_filter, required_one_mult, hide_from_results, hide_from_filters)
SELECT id, 'currency_symbol', 21,  0, 0, 'Currency Symbol', 300, null, null, 'VARCHAR2', 0, null, null, null, 1, 1
FROM pp_any_query_criteria WHERE description = 'Lease Payment Header and Invoice Detail'
;
--Currency
INSERT INTO pp_any_query_criteria_fields (id, detail_field, column_order, amount_field, include_in_select_criteria, column_Header,
column_width, display_field, display_table, column_type,
quantity_field, data_field, required_filter, required_one_mult, hide_from_results, hide_from_filters)
SELECT id, 'currency', 22,  0, 0, 'Currency', 300, null, null, 'VARCHAR2', 0, null, null, null, 0, 1
FROM pp_any_query_criteria WHERE description = 'Lease Payment Header and Invoice Detail'
;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3518, 0, 2017, 1, 0, 0, 47561, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_047561_lease_pymt_hdr_invoice_multi_curr_2_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;	