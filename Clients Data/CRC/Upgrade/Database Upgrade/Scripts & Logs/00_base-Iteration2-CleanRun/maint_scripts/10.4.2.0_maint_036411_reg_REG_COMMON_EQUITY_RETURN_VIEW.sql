/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_036411_reg_REG_COMMON_EQUITY_RETURN_VIEW.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 03/11/2014 Ryan Oliveria
||============================================================================
*/

create or replace view REG_COMMON_EQUITY_RETURN_VIEW
as
select CASE_NAME, COMPANY, JURISDICTION, MONTH_YEAR, RETURN_ON_COMMON, OVERALL_RETURN, REG_CASE_ID, MONTH_NUMBER
  from (select 'Financial Monitoring' CASE_NAME,
               RC.DESCRIPTION COMPANY,
               J.DESCRIPTION JURISDICTION,
               TO_CHAR(S.MONTH_YEAR, 'MM / YYYY') MONTH_YEAR,
               S.RETURN_ON_COMMON RETURN_ON_COMMON,
               S.OVERALL_RETURN OVERALL_RETURN,
               -1 REG_CASE_ID,
               to_number(to_char(s.MONTH_YEAR, 'yyyymm')) MONTH_NUMBER
          from REG_COMPANY RC, REG_JURISDICTION J, REG_FINANCIAL_MONITOR_SUMMARY S
         where S.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and S.REG_JURISDICTION_ID = J.REG_JURISDICTION_ID
        union
        select C.CASE_NAME CASE_NAME,
               RC.DESCRIPTION COMPANY,
               J.DESCRIPTION JURISDICTION,
               TO_CHAR(S.MONTH_YEAR, 'MM / YYYY') MONTH_YEAR,
               R.COMMON_EQUITY_RETURN RETURN_ON_COMMON,
               R.OVERALL_RETURN OVERALL_RETURN,
               C.REG_CASE_ID,
               to_number(to_char(s.MONTH_YEAR, 'yyyymm')) MONTH_NUMBER
          from REG_CASE                      C,
               REG_CASE_RESULTS              R,
               REG_COMPANY                   RC,
               REG_JURISDICTION              J,
               REG_FINANCIAL_MONITOR_SUMMARY S
         where S.REG_COMPANY_ID = RC.REG_COMPANY_ID
           and S.REG_JURISDICTION_ID = J.REG_JURISDICTION_ID
           and C.REG_CASE_TYPE = 1
           and C.REG_CASE_ID = R.REG_CASE_ID
           and R.RESULT_TYPE = 1 /* Jur Result Type */
           and S.REG_JURISDICTION_ID = C.REG_JURISDICTION_ID
           and TO_NUMBER(TO_CHAR(S.MONTH_YEAR, 'YYYYMM')) = R.MONTH_YEAR);

 --**************************
 -- Log the run of the script
 --**************************

 insert into PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
     SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
 values
    (1032, 0, 10, 4, 2, 0, 36411, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_036411_reg_REG_COMMON_EQUITY_RETURN_VIEW.sql', 1,
     SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
     SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;