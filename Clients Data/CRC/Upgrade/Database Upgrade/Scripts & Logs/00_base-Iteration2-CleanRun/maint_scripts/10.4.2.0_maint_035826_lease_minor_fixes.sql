/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_035826_lease_minor_fixes.sql
|| Description:
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 10.4.2.0 01/22/2014  Kyle Peterson
||============================================================================
*/

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (253, 'sub_account_id', 612);
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (253, 'sub_account_id', 613);
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
values
   (253, 'sub_account_id', 614);

update PP_IMPORT_TEMPLATE_FIELDS
   set IMPORT_LOOKUP_ID = 612
 where IMPORT_TEMPLATE_ID =
       (select IMPORT_TEMPLATE_ID from PP_IMPORT_TEMPLATE where DESCRIPTION = 'Leased Asset Add')
   and COLUMN_NAME = 'sub_account_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (902, 0, 10, 4, 2, 0, 35826, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_035826_lease_minor_fixes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;