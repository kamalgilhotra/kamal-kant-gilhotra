/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_048937_lessor_drop_lessees_sequence_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 09/13/2017 Jared Watkins  Drop the new sequence for the Lessor Lessee's workspace
||============================================================================
*/

drop sequence lsr_lessee_seq;


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3705, 0, 2017, 1, 0, 0, 48937, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048937_lessor_drop_lessees_sequence_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;