/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_045861_020_reg_schedule_templates_dml.sql
||============================================================================
|| Copyright (C) 2016 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2016.1.0.0 08/18/2016 Shane Ward		 Schedule Builder Template Functionality
||============================================================================
*/


 UPDATE ppbase_menu_items SET item_order = 6 WHERE label = 'Charting and Graphing Dashboard' AND 
 MODULE = 'REG' AND parent_menu_identifier = 'REPORTS' ;
 
 UPDATE ppbase_menu_items SET item_order = 5 WHERE label = 'Regulatory Analysis' AND 
 MODULE = 'REG' AND parent_menu_identifier = 'REPORTS' ;
 
 UPDATE ppbase_menu_items SET item_order = 4 WHERE label = 'Schedule Export' AND 
 MODULE = 'REG' AND parent_menu_identifier = 'REPORTS' ;

UPDATE ppbase_menu_items SET item_order = 3 WHERE label = 'Schedule Configuration' AND 
 MODULE = 'REG' AND parent_menu_identifier = 'REPORTS' ;

--Fix Reg_schedule_sql SQLs

update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.total_co_included_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 1;
 
update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.total_co_adjust_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 2;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.total_co_final_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 3;
 
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_alloc_results_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 4;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_adjust_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 5;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_final_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 6;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_recovery_class_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 7;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.jur_base_rev_req_amount * sign) total
          FROM reg_case_summary_ledger  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 8;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.unadjusted * sign) total
          FROM reg_case_composition  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND coverage_yr_ended = <case_year>
           AND a.schedule_id = <schedule_id>
           AND s.gl_month = <gl_month>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 9;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.adjustments * sign) total
          FROM reg_case_composition  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND coverage_yr_ended = <case_year>
           AND a.schedule_id = <schedule_id>
           AND s.gl_month = <gl_month>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 10;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(s.per_books_adjusted * sign) total
          FROM reg_case_composition  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND coverage_yr_ended = <case_year>
           AND a.schedule_id = <schedule_id>
           AND s.gl_month = <gl_month>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = <schedule_id>
 ORDER BY lines.line_order'
 where sql_id = 11;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(per_books * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_target_id = <alloc_target>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order'
 where sql_id = 12;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(adj_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_target_id = <alloc_target>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order'
 where sql_id = 13;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(alloc_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_target_id = <alloc_target>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order'
 where sql_id = 14;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(per_books * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_category_id = <alloc_category>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order'
 where sql_id = 15;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(adj_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_category_id = <alloc_category>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order'
 where sql_id = 16;
 
 update reg_schedule_sql set sql_string = 'SELECT decode(line_type_id, 1, nvl(sub.total, 0), null) total,
       lines.line_id
  FROM (SELECT l.line_id, Sum(alloc_amount * Sign) total
          FROM reg_alloc_results_w_tax  s,
               reg_schedule_line_acct   a,
               reg_schedule_line_number l
         WHERE reg_case_id = <case_id>
           AND case_year = <case_year>
           AND a.schedule_id = <schedule_id>
           AND reg_alloc_category_id = <alloc_category>
		   and <filter_string>
           AND s.reg_acct_id = a.reg_acct_id
           AND l.line_id = a.line_id
           AND l.schedule_id = a.schedule_id
         GROUP BY l.line_id) sub,
       reg_schedule_line_number lines
 WHERE sub.line_id(+) = lines.line_id
   AND lines.schedule_id = sub.schedule_id
 ORDER BY lines.line_order'
 where sql_id = 17;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3275, 0, 2016, 1, 0, 0, 045861, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2016.1.0.0_maint_045861_020_reg_schedule_templates_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;