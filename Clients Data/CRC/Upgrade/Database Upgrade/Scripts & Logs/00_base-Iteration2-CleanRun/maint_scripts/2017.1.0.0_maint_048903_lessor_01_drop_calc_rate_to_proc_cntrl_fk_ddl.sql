/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048903_lessor_01_drop_calc_rate_to_proc_cntrl_fk_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/28/2017 Charlie Shilling existing FK causes issues with lessor
||											only clients who wouldn't maintain
||											the ls_process_control table
||============================================================================
*/
ALTER TABLE LS_LEASE_CALCULATED_DATE_RATES
  DROP CONSTRAINT ls_lease_calc_date_rates_fk1;

ALTER TABLE LS_LEASE_CALCULATED_DATE_RATES
  ADD CONSTRAINT ls_lease_calc_date_rates_fk1 FOREIGN KEY ( company_id ) REFERENCES COMPANY_SETUP ( company_id ); 

  
--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(4052, 0, 2017, 1, 0, 0, 48903, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048903_lessor_01_drop_calc_rate_to_proc_cntrl_fk_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
