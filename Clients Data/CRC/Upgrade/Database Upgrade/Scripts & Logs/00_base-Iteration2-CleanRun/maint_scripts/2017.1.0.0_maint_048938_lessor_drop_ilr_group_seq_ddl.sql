/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048938_lessor_drop_ilr_group_seq_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date     Revised By      Reason for Change
|| ---------- -------- --------------  ------------------------------------
|| 2017.1.0.0 9/12/2017 Shane "C" Ward   Remove the Lessor-specific ILR Groups sequence
||============================================================================
*/

DROP SEQUENCE lsr_ilr_group_seq;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3699, 0, 2017, 1, 0, 0, 48938, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048938_lessor_drop_ilr_group_seq_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;