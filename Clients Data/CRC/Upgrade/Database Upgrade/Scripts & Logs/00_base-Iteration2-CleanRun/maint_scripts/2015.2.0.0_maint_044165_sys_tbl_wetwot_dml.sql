 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_044165_sys_tbl_wetwot_dml.sql
 ||============================================================================
 || Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2 06/19/2015 	D. Mendel   Remove duplicate powerplant_columns
 ||============================================================================
 */ 
 
delete from powerplant_columns
where table_name = 'wo_est_template_wo_type'
and column_name in ('TIME_STAMP', 'USER_ID', 'WO_EST_TEMPLATE_ID', 'WORK_ORDER_TYPE_ID');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2619, 0, 2015, 2, 0, 0, 044165, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044165_sys_tbl_wetwot_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;