/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_049482_lessor_01_create_var_payments_schedule_calc_tables_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2017.1.0.0 10/30/2017 Andrew Hill    Create tables & views for lessor variable payments calcs
||
||============================================================================
*/
CREATE GLOBAL TEMPORARY TABLE lsr_asset_schedule_tmp (lsr_asset_id NUMBER,
                                                      revision NUMBER,
                                                      set_of_books_id NUMBER,
                                                      month DATE,
                                                      interest_income_received NUMBER,
                                                      interest_income_accrued NUMBER,
                                                      interest_rental_recvd_spread NUMBER,
                                                      beg_deferred_rev NUMBER,
                                                      deferred_rev_activity NUMBER,
                                                      end_deferred_rev NUMBER,
                                                      beg_receivable NUMBER,
                                                      end_receivable NUMBER,
                                                      executory_accrual1 NUMBER,
                                                      executory_accrual2 NUMBER,
                                                      executory_accrual3 NUMBER,
                                                      executory_accrual4 NUMBER,
                                                      executory_accrual5 NUMBER,
                                                      executory_accrual6 NUMBER,
                                                      executory_accrual7 NUMBER,
                                                      executory_accrual8 NUMBER,
                                                      executory_accrual9 NUMBER,
                                                      executory_accrual10 NUMBER,
                                                      executory_paid1 NUMBER,
                                                      executory_paid2 NUMBER,
                                                      executory_paid3 NUMBER,
                                                      executory_paid4 NUMBER,
                                                      executory_paid5 NUMBER,
                                                      executory_paid6 NUMBER,
                                                      executory_paid7 NUMBER,
                                                      executory_paid8 NUMBER,
                                                      executory_paid9 NUMBER,
                                                      executory_paid10 NUMBER,
                                                      contingent_accrual1 NUMBER,
                                                      contingent_accrual2 NUMBER,
                                                      contingent_accrual3 NUMBER,
                                                      contingent_accrual4 NUMBER,
                                                      contingent_accrual5 NUMBER,
                                                      contingent_accrual6 NUMBER,
                                                      contingent_accrual7 NUMBER,
                                                      contingent_accrual8 NUMBER,
                                                      contingent_accrual9 NUMBER,
                                                      contingent_accrual10 NUMBER,
                                                      contingent_paid1 NUMBER,
                                                      contingent_paid2 NUMBER,
                                                      contingent_paid3 NUMBER,
                                                      contingent_paid4 NUMBER,
                                                      contingent_paid5 NUMBER,
                                                      contingent_paid6 NUMBER,
                                                      contingent_paid7 NUMBER,
                                                      contingent_paid8 NUMBER,
                                                      contingent_paid9 NUMBER,
                                                      contingent_paid10 NUMBER);
                                                      
COMMENT ON COLUMN lsr_asset_schedule_tmp.lsr_asset_id IS 'The Lessor Asset ID';
COMMENT ON COLUMN lsr_asset_schedule_tmp.revision IS 'The revision with which this asset is associated';
COMMENT ON COLUMN lsr_asset_schedule_tmp.set_of_books_id IS 'The Set of Books which which the values are associated';
COMMENT ON COLUMN lsr_asset_schedule_tmp.month IS 'The month with which the values are associated';
COMMENT ON COLUMN lsr_asset_schedule_tmp.interest_income_received IS 'The interest/income received in this period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.interest_income_accrued IS 'The interest/income accrued in this period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.interest_rental_recvd_spread IS 'The interest/income spread in this period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.beg_deferred_rev IS 'The deferred revenue at the beginning of the period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.deferred_rev_activity IS 'The deferred revenue activity in the period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.end_deferred_rev IS 'The deferred revenue at the end of the period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.beg_receivable IS 'The receivable amount at the beginning of the period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.end_receivable IS 'The receviable amount at the end of the period';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual1 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual2 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual3 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual4 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual5 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual6 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual7 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual8 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual9 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_accrual10 IS 'Executory Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid1 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid2 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid3 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid4 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid5 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid6 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid7 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid8 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid9 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.executory_paid10 IS 'Executory Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual1 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual2 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual3 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual4 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual5 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual6 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual7 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual8 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual9 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_accrual10 IS 'Contingent Accrual Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid1 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid2 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid3 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid4 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid5 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid6 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid7 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid8 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid9 IS 'Contingent Paid (Received) Buckets';
COMMENT ON COLUMN lsr_asset_schedule_tmp.contingent_paid10 IS 'Contingent Paid (Received) Buckets';

COMMENT ON TABLE lsr_asset_schedule_tmp IS 'Tempoary "asset schedule" (allocated from ILR Scheduled based on FMV) created for use in lessor variable payments calculations';

--Indices modeled on ls_asset_schedule
CREATE INDEX lsr_asset_sch_tmp_main_idx ON lsr_asset_schedule_tmp(lsr_asset_id, revision, set_of_books_id, MONTH);
CREATE INDEX lsr_asset_sch_tmp_sch_idx ON lsr_asset_schedule_tmp(lsr_asset_id, set_of_books_id, revision);
create index lsr_asset_sch_tmp_tmth_idx on lsr_asset_schedule_tmp(trunc(month, 'MONTH'));


CREATE OR REPLACE VIEW v_lsr_pseudo_asset_schedule AS
SELECT lsr_asset_id,
       revision,
       set_of_books_id,
       month,
       interest_income_received * allocation AS interest_income_received,
       interest_income_accrued * allocation AS interest_income_accrued,
       interest_rental_recvd_spread * allocation AS interest_rental_recvd_spread,
       beg_deferred_rev * allocation AS beg_deferred_rev,
       deferred_rev_activity * allocation AS deferred_rev_activity,
       end_deferred_rev * allocation AS end_deferred_rev,
       beg_receivable * allocation AS beg_receivable,
       end_receivable * allocation AS end_receivable,
       executory_accrual1 * allocation AS executory_accrual1,
       executory_accrual2 * allocation AS executory_accrual2,
       executory_accrual3 * allocation AS executory_accrual3,
       executory_accrual4 * allocation AS executory_accrual4,
       executory_accrual5 * allocation AS executory_accrual5,
       executory_accrual6 * allocation AS executory_accrual6,
       executory_accrual7 * allocation AS executory_accrual7,
       executory_accrual8 * allocation AS executory_accrual8,
       executory_accrual9 * allocation AS executory_accrual9,
       executory_accrual10 * allocation AS executory_accrual10,
       executory_paid1 * allocation AS executory_paid1,
       executory_paid2 * allocation AS executory_paid2,
       executory_paid3 * allocation AS executory_paid3,
       executory_paid4 * allocation AS executory_paid4,
       executory_paid5 * allocation AS executory_paid5,
       executory_paid6 * allocation AS executory_paid6,
       executory_paid7 * allocation AS executory_paid7,
       executory_paid8 * allocation AS executory_paid8,
       executory_paid9 * allocation AS executory_paid9,
       executory_paid10 * allocation AS executory_paid10,
       contingent_accrual1 * allocation AS contingent_accrual1,
       contingent_accrual2 * allocation AS contingent_accrual2,
       contingent_accrual3 * allocation AS contingent_accrual3,
       contingent_accrual4 * allocation AS contingent_accrual4,
       contingent_accrual5 * allocation AS contingent_accrual5,
       contingent_accrual6 * allocation AS contingent_accrual6,
       contingent_accrual7 * allocation AS contingent_accrual7,
       contingent_accrual8 * allocation AS contingent_accrual8,
       contingent_accrual9 * allocation AS contingent_accrual9,
       contingent_accrual10 * allocation AS contingent_accrual10,
       contingent_paid1 * allocation AS contingent_paid1,
       contingent_paid2 * allocation AS contingent_paid2,
       contingent_paid3 * allocation AS contingent_paid3,
       contingent_paid4 * allocation AS contingent_paid4,
       contingent_paid5 * allocation AS contingent_paid5,
       contingent_paid6 * allocation AS contingent_paid6,
       contingent_paid7 * allocation AS contingent_paid7,
       contingent_paid8 * allocation AS contingent_paid8,
       contingent_paid9 * allocation AS contingent_paid9,
       contingent_paid10 * allocation AS contingent_paid10
FROM (SELECT  asset.lsr_asset_id,
              sch.revision,
              sch.set_of_books_id,
              sch.month,
              sch.interest_income_received,
              sch.interest_income_accrued,
              sch.interest_rental_recvd_spread,
              sch.beg_deferred_rev,
              sch.deferred_rev_activity,
              sch.end_deferred_rev,
              sch.beg_receivable,
              sch.end_receivable,
              sch.executory_accrual1,
              sch.executory_accrual2,
              sch.executory_accrual3,
              sch.executory_accrual4,
              sch.executory_accrual5,
              sch.executory_accrual6,
              sch.executory_accrual7,
              sch.executory_accrual8,
              sch.executory_accrual9,
              sch.executory_accrual10,
              sch.executory_paid1,
              sch.executory_paid2,
              sch.executory_paid3,
              sch.executory_paid4,
              sch.executory_paid5,
              sch.executory_paid6,
              sch.executory_paid7,
              sch.executory_paid8,
              sch.executory_paid9,
              sch.executory_paid10,
              sch.contingent_accrual1,
              sch.contingent_accrual2,
              sch.contingent_accrual3,
              sch.contingent_accrual4,
              sch.contingent_accrual5,
              sch.contingent_accrual6,
              sch.contingent_accrual7,
              sch.contingent_accrual8,
              sch.contingent_accrual9,
              sch.contingent_accrual10,
              sch.contingent_paid1,
              sch.contingent_paid2,
              sch.contingent_paid3,
              sch.contingent_paid4,
              sch.contingent_paid5,
              sch.contingent_paid6,
              sch.contingent_paid7,
              sch.contingent_paid8,
              sch.contingent_paid9,
              sch.contingent_paid10,
              COALESCE(ratio_to_report(asset.fair_market_value) 
                        OVER(PARTITION BY asset.ilr_id, asset.revision, sch.MONTH, sch.set_of_books_id),
                        0) AS allocation
      FROM lsr_ilr_schedule sch
      JOIN lsr_asset asset ON sch.ilr_id = asset.ilr_id AND sch.revision = asset.revision);
      
CREATE OR REPLACE VIEW v_lsr_pseudo_asset_sch_sales as
SELECT  lsr_asset_id,
        revision,
        set_of_books_id,
        month,
        principal_received * allocation as principal_received,
        principal_accrued * allocation as principal_accrued,
        beg_long_term_receivable * allocation as beg_long_term_receivable,
        end_long_term_receivable * allocation as end_long_term_receivable,
        beg_unguaranteed_residual * allocation as beg_unguaranteed_residual,
        interest_unguaranteed_residual * allocation as interest_unguaranteed_residual,
        ending_unguaranteed_residual * allocation as ending_unguaranteed_residual,
        beg_net_investment * allocation as beg_net_investment,
        interest_net_investment * allocation as interest_net_investment,
        ending_net_investment * allocation as ending_net_investment,
        calculated_initial_rate * allocation AS calculated_initial_rate,
        calculated_rate * allocation AS calculated_rate
FROM (SELECT  asset.lsr_asset_id,
              sch.revision,
              sch.set_of_books_id,
              sch.month,
              sch.principal_received,
              sch.principal_accrued,
              sch.beg_long_term_receivable,
              sch.end_long_term_receivable,
              sch.beg_unguaranteed_residual,
              sch.interest_unguaranteed_residual,
              sch.ending_unguaranteed_residual,
              sch.beg_net_investment,
              sch.interest_net_investment,
              sch.ending_net_investment,
              sch.calculated_initial_rate,
              sch.calculated_rate,
              COALESCE(ratio_to_report(asset.fair_market_value) OVER(PARTITION BY asset.ilr_id,
                                                                                  asset.revision,
                                                                                  sch.month,
                                                                                  sch.set_of_books_id),
                      0) AS allocation
      FROM lsr_ilr_schedule_sales_direct sch
      JOIN lsr_asset asset ON sch.ilr_id = asset.ilr_id
      AND sch.revision = asset.revision);
      
CREATE GLOBAL TEMPORARY TABLE lsr_ilr_payment_month_tmp (payment_month DATE);

COMMENT ON COLUMN lsr_ilr_payment_month_tmp.payment_month IS 'Payment Month';
COMMENT ON TABLE lsr_ilr_payment_month_tmp IS 'Temporary table used to prevent multiple calls to payment terms retrieval function since var payments calcs are done in cursor loop (improves performance)';

CREATE INDEX lsr_ilr_pay_mth_pay_mth_idx ON lsr_ilr_payment_month_tmp(payment_month);

--Speeds up variable payment calcs
CREATE INDEX lsr_ilr_sch_ilr_rev_mth_idx ON lsr_ilr_schedule (ilr_id, revision, MONTH) TABLESPACE pwrplant_idx;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3864, 0, 2017, 1, 0, 0, 49482, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049482_lessor_01_create_var_payments_schedule_calc_tables_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;