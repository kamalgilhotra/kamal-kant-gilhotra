/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030680_lease_fix_workspace_identifiers.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/10/2013 Kyle Peterson  Point release
||============================================================================
*/

update PPBASE_WORKSPACE set LABEL = 'Search MLA' where WORKSPACE_IDENTIFIER = 'search_mla' and MODULE = 'LESSEE';
update PPBASE_WORKSPACE set LABEL = 'Search Asset' where WORKSPACE_IDENTIFIER = 'search_asset' and MODULE = 'LESSEE';
update PPBASE_WORKSPACE set LABEL = 'Search ILR' where WORKSPACE_IDENTIFIER = 'search_ilr' and MODULE = 'LESSEE';
update PPBASE_WORKSPACE set LABEL = 'Initiate MLA' where WORKSPACE_IDENTIFIER = 'initiate_mla' and MODULE = 'LESSEE';
update PPBASE_WORKSPACE set LABEL = 'Initiate ILR' where WORKSPACE_IDENTIFIER = 'initiate_ilr' and MODULE = 'LESSEE';
update PPBASE_WORKSPACE set LABEL = 'Initiate Asset' where WORKSPACE_IDENTIFIER = 'initiate_asset' and MODULE = 'LESSEE';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (463, 0, 10, 4, 1, 0, 30680, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030680_lease_fix_workspace_identifiers.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
