/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_010586_and_009917_tax_exp.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.4.2   07/02/2012 Sunjin Cone    Point Release
||============================================================================
*/

alter table REPAIR_WORK_ORDER_TEMP     add OVH_TO_UDG number(22,0) default 0;
alter table REPAIR_WORK_ORDER_SEGMENTS add OVH_TO_UDG number(22,0) default 0;
alter table REPAIR_WO_SEG_REPORTING    add OVH_TO_UDG number(22,0);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (161, 0, 10, 3, 4, 2, 10586, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.4.2_maint_010586_and_009917_tax_exp.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
