/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011085_05_update.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   12/07/2012 Colin Lee      Point Release
||============================================================================
*/

update PP_IMPORT_COLUMN
   set PARENT_TABLE = null
 where IMPORT_TYPE_ID = 52
   and COLUMN_NAME = 'mort_mem_mort_curve_id';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (263, 0, 10, 4, 0, 0, 11085, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011085_05_update.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
