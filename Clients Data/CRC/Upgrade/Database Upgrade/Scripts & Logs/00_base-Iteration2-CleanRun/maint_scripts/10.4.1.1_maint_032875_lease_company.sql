/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_032875_lease_company.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.1   10/01/2013 Brandon Beck   Point Release
||============================================================================
*/

alter table COMPANY_SETUP add IS_LEASE_COMPANY number(1,0);
comment on column COMPANY_SETUP.IS_LEASE_COMPANY is 'A column identifying whether or not the company is visible in the lease module.';

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX, DESCRIPTION,
    LONG_DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, READ_ONLY)
   select 'is_lease_company',
          'company_setup',
          'yes_no',
          'p',
          null,
          'Is Lease Company',
          null,
          max(COLUMN_RANK) + 1,
          null,
          0
     from POWERPLANT_COLUMNS
    where TABLE_NAME = 'company_setup';

update COMPANY_SETUP set IS_LEASE_COMPANY = 0 where IS_LEASE_COMPANY is null;

alter table PP_COMPANY_SECURITY_TEMP add IS_LEASE_COMPANY number(1,0);
comment on column PP_COMPANY_SECURITY_TEMP.IS_LEASE_COMPANY is 'A column identifying whether or not the company is visible in the lease module.';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (644, 0, 10, 4, 1, 1, 32875, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_032875_lease_company.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
