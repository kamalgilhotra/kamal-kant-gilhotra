/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_043034_pcm_wot_groups_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.1.0.0 03/12/2015 Alex P.          Populate pp_work_order_type_groups with 
||                                        cartisian join of wot and groups for customers
||                                        who are are not using the table.
||============================================================================
*/

insert into pp_work_order_type_groups( work_order_type_id, groups)
select work_order_type.work_order_type_id, pp_security_groups.groups
from work_order_type,
     pp_security_groups,
     (select count(1) wotg_rows from pp_work_order_type_groups where rownum = 1) wotg
where  wotg.wotg_rows = 0;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2373, 0, 2015, 1, 0, 0, 43034, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043034_pcm_wot_groups_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;