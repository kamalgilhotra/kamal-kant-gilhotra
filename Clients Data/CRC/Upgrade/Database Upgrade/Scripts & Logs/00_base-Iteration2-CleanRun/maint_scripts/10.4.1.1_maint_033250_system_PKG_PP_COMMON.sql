/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_033250_system_PKG_PP_COMMON.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.1.0   07/08/2013 Daniel Motter  Point release
|| 10.4.1.1   10/10/2013 C. Shilling    Add P_AUTONOMOUS_SQL
||============================================================================
*/

create or replace package PKG_PP_COMMON as

   type NUM_TABTYPE is table of number index by binary_integer;
   type DATE_TABTYPE is table of date index by binary_integer;
   type INT_TABTYPE is table of pls_integer index by binary_integer;
   type VARCHAR_TABTYPE is table of varchar2(2000) index by binary_integer;

   type PP_SYSTEM_CONTROL_ARR is table of PP_SYSTEM_CONTROL.CONTROL_VALUE%type index by PP_SYSTEM_CONTROL.CONTROL_NAME%type;

   SYSTEM_CONTROLS PP_SYSTEM_CONTROL_ARR;

   G_USER_ID varchar2(30) := user;

   subtype G_YES_NO_TYPE is char(1) not null;
   G_YES constant G_YES_NO_TYPE := 'Y';
   G_NO  constant G_YES_NO_TYPE := 'N';

   subtype G_TRUE_FALSE_TYPE is pls_integer range 0 .. 1 not null;
   G_TRUE  constant G_TRUE_FALSE_TYPE := 1;
   G_FALSE constant G_TRUE_FALSE_TYPE := 0;

   subtype G_RESULT_TYPE is pls_integer range 1 .. 3 not null;
   G_SUCCESS constant G_RESULT_TYPE := 1;
   G_FAIL    constant G_RESULT_TYPE := 2;
   G_WARNING constant G_RESULT_TYPE := 3;

   procedure P_INITIALIZE_SYSTEM_CONTROLS;

   procedure P_AUTONOMOUS_SQL(P_SQL_IN in varchar2);

   function F_CLEAN_STRING(P_IN_STRING in varchar2) return varchar2;

end PKG_PP_COMMON;
/

create or replace package body PKG_PP_COMMON as

   /*******************Initialize system controils ***************************/
   procedure P_INITIALIZE_SYSTEM_CONTROLS is

   begin
      if SYSTEM_CONTROLS.COUNT > 0 then
         SYSTEM_CONTROLS.DELETE;
      end if;

      for REC in (select CONTROL_NAME, CONTROL_VALUE from PP_SYSTEM_CONTROL)
      loop
         SYSTEM_CONTROLS(trim(UPPER(REC.CONTROL_NAME))) := REC.CONTROL_VALUE;
      end loop;
   end P_INITIALIZE_SYSTEM_CONTROLS;

   /********** Clean_String ***************************************************/
   -- This function takes in a SQL statement and executes it without doing
   --    any validations or sanitation on the statement itself. So BE VERY CAREFUL
   --    that the SQL is OK to execute before calling this function.
   -- IF the SQL executes successfully, changes will be committed.
   -- ELSE a rollback is issued and the error is returned to the caller
   procedure P_AUTONOMOUS_SQL(P_SQL_IN in varchar2) is
      pragma autonomous_transaction;

   begin
      begin
         execute immediate P_SQL_IN;
      exception
         when others then
            rollback;
            raise;
      end;

      commit;
   end P_AUTONOMOUS_SQL;

   /********** Clean_String ***************************************************/
   function F_CLEAN_STRING(P_IN_STRING in varchar2) return varchar2 as

      V_BAD_CHARS varchar2(200) := '/ -';

   begin

      return TRANSLATE(P_IN_STRING, V_BAD_CHARS, LPAD('_', LENGTH(V_BAD_CHARS), '_'));

   end F_CLEAN_STRING;

/****************** Initialization ********************************************/
begin
   G_USER_ID := user;

   P_INITIALIZE_SYSTEM_CONTROLS;

end PKG_PP_COMMON;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (698, 0, 10, 4, 1, 1, 33250, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.1_maint_033250_system_PKG_PP_COMMON.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;