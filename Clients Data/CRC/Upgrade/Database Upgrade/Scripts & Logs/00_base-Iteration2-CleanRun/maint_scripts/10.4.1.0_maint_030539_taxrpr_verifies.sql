/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030539_taxrpr_verifies.sql
||============================================================================
|| Copyright(C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.1.0 09/12/2013 Andrew Scott        Tax Repair Verifies
||============================================================================
*/

delete from PP_VERIFY_RESULTS
 where VERIFY_ID in (select VERIFY_ID
                       from PP_VERIFY
                      where UPPER(trim(DESCRIPTION)) in
                            ('VERIFY REPAIR BASIS BUCKETS COUNT OF CHARGE TYPES, COST ELEMENTS',
                             'VERIFY REPAIR BASIS BUCKETS INDICATOR EQUALS 0'));
delete from PP_VERIFY_BATCH
 where VERIFY_ID in (select VERIFY_ID
                       from PP_VERIFY
                      where UPPER(trim(DESCRIPTION)) in
                            ('VERIFY REPAIR BASIS BUCKETS COUNT OF CHARGE TYPES, COST ELEMENTS',
                             'VERIFY REPAIR BASIS BUCKETS INDICATOR EQUALS 0'));
delete from PP_VERIFY_CATEGORY
 where VERIFY_ID in (select VERIFY_ID
                       from PP_VERIFY
                      where UPPER(trim(DESCRIPTION)) in
                            ('VERIFY REPAIR BASIS BUCKETS COUNT OF CHARGE TYPES, COST ELEMENTS',
                             'VERIFY REPAIR BASIS BUCKETS INDICATOR EQUALS 0'));
delete from PP_VERIFY
 where UPPER(trim(DESCRIPTION)) in
       ('VERIFY REPAIR BASIS BUCKETS COUNT OF CHARGE TYPES, COST ELEMENTS',
        'VERIFY REPAIR BASIS BUCKETS INDICATOR EQUALS 0');

insert into PP_VERIFY
   (VERIFY_ID, DESCRIPTION, LONG_DESCRIPTION, ACTION_TO_TAKE, SEVERITY, ENABLE_DISABLE, SUBSYSTEM,
    sql, USER_ALERT, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP, COMMENT_ALERT,
    COMMENT_REQUIRED, DISPLAY_ON_SELECT_TABS, ONE_COMMENT)
   select MAX_ID + 1,
          'Verify Repair Basis Buckets Indicator Equals 0',
          'Verifies that any repair basis bucket used has a set of books indicator of 0 in the set of books table.  Having this set to 1 for any repair bucket causes unrecoverable data issues.  This verify joins the repair view into the books sob view.',
          'Call Powerplan',
          'Critical',
          '1',
          '13',
          'select distinct ''Warning!  Set of Books ''||to_char(set_of_books_id)||'', Basis ''||to_char(books_sob_view.book_summary_id)||'' Indicator is set to 1!'' err_msg ' ||
          'from books_sob_view,( ' || 'select distinct book_summary_id_output book_summary_id ' ||
          'from repair_book_summary ' || ') repair_book_summaries ' ||
          'where books_sob_view.book_summary_id = repair_book_summaries.book_summary_id',
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0
     from DUAL, (select max(VERIFY_ID) MAX_ID from PP_VERIFY)
    where not exists
    (select 1
             from PP_VERIFY
            where UPPER(DESCRIPTION) = 'VERIFY REPAIR BASIS BUCKETS INDICATOR EQUALS 0');

insert into PP_VERIFY
   (VERIFY_ID, DESCRIPTION, LONG_DESCRIPTION, ACTION_TO_TAKE, SEVERITY, ENABLE_DISABLE, SUBSYSTEM,
    sql, USER_ALERT, BALANCE_ON_CLOSE, ATTACH_AS_EXCEL, SHOW_ON_MYPP, COMMENT_ALERT,
    COMMENT_REQUIRED, DISPLAY_ON_SELECT_TABS, ONE_COMMENT)
   select MAX_ID + 1,
          'Verify Repair Basis Buckets Count of Charge Types, Cost Elements',
          'Verifies that any repair basis bucket only has one charge type assigned, and that each charge type only has one cost element assigned.',
          'Call Powerplan',
          'Critical',
          '1',
          '13',
          ' select distinct msg from ( ' ||
          'select ''Warning! Multiple Charge Types Found for Tax Repair Book Summary ''||to_char(repair_book_summaries.book_summary_id) msg ' ||
          'from charge_type, ' || '( ' || 'select distinct book_summary_id_output book_summary_id ' ||
          'from repair_book_summary ' || ') repair_book_summaries ' ||
          'where charge_type.book_summary_id = repair_book_summaries.book_summary_id ' ||
          'group by repair_book_summaries.book_summary_id ' ||
          'having count(distinct charge_type.charge_type_id) > 1 ' || 'union all ' ||
          'select ''Warning! Multiple Cost Elements Found for Tax Repair Book Summary ''||to_char(charge_type.charge_type_id) ' ||
          'from charge_type, ' || 'cost_element, ' || '( ' ||
          'select distinct book_summary_id_output book_summary_id ' || 'from repair_book_summary ' ||
          ') repair_book_summaries ' ||
          'where charge_type.book_summary_id = repair_book_summaries.book_summary_id ' ||
          'and charge_type.charge_type_id = cost_element.charge_type_id ' ||
          'group by charge_type.charge_type_id ' ||
          'having count(distinct cost_element.cost_element_id) > 1 )',
          0,
          0,
          0,
          0,
          0,
          0,
          0,
          0
     from DUAL, (select max(VERIFY_ID) MAX_ID from PP_VERIFY)
    where not exists
    (select 1
             from PP_VERIFY
            where UPPER(DESCRIPTION) =
                  'VERIFY REPAIR BASIS BUCKETS COUNT OF CHARGE TYPES, COST ELEMENTS');

insert into PP_VERIFY_CATEGORY_DESCRIPTION
   (VERIFY_CATEGORY_ID, DESCRIPTION, PRIORITY_ID)
   select MAX_ID + 1, 'Tax Repairs Checks', 1
     from DUAL, (select max(VERIFY_CATEGORY_ID) MAX_ID from PP_VERIFY_CATEGORY_DESCRIPTION)
    where not exists (select 1
             from PP_VERIFY_CATEGORY_DESCRIPTION
            where UPPER(trim(DESCRIPTION)) = 'TAX REPAIRS CHECKS');

insert into PP_VERIFY_CATEGORY
   (VERIFY_ID, VERIFY_CATEGORY_ID)
   select X.VERIFY_ID, X.VERIFY_CATEGORY_ID
     from (select VERIFY_ID, VERIFY_CATEGORY_ID
             from (select VERIFY_ID
                     from PP_VERIFY
                    where UPPER(trim(DESCRIPTION)) in
                          ('VERIFY REPAIR BASIS BUCKETS INDICATOR EQUALS 0',
                           'VERIFY REPAIR BASIS BUCKETS COUNT OF CHARGE TYPES, COST ELEMENTS')),
                  (select VERIFY_CATEGORY_ID
                     from PP_VERIFY_CATEGORY_DESCRIPTION
                    where UPPER(trim(DESCRIPTION)) in
                          ('ALL', 'TABLE MAINTENANCE CHECKS', 'TAX REPAIRS CHECKS'))) X
    where not exists (select 1
             from PP_VERIFY_CATEGORY Y
            where X.VERIFY_ID = Y.VERIFY_ID
              and X.VERIFY_CATEGORY_ID = Y.VERIFY_CATEGORY_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (611, 0, 10, 4, 1, 0, 30539, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030539_taxrpr_verifies.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
