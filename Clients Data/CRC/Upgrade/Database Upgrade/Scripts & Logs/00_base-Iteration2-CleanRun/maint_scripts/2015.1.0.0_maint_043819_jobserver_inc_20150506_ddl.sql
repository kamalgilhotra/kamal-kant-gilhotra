/*
||==========================================================================================
|| Application: PowerPlan
|| Module: 		Job Server
|| File Name:   maint_043819_jobserver_inc_20150506_ddl.sql
||==========================================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||==========================================================================================
|| Version    Date       Revised By          Reason for Change
|| ---------- ---------- ------------------- -----------------------------------------------
|| 2015.1     05/06/2014 Ryan Oliveria    	 Syncing Triggers and Sequences
||==========================================================================================
*/

SET serveroutput ON size 30000;

DECLARE
	doesObjectExist NUMBER := 0;
BEGIN

	--* Trigger: PP_JOB_WORKFLOW
	BEGIN
		doesObjectExist := SYS_DOES_TRIGGER_EXIST('PP_JOB_WORKFLOW','PWRPLANT');

		IF doesObjectExist = 0 THEN
			BEGIN
				dbms_output.put_line('Creating trigger PP_JOB_WORKFLOW ');

				EXECUTE IMMEDIATE 'CREATE OR REPLACE TRIGGER pp_job_workflow
      				BEFORE INSERT ON pp_job_workflow
      				FOR EACH ROW

				BEGIN
         				:new.JOB_WORKFLOW_ID := PP_JOB_WORKFLOW_SEQ.nextval;
      			END';

				EXECUTE IMMEDIATE 'ALTER TRIGGER "PWRPLANT"."PP_JOB_WORKFLOW" ENABLE';

			END;
		ELSE
			BEGIN
				dbms_output.put_line('Trigger PP_JOB_WORKFLOW exists');
			END;
		END IF;
	END;

	--* Sequence: PP_JOB_TRANSITION_SEQ
	BEGIN
		doesObjectExist := SYS_DOES_SEQUENCE_EXIST('PP_JOB_TRANSITION_SEQ','PWRPLANT');

		IF doesObjectExist = 0 THEN
			BEGIN
				dbms_output.put_line('Creating sequence PP_JOB_TRANSITION_SEQ ');

				EXECUTE IMMEDIATE 'CREATE SEQUENCE pp_job_transition_seq
  				MINVALUE 1
  				MAXVALUE 9999999999999999999999999999
  				INCREMENT BY 1
  				NOCYCLE
  				NOORDER
  				CACHE 20';

			END;
		ELSE
			BEGIN
				dbms_output.put_line('Sequence PP_JOB_TRANSITION_SEQ exists');
			END;
		END IF;
	END;

	--* Sequence: PP_JOB_WORKFLOW_TRANSITION_SEQ
	BEGIN
		doesObjectExist := SYS_DOES_SEQUENCE_EXIST('PP_JOB_WORKFLOW_TRANSITION_SEQ','PWRPLANT');

		IF doesObjectExist = 0 THEN
			BEGIN
				dbms_output.put_line('Creating sequence PP_JOB_WORKFLOW_TRANSITION_SEQ ');

				EXECUTE IMMEDIATE 'CREATE SEQUENCE pp_job_workflow_condition_seq
  				MINVALUE 1
  				MAXVALUE 9999999999999999999999999999
  				INCREMENT BY 1
  				NOCYCLE
  				NOORDER
  				CACHE 20';

			END;
		ELSE
			BEGIN
				dbms_output.put_line('Sequence PP_JOB_WORKFLOW_TRANSITION_SEQ exists');
			END;
		END IF;
	END;

END;
/


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2555, 0, 2015, 1, 0, 0, 43819, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043819_jobserver_inc_20150506_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;