/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_049206_lessor_04_cpr_asset_import_misc_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 11/06/2017 Shane "C" Ward		Modify Estimated Residual columns
||============================================================================
*/
ALTER TABLE lsr_import_cpr_asset MODIFY estimated_residual NUMBER(22,2);

ALTER TABLE lsr_import_cpr_asset_archive MODIFY estimated_residual NUMBER(22,2);

ALTER TABLE lsr_import_ext_asset MODIFY estimated_residual NUMBER(22,2);

ALTER TABLE lsr_import_ext_asset_archive MODIFY estimated_residual NUMBER(22,2);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (3882, 0, 2017, 1, 0, 0, 49206, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_049206_lessor_04_cpr_asset_import_misc_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;