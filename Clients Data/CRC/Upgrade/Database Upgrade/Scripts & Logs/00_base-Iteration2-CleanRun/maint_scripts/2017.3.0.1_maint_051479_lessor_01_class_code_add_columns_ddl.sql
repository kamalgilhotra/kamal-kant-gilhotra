/*
||=============================================================================
|| Application: PowerPlan
|| File Name:   maint_051479_lessor_01_class_code_add_columns_ddl.sql
||=============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||=============================================================================
|| Version     Date       Revised By       Reason for Change
|| ----------  ---------- ---------------- ------------------------------------
|| 2017.4.0.0  06/06/2018 David Conway     Add lessor indicators to class_code
||============================================================================
*/
/* add lessor-specific columns */
alter table class_code add (
  lsr_mla_indicator number(22,0) null,
  lsr_ilr_indicator number(22,0) null);

/* add comments to new columns */
COMMENT ON COLUMN class_code.lsr_mla_indicator
  IS 'Yes (1) / No (0) flag indicating whether the classification code may be associated with Master Lease Agreements within the Lessor module.' ;
COMMENT ON COLUMN class_code.lsr_ilr_indicator
  IS 'Yes (1) / No (0) flag indicating whether the classification code may be associated with Individual Lease Records within the Lessor module.' ;

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (6722, 0, 2017, 3, 0, 1, 51479, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.3.0.1_maint_051479_lessor_01_class_code_add_columns_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;