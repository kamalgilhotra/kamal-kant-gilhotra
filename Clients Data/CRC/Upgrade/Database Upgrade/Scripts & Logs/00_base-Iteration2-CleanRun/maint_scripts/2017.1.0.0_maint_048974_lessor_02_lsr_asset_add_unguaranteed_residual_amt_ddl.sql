/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_048974_lessor_02_lsr_asset_add_unguaranteed_residual_amt_ddl.sql
||============================================================================
|| Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2017.1.0.0 10/09/2017 Josh Sandler	  Add Unguaranteed Residual Amount Company Currency Column
||============================================================================
*/

ALTER TABLE lsr_asset
	ADD unguaranteed_res_amt_comp_curr	NUMBER(22,2)
;

COMMENT ON COLUMN lsr_asset.unguaranteed_res_amt_comp_curr IS 'The unguaranteed residual amount in company currency calculated as Estimated Residual-Guaranteed Residual.';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(3777, 0, 2017, 1, 0, 0, 48974, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.1.0.0_maint_048974_lessor_02_lsr_asset_add_unguaranteed_residual_amt_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
