/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039200_lease_add_interco_transfer_trans_types.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 07/23/2014 Charlie Shilling maint-39192
||============================================================================
*/

merge into JE_TRANS_TYPE JTT
using (select '3042' as TRANS_TYPE, '3042 - Lease Transfercompany Tranfer Debit (POST)' as DESCRIPTION
         from DUAL
       union
       select '3043' as TRANS_TYPE, '3043 - Lease Transfercompany Tranfer Credit (POST)' as DESCRIPTION
         from DUAL) A
on (JTT.TRANS_TYPE = A.TRANS_TYPE)
when matched then
   update set JTT.DESCRIPTION = A.DESCRIPTION
when not matched then
   insert (TRANS_TYPE, DESCRIPTION) values (A.TRANS_TYPE, A.DESCRIPTION);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1269, 0, 10, 4, 3, 0, 39200, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_039200_lease_add_interco_transfer_trans_types.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;