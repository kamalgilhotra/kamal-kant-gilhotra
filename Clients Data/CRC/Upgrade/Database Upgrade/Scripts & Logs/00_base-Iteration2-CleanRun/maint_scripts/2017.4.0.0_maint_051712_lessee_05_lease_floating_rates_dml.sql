 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: maint_051712_lessee_05_lease_floating_rates_dml.sql
 ||============================================================================
 || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version    Date       Revised By     Reason for Change
 || ---------- ---------- -------------- ----------------------------------------
 || 2017.4.0.0 06/26/2018 Shane "C" Ward  New Lease Type for Principal Payments
 ||============================================================================
 */

INSERT INTO ls_lease_type (lease_type_id, description, long_description)
VALUES (5, 'Fixed Principal Floating Interest', 'Fixed Principal with Floating Interest');

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (7626, 0, 2017, 4, 0, 0, 51712, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.4.0.0_maint_051712_lessee_05_lease_floating_rates_dml.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;