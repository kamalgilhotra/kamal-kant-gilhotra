/*
||============================================================================
|| Application: PowerPlan
|| Object Name: maint_050310_lessee_01_invoice_notes_descr_ddl.sql
|| Description:
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By     Reason for Change
|| ---------- ----------  -------------- ----------------------------------------
|| 2017.2.0.0  02/06/2018 Sarah Byers    Add ability to import notes and description on invoices
||============================================================================
*/

alter table ls_import_invoice add (description varchar2(35), notes varchar2(4000));
comment on column ls_import_invoice.DESCRIPTION is 'Short description of the invoice line.';
comment on column ls_import_invoice.NOTES is 'Detailed notes about the invoice line.';

alter table ls_import_invoice_archive add (description varchar2(35), notes varchar2(4000));
comment on column ls_import_invoice_archive.DESCRIPTION is 'Short description of the invoice line.';
comment on column ls_import_invoice_archive.NOTES is 'Detailed notes about the invoice line.';

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (4115, 0, 2017, 2, 0, 0, 50310, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2017.2.0.0_maint_050310_lessee_01_invoice_notes_descr_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;
