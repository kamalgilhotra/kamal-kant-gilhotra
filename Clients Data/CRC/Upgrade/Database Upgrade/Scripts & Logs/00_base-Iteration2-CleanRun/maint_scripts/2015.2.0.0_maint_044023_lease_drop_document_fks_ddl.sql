 /*
 ||============================================================================
 || Application: PowerPlan
 || File Name: Maint_44023_lease_drop_document_fks_ddl
 ||============================================================================
 || Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
 ||============================================================================
 || Version  Date       Revised By     Reason for Change
 || -------- ---------- -------------- ----------------------------------------
 || 2015.2   06/06/2015 Will Davis     Dropping foreign keys because lease documents
 ||                                    cannot be added to the database until after an
 ||                                    asset, ILR, or MLA is saved to the database
 ||============================================================================
 */

alter table ls_asset_document drop constraint fk1_ls_asset_document;
alter table ls_lease_document drop constraint r_ls_lease_document1;
alter table ls_ilr_document drop constraint r_ls_ilr_document1b;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2578, 0, 2015, 2, 0, 0, 044023, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044023_lease_drop_document_fks_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;