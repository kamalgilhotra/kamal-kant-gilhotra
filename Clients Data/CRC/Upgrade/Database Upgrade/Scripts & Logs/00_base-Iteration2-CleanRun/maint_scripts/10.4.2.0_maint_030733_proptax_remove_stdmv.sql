/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_030733_proptax_remove_stdmv.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.2.0 01/31/2014 Andrew Scott        Remove the standardized market value
||                                         rates as long as they are not in use.
||============================================================================
*/

----remove any reference of these rates to property tax types not found in the ledger.
----remove any of the "standardized market value rates" (where not used).

update PROPERTY_TAX_TYPE_DATA PTTD
   set PTTD.MARKET_VALUE_RATE_ID = null
 where PTTD.MARKET_VALUE_RATE_ID in (select PT_RATE_ID
                                       from PT_RATE_DEFINITION
                                      where RATE_TYPE_ID = 1 /* market value rate */
                                        and PT_RATE_ID in (100,
                                                           300,
                                                           600,
                                                           625,
                                                           950,
                                                           1000,
                                                           1050,
                                                           1350,
                                                           1500,
                                                           2000,
                                                           2300,
                                                           2500,
                                                           2900,
                                                           3000,
                                                           3333,
                                                           3500,
                                                           4000,
                                                           5000,
                                                           6000,
                                                           6666,
                                                           7000,
                                                           7500,
                                                           8000,
                                                           9000))
   and not exists (select 1
          from PT_LEDGER L, PT_LEDGER_TAX_YEAR Y
         where L.PROPERTY_TAX_LEDGER_ID = Y.PROPERTY_TAX_LEDGER_ID
           and L.PROPERTY_TAX_TYPE_ID = PTTD.PROPERTY_TAX_TYPE_ID);

--delete the standardized market value rates as long as
--they are not referenced on the property tax types
delete from PT_RATE_DEFINITION
 where RATE_TYPE_ID = 1 /* market value rate */
   and PT_RATE_ID in (100,
                      300,
                      600,
                      625,
                      950,
                      1000,
                      1050,
                      1350,
                      1500,
                      2000,
                      2300,
                      2500,
                      2900,
                      3000,
                      3333,
                      3500,
                      4000,
                      5000,
                      6000,
                      6666,
                      7000,
                      7500,
                      8000,
                      9000)
   and not exists
 (select 1 from PROPERTY_TAX_TYPE_DATA PTTD where PTTD.MARKET_VALUE_RATE_ID = PT_RATE_ID);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (933, 0, 10, 4, 2, 0, 30733, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_030733_proptax_remove_stdmv.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;