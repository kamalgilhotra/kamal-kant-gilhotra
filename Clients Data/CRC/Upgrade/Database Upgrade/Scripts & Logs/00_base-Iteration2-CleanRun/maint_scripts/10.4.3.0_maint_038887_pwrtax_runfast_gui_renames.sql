/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_038887_pwrtax_runfast_gui_renames.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------
|| 10.4.3.0 08/12/2014 Andrew Scott        change run fast workspace object names,
||                                         since they need to be checked out to turn off
||                                         auditing
||============================================================================
*/

----renamed the run fast workspaces
update PPBASE_WORKSPACE
   set WORKSPACE_UO_NAME = DECODE(WORKSPACE_IDENTIFIER,
                                   'depr_process',
                                   'uo_tax_depr_wksp_processing',
                                   'forecast_process',
                                   'uo_tax_fcst_wksp_processing',
                                   'deferred_process',
                                   'uo_tax_dfit_wksp_processing')
 where LOWER(MODULE) = 'powertax'
   and LOWER(WORKSPACE_IDENTIFIER) in ('depr_process', 'forecast_process', 'deferred_process');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1352, 0, 10, 4, 3, 0, 38887, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.3.0_maint_038887_pwrtax_runfast_gui_renames.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;