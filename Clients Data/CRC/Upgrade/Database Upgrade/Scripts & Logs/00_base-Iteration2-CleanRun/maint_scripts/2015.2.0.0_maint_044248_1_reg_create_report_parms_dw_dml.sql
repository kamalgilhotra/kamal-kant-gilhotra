/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_044248_1_reg_create_report_parms_dw_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/30/2015 Anand R        create new report parms dw for RMS
||                                       use PP_REPORT_TIME_OPTION_ID instead of description                                      	
||============================================================================
*/

update pp_reports_time_option set 
    description = 'Fcst Version + Start + End Year',
    parameter_uo_name = 'uo_ppbase_report_parms_dddw_three',
    label1 = 'Forecast Ledger',
    dwname2 = 'dw_reg_start_year',
    label2 = 'Start Year',
    keycolumn2 = 'start_year', 
    dwname3 = 'dw_reg_end_year',
    label3 = 'End Year',
    keycolumn3 = 'end_year'
 where PP_REPORT_TIME_OPTION_ID = 108;
 

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2664, 0, 2015, 2, 0, 0, 044248, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044248_1_reg_create_report_parms_dw_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;