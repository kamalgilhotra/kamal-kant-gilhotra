/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_008209_prov.sql
||============================================================================
|| Copyright (C) 2011 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.2.1   10/11/2011 Alex P.        Maint 8345 - ID=12
|| 10.3.3.0                             Same - ID=13
||============================================================================
*/

create or replace view ARO_SOB_VIEW
(ARO_ID, SET_OF_BOOKS_ID)
as
select A.ARO_ID, B.SET_OF_BOOKS_ID
  from ARO A, BOOKS_SOB_VIEW B
 where B.BOOK_SUMMARY_ID = NVL(A.BOOK_SUMMARY_OVERRIDE, (select max(BS.BOOK_SUMMARY_ID)
                                                          from BOOK_SUMMARY BS
                                                         where UPPER(trim(BOOK_SUMMARY_TYPE)) = 'ARO'));

--**************************
-- Log the run of the script
--**************************
/* 10.3.2.1
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (12, 0, 10, 3, 2, 1, 8345, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.2.1_maint_008345_ARO_SOB_VIEW.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
*/
insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (13, 0, 10, 3, 3, 0, 8345, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.3.3.0_maint_008345_ARO_SOB_VIEW.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
