/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_039509_reg_ifa_groups.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.2.7 08/12/2014 Ryan Oliveria    IFA Grouping Tables
||============================================================================
*/

insert into PPBASE_WORKSPACE
   (MODULE, WORKSPACE_IDENTIFIER, LABEL, WORKSPACE_UO_NAME, MINIHELP)
values
   ('REG', 'uo_reg_inc_group_ws', 'IFA - Groups', 'uo_reg_inc_group_ws', 'IFA - Manage Groups');

insert into PPBASE_MENU_ITEMS
   (MODULE, MENU_IDENTIFIER, MENU_LEVEL, ITEM_ORDER, LABEL, MINIHELP, PARENT_MENU_IDENTIFIER, WORKSPACE_IDENTIFIER, ENABLE_YN)
values
   ('REG', 'IFA_GROUP', 3, 5, 'IFA - Groups', 'IFA - Manage Groups', 'IFA_TOP', 'uo_reg_inc_group_ws', 1);


create table REG_INCREMENTAL_GROUP
(
 INCREMENTAL_GROUP_ID number(22,0) not null,
 USER_ID              varchar2(18),
 TIME_STAMP           date,
 DESCRIPTION          varchar2(35) not null,
 FORECAST_VERSION_ID  number(22,0) not null
);

alter table REG_INCREMENTAL_GROUP
   add constraint PK_REG_INCREMENTAL_GROUP
       primary key (INCREMENTAL_GROUP_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_GROUP
   add constraint FK_REG_INCREMENTAL_GROUP1
       foreign key (FORECAST_VERSION_ID)
       references REG_FORECAST_VERSION (FORECAST_VERSION_ID);


create table REG_INCREMENTAL_ADJ_GROUP
(
 INCREMENTAL_GROUP_ID number(22,0) not null,
 INCREMENTAL_ADJ_ID   number(22,0) not null,
 USER_ID              varchar2(18),
 TIME_STAMP           date
);

alter table REG_INCREMENTAL_ADJ_GROUP
   add constraint PK_REG_INCREMENTAL_ADJ_GROUP
       primary key (INCREMENTAL_GROUP_ID, INCREMENTAL_ADJ_ID)
       using index tablespace PWRPLANT_IDX;

alter table REG_INCREMENTAL_ADJ_GROUP
   add constraint FK_REG_INCREMENTAL_ADJ_GROUP1
       foreign key (INCREMENTAL_GROUP_ID)
       references REG_INCREMENTAL_GROUP (INCREMENTAL_GROUP_ID);

alter table REG_INCREMENTAL_ADJ_GROUP
   add constraint FK_REG_INCREMENTAL_ADJ_GROUP2
       foreign key (INCREMENTAL_ADJ_ID)
       references REG_INCREMENTAL_ADJUSTMENT (INCREMENTAL_ADJ_ID);


comment on table REG_INCREMENTAL_GROUP is 'Header information for a group of IFA''s.';
comment on column REG_INCREMENTAL_GROUP.INCREMENTAL_GROUP_ID is 'System assigned identifier for the IFA Group';
comment on column REG_INCREMENTAL_GROUP.USER_ID is 'Standard system-assigned user ID used for audit purposes';
comment on column REG_INCREMENTAL_GROUP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';
comment on column REG_INCREMENTAL_GROUP.DESCRIPTION is 'Description of the IFA Group';
comment on column REG_INCREMENTAL_GROUP.FORECAST_VERSION_ID is 'The reg forecast version that all IFA''s in this group must be related to.';

comment on table REG_INCREMENTAL_ADJ_GROUP is 'A mapping table to link the Group and the IFA.';
comment on column REG_INCREMENTAL_ADJ_GROUP.INCREMENTAL_GROUP_ID is 'The ID of the IFA Group';
comment on column REG_INCREMENTAL_ADJ_GROUP.INCREMENTAL_ADJ_ID is 'The ID of the IFA';
comment on column REG_INCREMENTAL_ADJ_GROUP.USER_ID is 'Standard system-assigned user ID used for audit purposes';
comment on column REG_INCREMENTAL_ADJ_GROUP.TIME_STAMP is 'Standard system-assigned timestamp used for audit purposes';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (1334, 0, 10, 4, 2, 7, 39509, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.7_maint_039509_reg_ifa_groups.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;