/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_043471_reg_hist_lyr_tax3_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 2015.2.0.0 06/18/2015 Andrew Scott   rename tax fp tables and rebuild their pkeys.
||                                      drop the fp and rev columns from tax tables
||============================================================================
*/

SET SERVEROUTPUT ON


ALTER TABLE TAX_DEPRECIATION_FP
RENAME TO TAX_DEPRECIATION_INC;

ALTER TABLE BASIS_AMOUNTS_FP
RENAME TO BASIS_AMOUNTS_INC;

ALTER TABLE TAX_BOOK_RECONCILE_FP
RENAME TO TAX_BOOK_RECONCILE_INC;

ALTER TABLE DEFERRED_INCOME_TAX_FP
RENAME TO DEFERRED_INCOME_TAX_INC;

ALTER TABLE UTILITY_ACCT_DEPRECIATION_FP
RENAME TO UTILITY_ACCT_DEPRECIATION_INC;

ALTER TABLE TAX_FCST_BUDGET_ADDS_FP
RENAME TO TAX_FCST_TRANS_ADDS_INC;

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from TAX_DEPRECIATION_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'TAX_DEPRECIATION_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table TAX_DEPRECIATION_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from BASIS_AMOUNTS_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'BASIS_AMOUNTS_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table BASIS_AMOUNTS_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from TAX_BOOK_RECONCILE_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'TAX_BOOK_RECONCILE_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table TAX_BOOK_RECONCILE_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from DEFERRED_INCOME_TAX_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'DEFERRED_INCOME_TAX_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table DEFERRED_INCOME_TAX_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from UTILITY_ACCT_DEPRECIATION_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'UTILITY_ACCT_DEPRECIATION_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table UTILITY_ACCT_DEPRECIATION_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/

declare
   PK_TO_DROP varchar2(100);
begin

   DBMS_OUTPUT.PUT_LINE('selecting pkey to drop from TAX_FCST_TRANS_ADDS_INC');

   select CONSTRAINT_NAME
     into PK_TO_DROP
     from ALL_CONSTRAINTS CONS
    where CONS.CONSTRAINT_TYPE = 'P'
      and CONS.TABLE_NAME = 'TAX_FCST_TRANS_ADDS_INC';

   DBMS_OUTPUT.PUT_LINE('pkey name : ' || PK_TO_DROP);

   execute immediate 'alter table TAX_FCST_TRANS_ADDS_INC drop constraint '||PK_TO_DROP;
   DBMS_OUTPUT.PUT_LINE(PK_TO_DROP||' dropped successfully');

end;
/


alter table TAX_DEPRECIATION_INC
  add constraint PK_TAX_DEPR_INC primary key (TAX_BOOK_ID, TAX_YEAR, TAX_RECORD_ID, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table BASIS_AMOUNTS_INC
  add constraint PK_BASIS_AMOUNTS_INC primary key (TAX_INCLUDE_ID, TAX_RECORD_ID, TAX_YEAR, TAX_ACTIVITY_CODE_ID, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table TAX_BOOK_RECONCILE_INC
  add constraint PK_TAX_BOOK_RECONCILE_INC primary key (TAX_INCLUDE_ID, TAX_YEAR, TAX_RECORD_ID, RECONCILE_ITEM_ID, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table DEFERRED_INCOME_TAX_INC
  add constraint PK_DEFERRED_INCOME_TAX_INC primary key (TAX_RECORD_ID, TAX_YEAR, TAX_MONTH, NORMALIZATION_ID, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table UTILITY_ACCT_DEPRECIATION_INC
  add constraint PK_UTIL_ACCT_DEPR_INC primary key (UTILITY_ACCOUNT_DEPR_ID, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;

alter table TAX_FCST_TRANS_ADDS_INC
  add constraint PK_TAX_FCST_TRANS_ADDS_INC primary key (TRANS_ID, INCREMENTAL_PROCESS_ID)
  using index 
  tablespace PWRPLANT_IDX;


alter table TAX_DEPRECIATION_INC drop column FUNDING_WO_ID;
alter table TAX_DEPRECIATION_INC drop column REVISION;

alter table BASIS_AMOUNTS_INC drop column FUNDING_WO_ID;
alter table BASIS_AMOUNTS_INC drop column REVISION;

alter table TAX_BOOK_RECONCILE_INC drop column FUNDING_WO_ID;
alter table TAX_BOOK_RECONCILE_INC drop column REVISION;

alter table DEFERRED_INCOME_TAX_INC drop column FUNDING_WO_ID;
alter table DEFERRED_INCOME_TAX_INC drop column REVISION;

alter table UTILITY_ACCT_DEPRECIATION_INC drop column FUNDING_WO_ID;
alter table UTILITY_ACCT_DEPRECIATION_INC drop column REVISION;

alter table TAX_FCST_TRANS_ADDS_INC drop column FUNDING_WO_ID;
alter table TAX_FCST_TRANS_ADDS_INC drop column REVISION;

alter table TAX_JOB_PARAMS drop column FUNDING_WO_ID;
alter table TAX_JOB_PARAMS drop column REVISION;



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2639, 0, 2015, 2, 0, 0, 043471, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_043471_reg_hist_lyr_tax3_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;