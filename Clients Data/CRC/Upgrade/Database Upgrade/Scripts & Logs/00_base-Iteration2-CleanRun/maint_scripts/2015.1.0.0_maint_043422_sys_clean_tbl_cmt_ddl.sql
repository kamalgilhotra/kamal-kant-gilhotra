/*
||============================================================================
|| Application: PowerPlan
|| File Name: maint_043422_sys_clean_tbl_cmt_ddl.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- ----------------------------------------
|| 2015.1.0.0 04/20/2015 Andrew Scott   Clean up some comments for the table doc in 2015.1
||============================================================================
*/

comment on column class_code.mla_indicator is 'Yes (1) / (0) No flag indicating whether the classification code may be associated with Master Lease Agreements within the lease module.';
comment on column class_code.ilr_indicator is 'Yes (1) / (0) No flag indicating whether the classification code may be associated with Individual Lease Records within the lease module.';
comment on column class_code.asset_indicator is 'Yes (1) / (0) No flag indicating whether the classification code may be associated with Leased Assets within the lease module.';
comment on column company_setup.auto_life_month is 'If the retirement method is Life/Auto, this specifies the accounting month (1 � 12) that the automatic retirement is to take place, solely looking at the vintage of the asset. This enables the �Auto Transactions� button on the CPR control window for the life_auto_month.  (See also �auto_life_month_monthly� below.)';
comment on column company_setup.auto_life_month_monthly is 'The list of months (e.g., 1, 2, 6, and 12) separated by commas, that �life auto� retirements are to be processed.  If this field is not null, auto_life_month column must be null. The processing logic is slightly different, looking at the in-service month as well as the in-service year';
comment on column pp_company_security_temp.company_id is 'System-assigned identifier of a particular company.';
comment on column pp_company_security_temp.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column pp_company_security_temp.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column pp_company_security_temp.gl_company_no is 'Records a company code or description used by external systems such as the general ledger system.';
comment on column pp_company_security_temp.owned is 'Owned indicator is an indicator whether the company is owned by the utility (1) or not (0) (e.g. co-tenant, etc.), or the special joint work order company (2).';
comment on column pp_company_security_temp.description is 'Records a short description of the company.';
comment on column pp_company_security_temp.status_code_id is 'Identifies a unique status: 1. Active 2. Inactive. If active, new assets can be set up for the company.  ''Inactive'' can be used for companies that are not on the CPR but are in PowerTax.';
comment on column pp_company_security_temp.short_description is 'Short company name for PowerTax windows.';
comment on column pp_company_security_temp.company_summary_id is 'Optional roll-up of companies for reporting; referencing the company summary table.';
comment on column pp_company_security_temp.auto_life_month is 'If the retirement method is Life/Auto, this specifies the accounting month (1 � 12) that the automatic retirement is to take place, solely looking at the vintage of the asset. This enables the �Auto Transactions� button on the CPR control window for the life_auto_month.  (See also �auto_life_month_monthly� below.)';
comment on column pp_company_security_temp.auto_curve_month is 'If the retirement method is Retire Curve Auto, this specifies the accounting month (1 - 12) that the automatic retirement is to take place.  This enables the ''Auto Transactions'' button on the CPR control window for the auto_curve_month.';
comment on column pp_company_security_temp.auto_close_wo_num is 'Accounting work order number default, when the system creates journal entries such as for automatic retirements.  This work order must be set up in the system.';
comment on column pp_company_security_temp.parent_company_id is 'System-assigned identifier of the parent company of this company.  This is used in the calculation of FAS34 interest and can be used in reporting.';
comment on column pp_company_security_temp.tax_return_id is 'Company number in tax compliance software (i.e., CORPTAX, Insource).';
comment on column pp_company_security_temp.closing_rollup_id is 'System-assigned identifier of a particular closing rollup. (Table Company Closing Rollup)';
comment on column pp_company_security_temp.auto_life_month_monthly is 'The list of months (e.g., 1, 2, 6, and 12) separated by commas, that �life auto� retirements are to be processed.  If this field is not null, auto_life_month column must be null. The processing logic is slightly different, looking at the in-service month as well as the in-service year';
comment on column pp_company_security_temp.company_type is 'C_corp, S_corp, LLC, Partnership, Exempt_Org, Disregarded_Ent';
comment on column pp_company_security_temp.company_ein is 'Federal taxpayer identification number';
comment on table budget_closings is 'This table contains the closing amounts by funding project/budget item, month, and type (CWIP, RWIP and Salvage).  This table is built based on the results in the budget_afudc_calc table, which is built by the Budget Afudc calculation logic.  The amounts in this table are then multipled by the percentages in the Budget Closings Pct table to load the forecast depreciation module with Plant Additions, Cost of Removal and Salvage Closings.';
comment on column budget_closings.id is 'System-assigned identifier for the applicable record.';
comment on column budget_closings.work_order_id is 'System-assigned identifier for a particular Funding Project or Budget Item.';
comment on column budget_closings.revision is 'The revision number of the project being processed.';
comment on column budget_closings.month_number is 'Integer representation of the month of the closings, e.g., May 2004 is ''200405''.';
comment on column budget_closings.expenditure_type_id is 'System-assigned identifier of the expenditure type.';
comment on column budget_closings.actuals_month_number is 'Integer representation of the month through which actuals are represented in the forecast, e.g., May 2004 is ''200405''.';
comment on column budget_closings.company_id is 'System-assigned identifier of a company.';
comment on column budget_closings.bus_segment_id is 'System-assigned identifier of the business segment.';
comment on column budget_closings.major_location_id is 'System-assigned identifier of the major location.';
comment on column budget_closings.job_task_id is 'System-assigned identifier of the job task.';
comment on column budget_closings.book_summary_id is 'System-assigned identifier of the basis bucket/book summary.';
comment on column budget_closings.vintage is 'Vintage of the closings amount.';
comment on column budget_closings.budget_plant_class_id is 'System-assigned identifier of the budget plant class.';
comment on column budget_closings.beg_cwip is 'The beginning WIP balance (RWIP or CWIP or Salvage based on expenditure type) balance of the month same exclusions as the amount field.';
comment on column budget_closings.current_month_amount is 'Book basis of expenditure for the current month.  The book basis is defined for each estimate charge type in the estimate charge type table.  If this is an actual month, this amount came from CWIP Charge table, if this is a forecast month then the data comes from either WO EST MONTHLY for funding projects or Budget Monthly Data for Budget Items.';
comment on column budget_closings.current_month_closings is 'Book closings to plan-in-service for the month (includes costs that are ineligible for AFUDC).  Based on the following formula (beg_cwip + AFUDC Debt + AFUDC Equity + Amount) * closings pct.';
comment on column budget_closings.end_cwip is 'The beginning WIP balance (RWIP or CWIP or Salvage based on expenditure type) balance of the month same exclusions as the amount field.';
comment on column budget_closings.closings_pct is 'The percent of the CWIP balance that should close to plant in-service this month.  This can come from the closing pattern if assigned or based on in-service dates and closing options. (See attached chart.)';
comment on column budget_closings.closing_type is 'System-assigned identifier of a particular closing type to indicate book v. tax closing amounts.';
comment on column budget_closings.user_id is 'Standard system-assigned user id user for audit purposes.';
comment on column budget_closings.time_stamp is 'Standard system-assigned timestamp user for audit purposes.';
comment on column budget_closings.fcst_depr_version_id is 'System-assigned identifier of the Depr Forecast Version.';
comment on column budget_closings.in_service_date is 'Date the project is scheduled to go in service.';
comment on column budget_closings.closing_pattern_id is 'System-assigned identifier of the closing pattern.';
comment on column budget_closings.closing_option_id is 'System-assigned identifier of the closing option.';
comment on table budget_closings_temp is 'This is a processing table used to calculate the closing amounts by funding project/budget item, month, and type (CWIP, RWIP and Salvage).  This table is built based on the results in the budget_afudc_calc table, which is built by the Budget Afudc calculation logic.  The amounts in this table are then multipled by the percentages in the Budget Closings Pct table to load the forecast depreciation module with Plant Additions, Cost of Removal and Salvage Closings.';
comment on column budget_closings_temp.id is 'System-assigned identifier for the applicable record.';
comment on column budget_closings_temp.work_order_id is 'System-assigned identifier for a particular Funding Project or Budget Item.';
comment on column budget_closings_temp.revision is 'The revision number of the project being processed.';
comment on column budget_closings_temp.month_number is 'Integer representation of the month of the closings, e.g., May 2004 is ''200405''.';
comment on column budget_closings_temp.expenditure_type_id is 'System-assigned identifier of the expenditure type.';
comment on column budget_closings_temp.actuals_month_number is 'Integer representation of the month through which actuals are represented in the forecast, e.g., May 2004 is ''200405''.';
comment on column budget_closings_temp.company_id is 'System-assigned identifier of a company.';
comment on column budget_closings_temp.bus_segment_id is 'System-assigned identifier of the business segment.';
comment on column budget_closings_temp.major_location_id is 'System-assigned identifier of the major location.';
comment on column budget_closings_temp.job_task_id is 'System-assigned identifier of the job task.';
comment on column budget_closings_temp.book_summary_id is 'System-assigned identifier of the basis bucket/book summary.';
comment on column budget_closings_temp.vintage is 'Vintage of the closings amount.';
comment on column budget_closings_temp.budget_plant_class_id is 'System-assigned identifier of the budget plant class.';
comment on column budget_closings_temp.beg_cwip is 'The beginning WIP balance (RWIP or CWIP or Salvage based on expenditure type) balance of the month same exclusions as the amount field.';
comment on column budget_closings_temp.current_month_amount is 'Book basis of expenditure for the current month.  The book basis is defined for each estimate charge type in the estimate charge type table.  If this is an actual month, this amount came from CWIP Charge table, if this is a forecast month then the data comes from either WO EST MONTHLY for funding projects or Budget Monthly Data for Budget Items.';
comment on column budget_closings_temp.current_month_closings is 'Book closings to plan-in-service for the month (includes costs that are ineligible for AFUDC).  Based on the following formula (beg_cwip + AFUDC Debt + AFUDC Equity + Amount) * closings pct.';
comment on column budget_closings_temp.end_cwip is 'The beginning WIP balance (RWIP or CWIP or Salvage based on expenditure type) balance of the month same exclusions as the amount field.';
comment on column budget_closings_temp.closings_pct is 'The percent of the CWIP balance that should close to plant in-service this month.  This can come from the closing pattern if assigned or based on in-service dates and closing options. (See attached chart.)';
comment on column budget_closings_temp.closing_type is 'System-assigned identifier of a particular closing type to indicate book v. tax closing amounts.';
comment on column budget_closings_temp.user_id is 'Standard system-assigned user id user for audit purposes.';
comment on column budget_closings_temp.time_stamp is 'Standard system-assigned timestamp user for audit purposes.';
comment on column budget_closings_temp.fcst_depr_version_id is 'System-assigned identifier of the Depr Forecast Version.';
comment on column budget_closings_temp.in_service_date is 'Date the project is scheduled to go in service.';
comment on column budget_closings_temp.closing_pattern_id is 'System-assigned identifier of the closing pattern.';
comment on column budget_closings_temp.closing_option_id is 'System-assigned identifier of the closing option.';
comment on table budget_closings_values is 'This table contains a row for each column that is used in the calculation of budgeted closings that are passed downstream into the Book and Tax Forecasts.';
comment on column budget_closings_values.value is 'Identifier for the column to which the SQL column applies; for example, "actual major_location_id" identifies the major location field for historical actuals.';
comment on column budget_closings_values.sql is 'SQL used to retrieve the applicable value for the field.';
comment on column budget_closings_values.time_stamp is 'Standard system-assigned timestamp user for audit purposes.';
comment on column budget_closings_values.user_id is 'Standard system-assigned user id user for audit purposes.';
comment on table budget_version_security is 'A table defining which users have been granted access to which Budget Versions.';
comment on column budget_version_security.users is 'User Id of the user granted access to the given Budget Version.';
comment on column budget_version_security.budget_version_id is 'System-assigned identifier for a particular version of the Budget.';
comment on column budget_version_security.user_id is 'Standard system-assigned user id user for audit purposes.';
comment on column budget_version_security.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on table budget_version_security_all is 'A table defining the Budget Versions to which all users have been granted access.';
comment on column budget_version_security_all.budget_version_id is 'System-assigned identifier for a particular version of the Budget.';
comment on column budget_version_security_all.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column budget_version_security_all.user_id is 'Standard system-assigned user id user for audit purposes.';
comment on table budget_version_security_arch is 'A table defining which users have been granted access to which Archived Budget Versions.';
comment on column budget_version_security_arch.users is 'User Id of the user granted access to the given Budget Version.';
comment on column budget_version_security_arch.budget_version_id is 'System-assigned identifier for a particular version of the Budget.';
comment on column budget_version_security_arch.user_id is 'Standard system-assigned user id user for audit purposes.';
comment on column budget_version_security_arch.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on table budget_version_sec_all_arch is 'A table defining the Archived Budget Versions to which all users have been granted access.';
comment on column budget_version_sec_all_arch.budget_version_id is 'System-assigned identifier for a particular version of the Budget.';
comment on column budget_version_sec_all_arch.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column budget_version_sec_all_arch.user_id is 'Standard system-assigned user id user for audit purposes.';
comment on table cpi_retro_input is '(C) [04] Stores monthly CPI adjustment amounts used in CPI Retro Calculation.';
comment on column cpi_retro_input.company_id is 'System-assigned identifier of a company.';
comment on column cpi_retro_input.work_order_number is 'Work order number of the work order.';
comment on column cpi_retro_input.notes is 'Additional text field for recording notes.';
comment on table ls_location_tax_district is 'This table holds the asset location to tax district mappings for leased asset tax processing';
comment on column ls_location_tax_district.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on column ls_location_tax_district.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on table pp_web_kvjd is 'Key Value JSON Data storage table for temporary data';
comment on table reg_component_activity is 'Table to hold all component activities for each regulatory Family/Source';
comment on column reg_component_activity.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_component_activity.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on table reg_import_ledger_adj_stg is 'Staging table for importing Regulatory Ledger Adjustments (Historic and Forecast).';
comment on column reg_import_ledger_adj_stg.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_import_ledger_adj_stg.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';
comment on table reg_import_ledger_adj_stg_arc is 'Archive table for importing Regulatory Ledger Adjustments (Historic and Forecast).';
comment on column reg_import_ledger_adj_stg_arc.user_id is 'Standard system-assigned user id used for audit purposes.';
comment on column reg_import_ledger_adj_stg_arc.time_stamp is 'Standard system-assigned timestamp used for audit purposes.';




--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2517, 0, 2015, 1, 0, 0, 43422, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_043422_sys_clean_tbl_cmt_ddl.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;