/*
||========================================================================================
|| Application: PowerPlan
|| File Name:   maint_042213_proptax_mktvalcnty_dml.sql
||========================================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||========================================================================================
|| Version  Date       Revised By          Reason for Change
|| -------- ---------- ------------------- -----------------------------------------------
|| 2015.1   01/27/2015 Andrew Scott        market value rate county not working in table maint.
||========================================================================================
*/

update powerplant_dddw 
set code_col = 'county_id'
where dropdown_name = 'pp_county_filter' and code_col = 'state_id_county_id';


--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2220, 0, 2015, 1, 0, 0, 42213, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.1.0.0_maint_042213_proptax_mktvalcnty_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;