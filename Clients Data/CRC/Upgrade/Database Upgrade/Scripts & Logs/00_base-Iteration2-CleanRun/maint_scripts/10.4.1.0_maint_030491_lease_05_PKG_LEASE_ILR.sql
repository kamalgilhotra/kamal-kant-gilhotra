/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_030491_lease_05_PKG_LEASE_ILR.sql
|| Description: Handles calculations around the ILR
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version     Date        Revised By        Reason for Change
|| ----------  ----------  -------------- ----------------------------------
|| 10.4.1.0    07/24/13    Brandon Beck   Point Release
||============================================================================
*/

create or replace package PKG_LEASE_ILR as
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number;
end PKG_LEASE_ILR;
/

create or replace package body PKG_LEASE_ILR as
   --**************************************************************************
   --                            VARIABLES
   --**************************************************************************

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number is
      MY_NUM number;
   begin
      MY_NUM := 1;
      return MY_NUM;
   end F_NEWREVISION;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

end PKG_LEASE_ILR;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (573, 0, 10, 4, 1, 0, 30491, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.1.0_maint_030491_lease_05_PKG_LEASE_ILR.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
