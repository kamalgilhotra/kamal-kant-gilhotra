/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_011494_pp_import_data.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   01/31/2013 Joseph King    Point Release
||============================================================================
*/

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 53 IMPORT_TYPE_ID, 'company_id' COLUMN_NAME, 19 IMPORT_LOOKUP_ID from DUAL
	minus select import_type_id, column_name, import_lookup_id from PP_IMPORT_COLUMN_LOOKUP;
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 53 IMPORT_TYPE_ID, 'company_id' COLUMN_NAME, 20 IMPORT_LOOKUP_ID from DUAL
	minus select import_type_id, column_name, import_lookup_id from PP_IMPORT_COLUMN_LOOKUP;
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 53 IMPORT_TYPE_ID, 'company_id' COLUMN_NAME, 21 IMPORT_LOOKUP_ID from DUAL
	minus select import_type_id, column_name, import_lookup_id from PP_IMPORT_COLUMN_LOOKUP;
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 53 IMPORT_TYPE_ID, 'utility_account_id' COLUMN_NAME, 28 IMPORT_LOOKUP_ID from DUAL
	minus select import_type_id, column_name, import_lookup_id from PP_IMPORT_COLUMN_LOOKUP;
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 53 IMPORT_TYPE_ID, 'utility_account_id' COLUMN_NAME, 30 IMPORT_LOOKUP_ID from DUAL
	minus select import_type_id, column_name, import_lookup_id from PP_IMPORT_COLUMN_LOOKUP;

insert into PP_IMPORT_LOOKUP
   (IMPORT_LOOKUP_ID, DESCRIPTION, LONG_DESCRIPTION, COLUMN_NAME, LOOKUP_SQL, IS_DERIVED,
    LOOKUP_TABLE_NAME, LOOKUP_COLUMN_NAME)
select 622, 'Activity Code.Description',
    'The passed in value corresponds to the Activity Code: Description. Translate to the Description using Description',
    'activity_code',
    '( select actcd.description from activity_code actcd where upper( trim( <importfield> ) ) = upper( trim( actcd.description ) ) )',
    0, 'activity_code', 'description'
from dual
where not exists (Select 1 from pp_import_lookup where import_lookup_id = 622);

/* update PP_IMPORT_TEMPLATE_FIELDS set AUTOCREATE_YN = 0 where AUTOCREATE_YN is null; */ /* Column no longer exists */

insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 53 IMPORT_TYPE_ID, 'activity_code' COLUMN_NAME, 622 IMPORT_LOOKUP_ID from DUAL
	minus select import_type_id, column_name, import_lookup_id from PP_IMPORT_COLUMN_LOOKUP;
insert into PP_IMPORT_COLUMN_LOOKUP
   (IMPORT_TYPE_ID, COLUMN_NAME, IMPORT_LOOKUP_ID)
   select 54 IMPORT_TYPE_ID, 'activity_code' COLUMN_NAME, 622 IMPORT_LOOKUP_ID from DUAL
	minus select import_type_id, column_name, import_lookup_id from PP_IMPORT_COLUMN_LOOKUP;

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (281, 0, 10, 4, 0, 0, 11494, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_011494_pp_import_data.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;