SET SERVEROUTPUT ON
/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_002868_cwip_wo_accrued_gain_loss_changes.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   03/26/2013 Sunjin Cone    Point Release
||============================================================================
*/

declare
   L_COLUMN_EXISTS varchar2(10);

begin
   select DECODE(count(*), 1, 'TRUE', 'FALSE') COLUMN_EXISTS
     into L_COLUMN_EXISTS
     from USER_TAB_COLUMNS
    where TABLE_NAME = 'WO_ACCRUED_GAIN_LOSS'
      and COLUMN_NAME = 'SET_OF_BOOKS_ID';

   if L_COLUMN_EXISTS = 'TRUE' then
      DBMS_OUTPUT.PUT_LINE('WO_ACCRUED_GAIN_LOSS already has SET_OF_BOOKS_ID column.');
      return;
   end if;

   execute immediate 'create table WO_ACCRUED_GAIN_LOSS_TEMPORARY as select * from WO_ACCRUED_GAIN_LOSS';
   DBMS_OUTPUT.PUT_LINE('Table WO_ACCRUED_GAIN_LOSS_TEMPORARY created.');

   execute immediate 'drop table WO_ACCRUED_GAIN_LOSS';
   DBMS_OUTPUT.PUT_LINE('Table WO_ACCRUED_GAIN_LOSS dropped.');

   execute immediate 'CREATE TABLE WO_ACCRUED_GAIN_LOSS
                   (
                    ASSET_ID         number(22,0) not null,
                    WORK_ORDER_ID    number(22,0) not null,
                    RESERVE_AMOUNT   number(22,2) null,
                    ACCR_GAIN_LOSS   number(22,2) null,
                    PEND_TRANS_ID    number(22,0) not null,
                    CPR_MONTH_NUMBER number(22,0) null,
                    POSTING_DATE     date         null,
                    USER_ID          varchar2(18) null,
                    TIME_STAMP       date         null,
                    UNIT_ITEM_ID     number(22,0) null,
                    SET_OF_BOOKS_ID  number(22,0) not null
                   )';
   DBMS_OUTPUT.PUT_LINE('Table WO_ACCRUED_GAIN_LOSS created.');

   execute immediate 'alter table WO_ACCRUED_GAIN_LOSS
                      add constraint WO_ACCRUED_GAIN_LOSS_PK
                          primary key (PEND_TRANS_ID, SET_OF_BOOKS_ID)
                          using index tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Primary key created.');

   execute immediate 'create index R_WO_ACCRUED_GAIN_LOSS3_IX
                      on WO_ACCRUED_GAIN_LOSS (WORK_ORDER_ID)
                         tablespace PWRPLANT_IDX';
   DBMS_OUTPUT.PUT_LINE('Index R_WO_ACCRUED_GAIN_LOSS3_IX created.');

   execute immediate 'alter table WO_ACCRUED_GAIN_LOSS
                      add constraint R_WO_ACCRUED_GAIN_LOSS1
                          foreign key (ASSET_ID)
                          references CPR_LEDGER (ASSET_ID)';
   DBMS_OUTPUT.PUT_LINE('Constraint R_WO_ACCRUED_GAIN_LOSS1 created.');

   execute immediate 'alter table WO_ACCRUED_GAIN_LOSS
                      add constraint R_WO_ACCRUED_GAIN_LOSS2
                          foreign key (WORK_ORDER_ID)
                          references WORK_ORDER_CONTROL (WORK_ORDER_ID)';
   DBMS_OUTPUT.PUT_LINE('Constraint R_WO_ACCRUED_GAIN_LOSS2 created.');

   execute immediate 'alter table WO_ACCRUED_GAIN_LOSS
                      add constraint R_WO_ACCRUED_GAIN_LOSS3
                          foreign key (SET_OF_BOOKS_ID)
                          references SET_OF_BOOKS (SET_OF_BOOKS_ID)';
   DBMS_OUTPUT.PUT_LINE('Constraint R_WO_ACCRUED_GAIN_LOSS3 created.');

   execute immediate 'insert into WO_ACCRUED_GAIN_LOSS
                      (ASSET_ID, WORK_ORDER_ID, RESERVE_AMOUNT, ACCR_GAIN_LOSS, PEND_TRANS_ID, CPR_MONTH_NUMBER,
                       POSTING_DATE, UNIT_ITEM_ID, SET_OF_BOOKS_ID)
                      select ASSET_ID,
                             WORK_ORDER_ID,
                             -RESERVE_AMOUNT,
                             ACCR_GAIN_LOSS,
                             PEND_TRANS_ID,
                             CPR_MONTH_NUMBER,
                             POSTING_DATE,
                             UNIT_ITEM_ID,
                             1
                        from WO_ACCRUED_GAIN_LOSS_TEMPORARY';

   execute immediate 'insert into WO_ACCRUED_GAIN_LOSS
                      (ASSET_ID, WORK_ORDER_ID, RESERVE_AMOUNT, ACCR_GAIN_LOSS, PEND_TRANS_ID, CPR_MONTH_NUMBER,
                       POSTING_DATE, UNIT_ITEM_ID, SET_OF_BOOKS_ID)
                      select A.ASSET_ID,
                             A.WORK_ORDER_ID,
                             0,
                             0,
                             A.PEND_TRANS_ID,
                             A.CPR_MONTH_NUMBER,
                             A.POSTING_DATE,
                             null,
                             B.SET_OF_BOOKS_ID
                        from WO_ACCRUED_GAIN_LOSS A, COMPANY_SET_OF_BOOKS B, CPR_LEDGER C
                       where A.ASSET_ID = C.ASSET_ID
                         and C.COMPANY_ID = B.COMPANY_ID
                         and B.SET_OF_BOOKS_ID <> 1
                         and B.INCLUDE_INDICATOR = 1';
   DBMS_OUTPUT.PUT_LINE('Inserts processed.');

   execute immediate 'drop table WO_ACCRUED_GAIN_LOSS_TEMPORARY';
   DBMS_OUTPUT.PUT_LINE('Table WO_ACCRUED_GAIN_LOSS_TEMPORARY dropped.');
end;
/

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (334, 0, 10, 4, 0, 0, 2868, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_002868_cwip_wo_accrued_gain_loss_changes.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;