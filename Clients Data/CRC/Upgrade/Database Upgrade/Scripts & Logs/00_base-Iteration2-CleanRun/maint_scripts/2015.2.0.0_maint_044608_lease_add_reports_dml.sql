/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_044608_lease_add_reports_dml.sql
||============================================================================
|| Copyright (C) 2015 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By       Reason for Change
|| ---------- ---------- ---------------- ------------------------------------
|| 2015.2     11/03/2013 Will Davis       Add various base lease reports
||============================================================================
*/

SET SERVEROUTPUT ON

declare
filter1 number;
filter2 number;
filter3 number;
filter4 number;
filter5 number;
filter6 number;
location varchar2(5000);
report_num number:=1;
begin
select max(pp_report_filter_id) + 1 into filter1 from pp_reports_filter;
select max(pp_report_filter_id) + 2 into filter2 from pp_reports_filter;
select max(pp_report_filter_id) + 3 into filter3 from pp_reports_filter;
select max(pp_report_filter_id) + 4 into filter4 from pp_reports_filter;
select max(pp_report_filter_id) + 5 into filter5 from pp_reports_filter;
select max(pp_report_filter_id) + 6 into filter6 from pp_reports_filter;

insert into pp_reports_filter
(pp_report_filter_id, description, table_name, filter_uo_name)
select
filter1, 'Lessee - Schedule Asset', 'company_setup', 'uo_ls_selecttabs_schedule_asset'
from dual;

insert into pp_reports_filter
(pp_report_filter_id, description, table_name, filter_uo_name)
select
filter2, 'Lessee - Schedule Asset Location', 'company_setup', 'uo_ls_selecttabs_schedule_asset_loc'
from dual;

insert into pp_reports_filter
(PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
select
filter3, 'Lessee - Tax Local and Tax District', 'uo_ls_selecttabs_tax_local_tax_dist'
from dual;

insert into pp_reports_filter
(PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
select
filter4, 'Lessee - All Schedule w/ Tax Local', 'uo_ls_selecttabs_sched_tax_local'
from dual;

insert into pp_reports_filter
(PP_REPORT_FILTER_ID, DESCRIPTION, FILTER_UO_NAME)
select
filter5, 'Lessee - All Schedule w/ UA', 'uo_ls_selecttabs_sched_ua'
from dual;

update pp_reports
set pp_report_filter_id = filter1
where lower(description) like '%schedule by asset%'
  and lower(report_type) like '%lessee%';

update pp_reports
set pp_report_time_option_id = 0
where report_type = 'Lessee - Schedules'
  and lower(description) like '%schedule by%';

location:='Report ' || report_num;
INSERT INTO pp_reports
 (REPORT_ID,DESCRIPTION,LONG_DESCRIPTION,SUBSYSTEM,DATAWINDOW,SPECIAL_NOTE,REPORT_TYPE,TIME_OPTION,REPORT_NUMBER,INPUT_WINDOW,FILTER_OPTION,STATUS,PP_REPORT_SUBSYSTEM_ID,REPORT_TYPE_ID,PP_REPORT_TIME_OPTION_ID,PP_REPORT_FILTER_ID,PP_REPORT_STATUS_ID,PP_REPORT_ENVIR_ID,DOCUMENTATION,USER_COMMENT,LAST_APPROVED_DATE,PP_REPORT_NUMBER,OLD_REPORT_NUMBER,DYNAMIC_DW,TURN_OFF_MULTI_THREAD)
 VALUES((select max(report_id) +1 from pp_reports),'High Low Terms by Company','High and Low Lease Terms by Company. Includes total terms and terms remaining. ','Lessee','dw_ls_rpt_remaining_terms','already has report time','Lessee - Other','Single Month','Lessee - 1030','','','',11,309,1,42,1,1,'','',to_date('', 'dd/mm/rrrr hh:mi:ss'),'Lessee - 1030','',0,null);
report_num:=report_num + 1;
location:='Report ' || report_num;
INSERT INTO pp_reports
 (REPORT_ID,DESCRIPTION,LONG_DESCRIPTION,SUBSYSTEM,DATAWINDOW,SPECIAL_NOTE,REPORT_TYPE,TIME_OPTION,REPORT_NUMBER,INPUT_WINDOW,FILTER_OPTION,STATUS,PP_REPORT_SUBSYSTEM_ID,REPORT_TYPE_ID,PP_REPORT_TIME_OPTION_ID,PP_REPORT_FILTER_ID,PP_REPORT_STATUS_ID,PP_REPORT_ENVIR_ID,DOCUMENTATION,USER_COMMENT,LAST_APPROVED_DATE,PP_REPORT_NUMBER,OLD_REPORT_NUMBER,DYNAMIC_DW,TURN_OFF_MULTI_THREAD)
 VALUES((select max(report_id) +1 from pp_reports),'Contingent Rental Report','Contingent rental amounts by ILR','Lessee','dw_ls_rpt_cont_rental','already has report time','Lessee - Other','Span','Lessee - 1031','','','',11,309,2,filter2,1,1,'','',to_date('', 'dd/mm/rrrr hh:mi:ss'),'Lessee - 1031','',0,null);
report_num:=report_num + 1;
location:='Report ' || report_num;
INSERT INTO pp_reports
 (REPORT_ID,DESCRIPTION,LONG_DESCRIPTION,SUBSYSTEM,DATAWINDOW,SPECIAL_NOTE,REPORT_TYPE,TIME_OPTION,REPORT_NUMBER,INPUT_WINDOW,FILTER_OPTION,STATUS,PP_REPORT_SUBSYSTEM_ID,REPORT_TYPE_ID,PP_REPORT_TIME_OPTION_ID,PP_REPORT_FILTER_ID,PP_REPORT_STATUS_ID,PP_REPORT_ENVIR_ID,DOCUMENTATION,USER_COMMENT,LAST_APPROVED_DATE,PP_REPORT_NUMBER,OLD_REPORT_NUMBER,DYNAMIC_DW,TURN_OFF_MULTI_THREAD)
 VALUES((select max(report_id) +1 from pp_reports),'Effective Tax Rates by District','Effective Tax Rates by District','Lessee','dw_ls_rpt_eff_tax_rate','already has report time','Lessee - Tax','Single Month','Lessee - 1032','','','',11,310,1,filter3,1,1,'','',to_date('', 'dd/mm/rrrr hh:mi:ss'),'Lessee - 1032','',0,null);
report_num:=report_num + 1;
location:='Report ' || report_num;
INSERT INTO pp_reports
 (REPORT_ID,DESCRIPTION,LONG_DESCRIPTION,SUBSYSTEM,DATAWINDOW,SPECIAL_NOTE,REPORT_TYPE,TIME_OPTION,REPORT_NUMBER,INPUT_WINDOW,FILTER_OPTION,STATUS,PP_REPORT_SUBSYSTEM_ID,REPORT_TYPE_ID,PP_REPORT_TIME_OPTION_ID,PP_REPORT_FILTER_ID,PP_REPORT_STATUS_ID,PP_REPORT_ENVIR_ID,DOCUMENTATION,USER_COMMENT,LAST_APPROVED_DATE,PP_REPORT_NUMBER,OLD_REPORT_NUMBER,DYNAMIC_DW,TURN_OFF_MULTI_THREAD)
 VALUES((select max(report_id) +1 from pp_reports),'High Low Interest Rates','High and Low Interest Rates by Company','Lessee','dw_ls_rpt_int_rates','already has report time','Lessee - Other','Single Month','Lessee - 1033','','','',11,309,1,filter1,1,1,'','',to_date('', 'dd/mm/rrrr hh:mi:ss'),'Lessee - 1033','',0,null);
report_num:=report_num + 1;
location:='Report ' || report_num;
INSERT INTO pp_reports
 (REPORT_ID,DESCRIPTION,LONG_DESCRIPTION,SUBSYSTEM,DATAWINDOW,SPECIAL_NOTE,REPORT_TYPE,TIME_OPTION,REPORT_NUMBER,INPUT_WINDOW,FILTER_OPTION,STATUS,PP_REPORT_SUBSYSTEM_ID,REPORT_TYPE_ID,PP_REPORT_TIME_OPTION_ID,PP_REPORT_FILTER_ID,PP_REPORT_STATUS_ID,PP_REPORT_ENVIR_ID,DOCUMENTATION,USER_COMMENT,LAST_APPROVED_DATE,PP_REPORT_NUMBER,OLD_REPORT_NUMBER,DYNAMIC_DW,TURN_OFF_MULTI_THREAD)
 VALUES((select max(report_id) +1 from pp_reports),'Net Book Value by ILR','Net Book Value by ILR','Lessee','dw_ls_rpt_nbv_by_ilr','already has report time','Lessee - Schedules','Single Month','Lessee - 1034','','','',11,308,1,filter2,1,1,'','',to_date('', 'dd/mm/rrrr hh:mi:ss'),'Lessee - 1034','',0,null);
report_num:=report_num + 1;
location:='Report ' || report_num;
INSERT INTO pp_reports
 (REPORT_ID,DESCRIPTION,LONG_DESCRIPTION,SUBSYSTEM,DATAWINDOW,SPECIAL_NOTE,REPORT_TYPE,TIME_OPTION,REPORT_NUMBER,INPUT_WINDOW,FILTER_OPTION,STATUS,PP_REPORT_SUBSYSTEM_ID,REPORT_TYPE_ID,PP_REPORT_TIME_OPTION_ID,PP_REPORT_FILTER_ID,PP_REPORT_STATUS_ID,PP_REPORT_ENVIR_ID,DOCUMENTATION,USER_COMMENT,LAST_APPROVED_DATE,PP_REPORT_NUMBER,OLD_REPORT_NUMBER,DYNAMIC_DW,TURN_OFF_MULTI_THREAD)
 VALUES((select max(report_id) +1 from pp_reports),'GL Account Balances - All Accounts','GL Account Balances - All Accounts','Lessee','dw_ls_rpt_gl_account_balances_all_accts','already has report time','Lessee - Other','Single Month','Lessee - 1040','','','',11,309,1,filter1,1,1,'','',to_date('', 'dd/mm/rrrr hh:mi:ss'),'Lessee - 1040','',0,null);
report_num:=report_num + 1;
location:='Report ' || report_num;
Insert into pp_reports (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_NUMBER, DYNAMIC_DW) Values ((select max(report_id) +1 from pp_reports) ,
'Accumulated Depr by Company', 'Accumulated Depreciation by Company', 'Lessee', 'dw_ls_rpt_reserve_balances_by_co', 'already has report time', 'Lessee - Other', 'Single Month', 'Lessee - 1048', '11',
'309', '1', filter2, '1', '1', 'Lessee - 1048', '0');
report_num:=report_num + 1;
location:='Report ' || report_num;
Insert into pp_reports (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_NUMBER, DYNAMIC_DW) Values ((select max(report_id) +1 from pp_reports) ,
'Accumulated Depr by Company YTD', 'Accumulated Depreciation by Company YTD', 'Lessee', 'dw_ls_rpt_reserve_balances_by_co_ytd', 'already has report time', 'Lessee - Other', 'Single Month', 'Lessee - 1049',
'11', '309', '1', filter2, '1', '1', 'Lessee - 1049', '0');
report_num:=report_num + 1;
location:='Report ' || report_num;
Insert into pp_reports (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_NUMBER, DYNAMIC_DW) Values ((select max(report_id) +1 from pp_reports) ,
'GL Account Balances - All Accts YTD', 'GL Account Balances - All Accounts YTD', 'Lessee', 'dw_ls_rpt_gl_account_balances_all_accts_ytd', 'already has report time', 'Lessee - Other', 'Single Month', 'Lessee - 1050',
'11', '309', '1', filter2, '1', '1', 'Lessee - 1050', '0');
report_num:=report_num + 1;
location:='Report ' || report_num;
Insert into pp_reports (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_NUMBER, DYNAMIC_DW) Values ((select max(report_id) +1 from pp_reports) ,
'Capitalized Cost by Company', 'Capitalized Cost by Company', 'Lessee', 'dw_ls_rpt_cap_cost_by_company', 'already has report time', 'Lessee - Other', 'Single Month', 'Lessee - 1051', '11', '309', '1',
filter2, '1', '1', 'Lessee - 1051', '0');
report_num:=report_num + 1;
location:='Report ' || report_num;
Insert into pp_reports (REPORT_ID, DESCRIPTION, LONG_DESCRIPTION, SUBSYSTEM, DATAWINDOW, SPECIAL_NOTE, REPORT_TYPE, TIME_OPTION, REPORT_NUMBER, PP_REPORT_SUBSYSTEM_ID, REPORT_TYPE_ID,
PP_REPORT_TIME_OPTION_ID, PP_REPORT_FILTER_ID, PP_REPORT_STATUS_ID, PP_REPORT_ENVIR_ID, PP_REPORT_NUMBER, DYNAMIC_DW) Values ((select max(report_id) +1 from pp_reports) ,
'Capitalized Cost by Company YTD', 'Capitalized Cost by Company YTD', 'Lessee', 'dw_ls_rpt_cap_cost_by_company_ytd', 'already has report time', 'Lessee - Other', 'Single Month', 'Lessee - 1052', '11',
'309', '1', filter2, '1', '1', 'Lessee - 1052', '0');
exception when others then
  dbms_output.put_line(sqlerrm || ' - ' || location);
end;
/



--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
	(ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
	SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
	(2956, 0, 2015, 2, 0, 0, 44608, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2015.2.0.0_maint_044608_lease_add_reports_dml.sql', 1,
	SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
	SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;