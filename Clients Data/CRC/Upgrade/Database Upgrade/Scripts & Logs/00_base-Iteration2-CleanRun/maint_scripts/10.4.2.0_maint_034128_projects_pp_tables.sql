/*
||============================================================================
|| Application: PowerPlant
|| Object Name: maint_034128_projects_pp_tables.sql
|| Description:
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.2.0   11/19/2013 Stephen Motter
||============================================================================
*/

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('action_code', TO_DATE('2013-11-18 16:37:59', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 's',
    'Action Code', 'always', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null);

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('crew_type', TO_DATE('2013-11-18 16:38:23', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 's',
    'Crew Type', 'always', null, null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null);

insert into POWERPLANT_TABLES
   (TABLE_NAME, TIME_STAMP, USER_ID, PP_TABLE_TYPE_ID, DESCRIPTION, SUBSYSTEM_SCREEN, SELECT_WINDOW,
    SUBSYSTEM_DISPLAY, SUBSYSTEM, DELIVERY, NOTES, ASSET_MANAGEMENT, BUDGET, CHARGE_REPOSITORY,
    CWIP_ACCOUNTING, DEPR_STUDIES, LEASE, PROPERTY_TAX, POWERTAX_PROVISION, POWERTAX, SYSTEM,
    UNITIZATION, WORK_ORDER_MANAGEMENT, CLIENT, WHERE_CLAUSE)
values
   ('work_situation', TO_DATE('2013-11-18 16:38:42', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT', 's',
    'Work Situation', 'always', null, null, null, null, null, null, null, null, null, null, null,
    null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('action_code_id', 'action_code', TO_DATE('2013-11-18 16:44:36', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 's', null, 'action code id', 1, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('action_indicator', 'action_code', TO_DATE('2013-11-18 16:49:10', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'l', null, 'action indicator', 2, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'action_code', TO_DATE('2013-11-18 16:38:03', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'description', 0, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_action_code', 'action_code', TO_DATE('2013-11-18 16:38:03', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'external action code', 4, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'action_code', TO_DATE('2013-11-18 16:38:03', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'long description', 5, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'action_code', TO_DATE('2013-11-18 16:38:03', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'time stamp', 100, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'action_code', TO_DATE('2013-11-18 16:38:03', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    null, 'e', null, 'user id', 101, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('crew_type_id', 'crew_type', TO_DATE('2013-11-18 16:40:29', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 's', null, 'crew type id', 1, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'crew_type', TO_DATE('2013-11-18 16:38:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    null, 'e', null, 'description', 0, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('external_value', 'crew_type', TO_DATE('2013-11-18 16:38:25', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'external value', 3, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('hourly_rate', 'crew_type', TO_DATE('2013-11-18 16:38:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    null, 'e', null, 'hourly rate', 4, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('long_description', 'crew_type', TO_DATE('2013-11-18 16:38:25', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'long description', 5, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'crew_type', TO_DATE('2013-11-18 16:38:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    null, 'e', null, 'time stamp', 100, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'crew_type', TO_DATE('2013-11-18 16:38:25', 'yyyy-mm-dd hh24:mi:ss'), 'PWRPLANT',
    null, 'e', null, 'user id', 101, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('wo_eng_est_type', 'crew_type', TO_DATE('2013-11-18 16:38:25', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'wo eng est type', 8, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('description', 'work_situation', TO_DATE('2013-11-18 16:38:44', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'description', 0, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('labor_hrs_load_pct', 'work_situation', TO_DATE('2013-11-18 16:38:44', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'labor hrs load pct', 2, null, null, null, null, null, null, null,
    null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('time_stamp', 'work_situation', TO_DATE('2013-11-18 16:38:44', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'time stamp', 100, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('user_id', 'work_situation', TO_DATE('2013-11-18 16:38:44', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 'e', null, 'user id', 101, null, null, null, null, null, null, null, null);

insert into POWERPLANT_COLUMNS
   (COLUMN_NAME, TABLE_NAME, TIME_STAMP, USER_ID, DROPDOWN_NAME, PP_EDIT_TYPE_ID, HELP_INDEX,
    DESCRIPTION, COLUMN_RANK, SHORT_LONG_DESCRIPTION, EDIT_MASK, DROPDOWN_PERCENT, DROPDOWN_RESTRICT,
    RELATED_TYPE, RELATED_TABLE, READ_ONLY, DEFAULT_VALUE)
values
   ('work_situation_id', 'work_situation', TO_DATE('2013-11-18 16:43:38', 'yyyy-mm-dd hh24:mi:ss'),
    'PWRPLANT', null, 's', null, 'work situation id', 5, null, null, null, null, null, null, null,
    null);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (765, 0, 10, 4, 2, 0, 34128, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.2.0_maint_034128_projects_pp_tables.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;