/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_029815_lease_payment_terms.sql
||============================================================================
|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.4.0.0   04/26/2013 Brandon Beck   Point Release
||============================================================================
*/

delete from LS_PAYMENT_TERM_TYPE where PAYMENT_TERM_TYPE_ID in (1, 3);

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (367, 0, 10, 4, 0, 0, 29815, 'C:\PlasticWks\powerplant\sql\maint_scripts', '10.4.0.0_maint_029815_lease_payment_terms.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;