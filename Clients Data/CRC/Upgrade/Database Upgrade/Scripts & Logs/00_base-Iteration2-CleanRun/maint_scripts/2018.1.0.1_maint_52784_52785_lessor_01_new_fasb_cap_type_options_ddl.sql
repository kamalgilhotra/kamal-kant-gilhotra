/*
||============================================================================
|| Application: PowerPlan
|| File Name:   maint_52784_52785_lessor_01_new_fasb_cap_type_options_ddl.sql
||============================================================================
|| Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version    Date        Revised By       Reason for Change
|| ---------- ----------  ---------------- --------------------------------------
|| 2018.1.0.1 12/03/2018  Josh Sandler     Add new options for Lessor cap types
||============================================================================
*/

ALTER TABLE lsr_fasb_type_sob
ADD separate_cost_of_goods_sw NUMBER(1);

ALTER TABLE lsr_fasb_type_sob
  ADD CONSTRAINT fk4_separate_cost_of_goods FOREIGN KEY (
    separate_cost_of_goods_sw
  ) REFERENCES yes_no (
    yes_no_id
  )
;


ALTER TABLE lsr_fasb_type_sob
ADD include_idc_sw NUMBER(1);

ALTER TABLE lsr_fasb_type_sob
  ADD CONSTRAINT fk5_include_idc FOREIGN KEY (
    include_idc_sw
  ) REFERENCES yes_no (
    yes_no_id
  )
;

alter table lsr_ilr_group
add (cost_of_goods_sold_account_id number(22,0) null,
     revenue_account_id number(22,0) null
);

comment on column lsr_ilr_group.cost_of_goods_sold_account_id is 'The cost of goods sold account';
comment on column lsr_ilr_group.revenue_account_id is 'The revenue account';

ALTER TABLE lsr_ilr_group ADD CONSTRAINT lsr_ilr_group_cog_fk FOREIGN KEY (cost_of_goods_sold_account_id) REFERENCES gl_account(gl_account_id);
alter table lsr_ilr_group add constraint lsr_ilr_group_revenue_fk foreign key (revenue_account_id) references gl_account(gl_account_id);

alter table lsr_ilr_account
add (cost_of_goods_sold_account_id number(22,0) null,
     revenue_account_id number(22,0) null
);

comment on column lsr_ilr_account.cost_of_goods_sold_account_id is 'The cost of goods sold account';
comment on column lsr_ilr_account.revenue_account_id is 'The revenue account';

ALTER TABLE lsr_ilr_account ADD CONSTRAINT lsr_ilr_account_cog_fk FOREIGN KEY (cost_of_goods_sold_account_id) REFERENCES gl_account(gl_account_id);
ALTER TABLE lsr_ilr_account ADD CONSTRAINT lsr_ilr_account_revenue_fk FOREIGN KEY (revenue_account_id) REFERENCES gl_account(gl_account_id);

--****************************************************
--Log the run of the script to local PP_SCHEMA_LOG
--****************************************************
INSERT INTO PP_SCHEMA_CHANGE_LOG
    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
VALUES
    (13042, 0, 2018, 1, 0, 1, 52785, 'C:\PlasticWks\powerplant\sql\maint_scripts', '2018.1.0.1_maint_52784_52785_lessor_01_new_fasb_cap_type_options_ddl.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
COMMIT;