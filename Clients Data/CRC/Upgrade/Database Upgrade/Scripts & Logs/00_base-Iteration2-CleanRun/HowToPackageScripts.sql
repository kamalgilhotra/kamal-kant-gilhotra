-- Version Data Maint file
-- Create a maint file with the following synatax: maint_<maint num>_sys_load_version_data.sql
-- Copy the last releases version of this file and remove the insert statements.
-- Add the Approved insert statement at the bottom.  Run the insert into the MASTERDB.
-- Merge the script to the DEV branch.  We want this script listed as being run
-- so that it will get picked up in the steps below as something that needs to be run.  At the
-- end of these scripts we will generate the contents of this file and update it in Plastic.

-- Renamed list of maint scripts
-- SQLPlus run in MasterDB
-- change the VERSION number and the STARTING SCRIPT NUMBER - The starting
-- script number should be something above the last script in the
-- previous version.  Typically you want to leave a gap incase a prior
-- release patch is putout after a Major release.

--SQL1Begin
select T1.SCRIPT_NAME,
       T1.NEW_SCRIPT_NAME,
       decode( T1.SCRIPT_TYPE, 'packages',
               -- Attach footer to packages when the files are copied      
               'copy ' || PSCL.SCRIPT_PATH || '\' || T1.SCRIPT_NAME || ' ' || T1.NEW_SCRIPT_NAME|| CHR(13)||
               --' ECHO.>> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO --************************** >> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO -- Log the run of the script >> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO --************************** >> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               --' ECHO.>> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO insert into PP_SCHEMA_CHANGE_LOG >> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO    (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH, >> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO     SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME) >> '|| T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO values >> '|| T1.NEW_SCRIPT_NAME || CHR(13) ||
               ' ECHO    (' || PSCL.ID || ', ' || PSCL.SKIPPED || ', ' || PSCL.MAJOR_VERSION || ', ' || PSCL.MINOR_VERSION || ', ' || PSCL.POINT_VERSION || ', ' || PSCL.PATCH_VERSION || ', ' || PSCL.MAINT_NUM || ', ''' || PSCL.SCRIPT_PATH || ''', >> ' || T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO     ''' || PSCL.SCRIPT_NAME || ''', ' || PSCL.SCRIPT_REVISION || ', SYSTIMESTAMP, SYS_CONTEXT(''USERENV'', ''OS_USER''), SYS_CONTEXT(''USERENV'', ''TERMINAL''), SYS_CONTEXT(''USERENV'', ''SERVICE_NAME'')); >> ' || T1.NEW_SCRIPT_NAME || CHR(13)||
               ' ECHO commit; >> '|| T1.NEW_SCRIPT_NAME || CHR(13),
               
               -- Copy non-packages as is
               'copy ' || PSCL.SCRIPT_PATH || '\' || T1.SCRIPT_NAME || ' ' || T1.NEW_SCRIPT_NAME
              ) BATCH_RENAME,
              
       'SPOOL &&&PP_SCRIPT_PATH.' || SUBSTR(T1.NEW_SCRIPT_NAME, 1, LENGTH(T1.NEW_SCRIPT_NAME) - 3) ||
       'log' || CHR(13) || '@&&&PP_SCRIPT_PATH.' || T1.NEW_SCRIPT_NAME || CHR(13) || 'SPOOL OFF' MASTER_CALL_SQL,
       'copy ' || T1.NEW_SCRIPT_NAME || ' ' || T1.SCRIPT_NAME BATCH_RENAME_REVERSE
  from (select 4949 + ROWNUM || '_' || to_char(MAJOR_VERSION) || '.' || to_char(MINOR_VERSION) || '.' || to_char(POINT_VERSION) || '.' || to_char(PATCH_VERSION ) || '_' || SCRIPT_NAME NEW_SCRIPT_NAME, 
          SCRIPT_NAME, ID, SCRIPT_REVISION, SCRIPT_TYPE
          from (select SCRIPT_NAME, max(ID) ID, SCRIPT_REVISION, major_version, minor_version, point_version, patch_version,
                       substr( lower(script_path)  , instr( lower(script_path)  , '\', -1, 1 ) + 1, length( lower(script_path)   ) - instr( lower(script_path)  , '\', -1, 1 )) SCRIPT_TYPE
                  from PP_SCHEMA_CHANGE_LOG
                 where to_char(MAJOR_VERSION) || '.' || to_char(MINOR_VERSION) || '.' || to_char(POINT_VERSION) || '.' || to_char(PATCH_VERSION )
              IN ( '2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
                   --and MINOR_VERSION = 4
                   --and POINT_VERSION = 0
                   --and PATCH_VERSION = 0
                 group by SCRIPT_NAME, SCRIPT_REVISION, major_version, minor_version, point_version, patch_version, lower(script_path)  
                 order by max(DATE_APPLIED))) T1,
         PP_SCHEMA_CHANGE_LOG PSCL
  where PSCL.ID = T1.ID
    and PSCL.SCRIPT_REVISION = T1.SCRIPT_REVISION
order by T1.NEW_SCRIPT_NAME;
--SQL1End

--Copy BATCH_RENAME column to copy_scripts.bat
--Run copy_scripts.bat
--check copy_scripts.log for issues run_scripts.sql

-- the rest of this script can be ran in one shot.  If all the results look good commit the DB changes

--Get maint list from MASTERDB - PP_SCHEMA_CHANGE_LOG
select *
from PP_SCHEMA_CHANGE_LOG
where to_char(MAJOR_VERSION) || '.' || to_char(MINOR_VERSION) || '.' || to_char(POINT_VERSION) || '.' || to_char(PATCH_VERSION )
        IN ( '2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0') 
order by DATE_APPLIED;

--Save to a text document and sort by name.
--File Name: 2017.4.0.0_masterdb_scripts_sorted_by_name.txt

--Compare the 2 files to make sure all maints are covered.

-- Clean up PP_SCHEMA_UPGRADE for 2017.4.0.0 in case we are repackaging.
delete PP_SCHEMA_UPGRADE
where to_char(MAJOR_VERSION) || '.' || to_char(MINOR_VERSION) || '.' || to_char(POINT_VERSION) || '.' || to_char(PATCH_VERSION )
        IN ( '2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0') 

-- Fill in PP_SCHEMA_UPGRADE in MASTERDB Database - in the future change the
-- MAJOR_VERSION || MINOR_VERSION || POINT_VERSION || PATCH_VERSION = 2017.4.0.0 to = current version

insert into PP_SCHEMA_UPGRADE
   (BASE_ID, BASE_SKIPPED, BASE_MAJOR_VERSION, BASE_MINOR_VERSION, BASE_POINT_VERSION, BASE_PATCH_VERSION,
    BASE_MAINT_NUM, BASE_SCRIPT_PATH, BASE_SCRIPT_NAME, BASE_SCRIPT_REVISION, BASE_DATE_APPLIED, BASE_OS_USER,
    BASE_TERMINAL, BASE_SERVICE_NAME, BASE_USER_ID, BASE_TIME_STAMP, RELEASED_ID, RELEASED_SKIPPED,
    RELEASED_MAJOR_VERSION, RELEASED_MINOR_VERSION, RELEASED_POINT_VERSION, RELEASED_PATCH_VERSION, RELEASED_MAINT_NUM,
    RELEASED_SCRIPT_PATH, RELEASED_SCRIPT_NAME, RELEASED_SCRIPT_REVISION, RELEASED_DATE_APPLIED, RELEASED_OS_USER,
    RELEASED_TERMINAL, RELEASED_SERVICE_NAME, RELEASED_USER_ID, RELEASED_TIME_STAMP, RELEASED_MODULE,
    RELEASED_NOT_NEEDED)
   select ID,
          SKIPPED,
          MAJOR_VERSION,
          MINOR_VERSION,
          POINT_VERSION,
          PATCH_VERSION,
          MAINT_NUM,
          SCRIPT_PATH,
          SCRIPT_NAME,
          SCRIPT_REVISION,
          DATE_APPLIED,
          OS_USER,
          TERMINAL,
          SERVICE_NAME,
          USER_ID,
          TIME_STAMP,
          ID              RELEASED_ID,
          SKIPPED         RELEASED_SKIPPED,
          MAJOR_VERSION   RELEASED_MAJOR_VERSION,
          MINOR_VERSION   RELEASED_MINOR_VERSION,
          POINT_VERSION   RELEASED_POINT_VERSION,
          PATCH_VERSION   RELEASED_PATCH_VERSION,
          MAINT_NUM       RELEASED_MAINT_NUM,
          null            RELEASED_SCRIPT_PATH,
          null            RELEASED_SCRIPT_NAME,
          SCRIPT_REVISION RELEASED_SCRIPT_REVISION,
          null            RELEASED_DATE_APPLIED,
          null            RELEASED_OS_USER,
          null            RELEASED_TERMINAL,
          null            RELEASED_SERVICE_NAME,
          null            RELEASED_USER_ID,
          null            RELEASED_TIME_STAMP,
          null            RELEASED_MODULE,
          0               RELEASED_NOT_NEEDED
     from PP_SCHEMA_CHANGE_LOG,
           (select max(ID) max_id,
                  lower(SCRIPT_PATH) max_script_path,
                  SCRIPT_NAME max_script_name
             from PP_SCHEMA_CHANGE_LOG
            where MAJOR_VERSION || '.' || MINOR_VERSION || '.' || POINT_VERSION || '.' || PATCH_VERSION 
         			IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
            group by lower(SCRIPT_PATH),
                  SCRIPT_NAME) T3
    where MAJOR_VERSION || '.' || MINOR_VERSION || '.' || POINT_VERSION || '.' || PATCH_VERSION 
	                IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
      and ID not in (select ID
                       from PP_SCHEMA_UPGRADE T1, PP_SCHEMA_CHANGE_LOG T2
                      where T1.BASE_ID = T2.ID
                        and MAJOR_VERSION || '.' || MINOR_VERSION || '.' || POINT_VERSION || '.' || PATCH_VERSION 
					      IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0') )
      and T3.max_id = ID
    order by MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, DATE_APPLIED;
    

-- Fill in PP_SCHEMA_UPGRADE MODULE column for with PACKAGE for packages ( we cannot tell what module the package is for)
update PP_SCHEMA_UPGRADE
set RELEASED_MODULE = 'PACKAGE'
where substr( BASE_SCRIPT_PATH, instr( BASE_SCRIPT_PATH, '\', -1, 1 ) + 1, length( BASE_SCRIPT_PATH ) - instr( BASE_SCRIPT_PATH, '\', -1, 1 )) = 'packages'
  and to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')

-- Fill in PP_SCHEMA_UPGRADE MODULE column for Property Tax update
update PP_SCHEMA_UPGRADE T1
   set RELEASED_MODULE =
        (select 'PROPTAX'
           from PP_SCHEMA_UPGRADE T2
          where (LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_proptax\_%' escape
                 '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_proptax.%' escape
                 '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_version-updates_proptax%' escape '\')
            and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
            and BASE_MAJOR_VERSION = 2017
            and BASE_MINOR_VERSION = 4
            and BASE_POINT_VERSION = 0
            and BASE_PATCH_VERSION = 0)
 where exists
 (select 'PROPTAX'
          from PP_SCHEMA_UPGRADE T2
         where (LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_proptax\_%' escape
                '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_proptax.%' escape
                '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_version-updates_proptax%' escape '\')
           and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
           and to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                     IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0'));


-- Fill in PP_SCHEMA_UPDATE MODULE column for Provision update
update PP_SCHEMA_UPGRADE T1
   set RELEASED_MODULE =
        (select 'PROV'
           from PP_SCHEMA_UPGRADE T2
          where (LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_prov\_%' escape
                 '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_prov.%' escape
                 '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_version-updates_prov%' escape '\')
            and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
            and to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0') )
 where exists
 (select 'PROV'
          from PP_SCHEMA_UPGRADE T2
         where (LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_prov\_%' escape
                '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_prov.%' escape
                '\' or LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_version-updates_prov%' escape '\')
           and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
           and to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0') );


-- Fill in PP_SCHEMA_UPDATE MODULE column for SYSTEM
update PP_SCHEMA_UPGRADE T1
   set RELEASED_MODULE =
        (select 'SYSTEM'
           from PP_SCHEMA_UPGRADE T2
          where LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_system\_%' escape
          '\'
            and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
            and to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0') )
 where exists (select 'SYSTEM'
          from PP_SCHEMA_UPGRADE T2
         where LOWER(SUBSTR(BASE_SCRIPT_NAME, 13)) like '\_system\_%' escape
         '\'
           and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
           and to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0'));

-- Fill in PP_SCHEMA_UPDATE MODULE column for other Modules
update PP_SCHEMA_UPGRADE T1
   set RELEASED_MODULE =
        (select UPPER(SUBSTR(BASE_SCRIPT_NAME,
                             14,
                             INSTR(SUBSTR(replace(BASE_SCRIPT_NAME, '.', '_'), 14), '_') - 1)) RELEASED_MODULE
           from PP_SCHEMA_UPGRADE T2
          where to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
            and RELEASED_MODULE is null
            and BASE_SCRIPT_NAME not like '%version%'
            and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME)
 where exists (select UPPER(SUBSTR(BASE_SCRIPT_NAME,
                            14,
                            INSTR(SUBSTR(replace(BASE_SCRIPT_NAME, '.', '_'), 14), '_') - 1)) RELEASED_MODULE
          from PP_SCHEMA_UPGRADE T2
         where to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
           and RELEASED_MODULE is null
           and BASE_SCRIPT_NAME not like '%version%'
           and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME);


-- Find missing Module columns - Typically will be the version-updates sql, set that to SYS
select *
  from PP_SCHEMA_UPGRADE
 where to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
   and RELEASED_MODULE is null;

-- Update to sys the scripts that have the null module.   
update PP_SCHEMA_UPGRADE
   set RELEASED_MODULE = 'SYS'
where to_char(BASE_MAJOR_VERSION) || '.' || to_char(BASE_MINOR_VERSION) || '.' || to_char(BASE_POINT_VERSION) || '.' || to_char(BASE_PATCH_VERSION)
                    IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
   and RELEASED_MODULE is null;   

-- Update the RELEASED_SCRIPT_NAME replace START NUMBER with a number > than the last version script number.
-- It will only update where the Released Script Name is null.
update PP_SCHEMA_UPGRADE T1
   set RELEASED_SCRIPT_NAME =
        (
        select 4949 + ROW_NUMBER || '_' || to_char(MAJOR_VERSION) || '.' || to_char(MINOR_VERSION) || '.' || to_char(POINT_VERSION) || '.' || to_char(PATCH_VERSION ) || '_' || T2.BASE_SCRIPT_NAME RELEASED_SCRIPT_NAME
           from (select BASE_SCRIPT_NAME, ROWNUM ROW_NUMBER
                   from (select BASE_SCRIPT_NAME, row_number() over(partition by base_script_name order by base_id desc) as the_row
                           from PP_SCHEMA_UPGRADE
                          where to_char(MAJOR_VERSION) || '.' || to_char(MINOR_VERSION) || '.' || to_char(POINT_VERSION) || '.' || to_char(PATCH_VERSION )
                                  IN ( '2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
                          order by BASE_DATE_APPLIED)
                    where the_row = 1) T2
          where T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
)
 where exists (select BASE_SCRIPT_NAME
          from PP_SCHEMA_UPGRADE T2
         where to_char(MAJOR_VERSION) || '.' || to_char(MINOR_VERSION) || '.' || to_char(POINT_VERSION) || '.' || to_char(PATCH_VERSION )
                 IN ( '2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0')
           and T1.BASE_SCRIPT_NAME = T2.BASE_SCRIPT_NAME
           and T1.RELEASED_SCRIPT_NAME is null
           )
         and T1.RELEASED_SCRIPT_NAME is null;


-- Update RELEASED Version to the new version for all records
update PP_SCHEMA_UPGRADE
   set RELEASED_VERSION = 2018100
 where RELEASED_VERSION <> 2018100
    or RELEASED_VERSION is null;


-- Verify that that BASE_SCRIPT_NAME, RELEASED_SCRIPT_NAME look correct and RELEASED_MODULE is completly filled in.
select BASE_ID, BASE_SCRIPT_NAME, RELEASED_SCRIPT_NAME, RELEASED_MODULE
  from PP_SCHEMA_UPGRADE
 where RELEASED_MAJOR_VERSION || '.' || RELEASED_MINOR_VERSION || '.' || RELEASED_POINT_VERSION || '.' || RELEASED_PATCH_VERSION 
         IN ('2017.4.0.1', '2017.4.0.2', '2017.4.0.3', '2018.1.0.0' )
 order by RELEASED_MODULE, BASE_DATE_APPLIED;


-- Creating the Version Data file.
-- Copy the previous release load_v2017.4.0.0_version_data.sql script and run the commented out SQL "Get Base Values".
-- Replace the existing insert statements with the output from the SQL.  Save the file with the new version number.

-- Copy the insert statements generated above into the maint_<maint num>_sys_load_version_data.sql script created in
-- the first step at the top of this document.
