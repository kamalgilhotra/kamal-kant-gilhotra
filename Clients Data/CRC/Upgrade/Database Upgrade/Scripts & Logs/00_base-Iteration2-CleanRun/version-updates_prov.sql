/*
||============================================================================
|| Application: PowerPlant
|| File Name:   version-updates_prov.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   01/18/2012 Lee Quinn      Point Release
||============================================================================
*/

update PP_VERSION
   set PROVISION_VERSION = 'Version 10.3.3.0'
where PP_VERSION_ID = 1;

update tax_accrual_system_control
set control_value = '10.3.3'
where control_name = 'Provision DB Version';

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (82, 0, 10, 3, 3, 1, 0, 'C:\PlasticWks\powerplant\sql', 'version-updates_prov.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
