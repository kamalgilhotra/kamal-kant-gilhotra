
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Financial Model Extract - Fcst Depr';
  l_table_name := 'EPE_FORECAST_DEPR_FIN_MODEL';
  l_subsystem := 'budget';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := '           ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'ACCOUNT',
        3,
        0,
        1,
        '',
        'Account',
        500,
        '',
        '',
        'CHAR',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'APR',
        9,
        1,
        1,
        '',
        '4',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'AUG',
        13,
        1,
        1,
        '',
        '8',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'DEC',
        17,
        1,
        1,
        '',
        '12',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'FCST_DEPR_VERSION_ID',
        1,
        0,
        1,
        '',
        'Fcst Depr Version Id',
        300,
        'description',
        'fcst_depr_version',
        'NUMBER',
        0,
        'fcst_depr_version_id',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'FEB',
        7,
        1,
        1,
        '',
        '2',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JAN',
        6,
        1,
        1,
        '',
        '1',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JUL',
        12,
        1,
        1,
        '',
        '7',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JUN',
        11,
        1,
        1,
        '',
        '6',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'MAR',
        8,
        1,
        1,
        '',
        '3',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'MAY',
        10,
        1,
        1,
        '',
        '5',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'NOV',
        16,
        1,
        1,
        '',
        '11',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'OCT',
        15,
        1,
        1,
        '',
        '10',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'OP_SEG',
        4,
        0,
        1,
        '',
        'Op Seg',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'SEP',
        14,
        1,
        1,
        '',
        '9',
        500,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'UNIT',
        5,
        0,
        1,
        '',
        'Unit',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'YEAR',
        2,
        100,
        1,
        '',
        'Year',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        1,
        2,
        '',
        0,
        0
FROM dual;
END;
/