
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Labor Rate Details';
  l_table_name := 'EPE_LABOR_RATE_DETAILS';
  l_subsystem := 'funding project';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := '           ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'APR_RATE',
        7,
        1,
        1,
        '',
        'Apr Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'AUG_RATE',
        11,
        1,
        1,
        '',
        'Aug Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'COMPANY',
        1,
        0,
        1,
        '',
        'Company',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'DEC_RATE',
        15,
        1,
        1,
        '',
        'Dec Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'FEB_RATE',
        5,
        1,
        1,
        '',
        'Feb Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JAN_RATE',
        4,
        1,
        1,
        '',
        'Jan Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JUL_RATE',
        10,
        1,
        1,
        '',
        'Jul Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JUN_RATE',
        9,
        1,
        1,
        '',
        'Jun Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'MAR_RATE',
        6,
        1,
        1,
        '',
        'Mar Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'MAY_RATE',
        8,
        1,
        1,
        '',
        'May Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'NOV_RATE',
        14,
        1,
        1,
        '',
        'Nov Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'OCT_RATE',
        13,
        1,
        1,
        '',
        'Oct Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'RATE_TYPE',
        2,
        0,
        1,
        '',
        'Rate Type',
        600,
        'description',
        'rate_type',
        'VARCHAR2',
        0,
        'description',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'SEP_RATE',
        12,
        1,
        1,
        '',
        'Sep Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'YEAR',
        3,
        0,
        1,
        '',
        'Year',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        1,
        '',
        0,
        0
FROM dual;
END;
/