
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'FP Estimates by Budget Version';
  l_table_name := 'EPE_FP_ESTIMATE_BY_BV';
  l_subsystem := 'funding project';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := '           ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'APRIL',
        12,
        1,
        1,
        '',
        'April',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'AUGUST',
        16,
        1,
        1,
        '',
        'August',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'BUDGET_PLANT_CLASS',
        7,
        0,
        1,
        '',
        'Budget Plant Class',
        500,
        'description',
        'budget_plant_class',
        'VARCHAR2',
        0,
        'description',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'BUDGET_VERSION_ID',
        1,
        0,
        1,
        '',
        'Budget Version Id',
        300,
        'description',
        'budget_version',
        'NUMBER',
        0,
        'budget_version_id',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'COMPANY_ID',
        50,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        'company',
        'NUMBER',
        0,
        'description',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'DECEMBER',
        20,
        1,
        1,
        '',
        'December',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'DEPARTMENT',
        6,
        0,
        1,
        '',
        'Department',
        500,
        'description',
        'department',
        'VARCHAR2',
        0,
        'description',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'EST_CHG_TYPE',
        5,
        0,
        1,
        '',
        'Est Chg Type',
        500,
        'description',
        'estimate_charge_type',
        'VARCHAR2',
        0,
        'description',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'EXPENDITURE_TYPE',
        4,
        0,
        1,
        '',
        'Expenditure Type',
        500,
        'description',
        'expenditure_type',
        'VARCHAR2',
        0,
        'description',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'FEBRUARY',
        10,
        1,
        1,
        '',
        'February',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'FUTURE_DOLLARS',
        48,
        1,
        1,
        '',
        'Future Dollars',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HIST_ACTUALS',
        49,
        1,
        1,
        '',
        'Hist Actuals',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_APR',
        25,
        0,
        1,
        '',
        'Hrs Apr',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_AUG',
        29,
        0,
        1,
        '',
        'Hrs Aug',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_DEC',
        33,
        0,
        1,
        '',
        'Hrs Dec',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_FEB',
        23,
        0,
        1,
        '',
        'Hrs Feb',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_JAN',
        22,
        0,
        1,
        '',
        'Hrs Jan',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_JUL',
        28,
        0,
        1,
        '',
        'Hrs Jul',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_JUN',
        27,
        0,
        1,
        '',
        'Hrs Jun',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_MAR',
        24,
        0,
        1,
        '',
        'Hrs Mar',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_MAY',
        26,
        0,
        1,
        '',
        'Hrs May',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_NOV',
        32,
        0,
        1,
        '',
        'Hrs Nov',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_OCT',
        31,
        0,
        1,
        '',
        'Hrs Oct',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_SEP',
        30,
        0,
        1,
        '',
        'Hrs Sep',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'HRS_TOTAL',
        34,
        0,
        1,
        '',
        'Hrs Total',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JANUARY',
        9,
        1,
        1,
        '',
        'January',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JOB_TASK',
        8,
        0,
        1,
        '',
        'Job Task',
        500,
        'description',
        'job_task_list',
        'VARCHAR2',
        0,
        'description',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JULY',
        15,
        1,
        1,
        '',
        'July',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'JUNE',
        14,
        1,
        1,
        '',
        'June',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'MARCH',
        11,
        1,
        1,
        '',
        'March',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'MAY',
        13,
        1,
        1,
        '',
        'May',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'NOVEMBER',
        19,
        1,
        1,
        '',
        'November',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'OCTOBER',
        18,
        1,
        1,
        '',
        'October',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_APR',
        38,
        0,
        1,
        '',
        'Qty Apr',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_AUG',
        42,
        0,
        1,
        '',
        'Qty Aug',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_DEC',
        46,
        0,
        1,
        '',
        'Qty Dec',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_FEB',
        36,
        0,
        1,
        '',
        'Qty Feb',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_JAN',
        35,
        0,
        1,
        '',
        'Qty Jan',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_JUL',
        41,
        0,
        1,
        '',
        'Qty Jul',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_JUN',
        40,
        0,
        1,
        '',
        'Qty Jun',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_MAR',
        37,
        0,
        1,
        '',
        'Qty Mar',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_MAY',
        39,
        0,
        1,
        '',
        'Qty May',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_NOV',
        45,
        0,
        1,
        '',
        'Qty Nov',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_OCT',
        44,
        0,
        1,
        '',
        'Qty Oct',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_SEP',
        43,
        0,
        1,
        '',
        'Qty Sep',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'QTY_TOTAL',
        47,
        0,
        1,
        '',
        'Qty Total',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'SEPTEMBER',
        17,
        1,
        1,
        '',
        'September',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'TOTAL',
        21,
        1,
        1,
        '',
        'Total',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'WORK_ORDER',
        2,
        0,
        1,
        '',
        'Work Order',
        300,
        'work_order_number',
        'work_order_control',
        'VARCHAR2',
        0,
        'work_order_number',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'YEAR',
        3,
        100,
        1,
        '',
        'Year',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual;
END;
/