DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lessor ILR by Set of Books (Current Revision)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessor';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'SELECT lct.ls_currency_type_id,
           lct.description currency_type,
           c.description company,
           ls.lease_number lease_number,
           ilr.ilr_number,
           st.description ilr_status,
           lg.description lease_group,
           ig.description ilr_group,
           lse.description lessee,
           ct.description capitalization_type,
           sob.description set_of_books,
           amt.iso_code currency,
           amt.currency_display_symbol AS currency_symbol,
           amt.revision as revision,
           ilr.current_revision,
           nvl(amt.beginning_lease_receivable,0) beginning_lease_receivable,
           nvl(amt.npv_lease_payments, 0) npv_lease_payments,
           nvl(amt.npv_guaranteed_residual, 0) npv_guaranteed_residual,
           nvl(amt.npv_unguaranteed_residual, 0) npv_unguaranteed_residual,
           nvl(amt.beginning_net_investment, 0) beginning_net_investment,
           nvl(amt.cost_of_goods_sold, 0) cost_of_goods_sold,
           nvl(SUM(decode(lct.ls_currency_type_id,
                1,
                la.fair_market_value,
                la.fair_market_value_comp_curr)),0) fair_market_value,
          nvl(amt.selling_profit_loss, 0) selling_profit_loss,
          nvl(amt.initial_direct_cost, 0) initial_direct_cost,
          COUNT(la.lsr_asset_id) as number_of_assets
        FROM v_lsr_ilr_mc_calc_amounts amt
        JOIN lsr_ilr ilr
        ON (ilr.ilr_id = amt.ilr_id AND ilr.current_revision = amt.revision)
        JOIN company c
        ON (ilr.company_id = c.company_id)
        JOIN ls_lease_currency_type lct
        ON (amt.ls_cur_type = lct.ls_currency_type_id)
        JOIN lsr_lease ls
        ON (ilr.lease_id = ls.lease_id)
        JOIN lsr_lessee lse
        ON (ls.lessee_id = lse.lessee_id)
        JOIN lsr_lease_group lg
        ON (ls.lease_group_id = lg.lease_group_id)
        JOIN lsr_ilr_group ig
        ON (ilr.ilr_group_id = ig.ilr_group_id)
        JOIN lsr_ilr_options o
        ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)
        JOIN lsr_cap_type ct
        ON (o.lease_cap_type_id = ct.cap_type_id)
        JOIN ls_ilr_status st
        ON (ilr.ilr_status_id = st.ilr_status_id)
        JOIN set_of_books sob
        ON (amt.set_of_books_id = sob.set_of_books_id)
        LEFT OUTER JOIN lsr_asset la
        ON (amt.ilr_id = la.ilr_id AND amt.revision = la.revision)
       GROUP BY lct.ls_currency_type_id,
            lct.description,
            c.description,
            ls.lease_number,
            ilr.ilr_number,
            st.description,
            lg.description,
            ig.description,
            lse.description,
            ct.description,
            sob.description,
            amt.iso_code,
            amt.currency_display_symbol,
            amt.revision,
            ilr.current_revision,
            amt.beginning_lease_receivable,
            amt.npv_lease_payments,
            amt.npv_guaranteed_residual,
            amt.npv_unguaranteed_residual,
            amt.beginning_net_investment,
            amt.cost_of_goods_sold,
            amt.selling_profit_loss,
            amt.initial_direct_cost
       ORDER BY ilr_number, currency_type, set_of_books   ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'beginning_net_investment',
        16,
        1,
        1,
        '',
        'Beginning Net Investment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'capitalization_type',
        10,
        0,
        1,
        '',
        'Capitalization Type',
        300,
        'description',
        'lsr_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        2,
        0,
        1,
        '',
        'Company',
        300,
        'description',
        'company',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cost_of_goods_sold',
        17,
        1,
        1,
        '',
        'Cost of Goods Sold',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        12,
        0,
        0,
        '',
        'Currency',
        300,
        'iso_code',
        'currency',
        'VARCHAR2',
        0,
        'iso_code',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_symbol',
        22,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_type',
        1,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'fair_market_value',
        18,
        1,
        1,
        '',
        'Fair Market Value',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_group',
        8,
        0,
        1,
        '',
        'Ilr Group',
        300,
        'description',
        'lsr_ilr_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        4,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'lsr_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_status',
        6,
        0,
        1,
        '',
        'Ilr Status',
        300,
        'description',
        'ls_ilr_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_group',
        7,
        0,
        1,
        '',
        'Lease Group',
        300,
        'description',
        'lsr_lease_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        3,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'lsr_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lessee',
        9,
        0,
        1,
        '',
        'Lessee',
        300,
        'description',
        'lsr_lessee',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'npv_guaranteed_residual',
        14,
        1,
        1,
        '',
        'NPV Guaranteed Residual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'npv_lease_payments',
        13,
        1,
        1,
        '',
        'NPV Lease Payments',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'npv_unguaranteed_residual',
        15,
        1,
        1,
        '',
        'NPV Unguaranteed Residual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'number_of_assets',
        21,
        0,
        1,
        '',
        'Qty Number of Assets',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'revision',
        5,
        0,
        1,
        '',
        'Revision',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        0,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        11,
        0,
        1,
        '',
        'Set Of Books',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'selling_profit_loss',
        19,
        1,
        1,
        '',
        'Selling Profit Loss',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'initial_direct_cost',
        20,
        1,
        1,
        '',
        'Initial Direct Costs',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
;
END;
/