
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Disclosure: Income Disclosure (Annual)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessor';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'WITH date_filter 
                  AS (
                    SELECT
                      f1.filter_value start_year,
                      f2.filter_value end_year
                    FROM
                      pp_any_required_filter f1
                      CROSS JOIN pp_any_required_filter f2
                    WHERE
                      upper(f1.column_name) = ''START YEAR''
                      AND   upper(f2.column_name) = ''END YEAR''
                  ),
                  asset_amounts AS (
                    SELECT
                      la.ilr_id,
                      la.revision,
                      ct.ls_currency_type_id ls_cur_type,
                      SUM(DECODE(ct.ls_currency_type_id,1,la.fair_market_value,la.fair_market_value_comp_curr) ) fair_market_value,
                      SUM(DECODE(ct.ls_currency_type_id,1,la.carrying_cost,la.carrying_cost_comp_curr) ) carrying_cost,
                      SUM(DECODE(ct.ls_currency_type_id,1,la.unguaranteed_residual_amount,la.unguaranteed_res_amt_comp_curr) ) unguaranteed_residual_amount,
                      SUM(DECODE(ct.ls_currency_type_id,1,la.guaranteed_residual_amount,la.guaranteed_res_amt_comp_curr) ) guaranteed_residual_amount
                    FROM
                      lsr_asset la
                      CROSS JOIN ls_lease_currency_type ct
                    GROUP BY
                      la.ilr_id,
                      la.revision,
                      ct.ls_currency_type_id
                    ORDER BY
                      1,
                      2,
                      3
                  ),
                  calc_amounts AS (
                    SELECT
                      amts.ilr_id,
                      amts.revision,
                      amts.set_of_books_id,
                      amts.ls_cur_type,
                      amts.cost_of_goods_sold,
                      amts.selling_profit_loss,
                      least(amts.beginning_lease_receivable,asset_amounts.fair_market_value) revenue,
                      asset_amounts.carrying_cost,
                      asset_amounts.unguaranteed_residual_amount,
                      asset_amounts.guaranteed_residual_amount
                    FROM
                      v_lsr_ilr_mc_calc_amounts amts
                      JOIN asset_amounts ON ( amts.ilr_id = asset_amounts.ilr_id
                                              AND amts.revision = asset_amounts.revision
                                              AND amts.ls_cur_type = asset_amounts.ls_cur_type )
                  ),
                  pre_schedule AS (
                    SELECT
                      date_filter.start_year,
                      date_filter.end_year,
                      sch.ilr_id,
                      sch.revision,
                      sch.set_of_books_id,
                      fasb_cap_type.DESCRIPTION fasb_cap_type,
                      TO_CHAR(sch.month,''yyyy'') year,
                      sch.ls_cur_type,
                      sch.iso_code,
                      sch.currency_display_symbol,
                      sch.interest_income_received interest_income_received,
                      sch.principal_received principal_received,
                      sch.recognized_profit,
                      CASE MONTH
                        WHEN LAST_VALUE(month)
                            OVER (PARTITION BY sch.ilr_id, sch.revision, sch.set_of_books_id, sch.ls_cur_type, to_char(sch.MONTH, ''yyyy'') 
                                  ORDER BY MONTH ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
                          THEN sch.ending_deferred_profit
                        ELSE 0
                      END as ending_deferred_profit
                    FROM
                      v_lsr_ilr_mc_schedule sch
                      JOIN lsr_ilr_options o ON ( sch.ilr_id = o.ilr_id
                                                  AND sch.revision = o.revision )
                      JOIN lsr_fasb_type_sob fasb_sob ON ( sch.set_of_books_id = fasb_sob.set_of_books_id
                                                           AND o.lease_cap_type_id = fasb_sob.cap_type_id )
                      JOIN lsr_fasb_cap_type fasb_cap_type ON fasb_sob.fasb_cap_type_id = fasb_cap_type.fasb_cap_type_id
                      JOIN date_filter ON ( TO_CHAR(sch.month,''yyyy'') BETWEEN date_filter.start_year AND date_filter.end_year )
                  ),
                  schedule AS (
                    SELECT
                      start_year,
                      end_year,
                      ilr_id,
                      revision,
                      set_of_books_id,
                      fasb_cap_type,
                      year,
                      ls_cur_type,
                      iso_code,
                      currency_display_symbol,
                      SUM(interest_income_received) interest_income_received,
                      SUM(principal_received) principal_received,
                      SUM(recognized_profit) recognized_profit,
                      SUM(ending_deferred_profit) ending_deferred_profit,
                      ROW_NUMBER() OVER(
                        PARTITION BY ilr_id,
                        revision,
                        set_of_books_id,
                        ls_cur_type
                        ORDER BY
                          year ASC
                      ) row_num
                    FROM
                      pre_schedule
                    GROUP BY
                      start_year,
                      end_year,
                      ilr_id,
                      revision,
                      set_of_books_id,
                      fasb_cap_type,
                      YEAR,
                      ls_cur_type,
                      iso_code,
                      currency_display_symbol
                    ORDER BY
                      ilr_id,
                      revision,
                      set_of_books_id,
                      ls_cur_type,
                      row_num
                  ) SELECT
                    schedule.start_year,
                    schedule.end_year,
                    lct.description currency_type,
                    c.description company,
                    ls.lease_number lease_number,
                    ls.description lease_description,
                    ilr.ilr_number,
                    st.description ilr_status,
                    lg.description lease_group,
                    ig.description ilr_group,
                    lse.description lessee,
                    ct.description capitalization_type,
                    DECODE(o.sublease_flag,1,''Yes'',''No'') sublease,
                    DECODE(ls.pre_payment_sw,1,''Yes'',''No'') prepay,
                    nvl( (
                      SELECT
                        DECODE(SUM(nvl(vp.variable_payment_id,0) ),0,''No'',''Yes'') variable_payments
                      FROM
                        lsr_ilr_payment_term pt
                        JOIN lsr_ilr_payment_term_var_pay vp ON(vp.ilr_id = pt.ilr_id
                                                                  AND vp.revision = pt.revision
                                                                  AND vp.payment_term_id = pt.payment_term_id)
                      WHERE
                        vp.ilr_id = ilr.ilr_id
                        AND   vp.revision = ilr.current_revision
                      GROUP BY
                        pt.ilr_id,
                        pt.revision
                    ),''No'') variable_payments,
                    sob.description set_of_books,
                    schedule.fasb_cap_type,
                    schedule.YEAR,
                    to_number(DECODE(LOWER(TRIM(schedule.fasb_cap_type)),''operating'',NULL,calc_amounts.cost_of_goods_sold) ) cost_of_goods_sold,
                    to_number(DECODE(lower(trim(schedule.fasb_cap_type)),''operating'',NULL,calc_amounts.revenue) ) revenue,
                    to_number(DECODE(lower(schedule.fasb_cap_type),''operating'',NULL,calc_amounts.selling_profit_loss) ) selling_profit_loss,
                    schedule.interest_income_received,
                    schedule.principal_received,
                    schedule.interest_income_received + nvl(schedule.principal_received,0) ttl_principal_interest_income,
                    calc_amounts.carrying_cost,
                    calc_amounts.unguaranteed_residual_amount,
                    calc_amounts.guaranteed_residual_amount,
                    to_number(DECODE(lower(schedule.fasb_cap_type),''direct finance'',schedule.recognized_profit, NULL) ) recognized_profit,
                    to_number(DECODE(lower(schedule.fasb_cap_type),''direct finance'',schedule.ending_deferred_profit, NULL) ) ending_deferred_profit,
                    schedule.iso_code currency,
                    schedule.currency_display_symbol currency_symbol
                    FROM
                    schedule
                    JOIN ls_lease_currency_type lct ON ( schedule.ls_cur_type = lct.ls_currency_type_id )
                    LEFT OUTER JOIN calc_amounts ON ( schedule.ilr_id = calc_amounts.ilr_id
                                                      AND schedule.revision = calc_amounts.revision
                                                      AND schedule.set_of_books_id = calc_amounts.set_of_books_id
                                                      AND schedule.ls_cur_type = calc_amounts.ls_cur_type
                                                      AND schedule.row_num = 1 )
                    JOIN lsr_ilr ilr ON ( schedule.ilr_id = ilr.ilr_id )
                    JOIN lsr_lease ls ON ( ilr.lease_id = ls.lease_id )
                    JOIN lsr_lessee lse ON ( ls.lessee_id = lse.lessee_id )
                    JOIN lsr_lease_group lg ON ( ls.lease_group_id = lg.lease_group_id )
                    JOIN lsr_ilr_group ig ON ( ilr.ilr_group_id = ig.ilr_group_id )
                    JOIN lsr_ilr_options o ON ( ilr.ilr_id = o.ilr_id
                                                AND ilr.current_revision = o.revision )
                    JOIN lsr_cap_type ct ON ( o.lease_cap_type_id = ct.cap_type_id )
                    JOIN company c ON ( ilr.company_id = c.company_id )
                    JOIN currency cur ON ( ls.contract_currency_id = cur.currency_id )
                    JOIN ls_ilr_status st ON ( ilr.ilr_status_id = st.ilr_status_id )
                    JOIN set_of_books sob ON ( schedule.set_of_books_id = sob.set_of_books_id )
                  ORDER BY
                    ilr_number,
                    schedule.set_of_books_id,
                    schedule.year';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'capitalization_type',
        12,
        0,
        1,
        '',
        'Capitalization Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'carrying_cost',
        25,
        1,
        1,
        '',
        'Carrying Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company',
        4,
        0,
        1,
        '',
        'Company',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'cost_of_goods_sold',
        19,
        1,
        1,
        '',
        'Cost Of Goods Sold',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'currency',
        28,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_symbol',
        29,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_type',
        3,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'end_year',
        2,
        0,
        1,
        '',
        'End Year',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        1,
        1,
        '',
        1,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'fasb_cap_type',
        17,
        0,
        1,
        '',
        'Fasb Cap Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'guaranteed_residual_amount',
        27,
        1,
        1,
        '',
        'Guaranteed Residual Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'recognized_profit',
        28,
        1,
        1,
        '',
        'Recognized Profit',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual

UNION ALL

SELECT  l_id,
        'ending_deferred_profit',
        29,
        1,
        1,
        '',
        'Ending Deferred Profit',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual

UNION ALL

SELECT  l_id,
        'ilr_group',
        10,
        0,
        1,
        '',
        'Ilr Group',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_number',
        7,
        0,
        1,
        '',
        'Ilr Number',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_status',
        8,
        0,
        1,
        '',
        'Ilr Status',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_income_received',
        22,
        1,
        1,
        '',
        'Interest Income Received',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_description',
        6,
        0,
        1,
        '',
        'Lease Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_group',
        9,
        0,
        1,
        '',
        'Lease Group',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_number',
        5,
        0,
        1,
        '',
        'Lease Number',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lessee',
        11,
        0,
        1,
        '',
        'Lessee',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'prepay',
        14,
        0,
        1,
        '',
        'Prepay',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_received',
        23,
        1,
        1,
        '',
        'Principal Received',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'revenue',
        20,
        1,
        1,
        '',
        'Revenue',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'selling_profit_loss',
        21,
        1,
        1,
        '',
        'Selling Profit Loss',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'set_of_books',
        16,
        0,
        1,
        '',
        'Set Of Books',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'start_year',
        1,
        0,
        1,
        '',
        'Start Year',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        1,
        1,
        '',
        1,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'sublease',
        13,
        0,
        1,
        '',
        'Sublease',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ttl_principal_interest_income',
        24,
        1,
        1,
        '',
        'Tot Princ Int Income',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'unguaranteed_residual_amount',
        26,
        1,
        1,
        '',
        'Unguaranteed Residual Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'variable_payments',
        15,
        0,
        1,
        '',
        'Variable Payments',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'year',
        18,
        0,
        1,
        '',
        'Year',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual
;

END;
/