
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Disclosure: Lease Summary by ILR';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessor';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'WITH asset_query AS (  SELECT DISTINCT ilr_id, revision, cpr_asset_id, Count(1) OVER (PARTITION BY ilr_id, revision) ilr_asset_count,  al.long_description asset_location, al.ext_asset_location external_asset_location, bs.description asset_business_segment, ua.description utility_account, ru.description retirement_unit,  county.description county, state.long_description state, td.description tax_district  FROM lsr_asset la   left OUTER JOIN cpr_ledger cpr ON (la.cpr_asset_id = cpr.asset_id)   left OUTER JOIN asset_location al ON (cpr.asset_location_id = al.asset_location_id)   left OUTER JOIN utility_account ua ON (cpr.utility_account_id = ua.utility_account_id)   left OUTER JOIN retirement_unit ru ON (cpr.retirement_unit_id = ru.retirement_unit_id)   left OUTER JOIN county ON (al.county_id = county.county_id)   left OUTER JOIN state ON (al.state_id = state.state_id)   left OUTER JOIN tax_district td ON (al.tax_district_id = td.tax_district_id)   left OUTER JOIN business_segment bs ON (cpr.bus_segment_id = bs.bus_segment_id)  )  SELECT  c.description company,  ls.lease_number lease_number,  ls.description lease_description,  ilr.ilr_number,  st.description ilr_status,  lg.description lease_group,  ig.description ilr_group,  lse.description lessee,  cur.iso_code currency,  captype.description capitalization_type,  Decode(o.sublease_flag,1,''Yes'',''No'') sublease,  sublease.lease_number sublease_lease_number,  Decode(o.interco_lease_flag,1,''Yes'',''No'') intercompany_lease,  intercompany.description intercompany_lease_company,  Decode(ls.pre_payment_sw,1,''Yes'',''No'') prepay,  Nvl((SELECT Decode(Sum(Nvl(vp.variable_payment_id,0)), 0, ''No'', ''Yes'') variable_payments  FROM lsr_ilr_payment_term pt   JOIN lsr_ilr_payment_term_var_pay vp ON (vp.ilr_id = pt.ilr_id AND vp.revision = pt.revision AND vp.payment_term_id = pt.payment_term_id)   WHERE vp.ilr_id = ilr.ilr_id AND vp.revision = ilr.current_revision  GROUP BY pt.ilr_id, pt.revision), ''No'') variable_payments,  ilr.est_in_svc_date ilr_estimated_in_service_date,  (SELECT Nvl(Sum(number_of_terms),0) FROM lsr_ilr_payment_term pt WHERE pt.ilr_id = ilr.ilr_id AND pt.revision = ilr.current_revision) number_of_payment_terms,  po.description bargain_purchse_option,  o.purchase_option_amt purchase_option_amount,  Decode(o.intent_to_purchase, 1, ''Yes'', ''No'') intent_to_purchase,  ct.description cancelable_type,  o.termination_amt termination_amount,  Nvl(asset_query.ilr_asset_count,0) number_of_assets,  asset_query.asset_location,  asset_query.external_asset_location,  asset_query.asset_business_segment,  asset_query.utility_account,  asset_query.retirement_unit,  asset_query.county,  asset_query.state,  asset_query.tax_district,  cur.currency_display_symbol currency_symbol  FROM lsr_lease ls   JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id)   JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id)   JOIN lsr_ilr ilr ON (ls.lease_id = ilr.lease_id)   JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id)   JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)   JOIN lsr_cap_type captype ON (o.lease_cap_type_id = captype.cap_type_id)   JOIN company c ON (ilr.company_id = c.company_id)   JOIN currency cur ON (ls.contract_currency_id = cur.currency_id)   JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id)   JOIN ls_purchase_option_type po ON (o.purchase_option_type_id = po.purchase_option_type_id)   JOIN ls_cancelable_type ct ON (o.cancelable_type_id = ct.cancelable_type_id)   LEFT OUTER JOIN asset_query ON (ilr.ilr_id = asset_query.ilr_id AND ilr.current_revision = asset_query.revision)   LEFT OUTER JOIN ls_lease sublease ON (o.sublease_id = sublease.lease_id)   LEFT OUTER JOIN company intercompany ON (o.interco_lease_company = intercompany.company_id)  ORDER BY ilr_number          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'asset_business_segment',
        29,
        0,
        1,
        '',
        'Asset Business Segment',
        300,
        'description',
        'business_segment',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'asset_location',
        27,
        0,
        1,
        '',
        'Asset Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'bargain_purchse_option',
        19,
        0,
        1,
        '',
        'Bargain Purchse Option',
        300,
        'description',
        'ls_purchase_option_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cancelable_type',
        24,
        0,
        1,
        '',
        'Cancelable Type',
        300,
        'description',
        'ls_cancelable_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'capitalization_type',
        10,
        0,
        1,
        '',
        'Capitalization Type',
        300,
        'description',
        'lsr_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        1,
        0,
        1,
        '',
        'Company',
        300,
        'description',
        'company',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'county',
        32,
        0,
        1,
        '',
        'County',
        300,
        'description',
        'county',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        9,
        0,
        0,
        '',
        'Currency',
        300,
        'iso_code',
        'currency',
        'VARCHAR2',
        0,
        'iso_code',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_symbol',
        35,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'external_asset_location',
        28,
        0,
        1,
        '',
        'External Asset Location',
        300,
        'ext_asset_location',
        'asset_location',
        'VARCHAR2',
        0,
        'ext_asset_location',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_estimated_in_service_date',
        17,
        0,
        1,
        '',
        'Ilr Estimated In Service Date',
        300,
        '',
        '',
        'DATE',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_group',
        7,
        0,
        1,
        '',
        'Ilr Group',
        300,
        'description',
        'lsr_ilr_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        4,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'lsr_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_status',
        5,
        0,
        1,
        '',
        'Ilr Status',
        300,
        'description',
        'ls_ilr_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'intent_to_purchase',
        21,
        0,
        1,
        '',
        'Intent To Purchase',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'intercompany_lease',
        13,
        0,
        1,
        '',
        'Intercompany Lease',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'intercompany_lease_company',
        14,
        0,
        1,
        '',
        'Intercompany Lease Company',
        300,
        'description',
        'company',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_description',
        3,
        0,
        1,
        '',
        'Lease Description',
        300,
        'description',
        'lsr_lease',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_group',
        6,
        0,
        1,
        '',
        'Lease Group',
        300,
        'description',
        'lsr_lease_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        2,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'lsr_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lessee',
        8,
        0,
        1,
        '',
        'Lessee',
        300,
        'description',
        'lsr_lessee',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'number_of_assets',
        26,
        0,
        1,
        '',
        'Number Of Assets',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'number_of_payment_terms',
        18,
        0,
        1,
        '',
        'Number Of Payment Terms',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'prepay',
        15,
        0,
        1,
        '',
        'Prepay',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'purchase_option_amount',
        20,
        1,
        1,
        '',
        'Purchase Option Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retirement_unit',
        31,
        0,
        1,
        '',
        'Retirement Unit',
        300,
        'description',
        'retirement_unit',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'state',
        33,
        0,
        1,
        '',
        'State',
        300,
        'long_description',
        'state',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sublease',
        11,
        0,
        1,
        '',
        'Sublease',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sublease_lease_number',
        12,
        0,
        1,
        '',
        'Sublease Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_district',
        34,
        0,
        1,
        '',
        'Tax District',
        300,
        'description',
        'tax_district',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'termination_amount',
        25,
        1,
        1,
        '',
        'Termination Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_account',
        30,
        0,
        1,
        '',
        'Utility Account',
        300,
        'description',
        'utility_account',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'variable_payments',
        16,
        0,
        1,
        '',
        'Variable Payments',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/