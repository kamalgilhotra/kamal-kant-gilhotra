
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Disclosure: Variable Lease Payments';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessor';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'SELECT  (SELECT filter_value FROM pp_any_required_filter WHERE  Upper(column_name)= ''START MONTHNUM'') AS start_monthnum,  (SELECT filter_value FROM pp_any_required_filter WHERE  Upper(column_name)= ''END MONTHNUM'') AS end_monthnum,  lct.description currency_type,  c.description company,  ls.lease_number lease_number,  ilr.ilr_number,  st.description ilr_status,  lg.description lease_group,  ig.description ilr_group,  lse.description lessee,  ct.description capitalization_type,  sob.description set_of_books,  To_Char(schedule.MONTH, ''yyyymm'') month_number,  schedule.iso_code currency,  buckets.bucket_name contingent_bucket,  Nvl(vp_form.description, ''Amount'') variable_payment,  Nvl(vp_form.payment_formula, ''N/A'') payment_formula,  Decode(buckets.bucket_number,        1,contingent_paid1,        2,contingent_paid2,        3,contingent_paid3,        4,contingent_paid4,        5,contingent_paid5,        6,contingent_paid6,        7,contingent_paid7,        8,contingent_paid8,        9,contingent_paid9,        10,contingent_paid10,        0) contingent_amount_received,  currency_display_symbol AS currency_symbol  FROM v_lsr_ilr_mc_schedule schedule   CROSS JOIN lsr_receivable_bucket_admin buckets   left OUTER JOIN lsr_ilr_payment_term_var_pay vp ON (vp.ilr_id = schedule.ilr_id AND vp.revision = schedule.revision AND vp.bucket_number = buckets.bucket_number)   left OUTER JOIN lsr_variable_payment vp_form ON (vp.variable_payment_id = vp_form.variable_payment_id)   JOIN company c ON (schedule.company_id = c.company_id)   JOIN lsr_ilr ilr ON (schedule.ilr_id = ilr.ilr_id AND schedule.revision = ilr.current_revision)   JOIN ls_lease_currency_type lct ON (schedule.ls_cur_type = lct.ls_currency_type_id)   JOIN lsr_lease ls ON (ilr.lease_id = ls.lease_id)   JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id)   JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id)   JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id)   JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)   JOIN lsr_cap_type ct ON (o.lease_cap_type_id = ct.cap_type_id)   JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id)   JOIN set_of_books sob ON (schedule.set_of_books_id = sob.set_of_books_id)  where buckets.receivable_type = ''Contingent''  AND to_char(schedule.month, ''yyyymm'') >=          (                SELECT filter_value                FROM   pp_any_required_filter                WHERE  upper(column_name)= ''START MONTHNUM'')  and to_char(schedule.month, ''yyyymm'') <=          (                SELECT filter_value                FROM   pp_any_required_filter                WHERE  upper(column_name)= ''END MONTHNUM'')  AND Decode(buckets.bucket_number,        1,contingent_paid1,        2,contingent_paid2,        3,contingent_paid3,        4,contingent_paid4,        5,contingent_paid5,        6,contingent_paid6,        7,contingent_paid7,        8,contingent_paid8,        9,contingent_paid9,        10,contingent_paid10,        0) <> 0  ORDER BY ilr_number, currency_type, set_of_books, MONTH, contingent_bucket          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'capitalization_type',
        11,
        0,
        1,
        '',
        'Capitalization Type',
        300,
        'description',
        'lsr_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        4,
        0,
        1,
        '',
        'Company',
        300,
        'description',
        'company',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_amount_received',
        18,
        1,
        1,
        '',
        'Contingent Amount Received',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_bucket',
        15,
        0,
        1,
        '',
        'Contingent Bucket',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        14,
        0,
        0,
        '',
        'Currency',
        300,
        'iso_code',
        'currency',
        'VARCHAR2',
        0,
        'iso_code',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_symbol',
        19,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_type',
        3,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'end_monthnum',
        2,
        0,
        1,
        '',
        'End Monthnum',
        300,
        'month_number',
        'pp_calendar',
        'VARCHAR2',
        0,
        'month_number',
        1,
        1,
        '',
        1,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_group',
        9,
        0,
        1,
        '',
        'Ilr Group',
        300,
        'description',
        'lsr_ilr_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        6,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'lsr_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_status',
        7,
        0,
        1,
        '',
        'Ilr Status',
        300,
        'description',
        'ls_ilr_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_group',
        8,
        0,
        1,
        '',
        'Lease Group',
        300,
        'description',
        'lsr_lease_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        5,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'lsr_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lessee',
        10,
        0,
        1,
        '',
        'Lessee',
        300,
        'description',
        'lsr_lessee',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'month_number',
        13,
        0,
        1,
        '',
        'Month Number',
        300,
        'month_number',
        'pp_calendar',
        'VARCHAR2',
        0,
        'month_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_formula',
        17,
        0,
        1,
        '',
        'Payment Formula',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        12,
        0,
        1,
        '',
        'Set Of Books',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'start_monthnum',
        1,
        0,
        1,
        '',
        'Start Monthnum',
        300,
        'month_number',
        'pp_calendar',
        'VARCHAR2',
        0,
        'month_number',
        1,
        1,
        '',
        1,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'variable_payment',
        16,
        0,
        1,
        '',
        'Variable Payment',
        300,
        'description',
        'lsr_variable_payment',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/