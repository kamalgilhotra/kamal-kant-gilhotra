DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lessor Schedule by ILR';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessor';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'SELECT lct.description currency_type, c.description company, ls.lease_number lease_number, ilr.ilr_number, st.description ' ||
	'  ilr_status, lg.description lease_group, ig.description ilr_group, lse.description lessee, ct.description capitalization_type, ' ||
	'  sob.description set_of_books, To_Char(schedule.MONTH, ''yyyymm'') month_number, schedule.iso_code currency, ' ||
	'  Nvl(schedule.principal_received,0) principal_received, schedule.interest_income_received, Nvl(schedule.principal_accrued,0) principal_accrued, ' ||
	'  schedule.interest_income_accrued, schedule.deferred_rent, schedule.accrued_rent, schedule.receivable_remeasurement, schedule.unguaran_residual_remeasure, ' ||
	'  schedule.beg_receivable, schedule.end_receivable, schedule.beg_long_term_receivable, schedule.end_long_term_receivable, ' ||
	'  schedule.lt_receivable_remeasurement, nvl(schedule.beginning_remaining_payments, 0) as beginning_remaining_payments, ' ||
	'  nvl(schedule.ending_remaining_payments, 0) as ending_remaining_payments, schedule.initial_direct_cost, schedule.beg_unguaranteed_residual, ' || 
	'  schedule.interest_unguaranteed_residual, schedule.ending_unguaranteed_residual, schedule.beg_net_investment, ' || 
	'  schedule.interest_net_investment, schedule.ending_net_investment, nvl(schedule.net_investment_remeasurement, 0) as net_investment_remeasurement, ' ||
	'  schedule.beginning_deferred_profit, schedule.recognized_profit, schedule.ending_deferred_profit, nvl(schedule.deferred_profit_remeasurement, 0) as deferred_profit_remeasurement, ' ||
	'  schedule.beginning_lt_deferred_profit, schedule.ending_lt_deferred_profit, nvl(schedule.lt_deferred_profit_remeasure, 0) lt_deferred_profit_remeasure, ' || 
	'  schedule.contingent_paid1+schedule.contingent_paid2+schedule.contingent_paid3+schedule.contingent_paid4+schedule.contingent_paid5+ ' ||
	'  schedule.contingent_paid6+schedule.contingent_paid7+schedule.contingent_paid8+schedule.contingent_paid9+schedule.contingent_paid10 contingent_received, ' || 
	'  schedule.executory_paid1+schedule.executory_paid2+schedule.executory_paid3+schedule.executory_paid4+schedule.executory_paid5+ ' ||
	'  schedule.executory_paid6+schedule.executory_paid7+schedule.executory_paid8+schedule.executory_paid9+schedule.executory_paid10 executory_received, ' || 
	'  currency_display_symbol AS currency_symbol ' || 
	'FROM v_lsr_ilr_mc_schedule schedule ' || 
	'JOIN company c ON (schedule.company_id = c.company_id) JOIN lsr_ilr ilr ON (schedule.ilr_id = ilr.ilr_id AND schedule.revision = ilr.current_revision) ' || 
	'JOIN ls_lease_currency_type lct ON (schedule.ls_cur_type = lct.ls_currency_type_id) ' || 
	'JOIN lsr_lease ls ON (ilr.lease_id = ls.lease_id) ' || 
	'JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id) ' || 
	'JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id) ' || 
	'JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id) ' || 
	'JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision) ' || 
	'JOIN lsr_cap_type ct ON (o.lease_cap_type_id = ct.cap_type_id) ' || 
	'JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id) ' || 
	'JOIN set_of_books sob ON (schedule.set_of_books_id = sob.set_of_books_id) ' || 
	'ORDER BY ilr_number, currency_type, set_of_books, MONTH ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'capitalization_type',
        9,
        0,
        1,
        '',
        'Capitalization Type',
        300,
        'description',
        'lsr_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        2,
        0,
        1,
        '',
        'Company',
        300,
        'description',
        'company',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_received',
        27,
        1,
        1,
        '',
        'Contingent Received',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        12,
        0,
        0,
        '',
        'Currency',
        300,
        'iso_code',
        'currency',
        'VARCHAR2',
        0,
        'iso_code',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_symbol',
        42,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_type',
        1,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_received',
        28,
        1,
        1,
        '',
        'Executory Received',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_group',
        7,
        0,
        1,
        '',
        'Ilr Group',
        300,
        'description',
        'lsr_ilr_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        4,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'lsr_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_status',
        5,
        0,
        1,
        '',
        'Ilr Status',
        300,
        'description',
        'ls_ilr_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interest_income_accrued',
        16,
        1,
        1,
        '',
        'Interest Income Accrued',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interest_income_received',
        14,
        1,
        1,
        '',
        'Interest Income Received',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_group',
        6,
        0,
        1,
        '',
        'Lease Group',
        300,
        'description',
        'lsr_lease_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        3,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'lsr_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lessee',
        8,
        0,
        1,
        '',
        'Lessee',
        300,
        'description',
        'lsr_lessee',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'month_number',
        11,
        0,
        1,
        '',
        'Month Number',
        300,
        'month_number',
        'pp_calendar',
        'VARCHAR2',
        0,
        'month_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'principal_accrued',
        15,
        1,
        1,
        '',
        'Principal Accrued',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'principal_received',
        13,
        1,
        1,
        '',
        'Principal Received',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        10,
        0,
        1,
        '',
        'Set Of Books',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'deferred_rent',
        17,
        1,
        1,
        '',
        'Deferred Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'accrued_rent',
        18,
        1,
        1,
        '',
        'Accrued Rent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beg_receivable',
        19,
        1,
        1,
        '',
        'Beginning Receivable',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'end_receivable',
        20,
        1,
        1,
        '',
        'Ending Receivable',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'receivable_remeasurement',
        21,
        1,
        1,
        '',
        'Receivable Remeasurement',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beg_long_term_receivable',
        22,
        1,
        1,
        '',
        'Beginning LT Receivable',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'end_long_term_receivable',
        23,
        1,
        1,
        '',
        'Ending LT Receivable',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lt_receivable_remeasurement',
        24,
        1,
        1,
        '',
        'LT Receivable Remeasurement',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beginning_remaining_payments',
        25,
        1,
        1,
        '',
        'Beginning Remaining Payments',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ending_remaining_payments',
        26,
        1,
        1,
        '',
        'Ending Remaining Payments',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'initial_direct_cost',
        27,
        1,
        1,
        '',
        'Initial Direct Costs',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beg_unguaranteed_residual',
        30,
        1,
        1,
        '',
        'Begin Unguaranteed Residual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interest_unguaranteed_residual',
        31,
        1,
        1,
        '',
        'Interest Unguaranteed Residual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ending_unguaranteed_residual',
        32,
        1,
        1,
        '',
        'Ending Unguaranteed Residual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'unguaran_residual_remeasure',
        33,
        1,
        1,
        '',
        'Unguaran Residual Remeasure',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beg_net_investment',
        34,
        1,
        1,
        '',
        'Beginning Net Investment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interest_net_investment',
        35,
        1,
        1,
        '',
        'Interest on Net Investment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ending_net_investment',
        36,
        1,
        1,
        '',
        'Ending Net Investment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'net_investment_remeasurement',
        37,
        1,
        1,
        '',
        'Net Investment Remeasurement',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beginning_deferred_profit',
        38,
        1,
        1,
        '',
        'Beginning Deferred Profit',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'recognized_profit',
        39,
        1,
        1,
        '',
        'Recognized Profit',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ending_deferred_profit',
        40,
        1,
        1,
        '',
        'Ending Deferred Profit',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'deferred_profit_remeasurement',
        41,
        1,
        1,
        '',
        'Deferred Profit Remeasurement',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beginning_lt_deferred_profit',
        42,
        1,
        1,
        '',
        'Beginning LT Deferred Profit',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ending_lt_deferred_profit',
        43,
        1,
        1,
        '',
        'Ending LT Deferred Profit',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lt_deferred_profit_remeasure',
        44,
        1,
        1,
        '',
        'LT Deferred Profit Remeasure',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
;

END;
/