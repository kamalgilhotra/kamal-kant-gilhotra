
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Disclosure: Undiscounted Cash Flows (Current Revision)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessor';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'WITH amounts AS (  SELECT ilr_id, revision, set_of_books_id, Nvl(YEAR, ''Total'') year, ls_cur_type, currency, currency_symbol, Sum(total_cashflow) total_cashflow  FROM (  SELECT  ilr_id,  revision,  set_of_books_id,  CASE   WHEN To_Number(To_Char(MONTH,''yyyy'')) >= year_filter.filter_value+5 THEN ''Thereafter''   ELSE To_Char(MONTH,''yyyy'')  END YEAR,  ls_cur_type,  iso_code AS currency,  currency_display_symbol AS currency_symbol,  Sum(interest_income_received + Nvl(principal_received,0) +  executory_paid1 + executory_paid2 + executory_paid3 + executory_paid4 + executory_paid5 +  executory_paid6 + executory_paid7 + executory_paid8 + executory_paid9 + executory_paid10 +  contingent_paid1 + contingent_paid2 + contingent_paid3 + contingent_paid4 + contingent_paid5 +  contingent_paid6 + contingent_paid7 + contingent_paid8 + contingent_paid9 + contingent_paid10) total_cashflow  FROM v_lsr_ilr_mc_schedule   CROSS JOIN pp_any_required_filter year_filter  WHERE Upper(year_filter.column_name) = ''START YEAR''  GROUP BY  ilr_id,  revision,  set_of_books_id,  CASE   WHEN To_Number(To_Char(MONTH,''yyyy'')) >= year_filter.filter_value+5 THEN ''Thereafter''   ELSE To_Char(MONTH,''yyyy'')  END,  ls_cur_type,  iso_code,  currency_display_symbol  ) schedule  WHERE schedule.YEAR >= (SELECT filter_value FROM pp_any_required_filter WHERE Upper(column_name) = ''START YEAR'')  GROUP BY rollup(ilr_id, revision, set_of_books_id,ls_cur_type, currency, currency_symbol, year)  HAVING ilr_id IS NOT NULL AND revision IS NOT NULL AND set_of_books_id IS NOT NULL AND ls_cur_type IS NOT NULL AND currency IS NOT NULL AND currency_symbol IS NOT NULL  )  SELECT  (SELECT filter_value start_year FROM pp_any_required_filter WHERE Upper(column_name) = ''START YEAR'') start_year,  lct.description currency_type,  c.description company,  ls.lease_number lease_number,  ilr.ilr_number,  st.description ilr_status,  lg.description lease_group,  ig.description ilr_group,  lse.description lessee,  ct.description capitalization_type,  Decode(o.sublease_flag,1,''Yes'',''No'') sublease,  Decode(o.interco_lease_flag,1,''Yes'',''No'') intercompany_lease,  Decode(ls.pre_payment_sw,1,''Yes'',''No'') prepay,  Nvl((SELECT Decode(Sum(Nvl(vp.variable_payment_id,0)), 0, ''No'', ''Yes'') variable_payments  FROM lsr_ilr_payment_term pt   JOIN lsr_ilr_payment_term_var_pay vp ON (vp.ilr_id = pt.ilr_id AND vp.revision = pt.revision AND vp.payment_term_id = pt.payment_term_id)  WHERE vp.ilr_id = ilr.ilr_id AND vp.revision = ilr.current_revision  GROUP BY pt.ilr_id, pt.revision), ''No'') variable_payments,  sob.description set_of_books,  amounts.YEAR,  amounts.currency,  amounts.total_cashflow,  amounts.currency_symbol  FROM amounts   JOIN lsr_ilr ilr ON (amounts.ilr_id = ilr.ilr_id AND amounts.revision = ilr.current_revision)   JOIN ls_lease_currency_type lct ON (amounts.ls_cur_type = lct.ls_currency_type_id)   JOIN lsr_lease ls ON (ilr.lease_id = ls.lease_id)   JOIN lsr_lessee lse ON (ls.lessee_id = lse.lessee_id)   JOIN lsr_lease_group lg ON (ls.lease_group_id = lg.lease_group_id)   JOIN lsr_ilr_group ig ON (ilr.ilr_group_id = ig.ilr_group_id)   JOIN lsr_ilr_options o ON (ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision)   JOIN lsr_cap_type ct ON (o.lease_cap_type_id = ct.cap_type_id)   JOIN company c ON (ilr.company_id = c.company_id)   JOIN currency cur ON (ls.contract_currency_id = cur.currency_id)   JOIN ls_ilr_status st ON (ilr.ilr_status_id = st.ilr_status_id)   JOIN set_of_books sob ON (amounts.set_of_books_id = sob.set_of_books_id)  ORDER BY ilr_number, currency_type, set_of_books, YEAR ASC          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'capitalization_type',
        10,
        0,
        1,
        '',
        'Capitalization Type',
        300,
        'description',
        'lsr_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        3,
        0,
        1,
        '',
        'Company',
        300,
        'description',
        'company',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        17,
        0,
        0,
        '',
        'Currency',
        300,
        'iso_code',
        'currency',
        'VARCHAR2',
        0,
        'iso_code',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_symbol',
        19,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_type',
        2,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_group',
        8,
        0,
        1,
        '',
        'Ilr Group',
        300,
        'description',
        'lsr_ilr_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        5,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'lsr_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_status',
        6,
        0,
        1,
        '',
        'Ilr Status',
        300,
        'description',
        'ls_ilr_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'intercompany_lease',
        12,
        0,
        1,
        '',
        'Intercompany Lease',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_group',
        7,
        0,
        1,
        '',
        'Lease Group',
        300,
        'description',
        'lsr_lease_group',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        4,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'lsr_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lessee',
        9,
        0,
        1,
        '',
        'Lessee',
        300,
        'description',
        'lsr_lessee',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'prepay',
        13,
        0,
        1,
        '',
        'Prepay',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        15,
        0,
        1,
        '',
        'Set Of Books',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'start_year',
        1,
        0,
        1,
        '',
        'Start Year',
        300,
        'year',
        'pp_table_years',
        'VARCHAR2',
        0,
        'year',
        1,
        1,
        '',
        1,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sublease',
        11,
        0,
        1,
        '',
        'Sublease',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'total_cashflow',
        18,
        1,
        1,
        '',
        'Total Cashflow',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'variable_payments',
        14,
        0,
        1,
        '',
        'Variable Payments',
        300,
        'initcap(description)',
        'yes_no',
        'VARCHAR2',
        0,
        'initcap(description)',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'year',
        16,
        0,
        1,
        '',
        'Year',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/