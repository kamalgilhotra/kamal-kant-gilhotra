
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Account Summary Query';
  l_table_name := 'Dynamic View';
  l_subsystem := 'cpr';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'SELECT report_time.start_month start_month,
       report_time.end_month end_month,
       set_of_books.description set_of_books,
       company.description company,
       business_segment.description business_segment,
       gl_account.description gl_account,
       func_class.description func_class,
       func_class.func_class_sort_id fc_sortid,
       utility_account.description utility_Account,
       major_location.description major_location,
       major_location.state_id state,
       sum(decode(to_char(account_summary.gl_posting_mo_yr, ''yyyymm''), report_time.start_month, nvl(account_summary.beginning_balance, 0), 0)) begin_balance,
        sum(nvl(account_summary.additions, 0)) additions,
       sum(nvl(account_summary.retirements, 0)) retirements,
       sum(nvl(account_summary.transfers_in, 0) + nvl(account_summary.transfers_out, 0)) transfers,
       sum(nvl(account_summary.adjustments, 0)) adjustments,
       sum(decode(to_char(account_summary.gl_posting_mo_yr, ''yyyymm''),  report_time.end_month, nvl(account_summary.ending_balance, 0), 0)) end_balance
  FROM account_summary,
       utility_account,
       (select a.filter_value start_month, b.filter_value end_month
          from pp_any_required_filter a, pp_any_required_filter b
         where a.column_name = ''START MONTH''
           and b.column_name = ''END MONTH''
        ) report_time,
                company,
                func_class,
                business_segment,
                set_of_books ,
                gl_account,
        major_location
  WHERE account_summary.utility_account_id = utility_account.utility_account_id 
    and account_summary.bus_segment_id = utility_account.bus_segment_id
    and account_summary.company_id = company.company_id
    and account_summary.bus_segment_id = business_segment.bus_segment_id
    and account_summary.set_of_books_id = set_of_books.set_of_books_id
    and utility_account.func_class_id = func_class.func_class_id
    and account_summary.gl_account_id = gl_account.gl_account_id
    and account_summary.major_location_id = major_location.major_location_id
    and to_char(account_summary.gl_posting_mo_yr, ''yyyymm'') <= report_time.end_month
    and to_char(account_summary.gl_posting_mo_yr, ''yyyymm'') >= report_time.start_month
  GROUP BY report_time.start_month,
           report_time.end_month,
           set_of_books.description,
           company.description,
           business_segment.description,
           gl_account.description,
           func_class.description,
           func_class.func_class_sort_id,
           utility_account.description,
           major_location.description, 
           major_location.state_id          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'additions',
        13,
        1,
        1,
        '',
        'Additions',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'adjustments',
        16,
        1,
        1,
        '',
        'Adjustments',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'begin_balance',
        12,
        1,
        1,
        '',
        'Begin Balance',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'business_segment',
        5,
        0,
        1,
        '',
        'Business Segment',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        4,
        0,
        1,
        '',
        'Company',
        400,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'end_balance',
        17,
        1,
        1,
        '',
        'End Balance',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'end_month',
        2,
        0,
        1,
        '',
        'End Month',
        300,
        'month_number',
        'cr_month_number',
        'VARCHAR2',
        0,
        'month_number',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'fc_sortid',
        8,
        0,
        1,
        '',
        'Fc Sortid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'func_class',
        7,
        0,
        1,
        '',
        'Func Class',
        700,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'gl_account',
        6,
        0,
        1,
        '',
        'Gl Account',
        700,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'major_location',
        10,
        0,
        1,
        '',
        'Major Location',
        700,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retirements',
        14,
        1,
        1,
        '',
        'Retirements',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        3,
        0,
        1,
        '',
        'Set Of Books',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        0,
        'description',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'start_month',
        1,
        0,
        1,
        '',
        'Start Month',
        300,
        'month_number',
        'cr_month_number',
        'VARCHAR2',
        0,
        'month_number',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'state',
        11,
        null,
        1,
        '',
        'State',
        300,
        '',
        '',
        'VARCHAR2',
        null,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'transfers',
        15,
        1,
        1,
        '',
        'Transfers',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_account',
        9,
        0,
        1,
        '',
        'Utility Account',
        700,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/