
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Detail CPR Rollforward Query (Slow)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'cpr';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'SELECT 
 report_time.start_month start_month,
 report_time.end_month end_month,
 set_of_books.description set_of_books,
 company.description company,
 business_segment.description business_segment,
 gl_account.description gl_account,
 func_class.description func_class,
 func_class.func_class_sort_id fc_sortid,
 utility_account.description utility_account,
major_location.description major_location,
major_location.state_id state,
al.long_description asset_location,
al.ext_asset_location ext_asset_location,
retirement_unit.description retirement_unit,
to_number(to_char(eng_in_service_year,''yyyy'')) vintage,
 SUM(case 
 when a.month_number < report_time.start_month 
 then NVL(basis_1 * basis_1_indicator,0) + NVL(basis_2 * basis_2_indicator,0) + NVL(basis_3 * basis_3_indicator,0) + NVL(basis_4 * basis_4_indicator,0) + NVL(basis_5 * basis_5_indicator,0) + NVL(basis_6 * basis_6_indicator,0) + NVL(basis_7 * basis_7_indicator,0) + NVL(basis_8 * basis_8_indicator,0) + NVL(basis_9 * basis_9_indicator,0) + NVL(basis_10 * basis_10_indicator,0) + NVL(basis_11 * basis_11_indicator,0) + NVL(basis_12 * basis_12_indicator,0) + NVL(basis_13 * basis_13_indicator,0) + NVL(basis_14 * basis_14_indicator,0) + NVL(basis_15 * basis_15_indicator,0) + NVL(basis_16 * basis_16_indicator,0) + NVL(basis_17 * basis_17_indicator,0) + NVL(basis_18 * basis_18_indicator,0) + NVL(basis_19 * basis_19_indicator,0) + NVL(basis_20 * basis_20_indicator,0) + NVL(basis_21 * basis_21_indicator,0) + NVL(basis_22 * basis_22_indicator,0) + NVL(basis_23 * basis_23_indicator,0) 
 else 0
 end) begin_balance,
 SUM(case 
 when a.month_number >= report_time.start_month and a.month_number <= report_time.end_month
 then DECODE(a.ferc_activity_code,1, NVL(basis_1 * basis_1_indicator,0) + NVL(basis_2 * basis_2_indicator,0) + NVL(basis_3 * basis_3_indicator,0) + NVL(basis_4 * basis_4_indicator,0) + NVL(basis_5 * basis_5_indicator,0) + NVL(basis_6 * basis_6_indicator,0) + NVL(basis_7 * basis_7_indicator,0) + NVL(basis_8 * basis_8_indicator,0) + NVL(basis_9 * basis_9_indicator,0) + NVL(basis_10 * basis_10_indicator,0) + NVL(basis_11 * basis_11_indicator,0) + NVL(basis_12 * basis_12_indicator,0) + NVL(basis_13 * basis_13_indicator,0) + NVL(basis_14 * basis_14_indicator,0) + NVL(basis_15 * basis_15_indicator,0) + NVL(basis_16 * basis_16_indicator,0) + NVL(basis_17 * basis_17_indicator,0) + NVL(basis_18 * basis_18_indicator,0) + NVL(basis_19 * basis_19_indicator,0) + NVL(basis_20 * basis_20_indicator,0) + NVL(basis_21 * basis_21_indicator,0) + NVL(basis_22 * basis_22_indicator,0) + NVL(basis_23 * basis_23_indicator,0) , 0)
 else 0
 end) additions,
 SUM(case 
 when a.month_number >= report_time.start_month and a.month_number <= report_time.end_month
 then DECODE(a.ferc_activity_code,2, NVL(basis_1 * basis_1_indicator,0) + NVL(basis_2 * basis_2_indicator,0) + NVL(basis_3 * basis_3_indicator,0) + NVL(basis_4 * basis_4_indicator,0) + NVL(basis_5 * basis_5_indicator,0) + NVL(basis_6 * basis_6_indicator,0) + NVL(basis_7 * basis_7_indicator,0) + NVL(basis_8 * basis_8_indicator,0) + NVL(basis_9 * basis_9_indicator,0) + NVL(basis_10 * basis_10_indicator,0) + NVL(basis_11 * basis_11_indicator,0) + NVL(basis_12 * basis_12_indicator,0) + NVL(basis_13 * basis_13_indicator,0) + NVL(basis_14 * basis_14_indicator,0) + NVL(basis_15 * basis_15_indicator,0) + NVL(basis_16 * basis_16_indicator,0) + NVL(basis_17 * basis_17_indicator,0) + NVL(basis_18 * basis_18_indicator,0) + NVL(basis_19 * basis_19_indicator,0) + NVL(basis_20 * basis_20_indicator,0) + NVL(basis_21 * basis_21_indicator,0) + NVL(basis_22 * basis_22_indicator,0) + NVL(basis_23 * basis_23_indicator,0) , 0)
 else 0
 end) retirements,
 SUM(case 
 when a.month_number >= report_time.start_month and a.month_number <= report_time.end_month
 then DECODE(a.ferc_activity_code,4,  NVL(basis_1 * basis_1_indicator,0) + NVL(basis_2 * basis_2_indicator,0) + NVL(basis_3 * basis_3_indicator,0) + NVL(basis_4 * basis_4_indicator,0) + NVL(basis_5 * basis_5_indicator,0) + NVL(basis_6 * basis_6_indicator,0) + NVL(basis_7 * basis_7_indicator,0) + NVL(basis_8 * basis_8_indicator,0) + NVL(basis_9 * basis_9_indicator,0) + NVL(basis_10 * basis_10_indicator,0) + NVL(basis_11 * basis_11_indicator,0) + NVL(basis_12 * basis_12_indicator,0) + NVL(basis_13 * basis_13_indicator,0) + NVL(basis_14 * basis_14_indicator,0) + NVL(basis_15 * basis_15_indicator,0) + NVL(basis_16 * basis_16_indicator,0) + NVL(basis_17 * basis_17_indicator,0) + NVL(basis_18 * basis_18_indicator,0) + NVL(basis_19 * basis_19_indicator,0) + NVL(basis_20 * basis_20_indicator,0) + NVL(basis_21 * basis_21_indicator,0) + NVL(basis_22 * basis_22_indicator,0) + NVL(basis_23 * basis_23_indicator,0) , 0)
 else 0
 end) transfers,
 SUM(case 
 when a.month_number >= report_time.start_month and a.month_number <= report_time.end_month
 then DECODE(a.ferc_activity_code,3, NVL(basis_1 * basis_1_indicator,0) + NVL(basis_2 * basis_2_indicator,0) + NVL(basis_3 * basis_3_indicator,0) + NVL(basis_4 * basis_4_indicator,0) + NVL(basis_5 * basis_5_indicator,0) + NVL(basis_6 * basis_6_indicator,0) + NVL(basis_7 * basis_7_indicator,0) + NVL(basis_8 * basis_8_indicator,0) + NVL(basis_9 * basis_9_indicator,0) + NVL(basis_10 * basis_10_indicator,0) + NVL(basis_11 * basis_11_indicator,0) + NVL(basis_12 * basis_12_indicator,0) + NVL(basis_13 * basis_13_indicator,0) + NVL(basis_14 * basis_14_indicator,0) + NVL(basis_15 * basis_15_indicator,0) + NVL(basis_16 * basis_16_indicator,0) + NVL(basis_17 * basis_17_indicator,0) + NVL(basis_18 * basis_18_indicator,0) + NVL(basis_19 * basis_19_indicator,0) + NVL(basis_20 * basis_20_indicator,0) + NVL(basis_21 * basis_21_indicator,0) + NVL(basis_22 * basis_22_indicator,0) + NVL(basis_23 * basis_23_indicator,0) , 0)
 else 0
 end) adjustments,
 SUM(case 
 when a.month_number <= report_time.end_month
 then NVL(basis_1 * basis_1_indicator,0) + NVL(basis_2 * basis_2_indicator,0) + NVL(basis_3 * basis_3_indicator,0) + NVL(basis_4 * basis_4_indicator,0) + NVL(basis_5 * basis_5_indicator,0) + NVL(basis_6 * basis_6_indicator,0) + NVL(basis_7 * basis_7_indicator,0) + NVL(basis_8 * basis_8_indicator,0) + NVL(basis_9 * basis_9_indicator,0) + NVL(basis_10 * basis_10_indicator,0) + NVL(basis_11 * basis_11_indicator,0) + NVL(basis_12 * basis_12_indicator,0) + NVL(basis_13 * basis_13_indicator,0) + NVL(basis_14 * basis_14_indicator,0) + NVL(basis_15 * basis_15_indicator,0) + NVL(basis_16 * basis_16_indicator,0) + NVL(basis_17 * basis_17_indicator,0) + NVL(basis_18 * basis_18_indicator,0) + NVL(basis_19 * basis_19_indicator,0) + NVL(basis_20 * basis_20_indicator,0) + NVL(basis_21 * basis_21_indicator,0) + NVL(basis_22 * basis_22_indicator,0) + NVL(basis_23 * basis_23_indicator,0) 
 else 0
 end) end_balance
FROM cpr_ledger l     ,
 cpr_activity a   ,
 cpr_act_basis b  ,
 asset_location al,
 set_of_books,
 utility_account,
 (select a.filter_value start_month, b.filter_value end_month
  from pp_any_required_filter a, pp_any_required_filter b
  where a.column_name = ''START MONTH''
  and b.column_name = ''END MONTH''
  ) report_time,
  company,
  func_class,
  business_segment,
  gl_account,
  major_location ,
  retirement_unit
WHERE   l.asset_id          = a.asset_id
    AND l.asset_location_id = al.asset_location_id
    AND a.asset_id          = b.asset_id
    AND a.asset_activity_id = b.asset_activity_id
    and  l.utility_account_id = utility_account.utility_account_id   
    and l.bus_segment_id = utility_account.bus_segment_id   
    and l.company_id = company.company_id   
    and l.bus_segment_id = business_segment.b us_segment_id   
    and set_of_books.description  in (select filter_value from pp_any_required_filter where column_name  = ''SET OF BOOKS'')
    and utility_account.func_class_id = func_class.func_class_id  
    and l.gl_account_id = gl_account.gl_account_id   
    and al.major_location_id = major_location.major_location_id   
    and a.month_number <=    report_time.end_month
    and l.retirement_unit_id = retirement_Unit.retirement_unit_id
  
GROUP BY  
 report_time.start_month,
 report_time.end_month,
 company.description,
 business_segment.description,
 gl_account.description,
 func_class.description,
 func_class.func_class_sort_id,
 utility_account.description,
 major_location.description   ,
 set_of_books.description,
major_location.state_id ,
retirement_unit.description ,
to_number(to_char(eng_in_service_year,''yyyy'')),
al.long_description ,
al.ext_asset_location       ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'additions',
        17,
        1,
        1,
        '',
        'Additions',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'adjustments',
        20,
        1,
        1,
        '',
        'Adjustments',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'asset_location',
        12,
        0,
        1,
        '',
        'Asset Location',
        800,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'begin_balance',
        16,
        1,
        1,
        '',
        'Begin Balance',
        800,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'business_segment',
        5,
        0,
        1,
        '',
        'Business Segment',
        500,
        'description',
        'business_segment',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        4,
        0,
        1,
        '',
        'Company',
        500,
        'description',
        'company',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'end_balance',
        21,
        1,
        1,
        '',
        'End Balance',
        800,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'end_month',
        2,
        0,
        1,
        '',
        'End Month',
        300,
        'month_number',
        'cr_month_number',
        'VARCHAR2',
        0,
        'month_number',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ext_asset_location',
        13,
        0,
        1,
        '',
        'Ext Asset Location',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'fc_sortid',
        8,
        0,
        1,
        '',
        'Fc Sortid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'func_class',
        7,
        0,
        1,
        '',
        'Func Class',
        500,
        'description',
        'func_class',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'gl_account',
        6,
        0,
        1,
        '',
        'Gl Account',
        700,
        'description',
        'gl_account',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'major_location',
        10,
        0,
        1,
        '',
        'Major Location',
        700,
        'description',
        'major_location',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retirement_unit',
        14,
        0,
        1,
        '',
        'Retirement Unit',
        700,
        'description',
        'retirement_unit',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retirements',
        18,
        1,
        1,
        '',
        'Retirements',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        3,
        0,
        1,
        '',
        'Set Of Books',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        0,
        'description',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'start_month',
        1,
        0,
        1,
        '',
        'Start Month',
        300,
        'month_number',
        'cr_month_number',
        'VARCHAR2',
        0,
        'month_number',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'state',
        11,
        0,
        1,
        '',
        'State',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'transfers',
        19,
        1,
        1,
        '',
        'Transfers',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_account',
        9,
        0,
        1,
        '',
        'Utility Account',
        700,
        'description',
        'utility_account',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'vintage',
        15,
        0,
        1,
        '',
        'Vintage',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/