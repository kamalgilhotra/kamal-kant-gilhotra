
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'WOs-Ret Est with repl flag no';
  l_table_name := 'Dynamic View';
  l_subsystem := 'Tax Repairs';
  l_query_type := 'system';
  l_is_multicurrency := null;
  
  l_query_sql := 'SELECT A.PROJECT_NUMBER, A.PROJECT, TO_CHAR(EFFECTIVE_ACCOUNTING_MONTH, ''MM/YYYY'') EFFECTIVE_ACCOUNTING_MONTH, TAX_UNIT, TAX_UNIT_SOURCE, TAX_LOCATION, REPLACEMENT_FLAG, REPLACEMENT_SOURCE, CURRENT_TAX_STATUS, TEST_TAX_STATUS, REVIEWED_DATE, REVIEW_USER, EXTERNAL_REVIEWED_DATE, EXTERNAL_REVIEW_USER, OVERRIDE_NOTE, REPLACEMENT_COST, TOTAL_ADDITION_ESTIMATES, TOTAL_RETIREMENT_ESTIMATES, ACTUAL_ADDITION_CHARGES, ACTUAL_TOTAL_CHARGES FROM (SELECT WOC.WORK_ORDER_NUMBER PROJECT_NUMBER, WOC.DESCRIPTION PROJECT, WOTS.EFFECTIVE_ACCOUNTING_MONTH EFFECTIVE_ACCOUNTING_MONTH, RUC.DESCRIPTION TAX_UNIT, WOTS.REPAIR_UNIT_SOURCE TAX_UNIT_SOURCE, RL.DESCRIPTION TAX_LOCATION, DECODE(WOTS.REPLACEMENT_FLAG, 0, ''NO'', 1, ''YES'') REPLACEMENT_FLAG, WOTS.REPLACEMENT_SOURCE REPLACEMENT_SOURCE, TS.DESCRIPTION CURRENT_TAX_STATUS, TS2.DESCRIPTION TEST_TAX_STATUS, WOTS.REVIEWED_DATE REVIEWED_DATE, WOTS.REVIEW_USER REVIEW_USER, WOTS.EXTERNAL_REVIEWED_DATE EXTERNAL_REVIEWED_DATE, WOTS.EXTERNAL_REVIEW_USER EXTERNAL_REVIEW_USER, WOTS.OVERRIDE_NOTE OVERRIDE_NOTE, WOTS.TEST_REPLACE_COST REPLACEMENT_COST, (SELECT SUM(AMOUNT) FROM WO_ESTIMATE A WHERE A.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND A.EXPENDITURE_TYPE_ID = 1 AND A.REVISION = (SELECT MAX(REVISION) FROM WO_ESTIMATE AA WHERE A.WORK_ORDER_ID = AA.WORK_ORDER_ID)) TOTAL_ADDITION_ESTIMATES, (SELECT SUM(AMOUNT) FROM WO_ESTIMATE A WHERE A.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND A.EXPENDITURE_TYPE_ID = 2 AND A.REVISION = (SELECT MAX(REVISION) FROM WO_ESTIMATE AA WHERE A.WORK_ORDER_ID = AA.WORK_ORDER_ID)) TOTAL_RETIREMENT_ESTIMATES, (SELECT SUM(AMOUNT) FROM CWIP_CHARGE CC, CHARGE_TYPE CT WHERE CC.WORK_ORDER_ID = WOTS.WORK_ORDER_ID AND CT.CHARGE_TYPE_ID = CC.CHARGE_TYPE_ID AND CT.EXCLUDE_FROM_TOTAL_CHARGES = 0 AND EXPENDITURE_TYPE_ID = 1) ACTUAL_ADDITION_CHARGES, (SELECT SUM(AMOUNT) FROM CWIP_CHARGE CC, CHARGE_TYPE CT WHERE CC.WORK_ORDER_ID = WOTS.WORK_ORDER_ID AND CT.CHARGE_TYPE_ID = CC.CHARGE_TYPE_ID AND CT.EXCLUDE_FROM_TOTAL_CHARGES = 0) ACTUAL_TOTAL_CHARGES FROM WORK_ORDER_TAX_STATUS WOTS, WORK_ORDER_CONTROL WOC, REPAIR_UNIT_CODE RUC, REPAIR_LOCATION RL, WO_TAX_STATUS TS, WO_TAX_STATUS TS2 WHERE RUC.REPAIR_UNIT_CODE_ID(+) = WOTS.REPAIR_UNIT_CODE_ID AND RL.REPAIR_LOCATION_ID(+) = WOTS.REPAIR_LOCATION_ID AND WOC.WORK_ORDER_ID = WOTS.WORK_ORDER_ID AND TS.TAX_STATUS_ID(+) = WOTS.TAX_STATUS_ID AND TS2.TAX_STATUS_ID(+) = WOTS.TEST_TAX_STATUS_ID AND WOTS.REPLACEMENT_FLAG = 0 AND WOTS.EFFECTIVE_ACCOUNTING_MONTH = (SELECT MAX(EFFECTIVE_ACCOUNTING_MONTH) FROM WORK_ORDER_TAX_STATUS WOTS2 WHERE WOTS.WORK_ORDER_ID = WOTS2.WORK_ORDER_ID) AND EXISTS (SELECT 1 FROM WO_ESTIMATE Z WHERE Z.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND Z.EXPENDITURE_TYPE_ID = 2 AND Z.REVISION = (SELECT MAX(REVISION) FROM WO_ESTIMATE ZZ WHERE Z.WORK_ORDER_ID = ZZ.WORK_ORDER_ID))) A, WORK_ORDER_CONTROL, COMPANY WHERE A.PROJECT_NUMBER = WORK_ORDER_CONTROL.WORK_ORDER_NUMBER  AND WORK_ORDER_CONTROL.COMPANY_ID = COMPANY.COMPANY_ID          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'actual_addition_charges',
        19,
        0,
        1,
        '',
        'Actual Addition Charges',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'actual_total_charges',
        20,
        0,
        1,
        '',
        'Actual Total Charges',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'current_tax_status',
        9,
        0,
        1,
        '',
        'Current Tax Status',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'effective_accounting_month',
        3,
        0,
        1,
        '',
        'Effective Accounting Month',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_review_user',
        14,
        0,
        1,
        '',
        'External Review User',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_reviewed_date',
        13,
        0,
        1,
        '',
        'External Reviewed Date',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'override_note',
        15,
        0,
        1,
        '',
        'Override Note',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'project',
        2,
        0,
        1,
        '',
        'Project',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'project_number',
        1,
        0,
        1,
        '',
        'Project Number',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_cost',
        16,
        1,
        1,
        '',
        'Replacement Cost',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_flag',
        7,
        0,
        1,
        '',
        'Replacement Flag',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_source',
        8,
        0,
        1,
        '',
        'Replacement Source',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'review_user',
        12,
        0,
        1,
        '',
        'Review User',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'reviewed_date',
        11,
        0,
        1,
        '',
        'Reviewed Date',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_location',
        6,
        0,
        1,
        '',
        'Tax Location',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_unit',
        4,
        0,
        1,
        '',
        'Tax Unit',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_unit_source',
        5,
        0,
        1,
        '',
        'Tax Unit Source',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'test_tax_status',
        10,
        0,
        1,
        '',
        'Test Tax Status',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'total_addition_estimates',
        17,
        0,
        1,
        '',
        'Total Addition Estimates',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'total_retirement_estimates',
        18,
        0,
        1,
        '',
        'Total Retirement Estimates',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/