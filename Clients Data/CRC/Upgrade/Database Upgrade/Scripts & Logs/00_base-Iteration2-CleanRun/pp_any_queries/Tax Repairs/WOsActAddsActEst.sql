
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'WOs - Act Adds > Act Est';
  l_table_name := 'Dynamic View';
  l_subsystem := 'Tax Repairs';
  l_query_type := 'system';
  l_is_multicurrency := null;
  
  l_query_sql := 'SELECT A.WORK_ORDER_ID, A.WORK_ORDER_NUMBER, A.WO_DESCR, A.EFFT_ACCT_MONTH, A.REPLACEMENT_FLAG, A.TAX_STATUS, A.TAX_UNIT, A.TAX_LOCATION, A.REPLACEMENT_COST, A.ADDITION_ESTIMATES, A.ADDITION_ACT_CHARGES, A.COR_SALV_DOLLARS, DECODE(REPLACEMENT_COST, 0, 0, NULL, 0, ROUND(ADDITION_ESTIMATES / REPLACEMENT_COST, 2)) RATIO_EST_TO_REPL_COST, DECODE(REPLACEMENT_COST, 0, 0, NULL, 0, ROUND(ADDITION_ACT_CHARGES / REPLACEMENT_COST, 2)) RATIO_ACT_TO_REPL_COST, DECODE(ADDITION_ESTIMATES, 0, 0, NULL, 0, ROUND(ADDITION_ACT_CHARGES / ADDITION_ESTIMATES, 2)) RATIO_ACT_TO_EST_COST    FROM (SELECT WOC.WORK_ORDER_ID WORK_ORDER_ID, WOC.WORK_ORDER_NUMBER WORK_ORDER_NUMBER, WOC.DESCRIPTION WO_DESCR, TO_CHAR(WOTS.EFFECTIVE_ACCOUNTING_MONTH, ''MM/YYYY'') EFFT_ACCT_MONTH, DECODE(WOTS.REPLACEMENT_FLAG, 1, ''YES'', 0, ''NO'', ''UNKNOWN'') REPLACEMENT_FLAG, TS.DESCRIPTION TAX_STATUS, RU.DESCRIPTION TAX_UNIT, TL.DESCRIPTION TAX_LOCATION, WOTS.TEST_REPLACE_COST REPLACEMENT_COST, (SELECT NVL(SUM(WE.AMOUNT), 0) FROM WO_ESTIMATE WE WHERE WE.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND WE.REVISION = (SELECT MAX(REVISION) FROM WO_ESTIMATE WE2 WHERE WE.WORK_ORDER_ID = WE2.WORK_ORDER_ID) AND WE.EXPENDITURE_TYPE_ID = 1) AS ADDITION_ESTIMATES, (SELECT NVL(SUM(CC.AMOUNT), 0) FROM CWIP_CHARGE CC WHERE CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND CC.EXPENDITURE_TYPE_ID = 1) ADDITION_ACT_CHARGES, (SELECT NVL(SUM(CC.AMOUNT), 0) FROM CWIP_CHARGE CC, CHARGE_TYPE CT WHERE CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND CC.EXPENDITURE_TYPE_ID = 2 AND CC.CHARGE_TYPE_ID = CT.CHARGE_TYPE_ID AND CT.PROCESSING_TYPE_ID > 1) COR_SALV_DOLLARS FROM WORK_ORDER_TAX_STATUS WOTS, WORK_ORDER_CONTROL WOC, WO_TAX_STATUS TS, REPAIR_LOCATION TL, REPAIR_UNIT_CODE RU WHERE WOC.WORK_ORDER_ID = WOTS.WORK_ORDER_ID AND TS.TAX_STATUS_ID(+) = WOTS.TAX_STATUS_ID AND TS.REPAIR_SCHEMA_ID IS NOT NULL AND WOTS.EFFECTIVE_ACCOUNTING_MONTH = (SELECT MAX(EFFECTIVE_ACCOUNTING_MONTH) FROM WORK_ORDER_TAX_STATUS WOTS2 WHERE WOTS.WORK_ORDER_ID = WOTS2.WORK_ORDER_ID) AND TL.REPAIR_LOCATION_ID(+) = WOTS.REPAIR_LOCATION_ID AND RU.REPAIR_UNIT_CODE_ID(+) = WOTS.REPAIR_UNIT_CODE_ID AND (SELECT NVL(SUM(CC.AMOUNT), 0) FROM CWIP_CHARGE CC WHERE CC.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND CC.EXPENDITURE_TYPE_ID = 1) > (SELECT NVL(SUM(WE.AMOUNT), 0) FROM WO_ESTIMATE WE WHERE WE.WORK_ORDER_ID = WOC.WORK_ORDER_ID AND WE.REVISION = (SELECT MAX(REVISION) FROM WO_ESTIMATE WE2 WHERE WE.WORK_ORDER_ID = WE2.WORK_ORDER_ID) AND WE.EXPENDITURE_TYPE_ID = 1) GROUP BY WOC.WORK_ORDER_ID, WOC.WORK_ORDER_NUMBER, WOC.DESCRIPTION, TO_CHAR(WOTS.EFFECTIVE_ACCOUNTING_MONTH, ''MM/YYYY''), DECODE(WOTS.REPLACEMENT_FLAG, 1, ''YES'', 0, ''NO'', ''UNKNOWN''), TS.DESCRIPTION, RU.DESCRIPTION, TL.DESCRIPTION, WOTS.TEST_REPLACE_COST) A, WORK_ORDER_CONTROL, COMPANY   WHERE A.WORK_ORDER_ID = WORK_ORDER_CONTROL.WORK_ORDER_ID  AND WORK_ORDER_CONTROL.COMPANY_ID = COMPANY.COMPANY_ID          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'addition_act_charges',
        11,
        0,
        1,
        '',
        'Addition Act Charges',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'addition_estimates',
        10,
        0,
        1,
        '',
        'Addition Estimates',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cor_salv_dollars',
        12,
        0,
        1,
        '',
        'Cor Salv Dollars',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'efft_acct_month',
        4,
        0,
        1,
        '',
        'Efft Acct Month',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ratio_act_to_est_cost',
        15,
        0,
        1,
        '',
        'Ratio Act To Est Cost',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ratio_act_to_repl_cost',
        14,
        0,
        1,
        '',
        'Ratio Act To Repl Cost',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ratio_est_to_repl_cost',
        13,
        0,
        1,
        '',
        'Ratio Est To Repl Cost',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_cost',
        9,
        1,
        1,
        '',
        'Replacement Cost',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_flag',
        5,
        0,
        1,
        '',
        'Replacement Flag',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_location',
        8,
        0,
        1,
        '',
        'Tax Location',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_status',
        6,
        0,
        1,
        '',
        'Tax Status',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_unit',
        7,
        0,
        1,
        '',
        'Tax Unit',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'wo_descr',
        3,
        0,
        1,
        '',
        'Wo Descr',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'work_order_id',
        1,
        0,
        1,
        '',
        'Work Order Id',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'work_order_number',
        2,
        0,
        1,
        '',
        'Work Order Number',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/