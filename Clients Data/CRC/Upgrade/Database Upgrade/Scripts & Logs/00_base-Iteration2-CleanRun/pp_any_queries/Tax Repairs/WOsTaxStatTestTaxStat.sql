
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'WOs - Tax Stat <> Test Tax Stat';
  l_table_name := 'Dynamic View';
  l_subsystem := 'Tax Repairs';
  l_query_type := 'system';
  l_is_multicurrency := null;
  
  l_query_sql := 'select project_number,         project,         to_char(effective_accounting_month, ''MM/YYYY'') effective_accounting_month,         tax_unit,         tax_unit_source,         tax_location,         replacement_flag,  replacement_source,         current_tax_status,         test_tax_status,         reviewed_date,         review_user,         external_reviewed_date,  external_review_user,         override_note,         replacement_cost,         total_addition_estimates,         total_retirement_estimates,  actual_addition_charges,         actual_total_charges    from (select woc.work_order_number project_number,                 woc.description project,  wots.effective_accounting_month effective_accounting_month,                 ruc.description tax_unit,                 wots.repair_unit_source tax_unit_source,                 rl.description tax_location,                 decode(wots.replacement_flag, 0, ''No'', 1, ''Yes'') replacement_flag,  wots.replacement_source replacement_source,ts.description current_tax_status,                 ts2.description test_tax_status,                 wots.reviewed_date reviewed_date,                 wots.review_user review_user,                 wots.external_reviewed_date external_reviewed_date,  wots.external_review_user external_review_user,                 wots.override_note override_note,  wots.test_replace_cost replacement_cost,                 (select sum(amount)                    from wo_estimate a                   where a.work_order_id = woc.work_order_id                     and a.expenditure_type_id = 1                     and a.revision =                         (select max(revision)                            from wo_estimate aa  where a.work_order_id = aa.work_order_id)) total_addition_estimates,                 (select sum(amount)                    from wo_estimate a                   where a.work_order_id = woc.work_order_id                     and a.expenditure_type_id = 2                     and a.revision = (select max(revision)                            from wo_estimate aa  where a.work_order_id = aa.work_order_id)) total_retirement_estimates,                 (select sum(amount)                    from cwip_charge cc, charge_type ct                   where cc.work_order_id = wots.work_order_id                     and ct.charge_type_id = cc.charge_type_id                     and ct.exclude_from_total_charges = 0                     and expenditure_type_id = 1) actual_addition_charges,                 (select sum(amount)                    from cwip_charge cc, charge_type ct                   where cc.work_order_id = wots.work_order_id  and ct.charge_type_id = cc.charge_type_id                     and ct.exclude_from_total_charges = 0) actual_total_charges            from work_order_tax_status wots,                 work_order_control    woc,                 repair_unit_code      ruc,                 repair_location       rl,                 wo_tax_status         ts, wo_tax_status         ts2,      company           where ruc.repair_unit_code_id = wots.repair_unit_code_id             and rl.repair_location_id = wots.repair_location_id             and woc.work_order_id = wots.work_order_id             and ts.tax_status_id = wots.tax_status_id             and ts2.tax_status_id = wots.test_tax_status_id             and wots.tax_status_id <> wots.test_tax_status_id             and wots.effective_accounting_month =                 (select max(effective_accounting_month)  from work_order_tax_status wots2                   where wots.work_order_id = wots2.work_order_id)             and woc.company_id = company.company_id  )          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'actual_addition_charges',
        19,
        0,
        1,
        '',
        'Actual Addition Charges',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'actual_total_charges',
        20,
        0,
        1,
        '',
        'Actual Total Charges',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'current_tax_status',
        9,
        0,
        1,
        '',
        'Current Tax Status',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'effective_accounting_month',
        3,
        0,
        1,
        '',
        'Effective Accounting Month',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_review_user',
        14,
        0,
        1,
        '',
        'External Review User',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_reviewed_date',
        13,
        0,
        1,
        '',
        'External Reviewed Date',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'override_note',
        15,
        0,
        1,
        '',
        'Override Note',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'project',
        2,
        0,
        1,
        '',
        'Project',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'project_number',
        1,
        0,
        1,
        '',
        'Project Number',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_cost',
        16,
        1,
        1,
        '',
        'Replacement Cost',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_flag',
        7,
        0,
        1,
        '',
        'Replacement Flag',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'replacement_source',
        8,
        0,
        1,
        '',
        'Replacement Source',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'review_user',
        12,
        0,
        1,
        '',
        'Review User',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'reviewed_date',
        11,
        0,
        1,
        '',
        'Reviewed Date',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_location',
        6,
        0,
        1,
        '',
        'Tax Location',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_unit',
        4,
        0,
        1,
        '',
        'Tax Unit',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_unit_source',
        5,
        0,
        1,
        '',
        'Tax Unit Source',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'test_tax_status',
        10,
        0,
        1,
        '',
        'Test Tax Status',
        600,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'total_addition_estimates',
        17,
        0,
        1,
        '',
        'Total Addition Estimates',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'total_retirement_estimates',
        18,
        0,
        1,
        '',
        'Total Retirement Estimates',
        600,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/