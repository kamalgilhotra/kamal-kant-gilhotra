
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Location Detail';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, la.description as asset_description,
       ls.description as leased_asset_status, ilrs.description as ilr_status,
       lfs.description as funding_status, cap.description as lease_cap_type,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ilr.external_ilr, ll.lease_id, ll.lease_number,
       al.long_description as location, al.address, t.description as town, al.state_id as state, al.zip_code, al.county_id county, al.grid_coordinate
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_asset_status ls, ls_ilr_status ilrs, ls_funding_status lfs, town t, ls_lease_cap_type cap, ls_ilr_options ilro
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and la.company_id = co.company_id
  and la.asset_location_id = al.asset_location_id
  and la.ls_asset_status_id = ls.ls_asset_status_id
  and ilr.ilr_status_id = ilrs.ilr_status_id
  and ilr.funding_status_id = lfs.funding_status_id
  and al.town_id = t.town_id (+)
  and cap.ls_lease_cap_type_id = ilro.lease_cap_type_id
  and ilro.ilr_id = ilr.ilr_id
  and ilro.revision = ilr.current_revision          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'address',
        16,
        0,
        1,
        '',
        'Address',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'asset_description',
        3,
        0,
        1,
        '',
        'Asset Description',
        300,
        'description',
        'ls_asset',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_description',
        9,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        8,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'county',
        20,
        0,
        1,
        '',
        'County',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_ilr',
        12,
        0,
        1,
        '',
        'External Ilr',
        300,
        'external_ilr',
        'ls_ilr',
        'VARCHAR2',
        0,
        'external_ilr',
        null,
        null,
        'external_ilr',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'funding_status',
        6,
        0,
        1,
        '',
        'Funding Status',
        300,
        'description',
        'ls_funding_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'grid_coordinate',
        21,
        0,
        1,
        '',
        'Grid Coordinate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        10,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        11,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_status',
        5,
        0,
        1,
        '',
        'Ilr Status',
        300,
        'description',
        'ls_ilr_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type',
        7,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_id',
        13,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        14,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'leased_asset_number',
        2,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'leased_asset_status',
        4,
        0,
        1,
        '',
        'Leased Asset Status',
        300,
        'description',
        'ls_asset_status',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'location',
        15,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        1,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'state',
        18,
        0,
        1,
        '',
        'State',
        300,
        'state_id',
        'state',
        'VARCHAR2',
        0,
        'state_id',
        null,
        null,
        'state_id',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'town',
        17,
        0,
        1,
        '',
        'Town',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'zip_code',
        19,
        0,
        1,
        '',
        'Zip Code',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/