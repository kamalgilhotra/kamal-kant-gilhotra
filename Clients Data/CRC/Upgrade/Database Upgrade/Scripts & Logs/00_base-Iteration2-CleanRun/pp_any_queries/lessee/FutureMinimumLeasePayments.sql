
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Future Minimum Lease Payments';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select    (select s.description from set_of_books s where s.set_of_books_id = sch2.set_of_books_id) as set_of_books,    c.description company_description,
       (select lg.description from ls_lease_group lg where mla.lease_group_id = lg.lease_group_id) as lease_group,    (select ls.description from ls_lessor ls where ls.lessor_id = mla.lessor_id)
       as lessor,    mla.lease_number as lease_number, mla.description as lease_description,    case when mla.pre_payment_sw = 1 then ''Prepay'' else ''Arrears'' end as prepay,
       (select ig.description from ls_ilr_group ig where ilr.ilr_group_id = ig.ilr_group_id) as ilr_group,    ilr.ilr_number as ilr_number,
       (select lct.description from ls_lease_cap_type lct where lct.ls_lease_cap_type_id = ilro.lease_cap_type_id) as capitalization_type,
       la.description as asset_description, la.serial_number as serial_number,    al.ext_asset_location as ext_asset_location, al.long_description as asset_loc_description,
       (select ua.description from utility_account ua where ua.utility_account_id = la.utility_account_id and ua.bus_segment_id = la.bus_segment_id) as utility_account,
       (select ru.description from retirement_unit ru where ru.retirement_unit_id = la.retirement_unit_id) as retirement_unit,    nvl(td.county_id, al.county_id) as county,
       nvl(td.state_id, al.state_id) as state,    td.tax_district_code as tax_district_code,    case when sch2.the_row = 1 then la.fmv else 0 end as fair_market_value,
       sch2.month_number as month_number,    case when sch2.the_row = 1 then sch2.beg_capital_cost else 0 end as beg_capital_cost,    nvl(sch2.residual_amount, 0) as residual_amount,
       nvl(sch2.termination_penalty_amount, 0) as termination_penalty_amount,    nvl(sch2.bargain_purchase_amount, 0) as bargain_purchase_amount,    nvl(sch2.beg_obligation, 0) as beg_obligation,
       nvl(sch2.beg_lt_obligation, 0) as beg_lt_obligation,    nvl(sch2.interest_accrual, 0) as interest_accrual, nvl(sch2.principal_accrual, 0) as principal_accrual,
       nvl(sch2.interest_paid, 0) as interest_paid, nvl(sch2.principal_paid, 0) as principal_paid, nvl(sch2.beg_liability, 0) as beg_liability,
       nvl(sch2.beg_lt_liability, 0) as beg_lt_liability from    ls_lease mla, ls_lease_company lsc,    ls_ilr ilr, ls_ilr_options ilro,    ls_asset la,
	   (       select row_number() over( partition by sch.ls_asset_id, sch.revision, sch.set_of_books_id order by sch.month ) as the_row,          sch.set_of_books_id, sch.ls_asset_id, sch.revision,
	   to_number(to_char(sch.month, ''yyyymm'')) as month_number,          sch.residual_amount as residual_amount, sch.term_penalty as termination_penalty_amount,          sch.bpo_price as bargain_purchase_amount, sch.beg_capital_cost as beg_capital_cost,
	   sch.beg_obligation as beg_obligation, sch.beg_lt_obligation as beg_lt_obligation,          sch.interest_accrual as interest_accrual, sch.principal_accrual as principal_accrual,          sch.interest_paid as interest_paid, sch.principal_paid as principal_paid, sch.beg_liability as beg_liability, sch.beg_lt_liability as beg_lt_liability
	   from ls_asset_schedule sch    ) sch2,    company c,    asset_location al, prop_tax_district td where mla.lease_id = lsc.lease_id and lsc.company_id = c.company_id and lsc.lease_id = ilr.lease_id (+) and lsc.company_id = ilr.company_id (+) and ilr.ilr_id = la.ilr_id (+)
	   and ilr.ilr_id = ilro.ilr_id (+) and ilr.current_revision = ilro.revision (+) and la.ls_asset_id = sch2.ls_asset_id and la.approved_revision = sch2.revision and la.asset_location_id = al.asset_location_id (+) and al.tax_district_id = td.tax_district_id (+) order by 1, 2, 3, 5, 8, 9, 11, 21          ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'asset_description',
        11,
        0,
        1,
        '',
        'Asset Description',
        300,
        'description',
        'ls_asset',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'asset_loc_description',
        14,
        0,
        1,
        '',
        'Asset Location',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'bargain_purchase_amount',
        25,
        1,
        1,
        '',
        'Bargain Purchase Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_capital_cost',
        22,
        1,
        1,
        '',
        'Beg Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_obligation',
        27,
        1,
        1,
        '',
        'Beg Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_obligation',
        26,
        1,
        1,
        '',
        'Beg Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'capitalization_type',
        10,
        0,
        1,
        '',
        'Capitalization Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_description',
        2,
        0,
        1,
        '',
        'Company',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'county',
        17,
        0,
        1,
        '',
        'County',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ext_asset_location',
        13,
        0,
        1,
        '',
        'Ext Asset Location',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'fair_market_value',
        20,
        1,
        1,
        '',
        'Fair Market Value',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_group',
        8,
        0,
        1,
        '',
        'ILR Group',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_number',
        9,
        0,
        1,
        '',
        'ILR Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_accrual',
        28,
        1,
        1,
        '',
        'Interest Accrual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_paid',
        30,
        1,
        1,
        '',
        'Interest Paid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_description',
        6,
        0,
        1,
        '',
        'Lease Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_group',
        3,
        0,
        1,
        '',
        'Lease Group',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_number',
        5,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lessor',
        4,
        0,
        1,
        '',
        'Lessor',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'month_number',
        21,
        0,
        1,
        '',
        'Month Number',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'prepay',
        7,
        0,
        1,
        '',
        'Prepay',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_accrual',
        29,
        1,
        1,
        '',
        'Principal Accrual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_paid',
        31,
        1,
        1,
        '',
        'Principal Paid',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'residual_amount',
        23,
        1,
        1,
        '',
        'Residual Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'retirement_unit',
        16,
        0,
        1,
        '',
        'Retirement Unit',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'serial_number',
        12,
        0,
        1,
        '',
        'Serial Number',
        300,
        'serial_number',
        'ls_component',
        'VARCHAR2',
        0,
        'serial_number',
        null,
        null,
        'serial_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'set_of_books',
        1,
        0,
        1,
        '',
        'Set Of Books',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'state',
        18,
        0,
        1,
        '',
        'State',
        300,
        'state_id',
        'state',
        'VARCHAR2',
        0,
        'state_id',
        null,
        null,
        'state_id',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_district_code',
        19,
        0,
        1,
        '',
        'Tax District Code',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'termination_penalty_amount',
        24,
        1,
        1,
        '',
        'Termination Penalty Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'utility_account',
        15,
        0,
        1,
        '',
        'Utility Account',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_liability',
        32,
        1,
        1,
        '',
        'Beg Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'beg_lt_liability',
        33,
        1,
        1,
        '',
        'Beg Lt Liability',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual
;

END;

/

COMMIT;