
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Effective Tax Rates by Asset';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'with rates as (
	select
    la.ls_asset_id,
		tl.tax_local_id,
		tsr.rate,
    al.state_id as state,
    null as tax_district_id,
    ''State'' as tax_type
	from ls_asset la, asset_location al, ls_tax_local tl, ls_lease ll,
       ls_ilr ilr, ls_lease_options llo, ls_asset_tax_map tax_map,
		(select tsr2.*, row_number() over(partition by tsr2.tax_local_id, tsr2.state_id order by tsr2.effective_date desc) as the_row
		 from ls_tax_state_rates tsr2
     where effective_date <= (select to_date(filter_value, ''yyyymmdd'') from pp_any_required_filter where upper(trim(column_name)) = ''EFFECTIVE DATE YYYYMMDD'')) tsr
	where tsr.tax_local_id = tl.tax_local_id
	and tl.tax_summary_id = llo.tax_summary_id
	and tsr.state_id = al.state_id
	and al.asset_location_id = la.asset_location_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ll.lease_id = llo.lease_id
  and ll.current_revision = llo.revision
  and tax_map.ls_asset_id = la.ls_asset_id
  and tax_map.tax_local_id = tl.tax_local_id
  and tax_map.status_code_id = 1
	and tsr.the_row = 1
union all
	select
		la.ls_asset_id,
		tl.tax_local_id,
		tdr.rate,
    null as state,
    td.tax_district_id,
    ''Local'' as tax_type
	from ls_asset la, ls_location_tax_district al, ls_tax_local tl, ls_lease ll,
       ls_ilr ilr, ls_lease_options llo, ls_asset_tax_map tax_map, tax_district td,
		(select tsr2.*, row_number() over(partition by tsr2.tax_local_id, tsr2.ls_tax_district_id order by tsr2.effective_date desc) as the_row
		 from ls_tax_district_rates tsr2
     where effective_date <= (select to_date(filter_value, ''yyyymmdd'') from pp_any_required_filter where upper(trim(column_name)) = ''EFFECTIVE DATE YYYYMMDD'')) tdr
	where tdr.tax_local_id = tl.tax_local_id
	and tl.tax_summary_id = llo.tax_summary_id
	and tdr.ls_tax_district_id = al.tax_district_id
	and al.asset_location_id = la.asset_location_id
	and tdr.the_row = 1
  and tax_map.ls_asset_id = la.ls_asset_id
  and tax_map.tax_local_id = tl.tax_local_id
  and tax_map.status_code_id = 1
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and ll.lease_id = llo.lease_id
  and ll.current_revision = llo.revision
  and al.tax_district_id = td.tax_district_id)
/*-----------------------------------------------------------------------------------
------------------------------------------------------------------------------------- */
select (select filter_value from pp_any_required_filter where upper(trim(column_name)) = ''EFFECTIVE DATE YYYYMMDD'') effective_date_yyyymmdd,
       la.ls_asset_id, la.leased_asset_number, ilr.ilr_id, ilr.ilr_number,
       ll.lease_id, ll.lease_number, co.company_id, co.description as company_description, lct.description as lease_cap_type, al.long_description as location,
       ltl.tax_local_id, ltl.description as tax_local, rates.tax_type, td.description as tax_district, rates.state,
       rates.rate
/*, sum(rates.rate) over(partition by la.ls_asset_id) as total_asset_rate */
from ls_asset la, ls_ilr ilr, ls_lease ll, company co, rates,
     tax_district td, ls_tax_local ltl, asset_location al, ls_ilr_options ilro, ls_lease_cap_type lct
where la.ls_asset_id = rates.ls_asset_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and la.company_id = co.company_id
  and la.asset_location_id = al.asset_location_id
  and rates.tax_district_id = td.tax_district_id (+)
  and rates.tax_local_id = ltl.tax_local_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'company_description',
        9,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        8,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'effective_date_yyyymmdd',
        1,
        0,
        1,
        '',
        'Effective Date Yyyymmdd',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        1,
        1,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        4,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        5,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type',
        10,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_id',
        6,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        7,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'leased_asset_number',
        3,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'location',
        11,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        2,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'rate',
        17,
        0,
        1,
        '',
        'Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'state',
        16,
        0,
        1,
        '',
        'State',
        300,
        'state_id',
        'state',
        'VARCHAR2',
        0,
        'state_id',
        null,
        null,
        'state_id',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_district',
        15,
        0,
        1,
        '',
        'Tax District',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_local',
        13,
        0,
        1,
        '',
        'Tax Local',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_local_id',
        12,
        0,
        1,
        '',
        'Tax Local Id',
        300,
        'description',
        'ls_tax_local',
        'NUMBER',
        0,
        'tax_local_id',
        null,
        null,
        'tax_local_id',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_type',
        14,
        0,
        1,
        '',
        'Tax Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/