DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Forecast Lease Asset Schedule By MLA';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select * from LS_FCST_ASSET_SCHED_BY_MLA_VW ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'ls_forecast_version', 1, 0, 1, null, 'Forecast Version', 400, 'description', 'ls_forecast_version', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_number', 2, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', 1, 2, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'conversion_date', 3, 0, 1, null, 'Conversion Date', 400, 'conversion_date', 'ls_forecast_version', 'VARCHAR2', 0, 'conversion_date', null, null, 'conversion_date', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ls_asset_id', 4, 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'leased_asset_number', 5, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_id', 6, 0, 1, null, 'Company Id', 300, 'description', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_description', 7, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_id', 8, 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_number', 9, 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_id', 10, 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_cap_type', 11, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'location', 12, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, 'long_description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'revision', 13, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'set_of_books_id', 14, 0, 1, null, 'Set Of Books Id', 300, 'description', 'set_of_books', 'NUMBER', 0, 'set_of_books_id', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'monthnum', 15, 0, 1, null, 'Monthnum', 300, 'month_number', '(select month_number from pp_calendar)', 'VARCHAR2', 0, 'month_number', null, null, 'the_sort', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_capital_cost', 16, 1, 0, null, 'Beg Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_capital_cost', 17, 1, 0, null, 'End Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_obligation', 18, 1, 0, null, 'Beg Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_obligation', 19, 1, 0, null, 'End Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_obligation', 20, 1, 0, null, 'Beg Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_obligation', 21, 1, 0, null, 'End Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_accrual', 22, 1, 0, null, 'Interest Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_accrual', 23, 1, 0, null, 'Principal Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_paid', 24, 1, 0, null, 'Interest Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_paid', 25, 1, 0, null, 'Principal Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual1', 26, 1, 0, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual2', 27, 1, 0, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual3', 28, 1, 0, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual4', 29, 1, 0, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual5', 30, 1, 0, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual6', 31, 1, 0, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual7', 32, 1, 0, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual8', 33, 1, 0, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual9', 34, 1, 0, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual10', 35, 1, 0, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid1', 36, 1, 0, null, 'Executory Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid2', 37, 1, 0, null, 'Executory Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid3', 38, 1, 0, null, 'Executory Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid4', 39, 1, 0, null, 'Executory Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid5', 40, 1, 0, null, 'Executory Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid6', 41, 1, 0, null, 'Executory Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid7', 42, 1, 0, null, 'Executory Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid8', 43, 1, 0, null, 'Executory Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid9', 44, 1, 0, null, 'Executory Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid10', 45, 1, 0, null, 'Executory Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual1', 46, 1, 0, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual2', 47, 1, 0, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual3', 48, 1, 0, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual4', 49, 1, 0, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual5', 50, 1, 0, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual6', 51, 1, 0, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual7', 52, 1, 0, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual8', 53, 1, 0, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual9', 54, 1, 0, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual10', 55, 1, 0, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid1', 56, 1, 0, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid2', 57, 1, 0, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid3', 58, 1, 0, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid4', 59, 1, 0, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid5', 60, 1, 0, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid6', 61, 1, 0, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid7', 62, 1, 0, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid8', 63, 1, 0, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid9', 64, 1, 0, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid10', 65, 1, 0, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'is_om', 66, 0, 1, null, 'Is Om', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'current_lease_cost', 67, 1, 0, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'residual_amount', 68, 1, 0, null, 'Residual Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'term_penalty', 69, 1, 0, null, 'Term Penalty', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'bpo_price', 70, 1, 0, null, 'Bpo Price', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'currency_type', 71, 0, 1, null, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'currency_display_symbol', 72, 0, 0, null, 'Currency Symbol', 300, '', '', 'VARCHAR2', 0, '', null, null, 'currency_display_symbol', 1, 1 FROM dual UNION ALL 
SELECT  l_id, 'iso_code', 73, 0, 1, null, 'Currency', 300, 'description', 'currency', 'VARCHAR2', 0, 'iso_code', null, null, 'iso_code', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_deferred_rent', 74, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'deferred_rent', 75, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_deferred_rent', 76, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_liability', 77, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_liability', 78, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_liability', 79, 1, 1, null, 'Beg LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_liability', 80, 1, 1, null, 'End LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_remeasurement', 81, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'liability_remeasurement', 82, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_net_rou_asset', 83, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'rou_asset_remeasurement', 84, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_net_rou_asset', 85, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'initial_direct_cost', 86, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'incentive_amount', 87, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_prepaid_rent', 88, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepay_amortization', 89, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepaid_rent', 90, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_prepaid_rent', 91, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_arrears_accrual', 92, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'arrears_accrual', 93, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_arrears_accrual', 94, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_adjust', 95, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_adjust', 96, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_reserve', 97, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_expense', 98, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_exp_alloc_adjust', 99, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_reserve', 100, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 101, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'impairment_activity', 102, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_accum_impair', 103, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual
;

END;

/

COMMIT;