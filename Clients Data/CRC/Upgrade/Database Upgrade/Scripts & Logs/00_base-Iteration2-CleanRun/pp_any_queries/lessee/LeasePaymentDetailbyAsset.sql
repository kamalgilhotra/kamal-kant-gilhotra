
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Payment Detail by Asset';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'WITH currency_selection AS (
SELECT lct.ls_currency_type_id, lct.description
FROM ls_lease_currency_type lct, pp_any_required_filter parf
where upper(trim(parf.column_name)) = ''CURRENCY TYPE''
AND  upper(trim(parf.filter_value)) =  upper(trim(lct.description))
)
select la.ls_asset_id,
       to_char(lpl.gl_posting_mo_yr, ''yyyymm'') as monthnum,
       co.company_id,
       co.description as company_description,
       la.leased_asset_number,
       ilr.ilr_id,
       ilr.ilr_number,
       ll.lease_id,
       ll.lease_number,
       lct.description as lease_cap_type,
       al.long_description as location,
       nvl(sum(decode(lpl.payment_type_id, 1, lpl.amount, 0)), 0) principal_payment,
       nvl(sum(decode(lpl.payment_type_id, 2, lpl.amount, 0)), 0) interest_payment,
       nvl(sum(decode(lpl.payment_type_id, 2, lpl.adjustment_amount, 0)), 0) interest_adjustment,
       nvl(sum(decode(lpl.payment_type_id, 3, lpl.amount, 0)), 0) executory_payment1,
       nvl(sum(decode(lpl.payment_type_id, 3, lpl.adjustment_amount, 0)), 0) executory_adjustment1,
       nvl(sum(decode(lpl.payment_type_id, 4, lpl.amount, 0)), 0) executory_payment2,
       nvl(sum(decode(lpl.payment_type_id, 4, lpl.adjustment_amount, 0)), 0) executory_adjustment2,
       nvl(sum(decode(lpl.payment_type_id, 5, lpl.amount, 0)), 0) executory_payment3,
       nvl(sum(decode(lpl.payment_type_id, 5, lpl.adjustment_amount, 0)), 0) executory_adjustment3,
       nvl(sum(decode(lpl.payment_type_id, 6, lpl.amount, 0)), 0) executory_payment4,
       nvl(sum(decode(lpl.payment_type_id, 6, lpl.adjustment_amount, 0)), 0) executory_adjustment4,
       nvl(sum(decode(lpl.payment_type_id, 7, lpl.amount, 0)), 0) executory_payment5,
       nvl(sum(decode(lpl.payment_type_id, 7, lpl.adjustment_amount, 0)), 0) executory_adjustment5,
       nvl(sum(decode(lpl.payment_type_id, 8, lpl.amount, 0)), 0) executory_payment6,
       nvl(sum(decode(lpl.payment_type_id, 8, lpl.adjustment_amount, 0)), 0) executory_adjustment6,
       nvl(sum(decode(lpl.payment_type_id, 9, lpl.amount, 0)), 0) executory_payment7,
       nvl(sum(decode(lpl.payment_type_id, 9, lpl.adjustment_amount, 0)), 0) executory_adjustment7,
       nvl(sum(decode(lpl.payment_type_id, 10, lpl.amount, 0)), 0) executory_payment8,
       nvl(sum(decode(lpl.payment_type_id, 10, lpl.adjustment_amount, 0)), 0) executory_adjustment8,
       nvl(sum(decode(lpl.payment_type_id, 11, lpl.amount, 0)), 0) executory_payment9,
       nvl(sum(decode(lpl.payment_type_id, 11, lpl.adjustment_amount, 0)), 0) executory_adjustment9,
       nvl(sum(decode(lpl.payment_type_id, 12, lpl.amount, 0)), 0) executory_payment10,
       nvl(sum(decode(lpl.payment_type_id, 12, lpl.adjustment_amount, 0)), 0) executory_adjustment10,
       nvl(sum(decode(lpl.payment_type_id, 13, lpl.amount, 0)), 0) contingent_payment1,
       nvl(sum(decode(lpl.payment_type_id, 13, lpl.adjustment_amount, 0)), 0) contingent_adjustment1,
       nvl(sum(decode(lpl.payment_type_id, 14, lpl.amount, 0)), 0) contingent_payment2,
       nvl(sum(decode(lpl.payment_type_id, 14, lpl.adjustment_amount, 0)), 0) contingent_adjustment2,
       nvl(sum(decode(lpl.payment_type_id, 15, lpl.amount, 0)), 0) contingent_payment3,
       nvl(sum(decode(lpl.payment_type_id, 15, lpl.adjustment_amount, 0)), 0) contingent_adjustment3,
       nvl(sum(decode(lpl.payment_type_id, 16, lpl.amount, 0)), 0) contingent_payment4,
       nvl(sum(decode(lpl.payment_type_id, 16, lpl.adjustment_amount, 0)), 0) contingent_adjustment4,
       nvl(sum(decode(lpl.payment_type_id, 17, lpl.amount, 0)), 0) contingent_payment5,
       nvl(sum(decode(lpl.payment_type_id, 17, lpl.adjustment_amount, 0)), 0) contingent_adjustment5,
       nvl(sum(decode(lpl.payment_type_id, 18, lpl.amount, 0)), 0) contingent_payment6,
       nvl(sum(decode(lpl.payment_type_id, 18, lpl.adjustment_amount, 0)), 0) contingent_adjustment6, 
       nvl(sum(decode(lpl.payment_type_id, 19, lpl.amount, 0)), 0) contingent_payment7,
       nvl(sum(decode(lpl.payment_type_id, 19, lpl.adjustment_amount, 0)), 0) contingent_adjustment7,
       nvl(sum(decode(lpl.payment_type_id, 20, lpl.amount, 0)), 0) contingent_payment8,
       nvl(sum(decode(lpl.payment_type_id, 20, lpl.adjustment_amount, 0)), 0) contingent_adjustment8,
       nvl(sum(decode(lpl.payment_type_id, 21, lpl.amount, 0)), 0) contingent_payment9,
       nvl(sum(decode(lpl.payment_type_id, 21, lpl.adjustment_amount, 0)), 0) contingent_adjustment9,
       nvl(sum(decode(lpl.payment_type_id, 22, lpl.amount, 0)), 0) contingent_payment10,
       nvl(sum(decode(lpl.payment_type_id, 22, lpl.adjustment_amount, 0)), 0) contingent_adjustment10,
       nvl(sum(decode(lpl.payment_type_id, 23, lpl.amount, 0)), 0) termination_penalty,
       nvl(sum(decode(lpl.payment_type_id, 24, lpl.amount, 0)), 0) sales_proceeds,
       nvl(tax_1, 0) as tax_1,
       nvl(tax_1_adjustment, 0) as tax_1_adjustment,
       nvl(tax_2, 0) as tax_2,
       nvl(tax_2_adjustment, 0) as tax_2_adjustment,
       nvl(tax_3, 0) as tax_3,
       nvl(tax_3_adjustment, 0) as tax_3_adjustment,
       nvl(tax_4, 0) as tax_4,
       nvl(tax_4_adjustment, 0) as tax_4_adjustment,
       nvl(tax_5, 0) as tax_5,
       nvl(tax_5_adjustment, 0) as tax_5_adjustment,
       nvl(tax_6, 0) as tax_6,
       nvl(tax_6_adjustment, 0) as tax_6_adjustment,
       nvl(tax_7, 0) as tax_7,
       nvl(tax_7_adjustment, 0) as tax_7_adjustment,
       InitCap(currency_selection.description) currency_type,
       lpl.iso_code currency,
       lpl.currency_display_symbol currency_symbol
  from ls_asset la
       JOIN company co ON (la.company_id = co.company_id)
       JOIN v_ls_payment_line_fx lpl ON (lpl.ls_asset_id = la.ls_asset_id)
       JOIN asset_location al ON (la.asset_location_id = al.asset_location_id)
       JOIN ls_ilr ilr ON (la.ilr_id = ilr.ilr_id)
       JOIN ls_lease ll ON (ilr.lease_id = ll.lease_id)
       JOIN ls_ilr_options ilro ON (la.ilr_id = ilro.ilr_id and la.approved_revision = ilro.revision)
       JOIN ls_lease_cap_type lct ON (ilro.lease_cap_type_id = lct.ls_lease_cap_type_id)
       JOIN currency_selection ON (lpl.ls_cur_type = currency_selection.ls_currency_type_id)
       left OUTER JOIN (select lmt.ls_asset_id,
               nvl(sum(decode(tax_local_id, 1, payment_amount, 0)), 0) as tax_1,
               nvl(sum(decode(tax_local_id, 1, adjustment_amount, 0)), 0) as tax_1_adjustment,
               nvl(sum(decode(tax_local_id, 2, payment_amount, 0)), 0) as tax_2,
               nvl(sum(decode(tax_local_id, 2, adjustment_amount, 0)), 0) as tax_2_adjustment,
               nvl(sum(decode(tax_local_id, 3, payment_amount, 0)), 0) as tax_3,
               nvl(sum(decode(tax_local_id, 3, adjustment_amount, 0)), 0) as tax_3_adjustment,
               nvl(sum(decode(tax_local_id, 4, payment_amount, 0)), 0) as tax_4,
               nvl(sum(decode(tax_local_id, 4, adjustment_amount, 0)), 0) as tax_4_adjustment,
               nvl(sum(decode(tax_local_id, 5, payment_amount, 0)), 0) as tax_5,
               nvl(sum(decode(tax_local_id, 5, adjustment_amount, 0)), 0) as tax_5_adjustment,
               nvl(sum(decode(tax_local_id, 6, payment_amount, 0)), 0) as tax_6,
               nvl(sum(decode(tax_local_id, 6, adjustment_amount, 0)), 0) as tax_6_adjustment,
               nvl(sum(decode(tax_local_id, 7, payment_amount, 0)), 0) as tax_7,
               nvl(sum(decode(tax_local_id, 7, adjustment_amount, 0)), 0) as tax_7_adjustment,
               contract_currency_id,
               company_currency_id,
               ls_cur_type
          from v_ls_monthly_tax_fx_vw lmt, currency_selection
         where to_char(gl_posting_mo_yr, ''yyyymm'') in
               (select filter_value
                  from pp_any_required_filter
                 where upper(trim(column_name)) = ''MONTHNUM'')
            AND lmt.ls_cur_type = currency_selection.ls_currency_type_id 
  and lmt.company_id in
               (select filter_value
                  from pp_any_required_filter
                 where upper(trim(column_name)) = ''COMPANY ID'')
         group by lmt.ls_asset_id,
         contract_currency_id,
               company_currency_id,
               ls_cur_type) taxes ON (la.ls_asset_id = taxes.ls_asset_id AND currency_selection.ls_currency_type_id = taxes.ls_cur_type)
 where to_char(la.company_id) in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''COMPANY ID'')
   and to_char(lpl.gl_posting_mo_yr, ''yyyymm'') in
       (select filter_value
          from pp_any_required_filter
         where upper(trim(column_name)) = ''MONTHNUM'')
 group by la.ls_asset_id,
          lpl.gl_posting_mo_yr,
          co.company_id,
          co.description,
          la.leased_asset_number,
          ilr.ilr_id,
          ilr.ilr_number,
          lct.description,
          al.long_description,
          ll.lease_id,
          ll.lease_number,
          tax_1,
          tax_1_adjustment,
          tax_2,
          tax_2_adjustment,
          tax_3,
          tax_3_adjustment,
          tax_4,
          tax_4_adjustment,
          tax_5,
          tax_5_adjustment,
          tax_6,
          tax_6_adjustment,
          tax_7,
          tax_7_adjustment,
          lpl.ls_cur_type,
          lpl.iso_code,
          lpl.currency_display_symbol,
          currency_selection.description      ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'company_description',
        4,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'company_id',
        3,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        1,
        2,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment1',
        37,
        1,
        1,
        '',
        'Contingent Adjustment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment10',
        55,
        1,
        1,
        '',
        'Contingent Adjustment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment2',
        39,
        1,
        1,
        '',
        'Contingent Adjustment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment3',
        41,
        1,
        1,
        '',
        'Contingent Adjustment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment4',
        43,
        1,
        1,
        '',
        'Contingent Adjustment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment5',
        45,
        1,
        1,
        '',
        'Contingent Adjustment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment6',
        47,
        1,
        1,
        '',
        'Contingent Adjustment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment7',
        49,
        1,
        1,
        '',
        'Contingent Adjustment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment8',
        51,
        1,
        1,
        '',
        'Contingent Adjustment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_adjustment9',
        53,
        1,
        1,
        '',
        'Contingent Adjustment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment1',
        36,
        1,
        1,
        '',
        'Contingent Payment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment10',
        54,
        1,
        1,
        '',
        'Contingent Payment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment2',
        38,
        1,
        1,
        '',
        'Contingent Payment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment3',
        40,
        1,
        1,
        '',
        'Contingent Payment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment4',
        42,
        1,
        1,
        '',
        'Contingent Payment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment5',
        44,
        1,
        1,
        '',
        'Contingent Payment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment6',
        46,
        1,
        1,
        '',
        'Contingent Payment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment7',
        48,
        1,
        1,
        '',
        'Contingent Payment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment8',
        50,
        1,
        1,
        '',
        'Contingent Payment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'contingent_payment9',
        52,
        1,
        1,
        '',
        'Contingent Payment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'currency',
        12,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_symbol',
        73,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1

FROM dual

UNION ALL

SELECT  l_id,
        'currency_type',
        72,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'ls_currency_type_id',
        1,
        2,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment1',
        17,
        1,
        1,
        '',
        'Executory Adjustment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment10',
        35,
        1,
        1,
        '',
        'Executory Adjustment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment2',
        19,
        1,
        1,
        '',
        'Executory Adjustment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment3',
        21,
        1,
        1,
        '',
        'Executory Adjustment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment4',
        23,
        1,
        1,
        '',
        'Executory Adjustment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment5',
        25,
        1,
        1,
        '',
        'Executory Adjustment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment6',
        27,
        1,
        1,
        '',
        'Executory Adjustment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment7',
        29,
        1,
        1,
        '',
        'Executory Adjustment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment8',
        31,
        1,
        1,
        '',
        'Executory Adjustment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_adjustment9',
        33,
        1,
        1,
        '',
        'Executory Adjustment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment1',
        16,
        1,
        1,
        '',
        'Executory Payment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment10',
        34,
        1,
        1,
        '',
        'Executory Payment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment2',
        18,
        1,
        1,
        '',
        'Executory Payment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment3',
        20,
        1,
        1,
        '',
        'Executory Payment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment4',
        22,
        1,
        1,
        '',
        'Executory Payment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment5',
        24,
        1,
        1,
        '',
        'Executory Payment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment6',
        26,
        1,
        1,
        '',
        'Executory Payment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment7',
        28,
        1,
        1,
        '',
        'Executory Payment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment8',
        30,
        1,
        1,
        '',
        'Executory Payment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'executory_payment9',
        32,
        1,
        1,
        '',
        'Executory Payment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_id',
        6,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ilr_number',
        7,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_adjustment',
        15,
        1,
        1,
        '',
        'Interest Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'interest_payment',
        14,
        1,
        1,
        '',
        'Interest Payment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_cap_type',
        10,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_id',
        8,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'lease_number',
        9,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'leased_asset_number',
        5,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'location',
        11,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'ls_asset_id',
        1,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'monthnum',
        2,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        2,
        'the_sort',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'principal_payment',
        13,
        1,
        1,
        '',
        'Principal Payment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'sales_proceeds',
        57,
        1,
        1,
        '',
        'Sales Proceeds',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_1',
        58,
        1,
        1,
        '',
        'Tax 1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_1_adjustment',
        59,
        1,
        1,
        '',
        'Tax 1 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_2',
        60,
        1,
        1,
        '',
        'Tax 2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_2_adjustment',
        61,
        1,
        1,
        '',
        'Tax 2 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_3',
        62,
        1,
        1,
        '',
        'Tax 3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_3_adjustment',
        63,
        1,
        1,
        '',
        'Tax 3 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_4',
        64,
        1,
        1,
        '',
        'Tax 4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_4_adjustment',
        65,
        1,
        1,
        '',
        'Tax 4 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_5',
        66,
        1,
        1,
        '',
        'Tax 5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_5_adjustment',
        67,
        1,
        1,
        '',
        'Tax 5 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_6',
        68,
        1,
        1,
        '',
        'Tax 6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_6_adjustment',
        69,
        1,
        1,
        '',
        'Tax 6 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_7',
        70,
        1,
        1,
        '',
        'Tax 7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'tax_7_adjustment',
        71,
        1,
        1,
        '',
        'Tax 7 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual

UNION ALL

SELECT  l_id,
        'termination_penalty',
        56,
        1,
        1,
        '',
        'Termination Penalty',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual
;

END;

/
