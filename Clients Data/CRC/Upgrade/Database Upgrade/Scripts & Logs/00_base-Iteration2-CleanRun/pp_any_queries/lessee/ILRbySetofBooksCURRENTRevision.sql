
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'ILR by Set of Books (CURRENT Revision)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := 1;
  
  l_query_sql := 'select cur.iso_code as currency, lct.description as currency_type, ll.ilr_id as ilr_id,    ll.ilr_number as ilr_number,   ll.revision as revision,
 (select a.description from approval_status a, ls_ilr_approval la where la.approval_status_id = a.approval_status_id and ll.ilr_id = la.ilr_id and ll.revision = la.revision) as revision_status,
(select s.description from set_of_books s where ll.set_of_books_id = s.set_of_books_id) as set_of_books,
c.description as company,   ll.net_present_value as net_present_value, ll.capital_cost as capital_cost,    ll.current_lease_cost as fair_market_value,
(select count(1) from ls_asset la where la.ilr_id = ll.ilr_id) as qty_number_of_assets,
cur.currency_display_symbol
from v_ls_ilr_header_fx_vw ll, company c, ls_lease ls, currency cur, ls_lease_currency_type lct
where ll.company_id = c.company_id
and ls.lease_id = ll.lease_id
and ll.currency_id = cur.currency_id
and lct.ls_currency_type_id = ll.ls_cur_type
and ll.revision = ll.current_revision
order by 2, 3          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'capital_cost',
        9,
        1,
        1,
        '',
        'Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        7,
        0,
        1,
        '',
        'Company',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        1,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_display_symbol',
        2,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_type',
        12,
        0,
        1,
        '',
        'Currency Type',
        300,
        'description',
        'ls_lease_currency_type',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        '',
        1,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'fair_market_value',
        10,
        1,
        1,
        '',
        'Fair Market Value',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        3,
        0,
        1,
        '',
        'ILR Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'net_present_value',
        8,
        1,
        1,
        '',
        'Net Present Value',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'qty_number_of_assets',
        11,
        0,
        1,
        '',
        'Qty Number Of Assets',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'revision',
        4,
        0,
        1,
        '',
        'Revision',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        6,
        0,
        1,
        '',
        'Set Of Books',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'revision_status',
        5,
        0,
        1,
        '',
        'Revision Status',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/