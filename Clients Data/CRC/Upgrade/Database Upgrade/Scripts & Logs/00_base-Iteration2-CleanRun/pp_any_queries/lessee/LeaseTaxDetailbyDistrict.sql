
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Tax Detail by District';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, tax.set_of_books_id, to_char(tax.gl_posting_mo_yr,''yyyymm'') as monthnum, la.description as asset_description,
       la.company_id, co.description as company_description, ilr.ilr_id,
       ilr.ilr_number, ll.lease_id, ll.lease_number, lct.description as lease_cap_type,
       al.long_description as location, al.EXT_ASSET_LOCATION as external_location_code,county.description as county, al.state_id as state,
       ltl.description as local_tax_type, decode(is_number(tax.tax_district_id), -1, tax.tax_district_id, td.description) as tax_district,
       ltl.pay_lessor,
       tax.tax_base as tax_base, tax.rate,
       nvl(tax.amount,0) as calc_amount, nvl(tax.adjustment_amount,0) as adjustment_amount,
       nvl(tax.amount,0) + nvl(tax.adjustment_amount,0) as total_tax
from ls_asset la, ls_ilr ilr, ls_lease ll,
     ls_monthly_tax tax, ls_tax_local ltl, company co,
     tax_district td, asset_location al, county,
     ls_ilr_options ilro, ls_lease_cap_type lct
where la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and tax.ls_asset_id = la.ls_asset_id
  and tax.accrual = 0
  and tax.tax_local_id = ltl.tax_local_id
  and tax.amount <> 0 and nvl(tax.rate,0) <> 0
  and case when is_number(tax.tax_district_id) = 1 then to_number(tax.tax_district_id) else -2 end = td.tax_district_id (+) /* outer join because converted data didn''t have tax district detail */
  and la.company_id = co.company_id
  and la.asset_location_id = al.asset_location_id
  and al.county_id = county.county_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and co.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  and to_char(tax.gl_posting_mo_yr,''yyyymm'') in (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'')
order by 1

          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'adjustment_amount',
        23,
        1,
        1,
        '',
        'Adjustment Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'asset_description',
        5,
        0,
        1,
        '',
        'Asset Description',
        300,
        'description',
        'ls_asset',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'calc_amount',
        22,
        1,
        1,
        '',
        'Calc Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_description',
        7,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        6,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'county',
        15,
        0,
        1,
        '',
        'County',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_location_code',
        14,
        0,
        1,
        '',
        'External Location Code',
        300,
        'ext_asset_location',
        'asset_location',
        'VARCHAR2',
        0,
        'EXT_ASSET_LOCATION',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        8,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        9,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type',
        12,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_id',
        10,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        11,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'leased_asset_number',
        2,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'local_tax_type',
        17,
        0,
        1,
        '',
        'Local Tax Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'location',
        13,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        1,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'monthnum',
        4,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        2,
        'the_sort',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'pay_lessor',
        19,
        0,
        1,
        '',
        'Pay Lessor',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'rate',
        21,
        0,
        1,
        '',
        'Rate',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books_id',
        3,
        null,
        1,
        '',
        'Set of Books ID',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        null,
        'set_of_books_id',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'state',
        16,
        0,
        1,
        '',
        'State',
        300,
        'state_id',
        'state',
        'VARCHAR2',
        0,
        'state_id',
        null,
        null,
        'state_id',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_base',
        20,
        1,
        1,
        '',
        'Tax Base',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_district',
        18,
        0,
        1,
        '',
        'Tax District',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'total_tax',
        24,
        1,
        1,
        '',
        'Total Tax',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/