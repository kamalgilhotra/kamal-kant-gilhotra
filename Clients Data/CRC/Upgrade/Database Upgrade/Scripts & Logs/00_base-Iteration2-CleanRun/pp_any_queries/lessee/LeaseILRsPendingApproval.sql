DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN
  l_query_description := 'Lease ILR''s Pending Approval';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  l_query_sql := '  select
	pt.ilr_id as ilr_id,
	pt.ilr_number as ilr_number,
	c.description as company,
	pt.revision,
  u.last_name || '', '' || u.first_name as sending_user,
	aps.description as approval_status,
	sob.description as set_of_books,
  amounts.current_lease_cost as current_lease_cost,
	amounts.CAPITAL_COST as Capital_cost,
	decode(current_lease_cost, 0, 0, round(100 * amounts.Capital_cost/amounts.current_lease_cost,2)) as capital_percent
from workflow wf, company c, approval_status aps,
	(select distinct p.ilr_id, p.revision, i.ilr_number, i.company_id, p.error_message
	from ls_pend_transaction p, ls_ilr i where i.ilr_id = p.ilr_id) pt, ls_ilr_amounts_set_of_books amounts, set_of_books sob, pp_security_users u
where wf.subsystem = ''lessee_ilr_approval''
and wf.id_field1 = to_char(pt.ilr_id)
and wf.id_field2 = to_char(pt.revision)
and c.company_id = pt.company_id
and wf.approval_status_id not in (1,3,6)
and amounts.ilr_id = pt.ilr_id
and amounts.revision = pt.revision
and amounts.set_of_books_id = sob.set_of_books_id
and wf.approval_status_id = aps.approval_status_id
and lower(trim(user_sent)) = lower(trim(u.users))          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'approval_status',
        6,
        0,
        1,
        '',
        'Approval Status',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'capital_cost',
        9,
        1,
        1,
        '',
        'Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'capital_percent',
        10,
        0,
        1,
        '',
        'Capital Percent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company',
        3,
        0,
        1,
        '',
        'Company',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'current_lease_cost',
        8,
        1,
        1,
        '',
        'Current Lease Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        1,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        2,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'revision',
        4,
        0,
        1,
        '',
        'Revision',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sending_user',
        5,
        0,
        1,
        '',
        'Sending User',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        7,
        0,
        1,
        '',
        'Set Of Books',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/