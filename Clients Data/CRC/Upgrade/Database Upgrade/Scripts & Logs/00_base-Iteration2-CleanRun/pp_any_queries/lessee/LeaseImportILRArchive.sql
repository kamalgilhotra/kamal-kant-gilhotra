
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Import ILR Archive';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.TIME_STAMP,arc.USER_ID,arc.ILR_ID,arc.ILR_NUMBER,arc.LEASE_XLATE,arc.LEASE_ID,arc.COMPANY_XLATE,arc.COMPANY_ID,arc.EST_IN_SVC_DATE,arc.ILR_GROUP_XLATE,
arc.ILR_GROUP_ID,arc.WORKFLOW_TYPE_XLATE,arc.WORKFLOW_TYPE_ID,arc.EXTERNAL_ILR,arc.NOTES,arc.INCEPTION_AIR,arc.PURCHASE_OPTION_TYPE_XLATE,arc.PURCHASE_OPTION_TYPE_ID,arc.PURCHASE_OPTION_AMT,arc.RENEWAL_OPTION_TYPE_XLATE,arc.RENEWAL_OPTION_TYPE_ID,
arc.CANCELABLE_TYPE_XLATE,arc.CANCELABLE_TYPE_ID,arc.ITC_SW,arc.PARTIAL_RETIRE_SW,arc.SUBLET_SW,arc.MUNI_BO_SW,arc.LEASE_CAP_TYPE_XLATE,arc.LEASE_CAP_TYPE_ID,arc.TERMINATION_AMT,arc.PAYMENT_TERM_ID,arc.PAYMENT_TERM_DATE,arc.PAYMENT_FREQ_XLATE,arc.PAYMENT_FREQ_ID,
arc.NUMBER_OF_TERMS,arc.PAID_AMOUNT,arc.EST_EXECUTORY_COST,arc.CONTINGENT_AMOUNT,arc.CLASS_CODE_XLATE1,arc.CLASS_CODE_ID1,arc.CLASS_CODE_VALUE1,arc.CLASS_CODE_XLATE2,arc.CLASS_CODE_ID2,arc.CLASS_CODE_VALUE2,arc.CLASS_CODE_XLATE3,arc.CLASS_CODE_ID3,arc.CLASS_CODE_VALUE3,
arc.CLASS_CODE_XLATE4,arc.CLASS_CODE_ID4,arc.CLASS_CODE_VALUE4,arc.CLASS_CODE_XLATE5,arc.CLASS_CODE_ID5,arc.CLASS_CODE_VALUE5,arc.CLASS_CODE_XLATE6,arc.CLASS_CODE_ID6,arc.CLASS_CODE_VALUE6,arc.CLASS_CODE_XLATE7,arc.CLASS_CODE_ID7,arc.CLASS_CODE_VALUE7,arc.CLASS_CODE_XLATE8,
arc.CLASS_CODE_ID8,arc.CLASS_CODE_VALUE8,arc.CLASS_CODE_XLATE9,arc.CLASS_CODE_ID9,arc.CLASS_CODE_VALUE9,arc.CLASS_CODE_XLATE10,arc.CLASS_CODE_ID10,arc.CLASS_CODE_VALUE10,arc.CLASS_CODE_XLATE11,arc.CLASS_CODE_ID11,arc.CLASS_CODE_VALUE11,arc.CLASS_CODE_XLATE12,arc.CLASS_CODE_ID12,
arc.CLASS_CODE_VALUE12,arc.CLASS_CODE_XLATE13,arc.CLASS_CODE_ID13,arc.CLASS_CODE_VALUE13,arc.CLASS_CODE_XLATE14,arc.CLASS_CODE_ID14,arc.CLASS_CODE_VALUE14,arc.CLASS_CODE_XLATE15,arc.CLASS_CODE_ID15,arc.CLASS_CODE_VALUE15,arc.CLASS_CODE_XLATE16,arc.CLASS_CODE_ID16,
arc.CLASS_CODE_VALUE16,arc.CLASS_CODE_XLATE17,arc.CLASS_CODE_ID17,arc.CLASS_CODE_VALUE17,arc.CLASS_CODE_XLATE18,arc.CLASS_CODE_ID18,arc.CLASS_CODE_VALUE18,arc.CLASS_CODE_XLATE19,arc.CLASS_CODE_ID19,arc.CLASS_CODE_VALUE19,arc.CLASS_CODE_XLATE20,arc.CLASS_CODE_ID20,
arc.CLASS_CODE_VALUE20,arc.LOADED,arc.IS_MODIFIED,arc.ERROR_MESSAGE,arc.UNIQUE_ILR_IDENTIFIER,arc.C_BUCKET_1,arc.C_BUCKET_2,arc.C_BUCKET_3,arc.C_BUCKET_4,arc.C_BUCKET_5,arc.C_BUCKET_6,arc.C_BUCKET_7,arc.C_BUCKET_8,arc.C_BUCKET_9,arc.C_BUCKET_10,arc.E_BUCKET_1,arc.E_BUCKET_2,
arc.E_BUCKET_3,arc.E_BUCKET_4,arc.E_BUCKET_5,arc.E_BUCKET_6,arc.E_BUCKET_7,arc.E_BUCKET_8,arc.E_BUCKET_9,arc.E_BUCKET_10,arc.INT_ACCRUAL_ACCOUNT_XLATE,arc.INT_ACCRUAL_ACCOUNT_ID,arc.INT_EXPENSE_ACCOUNT_XLATE,arc.INT_EXPENSE_ACCOUNT_ID,arc.EXEC_ACCRUAL_ACCOUNT_XLATE,
arc.EXEC_ACCRUAL_ACCOUNT_ID,arc.EXEC_EXPENSE_ACCOUNT_XLATE,arc.EXEC_EXPENSE_ACCOUNT_ID,arc.CONT_ACCRUAL_ACCOUNT_XLATE,arc.CONT_ACCRUAL_ACCOUNT_ID,arc.CONT_EXPENSE_ACCOUNT_XLATE,arc.CONT_EXPENSE_ACCOUNT_ID,arc.CAP_ASSET_ACCOUNT_XLATE,arc.CAP_ASSET_ACCOUNT_ID,
arc.ST_OBLIG_ACCOUNT_XLATE,arc.ST_OBLIG_ACCOUNT_ID,arc.LT_OBLIG_ACCOUNT_XLATE,arc.LT_OBLIG_ACCOUNT_ID,arc.AP_ACCOUNT_XLATE,arc.AP_ACCOUNT_ID,arc.RES_DEBIT_ACCOUNT_XLATE,arc.RES_DEBIT_ACCOUNT_ID,arc.RES_CREDIT_ACCOUNT_XLATE,arc.RES_CREDIT_ACCOUNT_ID,arc.FUNDING_STATUS_ID,arc.FUNDING_STATUS_XLATE
  from pp_import_run run, ls_import_ilr_archive arc
  where run.import_run_id = arc.import_run_id          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'ap_account_id',
        145,
        0,
        1,
        '',
        'Ap Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ap_account_xlate',
        144,
        0,
        1,
        '',
        'Ap Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_1',
        106,
        0,
        1,
        '',
        'C Bucket 1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_10',
        115,
        0,
        1,
        '',
        'C Bucket 10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_2',
        107,
        0,
        1,
        '',
        'C Bucket 2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_3',
        108,
        0,
        1,
        '',
        'C Bucket 3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_4',
        109,
        0,
        1,
        '',
        'C Bucket 4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_5',
        110,
        0,
        1,
        '',
        'C Bucket 5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_6',
        111,
        0,
        1,
        '',
        'C Bucket 6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_7',
        112,
        0,
        1,
        '',
        'C Bucket 7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_8',
        113,
        0,
        1,
        '',
        'C Bucket 8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'c_bucket_9',
        114,
        0,
        1,
        '',
        'C Bucket 9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cancelable_type_id',
        26,
        0,
        1,
        '',
        'Cancelable Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cancelable_type_xlate',
        25,
        0,
        1,
        '',
        'Cancelable Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cap_asset_account_id',
        139,
        0,
        1,
        '',
        'Cap Asset Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cap_asset_account_xlate',
        138,
        0,
        1,
        '',
        'Cap Asset Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id1',
        43,
        0,
        1,
        '',
        'Class Code Id1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id10',
        70,
        0,
        1,
        '',
        'Class Code Id10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id11',
        73,
        0,
        1,
        '',
        'Class Code Id11',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id12',
        76,
        0,
        1,
        '',
        'Class Code Id12',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id13',
        79,
        0,
        1,
        '',
        'Class Code Id13',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id14',
        82,
        0,
        1,
        '',
        'Class Code Id14',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id15',
        85,
        0,
        1,
        '',
        'Class Code Id15',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id16',
        88,
        0,
        1,
        '',
        'Class Code Id16',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id17',
        91,
        0,
        1,
        '',
        'Class Code Id17',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id18',
        94,
        0,
        1,
        '',
        'Class Code Id18',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id19',
        97,
        0,
        1,
        '',
        'Class Code Id19',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id2',
        46,
        0,
        1,
        '',
        'Class Code Id2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id20',
        100,
        0,
        1,
        '',
        'Class Code Id20',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id3',
        49,
        0,
        1,
        '',
        'Class Code Id3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id4',
        52,
        0,
        1,
        '',
        'Class Code Id4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id5',
        55,
        0,
        1,
        '',
        'Class Code Id5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id6',
        58,
        0,
        1,
        '',
        'Class Code Id6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id7',
        61,
        0,
        1,
        '',
        'Class Code Id7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id8',
        64,
        0,
        1,
        '',
        'Class Code Id8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id9',
        67,
        0,
        1,
        '',
        'Class Code Id9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value1',
        44,
        0,
        1,
        '',
        'Class Code Value1',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value10',
        71,
        0,
        1,
        '',
        'Class Code Value10',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value11',
        74,
        0,
        1,
        '',
        'Class Code Value11',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value12',
        77,
        0,
        1,
        '',
        'Class Code Value12',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value13',
        80,
        0,
        1,
        '',
        'Class Code Value13',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value14',
        83,
        0,
        1,
        '',
        'Class Code Value14',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value15',
        86,
        0,
        1,
        '',
        'Class Code Value15',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value16',
        89,
        0,
        1,
        '',
        'Class Code Value16',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value17',
        92,
        0,
        1,
        '',
        'Class Code Value17',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value18',
        95,
        0,
        1,
        '',
        'Class Code Value18',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value19',
        98,
        0,
        1,
        '',
        'Class Code Value19',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value2',
        47,
        0,
        1,
        '',
        'Class Code Value2',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value20',
        101,
        0,
        1,
        '',
        'Class Code Value20',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value3',
        50,
        0,
        1,
        '',
        'Class Code Value3',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value4',
        53,
        0,
        1,
        '',
        'Class Code Value4',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value5',
        56,
        0,
        1,
        '',
        'Class Code Value5',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value6',
        59,
        0,
        1,
        '',
        'Class Code Value6',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value7',
        62,
        0,
        1,
        '',
        'Class Code Value7',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value8',
        65,
        0,
        1,
        '',
        'Class Code Value8',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value9',
        68,
        0,
        1,
        '',
        'Class Code Value9',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate1',
        42,
        0,
        1,
        '',
        'Class Code Xlate1',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate10',
        69,
        0,
        1,
        '',
        'Class Code Xlate10',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate11',
        72,
        0,
        1,
        '',
        'Class Code Xlate11',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate12',
        75,
        0,
        1,
        '',
        'Class Code Xlate12',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate13',
        78,
        0,
        1,
        '',
        'Class Code Xlate13',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate14',
        81,
        0,
        1,
        '',
        'Class Code Xlate14',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate15',
        84,
        0,
        1,
        '',
        'Class Code Xlate15',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate16',
        87,
        0,
        1,
        '',
        'Class Code Xlate16',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate17',
        90,
        0,
        1,
        '',
        'Class Code Xlate17',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate18',
        93,
        0,
        1,
        '',
        'Class Code Xlate18',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate19',
        96,
        0,
        1,
        '',
        'Class Code Xlate19',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate2',
        45,
        0,
        1,
        '',
        'Class Code Xlate2',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate20',
        99,
        0,
        1,
        '',
        'Class Code Xlate20',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate3',
        48,
        0,
        1,
        '',
        'Class Code Xlate3',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate4',
        51,
        0,
        1,
        '',
        'Class Code Xlate4',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate5',
        54,
        0,
        1,
        '',
        'Class Code Xlate5',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate6',
        57,
        0,
        1,
        '',
        'Class Code Xlate6',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate7',
        60,
        0,
        1,
        '',
        'Class Code Xlate7',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate8',
        63,
        0,
        1,
        '',
        'Class Code Xlate8',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate9',
        66,
        0,
        1,
        '',
        'Class Code Xlate9',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        11,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_xlate',
        10,
        0,
        1,
        '',
        'Company Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cont_accrual_account_id',
        135,
        1,
        1,
        '',
        'Cont Accrual Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cont_accrual_account_xlate',
        134,
        0,
        1,
        '',
        'Cont Accrual Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cont_expense_account_id',
        137,
        0,
        1,
        '',
        'Cont Expense Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cont_expense_account_xlate',
        136,
        0,
        1,
        '',
        'Cont Expense Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_amount',
        41,
        1,
        1,
        '',
        'Contingent Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_1',
        116,
        0,
        1,
        '',
        'E Bucket 1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_10',
        125,
        0,
        1,
        '',
        'E Bucket 10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_2',
        117,
        0,
        1,
        '',
        'E Bucket 2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_3',
        118,
        0,
        1,
        '',
        'E Bucket 3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_4',
        119,
        0,
        1,
        '',
        'E Bucket 4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_5',
        120,
        0,
        1,
        '',
        'E Bucket 5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_6',
        121,
        0,
        1,
        '',
        'E Bucket 6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_7',
        122,
        0,
        1,
        '',
        'E Bucket 7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_8',
        123,
        0,
        1,
        '',
        'E Bucket 8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'e_bucket_9',
        124,
        0,
        1,
        '',
        'E Bucket 9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'error_message',
        104,
        0,
        1,
        '',
        'Error Message',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'est_executory_cost',
        40,
        1,
        1,
        '',
        'Est Executory Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'est_in_svc_date',
        12,
        0,
        1,
        '',
        'Est In Svc Date',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'exec_accrual_account_id',
        131,
        1,
        1,
        '',
        'Exec Accrual Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'exec_accrual_account_xlate',
        130,
        0,
        1,
        '',
        'Exec Accrual Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'exec_expense_account_id',
        133,
        0,
        1,
        '',
        'Exec Expense Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'exec_expense_account_xlate',
        132,
        0,
        1,
        '',
        'Exec Expense Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_ilr',
        17,
        0,
        1,
        '',
        'External Ilr',
        300,
        'external_ilr',
        'ls_ilr',
        'VARCHAR2',
        0,
        'external_ilr',
        null,
        null,
        'external_ilr',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'funding_status_id',
        150,
        0,
        1,
        '',
        'Funding Status Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'funding_status_xlate',
        151,
        0,
        1,
        '',
        'Funding Status Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_group_id',
        14,
        0,
        1,
        '',
        'Ilr Group Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_group_xlate',
        13,
        0,
        1,
        '',
        'Ilr Group Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        6,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        7,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_description',
        1,
        0,
        1,
        '',
        'Import Run Description',
        300,
        'description',
        '(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 252))',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_id',
        2,
        0,
        1,
        '',
        'Import Run Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'inception_air',
        19,
        0,
        1,
        '',
        'Inception Air',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'int_accrual_account_id',
        127,
        1,
        1,
        '',
        'Int Accrual Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'int_accrual_account_xlate',
        126,
        0,
        1,
        '',
        'Int Accrual Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'int_expense_account_id',
        129,
        0,
        1,
        '',
        'Int Expense Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'int_expense_account_xlate',
        128,
        0,
        1,
        '',
        'Int Expense Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'is_modified',
        103,
        0,
        1,
        '',
        'Is Modified',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'itc_sw',
        27,
        0,
        1,
        '',
        'Itc Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type_id',
        32,
        0,
        1,
        '',
        'Lease Cap Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type_xlate',
        31,
        0,
        1,
        '',
        'Lease Cap Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_id',
        9,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_xlate',
        8,
        0,
        1,
        '',
        'Lease Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'line_id',
        3,
        0,
        1,
        '',
        'Line Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'loaded',
        102,
        0,
        1,
        '',
        'Loaded',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lt_oblig_account_id',
        143,
        0,
        1,
        '',
        'Lt Oblig Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lt_oblig_account_xlate',
        142,
        0,
        1,
        '',
        'Lt Oblig Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'muni_bo_sw',
        30,
        0,
        1,
        '',
        'Muni Bo Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'notes',
        18,
        0,
        1,
        '',
        'Notes',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'number_of_terms',
        38,
        0,
        1,
        '',
        'Number Of Terms',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'paid_amount',
        39,
        1,
        1,
        '',
        'Paid Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'partial_retire_sw',
        28,
        0,
        1,
        '',
        'Partial Retire Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_freq_id',
        37,
        1,
        1,
        '',
        'Payment Freq Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_freq_xlate',
        36,
        0,
        1,
        '',
        'Payment Freq Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_term_date',
        35,
        0,
        1,
        '',
        'Payment Term Date',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_term_id',
        34,
        1,
        1,
        '',
        'Payment Term Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'purchase_option_amt',
        22,
        0,
        1,
        '',
        'Purchase Option Amt',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'purchase_option_type_id',
        21,
        0,
        1,
        '',
        'Purchase Option Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'purchase_option_type_xlate',
        20,
        0,
        1,
        '',
        'Purchase Option Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'renewal_option_type_id',
        24,
        0,
        1,
        '',
        'Renewal Option Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'renewal_option_type_xlate',
        23,
        0,
        1,
        '',
        'Renewal Option Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'res_credit_account_id',
        149,
        0,
        1,
        '',
        'Res Credit Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'res_credit_account_xlate',
        148,
        0,
        1,
        '',
        'Res Credit Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'res_debit_account_id',
        147,
        0,
        1,
        '',
        'Res Debit Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'res_debit_account_xlate',
        146,
        0,
        1,
        '',
        'Res Debit Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'st_oblig_account_id',
        141,
        0,
        1,
        '',
        'St Oblig Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'st_oblig_account_xlate',
        140,
        0,
        1,
        '',
        'St Oblig Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sublet_sw',
        29,
        0,
        1,
        '',
        'Sublet Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'termination_amt',
        33,
        0,
        1,
        '',
        'Termination Amt',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'time_stamp',
        4,
        0,
        1,
        '',
        'Time Stamp',
        300,
        '',
        '',
        'DATE',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'unique_ilr_identifier',
        105,
        0,
        1,
        '',
        'Unique Ilr Identifier',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'user_id',
        5,
        0,
        1,
        '',
        'User Id',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'workflow_type_id',
        16,
        0,
        1,
        '',
        'Workflow Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'workflow_type_xlate',
        15,
        0,
        1,
        '',
        'Workflow Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/