DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Asset Schedule by ILR';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select la.ls_asset_id, la.leased_asset_number, co.company_id, co.description as company_description, ilr.ilr_id, ilr.ilr_number,
     ll.lease_id, ll.lease_number, lct.description as lease_cap_type, al.long_description as location, las.REVISION, las.SET_OF_BOOKS_ID,
     to_char(las.MONTH, ''yyyymm'') as monthnum, las.BEG_CAPITAL_COST, las.END_CAPITAL_COST, las.BEG_OBLIGATION, las.END_OBLIGATION,
     las.BEG_LT_OBLIGATION, las.END_LT_OBLIGATION, las.INTEREST_ACCRUAL, las.PRINCIPAL_ACCRUAL, las.INTEREST_PAID, las.PRINCIPAL_PAID,
     las.EXECUTORY_ACCRUAL1, las.EXECUTORY_ACCRUAL2, las.EXECUTORY_ACCRUAL3, las.EXECUTORY_ACCRUAL4, las.EXECUTORY_ACCRUAL5, las.EXECUTORY_ACCRUAL6,
     las.EXECUTORY_ACCRUAL7, las.EXECUTORY_ACCRUAL8, las.EXECUTORY_ACCRUAL9, las.EXECUTORY_ACCRUAL10, las.EXECUTORY_PAID1, las.EXECUTORY_PAID2,
     las.EXECUTORY_PAID3, las.EXECUTORY_PAID4, las.EXECUTORY_PAID5, las.EXECUTORY_PAID6, las.EXECUTORY_PAID7, las.EXECUTORY_PAID8, las.EXECUTORY_PAID9,
     las.EXECUTORY_PAID10, las.CONTINGENT_ACCRUAL1, las.CONTINGENT_ACCRUAL2, las.CONTINGENT_ACCRUAL3, las.CONTINGENT_ACCRUAL4, las.CONTINGENT_ACCRUAL5,
     las.CONTINGENT_ACCRUAL6, las.CONTINGENT_ACCRUAL7, las.CONTINGENT_ACCRUAL8, las.CONTINGENT_ACCRUAL9, las.CONTINGENT_ACCRUAL10, las.CONTINGENT_PAID1,
     las.CONTINGENT_PAID2, las.CONTINGENT_PAID3, las.CONTINGENT_PAID4, las.CONTINGENT_PAID5, las.CONTINGENT_PAID6, las.CONTINGENT_PAID7, las.CONTINGENT_PAID8,
     las.CONTINGENT_PAID9, las.CONTINGENT_PAID10,las.IS_OM, las.CURRENT_LEASE_COST, las.RESIDUAL_AMOUNT, las.TERM_PENALTY, las.BPO_PRICE,
     las.BEG_DEFERRED_RENT, las.DEFERRED_RENT, las.END_DEFERRED_RENT, las.BEG_LIABILITY, las.END_LIABILITY, las.BEG_LT_LIABILITY, las.END_LT_LIABILITY,
     las.principal_remeasurement, las.liability_remeasurement, las.beg_net_rou_asset, las.rou_asset_remeasurement, las.end_net_rou_asset, 
     las.initial_direct_cost, las.incentive_amount, las.beg_prepaid_rent, las.prepay_amortization, las.prepaid_rent, las.end_prepaid_rent, 
     las.beg_arrears_accrual, las.arrears_accrual, las.end_arrears_accrual,las.executory_adjust, las.contingent_adjust, 
     ldf.begin_reserve, ldf.depr_expense, ldf.depr_exp_alloc_adjust, ldf.end_reserve, las.begin_accum_impair, las.impairment_activity, las.end_accum_impair
     from ls_asset la, ls_ilr ilr, ls_lease ll, company co, asset_location al, ls_lease_cap_type lct, ls_ilr_options ilro, ls_asset_schedule las, ls_depr_forecast ldf
     where la.ilr_id = ilr.ilr_id and ilr.lease_id = ll.lease_id and ilr.ilr_id = ilro.ilr_id and ilr.current_revision = ilro.revision
     and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id and la.asset_location_id = al.asset_location_id and la.company_id = co.company_id
     and las.ls_asset_id = la.ls_asset_id and las.revision = la.approved_revision
     and ldf.ls_asset_id (+) = las.ls_asset_id and ldf.revision (+) = las.revision
     and ldf.set_of_books_id (+) = las.set_of_books_id and ldf.month (+) = las.month
     and upper(ilr.ilr_number) in (select upper(filter_value) from pp_any_required_filter where upper(trim(column_name)) = ''ILR NUMBER'')  ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'ls_asset_id', 1, 0, 1, null, 'Ls Asset Id', 300, 'leased_asset_number', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'leased_asset_number', 2, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', null, null, 'leased_asset_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_id', 3, 0, 1, null, 'Company Id', 300, 'description', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'company_description', 4, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_id', 5, 0, 1, null, 'Ilr Id', 300, 'ilr_number', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'ilr_number', 6, 0, 1, null, 'Ilr Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', 1, 2, 'ilr_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_id', 7, 0, 1, null, 'Lease Id', 300, 'lease_number', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_number', 8, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', null, null, 'lease_number', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'lease_cap_type', 9, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, 'description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'location', 10, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, 'long_description', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'revision', 11, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'set_of_books_id', 12, 0, 1, null, 'Set Of Books Id', 300, 'description', 'set_of_books', 'NUMBER', 0, 'set_of_books_id', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'monthnum', 13, 0, 1, null, 'Monthnum', 300, 'month_number', '(select month_number from pp_calendar)', 'VARCHAR2', 0, 'month_number', null, null, 'the_sort', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_capital_cost', 14, 1, 1, null, 'Beg Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_capital_cost', 15, 1, 1, null, 'End Capital Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_obligation', 16, 1, 1, null, 'Beg Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_obligation', 17, 1, 1, null, 'End Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_obligation', 18, 1, 1, null, 'Beg Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_obligation', 19, 1, 1, null, 'End Lt Obligation', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_accrual', 20, 1, 1, null, 'Interest Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_accrual', 21, 1, 1, null, 'Principal Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'interest_paid', 22, 1, 1, null, 'Interest Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_paid', 23, 1, 1, null, 'Principal Paid', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual1', 24, 1, 1, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual2', 25, 1, 1, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual3', 26, 1, 1, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual4', 27, 1, 1, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual5', 28, 1, 1, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual6', 29, 1, 1, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual7', 30, 1, 1, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual8', 31, 1, 1, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual9', 32, 1, 1, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_accrual10', 33, 1, 1, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid1', 34, 1, 1, null, 'Executory Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid2', 35, 1, 1, null, 'Executory Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid3', 36, 1, 1, null, 'Executory Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid4', 37, 1, 1, null, 'Executory Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid5', 38, 1, 1, null, 'Executory Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid6', 39, 1, 1, null, 'Executory Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid7', 40, 1, 1, null, 'Executory Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid8', 41, 1, 1, null, 'Executory Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid9', 42, 1, 1, null, 'Executory Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_paid10', 43, 1, 1, null, 'Executory Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual1', 44, 1, 1, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual2', 45, 1, 1, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual3', 46, 1, 1, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual4', 47, 1, 1, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual5', 48, 1, 1, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual6', 49, 1, 1, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual7', 50, 1, 1, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual8', 51, 1, 1, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual9', 52, 1, 1, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_accrual10', 53, 1, 1, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid1', 54, 1, 1, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid2', 55, 1, 1, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid3', 56, 1, 1, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid4', 57, 1, 1, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid5', 58, 1, 1, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid6', 59, 1, 1, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid7', 60, 1, 1, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid8', 61, 1, 1, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid9', 62, 1, 1, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_paid10', 63, 1, 1, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'is_om', 64, 0, 1, null, 'Is Om', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'current_lease_cost', 65, 1, 1, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'residual_amount', 66, 1, 1, null, 'Residual Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'term_penalty', 67, 1, 1, null, 'Term Penalty', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'bpo_price', 68, 1, 1, null, 'Bpo Price', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_deferred_rent', 69, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'deferred_rent', 70, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_deferred_rent', 71, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_liability', 72, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_lt_liability', 73, 1, 1, null, 'Beg Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_liability', 74, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_lt_liability', 75, 1, 1, null, 'End Lt Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'principal_remeasurement', 76, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'liability_remeasurement', 77, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_net_rou_asset', 78, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'rou_asset_remeasurement', 79, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_net_rou_asset', 80, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'initial_direct_cost', 81, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'incentive_amount', 82, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_prepaid_rent', 83, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepay_amortization', 84, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'prepaid_rent', 85, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_prepaid_rent', 86, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'beg_arrears_accrual', 87, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'arrears_accrual', 88, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_arrears_accrual', 89, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'executory_adjust', 90, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'contingent_adjust', 91, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'begin_reserve', 92, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_expense', 93, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'depr_exp_alloc_adjust', 94, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_reserve', 95, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 96, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'impairment_activity', 97, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL 
SELECT  l_id, 'end_accum_impair', 98, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual 
;

END;

/


COMMIT;