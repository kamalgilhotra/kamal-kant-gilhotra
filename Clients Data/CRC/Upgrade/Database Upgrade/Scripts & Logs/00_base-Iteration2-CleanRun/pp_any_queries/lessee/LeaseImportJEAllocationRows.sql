
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Import JE Allocation Rows';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.TIME_STAMP,arc.USER_ID,arc.LS_ASSET_XLATE,arc.LS_ASSET_ID,arc.ILR_XLATE,arc.ILR_ID,arc.COMPANY_XLATE,arc.COMPANY_ID,arc.TRANS_TYPE_XLATE,
arc.TRANS_TYPE,arc.TAX_LOCAL_XLATE,arc.TAX_LOCAL_ID,arc.PERCENT,arc.ELEMENT_1,arc.ELEMENT_2,arc.ELEMENT_3,arc.ELEMENT_4,arc.ELEMENT_5,arc.ELEMENT_6,arc.ELEMENT_7,arc.ELEMENT_8,arc.ELEMENT_9,arc.ELEMENT_10,arc.ELEMENT_11,arc.ELEMENT_12,
arc.ELEMENT_13,arc.ELEMENT_14,arc.ELEMENT_15,arc.ELEMENT_16,arc.ELEMENT_17,arc.ELEMENT_18,arc.ELEMENT_19,arc.ELEMENT_20,arc.ELEMENT_21,arc.ELEMENT_22,arc.ELEMENT_23,arc.ELEMENT_24,arc.ELEMENT_25,arc.ELEMENT_26,arc.ELEMENT_27,arc.ELEMENT_28,
arc.ELEMENT_29,arc.ELEMENT_30,arc.LOADED,arc.IS_MODIFIED,arc.ERROR_MESSAGE,arc.LS_ASSET_OVERRIDE
  from pp_import_run run, ls_import_alloc_rows_archive arc
  where run.import_run_id = arc.import_run_id          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'company_id',
        11,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_xlate',
        10,
        0,
        1,
        '',
        'Company Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_1',
        17,
        0,
        1,
        '',
        'Element 1',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_10',
        26,
        0,
        1,
        '',
        'Element 10',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_11',
        27,
        0,
        1,
        '',
        'Element 11',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_12',
        28,
        0,
        1,
        '',
        'Element 12',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_13',
        29,
        0,
        1,
        '',
        'Element 13',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_14',
        30,
        0,
        1,
        '',
        'Element 14',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_15',
        31,
        0,
        1,
        '',
        'Element 15',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_16',
        32,
        0,
        1,
        '',
        'Element 16',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_17',
        33,
        0,
        1,
        '',
        'Element 17',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_18',
        34,
        0,
        1,
        '',
        'Element 18',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_19',
        35,
        0,
        1,
        '',
        'Element 19',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_2',
        18,
        0,
        1,
        '',
        'Element 2',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_20',
        36,
        0,
        1,
        '',
        'Element 20',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_21',
        37,
        0,
        1,
        '',
        'Element 21',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_22',
        38,
        0,
        1,
        '',
        'Element 22',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_23',
        39,
        0,
        1,
        '',
        'Element 23',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_24',
        40,
        0,
        1,
        '',
        'Element 24',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_25',
        41,
        0,
        1,
        '',
        'Element 25',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_26',
        42,
        0,
        1,
        '',
        'Element 26',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_27',
        43,
        0,
        1,
        '',
        'Element 27',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_28',
        44,
        0,
        1,
        '',
        'Element 28',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_29',
        45,
        0,
        1,
        '',
        'Element 29',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_3',
        19,
        0,
        1,
        '',
        'Element 3',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_30',
        46,
        0,
        1,
        '',
        'Element 30',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_4',
        20,
        0,
        1,
        '',
        'Element 4',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_5',
        21,
        0,
        1,
        '',
        'Element 5',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_6',
        22,
        0,
        1,
        '',
        'Element 6',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_7',
        23,
        0,
        1,
        '',
        'Element 7',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_8',
        24,
        0,
        1,
        '',
        'Element 8',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'element_9',
        25,
        0,
        1,
        '',
        'Element 9',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'error_message',
        49,
        0,
        1,
        '',
        'Error Message',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        9,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_xlate',
        8,
        0,
        1,
        '',
        'Ilr Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_description',
        1,
        0,
        1,
        '',
        'Import Run Description',
        300,
        'description',
        '(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 261))',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_id',
        2,
        0,
        1,
        '',
        'Import Run Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'is_modified',
        48,
        0,
        1,
        '',
        'Is Modified',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'line_id',
        3,
        0,
        1,
        '',
        'Line Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'loaded',
        47,
        0,
        1,
        '',
        'Loaded',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        7,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_override',
        50,
        0,
        1,
        '',
        'Ls Asset Override',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_xlate',
        6,
        0,
        1,
        '',
        'Ls Asset Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'percent',
        16,
        0,
        1,
        '',
        'Percent',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_local_id',
        15,
        0,
        1,
        '',
        'Tax Local Id',
        300,
        'description',
        'ls_tax_local',
        'VARCHAR2',
        0,
        'tax_local_id',
        null,
        null,
        'tax_local_id',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_local_xlate',
        14,
        0,
        1,
        '',
        'Tax Local Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'time_stamp',
        4,
        0,
        1,
        '',
        'Time Stamp',
        300,
        '',
        '',
        'DATE',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'trans_type',
        13,
        0,
        1,
        '',
        'Trans Type',
        300,
        'description',
        '(select * from je_trans_type where trans_type >= 3000)',
        'NUMBER',
        0,
        'trans_type',
        null,
        null,
        'trans_type',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'trans_type_xlate',
        12,
        0,
        1,
        '',
        'Trans Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'user_id',
        5,
        0,
        1,
        '',
        'User Id',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/