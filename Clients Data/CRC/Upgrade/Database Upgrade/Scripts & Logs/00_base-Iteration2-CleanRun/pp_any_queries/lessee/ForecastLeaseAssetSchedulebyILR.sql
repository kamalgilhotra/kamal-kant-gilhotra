DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Forecast Lease Asset Schedule by ILR';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := 'select * from LS_FCST_ASSET_SCHED_BY_ILR_VW ';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT  l_id, 'FORECAST_VERSION', 1, 0, 1, null, 'Forecast Version', 300, 'description', 'ls_forecast_version', 'VARCHAR2', 0, 'description', 1, 2, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'ILR_NUMBER', 2, 0, 1, null, 'ILR Number', 300, 'ilr_number', 'ls_ilr', 'VARCHAR2', 0, 'ilr_number', 1, 2, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONVERSION_DATE', 3, 0, 1, null, 'Conversion Date', 300, 'conversion_date', 'ls_forecast_version', 'DATE', 0, 'conversion_date', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LS_ASSET_ID', 4, 0, 1, null, 'LS Asset ID', 300, 'ls_asset_id', 'ls_asset', 'NUMBER', 0, 'ls_asset_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASED_ASSET_NUMBER', 5, 0, 1, null, 'Leased Asset Number', 300, 'leased_asset_number', 'ls_asset', 'VARCHAR2', 0, 'leased_asset_number', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'COMPANY_ID', 6, 0, 1, null, 'Company Id', 300, 'company_id', '(select * from company where is_lease_company = 1)', 'NUMBER', 0, 'company_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'COMPANY_DESCRIPTION', 7, 0, 1, null, 'Company Description', 300, 'description', '(select * from company where is_lease_company = 1)', 'VARCHAR2', 0, 'description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'ILR_ID', 8, 0, 1, null, 'ILR Id', 300, 'ilr_id', 'ls_ilr', 'NUMBER', 0, 'ilr_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASE_ID', 9, 0, 1, null, 'Lease Id', 300, 'lease_id', 'ls_lease', 'NUMBER', 0, 'lease_id', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASE_NUMBER', 10, 0, 1, null, 'Lease Number', 300, 'lease_number', 'ls_lease', 'VARCHAR2', 0, 'lease_number', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LEASE_CAP_TYPE', 11, 0, 1, null, 'Lease Cap Type', 300, 'description', 'ls_lease_cap_type', 'VARCHAR2', 0, 'description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'LOCATION', 12, 0, 1, null, 'Location', 300, 'long_description', 'asset_location', 'VARCHAR2', 0, 'long_description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'REVISION', 13, 0, 1, null, 'Revision', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'SET_OF_BOOKS', 14, 0, 1, null, 'Set Of Books', 300, 'description', 'set_of_books', 'VARCHAR2', 0, 'description', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'MONTHNUM', 15, 0, 1, null, 'Month Number', 300, 'month', 'ls_lease_asset_schedule', 'DATE', 0, 'month', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BEG_CAPITAL_COST', 16, 0, 1, null, 'Beginning Capital Cost', 300, '', '', 'NUMBER', 1, '', null, null, 'the_sort', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'END_CAPITAL_COST', 17, 0, 1, null, 'Ending Capital Cost', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BEG_OBLIGATION', 18, 0, 1, null, 'Beg Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'END_OBLIGATION', 19, 0, 1, null, 'End Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BEG_LT_OBLIGATION', 20, 0, 1, null, 'Beginning LT Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'END_LT_OBLIGATION', 21, 0, 1, null, 'Ending LT Obligation', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'INTEREST_ACCRUAL', 22, 0, 1, null, 'Interest Accrual', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'PRINCIPAL_ACCRUAL', 23, 0, 1, null, 'Principal Accrual', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'INTEREST_PAID', 24, 0, 1, null, 'Interest Paid', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'PRINCIPAL_PAID', 25, 0, 1, null, 'Principal Paid', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL1', 26, 0, 1, null, 'Executory Accrual1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL2', 27, 0, 1, null, 'Executory Accrual2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL3', 28, 0, 1, null, 'Executory Accrual3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL4', 29, 0, 1, null, 'Executory Accrual4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL5', 30, 0, 1, null, 'Executory Accrual5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL6', 31, 0, 1, null, 'Executory Accrual6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL7', 32, 0, 1, null, 'Executory Accrual7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL8', 33, 0, 1, null, 'Executory Accrual8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL9', 34, 0, 1, null, 'Executory Accrual9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_ACCRUAL10', 35, 0, 1, null, 'Executory Accrual10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID1', 36, 0, 1, null, 'Executory Paid1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID2', 37, 0, 1, null, 'Executory Paid2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID3', 38, 0, 1, null, 'Executory Paid3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID4', 39, 0, 1, null, 'Executory Paid4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID5', 40, 0, 1, null, 'Executory Paid5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID6', 41, 0, 1, null, 'Executory Paid6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID7', 42, 0, 1, null, 'Executory Paid7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID8', 43, 0, 1, null, 'Executory Paid8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID9', 44, 0, 1, null, 'Executory Paid9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'EXECUTORY_PAID10', 45, 0, 1, null, 'Executory Paid10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL1', 46, 0, 1, null, 'Contingent Accrual1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL2', 47, 0, 1, null, 'Contingent Accrual2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL3', 48, 0, 1, null, 'Contingent Accrual3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL4', 49, 0, 1, null, 'Contingent Accrual4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL5', 50, 0, 1, null, 'Contingent Accrual5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL6', 51, 0, 1, null, 'Contingent Accrual6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL7', 52, 0, 1, null, 'Contingent Accrual7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL8', 53, 0, 1, null, 'Contingent Accrual8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL9', 54, 0, 1, null, 'Contingent Accrual9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_ACCRUAL10', 55, 0, 1, null, 'Contingent Accrual10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID1', 56, 0, 1, null, 'Contingent Paid1', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID2', 57, 0, 1, null, 'Contingent Paid2', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID3', 58, 0, 1, null, 'Contingent Paid3', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID4', 59, 0, 1, null, 'Contingent Paid4', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID5', 60, 0, 1, null, 'Contingent Paid5', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID6', 61, 0, 1, null, 'Contingent Paid6', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID7', 62, 0, 1, null, 'Contingent Paid7', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID8', 63, 0, 1, null, 'Contingent Paid8', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID9', 64, 0, 1, null, 'Contingent Paid9', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CONTINGENT_PAID10', 65, 0, 1, null, 'Contingent Paid10', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'IS_OM', 66, 0, 1, null, 'is O&M', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'CURRENT_LEASE_COST', 67, 0, 1, null, 'Current Lease Cost', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'RESIDUAL_AMOUNT', 68, 0, 1, null, 'Residual Amount', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'TERM_PENALTY', 69, 0, 1, null, 'Term Penalty', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'BPO_PRICE', 70, 0, 1, null, 'BPO Price', 300, '', '', 'NUMBER', 1, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'currency_type', 71, 0, 1, null, 'Currency Type', 300, 'description', 'ls_lease_currency_type', 'VARCHAR2', 0, 'description', 1, 2, 'description', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'currency_display_symbol', 72, 0, 0, null, 'Currency Symbol', 300, '', '', 'VARCHAR2', 0, '', null, null, 'currency_display_symbol', 1, 1 FROM dual UNION ALL
SELECT  l_id, 'iso_code', 73, 0, 1, null, 'Currency', 300, 'description', 'currency', 'VARCHAR2', 0, 'iso_code', null, null, 'iso_code', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_deferred_rent', 74, 1, 1, null, 'Beg Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'deferred_rent', 75, 1, 1, null, 'Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_deferred_rent', 76, 1, 1, null, 'End Deferred Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_liability', 77, 1, 1, null, 'Beg Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_liability', 78, 1, 1, null, 'End Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_lt_liability', 79, 1, 1, null, 'Beg LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_lt_liability', 80, 1, 1, null, 'End LT Liability', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'principal_remeasurement', 81, 1, 1, null, 'Principal Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'liability_remeasurement', 82, 1, 1, null, 'Liability Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_net_rou_asset', 83, 1, 1, null, 'Beg Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'rou_asset_remeasurement', 84, 1, 1, null, 'ROU Asset Remeasurement', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_net_rou_asset', 85, 1, 1, null, 'End Net ROU Asset', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'initial_direct_cost', 86, 1, 1, null, 'Initial Direct Cost', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'incentive_amount', 87, 1, 1, null, 'Incentive Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_prepaid_rent', 88, 1, 1, null, 'Beg Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'prepay_amortization', 89, 1, 1, null, 'Prepay Amortization', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'prepaid_rent', 90, 1, 1, null, 'Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_prepaid_rent', 91, 1, 1, null, 'End Prepaid Rent', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'beg_arrears_accrual', 92, 1, 1, null, 'Beg Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'arrears_accrual', 93, 1, 1, null, 'Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_arrears_accrual', 94, 1, 1, null, 'End Arrears Accrual', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'executory_adjust', 95, 1, 1, null, 'Executory Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'contingent_adjust', 96, 1, 1, null, 'Contingent Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_reserve', 97, 1, 1, null, 'Begin Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'depr_expense', 98, 1, 1, null, 'Depr Expense', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'depr_exp_alloc_adjust', 99, 1, 1, null, 'Depr Expense Adjust', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_reserve', 100, 1, 1, null, 'End Reserve', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'begin_accum_impair', 101, 1, 1, null, 'Begin Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'impairment_activity', 102, 1, 1, null, 'Impairment Amount', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual UNION ALL
SELECT  l_id, 'end_accum_impair', 103, 1, 1, null, 'End Accum Impairment', 300, '', '', 'NUMBER', 0, '', null, null, '', 0, 0 FROM dual
;

END;

/


COMMIT;