
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Payment Detail by Location';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select to_char(lpl.gl_posting_mo_yr, ''yyyymm'') as monthnum, co.company_id, co.description as company_description,
  lct.description as lease_cap_type, al.long_description as location,
  nvl(sum(decode(lpl.payment_type_id, 1, lpl.amount, 0)), 0) principal_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.amount, 0)), 0) interest_payment,
  nvl(sum(decode(lpl.payment_type_id, 2, lpl.adjustment_amount, 0)), 0) interest_adjustment,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.amount, 0)), 0) executory_payment1,
	nvl(sum(decode(lpl.payment_type_id, 3, lpl.adjustment_amount, 0)), 0) executory_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.amount, 0)), 0) executory_payment2,
	nvl(sum(decode(lpl.payment_type_id, 4, lpl.adjustment_amount, 0)), 0) executory_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.amount, 0)), 0) executory_payment3,
	nvl(sum(decode(lpl.payment_type_id, 5, lpl.adjustment_amount, 0)), 0) executory_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.amount, 0)), 0) executory_payment4,
	nvl(sum(decode(lpl.payment_type_id, 6, lpl.adjustment_amount, 0)), 0) executory_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.amount, 0)), 0) executory_payment5,
	nvl(sum(decode(lpl.payment_type_id, 7, lpl.adjustment_amount, 0)), 0) executory_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.amount, 0)), 0) executory_payment6,
	nvl(sum(decode(lpl.payment_type_id, 8, lpl.adjustment_amount, 0)), 0) executory_adjustment6,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.amount, 0)), 0) executory_payment7,
	nvl(sum(decode(lpl.payment_type_id, 9, lpl.adjustment_amount, 0)), 0) executory_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.amount, 0)), 0) executory_payment8,
	nvl(sum(decode(lpl.payment_type_id, 10, lpl.adjustment_amount, 0)), 0) executory_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.amount, 0)), 0) executory_payment9,
	nvl(sum(decode(lpl.payment_type_id, 11, lpl.adjustment_amount, 0)), 0) executory_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.amount, 0)), 0) executory_payment10,
	nvl(sum(decode(lpl.payment_type_id, 12, lpl.adjustment_amount, 0)), 0) executory_adjustment10,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.amount, 0)), 0) contingent_payment1,
	nvl(sum(decode(lpl.payment_type_id, 13, lpl.adjustment_amount, 0)), 0) contingent_adjustment1,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.amount, 0)), 0) contingent_payment2,
	nvl(sum(decode(lpl.payment_type_id, 14, lpl.adjustment_amount, 0)), 0) contingent_adjustment2,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.amount, 0)), 0) contingent_payment3,
	nvl(sum(decode(lpl.payment_type_id, 15, lpl.adjustment_amount, 0)), 0) contingent_adjustment3,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.amount, 0)), 0) contingent_payment4,
	nvl(sum(decode(lpl.payment_type_id, 16, lpl.adjustment_amount, 0)), 0) contingent_adjustment4,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.amount, 0)), 0) contingent_payment5,
	nvl(sum(decode(lpl.payment_type_id, 17, lpl.adjustment_amount, 0)), 0) contingent_adjustment5,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.amount, 0)), 0) contingent_payment6,
	nvl(sum(decode(lpl.payment_type_id, 18, lpl.adjustment_amount, 0)), 0) contingent_adjustment6,
	nvl(sum(decode(lpl.payment_type_id, 19, lpl.amount, 0)), 0) contingent_payment7,
	nvl(sum(d ecode(lpl.payment_type_id, 19, lpl.adjustment_amount, 0)), 0) contingent_adjustment7,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.amount, 0)), 0) contingent_payment8,
	nvl(sum(decode(lpl.payment_type_id, 20, lpl.adjustment_amount, 0)), 0) contingent_adjustment8,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.amount, 0)), 0) contingent_payment9,
	nvl(sum(decode(lpl.payment_type_id, 21, lpl.adjustment_amount, 0)), 0) contingent_adjustment9,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.amount, 0)), 0) contingent_payment10,
	nvl(sum(decode(lpl.payment_type_id, 22, lpl.adjustment_amount, 0)), 0) contingent_adjustment10,
  nvl(sum(decode(lpl.payment_type_id, 23, lpl.amount, 0)), 0) termination_penalty,
  nvl(sum(decode(lpl.payment_type_id, 24, lpl.amount, 0)), 0) sales_proceeds,
  sum(nvl(tax_1,0)) as tax_1, sum(nvl(tax_1_adjustment,0)) as tax_1_adjustment,
  sum(nvl(tax_2,0)) as tax_2, sum(nvl(tax_2_adjustment,0)) as tax_2_adjustment,
  sum(nvl(tax_3,0)) as tax_3, sum(nvl(tax_3_adjustment,0)) as tax_3_adjustment,
  sum(nvl(tax_4,0)) as tax_4, sum(nvl(tax_4_adjustment,0)) as tax_4_adjustment,
  sum(nvl(tax_5,0)) as tax_5, sum(nvl(tax_5_adjustment,0)) as tax_5_adjustment,
  sum(nvl(tax_6,0)) as tax_6, sum(nvl(tax_6_adjustment,0)) as tax_6_adjustment,
  sum(nvl(tax_7,0)) as tax_7, sum(nvl(tax_7_adjustment,0)) as tax_7_adjustment
from ls_asset la, company co, ls_payment_line lpl, asset_location al, ls_ilr ilr, ls_lease ll, ls_ilr_options ilro, ls_lease_cap_type lct,
      (select lmt.ls_asset_id,
        nvl(sum(decode(tax_local_id, 1, amount, 0)),0) as tax_1,
        nvl(sum(decode(tax_local_id, 1, adjustment_amount, 0)),0) as tax_1_adjustment,
        nvl(sum(decode(tax_local_id, 2, amount, 0)),0) as tax_2,
        nvl(sum(decode(tax_local_id, 2, adjustment_amount, 0)),0) as tax_2_adjustment,
        nvl(sum(decode(tax_local_id, 3, amount, 0)),0) as tax_3,
        nvl(sum(decode(tax_local_id, 3, adjustment_amount, 0)),0) as tax_3_adjustment,
        nvl(sum(decode(tax_local_id, 4, amount, 0)),0) as tax_4,
        nvl(sum(decode(tax_local_id, 4, adjustment_amount, 0)),0) as tax_4_adjustment,
        nvl(sum(decode(tax_local_id, 5, amount, 0)),0) as tax_5,
        nvl(sum(decode(tax_local_id, 5, adjustment_amount, 0)),0) as tax_5_adjustment,
        nvl(sum(decode(tax_local_id, 6, amount, 0)),0) as tax_6,
        nvl(sum(decode(tax_local_id, 6, adjustment_amount, 0)),0) as tax_6_adjustment,
        nvl(sum(decode(tax_local_id, 7, amount, 0)),0) as tax_7,
        nvl(sum(decode(tax_local_id, 7, adjustment_amount, 0)),0) as tax_7_adjustment
       from ls_monthly_tax lmt, ls_asset la
       where to_char(gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
          and lmt.accrual = 0
          and lmt.ls_asset_id = la.ls_asset_id
          and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
       group by lmt.ls_asset_id) taxes
where la.company_id = co.company_id
  and la.company_id in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''COMPANY ID'')
  and to_char(lpl.gl_posting_mo_yr, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name))= ''MONTHNUM'')
  and lpl.ls_asset_id = la.ls_asset_id
  and la.asset_location_id = al.asset_location_id
  and la.ilr_id = ilr.ilr_id
  and ilr.lease_id = ll.lease_id
  and la.ilr_id = ilro.ilr_id
  and la.approved_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and la.ls_asset_id = taxes.ls_asset_id (+)
group by lpl.gl_posting_mo_yr, co.company_id, co.description, lct.description, al.long_description        ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'company_description',
        3,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        2,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment1',
        30,
        1,
        1,
        '',
        'Contingent Adjustment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment10',
        48,
        1,
        1,
        '',
        'Contingent Adjustment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment2',
        32,
        1,
        1,
        '',
        'Contingent Adjustment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment3',
        34,
        1,
        1,
        '',
        'Contingent Adjustment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment4',
        36,
        1,
        1,
        '',
        'Contingent Adjustment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment5',
        38,
        1,
        1,
        '',
        'Contingent Adjustment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment6',
        40,
        1,
        1,
        '',
        'Contingent Adjustment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment7',
        42,
        1,
        1,
        '',
        'Contingent Adjustment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment8',
        44,
        1,
        1,
        '',
        'Contingent Adjustment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_adjustment9',
        46,
        1,
        1,
        '',
        'Contingent Adjustment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment1',
        29,
        1,
        1,
        '',
        'Contingent Payment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment10',
        47,
        1,
        1,
        '',
        'Contingent Payment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment2',
        31,
        1,
        1,
        '',
        'Contingent Payment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment3',
        33,
        1,
        1,
        '',
        'Contingent Payment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment4',
        35,
        1,
        1,
        '',
        'Contingent Payment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment5',
        37,
        1,
        1,
        '',
        'Contingent Payment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment6',
        39,
        1,
        1,
        '',
        'Contingent Payment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment7',
        41,
        1,
        1,
        '',
        'Contingent Payment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment8',
        43,
        1,
        1,
        '',
        'Contingent Payment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'contingent_payment9',
        45,
        1,
        1,
        '',
        'Contingent Payment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment1',
        10,
        1,
        1,
        '',
        'Executory Adjustment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment10',
        28,
        1,
        1,
        '',
        'Executory Adjustment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment2',
        12,
        1,
        1,
        '',
        'Executory Adjustment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment3',
        14,
        1,
        1,
        '',
        'Executory Adjustment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment4',
        16,
        1,
        1,
        '',
        'Executory Adjustment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment5',
        18,
        1,
        1,
        '',
        'Executory Adjustment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment6',
        20,
        1,
        1,
        '',
        'Executory Adjustment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment7',
        22,
        1,
        1,
        '',
        'Executory Adjustment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment8',
        24,
        1,
        1,
        '',
        'Executory Adjustment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_adjustment9',
        26,
        1,
        1,
        '',
        'Executory Adjustment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment1',
        9,
        1,
        1,
        '',
        'Executory Payment1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment10',
        27,
        1,
        1,
        '',
        'Executory Payment10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment2',
        11,
        1,
        1,
        '',
        'Executory Payment2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment3',
        13,
        1,
        1,
        '',
        'Executory Payment3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment4',
        15,
        1,
        1,
        '',
        'Executory Payment4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment5',
        17,
        1,
        1,
        '',
        'Executory Payment5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment6',
        19,
        1,
        1,
        '',
        'Executory Payment6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment7',
        21,
        1,
        1,
        '',
        'Executory Payment7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment8',
        23,
        1,
        1,
        '',
        'Executory Payment8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'executory_payment9',
        25,
        1,
        1,
        '',
        'Executory Payment9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interest_adjustment',
        8,
        1,
        1,
        '',
        'Interest Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interest_payment',
        7,
        1,
        1,
        '',
        'Interest Payment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type',
        4,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'location',
        5,
        0,
        1,
        '',
        'Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'monthnum',
        1,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        2,
        'the_sort',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'principal_payment',
        6,
        1,
        1,
        '',
        'Principal Payment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sales_proceeds',
        50,
        1,
        1,
        '',
        'Sales Proceeds',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_1',
        51,
        1,
        1,
        '',
        'Tax 1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_1_adjustment',
        52,
        1,
        1,
        '',
        'Tax 1 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_2',
        53,
        1,
        1,
        '',
        'Tax 2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_2_adjustment',
        54,
        1,
        1,
        '',
        'Tax 2 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_3',
        55,
        1,
        1,
        '',
        'Tax 3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_3_adjustment',
        56,
        1,
        1,
        '',
        'Tax 3 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_4',
        57,
        1,
        1,
        '',
        'Tax 4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_4_adjustment',
        58,
        1,
        1,
        '',
        'Tax 4 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_5',
        59,
        1,
        1,
        '',
        'Tax 5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_5_adjustment',
        60,
        1,
        1,
        '',
        'Tax 5 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_6',
        61,
        1,
        1,
        '',
        'Tax 6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_6_adjustment',
        62,
        1,
        1,
        '',
        'Tax 6 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_7',
        63,
        1,
        1,
        '',
        'Tax 7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_7_adjustment',
        64,
        1,
        1,
        '',
        'Tax 7 Adjustment',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'termination_penalty',
        49,
        1,
        1,
        '',
        'Termination Penalty',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/