
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Import Component Archive';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.ERROR_MESSAGE,arc.TIME_STAMP,arc.USER_ID,arc.COMPONENT_ID,arc.LS_COMP_STATUS_ID,arc.COMPANY_ID,
arc.COMPANY_XLATE,arc.DATE_RECEIVED,arc.DESCRIPTION,arc.LONG_DESCRIPTION,arc.SERIAL_NUMBER,arc.PO_NUMBER,arc.AMOUNT,arc.LS_ASSET_ID,arc.LS_ASSET_XLATE,arc.INTERIM_INTEREST_START_DATE,arc.INTERIM_INTEREST,arc.INVOICE_NUMBER,arc.INVOICE_DATE,
arc.INVOICE_AMOUNT,arc.UNIQUE_COMPONENT_IDENTIFIER,arc.LS_ASSET_OVERRIDE
  from pp_import_run run, ls_import_component_archive arc
  where run.import_run_id = arc.import_run_id
            ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'amount',
        16,
        1,
        1,
        '',
        'Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        9,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_xlate',
        10,
        0,
        1,
        '',
        'Company Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'component_id',
        7,
        0,
        1,
        '',
        'Component Id',
        300,
        'description',
        'ls_component',
        'NUMBER',
        0,
        'component_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'date_received',
        11,
        0,
        1,
        '',
        'Date Received',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'description',
        12,
        0,
        1,
        '',
        'Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'error_message',
        4,
        0,
        1,
        '',
        'Error Message',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_description',
        1,
        0,
        1,
        '',
        'Import Run Description',
        300,
        'description',
        '(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 255))',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_id',
        2,
        0,
        1,
        '',
        'Import Run Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interim_interest',
        20,
        1,
        1,
        '',
        'Interim Interest',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'interim_interest_start_date',
        19,
        0,
        1,
        '',
        'Interim Interest Start Date',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'invoice_amount',
        23,
        1,
        1,
        '',
        'Invoice Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'invoice_date',
        22,
        0,
        1,
        '',
        'Invoice Date',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'invoice_number',
        21,
        0,
        1,
        '',
        'Invoice Number',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'line_id',
        3,
        0,
        1,
        '',
        'Line Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'long_description',
        13,
        0,
        1,
        '',
        'Long Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        17,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_override',
        25,
        0,
        1,
        '',
        'Ls Asset Override',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_xlate',
        18,
        0,
        1,
        '',
        'Ls Asset Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_comp_status_id',
        8,
        0,
        1,
        '',
        'Ls Comp Status Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'po_number',
        15,
        0,
        1,
        '',
        'Po Number',
        300,
        'po_number',
        'ls_component',
        'VARCHAR2',
        0,
        'po_number',
        null,
        null,
        'po_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'serial_number',
        14,
        0,
        1,
        '',
        'Serial Number',
        300,
        'serial_number',
        'ls_component',
        'VARCHAR2',
        0,
        'serial_number',
        null,
        null,
        'serial_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'time_stamp',
        5,
        0,
        1,
        '',
        'Time Stamp',
        300,
        '',
        '',
        'DATE',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'unique_component_identifier',
        24,
        0,
        1,
        '',
        'Unique Component Identifier',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'user_id',
        6,
        0,
        1,
        '',
        'User Id',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/