
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Import MLA Archive';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select run.description import_run_description, arc.IMPORT_RUN_ID,arc.LINE_ID,arc.TIME_STAMP,arc.USER_ID,arc.LEASE_ID,arc.LEASE_NUMBER,arc.DESCRIPTION,arc.LONG_DESCRIPTION,arc.LESSOR_XLATE,arc.LESSOR_ID,arc.LEASE_TYPE_XLATE,
arc.LEASE_TYPE_ID,arc.PAYMENT_DUE_DAY,arc.PRE_PAYMENT_SW,arc.LEASE_GROUP_XLATE,arc.LEASE_GROUP_ID,arc.LEASE_CAP_TYPE_XLATE,arc.LEASE_CAP_TYPE_ID,arc.WORKFLOW_TYPE_XLATE,arc.WORKFLOW_TYPE_ID,arc.MASTER_AGREEMENT_DATE,arc.NOTES,
arc.PURCHASE_OPTION_TYPE_XLATE,arc.PURCHASE_OPTION_TYPE_ID,arc.PURCHASE_OPTION_AMT,arc.RENEWAL_OPTION_TYPE_XLATE,arc.RENEWAL_OPTION_TYPE_ID,arc.CANCELABLE_TYPE_XLATE,arc.CANCELABLE_TYPE_ID,arc.ITC_SW,arc.PARTIAL_RETIRE_SW,
arc.SUBLET_SW,arc.MUNI_BO_SW,arc.COMPANY_XLATE,arc.COMPANY_ID,arc.MAX_LEASE_LINE,arc.VENDOR_XLATE,arc.VENDOR_ID,arc.PAYMENT_PCT,arc.CLASS_CODE_XLATE1,arc.CLASS_CODE_ID1,arc.CLASS_CODE_VALUE1,arc.CLASS_CODE_XLATE2,arc.CLASS_CODE_ID2,
arc.CLASS_CODE_VALUE2,arc.CLASS_CODE_XLATE3,arc.CLASS_CODE_ID3,arc.CLASS_CODE_VALUE3,arc.CLASS_CODE_XLATE4,arc.CLASS_CODE_ID4,arc.CLASS_CODE_VALUE4,arc.CLASS_CODE_XLATE5,arc.CLASS_CODE_ID5,arc.CLASS_CODE_VALUE5,arc.CLASS_CODE_XLATE6,
arc.CLASS_CODE_ID6,arc.CLASS_CODE_VALUE6,arc.CLASS_CODE_XLATE7,arc.CLASS_CODE_ID7,arc.CLASS_CODE_VALUE7,arc.CLASS_CODE_XLATE8,arc.CLASS_CODE_ID8,arc.CLASS_CODE_VALUE8,arc.CLASS_CODE_XLATE9,arc.CLASS_CODE_ID9,arc.CLASS_CODE_VALUE9,
arc.CLASS_CODE_XLATE10,arc.CLASS_CODE_ID10,arc.CLASS_CODE_VALUE10,arc.CLASS_CODE_XLATE11,arc.CLASS_CODE_ID11,arc.CLASS_CODE_VALUE11,arc.CLASS_CODE_XLATE12,arc.CLASS_CODE_ID12,arc.CLASS_CODE_VALUE12,arc.CLASS_CODE_XLATE13,arc.CLASS_CODE_ID13,
arc.CLASS_CODE_VALUE13,arc.CLASS_CODE_XLATE14,arc.CLASS_CODE_ID14,arc.CLASS_CODE_VALUE14,arc.CLASS_CODE_XLATE15,arc.CLASS_CODE_ID15,arc.CLASS_CODE_VALUE15,arc.CLASS_CODE_XLATE16,arc.CLASS_CODE_ID16,arc.CLASS_CODE_VALUE16,arc.CLASS_CODE_XLATE17,
arc.CLASS_CODE_ID17,arc.CLASS_CODE_VALUE17,arc.CLASS_CODE_XLATE18,arc.CLASS_CODE_ID18,arc.CLASS_CODE_VALUE18,arc.CLASS_CODE_XLATE19,arc.CLASS_CODE_ID19,arc.CLASS_CODE_VALUE19,arc.CLASS_CODE_XLATE20,arc.CLASS_CODE_ID20,arc.CLASS_CODE_VALUE20,arc.LOADED,
arc.IS_MODIFIED,arc.ERROR_MESSAGE,arc.UNIQUE_LEASE_IDENTIFIER,arc.AUTO_APPROVE,arc.AUTO_APPROVE_XLATE,arc.TAX_SUMMARY_XLATE,arc.TAX_SUMMARY_ID,arc.TAX_RATE_OPTION_XLATE,arc.TAX_RATE_OPTION_ID,arc.AUTO_GENERATE_INVOICES_XLATE,arc.AUTO_GENERATE_INVOICES,
arc.LS_RECONCILE_TYPE_ID,arc.LS_RECONCILE_TYPE_XLATE,arc.DAYS_IN_YEAR,arc.CUT_OFF_DAY,arc.LEASE_END_DATE,arc.DAYS_IN_MONTH_SW
  from pp_import_run run, ls_import_lease_archive arc
  where run.import_run_id = arc.import_run_id          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'auto_approve',
        105,
        0,
        1,
        '',
        'Auto Approve',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'auto_approve_xlate',
        106,
        0,
        1,
        '',
        'Auto Approve Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'auto_generate_invoices',
        112,
        0,
        1,
        '',
        'Auto Generate Invoices',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'auto_generate_invoices_xlate',
        111,
        0,
        1,
        '',
        'Auto Generate Invoices Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cancelable_type_id',
        30,
        0,
        1,
        '',
        'Cancelable Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cancelable_type_xlate',
        29,
        0,
        1,
        '',
        'Cancelable Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id1',
        42,
        0,
        1,
        '',
        'Class Code Id1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id10',
        69,
        0,
        1,
        '',
        'Class Code Id10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id11',
        72,
        0,
        1,
        '',
        'Class Code Id11',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id12',
        75,
        0,
        1,
        '',
        'Class Code Id12',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id13',
        78,
        0,
        1,
        '',
        'Class Code Id13',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id14',
        81,
        0,
        1,
        '',
        'Class Code Id14',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id15',
        84,
        0,
        1,
        '',
        'Class Code Id15',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id16',
        87,
        0,
        1,
        '',
        'Class Code Id16',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id17',
        90,
        0,
        1,
        '',
        'Class Code Id17',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id18',
        93,
        0,
        1,
        '',
        'Class Code Id18',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id19',
        96,
        0,
        1,
        '',
        'Class Code Id19',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id2',
        45,
        0,
        1,
        '',
        'Class Code Id2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id20',
        99,
        0,
        1,
        '',
        'Class Code Id20',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id3',
        48,
        0,
        1,
        '',
        'Class Code Id3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id4',
        51,
        0,
        1,
        '',
        'Class Code Id4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id5',
        54,
        0,
        1,
        '',
        'Class Code Id5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id6',
        57,
        0,
        1,
        '',
        'Class Code Id6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id7',
        60,
        0,
        1,
        '',
        'Class Code Id7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id8',
        63,
        0,
        1,
        '',
        'Class Code Id8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id9',
        66,
        0,
        1,
        '',
        'Class Code Id9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value1',
        43,
        0,
        1,
        '',
        'Class Code Value1',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value10',
        70,
        0,
        1,
        '',
        'Class Code Value10',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value11',
        73,
        0,
        1,
        '',
        'Class Code Value11',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value12',
        76,
        0,
        1,
        '',
        'Class Code Value12',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value13',
        79,
        0,
        1,
        '',
        'Class Code Value13',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value14',
        82,
        0,
        1,
        '',
        'Class Code Value14',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value15',
        85,
        0,
        1,
        '',
        'Class Code Value15',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value16',
        88,
        0,
        1,
        '',
        'Class Code Value16',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value17',
        91,
        0,
        1,
        '',
        'Class Code Value17',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value18',
        94,
        0,
        1,
        '',
        'Class Code Value18',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value19',
        97,
        0,
        1,
        '',
        'Class Code Value19',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value2',
        46,
        0,
        1,
        '',
        'Class Code Value2',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value20',
        100,
        0,
        1,
        '',
        'Class Code Value20',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value3',
        49,
        0,
        1,
        '',
        'Class Code Value3',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value4',
        52,
        0,
        1,
        '',
        'Class Code Value4',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value5',
        55,
        0,
        1,
        '',
        'Class Code Value5',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value6',
        58,
        0,
        1,
        '',
        'Class Code Value6',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value7',
        61,
        0,
        1,
        '',
        'Class Code Value7',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value8',
        64,
        0,
        1,
        '',
        'Class Code Value8',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value9',
        67,
        0,
        1,
        '',
        'Class Code Value9',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate1',
        41,
        0,
        1,
        '',
        'Class Code Xlate1',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate10',
        68,
        0,
        1,
        '',
        'Class Code Xlate10',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate11',
        71,
        0,
        1,
        '',
        'Class Code Xlate11',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate12',
        74,
        0,
        1,
        '',
        'Class Code Xlate12',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate13',
        77,
        0,
        1,
        '',
        'Class Code Xlate13',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate14',
        80,
        0,
        1,
        '',
        'Class Code Xlate14',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate15',
        83,
        0,
        1,
        '',
        'Class Code Xlate15',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate16',
        86,
        0,
        1,
        '',
        'Class Code Xlate16',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate17',
        89,
        0,
        1,
        '',
        'Class Code Xlate17',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate18',
        92,
        0,
        1,
        '',
        'Class Code Xlate18',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate19',
        95,
        0,
        1,
        '',
        'Class Code Xlate19',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate2',
        44,
        0,
        1,
        '',
        'Class Code Xlate2',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate20',
        98,
        0,
        1,
        '',
        'Class Code Xlate20',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate3',
        47,
        0,
        1,
        '',
        'Class Code Xlate3',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate4',
        50,
        0,
        1,
        '',
        'Class Code Xlate4',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate5',
        53,
        0,
        1,
        '',
        'Class Code Xlate5',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate6',
        56,
        0,
        1,
        '',
        'Class Code Xlate6',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate7',
        59,
        0,
        1,
        '',
        'Class Code Xlate7',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate8',
        62,
        0,
        1,
        '',
        'Class Code Xlate8',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate9',
        65,
        0,
        1,
        '',
        'Class Code Xlate9',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        36,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_xlate',
        35,
        0,
        1,
        '',
        'Company Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'cut_off_day',
        116,
        0,
        1,
        '',
        'Cut Off Day',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'days_in_month_sw',
        118,
        0,
        1,
        '',
        'Days In Month Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'days_in_year',
        115,
        0,
        1,
        '',
        'Days In Year',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'description',
        8,
        0,
        1,
        '',
        'Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'error_message',
        103,
        0,
        1,
        '',
        'Error Message',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_description',
        1,
        0,
        1,
        '',
        'Import Run Description',
        300,
        'description',
        '(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 251))',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_id',
        2,
        0,
        1,
        '',
        'Import Run Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'is_modified',
        102,
        0,
        1,
        '',
        'Is Modified',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'itc_sw',
        31,
        0,
        1,
        '',
        'Itc Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type_id',
        19,
        0,
        1,
        '',
        'Lease Cap Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type_xlate',
        18,
        0,
        1,
        '',
        'Lease Cap Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_end_date',
        117,
        0,
        1,
        '',
        'Lease End Date',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_group_id',
        17,
        0,
        1,
        '',
        'Lease Group Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_group_xlate',
        16,
        0,
        1,
        '',
        'Lease Group Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_id',
        6,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        7,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_type_id',
        13,
        0,
        1,
        '',
        'Lease Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_type_xlate',
        12,
        0,
        1,
        '',
        'Lease Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lessor_id',
        11,
        0,
        1,
        '',
        'Lessor Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lessor_xlate',
        10,
        0,
        1,
        '',
        'Lessor Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'line_id',
        3,
        0,
        1,
        '',
        'Line Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'loaded',
        101,
        0,
        1,
        '',
        'Loaded',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'long_description',
        9,
        0,
        1,
        '',
        'Long Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_reconcile_type_id',
        113,
        0,
        1,
        '',
        'Ls Reconcile Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_reconcile_type_xlate',
        114,
        0,
        1,
        '',
        'Ls Reconcile Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'master_agreement_date',
        22,
        0,
        1,
        '',
        'Master Agreement Date',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'max_lease_line',
        37,
        0,
        1,
        '',
        'Max Lease Line',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'muni_bo_sw',
        34,
        0,
        1,
        '',
        'Muni Bo Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'notes',
        23,
        0,
        1,
        '',
        'Notes',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'partial_retire_sw',
        32,
        0,
        1,
        '',
        'Partial Retire Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_due_day',
        14,
        1,
        1,
        '',
        'Payment Due Day',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'payment_pct',
        40,
        1,
        1,
        '',
        'Payment Pct',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'pre_payment_sw',
        15,
        0,
        1,
        '',
        'Pre Payment Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'purchase_option_amt',
        26,
        0,
        1,
        '',
        'Purchase Option Amt',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'purchase_option_type_id',
        25,
        0,
        1,
        '',
        'Purchase Option Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'purchase_option_type_xlate',
        24,
        0,
        1,
        '',
        'Purchase Option Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'renewal_option_type_id',
        28,
        0,
        1,
        '',
        'Renewal Option Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'renewal_option_type_xlate',
        27,
        0,
        1,
        '',
        'Renewal Option Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sublet_sw',
        33,
        0,
        1,
        '',
        'Sublet Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_rate_option_id',
        110,
        0,
        1,
        '',
        'Tax Rate Option Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_rate_option_xlate',
        109,
        0,
        1,
        '',
        'Tax Rate Option Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_summary_id',
        108,
        0,
        1,
        '',
        'Tax Summary Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_summary_xlate',
        107,
        0,
        1,
        '',
        'Tax Summary Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'time_stamp',
        4,
        0,
        1,
        '',
        'Time Stamp',
        300,
        '',
        '',
        'DATE',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'unique_lease_identifier',
        104,
        0,
        1,
        '',
        'Unique Lease Identifier',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'user_id',
        5,
        0,
        1,
        '',
        'User Id',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'vendor_id',
        39,
        0,
        1,
        '',
        'Vendor Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'vendor_xlate',
        38,
        0,
        1,
        '',
        'Vendor Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'workflow_type_id',
        21,
        0,
        1,
        '',
        'Workflow Type Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'workflow_type_xlate',
        20,
        0,
        1,
        '',
        'Workflow Type Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/