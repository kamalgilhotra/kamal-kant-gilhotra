
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Import Asset Archive';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select run.description import_run_description,
  arc.IMPORT_RUN_ID, LINE_ID, arc.TIME_STAMP, arc.USER_ID, LS_ASSET_ID, LEASED_ASSET_NUMBER, ILR_XLATE, ILR_ID, arc.DESCRIPTION, LONG_DESCRIPTION,
  QUANTITY, FMV, COMPANY_XLATE, COMPANY_ID, BUS_SEGMENT_XLATE, BUS_SEGMENT_ID, UTILITY_ACCOUNT_XLATE, UTILITY_ACCOUNT_ID, SUB_ACCOUNT_XLATE,
  SUB_ACCOUNT_ID, RETIREMENT_UNIT_XLATE, RETIREMENT_UNIT_ID, PROPERTY_GROUP_XLATE, PROPERTY_GROUP_ID, WORK_ORDER_XLATE, WORK_ORDER_ID,
  ASSET_LOCATION_XLATE, ASSET_LOCATION_ID, GUARANTEED_RESIDUAL_AMOUNT, EXPECTED_LIFE, ECONOMIC_LIFE, arc.NOTES, CLASS_CODE_XLATE1, CLASS_CODE_ID1,
  CLASS_CODE_VALUE1, CLASS_CODE_XLATE2, CLASS_CODE_ID2, CLASS_CODE_VALUE2, CLASS_CODE_XLATE3, CLASS_CODE_ID3, CLASS_CODE_VALUE3, CLASS_CODE_XLATE4,
  CLASS_CODE_ID4, CLASS_CODE_VALUE4, CLASS_CODE_XLATE5, CLASS_CODE_ID5, CLASS_CODE_VALUE5, CLASS_CODE_XLATE6, CLASS_CODE_ID6, CLASS_CODE_VALUE6, CLASS_CODE_XLATE7,
  CLASS_CODE_ID7, CLASS_CODE_VALUE7, CLASS_CODE_XLATE8, CLASS_CODE_ID8, CLASS_CODE_VALUE8, CLASS_CODE_XLATE9, CLASS_CODE_ID9, CLASS_CODE_VALUE9, CLASS_CODE_XLATE10,
  CLASS_CODE_ID10, CLASS_CODE_VALUE10, CLASS_CODE_XLATE11, CLASS_CODE_ID11, CLASS_CODE_VALUE11, CLASS_CODE_XLATE12, CLASS_CODE_ID12, CLASS_CODE_VALUE12, CLASS_CODE_XLATE13,
  CLASS_CODE_ID13, CLASS_CODE_VALUE13, CLASS_CODE_XLATE14, CLASS_CODE_ID14, CLASS_CODE_VALUE14, CLASS_CODE_XLATE15, CLASS_CODE_ID15, CLASS_CODE_VALUE15, CLASS_CODE_XLATE16,
  CLASS_CODE_ID16, CLASS_CODE_VALUE16, CLASS_CODE_XLATE17, CLASS_CODE_ID17, CLASS_CODE_VALUE17, CLASS_CODE_XLATE18, CLASS_CODE_ID18, CLASS_CODE_VALUE18, CLASS_CODE_XLATE19,
  CLASS_CODE_ID19, CLASS_CODE_VALUE19, CLASS_CODE_XLATE20, CLASS_CODE_ID20, CLASS_CODE_VALUE20, LOADED, IS_MODIFIED, ERROR_MESSAGE, UNIQUE_ASSET_IDENTIFIER, SERIAL_NUMBER,
  TAX_ASSET_LOCATION_XLATE, TAX_ASSET_LOCATION_ID, DEPARTMENT_XLATE,
  DEPARTMENT_ID, ESTIMATED_RESIDUAL, TAX_SUMMARY_ID, TAX_SUMMARY_XLATE, USED_YN_SW, PROPERTY_TAX_DATE, PROPERTY_TAX_AMOUNT
  from pp_import_run run, ls_import_asset_archive arc
  where run.import_run_id = arc.import_run_id          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'asset_location_id',
        29,
        0,
        1,
        '',
        'Asset Location Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'asset_location_xlate',
        28,
        0,
        1,
        '',
        'Asset Location Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'bus_segment_id',
        17,
        0,
        1,
        '',
        'Bus Segment Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'bus_segment_xlate',
        16,
        0,
        1,
        '',
        'Bus Segment Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id1',
        35,
        0,
        1,
        '',
        'Class Code Id1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id10',
        62,
        0,
        1,
        '',
        'Class Code Id10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id11',
        65,
        0,
        1,
        '',
        'Class Code Id11',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id12',
        68,
        0,
        1,
        '',
        'Class Code Id12',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id13',
        71,
        0,
        1,
        '',
        'Class Code Id13',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id14',
        74,
        0,
        1,
        '',
        'Class Code Id14',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id15',
        77,
        0,
        1,
        '',
        'Class Code Id15',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id16',
        80,
        0,
        1,
        '',
        'Class Code Id16',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id17',
        83,
        0,
        1,
        '',
        'Class Code Id17',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id18',
        86,
        0,
        1,
        '',
        'Class Code Id18',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id19',
        89,
        0,
        1,
        '',
        'Class Code Id19',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id2',
        38,
        0,
        1,
        '',
        'Class Code Id2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id20',
        92,
        0,
        1,
        '',
        'Class Code Id20',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id3',
        41,
        0,
        1,
        '',
        'Class Code Id3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id4',
        44,
        0,
        1,
        '',
        'Class Code Id4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id5',
        47,
        0,
        1,
        '',
        'Class Code Id5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id6',
        50,
        0,
        1,
        '',
        'Class Code Id6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id7',
        53,
        0,
        1,
        '',
        'Class Code Id7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id8',
        56,
        0,
        1,
        '',
        'Class Code Id8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_id9',
        59,
        0,
        1,
        '',
        'Class Code Id9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value1',
        36,
        0,
        1,
        '',
        'Class Code Value1',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value10',
        63,
        0,
        1,
        '',
        'Class Code Value10',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value11',
        66,
        0,
        1,
        '',
        'Class Code Value11',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value12',
        69,
        0,
        1,
        '',
        'Class Code Value12',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value13',
        72,
        0,
        1,
        '',
        'Class Code Value13',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value14',
        75,
        0,
        1,
        '',
        'Class Code Value14',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value15',
        78,
        0,
        1,
        '',
        'Class Code Value15',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value16',
        81,
        0,
        1,
        '',
        'Class Code Value16',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value17',
        84,
        0,
        1,
        '',
        'Class Code Value17',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value18',
        87,
        0,
        1,
        '',
        'Class Code Value18',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value19',
        90,
        0,
        1,
        '',
        'Class Code Value19',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value2',
        39,
        0,
        1,
        '',
        'Class Code Value2',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value20',
        93,
        0,
        1,
        '',
        'Class Code Value20',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value3',
        42,
        0,
        1,
        '',
        'Class Code Value3',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value4',
        45,
        0,
        1,
        '',
        'Class Code Value4',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value5',
        48,
        0,
        1,
        '',
        'Class Code Value5',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value6',
        51,
        0,
        1,
        '',
        'Class Code Value6',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value7',
        54,
        0,
        1,
        '',
        'Class Code Value7',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value8',
        57,
        0,
        1,
        '',
        'Class Code Value8',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value9',
        60,
        0,
        1,
        '',
        'Class Code Value9',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate1',
        34,
        0,
        1,
        '',
        'Class Code Xlate1',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate10',
        61,
        0,
        1,
        '',
        'Class Code Xlate10',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate11',
        64,
        0,
        1,
        '',
        'Class Code Xlate11',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate12',
        67,
        0,
        1,
        '',
        'Class Code Xlate12',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate13',
        70,
        0,
        1,
        '',
        'Class Code Xlate13',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate14',
        73,
        0,
        1,
        '',
        'Class Code Xlate14',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate15',
        76,
        0,
        1,
        '',
        'Class Code Xlate15',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate16',
        79,
        0,
        1,
        '',
        'Class Code Xlate16',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate17',
        82,
        0,
        1,
        '',
        'Class Code Xlate17',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate18',
        85,
        0,
        1,
        '',
        'Class Code Xlate18',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate19',
        88,
        0,
        1,
        '',
        'Class Code Xlate19',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate2',
        37,
        0,
        1,
        '',
        'Class Code Xlate2',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate20',
        91,
        0,
        1,
        '',
        'Class Code Xlate20',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate3',
        40,
        0,
        1,
        '',
        'Class Code Xlate3',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate4',
        43,
        0,
        1,
        '',
        'Class Code Xlate4',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate5',
        46,
        0,
        1,
        '',
        'Class Code Xlate5',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate6',
        49,
        0,
        1,
        '',
        'Class Code Xlate6',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate7',
        52,
        0,
        1,
        '',
        'Class Code Xlate7',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate8',
        55,
        0,
        1,
        '',
        'Class Code Xlate8',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_xlate9',
        58,
        0,
        1,
        '',
        'Class Code Xlate9',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        15,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_xlate',
        14,
        0,
        1,
        '',
        'Company Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'department_id',
        102,
        0,
        1,
        '',
        'Department Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'department_xlate',
        101,
        0,
        1,
        '',
        'Department Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'description',
        10,
        0,
        1,
        '',
        'Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'economic_life',
        32,
        0,
        1,
        '',
        'Economic Life',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'error_message',
        96,
        0,
        1,
        '',
        'Error Message',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'estimated_residual',
        103,
        0,
        1,
        '',
        'Estimated Residual',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'expected_life',
        31,
        0,
        1,
        '',
        'Expected Life',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'fmv',
        13,
        0,
        1,
        '',
        'Fmv',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'guaranteed_residual_amount',
        30,
        1,
        1,
        '',
        'Guaranteed Residual Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_id',
        9,
        0,
        1,
        '',
        'Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_xlate',
        8,
        0,
        1,
        '',
        'Ilr Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_description',
        1,
        0,
        1,
        '',
        'Import Run Description',
        300,
        'description',
        '(select * from pp_import_run where import_template_id in (select import_template_id from pp_import_template where import_type_id = 253))',
        'VARCHAR2',
        0,
        'description',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'import_run_id',
        2,
        0,
        1,
        '',
        'Import Run Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'is_modified',
        95,
        0,
        1,
        '',
        'Is Modified',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'leased_asset_number',
        7,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'line_id',
        3,
        0,
        1,
        '',
        'Line Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'loaded',
        94,
        0,
        1,
        '',
        'Loaded',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'long_description',
        11,
        0,
        1,
        '',
        'Long Description',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        6,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'notes',
        33,
        0,
        1,
        '',
        'Notes',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'property_group_id',
        25,
        0,
        1,
        '',
        'Property Group Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'property_group_xlate',
        24,
        0,
        1,
        '',
        'Property Group Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'property_tax_amount',
        108,
        1,
        1,
        '',
        'Property Tax Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'property_tax_date',
        107,
        0,
        1,
        '',
        'Property Tax Date',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'quantity',
        12,
        0,
        1,
        '',
        'Quantity',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retirement_unit_id',
        23,
        0,
        1,
        '',
        'Retirement Unit Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retirement_unit_xlate',
        22,
        0,
        1,
        '',
        'Retirement Unit Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'serial_number',
        98,
        0,
        1,
        '',
        'Serial Number',
        300,
        'serial_number',
        'ls_component',
        'VARCHAR2',
        0,
        'serial_number',
        null,
        null,
        'serial_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sub_account_id',
        21,
        0,
        1,
        '',
        'Sub Account Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sub_account_xlate',
        20,
        0,
        1,
        '',
        'Sub Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_asset_location_id',
        100,
        0,
        1,
        '',
        'Tax Asset Location Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_asset_location_xlate',
        99,
        0,
        1,
        '',
        'Tax Asset Location Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_summary_id',
        104,
        0,
        1,
        '',
        'Tax Summary Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'tax_summary_xlate',
        105,
        0,
        1,
        '',
        'Tax Summary Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'time_stamp',
        4,
        0,
        1,
        '',
        'Time Stamp',
        300,
        '',
        '',
        'DATE',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'unique_asset_identifier',
        97,
        0,
        1,
        '',
        'Unique Asset Identifier',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'used_yn_sw',
        106,
        0,
        1,
        '',
        'Used Yn Sw',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'user_id',
        5,
        0,
        1,
        '',
        'User Id',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_account_id',
        19,
        0,
        1,
        '',
        'Utility Account Id',
        300,
        'description',
        'utility_account',
        'NUMBER',
        0,
        'utility_account_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_account_xlate',
        18,
        0,
        1,
        '',
        'Utility Account Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'work_order_id',
        27,
        0,
        1,
        '',
        'Work Order Id',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'work_order_xlate',
        26,
        0,
        1,
        '',
        'Work Order Xlate',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/