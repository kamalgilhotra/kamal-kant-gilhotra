
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease Transfer Detail';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select to_char(trf.month,''yyyymm'') as monthnum,
       la1.ls_asset_id as from_ls_asset_id,
       las1.set_of_books_id,
       lct.description as lease_cap_type,
       la1.leased_asset_number as from_leased_asset_number,
       ilr1.ilr_id as from_ilr_id, ilr1.ilr_number as from_ilr_number, co1.company_id as from_company_id,
       co1.description as from_company_description,
       al1.long_description as from_location, dg1.mid_period_method as from_depreciation_type,
       la2.ls_asset_id as to_ls_asset_id, la2.leased_asset_number as to_leased_asset_number,
       ilr2.ilr_id as to_ilr_id, ilr2.ilr_number as to_ilr_number,
       ll.lease_id, ll.lease_number,
       co2.company_id as to_company_id, co2.description as to_company_description,
       al2.long_description as to_location, dg2.mid_period_method as to_depreciation_type,
       las2.current_lease_cost,
       decode(las1.is_om,0, las1.beg_obligation,0) as from_beg_obligation,
       decode(las1.is_om,0, las1.end_obligation,0) as from_end_obligation,
       decode(las1.is_om,0, las1.beg_lt_obligation,0) as from_beg_lt_obligation,
       decode(las1.is_om,0, las1.end_lt_obligation,0) as from_end_lt_obligation,
       decode(las1.is_om,0, las1.beg_capital_cost,0) as from_begin_capital_cost,
       decode(las1.is_om,0, las1.end_capital_cost,0) as from_end_capital_cost,
       cprd1.beg_reserve_month, cprd1.reserve_trans_out,
       0 as to_beg_obligation,
       decode(las2.is_om,0, las2.end_obligation,0) as to_end_obligation,
       0 as to_beg_lt_obligation,
       decode(las2.is_om,0, las2.end_lt_obligation,0) as to_end_lt_obligation,
       0 as to_begin_capital_cost,
       decode(las2.is_om,0, las2.end_capital_cost,0) as to_end_capital_cost,
       cprd2.reserve_trans_in,
       cprd2.depr_exp_alloc_adjust as depreciation_true_up
from ls_asset la1, ls_asset la2, ls_ilr ilr1, ls_ilr ilr2, ls_lease ll,
     ls_asset_schedule las1, ls_asset_schedule las2, ls_asset_transfer_history trf, ls_lease_cap_type lct,
     company co1, company co2, cpr_depr cprd1, ls_cpr_asset_map map1,
     cpr_depr cprd2, depr_group dg1, depr_group dg2, ls_cpr_asset_map map2,
     asset_location al1, asset_location al2
where la1.ls_asset_id = las1.ls_asset_id
  and la1.approved_revision = las1.revision
  and la2.ls_asset_id = las2.ls_asset_id
  and la2.approved_revision = las2.revision
  and la1.ilr_id = ilr1.ilr_id
  and la2.ilr_id = ilr2.ilr_id
  and ilr1.lease_id = ll.lease_id
  and ll.lease_cap_type_id = lct.ls_lease_cap_type_id
  and trf.from_ls_asset_id = la1.ls_asset_id
  and trf.to_ls_asset_id = la2.ls_asset_id
  and las1.month = las2.month
  and las1.month = trf.month
  and la1.company_id = co1.company_id
  and la2.company_id = co2.company_id
  and la1.ls_asset_id = map1.ls_asset_id
  and map1.asset_id = cprd1.asset_id (+)
  and cprd1.gl_posting_mo_yr = las1.month (+)
  and cprd1.depr_group_id = dg1.depr_group_id (+)
  and cprd1.set_of_books_id = las1.set_of_books_id (+)
  and la2.ls_asset_id = map2.ls_asset_id
  and map2.asset_id = cprd2.asset_id (+)
  and cprd2.gl_posting_mo_yr = las2.month (+)
  and cprd2.depr_group_id = dg2.depr_group_id (+)
  and cprd2.set_of_books_id = las2.set_of_books_id (+)
  and las1.set_of_books_id = las2.set_of_books_id
  and al1.asset_location_id = la1.asset_location_id
  and al2.asset_location_id = la2.asset_location_id
  and to_char(trf.month, ''yyyymm'') in (select filter_value from pp_any_required_filter where upper(trim(column_name)) = ''MONTHNUM'')          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'beg_reserve_month',
        31,
        1,
        1,
        '',
        'Beg Reserve Month',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'current_lease_cost',
        24,
        1,
        1,
        '',
        'Current Lease Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'depreciation_true_up',
        40,
        1,
        1,
        '',
        'Depreciation True Up',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_beg_lt_obligation',
        27,
        1,
        1,
        '',
        'From Beg Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_beg_obligation',
        25,
        1,
        1,
        '',
        'From Beg Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_begin_capital_cost',
        29,
        1,
        1,
        '',
        'From Begin Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_company_description',
        9,
        0,
        1,
        '',
        'From Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_company_id',
        8,
        0,
        1,
        '',
        'From Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_depreciation_type',
        12,
        0,
        1,
        '',
        'From Depreciation Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_end_capital_cost',
        30,
        1,
        1,
        '',
        'From End Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_end_lt_obligation',
        28,
        1,
        1,
        '',
        'From End Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_end_obligation',
        26,
        1,
        1,
        '',
        'From End Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_ilr_id',
        6,
        0,
        1,
        '',
        'From Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_ilr_number',
        7,
        0,
        1,
        '',
        'From Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_leased_asset_number',
        5,
        0,
        1,
        '',
        'From Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_location',
        11,
        0,
        1,
        '',
        'From Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'from_ls_asset_id',
        2,
        0,
        1,
        '',
        'From Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_cap_type',
        4,
        0,
        1,
        '',
        'Lease Cap Type',
        300,
        'description',
        'ls_lease_cap_type',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_id',
        17,
        0,
        1,
        '',
        'Lease Id',
        300,
        'lease_number',
        'ls_lease',
        'NUMBER',
        0,
        'lease_id',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'lease_number',
        18,
        0,
        1,
        '',
        'Lease Number',
        300,
        'lease_number',
        'ls_lease',
        'VARCHAR2',
        0,
        'lease_number',
        null,
        null,
        'lease_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'monthnum',
        1,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        2,
        'the_sort',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'reserve_trans_in',
        39,
        1,
        1,
        '',
        'Reserve Trans In',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'reserve_trans_out',
        32,
        1,
        1,
        '',
        'Reserve Trans Out',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books_id',
        3,
        null,
        1,
        '',
        'Set of Books ID',
        300,
        'description',
        'set_of_books',
        'VARCHAR2',
        null,
        'set_of_books_id',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_beg_lt_obligation',
        35,
        1,
        1,
        '',
        'To Beg Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_beg_obligation',
        33,
        1,
        1,
        '',
        'To Beg Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_begin_capital_cost',
        37,
        1,
        1,
        '',
        'To Begin Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_company_description',
        20,
        0,
        1,
        '',
        'To Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_company_id',
        19,
        0,
        1,
        '',
        'To Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_depreciation_type',
        23,
        0,
        1,
        '',
        'To Depreciation Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_end_capital_cost',
        38,
        1,
        1,
        '',
        'To End Capital Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_end_lt_obligation',
        36,
        1,
        1,
        '',
        'To End Lt Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_end_obligation',
        34,
        1,
        1,
        '',
        'To End Obligation',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_ilr_id',
        15,
        0,
        1,
        '',
        'To Ilr Id',
        300,
        'ilr_number',
        'ls_ilr',
        'NUMBER',
        0,
        'ilr_id',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_ilr_number',
        16,
        0,
        1,
        '',
        'To Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_leased_asset_number',
        14,
        0,
        1,
        '',
        'To Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_location',
        22,
        0,
        1,
        '',
        'To Location',
        300,
        'long_description',
        'asset_location',
        'VARCHAR2',
        0,
        'long_description',
        null,
        null,
        'long_description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'to_ls_asset_id',
        13,
        0,
        1,
        '',
        'To Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual;
END;
/