DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Lease GL Account Liability Balances by Asset (Company Currency)';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := 1;


  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_sql1 := 'select SET_OF_BOOKS_ID,SET_OF_BOOKS, ls_asset_id, leased_asset_number, ilr_number,
       company.description as company_description,
       company.company_id as company_id,
       (select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM'') as monthnum,
       gl.external_account_code,
       c.currency_display_symbol,
       c.iso_code AS currency,
       nvl(a.beg_cost,0) as beginning_cost,
       nvl(a.current_mo_adds, 0) as additions,
       nvl(a.current_mo_retirements,0) as retirements,
       nvl(a.current_mo_trf_to,0) as transfers_to,
       nvl(a.current_mo_trf_from,0) as transfers_from,
       nvl(a.end_cost,0) as ending_cost,
       decode(union_group, 2, sum(nvl(a.end_cost,0)) over(partition by ls_asset_id, set_of_books_id),0) as GAAP
from gl_account gl, company, currency c, currency_schema cs,
     (select cpra.SET_OF_BOOKS_ID, cpra.SET_OF_BOOKS SET_OF_BOOKS, ls_asset.ls_asset_id ls_asset_id, ls_asset.leased_asset_number leased_asset_number, ls_ilr.ilr_number ilr_number, company.company_id, cpr.gl_account_id,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), 0,activity_cost)) beg_cost,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             case when trim(activity_code) in (''UADD'', ''UADJ'') THEN  activity_cost ELSE 0 END,0)) current_mo_adds,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             case when trim(activity_code) in (''URGL'', ''URET'') THEN  activity_cost ELSE 0 END,0)) current_mo_retirements,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UTRT'', activity_cost,0),0)) current_mo_trf_to,
             sum(decode(cpra.gl_posting_mo_yr, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''),
             decode(trim(activity_code), ''UTRF'', activity_cost,0),0)) current_mo_trf_from,
             sum(cpra.activity_cost) end_cost,
             1 as union_group
      from (select ACT.ASSET_ID, ACT.activity_code, act.gl_posting_mo_yr, SB.SET_OF_BOOKS_ID,SB.DESCRIPTION SET_OF_BOOKS,
       sum(NVL(BAS.BASIS_1 * SB.BASIS_1_INDICATOR, 0) + NVL(BAS.BASIS_2 * SB.BASIS_2_INDICATOR, 0) +
           NVL(BAS.BASIS_3 * SB.BASIS_3_INDICATOR, 0) + NVL(BAS.BASIS_4 * SB.BASIS_4_INDICATOR, 0) +
           NVL(BAS.BASIS_5 * SB.BASIS_5_INDICATOR, 0) + NVL(BAS.BASIS_6 * SB.BASIS_6_INDICATOR, 0) +
           NVL(BAS.BASIS_7 * SB.BASIS_7_INDICATOR, 0) + NVL(BAS.BASIS_8 * SB.BASIS_8_INDICATOR, 0) +
           NVL(BAS.BASIS_9 * SB.BASIS_9_INDICATOR, 0) +
           NVL(BAS.BASIS_10 * SB.BASIS_10_INDICATOR, 0) +
           NVL(BAS.BASIS_11 * SB.BASIS_11_INDICATOR, 0) +
           NVL(BAS.BASIS_12 * SB.BASIS_12_INDICATOR, 0) +
           NVL(BAS.BASIS_13 * SB.BASIS_13_INDICATOR, 0) +
           NVL(BAS.BASIS_14 * SB.BASIS_14_INDICATOR, 0) +
           NVL(BAS.BASIS_15 * SB.BASIS_15_INDICATOR, 0) +
           NVL(BAS.BASIS_16 * SB.BASIS_16_INDICATOR, 0) +
           NVL(BAS.BASIS_17 * SB.BASIS_17_INDICATOR, 0) +
           NVL(BAS.BASIS_18 * SB.BASIS_18_INDICATOR, 0) +
           NVL(BAS.BASIS_19 * SB.BASIS_19_INDICATOR, 0) +
           NVL(BAS.BASIS_20 * SB.BASIS_20_INDICATOR, 0) +
           NVL(BAS.BASIS_21 * SB.BASIS_21_INDICATOR, 0) +
           NVL(BAS.BASIS_22 * SB.BASIS_22_INDICATOR, 0) +
           NVL(BAS.BASIS_23 * SB.BASIS_23_INDICATOR, 0) +
           NVL(BAS.BASIS_24 * SB.BASIS_24_INDICATOR, 0) +
           NVL(BAS.BASIS_25 * SB.BASIS_25_INDICATOR, 0) +
           NVL(BAS.BASIS_26 * SB.BASIS_26_INDICATOR, 0) +
           NVL(BAS.BASI';


  l_sql2 := 'S_27 * SB.BASIS_27_INDICATOR, 0) +
           NVL(BAS.BASIS_28 * SB.BASIS_28_INDICATOR, 0) +
           NVL(BAS.BASIS_29 * SB.BASIS_29_INDICATOR, 0) +
           NVL(BAS.BASIS_30 * SB.BASIS_30_INDICATOR, 0) +
           NVL(BAS.BASIS_31 * SB.BASIS_31_INDICATOR, 0) +
           NVL(BAS.BASIS_32 * SB.BASIS_32_INDICATOR, 0) +
           NVL(BAS.BASIS_33 * SB.BASIS_33_INDICATOR, 0) +
           NVL(BAS.BASIS_34 * SB.BASIS_34_INDICATOR, 0) +
           NVL(BAS.BASIS_35 * SB.BASIS_35_INDICATOR, 0) +
           NVL(BAS.BASIS_36 * SB.BASIS_36_INDICATOR, 0) +
           NVL(BAS.BASIS_37 * SB.BASIS_37_INDICATOR, 0) +
           NVL(BAS.BASIS_38 * SB.BASIS_38_INDICATOR, 0) +
           NVL(BAS.BASIS_39 * SB.BASIS_39_INDICATOR, 0) +
           NVL(BAS.BASIS_40 * SB.BASIS_40_INDICATOR, 0) +
           NVL(BAS.BASIS_41 * SB.BASIS_41_INDICATOR, 0) +
           NVL(BAS.BASIS_42 * SB.BASIS_42_INDICATOR, 0) +
           NVL(BAS.BASIS_43 * SB.BASIS_43_INDICATOR, 0) +
           NVL(BAS.BASIS_44 * SB.BASIS_44_INDICATOR, 0) +
           NVL(BAS.BASIS_45 * SB.BASIS_45_INDICATOR, 0) +
           NVL(BAS.BASIS_46 * SB.BASIS_46_INDICATOR, 0) +
           NVL(BAS.BASIS_47 * SB.BASIS_47_INDICATOR, 0) +
           NVL(BAS.BASIS_48 * SB.BASIS_48_INDICATOR, 0) +
           NVL(BAS.BASIS_49 * SB.BASIS_49_INDICATOR, 0) +
           NVL(BAS.BASIS_50 * SB.BASIS_50_INDICATOR, 0) +
           NVL(BAS.BASIS_51 * SB.BASIS_51_INDICATOR, 0) +
           NVL(BAS.BASIS_52 * SB.BASIS_52_INDICATOR, 0) +
           NVL(BAS.BASIS_53 * SB.BASIS_53_INDICATOR, 0) +
           NVL(BAS.BASIS_54 * SB.BASIS_54_INDICATOR, 0) +
           NVL(BAS.BASIS_55 * SB.BASIS_55_INDICATOR, 0) +
           NVL(BAS.BASIS_56 * SB.BASIS_56_INDICATOR, 0) +
           NVL(BAS.BASIS_57 * SB.BASIS_57_INDICATOR, 0) +
           NVL(BAS.BASIS_58 * SB.BASIS_58_INDICATOR, 0) +
           NVL(BAS.BASIS_59 * SB.BASIS_59_INDICATOR, 0) +
           NVL(BAS.BASIS_60 * SB.BASIS_60_INDICATOR, 0) +
           NVL(BAS.BASIS_61 * SB.BASIS_61_INDICATOR, 0) +
           NVL(BAS.BASIS_62 * SB.BASIS_62_INDICATOR, 0) +
           NVL(BAS.BASIS_63 * SB.BASIS_63_INDICATOR, 0) +
           NVL(BAS.BASIS_64 * SB.BASIS_64_INDICATOR, 0) +
           NVL(BAS.BASIS_65 * SB.BASIS_65_INDICATOR, 0) +
           NVL(BAS.BASIS_66 * SB.BASIS_66_INDICATOR, 0) +
           NVL(BAS.BASIS_67 * SB.BASIS_67_INDICATOR, 0) +
           NVL(BAS.BASIS_68 * SB.BASIS_68_INDICATOR, 0) +
           NVL(BAS.BASIS_69 * SB.BASIS_69_INDICATOR, 0) +
           NVL(BAS.BASIS_70 * SB.BASIS_70_INDICATOR, 0)) activity_cost
  from SET_OF_BOOKS SB, CPR_ACTIVITY ACT, CPR_ACT_BASIS BAS, ls_cpr_asset_map ls_map, cpr_ledger cpr, company_set_of_books c_sob
 where (ACT.ASSET_ACTIVITY_ID = BAS.ASSET_ACTIVITY_ID)
   and (ACT.ASSET_ID = BAS.ASSET_ID)
   and (ls_map.asset_id = act.asset_id)
   and cpr.asset_id = act.asset_id
   and c_sob.company_id = cpr.company_id
   and sb.set_of_books_id  = c_sob.set_of_books_id
   and sb.set_of_books_id = (select filter_value from pp_any_required_filter where upper(column_name) = ''SET OF BOOKS ID'')
   and act.gl_posting_mo_yr <= to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
   and cpr.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
 group by ACT.ASSET_ID, ACT.activity_code, act.gl_posting_mo_yr, SB.SET_OF_BOOKS_ID, SB.DESCRIPTION) cpra, ls_cpr_asset_map map, ls_asset, company, cpr_ledger cpr, ls_ilr, ls_lease
      where ls_asset.company_id = company.company_id
        and cpra.asset_id = map.asset_id
        and cpra.asset_id = cpr.asset_id
        and map.ls_asset_id = ls_asset.ls_asset_id
		and ls_asset.ilr_id = ls_ilr.ilr_id
		and ls_ilr.lease_id = ls_lease.lease_id
        and (ls_asset.ls_asset_status_id in (3,5)
             or (ls_asset.ls_asset_status_id = 4 and ls_asset.retirement_date >= add_months(to_date((select f';

  l_sql3 := 'ilter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''), ''yyyymm''),-1)))
        and cpra.gl_posting_mo_yr <= to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
        and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
       group by ls_asset.ls_asset_id,company.company_id, cpr.gl_account_id, ls_asset.leased_asset_number, ls_ilr.ilr_number, cpra.set_of_books, cpra.SET_OF_BOOKS_ID
UNION ALL
select sob.SET_OF_BOOKS_ID, sob.description, ls_asset.ls_asset_id,ls_asset.leased_asset_number, ls_ilr.ilr_number, company.company_id, ilra.st_oblig_account_id,
       -1 * nvl(sum(case when nvl(additions.is_add,0) = 1 then 0 else decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_liability - las.end_lt_liability)end),0) as beg_cost,
 	     -1 * sum(case when (nvl(is_trf,0)=0 and nvl(is_trft,0) = 0 and Nvl(is_ret,0) = 0 )
                         then (decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_liability - las.end_lt_liability,0)  + nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability - las.end_liability,0),0))
                                when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trft,0) = 0 and ((nvl(is_early_ret,0)=1 and Nvl(is_ret,0) = 1)
                      or (nvl(is_ret, 0) = 1 and nvl(is_trf,0) =1)))
                                      then -1 * las.principal_paid
                   WHEN (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trft,0) = 0 AND nvl(is_trft,0) = 0 AND Nvl(is_Ret,0) = 1 AND Nvl(is_early_ret,0)= 0 AND Nvl(is_add,0) = 1)
                                      then las.principal_paid else 0
                                      END -
        CASE WHEN Nvl(additions.is_add,0) = 1 THEN -1 * nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_liability - las.end_lt_liability,0),0) ELSE 0 END) as current_mo_adds,
        -1 * sum(case when (nvl(is_ret,0) = 1 and nvl(is_trf,0) <> 1 and nvl(is_early_ret,0) = 1)  then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_liability - las.end_lt_liability +  las.principal_paid,0) -  nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_liability - las.end_lt_liability,0),0)        when (nvl(is_ret,0) = 1 and nvl(is_trf,0) <> 1 and nvl(is_early_ret,0) = 0)          then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), (las.beg_liability - las.beg_lt_liability) *-1 ,0)      else 0 end) as current_mo_retirements,
        -1 * sum(case when (las.month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trft,0) = 1)  then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_liability - las.end_lt_liability,0) + nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability - las.end_liability,0),0) else 0 end) as current_mo_trf_to,
        -1 * sum(case when';

  l_sql4 := ' (ls_asset.retirement_date = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trf,0) = 1)  then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_liability - las.end_lt_liability + las.principal_paid,0) + nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability - las.end_liability,0),0) else 0 end) as current_mo_trf_from,
        -1 * sum(decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), (las.end_liability - las.end_lt_liability), 0)) as end_cost,
         2 as union_group
    from ls_ilr_account ilra, ls_ilr, ls_asset, v_ls_asset_schedule_fx_vw las, ls_ilr_options ilro, ls_lease_cap_type lct, company, ls_lease, set_of_books sob,
        (select a.*, 1 is_trf from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trf,
        (select a.*, 1 is_trft from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trft,
        (SELECT a.*, 1 is_add FROM cpr_activity a WHERE gl_posting_mo_yr = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
                                                  and asset_activity_id = 1
                                                  and trim(activity_code) = ''UADD'') additions,
        (SELECT a.*, 1 is_ret FROM cpr_activity a WHERE gl_posting_mo_yr = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
                                                  and trim(activity_code) = ''URGL'') retirements,
                        ls_cpr_asset_map map
    where las.ls_asset_id = ls_asset.ls_asset_id
      and las.revision = ls_asset.approved_revision
      and ls_asset.ilr_id = ls_ilr.ilr_id
      and ls_ilr.ilr_id = ilra.ilr_id
      and ls_ilr.ilr_id = ilro.ilr_id
      and ls_ilr.current_revision = ilro.revision
	    and ls_ilr.lease_id = ls_lease.lease_id
      and ls_asset.ls_asset_id = map.ls_asset_id
      and map.asset_id = additions.asset_id (+)
      and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
      and las.is_om = 0
      and ls_asset.company_id = company.company_id
      and las.month in (to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1))
      and ls_asset.ls_asset_id = trf.from_ls_asset_id (+)
      and ls_asset.ls_asset_id = trft.to_ls_asset_id (+)
      and map.asset_id = retirements.asset_id (+)
      and ls_asset.ls_asset_status_id >=3
      and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      AND las.ls_cur_type = 2
      and las.set_of_books_id = sob.set_of_books_id
      and sob.set_of_books_id = (select filter_value from pp_any_required_filter where upper(column_name) = ''SET OF BOOKS ID'')
  group by ls_asset.ls_asset_id,ilra.st_oblig_account_id, company.company_id, ls_asset.leased_asset_number, ls_ilr.ilr_number, sob.description, sob.SET_OF_BOOKS_ID
UNION ALL
select sob.SET_OF_BOOKS_ID, sob.description, ls_asset.ls_asset_id,ls_asset.leased_asset_number, ls_ilr.ilr_Number, company.company_id, ilra.lt_oblig_account_id,
     -1 * nvl(sum(CASE WHEN Nvl(is_add,0) = 1 THEN 0 ELSE decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTH';


  l_sql5 := 'NUM''),''yyyymm''), -1), las.end_lt_liability,0) end),0) as beg_cost,
       -1 * sum(case when (nvl(is_trf,0)=0 and nvl(is_trft,0) = 0 and nvl(is_ret,0) = 0)
                                      then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_lt_liability,0) - nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability,0),0)
                                      else 0 END -
                CASE WHEN Nvl(additions.is_add,0) = 1 THEN -1 * nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability,0),0) ELSE 0 END) as current_mo_adds,
       -1 * sum(case when (Nvl(is_ret,0) = 1  and nvl(is_trf,0) <> 1)
                                      then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_lt_liability,0) -
                                           decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability,0) else 0 end) as current_mo_retirements,
       -1 * sum(case when (nvl(is_trft,0) = 1)
                                      then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_lt_liability,0) - nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability,0),0) else 0 end) as current_mo_trf_to,
       -1 * sum(case when (ls_asset.retirement_date = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'') and nvl(is_trf,0) = 1)
                                      then decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_lt_liability,0) - nvl(decode(las.month, add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1), las.end_lt_liability,0),0) else 0 end) as current_mo_trf_from,
       -1 * nvl(sum(decode(las.month, to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), las.end_lt_liability)),0) as end_cost,
        3 as union_group
    from ls_ilr_account ilra, ls_ilr, ls_asset, v_ls_asset_schedule_fx_vw las, ls_ilr_options ilro, ls_lease_cap_type lct, company, ls_lease, set_of_books sob,
        (select a.*, 1 is_trf from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trf,
        (select a.*, 1 is_trft from ls_asset_transfer_history a where month = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')) trft,
        (SELECT a.*, 1 is_add FROM cpr_activity a WHERE gl_posting_mo_yr = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
                                                  and asset_activity_id = 1
                                                  and trim(activity_code) = ''UADD'') additions,
        (SELECT a.*, 1 is_ret FROM cpr_activity a WHERE gl_posting_mo_yr = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
                                                  and trim(activity_code) = ''URGL'') retirements,
                        ls_cpr_asset_map map
    where las.ls_asset_id = ls_asset.ls_asset_id
      and las.revision = ls_asset.approved_revision
      and ls_asset.ilr_id = ls_ilr.ilr_id
      and ls';


  l_sql6 := '_ilr.ilr_id = ilra.ilr_id
      and ls_ilr.ilr_id = ilro.ilr_id
	  and ls_ilr.lease_id = ls_lease.lease_id
      and ls_ilr.current_revision = ilro.revision
      and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
      and las.is_om = 0
      and ls_asset.company_id = company.company_id
      and las.month in (to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), add_months(to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm''), -1))
      and ls_asset.ls_asset_id = trf.from_ls_asset_id (+)
      and ls_asset.ls_asset_id = trft.to_ls_asset_id (+)
      AND ls_asset.ls_asset_id = map.ls_asset_id
      AND map.asset_id = additions.asset_id (+)
      AND map.asset_id = retirements.asset_id (+)
      and ls_asset.ls_asset_status_id >=3
      and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
      AND las.ls_cur_type = 2
      and las.set_of_books_id = sob.set_of_books_id
      and sob.set_of_books_id = (select filter_value from pp_any_required_filter where upper(column_name) = ''SET OF BOOKS ID'')
  group by ls_asset.ls_asset_id,ilra.lt_oblig_account_id, company.company_id, sob.description, ls_asset.leased_asset_number, ls_ilr.ilr_Number, sob.SET_OF_BOOKS_ID
UNION ALL
select sob.SET_OF_BOOKS_ID, sob.description, ls_asset.ls_asset_id,ls_asset.leased_asset_number, ls_ilr.ilr_number, company.company_id, dg.reserve_acct_id,
       -1 *  nvl(sum(nvl(cprd.beg_reserve_month,0)),0) as beg_cost,
       -1 * sum(nvl(curr_depr_expense,0) + nvl(depr_exp_alloc_adjust,0)+ Nvl(reserve_adjustment, 0)) as current_mo_adds,
       -1 * sum(nvl(retirements,0) + nvl(gain_loss,0)) as current_mo_retirements,
       -1 * sum(nvl(reserve_trans_in,0)) as current_mo_trf_to,
       -1 * sum(nvl(reserve_trans_out,0)) as current_mo_trf_from,
       -1 * sum(nvl(depr_reserve,0)) as end_cost,
       4 as union_group
from company, cpr_depr cprd, cpr_ledger cpr, ls_cpr_asset_map map, depr_group dg, ls_asset, ls_ilr, ls_lease, ls_ilr_options ilro, ls_lease_cap_type lct, set_of_books sob,ls_fasb_cap_type_sob_map
where company.company_id = cpr.company_id
  and map.asset_id = cpr.asset_id
  and cpr.asset_id = cprd.asset_id
  and cprd.gl_posting_mo_yr = to_date((select filter_value from pp_any_required_filter where upper(column_name) = ''MONTHNUM''),''yyyymm'')
  and cpr.depr_group_id = dg.depr_group_id
  and ls_asset.ls_asset_id = map.ls_asset_id
  and ls_asset.ilr_id = ls_ilr.ilr_id
  and company.company_id in (select filter_value from pp_any_required_filter where upper(column_name) = ''COMPANY ID'')
  and ls_ilr.lease_id = ls_lease.lease_id
  and ls_ilr.ilr_id = ilro.ilr_id
  and ls_ilr.current_revision = ilro.revision
  and ilro.lease_cap_type_id = lct.ls_lease_cap_type_id
  and ls_fasb_cap_type_sob_map.set_of_books_id = cprd.set_of_books_id
  and ls_fasb_cap_type_sob_map.lease_cap_type_id = ilro.lease_cap_type_id
  and ls_fasb_cap_type_sob_map.fasb_cap_type_id in (1,2,3,6)
  and sob.set_of_books_id = cprd.set_of_books_id
  and sob.set_of_books_id = (select filter_value from pp_any_required_filter where upper(column_name) = ''SET OF BOOKS ID'')
group by ls_asset.ls_asset_id,company.company_id, dg.reserve_acct_id,sob.description, ls_asset.leased_asset_number, ls_ilr.ilr_number, sob.SET_OF_BOOKS_ID
) a
where a.gl_account_id = gl.gl_account_id
  and a.company_id = company.company_id
  and cs.company_id = company.company_id
  and cs.currency_id = c.currency_id
  and cs.currency_type_id = 1
order by SET_OF_BOOKS_ID, external_account_code, leased_asset_number' ;


  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'additions',
        13,
        1,
        1,
        '',
        'Additions',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'beginning_cost',
        12,
        1,
        1,
        '',
        'Beginning Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_description',
        5,
        0,
        1,
        '',
        'Company Description',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'VARCHAR2',
        0,
        'description',
        null,
        null,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'company_id',
        1,
        0,
        1,
        '',
        'Company Id',
        300,
        'description',
        '(select * from company where is_lease_company = 1)',
        'NUMBER',
        0,
        'company_id',
        1,
        2,
        'description',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'currency',
        11,
        0,
        0,
        '',
        'Currency',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'currency_display_symbol',
        10,
        0,
        0,
        '',
        'Currency Symbol',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        1,
        1
FROM dual
UNION ALL
SELECT  l_id,
        'ending_cost',
        17,
        1,
        1,
        '',
        'Ending Cost',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'external_account_code',
        9,
        0,
        1,
        '',
        'External Account Code',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0

FROM dual
UNION ALL
SELECT  l_id,
        'gaap',
        18,
        1,
        1,
        '',
        'Gaap',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ls_asset_id',
        6,
        0,
        1,
        '',
        'Ls Asset Id',
        300,
        'leased_asset_number',
        'ls_asset',
        'NUMBER',
        0,
        'ls_asset_id',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'monthnum',
        2,
        0,
        1,
        '',
        'Monthnum',
        300,
        'month_number',
        '(select month_number from pp_calendar)',
        'VARCHAR2',
        0,
        'month_number',
        1,
        1,
        'the_sort',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retirements',
        14,
        1,
        1,
        '',
        'Retirements',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'transfers_from',
        16,
        1,
        1,
        '',
        'Transfers From',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'transfers_to',
        15,
        1,
        1,
        '',
        'Transfers To',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'ilr_number',
        8,
        0,
        1,
        '',
        'Ilr Number',
        300,
        'ilr_number',
        'ls_ilr',
        'VARCHAR2',
        0,
        'ilr_number',
        null,
        null,
        'ilr_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'leased_asset_number',
        7,
        0,
        1,
        '',
        'Leased Asset Number',
        300,
        'leased_asset_number',
        'ls_asset',
        'VARCHAR2',
        0,
        'leased_asset_number',
        null,
        null,
        'leased_asset_number',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books',
        4,
        0,
        1,
        '',
        'Set Of Books',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'set_of_books_id',
        3,
        0,
        1,
        '',
        'Set Of Books Id',
        300,
        'description',
        'set_of_books',
        'NUMBER',
        0,
        'set_of_books_id',
        1,
        1,
        'description',
        0,
        0
FROM dual
;

END;

/


