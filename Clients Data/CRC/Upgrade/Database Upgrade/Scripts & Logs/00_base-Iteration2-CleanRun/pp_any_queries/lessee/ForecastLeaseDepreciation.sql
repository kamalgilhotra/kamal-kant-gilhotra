DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Forecast Lease Depreciation';
  l_table_name := 'Dynamic View';
  l_subsystem := 'lessee';
  l_query_type := 'user';
  l_is_multicurrency := null;

  l_query_sql := '
SELECT fct.description as forecast_version, la.leased_asset_number, ilr.ilr_number, mla.lease_number, cs.description as company, sob.description as sob,
	  to_number(to_char(ldf.MONTH,''yyyymm'')) as month, ldf.depr_expense, sch.impairment_activity, ldf.begin_reserve, ldf.end_reserve, sch.end_capital_cost,
	  (sch.end_capital_cost-ldf.end_reserve) as ROU, llct.description as currency_type, curr.iso_code, curr.currency_display_symbol
	  FROM ls_lease mla,
		  ls_ilr ilr,
		  ls_asset la,
		  ls_depr_forecast ldf,
		  company cs,
		  set_of_books sob,
		  v_ls_asset_schedule_fx_vw sch,
		  ls_forecast_version fct,
          ls_lease_currency_type llct,
          currency curr
	  WHERE la.ilr_id = ilr.ilr_id
	  AND   ilr.lease_id = mla.lease_id
	  AND   fct.revision = ldf.revision
	  AND   abs(fct.set_of_books_id) = abs(sob.set_of_books_id)
	  AND   la.ls_asset_id = ldf.ls_asset_id
	  AND   la.company_id = cs.company_id
	  and   ldf.set_of_books_id = sob.set_of_books_id
	  AND   la.ls_asset_status_id = 3
	  and   sch.ls_asset_id = la.ls_asset_id
	  and   sch.revision = fct.revision
	  and   sch.month = ldf.month
	  and   sch.set_of_books_id = sob.set_of_books_id
      and   curr.currency_id = mla.contract_currency_id
      and   llct.ls_currency_type_id = sch.ls_cur_type
	  ORDER BY la.leased_asset_number, ldf.MONTH, sob.description';

  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/

  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));

  IF l_cnt > 0 THEN

    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));

    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;

    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;

  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));

  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);

  insert into pp_any_query_criteria(id,
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);

  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/

INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)
SELECT l_id,'forecast_version',1,0,1,NULL,'Forecast Version',300,'description','ls_forecast_version','VARCHAR2',0,'description',1,2,'description',0,0 FROM dual UNION ALL
SELECT l_id,'sob',2,0,1,NULL,'Set Of Books',300,'description','(select * from set_of_books where set_of_books_id in (select set_of_books_id from ls_fasb_cap_type_sob_map))','VARCHAR2',0,'description',0,0,'description',0,0 FROM dual UNION ALL
SELECT l_id,'company',3,0,1,NULL,'Company',300,'description','(select * from company where is_lease_company = 1)','VARCHAR2',0,'description',0,0,'description',0,0 FROM dual UNION ALL
SELECT l_id,'lease_number',4,0,1,NULL,'Lease Number',300,'lease_number','ls_lease','VARCHAR2',0,'lease_number',0,0,'lease_number',0,0 FROM dual UNION ALL
SELECT l_id,'ilr_number',5,0,1,NULL,'ILR Number',300,'ilr_number','ls_ilr','VARCHAR2',0,'ilr_number',0,0,'ilr_number',0,0 FROM dual UNION ALL
SELECT l_id,'leased_asset_number',6,0,1,NULL,'Leased Asset Number',300,'leased_asset_number','ls_asset','VARCHAR2',0,'leased_asset_number',0,0,'leased_asset_number',0,0 FROM dual UNION ALL
SELECT l_id,'month',7,0,1,NULL,'Month',300,'month_number','pp_calendar','number',0,'month_number',0,0,'month_number',0,0 FROM dual UNION ALL
SELECT l_id,'depr_expense',8,1,1,NULL,'Depr Expense',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'impairment_activity',9,1,1,NULL,'Impairment Amount',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'begin_reserve',10,1,1,NULL,'Begin Reserve',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_reserve',11,1,1,NULL,'End Reserve',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'end_capital_cost',12,1,1,NULL,'End Capital Cost',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'rou',13,1,1,NULL,'ROU',300,NULL,NULL,'NUMBER',0,NULL,0,0,NULL,0,0 FROM dual UNION ALL
SELECT l_id,'currency_type',14,0,1,NULL,'Currency Type',300,'description','ls_lease_currency_type','VARCHAR2',0,'description',1,2,'description',0,0 FROM dual UNION ALL
SELECT l_id,'currency_display_symbol',15,0,0,NULL,'Currency Symbol',300,NULL,NULL,'VARCHAR2',0,NULL,NULL,NULL,'currency_display_symbol',1,1 FROM dual UNION ALL
SELECT l_id,'iso_code',16,0,1,NULL,'Currency',300,'description','currency','VARCHAR2',0,'iso_code',NULL,NULL,'iso_code',0,0 FROM dual;

END;

/

COMMIT;