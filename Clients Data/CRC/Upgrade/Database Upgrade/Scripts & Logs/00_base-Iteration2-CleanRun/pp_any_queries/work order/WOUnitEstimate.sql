
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'WO Unit Estimate';
  l_table_name := 'Dynamic View';
  l_subsystem := 'work order';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := '	select fp.work_order_number project, 
			wo.work_order_number work_order, 
			rev.revision,
			bs.external_bus_segment op_seg_code,
			bs.description op_seg_description,
			et.description expenditure_type,
			ua.external_account_code utility_acct_code, 
			ua.description utility_acct_description,
			ru.external_retire_unit retire_unit_code, 
			ru.description retire_unit_description,
			ect.description est_chg_type,
			(select long_description from asset_location where asset_location.asset_location_id = nvl(woest.asset_location_id,wo.asset_location_id) ) asset_location,
			woest.serial_number serial_number,
			woest.quantity,
			woest.amount
	from 	company, 
			work_order_control wo,
			work_order_control fp,
			business_segment bs,
			wo_estimate woest,
			expenditure_type et,
			utility_account ua,
			retirement_unit ru,
			estimate_charge_type ect,
			(
				select 	a.work_order_id, 
							max(decode(sign(b.revision - 999),-1,a.current_revision,1000)) revision
				from work_order_control a, wo_estimate b 
				where a.funding_wo_indicator = 0
				and a.work_order_id = b.work_order_id
				group by a.work_order_id
			) rev
	where	wo.funding_wo_id = fp.work_order_id
	and 		wo.company_id = company.company_id
	and 		wo.bus_segment_id = bs.bus_segment_id
	and		wo.work_order_id = rev.work_order_id
	and 		woest.work_order_id = rev.work_order_id
	and		woest.revision = rev.revision
	and 		woest.work_order_id = wo.work_order_id
	and 		woest.expenditure_type_id = et.expenditure_type_id
	and		woest.utility_account_id = ua.utility_account_id
	and 		woest.retirement_unit_id = ru.retirement_unit_id
	and		woest.est_chg_type_id = ect.est_chg_type_id
order by project, work_order          ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'amount',
        15,
        1,
        1,
        '',
        'Amount',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'asset_location',
        12,
        0,
        1,
        '',
        'Asset Location',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'est_chg_type',
        11,
        0,
        1,
        '',
        'Est Chg Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'expenditure_type',
        6,
        0,
        1,
        '',
        'Expenditure Type',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'op_seg_code',
        4,
        0,
        1,
        '',
        'Op Seg Code',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'op_seg_description',
        5,
        0,
        1,
        '',
        'Op Seg Description',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'project',
        1,
        0,
        1,
        '',
        'Project',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'quantity',
        14,
        0,
        1,
        '',
        'Quantity',
        300,
        '',
        '',
        'NUMBER',
        1,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retire_unit_code',
        9,
        0,
        1,
        '',
        'Retire Unit Code',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'retire_unit_description',
        10,
        0,
        1,
        '',
        'Retire Unit Description',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'revision',
        3,
        0,
        1,
        '',
        'Revision',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'serial_number',
        13,
        0,
        1,
        '',
        'Serial Number',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_acct_code',
        7,
        0,
        1,
        '',
        'Utility Acct Code',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_acct_description',
        8,
        0,
        1,
        '',
        'Utility Acct Description',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'work_order',
        2,
        0,
        1,
        '',
        'Work Order',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/