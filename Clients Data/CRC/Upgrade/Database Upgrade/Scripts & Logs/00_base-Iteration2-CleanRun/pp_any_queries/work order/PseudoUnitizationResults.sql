
DECLARE
  l_query_description VARCHAR2(254);
  l_query_sql CLOB;
  l_table_name VARCHAR2(35);
  l_subsystem VARCHAR2(60);
  l_query_type VARCHAR2(35);
  l_is_multicurrency number(1,0);
  l_id NUMBER;
  l_sql1 VARCHAR2(4000);
  l_sql2 VARCHAR2(4000);
  l_sql3 VARCHAR2(4000);
  l_sql4 VARCHAR2(4000);
  l_sql5 VARCHAR(4000);
  l_sql6 VARCHAR2(4000);
  l_offset NUMBER;
  l_cnt number;
BEGIN

  l_query_description := 'Pseudo Unitization Results';
  l_table_name := 'Dynamic View';
  l_subsystem := 'work order';
  l_query_type := 'user';
  l_is_multicurrency := null;
  
  l_query_sql := 'select A.MONTH_NUMBER, C.GL_COMPANY_NO, D.BUDGET_ORGANIZATION, D.BUDGET_SUMMARY, D.BUDGET,        B.WORK_ORDER_NUMBER,        (select DESCRIPTION from GL_ACCOUNT where GL_ACCOUNT_ID = A.GL_ACCOUNT_ID) GL_ACCOUNT,        (select DESCRIPTION from BUSINESS_SEGMENT where BUS_SEGMENT_ID = A.BUS_SEGMENT_ID) BUS_SEGMENT,        (select DESCRIPTION            from UTILITY_ACCOUNT           where UTILITY_ACCOUNT_ID = A.UTILITY_ACCOUNT_ID             and BUS_SEGMENT_ID = A.BUS_SEGMENT_ID) UTILITY_ACCOUNT,        (select DESCRIPTION            from SUB_ACCOUNT           where SUB_ACCOUNT_ID = A.SUB_ACCOUNT_ID             and UTILITY_ACCOUNT_ID = A.UTILITY_ACCOUNT_ID             and BUS_SEGMENT_ID = A.BUS_SEGMENT_ID) SUB_ACCOUNT,        (select DESCRIPTION from MAJOR_LOCATION where MAJOR_LOCATION_ID = A.MAJOR_LOCATION_ID) MAJOR_LOCATION,        (select LONG_DESCRIPTION from ASSET_LOCATION where ASSET_LOCATION_ID = A.ASSET_LOCATION_ID) ASSET_LOCATION,       (select LONG_DESCRIPTION            from ASSET_LOCATION           where ASSET_LOCATION_ID = A.EST_ASSET_LOCATION_ID) EST_ASSET_LOCATION, CLASS_CODE_VALUE1,        CLASS_CODE_VALUE2, CLASS_CODE_VALUE3, CLASS_CODE_VALUE4, CLASS_CODE_VALUE5, CLASS_CODE_VALUE6,        CLASS_CODE_VALUE7, CLASS_CODE_VALUE8, CLASS_CODE_VALUE9, CLASS_CODE_VALUE10,        CLASS_CODE_VALUE11, CLASS_CODE_VALUE12, CLASS_CODE_VALUE13, CLASS_CODE_VALUE14,        CLASS_CODE_VALUE15, BASIS_1, BASIS_2, BASIS_3, BASIS_4, BASIS_5, BASIS_6, BASIS_7, BASIS_8,        BASIS_9, BASIS_10, BASIS_11, BASIS_12, BASIS_13, BASIS_14, BASIS_15, BASIS_16, BASIS_17,        BASIS_18, BASIS_19, BASIS_20, BASIS_21, BASIS_22, BASIS_23, BASIS_24, BASIS_25, BASIS_26,        BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32, BASIS_33, BASIS_34, BASIS_35,        BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41, BASIS_42, BASIS_43, BASIS_44,        BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50, BASIS_51, BASIS_52, BASIS_53,        BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59, BASIS_60, BASIS_61, BASIS_62,        BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68, BASIS_69, BASIS_70   from WO_PSEUDO_UNITIZE_SUMMARY A, WORK_ORDER_CONTROL B, COMPANY C,        (select BUD.BUDGET_ID, BUD.DESCRIPTION BUDGET, BUDS.DESCRIPTION BUDGET_SUMMARY,                 BUDO.DESCRIPTION BUDGET_ORGANIZATION            from BUDGET BUD            left outer join BUDGET_SUMMARY BUDS              on BUD.BUDGET_SUMMARY_ID = BUDS.BUDGET_SUMMARY_ID            left outer join BUDGET_ORGANIZATION BUDO              on BUD.BUDGET_ORGANIZATION_ID = BUDO.BUDGET_ORGANIZATION_ID) D  where A.WORK_ORDER_ID = B.WORK_ORDER_ID    and A.COMPANY_ID = B.COMPANY_ID    and A.COMPANY_ID = C.COMPANY_ID    and B.BUDGET_ID = D.BUDGET_ID           ';
  
  /*****************************************************************************
   BEGIN: DO NOT EDIT HERE!!
  *****************************************************************************/
  
  SELECT COUNT(1) INTO l_cnt
  FROM pp_any_query_criteria
  where lower(trim(description)) = lower(trim(l_query_description));
  
  IF l_cnt > 0 THEN
  
    SELECT ID INTO l_id
    FROM pp_any_query_criteria
    WHERE LOWER(TRIM(DESCRIPTION)) = LOWER(TRIM(l_query_description));
    
    DELETE FROM pp_any_query_criteria_fields
    where id = l_id;
  
    DELETE FROM pp_any_query_criteria
    WHERE ID = l_id;
  ELSE
    SELECT nvl(MAX(ID), 0) + 1 INTO l_id
    FROM pp_any_query_criteria;
  END IF;
  
  l_query_sql := TRIM(REGEXP_REPLACE(l_query_sql, '([[:space:]][[:space:]]+)|([[:cntrl:]]+)', ' '));
  
  l_sql1 := dbms_lob.substr(l_query_sql, 4000, 1);
  l_sql2 := dbms_lob.substr(l_query_sql, 4000, 4001);
  l_sql3 := dbms_lob.substr(l_query_sql, 4000, 8001);
  l_sql4 := dbms_lob.substr(l_query_sql, 4000, 12001);
  l_sql5 := dbms_lob.substr(l_query_sql, 4000, 16001);
  l_sql6 := dbms_lob.substr(l_query_sql, 4000, 20001);
  
  insert into pp_any_query_criteria(id, 
                                    source_id,
                                    criteria_field,
                                    table_name,
                                    DESCRIPTION,
                                    feeder_field,
                                    subsystem,
                                    query_type,
                                    SQL,
                                    sql2,
                                    sql3,
                                    sql4,
                                    sql5,
                                    sql6,
                                    is_multicurrency)
  values (l_id,
          1001,
          'none',
          l_table_name,
          l_query_description,
          'none',
          l_subsystem,
          l_query_type,
          l_sql1,
          l_sql2,
          l_sql3,
          l_sql4,
          l_sql5,
          l_sql6,
          l_is_multicurrency);
          
  /*****************************************************************************
   END: DO NOT EDIT HERE!!
  *****************************************************************************/
INSERT INTO pp_any_query_criteria_fields (ID,
                                            detail_field,
                                            column_order,
                                            amount_field,
                                            include_in_select_criteria,
                                            default_value,
                                            column_header,
                                            column_width,
                                            display_field,
                                            display_table,
                                            column_type,
                                            quantity_field,
                                            data_field,
                                            required_filter,
                                            required_one_mult,
                                            sort_col,
                                            hide_from_results,
                                            hide_from_filters)SELECT  l_id,
        'asset_location',
        12,
        0,
        1,
        '',
        'Asset Location',
        1000,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_1',
        29,
        1,
        1,
        '',
        'Basis 1',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_10',
        38,
        1,
        1,
        '',
        'Basis 10',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_11',
        39,
        1,
        1,
        '',
        'Basis 11',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_12',
        40,
        1,
        1,
        '',
        'Basis 12',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_13',
        41,
        1,
        1,
        '',
        'Basis 13',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_14',
        42,
        1,
        1,
        '',
        'Basis 14',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_15',
        43,
        1,
        1,
        '',
        'Basis 15',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_16',
        44,
        1,
        1,
        '',
        'Basis 16',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_17',
        45,
        1,
        1,
        '',
        'Basis 17',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_18',
        46,
        1,
        1,
        '',
        'Basis 18',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_19',
        47,
        1,
        1,
        '',
        'Basis 19',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_2',
        30,
        1,
        1,
        '',
        'Basis 2',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_20',
        48,
        1,
        1,
        '',
        'Basis 20',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_21',
        49,
        1,
        1,
        '',
        'Basis 21',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_22',
        50,
        1,
        1,
        '',
        'Basis 22',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_23',
        51,
        1,
        1,
        '',
        'Basis 23',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_24',
        52,
        1,
        1,
        '',
        'Basis 24',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_25',
        53,
        1,
        1,
        '',
        'Basis 25',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_26',
        54,
        1,
        1,
        '',
        'Basis 26',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_27',
        55,
        1,
        1,
        '',
        'Basis 27',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_28',
        56,
        1,
        1,
        '',
        'Basis 28',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_29',
        57,
        1,
        1,
        '',
        'Basis 29',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_3',
        31,
        1,
        1,
        '',
        'Basis 3',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_30',
        58,
        1,
        1,
        '',
        'Basis 30',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_31',
        59,
        1,
        1,
        '',
        'Basis 31',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_32',
        60,
        1,
        1,
        '',
        'Basis 32',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_33',
        61,
        1,
        1,
        '',
        'Basis 33',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_34',
        62,
        1,
        1,
        '',
        'Basis 34',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_35',
        63,
        1,
        1,
        '',
        'Basis 35',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_36',
        64,
        1,
        1,
        '',
        'Basis 36',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_37',
        65,
        1,
        1,
        '',
        'Basis 37',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_38',
        66,
        1,
        1,
        '',
        'Basis 38',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_39',
        67,
        1,
        1,
        '',
        'Basis 39',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_4',
        32,
        1,
        1,
        '',
        'Basis 4',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_40',
        68,
        1,
        1,
        '',
        'Basis 40',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_41',
        69,
        1,
        1,
        '',
        'Basis 41',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_42',
        70,
        1,
        1,
        '',
        'Basis 42',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_43',
        71,
        1,
        1,
        '',
        'Basis 43',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_44',
        72,
        1,
        1,
        '',
        'Basis 44',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_45',
        73,
        1,
        1,
        '',
        'Basis 45',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_46',
        74,
        1,
        1,
        '',
        'Basis 46',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_47',
        75,
        1,
        1,
        '',
        'Basis 47',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_48',
        76,
        1,
        1,
        '',
        'Basis 48',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_49',
        77,
        1,
        1,
        '',
        'Basis 49',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_5',
        33,
        1,
        1,
        '',
        'Basis 5',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_50',
        78,
        1,
        1,
        '',
        'Basis 50',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_51',
        79,
        1,
        1,
        '',
        'Basis 51',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_52',
        80,
        1,
        1,
        '',
        'Basis 52',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_53',
        81,
        1,
        1,
        '',
        'Basis 53',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_54',
        82,
        1,
        1,
        '',
        'Basis 54',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_55',
        83,
        1,
        1,
        '',
        'Basis 55',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_56',
        84,
        1,
        1,
        '',
        'Basis 56',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_57',
        85,
        1,
        1,
        '',
        'Basis 57',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_58',
        86,
        1,
        1,
        '',
        'Basis 58',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_59',
        87,
        1,
        1,
        '',
        'Basis 59',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_6',
        34,
        1,
        1,
        '',
        'Basis 6',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_60',
        88,
        1,
        1,
        '',
        'Basis 60',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_61',
        89,
        1,
        1,
        '',
        'Basis 61',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_62',
        90,
        1,
        1,
        '',
        'Basis 62',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_63',
        91,
        1,
        1,
        '',
        'Basis 63',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_64',
        92,
        1,
        1,
        '',
        'Basis 64',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_65',
        93,
        1,
        1,
        '',
        'Basis 65',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_66',
        94,
        1,
        1,
        '',
        'Basis 66',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_67',
        95,
        1,
        1,
        '',
        'Basis 67',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_68',
        96,
        1,
        1,
        '',
        'Basis 68',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_69',
        97,
        1,
        1,
        '',
        'Basis 69',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_7',
        35,
        1,
        1,
        '',
        'Basis 7',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_70',
        98,
        1,
        1,
        '',
        'Basis 70',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_8',
        36,
        1,
        1,
        '',
        'Basis 8',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'basis_9',
        37,
        1,
        1,
        '',
        'Basis 9',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'budget',
        5,
        0,
        1,
        '',
        'Budget',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'budget_organization',
        3,
        0,
        1,
        '',
        'Budget Organization',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'budget_summary',
        4,
        0,
        1,
        '',
        'Budget Summary',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'bus_segment',
        8,
        0,
        1,
        '',
        'Bus Segment',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value1',
        14,
        0,
        1,
        '',
        'Class Code Value1',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value10',
        23,
        0,
        1,
        '',
        'Class Code Value10',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value11',
        24,
        0,
        1,
        '',
        'Class Code Value11',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value12',
        25,
        0,
        1,
        '',
        'Class Code Value12',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value13',
        26,
        0,
        1,
        '',
        'Class Code Value13',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value14',
        27,
        0,
        1,
        '',
        'Class Code Value14',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value15',
        28,
        0,
        1,
        '',
        'Class Code Value15',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value2',
        15,
        0,
        1,
        '',
        'Class Code Value2',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value3',
        16,
        0,
        1,
        '',
        'Class Code Value3',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value4',
        17,
        0,
        1,
        '',
        'Class Code Value4',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value5',
        18,
        0,
        1,
        '',
        'Class Code Value5',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value6',
        19,
        0,
        1,
        '',
        'Class Code Value6',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value7',
        20,
        0,
        1,
        '',
        'Class Code Value7',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value8',
        21,
        0,
        1,
        '',
        'Class Code Value8',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'class_code_value9',
        22,
        0,
        1,
        '',
        'Class Code Value9',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'est_asset_location',
        13,
        0,
        1,
        '',
        'Est Asset Location',
        1000,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'gl_account',
        7,
        0,
        1,
        '',
        'Gl Account',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'gl_company_no',
        2,
        0,
        1,
        '',
        'Gl Company No',
        300,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'major_location',
        11,
        0,
        1,
        '',
        'Major Location',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'month_number',
        1,
        0,
        1,
        '',
        'Month Number',
        300,
        '',
        '',
        'NUMBER',
        0,
        '',
        1,
        2,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'sub_account',
        10,
        0,
        1,
        '',
        'Sub Account',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'utility_account',
        9,
        0,
        1,
        '',
        'Utility Account',
        800,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual
UNION ALL
SELECT  l_id,
        'work_order_number',
        6,
        0,
        1,
        '',
        'Work Order Number',
        500,
        '',
        '',
        'VARCHAR2',
        0,
        '',
        null,
        null,
        '',
        0,
        0
FROM dual;
END;
/