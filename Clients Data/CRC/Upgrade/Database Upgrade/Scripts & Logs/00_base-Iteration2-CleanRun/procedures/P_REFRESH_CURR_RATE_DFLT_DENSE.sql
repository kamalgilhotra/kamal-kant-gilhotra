
  CREATE OR REPLACE EDITIONABLE PROCEDURE "PWRPLANT"."P_REFRESH_CURR_RATE_DFLT_DENSE" as
begin                         
  merge into currency_rate_default_dense a
  using (select coalesce(y.exchange_date, x.exchange_date) as exchange_date,
                coalesce(y.currency_from, x.currency_from) as currency_from,
                coalesce(y.currency_to, x.currency_to) as currency_to,
                coalesce(y.exchange_rate_type_id, x.exchange_rate_type_id) as exchange_rate_type_id,
                coalesce(y.rate, -1) as rate,
                case when y.rate is null then 'KILL' else 'KEEP' end as status
          from currency_rate_default_dense x
          full join v_currency_rate_default_dense y  
            on (x.exchange_date = y.exchange_date 
                and x.currency_from = y.currency_from
                and x.currency_to = y.currency_to
                and x.exchange_rate_type_id = y.exchange_rate_type_id)) b on (a.exchange_date = b.exchange_date 
                                                                              and a.currency_from = b.currency_from
                                                                              and a.currency_to = b.currency_to
                                                                              and a.exchange_rate_type_id = b.exchange_rate_type_id)
  when matched then
    update set a.rate = b.rate
    where decode(a.rate, b.rate, 1, 0) = 0
    delete where b.status = 'KILL'
  when not matched then
    insert (a.exchange_date, a.currency_from, a.currency_to, a.exchange_rate_type_id, a.rate)
    values (b.exchange_date, b.currency_from, b.currency_to, b.exchange_rate_type_id, b.rate);
end;
/