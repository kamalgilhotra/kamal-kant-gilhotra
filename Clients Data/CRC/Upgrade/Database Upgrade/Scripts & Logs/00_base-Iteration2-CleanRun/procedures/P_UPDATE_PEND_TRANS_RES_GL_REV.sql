
  CREATE OR REPLACE EDITIONABLE PROCEDURE "PWRPLANT"."P_UPDATE_PEND_TRANS_RES_GL_REV" (P_PEND_TRANS_ID      PEND_TRANSACTION.PEND_TRANS_ID%type,
                                                           P_RESERVE            PEND_TRANSACTION.RESERVE%type,
                                                           P_GAIN_LOSS_REVERSAL PEND_TRANSACTION.GAIN_LOSS_REVERSAL%type) is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: POST_CHECK_SET_OF_BOOKS
   || Description:
   ||
   || !!!! DO NOT CALL THIS PROCEDURE EXCEPT FROM PEND_BASIS_INSERT_PEND_SOB !!!!!
   || !!!! DO NOT CALL THIS PROCEDURE EXCEPT FROM PEND_BASIS_INSERT_PEND_SOB !!!!!
   || !!!! DO NOT CALL THIS PROCEDURE EXCEPT FROM PEND_BASIS_INSERT_PEND_SOB !!!!!
   || !!!! DO NOT CALL THIS PROCEDURE EXCEPT FROM PEND_BASIS_INSERT_PEND_SOB !!!!!
   ||
   || Please speak with Charlie Shilling if you think an exception should be made.
   ||
   || In order to prevent updates to the reserve, cost_of_removal, salvage_cash,
   ||     salvage_returns, and reserve_credits ("adjusted" columns should be used
   ||   instead), we created a trigger that compares the :new and :old values
   ||   for those columns. However, we need to be able to modify that column
   ||     from the PEND_BASIS_INSERT_PEND_SOB trigger, so I created this procedure,
   ||     which will be called from PEND_BASIS_INSERT_PEND_SOB. Then, the
   ||   PEND_TRANS_CHECK_UPDATE trigger will use the WHO_CALLED_ME function to
   ||   ONLY allow the update if the caller is this procedure.
   ||
   || This needs to be outside of a package since WHO_CALLED_ME only gives the
   ||   package name and not the function within the package.
   ||
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.2.0 09/30/2013 Charlie Shilling init creation
   ||============================================================================
   */
begin
   --call functions to calculate gain_loss_reversal and reserve
   update PEND_TRANSACTION
      set RESERVE = P_RESERVE, GAIN_LOSS_REVERSAL = P_GAIN_LOSS_REVERSAL
    where PEND_TRANS_ID = P_PEND_TRANS_ID
      and LOWER(trim(DECODE(FERC_ACTIVITY_CODE, 2, DESCRIPTION, 'not unretire'))) <> 'unretire';
end P_UPDATE_PEND_TRANS_RES_GL_REV;
 
 
/