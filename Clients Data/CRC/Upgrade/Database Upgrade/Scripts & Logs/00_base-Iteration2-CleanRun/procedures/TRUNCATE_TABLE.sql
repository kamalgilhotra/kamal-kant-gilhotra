
  CREATE OR REPLACE EDITIONABLE PROCEDURE "PWRPLANT"."TRUNCATE_TABLE" (table_name IN VARCHAR2) AS
   /* Vers 1.2 4/6/05 */
   /* Vers 1.3 9/25/05  remove AUTONOMOUS_TRANSACTION*/

   sql_stmt VARCHAR2(4000);
BEGIN
   /* Truncate table */
   sql_stmt := 'truncate table ' || table_name;
   dbms_output.put_line(sql_stmt);
   EXECUTE IMMEDIATE sql_stmt;
   RETURN;
EXCEPTION
   WHEN OTHERS THEN
      raise_application_error(-20000,
                              'Failed Truncate Table ' || table_name ||
                              ' Error: ' || SQLERRM);
      RETURN;
END;
 
 
 
 
 
 
/