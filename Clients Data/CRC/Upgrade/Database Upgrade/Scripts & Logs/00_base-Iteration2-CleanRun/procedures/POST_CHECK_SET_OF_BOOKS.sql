
  CREATE OR REPLACE EDITIONABLE PROCEDURE "PWRPLANT"."POST_CHECK_SET_OF_BOOKS" (A_COMMIT_FLAG         in integer,
                                                    A_COMMIT_SINGLE_TRANS in varchar2) is
   /*
   ||============================================================================
   || Application: PowerPlant
   || Object Name: POST_CHECK_SET_OF_BOOKS
   || Description:
   ||============================================================================
   || Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   ||============================================================================
   || Version  Date       Revised By       Reason for Change
   || -------- ---------- ---------------- --------------------------------------
   || 10.4.0.0 02/05/2013 Charlie Shilling
   || 10.4.0.0 03/25/2013 Charlie Shilling Obey 'POST COMMIT EACH MANUAL TRANSACTION' sys control
   ||============================================================================
   */

   MISSING_PEND_TRANS_IDS PEND_TRANS_IDS_T;
   L_MISSING_COUNT        integer;

   L_UNITIZATION_COUNT integer;

begin
   DBMS_OUTPUT.PUT_LINE('    Begin post_check_set_of_books Oracle procedure');

   select distinct PEND_TRANS_ID bulk collect
     into MISSING_PEND_TRANS_IDS
     from (select A.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
             from PEND_TRANSACTION A, COMPANY_SET_OF_BOOKS B, PEND_BASIS C
            where A.COMPANY_ID = B.COMPANY_ID
              and A.PEND_TRANS_ID = C.PEND_TRANS_ID
              and B.INCLUDE_INDICATOR = 1
              and A.POSTING_STATUS = 2
           minus (select A.PEND_TRANS_ID, 1 SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A
                  where A.POSTING_STATUS = 2
                 union all
                 select B.PEND_TRANS_ID, B.SET_OF_BOOKS_ID
                   from PEND_TRANSACTION A, PEND_TRANSACTION_SET_OF_BOOKS B
                  where A.PEND_TRANS_ID = B.PEND_TRANS_ID
                    and A.POSTING_STATUS = 2))
    order by PEND_TRANS_ID;

   L_MISSING_COUNT := MISSING_PEND_TRANS_IDS.COUNT();

   DBMS_OUTPUT.PUT_LINE('      Post found ' || L_MISSING_COUNT ||
                        ' transactions with at least one missing set_of_books.');

   if L_MISSING_COUNT > 0 then
      begin
         update PEND_TRANSACTION
            set POSTING_STATUS = 3,
                POSTING_ERROR = 'POST 1019: ERROR: This transaction is missing one or more set_of_books from pend_transaction_set_of_books.'
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      exception
         when others then
            update PEND_TRANSACTION
               set POSTING_STATUS = 3
             where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));
      end;

      if A_COMMIT_SINGLE_TRANS = 'YES' then
         for COUNTER in 1 .. L_MISSING_COUNT
         loop
            select count(1)
              into L_UNITIZATION_COUNT
              from PEND_TRANSACTION A, PEND_TRANSACTION B
             where A.COMPANY_ID = B.COMPANY_ID
               and A.GL_JE_CODE = B.GL_JE_CODE
               and A.WORK_ORDER_NUMBER = B.WORK_ORDER_NUMBER
               and A.POSTING_STATUS = 2
               and B.PEND_TRANS_ID = MISSING_PEND_TRANS_IDS(COUNTER)
               and A.LDG_DEPR_GROUP_ID <= -1
               and A.LDG_DEPR_GROUP_ID >= -20; --ldg_depr_group_id between -20 and -1 means the transaction is from unitization

            if L_UNITIZATION_COUNT > 0 then
               --we have unitization charges pending on this work order/company/gl_je_code - error out other transactions as well.
               update PEND_TRANSACTION A
                  set POSTING_STATUS = 3
                where POSTING_STATUS = 2
                  and exists (select 1
                         from PEND_TRANSACTION B
                        where A.WORK_ORDER_NUMBER = B.WORK_ORDER_NUMBER
                          and A.COMPANY_ID = B.COMPANY_ID
                          and A.GL_JE_CODE = B.GL_JE_CODE
                          and B.PEND_TRANS_ID = MISSING_PEND_TRANS_IDS(COUNTER));
            end if;
         end loop;
      else
         update PEND_TRANSACTION A
            set POSTING_STATUS = 3
          where POSTING_STATUS = 2
            and exists
          (select 1
                   from PEND_TRANSACTION B
                  where A.WORK_ORDER_NUMBER = B.WORK_ORDER_NUMBER
                    and A.COMPANY_ID = B.COMPANY_ID
                    and A.GL_JE_CODE = B.GL_JE_CODE
                    and B.PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS)));
      end if;

      if A_COMMIT_FLAG = 1 then
         commit; --commit here to make sure that the error gets saved if the next few statements fail.
      end if;

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_transaction_temp');
      insert into PEND_TRANSACTION_TEMP
         (PEND_TRANS_ID, LDG_ASSET_ID, LDG_ACTIVITY_ID, TIME_STAMP, USER_ID, LDG_DEPR_GROUP_ID,
          BOOKS_SCHEMA_ID, RETIREMENT_UNIT_ID, UTILITY_ACCOUNT_ID, BUS_SEGMENT_ID, FUNC_CLASS_ID,
          SUB_ACCOUNT_ID, ASSET_LOCATION_ID, GL_ACCOUNT_ID, COMPANY_ID, GL_POSTING_MO_YR,
          SUBLEDGER_INDICATOR, ACTIVITY_CODE, GL_JE_CODE, WORK_ORDER_NUMBER, POSTING_QUANTITY,
          USER_ID1, POSTING_AMOUNT, USER_ID2, IN_SERVICE_YEAR, DESCRIPTION, LONG_DESCRIPTION,
          PROPERTY_GROUP_ID, RETIRE_METHOD_ID, POSTING_ERROR, POSTING_STATUS, COST_OF_REMOVAL,
          SALVAGE_CASH, SALVAGE_RETURNS, GAIN_LOSS, RESERVE, MISC_DESCRIPTION, FERC_ACTIVITY_CODE,
          SERIAL_NUMBER, RESERVE_CREDITS, REPLACEMENT_AMOUNT, GAIN_LOSS_REVERSAL, USER_ID3,
          DISPOSITION_CODE, MINOR_PEND_TRAN_ID, WIP_COMP_TRANSACTION, WIP_COMPUTATION_ID,
          TAX_ORIG_MONTH_NUMBER, IMPAIRMENT_EXPENSE_AMOUNT, ADJUSTED_COST_OF_REMOVAL,
          ADJUSTED_SALVAGE_CASH, ADJUSTED_SALVAGE_RETURNS, ADJUSTED_RESERVE_CREDITS,
          ADJUSTED_RESERVE)
         select PEND_TRANS_ID,
                LDG_ASSET_ID,
                LDG_ACTIVITY_ID,
                TIME_STAMP,
                USER_ID,
                LDG_DEPR_GROUP_ID,
                BOOKS_SCHEMA_ID,
                RETIREMENT_UNIT_ID,
                UTILITY_ACCOUNT_ID,
                BUS_SEGMENT_ID,
                FUNC_CLASS_ID,
                SUB_ACCOUNT_ID,
                ASSET_LOCATION_ID,
                GL_ACCOUNT_ID,
                COMPANY_ID,
                GL_POSTING_MO_YR,
                SUBLEDGER_INDICATOR,
                ACTIVITY_CODE,
                GL_JE_CODE,
                WORK_ORDER_NUMBER,
                POSTING_QUANTITY,
                USER_ID1,
                POSTING_AMOUNT,
                USER_ID2,
                IN_SERVICE_YEAR,
                DESCRIPTION,
                LONG_DESCRIPTION,
                PROPERTY_GROUP_ID,
                RETIRE_METHOD_ID,
                POSTING_ERROR,
                POSTING_STATUS,
                COST_OF_REMOVAL,
                SALVAGE_CASH,
                SALVAGE_RETURNS,
                GAIN_LOSS,
                RESERVE,
                MISC_DESCRIPTION,
                FERC_ACTIVITY_CODE,
                SERIAL_NUMBER,
                RESERVE_CREDITS,
                REPLACEMENT_AMOUNT,
                GAIN_LOSS_REVERSAL,
                USER_ID3,
                DISPOSITION_CODE,
                MINOR_PEND_TRAN_ID,
                WIP_COMP_TRANSACTION,
                WIP_COMPUTATION_ID,
                TAX_ORIG_MONTH_NUMBER,
                IMPAIRMENT_EXPENSE_AMOUNT,
                ADJUSTED_COST_OF_REMOVAL,
                ADJUSTED_SALVAGE_CASH,
                ADJUSTED_SALVAGE_RETURNS,
                ADJUSTED_RESERVE_CREDITS,
                ADJUSTED_RESERVE
           from PEND_TRANSACTION
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis_temp');
      insert into PEND_BASIS_TEMP
         (PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, GL_ACCT_1, BASIS_2, GL_ACCT_2, BASIS_3,
          GL_ACCT_3, BASIS_4, GL_ACCT_4, BASIS_5, GL_ACCT_5, BASIS_6, GL_ACCT_6, BASIS_7, GL_ACCT_7,
          BASIS_8, GL_ACCT_8, BASIS_9, GL_ACCT_9, BASIS_10, GL_ACCT_10, BASIS_11, GL_ACCT_11,
          BASIS_12, GL_ACCT_12, BASIS_13, GL_ACCT_13, BASIS_14, GL_ACCT_14, BASIS_15, GL_ACCT_15,
          BASIS_16, GL_ACCT_16, BASIS_17, GL_ACCT_17, BASIS_18, GL_ACCT_18, BASIS_19, GL_ACCT_19,
          BASIS_20, GL_ACCT_20, BASIS_21, GL_ACCT_21, BASIS_22, GL_ACCT_22, BASIS_23, GL_ACCT_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70, GL_ACCT_24, GL_ACCT_25, GL_ACCT_26, GL_ACCT_27, GL_ACCT_28, GL_ACCT_29,
          GL_ACCT_30, GL_ACCT_31, GL_ACCT_32, GL_ACCT_33, GL_ACCT_34, GL_ACCT_35, GL_ACCT_36,
          GL_ACCT_37, GL_ACCT_38, GL_ACCT_39, GL_ACCT_40, GL_ACCT_41, GL_ACCT_42, GL_ACCT_43,
          GL_ACCT_44, GL_ACCT_45, GL_ACCT_46, GL_ACCT_47, GL_ACCT_48, GL_ACCT_49, GL_ACCT_50,
          GL_ACCT_51, GL_ACCT_52, GL_ACCT_53, GL_ACCT_54, GL_ACCT_55, GL_ACCT_56, GL_ACCT_57,
          GL_ACCT_58, GL_ACCT_59, GL_ACCT_60, GL_ACCT_61, GL_ACCT_62, GL_ACCT_63, GL_ACCT_64,
          GL_ACCT_65, GL_ACCT_66, GL_ACCT_67, GL_ACCT_68, GL_ACCT_69, GL_ACCT_70)
         select PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                BASIS_1,
                GL_ACCT_1,
                BASIS_2,
                GL_ACCT_2,
                BASIS_3,
                GL_ACCT_3,
                BASIS_4,
                GL_ACCT_4,
                BASIS_5,
                GL_ACCT_5,
                BASIS_6,
                GL_ACCT_6,
                BASIS_7,
                GL_ACCT_7,
                BASIS_8,
                GL_ACCT_8,
                BASIS_9,
                GL_ACCT_9,
                BASIS_10,
                GL_ACCT_10,
                BASIS_11,
                GL_ACCT_11,
                BASIS_12,
                GL_ACCT_12,
                BASIS_13,
                GL_ACCT_13,
                BASIS_14,
                GL_ACCT_14,
                BASIS_15,
                GL_ACCT_15,
                BASIS_16,
                GL_ACCT_16,
                BASIS_17,
                GL_ACCT_17,
                BASIS_18,
                GL_ACCT_18,
                BASIS_19,
                GL_ACCT_19,
                BASIS_20,
                GL_ACCT_20,
                BASIS_21,
                GL_ACCT_21,
                BASIS_22,
                GL_ACCT_22,
                BASIS_23,
                GL_ACCT_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70,
                GL_ACCT_24,
                GL_ACCT_25,
                GL_ACCT_26,
                GL_ACCT_27,
                GL_ACCT_28,
                GL_ACCT_29,
                GL_ACCT_30,
                GL_ACCT_31,
                GL_ACCT_32,
                GL_ACCT_33,
                GL_ACCT_34,
                GL_ACCT_35,
                GL_ACCT_36,
                GL_ACCT_37,
                GL_ACCT_38,
                GL_ACCT_39,
                GL_ACCT_40,
                GL_ACCT_41,
                GL_ACCT_42,
                GL_ACCT_43,
                GL_ACCT_44,
                GL_ACCT_45,
                GL_ACCT_46,
                GL_ACCT_47,
                GL_ACCT_48,
                GL_ACCT_49,
                GL_ACCT_50,
                GL_ACCT_51,
                GL_ACCT_52,
                GL_ACCT_53,
                GL_ACCT_54,
                GL_ACCT_55,
                GL_ACCT_56,
                GL_ACCT_57,
                GL_ACCT_58,
                GL_ACCT_59,
                GL_ACCT_60,
                GL_ACCT_61,
                GL_ACCT_62,
                GL_ACCT_63,
                GL_ACCT_64,
                GL_ACCT_65,
                GL_ACCT_66,
                GL_ACCT_67,
                GL_ACCT_68,
                GL_ACCT_69,
                GL_ACCT_70
           from PEND_BASIS
          where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Delete from pend_basis');
      delete from PEND_BASIS
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      DBMS_OUTPUT.PUT_LINE('      Insert into pend_basis');
      insert into PEND_BASIS
         (PEND_TRANS_ID, TIME_STAMP, USER_ID, BASIS_1, GL_ACCT_1, BASIS_2, GL_ACCT_2, BASIS_3,
          GL_ACCT_3, BASIS_4, GL_ACCT_4, BASIS_5, GL_ACCT_5, BASIS_6, GL_ACCT_6, BASIS_7, GL_ACCT_7,
          BASIS_8, GL_ACCT_8, BASIS_9, GL_ACCT_9, BASIS_10, GL_ACCT_10, BASIS_11, GL_ACCT_11,
          BASIS_12, GL_ACCT_12, BASIS_13, GL_ACCT_13, BASIS_14, GL_ACCT_14, BASIS_15, GL_ACCT_15,
          BASIS_16, GL_ACCT_16, BASIS_17, GL_ACCT_17, BASIS_18, GL_ACCT_18, BASIS_19, GL_ACCT_19,
          BASIS_20, GL_ACCT_20, BASIS_21, GL_ACCT_21, BASIS_22, GL_ACCT_22, BASIS_23, GL_ACCT_23,
          BASIS_24, BASIS_25, BASIS_26, BASIS_27, BASIS_28, BASIS_29, BASIS_30, BASIS_31, BASIS_32,
          BASIS_33, BASIS_34, BASIS_35, BASIS_36, BASIS_37, BASIS_38, BASIS_39, BASIS_40, BASIS_41,
          BASIS_42, BASIS_43, BASIS_44, BASIS_45, BASIS_46, BASIS_47, BASIS_48, BASIS_49, BASIS_50,
          BASIS_51, BASIS_52, BASIS_53, BASIS_54, BASIS_55, BASIS_56, BASIS_57, BASIS_58, BASIS_59,
          BASIS_60, BASIS_61, BASIS_62, BASIS_63, BASIS_64, BASIS_65, BASIS_66, BASIS_67, BASIS_68,
          BASIS_69, BASIS_70, GL_ACCT_24, GL_ACCT_25, GL_ACCT_26, GL_ACCT_27, GL_ACCT_28, GL_ACCT_29,
          GL_ACCT_30, GL_ACCT_31, GL_ACCT_32, GL_ACCT_33, GL_ACCT_34, GL_ACCT_35, GL_ACCT_36,
          GL_ACCT_37, GL_ACCT_38, GL_ACCT_39, GL_ACCT_40, GL_ACCT_41, GL_ACCT_42, GL_ACCT_43,
          GL_ACCT_44, GL_ACCT_45, GL_ACCT_46, GL_ACCT_47, GL_ACCT_48, GL_ACCT_49, GL_ACCT_50,
          GL_ACCT_51, GL_ACCT_52, GL_ACCT_53, GL_ACCT_54, GL_ACCT_55, GL_ACCT_56, GL_ACCT_57,
          GL_ACCT_58, GL_ACCT_59, GL_ACCT_60, GL_ACCT_61, GL_ACCT_62, GL_ACCT_63, GL_ACCT_64,
          GL_ACCT_65, GL_ACCT_66, GL_ACCT_67, GL_ACCT_68, GL_ACCT_69, GL_ACCT_70)
         select PEND_TRANS_ID,
                TIME_STAMP,
                USER_ID,
                BASIS_1,
                GL_ACCT_1,
                BASIS_2,
                GL_ACCT_2,
                BASIS_3,
                GL_ACCT_3,
                BASIS_4,
                GL_ACCT_4,
                BASIS_5,
                GL_ACCT_5,
                BASIS_6,
                GL_ACCT_6,
                BASIS_7,
                GL_ACCT_7,
                BASIS_8,
                GL_ACCT_8,
                BASIS_9,
                GL_ACCT_9,
                BASIS_10,
                GL_ACCT_10,
                BASIS_11,
                GL_ACCT_11,
                BASIS_12,
                GL_ACCT_12,
                BASIS_13,
                GL_ACCT_13,
                BASIS_14,
                GL_ACCT_14,
                BASIS_15,
                GL_ACCT_15,
                BASIS_16,
                GL_ACCT_16,
                BASIS_17,
                GL_ACCT_17,
                BASIS_18,
                GL_ACCT_18,
                BASIS_19,
                GL_ACCT_19,
                BASIS_20,
                GL_ACCT_20,
                BASIS_21,
                GL_ACCT_21,
                BASIS_22,
                GL_ACCT_22,
                BASIS_23,
                GL_ACCT_23,
                BASIS_24,
                BASIS_25,
                BASIS_26,
                BASIS_27,
                BASIS_28,
                BASIS_29,
                BASIS_30,
                BASIS_31,
                BASIS_32,
                BASIS_33,
                BASIS_34,
                BASIS_35,
                BASIS_36,
                BASIS_37,
                BASIS_38,
                BASIS_39,
                BASIS_40,
                BASIS_41,
                BASIS_42,
                BASIS_43,
                BASIS_44,
                BASIS_45,
                BASIS_46,
                BASIS_47,
                BASIS_48,
                BASIS_49,
                BASIS_50,
                BASIS_51,
                BASIS_52,
                BASIS_53,
                BASIS_54,
                BASIS_55,
                BASIS_56,
                BASIS_57,
                BASIS_58,
                BASIS_59,
                BASIS_60,
                BASIS_61,
                BASIS_62,
                BASIS_63,
                BASIS_64,
                BASIS_65,
                BASIS_66,
                BASIS_67,
                BASIS_68,
                BASIS_69,
                BASIS_70,
                GL_ACCT_24,
                GL_ACCT_25,
                GL_ACCT_26,
                GL_ACCT_27,
                GL_ACCT_28,
                GL_ACCT_29,
                GL_ACCT_30,
                GL_ACCT_31,
                GL_ACCT_32,
                GL_ACCT_33,
                GL_ACCT_34,
                GL_ACCT_35,
                GL_ACCT_36,
                GL_ACCT_37,
                GL_ACCT_38,
                GL_ACCT_39,
                GL_ACCT_40,
                GL_ACCT_41,
                GL_ACCT_42,
                GL_ACCT_43,
                GL_ACCT_44,
                GL_ACCT_45,
                GL_ACCT_46,
                GL_ACCT_47,
                GL_ACCT_48,
                GL_ACCT_49,
                GL_ACCT_50,
                GL_ACCT_51,
                GL_ACCT_52,
                GL_ACCT_53,
                GL_ACCT_54,
                GL_ACCT_55,
                GL_ACCT_56,
                GL_ACCT_57,
                GL_ACCT_58,
                GL_ACCT_59,
                GL_ACCT_60,
                GL_ACCT_61,
                GL_ACCT_62,
                GL_ACCT_63,
                GL_ACCT_64,
                GL_ACCT_65,
                GL_ACCT_66,
                GL_ACCT_67,
                GL_ACCT_68,
                GL_ACCT_69,
                GL_ACCT_70
           from PEND_BASIS_TEMP;

      DBMS_OUTPUT.PUT_LINE('      Update adjusted columns on pend_transaction with saved values.');
      update PEND_TRANSACTION A
         set (ADJUSTED_COST_OF_REMOVAL,
               ADJUSTED_SALVAGE_CASH,
               ADJUSTED_SALVAGE_RETURNS,
               ADJUSTED_RESERVE_CREDITS,
               ADJUSTED_RESERVE) =
              (select ADJUSTED_COST_OF_REMOVAL,
                      ADJUSTED_SALVAGE_CASH,
                      ADJUSTED_SALVAGE_RETURNS,
                      ADJUSTED_RESERVE_CREDITS,
                      ADJUSTED_RESERVE
                 from PEND_TRANSACTION_TEMP B
                where A.PEND_TRANS_ID = B.PEND_TRANS_ID)
       where exists (select 1 from PEND_TRANSACTION_TEMP B where A.PEND_TRANS_ID = B.PEND_TRANS_ID);

      --dont' need to worry about other transactions on work order because that was already taken care of above (after the 1019 error)
      update PEND_TRANSACTION
         set POSTING_ERROR = 'POST 1020: ERROR: Post created new set_of_books data for this transaction. Please review and re-approve to Post.'
       where PEND_TRANS_ID in (select COLUMN_VALUE from table(MISSING_PEND_TRANS_IDS));

      if A_COMMIT_FLAG = 1 then
         commit;
      end if;
   end if;
end POST_CHECK_SET_OF_BOOKS;
 
 
/