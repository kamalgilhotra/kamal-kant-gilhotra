
  CREATE OR REPLACE EDITIONABLE PROCEDURE "PWRPLANT"."P_RESET_SEQUENCE" 
------------------------------------------------------------------------------
--Current Version: 1.0
--Created by Eric M. Berger on 06.09.16
--
--This procedure takes in a table name and a backup table name, and will reset
--the a table to it's original state.
--
--Sample call of the procedure:
--
--EXEC p_reset_sequence('sequence_name');
--
------------------------------------------------------------------------------
--
-----------------------------Version control----------------------------------
--  1.0 Eric Berger: Created the procedure.                           08.01.16
------------------------------------------------------------------------------
(
    a_sequence_name IN VARCHAR2
) IS
    v_sequence_name NUMBER(22,0);
    v_sequence_exists NUMBER(22,0);
BEGIN
    BEGIN
        SELECT To_Char(Count(*)) INTO v_sequence_name
        FROM all_sequences WHERE sequence_name = Upper(a_sequence_name);
    EXCEPTION WHEN NO_DATA_FOUND THEN
        Dbms_Output.put_line('Invalid Sequence Name');
    END;
END
;
 
/