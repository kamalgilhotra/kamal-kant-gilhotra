
  CREATE OR REPLACE EDITIONABLE PROCEDURE "PWRPLANT"."PA_REFRESH_SEQ" (SEQUENCE_NAME IN VARCHAR2)AS
    l_val NUMBER;
BEGIN
  EXECUTE IMMEDIATE 'select ' || SEQUENCE_NAME || '.nextval from dual' INTO l_val;
  EXECUTE IMMEDIATE 'alter sequence ' || SEQUENCE_NAME || ' increment by -' || l_val || ' minvalue 0';
  EXECUTE IMMEDIATE 'select ' || SEQUENCE_NAME || '.nextval from dual' INTO l_val;
  EXECUTE IMMEDIATE 'alter sequence ' || SEQUENCE_NAME || ' increment by 1 minvalue 0';

END; --PROCEDURE RESET_SEQ
 
 
/