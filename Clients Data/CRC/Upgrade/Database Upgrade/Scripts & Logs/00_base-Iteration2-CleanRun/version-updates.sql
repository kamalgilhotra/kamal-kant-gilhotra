/*
||============================================================================
|| Application: PowerPlant
|| File Name:   version-updates.sql
||============================================================================
|| Copyright (C) 2012 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version    Date       Revised By     Reason for Change
|| ---------- ---------- -------------- --------------------------------------
|| 10.3.3.0   11/30/2011 AYP            Point Release
||                                      Provision Version
|| 10.3.3.1   01/18/2012 Lee Quinn      Removed Provision and updated version
|| 10.3.3.2   02/10/2012 AYP            10.3.3.2 patch info
||============================================================================
*/

update PP_VERSION
   set PP_VERSION = 'Version 10.3.3.0',
       PP_PATCH   = 'Patch: 10.3.3.2',
       POWERTAX_VERSION = '10.3.3.0',
       DATE_INSTALLED = sysdate,
       POST_VERSION = 'Version 10.3.3.1, Dec 20, 2011'
where PP_VERSION_ID = 1;

update PP_PROCESSES
   set VERSION = '10.3.3.0'
 where trim(LOWER(EXECUTABLE_FILE)) in ('automatedreview.exe',
                                        'batreporter.exe',
                                        'budget_allocations.exe',
                                        'cr_allocations.exe',
                                        'cr_balances.exe',
                                        'cr_batch_derivation.exe',
                                        'cr_batch_derivation_bdg.exe',
                                        'cr_budget_entry_calc_all.exe',
                                        'cr_build_combos.exe',
                                        'cr_build_deriver.exe',
                                        'cr_delete_budget_allocations.exe',
                                        'cr_financial_reports_build.exe',
                                        'cr_flatten_structures.exe',
                                        'cr_man_jrnl_reversals.exe',
                                        'cr_posting.exe',
                                        'cr_posting_bdg.exe',
                                        'cr_sap_trueup.exe',
                                        'cr_sum.exe',
                                        'cr_sum_ab.exe',
                                        'cr_to_commitments.exe',
                                        'cwip_charge.exe',
                                        'populate_matlrec.exe',
                                        'ppmetricbatch.exe',
                                        'pptocr.exe',
                                        'ppverify.exe',
                                        'pp_archive.exe',
                                        'cr_derivation_trueup.exe');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'automatedreview.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'batreporter.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'budget_allocations.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_allocations.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_balances.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_batch_derivation.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_batch_derivation_bdg.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_budget_entry_calc_all.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_build_combos.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_build_deriver.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_delete_budget_allocations.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_financial_reports_build.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_flatten_structures.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_man_jrnl_reversals.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_posting.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_posting_bdg.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_sap_trueup.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_sum.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_sum_ab.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_to_commitments.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cwip_charge.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'populate_matlrec.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'ppmetricbatch.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'pptocr.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'ppverify.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'pp_archive.exe', '10.3.3.0');

insert into PP_VERSION_EXES
   (PP_VERSION, EXECUTABLE_FILE, EXECUTABLE_VERSION)
values
   ('10.3.3.2', 'cr_derivation_trueup.exe', '10.3.3.0');

--**************************
-- Log the run of the script
--**************************

insert into PP_SCHEMA_CHANGE_LOG
   (ID, SKIPPED, MAJOR_VERSION, MINOR_VERSION, POINT_VERSION, PATCH_VERSION, MAINT_NUM, SCRIPT_PATH,
    SCRIPT_NAME, SCRIPT_REVISION, DATE_APPLIED, OS_USER, TERMINAL, SERVICE_NAME)
values
   (89, 0, 10, 3, 3, 2, 0, 'C:\PlasticWks\powerplant\sql', 'version-updates.sql', 1,
    SYSTIMESTAMP, SYS_CONTEXT('USERENV', 'OS_USER'), SYS_CONTEXT('USERENV', 'TERMINAL'),
    SYS_CONTEXT('USERENV', 'SERVICE_NAME'));
commit;
