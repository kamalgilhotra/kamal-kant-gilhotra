CREATE OR REPLACE TYPE t_lsr_rates_implicit_in_lease AUTHID current_user AS OBJECT (rate_implicit FLOAT,
                                                                rate_implicit_ni float);