CREATE OR REPLACE TYPE LSR_NPV_VALUES AUTHID CURRENT_USER AS OBJECT
(
	payments NUMBER,
	guaranteed_residual NUMBER,
	unguaranteed_residual NUMBER
);
/