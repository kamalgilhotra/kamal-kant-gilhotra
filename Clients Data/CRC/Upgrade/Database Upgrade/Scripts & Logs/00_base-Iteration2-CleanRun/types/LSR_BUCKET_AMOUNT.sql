CREATE OR REPLACE  TYPE LSR_BUCKET_AMOUNT AUTHID current_user AS OBJECT (bucket_name VARCHAR2(254),
       	  	                                                         amount NUMBER);
/
