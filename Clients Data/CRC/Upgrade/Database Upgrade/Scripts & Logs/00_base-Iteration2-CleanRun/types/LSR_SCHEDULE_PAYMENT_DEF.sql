CREATE OR REPLACE TYPE LSR_SCHEDULE_PAYMENT_DEF AUTHID current_user AS OBJECT (payment_month_frequency NUMBER,
                                                            is_prepay number(1,0),
                                                            payment_group NUMBER, --Group months based on date payment takes place
                                                            MONTH DATE,
                                                            number_of_terms NUMBER,
                                                            iter NUMBER,
                                                            payment_amount NUMBER,
                                                            partial_month_percent NUMBER(22,8));
/