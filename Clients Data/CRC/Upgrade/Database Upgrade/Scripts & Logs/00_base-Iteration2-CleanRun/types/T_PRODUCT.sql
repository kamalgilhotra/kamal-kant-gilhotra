CREATE OR REPLACE TYPE "PWRPLANT"."T_PRODUCT" AUTHID current_user as object
/*
||============================================================================
|| Application: PowerPlant
|| Object Name: T_PRODUCT
|| Description:
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------  ---------- -------------- ----------------------------------------
|| V10.2.0  09/25/2008 Lee Quinn      Point Release
||============================================================================
*/
(
   PRODUCT number,
   static function ODCIAGGREGATEINITIALIZE(L_PRODUCT in out T_PRODUCT) return number,
   member function ODCIAGGREGATEITERATE(self in out T_PRODUCT, value in number) return number,
   member function ODCIAGGREGATETERMINATE(self        in T_PRODUCT,
                                          RETURNVALUE out number,
                                          FLAGS       in number) return number,
   member function ODCIAGGREGATEMERGE(self in out T_PRODUCT, value in T_PRODUCT) return number
);
/
CREATE OR REPLACE TYPE BODY "PWRPLANT"."T_PRODUCT" is
/*
||============================================================================
|| Application: PowerPlant
|| Type Body: T_PRODUCT
|| Description:
||============================================================================
|| Copyright (C) 2009 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------  ---------- -------------- ----------------------------------------
|| V10.2.0  09/25/2008 Lee Quinn      Point Release
||============================================================================
*/

   static function ODCIAGGREGATEINITIALIZE(L_PRODUCT in out T_PRODUCT) return number is
   begin
      L_PRODUCT := T_PRODUCT(1);
      return ODCICONST.SUCCESS;
   end;

   member function ODCIAGGREGATEITERATE(self in out T_PRODUCT, value in number) return number is
   begin
      self.PRODUCT := self.PRODUCT * value;
      return ODCICONST.SUCCESS;
   end;

   member function ODCIAGGREGATETERMINATE(self        in T_PRODUCT,
                                          RETURNVALUE out number,
                                          FLAGS       in number) return number is
   begin
      RETURNVALUE := self.PRODUCT;
      return ODCICONST.SUCCESS;
   end;

   member function ODCIAGGREGATEMERGE(self in out T_PRODUCT, value in T_PRODUCT) return number is
   begin
      self.PRODUCT := self.PRODUCT * value.PRODUCT;
      return ODCICONST.SUCCESS;
   end;
end;
/
