CREATE OR REPLACE TYPE t_lsr_ilr_id_revision AUTHID current_user AS OBJECT(ilr_id NUMBER(22,2), revision NUMBER(22,2));
