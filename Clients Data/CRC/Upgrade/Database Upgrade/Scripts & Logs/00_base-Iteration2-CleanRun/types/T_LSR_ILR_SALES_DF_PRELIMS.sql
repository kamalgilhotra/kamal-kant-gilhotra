create or replace TYPE t_lsr_ilr_sales_df_prelims AUTHID current_user AS OBJECT ( payment_info lsr_ilr_op_sch_pay_info_tab,
                                                              rates t_lsr_ilr_schedule_all_rates,
                                                              initial_direct_costs lsr_init_direct_cost_info_tab,
                                                              sales_type_info lsr_ilr_sales_sch_info,
															  set_of_books_id NUMBER);