create or replace TYPE t_lsr_ilr_schedule_all_rates AUTHID current_user AS OBJECT ( calculated_rates t_lsr_rates_implicit_in_lease,
                                                                override_rates t_lsr_rates_implicit_in_lease,
                                                                rates_used t_lsr_rates_implicit_in_lease);
