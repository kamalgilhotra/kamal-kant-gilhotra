CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_PAY_TERM AUTHID current_user AS OBJECT( payment_month_frequency NUMBER,
                                                                              payment_term_start_date date,
                                                                              number_of_terms NUMBER,
                                                                              payment_amount NUMBER,
                                                                              executory_buckets lsr_bucket_amount_tab,
                                                                              contingent_buckets lsr_bucket_amount_tab,
                                                                              is_prepay number(1,0),
                                                                              remeasurement_date date,
                                                                              payment_term_id NUMBER(22,0),
                                                                              payment_term_type_id NUMBER(22,0),
                                                                              first_partial_month_percent NUMBER(22,8),
                                                                              last_partial_month_percent NUMBER(22,8))
/