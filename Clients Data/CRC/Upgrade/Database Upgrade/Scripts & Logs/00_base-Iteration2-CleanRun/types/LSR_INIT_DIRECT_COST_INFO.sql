CREATE OR REPLACE  TYPE LSR_INIT_DIRECT_COST_INFO AUTHID current_user AS OBJECT (idc_group_id NUMBER,
                                                            date_incurred DATE,
                                                            amount NUMBER,
                                                            description varchar2(254),
															set_of_books_id NUMBER);
