CREATE OR REPLACE TYPE LSR_ILR_SALES_SCH_INFO AUTHID CURRENT_USER AS OBJECT
(
	carrying_cost NUMBER,
	carrying_cost_company_curr NUMBER,
	fair_market_value NUMBER,
	fair_market_value_company_curr NUMBER,
	guaranteed_residual NUMBER,
	estimated_residual NUMBER,
	days_in_year NUMBER,
	purchase_option_amount NUMBER,
	termination_amount NUMBER,
	remeasurement_date DATE,
	investment_amount NUMBER,
	original_profit_loss NUMBER,
	new_beg_receivable NUMBER,
	prior_end_unguaran_residual NUMBER,
	remeasure_month_fixed_payment NUMBER,
	include_idc_sw NUMBER,
	use_orig_rate NUMBER,
	accrued_deferred_rent NUMBER,
	unamortized_idc NUMBER,
	npv LSR_NPV_VALUES,
	selling_profit_loss NUMBER,
	set_of_books_id NUMBER
);
/