CREATE OR REPLACE TYPE LSR_ILR_OP_SCH_PAY_INFO AUTHID current_user AS OBJECT  (payment_month_frequency NUMBER,
                                                                                payment_term_start_date date,
                                                                                MONTH DATE,
                                                                                number_of_terms NUMBER,
                                                                                payment_amount NUMBER,
                                                                                contingent_buckets lsr_bucket_amount_tab,
                                                                                executory_buckets lsr_bucket_amount_tab,
                                                                                is_prepay NUMBER(1,0),
                                                                                partial_month_percent NUMBER(22,8),
                                                                                iter NUMBER)
/
