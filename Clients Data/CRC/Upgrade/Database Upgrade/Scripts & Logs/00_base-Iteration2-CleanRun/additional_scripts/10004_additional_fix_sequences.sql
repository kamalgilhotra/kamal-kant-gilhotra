alter sequence PT_PROCESS_OCCURRENCE_SEQ increment by 12405;
select PT_PROCESS_OCCURRENCE_SEQ.nextval from dual;
alter sequence PT_PROCESS_OCCURRENCE_SEQ increment by 1;
select PT_PROCESS_OCCURRENCE_SEQ.nextval from dual;
alter sequence TAX_JOB_PARAMS_SEQ increment by 4198;
select TAX_JOB_PARAMS_SEQ.nextval from dual;
alter sequence TAX_JOB_PARAMS_SEQ increment by 1;
select TAX_JOB_PARAMS_SEQ.nextval from dual;


--**************************
-- Log the run of the script
--**************************

insert into PP_UPDATE_FLEX
(COL_ID,
TYPE_NAME,
FLEXVCHAR1)
select NVL(MAX(COL_ID), 0) + 1,
'UPDATE_SCRIPTS',
'10004_additional_fix_sequences.sql'
from PP_UPDATE_FLEX;
commit;

