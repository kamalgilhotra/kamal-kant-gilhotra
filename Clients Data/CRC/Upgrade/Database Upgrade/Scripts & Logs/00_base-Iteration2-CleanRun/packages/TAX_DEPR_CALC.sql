create or replace package TAX_DEPR_CALC as
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: TAX_DEPR_CALC
   --|| Description: tax depreciation calculation
   --||============================================================================
   --|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 5.0      02/13/2013 Roger Roach    Original Version
   --|| 5.1      03/12/2013 Roger Roach    change the organization of the collection
   --|| 5.2      03/27/2013 Roger Roach    added columns compare_rate and tax_activity_type_id
   --|| 5.21     04/01/2013 Roger Roach    changed round with trunc
   --|| 5.22     04/05/2013 Roger Roach    added session parameter option
   --|| 5.23     09/05/2013 Roger Roach    reorganize of the collection tax_calc_adj was wrong
   --|| 5.24     09/05/2013 Roger Roach    Fixed problem with updating tax_reconcile_items
   --|| 5.25     09/17/2013 Roger Roach    The tax_recncile balance not rolling foward
   --|| 5.26     09/17/2013 Roger Roach    Same problem as above
   --|| 5.27     09/19/2013 Roger Roach    set the column input_retire_ind to zeor on null
   --|| 5.28     10/11/2013 Roger Roach    the bonus was not added to tax_reconcile
   --||                                    the next year was not correct so the next year balance was wrong.
   --|| 5.29     12/05/2013 Roger Roach    Maint-34525 fixed the wrong short year rate used for Maint-29675
   --|| 5.30     12/13/2013 Roger Roach    Changed the year from pls_integer to number
   --|| 5.31     12/13/2013 Roger Roach    Changed the select statements to default to zero when the column value is null
   --|| 5.32     01/22/2014 Andrew Scott   maint-35392
   --||                                    1. MLP changes merging tax_control,
   --||                                    tax_depreciation, tax_depr_adjust all into
   --||                                    tax_depreciation.
   --||                                    2. TWC hit a memory/volume error that was
   --||                                    corrected by Roger.  Altered to only pull
   --||                                    the needed tax_reconcile records.added
   --||                                    3. added "or l_current_record_id = 0 then " to GET_TAX_DEPR index creation."
   --||                                    4. forced break in GET_TAX_DEPR to be by tax_record_id
   --||                                    5. added some debug logging to help identify future issues by tax record
   --|| 5.33     02/17/2014 Andrew Scott   1. maint-29975.  Quantity is not rolling forward and Quantity End is
   --||                                    not being updated.
   --||                                    2. maint-10475, pt2.  l_capital_gain_loss needed to be changed to
   --||                                    l_ex_capital_gain_loss in last if statement.
   --||                                    3. maint-36076, pt1.  volume issue: increased limits of
   --||                                    type_tax_depr_save_rec from 40 to 1000
   --||                                    type_tax_depr_rec from 10040 to 11000
   --||                                    4. maint-36076, pt2.  every tax book should have at least one row in
   --||                                    tax_include_activity (for tax_include_id = 1).  Encountering this
   --||                                    previously caused a confusing ORA array error.  Check for
   --||                                    this condition and provide a more user friendly error that explains
   --||                                    what is wrong.
   --|| 5.34     03/13/2014 Andrew Scott   maint-37060. Net gross problem
   --||                                    may double up the reserve at switch, causing
   --||                                    the depr base to be way off.
   --|| 5.35     04/02/2014 Andrew Scott   maint-37521. The whole life component of the
   --||                                    remaining life plan for the SYD calc was missing
   --||                                    its "trunc".
   --||
   --|| PACKAGE RELEASED FOR 10.4.2.0
   --||
   --|| PP Version  Version    Date           Revised By     Reason for Change
   --|| ----------  ---------  -------------  -------------  -------------------------
   --|| 10.4.3.0    5.36       04/28/2014     Andrew Scott   QA discovered a minor logging bug during
   --||                                                      maint-37527 testing.  total row count at
   --||                                                      end does not match the actual number rows
   --||                                                      processed.
   --|| 10.4.2.1    5.37       05/16/2014     Andrew Scott   maint-38243.  calc logic breaks on vintage
   --||                                                      rate.  for loop's 'i' referenced after the
   --||                                                      loop, which cannot happen.
   --|| 10.4.2.2    5.38       07/24/2014     Andrew Scott   maint-39208.  for tax records using more than one
   --||                                                      calc reconcile items, only the 1st calc occurs, not
   --||                                                      the other calcs.  e.g. 12% Tax Repairs, then 50% Bonus
   --|| 10.4.3.0    5.39       08/13/2014     Andrew Scott   maint-38887.  turn off auditing.
   --|| 10.4.3.1    5.40       10/28/2014     M Bradley      maint-40988.  when moving the trailing tax records in the
   --||                                                      batch of 10,000, the sort is affected for that tax record
   --||                                                      on the break.
   --|| 10.4.3.1    5.41       11/03/2014     M Bradley      maint-40988 (pt2). The run fast process is not considering
   --||                                                      basis differences in its tax balance and depreciable basis
   --||                                                      calculation if the basis differenced required an insert
   --||                                                      into tax book reconcile.  This ultimately causes a bad
   --||                                                      depreciation calculation and an end of year out of balance
   --||                                                      scenario.  M also discovered tax_book_reconcile-
   --||                                                      basis_amount_transfer was null and had to be defaulted to 0.
   --|| 10.4.3.2    5.42       01/14/2015     Andrew Scott   maint-42180.  calc should not be updating accum_salvage.
   --||                                                      2. tax depr cursor was using cor_expense for accum_ordin_retires_end
   --|| 2015.1      5.43       02/10/2015     Andrew Scott   maint-42696.  1.  changed to allow tax rate id = 0.  2. better
   --||                                                      error handling of rows in tax_rate_control without rows in
   --||                                                      tax_rate.
   --|| 10.4.3.4    5.44       04/15/2015     M Bradley      maint-43643 The ending balances for basis differences newly inserted
   --||                                                      were not being populated.
   --|| 2015.2      5.45       09/11/2015     Andrew Scott   maint-44718 Prevent zeroing out of bonus beg balance in processing
   --|| 2015.2.0.0  5.46       09/17/2015     Sarah Byers    PP-44918: Added TRANSFER_DEPR to TAX_DEPRECIATION
   --||
   --|| Version     Date       Revised By     Reason for Change
   --|| --------    ---------- -------------- ----------------------------------------
   --|| 2015.2.0.0  11/30/2015 Andrew Scott   maint-45240.  Update version to be 2015.2.0.0.
   --||============================================================================

   -- ============================== Tax Program Structure ========================
   --
   --                            calc_depr  -->  read tax_job_params
   --                                |
   --                                |
   --                                |
   --                                |--------------> Get Records
   --                                |
   --                  Loop  ------->|
   --                        ^       |--------------> Filter Records
   --                        |       |
   --                        |       |
   --                        |       |--------------> Calc Records
   --                        |
   --                        |       | <- Update Depreciation Records
   --                        <-------|
   --                                | <- Update Reconcile Records
   --                                |
   --                                | <- Commit
   --                               END
   --
   -- =============================================================================
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   g_limit constant pls_integer := 10000;
   subtype hash_t is varchar2(100);
   g_sep constant varchar2(1) := '-';
   g_start_time timestamp;

   type tax_bal_rec_rec is record(
      id             number(22, 0),
      reconcile_item number(22, 0),
      tax_year       number(22, 2));

   type tax_rec_bal_type is table of tax_bal_rec_rec index by pls_integer;
   type tax_rec_bal_table_type is table of tax_rec_bal_type index by hash_t;

   g_tax_reconcile_bal_hash tax_rec_bal_table_type;

   type tax_depr_bals_rec is record(
      id       number(22, 0),
      tax_year number(22, 2));

   type tax_depr_bals_rec_type is table of tax_depr_bals_rec index by pls_integer;
   type tax_depr_bals_table_type is table of tax_depr_bals_rec_type index by hash_t;

   g_tax_depr_bals_hash tax_depr_bals_table_type;

   type tax_rec_rec is record(
      id             number(22, 0),
      reconcile_item number(22, 0));

   type tax_rec_type is table of tax_rec_rec index by pls_integer;
   type tax_rec_table_type is table of tax_rec_type index by hash_t;

   g_tax_reconcile_hash tax_rec_table_type;

   type tax_book_activity_type is table of pls_integer index by pls_integer;
   type tax_book_activity_table_type is table of tax_book_activity_type index by hash_t;

   g_tax_book_activity_hash tax_book_activity_table_type;

   type tax_depr_hash_type is table of number(22, 0) index by hash_t;

   g_tax_depr_hash     tax_depr_hash_type;
   g_tax_reconcile_index_hash tax_depr_hash_type;

   type tax_convention_table_type is table of pls_integer index by binary_integer;

   g_tax_convention_hash tax_convention_table_type;

   type tax_rates_rec is record(
      id      number(22, 0),
      rate_id number(22, 0));

   type tax_rates_rec_type is table of tax_rates_rec index by pls_integer;

   type tax_rates_table_type is table of tax_rates_rec_type index by hash_t;

   g_tax_rates_hash  tax_rates_table_type;
   g_tax_limit2_hash tax_rates_table_type;

   g_table_book_ids table_list_id_type;

   g_version    number(22, 0);
   g_start_year number(22, 2);
   g_end_year   number(22, 2);

   g_table_vintage_ids table_list_id_type;
   g_table_class_ids   table_list_id_type;
   g_table_company_ids table_list_id_type;
   g_table_record_ids table_list_id_type;

   g_table_book_count    number(22, 0);
   g_table_vintage_count number(22, 0);
   g_table_class_count   number(22, 0);
   g_table_company_count number(22, 0);

   cursor tax_depr_cur is
      select distinct td.tax_record_id,
                      td.tax_book_id,
                      td.tax_year,
                      nvl(td.book_balance, 0)  book_balance,
                      nvl(td.tax_balance, 0) tax_balance,
                      nvl(remaining_life, 0) remaining_life,
                      nvl(td.accum_reserve, 0) accum_reserve,
                      nvl(td.sl_reserve, 0) sl_reserve,
                      nvl(depreciable_base, 0) depreciable_base,
                      nvl(td.fixed_depreciable_base, 0) fixed_depreciable_base,
                      nvl(actual_salvage, 0)  actual_salvage,
                      nvl(td.estimated_salvage, 0) estimated_salvage,
                      nvl(td.accum_salvage, 0) accum_salvage,
                      nvl(additions, 0)  additions,
                      nvl(transfers, 0) transfers,
                      nvl(adjustments, 0) adjustments,
                      nvl(retirements, 0) retirements,
                      nvl(extraordinary_retires, 0) extraordinary_retires,
                      nvl(td.accum_ordinary_retires, 0) accum_ordinary_retires,
                      nvl(cost_of_removal, 0) cost_of_removal,
                      nvl(est_salvage_pct, 0) est_salvage_pct,
                      nvl(retire_invol_conv, 0) retire_invol_conv,
                      nvl(salvage_invol_conv, 0) salvage_invol_conv,
                      nvl(salvage_extraord, 0) salvage_extraord,
                      nvl(td.reserve_at_switch, 0) reserve_at_switch,
                      nvl(td.quantity, 0) quantity,
                      nvl(td.quantity_end, 0) quantity_end,
                      nvl(convention_id, 0) convention_id,
                      nvl(extraordinary_convention, 0) extraordinary_convention,
                      nvl(tax_limit_id, -1) tax_limit_id,
                      nvl(tax_rate_id, 0) tax_rate_id,
                      nvl(td.number_months_beg, 0) number_months_beg,
                      nvl(td.number_months_end, 0) number_months_end,
                      nvl(tr.book_balance, 0) book_balance_xfer,
                      nvl(tr.tax_balance, 0) tax_balance_xfer,
                      nvl(tr.accum_reserve, 0) accum_reserve_xfer,
                      nvl(tr.sl_reserve, 0) sl_reserve_xfer,
                      nvl(tr.fixed_depreciable_base, 0) fixed_depreciable_base_xfer,
                      nvl(tr.estimated_salvage, 0) estimated_salvage_xfer,
                      nvl(tr.accum_salvage, 0) accum_salvage_xfer,
                      nvl(tr.accum_ordinary_retires, 0) accum_ordinary_retires_xfer,
                      nvl(tr.reserve_at_switch, 0) reserve_at_switch_xfer,
                      nvl(tr.quantity, 0) quantity_xfer,
                      nvl(td.transfer_depr, 0) transfer_depr,
                      nvl(td.tax_credit_id, 0) tax_credit_id,
                      nvl(v.year , 0) vintage_year,
                      nvl(td.job_creation_amount, 0) job_creation_amount,
                      nvl(td.cor_res_impact, 0) cor_res_impact,
                      nvl(td.cor_expense, 0) cor_expense,
                      nvl(td.salvage_res_impact, 0) salvage_res_impact,
                      nvl(td.accum_ordin_retires_end, 0) accum_ordin_retires_end,
                      nvl(td.accum_salvage_end, 0) accum_salvage_end,
                      nvl(td.sl_reserve_end, 0) sl_reserve_end,
                      nvl(td.accum_reserve_end, 0) accum_reserve_end,
                      nvl(td.tax_balance_end, 0) tax_balance_end,
                      nvl(td.book_balance_end, 0) book_balance_end,
                      nvl(td.adjusted_retire_basis, 0) adjusted_retire_basis,
                      nvl(td.transfer_res_impact, 0) transfer_res_impact,
                      nvl(td.ex_retire_res_impact, 0) ex_retire_res_impact,
                      nvl(td.retire_res_impact, 0) retire_res_impact,
                      nvl(td.over_adj_depreciation, 0) over_adj_depreciation,
                      nvl(td.calc_depreciation, 0) calc_depreciation,
                      nvl(td.capital_gain_loss, 0) capital_gain_loss,
                      nvl(td.ex_gain_loss, 0) ex_gain_loss,
                      nvl(td.gain_loss, 0) gain_loss,
                      nvl(td.depreciation, 0) depreciation,
                      nvl(td.reserve_at_switch_end, 0) reserve_at_switch_end,
                      nvl(td.estimated_salvage_end, 0) estimated_salvage_end,
                      nvl(td.depreciation_adjust,0) depreciation_adjust,
                      nvl(td.book_balance_adjust,0) book_balance_adjust,
                      nvl(td.accum_reserve_adjust,0) accum_reserve_adjust,
                      nvl(td.depreciable_base_adjust,0) depreciable_base_adjust,
                      nvl(td.gain_loss_adjust,0) gain_loss_adjust,
                      nvl(td.cap_gain_loss_adjust,0) cap_gain_loss_adjust,
                      nvl(td.book_balance_adjust_method,1) book_balance_adjust_method,
                      nvl(td.accum_reserve_adjust_method,1)  accum_reserve_adjust_method,
                      nvl(td.depreciable_base_adjust_method,1) depreciable_base_adjust_method,
                      nvl(td.depreciation_adjust_method,1) depreciation_adjust_method
        from vintage v,
             tax_record_control trc,
             tax_depreciation td,
             (select tax_record_id,
                     tax_book_id,
                     tax_year,
                     sum(book_balance) book_balance,
                     sum(tax_balance) tax_balance,
                     sum(accum_reserve) accum_reserve,
                     sum(sl_reserve) sl_reserve,
                     sum(fixed_depreciable_base) fixed_depreciable_base,
                     sum(estimated_salvage) estimated_salvage,
                     sum(accum_salvage) accum_salvage,
                     sum(accum_ordinary_retires) accum_ordinary_retires,
                     sum(reserve_at_switch) reserve_at_switch,
                     sum(quantity) quantity
                from tax_depreciation_transfer
               group by tax_record_id, tax_book_id, tax_year) tr
       where (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (td.tax_book_id in (select * from table(g_table_book_ids)) or g_table_book_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and td.tax_year between g_start_year and g_end_year
         and trc.tax_record_id = td.tax_record_id
         and td.tax_book_id = tr.tax_book_id(+)
         and td.tax_record_id = tr.tax_record_id(+)
         and td.tax_year = tr.tax_year(+)
         and v.vintage_id = trc.vintage_id
         and trc.version_id = g_version
       order by td.tax_record_id, td.tax_book_id, td.tax_year;

   type type_tax_depr_rec is varray(11000) of tax_depr_cur%rowtype;

   type type_tax_depr_save_rec is varray(1000) of tax_depr_cur%rowtype;

   g_tax_depr_rec       type_tax_depr_rec;
   g_tax_depr_save1_rec type_tax_depr_save_rec;
   g_tax_depr_save2_rec type_tax_depr_save_rec;

   g_tax_depr_last_record_id number(22, 0);
   g_tax_depr_last_book_id   number(22, 0);

   cursor tax_convention_cur is
      select convention_id,
             retire_depr_id,
             retire_bal_id,
             retire_reserve_id,
             gain_loss_id,
             salvage_id,
             est_salvage_id,
             cost_of_removal_id,
             cap_gain_id
        from tax_convention;

   type type_tax_convention_rec is table of tax_convention_cur%rowtype;

   g_tax_convention_rec type_tax_convention_rec;

   cursor tax_rates_cur is
      select nvl(tr.tax_rate_id,0) tax_rate_id,
             nvl(rate,0) rate,
             nvl(rate1,0) rate1,
             nvl(tr.year,0) year,
             net_gross net_gross,
             nvl(life, 0 ) life,
             nvl(remaining_life_plan,0) remaining_life_plan,
             nvl(start_method,0) start_method,
             nvl(rounding_convention,0) rounding_convention,
             nvl(half_year_convention,0) half_year_convention,
             nvl(switched_year,0) switched_year
        from tax_rates tr, tax_rate_control trc
       where tr.tax_rate_id = trc.tax_rate_id
       order by tr.tax_rate_id, tr.year;

   type type_tax_rates_rec is table of tax_rates_cur%rowtype;

   g_tax_rates_rec type_tax_rates_rec;

   cursor tax_book_activity_cur is
      select trc.tax_record_id,
             tia.tax_book_id,
             b.tax_year,
             nvl(b.amount,0) amount,
             nvl(t.tax_activity_code_id,0) tax_activity_code_id,
             nvl(t.tax_activity_type_id,1) tax_activity_type_id
        from basis_amounts        b,
             tax_activity_code    t,
             tax_record_control   trc,
             tax_include_activity tia
       where b.tax_activity_code_id = t.tax_activity_code_id
         and b.tax_record_id = trc.tax_record_id
         and trc.tax_record_id = b.tax_record_id
         and tia.tax_include_id = b.tax_include_id
         and trc.version_id = g_version
         and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (tia.tax_book_id in (select * from table(g_table_book_ids)) or g_table_book_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and b.tax_year between g_start_year and g_end_year
       order by trc.tax_record_id, tia.tax_book_id, b.tax_year;


   type type_tax_book_activity_rec is table of tax_book_activity_cur%rowtype;

   g_tax_book_activity_rec type_tax_book_activity_rec;

   cursor tax_reconcile_cur is
      select tbr.tax_record_id,
             tbr.tax_year,
             nvl(tbr.basis_amount_beg,0) basis_amount_beg,
             nvl(tbr.basis_amount_end,0) basis_amount_end,
             nvl(basis_amount_activity,0) basis_amount_activity,
             nvl(tbr.reconcile_item_id,0) reconcile_item_id,
             trim(upper(tri.type)) reconcile_item_type,
             nvl(tbr.tax_include_id,0) tax_include_id,
             nvl(basis_amount_input_retire,0) basis_amount_input_retire,
             nvl(tri.input_retire_ind,0) input_retire_ind,
             nvl(tbrt.basis_amount_beg, 0) basis_amount_transfer,
             nvl(tri.calced,0) calced,
             nvl(tri.depr_deduction,0) depr_deduction
        from tax_book_reconcile tbr,
             (select tax_record_id,
                     tax_include_id,
                     reconcile_item_id,
                     tax_year,
                     sum(basis_amount_beg) basis_amount_beg
                from tax_book_reconcile_transfer
               group by tax_record_id, tax_include_id, reconcile_item_id, tax_year) tbrt,
             tax_record_control trc,
             tax_reconcile_item tri
       where tbr.tax_record_id = trc.tax_record_id
         and tri.reconcile_item_id = tbr.reconcile_item_id
         and tbrt.tax_include_id(+) = tbr.tax_include_id
         and tbrt.tax_year(+) = tbr.tax_year
         and tbrt.tax_record_id(+) = tbr.tax_record_id
         and tbrt.reconcile_item_id(+) = tbr.reconcile_item_id
         and trc.tax_record_id in (select *  from table(g_table_record_ids))
         and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and tbr.tax_year between g_start_year and g_end_year + 1.1
         and trc.version_id = g_version
       order by tbr.tax_record_id, tbr.tax_include_id, tbr.tax_year;

   type type_tax_reconcile_rec is table of tax_reconcile_cur%rowtype;

   g_tax_reconcile_rec type_tax_reconcile_rec;

   cursor tax_limitation_cur is
      select tax_limitation.tax_limit_id,
             tax_limitation.limitation,
             year,
             nvl(tax_limit.compare_rate, 0) compare_rate
        from tax_limitation, tax_limit
       where tax_limitation.tax_limit_id = tax_limit.tax_limit_id
       order by year;

   type type_tax_limitation_rec is table of tax_limitation_cur%rowtype;

   g_tax_limitation_rec type_tax_limitation_rec;

   cursor tax_includes_activity_cur is
      select tax_book_id, tax_include_id from tax_include_activity order by tax_book_id;
   type tax_includes_type is table of number(22, 0);

   type type_tax_include_activity_tab is record(
      tax_book_id     number(22, 0),
      tax_include_ids tax_includes_type);

   type tax_includes_activity_type is table of type_tax_include_activity_tab;
   g_tax_includes_activity_rec tax_includes_activity_type;

   cursor tax_limit2_cur is
      select tl.tax_limit_id,
             nvl(tl.tax_credit_percent,0) tax_credit_percent,
             nvl(tl.basis_reduction_percent,0) basis_reduction_percent,
             nvl(tl.reconcile_item_id,0) reconcile_item_id,
             nvl(tl.tax_include_id,0) tax_include_id,
             tl.calc_future_years,
             nvl(tcs.ordering,0) ordering,
             nvl(tri.calced,0) calced,
             nvl(tri.depr_deduction,0) depr_deduction,
             nvl(td.tax_book_id,0) tax_book_id,
             nvl(tcs.tax_credit_id,0) tax_credit_id,
             nvl(trc.tax_record_id,0) tax_record_id,
             nvl(td.tax_year,0) tax_year
        from tax_limit          tl,
             tax_depreciation   td,
             tax_credit_schema  tcs,
             tax_reconcile_item tri,
             tax_record_control trc
       where tcs.tax_credit_id = td.tax_credit_id
         and td.tax_record_id = trc.tax_record_id
         and tl.tax_limit_id = tcs.tax_limit_id
         and tl.reconcile_item_id = tri.reconcile_item_id(+)
         and trc.version_id = g_version
         and ( td.tax_book_id in (select * from table(g_table_book_ids)) or g_table_book_count = 0)
         and td.tax_year between g_start_year and g_end_year
       order by trc.tax_record_id, td.tax_book_id, td.tax_year, tax_limit_id;

   type type_tax_limit2_rec is table of tax_limit2_cur%rowtype;

   g_tax_limit2_rec type_tax_limit2_rec;

   function get_version return varchar2;

   function calc_depr(a_job_no number) return integer;

   function get_tax_depr(a_start_row out pls_integer) return integer;

   function get_tax_convention return integer;

   function get_tax_rates return integer;

   function get_tax_book_activity return integer;

   function get_tax_reconcile return integer;

   function get_tax_limitation return integer;

   function get_tax_include_activity return integer;

   function get_tax_job_creation return integer;

   procedure write_log(a_job_no     number,
                       a_error_type number,
                       a_code       number,
                       a_msg        varchar2);

   function update_tax_depr(a_start_row pls_integer,
                            a_num_rec   pls_integer) return number;

   function update_tax_book_reconcile(a_num_rec pls_integer) return number;

   function insert_tax_book_reconcile(a_num_rec               pls_integer,
                                      a_add_tax_reconcile_rec in out nocopy type_tax_reconcile_rec)
      return number;

   function calc(a_tax_depr_index            integer,
                 a_tax_depr_bals_index       integer,
                 a_tax_depr_bals_rows        pls_integer,
                 a_tax_convention_rec        tax_convention_cur%rowtype,
                 a_tax_convention_rows       pls_integer,
                 a_extraord_convention_rec   tax_convention_cur%rowtype,
                 a_extraord_convention_rows  pls_integer,
                 a_tax_limitation_sub_rec    type_tax_limitation_rec,
                 a_tax_limitation_rows       pls_integer,
                 a_tax_reconcile_sub_rec     in out nocopy type_tax_reconcile_rec,
                 a_tax_reconcile_rows        in out nocopy pls_integer,
                 a_reconcile_bals_sub_rec    in out nocopy type_tax_reconcile_rec,
                 a_reconcile_bals_rows       in out nocopy pls_integer,
                 a_tax_job_creation_sub_rec  type_tax_limit2_rec,
                 a_tax_job_creation_rows     pls_integer,
                 a_tax_book_activity_sub_rec type_tax_book_activity_rec,
                 a_tax_book_activity_rows    pls_integer,
                 a_tax_rates_sub_rec         type_tax_rates_rec,
                 a_tax_rates_rows            pls_integer,
                 a_short_months              in number,
                 a_add_tax_reconcile_rec     in out nocopy type_tax_reconcile_rec,
                 a_add_tax_reconcile_rows    in out nocopy integer,
                 a_nyear                     number) return number;

   procedure set_session_parameter;

   g_job_no  number(22, 0);
   g_line_no number(22, 0);

   g_debug_record_id number(22, 0);

end TAX_DEPR_CALC;
/
create or replace package body TAX_DEPR_CALC is
   -- =============================================================================
   --  Function GET_VERSION
   -- =============================================================================

   function get_version return varchar2 is
   begin
      return '2015.2.0.0';
   end get_version;

   -- =============================================================================
   --  Function FILTER_TAX_BOOK_ACTIVITY
   -- =============================================================================
   function filter_tax_book_activity(a_tax_book_activity_rec in out nocopy type_tax_book_activity_rec,
                                     a_tax_record_id         pls_integer,
                                     a_tax_book_id           pls_integer,
                                     a_tax_year              number) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_book_activity_hash(to_char(a_tax_record_id) || g_sep || to_char(a_tax_book_id) || g_sep || to_char(a_tax_year))
                 .count;
      a_tax_book_activity_rec.delete;

      for i in 1 .. l_count
      loop
         l_row := g_tax_book_activity_hash(to_char(a_tax_record_id) || g_sep ||
                                           to_char(a_tax_book_id) || g_sep || to_char(a_tax_year)) (i);
         a_tax_book_activity_rec.extend;
         a_tax_book_activity_rec(i) := g_tax_book_activity_rec(l_row);
      end loop;
      return l_count;

   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_book_activity ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_book_activity;

   -- =============================================================================
   --  Function FILTER_TAX_RECONCILE
   -- =============================================================================
   function filter_tax_reconcile(a_tax_reconcile_rec in out nocopy type_tax_reconcile_rec,
                                 a_tax_record_id     pls_integer,
                                 a_include_ids       tax_includes_type,
                                 l_tax_year          number) return pls_integer is
      i                     pls_integer;
      k                     pls_integer;
      l_count               pls_integer;
      l_index               pls_integer;
      l_row_count           pls_integer;
      l_tax_reconcile_count pls_integer;
      l_include_id_count    pls_integer;
      l_include_id          pls_integer;
      l_code                pls_integer;
   begin
      l_row_count := 0;
      a_tax_reconcile_rec.delete;
      l_include_id_count := a_include_ids.count;
      for i in 1 .. l_include_id_count
      loop
         l_include_id := a_include_ids(i);
         begin
            l_tax_reconcile_count := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep || to_char(l_include_id) || g_sep || to_char(l_tax_year))
                                     .count;
         exception
            when no_data_found then
               l_tax_reconcile_count := 0;
         end;

         for k in 1 .. l_tax_reconcile_count
         loop
            l_index     := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                                to_char(l_tax_year))(k).id;
            l_row_count := l_row_count + 1;
            a_tax_reconcile_rec.extend;
            a_tax_reconcile_rec(l_row_count) := g_tax_reconcile_rec(l_index);

            /** START Breakpoint for Debugger.**/
            if a_tax_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. FILTER_TAX_RECONCILE. end of loop, l_row_count : '||l_row_count||', l_tax_year : '||l_tax_year||', l_include_id : '||l_include_id);
            end if;
            /** END Breakpoint for Debugger.**/
         end loop;
      end loop;
      i := 0;
      return l_row_count;
   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_reconcile_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_reconcile;

   -- =============================================================================
   --  Function ADD_TAX_RECONCILE
   -- =============================================================================
   function add_tax_reconcile(   a_tax_record_id     pls_integer,
                                 a_include_id        pls_integer,
                                 a_tax_year          number,
                                 a_reconcile_item_id  pls_integer,
                                 a_record_index pls_integer) return pls_integer is
      k                     pls_integer;
      l_tax_reconcile_count pls_integer;
      l_include_id_count    pls_integer;
      l_include_id          pls_integer;
      l_code                pls_integer;
   begin
      l_include_id := a_include_id;
      begin
            l_tax_reconcile_count := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                 to_char(l_include_id) ||
                 g_sep || to_char(a_tax_year)).count;
      exception
            when no_data_found then
               l_tax_reconcile_count := 0;
      end;
      if l_tax_reconcile_count > 0 then
            k := l_tax_reconcile_count + 1;
            g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                             to_char(a_tax_year))(k).id := a_record_index;
      else
            g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                             to_char(a_tax_year))(1).id := a_record_index;
      end if;
      g_tax_reconcile_index_hash( to_char(a_tax_record_id) || g_sep ||
                                  to_char(l_include_id) || g_sep ||
                                  to_char(a_tax_year) || g_sep ||
                                  to_char(a_reconcile_item_id) ) := a_record_index;
      return 1;
   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_reconcile_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end add_tax_reconcile;

   -- =============================================================================
   --  Function FILTER_TAX_RECONCILE_BALS
   -- =============================================================================
   function filter_tax_reconcile_bals(a_tax_reconcile_rec in out nocopy type_tax_reconcile_rec,
                                      a_tax_record_id     pls_integer,
                                      a_include_ids       tax_includes_type,
                                      a_tax_year          number) return pls_integer is
      i                     pls_integer;
      l_code                pls_integer;
      l_count               pls_integer;
      l_include_id          pls_integer;
      l_index               pls_integer;
      l_row_count           pls_integer;
      l_tax_reconcile_count pls_integer;
      l_include_id_count    pls_integer;
      l_low_year            number(22,2); -- 5.30 Fix
      l_tax_year            number(22,2);  -- 5.30 Fix
   begin

      a_tax_reconcile_rec.delete;
      l_include_id_count := a_include_ids.count;
      l_low_year         := 0;
      -- find the bal year
      for i in 1 .. l_include_id_count
      loop
         l_include_id := a_include_ids(i);
         begin
            l_tax_reconcile_count := g_tax_reconcile_bal_hash(to_char(a_tax_record_id) || g_sep || to_char(l_include_id))
                                     .count;
         exception
            when no_data_found then
               l_tax_reconcile_count := 0;
         end;

         for k in 1 .. l_tax_reconcile_count
         loop
            l_index := g_tax_reconcile_bal_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id))(k).id;
            if (g_tax_reconcile_rec(l_index)
               .tax_year > a_tax_year and
                (g_tax_reconcile_rec(l_index).tax_year < l_low_year or l_low_year = 0)) then
               l_low_year := g_tax_reconcile_rec(l_index).tax_year;
            end if;
         end loop;
      end loop;

      l_row_count := 0;
      l_tax_year  := l_low_year;
      for i in 1 .. l_include_id_count
      loop
         l_include_id := a_include_ids(i);
         begin
            l_tax_reconcile_count := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep || to_char(l_include_id) || g_sep || to_char(l_tax_year))
                                     .count;
         exception
            when no_data_found then
               l_tax_reconcile_count := 0;
         end;
         for k in 1 .. l_tax_reconcile_count
         loop
            l_index     := g_tax_reconcile_hash(to_char(a_tax_record_id) || g_sep ||
                                                to_char(l_include_id) || g_sep ||
                                                to_char(l_tax_year))(k).id;
            l_row_count := l_row_count + 1;
            a_tax_reconcile_rec.extend;
            a_tax_reconcile_rec(l_row_count) := g_tax_reconcile_rec(l_index);
         end loop;
      end loop;

      i := 0;
      return l_row_count;

   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_reconcile_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_reconcile_bals;

   -- =============================================================================
   --  Function FILTER_TAX_LIMITATION
   -- =============================================================================
   function filter_tax_limitation(a_tax_limitation_sub_rec in out nocopy type_tax_limitation_rec,
                                  a_limit_id               pls_integer) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_limitation_rec.count;
      a_tax_limitation_sub_rec.delete;
      l_row := 0;
      for i in 1 .. l_count
      loop
         if g_tax_limitation_rec(i).tax_limit_id = a_limit_id then
            l_row := 1 + l_row;
            a_tax_limitation_sub_rec.extend;
            a_tax_limitation_sub_rec(l_row) := g_tax_limitation_rec(i);
         end if;
      end loop;
      return l_row;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_limitation ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_limitation;

   -- =============================================================================
   --  Function FILTER_TAX_LIMIT
   -- =============================================================================
   function filter_tax_limit(a_tax_limit_sub_rec in out nocopy type_tax_limit2_rec,
                             a_record_id         pls_integer,
                             a_book_id           pls_integer,
                             a_tax_year          number) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_index pls_integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_limit2_rec.count;
      if a_tax_limit_sub_rec.exists(1) then
         a_tax_limit_sub_rec.delete;
      end if;
      l_row := 0;

      l_count := g_tax_limit2_hash(to_char(a_record_id) || g_sep || to_char(a_book_id) || g_sep || to_char(a_tax_year)).count;
      for i in 1 .. l_count
      loop
         l_index := g_tax_limit2_hash(to_char(a_record_id) || g_sep || to_char(a_book_id) || g_sep || to_char(a_tax_year))(i).id;
         a_tax_limit_sub_rec.extend;
         a_tax_limit_sub_rec(i) := g_tax_limit2_rec(l_index);
      end loop;
      return l_count;
   exception
      when no_data_found then
         return 0;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_limit ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_limit;

   -- =============================================================================
   --  Function FILTER_TAX_RATES
   -- =============================================================================
   function filter_tax_rates(a_tax_rates_sub_rec in out nocopy type_tax_rates_rec,
                             a_rate_id           pls_integer) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_index pls_integer;
      l_code  pls_integer;
   begin

      l_count := g_tax_rates_rec.count;
      if a_tax_rates_sub_rec.exists(1) then
         a_tax_rates_sub_rec.delete;
      end if;
      l_row := 0;

      begin
         l_count := g_tax_rates_hash(a_rate_id).count;
      exception
         when no_data_found then
            WRITE_LOG(G_JOB_NO, 1, 0, 'Error!  No tax rates found for tax rate id = '||a_rate_id||' ! Exiting...');
            return - 1;
      end;

      for i in 1 .. l_count
      loop
         l_index := g_tax_rates_hash(a_rate_id)(i).id;
         a_tax_rates_sub_rec.extend(1);
         a_tax_rates_sub_rec(i) := g_tax_rates_rec(l_index);
      end loop;
      return l_count;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_rates ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_rates;

   -- =============================================================================
   --  Function CALC_DEPR
   -- =============================================================================
   function calc_depr(a_job_no number) return integer is
      cursor tax_job_params_cur is
         select version, tax_year, vintage, tax_book_id, tax_class_id, company_id
           from tax_job_params
          where job_no = a_job_no;
      l_version       number(22, 0);
      l_tax_year      number(22, 2);
      l_vintage       number(22, 0);
      l_tax_book_id   number(22, 0);
      l_tax_record_id number(22, 0);
      l_tax_class_id  number(22, 0);
      l_company_id    number(22, 0);
      type tax_years_type is table of number(22, 2) index by pls_integer;
      tax_years             tax_years_type;
      tax_year_count        pls_integer;
      l_current_year        number(22, 2);
      l_depr_index          pls_integer;
      l_tax_depr_rows       pls_integer;
      l_tax_depr_bals_index pls_integer;
      code                  integer;
      rows                  pls_integer;
      l_short_months        pls_integer;
      l_next_year           number(22, 2);
      l_year                number(22, 2);
      l_year_count          pls_integer;
      k                     pls_integer;

      l_tax_book_activity_sub_rec  type_tax_book_activity_rec;
      l_tax_book_activity_sub_rows pls_integer;
      l_count                      pls_integer;
      l_tax_include_activity_index pls_integer;
      l_tax_book_reconcile_rows    pls_integer;
      l_tax_reconcile_sub_rows     pls_integer;
      l_tax_reconcile_sub_rec      type_tax_reconcile_rec;
      l_reconcile_bals_sub_rows    pls_integer;
      l_reconcile_bals_sub_rec     type_tax_reconcile_rec;
      l_tax_limitation_rows        pls_integer;
      l_tax_limitation_sub_rows    pls_integer;
      l_tax_rates_rows             pls_integer;
      l_tax_book_activity_rows     pls_integer;
      l_tax_include_activity_rows  pls_integer;
      l_tax_convention_rows        pls_integer;
      l_extraordinary_conv_index   pls_integer;
      l_tax_convention_index       pls_integer;
      l_tax_limitation_sub_rec     type_tax_limitation_rec;
      l_tax_job_creation_sub_rec   type_tax_limit2_rec;
      l_tax_job_creation_sub_rows  pls_integer;
      l_tax_rates_sub_rec          type_tax_rates_rec;
      l_tax_rates_sub_rows         pls_integer;
      l_add_tax_reconcile_rec      type_tax_reconcile_rec;
      l_add_tax_reconcile_rows     pls_integer;
      l_add_total_tax_reconcile_rows     pls_integer := 0;
      l_code                       pls_integer;
      l_start_row                  pls_integer;
      l_total_rows                 pls_integer := 0;
      m                            pls_integer;
      rtn_code                     number(22,0);

   begin
      g_start_time := current_timestamp;
      g_job_no  := a_job_no;
      g_line_no := 1;
      dbms_output.enable(100000);
      write_log(g_job_no, 0, 0, 'Tax Depreciation  Started Version=' || get_version());
      set_session_parameter();

      rtn_code := AUDIT_TABLE_PKG.setcontext('audit','no');

      g_table_book_ids         := table_list_id_type(-1);
      g_table_vintage_ids      := table_list_id_type();
      g_table_class_ids        := table_list_id_type(-1);
      g_table_company_ids      := table_list_id_type(-1);
      g_table_record_ids       := table_list_id_type(-1);
      g_tax_depr_save1_rec     := type_tax_depr_save_rec();
      g_tax_depr_save2_rec     := type_tax_depr_save_rec();

      tax_year_count        := 0;
      g_table_book_count    := 0;
      g_table_vintage_count := 0;
      g_table_class_count   := 0;
      g_table_company_count := 0;

      l_tax_limitation_sub_rec    := type_tax_limitation_rec();
      l_tax_rates_sub_rec         := type_tax_rates_rec();
      l_tax_job_creation_sub_rec  := type_tax_limit2_rec();
      l_add_tax_reconcile_rec     := type_tax_reconcile_rec();
      l_reconcile_bals_sub_rec    := type_tax_reconcile_rec();
      l_tax_book_activity_sub_rec := type_tax_book_activity_rec();

      /**enter the record id to follow through the Debugger and/or logs here: **/
      g_debug_record_id := 999999999999;

      open tax_job_params_cur;
      loop
         fetch tax_job_params_cur
            into l_version, l_tax_year, l_vintage, l_tax_book_id, l_tax_class_id, l_company_id;
         if not l_version is null then
            g_version := l_version;
         end if;
         if not l_tax_year is null then
            tax_year_count := tax_year_count + 1;
            tax_years(tax_year_count) := l_tax_year;
         end if;
         if not l_vintage is null then
            g_table_vintage_count := 1 + g_table_vintage_count;
            g_table_vintage_ids.extend;
            g_table_vintage_ids(g_table_vintage_count) := l_vintage;
         end if;
         if not l_tax_book_id is null then
            g_table_book_count := 1 + g_table_book_count;
            g_table_book_ids.extend;
            g_table_book_ids(g_table_book_count) := l_tax_book_id;
         end if;
         if not l_tax_class_id is null then
            g_table_class_count := 1 + g_table_class_count;
            g_table_class_ids.extend;
            g_table_class_ids(g_table_class_count) := l_tax_class_id;
         end if;
         if not l_company_id is null then
            g_table_company_count := 1 + g_table_company_count;
            g_table_company_ids.extend;
            g_table_company_ids(g_table_company_count) := l_company_id;
         end if;
         exit when tax_job_params_cur%notfound;
      end loop;

      g_start_year := tax_years(tax_years.first);
      g_end_year   := tax_years(tax_years.last);

      l_tax_convention_rows       := get_tax_convention();
      l_tax_rates_rows            := get_tax_rates();
      l_tax_book_activity_rows    := get_tax_book_activity();
      l_tax_limitation_rows       := get_tax_limitation();
      l_tax_include_activity_rows := get_tax_include_activity();
      l_tax_reconcile_sub_rec     := type_tax_reconcile_rec();

      rows := get_tax_job_creation();
      loop
         l_tax_depr_rows := get_tax_depr(l_start_row);
         exit when l_tax_depr_rows = 0;

         l_tax_book_reconcile_rows   := get_tax_reconcile();

         l_current_year := 0;
         l_total_rows   := l_total_rows + (l_tax_depr_rows - l_start_row + 1);

         for l_depr_index in l_start_row .. l_tax_depr_rows
         loop
            l_tax_year      := g_tax_depr_rec(l_depr_index).tax_year;
            l_tax_record_id := g_tax_depr_rec(l_depr_index).tax_record_id;
            l_tax_book_id   := g_tax_depr_rec(l_depr_index).tax_book_id;

            /** START Breakpoint for Debugger.**/
            if l_tax_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. CALC_DEPR. start of inner loop, tax book : '||l_tax_book_id||', tax year : '||l_tax_year);
            end if;
            /** END Breakpoint for Debugger.**/

            -- see if the the tax year has changed from the previous record
            if l_current_year <> g_tax_depr_rec(l_depr_index).tax_year then
               l_current_year := g_tax_depr_rec(l_depr_index).tax_year;
               begin
                  select nvl(months, 0)
                    into l_short_months
                    from tax_year_version
                   where version_id = g_version
                     and tax_year = l_current_year;
                  if (l_short_months <= 0 or l_short_months >= 12) then
                     l_short_months := 12;
                  end if;
               exception
                  when no_data_found then
                     l_short_months := 12;
               end;
               select nvl(min(tax_year), 0)
                 into l_next_year
                 from tax_year_version
                where version_id = g_version
                  and tax_year > l_current_year;
            end if;

            l_tax_depr_bals_index := 0;
            if g_tax_depr_bals_hash(to_char(l_tax_record_id) || g_sep || to_char(l_tax_book_id))
             .exists(1) then
               l_year_count := g_tax_depr_bals_hash(to_char(l_tax_record_id) || g_sep || to_char(l_tax_book_id))
                               .count;

               for k in 1 .. l_year_count
               loop
                  l_year := g_tax_depr_bals_hash(to_char(l_tax_record_id) || g_sep ||
                                                 to_char(l_tax_book_id))(k).tax_year;
                  if l_year = l_next_year  then
                     l_tax_depr_bals_index := g_tax_depr_bals_hash(to_char(l_tax_record_id) ||
                                                g_sep || to_char(l_tax_book_id))(k).id;
                  end if;
               end loop;
            end if;

            -- get tax book activity record
            l_tax_book_activity_sub_rows := filter_tax_book_activity(l_tax_book_activity_sub_rec,
                                                                     l_tax_record_id,
                                                                     l_tax_book_id,
                                                                     l_tax_year);


            -- check for any activity
            l_tax_include_activity_index := 0;
            for k in 1 .. l_tax_include_activity_rows
            loop
               if g_tax_includes_activity_rec(k).tax_book_id = l_tax_book_id then
                  l_tax_include_activity_index := k;
                  exit;
               end if;
            end loop;

            -- if no activity index was found, it means that there was no row in tax include activity, which is
            -- a config issue/bad data.  This may have other unintended consequences, so error out with a clear
            -- message.
            if l_tax_include_activity_index = 0 or l_tax_include_activity_index is null then
               WRITE_LOG(G_JOB_NO, 1, 0, 'Error!  Tax book id '||l_tax_book_id||' was not found in tax_include_activity.  Please fix your system configuration for this tax book and try again.');
               return - 1;
            end if;

            if l_tax_include_activity_index <= l_tax_include_activity_rows then
               l_tax_reconcile_sub_rows := filter_tax_reconcile(l_tax_reconcile_sub_rec,
                                                                l_tax_record_id,
                                                                g_tax_includes_activity_rec(l_tax_include_activity_index)
                                                                .tax_include_ids,
                                                                l_tax_year);

               l_reconcile_bals_sub_rows := filter_tax_reconcile_bals(l_reconcile_bals_sub_rec,
                                                                      l_tax_record_id,
                                                                      g_tax_includes_activity_rec(l_tax_include_activity_index)
                                                                      .tax_include_ids,
                                                                      l_tax_year);
            else
               l_tax_reconcile_sub_rows  := 0;
               l_reconcile_bals_sub_rows := 0;
            end if;

            l_tax_convention_index := g_tax_convention_hash(g_tax_depr_rec(l_depr_index)
                                                            .convention_id);

            l_extraordinary_conv_index := g_tax_convention_hash(g_tax_depr_rec(l_depr_index)
                                                                .extraordinary_convention);

            l_tax_limitation_sub_rows := filter_tax_limitation(l_tax_limitation_sub_rec,
                                                               g_tax_depr_rec(l_depr_index)
                                                               .tax_limit_id);

            l_tax_job_creation_sub_rows := filter_tax_limit(l_tax_job_creation_sub_rec,
                                                            l_tax_record_id,
                                                            l_tax_book_id,
                                                            l_tax_year);

            l_tax_rates_sub_rows := filter_tax_rates(l_tax_rates_sub_rec,
                                                     g_tax_depr_rec(l_depr_index).tax_rate_id);

            ----filter tax rates function was enhanced to exit if there were no rates to use
            ----bad config, but force the exit if encountered, don't continue on.
            ----error message for this situation is in the filter tax rates function.
            if l_tax_rates_sub_rows < 0 then
               return -1;
            end if;

            if l_tax_rates_sub_rows <> 0 then
               l_code := calc(l_depr_index,
                              l_tax_depr_bals_index,
                              l_tax_depr_bals_index,
                              g_tax_convention_rec(l_tax_convention_index),
                              l_tax_convention_index,
                              g_tax_convention_rec(l_extraordinary_conv_index),
                              l_extraordinary_conv_index,
                              l_tax_limitation_sub_rec,
                              l_tax_limitation_sub_rows,
                              l_tax_reconcile_sub_rec,
                              l_tax_reconcile_sub_rows,
                              l_reconcile_bals_sub_rec,
                              l_reconcile_bals_sub_rows,
                              l_tax_job_creation_sub_rec,
                              l_tax_job_creation_sub_rows,
                              l_tax_book_activity_sub_rec,
                              l_tax_book_activity_sub_rows,
                              l_tax_rates_sub_rec,
                              l_tax_rates_sub_rows,
                              l_short_months,
                              l_add_tax_reconcile_rec,
                              l_add_tax_reconcile_rows,
                              l_next_year);


               if l_code = -1 then
                  return - 1;
               end if;
               l_add_total_tax_reconcile_rows := l_add_tax_reconcile_rows + l_add_total_tax_reconcile_rows;
            end if;
         end loop;
         l_code := update_tax_depr(l_start_row, l_tax_depr_rows);
         l_code := insert_tax_book_reconcile(l_add_total_tax_reconcile_rows, l_add_tax_reconcile_rec);
         l_code := update_tax_book_reconcile(l_tax_book_reconcile_rows+l_add_total_tax_reconcile_rows);
         l_add_total_tax_reconcile_rows := 0;
         l_add_tax_reconcile_rec.delete;
         if l_code = -1 then
            return - 1;
         end if;
      end loop;

      write_log(g_job_no, 0, 0, 'Total Calculation Rows=' || to_char(l_total_rows));
      commit;
      rtn_code := AUDIT_TABLE_PKG.setcontext('audit','yes');
      write_log(g_job_no, 0, 0, 'Finished');
      return 0;

   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ', tax record : ' || l_tax_record_id || dbms_utility.format_error_backtrace);
         return 0;
   end;

   -- =============================================================================
   --  Function GET_TAX_DEPR
   -- =============================================================================
   function get_tax_depr(a_start_row out pls_integer) return integer is
      l_count             integer;
      l_save_count        pls_integer;
      i                   pls_integer;
      j                   pls_integer;
      k                   pls_integer;
      code                pls_integer;
      l_current_record_id pls_integer;
      l_current_book_id   pls_integer;
      l_year_count        pls_integer;
      type array_of_intgers is table of pls_integer;
      l_records_not_users            array_of_intgers;
      l_tax_depr_last_save_record_id pls_integer;
      l_tax_depr_last_save_book_id   pls_integer;
      l_move_row_count               pls_integer;
   begin
      if not tax_depr_cur%isopen then
         write_log(g_job_no, 1, 0, 'Tax Depreciation Rows: 0 ');
         open tax_depr_cur;
      else
         g_tax_depr_hash.delete;
         g_tax_depr_bals_hash.delete;
      end if;
      a_start_row := 1;
      fetch tax_depr_cur bulk collect
         into g_tax_depr_rec limit g_limit;

      i       := g_tax_depr_rec.first;
      l_count := g_tax_depr_rec.count;

      if l_count = 0 then
         close tax_depr_cur;

      else
         g_tax_depr_last_record_id := g_tax_depr_rec(l_count).tax_record_id;
         g_tax_depr_last_book_id   := g_tax_depr_rec(l_count).tax_book_id;
      end if;

      /** START Breakpoint for Debugger.**/
      if g_tax_depr_last_record_id = g_debug_record_id then
         WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_DEPR. equal to g_tax_depr_last_record_id.');
      end if;
      /** END Breakpoint for Debugger.**/

      -- save the last records that are not complete
      k := 0;
      g_tax_depr_save2_rec.delete;
      if l_count = g_limit then
         j := 1;
         for j in reverse 1 .. l_count
         loop
            if g_tax_depr_last_record_id = g_tax_depr_rec(j).tax_record_id then
               g_tax_depr_save2_rec.extend;
               k := k + 1;
               g_tax_depr_save2_rec(k) := g_tax_depr_rec(j);
            else
               exit;
            end if;
         end loop;
         l_count := l_count - k;
      else
         g_tax_depr_last_record_id := 0;
         g_tax_depr_last_book_id   := 0;
      end if;

      -- restore the save records
      j := 0;

      if g_tax_depr_save1_rec.count > 0 then
         l_save_count := g_tax_depr_save1_rec.count;
         k            := 0;
         for j in reverse 1 .. l_save_count
         loop
            g_tax_depr_rec.extend;
            g_tax_depr_rec(l_count + k + 1) := g_tax_depr_save1_rec(j);
            k := k + 1;
         end loop;
         l_count := l_count + l_save_count;
         -- we must reorganize the collection
         l_tax_depr_last_save_record_id := g_tax_depr_save1_rec(1).tax_record_id;
         l_tax_depr_last_save_book_id   := g_tax_depr_save1_rec(1).tax_book_id;
         l_move_row_count               := 0;
         for j in 1 .. l_count
         loop
            if l_tax_depr_last_save_record_id = g_tax_depr_rec(j).tax_record_id then
               l_move_row_count := l_move_row_count + 1;
            else
               exit;
            end if;
         end loop;

         for j in 1 .. l_move_row_count
         loop
            g_tax_depr_rec.extend;
            g_tax_depr_rec(l_count + j) := g_tax_depr_rec(j);
         end loop;
         l_count     := l_count + l_move_row_count;
         a_start_row := l_move_row_count + 1;
      end if;

      g_tax_depr_save1_rec := g_tax_depr_save2_rec;

      -- create an index to the data

      i                   := a_start_row;
      l_current_record_id := 0;
      l_current_book_id   := 0;
      k := 1;
      g_table_record_ids.delete;

      while i <= l_count
      loop

         if  l_current_record_id <> g_tax_depr_rec(i).tax_record_id or
            l_current_record_id = 0 then
             g_table_record_ids.extend;
             g_table_record_ids(k) := g_tax_depr_rec(i).tax_record_id;
             k := k + 1;
         end if;

         g_tax_depr_hash(to_char(g_tax_depr_rec(i).tax_record_id) || g_sep || to_char(g_tax_depr_rec(i).tax_book_id) || g_sep || to_char(g_tax_depr_rec(i).tax_year)) := i;

         if l_current_record_id = g_tax_depr_rec(i).tax_record_id and
            l_current_book_id = g_tax_depr_rec(i).tax_book_id then

            /** START Breakpoint for Debugger.**/
            if l_current_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_DEPR, "while" statement creating the index, the if part. ID : '||I||', L_COUNT : '||L_COUNT||', TAX_YEAR : '||g_tax_depr_rec(I).TAX_YEAR||', L_CURRENT_BOOK_ID : '||L_CURRENT_BOOK_ID);
            end if;
            /** END Breakpoint for Debugger.**/

            l_year_count := l_year_count + 1;
            g_tax_depr_bals_hash(to_char(g_tax_depr_rec(i).tax_record_id) || g_sep || to_char(g_tax_depr_rec(i).tax_book_id))(l_year_count).tax_year := g_tax_depr_rec(i)
                                                                                                                                                        .tax_year;
            g_tax_depr_bals_hash(to_char(g_tax_depr_rec(i).tax_record_id) || g_sep || to_char(g_tax_depr_rec(i).tax_book_id))(l_year_count).id := i;
         else
            l_year_count        := 1;
            l_current_record_id := g_tax_depr_rec(i).tax_record_id;
            l_current_book_id   := g_tax_depr_rec(i).tax_book_id;

            /** START Breakpoint for Debugger.**/
            if l_current_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_DEPR, "while" statement creating the index, the else part. ID : '||I||', L_COUNT : '||L_COUNT||', TAX_YEAR : '||g_tax_depr_rec(I).TAX_YEAR||', L_CURRENT_BOOK_ID : '||L_CURRENT_BOOK_ID);
            end if;
            /** END Breakpoint for Debugger.**/

            g_tax_depr_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_book_id))(l_year_count).tax_year := g_tax_depr_rec(i)
                                                                                                                                .tax_year;
            g_tax_depr_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_book_id))(l_year_count).id := i;
         end if;
         i := i + 1;
      end loop;

      -- g_tax_depr_rec.delete;
      write_log(g_job_no, 1, 0, 'Tax Depreciation Rows: ' || to_char(l_count - a_start_row + 1));
      return l_count;

   exception
      when others then
         code := sqlcode;
         write_log(g_job_no,
                   4,
                   code,
                   ' Tax Depr ' || sqlerrm(code) || ', Record '||l_current_record_id || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_depr;

   -- =============================================================================
   --  Function GET_TAX_CONVENTION
   -- =============================================================================
   function get_tax_convention return integer is
      l_count integer;
      i       pls_integer;
      code    pls_integer;
   begin
      if not tax_convention_cur%isopen then
         open tax_convention_cur;
      end if;

      fetch tax_convention_cur bulk collect
         into g_tax_convention_rec;

      i       := g_tax_convention_rec.first;
      l_count := g_tax_convention_rec.count;

      if l_count = 0 then
         close tax_convention_cur;
      end if;

      i := 1;
      while i <= l_count
      loop
         g_tax_convention_hash(g_tax_convention_rec(i).convention_id) := i;
         i := i + 1;
      end loop;

      write_log(g_job_no, 1, 0, 'Tax Convention Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_convention;

   -- =============================================================================
   --  Function GET_TAX_RATES
   -- =============================================================================
   function get_tax_rates return integer is
      l_count        integer;
      i              pls_integer;
      code           pls_integer;
      l_last_rate_id pls_integer;
      l_rows         pls_integer;
   begin
      if not tax_rates_cur%isopen then
         open tax_rates_cur;
      end if;

      fetch tax_rates_cur bulk collect
         into g_tax_rates_rec;

      i       := g_tax_rates_rec.first;
      l_count := g_tax_rates_rec.count;

      if l_count = 0 then
         close tax_rates_cur;
      end if;

      -- create an index to the data
      l_count        := g_tax_rates_rec.count;
      i              := 1;
      l_last_rate_id := 0;
      while i <= l_count
      loop
         if g_tax_rates_rec(i).tax_rate_id = l_last_rate_id and i <> 1 then
            l_rows := l_rows + 1;
            g_tax_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         else
            l_rows := 1;
            l_last_rate_id := g_tax_rates_rec(i).tax_rate_id;
            g_tax_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         end if;
         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Rates Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_rates;

   -- =============================================================================
   --  Function GET_TAX_BOOK_ACTIVITY
   -- =============================================================================
   function get_tax_book_activity return integer is
      l_count     integer;
      i           pls_integer;
      code        pls_integer;
      l_row_count pls_integer;
      l_found     boolean;
   begin
      if not tax_book_activity_cur%isopen then
         open tax_book_activity_cur;
      end if;

      fetch tax_book_activity_cur bulk collect
         into g_tax_book_activity_rec;

      i       := g_tax_book_activity_rec.first;
      l_count := g_tax_book_activity_rec.count;

      close tax_book_activity_cur;

      -- create an index to the data
      l_count     := g_tax_book_activity_rec.count;
      i           := 1;
      l_row_count := 0;
      while i <= l_count
      loop
         begin

            /** START Breakpoint for Debugger.**/
            if g_tax_book_activity_rec(i).tax_record_id = g_debug_record_id then
               WRITE_LOG(G_JOB_NO, 1, 0, ' *** tr '||g_debug_record_id||' discovered. GET_TAX_BOOK_ACTIVITY, "while" statement creating the index. ID : '||I||', L_COUNT : '||L_COUNT||', TAX_YEAR : '||g_tax_book_activity_rec(i).tax_year||', TAX BOOK : '||g_tax_book_activity_rec(i).tax_book_id);
            end if;
            /** END Breakpoint for Debugger.**/

            l_found := g_tax_book_activity_hash(to_char(g_tax_book_activity_rec(i).tax_record_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_book_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_year))
                       .exists(1);
         exception
            when no_data_found then
               l_found := false;
         end;
         if l_found = true then
            l_row_count := 1 + l_row_count;
         else
            l_row_count := 1;
         end if;
         g_tax_book_activity_hash(to_char(g_tax_book_activity_rec(i).tax_record_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_book_id) || g_sep || to_char(g_tax_book_activity_rec(i).tax_year))(l_row_count) := i;
         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Book Activity Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no,
                   4,
                   code,
                   'Tax Book Activity i=' || to_char(i) || ' ' || sqlerrm(code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_book_activity;

   -- =============================================================================
   --  Function GET_TAX_RECONCILE
   -- =============================================================================
   function get_tax_reconcile return integer is
      l_count                  integer;
      i                        pls_integer;
      code                     pls_integer;
      l_current_record_id      pls_integer;
      l_current_include_id     pls_integer;
      l_current_bal_record_id  pls_integer;
      l_current_bal_include_id pls_integer;

      l_year_count              pls_integer;
      l_tax_reconcile_bal_count pls_integer;
      l_tax_reconcile_count     pls_integer;
      l_current_year            number(22, 2);
   begin
      if not tax_reconcile_cur%isopen then
         open tax_reconcile_cur;
      end if;

      g_tax_reconcile_index_hash.delete;
      g_tax_reconcile_hash.delete;
      g_tax_reconcile_bal_hash.delete;

      fetch tax_reconcile_cur bulk collect
         into g_tax_reconcile_rec;

      i       := g_tax_reconcile_rec.first;
      l_count := g_tax_reconcile_rec.count;

      close tax_reconcile_cur;

      -- create an index to the data
      l_count                   := g_tax_reconcile_rec.count;
      i                         := 1;
      l_current_bal_record_id   := 0;
      l_current_bal_include_id  := 0;
      l_current_record_id       := 0;
      l_current_include_id      := 0;
      l_current_year            := 0;
      l_tax_reconcile_bal_count := 0;
      while i <= l_count
      loop
         if l_current_record_id = g_tax_reconcile_rec(i).tax_record_id and
                l_current_include_id = g_tax_reconcile_rec(i).tax_include_id and
                g_tax_reconcile_rec(i).tax_year = l_current_year then
            l_tax_reconcile_count := 1 + l_tax_reconcile_count;

         else
            l_tax_reconcile_count := 1;
            l_current_record_id   := g_tax_reconcile_rec(i).tax_record_id;
            l_current_include_id  := g_tax_reconcile_rec(i).tax_include_id;
            l_current_year        := g_tax_reconcile_rec(i).tax_year;

         end if;

         g_tax_reconcile_index_hash( to_char(l_current_record_id) || g_sep ||
                                  to_char(l_current_include_id) || g_sep ||
                                  to_char(l_current_year) || g_sep ||
                                  to_char(g_tax_reconcile_rec(i).reconcile_item_id) ) := i;


         g_tax_reconcile_hash(to_char(l_current_record_id) || g_sep ||
                to_char(l_current_include_id) || g_sep ||
                to_char(l_current_year))(l_tax_reconcile_count).reconcile_item := g_tax_reconcile_rec(i).reconcile_item_id;

         g_tax_reconcile_hash(to_char(l_current_record_id) || g_sep ||
                 to_char(l_current_include_id) || g_sep ||
                 to_char(l_current_year))(l_tax_reconcile_count).id := i;

         if l_current_bal_record_id = g_tax_reconcile_rec(i).tax_record_id and
            l_current_bal_include_id = g_tax_reconcile_rec(i).tax_include_id then
            l_tax_reconcile_bal_count := l_tax_reconcile_bal_count + 1;
         else
            l_tax_reconcile_bal_count := 1;
            l_current_bal_include_id  := g_tax_reconcile_rec(i).tax_include_id;
            l_current_bal_record_id   := g_tax_reconcile_rec(i).tax_record_id;
         end if;

         g_tax_reconcile_bal_hash(to_char(l_current_bal_record_id) || g_sep ||
               to_char(l_current_bal_include_id))(l_tax_reconcile_bal_count).tax_year := g_tax_reconcile_rec(i).tax_year;

         g_tax_reconcile_bal_hash(to_char(l_current_bal_record_id) || g_sep ||
              to_char(l_current_bal_include_id))(l_tax_reconcile_bal_count).reconcile_item := g_tax_reconcile_rec(i).reconcile_item_id;

         g_tax_reconcile_bal_hash(to_char(l_current_bal_record_id) || g_sep ||
              to_char(l_current_bal_include_id))(l_tax_reconcile_bal_count).id := i;

         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Reconcile Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_reconcile;

   -- =============================================================================
   --  Function GET_TAX_LIMITATION
   -- =============================================================================
   function get_tax_limitation return integer is
      l_count integer;
      i       pls_integer;
      code    pls_integer;
   begin
      if not tax_limitation_cur%isopen then
         open tax_limitation_cur;
      end if;

      fetch tax_limitation_cur bulk collect
         into g_tax_limitation_rec;

      i       := g_tax_limitation_rec.first;
      l_count := g_tax_limitation_rec.count;

      if l_count = 0 then
         close tax_limitation_cur;
      end if;

      write_log(g_job_no, 1, 0, 'Tax limitation Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_limitation;

   -- =============================================================================
   --  Function GET_TAX_INCLUDE_ACTIVITY
   -- =============================================================================
   function get_tax_include_activity return integer is
      l_count          integer;
      i                pls_integer;
      code             pls_integer;
      l_tax_book_id    pls_integer;
      l_prev_book_id   pls_integer;
      l_tax_include_id pls_integer;
      l_id_count       pls_integer;
      l_row_count      pls_integer := 0;
   begin
      i                           := 0;
      g_tax_includes_activity_rec := tax_includes_activity_type();
      l_prev_book_id              := -111111;
      open tax_includes_activity_cur;
      loop
         fetch tax_includes_activity_cur
            into l_tax_book_id, l_tax_include_id;
         exit when tax_includes_activity_cur%notfound;
         l_row_count := l_row_count + 1;
         if l_tax_book_id <> l_prev_book_id then
            i := 1 + i;
            g_tax_includes_activity_rec.extend;
            g_tax_includes_activity_rec(i).tax_book_id := l_tax_book_id;
            g_tax_includes_activity_rec(i).tax_include_ids := tax_includes_type();
            g_tax_includes_activity_rec(i).tax_include_ids.extend;
            g_tax_includes_activity_rec(i).tax_include_ids(1) := l_tax_include_id;
            l_id_count := 1;
            l_prev_book_id := l_tax_book_id;
         else
            g_tax_includes_activity_rec(i).tax_include_ids.extend;
            l_id_count := l_id_count + 1;
            g_tax_includes_activity_rec(i).tax_include_ids(l_id_count) := l_tax_include_id;

         end if;
      end loop;
      l_count := g_tax_includes_activity_rec.count;
      close tax_includes_activity_cur;
      write_log(g_job_no, 1, 0, 'Tax Include Activity Rows: ' || to_char(l_row_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         dbms_output.put_line('Tax Include Activity ' || sqlerrm(code) || ' ' ||
                              dbms_utility.format_error_backtrace);
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_include_activity;

   -- =============================================================================
   --  Function GET_TAX_JOB_CREATION
   -- =============================================================================
   function get_tax_job_creation return integer is
      l_count          integer;
      i                pls_integer;
      code             pls_integer;
      l_row            pls_integer := 0;
      l_last_book_id   pls_integer;
      l_last_record_id pls_integer;
      l_last_tax_year  number;
   begin
      if not tax_limit2_cur%isopen then
         open tax_limit2_cur;
      end if;

      fetch tax_limit2_cur bulk collect
         into g_tax_limit2_rec;

      i       := g_tax_limit2_rec.first;
      l_count := g_tax_limit2_rec.count;

      close tax_limit2_cur;

      -- create an index to the data
      l_count          := g_tax_limit2_rec.count;
      i                := 1;
      l_last_record_id := 0;
      l_last_book_id   := 0;
      l_last_tax_year   := 0;
      while i <= l_count
      loop

         if g_tax_limit2_rec(i)
          .tax_record_id = l_last_record_id and g_tax_limit2_rec(i).tax_book_id = l_last_book_id and
           g_tax_limit2_rec(i).tax_year = l_last_tax_year then
            l_row := 1 + l_row;
         else
            l_row            := 1;
            l_last_record_id := g_tax_limit2_rec(i).tax_record_id;
            l_last_book_id   := g_tax_limit2_rec(i).tax_book_id;
            l_last_tax_year   := g_tax_limit2_rec(i).tax_year;
         end if;
         g_tax_limit2_hash(to_char(g_tax_limit2_rec(i).tax_record_id) || g_sep ||
                           to_char(g_tax_limit2_rec(i).tax_book_id) || g_sep ||
                           to_char(g_tax_limit2_rec(i).tax_year))(l_row).id := i;
         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Tax Job Creation Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_job_creation;

   -- =============================================================================
   --  Function UPDATE_TAX_DEPR
   -- =============================================================================
   function update_tax_depr(a_start_row pls_integer,
                            a_num_rec   pls_integer) return number as
      i      pls_integer;
      l_code pls_integer;

   begin
      forall i in a_start_row .. a_num_rec
         update tax_depreciation
            set book_balance = g_tax_depr_rec(i).book_balance,
                tax_balance = g_tax_depr_rec(i).tax_balance,
                remaining_life = g_tax_depr_rec(i).remaining_life,
                accum_reserve = g_tax_depr_rec(i).accum_reserve,
                sl_reserve = g_tax_depr_rec(i).sl_reserve,
                depreciable_base = g_tax_depr_rec(i).depreciable_base,
                fixed_depreciable_base = g_tax_depr_rec(i).fixed_depreciable_base,
                actual_salvage = g_tax_depr_rec(i).actual_salvage,
                estimated_salvage_end = g_tax_depr_rec(i).estimated_salvage_end,
                additions = g_tax_depr_rec(i).additions,
                transfers = g_tax_depr_rec(i).transfers,
                adjustments = g_tax_depr_rec(i).adjustments,
                retirements = g_tax_depr_rec(i).retirements,
                extraordinary_retires = g_tax_depr_rec(i).extraordinary_retires,
                accum_ordinary_retires = g_tax_depr_rec(i).accum_ordinary_retires,
                cost_of_removal = g_tax_depr_rec(i).cost_of_removal,
                est_salvage_pct = g_tax_depr_rec(i).est_salvage_pct,
                retire_invol_conv = g_tax_depr_rec(i).retire_invol_conv,
                salvage_invol_conv = g_tax_depr_rec(i).salvage_invol_conv,
                salvage_extraord = g_tax_depr_rec(i).salvage_extraord,
                reserve_at_switch = g_tax_depr_rec(i).reserve_at_switch,
                reserve_at_switch_end = g_tax_depr_rec(i).reserve_at_switch_end,
                depreciation = g_tax_depr_rec(i).depreciation + nvl(g_tax_depr_rec(i).transfer_depr, 0),
                gain_loss = g_tax_depr_rec(i).gain_loss,
                ex_gain_loss = g_tax_depr_rec(i).ex_gain_loss,
                capital_gain_loss = g_tax_depr_rec(i).capital_gain_loss,
                calc_depreciation = g_tax_depr_rec(i).calc_depreciation,
                over_adj_depreciation = g_tax_depr_rec(i).over_adj_depreciation,
                retire_res_impact = g_tax_depr_rec(i).retire_res_impact,
                ex_retire_res_impact = g_tax_depr_rec(i).ex_retire_res_impact,
                transfer_res_impact = g_tax_depr_rec(i).transfer_res_impact,
                salvage_res_impact = g_tax_depr_rec(i).salvage_res_impact,
                adjusted_retire_basis = g_tax_depr_rec(i).adjusted_retire_basis,
                book_balance_end = g_tax_depr_rec(i).book_balance_end,
                tax_balance_end = g_tax_depr_rec(i).tax_balance_end,
                accum_reserve_end = g_tax_depr_rec(i).accum_reserve_end,
                sl_reserve_end = g_tax_depr_rec(i).sl_reserve_end,
                accum_salvage_end = g_tax_depr_rec(i).accum_salvage_end,
                accum_ordin_retires_end = g_tax_depr_rec(i).accum_ordin_retires_end,
                cor_expense = g_tax_depr_rec(i).cor_expense,
                cor_res_impact = g_tax_depr_rec(i).cor_res_impact,
                job_creation_amount = g_tax_depr_rec(i).job_creation_amount,
                number_months_beg = g_tax_depr_rec(i).number_months_beg,
                number_months_end = g_tax_depr_rec(i).number_months_end,
                quantity = g_tax_depr_rec(i).quantity,
                quantity_end = g_tax_depr_rec(i).quantity_end
          where tax_book_id = g_tax_depr_rec(i).tax_book_id
            and tax_record_id = g_tax_depr_rec(i).tax_record_id
            and tax_year = g_tax_depr_rec(i).tax_year;
      write_log(g_job_no, 3, 0, 'Update Tax Depreciation Rows=' || to_char(a_num_rec - a_start_row + 1));
      return 0;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Update Tax Depr ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return -1;
   end update_tax_depr;

   -- =============================================================================
   --  Function INSERT_TAX_BOOK_RECONCILE
   -- =============================================================================
   function insert_tax_book_reconcile(a_num_rec               pls_integer,
                                      a_add_tax_reconcile_rec in out nocopy type_tax_reconcile_rec)
      return number as
      i    pls_integer;
      j    pls_integer;
      l_code pls_integer;
      li_errors pls_integer;
      dml_errors EXCEPTION;
      PRAGMA EXCEPTION_INIT(dml_errors, -24381);

   begin
      forall i in 1 .. a_num_rec save exceptions
         insert into tax_book_reconcile
            (tax_include_id, tax_record_id, tax_year, reconcile_item_id, basis_amount_beg,
             basis_amount_end, basis_amount_activity, basis_amount_transfer)
         --calced,");
         --depr_deduction ");
         values
            (a_add_tax_reconcile_rec(i).tax_include_id, a_add_tax_reconcile_rec(i).tax_record_id,
             a_add_tax_reconcile_rec(i).tax_year, a_add_tax_reconcile_rec(i).reconcile_item_id,
             a_add_tax_reconcile_rec(i).basis_amount_beg,
             a_add_tax_reconcile_rec(i).basis_amount_end,
             a_add_tax_reconcile_rec(i).basis_amount_activity,
             a_add_tax_reconcile_rec(i).basis_amount_transfer);
      return 0;
      write_log(g_job_no, 3, 0, 'Insert Tax Book Reconcile Rows=' || to_char(a_num_rec));

   exception
      WHEN dml_errors THEN -- Now we figure out what failed and why.
           li_errors := SQL%BULK_EXCEPTIONS.COUNT;

           FOR i IN 1..li_errors LOOP
              j := SQL%BULK_EXCEPTIONS(i).ERROR_INDEX;
              write_log(g_job_no,
                   4,
                   l_code,
                   'Insert Tax Book Reconcile index=' || SQL%BULK_EXCEPTIONS(i).ERROR_INDEX ||
                         ' error= ' || SQLERRM(-SQL%BULK_EXCEPTIONS(i).ERROR_CODE) ||
                         ' rec= ' ||  to_char( a_add_tax_reconcile_rec(i).tax_record_id) ||
                         ' inc= ' ||  to_char( a_add_tax_reconcile_rec(i).tax_include_id) ||
                         ' year= ' || to_char( a_add_tax_reconcile_rec(i).tax_year) ||
                         ' item= ' || to_char( a_add_tax_reconcile_rec(i).reconcile_item_id) );
               if i > 100 then
                  exit;
               end if;
             end loop;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Insert Tax Book Reconcile ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end insert_tax_book_reconcile;

   -- =============================================================================
   --  Function UPDATE_TAX_BOOK_RECONCILE
   -- =============================================================================
   function update_tax_book_reconcile(a_num_rec pls_integer) return number as
      i      pls_integer;
      l_code pls_integer;
   begin
      forall i in 1 .. a_num_rec
         update tax_book_reconcile
            set basis_amount_beg = g_tax_reconcile_rec(i).basis_amount_beg,
                basis_amount_end = g_tax_reconcile_rec(i).basis_amount_end,
                basis_amount_activity = g_tax_reconcile_rec(i).basis_amount_activity,
                basis_amount_transfer = g_tax_reconcile_rec(i).basis_amount_transfer
         -- calced = :SQL_ARGUMENT(tax_reconcile_calced_val),
         -- depr_deduction = :SQL_ARGUMENT(tax_reconcile_depr_deduction)
          where tax_include_id = g_tax_reconcile_rec(i).tax_include_id
            and tax_record_id = g_tax_reconcile_rec(i).tax_record_id
            and tax_year = g_tax_reconcile_rec(i).tax_year
            and reconcile_item_id = g_tax_reconcile_rec(i).reconcile_item_id;

      write_log(g_job_no, 6, 0, 'Update Tax Book Reconcile Rows=' || to_char(a_num_rec));
      return 0;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Update Tax Book Reconcile ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end update_tax_book_reconcile;
   -- ************************************** Calc ************************************************
   function calc(a_tax_depr_index            integer,
                 a_tax_depr_bals_index       integer,
                 a_tax_depr_bals_rows        pls_integer,
                 a_tax_convention_rec        tax_convention_cur%rowtype,
                 a_tax_convention_rows       pls_integer,
                 a_extraord_convention_rec   tax_convention_cur%rowtype,
                 a_extraord_convention_rows  pls_integer,
                 a_tax_limitation_sub_rec    type_tax_limitation_rec,
                 a_tax_limitation_rows       pls_integer,
                 a_tax_reconcile_sub_rec     in out nocopy type_tax_reconcile_rec,
                 a_tax_reconcile_rows        in out nocopy pls_integer,
                 a_reconcile_bals_sub_rec    in out nocopy type_tax_reconcile_rec,
                 a_reconcile_bals_rows       in out nocopy pls_integer,
                 a_tax_job_creation_sub_rec  type_tax_limit2_rec,
                 a_tax_job_creation_rows     pls_integer,
                 a_tax_book_activity_sub_rec type_tax_book_activity_rec,
                 a_tax_book_activity_rows    pls_integer,
                 a_tax_rates_sub_rec         type_tax_rates_rec,
                 a_tax_rates_rows            pls_integer,
                 a_short_months              in number,
                 a_add_tax_reconcile_rec     in out nocopy type_tax_reconcile_rec,
                 a_add_tax_reconcile_rows    in out nocopy integer,
                 a_nyear                     number) return number is

      l_tax_job_creation_ordered    type_tax_limit2_rec;
      l_add_tax_reconcile_bals_flag boolean;
      l_net                         character(10);
      l_method                      pls_integer;
      l_code                        pls_integer;
      l_rlife                       pls_integer;
      l_wlife                       pls_integer;
      l_switched                    pls_integer;
      l_hconv                       pls_integer;
      l_trid                        pls_integer;
      l_jrow                        pls_integer;
      l_calc_future_years           pls_integer;
      l_calced                      pls_integer;
      l_depr_deduction              pls_integer;
      l_compare_rate                pls_integer;
      l_reconcile_index             pls_integer;

      l_life      binary_double;
      l_add_ratio binary_double;

      l_rem_life                   binary_double := 0;
      l_flife                      binary_double := 0;
      l_job_creation               binary_double;
      l_job_creation_percent       binary_double;
      l_job_creation_amount        binary_double := 0;
      l_reserve_at_switch_init     binary_double;
      l_basis_reduction_percent    binary_double;
      l_tax_credit_base            binary_double;
      l_tax_credit_amount          binary_double;
      l_tax_credit_basis_reduction binary_double;

      l_years_used            binary_double := 0;
      l_months_used           binary_double := 0;
      l_yr1_fraction          binary_double := 0;
      l_yr2_fraction          binary_double := 0;
      l_orig_short_months     binary_double := 0;
      l_short_year_net_adjust binary_double := 0;
      l_yr1_rate              binary_double := 0;
      l_yr2_rate              binary_double := 0;

      l_scoop boolean;

      l_tax_limitation binary_double := 0;
      i                pls_integer;
      j                pls_integer;
      k                pls_integer;
      l_num            pls_integer;
      l_pos            pls_integer;
      l_vintage        pls_integer;
      l_jc             pls_integer;
      r                pls_integer;
      l_found          boolean;

      l_ratio     binary_double := 0;
      l_ratio1    binary_double := 0;
      l_change    binary_double := 0;
      l_amount    binary_double := 0;
      l_depr_rate binary_double;
      type num_ary200_type is table of binary_double index by binary_integer;
      l_tax_rates num_ary200_type;

      l_retire_depr_conv     binary_double := 0;
      l_retire_bal_conv      binary_double := 0;
      l_retire_res_conv      binary_double := 0;
      l_gain_loss_conv       binary_double := 0;
      l_cap_gain_conv        binary_double := 0;
      l_salvage_conv         binary_double := 0;
      l_est_salvage_conv     binary_double := 0;
      l_cost_of_removal_conv binary_double := 0;
      l_retire_depr_ratio    binary_double := 0;
      l_retire_res_ratio     binary_double := 0;
      l_retire_bal_ratio     binary_double := 0;
      l_salvage_ratio        binary_double := 0;
      l_adr_salvage_ratio    binary_double := 0;
      l_beg_salvage_ratio    binary_double := 0;
      l_cor_ratio            binary_double := 0;
      l_depr_salvage_ratio   binary_double := 0;

      l_ex_retire_depr_conv   binary_double := 0;
      l_ex_retire_bal_conv    binary_double := 0;
      l_ex_retire_res_conv    binary_double := 0;
      l_ex_gain_loss_conv     binary_double := 0;
      l_ex_cap_gain_conv      binary_double := 0;
      l_ex_salvage_conv       binary_double := 0;
      l_ex_est_salvage_conv   binary_double := 0;
      l_ex_retire_depr_ratio  binary_double := 0;
      l_ex_retire_res_ratio   binary_double := 0;
      l_ex_retire_bal_ratio   binary_double := 0;
      l_ex_salvage_ratio      binary_double := 0;
      l_ex_beg_salvage_ratio  binary_double := 0;
      l_ex_depr_salvage_ratio binary_double := 0;

      l_tax_reconcile_beg      num_ary200_type;
      l_tax_reconcile_act      num_ary200_type;
      l_tax_reconcile_end      num_ary200_type;
      l_tax_reconcile_ret      num_ary200_type;
      l_tax_reconcile_transfer num_ary200_type;
      l_tax_reconcile_type     num_ary200_type;

      type pls_integer_ary_type is table of pls_integer index by binary_integer;
      l_input_retire_ind pls_integer_ary_type;

      l_begin_res_impact   binary_double := 0;
      l_reserve            binary_double := 0;
      l_retirement_depr    binary_double := 0;
      l_ex_retirement_depr binary_double := 0;
      l_rounding           pls_integer;

      --* Activity Variables

      l_book_retirement          binary_double := 0;
      l_book_adjustment          binary_double := 0;
      l_book_addition            binary_double := 0;
      l_book_extraord_retirement binary_double := 0;

      --* Adjustment Variables

      l_book_balance_adjust     binary_double := 0;
      l_accum_reserve_adjust    binary_double := 0;
      l_depreciable_base_adjust binary_double := 0;
      l_depreciation_adjust     binary_double := 0;
      l_gain_loss_adjust        binary_double := 0;
      l_cap_gain_loss_adjust    binary_double := 0;

      l_book_balance_adjust_method   binary_double;
      l_accum_reserve_adjust_method  binary_double;
      l_depreciable_base_adjust_meth binary_double;
      l_depreciation_adjust_meth     binary_double;
      l_ex_begin_res_impact          binary_double := 0;
      l_ex_retire_res_impact         binary_double := 0;

      --* EXEC SQL BEGIN DECLARE SECTION;
      l_rowid                  binary_double;
      l_tax_reconcile_beg_val  binary_double;
      l_tax_reconcile_act_val  binary_double;
      l_tax_reconcile_end_val  binary_double;
      l_tax_year               binary_double := 0;
      l_tax_year_1             binary_double := 0;
      l_tax_book_id            binary_double := 0;
      l_tax_record_id          binary_double := 0;
      l_reconcile_item_id      binary_double := 0;
      l_book_balance           binary_double := 0;
      l_tax_balance            binary_double := 0;
      l_remaining_life         binary_double := 0;
      l_accum_reserve          binary_double := 0;
      l_sl_reserve             binary_double := 0;
      l_depreciable_base       binary_double := 0;
      l_fixed_depreciable_base binary_double := 0;
      l_actual_salvage         binary_double := 0;
      l_estimated_salvage      binary_double := 0;
      l_accum_salvage          binary_double := 0;
      l_additions              binary_double := 0;
      l_transfers              binary_double := 0;
      l_adjustments            binary_double := 0;
      l_retirements            binary_double := 0;

      l_cap_gain_retirements    binary_double;
      l_cap_gain_ex_retirements binary_double;
      l_extraordinary_retires   binary_double := 0;
      l_accum_ordinary_retires  binary_double := 0;
      l_depreciation            binary_double := 0;
      l_cost_of_removal         binary_double := 0;
      l_gain_loss               binary_double := 0;
      l_capital_gain_loss       binary_double := 0;
      l_ex_capital_gain_loss    binary_double := 0;
      l_est_salvage_pct         binary_double := 0;
      l_book_balance_end        binary_double := 0;
      l_tax_balance_end         binary_double := 0;
      l_accum_reserve_end       binary_double := 0;
      l_sl_reserve_end          binary_double := 0;
      l_accum_salvage_end       binary_double := 0;
      l_accum_ordin_retires_end binary_double := 0;
      l_retire_invol_conv       binary_double := 0;
      l_salvage_invol_conv      binary_double := 0;
      l_salvage_extraord        binary_double := 0;
      l_calc_depreciation       binary_double := 0;
      l_over_adj_depreciation   binary_double := 0;
      l_retire_res_impact       binary_double := 0;
      l_transfer_res_impact     binary_double := 0;
      l_salvage_res_impact      binary_double := 0;
      l_adjusted_retire_basis   binary_double := 0;
      l_reserve_at_switch       binary_double := 0;
      l_reserve_at_switch_end   binary_double := 0;
      l_quantity                binary_double := 0;
      l_quantity_end            binary_double := 0;
      l_cor_res_impact          binary_double := 0;
      l_cor_expense             binary_double := 0;
      l_number_months_beg       binary_double := 0;
      l_number_months_end       binary_double := 0;
      l_ex_gain_loss            binary_double := 0;

      --* Transfer Variables

      l_book_balance_transfer        binary_double;
      l_tax_balance_transfer         binary_double;
      l_accum_reserve_transfer       binary_double;
      l_sl_reserve_transfer          binary_double;
      l_fixed_depreciable_base_trans binary_double;
      l_estimated_salvage_transfer   binary_double;
      l_accum_salvage_transfer       binary_double;
      l_accum_ordinary_retires_trans binary_double;
      l_reserve_at_switch_transfer   binary_double;
      l_quantity_transfer            binary_double;

      /* EXEC SQL END DECLARE SECTION;        */
      /*l_tax_convention_rows pls_integer;
      l_extraord_convention_rows pls_integer;
      l_tax_rates_rows pls_integer;
      l_tax_reconcile_rows pls_integer;
      l_tax_limitation_rows pls_integer;
      l_tax_job_creation_rows pls_integer;
      l_tax_book_activity_rows pls_integer;
      l_tax_depr_bals_rows pls_integer;
      l_reconcile_bals_rows pls_integer; */
      l_rows  pls_integer;
      l_nyear pls_integer;
      -- Maint -10475
      l_first_year_retire    boolean;
      l_ex_first_year_retire boolean;
      l_short_months         binary_double;
      l_msg varchar2(100);

   begin

      l_short_months := a_short_months;
      /*
      l_tax_convention_rows := a_tax_convention_sub_rec.count;
      l_extraord_convention_rows := a_tax_convention_sub_rec.count;
      l_tax_rates_rows := a_tax_rates_sub_rec.count;
      l_tax_reconcile_rows := a_tax_reconcile_sub_rec.count;
      l_tax_limitation_rows := a_tax_limitation_sub_rec.count;
      l_tax_job_creation_rows  := a_tax_job_creation_rec.count;
      l_tax_book_activity_rows  := a_tax_book_activity_rec.count;
      l_tax_depr_bals_rows  := a_tax_depr_bals_sub_rec.count;
      l_reconcile_bals_rows  := a_reconcile_bals_sub_rec.count;
      */

      /* move the items in the tax_depreciation table to variables
      get rid of the statements we don't end up needing  */

      --if dw_tax_depr.rowcount() > 0 then
      l_book_balance_transfer        := nvl(g_tax_depr_rec(a_tax_depr_index).book_balance_xfer, 0);
      l_tax_balance_transfer         := nvl(g_tax_depr_rec(a_tax_depr_index).tax_balance_xfer, 0);
      l_accum_reserve_transfer       := nvl(g_tax_depr_rec(a_tax_depr_index).accum_reserve_xfer, 0);
      l_sl_reserve_transfer          := nvl(g_tax_depr_rec(a_tax_depr_index).sl_reserve_xfer, 0);
      l_fixed_depreciable_base_trans := nvl(g_tax_depr_rec(a_tax_depr_index)
                                            .fixed_depreciable_base_xfer,
                                            0);
      l_estimated_salvage_transfer   := nvl(g_tax_depr_rec(a_tax_depr_index).estimated_salvage_xfer,
                                            0);
      l_accum_salvage_transfer       := nvl(g_tax_depr_rec(a_tax_depr_index).accum_salvage_xfer, 0);
      l_accum_ordinary_retires_trans := nvl(g_tax_depr_rec(a_tax_depr_index)
                                            .accum_ordinary_retires_xfer,
                                            0);
      l_reserve_at_switch_transfer   := nvl(g_tax_depr_rec(a_tax_depr_index).reserve_at_switch_xfer,
                                            0);
      l_quantity_transfer            := nvl(g_tax_depr_rec(a_tax_depr_index).quantity_xfer, 0);
      /*
      else
      {
        book_balance_transfer             =  0;
        tax_balance_transfer                     =  0;
        accum_reserve_transfer             =  0;
        sl_reserve_transfer                   =  0;
        fixed_depreciable_base_transfer     =  0;
        estimated_salvage_transfer           =  0;
        accum_salvage_transfer               =  0;
        accum_ordinary_retires_transfer       =  0;
        reserve_at_switch_transfer               =  0;
        quantity_transfer                  =  0;
      }
      */
      a_add_tax_reconcile_rows := 0;

      l_tax_year               := g_tax_depr_rec(a_tax_depr_index).tax_year;
      l_book_balance           := nvl(g_tax_depr_rec(a_tax_depr_index).book_balance, 0) +
                                  l_book_balance_transfer;
      l_tax_balance            := nvl(g_tax_depr_rec(a_tax_depr_index).tax_balance, 0) +
                                  l_tax_balance_transfer;
      l_remaining_life         := nvl(g_tax_depr_rec(a_tax_depr_index).remaining_life, 0);
      l_accum_reserve          := nvl(g_tax_depr_rec(a_tax_depr_index).accum_reserve, 0) +
                                  l_accum_reserve_transfer;
      l_sl_reserve             := nvl(g_tax_depr_rec(a_tax_depr_index).sl_reserve, 0) +
                                  l_sl_reserve_transfer;
      l_fixed_depreciable_base := nvl(g_tax_depr_rec(a_tax_depr_index).fixed_depreciable_base, 0);

      if (l_fixed_depreciable_base <> 0) then
         l_fixed_depreciable_base := l_fixed_depreciable_base + l_fixed_depreciable_base_trans;
      end if;

      l_actual_salvage    := nvl(g_tax_depr_rec(a_tax_depr_index).actual_salvage, 0);
      l_estimated_salvage := nvl(g_tax_depr_rec(a_tax_depr_index).estimated_salvage, 0);

      if (l_estimated_salvage <> 0) then
         l_estimated_salvage := l_estimated_salvage + l_estimated_salvage_transfer;
      end if;

      l_accum_salvage          := nvl(g_tax_depr_rec(a_tax_depr_index).accum_salvage, 0) +
                                  l_accum_salvage_transfer;
      l_additions              := nvl(g_tax_depr_rec(a_tax_depr_index).additions, 0);
      l_transfers              := nvl(g_tax_depr_rec(a_tax_depr_index).transfers, 0);
      l_adjustments            := nvl(g_tax_depr_rec(a_tax_depr_index).adjustments, 0);
      l_retirements            := nvl(g_tax_depr_rec(a_tax_depr_index).retirements, 0);
      l_extraordinary_retires  := nvl(g_tax_depr_rec(a_tax_depr_index).extraordinary_retires, 0);
      l_accum_ordinary_retires := nvl(g_tax_depr_rec(a_tax_depr_index).accum_ordinary_retires, 0) +
                                  l_accum_ordinary_retires_trans;
      l_cost_of_removal        := nvl(g_tax_depr_rec(a_tax_depr_index).cost_of_removal, 0);
      l_est_salvage_pct        := nvl(g_tax_depr_rec(a_tax_depr_index).est_salvage_pct, 0);
      l_retire_invol_conv      := nvl(g_tax_depr_rec(a_tax_depr_index).retire_invol_conv, 0);
      l_salvage_invol_conv     := nvl(g_tax_depr_rec(a_tax_depr_index).salvage_invol_conv, 0);
      l_salvage_extraord       := nvl(g_tax_depr_rec(a_tax_depr_index).salvage_extraord, 0);
      l_reserve_at_switch      := nvl(g_tax_depr_rec(a_tax_depr_index).reserve_at_switch, 0) +
                                  l_reserve_at_switch_transfer;
      l_quantity               := nvl(g_tax_depr_rec(a_tax_depr_index).quantity, 0);
      l_number_months_beg      := nvl(g_tax_depr_rec(a_tax_depr_index).number_months_beg, 0);
      l_reserve_at_switch_init := 0;

      l_add_ratio := 1;

      --* Get The Tax Adjustments

      l_book_balance_adjust     := nvl(g_tax_depr_rec(a_tax_depr_index)
                                       .book_balance_adjust,
                                       0);
      l_accum_reserve_adjust    := nvl(g_tax_depr_rec(a_tax_depr_index)
                                       .accum_reserve_adjust,
                                       0);
      l_depreciable_base_adjust := nvl(g_tax_depr_rec(a_tax_depr_index)
                                       .depreciable_base_adjust,
                                       0);
      l_depreciation_adjust     := nvl(g_tax_depr_rec(a_tax_depr_index)
                                       .depreciation_adjust,
                                       0);
      l_gain_loss_adjust        := nvl(g_tax_depr_rec(a_tax_depr_index).gain_loss_adjust,
                                       0);
      l_cap_gain_loss_adjust    := nvl(g_tax_depr_rec(a_tax_depr_index)
                                       .cap_gain_loss_adjust,
                                       0);

      l_book_balance_adjust_method   := nvl(g_tax_depr_rec(a_tax_depr_index)
                                            .book_balance_adjust_method,
                                            1);
      l_accum_reserve_adjust_method  := nvl(g_tax_depr_rec(a_tax_depr_index)
                                            .accum_reserve_adjust_method,
                                            1);
      l_depreciable_base_adjust_meth := nvl(g_tax_depr_rec(a_tax_depr_index)
                                            .depreciable_base_adjust_method,
                                            1);
      l_depreciation_adjust_meth     := nvl(g_tax_depr_rec(a_tax_depr_index)
                                            .depreciation_adjust_method,
                                            1);


      --* Get The Tax Conventions
      if (a_tax_convention_rows = 0) then
         l_retire_depr_conv := 0;
         l_retire_bal_conv  := 0;
         l_retire_res_conv  := 0;
         l_gain_loss_conv   := 0;
         l_salvage_conv     := 0;
         l_est_salvage_conv := 0;
         l_cap_gain_conv    := 0;
      else
         l_retire_depr_conv     := a_tax_convention_rec.retire_depr_id;
         l_retire_bal_conv      := a_tax_convention_rec.retire_bal_id;
         l_retire_res_conv      := a_tax_convention_rec.retire_reserve_id;
         l_gain_loss_conv       := a_tax_convention_rec.gain_loss_id;
         l_salvage_conv         := a_tax_convention_rec.salvage_id;
         l_est_salvage_conv     := a_tax_convention_rec.est_salvage_id;
         l_cost_of_removal_conv := a_tax_convention_rec.cost_of_removal_id;
         l_cap_gain_conv        := a_tax_convention_rec.cap_gain_id;
      end if;

      if (a_extraord_convention_rows = 0) then
         l_ex_retire_depr_conv := 0;
         l_ex_retire_bal_conv  := 0;
         l_ex_retire_res_conv  := 0;
         l_ex_gain_loss_conv   := 0;
         l_ex_salvage_conv     := 0;
         l_ex_est_salvage_conv := 0;
      else
         l_ex_retire_depr_conv := a_extraord_convention_rec.retire_depr_id;
         l_ex_retire_bal_conv  := a_extraord_convention_rec.retire_bal_id;
         l_ex_retire_res_conv  := a_extraord_convention_rec.retire_reserve_id;
         l_ex_gain_loss_conv   := a_extraord_convention_rec.gain_loss_id;
         l_ex_salvage_conv     := a_extraord_convention_rec.salvage_id;
         l_ex_est_salvage_conv := a_extraord_convention_rec.est_salvage_id;
         l_ex_cap_gain_conv    := a_extraord_convention_rec.cap_gain_id;
      end if;

      --* get the tax rates, and lives and conventions for remaining life rates.

      /*  dw_tax_rates.setsort('tax_rates_year a')
      dw_tax_rates.sort() */

      l_num := a_tax_rates_rows;

      for i in 1 .. l_num
      loop
         l_tax_rates(i) := a_tax_rates_sub_rec(i).rate;
      end loop;
      l_net      := a_tax_rates_sub_rec(1).net_gross;
      l_rounding := nvl(a_tax_rates_sub_rec(1).rounding_convention, 0);
      l_life     := nvl(a_tax_rates_sub_rec(1).life, 0);
      l_rlife    := nvl(a_tax_rates_sub_rec(1).remaining_life_plan, 0);
      l_method   := nvl(a_tax_rates_sub_rec(1).start_method, 0);
      l_vintage  := nvl(g_tax_depr_rec(a_tax_depr_index).vintage_year, 0);
      l_switched := nvl(a_tax_rates_sub_rec(1).switched_year, 0);
      l_hconv    := nvl(a_tax_rates_sub_rec(1).half_year_convention, 0);

      --* upper is important for good match

      l_net := upper(l_net);

      /*i = round; */

      -- which depreciation rates should we use for this tax year, if not on remaining life.

      if (trunc(l_tax_year) <> l_vintage) then
         if (l_number_months_beg = -1 or l_number_months_beg = 0) then
            l_number_months_beg := (round(l_tax_year) - l_vintage) * 12.0;
         end if;
      end if;

      l_orig_short_months := l_short_months;

      if (trunc(l_tax_year) = l_vintage) then
         if (l_number_months_beg = -1 or l_number_months_beg = 0) then
            if (l_hconv <> 1) then
               l_short_months := 12;
            else
               l_short_months := l_short_months / 2;
            end if;
         end if;
      end if;

      l_number_months_end := l_number_months_beg + l_short_months;
      l_years_used        := trunc((l_number_months_beg / 12));
      --l_months_used              =  (l_number_months_beg % 12);
      l_months_used := mod(l_number_months_beg, 12.0);
      --yr1_fraction                :=  mid(12.0 - l_months_used,l_short_month);
      if (12.0 - l_months_used) > l_short_months then
         l_yr1_fraction := l_short_months;
      else
         l_yr1_fraction := 12.0 - l_months_used;
      end if;
      l_yr2_fraction := l_short_months - l_yr1_fraction;

      l_yr1_fraction := l_yr1_fraction / 12.0;
      l_yr2_fraction := l_yr2_fraction / 12.0;

      i := l_years_used + 1;

      --i =  l_tax_year - l_vintage + 1;

      l_scoop := false;
      if (i != 1) then
         if (i >= a_tax_rates_rows and (l_months_used + l_short_months) >= 12 and
            l_accum_reserve <> 0) then
            l_scoop := true;
         end if;
         if (i >= (a_tax_rates_rows + 1) and l_accum_reserve <> 0) then
            l_scoop := true;
         end if;
      else
         if (l_tax_rates(i) = 0 and l_accum_reserve <> 0) then
            l_scoop := true;
         end if;
      end if;
      --* If a life rate, then there are only 2 rates, the first year and all subsequent years
      --* there us never scoop

      if (l_method = 6) then
         if (i > 1) then
            i := 2;
         end if;
         l_scoop := false;
      end if;

      --* If a vintage rate, match the tax_year with the designated year

      if (l_method = 7) then
         l_num := a_tax_rates_rows;
         for ti in 1 .. l_num
         loop
            --maint 38243
            if trunc(l_tax_year) = a_tax_rates_sub_rec(ti).year then
               i := ti;
               exit;
            end if;
         end loop;

         if (i > l_num) then
            i := a_tax_rates_rows + 1;
         else
            -- IF this is the first year, then replace the first year rates.
            if (trunc(l_tax_year) = l_vintage and l_number_months_beg = 0) then
               l_tax_rates(i) := a_tax_rates_sub_rec(i).rate1 * (l_orig_short_months / 12);
            end if;
         end if;

         l_scoop := false;
         if (i = a_tax_rates_rows and (l_months_used + l_short_months) >= 12 and
            l_accum_reserve <> 0) then
            l_scoop := true;
         end if;
      end if;

      if (l_rlife <> 1 ) then
         if ((i > a_tax_rates_rows) or (i < 1)) then
            l_depr_rate := 0;
         else
            if (l_method = 6 and i = 2) then
               --  rate should just be the 2nd year of a like rate
               l_depr_rate := l_tax_rates(i) * (l_yr1_fraction + l_yr2_fraction);
            else
               l_depr_rate := l_tax_rates(i) * l_yr1_fraction;
            end if;

            if (i + 1 <= a_tax_rates_rows) then -- Maint-34525  fixed the wrong rate used for Maint-29675
               l_depr_rate := l_depr_rate + (l_tax_rates(i + 1) * l_yr2_fraction);
            end if;
            if (i > a_tax_rates_rows) then
               l_yr1_rate := 0;
            else
               l_yr1_rate := l_tax_rates(i);
            end if;
            if ((i + 1) > a_tax_rates_rows) then
               l_yr2_rate := 0;
            else
               l_yr2_rate := l_tax_rates(i + 1);
            end if;
            if (trim(l_net) = 'GRNET') then
               if (i < l_switched) then
                  l_net := 'GROSS';
               else
                  l_net := 'NET';
               end if;
            end if;
            if (trim(l_net) = 'NETGR') then
               if (i = l_switched) then
                  l_reserve_at_switch_init := l_accum_reserve;
                  l_reserve_at_switch      := l_reserve_at_switch + l_reserve_at_switch_init;
               end if;
               if (i < l_switched) then
                  l_net := 'NET';
               else
                  l_net := 'GROSS';
               end if;
            end if;
         end if;
      end if;

      --* calculate the depreciation rate to use this tax year, if on remaining life plan.

      if (l_rlife = 1) then
         l_net := 'NET';
         if (i < 1) then
            l_depr_rate := 0;
            l_yr1_rate  := 0;
            l_yr2_rate  := 0;
         else
            if (l_tax_balance = 0) then
               --* check for divide by zero
               l_rem_life := 0;
            else
               l_rem_life := l_life * (l_sl_reserve / l_tax_balance);
            end if;
            if (l_rem_life > l_life) then
               l_rem_life := l_life;
            end if;

            if (l_rem_life < 1) then
               l_rem_life := 1;
            end if;
            --* sl case is easy

            if (l_method != 2) then
               --* sl case
               if (l_rem_life != 0) then
                  --* div by 0 protect
                  l_depr_rate := (1.0 / l_rem_life) * (l_short_months / 12.0);
                  l_yr1_rate  := 1 / l_rem_life;
                  l_yr2_rate  := 1 / l_rem_life;
               else
                  l_depr_rate := 0;
                  l_yr1_rate  := 0;
                  l_yr2_rate  := 0;
               end if;
               --*syd case requires syd calc
            else
               --* syd case

               l_wlife := trunc(l_rem_life); --* whole number part
               l_flife := l_rem_life - l_wlife; --* fractional part

               if (((l_wlife = 0) and (l_flife = 0)) or l_wlife = -1) then
                  --* div by zero check
                  l_depr_rate := 0;
                  l_yr1_rate  := 0;
                  l_yr2_rate  := 0;
               else
                  l_depr_rate := l_rem_life /
                                 (((l_wlife + 1) * l_wlife / 2) + ((l_wlife + 1) * l_flife));
                  l_yr1_rate  := l_depr_rate;
                  l_yr2_rate  := l_depr_rate;
                  l_depr_rate := l_depr_rate * (l_short_months / 12.0);
               end if;

            end if; --* syd case
         end if;

         if (abs(l_depr_rate) > 1) then
            l_depr_rate := 1;
         end if;
         if (abs(l_yr1_rate) > 1) then
            l_yr1_rate := 1;
         end if;
         if (abs(l_yr2_rate) > 1) then
            l_yr2_rate := 1;
         end if;
         l_depr_rate := round(l_depr_rate, 5);
         l_yr1_rate  := round(l_yr1_rate, 5);
         l_yr2_rate  := round(l_yr2_rate, 5);

         --* dont scoop remaining life plan stuff if the depreciation rate is still alive
         --* otherwise let the scoop decision stand as before.

         if (l_depr_rate <> 0) then
            l_scoop := false;
         end if;
      end if; --* rlife = 1

      --*
      --* establish tax limitations (e.g. luxury auto if applicable)
      --*
      l_tax_limitation := -1;

      if (a_tax_limitation_rows <> 0) then
         /*dw_tax_limitation.setsort('year a')
         dw_tax_limitation.sort() */

         i := trunc(l_tax_year) - l_vintage + 1;

         if (i > a_tax_limitation_rows or i < 1) then
            l_tax_limitation := -1;
         else
            l_tax_limitation := nvl(a_tax_limitation_sub_rec(i).limitation, 0) *
                                (l_short_months / 12.0);
            l_compare_rate   := nvl(a_tax_limitation_sub_rec(i).compare_rate, 0);
            if (l_compare_rate != 1) then
               l_compare_rate := 0;
            end if;
         end if;
      end if;

      --* 9/11/2001 job creation.
      --* Make the Reconciling Item exist, and make sure it has the right include_id

      l_jrow                 := 0;
      l_job_creation         := -1;
      l_job_creation_percent := 0;

      --trid :=   dw_tax_depr.getitemnumber(1,'tax_record_id')
      --nyear := get_next_year(tax_year,tax_depr->tax_record_id); //select min(tax_year) into :nyear  from tax_depreciation where tax_year > :tax_year and tax_record_id =  :trid;

      -- Zero out any activity in calculated basis difference before starting
      for j in 1 .. a_tax_reconcile_rows
      loop
         if (a_tax_reconcile_sub_rec(j).calced = 1) then
            a_tax_reconcile_sub_rec(j).basis_amount_activity := 0;
         end if;
      end loop;

      if (a_tax_job_creation_rows != 0) then
         -- sort tax_job_creation
         l_tax_job_creation_ordered := type_tax_limit2_rec();
         l_tax_job_creation_ordered.extend(a_tax_job_creation_rows);
         for j in 1 .. a_tax_job_creation_rows
         loop
            l_pos := 1;
            for k in 1 .. a_tax_job_creation_rows
            loop
               if (a_tax_job_creation_sub_rec(j).ordering > a_tax_job_creation_sub_rec(k).ordering) then
                  l_pos := l_pos + 1;
               end if;
            end loop;
            l_tax_job_creation_ordered(l_pos) := a_tax_job_creation_sub_rec(j);
         end loop;

         l_tax_credit_base     := 0;
         l_job_creation_amount := 0;
         for l_jc in 1 .. a_tax_job_creation_rows
         loop
            l_job_creation            := l_tax_job_creation_ordered(l_jc).reconcile_item_id;
            l_job_creation_percent    := l_tax_job_creation_ordered(l_jc).tax_credit_percent;
            l_basis_reduction_percent := l_tax_job_creation_ordered(l_jc).basis_reduction_percent;
            l_calc_future_years       := l_tax_job_creation_ordered(l_jc).calc_future_years;
            l_calced                  := l_tax_job_creation_ordered(l_jc).calced;
            l_depr_deduction          := l_tax_job_creation_ordered(l_jc).depr_deduction;
            if (l_calc_future_years is null) then
               -- is null
               l_calc_future_years := 1;
            end if;
            l_tax_credit_amount := 0;
            -- first time through loop - tax_credit_base = book additions and basis difference activity for items not calced
            if (l_jc = 1) then
               for j in 1 .. a_tax_book_activity_rows
               loop
                  -- Maint-10475
                  --if(tax_book_activity[j]->tax_activity_code_id == 1)
                  if (nvl(a_tax_book_activity_sub_rec(j).tax_activity_type_id, 1) = 1) then
                     l_tax_credit_base := l_tax_credit_base + a_tax_book_activity_sub_rec(j).amount;
                  end if;
               end loop;
               for j in 1 .. a_tax_reconcile_rows
               loop
                  if (a_tax_reconcile_sub_rec(j).calced = 0) then
                     l_tax_credit_base := l_tax_credit_base + a_tax_reconcile_sub_rec(j)
                                         .basis_amount_activity;
                  end if;
               end loop;
            end if;
            l_tax_credit_amount := l_tax_credit_base * l_job_creation_percent;
            if (l_number_months_beg != 0) then
               l_tax_credit_amount := l_tax_credit_amount * l_calc_future_years;
            end if;
            l_tax_credit_basis_reduction := -l_tax_credit_amount * l_basis_reduction_percent;
            l_tax_credit_base            := l_tax_credit_base + l_tax_credit_basis_reduction;
            l_job_creation_amount        := l_job_creation_amount +
                                            (l_tax_credit_amount * l_depr_deduction);

            l_jrow := -1;
            -- find reconcile_item_id
            for j in 1 .. a_tax_reconcile_rows
            loop
               if (a_tax_reconcile_sub_rec(j).reconcile_item_id = l_job_creation)  and
                   a_tax_reconcile_sub_rec(j).tax_include_id = l_tax_job_creation_ordered(l_jc).tax_include_id then
                  l_jrow := j;
               end if;
            end loop;
            if (l_jrow = -1) then
               -- No row found
               l_jrow := 1;

               a_add_tax_reconcile_rec.extend;
               j := a_add_tax_reconcile_rec.count;
               a_add_tax_reconcile_rec(j).reconcile_item_id := l_job_creation;
               a_add_tax_reconcile_rec(j).tax_record_id := g_tax_depr_rec(a_tax_depr_index)
                                                           .tax_record_id;
               a_add_tax_reconcile_rec(j).tax_year := l_tax_year;
               a_add_tax_reconcile_rec(j).calced := l_calced;
               a_add_tax_reconcile_rec(j).depr_deduction := l_depr_deduction;
               a_add_tax_reconcile_rec(j).basis_amount_activity := l_tax_credit_basis_reduction;
               a_add_tax_reconcile_rec(j).basis_amount_end := 0;
               a_add_tax_reconcile_rec(j).basis_amount_beg := 0;
               a_add_tax_reconcile_rec(j).basis_amount_transfer := 0;
               a_add_tax_reconcile_rec(j).tax_include_id := l_tax_job_creation_ordered(l_jc)
                                                            .tax_include_id;
               a_add_tax_reconcile_rows := a_add_tax_reconcile_rows + 1;

               --tax_reconcile[tax_reconcile_rows]->basis_amount_transfer := 0;
               --tax_reconcile[tax_reconcile_rows]->input_retire_ind := 0;
               --tax_reconcile[tax_reconcile_rows]->depr_deduction := dl_epr_deduction;
               --tax_reconcile[tax_reconcile_rows]->tax_include_id := l_tax_job_creation_ordered[jc]->tax_include_id;

               g_tax_reconcile_rec.extend;
               l_rows := g_tax_reconcile_rec.count;
               g_tax_reconcile_rec(l_rows) := a_add_tax_reconcile_rec(j);
               l_code := add_tax_reconcile( a_add_tax_reconcile_rec(j).tax_record_id,
                                  a_add_tax_reconcile_rec(j).tax_include_id,
                                  a_add_tax_reconcile_rec(j).tax_year,
                                  a_add_tax_reconcile_rec(j).reconcile_item_id ,
                                  l_rows);
               -- add record to hash for tax_reconcile and tax_reconcile_bal

               -----Add Basis difference to current year sub rec
               a_tax_reconcile_sub_rec.extend;
               j := a_tax_reconcile_sub_rec.count;
               a_tax_reconcile_sub_rec(j).reconcile_item_id := l_job_creation;
               a_tax_reconcile_sub_rec(j).tax_record_id := g_tax_depr_rec(a_tax_depr_index)
                                                           .tax_record_id;
               a_tax_reconcile_sub_rec(j).tax_year := l_tax_year;
               a_tax_reconcile_sub_rec(j).calced := l_calced;
               a_tax_reconcile_sub_rec(j).depr_deduction := l_depr_deduction;
               a_tax_reconcile_sub_rec(j).basis_amount_activity := l_tax_credit_basis_reduction;
               a_tax_reconcile_sub_rec(j).basis_amount_end := 0;
               a_tax_reconcile_sub_rec(j).basis_amount_beg := 0;
               a_tax_reconcile_sub_rec(j).basis_amount_transfer := 0;
               a_tax_reconcile_sub_rec(j).tax_include_id := l_tax_job_creation_ordered(l_jc)
                                                            .tax_include_id;
               a_tax_reconcile_rows := a_tax_reconcile_rows + 1;

            else
               --a_tax_reconcile_sub_rec.extend;
               a_tax_reconcile_sub_rec(l_jrow).basis_amount_activity := l_tax_credit_basis_reduction;
            end if;

            if (a_nyear <> 0) then
               --if not isnull(nyear) then
               l_found := false;
               for j in 1 .. a_reconcile_bals_rows
               loop
                  if (a_reconcile_bals_sub_rec(j).reconcile_item_id = l_job_creation) and
                      a_reconcile_bals_sub_rec(j).tax_include_id = l_tax_job_creation_ordered(l_jc).tax_include_id then
                     l_found := true;
                     l_jrow  := j;
                  end if;
               end loop;
               --format := 'reconcile_item_id = ' + string(job_creation)
               --jrow := dw_reconcile_bals.find(fformat,1,dw_reconcile_bals.rowcount())
               if (l_found = false) then
                  --  jrow = 0 then

                  a_add_tax_reconcile_rec.extend;
                  j := a_add_tax_reconcile_rec.count;
                  a_add_tax_reconcile_rec(j).reconcile_item_id := l_job_creation;
                  a_add_tax_reconcile_rec(j).tax_record_id := g_tax_depr_rec(a_tax_depr_index)
                                                              .tax_record_id;
                  a_add_tax_reconcile_rec(j).tax_year := a_nyear;
                  a_add_tax_reconcile_rec(j).tax_include_id := l_tax_job_creation_ordered(l_jc)
                                                               .tax_include_id;
                  a_add_tax_reconcile_rec(j).basis_amount_end := 0;
                  a_add_tax_reconcile_rec(j).basis_amount_activity := 0;
                  a_add_tax_reconcile_rec(j).basis_amount_beg := 0;
                  a_add_tax_reconcile_rec(j).basis_amount_transfer := 0;
                  a_add_tax_reconcile_rec(j).calced := l_calced;
                  a_add_tax_reconcile_rec(j).depr_deduction := l_depr_deduction;
                  a_add_tax_reconcile_rows := a_add_tax_reconcile_rows + 1;
                  l_add_tax_reconcile_bals_flag := true;

                  a_reconcile_bals_sub_rec.extend;
                  a_reconcile_bals_rows := a_reconcile_bals_sub_rec.count;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).reconcile_item_id := l_job_creation;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).tax_record_id := g_tax_depr_rec(a_tax_depr_index)
                                                              .tax_record_id;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).tax_year := a_nyear;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).tax_include_id := l_tax_job_creation_ordered(l_jc)
                                                               .tax_include_id;

                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).basis_amount_end := 0;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).basis_amount_activity := 0;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).basis_amount_beg := 0;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).basis_amount_transfer := 0;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).calced := l_calced;
                  a_reconcile_bals_sub_rec(a_reconcile_bals_rows).depr_deduction := l_depr_deduction;

                  g_tax_reconcile_rec.extend;
                  l_rows := g_tax_reconcile_rec.count;
                  g_tax_reconcile_rec(l_rows) := a_add_tax_reconcile_rec(j);
                  l_code := add_tax_reconcile( a_add_tax_reconcile_rec(j).tax_record_id,
                                  a_add_tax_reconcile_rec(j).tax_include_id,
                                  a_add_tax_reconcile_rec(j).tax_year,
                                  a_add_tax_reconcile_rec(j).reconcile_item_id ,
                                  l_rows);
               end if;

            end if;

         end loop;

         -- if an account does not point to a job creation reconciling item, then make sure the amount is 0 this year.
         -- Must hardcode the reconcile item id

         --if(tax_job_creation_rows == 0 )
         --  {
         --    /*fformat = 'reconcile_item_id = -99'  */
         --    /*jrow = dw_tax_reconcile.find(fformat,1,dw_tax_Reconcile.rowcount()) */
         --     found = FALSE;
         --    for(j = 0; j < tax_reconcile_rows;++j)
         --    {
         --        if(tax_reconcile[j]->reconcile_item_id == -99)
         --      {
         --             found := TRUE;
         --       jrow := j;
         --      }
         --    }
         --     if(found == TRUE)
         --    tax_reconcile[jrow]->basis_amount_activity := 0;

      end if; /*  dw_tax_job_creation.rowcount() = 0 */

      --*
      --* process current year activity:
      --*
      if l_quantity is null then
         l_quantity := 0;
      end if;
      if l_quantity_transfer is null then
         l_quantity_transfer := 0;
      end if;

      --*
      --* get current year activity: book retirements, additions and adjustments
      --*
      -- Maint-10475
      l_first_year_retire    := false;
      l_ex_first_year_retire := false;

      for i in 1 .. a_tax_book_activity_rows
      loop
         l_amount := nvl(a_tax_book_activity_sub_rec(i).amount, 0);
         -- Maint-10475
         -- j := a_tax_book_activity_sub_rec(i).tax_activity_code_id;
         j := nvl(a_tax_book_activity_sub_rec(i).tax_activity_type_id, 1);
         if a_tax_book_activity_sub_rec(i).tax_activity_code_id = 10 then
            if j = 4 then
               l_ex_first_year_retire := true;
            else
               l_first_year_retire := true;
            end if;
         end if;
         case --* type of activity
            when j = 1 or j = 5 or j = 6 or j = 7 or j = 8 then
               l_book_addition := l_book_addition + l_amount;
            when j = 2 or j = 12 then
               l_book_retirement := l_book_retirement + l_amount;
            when j = 3 then
               l_book_adjustment := l_book_adjustment + l_amount;
            when j = 4 then
               l_book_extraord_retirement := l_book_extraord_retirement + l_amount;
         end case;
      end loop;

      --*
      --* put the book to tax basis reconciliation activity in an array and also
      --* put the beginning tax to book differences in an array. pay attention to sign
      --* book difference is negative, tax difference is positive
      --*


      for i in 1 .. a_tax_reconcile_rows
      loop
         l_tax_reconcile_beg(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_beg,0) + nvl(a_tax_reconcile_sub_rec(i)
                                   .basis_amount_transfer,0);
         l_tax_reconcile_act(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_activity,0);
         l_tax_reconcile_ret(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_input_retire,0);
         l_tax_reconcile_transfer(i) := nvl(a_tax_reconcile_sub_rec(i).basis_amount_transfer,0);
         l_input_retire_ind(i) := nvl(a_tax_reconcile_sub_rec(i).input_retire_ind,0);

         if (l_input_retire_ind(i) != 0) then
            l_input_retire_ind(i) := 1;
         end if;

         if instr(upper(nvl(a_tax_reconcile_sub_rec(i).reconcile_item_type, ' ')), 'CREDIT') = 0 then
            l_tax_reconcile_type(i) := 1;
         else
            l_tax_reconcile_type(i) := 0;
         end if;
         /*    if(tax_reconcile[j]->basis_amount_beg_isnull == 1)
                tax_reconcile_beg[i] = 0;
              done while retrieving data from database
         */

      /*    if(tax_reconcile[j]->BASIS_AMOUNT_ACTIVITY.IsNull then
                         tax_reconcile_act[i] = 0;
                  */

      end loop;

      --*
      --* Determine Tax Basis Additions.  If any tax book reconciling items are input,
      --* treat as adjustments if there are no book additions
      --*

      l_additions             := l_book_addition;
      l_adjustments           := l_book_adjustment;
      l_retirements           := l_book_retirement;
      l_extraordinary_retires := l_book_extraord_retirement;

      i := a_tax_reconcile_rows; /*upperbound(l_tax_reconcile_beg) */

      for j in 1 .. i
      loop
         -- if(fabs(book_addition) > 1)
         l_additions := l_additions + l_tax_reconcile_act(j);
         --   else
      --       adjustments := adjustments + tax_reconcile_act[j - 1];
      end loop;

      --* Process the Book Adjustments by Ratioing Through the Tax to Book differences.

      if (l_book_balance != 0.0) then
         l_ratio := l_book_adjustment / l_book_balance;
      else
         l_ratio := 0;
      end if;

      for j in 1 .. i
      loop
         l_change := l_tax_reconcile_beg(j) * l_ratio;

         l_tax_reconcile_end(j) := l_tax_reconcile_beg(j) + l_change + l_tax_reconcile_act(j);

         l_adjustments := l_adjustments + l_change;
      end loop;

      --* Process the Book Retirements by Ratioing Through
      --* the Tax to Book Differences,
      --* and Determine Tax Basis Retirement.

      l_ratio1 := (l_book_retirement + l_book_extraord_retirement);
      if (l_ratio1 != 0) then
         l_ratio1 := l_book_retirement / (l_book_retirement + l_book_extraord_retirement);
      end if;

      l_ratio := (l_book_balance + l_book_addition + l_book_adjustment);
      if (l_ratio != 0) then

         l_ratio := (l_book_retirement + l_book_extraord_retirement) /
                    (l_book_balance + l_book_addition + l_book_adjustment);
      else
         if (round(l_tax_balance, 2) <> 0) then
            l_ratio                    := (l_book_retirement + l_book_extraord_retirement) /
                                          l_tax_balance;
            l_book_retirement          := 0;
            l_book_extraord_retirement := 0;
            l_retirements              := 0;
            l_extraordinary_retires    := 0;
         else
            l_ratio := 0;
         end if;
      end if;

      l_cap_gain_retirements    := l_retirements;
      l_cap_gain_ex_retirements := l_extraordinary_retires;

      if (l_ratio != 0) then
         for j in 1 .. i
         loop
            l_change := (l_tax_reconcile_end(j) * l_ratio * (1 - l_input_retire_ind(j))) +
                        l_tax_reconcile_ret(j);
            l_tax_reconcile_end(j) := l_tax_reconcile_end(j) - l_change;
            l_retirements := l_retirements + (l_change * l_ratio1);
            l_cap_gain_retirements := l_cap_gain_retirements +
                                      (l_change * l_ratio1 * l_tax_reconcile_type(j));
            l_extraordinary_retires := l_extraordinary_retires + (l_change * (1 - l_ratio1));
            l_cap_gain_ex_retirements := l_cap_gain_ex_retirements +
                                         (l_change * (1 - l_ratio1) * l_tax_reconcile_type(j));
         end loop;
      end if;

      --*
      --* Determine Ending Book Basis Balance
      --*

      l_book_balance_end := l_book_balance + l_book_addition + l_book_adjustment -
                            l_book_retirement - l_book_extraord_retirement + l_book_balance_adjust;

      --* Set The Retirement Depreciation Convention

      case trunc(l_retire_depr_conv)
         when 1 then
            l_retire_depr_ratio := 0;
         when 2 then
            l_retire_depr_ratio := 0.5;
         when 3 then
            l_retire_depr_ratio := 0.5;
         when 4 then
            l_retire_depr_ratio := 1;
      end case;

      if (l_retire_depr_ratio <> 1 and trunc(l_tax_year) = l_vintage) then
         l_retire_depr_ratio := 0; --* First year retirements have 0 depr.
         l_first_year_retire := true; -- Maint-10475
      end if;

      -- Maint-10475 - Make Last year of recovery completely depreciation for retire depr conv = 3
      if l_retire_depr_conv = 3 and l_scoop = true then
         l_retire_depr_ratio := 1;
      end if;

      --* Set The Retirement balance Convention

      case
         when trunc(l_retire_bal_conv) = 1 or trunc(l_retire_bal_conv) = 3 or
              trunc(l_retire_bal_conv) = 4 then
            l_retire_bal_ratio := 0;
         when trunc(l_retire_bal_conv) = 2 then
            l_retire_bal_ratio := 1;
      end case;

      -- If an ordinary or extraordinary retirement finishes off the tax balance, and
      -- this is an ADR-type convention for retirements, then scoop

      -- Maint - 10475 commented out accum_ordinary retires
      if (((l_retire_bal_conv = 3 or l_retire_bal_conv = 4 /*or l_accum_ordinary_retires <> 0 */
         ) and
         round(l_tax_balance - l_retirements - l_extraordinary_retires - l_accum_ordinary_retires,
                 0) <= 0 and l_tax_balance > 0) or
         ((l_retire_bal_conv = 3 or l_retire_bal_conv = 4 /* or l_accum_ordinary_retires <> 0  */
         ) and
         round(l_tax_balance - l_retirements - l_extraordinary_retires - l_accum_ordinary_retires,
                 0) >= 0 and l_tax_balance < 0)) then
         l_scoop := true;
      end if;

      --* Set The Retirement Reserve Convention

      case
         when trunc(l_retire_res_conv) = 1 then
            l_retire_res_ratio := 0;
         when trunc(l_retire_res_conv) = 2 or round(l_retire_res_conv) = 4 then
            if ((l_tax_balance + l_additions) <> 0) then

               l_retire_res_ratio := (l_accum_reserve - (l_accum_salvage * l_salvage_ratio) +
                                     l_accum_reserve_adjust * l_accum_reserve_adjust_method) /
                                     (l_tax_balance + l_additions);
            else
               l_retire_res_ratio := 0;
            end if;
         when trunc(l_retire_res_conv) = 3 then
            l_retire_res_ratio := 1;
      end case;

      --* Set The Salvage Convention

      case
         when trunc(l_salvage_conv) = 1 then
            l_salvage_ratio      := 0;
            l_beg_salvage_ratio  := 0;
            l_depr_salvage_ratio := 0;
         when trunc(l_salvage_conv) = 2 then
            l_salvage_ratio      := 1;
            l_beg_salvage_ratio  := 1;
            l_depr_salvage_ratio := 0;
         when trunc(l_salvage_conv) = 3 then
            l_salvage_ratio      := 1;
            l_beg_salvage_ratio  := 0;
            l_depr_salvage_ratio := 0;
         when trunc(l_salvage_conv) = 4 then
            l_salvage_ratio      := 1;
            l_beg_salvage_ratio  := 1;
            l_depr_salvage_ratio := 1;
         when trunc(l_salvage_conv) = 5 then
            l_salvage_ratio      := 1;
            l_beg_salvage_ratio  := .5;
            l_depr_salvage_ratio := 0;
            l_add_ratio          := .5;

      end case;

      -- Set The cost of removal Convention

      case
         when trunc(l_cost_of_removal_conv) = 1 then
            l_cor_ratio := 0;
         when trunc(l_cost_of_removal_conv) = 2 then
            l_cor_ratio := 1;
      end case;

      case
         when round(l_est_salvage_conv) = 4 then
            l_adr_salvage_ratio := 0;

         else
            l_adr_salvage_ratio := 1;
      end case;
      --* Set The Retirement Depreciation Convention

      case
         when trunc(l_ex_retire_depr_conv) = 1 then
            l_ex_retire_depr_ratio := 0;
         when trunc(l_ex_retire_depr_conv) = 2 then
            l_ex_retire_depr_ratio := 0.5;
         when trunc(l_ex_retire_depr_conv) = 3 then
            l_ex_retire_depr_ratio := 0.5;
         when trunc(l_ex_retire_depr_conv) = 4 then
            l_ex_retire_depr_ratio := 1;
      end case;

      if (l_ex_retire_depr_ratio <> 1 and l_tax_year = l_vintage) then
         l_ex_retire_depr_ratio := 0; -- First year retirements have 0 depr.
         l_ex_first_year_retire := true; -- Maint-10475
      end if;

      -- Maint-10475
      if l_ex_retire_bal_conv = 3 and l_scoop = true then
         l_ex_retire_depr_ratio := 1;
      end if;
      --* Set The Retirement balance Convention

      case
         when trunc(l_ex_retire_bal_conv) = 1 or trunc(l_ex_retire_bal_conv) = 3 or
              trunc(l_ex_retire_bal_conv) = 4 then
            l_ex_retire_bal_ratio := 0;
         when trunc(l_ex_retire_bal_conv) = 2 then
            l_ex_retire_bal_ratio := 1;
      end case;

      --* Set The Retirement Reserve Convention

      case
         when trunc(l_ex_retire_res_conv) = 1 then
            l_ex_retire_res_ratio := 0;
         when trunc(l_ex_retire_res_conv) = 2 or trunc(l_ex_retire_res_conv) = 4 then
            if ((l_tax_balance + l_additions) != 0) then
               l_ex_retire_res_ratio := (l_accum_reserve - (l_accum_salvage * l_salvage_ratio) +
                                        l_accum_reserve_adjust * l_accum_reserve_adjust_method) /
                                        (l_tax_balance + l_additions);
            else
               l_ex_retire_res_ratio := 0;
            end if;
         when trunc(l_ex_retire_res_conv) = 3 then
            l_ex_retire_res_ratio := 1;
      end case;

      --* Set The Salvage Convention

      case
         when trunc(l_ex_salvage_conv) = 1 then
            l_ex_salvage_ratio      := 0;
            l_ex_beg_salvage_ratio  := 0;
            l_ex_depr_salvage_ratio := 0;
         when trunc(l_ex_salvage_conv) = 2 then
            l_ex_salvage_ratio      := 1;
            l_ex_beg_salvage_ratio  := 1;
            l_ex_depr_salvage_ratio := 0;
         when trunc(l_ex_salvage_conv) = 3 then
            l_ex_salvage_ratio      := 1;
            l_ex_beg_salvage_ratio  := 0;
            l_ex_depr_salvage_ratio := 0;
         when trunc(l_ex_salvage_conv) = 4 then
            l_ex_salvage_ratio      := 1;
            l_ex_beg_salvage_ratio  := 1;
            l_ex_depr_salvage_ratio := 1;
      end case;

      case
         when trunc(l_book_balance_adjust_method) = 1 then
            l_book_balance_adjust_method := 1;
         when trunc(l_book_balance_adjust_method) = 2 then
            l_book_balance_adjust_method := 0.5;
         when trunc(l_book_balance_adjust_method) = 3 then
            l_book_balance_adjust_method := 0;
         else
            null;
      end case;
      /*
      switch((int)accum_reserve_adjust_method)
          {
          case 1:
               accum_reserve_adjust_method = 1;
               break;
      /*    case 2
              accum_reserve_adjust_method = .5
              break; *
            default:
               accum_reserve_adjust_method = 0;
               break;
          };*/

      if (l_accum_reserve_adjust_method = 1) then
         l_accum_reserve_adjust_method := 1;
         /*else if(accum_reserve_adjust_method == 2)
         accum_reserve_adjust_method = .5; */
      else
         l_accum_reserve_adjust_method := 0;
      end if;
      /*
      switch((int)depreciable_base_adjust_method)
          {
          case 1:
               depreciable_base_adjust_method := 1;
               break;
          case 2:
               depreciable_base_adjust_method := 1 ; /*.5;*
               break;
          case 3:
               depreciable_base_adjust_method := 0;
               break;
          }; */
      if (l_depreciable_base_adjust_meth = 1) then
         l_depreciable_base_adjust_meth := 1;
      elsif (l_depreciable_base_adjust_meth = 2) then
         l_depreciable_base_adjust_meth := .5;
      elsif (l_depreciable_base_adjust_meth = 3) then
         l_depreciable_base_adjust_meth := 0;
      end if;

      --* If cnstant dollar-amount estimated salvage, than calculate an estimated salvage percent
      if ((l_est_salvage_conv = 5 or l_est_salvage_conv = 6) and l_estimated_salvage <> 0) then
         if (abs(l_tax_balance + l_additions + l_adjustments - l_retirements -
                 l_extraordinary_retires - l_accum_ordinary_retires) < 0.0001) then
            l_est_salvage_pct := 0;
         else
            l_est_salvage_pct := (l_estimated_salvage -
                                 (l_accum_salvage + l_actual_salvage + l_salvage_extraord)) /
                                 (l_tax_balance + l_additions + l_adjustments - l_retirements -
                                 l_extraordinary_retires - l_accum_ordinary_retires);
         end if;
         if ((l_est_salvage_pct * l_estimated_salvage) <= 0) then
            l_est_salvage_pct := 0;
         end if;
      end if;

      l_begin_res_impact := l_retirements * (l_retire_res_ratio);

      l_ex_begin_res_impact := l_extraordinary_retires * (l_ex_retire_res_ratio);

      if (l_net = 'GROSS') then
         l_retirement_depr    := l_retirements * l_retire_depr_ratio * l_depr_rate;
         l_ex_retirement_depr := l_extraordinary_retires * l_ex_retire_depr_ratio * l_depr_rate;
      else
         l_retirement_depr    := (l_retirements - l_begin_res_impact) * l_retire_depr_ratio *
                                 l_depr_rate;
         l_ex_retirement_depr := (l_extraordinary_retires - l_ex_begin_res_impact) *
                                 l_ex_retire_depr_ratio * l_depr_rate;
      end if;

      -- retire_res_impact := begin_res_impact + ex_begin_res_impact;

      if (l_retire_res_ratio <> 0) then
         if (abs(l_begin_res_impact + l_retirement_depr) > abs(l_retirements) and
            l_retire_res_conv <> 4) then
            l_retire_res_impact := l_retirements;
         else
            l_retire_res_impact := l_begin_res_impact + l_retirement_depr;
         end if;
      end if;

      if (l_ex_retire_res_ratio <> 0) then
         if (abs(l_ex_begin_res_impact + l_ex_retirement_depr) > abs(l_extraordinary_retires) and
            l_ex_retire_res_conv <> 4) then
            l_ex_retire_res_impact := l_extraordinary_retires;
         else
            l_ex_retire_res_impact := l_ex_begin_res_impact + l_ex_retirement_depr;
         end if;
      end if;

      l_retire_res_impact := l_retire_res_impact + l_ex_retire_res_impact;

      l_depreciable_base := l_tax_balance + l_adjustments * l_book_balance_adjust_method +
                            l_additions * l_add_ratio - l_retirements - l_extraordinary_retires +
                            (l_retirements * l_retire_depr_ratio) +
                            (l_extraordinary_retires * l_ex_retire_depr_ratio) +
                            (l_depreciable_base_adjust * l_depreciable_base_adjust_meth);

      if (l_tax_balance <> 0) then
         l_depreciable_base := l_depreciable_base -
                               (l_reserve_at_switch * l_depreciable_base / l_tax_balance);
      end if;

      if ((l_accum_reserve - (l_accum_salvage * l_salvage_ratio)) = 0.0) then
         l_reserve_at_switch_end := 0;
      else
         l_reserve_at_switch_end := l_reserve_at_switch -
                                    ((l_reserve_at_switch /
                                    (l_accum_reserve - (l_accum_salvage * l_salvage_ratio))) *
                                    (l_begin_res_impact + l_ex_begin_res_impact));
      end if;

      --* Estimated Salvage Reduces Depreciable Base Election

      if (l_est_salvage_conv = 2 or l_est_salvage_conv = 6) then
         l_depreciable_base := l_depreciable_base * (1 - l_est_salvage_pct);
      end if;

      --* Calculate the salvage impact on the beginning reserve
      l_salvage_res_impact := l_actual_salvage * l_salvage_ratio * l_beg_salvage_ratio +
                              l_salvage_extraord * l_ex_salvage_ratio * l_ex_beg_salvage_ratio;

      l_cor_res_impact := l_cost_of_removal * l_cor_ratio;

      l_short_year_net_adjust := ((l_tax_balance - l_reserve_at_switch - l_accum_reserve) *
                                 l_yr1_rate * l_months_used / 12) /
                                 (1 - (l_yr1_rate * l_months_used / 12));

      if (l_net = 'NET') then
         l_reserve := l_accum_reserve + l_salvage_res_impact - l_cor_res_impact -
                      (l_begin_res_impact * (1 - l_retire_depr_ratio)) -
                      (l_ex_begin_res_impact * (1 - l_ex_retire_depr_ratio)) +
                      (l_accum_reserve_adjust * l_accum_reserve_adjust_method) -
                      l_short_year_net_adjust;
      end if;

      --* Re-Calculate the salvage impact on the ending reserve

      l_salvage_res_impact := (l_actual_salvage * l_salvage_ratio) +
                              (l_salvage_extraord * l_ex_salvage_ratio);

      if ((l_reserve * l_depreciable_base) >= 0 and abs(l_reserve) >= abs(l_depreciable_base)) then
         l_depreciable_base := 0;
      else
         if (l_net = 'NET') then
            l_depreciable_base := l_depreciable_base - l_reserve +
                                  (l_accum_salvage + l_salvage_res_impact) *
                                  (l_depr_salvage_ratio * l_salvage_ratio);
         end if;
      end if;

      if (l_fixed_depreciable_base <> 0) then
         l_depreciable_base := l_fixed_depreciable_base;
      end if;

      if (l_net = 'NET') then
         l_calc_depreciation := l_depreciable_base * l_yr1_rate * l_yr1_fraction;
         l_depreciable_base  := l_depreciable_base - l_short_year_net_adjust - l_calc_depreciation;
         l_calc_depreciation := l_calc_depreciation +
                                (l_depreciable_base * l_yr2_rate * l_yr2_fraction);
         if ((l_yr1_rate * l_yr1_fraction) + (l_yr2_rate * l_yr2_fraction)) <> 0 then
            -- div by 0 check
            l_depreciable_base := l_calc_depreciation /
                                  ((l_yr1_rate * l_yr1_fraction) + (l_yr2_rate * l_yr2_fraction));
         end if;
      else
         l_calc_depreciation := l_depreciable_base * l_depr_rate;
      end if;
      --*
      --* Check tax limitation e.g. luxury autos
      --*

      if (l_tax_limitation != -1 and l_quantity > 0) then
         --  if( (l_quantity * tax_limitation) < l_calc_depreciation)
         if (l_compare_rate = 1) then
            if ((l_quantity * l_tax_limitation) < l_calc_depreciation) then
               l_calc_depreciation := l_quantity * l_tax_limitation;
            end if;
         else
            l_calc_depreciation := l_quantity * l_tax_limitation;
         end if;
      end if;

      --*
      --* Determine Ending Tax Basis Balance and Other Balances
      --*

      l_tax_balance_end := l_tax_balance + l_additions + l_adjustments -
                           (l_retirements * l_retire_bal_ratio) -
                           (l_extraordinary_retires * l_ex_retire_bal_ratio);

      l_accum_ordin_retires_end := l_accum_ordinary_retires +
                                   (l_retirements * (1 - l_retire_bal_ratio)) +
                                   (l_extraordinary_retires * (1 - l_ex_retire_bal_ratio));

      l_accum_reserve_end := l_accum_reserve + l_calc_depreciation - l_retire_res_impact +
                             l_salvage_res_impact - l_cor_res_impact + l_accum_reserve_adjust +
                             l_depreciation_adjust;

      l_accum_reserve_end       := round(l_accum_reserve_end, 2);
      l_accum_ordin_retires_end := round(l_accum_ordin_retires_end, 2);
      l_tax_balance_end         := round(l_tax_balance_end, 2);

      l_quantity_end := l_quantity + l_quantity_transfer;

      if ((l_accum_reserve_end * (l_tax_balance_end - (l_tax_balance_end - (l_accum_ordin_retires_end *
         l_adr_salvage_ratio)) * l_est_salvage_pct)) >= 0 and
         abs(l_accum_reserve_end) >=
         (abs(l_tax_balance_end -
               ((l_tax_balance_end - (l_accum_ordin_retires_end * l_adr_salvage_ratio)) *
               l_est_salvage_pct)))) then
         l_over_adj_depreciation := l_accum_reserve_end -
                                    (l_tax_balance_end - ((l_tax_balance_end - (l_accum_ordin_retires_end *
                                    l_adr_salvage_ratio)) * l_est_salvage_pct));

         if (abs(l_over_adj_depreciation) > abs(l_calc_depreciation + l_depreciation_adjust)) then
            l_over_adj_depreciation := round(l_calc_depreciation + l_depreciation_adjust, 2);
            if (l_est_salvage_pct >= 0) then
               if (abs(l_accum_reserve_end - l_over_adj_depreciation) > abs(l_tax_balance_end)) then
                  l_gain_loss := l_accum_reserve_end - l_over_adj_depreciation - l_tax_balance_end;
                  if (l_gain_loss > l_salvage_res_impact) then
                     /*  l_accum_reserve_adjust := l_accum_reserve_adjust - (l_gain_loss - l_salvage_res_impact);
                     l_salvage_res_impact := 0; */
                     l_over_adj_depreciation := l_over_adj_depreciation -
                                                (l_salvage_res_impact - l_gain_loss);
                     l_gain_loss             := l_salvage_res_impact;
                     l_salvage_res_impact    := 0;
                  else
                     l_salvage_res_impact := l_salvage_res_impact - l_gain_loss;
                  end if;
               end if;
            else
               --* est_salvage_pct < 0
               if (abs(l_accum_reserve_end - l_over_adj_depreciation) >
                  abs(l_tax_balance_end * (1 - l_est_salvage_pct))) then
                  l_gain_loss := l_accum_reserve_end - l_over_adj_depreciation -
                                 (l_tax_balance_end * (1 - l_est_salvage_pct));
                  if (l_gain_loss > l_salvage_res_impact) then
                     /*  accum_reserve_adjust := l_accum_reserve_adjust - (l_gain_loss - l_salvage_res_impact);
                     l_salvage_res_impact := 0; */
                     l_over_adj_depreciation := l_over_adj_depreciation -
                                                (l_salvage_res_impact - l_gain_loss);
                     l_gain_loss             := l_salvage_res_impact;
                     l_salvage_res_impact    := 0;
                  else
                     l_salvage_res_impact := l_salvage_res_impact - l_gain_loss;
                  end if;
               end if;
            end if; --* est_salvage_pct >=  0
         end if;
         l_accum_reserve_end := l_accum_reserve_end - l_gain_loss - l_over_adj_depreciation;
         l_scoop             := false;
      end if;

      if (l_scoop = true) then
         --* scoop rest of depreciation in the last year of depreciation
         l_amount := (l_tax_balance_end -
                     (l_tax_balance_end - (l_accum_ordin_retires_end * l_adr_salvage_ratio)) *
                     l_est_salvage_pct) - l_accum_reserve_end;

         l_accum_reserve_end := l_accum_reserve_end + l_amount;

         l_over_adj_depreciation := l_over_adj_depreciation - l_amount;

      end if; --* scoop
      if (l_retire_bal_conv = 4) then
         if (abs(l_tax_balance_end - l_accum_reserve_end) < 0.1 and
            abs(l_tax_balance_end - l_accum_ordin_retires_end) < 0.1) then
            l_retire_res_impact       := l_retire_res_impact + l_accum_reserve_end;
            l_tax_balance_end         := 0;
            l_accum_reserve_end       := 0;
            l_accum_ordin_retires_end := 0;
         end if;
      end if;

      l_depreciation := l_calc_depreciation + l_depreciation_adjust - l_over_adj_depreciation;

      l_accum_salvage_end := (l_accum_salvage * l_salvage_ratio) + l_salvage_res_impact;

      if (l_rlife = 1 and l_life > 0) then
         l_sl_reserve_end := l_sl_reserve - ((l_tax_balance + l_tax_balance_end) / (l_life * 2));
      else
         l_sl_reserve_end := 0;
      end if;

      --if((tax_balance + additions) != 0) /* div by 0 check */
      --  {
      if (l_gain_loss_conv = 2) then
         --* On Asset
         l_gain_loss := l_gain_loss + l_actual_salvage -
                        (l_retirements - (l_retire_res_impact - l_ex_retire_res_impact));

         --  (((al_ccum_reserve + (l_accum_reserve_adjust * l_accum_reserve_adjust_method)
         --  - (l_accum_salvage * l_salvage_ratio )) / (l_tax_balance + l_additions)
         --     * l_retirements) + l_retirement_depr)) ;
         -- if(l_cap_gain_conv ==  3 l_actual_salvage - l_cap_gain_retirements > 0 )
         --     capital_gain_loss = actual_salvage - cap_gain_retirements;

         if ((l_cap_gain_conv = 3 or l_cap_gain_conv = 4 or l_cap_gain_conv = 5 or
            l_cap_gain_conv = 6) and (l_actual_salvage - l_cap_gain_retirements) > 0) then
            l_capital_gain_loss := l_actual_salvage - l_cap_gain_retirements;
         end if;
         if ((l_cap_gain_conv = 4 or l_cap_gain_conv = 5) and l_gain_loss > 0) then
            l_capital_gain_loss := (l_gain_loss - (l_gain_loss - l_capital_gain_loss) * .2);
         end if;
         if ((l_cap_gain_conv = 3 --* 1245 -  Loss Ordinary
            or l_cap_gain_conv = 4 --* 1250 Loss Ordinary
            ) and l_gain_loss < 0) then
            l_capital_gain_loss := 0;
         elsif ((l_cap_gain_conv = 5 --* 1245 - Loss Capital
               or l_cap_gain_conv = 6 --* 1250 Loss Capital
               ) and l_gain_loss < 0) then
            l_capital_gain_loss := l_gain_loss;
         end if;
         if l_first_year_retire = true then
            l_capital_gain_loss := 0; -- Maint 10475
         end if;
      end if;
      if (l_ex_gain_loss_conv = 2) then
         --* On Asset
         l_gain_loss    := l_gain_loss + l_salvage_extraord -
                           (l_extraordinary_retires - (l_ex_retire_res_impact)); -- - ex_retire_res_impact));
         l_ex_gain_loss := l_salvage_extraord -
                           (l_extraordinary_retires - (l_ex_retire_res_impact));

         --    (((l_accum_reserve + (l_accum_reserve_adjust * l_accum_reserve_adjust_method)
         --     - (accum_salvage * salvage_ratio )) / (l_tax_balance + l_additions)
         --   * extraordinary_retires) + ex_retirement_depr));
         if ((l_ex_cap_gain_conv = 3 or l_ex_cap_gain_conv = 4 or l_ex_cap_gain_conv = 5 or
            l_ex_cap_gain_conv = 6) and l_salvage_extraord - l_cap_gain_ex_retirements > 0) then
            l_ex_capital_gain_loss := l_capital_gain_loss + l_salvage_extraord -
                                      l_cap_gain_ex_retirements;
         end if;

         if ((l_ex_cap_gain_conv = 4 or l_ex_cap_gain_conv = 5) and l_ex_gain_loss > 0) then
            l_ex_capital_gain_loss := (l_ex_gain_loss -
                                      ((l_ex_gain_loss - l_ex_capital_gain_loss) * .2));
         end if;

         if ((l_ex_cap_gain_conv = 3 --* 1245 - Loss Ordinary
            or l_ex_cap_gain_conv = 4 --* 1250 Loss Ordinary
            ) and l_gain_loss < 0) then
            l_ex_capital_gain_loss := 0;
         elsif ((l_ex_cap_gain_conv = 5 --* 1245 - Loss Capital
               or l_ex_cap_gain_conv = 6 --* 1250 Loss Capital
               ) and l_gain_loss < 0) then
            l_ex_capital_gain_loss := l_ex_gain_loss;
         end if;
         if l_ex_first_year_retire = true then
            l_ex_capital_gain_loss := 0; -- Maint 10475, pt2
         end if;
      end if;
      -- }

      if (l_gain_loss_conv = 3) then
         --  Salvage = Gain
         l_gain_loss := l_gain_loss + l_actual_salvage;
      end if;

      if (l_ex_gain_loss_conv = 3) then
         -- Salvage = Gain
         l_gain_loss    := l_gain_loss + l_salvage_extraord;
         l_ex_gain_loss := l_salvage_extraord;
      end if;

      l_gain_loss := l_gain_loss + l_gain_loss_adjust;

      l_cor_expense := l_cost_of_removal * (1 - l_cor_ratio);

      if (l_cap_gain_conv = 1) then
         l_capital_gain_loss := l_gain_loss - l_ex_gain_loss;
      end if;

      if (l_ex_cap_gain_conv = 1) then
         l_ex_capital_gain_loss := l_ex_gain_loss;
      end if;

      l_capital_gain_loss := l_capital_gain_loss + l_ex_capital_gain_loss;
      l_tax_book_id       := g_tax_depr_rec(a_tax_depr_index).tax_book_id;
      l_tax_record_id     := g_tax_depr_rec(a_tax_depr_index).tax_record_id;
      l_tax_year_1        := l_tax_year + 1;
      for i in 1 .. a_tax_reconcile_rows
      loop
         if (abs(l_tax_reconcile_beg(i)) < .0000001) then
            l_tax_reconcile_beg(i) := 0;
         end if;
         -- tax_reconcile[i - 1]->basis_amount_beg   := l_tax_reconcile_beg(i);
         if (abs(l_tax_reconcile_act(i)) < .0000001) then
            l_tax_reconcile_act(i) := 0;
         end if;
         a_tax_reconcile_sub_rec(i).basis_amount_activity := l_tax_reconcile_act(i);
         if (abs(l_tax_reconcile_end(i)) < .0000001) then
            l_tax_reconcile_end(i) := 0;
         end if;
         a_tax_reconcile_sub_rec(i).basis_amount_end := l_tax_reconcile_end(i);
         if (abs(l_tax_reconcile_transfer(i)) < .0000001) then
            l_tax_reconcile_transfer(i) := 0;
         end if;
         a_tax_reconcile_sub_rec(i).basis_amount_transfer := l_tax_reconcile_transfer(i);
         /*if(reconcile_bals_rows >= i)
             {
         reconcile_bals[i - 1]->basis_amount_beg = l_tax_reconcile_end[i - 1];
             } */
         l_msg :=  to_char(a_tax_reconcile_sub_rec(i).tax_record_id) || g_sep ||
                                   to_char(a_tax_reconcile_sub_rec(i).tax_include_id) || g_sep ||
                                   to_char(a_tax_reconcile_sub_rec(i).tax_year) || g_sep ||
                                   to_char(a_tax_reconcile_sub_rec(i).reconcile_item_id);
         begin
           l_reconcile_index := g_tax_reconcile_index_hash( to_char(a_tax_reconcile_sub_rec(i).tax_record_id) || g_sep ||
                                     to_char(a_tax_reconcile_sub_rec(i).tax_include_id) || g_sep ||
                                     to_char(a_tax_reconcile_sub_rec(i).tax_year) || g_sep ||
                                     to_char(a_tax_reconcile_sub_rec(i).reconcile_item_id) );
           g_tax_reconcile_rec(l_reconcile_index) := a_tax_reconcile_sub_rec(i);

           if (a_reconcile_bals_rows > 0) then
              /* tax_reconcile[i - 1]->tax_year + 1 */
              l_found := false;
              l_nyear := 0;
              for j in 1 .. a_reconcile_bals_rows
              loop
                 if (a_reconcile_bals_sub_rec(j).tax_include_id = a_tax_reconcile_sub_rec(i).tax_include_id and
                    a_reconcile_bals_sub_rec(j).tax_record_id = a_tax_reconcile_sub_rec(i).tax_record_id and
                    a_reconcile_bals_sub_rec(j).tax_year > a_tax_reconcile_sub_rec(i).tax_year and
                    a_reconcile_bals_sub_rec(j).reconcile_item_id = a_tax_reconcile_sub_rec(i).reconcile_item_id) then
                    l_found := true;
                    if (a_reconcile_bals_sub_rec(j).tax_year < l_nyear or l_nyear = 0) then
                       l_nyear := a_reconcile_bals_sub_rec(j).tax_year;
                       l_pos   := j;
                    end if;
                 end if;
              end loop;
              if (l_found) then
                 a_reconcile_bals_sub_rec(l_pos).basis_amount_beg := l_tax_reconcile_end(i);
                 l_reconcile_index := g_tax_reconcile_index_hash( to_char(a_reconcile_bals_sub_rec(l_pos).tax_record_id) || g_sep ||
                                     to_char(a_reconcile_bals_sub_rec(l_pos).tax_include_id) || g_sep ||
                                     to_char(a_reconcile_bals_sub_rec(l_pos).tax_year) || g_sep ||
                                     to_char(a_reconcile_bals_sub_rec(l_pos).reconcile_item_id) );
                 g_tax_reconcile_rec(l_reconcile_index) :=a_reconcile_bals_sub_rec(l_pos);
              end if;
           end if;
         exception
            when no_data_found then
               write_log(g_job_no,
                           4,
                           l_code,
                            ' jrow=' || to_char(l_jrow ) ||  ' ' || l_msg ||
                           sqlerrm(l_code) || ' ' || dbms_utility.format_error_backtrace);
            when others then
                 l_code := sqlcode;
                 write_log(g_job_no,
                           4,
                           l_code,
                           'Calc Record ID=' || to_char(g_tax_depr_rec(a_tax_depr_index).tax_record_id) || '  ' || l_msg ||
                           sqlerrm(l_code) || ' ' || dbms_utility.format_error_backtrace);
                  return - 1;
         end;
      end loop;

      /*rowid = tax_depr->rowid;*/
      -- if(fabs(book_balance) < .0000001)
      --    book_balance = 0;
      -- tax_depr->book_balance=book_balance;

      -- if(fabs(tax_balance) < .0000001)
      --    tax_balance = 0;
      --    tax_depr->tax_balance=tax_balance;

      if (abs(l_remaining_life) < .0000001) then
         l_remaining_life := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).remaining_life := l_remaining_life;

      --    if(fabs(accum_reserve) < .0000001)
      --    accum_reserve = 0;
      --    tax_depr->accum_reserve=accum_reserve;

      --    if(fabs(sl_reserve) < .0000001)
      --    sl_reserve = 0;
      --    tax_depr->sl_reserve=sl_reserve;

      if (abs(l_depreciable_base) < .0000001) then
         l_depreciable_base := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).depreciable_base := l_depreciable_base;

      --   if(fabs(fixed_depreciable_base) < .0000001)
      --    fixed_depreciable_base = 0;
      --   tax_depr->fixed_depreciable_base=fixed_depreciable_base;

      --   if(fabs(actual_salvage) < .0000001)
      --    actual_salvage = 0;
      --   tax_depr->actual_salvage=actual_salvage;

      if (abs(l_estimated_salvage) < .0000001) then
         l_estimated_salvage := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).estimated_salvage_end := l_estimated_salvage;

      -- maint-42180.  calc should not be updating accum_salvage.
      --if (abs(l_accum_salvage) < .0000001) then
      --   l_accum_salvage := 0;
      --end if;
      --g_tax_depr_rec(a_tax_depr_index).accum_salvage := l_accum_salvage;

      if (abs(l_additions) < .0000001) then
         l_additions := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).additions := l_additions;

      if (abs(l_tax_balance_transfer) < .0000001) then
         l_tax_balance_transfer := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).transfers := l_tax_balance_transfer;
      if (abs(l_adjustments) < .0000001) then
         l_adjustments := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).adjustments := l_adjustments;

      if (abs(l_retirements) < .0000001) then
         l_retirements := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).retirements := l_retirements;

      if (abs(l_extraordinary_retires) < .0000001) then
         l_extraordinary_retires := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).extraordinary_retires := l_extraordinary_retires;

      --    if(fabs(accum_ordinary_retires) < .0000001)
      --       accum_ordinary_retires = 0;
      --    tax_depr->accum_ordinary_retires=accum_ordinary_retires;

      if (abs(l_cost_of_removal) < .0000001) then
         l_cost_of_removal := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).cost_of_removal := l_cost_of_removal;

      if (abs(l_cor_expense) < .0000001) then
         l_cor_expense := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).cor_expense := l_cor_expense;

      if (abs(l_cor_res_impact) < .0000001) then
         l_cor_res_impact := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).cor_res_impact := l_cor_res_impact;

      g_tax_depr_rec(a_tax_depr_index).number_months_beg := l_number_months_beg;
      g_tax_depr_rec(a_tax_depr_index).number_months_end := l_number_months_end;

      if (abs(l_depreciation) < .0000001) then
         l_depreciation := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).depreciation := l_depreciation + l_job_creation_amount;

      if (abs(l_gain_loss) < .0000001) then
         l_gain_loss := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).gain_loss := l_gain_loss;

      if (abs(l_ex_gain_loss) < .0000001) then
         l_ex_gain_loss := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).ex_gain_loss := l_ex_gain_loss;

      if (abs(l_capital_gain_loss) < .0000001) then
         l_capital_gain_loss := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).capital_gain_loss := l_capital_gain_loss;

      --if(fabs(est_salvage_pct) < .0000001)
      --  est_salvage_pct := 0;
      --ax_depr->est_salvage_pct:=est_salvage_pct;

      if (abs(l_book_balance_end) < .0000001) then
         l_book_balance_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).book_balance_end := l_book_balance_end;

      if (abs(l_tax_balance_end) < .0000001) then
         l_tax_balance_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).tax_balance_end := l_tax_balance_end;

      if (abs(l_accum_reserve_end) < .0000001) then
         l_accum_reserve_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).accum_reserve_end := l_accum_reserve_end;

      if (abs(l_sl_reserve_end) < .0000001) then
         l_sl_reserve_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).sl_reserve_end := l_sl_reserve_end;

      if (abs(l_accum_salvage_end) < .0000001) then
         l_accum_salvage_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).accum_salvage_end := l_accum_salvage_end;

      if (abs(l_accum_ordin_retires_end) < .0000001) then
         l_accum_ordin_retires_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).accum_ordin_retires_end := l_accum_ordin_retires_end;

      if (abs(l_quantity_end) < .0000001) then
         l_quantity_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).quantity_end := l_quantity_end;

      if (abs(l_retire_invol_conv) < .0000001) then
         l_retire_invol_conv := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).retire_invol_conv := l_retire_invol_conv;

      if (abs(l_salvage_invol_conv) < .0000001) then
         l_salvage_invol_conv := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).salvage_invol_conv := l_salvage_invol_conv;

      if (abs(l_salvage_extraord) < .0000001) then
         l_salvage_extraord := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).salvage_extraord := l_salvage_extraord;

      if (abs(l_calc_depreciation) < .0000001) then
         l_calc_depreciation := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).calc_depreciation := l_calc_depreciation;

      if (abs(l_over_adj_depreciation) < .0000001) then
         l_over_adj_depreciation := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).over_adj_depreciation := l_over_adj_depreciation;

      if (abs(l_retire_res_impact) < .0000001) then
         l_retire_res_impact := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).retire_res_impact := l_retire_res_impact;

      if (abs(l_ex_retire_res_impact) < .0000001) then
         l_ex_retire_res_impact := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).ex_retire_res_impact := l_ex_retire_res_impact;

      if (abs(l_accum_reserve_transfer) < .0000001) then
         l_accum_reserve_transfer := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).transfer_res_impact := l_accum_reserve_transfer;

      if (abs(l_salvage_res_impact) < .0000001) then
         l_salvage_res_impact := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).salvage_res_impact := l_salvage_res_impact;

      if (abs(l_adjusted_retire_basis) < .0000001) then
         l_adjusted_retire_basis := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).adjusted_retire_basis := l_adjusted_retire_basis;
      if (abs(l_reserve_at_switch) < .0000001) then
         l_reserve_at_switch := 0;
      end if;

      ----maint-37060 : comment out the below block.
      ----if (l_reserve_at_switch_init <> 0) then
      ----   g_tax_depr_rec(a_tax_depr_index).reserve_at_switch := l_reserve_at_switch_init;
      ----end if;

      if (abs(l_reserve_at_switch_end) < .0000001) then
         l_reserve_at_switch_end := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).reserve_at_switch_end := l_reserve_at_switch_end;

      if (abs(l_estimated_salvage) < .0000001) then
         l_estimated_salvage := 0;
      end if;
      g_tax_depr_rec(a_tax_depr_index).estimated_salvage_end := l_estimated_salvage;
      g_tax_depr_rec(a_tax_depr_index).job_creation_amount := l_job_creation_amount;
      if (a_tax_depr_bals_rows > 0) then
         /*rowid = tax_depr_bals->rowid; */
         if (abs(l_book_balance_end) < .0000001) then
            l_book_balance_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).book_balance := l_book_balance_end;

         if (abs(l_estimated_salvage) < .0000001) then
            l_estimated_salvage := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).estimated_salvage_end := l_estimated_salvage;

         if (abs(l_tax_balance_end) < .0000001) then
            l_tax_balance_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).tax_balance := l_tax_balance_end;

         if (abs(l_accum_reserve_end) < .0000001) then
            l_accum_reserve_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).accum_reserve := l_accum_reserve_end;

         if (abs(l_reserve_at_switch_end) < .0000001) then
            l_reserve_at_switch_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).reserve_at_switch := l_reserve_at_switch_end;

         if (abs(l_sl_reserve_end) < .0000001) then
            l_sl_reserve_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).sl_reserve := l_sl_reserve_end;

         if (abs(l_accum_salvage_end) < .0000001) then
            l_accum_salvage_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).accum_salvage := l_accum_salvage_end;

         if (abs(l_accum_ordin_retires_end) < .0000001) then
            l_accum_ordin_retires_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).accum_ordinary_retires := l_accum_ordin_retires_end;

         if (abs(l_fixed_depreciable_base) < .0000001) then
            l_fixed_depreciable_base := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).fixed_depreciable_base := l_fixed_depreciable_base;
         -- Rake foreward est_salvage pst. If user wants to turn offf est salvage for a tax year,
         -- then input very low number (.e.g. .00001) and the rake forward will not occur.

         if (g_tax_depr_rec(a_tax_depr_bals_index).est_salvage_pct = 0) then
            g_tax_depr_rec(a_tax_depr_bals_index).est_salvage_pct := l_est_salvage_pct;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).number_months_beg := l_number_months_end;

         if (abs(l_quantity_end) < .0000001) then
            l_quantity_end := 0;
         end if;
         g_tax_depr_rec(a_tax_depr_bals_index).quantity := l_quantity_end;

      end if;

      return 0;

   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Calc Record ID=' || to_char(g_tax_depr_rec(a_tax_depr_index).tax_record_id) || '  ' || l_msg ||
                   sqlerrm(l_code) || ' ' || dbms_utility.format_error_backtrace);
         return - 1;
   end calc;

   -- =============================================================================
   --  Procedure SET_SESSION_PARAMETER
   -- =============================================================================
   procedure set_session_parameter is
      cursor session_param_cur is
         select parameter, value, type
           from pp_session_parameters
          where lower(users) = 'all'
             or lower(users) = 'depr';
      l_count pls_integer;
      i       pls_integer;
      type type_session_param_rec is table of session_param_cur%rowtype;
      l_session_param_rec type_session_param_rec;
      l_code              pls_integer;
      l_sql               varchar2(200);
   begin

      open session_param_cur;
      fetch session_param_cur bulk collect
         into l_session_param_rec;
      close session_param_cur;

      l_count := l_session_param_rec.count;
      for i in 1 .. l_count
      loop
         if l_session_param_rec(i).type = 'number' or l_session_param_rec(i).type = 'boolean' then
            l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
                    .value;
         elsif l_session_param_rec(i).type = 'string' then
            l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ''' || l_session_param_rec(i)
                    .value || '''';
         end if;
         execute immediate l_sql;
         write_log(g_job_no,
                   0,
                   0,
                   'Set Session Parameter ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
                   .value);
      end loop;
      return;
   exception
      when no_data_found then
         return;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   5,
                   l_code,
                   'Set Session Parameter ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return;
   end set_session_parameter;

   -- =============================================================================
   --  Procedure WRITE_LOG
   -- =============================================================================
   procedure write_log(a_job_no     number,
                       a_error_type number,
                       a_code       number,
                       a_msg        varchar2) as
      pragma autonomous_transaction;
      l_code     pls_integer;
      l_msg      varchar2(2000);
      l_cur_ts   timestamp;
      l_date_str varchar2(100);
   begin
      l_cur_ts   := current_timestamp;
      l_date_str := to_char(sysdate, 'HH24:MI:SS');
      l_msg      := a_msg;
      dbms_output.put_line(l_msg);
      insert into tax_job_log
         (job_no, line_no, log_date, error_type, error_code, msg)
      values
         (a_job_no, g_line_no, sysdate, a_error_type, a_code, l_msg);
      commit;
      g_line_no := g_line_no + 1;
      return;
   exception
      when others then
         l_code := sqlcode;
         dbms_output.put_line('Write Log ' || sqlerrm(l_code) || ' ' ||
                              dbms_utility.format_error_backtrace);
         return;
   end write_log;

end TAX_DEPR_CALC;
/
