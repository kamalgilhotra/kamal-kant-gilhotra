/*
||============================================================================
|| Application: PowerPlant
|| File Name:   maint_039519_lease_PKG_LEASE_ILR.sql
||============================================================================
|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By       Reason for Change
|| -------- ---------- ---------------- --------------------------------------
|| 10.4.3.0 06/25/2014 Kyle Peterson
|| 10.4.3.0 07/07/2014 Charlie Shilling
|| 10.4.3.0 07/24/2014 Charlie Shilling Use "where revision < 9999" in f_newrevision
|| 2016.1.0 03/11/2016 Anand R          PP-45366 copy ls_depr_forecast table
|| 2016.1.0 08/08/2016 Anand R          PP-48364 add contract_currency_id to ls_asset table copy
|| 2017.1.0 06/26/2017 Jared S.		    PP-48330 Added columns for deferred rent
|| 2017.3.0 03/02/2018 Anand R          PP-50205 Add new Lessee tables and columns to Lease forecast
|| 2018.1.0 10/12/2018 K. Powers        PP-50205 Add new impairment columns
|| 2018.1.0 02/05/2019 Chris Trigg		PP-52891 Add default discount rate function
||============================================================================
*/

create or replace package PKG_LEASE_ILR as
   G_PKG_VERSION varchar(35) := '2018.2.1.0';

   type CR_DERIVER_TYPE is table of CR_DERIVER_CONTROL%rowtype;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the passed in revision information as the starting point, and copy everything, including the schedule
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *      number: in: a_revision = the revision to copy
   *   @@RETURN
   *      number: new revision number for success
   *            -1 for failure
   */
   function F_COPYREVISION(A_ILR_ID   number,
                           A_REVISION number:=null,
                           A_TO_REVISION number:=null) return number;

   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for all in-service ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function f_RETIREREVISION(A_ILR_ID      number,
                             A_LS_ASSET_ID number,
                             A_MONTH       date,
							 A_FLAG        NUMBER,
							 A_ACCT_MONTH DATE) return number;
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in asset/ILR being retired
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a retired revision
   *      number: in: a_ls_asset_id = the ls_asset_id to create a retired revision
   *      date: in: a_month = the retirement month to use
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS return number;

   /*
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for the passed in array of ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS(A_ILR_IDS in PKG_LEASE_CALC.NUM_ARRAY) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *    number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: 1 for success
   *            0 for failure
   */
   function F_COPYASSET(A_LS_ASSET_ID number,
                        A_PCT         number,
                        A_QTY         number) return number;

   function F_GETTAXES(A_ILR_ID number) return varchar2;
   
   -- Pass in 0 to a_start_log to prevent a new log from starting
   procedure P_CALC_II(A_ILR_ID number, A_REVISION number, A_START_LOG number :=null); 

   function F_GET_DEFAULT_DISC_RATE(A_ILR_ID number,
									A_TERM_MONTHS number,
									A_IN_SERVICE_DATE date,
									A_REMEASUREMENT_DATE date,
									A_BORROWING_CURRENCY number) return number;

   /*
   *   @@DESCRIPTION
   *   	This function pulls the correct discount rate based on the
   *	parameters passed to the function.
   *   @@PARAMS
   *	number: in: A_ILR_ID = id of ILR being calculated
   *    number: in: A_TERM_MONTHS = number of months for the term
   *    date: in:   A_IN_SERVICE_DATE = Date ILR placed in service
   *    number: in: A_BORROWING_CURRENCY = currency used by process, if any
   *   @@RETURN
   *      number: >= 0 for success
   *              -1 for failure
   */

function F_PULL_ILR_PAYMENT_TERM_MONTHS(a_ilr_id NUMBER, a_revision number) RETURN NUMBER;
 --##########################################################################
 -- PULL PAYMENT TERM LENGTH FOR ILR IN MONTHS
 --##########################################################################
 
end PKG_LEASE_ILR;
/
create or replace package body PKG_LEASE_ILR as
   --**************************************************************************
   --                            VARIABLES
   --**************************************************************************
   --**************************************************************************
   --                            FORWARD DECLARATIONS
   --**************************************************************************

   --**************************************************************************
   --                            PROCEDURES
   --**************************************************************************

   --**************************************************************************
   --                            FUNCTIONS
   --**************************************************************************
   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *   @@RETURN
   *      number: the revision added for success
   *            -1 for failure
   */
   function F_NEWREVISION(A_ILR_ID number) return number is

      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Revision for: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   COPY Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      L_STATUS := 'Get new revision';
      select nvl(max(REVISION),0) + 1 into L_REVISION from LS_ILR_APPROVAL where ILR_ID = A_ILR_ID and REVISION < 9999;
      PKG_PP_LOG.P_WRITE_MESSAGE('   NEW Revision: ' || TO_CHAR(L_REVISION));

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT, in_service_exchange_rate
          , payment_shift, deferred_rent_sw, depr_calc_method, schedule_payment_shift, asset_quantity, default_rate_flag, borrowing_curr_id
          )
         select A.ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT,
                in_service_exchange_rate,
                payment_shift,
                deferred_rent_sw,
                nvl(depr_calc_method,0),
                schedule_payment_shift,
				asset_quantity,
				default_rate_flag,
				borrowing_curr_id
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                L_REVISION,
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10,
          ESCALATION, ESCALATION_FREQ_ID, ESCALATION_PCT)
         select A.ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10,
                ESCALATION,
                ESCALATION_FREQ_ID,
                ESCALATION_PCT
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_asset_map';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID, L_REVISION, A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

	  L_STATUS := 'LOAD ls_ilr_weighted_avg_rates';
      insert into LS_ILR_WEIGHTED_AVG_RATES
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, GROSS_WEIGHTED_AVG_RATE, NET_WEIGHTED_AVG_RATE)
         select A.ILR_ID, L_REVISION, A.SET_OF_BOOKS_ID, A.GROSS_WEIGHTED_AVG_RATE, A.NET_WEIGHTED_AVG_RATE
           from LS_ILR_WEIGHTED_AVG_RATES A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;


      return L_REVISION;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_NEWREVISION;

   /*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in ILR.
   *      It will use the passed in revision information as the starting point, and copy everything, including the schedule
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new revision
   *      number: in: a_revision = the revision to copy
   *   @@RETURN
   *      number: new revision number for success
   *            -1 for failure
   */
   function F_COPYREVISION(A_ILR_ID   number,
                           A_REVISION number :=null,
                           A_TO_REVISION number:=null) return number is

      L_STATUS   varchar2(2000);
      L_TO_REVISION number;
	  L_FROM_REVISION number;
      RTN        number;
      IS_AUTO    varchar2(254);

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
	  
	  if A_REVISION is null then
		L_STATUS := 'Retrieving Current Approved Revision for:' || TO_CHAR(A_ILR_ID)  ;
		
		select current_revision
		into L_FROM_REVISION
		from LS_ILR
		where ilr_id = A_ILR_ID;
	  else
		
		L_FROM_REVISION := A_REVISION;
	  end if;
	  
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW Revision for: ' || TO_CHAR(A_ILR_ID) ||
                                 ' copied from revision: ' || TO_CHAR(L_FROM_REVISION));

      L_STATUS := 'Get new revision';
      if A_TO_REVISION is null then
        select nvl(max(REVISION),0) + 1
          into L_TO_REVISION
          from LS_ILR_APPROVAL
         where ILR_ID = A_ILR_ID
           and REVISION < 9999;
        PKG_PP_LOG.P_WRITE_MESSAGE('   NEW Revision: ' || TO_CHAR(L_TO_REVISION));
       else
         L_TO_REVISION := A_TO_REVISION;
       end if;

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID, L_TO_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD ilr_renewal';
      insert into LS_ILR_RENEWAL
         (ILR_RENEWAL_ID, ILR_ID, REVISION, RENEWAL_OPTIONS_ID)
         select ls_ilr_renewal_seq.nextval, A.ILR_ID, L_TO_REVISION, A.RENEWAL_OPTIONS_ID
           from LS_ILR_RENEWAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD ilr_renewal_options';

      for l_lir in (select ds1.ilr_renewal_id as ilr_renewal_id_from, ds2.ilr_renewal_id as ilr_renewal_id_to from
                   (select ilr_renewal_id, row_number() over(order by ilr_renewal_id) as row_num from ls_ilr_renewal where ilr_id = A_ILR_ID and revision = L_FROM_REVISION ) ds1,
                   (select ilr_renewal_id, row_number() over(order by ilr_renewal_id) as row_num from ls_ilr_renewal where ilr_id = A_ILR_ID and revision = L_TO_REVISION ) ds2
                   where ds1.row_num = ds2.row_num )
      loop
        insert into LS_ILR_RENEWAL_OPTIONS
         (ILR_RENEWAL_OPTION_ID, ILR_RENEWAL_ID, ILR_RENEWAL_PROBABILITY_ID,
         NOTICE_REQUIREMENT_MONTHS, PAYMENT_FREQ_ID, NUMBER_OF_TERMS,
         AMOUNT_PER_TERM, RENEWAL_START_DATE)
         select ls_ilr_renewal_options_seq.nextval, l_lir.ILR_RENEWAL_ID_TO, ILR_RENEWAL_PROBABILITY_ID,
         NOTICE_REQUIREMENT_MONTHS, PAYMENT_FREQ_ID, NUMBER_OF_TERMS,
         AMOUNT_PER_TERM, RENEWAL_START_DATE
           from LS_ILR_RENEWAL_OPTIONS A
          where A.ILR_RENEWAL_ID IN (l_lir.ILR_RENEWAL_ID_FROM);
      end loop;

     L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT, in_service_exchange_rate
          , payment_shift, INTENT_TO_PURCHASE,  SPECIALIZED_ASSET,
                LIKELY_TO_COLLECT, SUBLEASE_FLAG, SUBLEASE_ID, INTERCO_LEASE_FLAG,INTERCO_LEASE_COMPANY, deferred_rent_sw,
          depr_calc_method, schedule_payment_shift, ASSET_QUANTITY, borrowing_curr_id, default_rate_flag
          )
         select A.ILR_ID,
                L_TO_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT,
				A.in_service_exchange_rate AS in_service_exchange_rate,
                A.payment_shift,
                A.INTENT_TO_PURCHASE,
                A.SPECIALIZED_ASSET,
                A.LIKELY_TO_COLLECT,
                A.SUBLEASE_FLAG,
                A.SUBLEASE_ID,
                A.INTERCO_LEASE_FLAG,
                A.INTERCO_LEASE_COMPANY,
                A.DEFERRED_RENT_SW,
                nvl(A.DEPR_CALC_METHOD,0),
                A.schedule_payment_shift,
				A.ASSET_QUANTITY,
				A.BORROWING_CURR_ID,
				A.DEFAULT_RATE_FLAG
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;

	   L_STATUS := 'LOAD ILR_PURCHASE_OPTIONS';
		INSERT INTO LS_ILR_PURCHASE_OPTIONS
		(ILR_ID,
		REVISION,
		ILR_PURCHASE_OPTION_ID,
		ILR_PURCHASE_PROBABILITY_ID,
		DECISION_NOTICE,
		DECISION_DATE,
		PURCHASE_DATE,
		PURCHASE_OPTION_TYPE,
		PURCHASE_AMT
		)
		SELECT A.ILR_ID,
			   L_TO_REVISION,
			   LS_ILR_PURCHASE_OPTIONS_SEQ.NEXTVAL,
			   A.ILR_PURCHASE_PROBABILITY_ID,
			   A.DECISION_NOTICE,
			   A.DECISION_DATE,
			   A.PURCHASE_DATE,
			   A.PURCHASE_OPTION_TYPE,
			   A.PURCHASE_AMT
			FROM LS_ILR_PURCHASE_OPTIONS A
			WHERE A.ILR_ID = A_ILR_ID
            AND A.REVISION = L_FROM_REVISION;

	  L_STATUS := 'LOAD ILR_TERMINATION_OPTIONS';
		INSERT INTO LS_ILR_TERMINATION_OPTIONS
		(ILR_ID,
		REVISION,
		ILR_TERMINATION_OPTION_ID,
		ILR_TERMINATION_PROBABILITY_ID,
		DECISION_NOTICE,
		DECISION_DATE,
		TERMINATION_DATE,
		TERMINATION_AMT
		)
		SELECT A.ILR_ID,
			   L_TO_REVISION,
			   LS_ILR_TERMINATION_OPTIONS_SEQ.NEXTVAL,
			   A.ILR_TERMINATION_PROBABILITY_ID,
			   A.DECISION_NOTICE,
			   A.DECISION_DATE,
			   A.TERMINATION_DATE,
			   A.TERMINATION_AMT
			FROM LS_ILR_TERMINATION_OPTIONS A
			WHERE A.ILR_ID = A_ILR_ID
            AND A.REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                L_TO_REVISION,
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10,
          MAKE_II_PAYMENT, INTERIM_INTEREST_BEGIN_DATE, ESCALATION, ESCALATION_FREQ_ID, ESCALATION_PCT)
         select A.ILR_ID,
                L_TO_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10,
                MAKE_II_PAYMENT,
                INTERIM_INTEREST_BEGIN_DATE,
                ESCALATION,
                ESCALATION_FREQ_ID,
                ESCALATION_PCT
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD ilr_payment_term_var_paymnt';
      insert into LS_ILR_PAYMENT_TERM_VAR_PAYMNT
         (ILR_ID, REVISION, PAYMENT_TERM_ID, RENT_TYPE, BUCKET_NUMBER, VARIABLE_PAYMENT_ID,
          INCL_IN_INITIAL_MEASURE, RUN_ORDER, INITIAL_VAR_PAYMENT_VALUE, SET_OF_BOOKS_ID)
         select A.ILR_ID,
                L_TO_REVISION,
                A.PAYMENT_TERM_ID,
                A.RENT_TYPE,
                A.BUCKET_NUMBER,
                A.VARIABLE_PAYMENT_ID,
                A.INCL_IN_INITIAL_MEASURE,
                A.RUN_ORDER,
                A.INITIAL_VAR_PAYMENT_VALUE,
				A.SET_OF_BOOKS_ID
           from LS_ILR_PAYMENT_TERM_VAR_PAYMNT A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD ilr_asset_map';
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID, L_TO_REVISION, A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;

	  L_STATUS := 'LOAD ilr_incentive';
	  insert into LS_ILR_INCENTIVE
		(ILR_INCENTIVE_ID, ILR_ID, REVISION, INCENTIVE_GROUP_ID, DATE_INCURRED, AMOUNT, DESCRIPTION)
	  SELECT LS_ILR_INCENTIVE_SEQ.NEXTVAL, A.ILR_ID, L_TO_REVISION, A.INCENTIVE_GROUP_ID, A.DATE_INCURRED, A.AMOUNT, A.DESCRIPTION
		from LS_ILR_INCENTIVE a
		where A.ILR_ID = A_ILR_ID
		 and A.REVISION = L_FROM_REVISION;

	  L_STATUS := 'LOAD ilr_init_direct_cost';
	  insert into LS_ILR_INITIAL_DIRECT_COST
		(ILR_IDC_ID, ILR_ID, REVISION, IDC_GROUP_ID, DATE_INCURRED, AMOUNT, DESCRIPTION)
	  SELECT LS_ILR_IDC_SEQ.NEXTVAL, A.ILR_ID, L_TO_REVISION, A.IDC_GROUP_ID, A.DATE_INCURRED, A.AMOUNT, A.DESCRIPTION
		from LS_ILR_INITIAL_DIRECT_COST a
		where A.ILR_ID = A_ILR_ID
		 and A.REVISION = L_FROM_REVISION;

	  if A_TO_REVISION is null then
      L_STATUS := 'LOAD ilr_schedule';
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_MATH_AMOUNT,
          IDC_MATH_AMOUNT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
          ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS, BEG_NET_ROU_ASSET,
          CONTINGENT_ADJUST, CURRENT_LEASE_COST,ESCALATION_MONTH,ESCALATION_PCT,EXECUTORY_ADJUST,PAYMENT_MONTH,	END_NET_ROU_ASSET,
          BEGIN_ACCUM_IMPAIR,IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR, incentive_cap_amount)
         select ILR_ID, L_TO_REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
                BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
                END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
                INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
                CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
                CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
                EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
                EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
                IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
                PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
                PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_MATH_AMOUNT,
                IDC_MATH_AMOUNT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
                BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
                ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
                UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,
                CONTINGENT_ADJUST, CURRENT_LEASE_COST,ESCALATION_MONTH,ESCALATION_PCT,EXECUTORY_ADJUST,PAYMENT_MONTH,END_NET_ROU_ASSET,
                BEGIN_ACCUM_IMPAIR,IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR,incentive_cap_amount
          from LS_ILR_SCHEDULE
          where ILR_ID = A_ILR_ID
            and REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD asset_schedule';
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          PARTIAL_MONTH_PERCENT, BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
          REMAINING_PRINCIPAL, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,CONTINGENT_ADJUST,CURRENT_LEASE_COST,END_NET_ROU_ASSET,EXECUTORY_ADJUST,
          BEGIN_ACCUM_IMPAIR, IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR, incentive_cap_amount)
         select LS_ASSET_ID, L_TO_REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
                BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
                END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
                INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
                CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
                CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
                EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
                EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
                IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
                PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
                PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
                PARTIAL_MONTH_PERCENT, BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
                REMAINING_PRINCIPAL, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
                UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,CONTINGENT_ADJUST,CURRENT_LEASE_COST,END_NET_ROU_ASSET,EXECUTORY_ADJUST,
                BEGIN_ACCUM_IMPAIR,IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR,incentive_cap_amount
           from LS_ASSET_SCHEDULE
          where LS_ASSET_ID in (select LS_ASSET_ID
                                  from LS_ILR_ASSET_MAP
                                 where ILR_ID = A_ILR_ID
                                   and REVISION = L_FROM_REVISION)
            and REVISION = L_FROM_REVISION;

            L_STATUS := 'LOAD depr_forecast';
            insert into LS_DEPR_FORECAST
            (LS_ASSET_ID,REVISION,SET_OF_BOOKS_ID,MONTH,BEGIN_RESERVE,DEPR_EXPENSE,DEPR_EXP_ALLOC_ADJUST,END_RESERVE,
             DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, PARTIAL_MONTH_PERCENT)
            select LS_ASSET_ID,L_TO_REVISION,SET_OF_BOOKS_ID,MONTH,BEGIN_RESERVE,DEPR_EXPENSE,DEPR_EXP_ALLOC_ADJUST,END_RESERVE,
                   DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, PARTIAL_MONTH_PERCENT
            from LS_DEPR_FORECAST
            where LS_ASSET_ID in (select LS_ASSET_ID
                                  from LS_ILR_ASSET_MAP
                                 where ILR_ID = A_ILR_ID
                                   and REVISION = L_FROM_REVISION)
            and REVISION = L_FROM_REVISION;

      L_STATUS := 'LOAD asset_schedule_tax';
      insert into ls_asset_schedule_tax (
        ls_asset_id, revision, set_of_books_id, schedule_month, gl_posting_mo_yr,
        tax_local_id, vendor_id, tax_district_id, tax_base, tax_rate, payment_amount)
      select ls_asset_id, L_TO_REVISION, set_of_books_id, schedule_month, gl_posting_mo_yr,
             tax_local_id, vendor_id, tax_district_id, tax_base, tax_rate, payment_amount
        from ls_asset_schedule_tax
       where ls_asset_id in (
               select ls_asset_id from ls_ilr_asset_map
                where ilr_id = A_ILR_ID
                  and revision = L_FROM_REVISION)
         and revision = L_FROM_REVISION;

		end if;
		
	  L_STATUS := 'LOAD ls_ilr_weighted_avg_rates';
      insert into LS_ILR_WEIGHTED_AVG_RATES
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, GROSS_WEIGHTED_AVG_RATE, NET_WEIGHTED_AVG_RATE)
         select A.ILR_ID, L_TO_REVISION, A.SET_OF_BOOKS_ID, A.GROSS_WEIGHTED_AVG_RATE, A.NET_WEIGHTED_AVG_RATE
           from LS_ILR_WEIGHTED_AVG_RATES A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_FROM_REVISION;



      return L_TO_REVISION;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_COPYREVISION;

	/*
	* 		--Deprecated Function, no longer in use - SCW
	*
	*   @@DESCRIPTION
	*      This function converts forecasts into new "real" revisions for all in-service ILR's
	*      NO COMMITS or ROLLBACK in this function
	*   @@PARAMS
	*      none
	*   @@RETURN
	*      number: 1 for success
	*            -1 for failure
	*/
	function F_CONVERTFORECASTS return number is

		L_STATUS varchar2(2000);
		L_IDS PKG_LEASE_CALC.NUM_ARRAY;

	begin
		return F_CONVERTFORECASTS(L_IDS);

	exception
		when others then
			L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
			PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

			return -1;
	end F_CONVERTFORECASTS;

   /*
   *
   * 		--Deprecated Function, no longer in use - SCW
   *   @@DESCRIPTION
   *      This function converts forecasts into new "real" revisions for the passed in array of ILR's
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      none
   *   @@RETURN
   *      number: 1 for success
   *            -1 for failure
   */
   function F_CONVERTFORECASTS(A_ILR_IDS in PKG_LEASE_CALC.NUM_ARRAY) return number is

      L_STATUS varchar2(2000);
      L_RTN    number;
	  L_ID_COUNT number;
	  L_IDS T_NUM_ARRAY;

   begin
	  PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ILR.F_CONVERTFORECASTS');

      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('Converting forecast revisions');

	  L_STATUS := 'Initializing ILR Array';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
	  L_ID_COUNT := A_ILR_IDS.count;
	  if L_ID_COUNT = 0 then
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for all in-service ILR''s');
	  elsif L_ID_COUNT = 1 then
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for 1 ILR');
	  else
		PKG_PP_LOG.P_WRITE_MESSAGE('-- Converting forecasts for '||to_char(L_ID_COUNT)||' ILR''s');
	  end if;

	  L_IDS := T_NUM_ARRAY();
	  for I in 1..A_ILR_IDS.count
	  loop
		L_IDS.extend;
		L_IDS(I) := A_ILR_IDS(I);
	  end loop;

      L_STATUS := 'Loading ILR approvals';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select A.ILR_ID,
                (select max(REVISION) + 1
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.APPROVAL_TYPE_ID,
                1
           from LS_ILR_APPROVAL A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ilr_renewal';
      insert into LS_ILR_RENEWAL
         (ILR_RENEWAL_ID, ILR_ID, REVISION, RENEWAL_OPTIONS_ID)
         select ls_ilr_renewal_seq.nextval,
         A.ILR_ID,
         (select max(REVISION) + 1
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
         A.RENEWAL_OPTIONS_ID
           from LS_ILR_RENEWAL A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'LOADing ilr_renewal_options';

      for l_lir in (select ds1.ilr_renewal_id as ilr_renewal_id_from, ds2.ilr_renewal_id as ilr_renewal_id_to from
                   (select ilr_renewal_id, row_number() over(order by ilr_renewal_id) as row_num
                       from ls_ilr_renewal, LS_ILR ILR
                       where ILR_STATUS_ID = 2
			                      and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
                            and revision = 9999 ) ds1,
                   (select ilr_renewal_id, row_number() over(order by ilr_renewal_id) as row_num
                       from ls_ilr_renewal, LS_ILR ILR
                       where ILR.ILR_STATUS_ID = 2
			                       and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
                             and revision = (select max(REVISION) + 1
                                             from LS_ILR_APPROVAL
                                             where ILR_ID = ILR.ILR_ID
                                             and REVISION < 9999)
                                             ) ds2
                   where ds1.row_num = ds2.row_num )
      loop
        insert into LS_ILR_RENEWAL_OPTIONS
         (ILR_RENEWAL_OPTION_ID, ILR_RENEWAL_ID, ILR_RENEWAL_PROBABILITY_ID,
         NOTICE_REQUIREMENT_MONTHS, PAYMENT_FREQ_ID, NUMBER_OF_TERMS,
         AMOUNT_PER_TERM, RENEWAL_START_DATE)
         select ls_ilr_renewal_options_seq.nextval,
         l_lir.ILR_RENEWAL_ID_TO,
         ILR_RENEWAL_PROBABILITY_ID,
         NOTICE_REQUIREMENT_MONTHS,
         PAYMENT_FREQ_ID,
         NUMBER_OF_TERMS,
         AMOUNT_PER_TERM,
         RENEWAL_START_DATE
           from LS_ILR_RENEWAL_OPTIONS A
          where A.ILR_RENEWAL_ID IN (l_lir.ILR_RENEWAL_ID_FROM);
      end loop;

      L_STATUS := 'Loading ILR options';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT
          , payment_shift,IN_SERVICE_EXCHANGE_RATE, INTENT_TO_PURCHASE,  SPECIALIZED_ASSET,
                LIKELY_TO_COLLECT, SUBLEASE_FLAG, SUBLEASE_ID, INTERCO_LEASE_FLAG,INTERCO_LEASE_COMPANY, DEFERRED_RENT_SW,
          depr_calc_method, schedule_payment_shift
          )
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.PURCHASE_OPTION_TYPE_ID,
                A.PURCHASE_OPTION_AMT,
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                A.TERMINATION_AMT,
                A.payment_shift,
                A.IN_SERVICE_EXCHANGE_RATE,
                A.INTENT_TO_PURCHASE,
                A.SPECIALIZED_ASSET,
                A.LIKELY_TO_COLLECT,
                A.SUBLEASE_FLAG,
                A.SUBLEASE_ID,
                A.INTERCO_LEASE_FLAG,
                A.INTERCO_LEASE_COMPANY,
                A.DEFERRED_RENT_SW,
                nvl(A.DEPR_CALC_METHOD,0),
                A.schedule_payment_shift
           from LS_ILR_OPTIONS A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR amounts';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID,
                NET_PRESENT_VALUE,
                INTERNAL_RATE_RETURN,
                CAPITAL_COST,
                CURRENT_LEASE_COST,
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading payment terms';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10,
          MAKE_II_PAYMENT, INTERIM_INTEREST_BEGIN_DATE, ESCALATION, ESCALATION_FREQ_ID, ESCALATION_PCT)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                EST_EXECUTORY_COST,
                PAID_AMOUNT,
                CONTINGENT_AMOUNT,
                CURRENCY_TYPE_ID,
                C_BUCKET_1,
                C_BUCKET_2,
                C_BUCKET_3,
                C_BUCKET_4,
                C_BUCKET_5,
                C_BUCKET_6,
                C_BUCKET_7,
                C_BUCKET_8,
                C_BUCKET_9,
                C_BUCKET_10,
                E_BUCKET_1,
                E_BUCKET_2,
                E_BUCKET_3,
                E_BUCKET_4,
                E_BUCKET_5,
                E_BUCKET_6,
                E_BUCKET_7,
                E_BUCKET_8,
                E_BUCKET_9,
                E_BUCKET_10,
                MAKE_II_PAYMENT,
                INTERIM_INTEREST_BEGIN_DATE,
                ESCALATION,
                ESCALATION_FREQ_ID,
                ESCALATION_PCT
           from LS_ILR_PAYMENT_TERM A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading payment_term_var_paymnt';
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_PAYMENT_TERM_VAR_PAYMNT
         (ILR_ID, REVISION, PAYMENT_TERM_ID, RENT_TYPE, BUCKET_NUMBER, VARIABLE_PAYMENT_ID,
          INCL_IN_INITIAL_MEASURE, RUN_ORDER, INITIAL_VAR_PAYMENT_VALUE, SET_OF_BOOKS_ID)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.PAYMENT_TERM_ID,
                A.RENT_TYPE,
                A.BUCKET_NUMBER,
                A.VARIABLE_PAYMENT_ID,
                A.INCL_IN_INITIAL_MEASURE,
                A.RUN_ORDER,
                A.INITIAL_VAR_PAYMENT_VALUE,
				A.SET_OF_BOOKS_ID
           from LS_ILR_PAYMENT_TERM_VAR_PAYMNT A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			      and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ilr_asset_map';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_ASSET_MAP
         (ILR_ID, REVISION, LS_ASSET_ID)
         select A.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                A.LS_ASSET_ID
           from LS_ILR_ASSET_MAP A, LS_ILR ILR
          where A.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and A.REVISION = 9999;

      L_STATUS := 'Loading ILR schedule';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ILR_SCHEDULE
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_MATH_AMOUNT,
          IDC_MATH_AMOUNT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
          ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,
		  CONTINGENT_ADJUST, CURRENT_LEASE_COST,ESCALATION_MONTH,ESCALATION_PCT,EXECUTORY_ADJUST,PAYMENT_MONTH,	END_NET_ROU_ASSET,
		  BEGIN_ACCUM_IMPAIR,IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR, incentive_cap_amount)
         select SCH.ILR_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
                BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
                END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
                INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
                CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
                CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
                EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
                EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
                IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
                PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
                PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_MATH_AMOUNT,
                IDC_MATH_AMOUNT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
                BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
                ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
                UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,
		        CONTINGENT_ADJUST, CURRENT_LEASE_COST,ESCALATION_MONTH,ESCALATION_PCT,EXECUTORY_ADJUST,PAYMENT_MONTH, END_NET_ROU_ASSET,
		        BEGIN_ACCUM_IMPAIR,IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR, incentive_cap_amount
           from LS_ILR_SCHEDULE SCH, LS_ILR ILR
          where SCH.ILR_ID = ILR.ILR_ID
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and REVISION = 9999;

      L_STATUS := 'Loading asset schedule';
	  PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      insert into LS_ASSET_SCHEDULE
         (LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          PARTIAL_MONTH_PERCENT, BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
          REMAINING_PRINCIPAL, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,CONTINGENT_ADJUST,CURRENT_LEASE_COST,END_NET_ROU_ASSET,EXECUTORY_ADJUST,
          BEGIN_ACCUM_IMPAIR, IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR, incentive_cap_amount)
         select LS_ASSET_ID,
                (select max(REVISION)
                   from LS_ILR_APPROVAL
                  where ILR_ID = ILR.ILR_ID
                    and REVISION < 9999),
                SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
                BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
                END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
                INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
                CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
                CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
                CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
                EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
                EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
                CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
                CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
                EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
                EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
                IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
                PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
                PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
                PARTIAL_MONTH_PERCENT, BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
                REMAINING_PRINCIPAL, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
                UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,CONTINGENT_ADJUST,CURRENT_LEASE_COST,END_NET_ROU_ASSET,EXECUTORY_ADJUST,
          BEGIN_ACCUM_IMPAIR, IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR,incentive_cap_amount
           from LS_ASSET_SCHEDULE, LS_ILR ILR
          where LS_ASSET_SCHEDULE.LS_ASSET_ID in
                (select LS_ASSET_ID
                   from LS_ILR_ASSET_MAP
                  where ILR_ID = ILR.ILR_ID
                    and REVISION = (select max(REVISION)
                                      from LS_ILR_APPROVAL
                                     where ILR_ID = ILR.ILR_ID
                                       and REVISION < 9999))
            and ILR.ILR_STATUS_ID = 2
			and (ILR.ILR_ID in (select COLUMN_VALUE from table(L_IDS)) or L_ID_COUNT = 0)
            and REVISION = 9999;

	  PKG_PP_ERROR.REMOVE_MODULE_NAME;
      return 1;

   exception
      when others then
		 PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end F_CONVERTFORECASTS;

   /*
   *   @@DESCRIPTION
   *      This function copies an ILR into a new ILR
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a new ilr
   *    number: in: a_pct = the percent to transfer
   *   @@RETURN
   *      number: the ls_ilr_id for success
   *            -1 for failure
   */
   function F_COPYILR(A_ILR_ID number,
                      A_PCT    number) return number is

      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;
      L_ILR_ID           number;

   begin
      PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
      PKG_PP_LOG.P_WRITE_MESSAGE('NEW ILR from: ' || TO_CHAR(A_ILR_ID));

      L_STATUS := 'Get current revision';
      select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
      PKG_PP_LOG.P_WRITE_MESSAGE('   Revision: ' || TO_CHAR(L_CURRENT_REVISION));

      select LS_ILR_SEQ.NEXTVAL into L_ILR_ID from DUAL;
      PKG_PP_LOG.P_WRITE_MESSAGE('TO ILR: ' || TO_CHAR(L_ILR_ID));

      L_STATUS   := 'Get new revision';
      L_REVISION := 1;

      L_STATUS := 'LOAD ilr';
      insert into LS_ILR
         (ILR_ID, ILR_NUMBER, LEASE_ID, COMPANY_ID, EST_IN_SVC_DATE,
          EXTERNAL_ILR, ILR_STATUS_ID, ILR_GROUP_ID, NOTES,
          CURRENT_REVISION, WORKFLOW_TYPE_ID, RATE_GROUP_ID, FUNDING_STATUS_ID /* WMD */ )
         select L_ILR_ID,
		 /* CJS 3/3/15 Fixed TRF% functionality*/
                /* CJS 4/11/17 Re-fixed TRF% functionality... */
                substr(trim(replace(regexp_replace(A.ILR_NUMBER,'TRF(.)'),'()')) ||
                ' (TRF' ||
						  (select 1 + count(1)
               from LS_ILR LL
               where LL.ILR_NUMBER like Trim(replace(regexp_replace(a.ILR_NUMBER,'TRF(.)'),'()')) || '%(TRF%)') || ')'
               , 0, 35),
                A.LEASE_ID,
                A.COMPANY_ID,
                A.EST_IN_SVC_DATE,
                A.EXTERNAL_ILR,
                1,
                A.ILR_GROUP_ID,
                A.NOTES,
                L_REVISION,
                A.WORKFLOW_TYPE_ID,
                A.RATE_GROUP_ID,
                A.FUNDING_STATUS_ID
           from LS_ILR A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD class codes';
      insert into LS_ILR_CLASS_CODE
         (CLASS_CODE_ID, ILR_ID, "VALUE")
         select A.CLASS_CODE_ID, L_ILR_ID, a."VALUE"
           from LS_ILR_CLASS_CODE A
          where A.ILR_ID = A_ILR_ID;


	  L_STATUS := 'LOAD ilr_account';
      insert into LS_ILR_ACCOUNT
         (ILR_ID, INT_ACCRUAL_ACCOUNT_ID, INT_EXPENSE_ACCOUNT_ID, EXEC_ACCRUAL_ACCOUNT_ID,
          EXEC_EXPENSE_ACCOUNT_ID, CONT_ACCRUAL_ACCOUNT_ID, CONT_EXPENSE_ACCOUNT_ID,
          CAP_ASSET_ACCOUNT_ID, ST_OBLIG_ACCOUNT_ID, LT_OBLIG_ACCOUNT_ID, AP_ACCOUNT_ID, RES_DEBIT_ACCOUNT_ID, RES_CREDIT_ACCOUNT_ID,
          CURRENCY_GAIN_LOSS_DR_ACCT_ID, CURRENCY_GAIN_LOSS_CR_ACCT_ID, ST_DEFERRED_ACCOUNT_ID, LT_DEFERRED_ACCOUNT_ID,
          PREPAID_RENT_ACCOUNT_ID, INCENTIVE_ACCOUNT_ID, INIT_DIRECT_COST_ACCOUNT_ID,IMPAIR_EXPENSE_ACCOUNT_ID,IMPAIR_REVERSAL_ACCOUNT_ID,
          IMPAIR_ACCUM_AMORT_ACCOUNT_ID)
         select L_ILR_ID,
                A.INT_ACCRUAL_ACCOUNT_ID,
                A.INT_EXPENSE_ACCOUNT_ID,
                A.EXEC_ACCRUAL_ACCOUNT_ID,
                A.EXEC_EXPENSE_ACCOUNT_ID,
                A.CONT_ACCRUAL_ACCOUNT_ID,
                A.CONT_EXPENSE_ACCOUNT_ID,
                A.CAP_ASSET_ACCOUNT_ID,
                A.ST_OBLIG_ACCOUNT_ID,
                A.LT_OBLIG_ACCOUNT_ID,
                A.AP_ACCOUNT_ID,
                A.RES_DEBIT_ACCOUNT_ID,
                A.RES_CREDIT_ACCOUNT_ID,
                A.CURRENCY_GAIN_LOSS_DR_ACCT_ID,
                A.CURRENCY_GAIN_LOSS_CR_ACCT_ID,
                A.ST_DEFERRED_ACCOUNT_ID,
                A.LT_DEFERRED_ACCOUNT_ID,
                A.PREPAID_RENT_ACCOUNT_ID,
                A.INCENTIVE_ACCOUNT_ID,
                A.INIT_DIRECT_COST_ACCOUNT_ID,
                A.IMPAIR_EXPENSE_ACCOUNT_ID,
                A.IMPAIR_REVERSAL_ACCOUNT_ID,
                A.IMPAIR_ACCUM_AMORT_ACCOUNT_ID
           from LS_ILR_ACCOUNT A
          where A.ILR_ID = A_ILR_ID;

      L_STATUS := 'LOAD ilr_approval';
      insert into LS_ILR_APPROVAL
         (ILR_ID, REVISION, APPROVAL_TYPE_ID, APPROVAL_STATUS_ID)
         select L_ILR_ID, L_REVISION, A.APPROVAL_TYPE_ID, 1
           from LS_ILR_APPROVAL A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_options';
      insert into LS_ILR_OPTIONS
         (ILR_ID, REVISION, PURCHASE_OPTION_TYPE_ID, PURCHASE_OPTION_AMT, RENEWAL_OPTION_TYPE_ID,
          CANCELABLE_TYPE_ID, ITC_SW, PARTIAL_RETIRE_SW, SUBLET_SW, MUNI_BO_SW, INCEPTION_AIR,
          LEASE_CAP_TYPE_ID, TERMINATION_AMT, in_service_exchange_rate
          , payment_shift, deferred_rent_sw, depr_calc_method, schedule_payment_shift
          )
         select L_ILR_ID,
                L_REVISION,
                A.PURCHASE_OPTION_TYPE_ID,
                ROUND(A_PCT * A.PURCHASE_OPTION_AMT, 2),
                A.RENEWAL_OPTION_TYPE_ID,
                A.CANCELABLE_TYPE_ID,
                A.ITC_SW,
                A.PARTIAL_RETIRE_SW,
                A.SUBLET_SW,
                A.MUNI_BO_SW,
                A.INCEPTION_AIR,
                A.LEASE_CAP_TYPE_ID,
                ROUND(A_PCT * A.TERMINATION_AMT, 2),
				        in_service_exchange_rate,
                payment_shift,
                deferred_rent_sw,
                nvl(depr_calc_method,0),
                schedule_payment_shift
           from LS_ILR_OPTIONS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_amounts';
      insert into LS_ILR_AMOUNTS_SET_OF_BOOKS
         (ILR_ID, REVISION, SET_OF_BOOKS_ID, NET_PRESENT_VALUE, INTERNAL_RATE_RETURN, CAPITAL_COST,
          CURRENT_LEASE_COST, IS_OM)
         select L_ILR_ID,
                L_REVISION,
                A.SET_OF_BOOKS_ID,
                ROUND(A_PCT * NET_PRESENT_VALUE, 2),
                INTERNAL_RATE_RETURN,
                ROUND(A_PCT * CAPITAL_COST, 2),
                ROUND(A_PCT * CURRENT_LEASE_COST, 2),
                IS_OM
           from LS_ILR_AMOUNTS_SET_OF_BOOKS A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;

      L_STATUS := 'LOAD ilr_payment_term';
      insert into LS_ILR_PAYMENT_TERM
         (ILR_ID, REVISION, PAYMENT_TERM_ID, PAYMENT_TERM_TYPE_ID, PAYMENT_TERM_DATE,
          PAYMENT_FREQ_ID, NUMBER_OF_TERMS, EST_EXECUTORY_COST, PAID_AMOUNT, CONTINGENT_AMOUNT,
          CURRENCY_TYPE_ID, C_BUCKET_1, C_BUCKET_2, C_BUCKET_3, C_BUCKET_4, C_BUCKET_5, C_BUCKET_6,
          C_BUCKET_7, C_BUCKET_8, C_BUCKET_9, C_BUCKET_10, E_BUCKET_1, E_BUCKET_2, E_BUCKET_3,
          E_BUCKET_4, E_BUCKET_5, E_BUCKET_6, E_BUCKET_7, E_BUCKET_8, E_BUCKET_9, E_BUCKET_10,
          ESCALATION, ESCALATION_FREQ_ID, ESCALATION_PCT)
         select L_ILR_ID,
                L_REVISION,
                PAYMENT_TERM_ID,
                PAYMENT_TERM_TYPE_ID,
                PAYMENT_TERM_DATE,
                PAYMENT_FREQ_ID,
                NUMBER_OF_TERMS,
                ROUND(A_PCT * EST_EXECUTORY_COST, 2),
                ROUND(A_PCT * PAID_AMOUNT, 2),
                ROUND(A_PCT * CONTINGENT_AMOUNT, 2),
                CURRENCY_TYPE_ID,
                ROUND(A_PCT * C_BUCKET_1, 2),
                ROUND(A_PCT * C_BUCKET_2, 2),
                ROUND(A_PCT * C_BUCKET_3, 2),
                ROUND(A_PCT * C_BUCKET_4, 2),
                ROUND(A_PCT * C_BUCKET_5, 2),
                ROUND(A_PCT * C_BUCKET_6, 2),
                ROUND(A_PCT * C_BUCKET_7, 2),
                ROUND(A_PCT * C_BUCKET_8, 2),
                ROUND(A_PCT * C_BUCKET_9, 2),
                ROUND(A_PCT * C_BUCKET_10, 2),
                ROUND(A_PCT * E_BUCKET_1, 2),
                ROUND(A_PCT * E_BUCKET_2, 2),
                ROUND(A_PCT * E_BUCKET_3, 2),
                ROUND(A_PCT * E_BUCKET_4, 2),
                ROUND(A_PCT * E_BUCKET_5, 2),
                ROUND(A_PCT * E_BUCKET_6, 2),
                ROUND(A_PCT * E_BUCKET_7, 2),
                ROUND(A_PCT * E_BUCKET_8, 2),
                ROUND(A_PCT * E_BUCKET_9, 2),
                ROUND(A_PCT * E_BUCKET_10, 2),
                ESCALATION,
                ESCALATION_FREQ_ID,
                ESCALATION_PCT
           from LS_ILR_PAYMENT_TERM A
          where A.ILR_ID = A_ILR_ID
            and A.REVISION = L_CURRENT_REVISION;


      return L_ILR_ID;

   exception
      when others then
         L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

         return -1;
   end F_COPYILR;

   /*
   *   @@DESCRIPTION
   *      This function copies an asset into another leased asset
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ls_asset_id = the asset to copy
   *    number: in: a_pct. Percent to copy
   *    number: in: a_qty.  The quantity to copy
   *   @@RETURN
   *      number: The ls_asset_id added for success
   *            -1 for failure
   */
	FUNCTION f_copyasset(a_ls_asset_id NUMBER, a_pct NUMBER, a_qty NUMBER)
	  RETURN NUMBER IS

	  l_status     VARCHAR2(2000);
	  l_asset_id   NUMBER;
	  temp_deriver cr_deriver_type;
	  l_sqls       VARCHAR2(2000);
	  l_fields     VARCHAR2(2000);
	  l_count      NUMBER;

	BEGIN
	  pkg_pp_log.p_start_log(p_process_id => pkg_lease_common.f_getprocess());
	  pkg_pp_log.p_write_message('NEW Asset from: ' || to_char(a_ls_asset_id));

	  SELECT ls_asset_seq.nextval INTO l_asset_id FROM dual;
	  pkg_pp_log.p_write_message('TO Asset: ' || to_char(l_asset_id));

	  /* WMD */
	  l_status := 'LOAD asset';
	  INSERT INTO ls_asset
		(ls_asset_id, leased_asset_number, ls_asset_status_id, description,
		 long_description,
		 termination_penalty_amount, sale_proceed_amount, expected_life,
		 economic_life, fmv, property_group_id, utility_account_id,
		 bus_segment_id, sub_account_id, retirement_unit_id, work_order_id,
		 func_class_id, asset_location_id, in_service_date, serial_number,
		 actual_residual_amount, guaranteed_residual_amount, notes, company_id,
		 quantity, is_early_ret, tax_asset_location_id, department_id,
		 property_tax_date, property_tax_amount, used_yn_sw, tax_summary_id,
		 estimated_residual,
		 contract_currency_id)
		SELECT l_asset_id,
			   substr(TRIM(REPLACE(regexp_replace(a.leased_asset_number,
												   'TRF(.)'),
									'()')) || ' (TRF' ||
					   (SELECT 1 + COUNT(1)
						FROM   ls_asset ll
						WHERE  ll.leased_asset_number LIKE
							   TRIM(REPLACE(regexp_replace(a.leased_asset_number,
														   'TRF(.)'),
											'()')) || '%(TRF%)') || ')'
					   
					  ,
					   0,
					   35), 1,
			   substr('(TRF) ' || REPLACE(description, '(TRF) ', ''), 0, 35),
			   substr('(TRF) ' || REPLACE(long_description, '(TRF) ', ''),
					   0,
					   254),
			   round(a_pct * termination_penalty_amount, 2), sale_proceed_amount,
			   expected_life, economic_life, round(a_pct * fmv, 2),
			   property_group_id, utility_account_id, bus_segment_id,
			   sub_account_id, retirement_unit_id, work_order_id, func_class_id,
			   asset_location_id, in_service_date, serial_number,
			   round(a_pct * actual_residual_amount, 2),
			   round(a_pct * guaranteed_residual_amount, 2), notes, company_id,
			   a_qty, 0, tax_asset_location_id, department_id, property_tax_date,
			   property_tax_amount, used_yn_sw, tax_summary_id,
			   estimated_residual, contract_currency_id
		FROM   ls_asset a
		WHERE  a.ls_asset_id = a_ls_asset_id;

	  l_status := 'LOAD class codes';

	  INSERT INTO ls_asset_class_code
		(class_code_id, ls_asset_id, "VALUE")
		SELECT a.class_code_id, l_asset_id, a."VALUE"
		FROM   ls_asset_class_code a
		WHERE  a.ls_asset_id = a_ls_asset_id;

	  l_status := 'LOAD cr deriver control';
	  l_fields := 'SCO_BILLING_TYPE_ID, VALIDATION_MESSAGE, DESCRIPTION, PERCENT, TYPE,  LS_JE_TRANS_TYPE, ';

	  SELECT COUNT(1)
	  INTO   l_count
	  FROM   all_tab_columns
	  WHERE  table_name = 'CR_DERIVER_CONTROL'
	  AND    column_name = 'TAX_LOCAL_ID';

	  IF l_count > 0 THEN
		l_fields := l_fields || 'TAX_LOCAL_ID, ';
	  END IF;

	  FOR col IN (SELECT pkg_pp_common.f_clean_string(description) element_column
				  FROM   cr_elements
				  WHERE  element_column IS NOT NULL) LOOP
		l_fields := l_fields || col.element_column || ', ';
	  END LOOP;
	  l_fields := rtrim(l_fields, ', ');
	  l_sqls   := 'insert into CR_DERIVER_CONTROL (STRING, ID, ' || l_fields || ')';
	  l_sqls   := l_sqls || ' select ''' || to_char(l_asset_id) ||
				  '''|| '':'' || ls_je_trans_type

					   , costrepository.nextval, ' || l_fields;
	  l_sqls   := l_sqls ||
				  ' from CR_DERIVER_CONTROL where lower(type) in (''lessee'', ''lessee offset'')';
	  l_sqls   := l_sqls ||
				  ' and substr(string, 0, instr(string, '':'') -1) = ''' ||
				  to_char(a_ls_asset_id) || '''';

	  EXECUTE IMMEDIATE l_sqls;

	  --Loop over and create and load components to new asset
	  FOR i IN (SELECT component_id
				FROM   ls_component
				WHERE  ls_asset_id = a_ls_asset_id) LOOP
		l_status := 'Copying Components';
		INSERT INTO ls_component
		  (component_id, ls_comp_status_id, company_id, date_received,
		   description, long_description, serial_number, po_number, amount,
		   ls_asset_id, interim_interest, user_id, time_stamp, prop_tax_exempt,
		   manufacturer, model)
		  SELECT ls_component_seq.nextval, ls_comp_status_id, company_id,
				 date_received, description, long_description, serial_number,
				 po_number, amount, l_asset_id, interim_interest, user_id,
				 time_stamp, prop_tax_exempt, manufacturer, model
		  FROM   ls_component
		  WHERE  ls_asset_id = a_ls_asset_id
		  AND    component_id = i.component_id;
		  
		l_status := 'Copying Component Charges';
		INSERT INTO ls_component_charge
		  (id, component_id, interim_interest_start_date, invoice_date,
		   invoice_number, amount)
		  SELECT (SELECT nvl(MAX(ls_component_charge.id), 0)
				   FROM   ls_component_charge) + rownum,
				 ls_component_seq.currval AS component_id,
				 cc.interim_interest_start_date, cc.invoice_date,
				 cc.invoice_number, cc.amount
		  FROM   ls_component_charge cc
		  WHERE  cc.component_id = i.component_id;
	  END LOOP;

	  l_status := 'Updating Asset FMV';
	  UPDATE ls_asset
	  SET    fmv =
			  (SELECT nvl(SUM(cc.amount), 0)
			   FROM   ls_component_charge cc, ls_component lc
			   WHERE  cc.component_id = lc.component_id
			   AND    lc.ls_asset_id = a_ls_asset_id
			   AND    interim_interest_start_date IS NOT NULL)
	  WHERE  ls_asset_id = l_asset_id
	  AND    EXISTS
	   (SELECT 1
			  FROM   ls_asset a, ls_ilr i, ls_lease l, ls_lease_group g
			  WHERE  a_ls_asset_id = a.ls_asset_id
			  AND    a.ilr_id = i.ilr_id
			  AND    i.lease_id = l.lease_id
			  AND    l.lease_group_id = g.lease_group_id
			  AND    g.require_components = 1);

	  pkg_pp_log.p_write_message('Ending F_COPYASSET');
	  RETURN l_asset_id;
	EXCEPTION
	  WHEN OTHERS THEN
		pkg_pp_log.p_write_message('ERROR!');
		l_status := substr(l_status || ': ' || SQLERRM, 1, 2000);
		pkg_pp_log.p_write_message(l_status);
		RETURN - 1;
	END f_copyasset;

   function F_GETTAXES(A_ILR_ID number)
                     return varchar2 is
      L_STATUS varchar2(400);
      L_ASSETS PKG_LEASE_CALC.NUM_ARRAY;

   begin

      L_STATUS := 'Getting taxes for ILR_ID: '||to_char(A_ILR_ID);

      --create an array of assets under this ILR that do not already have a tax summary
      select LS_ASSET_ID
      bulk collect into L_ASSETS
      from LS_ASSET
      where ILR_ID = A_ILR_ID;

      --map the assets under this ILR to local taxes
      PKG_LEASE_ASSET_POST.P_GETTAXES(L_ASSETS);

      return 'OK';
   exception
      when others then
         return L_STATUS;
   end F_GETTAXES;

procedure P_CALC_II(A_ILR_ID number, A_REVISION in number, A_START_LOG in number := null) is
	II_AMOUNT number;
	type COMPONENT_TABLE is table of LS_COMPONENT%rowtype;
	L_COMPONENT_TABLE COMPONENT_TABLE;
	DAYS_IN_YEAR number;
	CUT_OFF_DAY number;
	IN_SVC_DATE date;
	i number;
	II_DATE date;
	II_MONTH date;
	DAYS_IN_MONTH_SW number;
	type COMPONENT_MONTHLY_II is table of LS_COMPONENT_MONTHLY_II_STG%rowtype;
	L_REC LS_COMPONENT_MONTHLY_II_STG%rowtype;
	L_COMPONENT_MONTHLY_II COMPONENT_MONTHLY_II := COMPONENT_MONTHLY_II();
	L_PROC_ID number;
   begin
    PKG_PP_ERROR.SET_MODULE_NAME('PKG_LEASE_ILR.P_CALC_II');

	--0 = don't start a log in here, any other value will start a new log
	if a_start_log <> 0 then
		select PROCESS_ID
		into L_PROC_ID
		from PP_PROCESSES
		where lower(DESCRIPTION) = 'lessee calculations';

		PKG_PP_LOG.P_START_LOG(L_PROC_ID);
	end if;
	/* CJS 4/27/15 Move before checking for anything to calculate; if in service date changed, rows might not get deleted */
		--clear out old records
	delete from LS_COMPONENT_MONTHLY_II_STG
	where COMPONENT_ID in (select LC.COMPONENT_ID
		from LS_COMPONENT LC
		join LS_ASSET LA on LC.LS_ASSET_ID = LA.LS_ASSET_ID
		join LS_ILR LI on LI.ILR_ID = LA.ILR_ID
		where LI.ILR_ID = A_ILR_ID);

  delete from ls_calc_ii_stg
  where component_id in (select component_id from ls_component where ls_asset_id in (select ls_asset_id from ls_asset where ilr_id = A_ILR_ID));

  insert into ls_calc_ii_stg
  (id, month, component_id, interim_interest_start_date, current_lease_cost, est_in_svc_date, cut_off_day, ii_rate, days_in_month_basis, days_in_year_basis)
  With all_months(month) as
  (select min(trunc(interim_interest_start_date,'month')) month
  from ls_component_charge cc, ls_component lc, ls_asset la
  where cc.component_id = lc.component_id
    and lc.ls_asset_id = la.ls_asset_id
    and la.ilr_id = A_ILR_ID
  UNION ALL
  select add_months(all_months.month,1) month
    from dual
      inner join all_months on months_between(
      (select trunc(est_in_svc_date, 'month') from ls_ilr where ilr_id = A_ILR_ID)
      , month) > 0)
  select
    cc.id,
    all_months.month,
    lc.component_id,
    cc.interim_interest_start_date,
    cc.amount,
    trunc(ilr.est_in_svc_date, 'month'),
    ll.cut_off_day,
    lir.rate,
    decode(ll.days_in_month_sw, 1, 30, extract(day from last_day(all_months.month))),
    ll.days_in_year
  from ls_component_charge cc, ls_component lc, ls_asset la, ls_ilr ilr, ls_lease ll, ls_lease_interim_rates lir, all_months
  where cc.component_id = lc.component_id
    and lc.ls_asset_id = la.ls_asset_id
    and la.ilr_id = ilr.ilr_id
    and ilr.lease_id = ll.lease_id
    and ll.lease_id = lir.lease_id
    and cc.interim_interest_start_date is not null
    and ilr.ilr_id = A_ILR_ID
    and all_months.month between trunc(cc.interim_interest_start_date,'month') and trunc(ilr.est_in_svc_date, 'month')
    and lir.month =
			  (
				  select max(l2.month)
				  from ls_lease_interim_rates l2
				  where l2.lease_id = lir.lease_id
				  and l2.month <= last_day(all_months.month)
			);

  /* Set interim interest month */
  update ls_calc_ii_stg
  set days_of_interest = days_in_month_basis - extract(day from interim_interest_start_date) + 1,
      roll_forward_amount = case when cut_off_day <=extract(day from interim_interest_start_date) then
      ii_rate * (1/days_in_year_basis) * nvl(days_in_month_basis - extract(day from interim_interest_start_date) + 1,0) * nvl(current_lease_cost,0) else 0 end,
      amount = case when cut_off_day <=extract(day from interim_interest_start_date) then 0 else
      ii_rate * (1/days_in_year_basis) * nvl(days_in_month_basis - extract(day from interim_interest_start_date) + 1,0) * nvl(current_lease_cost,0) end
  where trunc(interim_interest_start_date,'month') = month;


  /* Set other months other than in service */
  update ls_calc_ii_stg
  set days_of_interest = days_in_month_basis
  where trunc(est_in_svc_date,'month') > month
    and trunc(interim_interest_start_date,'month') <> month;

  /* Set Balances On Interim Row */
  update ls_calc_ii_stg
  set amount =  ii_rate * (1/days_in_year_basis) * nvl(days_of_interest,0) * nvl(current_lease_cost,0)
  where trunc(interim_interest_start_date,'month') <> month;



  /* Roll forward as necessary */
  merge into ls_calc_ii_stg a
  using(
  select id, add_months(month, 1) month, roll_forward_amount
  from ls_calc_ii_stg
  where nvl(roll_forward_amount,0) <> 0) b
  on (a.id = b.id and a.month = b.month)
  when matched then update set a.amount = nvl(a.amount,0) + nvl(b.roll_forward_amount,0);



  insert into LS_COMPONENT_MONTHLY_II_STG
  (component_id, month, amount)
  select
  component_id, month, sum(nvl(amount,0))
  from ls_calc_ii_stg
  group by component_id, month
  having sum(amount) <> 0;

	-- update ls_component
	update LS_COMPONENT LC
	set LC.INTERIM_INTEREST =
	(
		select nvl(sum(AA.AMOUNT),0)
		from LS_COMPONENT_MONTHLY_II_STG AA
		where AA.COMPONENT_ID = LC.COMPONENT_ID
	)
	where exists
	(
		select 1
		from LS_ASSET LA
		where LA.ILR_ID = A_ILR_ID
		and LC.LS_ASSET_ID = LA.LS_ASSET_ID
	);

  /* WMD we need to reset the interest in the in-service month so that we don't double it up when we re-run calcs */
merge into ls_calc_ii_stg a using (
select z.id, lc.ls_asset_id, z.month, ilrpt.paid_amount
from ls_asset la, ls_component lc, ls_calc_ii_stg z, ls_ilr ilr,
     (select *
      from ls_ilr_payment_term
      where ilr_id = A_ILR_ID
        and revision = A_REVISION
        and (ilr_id, payment_term_id) in (
                                          select ilr.ilr_id,  min(ilrpt.payment_term_id)
                                          from ls_ilr ilr, ls_ilr_payment_term ilrpt
                                          where ilr.ilr_id = ilrpt.ilr_id
                                            and trunc(ilr.est_in_svc_date,'month') >= trunc(ilrpt.payment_term_date, 'month')
                                            and ilrpt.revision = A_REVISION
                                          group by ilr.ilr_id)) ILRPT
where z.component_id = lc.component_id
  and lc.ls_asset_id = la.ls_asset_id
  and la.ilr_id = ilrpt.ilr_id
  and la.ilr_id = A_ILR_ID
  and la.ilr_id = ilr.ilr_id
  and z.month >=trunc(ilr.est_in_svc_date,'month')) b
on (a.id = b.id and a.month = b.month)
when matched then update set payment_for_month = b.paid_amount;

merge into ls_calc_ii_stg a using (
select z.id, lc.ls_asset_id, z.month, las.principal_accrual
from ls_asset la, ls_component lc, ls_calc_ii_stg z, ls_ilr ilr,
     ls_asset_schedule las
where z.component_id = lc.component_id
  and lc.ls_asset_id = la.ls_asset_id
  and la.ilr_id = A_ILR_ID
  and la.ilr_id = ilr.ilr_id
  and z.month = las.month
  and las.revision = A_REVISION
  and las.ls_asset_id = la.ls_asset_id
  and las.set_of_books_id = 1 -- Always pull from set of books 1
  and trunc(z.month,'month') = trunc(z.est_in_svc_date,'month')
  ) b
on (a.id = b.id and a.month = b.month)
when matched then update set principal_for_month = b.principal_accrual;

merge into ls_asset_schedule a using(
select distinct lc.ls_asset_id, z.month, A_REVISION as revision, nvl(principal_for_month,0) principal_for_month,
        nvl(payment_for_month,0) * la.ratio payment_for_month
from ls_calc_ii_stg z, ls_component lc, ls_asset a,
     (select ls_asset_id, ratio_to_report(fmv) over(partition by ilr_id) ratio
      from ls_asset
      where ilr_id = A_ILR_ID
        and ls_asset_status_id = 3) la
where nvl(z.payment_for_month,0) <> 0
  and z.component_id = lc.component_id
  and lc.ls_asset_id = a.ls_asset_id
  and z.month >= (select min(gl_posting_mo_yr) from ls_process_control where company_id = a.company_id and lam_closed is null)
  and lc.ls_asset_id = la.ls_asset_id) b
on (a.ls_asset_id = b.ls_asset_id and a.month = b.month and a.revision = b.revision)
when matched then update set a.interest_accrual = nvl(b.payment_for_month,0) - nvl(b.principal_for_month,0),
                             a.interest_paid = nvl(b.payment_for_month,0) - nvl(b.principal_for_month,0);

	/* CJS 2/16/15 Updating schedules with amounts */
	update LS_ASSET_SCHEDULE LAS
	set (LAS.INTEREST_ACCRUAL, LAS.INTEREST_PAID) = (
	       select nvl(case when las.month >= trunc(ilr.est_in_svc_date,'month') then las.interest_accrual else 0 end + sum(cm.AMOUNT),0),
                nvl(case when las.month >= trunc(ilr.est_in_svc_date,'month') then las.interest_paid else 0 end + sum(cm.amount),0)
	       from ls_component_monthly_ii_stg cm, ls_component c, ls_asset la, ls_ilr ilr
	       where cm.component_id = c.component_id
	       and cm.month = las.month
	       and c.ls_asset_id = las.ls_asset_id
         and c.ls_asset_id = la.ls_asset_id
         and la.ilr_id = ilr.ilr_id
         and cm.month >= (select min(gl_posting_mo_yr) from ls_process_control where company_id = la.company_id and lam_closed is null)
         and la.ilr_id = A_ILR_ID
         group by ilr.est_in_svc_date)
	where las.ls_asset_id in (Select ls_asset_id from ls_asset where ilr_id = A_ILR_ID)
    and exists(
	   select 1
	   from ls_asset la, ls_component c, ls_component_monthly_ii_stg cm
	   where la.ilr_id = A_ILR_ID
	   and la.ls_asset_id = las.ls_asset_id
	   and la.ls_asset_id = c.ls_asset_id
	   and c.component_id = cm.component_id
     and cm.month>=(select min(gl_posting_mo_yr) from ls_process_control where company_id = la.company_id and lam_closed is null)
	   and cm.month = las.month
	   and las.revision = A_REVISION);

	--Updating ILR schedule
	update LS_ILR_SCHEDULE LIS
	set (LIS.INTEREST_ACCRUAL, LIS.INTEREST_PAID) = (
	       select nvl(sum(INTEREST_ACCRUAL),0), nvl(sum(INTEREST_ACCRUAL),0)
	       from ls_asset_schedule las, ls_asset la
	       where las.ls_asset_id = la.ls_asset_id
		   and las.month = lis.month
		   and las.revision = A_REVISION
		   and las.set_of_books_id = lis.set_of_books_id
		   and la.ilr_id = A_ILR_ID)
	where LIS.ILR_ID = A_ILR_ID
    and LIS.REVISION = A_REVISION
    and exists(
	   select 1
	   from ls_asset la, ls_component c, ls_component_monthly_ii_stg cm, ls_ilr ilr
	   where ilr.ilr_id = A_ILR_ID
	   and la.ilr_id = ilr.ilr_id
	   and la.ilr_id = lis.ilr_id
	   and la.ls_asset_id = c.ls_asset_id
	   and c.component_id = cm.component_id
	   and cm.month = lis.month
	   and lis.revision = A_REVISION);

  delete from ls_calc_ii_stg_arc
  where (id, month) in (select id, month from ls_calc_ii_stg);

  insert into ls_calc_ii_stg_arc
  select * from ls_calc_ii_stg;

  delete from ls_calc_ii_stg;

	PKG_PP_ERROR.REMOVE_MODULE_NAME;
   exception
    when others then
      PKG_PP_ERROR.RAISE_ERROR('ERROR','Failed Operation');
   end P_CALC_II;
/* CJS Adding retirement revision function */
/*
   *   @@DESCRIPTION
   *      This function create a new revision for the passed in asset/ILR being retired/transferred
   *      It will use the current revision information as the starting point based in ls_ilr.current_revision
   *      NO COMMITS or ROLLBACK in this function
   *   @@PARAMS
   *      number: in: a_ilr_id = the ilr_id to create a retired revision
   *      number: in: a_ls_asset_id = the ls_asset_id to create a retired revision
   *      date:   in: a_month = the retirement/transfer month to use
   *      number: in: a_flag = flag indicating transfer or retirement; 0 is retirement, 1 is transfer)
   *   @@RETURN
   *      number: 1 for success
   *             -1 for failure
   */
   function F_RETIREREVISION(A_ILR_ID number, A_LS_ASSET_ID number, A_MONTH date, A_FLAG NUMBER, A_ACCT_MONTH date) return number is
      L_STATUS           varchar2(2000);
      L_REVISION         number;
      L_CURRENT_REVISION number;
      L_PREV_REVISION    number;
      L_RTN				 number;
      L_RETIRE_REVISION	 number;
      L_NEW_REV          number;
      L_SQLS			 varchar2(2000);
      L_PEND_TRANS_COUNTER NUMBER:=-1;
   begin
     PKG_PP_LOG.P_START_LOG(P_PROCESS_ID => PKG_LEASE_COMMON.F_GETPROCESS());
     PKG_PP_LOG.P_WRITE_MESSAGE('RETIREMENT Revision for: ' || TO_CHAR(A_ILR_ID));
	   L_STATUS := 'Get current revision';
     select CURRENT_REVISION into L_CURRENT_REVISION from LS_ILR where ILR_ID = A_ILR_ID;
     PKG_PP_LOG.P_WRITE_MESSAGE('   Current Revision: ' || TO_CHAR(L_CURRENT_REVISION));

     L_STATUS := 'Check Pending Transactions';
     --pull this now for later on
     SELECT COUNT(1)
       INTO L_PEND_TRANS_COUNTER
       FROM PEND_TRANSACTION
      WHERE LDG_ASSET_ID in
           (SELECT ASSET_ID
              FROM LS_CPR_ASSET_MAP
             WHERE LS_ASSET_ID IN
                   (SELECT LS_ASSET_ID
                      FROM LS_ASSET
                     WHERE ILR_ID = A_ILR_ID
                       AND LS_ASSET_ID <> A_LS_ASSET_ID))
        and trim(activity_code) = 'URGL';
     PKG_PP_LOG.P_WRITE_MESSAGE('L_PEND_TRANS_COUNTER:'||L_PEND_TRANS_COUNTER);

     L_STATUS := 'Get Existing Retirement Revision';
     PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
     -- If processing through multiple assets being retired on a single ILR, find the retire revision that was already created
     SELECT Nvl(Max(b.revision),-1)
       into L_RETIRE_REVISION
       FROM ls_ilr a
       left OUTER join ls_ilr_approval b ON a.ilr_id = b.ilr_id
                                        AND Nvl(b.revision_desc,'null') = 'LEASERETIRE'
                                        AND b.approval_status_id <> 3
      WHERE a.ILR_ID = A_ILR_ID;

     -- Check to see if there are other assets associated with the ILR that haven't been retired
     L_RTN:=0;
     select count(1)
       into L_RTN
       from LS_ASSET
      where ILR_ID = A_ILR_ID
        and LS_ASSET_STATUS_ID <> 4;

     if A_FLAG = 0 and L_RTN > 0 and L_RETIRE_REVISION = -1 then --if more than 1 asset, and no existing retirement revision, create a new one
       L_NEW_REV := F_COPYREVISION(A_ILR_ID, L_CURRENT_REVISION);

       if L_NEW_REV = -1 then
         return -1;
       else
         update ls_ilr_approval
            set revision_desc = 'LEASERETIRE', approval_status_id = 2
          where ilr_id = A_ILR_ID
            and revision = L_NEW_REV;

         L_RETIRE_REVISION := L_NEW_REV;
       end if;
       PKG_PP_LOG.P_WRITE_MESSAGE('   Retirement Revision: ' || TO_CHAR(L_RETIRE_REVISION));
	   
     end if;

     L_STATUS := 'Get previous revision';
	   select max(REVISION)
	     into L_PREV_REVISION
	     from LS_ILR_APPROVAL
	    where ILR_ID = A_ILR_ID
	      and REVISION <> L_CURRENT_REVISION
	      and (APPROVAL_STATUS_ID in (3, 6)
             or (APPROVAL_TYPE_ID = (select WORKFLOW_TYPE_ID
                                       from WORKFLOW_TYPE
                                      where lower(description) = 'auto approve')
                 and APPROVAL_STATUS_ID  = 1));

	   if L_RTN > 0 then /* Other assets associated with ILR besides retired asset exist */
		   if A_FLAG = 0 then /* Retirement */
			   L_STATUS:= 'Adding retired revision to current revision for ILR with other assets attached';
			   PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
			   /* Transfer is included in this portion; not possible in other scenario */

         --Clear out any previous rows to prevent PK errors
         delete from ls_asset_schedule where ls_asset_id = A_LS_ASSET_ID and revision = L_RETIRE_REVISION;

         PKG_PP_LOG.P_WRITE_MESSAGE(sql%rowcount || ' rows deleted from asset schedule for revision ' || L_RETIRE_REVISION);
         insert into LS_ASSET_SCHEDULE(
          LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          PARTIAL_MONTH_PERCENT, BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
          REMAINING_PRINCIPAL, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,CONTINGENT_ADJUST,CURRENT_LEASE_COST,END_NET_ROU_ASSET,EXECUTORY_ADJUST,
          BEGIN_ACCUM_IMPAIR, IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR, incentive_cap_amount)
         select A.LS_ASSET_ID, L_RETIRE_REVISION, A.SET_OF_BOOKS_ID, A.month, A.RESIDUAL_AMOUNT, A.TERM_PENALTY, A.BPO_PRICE,
             BEG_CAPITAL_COST, decode(month, A_ACCT_MONTH, 0, A.END_CAPITAL_COST),
             A.BEG_OBLIGATION,
             decode(month, A_ACCT_MONTH, 0, A.END_OBLIGATION),
             A.BEG_LT_OBLIGATION,
             decode(month, A_ACCT_MONTH, 0, A.END_LT_OBLIGATION),
             A.BEG_LIABILITY,
             decode(month, A_ACCT_MONTH, 0, A.END_LIABILITY),
             A.BEG_LT_LIABILITY,
             decode(month, A_ACCT_MONTH, 0, A.END_LT_LIABILITY),
             A.INTEREST_ACCRUAL, A.PRINCIPAL_ACCRUAL, A.INTEREST_PAID, A.PRINCIPAL_PAID,
             A.CONTINGENT_ACCRUAL1, A.CONTINGENT_ACCRUAL2, A.CONTINGENT_ACCRUAL3, A.CONTINGENT_ACCRUAL4,
             A.CONTINGENT_ACCRUAL5, A.CONTINGENT_ACCRUAL6, A.CONTINGENT_ACCRUAL7, A.CONTINGENT_ACCRUAL8,
             A.CONTINGENT_ACCRUAL9, A.CONTINGENT_ACCRUAL10, A.EXECUTORY_ACCRUAL1, A.EXECUTORY_ACCRUAL2,
             A.EXECUTORY_ACCRUAL3, A.EXECUTORY_ACCRUAL4, A.EXECUTORY_ACCRUAL5, A.EXECUTORY_ACCRUAL6,
             A.EXECUTORY_ACCRUAL7, A.EXECUTORY_ACCRUAL8, A.EXECUTORY_ACCRUAL9, A.EXECUTORY_ACCRUAL10,
             A.CONTINGENT_PAID1, A.CONTINGENT_PAID2, A.CONTINGENT_PAID3, A.CONTINGENT_PAID4, A.CONTINGENT_PAID5,
             A.CONTINGENT_PAID6, A.CONTINGENT_PAID7, A.CONTINGENT_PAID8, A.CONTINGENT_PAID9, A.CONTINGENT_PAID10,
             A.EXECUTORY_PAID1, A.EXECUTORY_PAID2, A.EXECUTORY_PAID3, A.EXECUTORY_PAID4, A.EXECUTORY_PAID5,
             A.EXECUTORY_PAID6, A.EXECUTORY_PAID7, A.EXECUTORY_PAID8, A.EXECUTORY_PAID9, A.EXECUTORY_PAID10,
             A.IS_OM, A.BEG_DEFERRED_RENT, A.DEFERRED_RENT, A.END_DEFERRED_RENT, A.BEG_ST_DEFERRED_RENT, A.END_ST_DEFERRED_RENT,
             A.PRINCIPAL_REMEASUREMENT, A.LIABILITY_REMEASUREMENT, A.INCENTIVE_AMOUNT, A.INITIAL_DIRECT_COST,
             A.INCENTIVE_MATH_AMOUNT, A.IDC_MATH_AMOUNT,
             A.PREPAY_AMORTIZATION, A.BEG_PREPAID_RENT, A.PREPAID_RENT, A.END_PREPAID_RENT, A.ROU_ASSET_REMEASUREMENT,
             A.PARTIAL_TERM_GAIN_LOSS, A.ADDITIONAL_ROU_ASSET,
             A.PARTIAL_MONTH_PERCENT, A.BEG_ARREARS_ACCRUAL, A.ARREARS_ACCRUAL, A.END_ARREARS_ACCRUAL,
             A.REMAINING_PRINCIPAL,
             decode(month, A_ACCT_MONTH,
                           -1 * ((A.END_OBLIGATION -
                                 CASE WHEN LS_ASSET.ESTIMATED_RESIDUAL = 0
                                       AND lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', LS_ASSET.COMPANY_ID))) = 'yes' THEN
                                   NVL(LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT,0)
                                 ELSE
                                   CASE WHEN lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', LS_ASSET.COMPANY_ID))) = 'no' THEN
                                      0
                                   else
                                     round(LS_ASSET.ESTIMATED_RESIDUAL * LS_ASSET.FMV, 2)
                                   END
                                 end
                                - NVL(A.END_LT_OBLIGATION, 0)) - NVL(A.OBLIGATION_RECLASS, 0)),
                           A.ST_OBLIGATION_REMEASUREMENT) ST_OBLIGATION_REMEASUREMENT,
             decode(month, A_ACCT_MONTH,
                           -1 * ((A.END_LT_OBLIGATION -
                                 CASE WHEN LS_ASSET.ESTIMATED_RESIDUAL = 0
                                       AND lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', LS_ASSET.COMPANY_ID))) = 'yes' THEN
                                   NVL(LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT,0)
                                 ELSE
                                   CASE WHEN lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', LS_ASSET.COMPANY_ID))) = 'no' THEN
                                     0
                                   else
                                     round(LS_ASSET.ESTIMATED_RESIDUAL * LS_ASSET.FMV, 2)
                                   end
                                 end) + NVL(A.OBLIGATION_RECLASS, 0)),
                           A.LT_OBLIGATION_REMEASUREMENT) LT_OBLIGATION_REMEASUREMENT,
             decode(month, A_ACCT_MONTH, 0, A.OBLIGATION_RECLASS) OBLIGATION_RECLASS,
             decode(month, A_ACCT_MONTH,
                           -1 * ((NVL(A.END_LT_LIABILITY, 0) - NVL(A.END_LT_OBLIGATION, 0)- NVL(A.UNACCRUED_INTEREST_RECLASS, 0))
                                  - (NVL(A.END_LIABILITY, 0) - NVL(A.END_OBLIGATION, 0))),
                           A.UNACCRUED_INTEREST_REMEASURE) UNACCRUED_INTEREST_REMEASURE,
             decode(month, A_ACCT_MONTH, 0, A.UNACCRUED_INTEREST_RECLASS) UNACCRUED_INTEREST_RECLASS,
             A.BEG_NET_ROU_ASSET,A.CONTINGENT_ADJUST,
             decode(month, A_ACCT_MONTH, 0, A.CURRENT_LEASE_COST),
             decode(month, A_ACCT_MONTH, 0, A.END_NET_ROU_ASSET),
             A.EXECUTORY_ADJUST,
             A.BEGIN_ACCUM_IMPAIR, A.IMPAIRMENT_ACTIVITY,
             decode(month, A_ACCT_MONTH, 0, A.END_ACCUM_IMPAIR),
			 A.incentive_cap_amount
          from LS_ASSET_SCHEDULE A
          join LS_ASSET on LS_ASSET.LS_ASSET_ID = A.LS_ASSET_ID
         where A.LS_ASSET_ID = A_LS_ASSET_ID
           and A.REVISION = L_CURRENT_REVISION
           and A.MONTH <= A_ACCT_MONTH;

         IF A_MONTH < A_ACCT_MONTH THEN
           L_STATUS := 'Update ending amounts on RETIREMENT asset schedule';

           L_SQLS := 'update LS_ASSET_SCHEDULE
                         set END_CAPITAL_COST = 0,
                             END_OBLIGATION = 0,
                             END_LT_OBLIGATION = 0,
                             END_LIABILITY = 0,
                             END_LT_LIABILITY = 0,
                             INTEREST_ACCRUAL = 0,
                             INTEREST_PAID = 0,
                             PRINCIPAL_ACCRUAL = 0,
                             PRINCIPAL_PAID = 0,
                             END_NET_ROU_ASSET = 0,
                             END_ACCUM_IMPAIR = 0,
                             CURRENT_LEASE_COST = 0';

           for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
           loop
             L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
           end loop;

           L_SQLS := L_SQLS || ' where ls_asset_id = '||A_LS_ASSET_ID||' and revision = '||L_RETIRE_REVISION;
           L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_ACCT_MONTH,'yyyymm')||',''yyyymm'')';

           execute immediate L_SQLS;
         end if;

         L_STATUS := 'Update asset with current revision';
         update LS_ASSET
         set APPROVED_REVISION = L_RETIRE_REVISION
         where LS_ASSET_ID = A_LS_ASSET_ID;

         L_STATUS := 'Deleting from ls_depr_forecast';
         delete from ls_depr_forecast
          where ls_asset_id = A_LS_ASSET_ID
          and revision = L_RETIRE_REVISION;

         L_STATUS := 'Copying prior rows in from ls_depr_forecast';
         insert into ls_depr_forecast (
           LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, BEGIN_RESERVE, DEPR_EXPENSE, DEPR_EXP_ALLOC_ADJUST, END_RESERVE,
           DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, PARTIAL_MONTH_PERCENT)
         select LS_ASSET_ID, L_RETIRE_REVISION, SET_OF_BOOKS_ID, MONTH,
                BEGIN_RESERVE,
                CASE WHEN A_MONTH < A_ACCT_MONTH AND MONTH = A_ACCT_MONTH then 0 else DEPR_EXPENSE END DEPR_EXPENSE,
                DEPR_EXP_ALLOC_ADJUST,
                decode(month, A_ACCT_MONTH, 0, END_RESERVE) END_RESERVE,
                depr_group_id, mid_period_method, mid_period_conv, partial_month_percent
           from ls_depr_forecast
          where ls_asset_id = A_LS_ASSET_ID
            and revision = L_CURRENT_REVISION
            and month <= A_ACCT_MONTH;

         L_STATUS:='Clear ILR Asset Map';
         PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
         delete from LS_ILR_ASSET_MAP
         where LS_ASSET_ID = A_LS_ASSET_ID
         and ILR_ID = A_ILR_ID
         and REVISION = L_RETIRE_REVISION;

         --there is a scenario where you could retire (e.g.) 2 of 3 assets and leave the 3rd, and now need to have this be the real revision
         IF L_PEND_TRANS_COUNTER = 0 THEN
           L_STATUS := 'CLEAR ilr_schedule (pend trans check)';
           PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
           delete from LS_ILR_SCHEDULE
           where ILR_ID = A_ILR_ID
           and REVISION = L_RETIRE_REVISION;

           L_STATUS := 'LOAD ilr_schedule';
           PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
           insert into LS_ILR_SCHEDULE
           (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
            BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
            END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
            INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
            CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
            CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
            CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
            EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
            EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
            CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
            CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
            EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
            EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
            IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
            PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
            PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_MATH_AMOUNT,
            IDC_MATH_AMOUNT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
            BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
            ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
            UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS, BEG_NET_ROU_ASSET,
            CONTINGENT_ADJUST, CURRENT_LEASE_COST,END_NET_ROU_ASSET, EXECUTORY_ADJUST,incentive_cap_amount)
           select A.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.month,
              SUM(S.RESIDUAL_AMOUNT), SUM(S.TERM_PENALTY), SUM(S.BPO_PRICE),
              SUM(S.BEG_CAPITAL_COST), SUM(S.END_CAPITAL_COST), SUM(S.BEG_OBLIGATION), SUM(S.END_OBLIGATION), SUM(S.BEG_LT_OBLIGATION),
              SUM(S.END_LT_OBLIGATION), SUM(S.BEG_LIABILITY), SUM(S.END_LIABILITY), SUM(S.BEG_LT_LIABILITY), SUM(S.END_LT_LIABILITY),
              SUM(S.INTEREST_ACCRUAL), SUM(S.PRINCIPAL_ACCRUAL), SUM(S.INTEREST_PAID), SUM(S.PRINCIPAL_PAID),
              SUM(S.CONTINGENT_ACCRUAL1), SUM(S.CONTINGENT_ACCRUAL2), SUM(S.CONTINGENT_ACCRUAL3), SUM(S.CONTINGENT_ACCRUAL4),
              SUM(S.CONTINGENT_ACCRUAL5), SUM(S.CONTINGENT_ACCRUAL6), SUM(S.CONTINGENT_ACCRUAL7), SUM(S.CONTINGENT_ACCRUAL8),
              SUM(S.CONTINGENT_ACCRUAL9), SUM(S.CONTINGENT_ACCRUAL10), SUM(S.EXECUTORY_ACCRUAL1), SUM(S.EXECUTORY_ACCRUAL2),
              SUM(S.EXECUTORY_ACCRUAL3), SUM(S.EXECUTORY_ACCRUAL4), SUM(S.EXECUTORY_ACCRUAL5), SUM(S.EXECUTORY_ACCRUAL6),
              SUM(S.EXECUTORY_ACCRUAL7), SUM(S.EXECUTORY_ACCRUAL8), SUM(S.EXECUTORY_ACCRUAL9), SUM(S.EXECUTORY_ACCRUAL10),
              SUM(S.CONTINGENT_PAID1), SUM(S.CONTINGENT_PAID2), SUM(S.CONTINGENT_PAID3), SUM(S.CONTINGENT_PAID4), SUM(S.CONTINGENT_PAID5),
              SUM(S.CONTINGENT_PAID6), SUM(S.CONTINGENT_PAID7), SUM(S.CONTINGENT_PAID8), SUM(S.CONTINGENT_PAID9), SUM(S.CONTINGENT_PAID10),
              SUM(S.EXECUTORY_PAID1), SUM(S.EXECUTORY_PAID2), SUM(S.EXECUTORY_PAID3), SUM(S.EXECUTORY_PAID4), SUM(S.EXECUTORY_PAID5),
              SUM(S.EXECUTORY_PAID6), SUM(S.EXECUTORY_PAID7), SUM(S.EXECUTORY_PAID8), SUM(S.EXECUTORY_PAID9), SUM(S.EXECUTORY_PAID10),
              S.IS_OM, SUM(S.BEG_DEFERRED_RENT), SUM(S.DEFERRED_RENT), SUM(S.END_DEFERRED_RENT), SUM(S.BEG_ST_DEFERRED_RENT), SUM(S.END_ST_DEFERRED_RENT),
              SUM(S.PRINCIPAL_REMEASUREMENT), SUM(S.LIABILITY_REMEASUREMENT), SUM(S.INCENTIVE_AMOUNT), SUM(S.INITIAL_DIRECT_COST),
              SUM(S.PREPAY_AMORTIZATION), SUM(S.BEG_PREPAID_RENT), SUM(S.PREPAID_RENT), SUM(S.END_PREPAID_RENT), SUM(S.INCENTIVE_MATH_AMOUNT),
              SUM(S.IDC_MATH_AMOUNT), SUM(S.ROU_ASSET_REMEASUREMENT), SUM(S.PARTIAL_TERM_GAIN_LOSS), SUM(S.ADDITIONAL_ROU_ASSET),
              SUM(S.BEG_ARREARS_ACCRUAL), SUM(S.ARREARS_ACCRUAL), SUM(S.END_ARREARS_ACCRUAL), SUM(S.REMAINING_PRINCIPAL),
              SUM(S.ST_OBLIGATION_REMEASUREMENT), SUM(S.LT_OBLIGATION_REMEASUREMENT), SUM(S.OBLIGATION_RECLASS),
              SUM(S.UNACCRUED_INTEREST_REMEASURE), SUM(S.UNACCRUED_INTEREST_RECLASS), SUM(S.BEG_NET_ROU_ASSET),
              SUM(S.CONTINGENT_ADJUST), SUM(S.CURRENT_LEASE_COST),SUM(S.END_NET_ROU_ASSET), SUM(EXECUTORY_ADJUST), SUM(S.incentive_cap_amount)
           from LS_ASSET_SCHEDULE S
           join LS_ASSET A on A.LS_ASSET_ID = S.LS_ASSET_ID
           where A.ILR_ID = A_ILR_ID
           and S.REVISION = L_RETIRE_REVISION
           group by A.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.month, S.IS_OM;

           --Need to carry forward unretired assets onto new revision
           L_STATUS:='Updating other assets current revision';
           PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
           UPDATE LS_ASSET
           SET APPROVED_REVISION = L_RETIRE_REVISION
           WHERE ILR_ID = A_ILR_ID
           AND LS_ASSET_STATUS_ID = 3
           AND LS_ASSET_ID IN (
               SELECT LS_ASSET_ID
               FROM LS_ILR_ASSET_MAP
               WHERE ILR_ID = A_ILR_ID
               AND REVISION = L_RETIRE_REVISION);

           L_STATUS:='Updating ILR approved revision';
           PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
           update LS_ILR_APPROVAL
           set APPROVAL_STATUS_ID = 3, approver = USER, approval_date = sysdate
           where ILR_ID = A_ILR_ID
           and revision = L_RETIRE_REVISION;
		   
		  --Populate acct_month_approved and revision_app_order on ls_ilr_approval
		   L_STATUS := 'Update LS ILR Approval Revision Order and Approval Accounting Month';
		   PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);	  
		   UPDATE ls_ilr_approval lia
		   set acct_month_approved = A_ACCT_MONTH,
		   revision_app_order = (select NVL(max(nvl(revision_app_order,0)),0) + 1 from ls_ilr_approval a where a.ilr_id = A_ILR_ID and a.revision <> L_RETIRE_REVISION )
		   where ilr_id = A_ILR_ID
		   and revision = L_RETIRE_REVISION;


           L_STATUS:='Updating ILR current revision';
           PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
           update LS_ILR
           set CURRENT_REVISION = L_RETIRE_REVISION
           where ILR_ID = A_ILR_ID;

         end if; --end pend trans check

		   else /* Transfer; A_FLAG = 1 */

         L_STATUS := 'Clip transfer_from asset schedule';

         delete from LS_ASSET_SCHEDULE
         where LS_ASSET_ID = A_LS_ASSET_ID
         and REVISION = L_PREV_REVISION
         and month > A_MONTH;

         L_STATUS := 'Update ending amounts on transfer_from asset schedule';

         L_SQLS := 'update LS_ASSET_SCHEDULE set END_CAPITAL_COST = 0, END_OBLIGATION = 0, END_LT_OBLIGATION = 0,  END_LIABILITY = 0, END_LT_LIABILITY = 0';
         L_SQLS := L_SQLS||', INTEREST_ACCRUAL = 0, INTEREST_PAID = 0, PRINCIPAL_ACCRUAL = 0, PRINCIPAL_PAID = 0,  END_NET_ROU_ASSET = 0, END_ACCUM_IMPAIR = 0, CURRENT_LEASE_COST = 0';

         for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
         loop
           L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
         end loop;

         L_SQLS := L_SQLS || ' where ls_asset_id = '||A_LS_ASSET_ID||' and revision = '||L_PREV_REVISION;
         L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_MONTH,'yyyymm')||',''yyyymm'')';

         execute immediate L_SQLS;
		   end if;
	  else /* No assets associated with ILR other than retired/transferred asset; L_RTN = 0 */
		  if A_FLAG = 0 then /* retired asset revision */
		    L_STATUS := 'Get new revision';
		    if L_RETIRE_REVISION = -1 then -- if its a loner asset/ILR, create a new revision; let's use other functions and update status at end
          L_REVISION := F_COPYREVISION(A_ILR_ID, L_CURRENT_REVISION);
          PKG_PP_LOG.P_WRITE_MESSAGE('   RETIRE Revision: ' || TO_CHAR(L_REVISION));

          IF L_REVISION = -1 THEN
            RETURN -1;
          END IF;

          L_STATUS := 'Update LS_ILR_APPROVAL';
          update ls_ilr_approval
          set revision_desc = 'LEASERETIRE', approval_status_id = 2
          where ilr_id = A_ILR_ID
          and revision = L_REVISION;

          L_RETIRE_REVISION := L_REVISION;
        end if; --done with the last asset, if had been posting multiple assets

        L_STATUS := 'CLEAR asset schedule for retire revision';
        delete from LS_ASSET_SCHEDULE
        where LS_ASSET_ID = A_LS_ASSET_ID
          and REVISION = L_RETIRE_REVISION;

			  L_STATUS := 'LOAD asset schedule for retire revision';
        insert into LS_ASSET_SCHEDULE(
          LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
          BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
          END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
          INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
          CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
          CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
          CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
          EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
          EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
          CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
          CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
          EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
          EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
          IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
          PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST, INCENTIVE_MATH_AMOUNT, IDC_MATH_AMOUNT,
          PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
          PARTIAL_MONTH_PERCENT, BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL,
          REMAINING_PRINCIPAL, ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
          UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS,BEG_NET_ROU_ASSET,CONTINGENT_ADJUST,CURRENT_LEASE_COST,END_NET_ROU_ASSET,EXECUTORY_ADJUST,
          BEGIN_ACCUM_IMPAIR, IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR, incentive_cap_amount)
        select A.LS_ASSET_ID, L_RETIRE_REVISION, A.SET_OF_BOOKS_ID, A.month, A.RESIDUAL_AMOUNT, A.TERM_PENALTY, A.BPO_PRICE,
               BEG_CAPITAL_COST, decode(month, A_ACCT_MONTH, 0, A.END_CAPITAL_COST),
               A.BEG_OBLIGATION,
               decode(month, A_ACCT_MONTH, 0, A.END_OBLIGATION),
               A.BEG_LT_OBLIGATION,
               decode(month, A_ACCT_MONTH, 0, A.END_LT_OBLIGATION),
               A.BEG_LIABILITY,
               decode(month, A_ACCT_MONTH, 0, A.END_LIABILITY),
               A.BEG_LT_LIABILITY,
               decode(month, A_ACCT_MONTH, 0, A.END_LT_LIABILITY),
               A.INTEREST_ACCRUAL, A.PRINCIPAL_ACCRUAL, A.INTEREST_PAID, A.PRINCIPAL_PAID,
               A.CONTINGENT_ACCRUAL1, A.CONTINGENT_ACCRUAL2, A.CONTINGENT_ACCRUAL3, A.CONTINGENT_ACCRUAL4,
               A.CONTINGENT_ACCRUAL5, A.CONTINGENT_ACCRUAL6, A.CONTINGENT_ACCRUAL7, A.CONTINGENT_ACCRUAL8,
               A.CONTINGENT_ACCRUAL9, A.CONTINGENT_ACCRUAL10, A.EXECUTORY_ACCRUAL1, A.EXECUTORY_ACCRUAL2,
               A.EXECUTORY_ACCRUAL3, A.EXECUTORY_ACCRUAL4, A.EXECUTORY_ACCRUAL5, A.EXECUTORY_ACCRUAL6,
               A.EXECUTORY_ACCRUAL7, A.EXECUTORY_ACCRUAL8, A.EXECUTORY_ACCRUAL9, A.EXECUTORY_ACCRUAL10,
               A.CONTINGENT_PAID1, A.CONTINGENT_PAID2, A.CONTINGENT_PAID3, A.CONTINGENT_PAID4, A.CONTINGENT_PAID5,
               A.CONTINGENT_PAID6, A.CONTINGENT_PAID7, A.CONTINGENT_PAID8, A.CONTINGENT_PAID9, A.CONTINGENT_PAID10,
               A.EXECUTORY_PAID1, A.EXECUTORY_PAID2, A.EXECUTORY_PAID3, A.EXECUTORY_PAID4, A.EXECUTORY_PAID5,
               A.EXECUTORY_PAID6, A.EXECUTORY_PAID7, A.EXECUTORY_PAID8, A.EXECUTORY_PAID9, A.EXECUTORY_PAID10,
               A.IS_OM, A.BEG_DEFERRED_RENT, A.DEFERRED_RENT, A.END_DEFERRED_RENT, A.BEG_ST_DEFERRED_RENT, A.END_ST_DEFERRED_RENT,
               A.PRINCIPAL_REMEASUREMENT, A.LIABILITY_REMEASUREMENT, A.INCENTIVE_AMOUNT, A.INITIAL_DIRECT_COST,
               A.INCENTIVE_MATH_AMOUNT, A.IDC_MATH_AMOUNT,
               A.PREPAY_AMORTIZATION, A.BEG_PREPAID_RENT, A.PREPAID_RENT, A.END_PREPAID_RENT, A.ROU_ASSET_REMEASUREMENT,
               A.PARTIAL_TERM_GAIN_LOSS, A.ADDITIONAL_ROU_ASSET,
               A.PARTIAL_MONTH_PERCENT, A.BEG_ARREARS_ACCRUAL, A.ARREARS_ACCRUAL, A.END_ARREARS_ACCRUAL,
               A.REMAINING_PRINCIPAL,
               decode(month, A_ACCT_MONTH,
                             -1 * ((A.END_OBLIGATION -
                                   CASE WHEN LS_ASSET.ESTIMATED_RESIDUAL = 0
                                         AND lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', LS_ASSET.COMPANY_ID))) = 'yes' THEN
                                     NVL(LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT,0)
                                   ELSE
                                     CASE WHEN lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', LS_ASSET.COMPANY_ID))) = 'no' THEN
                                        0
                                     else
                                       round(LS_ASSET.ESTIMATED_RESIDUAL * LS_ASSET.FMV, 2)
                                     END
                                   end
                                  - NVL(A.END_LT_OBLIGATION, 0)) - NVL(A.OBLIGATION_RECLASS, 0)),
                             A.ST_OBLIGATION_REMEASUREMENT) ST_OBLIGATION_REMEASUREMENT,
               decode(month, A_ACCT_MONTH,
                             -1 * ((A.END_LT_OBLIGATION -
                                   CASE WHEN LS_ASSET.ESTIMATED_RESIDUAL = 0
                                         AND lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Amort to Guaranteed Residual', LS_ASSET.COMPANY_ID))) = 'yes' THEN
                                     NVL(LS_ASSET.GUARANTEED_RESIDUAL_AMOUNT,0)
                                   ELSE
                                     CASE WHEN lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_COMPANY('Lease Capitalize Estimated Residual', LS_ASSET.COMPANY_ID))) = 'no' THEN
                                       0
                                     else
                                       round(LS_ASSET.ESTIMATED_RESIDUAL * LS_ASSET.FMV, 2)
                                     end
                                   end) + NVL(A.OBLIGATION_RECLASS, 0)),
                             A.LT_OBLIGATION_REMEASUREMENT) LT_OBLIGATION_REMEASUREMENT,
               decode(month, A_ACCT_MONTH, 0, A.OBLIGATION_RECLASS) OBLIGATION_RECLASS,
               decode(month, A_ACCT_MONTH,
                             -1 * ((NVL(A.END_LT_LIABILITY, 0) - NVL(A.END_LT_OBLIGATION, 0)- NVL(A.UNACCRUED_INTEREST_RECLASS, 0))
                                  	- (NVL(A.END_LIABILITY, 0) - NVL(A.END_OBLIGATION, 0))),
                             A.UNACCRUED_INTEREST_REMEASURE) UNACCRUED_INTEREST_REMEASURE,
               decode(month, A_ACCT_MONTH, 0, A.UNACCRUED_INTEREST_RECLASS) UNACCRUED_INTEREST_RECLASS,
			   A.BEG_NET_ROU_ASSET,A.CONTINGENT_ADJUST,
			   decode(month, A_ACCT_MONTH, 0, A.CURRENT_LEASE_COST),
			   decode(month, A_ACCT_MONTH, 0, A.END_NET_ROU_ASSET),
			   A.EXECUTORY_ADJUST,
               A.BEGIN_ACCUM_IMPAIR, A.IMPAIRMENT_ACTIVITY,
			   decode(month, A_ACCT_MONTH, 0, A.END_ACCUM_IMPAIR), A.incentive_cap_amount
          from LS_ASSET_SCHEDULE A
          join LS_ASSET on LS_ASSET.LS_ASSET_ID = A.LS_ASSET_ID
         where A.LS_ASSET_ID = A_LS_ASSET_ID
           and A.REVISION = L_CURRENT_REVISION
           and A.MONTH <= A_ACCT_MONTH;

			  L_STATUS := 'Update asset with retire revision';
			  update LS_ASSET
			  set APPROVED_REVISION = L_RETIRE_REVISION
			  where LS_ASSET_ID = A_LS_ASSET_ID;

        IF A_MONTH < A_ACCT_MONTH THEN

          L_STATUS := 'Update ending amounts on RETIREMENT asset schedule';
          L_SQLS := 'update LS_ASSET_SCHEDULE set END_CAPITAL_COST = 0,
                            END_OBLIGATION = 0,
                            END_LT_OBLIGATION = 0,
                            END_LIABILITY = 0,
                            END_LT_LIABILITY = 0,
                            INTEREST_ACCRUAL = 0,
                            INTEREST_PAID = 0,
                            PRINCIPAL_ACCRUAL = 0,
                            PRINCIPAL_PAID = 0,
                            END_NET_ROU_ASSET = 0,
                            CURRENT_LEASE_COST = 0';

          for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
          loop
             L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
          end loop;

          L_SQLS := L_SQLS || ' where ls_asset_id = '||A_LS_ASSET_ID||' and revision = '||L_RETIRE_REVISION;
          L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_ACCT_MONTH,'yyyymm')||',''yyyymm'')';

          execute immediate L_SQLS;
        end if;

        L_STATUS := 'Clearing ls_depr_forecast';
        delete from ls_depr_forecast
        where ls_asset_id = A_LS_ASSET_ID
        and revision = L_RETIRE_REVISION;

        L_STATUS := 'Copying prior rows in from ls_depr_forecast';
        insert into ls_depr_forecast (
          LS_ASSET_ID, REVISION, SET_OF_BOOKS_ID, MONTH, BEGIN_RESERVE, DEPR_EXPENSE, DEPR_EXP_ALLOC_ADJUST, END_RESERVE,
          DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, PARTIAL_MONTH_PERCENT)
        select LS_ASSET_ID, L_RETIRE_REVISION, SET_OF_BOOKS_ID, MONTH,
             BEGIN_RESERVE,
             CASE WHEN A_MONTH < A_ACCT_MONTH AND MONTH = A_ACCT_MONTH then 0 else DEPR_EXPENSE END DEPR_EXPENSE,
             DEPR_EXP_ALLOC_ADJUST,
             decode(month, A_ACCT_MONTH, 0, END_RESERVE) END_RESERVE,
             DEPR_GROUP_ID, MID_PERIOD_METHOD, MID_PERIOD_CONV, PARTIAL_MONTH_PERCENT
          from ls_depr_forecast
         where ls_asset_id = A_LS_ASSET_ID
           and revision = L_CURRENT_REVISION
           and month <= A_ACCT_MONTH;

        L_STATUS := 'CLEAR ilr_schedule';
        delete from LS_ILR_SCHEDULE
        where ILR_ID = A_ILR_ID
          and REVISION = L_RETIRE_REVISION;

			  L_STATUS := 'LOAD ilr_schedule';
			  insert into LS_ILR_SCHEDULE
			  (ILR_ID, REVISION, SET_OF_BOOKS_ID, month, RESIDUAL_AMOUNT, TERM_PENALTY, BPO_PRICE,
        BEG_CAPITAL_COST, END_CAPITAL_COST, BEG_OBLIGATION, END_OBLIGATION, BEG_LT_OBLIGATION,
        END_LT_OBLIGATION, BEG_LIABILITY, END_LIABILITY, BEG_LT_LIABILITY, END_LT_LIABILITY,
        INTEREST_ACCRUAL, PRINCIPAL_ACCRUAL, INTEREST_PAID, PRINCIPAL_PAID,
        CONTINGENT_ACCRUAL1, CONTINGENT_ACCRUAL2, CONTINGENT_ACCRUAL3, CONTINGENT_ACCRUAL4,
        CONTINGENT_ACCRUAL5, CONTINGENT_ACCRUAL6, CONTINGENT_ACCRUAL7, CONTINGENT_ACCRUAL8,
        CONTINGENT_ACCRUAL9, CONTINGENT_ACCRUAL10, EXECUTORY_ACCRUAL1, EXECUTORY_ACCRUAL2,
        EXECUTORY_ACCRUAL3, EXECUTORY_ACCRUAL4, EXECUTORY_ACCRUAL5, EXECUTORY_ACCRUAL6,
        EXECUTORY_ACCRUAL7, EXECUTORY_ACCRUAL8, EXECUTORY_ACCRUAL9, EXECUTORY_ACCRUAL10,
        CONTINGENT_PAID1, CONTINGENT_PAID2, CONTINGENT_PAID3, CONTINGENT_PAID4, CONTINGENT_PAID5,
        CONTINGENT_PAID6, CONTINGENT_PAID7, CONTINGENT_PAID8, CONTINGENT_PAID9, CONTINGENT_PAID10,
        EXECUTORY_PAID1, EXECUTORY_PAID2, EXECUTORY_PAID3, EXECUTORY_PAID4, EXECUTORY_PAID5,
        EXECUTORY_PAID6, EXECUTORY_PAID7, EXECUTORY_PAID8, EXECUTORY_PAID9, EXECUTORY_PAID10,
        IS_OM, BEG_DEFERRED_RENT, DEFERRED_RENT, END_DEFERRED_RENT, BEG_ST_DEFERRED_RENT, END_ST_DEFERRED_RENT,
        PRINCIPAL_REMEASUREMENT, LIABILITY_REMEASUREMENT, INCENTIVE_AMOUNT, INITIAL_DIRECT_COST,
        PREPAY_AMORTIZATION, BEG_PREPAID_RENT, PREPAID_RENT, END_PREPAID_RENT, INCENTIVE_MATH_AMOUNT,
        IDC_MATH_AMOUNT, ROU_ASSET_REMEASUREMENT, PARTIAL_TERM_GAIN_LOSS, ADDITIONAL_ROU_ASSET,
        BEG_ARREARS_ACCRUAL, ARREARS_ACCRUAL, END_ARREARS_ACCRUAL, REMAINING_PRINCIPAL,
        ST_OBLIGATION_REMEASUREMENT, LT_OBLIGATION_REMEASUREMENT, OBLIGATION_RECLASS,
        UNACCRUED_INTEREST_REMEASURE, UNACCRUED_INTEREST_RECLASS, BEG_NET_ROU_ASSET,
        CONTINGENT_ADJUST, CURRENT_LEASE_COST,END_NET_ROU_ASSET, EXECUTORY_ADJUST,
        BEGIN_ACCUM_IMPAIR,IMPAIRMENT_ACTIVITY,END_ACCUM_IMPAIR,incentive_cap_amount)
			  select A.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.month,
               SUM(S.RESIDUAL_AMOUNT), SUM(S.TERM_PENALTY), SUM(S.BPO_PRICE),
               SUM(S.BEG_CAPITAL_COST), SUM(S.END_CAPITAL_COST), SUM(S.BEG_OBLIGATION), SUM(S.END_OBLIGATION), SUM(S.BEG_LT_OBLIGATION),
               SUM(S.END_LT_OBLIGATION), SUM(S.BEG_LIABILITY), SUM(S.END_LIABILITY), SUM(S.BEG_LT_LIABILITY), SUM(S.END_LT_LIABILITY),
               SUM(S.INTEREST_ACCRUAL), SUM(S.PRINCIPAL_ACCRUAL), SUM(S.INTEREST_PAID), SUM(S.PRINCIPAL_PAID),
               SUM(S.CONTINGENT_ACCRUAL1), SUM(S.CONTINGENT_ACCRUAL2), SUM(S.CONTINGENT_ACCRUAL3), SUM(S.CONTINGENT_ACCRUAL4),
               SUM(S.CONTINGENT_ACCRUAL5), SUM(S.CONTINGENT_ACCRUAL6), SUM(S.CONTINGENT_ACCRUAL7), SUM(S.CONTINGENT_ACCRUAL8),
               SUM(S.CONTINGENT_ACCRUAL9), SUM(S.CONTINGENT_ACCRUAL10), SUM(S.EXECUTORY_ACCRUAL1), SUM(S.EXECUTORY_ACCRUAL2),
               SUM(S.EXECUTORY_ACCRUAL3), SUM(S.EXECUTORY_ACCRUAL4), SUM(S.EXECUTORY_ACCRUAL5), SUM(S.EXECUTORY_ACCRUAL6),
               SUM(S.EXECUTORY_ACCRUAL7), SUM(S.EXECUTORY_ACCRUAL8), SUM(S.EXECUTORY_ACCRUAL9), SUM(S.EXECUTORY_ACCRUAL10),
               SUM(S.CONTINGENT_PAID1), SUM(S.CONTINGENT_PAID2), SUM(S.CONTINGENT_PAID3), SUM(S.CONTINGENT_PAID4), SUM(S.CONTINGENT_PAID5),
               SUM(S.CONTINGENT_PAID6), SUM(S.CONTINGENT_PAID7), SUM(S.CONTINGENT_PAID8), SUM(S.CONTINGENT_PAID9), SUM(S.CONTINGENT_PAID10),
               SUM(S.EXECUTORY_PAID1), SUM(S.EXECUTORY_PAID2), SUM(S.EXECUTORY_PAID3), SUM(S.EXECUTORY_PAID4), SUM(S.EXECUTORY_PAID5),
               SUM(S.EXECUTORY_PAID6), SUM(S.EXECUTORY_PAID7), SUM(S.EXECUTORY_PAID8), SUM(S.EXECUTORY_PAID9), SUM(S.EXECUTORY_PAID10),
               S.IS_OM, SUM(S.BEG_DEFERRED_RENT), SUM(S.DEFERRED_RENT), SUM(S.END_DEFERRED_RENT), SUM(S.BEG_ST_DEFERRED_RENT), SUM(S.END_ST_DEFERRED_RENT),
               SUM(S.PRINCIPAL_REMEASUREMENT), SUM(S.LIABILITY_REMEASUREMENT), SUM(S.INCENTIVE_AMOUNT), SUM(S.INITIAL_DIRECT_COST),
               SUM(S.PREPAY_AMORTIZATION), SUM(S.BEG_PREPAID_RENT), SUM(S.PREPAID_RENT), SUM(S.END_PREPAID_RENT), SUM(S.INCENTIVE_MATH_AMOUNT),
               SUM(S.IDC_MATH_AMOUNT), SUM(S.ROU_ASSET_REMEASUREMENT), SUM(S.PARTIAL_TERM_GAIN_LOSS), SUM(S.ADDITIONAL_ROU_ASSET),
               SUM(S.BEG_ARREARS_ACCRUAL), SUM(S.ARREARS_ACCRUAL), SUM(S.END_ARREARS_ACCRUAL), SUM(S.REMAINING_PRINCIPAL),
               SUM(S.ST_OBLIGATION_REMEASUREMENT), SUM(S.LT_OBLIGATION_REMEASUREMENT), SUM(S.OBLIGATION_RECLASS),
               SUM(S.UNACCRUED_INTEREST_REMEASURE), SUM(S.UNACCRUED_INTEREST_RECLASS), SUM(S.BEG_NET_ROU_ASSET),
               SUM(S.CONTINGENT_ADJUST), SUM(S.CURRENT_LEASE_COST),SUM(S.END_NET_ROU_ASSET), SUM(EXECUTORY_ADJUST),
               SUM(S.BEGIN_ACCUM_IMPAIR),SUM(S.IMPAIRMENT_ACTIVITY),SUM(S.END_ACCUM_IMPAIR), SUM(S.incentive_cap_amount)
         from LS_ASSET_SCHEDULE S
         join LS_ASSET A on A.LS_ASSET_ID = S.LS_ASSET_ID
        where A.ILR_ID = A_ILR_ID
          and S.REVISION = L_RETIRE_REVISION
          and S.MONTH <= A_ACCT_MONTH
        group by A.ILR_ID, S.REVISION, S.SET_OF_BOOKS_ID, S.month, S.IS_OM;

        L_STATUS:='Updating ILR approved revision';
        update LS_ILR_APPROVAL
        set APPROVAL_STATUS_ID = 3, approver = USER, approval_date = sysdate
        where ILR_ID = A_ILR_ID
        and revision = L_RETIRE_REVISION;
		
	    --Populate acct_month_approved and revision_app_order on ls_ilr_approval
		L_STATUS := 'Update LS ILR Approval Revision Order and Approval Accounting Month';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);	  
		UPDATE ls_ilr_approval lia
		set acct_month_approved = A_ACCT_MONTH,
		revision_app_order = (select NVL(max(nvl(revision_app_order,0)),0) + 1 from ls_ilr_approval a where a.ilr_id = A_ILR_ID and a.revision <> L_RETIRE_REVISION )
		where ilr_id = A_ILR_ID
		and revision = L_RETIRE_REVISION;

        L_STATUS:='Updating ILR current revision';
        update LS_ILR
        set CURRENT_REVISION = L_RETIRE_REVISION
        where ILR_ID = A_ILR_ID;

        L_STATUS:='Clear ILR Asset Map';
        delete from LS_ILR_ASSET_MAP
        where LS_ASSET_ID = A_LS_ASSET_ID
        and ILR_ID = A_ILR_ID
        and REVISION = L_RETIRE_REVISION;

		  else /* A_FLAG = 1. Intercompany transfer could leave asset/ILR by itself also if a full transfer.
				      Need to clip previous schedule if nothing there for current revision, otherwise leave as is */
			  L_STATUS:= 'Checking for transfer_from schedule related to current revision';
        select count(1)
        into L_RTN
        from LS_ILR_SCHEDULE
        where ILR_ID = A_ILR_ID
        and REVISION = L_CURRENT_REVISION;

        if L_RTN = 0 then

          L_STATUS:= 'Clipping TRANSFER_FROM ILR/Asset schedule for revision: '||L_PREV_REVISION;
          PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

          L_STATUS:= 'Clipping transfer_from asset schedule';
          delete from LS_ASSET_SCHEDULE
          where LS_ASSET_ID = A_LS_ASSET_ID
          and REVISION = L_PREV_REVISION
          and month > A_MONTH;

          L_STATUS:= 'Clipping transfer_from ILR schedule';
          delete from LS_ILR_SCHEDULE
          where ILR_ID = A_ILR_ID
          and REVISION = L_PREV_REVISION
          and month > A_MONTH;

          L_STATUS := 'Update ending amounts on transfer_from asset schedule';

          L_SQLS := 'update LS_ASSET_SCHEDULE set END_CAPITAL_COST = 0, END_OBLIGATION = 0, END_LT_OBLIGATION = 0, END_LIABILITY = 0, END_LT_LIABILITY = 0';
          L_SQLS := L_SQLS||', INTEREST_ACCRUAL = 0, INTEREST_PAID = 0, PRINCIPAL_ACCRUAL = 0, PRINCIPAL_PAID = 0,  END_NET_ROU_ASSET = 0, END_ACCUM_IMPAIR = 0, CURRENT_LEASE_COST = 0';

          for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
          loop
             L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
          end loop;

          L_SQLS := L_SQLS || ' where ls_asset_id = '||A_LS_ASSET_ID||' and revision = '||L_PREV_REVISION;
          L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_MONTH,'yyyymm')||',''yyyymm'')';

          execute immediate L_SQLS;

          L_STATUS := 'Update ending amounts on transfer_from ILR schedule';

          L_SQLS := 'update LS_ILR_SCHEDULE set END_CAPITAL_COST = 0, END_OBLIGATION = 0, END_LT_OBLIGATION = 0, END_LIABILITY = 0, END_LT_LIABILITY = 0, CURRENT_LEASE_COST = 0';
          L_SQLS := L_SQLS||', INTEREST_ACCRUAL = 0, INTEREST_PAID = 0, PRINCIPAL_ACCRUAL = 0, PRINCIPAL_PAID = 0, END_NET_ROU_ASSET = 0';

          for i in (select rent_type, bucket_number from ls_rent_bucket_admin where status_code_id = 1 order by 1,2)
          loop
             L_SQLS := L_SQLS || ', '||i.rent_type||'_ACCRUAL'||i.bucket_number||' = 0, '||i.rent_type||'_PAID'||i.bucket_number||' = 0';
          end loop;

          L_SQLS := L_SQLS || ' where ilr_id = '||A_ILR_ID||' and revision = '||L_PREV_REVISION;
          L_SQLS := L_SQLS || ' and month = to_date('||to_char(A_MONTH,'yyyymm')||',''yyyymm'')';

          execute immediate L_SQLS;
			  end if;
		  end if;
	  end if;

		L_STATUS:='Finished loading retirement revision';
		PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);

    return 1;
  exception
    when others then
      L_STATUS := SUBSTR(L_STATUS || ': ' || sqlerrm, 1, 2000);
      PKG_PP_LOG.P_WRITE_MESSAGE(L_STATUS);
      return -1;
  end F_RETIREREVISION;

   /*
   *   @@DESCRIPTION
   *   	This function pulls the correct discount rate based on the
   *	parameters passed to the function.
   *   @@PARAMS
   *	number: in: A_ILR_ID = id of ILR being calculated
   *    number: in: A_TERM_MONTHS = number of months for the term
   *    date: in:   A_IN_SERVICE_DATE = Date ILR placed in service
   *    number: in: A_BORROWING_CURRENCY = currency used by process, if any
   *   @@RETURN
   *      number: >= 0 for success
   *              -1 for failure
   */
  function F_GET_DEFAULT_DISC_RATE(A_ILR_ID number,
								   A_TERM_MONTHS number,
								   A_IN_SERVICE_DATE date,
								   A_REMEASUREMENT_DATE date,
								   A_BORROWING_CURRENCY number) return number is
      L_RTN number;
	  L_RATE_TYPE_CONTROL number;
	  L_RATE_TYPE_DESC varchar2(35);
	  L_RATE_TYPE_ID number;
	  L_SQL varchar2(2000);
	  L_COMPANY_ID number;
	  L_ILR_GROUP number;
	  L_LEASE_GROUP number;
	  L_STATUS varchar2(2000);
	  L_TERM_DIFF number;
	  L_TERM_MONTHS number;
	  L_ILR_EFFECTIVE_DATE date;
	  
	  disc_rate_error exception;
	  
   begin
      L_RTN := -1;
	  L_TERM_MONTHS := A_TERM_MONTHS;

	  L_STATUS := 'Checking for the default rate type for the ILR ID: ' || to_char(A_ILR_ID);

	  L_RATE_TYPE_CONTROL := cast(lower(trim(pkg_pp_system_control.f_pp_system_control_company('Lessee Default Disc Rate Map', -1))) as number);

	  if L_RATE_TYPE_CONTROL is null then
		L_STATUS := 'Unable to determine the rate type for ILR ID: ' || to_char(A_ILR_ID);
		raise disc_rate_error;
   	  end if;

  	L_STATUS := 'Pulling Company, ILR and Lease Group' ;
	
	  select LS_ILR.COMPANY_ID, LS_ILR.ILR_GROUP_ID, LS_LEASE.LEASE_GROUP_ID
	    into L_COMPANY_ID, L_ILR_GROUP, L_LEASE_GROUP
		from LS_ILR,   
			 LS_LEASE
	   where (LS_ILR.ILR_ID = A_ILR_ID and 
			  LS_LEASE.LEASE_ID = LS_ILR.LEASE_ID);

	  if L_COMPANY_ID is null or L_ILR_GROUP is null or L_LEASE_GROUP is null then
		L_STATUS := 'Unable to determine the Company, ILR and Lease Group for ILR ID: ' || to_char(A_ILR_ID);
		raise disc_rate_error;
   	  end if;
	  
	  begin

		  L_SQL := 'select rate_type_id from ls_ilr_default_rate_type_map map where company_id = ' || to_char(L_COMPANY_ID);

		  if L_RATE_TYPE_CONTROL = 1 then
			L_SQL := L_SQL || ' and map.lease_group_id = ' || to_char(L_LEASE_GROUP);
		  elsif L_RATE_TYPE_CONTROL = 2 then
			L_SQL := L_SQL || ' and map.ilr_group_id = ' || to_char(L_ILR_GROUP);
		  elsif L_RATE_TYPE_CONTROL <> 3 then
			L_STATUS := 'Bad Lessee Default Disc Rate Map setting.';
			raise disc_rate_error;
		  end if;

		  l_status := 'Retrieving Default Rate Type';
		  
		  execute immediate L_SQL
		  into L_RATE_TYPE_ID;
		  
		  exception
			when no_data_found then
			  RAISE_APPLICATION_ERROR(-20001, 'Unable to find Default Rate Type for ILR. Confirm a valid mapping of attributes for the Current ILR.');
			  return -1;

	  end;
	  
	  -- If we have a remeasurement date, recalculate the terms based on it
	  if A_REMEASUREMENT_DATE is not null then
		L_TERM_DIFF := MONTHS_BETWEEN(TRUNC(A_REMEASUREMENT_DATE, 'MONTH'), TRUNC(A_IN_SERVICE_DATE, 'MONTH'));		
		L_TERM_MONTHS := A_TERM_MONTHS - L_TERM_DIFF;
		
		L_ILR_EFFECTIVE_DATE := A_REMEASUREMENT_DATE;
	  else
		L_ILR_EFFECTIVE_DATE := A_IN_SERVICE_DATE;
	  end if;	  
	  
	  begin

		  l_status := 'Retrieving default rate';
		  
		  select rate
			into L_RTN
			from (select rate from ls_ilr_default_rates
				   where borrowing_curr_id = A_BORROWING_CURRENCY and
						 L_ILR_EFFECTIVE_DATE >= effective_date and
						 term_length <= L_TERM_MONTHS and
						 rate_type_id = L_RATE_TYPE_ID
				   order by effective_date desc, term_length desc)
		   where rownum = 1;

		  exception
			when no_data_found then
				--Pull Default Rate Type name for informative Message
				select description
				into l_rate_type_desc
				from ls_ilr_default_rate_type
				where rate_type_id = l_rate_type_id;
			
				RAISE_APPLICATION_ERROR(-20001, 'Unable to find Default Rate for ILR. Confirm Default Discount Rate Type is configured appropriately for Default Rate Type: '|| l_rate_type_desc);
				return -1;
			when others then
				raise_application_error (-20002, 'Other Error');

	  end;

	  return L_RTN;

	  exception
		when others then
		  L_STATUS := SUBSTR(L_STATUS || ': ' || nvl(sqlerrm, 'No SQL Error'), 1, 2000);
  		  RAISE_APPLICATION_ERROR(-20000, L_STATUS);
		  return -1;
	end F_GET_DEFAULT_DISC_RATE;

 --##########################################################################
 -- PULL PAYMENT TERM LENGTH FOR ILR IN MONTHS
 --##########################################################################

  FUNCTION F_PULL_ILR_PAYMENT_TERM_MONTHS(a_ilr_id NUMBER, a_revision NUMBER)
  RETURN NUMBER
  IS
  L_RTN NUMBER;
  BEGIN
	
		SELECT SUM(CASE payment_freq_id
					 WHEN 1 THEN -- Annual
					  number_of_terms * 12
					 WHEN 2 THEN -- Semi Annual
					  number_of_terms * 6
					 WHEN 3 THEN -- Quarterly
					  number_of_terms * 3
					 ELSE -- Monthly
					  number_of_terms
				   END) terms
		INTO   L_RTN
		FROM   ls_ilr_payment_term
		WHERE  ilr_id = a_ilr_id
		AND    revision = a_revision;
	
	RETURN L_RTN;
	
	EXCEPTION
		WHEN OTHERS THEN
         RETURN 0;
	
  END F_PULL_ILR_PAYMENT_TERM_MONTHS;

--**************************************************************************
--                            Initialize Package
--**************************************************************************

end PKG_LEASE_ILR;
/
