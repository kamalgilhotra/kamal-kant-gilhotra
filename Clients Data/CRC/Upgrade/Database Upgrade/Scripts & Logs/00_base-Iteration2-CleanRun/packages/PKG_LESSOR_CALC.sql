CREATE OR REPLACE PACKAGE pkg_lessor_calc AS
	/*
	||============================================================================
	|| Application: PowerPlant
	|| Object Name: PKG_LESSOR_CALC
	|| Description:
	||============================================================================
	|| Copyright (C) 2017 by PowerPlan Consultants, Inc. All Rights Reserved
	||============================================================================
	|| Version    Date       Revised By      Reason For Change
	|| ---------- ---------- --------------  -------------------------------------
	|| 2017.1.0.0 11/01/2017 C.Shilling		 Create package for month-end processes
	|| 2017.3.0.0 02/19/2018 Anand R         PP-50386 Add JEs for lessor accruals during month end for Sales and Direct Finance ILRs
	|| 2017.3.0.0 02/19/2018 Anand R         PP-50387 Add JEs for lessor accruals during month end for Operating type ILRs
	|| 2017.4.0.0 05/11/2018 Anand R         PP-51027 Move reclass JE from Lock month to Close month
	|| 2017.4.0.0 06/01/2018 Anand R         PP-51330 Add JEs for Termination of Operating type ILR
	|| 2017.4.0.0 06/27/2018 Alex  H         PP-50116 Add MEC calculation for Auto Termination
	|| 2017.4.0.0 06/28/2018 K Powers        PP-51563 Add ST/LT gain and avg rates
	|| 2018.1.0.0 11/14/2018 Anand R         PP-52545 Modify amount for re class JEs if re measurement exists
	|| 2018.1.0.0 11/21/2018 David Conway    PP-52738 Modify join for terminations to include Mixed Cap Types.
	|| 2018.1.0.0 11/26/2018 David Conway    PP-52740 Modify auto terminations to produce JEs for all sets of books.
	|| 2018.1.0.0 11/27/2018 David Conway    PP-52740 Prevent duplication of ILRs in Auto terminations, since Set Of Books now gives us multiple rows.
	|| 2018.1.0.0 11/28/2018 David Conway    PP-52752 Prevent duplicate JEs re-recognizing CPR assets in termination.
	|| 2018.3.0.0 04/22/2018 B. Beck	     Change call for asset re-recognition
	||============================================================================
	*/
	G_PKG_VERSION varchar(35) := '2018.2.1.0';
	
	type NUM_ARRAY is table of number(22) index by binary_integer;

	TYPE currency_gain_loss_rec IS RECORD(
		company_id                    	company_setup.company_id%TYPE,
		ilr_id                        	lsr_ilr.ilr_id%TYPE,
		ilr_number                    	lsr_ilr.ilr_number%TYPE,
		iso_code                      	currency.iso_code%TYPE,
		currency_display_symbol       	currency.currency_display_symbol%TYPE,
		gain_loss_fx                  	v_lsr_ilr_mc_schedule.st_currency_gain_loss%TYPE,
		MONTH                         	v_lsr_ilr_mc_schedule.month%TYPE,
		curr_gain_loss_acct_id 			    lsr_ilr_account.curr_gain_loss_acct_id%TYPE,
		curr_gain_loss_offset_acct_id 	lsr_ilr_account.curr_gain_loss_offset_acct_id%TYPE,
    st_currency_gain_loss           v_lsr_ilr_mc_schedule.st_currency_gain_loss%TYPE,
    lt_currency_gain_loss           v_lsr_ilr_mc_schedule.lt_currency_gain_loss%TYPE,
    st_receivable_account_id        lsr_ilr_account.st_receivable_account_id%TYPE,
    lt_receivable_account_id        lsr_ilr_account.lt_receivable_account_id%TYPE 
	);
	TYPE currency_gain_loss_tbl IS TABLE OF currency_gain_loss_rec;

	PROCEDURE p_variable_payments_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_calc_type  IN VARCHAR2);

	PROCEDURE p_accruals_calc(
			a_company_id  NUMBER,
			a_month       DATE,
			a_end_log     NUMBER:=NULL);

	PROCEDURE p_accruals_approve(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_invoice_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_invoice_approve(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_auto_termination_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
            a_end_log    IN NUMBER:=NULL)
	;

	PROCEDURE p_auto_termination_approve(
			a_company_id IN NUMBER,
        	a_month      IN DATE,
        	a_end_log    IN NUMBER:=NULL)
	;

    PROCEDURE p_auto_termination_approve(
        	a_month       DATE,
            a_ilrs        NUM_ARRAY)
	;

	PROCEDURE p_lsr_closed(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log IN NUMBER:=NULL)
	;

	FUNCTION f_currency_gain_loss(
			a_company_ids     IN t_num_array,
			a_month           IN DATE,
			a_set_of_books_id IN NUMBER)
		RETURN currency_gain_loss_tbl
		PIPELINED;

	PROCEDURE p_currency_gain_loss_approve(
			a_company_id      IN NUMBER,
			a_month           IN DATE,
			a_end_log         IN NUMBER := NULL)
	;

	PROCEDURE p_lessor_reclass_jes(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log IN NUMBER:=NULL)
	;

END pkg_lessor_calc;
/
CREATE OR REPLACE PACKAGE BODY pkg_lessor_calc AS
	--**************************************************************************
	--                            p_variable_payments_calc
	--             --------------------------------
	-- @@ description
	--    this package will create a pseduo asset schedule by allocating ILR amounts
	--    to the asset level and then callthe variable payments calculation.
	-- @@params
	--    company: a_company_id
	--       the company to process accruals for
	--    date: a_month
	--       the month to process accruals for
	--    calc type: a_calc_type
	--       'invoices' or 'accruals'
	--
	--**************************************************************************
	PROCEDURE p_variable_payments_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_calc_type  IN VARCHAR2)
	IS
		rtn_str					VARCHAR2(2000);
	BEGIN
		----------------------------------------------------------
		--Populate lessor asset temp table
		----------------------------------------------------------
		DELETE FROM lsr_asset_schedule_tmp;

		INSERT INTO lsr_asset_schedule_tmp(
			lsr_asset_id,
			revision,
			set_of_books_id,
			MONTH,
			interest_income_received,
			interest_income_accrued,
			interest_rental_recvd_spread,
			beg_deferred_rev,
			deferred_rev_activity,
			end_deferred_rev,
			beg_receivable,
			end_receivable,
			executory_accrual1,
			executory_accrual2,
			executory_accrual3,
			executory_accrual4,
			executory_accrual5,
			executory_accrual6,
			executory_accrual7,
			executory_accrual8,
			executory_accrual9,
			executory_accrual10,
			executory_paid1,
			executory_paid2,
			executory_paid3,
			executory_paid4,
			executory_paid5,
			executory_paid6,
			executory_paid7,
			executory_paid8,
			executory_paid9,
			executory_paid10,
			contingent_accrual1,
			contingent_accrual2,
			contingent_accrual3,
			contingent_accrual4,
			contingent_accrual5,
			contingent_accrual6,
			contingent_accrual7,
			contingent_accrual8,
			contingent_accrual9,
			contingent_accrual10,
			contingent_paid1,
			contingent_paid2,
			contingent_paid3,
			contingent_paid4,
			contingent_paid5,
			contingent_paid6,
			contingent_paid7,
			contingent_paid8,
			contingent_paid9,
			contingent_paid10
		)
		SELECT sch.lsr_asset_id,
			sch.revision,
			sch.set_of_books_id,
			sch.MONTH,
			sch.interest_income_received,
			sch.interest_income_accrued,
			sch.interest_rental_recvd_spread,
			sch.beg_deferred_rev,
			sch.deferred_rev_activity,
			sch.end_deferred_rev,
			sch.beg_receivable,
			sch.end_receivable,
			sch.executory_accrual1,
			sch.executory_accrual2,
			sch.executory_accrual3,
			sch.executory_accrual4,
			sch.executory_accrual5,
			sch.executory_accrual6,
			sch.executory_accrual7,
			sch.executory_accrual8,
			sch.executory_accrual9,
			sch.executory_accrual10,
			sch.executory_paid1,
			sch.executory_paid2,
			sch.executory_paid3,
			sch.executory_paid4,
			sch.executory_paid5,
			sch.executory_paid6,
			sch.executory_paid7,
			sch.executory_paid8,
			sch.executory_paid9,
			sch.executory_paid10,
			sch.contingent_accrual1,
			sch.contingent_accrual2,
			sch.contingent_accrual3,
			sch.contingent_accrual4,
			sch.contingent_accrual5,
			sch.contingent_accrual6,
			sch.contingent_accrual7,
			sch.contingent_accrual8,
			sch.contingent_accrual9,
			sch.contingent_accrual10,
			sch.contingent_paid1,
			sch.contingent_paid2,
			sch.contingent_paid3,
			sch.contingent_paid4,
			sch.contingent_paid5,
			sch.contingent_paid6,
			sch.contingent_paid7,
			sch.contingent_paid8,
			sch.contingent_paid9,
			sch.contingent_paid10
		FROM v_lsr_pseudo_asset_schedule sch
		INNER JOIN lsr_asset la
			ON la.lsr_asset_id = sch.lsr_asset_id
			AND sch.revision = la.revision
		INNER JOIN lsr_ilr ilr
			ON ilr.ilr_id = la.ilr_id
			AND ilr.current_revision = sch.revision
		WHERE ilr.company_id = A_COMPANY_ID
		AND ilr.ilr_status_id IN (2,3) -- 2 = in-service; 3 = retired
		;

		----------------------------------------------------------
		--Update VP buckets
		----------------------------------------------------------
		rtn_str := pkg_lessor_var_payments.f_calc_asset_buckets_month_end(a_company_id, a_month, a_calc_type);
		IF rtn_str <> 'OK' THEN
			pkg_pp_log.p_write_message('PKG_LESSOR_VAR_PAYMENTS.F_CALC_ASSET_BUCKET_MONTH_END returned an error: ' || rtn_str);
			Raise_Application_Error(-20000, 'PKG_LESSOR_VAR_PAYMENTS.F_CALC_ASSET_BUCKET_MONTH_END returned an error: ' || rtn_str);
		END IF ;
	END p_variable_payments_calc;

	--**************************************************************************
	--                            p_accruals_calc
	--             --------------------------------
	-- @@ description
	--    this package will stage the monthly accrual numbers by lsr_ilr.
	--    it will load from the ILR schedule into lsr_monthly_accrual_stg
	--  this package uses merge statements, so it can be run multiple times.
	-- @@params
	--    company: a_company_id
	--       the company to process accruals for
	--    date: a_month
	--       the month to process accruals for
	--    end log: a_end_log
	--       whether or not to end the log. 0 = continue logging, 1 = end log
	--
	--**************************************************************************
	PROCEDURE p_accruals_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	IS
		L_ILR_SCHEDULE_TABLE		pkg_lessor_common.ilr_schedule_line_table;
		SCHEDULEINDEX number;
	BEGIN
		pkg_pp_log.p_write_message('Starting p_accruals_calc');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		----------------------------------------------------------
		--Clearing prior runs for this company/month combination
		----------------------------------------------------------
--		L_STATUS:='Clearing prior runs for this company/month combination';
		delete from lsr_monthly_accrual_stg
		where ILR_ID IN (SELECT ILR_ID FROM LSR_ILR WHERE company_id = A_COMPANY_ID)
		and gl_posting_mo_yr = A_MONTH;

		----------------------------------------------------------
		--Calculate the accrual variable payments
		----------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Handling Variable Payments - Accruals');
		p_variable_payments_calc(A_COMPANY_ID, A_MONTH, 'accruals');

		-------------------------------------------------------------------------
		--get collection of schedule rows to build accruals from
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Pulling schedule rows to use for accruals');
		SELECT sch.ilr_id,
			sch.set_of_books_id,
			1,
			sch.company_id,
			sch.month,
			0,
			sch.interest_income_accrued,
			sch.interest_unguaranteed,
			sch.recognized_profit,
			sch.initial_direct_cost,
			sch.executory_accrual1,
			sch.executory_accrual2,
			sch.executory_accrual3,
			sch.executory_accrual4,
			sch.executory_accrual5,
			sch.executory_accrual6,
			sch.executory_accrual7,
			sch.executory_accrual8,
			sch.executory_accrual9,
			sch.executory_accrual10,
			sch.contingent_accrual1,
			sch.contingent_accrual2,
			sch.contingent_accrual3,
			sch.contingent_accrual4,
			sch.contingent_accrual5,
			sch.contingent_accrual6,
			sch.contingent_accrual7,
			sch.contingent_accrual8,
			sch.contingent_accrual9,
			sch.contingent_accrual10,
			sch.accrued_rent,
			sch.deferred_rent
		BULK COLLECT INTO L_ILR_SCHEDULE_TABLE
		FROM (
			SELECT sch.ilr_id,
				sch.set_of_books_id,
				ilr.company_id,
				sch.month,
				sch.interest_income_accrued,
				sales.interest_unguaranteed_residual interest_unguaranteed,
				direct.recognized_profit recognized_profit,
				sch.initial_direct_cost,
				sch.executory_accrual1,
				sch.executory_accrual2,
				sch.executory_accrual3,
				sch.executory_accrual4,
				sch.executory_accrual5,
				sch.executory_accrual6,
				sch.executory_accrual7,
				sch.executory_accrual8,
				sch.executory_accrual9,
				sch.executory_accrual10,
				sch.contingent_accrual1,
				sch.contingent_accrual2,
				sch.contingent_accrual3,
				sch.contingent_accrual4,
				sch.contingent_accrual5,
				sch.contingent_accrual6,
				sch.contingent_accrual7,
				sch.contingent_accrual8,
				sch.contingent_accrual9,
				sch.contingent_accrual10,
				sch.accrued_rent,
				sch.deferred_rent
			FROM lsr_ilr_schedule sch
			INNER JOIN lsr_ilr ilr
				ON sch.ilr_id = ilr.ilr_id
				AND sch.revision = ilr.current_revision
			LEFT JOIN lsr_ilr_schedule_direct_fin direct
				ON sch.ilr_id = direct.ilr_id
				AND sch.revision = direct.revision
				AND sch.set_of_books_id = direct.set_of_books_id
				and sch.month = direct.month
			LEFT JOIN lsr_ilr_schedule_sales_direct sales
				ON sch.ilr_id = sales.ilr_id
				AND sch.revision = sales.revision
				AND sch.set_of_books_id = sales.set_of_books_id
				and sch.month = sales.month
			WHERE ilr.company_id = A_COMPANY_ID
			AND sch.month = A_MONTH
			AND ilr.ilr_status_id IN (2,3,4) -- 2 = in-service; 3 = retired 4 = pending termination
			AND ilr.ilr_id NOT IN (SELECT ilr_id FROM lsr_ilr_termination_st_df 
                             WHERE termination_date <= Last_Day(A_MONTH) 
                              AND Lower(termination_source) = 'early termination'
							  UNION
							  SELECT ilr_id FROM lsr_ilr_termination_op 
								WHERE termination_date <= Last_Day(A_MONTH) 
                              AND Lower(termination_source) = 'early termination')
		) sch;

		-------------------------------------------------------------------------
		--Inserting interest accrual
		-------------------------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting interest accrual');
    forall SCHEDULEINDEX in indices of L_ILR_SCHEDULE_TABLE
      merge into LSR_MONTHLY_ACCRUAL_STG stg
      using (select 0 as accrual_id,
							(SELECT Decode(Lower(fasb_ct.description),'operating',25,'sales type',2,2) accrual_type_id
								FROM lsr_ilr_options o
									JOIN lsr_ilr ilr ON ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision
									JOIN lsr_fasb_type_sob fasb_sob ON fasb_sob.cap_type_id = o.lease_cap_type_id
									JOIN lsr_fasb_cap_type fasb_ct ON fasb_ct.fasb_cap_type_id = fasb_sob.fasb_cap_type_id
								WHERE o.ilr_id = L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID
								AND fasb_sob.set_of_books_id = L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID) AS accrual_type_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ilr_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).INTEREST_INCOME as amount,
              A_MONTH as gl_posting_mo_yr,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
              from DUAL
          where L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).INTEREST_INCOME <> 0) b
      on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
          and b.ILR_ID = stg.ILR_ID
          and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
          and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
      when matched then update set stg.AMOUNT = b.AMOUNT
      when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
        values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.ILR_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);


		-------------------------------------------------------------------------
		--Inserting unguaranteed interest accrual for Sales Type and Direct Finance
		-------------------------------------------------------------------------

		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting unguaranteed interest accrual');
    forall SCHEDULEINDEX in indices of L_ILR_SCHEDULE_TABLE
      merge into LSR_MONTHLY_ACCRUAL_STG stg
      using (select 0 as accrual_id,
			  26 AS accrual_type_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ilr_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_unguaranteed as amount,
              A_MONTH as gl_posting_mo_yr,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
              from DUAL
          where L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_unguaranteed <> 0) b
      on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
          and b.ILR_ID = stg.ILR_ID
          and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
          and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
      when matched then update set stg.AMOUNT = b.AMOUNT
      when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
        values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.ILR_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

		-------------------------------------------------------------------------
		--Inserting earned profit accrual
		-------------------------------------------------------------------------

		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting unguaranteed residual interest accrual');
    forall SCHEDULEINDEX in indices of L_ILR_SCHEDULE_TABLE
      merge into LSR_MONTHLY_ACCRUAL_STG stg
      using (select 0 as accrual_id,
			  27 AS accrual_type_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ilr_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).RECOGNIZED_PROFIT as amount,
              A_MONTH as gl_posting_mo_yr,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
              from DUAL
          where L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).RECOGNIZED_PROFIT <> 0) b
      on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
          and b.ILR_ID = stg.ILR_ID
          and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
          and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
      when matched then update set stg.AMOUNT = b.AMOUNT
      when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
        values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.ILR_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

		-------------------------------------------------------------------------
		--Inserting executory accrual
		-------------------------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting executory accrual');
		PKG_LESSOR_BUCKET.P_CALC_EXEC_ACCRUALS(L_ILR_SCHEDULE_TABLE);

		-------------------------------------------------------------------------
		--Inserting contingent accrual
		-------------------------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting contingent accrual');
		PKG_LESSOR_BUCKET.P_CALC_CONT_ACCRUALS(L_ILR_SCHEDULE_TABLE);

		-------------------------------------------------------------------------
		--Inserting amortized initial cost for Operating type ILR
		-------------------------------------------------------------------------
    PKG_PP_LOG.P_WRITE_MESSAGE('Inserting Initial Direct Cost accrual');
    forall SCHEDULEINDEX in indices of L_ILR_SCHEDULE_TABLE
      merge into LSR_MONTHLY_ACCRUAL_STG stg
      using ( select 0 as accrual_id,
              28 AS accrual_type_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ilr_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).INITIAL_DIRECT_COST as amount,
              A_MONTH as gl_posting_mo_yr,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
              from DUAL
              where L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).INITIAL_DIRECT_COST <> 0
                and exists ( select 'x' 
								from LSR_ILR ILR
								INNER JOIN LSR_ILR_OPTIONS LIO
									ON LIO.ILR_ID = ILR.ILR_ID
									AND LIO.REVISION = ILR.CURRENT_REVISION
								INNER JOIN LSR_FASB_TYPE_SOB LFTS
									ON LFTS.CAP_TYPE_ID = LIO.LEASE_CAP_TYPE_ID
								INNER JOIN LSR_FASB_CAP_TYPE LFCT
									ON LFCT.FASB_CAP_TYPE_ID = LFTS.FASB_CAP_TYPE_ID
                             WHERE LFTS.SET_OF_BOOKS_ID = L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID
							 AND ILR.ILR_ID = L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID
                             AND LOWER(TRIM(LFCT.DESCRIPTION)) = 'operating'
                           )
                ) b
      on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
          and b.ILR_ID = stg.ILR_ID
          and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
          and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
      when matched then update set stg.AMOUNT = b.AMOUNT
      when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
        values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.ILR_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

		-------------------------------------------------------------------------
		--Inserting Deferred Rent
		-------------------------------------------------------------------------

		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting Deferred Rent');
    forall SCHEDULEINDEX in indices of L_ILR_SCHEDULE_TABLE
      merge into LSR_MONTHLY_ACCRUAL_STG stg
      using (select 0 as accrual_id,
			  29 AS accrual_type_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ilr_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).DEFERRED_RENT as amount,
              A_MONTH as gl_posting_mo_yr,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
              from DUAL
          where L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).DEFERRED_RENT <> 0) b
      on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
          and b.ILR_ID = stg.ILR_ID
          and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
          and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
      when matched then update set stg.AMOUNT = b.AMOUNT
      when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
        values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.ILR_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);

		-------------------------------------------------------------------------
		--Inserting Accrued Rent
		-------------------------------------------------------------------------

		PKG_PP_LOG.P_WRITE_MESSAGE('Inserting Accrued Rent');
    forall SCHEDULEINDEX in indices of L_ILR_SCHEDULE_TABLE
      merge into LSR_MONTHLY_ACCRUAL_STG stg
      using (select 0 as accrual_id,
			  30 AS accrual_type_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID as ilr_id,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ACCRUED_RENT as amount,
              A_MONTH as gl_posting_mo_yr,
              L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID as set_of_books_id
              from DUAL
          where L_ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ACCRUED_RENT <> 0) b
      on (b.ACCRUAL_TYPE_ID = stg.ACCRUAL_TYPE_ID
          and b.ILR_ID = stg.ILR_ID
          and b.GL_POSTING_MO_YR = stg.GL_POSTING_MO_YR
          and b.SET_OF_BOOKS_ID = stg.SET_OF_BOOKS_ID)
      when matched then update set stg.AMOUNT = b.AMOUNT
      when not matched then insert (ACCRUAL_ID, ACCRUAL_TYPE_ID, ILR_ID, AMOUNT, GL_POSTING_MO_YR, SET_OF_BOOKS_ID)
        values (LSR_MONTHLY_ACCRUAL_STG_SEQ.NEXTVAL, b.ACCRUAL_TYPE_ID, b.ILR_ID, b.AMOUNT, b.GL_POSTING_MO_YR, b.SET_OF_BOOKS_ID);


		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	exception
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_accruals_calc;

	--**************************************************************************
	--                            p_accruals_approve
	--             --------------------------------
	-- @@ description
	--    this procedure will approve and post the monthly accruals
	-- @@params
	--    date: a_month
	--       the month to process accruals for
	--			a_company_id
	--		 the company to process accruals for
	--			a_end_log
	--		 optional flag for whether or not the procedure should close the log
	-- @@return
	--    none
	--
	--**************************************************************************
	PROCEDURE p_accruals_approve(
			a_company_id IN NUMBER,
        	a_month      IN DATE,
        	a_end_log    IN NUMBER:=NULL)

	IS
    L_MSG VARCHAR2(2000);
    L_GL_JE_CODE VARCHAR2(35);
    L_RTN NUMBER;
	BEGIN
		pkg_pp_log.p_write_message('Starting p_accruals_approve');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));
		pkg_pp_log.p_write_message('	Processing JEs for lessor accruals');

    L_MSG:='Retrieving L_GL_JE_CODE';
    select gl_je_code
    into L_GL_JE_CODE
    from standard_journal_entries, gl_je_control
    where standard_journal_entries.je_id = gl_je_control.je_id
    and upper(ltrim(rtrim(process_id))) = 'LESSOR ACCRUALS';

    IF L_GL_JE_CODE IS NULL THEN
       L_MSG := 'Error Retrieving L_GL_JE_CODE';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       RETURN ;
    END IF;

    --Send JEs for Accruals only for Sales and Direct Finance types.
	--Use bucket admin accounts for Exec and Contingent amounts.
    FOR L_LMAS IN
      (
        WITH LSR_ADMIN_BUCKET_SUBQUERY AS
          (  select bucket_number + 2 as ACCRUAL_TYPE_KEY, RECEIVABLE_TYPE, BUCKET_NUMBER, ACCRUAL_ACCT_ID, RECEIVABLE_ACCT_ID
             from lsr_receivable_bucket_admin  where lower(receivable_type) = 'executory'
             UNION
             select bucket_number + 12 as ACCRUAL_TYPE_KEY, RECEIVABLE_TYPE, BUCKET_NUMBER, ACCRUAL_ACCT_ID, RECEIVABLE_ACCT_ID
             from lsr_receivable_bucket_admin  where lower(receivable_type) = 'contingent'
          )
        SELECT DISTINCT JMTT.JE_METHOD_ID as JE_METHOD_ID,
               LMAS.ILR_ID AS ILR_ID,
               LMAS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
               LMAS.ACCRUAL_TYPE_ID AS ACCRUAL_TYPE_ID,
               LCDR.RATE AS CONV_RATE,
               LMAS.AMOUNT AS AMOUNT,
               LEASE.CONTRACT_CURRENCY_ID AS CONTRACT_CURRENCY_ID,
               CS.CURRENCY_ID AS COMPANY_CURRENCY_ID,
               JM.AMOUNT_TYPE AS AMOUNT_TYPE,
               JMSOB.REVERSAL_CONVENTION as REVERSAL_CONVENTION,
               LFTS.FASB_CAP_TYPE_ID as FASB_CAP_TYPE_ID,
               LIA.INI_DIRECT_COST_ACCOUNT_ID,
               LIA.INT_ACCRUAL_ACCOUNT_ID,
               LIA.INT_EXPENSE_ACCOUNT_ID,
               LIA.UNGUARAN_RES_ACCOUNT_ID,
               LIA.INT_UNGUARAN_RES_ACCOUNT_ID,
			   LIA.DEF_SELLING_PROFIT_ACCOUNT_ID,
               LIA.SELL_PROFIT_LOSS_ACCOUNT_ID,
			   LIA.DEF_COSTS_ACCOUNT_ID,
               LABS.ACCRUAL_ACCT_ID,
               LABS.RECEIVABLE_ACCT_ID,
			   LIA.ACCRUED_RENT_ACCT_ID,
			   LIA.DEFERRED_RENT_ACCT_ID
        FROM lsr_monthly_accrual_stg LMAS
        INNER JOIN LSR_ILR ILR
              ON ILR.ILR_ID = LMAS.ILR_ID
        INNER JOIN LSR_LEASE LEASE
              ON LEASE.LEASE_ID = ILR.LEASE_ID
        INNER JOIN LSR_ILR_OPTIONS LIO
              ON LIO.ILR_ID = ILR.ILR_ID AND LIO.REVISION = ILR.CURRENT_REVISION
        INNER JOIN LSR_ILR_ACCOUNT LIA
              ON LIA.ILR_ID = ILR.ILR_ID
        INNER JOIN LSR_CAP_TYPE LCT
              ON LCT.CAP_TYPE_ID = LIO.LEASE_CAP_TYPE_ID
        INNER JOIN LSR_FASB_TYPE_SOB LFTS
            ON LFTS.CAP_TYPE_ID = LCT.CAP_TYPE_ID AND LFTS.SET_OF_BOOKS_ID = LMAS.SET_OF_BOOKS_ID
        INNER JOIN JE_METHOD_SET_OF_BOOKS JMSOB
            ON JMSOB.SET_OF_BOOKS_ID = LFTS.SET_OF_BOOKS_ID
        INNER JOIN JE_METHOD_TRANS_TYPE JMTT
            ON JMTT.JE_METHOD_ID = JMSOB.JE_METHOD_ID
        INNER JOIN JE_METHOD JM
            ON JM.JE_METHOD_ID = JMSOB.JE_METHOD_ID
        INNER JOIN COMPANY_JE_METHOD_VIEW CJMV
            ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JMTT.JE_METHOD_ID
        INNER JOIN CURRENCY_SCHEMA CS
            ON CS.COMPANY_ID = ILR.COMPANY_ID
        INNER JOIN LS_LEASE_CALCULATED_DATE_RATES LCDR
            ON LCDR.COMPANY_ID = ILR.COMPANY_ID AND LCDR.CONTRACT_CURRENCY_ID = LEASE.CONTRACT_CURRENCY_ID AND LCDR.COMPANY_CURRENCY_ID = CS.CURRENCY_ID
                            AND LCDR.ACCOUNTING_MONTH = LMAS.GL_POSTING_MO_YR
		INNER JOIN PP_SYSTEM_CONTROL_COMPANIES AVG_RATES
            ON AVG_RATES.COMPANY_ID = ILR.COMPANY_ID
        LEFT JOIN LSR_ADMIN_BUCKET_SUBQUERY LABS
             ON LMAS.ACCRUAL_TYPE_ID = LABS.ACCRUAL_TYPE_KEY
        WHERE JMTT.TRANS_TYPE IN
              ( SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE
                WHERE TRANS_TYPE BETWEEN 4010 and 4017 OR TRANS_TYPE IN (4032,4033,4034,4035,4036,4037,4056,4057)
              )
              AND ILR.COMPANY_ID = A_COMPANY_ID
              AND LMAS.GL_POSTING_MO_YR = A_MONTH
              AND ( LMAS.ACCRUAL_TYPE_ID BETWEEN 2 AND 22 OR LMAS.ACCRUAL_TYPE_ID IN (25,26,27,28,29,30) )
              AND LMAS.AMOUNT <> 0.0
              AND AVG_RATES.CONTROL_NAME = 'Lease MC: Use Average Rates'
              AND LCDR.EXCHANGE_RATE_TYPE_ID = DECODE(LOWER(AVG_RATES.CONTROL_VALUE), 'yes', 4, 1)
        ORDER BY JE_METHOD_ID, ILR_ID, SET_OF_BOOKS_ID, ACCRUAL_TYPE_ID
      )
      LOOP
        CASE
          WHEN L_LMAS.ACCRUAL_TYPE_ID = 2 THEN
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4010, L_LMAS.AMOUNT ,
                L_LMAS.INT_ACCRUAL_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4011, L_LMAS.AMOUNT ,
                L_LMAS.INT_EXPENSE_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

          WHEN L_LMAS.ACCRUAL_TYPE_ID >= 3 AND L_LMAS.ACCRUAL_TYPE_ID <= 12 THEN
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4012, L_LMAS.AMOUNT ,
                L_LMAS.ACCRUAL_ACCT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4013, L_LMAS.AMOUNT ,
                L_LMAS.RECEIVABLE_ACCT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

          WHEN L_LMAS.ACCRUAL_TYPE_ID >= 13 AND L_LMAS.ACCRUAL_TYPE_ID <= 22 THEN
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4014, L_LMAS.AMOUNT ,
                L_LMAS.ACCRUAL_ACCT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4015, L_LMAS.AMOUNT ,
                L_LMAS.RECEIVABLE_ACCT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

		  WHEN L_LMAS.ACCRUAL_TYPE_ID = 25 THEN --Operating Income
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4010, L_LMAS.AMOUNT ,
                L_LMAS.DEFERRED_RENT_ACCT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4011, L_LMAS.AMOUNT ,
                L_LMAS.INT_EXPENSE_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

          WHEN L_LMAS.ACCRUAL_TYPE_ID = 26 THEN
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4016, L_LMAS.AMOUNT ,
                L_LMAS.UNGUARAN_RES_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4017, L_LMAS.AMOUNT ,
                L_LMAS.INT_UNGUARAN_RES_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

          WHEN L_LMAS.ACCRUAL_TYPE_ID = 27 AND L_LMAS.FASB_CAP_TYPE_ID in (2,3) THEN
               -- Replace SELL_PROFIT_LOSS_ACCOUNT_ID with DEF_SELLING_PROFIT_ACCT_ID when created
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4056, L_LMAS.AMOUNT ,
                L_LMAS.DEF_SELLING_PROFIT_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4057, L_LMAS.AMOUNT ,
                L_LMAS.SELL_PROFIT_LOSS_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

		  WHEN L_LMAS.ACCRUAL_TYPE_ID = 28 THEN --Operating type - IDC Amortization
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4032, L_LMAS.AMOUNT ,
                L_LMAS.INI_DIRECT_COST_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;
               -- Replace INI_DIRECT_COST_ACCOUNT_ID with INI_DIRECT_COST_DEF_ACCOUNT_ID when created
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4033, L_LMAS.AMOUNT ,
                L_LMAS.DEF_COSTS_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

		  WHEN L_LMAS.ACCRUAL_TYPE_ID = 29 THEN --Deferred Rent
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4036, L_LMAS.AMOUNT,
                L_LMAS.INT_ACCRUAL_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;
               -- Replace INI_DIRECT_COST_ACCOUNT_ID with INI_DIRECT_COST_DEF_ACCOUNT_ID when created
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4037, L_LMAS.AMOUNT,
                L_LMAS.DEFERRED_RENT_ACCT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

		  WHEN L_LMAS.ACCRUAL_TYPE_ID = 30 THEN --Accrued Rent
               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4034, L_LMAS.AMOUNT ,
                L_LMAS.ACCRUED_RENT_ACCT_ID, 0, A_COMPANY_ID, A_MONTH,
                1, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;

               L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_LMAS.ILR_ID, 4035, L_LMAS.AMOUNT ,
                L_LMAS.INT_ACCRUAL_ACCOUNT_ID, 0, A_COMPANY_ID, A_MONTH,
                0, L_GL_JE_CODE, L_LMAS.SET_OF_BOOKS_ID, L_LMAS.JE_METHOD_ID, L_LMAS.AMOUNT_TYPE, L_LMAS.REVERSAL_CONVENTION,
                L_LMAS.CONV_RATE, L_LMAS.CONTRACT_CURRENCY_ID, L_LMAS.COMPANY_CURRENCY_ID, L_MSG);

               IF L_RTN = -1 then
                  PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                  ROLLBACK;
                  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
               END IF;
          ELSE
			raise_application_error(-20000, substr('Error processing Lessor Accrual JEs - No valid case for JE processing found' || chr(10) || f_get_call_stack, 1, 2000));
        END CASE;
      END LOOP;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;

    exception
    when OTHERS THEN
      IF SQLCODE BETWEEN -20999 AND -20000 THEN
        rollback;
        pkg_pp_log.p_write_message(sqlerrm);
        IF a_end_log = 1 THEN
          pkg_pp_log.p_end_log;
        END IF;
        RAISE;
      --PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
      ELSIF SQLCODE = 100 THEN
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing Lessor Accrual JEs - ' || sqlerrm || f_get_call_stack);
        IF a_end_log = 1 THEN
          pkg_pp_log.p_end_log;
        END IF;
        raise_application_error(-20000, substr('Error processing Lessor Accrual JEs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
      ELSE
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing Lessor Accrual JEs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
        IF a_end_log = 1 THEN
          pkg_pp_log.p_end_log;
        END IF;
        RAISE;
      END IF;

	END p_accruals_approve;

	--**************************************************************************
	--                            p_invoice_calc
	--             --------------------------------
	-- @@ description
	--    this function generates Lessor invoices based on the schedule lines for a company and month
	-- @@params
	--	  company: a_company_id
	--			the company to process for
	--    date: a_month
	--       the month to process accruals for
	-- @@return
	--		none - any errors are returned via exceptions.
	--
	--**************************************************************************
	PROCEDURE p_invoice_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)

	IS
		missing_workflows		VARCHAR2(4000);
		ilr_schedule_table		pkg_lessor_common.ilr_schedule_line_table;
	BEGIN
		----------------------------------------------------------
		--Start log and print inputs
		----------------------------------------------------------
		pkg_pp_log.p_write_message('Starting p_invoice_calc');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		----------------------------------------------------------
		--Calculate the invoice variable payments
		----------------------------------------------------------
		PKG_PP_LOG.P_WRITE_MESSAGE('Handling Variable Payments - Invoices');
		p_variable_payments_calc(A_COMPANY_ID, A_MONTH, 'invoices');

		----------------------------------------------------------
		--Clear previous records if they're initiated or rejected
		----------------------------------------------------------
		pkg_pp_log.p_write_message('Deleting records from LSR_INVOICE_LINE');
		DELETE FROM lsr_invoice_line line
		WHERE EXISTS (
			SELECT 1
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON inv.invoice_id = app.invoice_id
			WHERE line.invoice_id = inv.invoice_id
			AND ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
			AND app.approval_status_id IN (1, 4, 7) -- 1 = initiated; 4 = rejected; 7 = unrejected
		);

		pkg_pp_log.p_write_message('Deleting records from WORKFLOW_DETAIL');
		DELETE FROM WORKFLOW_DETAIL
			WHERE  workflow_id IN
				   ( SELECT workflow_id
					 FROM   WORKFLOW
					 WHERE  id_field1 IN
							( SELECT To_char( invoice_id )
							  FROM   LSR_INVOICE_APPROVAL app
							  WHERE  approval_status_id in (1,4,7) ) AND
							subsystem = 'lsr_invoice_approval' );


		pkg_pp_log.p_write_message('Deleting records from WORKFLOW');
		DELETE FROM WORKFLOW
		WHERE  id_field1 IN
			   ( SELECT To_char( invoice_id )
				 FROM   LSR_INVOICE_APPROVAL
				 WHERE  approval_status_id in (1,4,7) ) AND
			   subsystem = 'lsr_invoice_approval';

		pkg_pp_log.p_write_message('Deleting records from LSR_INVOICE_APPROVAL');
		DELETE FROM lsr_invoice_approval app
		WHERE app.invoice_id IN (
			SELECT inv.invoice_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON inv.invoice_id = app.invoice_id
			WHERE ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
		)
		AND app.approval_status_id IN (1, 4, 7); -- 1 = initiated; 4 = rejected; 7 = unrejected

		pkg_pp_log.p_write_message('Deleting records from LSR_INVOICE');
		DELETE FROM lsr_invoice inv
		WHERE EXISTS (
			SELECT 1
			FROM lsr_ilr ilr
			WHERE inv.ilr_id = ilr.ilr_id
			AND ilr.company_id = A_COMPANY_ID
		)
		AND NOT EXISTS ( --we've already removed these records from the approval table, so just remove anything not in the approval table
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE app.invoice_id = inv.invoice_id
		)
		AND inv.gl_posting_mo_yr = A_MONTH;

		-------------------------------------------------------------------------
		--get collection of schedule rows to build invoice lines from
		--	This needs to be before we insert into LSR_INVOICE because of the NOT EXISTS clauses
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Pulling schedule rows to use for invoice lines');
		SELECT sch.ilr_id,
			sch.set_of_books_id,
			Row_Number() OVER (PARTITION BY sch.ilr_id, sch.set_of_books_id ORDER BY sch.ilr_id, sch.set_of_books_id),
			sch.company_id,
			sch.month,
			sch.principal_received,
			sch.interest_income_received,
			sch.interest_unguaranteed,
			sch.initial_direct_cost,
			sch.recognized_profit,
			sch.executory_paid1,
			sch.executory_paid2,
			sch.executory_paid3,
			sch.executory_paid4,
			sch.executory_paid5,
			sch.executory_paid6,
			sch.executory_paid7,
			sch.executory_paid8,
			sch.executory_paid9,
			sch.executory_paid10,
			sch.contingent_paid1,
			sch.contingent_paid2,
			sch.contingent_paid3,
			sch.contingent_paid4,
			sch.contingent_paid5,
			sch.contingent_paid6,
			sch.contingent_paid7,
			sch.contingent_paid8,
			sch.contingent_paid9,
			sch.contingent_paid10,
			sch.accrued_rent,
			sch.deferred_rent
		BULK COLLECT INTO ilr_schedule_table
		FROM (
			SELECT sch.ilr_id,
				sch.set_of_books_id,
				ilr.company_id,
				sch.month,
				sch_sd.principal_received,
				sch.interest_income_received,
				sch_sd.interest_unguaranteed_residual interest_unguaranteed,
				direct.recognized_profit recognized_profit,
				0 as initial_direct_cost,
				sch.executory_paid1,
				sch.executory_paid2,
				sch.executory_paid3,
				sch.executory_paid4,
				sch.executory_paid5,
				sch.executory_paid6,
				sch.executory_paid7,
				sch.executory_paid8,
				sch.executory_paid9,
				sch.executory_paid10,
				sch.contingent_paid1,
				sch.contingent_paid2,
				sch.contingent_paid3,
				sch.contingent_paid4,
				sch.contingent_paid5,
				sch.contingent_paid6,
				sch.contingent_paid7,
				sch.contingent_paid8,
				sch.contingent_paid9,
				sch.contingent_paid10,
				sch.accrued_rent,
				sch.deferred_rent
			FROM lsr_ilr_schedule_sales_direct sch_sd
			INNER JOIN lsr_ilr_schedule sch
				ON sch.ilr_id = sch_sd.ilr_id
				AND sch.set_of_books_id = sch_sd.set_of_books_id
				AND sch.month = sch_sd.month
				AND sch.revision = sch_sd.revision
			LEFT JOIN lsr_ilr_schedule_direct_fin direct
				ON sch.ilr_id = direct.ilr_id
				AND sch.revision = direct.revision
				AND sch.set_of_books_id = direct.set_of_books_id
				and sch.month = direct.month
			INNER JOIN lsr_ilr ilr
				ON sch.ilr_id = ilr.ilr_id
				AND sch.revision = ilr.current_revision
			INNER JOIN lsr_ilr_options opt
				ON ilr.ilr_id = opt.ilr_id
				AND ilr.current_revision = opt.revision
			INNER JOIN lsr_fasb_type_sob fasb_sob
				ON opt.lease_cap_type_id = fasb_sob.cap_type_id
				AND sch.set_of_books_id = fasb_sob.set_of_books_id
			WHERE ilr.company_id = A_COMPANY_ID
			AND sch.month = A_MONTH
			AND ilr.ilr_status_id IN (2,3,4) -- 2 = in-service; 3 = retired 4 = pending termination
			AND ilr.ilr_id NOT IN (SELECT ilr_id FROM lsr_ilr_termination_st_df 
                             WHERE termination_date <= Last_Day(A_MONTH) 
                              AND Lower(termination_source) = 'early termination')
			AND NOT EXISTS (
				SELECT 1
				FROM lsr_invoice inv
				WHERE inv.gl_posting_mo_yr = A_MONTH
				AND inv.ilr_id = ilr.ilr_id
			)
			AND fasb_sob.fasb_cap_type_id IN (2,3)
			UNION
			SELECT sch.ilr_id,
				sch.set_of_books_id,
				ilr.company_id,
				sch.month,
				0 AS principal_received,
				sch.interest_income_received,
				0 AS interest_unguaranteed,
				0 AS recognized_profit,
				0 as initial_direct_cost,
				sch.executory_paid1,
				sch.executory_paid2,
				sch.executory_paid3,
				sch.executory_paid4,
				sch.executory_paid5,
				sch.executory_paid6,
				sch.executory_paid7,
				sch.executory_paid8,
				sch.executory_paid9,
				sch.executory_paid10,
				sch.contingent_paid1,
				sch.contingent_paid2,
				sch.contingent_paid3,
				sch.contingent_paid4,
				sch.contingent_paid5,
				sch.contingent_paid6,
				sch.contingent_paid7,
				sch.contingent_paid8,
				sch.contingent_paid9,
				sch.contingent_paid10,
			    sch.accrued_rent,
			    sch.deferred_rent
			FROM lsr_ilr_schedule sch
			INNER JOIN lsr_ilr ilr
				ON sch.ilr_id = ilr.ilr_id
				AND sch.revision = ilr.current_revision
			INNER JOIN lsr_ilr_options opt
				ON ilr.ilr_id = opt.ilr_id
				AND ilr.current_revision = opt.revision
			INNER JOIN lsr_fasb_type_sob fasb_sob
				ON opt.lease_cap_type_id = fasb_sob.cap_type_id
				AND sch.set_of_books_id = fasb_sob.set_of_books_id
			WHERE ilr.company_id = A_COMPANY_ID
			AND sch.MONTH = A_MONTH
			AND ilr.ilr_status_id IN (2,3,4) -- 2 = in-service; 3 = retired 4 = pending termination
			AND ilr.ilr_id NOT IN (SELECT ilr_id FROM lsr_ilr_termination_op 
                             WHERE termination_date <= Last_Day(A_MONTH) 
                              AND Lower(termination_source) = 'early termination') 
			AND NOT EXISTS (
				SELECT 1
				FROM lsr_invoice inv
				WHERE inv.gl_posting_mo_yr = A_MONTH
				AND inv.ilr_id = ilr.ilr_id
			)
			AND fasb_sob.fasb_cap_type_id IN (1) -- 1 = operating
		) sch;

		----------------------------------------------------------
		--Create invoice headers
		----------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice header rows');
		INSERT INTO lsr_invoice (invoice_id, invoice_number, ilr_id, gl_posting_mo_yr)
		SELECT lsr_invoice_seq.NEXTVAL,
			SubStr('AUTO ' || To_Char(A_MONTH, 'MM/YYYY') || '-' || To_Char(A_COMPANY_ID) || '-' || To_Char(a.ilr_id), 0, 35),
			a.ilr_id,
			A_MONTH
		FROM (
			SELECT DISTINCT ilr.ilr_id AS ilr_id,
				ilr.company_id AS company_id
			FROM lsr_ilr_schedule sch
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = sch.ilr_id
				AND sch.revision = ilr.current_revision
			WHERE ilr.ilr_status_id IN (2,3,4) -- 2 = in-service; 3 = retired
			AND sch.MONTH = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND ilr.ilr_id not in (SELECT ilr_id FROM lsr_ilr_termination_op 
                             WHERE termination_date <= Last_Day(A_MONTH) 
                              AND Lower(termination_source) = 'early termination'
							  union
							  SELECT ilr_id FROM lsr_ilr_termination_st_df 
                             WHERE termination_date <= Last_Day(A_MONTH) 
                              AND Lower(termination_source) = 'early termination') 
			AND NOT EXISTS (
				SELECT 1
				FROM lsr_invoice inv
				WHERE inv.gl_posting_mo_yr = A_MONTH
				AND inv.ilr_id = ilr.ilr_id
			)
		) a;

		----------------------------------------------------------
		--Create approval rows
		----------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice approval rows');
		INSERT INTO lsr_invoice_approval (invoice_id, approval_type_id, approval_status_id)
		SELECT inv.invoice_id,
			Decode(Nvl(lg.requires_approval, 1),
				0,
				auto.workflow_type_id,
				man.workflow_type_id),
			1
		FROM lsr_invoice inv
		INNER JOIN lsr_ilr ilr
			ON inv.ilr_id = ilr.ilr_id
		INNER JOIN lsr_lease lease
			ON ilr.lease_id = lease.lease_id
		INNER JOIN lsr_lease_group lg
			ON lease.lease_group_id = lg.lease_group_id
		FULL OUTER JOIN workflow_type auto
			ON lower(nvl(auto.external_workflow_type, ' ')) = 'auto'
			AND Lower(auto.subsystem) LIKE '%lsr_invoice_approval%'
			AND auto.active = 1
		FULL OUTER JOIN workflow_type man
			ON lower(nvl(man.external_workflow_type, ' ')) <> 'auto'
			AND Lower(man.subsystem) LIKE '%lsr_invoice_approval%'
			AND man.active = 1
		WHERE ilr.company_id = A_COMPANY_ID
		AND inv.gl_posting_mo_yr = A_MONTH
		AND NOT EXISTS (
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE inv.invoice_id = app.invoice_id
		);

		-------------------------------------------------------------------------
		--Validate that all approval records were able to find a workflow_type_id
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Validating that all approval workflows were found');
		missing_workflows := '';
		FOR rec IN (SELECT DISTINCT Nvl(lg.requires_approval, 1) AS requires_approval
					FROM lsr_invoice_approval app
					INNER JOIN lsr_invoice inv
						ON inv.invoice_id = app.invoice_id
					INNER JOIN lsr_ilr ilr
						ON ilr.ilr_id = inv.ilr_id
					INNER JOIN lsr_lease lease
						ON lease.lease_id = ilr.lease_id
					INNER JOIN lsr_lease_group lg
						ON lg.lease_group_id = lease.lease_group_id
					WHERE app.approval_type_id IS NULL
					AND inv.gl_posting_mo_yr = A_MONTH
					AND ilr.company_id = A_COMPANY_ID
					AND app.approval_status_id = 1) --1 = initiated
		LOOP
			--if this isn't our first record, add the comma
			IF Nvl(Length(missing_workflows),0) <> 0 THEN
				missing_workflows := missing_workflows || ', ';
			END IF;

			IF rec.requires_approval = 0 THEN
				missing_workflows := missing_workflows || 'Could not find auto approval workflow for "lsr_invoice_approval.';
			ELSE
				missing_workflows := missing_workflows || 'Could not find manual approval workflow for "lsr_invoice_approval.';
			END IF;
		END LOOP;

		IF Nvl(Length(missing_workflows), 0) <> 0 THEN
			--there was at least one missing workflow - throw error
			Raise_Application_Error(-20000, missing_workflows);
		END IF;

		-------------------------------------------------------------------------
		--Create invoice lines for principal received
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for principal_received');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				1, --1 = principal
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).principal_received,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).principal_received <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for interest received
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for interest_income_received');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				(SELECT Decode(Lower(fasb_ct.description),'operating',25,'sales type',2,2) invoice_type_id --2 = Interest, 25 = Income
								FROM lsr_ilr_options o
									JOIN lsr_ilr ilr ON ilr.ilr_id = o.ilr_id AND ilr.current_revision = o.revision
									JOIN lsr_fasb_type_sob fasb_sob ON fasb_sob.cap_type_id = o.lease_cap_type_id
									JOIN lsr_fasb_cap_type fasb_ct ON fasb_ct.fasb_cap_type_id = fasb_sob.fasb_cap_type_id
								WHERE o.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ILR_ID
								AND fasb_sob.set_of_books_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).SET_OF_BOOKS_ID) AS invoice_type_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_income,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_income <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for interest unguaranteed
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for interest_unguaranteed');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				26,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_unguaranteed,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).interest_unguaranteed <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Create invoice lines for earned profit
		-------------------------------------------------------------------------
		pkg_pp_log.p_write_message('Generating invoice lines for earned profit');
		FORALL scheduleindex IN INDICES OF ilr_schedule_table
			INSERT INTO lsr_invoice_line (invoice_id, invoice_line_number, invoice_type_id, amount, description, notes, set_of_books_id)
			SELECT inv.invoice_id,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).rownumber,
				27,
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).recognized_profit,
				'', --description
				'Automatically generated on ' || TO_CHAR(sysdate) || '.', --notes
				ILR_SCHEDULE_TABLE(SCHEDULEINDEX).set_of_books_id
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			INNER JOIN lsr_invoice_approval app
				ON app.invoice_id = inv.invoice_id
			WHERE inv.gl_posting_mo_yr = A_MONTH
			AND ilr.company_id = A_COMPANY_ID
			AND ilr.ilr_id = ILR_SCHEDULE_TABLE(SCHEDULEINDEX).ilr_id
			AND ILR_SCHEDULE_TABLE(SCHEDULEINDEX).recognized_profit <> 0
			AND app.approval_status_id = 1; --1 = initiated

		-------------------------------------------------------------------------
		--Call procedures for executory and contingent buckets
		-------------------------------------------------------------------------
		pkg_lessor_bucket.p_calc_exec_invoices(ilr_schedule_table);
		pkg_lessor_bucket.p_calc_cont_invoices(ilr_schedule_table);

		-------------------------------------------------------------------------
		--Rollup payment lines to the header
		-------------------------------------------------------------------------
		UPDATE lsr_invoice inv
		SET (inv.invoice_principal, inv.invoice_interest, inv.invoice_executory, inv.invoice_contingent, inv.invoice_recognized_profit) = (
			SELECT
				Sum(CASE WHEN line.invoice_type_id = 1 THEN line.amount ELSE 0 END) AS invoice_principal,
				Sum(CASE WHEN line.invoice_type_id IN (2,25,26) THEN line.amount ELSE 0 END) AS invoice_interest,
				Sum(CASE WHEN line.invoice_type_id IN (3,4,5,6,7,8,9,10,11,12) THEN line.amount ELSE 0 END) AS invoice_executory,
				Sum(CASE WHEN line.invoice_type_id IN (13,14,15,16,17,18,19,20,21,22) THEN line.amount ELSE 0 END) AS invoice_contingent,
				Sum(CASE WHEN line.invoice_type_id = 27 THEN line.amount ELSE 0 END) AS invoice_recognized_profit
			FROM lsr_invoice_line line
			WHERE line.invoice_id = inv.invoice_id
			AND line.set_of_books_id = (
				SELECT Min(set_of_books_id)
				FROM lsr_invoice_line line2
				WHERE line.invoice_id = line2.invoice_id
			)
			GROUP BY line.invoice_id
		)
		WHERE EXISTS (
			SELECT 1
			FROM lsr_ilr ilr
			WHERE ilr.ilr_id = inv.ilr_id
			AND ilr.company_id = A_COMPANY_ID
		)
		AND inv.gl_posting_mo_yr = A_MONTH
		AND EXISTS (
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE app.invoice_id = inv.invoice_id
			AND app.approval_status_id = 1
		);

		-----------------------------------------------------------------------
		--Delete approvals and headers with no invoice lines
		-----------------------------------------------------------------------
		DELETE FROM lsr_invoice_approval app
		WHERE NOT EXISTS (
			SELECT 1
			FROM lsr_invoice_line line
			WHERE app.invoice_id = line.invoice_id
		)
		AND EXISTS ( --only remove invoices from this company and month
			SELECT 1
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = inv.ilr_id
			WHERE app.invoice_id = inv.invoice_id
			AND ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
		)
		AND app.approval_status_id = 1;

		DELETE FROM lsr_invoice inv
		WHERE NOT EXISTS (
			SELECT 1
			FROM lsr_invoice_line line
			WHERE inv.invoice_id = line.invoice_id
		)
		AND NOT EXISTS ( --we've already removed these records from the approval table, so just remove anything not in the approval table
			SELECT 1
			FROM lsr_invoice_approval app
			WHERE app.invoice_id = inv.invoice_id
		)
		AND EXISTS ( --make sure we're only looking at this company's ILRs
			SELECT 1
			FROM lsr_ilr ilr
			WHERE ilr.ilr_id = inv.ilr_id
			AND ilr.company_id = A_COMPANY_ID
		)
		AND inv.gl_posting_mo_yr = A_MONTH;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_end_log;
		END IF;
	exception
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_invoice_calc;

	--**************************************************************************
	--                            p_invoice_approve
	--             --------------------------------
	-- @@ description
	--    this function will approve and post the monthly invoice numbers
	-- @@params
	--    number: a_company_id
	--		the company to process invoices for
	--    date: a_month
	--      the month to process invoices for
	--	  number: a_end_log
	--		whether the log should be closed at the end of the process or not
	-- @@return
	--    no return
	--
	--**************************************************************************
	PROCEDURE p_invoice_approve(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log    IN NUMBER:=NULL)
	IS
		l_unapproved_count 	INTEGER;
		l_msg VARCHAR2(2000);
		l_gl_je_code VARCHAR2(35);
		l_rtn NUMBER;
	BEGIN
		pkg_pp_log.p_write_message('Starting p_invoice_approve');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		-----------------------------------------------------------------------
		--Auto approve invoices that meeting the following criteria:
		--	1. Lease group is set to auto approve
		--	2. Workflow type is auto approve
		--	3. Status is not in (pending approval, approved, rejected, auto-approved)
		-----------------------------------------------------------------------
		pkg_pp_log.p_write_message('Auto-Approving Invoices');

		UPDATE lsr_invoice_approval app
		SET app.approval_status_id = 6,
			app.approval_date = SYSDATE
		WHERE EXISTS ( --lease group set to auto_approve
			SELECT 1
			FROM lsr_invoice invoice
			JOIN lsr_ilr ilr ON invoice.ilr_id = ilr.ilr_id
			JOIN lsr_lease lease ON ilr.lease_id = lease.lease_id
			JOIN lsr_lease_group grp ON lease.lease_group_id = grp.lease_group_id
			WHERE app.invoice_id = invoice.invoice_id
			AND ilr.company_id = a_company_id
			AND invoice.gl_posting_mo_yr = a_month
			AND coalesce(grp.requires_approval, 1) = 0
		)
		AND EXISTS ( -- workflow type is auto approve
			SELECT 1
			FROM workflow_type wt
			WHERE wt.workflow_type_id = app.approval_type_id
			AND Lower(Nvl(wt.external_workflow_type, '')) = 'auto'
			AND Lower(wt.subsystem) LIKE '%lsr_invoice_approval%'
		)
		AND app.approval_status_id NOT IN (2,3,4,6); --respectively: Pending Approval, Approved, Rejected, Approved - Auto

		-----------------------------------------------------------------------
		--Validate that all invoices for the month are either approved or auto approved
		-----------------------------------------------------------------------
		pkg_pp_log.p_write_message('Checking for any unapproved Invoices');

		SELECT Count(1)
		INTO l_unapproved_count
		FROM lsr_invoice_approval app
		WHERE EXISTS (
			SELECT 1
			FROM lsr_invoice inv
			INNER JOIN lsr_ilr ilr
				ON inv.ilr_id = ilr.ilr_id
			WHERE app.invoice_id = inv.invoice_id
			AND ilr.company_id = A_COMPANY_ID
			AND inv.gl_posting_mo_yr = A_MONTH
		)
		AND app.approval_status_id NOT IN (3,6); --3 = approved, 6 = auto-approved

		IF l_unapproved_count <> 0 THEN
			Raise_Application_Error(-20000, 'ERROR: ' || l_unapproved_count || ' lessor invoices have not been approved for company_id ' || A_COMPANY_ID || ' and month ' || A_MONTH || '.');
		END IF;

		-----------------------------------------------------------------------
		--Now that we know all the pending invoice records have been approved, we can send the JEs
		-----------------------------------------------------------------------
		pkg_pp_log.p_write_message('Retrieving L_GL_JE_CODE for Lessor Invoices');

		select gl_je_code
		into l_gl_je_code
		from standard_journal_entries, gl_je_control
		where standard_journal_entries.je_id = gl_je_control.je_id
		and upper(ltrim(rtrim(process_id))) = 'LESSOR INVOICES';

		IF l_gl_je_code IS NULL THEN
		   l_msg := 'Error Retrieving L_GL_JE_CODE for Lessor Invoices';
		   PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
		   RETURN;
		END IF;

		--Send JEs for Invoices
		pkg_pp_log.p_write_message('Pulling Invoice records to send JEs');

		FOR l_invoice IN
		(
        WITH receivable_buckets AS (
		    SELECT bucket_number + 2 AS invoice_type_key, receivable_type, bucket_number, accrual_acct_id
		    FROM lsr_receivable_bucket_admin  WHERE LOWER(receivable_type) = 'executory'
		    UNION
		    SELECT bucket_number + 12 AS invoice_type_key, receivable_type, bucket_number, accrual_acct_id
		    FROM lsr_receivable_bucket_admin  WHERE LOWER(receivable_type) = 'contingent'
		)
		SELECT
			DISTINCT jmsob.je_method_id AS je_method_id,
			li.ilr_id AS ilr_id,
			lil.set_of_books_id AS set_of_books_id,
			lil.invoice_type_id AS invoice_type_id,
			lcdr.rate AS conv_rate,
			lil.amount AS amount,
			lease.contract_currency_id AS contract_currency_id,
			cs.currency_id AS company_currency_id,
			jm.amount_type AS amount_type,
			jmsob.reversal_convention AS reversal_convention,
			lfts.fasb_cap_type_id AS fasb_cap_type_id,
			lia.ar_account_id AS ar_account_id, --for 4018, accounts receivable debit
			lia.int_accrual_account_id AS int_accrual_account_id, --for 4019, interest invoice credit
			lia.st_receivable_account_id AS st_receivable_account_id, --for 4022, st receivable credit
			buckets.accrual_acct_id AS accrual_acct_id --for 4020/21, executory/contingent invoice credit
		FROM lsr_invoice li
			INNER JOIN lsr_invoice_line lil ON li.invoice_id = lil.invoice_id
			INNER JOIN lsr_ilr ilr ON ilr.ilr_id = li.ilr_id
			INNER JOIN lsr_lease lease ON lease.lease_id = ilr.lease_id
			INNER JOIN lsr_ilr_options lio ON lio.ilr_id = ilr.ilr_id AND lio.revision = ilr.current_revision
			INNER JOIN lsr_ilr_account lia ON lia.ilr_id = ilr.ilr_id
			INNER JOIN lsr_fasb_type_sob lfts ON lfts.cap_type_id = lio.lease_cap_type_id AND lfts.set_of_books_id = lil.set_of_books_id
			INNER JOIN je_method_set_of_books jmsob ON jmsob.set_of_books_id = lfts.set_of_books_id
			INNER JOIN je_method_trans_type jmtt ON jmtt.je_method_id = jmsob.je_method_id
			INNER JOIN Je_method jm ON jm.je_method_id = jmsob.je_method_id
			INNER JOIN company_je_method_view cjmv ON cjmv.company_id = ilr.company_id AND cjmv.je_method_id = jmtt.je_method_id
			INNER JOIN currency_schema cs ON cs.company_id = ilr.company_id
			INNER JOIN ls_lease_calculated_date_rates lcdr ON lcdr.company_id = ilr.company_id AND lcdr.contract_currency_id = lease.contract_currency_id
				AND lcdr.company_currency_id = cs.currency_id AND lcdr.accounting_month = li.gl_posting_mo_yr
		    INNER JOIN pp_system_control_companies avg_rates on avg_rates.company_id = ilr.company_id
			LEFT OUTER JOIN receivable_buckets buckets ON lil.invoice_type_id = buckets.invoice_type_key
		WHERE jmsob.je_method_id IN (SELECT DISTINCT je_method_id FROM je_method_trans_type WHERE trans_type BETWEEN 4018 AND 4022)
		  AND ilr.company_id = A_COMPANY_ID
		  AND li.gl_posting_mo_yr = A_MONTH
		  AND (lil.invoice_type_id BETWEEN 1 AND 22 OR lil.invoice_type_id IN (25))
		  AND lfts.fasb_cap_type_id IN (1,2,3)
          AND AVG_RATES.CONTROL_NAME = 'Lease MC: Use Average Rates'
          AND LCDR.EXCHANGE_RATE_TYPE_ID = DECODE(LOWER(AVG_RATES.CONTROL_VALUE), 'yes', 4, 1)
		  AND cs.currency_type_id = 1
		ORDER BY je_method_id, ilr_id, set_of_books_id, invoice_type_id
        )
        LOOP
			CASE
			--Send the JEs for Short Term Receivable Credit (and balancing Accounts Receivable Debit)
			WHEN l_invoice.invoice_type_id = 1 AND l_invoice.fasb_cap_type_id IN (2,3) THEN
				IF l_invoice.amount <> 0 THEN
					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4022, l_invoice.amount,
						l_invoice.st_receivable_account_id, 0, A_COMPANY_ID, A_MONTH,
						0, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;

					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4018, l_invoice.amount,
						l_invoice.ar_account_id, 0, A_COMPANY_ID, A_MONTH,
						1, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;
				END IF;
			--Send the JEs for the Interest Invoice Credit (and balancing Accounts Receivable Debit)
			WHEN (l_invoice.invoice_type_id = 2 and l_invoice.fasb_cap_type_id IN (2,3)) OR (l_invoice.invoice_type_id = 25 and l_invoice.fasb_cap_type_id = 1) THEN
				IF l_invoice.amount <> 0 THEN
					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4019, l_invoice.amount,
						l_invoice.int_accrual_account_id, 0, A_COMPANY_ID, A_MONTH,
						0, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;

					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4018, l_invoice.amount,
						l_invoice.ar_account_id, 0, A_COMPANY_ID, A_MONTH,
						1, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;
				END IF;
			--Send the JEs for the Executory Invoice Credit (and balancing Accounts Receivable Debit)
			WHEN l_invoice.invoice_type_id >= 3 AND l_invoice.invoice_type_id <= 12 THEN
				IF l_invoice.amount <> 0 THEN
					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4020, l_invoice.amount,
						l_invoice.accrual_acct_id, 0, A_COMPANY_ID, A_MONTH,
						0, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;

					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4018, l_invoice.amount,
						l_invoice.ar_account_id, 0, A_COMPANY_ID, A_MONTH,
						1, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;
				END IF;
			--Send the JEs for Contingent Invoice Credit (and balancing Accounts Receivable Debit)
			WHEN l_invoice.invoice_type_id >= 13 and l_invoice.invoice_type_id <= 22 THEN
				IF l_invoice.amount <> 0 THEN
					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4021, l_invoice.amount,
						l_invoice.accrual_acct_id, 0, A_COMPANY_ID, A_MONTH,
						0, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;

					l_rtn := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_invoice.ilr_id, 4018, l_invoice.amount,
						l_invoice.ar_account_id, 0, A_COMPANY_ID, A_MONTH,
						1, l_gl_je_code, l_invoice.set_of_books_id, l_invoice.je_method_id, l_invoice.amount_type, l_invoice.reversal_convention,
						l_invoice.conv_rate, l_invoice.contract_currency_id, l_invoice.company_currency_id, l_msg);

					IF l_rtn = -1 THEN
						PKG_PP_LOG.P_WRITE_MESSAGE(l_msg);
						ROLLBACK;
						raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
					END IF;
				END IF;
			END CASE;
	    END LOOP;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;

		-----------------------------------------------------------------------
		--Add in the exception handling logic
		-----------------------------------------------------------------------
		exception
		when OTHERS THEN
		  IF SQLCODE BETWEEN -20999 AND -20000 THEN
			rollback;
			pkg_pp_log.p_write_message(sqlerrm);
			IF a_end_log = 1 THEN
			  pkg_pp_log.p_end_log;
			END IF;
			RAISE;
		  --PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
		  ELSIF SQLCODE = 100 THEN
			ROLLBACK;
			pkg_pp_log.p_write_message('Error processing Lessor Invoice JEs - ' || sqlerrm || f_get_call_stack);
			IF a_end_log = 1 THEN
			  pkg_pp_log.p_end_log;
			END IF;
			raise_application_error(-20000, substr('Error processing Lessor Invoice JEs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
		  ELSE
			ROLLBACK;
			pkg_pp_log.p_write_message('Error processing Lessor Invoice JEs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
			IF a_end_log = 1 THEN
			  pkg_pp_log.p_end_log;
			END IF;
			RAISE;
		  END IF;
	END p_invoice_approve;

	--**************************************************************************
	--                            f_auto_termination_calc
	--             --------------------------------
	-- @@ description
	--    this function will stage the monthly accrual numbers by ls_asset.
	--    it will load from ls_asset_schedule into ls_monthly_accrual_stg
	--  this function uses merge statements, so it can be run multiple times.
	-- @@params
	--    date: a_month
	--       the month to process auto_termination for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
 	--**************************************************************************
	--                            f_auto_termination_calc
	--             --------------------------------
	-- @@ description
	--    this function will stage the monthly accrual numbers by ls_asset.
	--    it will load from ls_asset_schedule into ls_monthly_accrual_stg
	--  this function uses merge statements, so it can be run multiple times.
	-- @@params
	--    date: a_month
	--       the month to process auto_termination for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_auto_termination_calc(
			a_company_id IN NUMBER,
			a_month      IN DATE,
            a_end_log    IN NUMBER:=NULL)
	IS
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Calculate Auto Termination'));
		pkg_pp_log.p_write_message('Starting p_auto_termination_calc');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		pkg_pp_log.p_write_message('Finding all In-Service ILRs eligible to Terminate for month ' || To_Char(a_month,'yyyymmdd') );

		for L_TERM_CALC in (
		    SELECT ILR.ILR_ID AS ILR_ID,
					ILR.CURRENT_REVISION AS ILR_REVISION,
					ILR.COMPANY_ID as COMPANY_ID,
					(CASE WHEN cntrl.exchange_lock_date > Last_Day(cntrl.gl_posting_mo_yr) THEN Last_Day(cntrl.gl_posting_mo_yr)
						ELSE cntrl.exchange_lock_date END)    AS termination_date,
					exchg_rate.rate AS exchange_lock_rate, 
					lfts.fasb_cap_type_id AS fasb_cap_type_id,
					nvl(sched.end_deferred_rent,0) + nvl(sched.end_accrued_rent,0) AS rent_balance,
					Nvl(dc1.idc_amount, 0)- dc2.unamortized_amount AS deferred_costs,
					(nvl(sched.end_deferred_rent,0) + nvl(sched.end_accrued_rent,0)) - (Nvl(dc1.idc_amount, 0) - dc2.unamortized_amount) - lio.termination_amt AS income_gainloss,
					lia.sell_profit_loss_account_id AS income_gainloss_acct_id,
					(case 
					   when sched.end_deferred_rent <> 0  then lia.deferred_rent_acct_id
					   when sched.end_accrued_rent <> 0  then lia.accrued_rent_acct_id
					end ) AS rent_balance_acct_id ,
					lia.def_costs_account_id AS deferred_costs_acct_id,
					lia.ar_account_id AS penalty_other_paymts_acct_id,
					sched.end_receivable AS current_ls_receivable,
					term_st.ending_unguaranteed_residual AS unguaranteed_residual,
					Nvl(term_df.end_deferred_profit, 0) AS deferred_profit,
					lio.purchase_option_amt AS purchase_option_amt,
					lio.termination_amt AS termination_amt,
					(lio.purchase_option_amt + lio.termination_amt) AS total_future_paymts,
					sched.end_receivable - (lio.purchase_option_amt + lio.termination_amt) AS net_ls_receivable,
					sched.end_receivable - (lio.purchase_option_amt + lio.termination_amt) + term_st.ending_unguaranteed_residual AS new_net_investment,
					totals.guarantee_resid AS total_guarantee_resid,
					totals.unguarantee_resid AS total_unguarantee_resid,
					totals.asset_num AS asset_count,
					sched.set_of_books_id,
					lio.lease_cap_type_id,
          nvl(term_df.begin_lt_deferred_profit,0) beg_lt_def_profit
		    FROM  lsr_ilr_schedule sched
		INNER JOIN  lsr_ilr  ilr
		  on ilr.ilr_id = sched.ilr_id
		  AND ilr.ilr_status_id = 2
		  AND ilr.current_revision = sched.revision
		  AND ilr.company_id =  a_company_id
		INNER JOIN lsr_ilr_options lio
		 ON sched.ilr_id = lio.ilr_id
		 AND sched.revision = lio.revision                  
		INNER JOIN lsr_ilr_approval approve
		  ON  approve.ilr_id = sched.ilr_id
		  AND approve.revision = sched.revision
		  AND approval_Status_id IN (3,6)
		INNER JOIN company_set_of_books csob ON csob.company_id = ilr.company_id AND csob.set_of_books_id = sched.set_of_books_id AND csob.include_indicator=1
		INNER JOIN (SELECT ilr_id, revision, set_of_books_id, Max(MONTH) AS last_month FROM lsr_ilr_schedule GROUP BY ilr_id, revision, set_of_books_id) max_month
		  ON max_month.ilr_id = sched.ilr_id
		  AND max_month.revision = sched.revision
		  AND max_month.set_of_books_id = sched.set_of_books_id
		  AND max_month.last_month = sched.month
		INNER JOIN lsr_lease lease
		  ON lease.lease_id = ilr.lease_id                     
		INNER JOIN lsr_fasb_type_sob LFTS
		  ON LFTS.cap_type_id = lio.lease_cap_type_id 
		  AND lfts.set_of_books_id = sched.set_of_books_id
		INNER JOIN lsr_ilr_account lia
		  ON lia.ilr_id = sched.ilr_id
		LEFT JOIN (select ilr_id, revision, nvl(sum(amount),0) as idc_amount from lsr_ilr_initial_direct_cost GROUP BY ilr_id, revision) dc1
		  ON sched.ilr_id = dc1.ilr_id
		  AND sched.revision = dc1.revision
		INNER JOIN (select ilr_id, revision, set_of_books_id,  nvl(sum(initial_direct_cost),0) as unamortized_amount from lsr_ilr_schedule WHERE MONTH <= a_month GROUP BY ilr_id, revision, set_of_books_id ) dc2
		  ON sched.ilr_id = dc2.ilr_id
		  AND sched.revision = dc2.revision
		  AND sched.set_of_books_id = dc2.set_of_books_id
		INNER JOIN lsr_process_control cntrl
		  ON cntrl.company_id = ilr.company_id
		  AND cntrl.lsr_closed IS NOT NULL
		  AND cntrl.open_next IS NULL
		  AND cntrl.exchange_lock_date IS NOT null
		  AND cntrl.gl_posting_mo_yr = a_month
		  AND cntrl.gl_posting_mo_yr = sched.month
		INNER JOIN ls_lease_calculated_date_rates exchg_rate
		  ON exchg_rate.company_id = a_company_id
		  AND exchg_rate.accounting_month = cntrl.gl_posting_mo_yr
		  AND exchg_rate.contract_currency_id = lease.contract_currency_id
		  AND exchg_rate.company_currency_id = (select currency_id from currency_schema where currency_type_id = 1 and company_id = cntrl.company_id)
		INNER JOIN pp_system_control_companies avg_rates
		  on avg_rates.company_id = ilr.company_id
		INNER JOIN (SELECT ilr_id, 
						   revision, 
						   Sum(guaranteed_residual_amount) AS guarantee_resid,
						   Sum( unguaranteed_residual_amount) AS  unguarantee_resid,
						   Count(*) AS asset_num     
					  FROM lsr_asset GROUP BY ilr_id, revision) totals
		  ON sched.ilr_id = totals.ilr_id
		  AND sched.revision = totals.revision
		LEFT JOIN   lsr_ilr_schedule_direct_fin term_df
			ON sched.ilr_id = term_df.ilr_id
			AND sched.revision = term_df.revision
			AND sched.set_of_books_id = term_df.set_of_books_id
			AND term_df.MONTH = sched.month
		LEFT JOIN   lsr_ilr_schedule_sales_direct term_st
			ON sched.ilr_id = term_st.ilr_id
			AND sched.revision = term_st.revision
			AND sched.set_of_books_id = term_st.set_of_books_id
			AND term_st.MONTH = sched.month    
		WHERE sched.month = a_month
		AND avg_rates.control_name = 'Lease MC: Use Average Rates'
		AND exchg_rate.exchange_rate_type_id = decode(lower(avg_rates.control_value), 'yes', 4, 1)
		)
		LOOP
		CASE 
        --for Operating ILRs Insert new auto terminated record
        WHEN L_TERM_CALC.FASB_CAP_TYPE_ID = 1 THEN
			pkg_pp_log.p_write_message('Inserting Operating ILR ' || L_TERM_CALC.ilr_id || ' into lsr_ilr_termination_op' );
			insert into lsr_ilr_termination_op (ilr_id, termination_date, rent_balance, deferred_costs, penalty_other_payment, income_gainloss, 
				rent_balance_acct_id, deferred_costs_acct_id, penalty_other_payment_acct_id, income_gainloss_acct_id, termination_source, comments, termination_exchange_rate,
				set_of_books_id)
			values (L_TERM_CALC.ilr_id, 
							   L_TERM_CALC.termination_date,
							   L_TERM_CALC.rent_balance,  
							   L_TERM_CALC.deferred_costs,
							   L_TERM_CALC.termination_amt,
							   L_TERM_CALC.income_gainloss,  
							   L_TERM_CALC.rent_balance_acct_id,
							   L_TERM_CALC.deferred_costs_acct_id,
							   L_TERM_CALC.penalty_other_paymts_acct_id,
							   L_TERM_CALC.income_gainloss_acct_id, 
							   'Auto Termination',
							   'Auto Termination Record generated from MEC Calculate process',
							   L_TERM_CALC.exchange_lock_rate,
							   L_TERM_CALC.set_of_books_id);
        --for Sales Type / Direct Finance insert new auto terminated records                                                                                                              	
		WHEN L_TERM_CALC.FASB_CAP_TYPE_ID in (2,3) THEN
			pkg_pp_log.p_write_message('Inserting Sales Type/Direct Finance ILR ' || L_TERM_CALC.ilr_id || ' into lsr_ilr_termination_st_df' );
			insert into lsr_ilr_termination_st_df (ilr_id, termination_date, current_lease_receivable, future_payment, net_lease_receivable, unguaranteed_residual, net_investment, 
				purchase_option_amount, termination_amount, other_future_payments, termination_source, comments, termination_exchange_rate, deferred_profit,
				set_of_books_id, begin_lt_deferred_profit)
			values (L_TERM_CALC.ilr_id, 
							  L_TERM_CALC.termination_date,
							  L_TERM_CALC.current_ls_receivable,
							  L_TERM_CALC.total_future_paymts,
							  L_TERM_CALC.net_ls_receivable,
							  L_TERM_CALC.unguaranteed_residual,
							  L_TERM_CALC.new_net_investment,
							  L_TERM_CALC.purchase_option_amt,
							  L_TERM_CALC.termination_amt,
							  0,
							  'Auto Termination',
							  'Auto Termination Record generated from MEC Calculate process',
							  L_TERM_CALC.exchange_lock_rate,
							  L_TERM_CALC.deferred_profit,
							  L_TERM_CALC.set_of_books_id,
                L_TERM_CALC.beg_lt_def_profit);
			--update the asset amounts here for ST/DF
			for L_ASSET in
				(select lsr_asset_id, guaranteed_residual_amount, unguaranteed_residual_amount from lsr_asset
					where ilr_id = L_TERM_CALC.ilr_id
					and revision = L_TERM_CALC.ilr_revision
					and L_TERM_CALC.set_of_books_id = (
					    select min(csob.set_of_books_id)
					    from company_set_of_books csob
					    inner join lsr_fasb_type_sob fts on fts.set_of_books_id = csob.set_of_books_id and fts.fasb_cap_type_id in (2,3)
					    where csob.company_id = a_company_id
					      and fts.cap_type_id = L_TERM_CALC.lease_cap_type_id
					      and csob.include_indicator = 1
					  )
				)
				loop
					pkg_pp_log.p_write_message('Updating Actual Residual amount on LSR_ASSET: ' || L_ASSET.lsr_asset_id);
						update lsr_asset
						set actual_residual_amount = ((L_ASSET.guaranteed_residual_amount/(decode(L_TERM_CALC.total_guarantee_resid, 0, L_TERM_CALC.asset_count, L_TERM_CALC.total_guarantee_resid)) * L_TERM_CALC.net_ls_receivable) + ((L_ASSET.unguaranteed_residual_amount/(decode(L_TERM_CALC.total_unguarantee_resid, 0, L_TERM_CALC.asset_count, L_TERM_CALC.total_unguarantee_resid))) * L_TERM_CALC.unguaranteed_residual)),
							actual_residual_amt_comp_curr = ((L_ASSET.guaranteed_residual_amount/(decode(L_TERM_CALC.total_guarantee_resid, 0, L_TERM_CALC.asset_count, L_TERM_CALC.total_guarantee_resid)) * L_TERM_CALC.net_ls_receivable) + ((L_ASSET.unguaranteed_residual_amount/(decode(L_TERM_CALC.total_unguarantee_resid, 0, L_TERM_CALC.asset_count, L_TERM_CALC.total_unguarantee_resid))) * L_TERM_CALC.unguaranteed_residual))* L_TERM_CALC.exchange_lock_rate
						where lsr_asset_id = L_ASSET.lsr_asset_id;
				end loop;
		 end case;

			--insert each ilr into the filter table
			insert into lsr_term_ilr_filter(ilr_id)
				  select L_TERM_CALC.ilr_id from dual
				  where not exists ( select 1 from lsr_term_ilr_filter where ilr_id = L_TERM_CALC.ilr_id ); 
		 end loop;

		pkg_pp_log.p_write_message('Updating ILR statuses to Pending Termination');
		--update status to pending termination
		update lsr_ilr
		  set ilr_status_id = 4
		  where ilr_id in (select ilr_id from lsr_term_ilr_filter);

		update lsr_ilr_approval
		  set approval_status_id = 8
		  where (ilr_id,revision) in (select ilr_id, current_revision from lsr_ilr A
										where A.ilr_id in (select ilr_id from lsr_term_ilr_filter));


			IF a_end_log = 1 THEN
				pkg_pp_log.p_write_message('Closing log');
				pkg_pp_log.p_end_log;
			END IF;

		exception
			when OTHERS THEN
			  IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
				  pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			  --PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			  ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor Auto Termination  JEs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
				  pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor Auto Termination JEs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			  ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor Auto Termination JEs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
				  pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			  END IF;

	END p_auto_termination_calc;

	PROCEDURE p_rerecognize_cpr_assets(a_month DATE)
	IS
		l_asset_rerecognition_tbl pkg_lessor_common.asset_rerecognition_table;
		l_numRows number;
	BEGIN

		SELECT 
			asset.cpr_asset_id AS ldg_asset_id, -99 /* this prevents Post from recalculating basis amounts */AS ldg_activity_id, arc.ldg_depr_group_id, arc.books_schema_id, arc.retirement_unit_id,
			arc.utility_account_id, arc.bus_segment_id, arc.func_class_id, arc.sub_account_id, arc.asset_location_id, arc.gl_account_id, ilr.company_id, To_Date(To_Char(term.termination_date, 'YYYYMM'), 'YYYYMM') AS gl_posting_mo_yr,
			arc.subledger_indicator, arc.activity_code, arc.work_order_number, arc.posting_quantity, SYS_CONTEXT ('USERENV', 'SESSION_USER') AS user_id1,
			Round(asset.actual_residual_amount * term.termination_exchange_rate, 2) as posting_amount, To_Date(To_Char(SYSDATE, 'YYYYMM'), 'YYYYMM') AS in_service_year, 'Asset Rerecognition' AS description,
			decode(term.termination_source, 'Early Termination', term.comments, 'Auto Terminated Lease - End of Lessor Contract') as long_description,
			arc.property_group_id, arc.retire_method_id, 1 AS posting_status, 0 AS cost_of_removal, 0 AS salvage_cash, 0 AS salvage_returns, 0 AS reserve,
			'Lessor ILR:' || ilr.ilr_id || '; REVISION:' || ilr.current_revision AS misc_description, arc.ferc_activity_code, arc.serial_number, 0 AS reserve_credits, 0 AS gain_loss_reversal, arc.disposition_code,
			act.asset_activity_id, arc.posting_amount AS orig_posting_amount, term.set_of_books_id
		BULK COLLECT INTO l_asset_rerecognition_tbl
		FROM lsr_ilr_termination_st_df term
		INNER JOIN lsr_term_ilr_filter filter
			ON filter.ilr_id = term.ilr_id
		INNER JOIN lsr_ilr ilr
		 	ON term.ilr_id = ilr.ilr_id
		INNER JOIN lsr_asset asset
			ON ilr.ilr_id = asset.ilr_id
			AND ilr.current_revision = asset.revision
		INNER JOIN cpr_ledger cpr
			ON asset.cpr_asset_id = cpr.asset_id
		INNER JOIN cpr_activity act
			ON asset.cpr_asset_id = act.asset_id
		INNER JOIN pend_transaction_archive arc
			ON act.activity_status = arc.pend_trans_id
		WHERE To_Char(term.termination_date, 'YYYYMM') = To_Char(a_month, 'YYYYMM')
		AND act.asset_activity_id = (
			SELECT Max(asset_activity_id)
			FROM cpr_activity act2
			WHERE act.asset_id = act2.asset_id
			AND act.description = 'Asset Derecognition' )
		and term.set_of_books_id = (
			select min(set_of_books_id)
			from lsr_ilr_termination_st_df
			where ilr_id = term.ilr_id
		);
		
	l_numRows := SQL%ROWCOUNT;

	if l_numRows > 0 then
		pkg_lessor_common.p_rerecognize_cpr_assets(l_asset_rerecognition_tbl);
	end if;

	EXCEPTION
		WHEN OTHERS THEN
		IF SQLCODE BETWEEN -20999 AND -20000 THEN
			ROLLBACK;
			pkg_pp_log.p_write_message(SQLERRM);
			RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
		ELSIF SQLCODE = 100 THEN
			ROLLBACK;
			pkg_pp_log.p_write_message('Error creating Lessor Termination Asset Re-recognition pending transactions - ' || SQLERRM || f_get_call_stack);
			Raise_Application_Error(-20000, SubStr('Error creating Lessor Termination Asset Re-recognition pending transactions - ORA-01403 - NO DATA FOUND' || Chr(10) || f_get_call_stack, 1, 2000));
		ELSE
			ROLLBACK;
			pkg_pp_log.p_write_message('Error creating Lessor Termination Asset Re-recognition pending transactions - ORA' || SQLCODE || ' - ' || SQLERRM || Chr(10) || f_get_call_stack);
			RAISE;
		END IF;
	END p_rerecognize_cpr_assets;

	--**************************************************************************
	--                            f_auto_termination_approve
	--             --------------------------------
	-- @@ description
	--    this function will approve and post the monthly accrual numbers by ls_asset.
	--    it will load from ls_asset_schedule into
	-- @@params
	--    date: a_month
	--       the month to process auto_termination for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_auto_termination_approve(
          a_month       IN DATE,
          a_end_log     IN NUMBER:=NULL )
  IS
    L_MSG VARCHAR2(2000);
    L_GL_JE_CODE VARCHAR2(35);
    L_RTN NUMBER;
  BEGIN
		pkg_pp_log.p_write_message('Starting p_auto_termination_approve');
    pkg_pp_log.p_write_message('Retrieving L_GL_JE_CODE');
    select gl_je_code
    into L_GL_JE_CODE
    from standard_journal_entries, gl_je_control
    where standard_journal_entries.je_id = gl_je_control.je_id
    and upper(ltrim(rtrim(process_id))) = 'LESSOR TERMINATION';

    IF L_GL_JE_CODE IS NULL THEN
       L_MSG := 'Error Retrieving L_GL_JE_CODE';
       PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
       RETURN ;
    END IF;

    --grab the cursor for the termination amounts we need to book
    FOR L_TERM IN
      (
      SELECT DISTINCT JMTT.JE_METHOD_ID as JE_METHOD_ID,
           ILR.ILR_ID AS ILR_ID,
           ILR.COMPANY_ID as COMPANY_ID,
           SCH.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
           coalesce(TERM_OP.TERMINATION_EXCHANGE_RATE, TERM_ST.TERMINATION_EXCHANGE_RATE, 0) AS CONV_RATE,
           nvl(TERM_OP.DEFERRED_COSTS,0) AS DEFERRED_COSTS,
           nvl(TERM_OP.RENT_BALANCE,0) AS RENT_BALANCE,
           SCH.END_DEFERRED_RENT AS DEFERRED_RENT,
           SCH.END_ACCRUED_RENT AS ACCRUED_RENT,
           nvl(TERM_OP.PENALTY_OTHER_PAYMENT,0) AS TERMINATION_PENALTY,
           nvl(TERM_ST.CURRENT_LEASE_RECEIVABLE,0) AS CURRENT_LEASE_RECEIVABLE,
           nvl(TERM_ST.FUTURE_PAYMENT,0) AS FUTURE_PAYMENT,
           nvl(TERM_ST.UNGUARANTEED_RESIDUAL,0) AS UNGUARANTEED_RESIDUAL,
           nvl(TERM_ST.DEFERRED_PROFIT,0) AS DEFERRED_PROFIT,
           LEASE.CONTRACT_CURRENCY_ID AS CONTRACT_CURRENCY_ID,
           CS.CURRENCY_ID AS COMPANY_CURRENCY_ID,
           JM.AMOUNT_TYPE AS AMOUNT_TYPE,
           JMSOB.REVERSAL_CONVENTION AS REVERSAL_CONVENTION,
           LFTS.FASB_CAP_TYPE_ID AS FASB_CAP_TYPE_ID,
           LIA.DEF_COSTS_ACCOUNT_ID AS DEF_COSTS_ACCOUNT_ID,
           nvl(TERM_OP.RENT_BALANCE_ACCT_ID,0) AS RENT_BALANCE_ACCT_ID,
           nvl(TERM_OP.PENALTY_OTHER_PAYMENT_ACCT_ID,0) AS TERMINATION_PENALTY_ACCT_ID,
           nvl(TERM_OP.INCOME_GAINLOSS_ACCT_ID,0) AS INCOME_GAINLOSS_ACCT_ID,
           LIA.PROP_PLANT_ACCOUNT_ID AS PROP_PLANT_ACCOUNT_ID,
           LIA.ST_RECEIVABLE_ACCOUNT_ID AS ST_RECEIVABLE_ACCOUNT_ID,
           LIA.UNGUARAN_RES_ACCOUNT_ID AS UNGUARAN_RES_ACCOUNT_ID,
           LIA.AR_ACCOUNT_ID AS AR_ACCOUNT_ID,
           LIA.DEF_SELLING_PROFIT_ACCOUNT_ID AS DEF_SELLING_PROFIT_ACCOUNT_ID,
           LIA.SELL_PROFIT_LOSS_ACCOUNT_ID AS SELL_PROFIT_LOSS_ACCOUNT_ID,
           LIA.LT_DEF_SELL_PROFIT_ACCOUNT_ID AS LT_DEF_SELL_PROFIT_ACCOUNT_ID,
           nvl(TERM_ST.BEGIN_LT_DEFERRED_PROFIT,0) AS BEG_LT_DEF_PROFIT
      FROM LSR_ILR ILR
      INNER JOIN LSR_ILR_SCHEDULE SCH
            ON SCH.ILR_ID = ILR.ILR_ID
            AND SCH.REVISION = ILR.CURRENT_REVISION
      LEFT OUTER JOIN LSR_ILR_TERMINATION_OP TERM_OP
        ON ILR.ILR_ID = TERM_OP.ILR_ID
       AND SCH.SET_OF_BOOKS_ID = TERM_OP.SET_OF_BOOKS_ID
      LEFT OUTER JOIN LSR_ILR_TERMINATION_ST_DF TERM_ST
        ON ILR.ILR_ID = TERM_ST.ILR_ID
       AND SCH.SET_OF_BOOKS_ID = TERM_ST.SET_OF_BOOKS_ID 
      inner join lsr_term_ilr_filter filter
        on ilr.ilr_id = filter.ilr_id
      INNER JOIN LSR_LEASE LEASE
          ON LEASE.LEASE_ID = ILR.LEASE_ID
      INNER JOIN LSR_ILR_OPTIONS LIO
          ON LIO.ILR_ID = ILR.ILR_ID AND LIO.REVISION = ILR.CURRENT_REVISION
      INNER JOIN LSR_ILR_ACCOUNT LIA
          ON LIA.ILR_ID = ILR.ILR_ID
      INNER JOIN LSR_CAP_TYPE LCT
          ON LCT.CAP_TYPE_ID = LIO.LEASE_CAP_TYPE_ID
      INNER JOIN LSR_FASB_TYPE_SOB LFTS
        ON LFTS.CAP_TYPE_ID = LCT.CAP_TYPE_ID AND LFTS.SET_OF_BOOKS_ID = SCH.SET_OF_BOOKS_ID
      INNER JOIN JE_METHOD_SET_OF_BOOKS JMSOB
        ON JMSOB.SET_OF_BOOKS_ID = LFTS.SET_OF_BOOKS_ID
      INNER JOIN JE_METHOD_TRANS_TYPE JMTT
        ON JMTT.JE_METHOD_ID = JMSOB.JE_METHOD_ID
      INNER JOIN JE_METHOD JM
        ON JM.JE_METHOD_ID = JMSOB.JE_METHOD_ID
      INNER JOIN COMPANY_JE_METHOD_VIEW CJMV
        ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JMTT.JE_METHOD_ID
      INNER JOIN CURRENCY_SCHEMA CS
        ON CS.COMPANY_ID = ILR.COMPANY_ID
      WHERE JMTT.TRANS_TYPE IN
          ( SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE
             WHERE TRANS_TYPE BETWEEN 4060 AND 4067
                OR TRANS_TYPE BETWEEN 4040 AND 4047
                OR TRANS_TYPE in (4080, 4081)
          )
          AND SCH.MONTH = A_MONTH
      ORDER BY JE_METHOD_ID, ILR_ID, SET_OF_BOOKS_ID
      )
      LOOP
      CASE
        --for Operating ILRs
        WHEN L_TERM.FASB_CAP_TYPE_ID = 1 THEN
           IF NVL(L_TERM.DEFERRED_COSTS, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Deferred Initial Costs JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4040, L_TERM.DEFERRED_COSTS,
            L_TERM.INCOME_GAINLOSS_ACCT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;

             L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4041, L_TERM.DEFERRED_COSTS,
            L_TERM.DEF_COSTS_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;
           END IF;

           IF NVL(L_TERM.DEFERRED_RENT, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Deferred Rent JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4042, L_TERM.RENT_BALANCE,
            L_TERM.RENT_BALANCE_ACCT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;

             L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4043, L_TERM.RENT_BALANCE,
            L_TERM.INCOME_GAINLOSS_ACCT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;
           END IF;

           IF NVL(L_TERM.ACCRUED_RENT, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Accrued JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4044, L_TERM.RENT_BALANCE,
            L_TERM.INCOME_GAINLOSS_ACCT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;

             L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4045, L_TERM.RENT_BALANCE,
            L_TERM.RENT_BALANCE_ACCT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;
           END IF;

           IF NVL(L_TERM.TERMINATION_PENALTY, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Termination Penalty JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4046, L_TERM.TERMINATION_PENALTY,
            L_TERM.TERMINATION_PENALTY_ACCT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;

             L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4047, L_TERM.TERMINATION_PENALTY,
            L_TERM.INCOME_GAINLOSS_ACCT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
			  raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
             END IF;
           END IF;

        --for Sales-Type/Direct Finance ILRs
        WHEN L_TERM.FASB_CAP_TYPE_ID IN (2,3) THEN
          IF NVL(L_TERM.CURRENT_LEASE_RECEIVABLE, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Receivable Termination JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4060, L_TERM.CURRENT_LEASE_RECEIVABLE,
            L_TERM.PROP_PLANT_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

            IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4061, L_TERM.CURRENT_LEASE_RECEIVABLE,
            L_TERM.ST_RECEIVABLE_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;
          END IF;

          IF NVL(L_TERM.UNGUARANTEED_RESIDUAL, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Unguaranteed Residual JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4062, L_TERM.UNGUARANTEED_RESIDUAL,
            L_TERM.PROP_PLANT_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

            IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4063, L_TERM.UNGUARANTEED_RESIDUAL,
            L_TERM.UNGUARAN_RES_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;
          END IF;

          IF NVL(L_TERM.FUTURE_PAYMENT, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Future Payment JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4064, L_TERM.FUTURE_PAYMENT,
            L_TERM.AR_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

            IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4065, L_TERM.FUTURE_PAYMENT,
            L_TERM.PROP_PLANT_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;
          END IF;

          IF NVL(L_TERM.DEFERRED_PROFIT, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending Deferred Profit JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4066, L_TERM.DEFERRED_PROFIT,
            L_TERM.DEF_SELLING_PROFIT_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

            IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4067, L_TERM.DEFERRED_PROFIT,
            L_TERM.SELL_PROFIT_LOSS_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
            0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
            L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;
          END IF;
            
          -- LT DEFERRED PROFIT
          IF NVL(L_TERM.BEG_LT_DEF_PROFIT, 0.0) <> 0.0 THEN
            pkg_pp_log.p_write_message('Sending LT Deferred Profit Termination JEs for ILR ' || L_TERM.ILR_ID || ' Month ' || A_MONTH || ' SOB ' || L_TERM.SET_OF_BOOKS_ID);

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4080, L_TERM.BEG_LT_DEF_PROFIT,
                                                   L_TERM.LT_DEF_SELL_PROFIT_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
                                                   1, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, 
                                                   L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
                                                   L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, 
                                                   L_TERM.COMPANY_CURRENCY_ID, L_MSG);

            IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;

            L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_TERM.ILR_ID, 4081, L_TERM.BEG_LT_DEF_PROFIT,
                                                   L_TERM.DEF_SELLING_PROFIT_ACCOUNT_ID, 0, L_TERM.COMPANY_ID, A_MONTH,
                                                   0, L_GL_JE_CODE, L_TERM.SET_OF_BOOKS_ID, L_TERM.JE_METHOD_ID, 
                                                   L_TERM.AMOUNT_TYPE, L_TERM.REVERSAL_CONVENTION,
                                                   L_TERM.CONV_RATE, L_TERM.CONTRACT_CURRENCY_ID, 
                                                   L_TERM.COMPANY_CURRENCY_ID, L_MSG);

             IF L_RTN = -1 then
              PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
              ROLLBACK;
              raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
            END IF;
          END IF;
        ELSE
          raise_application_error(-20000, substr('Error processing Lessor Termination JEs - No valid cap type case for JE processing found' || chr(10) || f_get_call_stack, 1, 2000));
        END CASE;
      END LOOP;

	  p_rerecognize_cpr_assets(a_month);

      --Update the ILR status and revision status to 'Terminated'
      update lsr_ilr
      set ilr_status_id = 5
      where ilr_id in (select ilr_id from lsr_term_ilr_filter);

      update lsr_ilr_approval
      set approval_status_id = 9
      where (ilr_id,revision) in (select ilr_id, current_revision from lsr_ilr A
                                    where A.ilr_id in (select ilr_id from lsr_term_ilr_filter));


    IF a_end_log = 1 THEN
      pkg_pp_log.p_write_message('Closing log');
      pkg_pp_log.p_end_log;
    END IF;

    exception
    when OTHERS THEN
      IF SQLCODE BETWEEN -20999 AND -20000 THEN
      rollback;
      pkg_pp_log.p_write_message(sqlerrm);
      IF a_end_log = 1 THEN
        pkg_pp_log.p_end_log;
      END IF;
      RAISE;
      --PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
      ELSIF SQLCODE = 100 THEN
      ROLLBACK;
      pkg_pp_log.p_write_message('Error processing Lessor Termination JEs - ' || sqlerrm || f_get_call_stack);
      IF a_end_log = 1 THEN
        pkg_pp_log.p_end_log;
      END IF;
      raise_application_error(-20000, substr('Error processing Lessor Termination JEs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
      ELSE
      ROLLBACK;
      pkg_pp_log.p_write_message('Error processing Lessor Termination JEs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
      IF a_end_log = 1 THEN
        pkg_pp_log.p_end_log;
      END IF;
      RAISE;
      END IF;
  END p_auto_termination_approve;

  -- This procedure is called from Termination Review workspace for 1 or more ILRs
  PROCEDURE p_auto_termination_approve(
          a_month       DATE,
          a_ilrs        NUM_ARRAY
          )
  IS
  l_end_log number := 1;
  BEGIN
    pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Approve Auto Termination'));
    pkg_pp_log.p_write_message('Starting p_auto_termination_approve for selected ILRs');
    for i in A_ILRS.first .. A_ILRS.last loop
          insert into lsr_term_ilr_filter(ilr_id)
          select ilr_id from lsr_ilr
          where ilr_id = a_ilrs(i)
            and not exists ( select 1 from lsr_term_ilr_filter where ilr_id = a_ilrs(i) );
    end loop;

    p_auto_termination_approve(a_month, l_end_log);

  END p_auto_termination_approve;

  -- This procedure is called from Month End workspace for a company
  PROCEDURE p_auto_termination_approve(
          a_company_id  NUMBER,
          a_month       DATE,
          a_end_log     NUMBER:=NULL )
  IS
    l_end_log number := 1;
  BEGIN
    pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Approve Auto Termination'));
    pkg_pp_log.p_write_message('Starting p_auto_termination_approve for Month End');
    pkg_pp_log.p_write_message('  a_company_id: ' || to_char(a_company_id));
    pkg_pp_log.p_write_message('  a_month: ' || To_Char(a_month,'yyyymmdd'));
    pkg_pp_log.p_write_message('  a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));
    pkg_pp_log.p_write_message('  Processing JEs for lessor terminations');

    insert into lsr_term_ilr_filter(ilr_id)
    select ilr_id
	from lsr_ilr
    where company_id = a_company_id
	and ilr_status_id = 4
	AND ilr_id IN (
		SELECT ilr_id
		FROM lsr_ilr_termination_st_df
		WHERE To_Char(termination_date, 'YYYYMM') = To_Char(a_month, 'YYYYMM')
		UNION
		SELECT ilr_id
		FROM lsr_ilr_termination_op
		WHERE To_Char(termination_date, 'YYYYMM') = To_Char(a_month, 'YYYYMM')
	);

	pkg_pp_log.p_write_message('Inserted ' || SQL%ROWCOUNT || ' rows into lsr_term_ilr_filter.');

    p_auto_termination_approve( a_month, l_end_log);

   END p_auto_termination_approve;

	--**************************************************************************
	--                            f_currency_gain_loss
	--             --------------------------------
	-- @@ description
	--    this function will return the currency gain/loss for the given companies, month, and set of books.
	--    this is used to generate journal entries associated with the gain/loss.
	-- @@params
	--    t_num_array: a_company_ids
	--      the companies for which to return currency gain/loss
	--    date: a_month
	--       the month for which to return currency gain/loss
	-- @@return
	--    currency_gain_loss_tbl pipelined:
	--       a pipelined pl/sql table containing the results of the calculation
	--**************************************************************************
	FUNCTION f_currency_gain_loss(
			a_company_ids     IN t_num_array,
			a_month           IN DATE,
			a_set_of_books_id IN NUMBER)
		RETURN currency_gain_loss_tbl
		pipelined
	IS
	BEGIN
		FOR item IN (
			WITH term_records AS (SELECT ILR_ID, Termination_source, termination_Date FROM LSR_ILR_TERMINATION_ST_DF
								  UNION
								  SELECT ILR_ID, Termination_source, termination_Date FROM LSR_ILR_TERMINATION_OP
								  )	
			SELECT sch.company_id,
				sch.ilr_id,
				sch.ilr_number,
				sch.iso_code,
				sch.currency_display_symbol,
				sch.ST_CURRENCY_GAIN_LOSS + sch.LT_CURRENCY_GAIN_LOSS gain_loss_fx,
				sch.month,
				acct.curr_gain_loss_acct_id,
				acct.curr_gain_loss_offset_acct_id,
				sch.ST_CURRENCY_GAIN_LOSS,
				sch.LT_CURRENCY_GAIN_LOSS,
        acct.st_receivable_account_id,
        acct.lt_receivable_account_id
			FROM v_lsr_ilr_mc_schedule sch
			INNER JOIN ls_lease_currency_type cur_type
				ON sch.ls_cur_type = cur_type.ls_currency_type_id
			INNER JOIN lsr_ilr ilr
				ON ilr.ilr_id = sch.ilr_id
				AND ilr.current_revision = sch.revision
			INNER JOIN lsr_ilr_account acct
				ON acct.ilr_id = ilr.ilr_id
			WHERE sch.company_id MEMBER OF A_COMPANY_IDS
			AND sch.MONTH = A_MONTH
			AND sch.set_of_books_id = A_SET_OF_BOOKS_ID
			AND Lower(cur_type.description) = 'company'
			AND (sch.ST_CURRENCY_GAIN_LOSS + sch.LT_CURRENCY_GAIN_LOSS) <> 0
			AND ilr.ilr_status_id IN (2,3,4,5) -- 2 = in-service; 3 = retired 4 = pending termination, 5 = terminated
			AND NOT EXISTS(
							SELECT ILR_ID FROM term_records 
									      WHERE Trunc(TERMINATION_DATE, 'MM') <= A_MONTH
									      AND Lower(termination_source) = 'early termination'
										  AND ilr.ilr_id = term_records.ilr_id
							UNION
							SELECT ilr_id FROM term_records 
										  WHERE Lower(termination_source) = 'auto termination'
										  AND termination_date > LAST_DAY(A_MONTH)
										  AND ilr.ilr_id = term_records.ilr_id)) 
		LOOP
			PIPE ROW(item);
		END LOOP;
	END f_currency_gain_loss;

	--**************************************************************************
	--                            f_currency_gain_loss_approve
	--             --------------------------------
	-- @@ description
	--    this function will approve and post the monthly gain/loss entries resulting from
	--         fluctuations in currency exchange rates.
	-- @@params
	--    date: a_month
	--       the month to process currency gain/loss for
	-- @@return
	--    varchar2: a message back to the caller
	--       'ok' = success
	--       all else = failure
	--
	--**************************************************************************
	PROCEDURE p_currency_gain_loss_approve(
			A_COMPANY_ID      in number,
			A_MONTH           in date,
			A_END_LOG         in number := null)
	IS
    L_MSG VARCHAR2(2000);
    L_GL_JE_CODE VARCHAR2(35);
    L_RTN NUMBER;
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lessor - Approve Currency Gain/Loss'));
		pkg_pp_log.p_write_message('Starting Lessor currency gain loss approve');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

		----------------------------------------------------------
		--Validate inputs
		----------------------------------------------------------
		IF a_company_id IS NULL THEN
			Raise_Application_Error(-20000, 'NULL company ID passed to pkg_lease_calc.p_currency_gain_loss_approve.');
		END IF;

		IF a_month IS NULL THEN
			Raise_Application_Error(-20000, 'NULL month passed to pkg_lease_calc.p_currency_gain_loss_approve.');
		END IF;

		L_MSG:='Retrieving L_GL_JE_CODE';
		select gl_je_code
		into L_GL_JE_CODE
		from standard_journal_entries, gl_je_control
		where standard_journal_entries.je_id = gl_je_control.je_id
		and lower(ltrim(rtrim(process_id))) = 'lessor curr g/l';

		IF L_GL_JE_CODE IS NULL THEN
			L_MSG := 'Error Retrieving L_GL_JE_CODE';
			PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
			RETURN ;
		END IF;

		-----------------------------------------------------------------
		--Loop over schedule lines for each je method and set of books id
		-----------------------------------------------------------------

		FOR L_SCH IN
			( 	
			WITH term_records AS (SELECT ILR_ID, Termination_source, termination_Date FROM LSR_ILR_TERMINATION_ST_DF
								  UNION
								  SELECT ILR_ID, Termination_source, termination_Date FROM LSR_ILR_TERMINATION_OP
								  )		
			SELECT DISTINCT JMTT.JE_METHOD_ID as JE_METHOD_ID,
               VLIMS.ILR_ID AS ILR_ID,
               VLIMS.ILR_NUMBER AS ILR_NUMBER,
               VLIMS.ISO_CODE AS ISO_CODE,
               VLIMS.CURRENCY_DISPLAY_SYMBOL AS CURRENCY_DISPLAY_SYMBOL,
               VLIMS.SET_OF_BOOKS_ID AS SET_OF_BOOKS_ID,
               VLIMS.ST_CURRENCY_GAIN_LOSS + VLIMS.LT_CURRENCY_GAIN_LOSS AS GAIN_LOSS_FX,
               VLIMS.ST_CURRENCY_GAIN_LOSS,
               VLIMS.LT_CURRENCY_GAIN_LOSS,
               VLIMS.MONTH AS MONTH,
               VLIMS.CONTRACT_CURRENCY_ID AS CONTRACT_CURRENCY_ID,
               CS.CURRENCY_ID AS COMPANY_CURRENCY_ID,
               JM.AMOUNT_TYPE AS AMOUNT_TYPE,
               JMSOB.REVERSAL_CONVENTION as REVERSAL_CONVENTION,
               LFTS.FASB_CAP_TYPE_ID as FASB_CAP_TYPE_ID,
               LIA.CURR_GAIN_LOSS_ACCT_ID,
               LIA.CURR_GAIN_LOSS_OFFSET_ACCT_ID,
               LIA.ST_RECEIVABLE_ACCOUNT_ID,
               LIA.LT_RECEIVABLE_ACCOUNT_ID
			FROM V_LSR_ILR_MC_SCHEDULE VLIMS
			INNER JOIN LSR_ILR ILR
              ON ILR.ILR_ID = VLIMS.ILR_ID
			INNER JOIN LSR_ILR_OPTIONS LIO
              ON LIO.ILR_ID = VLIMS.ILR_ID AND LIO.REVISION = VLIMS.REVISION AND ILR.CURRENT_REVISION = VLIMS.REVISION
			INNER JOIN LSR_ILR_ACCOUNT LIA
              ON LIA.ILR_ID = VLIMS.ILR_ID
			INNER JOIN LSR_CAP_TYPE LCT
              ON LCT.CAP_TYPE_ID = LIO.LEASE_CAP_TYPE_ID
			INNER JOIN LSR_FASB_TYPE_SOB LFTS
              ON LFTS.CAP_TYPE_ID = LCT.CAP_TYPE_ID AND LFTS.SET_OF_BOOKS_ID = VLIMS.SET_OF_BOOKS_ID
			INNER JOIN LSR_FASB_CAP_TYPE LFCT
              ON LFCT.FASB_CAP_TYPE_ID = LFTS.FASB_CAP_TYPE_ID
			INNER JOIN JE_METHOD_SET_OF_BOOKS JMSOB
              ON JMSOB.SET_OF_BOOKS_ID = LFTS.SET_OF_BOOKS_ID
			INNER JOIN JE_METHOD_TRANS_TYPE JMTT
              ON JMTT.JE_METHOD_ID = JMSOB.JE_METHOD_ID
			INNER JOIN JE_METHOD JM
              ON JM.JE_METHOD_ID = JMSOB.JE_METHOD_ID
			INNER JOIN COMPANY_JE_METHOD_VIEW CJMV
              ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JMTT.JE_METHOD_ID
			INNER JOIN ls_lease_currency_type LLCT
			  ON VLIMS.LS_CUR_TYPE = LLCT.LS_CURRENCY_TYPE_ID
			INNER JOIN CURRENCY_SCHEMA CS
              ON CS.COMPANY_ID = ILR.COMPANY_ID
			WHERE JMTT.TRANS_TYPE IN 
              ( SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE 
                WHERE TRANS_TYPE IN (4051,4052,4068) 
			)
              AND ILR.COMPANY_ID = A_COMPANY_ID
              AND VLIMS.MONTH = A_MONTH
              AND (VLIMS.ST_CURRENCY_GAIN_LOSS + VLIMS.LT_CURRENCY_GAIN_LOSS) <> 0.0
              AND Lower(LLCT.description) = 'company'
              AND LOWER(LFCT.DESCRIPTION) IN ('sales type', 'direct finance')
			  AND ilr.ilr_status_id IN (2,3,4,5) -- 2 = in-service; 3 = retired 4 = pending termination, 5 = terminated
			  AND NOT EXISTS(
							SELECT ILR_ID FROM term_records 
									      WHERE Trunc(TERMINATION_DATE, 'MM') <= A_MONTH
									      AND Lower(termination_source) = 'early termination'
										  AND ilr.ilr_id = term_records.ilr_id
							UNION
							SELECT ilr_id FROM term_records 
										  WHERE Lower(termination_source) = 'auto termination'
										  AND termination_date > LAST_DAY(A_MONTH)
										  AND ilr.ilr_id = term_records.ilr_id)  
			ORDER BY JE_METHOD_ID, ILR_ID, SET_OF_BOOKS_ID
			)	
		LOOP
			if l_sch.lt_currency_gain_loss <> 0.0 then
				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_SCH.ILR_ID, 4051, L_SCH.LT_CURRENCY_GAIN_LOSS, 
					L_SCH.CURR_GAIN_LOSS_ACCT_ID, 1, A_COMPANY_ID, A_MONTH,
					1, L_GL_JE_CODE, L_SCH.SET_OF_BOOKS_ID, L_SCH.JE_METHOD_ID, L_SCH.AMOUNT_TYPE, L_SCH.REVERSAL_CONVENTION,
					1, L_SCH.CONTRACT_CURRENCY_ID, L_SCH.COMPANY_CURRENCY_ID, L_MSG);

                IF L_RTN = -1 then
                    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                    ROLLBACK;
					raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
                END IF;

				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_SCH.ILR_ID, 4052, L_SCH.LT_CURRENCY_GAIN_LOSS,
					L_SCH.LT_RECEIVABLE_ACCOUNT_ID, 1, A_COMPANY_ID, A_MONTH,
					0, L_GL_JE_CODE, L_SCH.SET_OF_BOOKS_ID, L_SCH.JE_METHOD_ID, L_SCH.AMOUNT_TYPE, L_SCH.REVERSAL_CONVENTION,
					1, L_SCH.CONTRACT_CURRENCY_ID, L_SCH.COMPANY_CURRENCY_ID, L_MSG);

                IF L_RTN = -1 then
                    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                    ROLLBACK;
					raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
                END IF;
			end if;

			if l_sch.st_currency_gain_loss <> 0.0 then
				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_SCH.ILR_ID, 4068, L_SCH.ST_CURRENCY_GAIN_LOSS, 
					L_SCH.CURR_GAIN_LOSS_ACCT_ID, 1, A_COMPANY_ID, A_MONTH,
					1, L_GL_JE_CODE, L_SCH.SET_OF_BOOKS_ID, L_SCH.JE_METHOD_ID, L_SCH.AMOUNT_TYPE, L_SCH.REVERSAL_CONVENTION, 
					1, L_SCH.CONTRACT_CURRENCY_ID, L_SCH.COMPANY_CURRENCY_ID, L_MSG);

                IF L_RTN = -1 then
                    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                    ROLLBACK;
					raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
                END IF;

				L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(L_SCH.ILR_ID, 4069, L_SCH.ST_CURRENCY_GAIN_LOSS,
					L_SCH.ST_RECEIVABLE_ACCOUNT_ID, 1, A_COMPANY_ID, A_MONTH,
					0, L_GL_JE_CODE, L_SCH.SET_OF_BOOKS_ID, L_SCH.JE_METHOD_ID, L_SCH.AMOUNT_TYPE, L_SCH.REVERSAL_CONVENTION,
					1, L_SCH.CONTRACT_CURRENCY_ID, L_SCH.COMPANY_CURRENCY_ID, L_MSG);

                IF L_RTN = -1 then
                    PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
                    ROLLBACK;
					raise_application_error(-20000,'Failure in PKG_LESSOR_COMMON.F_MC_BOOKJE');
                END IF;
			end if;
		END LOOP;

		IF a_end_log = 1 THEN
			pkg_pp_log.p_write_message('Closing log');
			pkg_pp_log.p_end_log;
		END IF;
	EXCEPTION
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_currency_gain_loss_approve;

	--**************************************************************************
	--                            p_lsr_closed
	--             --------------------------------
	-- @@ description
	--    This procedure will lock the Lease module (from the Lessor-side) which effectively doesn't mean anything here,
	--		but we should specifically call out that this is the "Lock Lease and Open Next Month" process in the Lease MEC workspace.
	--		Additionally this procedure calculates and sends the Short-/Long-Term Receivable Reclass JEs.
	-- @@params
	--    number: a_company_id
	--      the company for which we are locking the month and sending reclass JEs
	--    date: a_month
	--       the month for which we are sending reclass JEs
	--    number: a_end_log
	--		 a flag to determine whether or not we end the logging process in this procedure
	--**************************************************************************
	PROCEDURE p_lsr_closed(
			a_company_id IN NUMBER,
			a_month      IN DATE,
			a_end_log IN NUMBER:=NULL)
	IS
		l_count		INTEGER;
	BEGIN
		pkg_pp_log.p_start_log(PKG_LESSOR_COMMON.F_GET_PROCESS_ID('Lease - Lock'));
		pkg_pp_log.p_write_message('Starting p_lsr_closed');
		pkg_pp_log.p_write_message('	a_company_id: ' || a_company_id);
		pkg_pp_log.p_write_message('	a_month: ' || To_Char(a_month,'yyyymmdd'));
		pkg_pp_log.p_write_message('	a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));


	exception
		when OTHERS THEN
			IF SQLCODE BETWEEN -20999 AND -20000 THEN
				rollback;
				pkg_pp_log.p_write_message(sqlerrm);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			--PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
			ELSIF SQLCODE = 100 THEN
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
			ELSE
				ROLLBACK;
				pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
				IF a_end_log = 1 THEN
					pkg_pp_log.p_end_log;
				END IF;
				RAISE;
			END IF;
	END p_lsr_closed;

	--**************************************************************************
  --                            p_lessor_reclass_jes
  --                      --------------------------------
  -- @@ description
  --    This procedure will calculate and send the Short-/Long-Term Receivable Reclass JEs.
  -- @@params
  --    number: a_company_id
  --      the company for which we are locking the month and sending reclass JEs
  --    date: a_month
  --       the month for which we are sending reclass JEs
  --    number: a_end_log
  --     a flag to determine whether or not we end the logging process in this procedure
  --**************************************************************************
  PROCEDURE p_lessor_reclass_jes(
      a_company_id IN NUMBER,
      a_month      IN DATE,
      a_end_log IN NUMBER:=NULL)
  IS
    L_MSG varchar2(2000);
	L_RTN number;
	L_GL_JE_CODE   varchar2(35);
	L_AMOUNT         NUMBER(22,2);
  BEGIN
    pkg_pp_log.p_write_message('Starting Lessor Reclass JEs');
    pkg_pp_log.p_write_message('  a_company_id: ' || a_company_id);
    pkg_pp_log.p_write_message('  a_month: ' || To_Char(a_month,'yyyymmdd'));
    pkg_pp_log.p_write_message('  a_end_log: ' || Nvl(To_Char(a_end_log), 'NULL'));

    -------------------------------------------------------------------------------
    --Reclass Long to Short Term Receivables JE's for Sales Type and Direct Finance
    -------------------------------------------------------------------------------
    select NVL(E.GL_JE_CODE, 'Lessor Reclass')
      into L_GL_JE_CODE
      from GL_JE_CONTROL G, STANDARD_JOURNAL_ENTRIES E
      where E.JE_ID = G.JE_ID
      and G.PROCESS_ID = 'Lessor Reclass';

      for l_reclass in
        ( select distinct JM.JE_METHOD_ID   as JE_METHOD_ID,
          JM.AMOUNT_TYPE              as AMOUNT_TYPE,
          JMSOB.SET_OF_BOOKS_ID         as SET_OF_BOOKS_ID,
          JMSOB.REVERSAL_CONVENTION     as REVERSAL_CONVENTION,
          LFTS.FASB_CAP_TYPE_ID       as FASB_CAP_TYPE_ID,
          LFCT.DESCRIPTION        as CAP_TYPE_DESCRIPTION,
          SCHED.ILR_ID          as ILR_ID,
          SCHED.REVISION          as REVISION,
          SCHED.BEG_LT_RECEIVABLE     as BEG_LT_RECEIVABLE,
          SCHED.END_LT_RECEIVABLE     as END_LT_RECEIVABLE,
          NVL(DF.BEGIN_LT_DEFERRED_PROFIT,0) as BEG_LT_DEF_PROFIT,
          NVL(DF.END_LT_DEFERRED_PROFIT,0) as END_LT_DEF_PROFIT,
          NVL(DF.LT_DEFERRED_PROFIT_REMEASURE,0) as LT_DEFERRED_PROFIT_REMEASURE,
		  LIA.ST_RECEIVABLE_ACCOUNT_ID  as ST_RECEIVABLE_ACCOUNT_ID,
          LIA.LT_RECEIVABLE_ACCOUNT_ID  as LT_RECEIVABLE_ACCOUNT_ID,
          LIA.DEF_SELLING_PROFIT_ACCOUNT_ID as ST_DEF_SELL_PROFIT_ACCOUNT_ID,
          LIA.LT_DEF_SELL_PROFIT_ACCOUNT_ID as LT_DEF_SELL_PROFIT_ACCOUNT_ID,
          SCHED.LT_RECEIVABLE_REMEASUREMENT as LT_RECEIVABLE_REMEASUREMENT,
          ILR.COMPANY_ID          as COMPANY_ID,
          LCDR.RATE             as CONV_RATE,
          LEASE.CONTRACT_CURRENCY_ID    as CONTRACT_CURRENCY_ID,
          CS.CURRENCY_ID          as COMPANY_CURRENCY_ID
          from JE_METHOD JM
          inner join JE_METHOD_SET_OF_BOOKS JMSOB ON JMSOB.JE_METHOD_ID = JM.JE_METHOD_ID
          inner join JE_METHOD_TRANS_TYPE JMTT ON JMTT.JE_METHOD_ID = JM.JE_METHOD_ID
          inner join LSR_FASB_TYPE_SOB LFTS ON LFTS.SET_OF_BOOKS_ID = JMSOB.SET_OF_BOOKS_ID
          inner join LSR_FASB_CAP_TYPE LFCT ON LFCT.FASB_CAP_TYPE_ID = LFTS.FASB_CAP_TYPE_ID
          inner join LSR_ILR_OPTIONS LIO ON LIO.LEASE_CAP_TYPE_ID = LFTS.CAP_TYPE_ID
          inner join LSR_ILR ILR ON ILR.ILR_ID = LIO.ILR_ID AND ILR.CURRENT_REVISION = LIO.REVISION
          inner join LSR_ILR_ACCOUNT LIA ON LIA.ILR_ID = ILR.ILR_ID
          inner join LSR_LEASE LEASE ON LEASE.LEASE_ID = ILR.LEASE_ID
          inner join COMPANY_JE_METHOD_VIEW CJMV ON CJMV.COMPANY_ID = ILR.COMPANY_ID AND CJMV.JE_METHOD_ID = JM.JE_METHOD_ID
          inner join LSR_ILR_SCHEDULE SCHED ON SCHED.ILR_ID = LIO.ILR_ID AND SCHED.REVISION = LIO.REVISION AND SCHED.SET_OF_BOOKS_ID = JMSOB.SET_OF_BOOKS_ID
          inner join CURRENCY_SCHEMA CS ON CS.COMPANY_ID = ILR.COMPANY_ID
          inner join LS_LEASE_CALCULATED_DATE_RATES LCDR ON LCDR.COMPANY_ID = ILR.COMPANY_ID 
                                                        AND LCDR.CONTRACT_CURRENCY_ID = LEASE.CONTRACT_CURRENCY_ID
		                                                    AND LCDR.COMPANY_CURRENCY_ID = CS.CURRENCY_ID 
                                                        AND LCDR.ACCOUNTING_MONTH = SCHED.MONTH
                                                        AND LCDR.EXCHANGE_RATE_TYPE_ID = 1
          left outer join LSR_ILR_SCHEDULE_DIRECT_FIN DF ON DF.ILR_ID = SCHED.ILR_ID
                                                       AND DF.REVISION = SCHED.REVISION
                                                       AND DF.SET_OF_BOOKS_ID = SCHED.SET_OF_BOOKS_ID
                                                       AND DF.MONTH = SCHED.MONTH
		  where JMTT.TRANS_TYPE IN
					( SELECT DISTINCT JE_METHOD_ID FROM JE_METHOD_TRANS_TYPE
												WHERE TRANS_TYPE in (4027, 4028, 4055, 4078))
			  and LFCT.DESCRIPTION IN ('Sales Type', 'Direct Finance')
			  and SCHED.MONTH = A_MONTH
			  and ILR.COMPANY_ID = A_COMPANY_ID 
			  and ILR.ILR_STATUS_ID = 2  --Only Approved ILRs
			)
      loop
        L_AMOUNT := NVL( (l_reclass.beg_lt_receivable + l_reclass.lt_receivable_remeasurement - l_reclass.end_lt_receivable), 0);

        if L_AMOUNT <> 0 then

           L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_reclass.ILR_ID, 4027, L_AMOUNT,
             l_reclass.ST_RECEIVABLE_ACCOUNT_ID, 0, l_reclass.COMPANY_ID, A_MONTH,
             1, L_GL_JE_CODE, l_reclass.SET_OF_BOOKS_ID, l_reclass.JE_METHOD_ID, l_reclass.AMOUNT_TYPE, l_reclass.REVERSAL_CONVENTION, l_reclass.CONV_RATE,
             l_reclass.CONTRACT_CURRENCY_ID, l_reclass.COMPANY_CURRENCY_ID, L_MSG);

           if L_RTN = -1 then
           PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
           Raise_Application_Error(-20000, 'Unable to create JE for Trans Type 4027');
           end if;

           L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_reclass.ILR_ID, 4028, L_AMOUNT,
             l_reclass.LT_RECEIVABLE_ACCOUNT_ID, 0, l_reclass.COMPANY_ID, A_MONTH,
             0, L_GL_JE_CODE, l_reclass.SET_OF_BOOKS_ID, l_reclass.JE_METHOD_ID, l_reclass.AMOUNT_TYPE, l_reclass.REVERSAL_CONVENTION, l_reclass.CONV_RATE,
             l_reclass.CONTRACT_CURRENCY_ID, l_reclass.COMPANY_CURRENCY_ID, L_MSG);

           if L_RTN = -1 then
           PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
           Raise_Application_Error(-20000, 'Unable to create JE for Trans Type 4028');
           end if;
        end if;
        
        -- ST/LT Deferred Profit Reclass
        L_AMOUNT := L_RECLASS.BEG_LT_DEF_PROFIT + L_RECLASS.LT_DEFERRED_PROFIT_REMEASURE - L_RECLASS.END_LT_DEF_PROFIT;
        if L_AMOUNT <> 0 then
          L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_reclass.ILR_ID, 4078, L_AMOUNT,
                                                 l_reclass.LT_DEF_SELL_PROFIT_ACCOUNT_ID, 0, 
                                                 l_reclass.COMPANY_ID, A_MONTH, 1, L_GL_JE_CODE, 
                                                 l_reclass.SET_OF_BOOKS_ID, l_reclass.JE_METHOD_ID, 
                                                 l_reclass.AMOUNT_TYPE, l_reclass.REVERSAL_CONVENTION, 
                                                 l_reclass.CONV_RATE, l_reclass.CONTRACT_CURRENCY_ID, 
                                                 l_reclass.COMPANY_CURRENCY_ID, L_MSG);

           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             Raise_Application_Error(-20000, 'Unable to create JE for Trans Type 4078');
           end if;

           L_RTN := PKG_LESSOR_COMMON.F_MC_BOOKJE(l_reclass.ILR_ID, 4055, L_AMOUNT,
                                                  l_reclass.ST_DEF_SELL_PROFIT_ACCOUNT_ID, 0, 
                                                  l_reclass.COMPANY_ID, A_MONTH, 0, L_GL_JE_CODE, 
                                                  l_reclass.SET_OF_BOOKS_ID, l_reclass.JE_METHOD_ID, 
                                                  l_reclass.AMOUNT_TYPE, l_reclass.REVERSAL_CONVENTION, 
                                                  l_reclass.CONV_RATE, l_reclass.CONTRACT_CURRENCY_ID, 
                                                  l_reclass.COMPANY_CURRENCY_ID, L_MSG);

           if L_RTN = -1 then
             PKG_PP_LOG.P_WRITE_MESSAGE(L_MSG);
             Raise_Application_Error(-20000, 'Unable to create JE for Trans Type 4055');
           end if;
        end if;
       end loop;

    IF a_end_log = 1 THEN
      pkg_pp_log.p_write_message('Closing log');
      pkg_pp_log.p_end_log;
    END IF;

  exception
    when OTHERS THEN
      IF SQLCODE BETWEEN -20999 AND -20000 THEN
        rollback;
        pkg_pp_log.p_write_message(sqlerrm);
        IF a_end_log = 1 THEN
          pkg_pp_log.p_end_log;
        END IF;
        RAISE;
      --PowerBuilder can't distinguish between nothing returning from the procedure (expected) and an actual no_data_found error in the procedure
      ELSIF SQLCODE = 100 THEN
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing Lessor ILRs - ' || sqlerrm || f_get_call_stack);
        IF a_end_log = 1 THEN
          pkg_pp_log.p_end_log;
        END IF;
        raise_application_error(-20000, substr('Error processing Lessor ILRs - ORA-01403 - NO DATA FOUND' || chr(10) || f_get_call_stack, 1, 2000));
      ELSE
        ROLLBACK;
        pkg_pp_log.p_write_message('Error processing lessor ILRs - ORA' || SQLCODE || ' - ' || sqlerrm || chr(10) || f_get_call_stack);
        IF a_end_log = 1 THEN
          pkg_pp_log.p_end_log;
        END IF;
        RAISE;
      END IF;
  END p_lessor_reclass_jes;

END pkg_lessor_calc;
/
