   /*
   ||=================================================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_COMMON
   || Description:
   ||=================================================================================================
   || Copyright (C) 2018 by PowerPlan, Inc. All Rights Reserved.
   ||=================================================================================================
   || Version  Date       Revised By        Reason for Change
   || -------- ---------- --------------    ----------------------------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck            Original Version
   || 10.4.2.0 03/28/2014 Kyle Peterson
   || 10.4.3.0 05/29/2014 Charlie Shilling	Add trans_type column to gl_transaction insert
   || 10.4.3.0 07/07/2014 Charlie Shilling
   || 2015.2.0 09/29/2015 Andrew Scott      Maint-45012 Join ls_ilr_payment_term for init life select
   || 2017.1.0 07/12/2017 Johnny Sisouphnah Maint-48238 Translate amounts into company currency
   || 2017.3.0 04/05/2017 Josh Sandler      PP-50691 Return lease term vs. economic life indicator with the life number
   || 2017.4.0 05/29/2018 David Conway      PP-51349 Overloaded F_MC_BOOKJE for A_GL_TRANS_STATUS_ID,
   ||                                         and added this parm to GL_TRANSACTION insert.
   || 2018.1.0 10/18/2018 A.Smith           PP-52520 add F_BALANCE_JE to fix out of balance for rounding on MC conversion
   || 2018.1.0.2 2/6/2019 C.Yura            PP-53064 add F_BALANCE_MC_AUDIT rounding plug balancing for MC Audit table
   ||=================================================================================================
   */

create or replace package PKG_LEASE_COMMON as
   G_PKG_VERSION varchar(35) := '2018.2.1.0';

   type t_init_life IS record (init_life NUMBER,
                               life_type varchar2(35));

   function F_GET_INIT_LIFE
   (
    A_LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
    A_REVISION number,
    A_ILR_ID LS_ILR.ILR_ID%type,
    A_ECONOMIC_LIFE number
   ) return t_init_life;
   
   procedure p_startLog (a_process_description in varchar2);

   -- function the return the process id for lessee calcs
   function F_GETPROCESS return number;

   -- Function to perform booking of LESSEE JEs
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_MSG           out varchar2) return number;

   --overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number;

   -- Function to perform booking of LESSEE JEs (Multicurrencies)
   function F_MC_BOOKJE(A_LS_ASSET_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_ASSET_ACT_ID  in number,
                        A_DG_ID         in number,
                        A_WO_ID         in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_PEND_TRANS_ID in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
					    A_EXCHANGE_RATE in number,
					    A_CURRENCY_FROM in number,
					    A_CURRENCY_TO   in number,
						A_MSG           out varchar2) return number;

   --overlaoaded function with A_ORIG for the ORIGINATOR column on GL_TRANSACTION (Multicurrencies)
   function F_MC_BOOKJE(A_LS_ASSET_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_ASSET_ACT_ID  in number,
                        A_DG_ID         in number,
                        A_WO_ID         in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_PEND_TRANS_ID in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
					    A_EXCHANGE_RATE in number,
					    A_CURRENCY_FROM in number,
					    A_CURRENCY_TO   in number,
                        A_ORIG          in varchar2,
                        A_MSG           out varchar2) return number;

   --overloaded function with A_POSTING_ASSET for the determining which ASSET_ID to use in PP Journal Layouts, LS_ASSET_ID or ASSET_ID (Multicurrencies)
   function F_MC_BOOKJE(A_LS_ASSET_ID   in number,
                        A_TRANS_TYPE    in number,
                        A_AMT           in number,
                        A_ASSET_ACT_ID  in number,
                        A_DG_ID         in number,
                        A_WO_ID         in number,
                        A_GL_ACCOUNT_ID in number,
                        A_GAIN_LOSS     in number,
                        A_PEND_TRANS_ID in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_DR_CR         in number,
                        A_GL_JC         in varchar2,
                        A_SOB_ID        in number,
					    A_EXCHANGE_RATE in number,
					    A_CURRENCY_FROM in number,
					    A_CURRENCY_TO   in number,
                        A_ORIG          in varchar2,
						A_POSTING_ASSET_ID in number,
                        A_MSG           out varchar2) return number;

-- Function to balance LESSEE JEs (Multicurrencies)
   function F_BALANCE_JE(A_LS_ASSET_ID   in number,
                        A_PEND_TRANS_ID in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_GL_JC         in varchar2,
                        A_MSG           out varchar2) return number;
						
-- Function to balance LESSEE JEs (Multicurrencies)
   function F_BALANCE_MC_AUDIT(A_LS_ASSET_ID   in number,
                        A_PEND_TRANS_ID in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_GL_JC         in varchar2,
                        A_MSG           out varchar2) return number;
						
-- Function to revert POST JEs to contract currency and insert records in mc audit
   function F_MC_POST_CONTRACT_CURR(A_PEND_TRANS_ID in number,
									A_COMPANY_ID    in number,
									A_MONTH         in date,
									A_MSG           out varchar2) return number;		

-- Function to check for multiple sets of books per JE method - not allowed with use of Contract Currency JE system control
   function F_JE_METHOD_SOB_CHECK(A_MSG out varchar2) return number;									

end PKG_LEASE_COMMON;
/
create or replace package body PKG_LEASE_COMMON as
   /*
   ||================================================================================================
   || Application: PowerPlant
   || Object Name: PKG_LEASE_COMMON
   || Description:
   ||================================================================================================
   || Copyright (C) 2017 by PowerPlan, Inc. All Rights Reserved.
   ||================================================================================================
   || Version  Date       Revised By        Reason for Change
   || -------- ---------- --------------    ---------------------------------------------------------
   || 10.4.0.1 05/07/2013 B.Beck            Original Version
   || 10.4.2.0 03/28/2014 Kyle Peterson
   || 10.4.3.0 05/29/2014 Charlie Shilling	add trans_type column to gl_transaction insert
   || 10.4.3.0 07/07/2014 Charlie Shilling
   || 2015.2.0 09/29/2015 Andrew Scott      Maint-45012 Join ls_ilr_payment_term for init life select
   || 2017.1.0 07/12/2017 Johnny Sisouphnah Maint-48238 Translate amounts into company currency
   || 2017.3.0 04/05/2017 Josh Sandler      PP-50691 Return lease term vs. economic life indicator with the life number
   || 2017.4.0 05/29/2018 David Conway      PP-51349 Overloaded F_MC_BOOKJE for A_GL_TRANS_STATUS_ID.
   || 2018.1.0 10/18/2018 A.Smith           PP-52520 add F_BALANCE_JE to fix out of balance for rounding on MC conversion
   || 2018.1.0.2 2/6/2019 C.Yura            PP-53064 add F_BALANCE_MC_AUDIT rounding plug balancing for MC Audit table
   ||================================================================================================
   */

   --**************************************************************
   --         TYPE DECLARATIONS
   --**************************************************************

   --**************************************************************
   --         VARIABLES
   --**************************************************************
   L_PROCESS_ID number;

   --**************************************************************
   --         PROCEDURES
   --**************************************************************
    function F_GET_INIT_LIFE
   (
    A_LS_ASSET_ID LS_ASSET.LS_ASSET_ID%type,
    A_REVISION number,
    A_ILR_ID LS_ILR.ILR_ID%type,
    A_ECONOMIC_LIFE number
   ) return t_init_life
   is
    L_INIT_LIFE    number;
    L_OWNER_TRF    number;
    L_LIFE_TYPE    VARCHAR2(35);
    LT_INIT_LIFE   t_init_life;
   begin
     -- get the initial life (all set of books have the same remaining life
    select sum(decode(payment_freq_id, 1,12,2,6,3,3,1) * number_of_terms), 'lease term'
    into L_INIT_LIFE, L_LIFE_TYPE
    from ls_ilr_payment_term
    where ilr_id = A_ILR_ID
      and revision = A_REVISION
    ;

    select case when purchase_option_type_id = 1 then 0 else 1 end
    into L_OWNER_TRF
    from ls_ilr_options
    where ilr_id = a_ilr_id
    and revision = a_revision;

    -- if there is an ownership transfer, the init life (depreciable life)
    -- should be based on the economic life of the asset and not the lease term length
    -- CJS 4/11/17 According to the standards, if their is not an ownership transfer,
    -- the depreciable life should the shorter of the two (economic life vs lease term length)
    if L_OWNER_TRF = 1 then
      L_INIT_LIFE := A_ECONOMIC_LIFE;
      L_LIFE_TYPE := 'economic life';
    elsif L_OWNER_TRF = 0 AND A_ECONOMIC_LIFE < L_INIT_LIFE then
      L_INIT_LIFE := A_ECONOMIC_LIFE;
      L_LIFE_TYPE := 'economic life';
    end if;

    lt_init_life.init_life := L_INIT_LIFE;
    lt_init_life.life_type := L_LIFE_TYPE;

    return lt_init_life;
   end F_GET_INIT_LIFE;


   procedure P_SETPROCESS is
   begin
      select PROCESS_ID
        into L_PROCESS_ID
        from PP_PROCESSES
       where UPPER(DESCRIPTION) = 'LESSEE CALCULATIONS';
   exception
      when others then
         /* this catches all SQL errors, including no_data_found */
         L_PROCESS_ID := -1;
   end P_SETPROCESS;

   --**************************************************************
   --         FUNCTIONS
   --**************************************************************
   -- Returns the process id for lease calc
   function F_GETPROCESS return number is

   begin
      return L_PROCESS_ID;
   end F_GETPROCESS;

   --Wrapper for the overloaded version
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;

   begin

      return f_bookje(A_LS_ASSET_ID, A_TRANS_TYPE, A_AMT, A_ASSET_ACT_ID, A_DG_ID,
                      A_WO_ID, A_GL_ACCOUNT_ID, A_GAIN_LOSS, A_PEND_TRANS_ID,
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID, '', A_MSG);
   exception
      when others then
         return -1;
   end F_BOOKJE;


   --Wrapper for the overloaded version (Multicurrencies)
   function F_MC_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
				     A_EXCHANGE_RATE in number,
					 A_CURRENCY_FROM in number,
					 A_CURRENCY_TO   in number,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;

   begin

      return F_MC_BOOKJE(A_LS_ASSET_ID, A_TRANS_TYPE, A_AMT, A_ASSET_ACT_ID, A_DG_ID,
                      A_WO_ID, A_GL_ACCOUNT_ID, A_GAIN_LOSS, A_PEND_TRANS_ID,
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID,
					  A_EXCHANGE_RATE, A_CURRENCY_FROM, A_CURRENCY_TO, '', A_MSG);
   exception
      when others then
         return -1;
   end F_MC_BOOKJE;

   --Wrapper for the overloaded version (Multicurrencies)
   function F_MC_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
				     A_EXCHANGE_RATE in number,
					 A_CURRENCY_FROM in number,
					 A_CURRENCY_TO   in number,
					 A_ORIG			 in varchar2,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;

   begin

      return F_MC_BOOKJE(A_LS_ASSET_ID, A_TRANS_TYPE, A_AMT, A_ASSET_ACT_ID, A_DG_ID,
                      A_WO_ID, A_GL_ACCOUNT_ID, A_GAIN_LOSS, A_PEND_TRANS_ID,
                      A_COMPANY_ID, A_MONTH, A_DR_CR, A_GL_JC, A_SOB_ID,
					  A_EXCHANGE_RATE, A_CURRENCY_FROM, A_CURRENCY_TO, A_ORIG, A_LS_ASSET_ID, A_MSG);
   exception
      when others then
         return -1;
   end F_MC_BOOKJE;

   --**************************************************************************
   --                            F_BOOKJE
   --**************************************************************************
   function F_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
                     A_ORIG          in varchar2,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;
      L_MY_COUNT number;

   begin

      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);
      L_PROCESSED := 0;
      for L_METHOD_REC in (select M.JE_METHOD_ID        as JE_METHOD_ID,
                                  S.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
                                  M.AMOUNT_TYPE         as AMOUNT_TYPE,
                                  S.REVERSAL_CONVENTION as REVERSAL_CONVENTION
                             from JE_METHOD              M,
                                  JE_METHOD_SET_OF_BOOKS S,
                                  COMPANY_SET_OF_BOOKS   C,
                                  JE_METHOD_TRANS_TYPE   T
                            where M.JE_METHOD_ID = S.JE_METHOD_ID
                              and C.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                              and C.INCLUDE_INDICATOR = 1
                              and C.COMPANY_ID = A_COMPANY_ID
                              and M.JE_METHOD_ID = T.JE_METHOD_ID
                              and T.TRANS_TYPE = A_TRANS_TYPE
                              and C.SET_OF_BOOKS_ID = A_SOB_ID)
      loop
         L_PROCESSED  := 1;

       -- before getting the GL_ACCOUNT, check to see if we want to IGNORE the JE type
      select count(1)
      into l_my_count
      from ls_lease_type_jes_exclude a, ls_ilr_options lo, ls_asset la
      where lo.revision = la.approved_revision
      and lo.lease_cap_type_id = a.lease_cap_type_id
      and a.trans_type = A_TRANS_TYPE
      and a.company_id = A_COMPANY_ID
      and la.ls_asset_id = A_LS_ASSET_ID
      and lo.ilr_id=la.ilr_id
      ;

      L_GL_ACCOUNT := 'IGNORE';
      if l_my_count = 0 then
         L_GL_ACCOUNT := PP_GL_TRANSACTION2(A_TRANS_TYPE,
                                            A_LS_ASSET_ID,
                                            A_ASSET_ACT_ID,
                                            A_DG_ID,
                                            A_WO_ID,
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            A_PEND_TRANS_ID,
                                            L_METHOD_REC.JE_METHOD_ID,
                                            L_METHOD_REC.SET_OF_BOOKS_ID,
                                            L_METHOD_REC.AMOUNT_TYPE,
                                            L_METHOD_REC.REVERSAL_CONVENTION);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;
      end if;


         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
            select PWRPLANT1.NEXTVAL,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT * NVL(L_METHOD_REC.REVERSAL_CONVENTION,1),
                   A_GL_JC,
                   to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1)),
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE),
                   'LESSEE',
                   A_ORIG,
                   CASE WHEN A_TRANS_TYPE IN (3041, 3045, 3040, 3046) THEN TO_CHAR(A_ASSET_ACT_ID) ELSE '' END,
                   A_PEND_TRANS_ID,
                   A_LS_ASSET_ID,
                   L_METHOD_REC.AMOUNT_TYPE,
                   L_METHOD_REC.JE_METHOD_ID,
                   null,
				   A_TRANS_TYPE
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;
      end loop;

      return 1;
   exception
      when others then
		 a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_BOOKJE;

   --  F_MC_BOOKJE (Multicurrencies)
   function F_MC_BOOKJE(A_LS_ASSET_ID   in number,
                     A_TRANS_TYPE    in number,
                     A_AMT           in number,
                     A_ASSET_ACT_ID  in number,
                     A_DG_ID         in number,
                     A_WO_ID         in number,
                     A_GL_ACCOUNT_ID in number,
                     A_GAIN_LOSS     in number,
                     A_PEND_TRANS_ID in number,
                     A_COMPANY_ID    in number,
                     A_MONTH         in date,
                     A_DR_CR         in number,
                     A_GL_JC         in varchar2,
                     A_SOB_ID        in number,
			         A_EXCHANGE_RATE in number,
					 A_CURRENCY_FROM in number,
					 A_CURRENCY_TO   in number,
                     A_ORIG          in varchar2,
					 A_POSTING_ASSET_ID in number,
                     A_MSG           out varchar2) return number is
      L_GL_ACCOUNT varchar2(2000);
      L_PROCESSED  number;
      L_MY_COUNT number;
	  L_GL_TRANS_ID number;
	  L_CONTRACT_JES  varchar2(100);
	  L_JE_METHOD_SOB_CHECK number;

   begin
	  
	  A_MSG       := 'Getting Contract Currency JE System Control';
	  L_CONTRACT_JES:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', A_COMPANY_ID)));
	  
	  if L_CONTRACT_JES IS NULL then
		L_CONTRACT_JES := 'no';
	  end if;
	  
	  if L_CONTRACT_JES = 'yes' then
		A_MSG := 'Checking JE Method Set of Books' ;
		L_JE_METHOD_SOB_CHECK := F_JE_METHOD_SOB_CHECK(A_MSG);
		
		if L_JE_METHOD_SOB_CHECK > 0 then
		      A_MSG := 'You cannot book contract currency JEs with more than one set of books configured per JE Method';
			  return -1;
		end if;
	  end if;
   
      A_MSG       := 'Starting JE Creation for trans_type: ' || TO_CHAR(A_TRANS_TYPE);
      L_PROCESSED := 0;
      for L_METHOD_REC in (select M.JE_METHOD_ID        as JE_METHOD_ID,
                                  S.SET_OF_BOOKS_ID     as SET_OF_BOOKS_ID,
                                  M.AMOUNT_TYPE         as AMOUNT_TYPE,
                                  S.REVERSAL_CONVENTION as REVERSAL_CONVENTION
                             from JE_METHOD              M,
                                  JE_METHOD_SET_OF_BOOKS S,
                                  COMPANY_SET_OF_BOOKS   C,
                                  JE_METHOD_TRANS_TYPE   T
                            where M.JE_METHOD_ID = S.JE_METHOD_ID
                              and C.SET_OF_BOOKS_ID = S.SET_OF_BOOKS_ID
                              and C.INCLUDE_INDICATOR = 1
                              and C.COMPANY_ID = A_COMPANY_ID
                              and M.JE_METHOD_ID = T.JE_METHOD_ID
                              and T.TRANS_TYPE = A_TRANS_TYPE
                              and C.SET_OF_BOOKS_ID = A_SOB_ID)
      loop
      L_PROCESSED  := 1;

      -- before getting the GL_ACCOUNT, check to see if we want to IGNORE the JE type
      select count(1)
      into l_my_count
      from ls_lease_type_jes_exclude a, ls_ilr_options lo, ls_asset la
      where lo.revision = la.approved_revision
      and lo.lease_cap_type_id = a.lease_cap_type_id
      and a.trans_type = A_TRANS_TYPE
      and a.company_id = A_COMPANY_ID
      and la.ls_asset_id = A_LS_ASSET_ID
      and lo.ilr_id=la.ilr_id
      ;

      L_GL_ACCOUNT := 'IGNORE';
      if l_my_count = 0 then
         L_GL_ACCOUNT := PP_GL_TRANSACTION2(A_TRANS_TYPE,
                                            A_POSTING_ASSET_ID,
                                            A_ASSET_ACT_ID,
                                            A_DG_ID,
                                            A_WO_ID,
                                            A_GL_ACCOUNT_ID,
                                            A_GAIN_LOSS,
                                            A_PEND_TRANS_ID,
                                            L_METHOD_REC.JE_METHOD_ID,
                                            L_METHOD_REC.SET_OF_BOOKS_ID,
                                            L_METHOD_REC.AMOUNT_TYPE,
                                            L_METHOD_REC.REVERSAL_CONVENTION);

         if LOWER(L_GL_ACCOUNT) like '%error%' then
            A_MSG := L_GL_ACCOUNT;
            return - 1;
         end if;
      end if;

      select PWRPLANT1.NEXTVAL into L_GL_TRANS_ID from dual;

         A_MSG := 'Inserting into gl_transaction: ' || TO_CHAR(A_TRANS_TYPE);
         insert into GL_TRANSACTION
            (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
             GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
             ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
            select L_GL_TRANS_ID,
                   A_MONTH,
                   C.GL_COMPANY_NO,
                   L_GL_ACCOUNT,
                   A_DR_CR,
                   A_AMT * decode(L_CONTRACT_JES, 'yes', 1, A_EXCHANGE_RATE) * NVL(L_METHOD_REC.REVERSAL_CONVENTION,1),
                   A_GL_JC,
                   to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1)),
                   'TRANS TYPE: ' || TO_CHAR(A_TRANS_TYPE),
                   'LESSEE',
                   A_ORIG,
                   CASE WHEN A_TRANS_TYPE IN (3041, 3045, 3040, 3046) THEN TO_CHAR(A_ASSET_ACT_ID) ELSE '' END,
                   A_PEND_TRANS_ID,
                   A_LS_ASSET_ID,
                   L_METHOD_REC.AMOUNT_TYPE,
                   L_METHOD_REC.JE_METHOD_ID,
                   null,
           A_TRANS_TYPE
              from COMPANY_SETUP C
             where C.COMPANY_ID = A_COMPANY_ID;

         A_MSG := 'Inserting into ls_mc_gl_transaction_audit: ' || TO_CHAR(A_TRANS_TYPE);
     insert into LS_MC_GL_TRANSACTION_AUDIT
        (COMPANY_ID, GL_POSTING_MO_YR, GL_TRANS_ID, TRANS_TYPE, GL_JE_CODE,
      FROM_CURRENCY, TO_CURRENCY, EXCHANGE_RATE, CALCULATED_AMOUNT, TRANSLATED_AMOUNT, LS_CURRENCY_TYPE_ID)
         select A_COMPANY_ID,
              A_MONTH,
          L_GL_TRANS_ID,
          A_TRANS_TYPE,
          A_GL_JC,
          A_CURRENCY_FROM,
          A_CURRENCY_TO,
          A_EXCHANGE_RATE,
          A_AMT * NVL(L_METHOD_REC.REVERSAL_CONVENTION,1),
          A_AMT * decode(L_CONTRACT_JES, 'yes', 1, A_EXCHANGE_RATE) * NVL(L_METHOD_REC.REVERSAL_CONVENTION,1),
		  decode(L_CONTRACT_JES, 'yes', 1, 2)
      from DUAL;

      end loop;

      return 1;
   exception
      when others then
     a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_MC_BOOKJE;

   -- Function to balance LESSEE JEs (Multicurrencies)
   function F_BALANCE_JE(A_LS_ASSET_ID   in number,
                        A_PEND_TRANS_ID in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_GL_JC         in varchar2,
                        A_MSG           out varchar2) return number is
	
      type tt_array is varray(7) of number;
      check_array tt_array;
      
      tt  number;
      tt_match number;
      L_GL_TRANS_ID number;
      L_NEW_TRANS_ID number;
	  L_CONTRACT_JES  varchar2(100);
      
   begin

   A_MSG       := 'Getting Contract Currency JE System Control';
   L_CONTRACT_JES:=lower(trim(PKG_PP_SYSTEM_CONTROL.f_pp_system_control_company('Lease: Contract Currency JEs', A_COMPANY_ID)));
          			  
   if L_CONTRACT_JES IS NULL then
		L_CONTRACT_JES := 'no';
   end if;
   
   if L_CONTRACT_JES = 'yes' then
		check_array := tt_array(3026,3001);
	else 
	    check_array := tt_array(3042, 3033, 3055, 3061, 3059, 3002, 3011);
   end if;	
   
      
   A_MSG       := 'Balancing JE for pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');

   for L_BAL_REC in 
             (select month, company_number, gl_je_code, gl_status_id, source, pend_trans_id, asset_id, amount_type, je_method_id
							, sum(amount * decode(debit_credit_indicator, 0, -1, 1) ) as balance
							from gl_transaction 
							where pend_trans_id = nvl(a_pend_trans_id, -1)
							and asset_id = a_ls_asset_id
              and trim(company_number) = (select trim(c.gl_company_no) from COMPANY_SETUP C where C.COMPANY_ID = a_company_id)
              and month = a_month
              and trim(gl_je_code) = trim(a_gl_jc)
              and gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1))
							group by month, company_number, gl_je_code, gl_status_id, source, pend_trans_id, amount_type, je_method_id, asset_id
							having sum(amount * decode(debit_credit_indicator, 0, -1, 1) ) <> 0
							)
      loop
           
           if ABS(L_BAL_REC.Balance) >= 1.0 then
             -- should not adjust  anything more than 1 of currency             
             A_MSG := 'ERROR: Aborting because transaction JE''s are out of balance by more than 1.0. OOB = ' || TO_CHAR(L_BAL_REC.BALANCE) 
                      || '. For pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');
             return -1;
           end if;
           
           --look for trans types to plug
           for tt in 1..check_array.count loop
             TT_MATCH := 0;
             
             select count(1), max(gl_trans_id) into TT_MATCH, L_GL_TRANS_ID
             from gl_transaction
             where pend_trans_id = L_BAL_REC.PEND_TRANS_ID
              and asset_id = L_BAL_REC.ASSET_ID
              and company_number = L_BAL_REC.COMPANY_NUMBER
              and month = L_BAL_REC.MONTH
              and gl_je_code = L_BAL_REC.GL_JE_CODE
              and gl_status_id = L_BAL_REC.GL_STATUS_ID
			  and je_method_id = L_BAL_REC.JE_METHOD_ID
              and amount_type = L_BAL_REC.amount_type
			  and trans_type = check_array(tt);
              
             if TT_MATCH = 0 THEN
               if TT = check_array.count THEN
                 -- out of TT so just get max txn
                 select count(1), max(gl_trans_id) into TT_MATCH, L_GL_TRANS_ID
                 from gl_transaction
                 where pend_trans_id = L_BAL_REC.PEND_TRANS_ID
                  and asset_id = L_BAL_REC.ASSET_ID
                  and company_number = L_BAL_REC.COMPANY_NUMBER
                  and month = L_BAL_REC.MONTH
                  and gl_je_code = L_BAL_REC.GL_JE_CODE
                  and gl_status_id = L_BAL_REC.GL_STATUS_ID
				  and je_method_id = L_BAL_REC.JE_METHOD_ID
				  and amount_type = L_BAL_REC.amount_type;
               else
                 CONTINUE;
               end if;
             end if;
             
             --found so insert plug.  need to flip the sign if adjusting a debit entry
             PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG || '; OOB='||to_char(l_bal_rec.balance) );
        		
             select PWRPLANT1.NEXTVAL into L_NEW_TRANS_ID from dual;

             A_MSG := 'Inserting balancing entry into gl_transaction: for pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');
             insert into GL_TRANSACTION
              (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
               GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
               ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
              select L_NEW_TRANS_ID,
                     GL.MONTH,
                     GL.COMPANY_NUMBER,
                     GL.GL_ACCOUNT,
                     GL.DEBIT_CREDIT_INDICATOR,
                     L_BAL_REC.BALANCE * DECODE(GL.DEBIT_CREDIT_INDICATOR,1, -1, 1),
                     GL.GL_JE_CODE,
                     GL.GL_STATUS_ID,
                     GL.DESCRIPTION,
                     GL.SOURCE,
                     GL.ORIGINATOR,
                     GL.COMMENTS,
                     GL.PEND_TRANS_ID,
                     GL.ASSET_ID,
                     GL.AMOUNT_TYPE,
                     GL.JE_METHOD_ID,
                     GL.TAX_ORIG_MONTH_NUMBER,
                     GL.TRANS_TYPE
                from GL_TRANSACTION GL
               where gl_trans_id = L_GL_TRANS_ID;

             A_MSG := 'Inserting balancing entry into ls_mc_gl_transaction_audit: for pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');

             insert into LS_MC_GL_TRANSACTION_AUDIT
                (COMPANY_ID, GL_POSTING_MO_YR, GL_TRANS_ID, TRANS_TYPE, GL_JE_CODE,
              FROM_CURRENCY, TO_CURRENCY, EXCHANGE_RATE, CALCULATED_AMOUNT, TRANSLATED_AMOUNT, LS_CURRENCY_TYPE_ID)
                 select MC.COMPANY_ID,
                      MC.GL_POSTING_MO_YR,
                      L_NEW_TRANS_ID,
                      MC.TRANS_TYPE,
                      MC.GL_JE_CODE,
                      MC.FROM_CURRENCY,
                      MC.TO_CURRENCY,
                      MC.EXCHANGE_RATE,
                      decode(L_CONTRACT_JES, 'no', 0, (select GL.AMOUNT from GL_TRANSACTION GL where GL.GL_TRANS_ID = L_NEW_TRANS_ID)),
                      (select GL.AMOUNT from GL_TRANSACTION GL where GL.GL_TRANS_ID = L_NEW_TRANS_ID),
					 MC.LS_CURRENCY_TYPE_ID
              from LS_MC_GL_TRANSACTION_AUDIT MC
               where MC.gl_trans_id = L_GL_TRANS_ID;

              exit;
              
           end loop;
           
      end loop;

      return 1;
   exception
      when others then
     a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_BALANCE_JE;

   
  function F_MC_POST_CONTRACT_CURR(A_PEND_TRANS_ID in number,
                        A_COMPANY_ID    in number,
                        A_MONTH         in date,
                        A_MSG           out varchar2) return number is
	
	
   L_COUNT number;   

   begin
      L_COUNT := 0;
      A_MSG       := 'Converting POST JEs to Contract Currency for pend_trans: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null');
	
   for L_CONTRACT_UPDATE in	
	  (select a.gl_trans_id gl_trans_id, a.gl_je_code, case when trans_type in (3030,3031,3043,3044) then nvl(war.net_weighted_avg_rate,0) else nvl(war.gross_weighted_avg_rate,0) end exchange_Rate, a.amount amount, 
	    b.contract_currency_id contract_currency_id, cs.currency_id company_currency_id, a.trans_type trans_type
		From gl_transaction a, ls_asset b, ls_ilr_weighted_avg_Rates war, je_method_set_of_books je_sob, currency_schema cs
			where a.pend_trans_id = nvl(a_pend_trans_id,-1)
			  and a.asset_id = b.ls_asset_id
              and b.ilr_id = war.ilr_id
              and je_sob.je_method_id = a.je_method_id
              and war.set_of_books_id = je_sob.set_of_books_id
              and war.revision = b.approved_revision
			  and b.company_id = cs.company_id
              and trim(a.company_number) = (select trim(c.gl_company_no) from COMPANY_SETUP C where C.COMPANY_ID = a_company_id)
              and a.month = a_month
              and a.trans_type in (3025,3026,3030,3031,3034,3035,3043,3044,3047,3048)
              and a.gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', a_company_id)), 1)))
	loop
	  
	L_COUNT := L_COUNT+1;
	if L_CONTRACT_UPDATE.exchange_rate = 0 then        
		 A_MSG := 'ERROR: Aborting because net weighted avg rate for contract currency conversion = 0'
				  || '. For pend_trans: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null');
		 return -1;
    end if;

	 A_MSG := 'Updating POST GL Transactions to Contract Currency: for pend_trans: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null');

	 update gl_transaction set amount = L_CONTRACT_UPDATE.amount/L_CONTRACT_UPDATE.exchange_rate where gl_trans_id = L_CONTRACT_UPDATE.gl_trans_id;
		
	 A_MSG := 'Inserting POST contract currency conversion entry into ls_mc_gl_transaction_audit: for pend_trans: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null');

	 insert into LS_MC_GL_TRANSACTION_AUDIT
		(COMPANY_ID, GL_POSTING_MO_YR, GL_TRANS_ID, TRANS_TYPE, GL_JE_CODE,
	     FROM_CURRENCY, TO_CURRENCY, EXCHANGE_RATE, CALCULATED_AMOUNT, TRANSLATED_AMOUNT, LS_CURRENCY_TYPE_ID)
		values
		(A_COMPANY_ID, A_MONTH, L_CONTRACT_UPDATE.GL_TRANS_ID, L_CONTRACT_UPDATE.TRANS_TYPE, L_CONTRACT_UPDATE.gl_je_code, L_CONTRACT_UPDATE.CONTRACT_CURRENCY_ID, L_CONTRACT_UPDATE.COMPANY_CURRENCY_ID, L_CONTRACT_UPDATE.EXCHANGE_RATE,
		 L_CONTRACT_UPDATE.amount/L_CONTRACT_UPDATE.exchange_rate, L_CONTRACT_UPDATE.amount/L_CONTRACT_UPDATE.exchange_rate, 1);

	 end loop;
	
	if L_COUNT = 0 then
     A_MSG := 'ERROR: Aborting because F_MC_POST_CONTRACT_CURR could not find POST transactions to convert to contract currency'
				  || '. For pend_trans: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null');
		 return -1;    
   end if;

      return 1;
   exception
      when others then
     a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_MC_POST_CONTRACT_CURR;

   function F_JE_METHOD_SOB_CHECK(A_MSG out varchar2) return number is
      L_RTN number;
    begin 
        select count(*) into L_RTN from (select je_method_id from je_method_set_of_books group by je_method_id having count(*) > 1);
    return L_RTN;  
    exception
      when others then
      a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
    end F_JE_METHOD_SOB_CHECK;
  
   -- Function to balance LESSEE JEs (Multicurrencies)
   function F_BALANCE_MC_AUDIT(A_LS_ASSET_ID   in number,
							   A_PEND_TRANS_ID in number,
							   A_COMPANY_ID    in number,
							   A_MONTH         in date,
							   A_GL_JC         in varchar2,
							   A_MSG           out varchar2) return number is
	
      type tt_array is varray(2) of number;
      check_array tt_array;
      
      tt  number;
      tt_match number;
      L_GL_TRANS_ID number;
      L_NEW_TRANS_ID number;
      
   begin

      check_array := tt_array(3026,3001);
      A_MSG       := 'Balancing MC Audit for pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');

   for L_BAL_REC in 
             (select gl_trans.month, gl_trans.company_number, gl_trans.gl_je_code, gl_trans.gl_status_id, gl_trans.source, gl_trans.pend_trans_id, 
                     gl_trans.asset_id, gl_trans.amount_type, gl_trans.je_method_id, sum(mc_audit.calculated_amount * decode(gl_trans.debit_credit_indicator, 0, -1, 1) ) as balance
			  from gl_transaction gl_trans, LS_MC_GL_TRANSACTION_AUDIT mc_audit
			   where gl_trans.gl_Trans_id = mc_audit.gl_Trans_id 
              and gl_trans.pend_trans_id = nvl(a_pend_trans_id, -1)
			  and gl_trans.asset_id = a_ls_asset_id
              and trim(gl_trans.company_number) = (select trim(c.gl_company_no) from COMPANY_SETUP C where C.COMPANY_ID = a_company_id)
              and gl_trans.month = a_month
              and trim(gl_trans.gl_je_code) = trim(a_gl_jc)
              and gl_trans.gl_status_id = to_number(nvl(trim(pkg_pp_system_control.f_pp_system_control_company('Lease Specific GL Trans Status', A_COMPANY_ID)), 1))
				group by gl_trans.month, gl_trans.company_number, gl_trans.gl_je_code, gl_trans.gl_status_id, gl_trans.source, gl_trans.pend_trans_id, gl_trans.amount_type, gl_trans.je_method_id, 
                         gl_trans.asset_id
				having sum(mc_audit.calculated_amount * decode(gl_trans.debit_credit_indicator, 0, -1, 1) ) <> 0
			  )
      loop
           
           if ABS(L_BAL_REC.Balance) >= 1.0 then
             -- should not adjust  anything more than 1 of currency             
             A_MSG := 'ERROR: Aborting because transaction JE''s are out of balance by more than 1.0. OOB = ' || TO_CHAR(L_BAL_REC.BALANCE) 
                      || '. For pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');
             return -1;
           end if;
           
           --look for trans types to plug
           for tt in 1..check_array.count loop
             TT_MATCH := 0;
             
             select count(1), max(gl_trans_id) into TT_MATCH, L_GL_TRANS_ID
             from gl_transaction
             where pend_trans_id = L_BAL_REC.PEND_TRANS_ID
              and asset_id = L_BAL_REC.ASSET_ID
              and company_number = L_BAL_REC.COMPANY_NUMBER
              and month = L_BAL_REC.MONTH
              and gl_je_code = L_BAL_REC.GL_JE_CODE
              and gl_status_id = L_BAL_REC.GL_STATUS_ID
			  and je_method_id = L_BAL_REC.JE_METHOD_ID
			  and amount_type = L_BAL_REC.amount_type
              and trans_type = check_array(tt);
              
             if TT_MATCH = 0 THEN
               if TT = check_array.count THEN
                 -- out of TT so just get max txn
                 select count(1), max(gl_trans_id) into TT_MATCH, L_GL_TRANS_ID
                 from gl_transaction
                 where pend_trans_id = L_BAL_REC.PEND_TRANS_ID
                  and asset_id = L_BAL_REC.ASSET_ID
                  and company_number = L_BAL_REC.COMPANY_NUMBER
                  and month = L_BAL_REC.MONTH
                  and gl_je_code = L_BAL_REC.GL_JE_CODE
                  and gl_status_id = L_BAL_REC.GL_STATUS_ID
				  and je_method_id = L_BAL_REC.JE_METHOD_ID
				  and amount_type = L_BAL_REC.amount_type;
               else
                 CONTINUE;
               end if;
             end if;
             
             --found so insert plug.  need to flip the sign if adjusting a debit entry
             PKG_PP_LOG.P_WRITE_MESSAGE(A_MSG || '; OOB='||to_char(l_bal_rec.balance) );
        		
             select PWRPLANT1.NEXTVAL into L_NEW_TRANS_ID from dual;

             A_MSG := 'Inserting balancing entry into gl_transaction: for pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');
             insert into GL_TRANSACTION
              (GL_TRANS_ID, "MONTH", COMPANY_NUMBER, GL_ACCOUNT, DEBIT_CREDIT_INDICATOR, AMOUNT,
               GL_JE_CODE, GL_STATUS_ID, DESCRIPTION, source, ORIGINATOR, COMMENTS, PEND_TRANS_ID,
               ASSET_ID, AMOUNT_TYPE, JE_METHOD_ID, TAX_ORIG_MONTH_NUMBER, TRANS_TYPE)
              select L_NEW_TRANS_ID,
                     GL.MONTH,
                     GL.COMPANY_NUMBER,
                     GL.GL_ACCOUNT,
                     GL.DEBIT_CREDIT_INDICATOR,
                     0,
                     GL.GL_JE_CODE,
                     3, /*Don't want to send 0s to the CR or GL*/
                     GL.DESCRIPTION,
                     GL.SOURCE,
                     GL.ORIGINATOR,
                     GL.COMMENTS,
                     GL.PEND_TRANS_ID,
                     GL.ASSET_ID,
                     GL.AMOUNT_TYPE,
                     GL.JE_METHOD_ID,
                     GL.TAX_ORIG_MONTH_NUMBER,
                     GL.TRANS_TYPE
                from GL_TRANSACTION GL
               where gl_trans_id = L_GL_TRANS_ID;

             A_MSG := 'Inserting balancing entry into ls_mc_gl_transaction_audit: for pend_trans/asset_id: ' || nvl(TO_CHAR(A_PEND_TRANS_ID),'null') || '/' || nvl(TO_CHAR(A_LS_ASSET_ID),'null');

             insert into LS_MC_GL_TRANSACTION_AUDIT
                (COMPANY_ID, GL_POSTING_MO_YR, GL_TRANS_ID, TRANS_TYPE, GL_JE_CODE,
                 FROM_CURRENCY, TO_CURRENCY, EXCHANGE_RATE, CALCULATED_AMOUNT, TRANSLATED_AMOUNT, LS_CURRENCY_TYPE_ID)
                 select MC.COMPANY_ID,
                      MC.GL_POSTING_MO_YR,
                      L_NEW_TRANS_ID,
                      MC.TRANS_TYPE,
                      MC.GL_JE_CODE,
                      MC.FROM_CURRENCY,
                      MC.TO_CURRENCY,
                      MC.EXCHANGE_RATE,
                      L_BAL_REC.BALANCE * DECODE((select GL.DEBIT_CREDIT_INDICATOR from GL_TRANSACTION GL where GL.GL_TRANS_ID = L_NEW_TRANS_ID),1, -1, 1),
                      0,
					 MC.LS_CURRENCY_TYPE_ID
              from LS_MC_GL_TRANSACTION_AUDIT MC
               where MC.gl_trans_id = L_GL_TRANS_ID;

              exit;
              
           end loop;
           
      end loop;

      return 1;
   exception
      when others then
     a_msg:=SUBSTR(A_MSG || ': ' || sqlerrm, 1, 2000);
         return -1;
   end F_BALANCE_MC_AUDIT;
   
   procedure p_startLog (a_process_description varchar2) is 
   begin
		--Built with Process description as part of the function for if Lessee ever decides to break out its processes
		--Start log for SSP_LEASE_CONTROL
		pkg_pp_log.p_start_log(pkg_lease_common.f_getProcess());
	
   end p_startLog;

 begin
   -- initialize the package
   P_SETPROCESS;
end PKG_LEASE_COMMON;
/
