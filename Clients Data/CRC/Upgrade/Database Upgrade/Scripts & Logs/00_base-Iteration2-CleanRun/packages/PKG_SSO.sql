/*
||============================================================================
|| Application: PowerPlan
|| Object Name: PKG_SSO.sql
|| Description: Functionality to mass-add SSO users from the backend.
||============================================================================
|| Copyright (C) 2015 by PowerPlan Consultants, Inc. All Rights Reserved.
||============================================================================
|| Version  Date       Revised By     Reason for Change
|| -------- ---------- -------------- ----------------------------------------
|| 2015.1.0 05/13/2015 D. Motter      Original Version
||============================================================================
*/

create or replace package PKG_SSO as
    procedure F_ADD_USER    (username   varchar2);
    procedure F_ADD_USERS   (userlist   PKG_PP_COMMON.VARCHAR_TABTYPE);
end PKG_SSO;
/

create or replace package body PKG_SSO as  

    --**************************************************************************
    --                            F_ADD_USER
    --**************************************************************************   
    
	procedure F_ADD_USER    (username   varchar2) is
        l_copy_from     varchar2(255)   :=  'pwrplant';
        l_password      varchar2(255)   :=  'default';
        val             number;
    begin
        /* Delete old records for rerunability */
        delete from CR_COMPANY_SECURITY where lower(users) = lower(username);
        delete from PP_COMPANY_SECURITY where lower(users) = lower(username);
        delete from PP_SECURITY_USERS_GROUPS where lower(users) = lower(username);
        delete from PP_SECURITY_USERS where lower(users) = lower(username);

        /* Copy user information from another user */
        insert into PP_SECURITY_USERS
            (users, department_id, group_control, status, company_id,
            del_count, directory, language_id, locale_id, digit_len)
        select
            lower(username), department_id, group_control, status, company_id,
            del_count, directory, language_id, locale_id, digit_len
        from PP_SECURITY_USERS where lower(users) = lower(l_copy_from);

        insert into PP_SECURITY_USERS_GROUPS
            (users, groups)
        select
            lower(username), groups
        from PP_SECURITY_USERS_GROUPS where lower(users) = lower(l_copy_from);

        /* Update user information */
        update PP_SECURITY_USERS set
            mail_id = '',
            locale_id = '',
            digit_len = 2,
            group_control = '',
            description = initcap(substr(username, 2)) || ', ' || initcap(substr(username, 1, 1)) || '.',
            last_name = initcap(substr(username, 2)),
            first_name = initcap(substr(username, 1, 1)) || '.'
        where lower(users) = lower(username);

        /* Give access to all companies */
        insert into PP_COMPANY_SECURITY
            (company_id, users)
        select
            company_id, lower(username)
        from COMPANY_SETUP;

        insert into CR_COMPANY_SECURITY
            (valid_values, users)
        select
            distinct valid_values, lower(username)
        from CR_COMPANY_SECURITY;

        /* Need to use execute immediate for DDL in PL/SQL */
        execute immediate 'select count(*) from all_users where lower(username) = ''' || lower(username) || '''' into val;
        if val > 0 then
            execute immediate 'drop user "' || upper(username) || '" cascade';
        end if;
        execute immediate 'create user ' || lower(username) || ' identified by ' || l_password ||
                          ' default tablespace USERS temporary tablespace TEMP quota unlimited on USERS';
        execute immediate 'grant pwrplant_role_user to ' || upper(username);
        execute immediate 'grant pwrplant_role_dev to ' || upper(username);
        execute immediate 'alter user ' || lower(username) || ' default role pwrplant_role_user';
        execute immediate 'alter user ' || lower(username) || ' grant connect through ' || lower(username);
        execute immediate 'alter user PPCSSO account unlock';
        execute immediate 'alter user PPCSSO identified by ppcsso';

        dbms_output.put_line('User ' || username || ' successfully added as single sign on.');
    end F_ADD_USER;

    --**************************************************************************
    --                            F_ADD_USERS
    --**************************************************************************    
    
    procedure F_ADD_USERS   (userlist   PKG_PP_COMMON.VARCHAR_TABTYPE) is
    begin
        for i in userlist.first .. userlist.last loop
            F_ADD_USER(userlist(i));
        end loop;
    end F_ADD_USERS;
    
end PKG_SSO;
/