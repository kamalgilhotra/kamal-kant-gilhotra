create or replace package PKG_PP_GET_VALUE_ASYNC is
  --||============================================================================
  --|| Application: PowerPlan
  --|| Object Name: pkg_pp_get_value_async
  --|| Description:
  --||============================================================================
  --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
  --||============================================================================
  --|| Version  Date       Revised By        Reason for Change
  --|| -------- ---------- ----------------- -------------------------------------
  --|| 1.0      12/12/2013 Stephen Motter      Original Version
  --||============================================================================
  G_PKG_VERSION varchar(35) := '2018.2.1.0'; 
 
  function F_GET_VARCHAR2(A_ARG varchar2) return varchar2;

  function F_GET_NUMBER(A_ARG varchar2) return number;

  function F_GET_DATE(A_ARG varchar2) return date;

end PKG_PP_GET_VALUE_ASYNC;
/

create or replace package body PKG_PP_GET_VALUE_ASYNC is
  -- =============================================================================
  --  Function F_GET_VARCHAR2
  -- =============================================================================
  function F_GET_VARCHAR2(A_ARG varchar2) return varchar2 as
    pragma autonomous_transaction;
    RETURN_VAL varchar2(4000);
  begin
    execute immediate A_ARG
      into RETURN_VAL;
    return RETURN_VAL;
  exception
    when others then
      return null;
  end;
  -- =============================================================================
  --  Function F_GET_NUMBER
  -- =============================================================================
  function F_GET_NUMBER(A_ARG varchar2) return number as
    pragma autonomous_transaction;
    RETURN_VAL number;
  begin
    execute immediate A_ARG
      into RETURN_VAL;
    return RETURN_VAL;
  exception
    when others then
      return null;
  end;
  -- =============================================================================
  --  Function F_GET_DATE
  -- =============================================================================
  function F_GET_DATE(A_ARG varchar2) return date as
    pragma autonomous_transaction;
    RETURN_VAL date;
  begin
    execute immediate A_ARG
      into RETURN_VAL;
    return RETURN_VAL;
  exception
    when others then
      return null;
  end;
end PKG_PP_GET_VALUE_ASYNC;
/