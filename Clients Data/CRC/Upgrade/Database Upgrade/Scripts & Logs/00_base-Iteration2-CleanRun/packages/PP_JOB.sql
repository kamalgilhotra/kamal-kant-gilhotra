create or replace package PP_JOB is
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: PP_JOB
   --|| Description: Submit Jobs to Oracle Job Queue
   --||============================================================================
   --|| Copyright (C) 2013 by PowerPlan Consultants, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 1.0      03/20/2013 Roger Roach    Original Version
   --|| 1.1      05/24/2013 Roger Roach    Added second submit for tax archiving
   --|| 1.3      02/18/2014 Andrew Scott   Remove "pwrplant" schema reference (maint 34866)
   --|| 1.4      08/16/2015 Andrew Scott   allow DBMS_SCHEDULER to be used (maint 44603)
   --||============================================================================
   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   
   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2,
                   A_ID    pls_integer) return integer;

   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2) return integer;

   function REMOVE(A_ID integer) return integer;

   function CREATE_JOB(A_TYPE  varchar2,
                   A_FUNCT varchar2,
                   A_ID    pls_integer) return integer;

   function CREATE_JOB(A_TYPE  varchar2,
                   A_FUNCT varchar2) return integer;

   function DROP_JOB(A_ID    pls_integer) return integer;

end PP_JOB;
/
create or replace package body PP_JOB is

   -- =============================================================================
   --  Function SUBMIT.  Uses dbms_job.
   -- =============================================================================
   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2,
                   A_ID    pls_integer) return integer is

      L_SQL  varchar2(200);
      L_NUM  pls_integer;
      L_CODE pls_integer;

   begin
      DBMS_OUTPUT.ENABLE(10000);

      case A_TYPE
         when 'function' then
            L_SQL := 'declare code number; begin code := ' || A_FUNCT || '(' ||
                     TO_CHAR(A_ID) || '); end;';
         when 'procedure' then
            L_SQL := A_FUNCT || '(' || TO_CHAR(A_ID) || ');';
         else
            return -1;
      end case;

      DBMS_JOB.SUBMIT(L_NUM, L_SQL, sysdate, null);
      DBMS_OUTPUT.PUT_LINE('Submit Tax Job ' || TO_CHAR(L_NUM) || ' ');
      commit;
      return(L_NUM);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Submit Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end SUBMIT;

   -- =============================================================================
   --  Function SUBMIT.  Uses dbms_job.
   -- =============================================================================
   function SUBMIT(A_TYPE  varchar2,
                   A_FUNCT varchar2) return integer is

      L_SQL  varchar2(200);
      L_NUM  pls_integer;
      L_CODE pls_integer;

   begin
      DBMS_OUTPUT.ENABLE(10000);

      case A_TYPE
         when 'function' then
            L_SQL := 'declare code number; begin code := ' || A_FUNCT || ' ; end;';
         when 'procedure' then
            L_SQL := A_FUNCT;
         else
            return - 1;
      end case;

      DBMS_JOB.SUBMIT(L_NUM, L_SQL, sysdate, null);
      DBMS_OUTPUT.PUT_LINE('Submit Tax Job ' || TO_CHAR(L_NUM) || ' ');
      commit;
      return(L_NUM);
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Submit Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end SUBMIT;

   -- =============================================================================
   --  Function REMOVE.  Uses dbms_job.
   -- =============================================================================
   function REMOVE(A_ID integer) return integer is

      L_CODE pls_integer;

   begin
      DBMS_JOB.REMOVE(A_ID);
      commit;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Remove Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end REMOVE;

   -- =============================================================================
   --  Function CREATE_JOB.  Uses dbms_scheduler.
   -- =============================================================================
   function CREATE_JOB(A_TYPE  varchar2,
                   A_FUNCT varchar2,
                   A_ID    pls_integer) return integer is

      L_SQL  varchar2(200);
      L_JOB_NAME  varchar2(200);
      L_CODE pls_integer;

   begin
      DBMS_OUTPUT.ENABLE(10000);

      case A_TYPE
         when 'function' then
            L_SQL := 'declare code number; begin code := ' || A_FUNCT || '(' ||
                     TO_CHAR(A_ID) || '); end;';
         when 'procedure' then
            L_SQL := A_FUNCT || '(' || TO_CHAR(A_ID) || ');';
         else
            return -1;
      end case;

      L_JOB_NAME := 'PPJOB_'||TO_CHAR(A_ID);

      DBMS_SCHEDULER.CREATE_JOB( 
         JOB_NAME => L_JOB_NAME, 
         JOB_TYPE => 'plsql_block',
         JOB_ACTION => L_SQL
         );
      DBMS_SCHEDULER.ENABLE (L_JOB_NAME);
      DBMS_OUTPUT.PUT_LINE('Create and Enable Job ' || L_JOB_NAME || ' ');
      commit;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Create Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end CREATE_JOB;

   -- =============================================================================
   --  Function CREATE_JOB.  Uses dbms_scheduler.
   -- =============================================================================
   function CREATE_JOB(A_TYPE  varchar2,
                   A_FUNCT varchar2) return integer is

      L_SQL  varchar2(200);
      L_JOB_NAME  varchar2(200);
      L_CODE pls_integer;

   begin
      DBMS_OUTPUT.ENABLE(10000);

      case A_TYPE
         when 'function' then
            L_SQL := 'declare code number; begin code := ' || A_FUNCT || ' ; end;';
         when 'procedure' then
            L_SQL := A_FUNCT;
         else
            return - 1;
      end case;

      L_JOB_NAME := dbms_scheduler.generate_job_name( prefix => 'PPJOB_AUTO_');

      DBMS_SCHEDULER.CREATE_JOB( 
         JOB_NAME => L_JOB_NAME, 
         JOB_TYPE => 'plsql_block',
         JOB_ACTION => L_SQL
         );
      DBMS_SCHEDULER.ENABLE (L_JOB_NAME);
      DBMS_OUTPUT.PUT_LINE('Create and Enable Job ' || L_JOB_NAME || ' ');
      commit;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Create Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end CREATE_JOB;

   -- =============================================================================
   --  Function DROP_JOB.  Uses dbms_scheduler.
   -- =============================================================================
   function DROP_JOB(A_ID pls_integer) return integer is

      L_CODE pls_integer;
      L_JOB_NAME varchar2(200);

   begin
      L_JOB_NAME := 'PPJOB_'||TO_CHAR(A_ID);
      DBMS_SCHEDULER.DROP_JOB( 
         JOB_NAME => L_JOB_NAME, 
         FORCE => TRUE,
         COMMIT_SEMANTICS => 'ABSORB_ERRORS'
         );
      commit;
      return 0;
   exception
      when others then
         L_CODE := sqlcode;
         RAISE_APPLICATION_ERROR(-20029,
                                 'Drop Job ' || sqlerrm(L_CODE) || ' ' ||
                                 DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
         return L_CODE;
   end DROP_JOB;

end PP_JOB;
/
