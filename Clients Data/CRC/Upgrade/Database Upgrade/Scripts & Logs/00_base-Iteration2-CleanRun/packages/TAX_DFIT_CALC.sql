CREATE OR REPLACE package tax_dfit_calc is
   --||============================================================================
   --|| Application: PowerPlan
   --|| Object Name: TAX_DFIT_CALC
   --|| Description: deferred income tax calculation
   --||============================================================================
   --|| Copyright (C) 2014 by PowerPlan, Inc. All Rights Reserved.
   --||============================================================================
   --|| Version  Date       Revised By     Reason for Change
   --|| -------- ---------- -------------- ----------------------------------------
   --|| 5.0      03/14/2013 Roger Roach    Original Version
   --|| 5.10     04/05/2013 Roger Roach    Added session parameter option
   --|| 5.11     09/03/2013 Roger Roach    Fix problem in the GET_DEF_TAX_BASIS function with wrong values being used
   --|| 5.12     09/03/2013 Roger Roach    increate the size of array for type_tax_detail_save_rec from 40 to 240
   --|| 5.13     09/03/2013 Roger Roach    fixed problem on get_def_tax_basis
   --|| 5.14     01/10/2013 Andrew Scott   maint-35392 MLP changes merging tax_control, tax_depreciation,
   --||                                    tax_depr_adjust all into tax_depreciation.
   --||
   --|| PACKAGE RELEASED FOR 10.4.2.0
   --||
   --|| PP Version  Version    Date           Revised By     Reason for Change
   --|| ----------  ---------  -------------  -------------  -------------------------
   --|| 10.4.3.0    5.15       04/28/2014     Andrew Scott   QA discovered a minor logging bug during
   --||                                                      maint-37527 testing.  total row count at
   --||                                                      end does not match the actual number rows
   --||                                                      processed.
   --|| 10.4.3.0    5.16       08/13/2014     Andrew Scott   maint-38887.  turn off auditing.
   --|| 10.4.3.0    5.17       09/03/2014     M Bradley      Save Basis Diff Add Ret Back to the database,
   --||                                                      get tax basis function was saving to wrong hash var.
   --|| 10.4.3.0    5.18       09/19/2014     Andrew Scott   maint-29640.  alloc bonus tax overheads change.
   --||
   --|| PACKAGE RELEASED FOR 10.4.3.0
   --||
   --|| PP Version  Version    Date           Revised By     Reason for Change
   --|| ----------  ---------  -------------  -------------  -------------------------
   --|| 2015.1      5.19       02/06/2015     Daniel Motter  maint-42697 increased size of records arrays
   --|| 2015.1      5.20       02/10/2015     Andrew Scott   maint-42696.  1.  changed to allow tax rate id = 0.  2. better
   --||                                                      error handling of rows in tax_rate_control without rows in
   --||                                                      tax_rate.
   --||
   --|| Version     Date       Revised By     Reason for Change
   --|| --------    ---------- -------------- ----------------------------------------
   --|| 2015.2.0.0  11/30/2015 Andrew Scott   maint-45240.  Update version to be 2015.2.0.0.
   --|| 2016.1.1.0  01/27/2017 Rob Burns      maint-47015.  Short tax year rollforward.
   --|| 2017.4.0.2  01/31/2019 Michael B.     PC-6129 Statutory Trueup
   --||============================================================================

   G_PKG_VERSION varchar(35) := '2018.2.1.0';
   g_limit constant pls_integer := 10000;
   subtype hash_t is varchar2(100);
   g_sep constant varchar2(1) := '-';
   g_start_time                   timestamp;
   g_tax_detail_last_record_id    pls_integer;
   g_tax_detail_last_tax_month    pls_integer;
   g_tax_detail_last_norm_id      pls_integer;
   g_def_tax_basis_last_record_id pls_integer;
   g_def_tax_basis_last_tax_month pls_integer;
   g_def_tax_basis_last_norm_id   pls_integer;
   g_tax_year_count               number(22, 2);
   g_table_norm_count             number(22, 0);
   g_table_vintage_count          number(22, 0);
   g_table_class_count            number(22, 0);
   g_table_company_count          number(22, 0);
   g_version                      number(22, 0);
   g_start_year                   number(22, 2);
   g_end_year                     number(22, 2);
   g_table_vintage_ids            table_list_id_type;
   g_table_class_ids              table_list_id_type;
   g_table_company_ids            table_list_id_type;
   g_table_norm_ids               table_list_id_type;
   type tax_detail_hash_type is table of number(22, 0) index by hash_t;
   g_tax_detail_hash    tax_detail_hash_type;
   g_def_tax_basis_hash tax_detail_hash_type;
   type tax_detail_bals_rec is record(
      id       number(22, 0),
      tax_year number(22, 2));
   type tax_detail_bals_rec_type is table of tax_detail_bals_rec index by pls_integer;
   type tax_detail_bals_table_type is table of tax_detail_bals_rec_type index by hash_t;
   g_tax_detail_bals_hash    tax_detail_bals_table_type;
   g_def_tax_basis_bals_hash tax_detail_bals_table_type;
   type tax_rates_rec is record(
      id      number(22, 0),
      rate_id number(22, 0));
   type tax_rates_rec_type is table of tax_rates_rec index by pls_integer;
   type tax_rates_table_type is table of tax_rates_rec_type index by hash_t;
   g_tax_rates_hash  tax_rates_table_type;
   g_dfit_rates_hash tax_rates_table_type;

   cursor tax_basis_amount_cur is
      select normalization_schema.jurisdiction_id,
             js.tax_book_id,
             dit.tax_record_id tax_record_id,
             dit.tax_year tax_year,
             sum(dit.norm_diff_balance_beg +
                 nvl(deferred_income_tax_transfer.norm_diff_balance_beg, 0)) tax_only_norm_diff_amount,
             sum(decode(basis_amount_activity, 0, 0, -basis_diff_add_ret)) orig_diff,
             sum(tax_book_reconcile.basis_amount_beg +
                 nvl(tax_book_reconcile_transfer.basis_amount_beg, 0)) basis_amount_beg,
             sum(tax_book_reconcile.basis_amount_end) basis_amount_end
        from deferred_income_tax dit,
             normalization_schema,
             jurisdiction js,
             tax_book_reconcile,
             tax_record_control trc,
             tax_include_activity,
             (select tax_record_id,
                     tax_year,
                     tax_month,
                     normalization_id,
                     time_slice_id,
                     sum(def_income_tax_balance_beg) def_income_tax_balance_beg,
                     sum(norm_diff_balance_beg) norm_diff_balance_beg
                from deferred_income_tax_transfer
               group by tax_record_id, tax_year, tax_month, normalization_id, time_slice_id) deferred_income_tax_transfer,
             (select tax_include_id,
                     tax_year,
                     tax_record_id,
                     reconcile_item_id,
                     sum(basis_amount_beg) basis_amount_beg
                from tax_book_reconcile_transfer
               group by tax_include_id, tax_year, tax_record_id, reconcile_item_id) tax_book_reconcile_transfer
       where (dit.normalization_id = normalization_schema.normalization_id)
         and (normalization_schema.reconcile_item_id = tax_book_reconcile.reconcile_item_id)
         and tax_book_reconcile.tax_include_id = tax_include_activity.tax_include_id
         and tax_include_activity.tax_book_id = normalization_schema.norm_from_tax
         and (trc.tax_record_id = dit.tax_record_id)
         and (tax_book_reconcile.tax_year = dit.tax_year)
         and (tax_book_reconcile.tax_record_id = dit.tax_record_id)
         and dit.tax_record_id = deferred_income_tax_transfer.tax_record_id(+)
         and dit.tax_year = deferred_income_tax_transfer.tax_year(+)
         and dit.tax_month = deferred_income_tax_transfer.tax_month(+)
         and dit.normalization_id = deferred_income_tax_transfer.normalization_id(+)
         and dit.time_slice_id = deferred_income_tax_transfer.time_slice_id(+)
         and normalization_schema.jurisdiction_id = js.jurisdiction_id
         and tax_book_reconcile.tax_include_id = tax_book_reconcile_transfer.tax_include_id(+)
         and tax_book_reconcile.tax_year = tax_book_reconcile_transfer.tax_year(+)
         and tax_book_reconcile.tax_record_id = tax_book_reconcile_transfer.tax_record_id(+)
         and tax_book_reconcile.reconcile_item_id =
             tax_book_reconcile_transfer.reconcile_item_id(+)
         and dit.tax_year between g_start_year and g_end_year
         and (normalization_schema.normalization_id in (select * from table(g_table_norm_ids)) or
             g_table_norm_count = 0)
         and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and (normalization_schema.amortization_type_id = -1)
         and trc.version_id = g_version
       group by normalization_schema.jurisdiction_id,
                dit.tax_record_id,
                dit.tax_year,
                js.tax_book_id;
   type type_tax_basis_amount_rec is table of tax_basis_amount_cur%rowtype;
   type tax_basis_amount_struct_rec is record(
      jurisdiction_id           number(22, 0),
      tax_book_id               number(22, 0),
      tax_record_id             number(22, 0),
      tax_year                  number(24, 2),
      tax_only_norm_diff_amount float,
      orig_diff                 float,
      basis_amount_beg          float,
      basis_amount_end          float);
   --type type_tax_basis_amount_rec is table of TAX_BASIS_AMOUNT_STRUCT_REC;
   g_tax_basis_amount_rec type_tax_basis_amount_rec;

   cursor def_income_tax_transfers_cur is
      select sum(dt.norm_diff_balance_beg) norm_diff_balance_beg,
             sum(dt.def_income_tax_balance_beg) def_income_tax_balance_beg,
             dt.tax_record_id,
             dt.tax_year,
             dt.normalization_id
        from deferred_income_tax_transfer dt
       where (dt.normalization_id in (select * from table(g_table_norm_ids)) or
             g_table_norm_count = 0)
         and dt.tax_year between g_start_year and g_end_year
       group by dt.tax_record_id, dt.tax_year, dt.normalization_id;
   type type_def_income_tax_trans_rec is table of def_income_tax_transfers_cur%rowtype;
   type def_income_tax_trans_str_rec is record(
      norm_diff_balance_beg      number(22, 2),
      def_income_tax_balance_beg number(22, 2),
      tax_record_id              number(22, 0),
      tax_year                   number(22, 2),
      normalization_id           number(22, 0));
   --type type_def_income_tax_trans_rec is table of  def_income_tax_trans_str_REC;
   g_def_income_tax_transfers_rec type_def_income_tax_trans_rec;

   cursor tax_detail_cur /*(a_tax_basis_amount_rec type_tax_basis_amount_rec,a_def_income_tax_trans  type_def_income_tax_trans_rec) */
   is
      select tda.accum_reserve - tdb.accum_reserve diff_accum_reserve,
             ns.norm_from_tax,
             ns.norm_to_tax,
             trc.tax_class_id,
             trc.vintage_id,
             to_number(to_char(nvl(trc.in_service_month, to_date('111111', 'YYMMDD')), 'YYYYMMDD')) in_service_month,
             dfi.def_income_tax_balance_beg,
             dfi.def_income_tax_provision,
             dfi.def_income_tax_reversal,
             dfi.aram_rate,
             dfi.def_income_tax_balance_end,
             dfi.norm_diff_balance_beg,
             dfi.norm_diff_balance_end,
             dfi.life,
             dfi.def_income_tax_adjust,
             dfi.def_income_tax_retire,
             dfi.def_income_tax_gain_loss,
             dfi.aram_rate_end,
             tda.depreciation da_depreciation,
             tda.gain_loss da_gain_loss,
             tda.cor_expense da_cor_expense,
             tda.retirements da_retirements,
             tda.extraordinary_retires da_extraordinary_retires,
             tda.capitalized_depr da_capitalized_depr,
             tdb.depreciation db_depreciation,
             tdb.gain_loss db_gain_loss,
             tdb.cor_expense db_cor_expense,
             tdb.capitalized_depr db_capitalized_depr,
             trc.company_id,
             dfi.tax_year,
             ns.jurisdiction_id,
             ns.def_income_tax_rate_id,
             dfi.tax_record_id,
             dfi.tax_month,
             dfi.normalization_id normalization_id,
             dfi.gain_loss_def_tax_balance,
             tda.tax_rate_id,
             v.year vintage_year,
             tda.tax_book_id,
             dfi.gain_loss_def_tax_balance_end,
             dfi.basis_diff_add_ret,
             ns.cost_of_removal_ft,
             ns.cap_depr_ind,
             0 dit_rate,
             decode(tda.tax_balance_end,
                    0,
                    -decode(ns.jurisdiction_id - ttb.jurisdiction_id,
                            0,
                            ttb.tax_only_norm_diff_amount,
                            0) +
                    decode(ns.jurisdiction_id - ttb.jurisdiction_id, 0, ttb.orig_diff, 0),

                    decode(
                          alloc_bonus_option,
                          'yes',

                           - ((((tda.tax_balance_end - nvl(bonus_amount,0) ) - (tda.accum_reserve_end - nvl(bonus_amount,0) )) / (tda.tax_balance_end - nvl(bonus_amount,0) )) *
                            basis_amount_end) - decode(ns.jurisdiction_id - ttb.jurisdiction_id,
                                  0,
                                  ttb.tax_only_norm_diff_amount,
                                  0) +
                           decode(ns.jurisdiction_id - ttb.jurisdiction_id, 0, ttb.orig_diff, 0),

                           - (((tda.tax_balance_end - tda.accum_reserve_end) / tda.tax_balance_end) *
                            basis_amount_end) - decode(ns.jurisdiction_id - ttb.jurisdiction_id,
                                  0,
                                  ttb.tax_only_norm_diff_amount,
                                  0) +
                           decode(ns.jurisdiction_id - ttb.jurisdiction_id, 0, ttb.orig_diff, 0)
                     )

             ) amount_to_amortize,
             nvl(tditt.def_income_tax_balance_beg, 0) def_income_tax_bal_transfer,
             nvl(tditt.norm_diff_balance_beg, 0) norm_diff_balance_transfer,
             tda.tax_balance tax_balance_from,
             tda.accum_reserve accum_reserve_from,
             no_zero_check,
             tda.accum_reserve_adjust accum_reserve_adjust_a,
             tdb.accum_reserve_adjust accum_reserve_adjust_b,
             decode(NORMALIZATION_SCHEMA_TRUEUP.normalization_id, null, 0, 1) statutory_trueup,
             decode(NORMALIZATION_SCHEMA_TRUEUP.trueup_Adjustment, null, 0, NORMALIZATION_SCHEMA_TRUEUP.trueup_Adjustment) trueup_Adjustment
        from normalization_schema ns,
             tax_record_control trc,
             tax_depreciation tda,
             tax_depreciation tdb,
             amortization_type att,
             deferred_rates dr,
             vintage v,
             (select deferred_income_tax.*,
                     normalization_schema.jurisdiction_id,
                     normalization_schema.norm_from_tax,
                     trc2.company_id,
                     trc2.tax_class_id
                from deferred_income_tax, normalization_schema, tax_record_control trc2
               where deferred_income_tax.tax_year between g_start_year and g_end_year
                 and trc2.version_id = g_version
                 and deferred_income_tax.tax_record_id = trc2.tax_record_id
                 and deferred_income_tax.normalization_id = normalization_schema.normalization_id) dfi,
             --table(g_tax_basis_amount_rec) ttb,
             temp_global_tax_basis_amount ttb, -- temp_tax_basis_amount
             --table( g_def_income_tax_transfers_rec) tditt
             temp_g_def_income_tax_trans tditt, -- temp_def_income_tax_transfers
             (
                select
                   tax_book_reconcile.tax_record_id,
                   tax_book_reconcile.tax_year,
                   tax_include_activity.tax_book_id,
                   sum(nvl(tax_book_reconcile.basis_amount_end,0)) bonus_amount
                from
                   tax_book_reconcile,
                   tax_reconcile_item,
                   tax_include_activity
                where tax_book_reconcile.reconcile_item_id = tax_reconcile_item.reconcile_item_id
                and tax_reconcile_item.depr_deduction = 1 /*bonus*/
                and tax_book_reconcile.tax_include_id = tax_include_activity.tax_include_id
                group by
                   tax_book_reconcile.tax_record_id,
                   tax_book_reconcile.tax_year,
                   tax_include_activity.tax_book_id
             ) bonus_view,
             (
                select max(alloc_bonus_option) alloc_bonus_option
                from (
                   select lower(trim(nvl(option_value,pp_default_value))) alloc_bonus_option
                   from ppbase_system_options
                   where lower(system_option_id) = lower('Deferred-Allocate Bonus Depr Tax Recovery to Overheads')
                   union
                   select 'no' from dual /*in case the system option does not exist in the db--it always should*/
                )
             ) sys_option_view,
	           NORMALIZATION_SCHEMA_TRUEUP
       where (ns.norm_from_tax = tda.tax_book_id)
         and ns.norm_to_tax = tdb.tax_book_id
         and ns.amortization_type_id = 1
         and (ns.normalization_id in (select * from table(g_table_norm_ids)) or
             g_table_norm_count = 0)
         and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and tda.tax_year between g_start_year and g_end_year
         and (dfi.jurisdiction_id = ttb.jurisdiction_id(+))
         and (dfi.norm_from_tax = ttb.tax_book_id(+))
         and (dfi.tax_record_id = ttb.tax_record_id(+))
         and dfi.tax_year = ttb.tax_year(+)
         and (dfi.tax_year = tditt.tax_year(+))
         and (dfi.tax_record_id = tditt.tax_record_id(+))
         and (dfi.normalization_id = tditt.normalization_id(+))
         and (ns.norm_to_tax = tdb.tax_book_id)
         and (ns.normalization_id = dfi.normalization_id)
         and (dfi.tax_record_id = trc.tax_record_id)
         and (dfi.tax_record_id = tda.tax_record_id)
         and (dfi.tax_record_id = tdb.tax_record_id)
         and (ns.amortization_type_id = att.amortization_type_id)
         and (ns.def_income_tax_rate_id = dr.def_income_tax_rate_id)
         and (v.vintage_id = trc.vintage_id)
         and (dfi.tax_year = tda.tax_year)
         and (tdb.tax_year = tda.tax_year)
         and dfi.tax_year between g_start_year and g_end_year
         and trc.version_id = g_version
         and tda.tax_record_id = bonus_view.tax_record_id (+)
         and tda.tax_year = bonus_view.tax_year (+)
         and tda.tax_book_id = bonus_view.tax_book_id (+)
        and dfi.normalization_id = NORMALIZATION_SCHEMA_TRUEUP.normalization_id (+)
        and dfi.tax_year=NORMALIZATION_SCHEMA_TRUEUP.tax_Year (+)
        and dfi.tax_Class_id=NORMALIZATION_SCHEMA_TRUEUP.tax_class_id (+)
        and dfi.company_id=NORMALIZATION_SCHEMA_TRUEUP.company_id (+)
       order by trc.tax_record_id, normalization_id, dfi.tax_month, tda.tax_year;
   type type_tax_detail_rec is varray(11000) of tax_detail_cur%rowtype;
   type type_tax_detail_save_rec is varray(1000) of tax_detail_cur%rowtype;
   g_tax_detail_rec          type_tax_detail_rec;
   g_tax_detail_save2_rec    type_tax_detail_save_rec;
   g_tax_detail_save1_rec    type_tax_detail_save_rec;
   g_tax_depr_last_record_id number(22, 0);
   g_tax_depr_last_norm_id   number(22, 0);

   cursor dfit_rates_cur is
      select def_income_tax_rate_id,
             to_number(to_char(effective_date, 'YYYYMMDD')) effective_date,
             monthly_rate,
             annual_rate
        from deferred_income_tax_rates
       order by def_income_tax_rate_id, effective_date asc;
   type type_dfit_rates_rec is table of dfit_rates_cur%rowtype;
   g_dfit_rates_rec type_dfit_rates_rec;
   cursor tax_rates_cur is
      select tr.tax_rate_id,
             rate,
             tr.year,
             net_gross,
             life,
             remaining_life_plan,
             start_method,
             rounding_convention,
             switched_year
        from tax_rates tr, tax_rate_control trc
       where tr.tax_rate_id = trc.tax_rate_id
       order by tr.tax_rate_id, tr.year;
   type type_tax_rates_rec is table of tax_rates_cur%rowtype;
   g_tax_rates_rec type_tax_rates_rec;

   cursor def_tax_basis_cur is
      select td.tax_balance_end,
             td.accum_reserve_end,
             ns.norm_from_tax,
             ns.norm_to_tax,
             trc.tax_class_id,
             trc.vintage_id,
             to_number(to_char(nvl(trc.in_service_month, to_date('111111', 'YYMMDD')), 'YYYYMMDD')),
             dfi.def_income_tax_balance_beg,
             dfi.def_income_tax_provision,
             dfi.def_income_tax_reversal,
             dfi.aram_rate,
             dfi.def_income_tax_balance_end,
             dfi.norm_diff_balance_beg,
             dfi.norm_diff_balance_end,
             dfi.life,
             dfi.def_income_tax_adjust,
             dfi.def_income_tax_retire,
             dfi.def_income_tax_gain_loss,
             dfi.aram_rate_end,
             trc.company_id,
             dfi.tax_year,
             ns.jurisdiction_id,
             ns.def_income_tax_rate_id,
             dfi.tax_record_id,
             dfi.tax_month,
             dfi.normalization_id,
             dfi.gain_loss_def_tax_balance,
             v.year vintage_year,
             tia.tax_book_id,
             dfi.gain_loss_def_tax_balance_end,
             tbr.basis_amount_beg,
             tbr.basis_amount_end,
             tbr.basis_amount_transfer,
             tbr.basis_amount_activity,
             ns.reconcile_item_id,
             dfi.basis_diff_add_ret,
             dfi.input_amortization,
             ns.book_depr_alloc_ind,
             ns.basis_diff_retire_reversal,
             td.number_months_beg,
             1 dit_rate,
             ns.amortization_type_id,
             nvl(tditt.def_income_tax_balance_beg, 0) def_income_tax_bal_transfer,
             nvl(tditt.norm_diff_balance_beg, 0) norm_diff_balance_transfer,
             ns.basis_diff_activity_split,
             nvl(bonus_view.bonus_amount,0) bonus_amount,
             sys_option_view.alloc_bonus_option,
	           decode(NORMALIZATION_SCHEMA_TRUEUP.normalization_id, null, 0, 1) statutory_trueup,
             decode(NORMALIZATION_SCHEMA_TRUEUP.trueup_Adjustment, null, 0, NORMALIZATION_SCHEMA_TRUEUP.trueup_Adjustment) trueup_Adjustment
        from tax_depreciation            td,
             normalization_schema        ns,
             tax_record_control          trc,
             (select deferred_income_tax.*,
                        trc2.company_id,
                        trc2.tax_class_id
                   from deferred_income_tax, tax_record_control trc2
                  where deferred_income_tax.tax_year between g_start_year and g_end_year
                    and trc2.version_id = g_version
                    and deferred_income_tax.tax_record_id = trc2.tax_record_id)         dfi,
             tax_book_reconcile          tbr,
             amortization_type           at,
             tax_include_activity        tia,
             deferred_rates              dr,
             vintage                     v,
             temp_g_def_income_tax_trans tditt, -- temp_def_income_tax_transfers
             (
                select
                   tax_book_reconcile.tax_record_id,
                   tax_book_reconcile.tax_year,
                   tax_include_activity.tax_book_id,
                   sum(nvl(tax_book_reconcile.basis_amount_end,0)) bonus_amount
                from
                   tax_book_reconcile,
                   tax_reconcile_item,
                   tax_include_activity
                where tax_book_reconcile.reconcile_item_id = tax_reconcile_item.reconcile_item_id
                and tax_reconcile_item.depr_deduction = 1 /*bonus*/
                and tax_book_reconcile.tax_include_id = tax_include_activity.tax_include_id
                group by
                   tax_book_reconcile.tax_record_id,
                   tax_book_reconcile.tax_year,
                   tax_include_activity.tax_book_id
             ) bonus_view,
             (
                select max(alloc_bonus_option) alloc_bonus_option
                from (
                   select lower(trim(nvl(option_value,pp_default_value))) alloc_bonus_option
                   from ppbase_system_options
                   where lower(system_option_id) = lower('Deferred-Allocate Bonus Depr Tax Recovery to Overheads')
                   union
                   select 'no' from dual /*in case the system option does not exist in the db--it always should*/
                )
             ) sys_option_view,
             NORMALIZATION_SCHEMA_TRUEUP
       where (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
             g_table_vintage_count = 0)
         and (ns.normalization_id in (select * from table(g_table_norm_ids)) or
             g_table_norm_count = 0)
         and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
             g_table_class_count = 0)
         and (trc.company_id in (select * from table(g_table_company_ids)) or
             g_table_company_count = 0)
         and tbr.tax_year between g_start_year and g_end_year
         and (dfi.normalization_id = tditt.normalization_id(+))
         and (dfi.tax_record_id = tditt.tax_record_id(+))
         and (dfi.tax_year = tditt.tax_year(+))
         and (ns.normalization_id = dfi.normalization_id)
         and (dfi.tax_record_id = trc.tax_record_id)
         and (ns.amortization_type_id = at.amortization_type_id)
         and (ns.def_income_tax_rate_id = dr.def_income_tax_rate_id)
         and (v.vintage_id = trc.vintage_id)
         and (tbr.tax_include_id = tia.tax_include_id)
         and (ns.reconcile_item_id = tbr.reconcile_item_id)
         and (trc.tax_record_id = tbr.tax_record_id)
         and (dfi.tax_year = tbr.tax_year)
         and (td.tax_record_id = dfi.tax_record_id)
         and (td.tax_year = dfi.tax_year)
         and (td.tax_book_id = ns.norm_from_tax)
         and (ns.amortization_type_id <= 0)
         and (ns.norm_from_tax = tia.tax_book_id)
         and trc.version_id = g_version
         and td.tax_record_id = bonus_view.tax_record_id (+)
         and td.tax_year = bonus_view.tax_year (+)
         and td.tax_book_id = bonus_view.tax_book_id (+)
        and dfi.normalization_id = NORMALIZATION_SCHEMA_TRUEUP.normalization_id (+)
        and dfi.tax_year=NORMALIZATION_SCHEMA_TRUEUP.tax_Year (+)
        and dfi.tax_class_id=NORMALIZATION_SCHEMA_TRUEUP.tax_class_id (+)
        and dfi.company_id=NORMALIZATION_SCHEMA_TRUEUP.company_id (+)
       order by td.tax_record_id, dfi.normalization_id, dfi.tax_month, tbr.tax_year;

   type type_def_tax_basis_rec is varray(11000) of def_tax_basis_cur%rowtype;
   type type_def_tax_basis_save_rec is varray(1000) of def_tax_basis_cur%rowtype;
   g_def_tax_basis_rec       type_def_tax_basis_rec;
   g_def_tax_basis_save2_rec type_def_tax_basis_save_rec;
   g_def_tax_basis_save1_rec type_def_tax_basis_save_rec;
   function get_version return varchar2;
   function calc_dfit(a_job_no number) return integer;
   function calc(a_tax_detail_index      pls_integer,
                 a_tax_rates_sub_rec     type_tax_rates_rec,
                 a_tax_rates_rows        pls_integer,
                 a_tax_detail_bals_index pls_integer,
                 a_dfit_rates            type_dfit_rates_rec,
                 a_dfit_rates_rows       pls_integer) return number;
   function basis_calc(a_def_tax_index         pls_integer,
                       a_def_tax_rates_sub_rec type_dfit_rates_rec,
                       a_def_tax_rates_rows    pls_integer,
                       a_def_tax_bals_index    pls_integer,
                       a_def_tax_bals_rows     pls_integer,
                       a_short_months          pls_integer) return pls_integer;
   function update_deferred_income_tax(a_start_row pls_integer,
                                       a_num_rec   pls_integer) return number;
   function update_def_tax_basis(a_start_row pls_integer,
                                 a_num_rec   pls_integer) return number;
   procedure set_session_parameter;
   procedure write_log(a_job_no     number,
                       a_error_type number,
                       a_code       number,
                       a_msg        varchar2);
   g_job_no  number(22, 0);
   g_line_no number(22, 0);
end TAX_DFIT_CALC;

/

CREATE OR REPLACE package body tax_dfit_calc is

   /* function get_basis_amount return tax_basis_amount_cur%rowtype pipelined is
   l_tax_basis_table  type_tax_basis_amount_rec;
   begin

     FETCH tax_basis_amount_cur BULK COLLECT INTO  l_tax_basis_table;
     for i in 1..l_tax_basis_table.count loop
        pipe row(l_tax_basis_table(i));
     end loop;


   end get_basis_amount;*/

   -- =============================================================================
   --  Function GET_VERSION
   -- =============================================================================
   function get_version return varchar2 is
   begin
      return '2017.4.0.2';
   end get_version;

   -- =============================================================================
   --  Function GET_TAX_DETAIL
   -- =============================================================================
   function get_tax_detail(a_start_row out pls_integer) return pls_integer is
      l_code  pls_integer;
      l_sql   varchar(4000);
      l_count pls_integer;
      i       pls_integer;
      k       pls_integer;
      j       pls_integer;
      type tax_basis_amount_struct_rec is record(
         jurisdiction_id           number(22, 0),
         tax_book_id               number(22, 0),
         tax_record_id             number(22, 0),
         tax_year                  number(24, 2),
         tax_only_norm_diff_amount float,
         orig_diff                 float,
         basis_amount_beg          float,
         basis_amount_end          float);
      l_move_row_count              pls_integer;
      l_save_count                  integer;
      l_current_record_id           pls_integer;
      l_current_tax_month           pls_integer;
      l_current_norm_id             pls_integer;
      l_year_count                  pls_integer;
      l_tax_det_last_save_record_id pls_integer;
      l_tax_det_last_save_tax_month pls_integer;
      l_tax_det_last_save_norm_id   pls_integer;

      l_tax_basis_amount_rec type_tax_basis_amount_rec;
      type tax_basis_amount_type is table of tax_basis_amount_struct_rec;
      l_tax_basis_table          tax_basis_amount_type;
      l_def_income_tax_trans_rec type_def_income_tax_trans_rec;
      cursor l_tax_detail_cur(a_tax_basis_amount_rec type_tax_basis_amount_rec,
                              a_def_income_tax_trans type_def_income_tax_trans_rec) is
         select tda.accum_reserve - tdb.accum_reserve diff_accum_reserve,
                ns.norm_from_tax,
                ns.norm_to_tax,
                trc.tax_class_id,
                trc.vintage_id,
                to_number(to_char(nvl(trc.in_service_month, to_date('111111', 'YYMMDD')),
                                  'YYYYMMDD')) in_service_month,
                dfi.def_income_tax_balance_beg,
                dfi.def_income_tax_provision,
                dfi.def_income_tax_reversal,
                dfi.aram_rate,
                dfi.def_income_tax_balance_end,
                dfi.norm_diff_balance_beg,
                dfi.norm_diff_balance_end,
                dfi.life,
                dfi.def_income_tax_adjust,
                dfi.def_income_tax_retire,
                dfi.def_income_tax_gain_loss,
                dfi.aram_rate_end,
                tda.depreciation da_depreciation,
                tda.gain_loss da_gain_loss,
                tda.cor_expense da_cor_expense,
                tda.retirements da_retirements,
                tda.extraordinary_retires da_extraordinary_retires,
                tda.capitalized_depr da_capitalized_depr,
                tdb.depreciation db_depreciation,
                tdb.gain_loss db_gain_loss,
                tdb.cor_expense db_cor_expense,
                tdb.capitalized_depr db_capitalized_depr,
                trc.company_id,
                dfi.tax_year,
                ns.jurisdiction_id,
                ns.def_income_tax_rate_id,
                dfi.tax_record_id,
                dfi.tax_month,
                dfi.normalization_id normalization_id,
                dfi.gain_loss_def_tax_balance,
                tda.tax_rate_id,
                v.year vintage_year,
                tda.tax_book_id,
                dfi.gain_loss_def_tax_balance_end,
                dfi.basis_diff_add_ret,
                ns.cost_of_removal_ft,
                ns.cap_depr_ind,
                0 dit_rate,
                decode(tda.tax_balance_end,
                       0,
                       -decode(ns.jurisdiction_id - ttb.jurisdiction_id,
                               0,
                               ttb.tax_only_norm_diff_amount,
                               0) +
                       decode(ns.jurisdiction_id - ttb.jurisdiction_id, 0, ttb.orig_diff, 0),
                       - (((tda.tax_balance_end - tda.accum_reserve_end) / tda.tax_balance_end) *
                         basis_amount_end) - decode(ns.jurisdiction_id - ttb.jurisdiction_id,
                               0,
                               ttb.tax_only_norm_diff_amount,
                               0) +
                        decode(ns.jurisdiction_id - ttb.jurisdiction_id, 0, ttb.orig_diff, 0)) amount_to_amortize,
                nvl(tditt.def_income_tax_balance_beg, 0) def_income_tax_bal_transfer,
                nvl(tditt.norm_diff_balance_beg, 0) norm_diff_balance_transfer,
                tda.tax_balance tax_balance_from,
                tda.accum_reserve accum_reserve_from,
                no_zero_check,
                tda.accum_reserve_adjust accum_reserve_adjust_a,
                tdb.accum_reserve_adjust accum_reserve_adjust_b,
	           decode(NORMALIZATION_SCHEMA_TRUEUP.normalization_id, null, 0, 1) statutory_trueup,
             decode(NORMALIZATION_SCHEMA_TRUEUP.trueup_Adjustment, null, 0, NORMALIZATION_SCHEMA_TRUEUP.trueup_Adjustment) trueup_Adjustment
           from normalization_schema ns,
                tax_record_control trc,
                tax_depreciation tda,
                tax_depreciation tdb,
                amortization_type att,
                deferred_rates dr,
                vintage v,
                NORMALIZATION_SCHEMA_TRUEUP,
                (select deferred_income_tax.*,
                        normalization_schema.jurisdiction_id,
                        normalization_schema.norm_from_tax,
                        trc2.company_id,
                        trc2.tax_class_id
                   from deferred_income_tax, normalization_schema, tax_record_control trc2
                  where deferred_income_tax.tax_year between g_start_year and g_end_year
                    and trc2.version_id = g_version
                    and deferred_income_tax.tax_record_id = trc2.tax_record_id
                    and deferred_income_tax.normalization_id = normalization_schema.normalization_id) dfi,
                -- table(a_tax_basis_amount_rec) ttb,
                temp_global_tax_basis_amount ttb, -- temp_tax_basis_amount
                -- table(a_def_income_tax_trans) tditt
                temp_g_def_income_tax_trans tditt -- temp_def_income_tax_transfers
          where (ns.norm_from_tax = tda.tax_book_id)
            and ns.norm_to_tax = tdb.tax_book_id
            and ns.amortization_type_id = 1
            and (ns.normalization_id in (select * from table(g_table_norm_ids)) or
                g_table_norm_count = 0)
            and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
                g_table_vintage_count = 0)
            and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
                g_table_class_count = 0)
            and (trc.company_id in (select * from table(g_table_company_ids)) or
                g_table_company_count = 0)
            and tda.tax_year between g_start_year and g_end_year
            and (dfi.jurisdiction_id = ttb.jurisdiction_id(+))
            and (dfi.norm_from_tax = ttb.tax_book_id(+))
            and (dfi.tax_record_id = ttb.tax_record_id(+))
            and dfi.tax_year = ttb.tax_year(+)
            and (dfi.tax_year = tditt.tax_year(+))
            and (dfi.tax_record_id = tditt.tax_record_id(+))
            and (dfi.normalization_id = tditt.normalization_id(+))
            and (ns.norm_to_tax = tdb.tax_book_id)
            and (ns.normalization_id = dfi.normalization_id)
            and (dfi.tax_record_id = trc.tax_record_id)
            and (dfi.tax_record_id = tda.tax_record_id)
            and (dfi.tax_record_id = tdb.tax_record_id)
            and (ns.amortization_type_id = att.amortization_type_id)
            and (ns.def_income_tax_rate_id = dr.def_income_tax_rate_id)
            and (v.vintage_id = trc.vintage_id)
            and (dfi.tax_year = tda.tax_year)
            and (tdb.tax_year = tda.tax_year)
            and dfi.tax_year between g_start_year and g_end_year
            and trc.version_id = g_version
        and dfi.normalization_id = NORMALIZATION_SCHEMA_TRUEUP.normalization_id (+)
        and dfi.tax_year=NORMALIZATION_SCHEMA_TRUEUP.tax_Year (+)
        and dfi.tax_class_id=NORMALIZATION_SCHEMA_TRUEUP.tax_class_id (+)
        and dfi.company_id=NORMALIZATION_SCHEMA_TRUEUP.company_id (+)
          order by tda.tax_year;

   begin

      if not tax_detail_cur%isopen then
         l_sql := 'drop table temp_tax_basis_amount';
         --execute immediate l_sql;
         l_sql := ' create table temp_tax_basis_amount  as SELECT normalization_schema.jurisdiction_id, ' ||
                  '     js.tax_book_id,dit.tax_record_id tax_record_id, dit.tax_year tax_year, ' ||
                  '      sum(dit.norm_diff_balance_beg + ' ||
                  '         nvl(deferred_income_tax_transfer.norm_diff_balance_beg,0) ) tax_only_norm_diff_amount, ' ||
                  '      sum(DECODE(basis_amount_activity, 0, 0, - basis_diff_add_ret)) orig_diff, ' ||
                  '      sum(tax_book_reconcile.basis_amount_beg + ' ||
                  '        nvl(tax_book_reconcile_transfer.basis_amount_beg,0)) basis_amount_beg, ' ||
                  '      sum(tax_book_reconcile.basis_amount_end) basis_amount_end   ' ||
                  '    FROM deferred_income_tax dit, ' || '        normalization_schema , ' ||
                  '        jurisdiction js, ' || '        tax_book_reconcile , ' ||
                  '        tax_record_control trc , ' || '        tax_include_activity, ' ||
                  '        (SELECT tax_record_id, ' || '           tax_year, ' ||
                  '            tax_month, ' || '          normalization_id, ' ||
                  '            time_slice_id, ' ||
                  '          sum(def_income_tax_balance_beg) def_income_tax_balance_beg, ' ||
                  '          sum(norm_diff_balance_beg) norm_diff_balance_beg         ' ||
                  '           FROM deferred_income_tax_transfer        ' ||
                  '            GROUP BY         tax_record_id, ' || '              tax_year, ' ||
                  '              tax_month, ' || '              normalization_id, ' ||
                  '              time_slice_id) deferred_income_tax_transfer, ' ||
                  '      (SELECT tax_include_id, ' || '          tax_year, ' ||
                  '            tax_record_id, ' || '            reconcile_item_id, ' ||
                  '            sum(basis_amount_beg) basis_amount_beg         ' ||
                  '          FROM tax_book_reconcile_transfer         ' ||
                  '                  GROUP BY          tax_include_id, ' ||
                  '                tax_year, ' || '                tax_record_id, ' ||
                  '                reconcile_item_id) tax_book_reconcile_transfer          ' ||
                  '     WHERE 1= 0 ' || ' GROUP BY normalization_schema.jurisdiction_id,  ' ||
                  '       dit.tax_record_id,dit.tax_year,js.tax_book_id ';
         -- execute immediate l_sql;

         insert into temp_global_tax_basis_amount
            select normalization_schema.jurisdiction_id,
                   js.tax_book_id,
                   dit.tax_record_id tax_record_id,
                   dit.tax_year tax_year,
                   sum(dit.norm_diff_balance_beg +
                       nvl(deferred_income_tax_transfer.norm_diff_balance_beg, 0)) tax_only_norm_diff_amount,
                   sum(decode(basis_amount_activity, 0, 0, -basis_diff_add_ret)) orig_diff,
                   sum(tax_book_reconcile.basis_amount_beg +
                       nvl(tax_book_reconcile_transfer.basis_amount_beg, 0)) basis_amount_beg,
                   sum(tax_book_reconcile.basis_amount_end) basis_amount_end
              from deferred_income_tax dit,
                   normalization_schema,
                   jurisdiction js,
                   tax_book_reconcile,
                   tax_record_control trc,
                   tax_include_activity,
                   (select tax_record_id,
                           tax_year,
                           tax_month,
                           normalization_id,
                           time_slice_id,
                           sum(def_income_tax_balance_beg) def_income_tax_balance_beg,
                           sum(norm_diff_balance_beg) norm_diff_balance_beg
                      from deferred_income_tax_transfer
                     group by tax_record_id, tax_year, tax_month, normalization_id, time_slice_id) deferred_income_tax_transfer,
                   (select tax_include_id,
                           tax_year,
                           tax_record_id,
                           reconcile_item_id,
                           sum(basis_amount_beg) basis_amount_beg
                      from tax_book_reconcile_transfer
                     group by tax_include_id, tax_year, tax_record_id, reconcile_item_id) tax_book_reconcile_transfer
             where (dit.normalization_id = normalization_schema.normalization_id)
               and (normalization_schema.reconcile_item_id = tax_book_reconcile.reconcile_item_id)
               and tax_book_reconcile.tax_include_id = tax_include_activity.tax_include_id
               and tax_include_activity.tax_book_id = normalization_schema.norm_from_tax
               and (trc.tax_record_id = dit.tax_record_id)
               and (tax_book_reconcile.tax_year = dit.tax_year)
               and (tax_book_reconcile.tax_record_id = dit.tax_record_id)
               and dit.tax_record_id = deferred_income_tax_transfer.tax_record_id(+)
               and dit.tax_year = deferred_income_tax_transfer.tax_year(+)
               and dit.tax_month = deferred_income_tax_transfer.tax_month(+)
               and dit.normalization_id = deferred_income_tax_transfer.normalization_id(+)
               and dit.time_slice_id = deferred_income_tax_transfer.time_slice_id(+)
               and normalization_schema.jurisdiction_id = js.jurisdiction_id
               and tax_book_reconcile.tax_include_id =
                   tax_book_reconcile_transfer.tax_include_id(+)
               and tax_book_reconcile.tax_year = tax_book_reconcile_transfer.tax_year(+)
               and tax_book_reconcile.tax_record_id = tax_book_reconcile_transfer.tax_record_id(+)
               and tax_book_reconcile.reconcile_item_id =
                   tax_book_reconcile_transfer.reconcile_item_id(+)
               and dit.tax_year between g_start_year and g_end_year
               and (normalization_schema.normalization_id in
                   (select * from table(g_table_norm_ids)) or g_table_norm_count = 0)
               and (trc.vintage_id in (select * from table(g_table_vintage_ids)) or
                   g_table_vintage_count = 0)
               and (trc.tax_class_id in (select * from table(g_table_class_ids)) or
                   g_table_class_count = 0)
               and (trc.company_id in (select * from table(g_table_company_ids)) or
                   g_table_company_count = 0)
               and (normalization_schema.amortization_type_id = -1)
               and trc.version_id = g_version
             group by normalization_schema.jurisdiction_id,
                      dit.tax_record_id,
                      dit.tax_year,
                      js.tax_book_id;

         l_sql := ' create table temp_def_income_tax_transfers   as    SELECT    ' ||
                  '    sum(dt.norm_diff_balance_beg) norm_diff_balance_beg,  ' ||
                  '      sum(dt.def_income_tax_balance_beg) def_income_tax_balance_beg,   ' ||
                  '       dt.tax_record_id,dt.tax_year,dt.normalization_id    ' ||
                  '       from     deferred_income_tax_transfer  dt           ' || '     where   ' ||
                  '        1 = 0 ' ||
                  '     group by dt.tax_record_id,dt.tax_year,dt.normalization_id ';

         l_sql := 'drop table temp_def_income_tax_transfers';
         -- execute immediate l_sql;
         insert into temp_g_def_income_tax_trans
            select sum(dt.norm_diff_balance_beg) norm_diff_balance_beg,
                   sum(dt.def_income_tax_balance_beg) def_income_tax_balance_beg,
                   dt.tax_record_id,
                   dt.tax_year,
                   dt.normalization_id
              from deferred_income_tax_transfer dt
             where (dt.normalization_id in (select * from table(g_table_norm_ids)) or
                   g_table_norm_count = 0)
               and dt.tax_year between g_start_year and g_end_year
             group by dt.tax_record_id, dt.tax_year, dt.normalization_id;

         open tax_basis_amount_cur;
         fetch tax_basis_amount_cur bulk collect
            into g_tax_basis_amount_rec;
         close tax_basis_amount_cur;

         open def_income_tax_transfers_cur;
         fetch def_income_tax_transfers_cur bulk collect
            into g_def_income_tax_transfers_rec;
         close def_income_tax_transfers_cur;

         open tax_detail_cur; --(l_tax_basis_amount_rec,l_def_income_tax_trans_rec);
      else
         g_tax_detail_rec.delete;
      end if;

      a_start_row := 1;
      fetch tax_detail_cur bulk collect
         into g_tax_detail_rec limit g_limit;

      i       := g_tax_detail_rec.first;
      l_count := g_tax_detail_rec.count;

      if l_count = 0 then
         close tax_detail_cur;
         return 0;
      else
         g_tax_detail_last_record_id := g_tax_detail_rec(l_count).tax_record_id;
         g_tax_detail_last_tax_month := g_tax_detail_rec(l_count).tax_month;
         g_tax_detail_last_norm_id   := g_tax_detail_rec(l_count).normalization_id;
      end if;

      -- save the last records that are not complete
      k := 0;
      g_tax_detail_save2_rec.delete;
      if l_count = g_limit then
         j := 1;
         for j in reverse 1 .. l_count
         loop
            if g_tax_detail_last_record_id = g_tax_detail_rec(j).tax_record_id and
               g_tax_detail_last_tax_month = g_tax_detail_rec(j).tax_month and
               g_tax_detail_last_norm_id = g_tax_detail_rec(j).normalization_id then
               g_tax_detail_save2_rec.extend;
               k := k + 1;
               g_tax_detail_save2_rec(k) := g_tax_detail_rec(j);
            else
               exit;
            end if;
         end loop;
         l_count := l_count - k;
      else
         g_tax_detail_last_record_id := 0;
         g_tax_detail_last_tax_month := 0;
         g_tax_detail_last_norm_id   := 0;
      end if;

      -- restore the save records
      j := 0;

      if g_tax_detail_save1_rec.count > 0 then
         l_save_count := g_tax_detail_save1_rec.count;
         for j in 1 .. l_save_count
         loop
            g_tax_detail_rec.extend;
            g_tax_detail_rec(l_count + j) := g_tax_detail_save1_rec(j);
            k := k + 1;
         end loop;
         -- we must reorganize the collection
         l_tax_det_last_save_record_id := g_tax_detail_save1_rec(1).tax_record_id;
         l_tax_det_last_save_tax_month := g_tax_detail_save1_rec(1).tax_month;
         l_tax_det_last_save_norm_id   := g_tax_detail_save1_rec(1).normalization_id;
         l_move_row_count              := 0;
         for j in 1 .. l_count
         loop
            if l_tax_det_last_save_record_id = g_tax_detail_rec(j).tax_record_id and
               l_tax_det_last_save_tax_month = g_tax_detail_rec(j).tax_month and
               l_tax_det_last_save_norm_id = g_tax_detail_rec(j).normalization_id then
               l_move_row_count := l_move_row_count + 1;
            else
               exit;
            end if;
         end loop;

         l_count := l_count + l_save_count;
         for j in 1 .. l_move_row_count
         loop
            g_tax_detail_rec.extend;
            g_tax_detail_rec(l_count + j) := g_tax_detail_rec(j);
         end loop;

         l_count     := l_count + l_move_row_count;
         a_start_row := l_move_row_count + 1;

      end if;

      g_tax_detail_save1_rec := g_tax_detail_save2_rec;

      -- create an index to the data
      g_tax_detail_bals_hash.delete;
      g_tax_detail_hash.delete;
      i                   := a_start_row;
      l_current_record_id := 0;
      l_current_tax_month := 0;
      l_current_norm_id   := 0;
      while i <= l_count
      loop

         g_tax_detail_hash(to_char(g_tax_detail_rec(i).tax_record_id) || g_sep || to_char(g_tax_detail_rec(i).tax_month) || g_sep || to_char(g_tax_detail_rec(i).normalization_id) || g_sep || to_char(g_tax_detail_rec(i).tax_year)) := i;
         if l_current_record_id = g_tax_detail_rec(i).tax_record_id and
            l_current_norm_id = g_tax_detail_rec(i).normalization_id and
            l_current_tax_month = g_tax_detail_rec(i).tax_month then
            l_year_count := l_year_count + 1;
         else
            l_year_count        := 1;
            l_current_record_id := g_tax_detail_rec(i).tax_record_id;
            l_current_tax_month := g_tax_detail_rec(i).tax_month;
            l_current_norm_id   := g_tax_detail_rec(i).normalization_id;
            if l_current_record_id = 117152 then
               j := 0;
            end if;
         end if;

         g_tax_detail_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_tax_month) || g_sep || to_char(l_current_norm_id))(l_year_count).tax_year := g_tax_detail_rec(i)
                                                                                                                                                                        .tax_year;
         g_tax_detail_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_tax_month) || g_sep || to_char(l_current_norm_id))(l_year_count).id := i;
         i := i + 1;
      end loop;

      -- g_tax_depr_rec.delete;
      write_log(g_job_no, 1, 0, 'Tax Detail Rows: ' || to_char(l_count - a_start_row + 1));
      return l_count;

   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   ' Tax Detail Amount ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end get_tax_detail;

   -- =============================================================================
   --  Function GET_TAX_RATES
   -- =============================================================================
   function get_tax_rates return pls_integer is

      l_count        integer;
      i              pls_integer;
      code           pls_integer;
      l_last_rate_id pls_integer;
      l_rows         pls_integer;

   begin
      if not tax_rates_cur%isopen then
         open tax_rates_cur;
      end if;

      fetch tax_rates_cur bulk collect
         into g_tax_rates_rec;

      i       := g_tax_rates_rec.first;
      l_count := g_tax_rates_rec.count;

      close tax_rates_cur;

      -- create an index to the data
      l_count        := g_tax_rates_rec.count;
      i              := 1;
      l_last_rate_id := 0;
      while i <= l_count
      loop

         if g_tax_rates_rec(i).tax_rate_id = l_last_rate_id and i <> 1 then
            l_rows := l_rows + 1;
            g_tax_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         else
            l_rows := 1;
            l_last_rate_id := g_tax_rates_rec(i).tax_rate_id;
            g_tax_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         end if;
         i := i + 1;
      end loop;

      write_log(g_job_no, 1, 0, 'Tax Rates Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;

   end get_tax_rates;

   -- =============================================================================
   --  Function GET_DFIT_RATES
   -- =============================================================================
   function get_dfit_rates return pls_integer is

      l_count        integer;
      i              pls_integer;
      code           pls_integer;
      l_last_rate_id pls_integer;
      l_rows         pls_integer;

   begin
      if not dfit_rates_cur%isopen then
         open dfit_rates_cur;
      end if;

      fetch dfit_rates_cur bulk collect
         into g_dfit_rates_rec;

      i       := g_dfit_rates_rec.first;
      l_count := g_dfit_rates_rec.count;

      close dfit_rates_cur;

      -- create an index to the data
      l_count        := g_dfit_rates_rec.count;
      i              := 1;
      l_last_rate_id := -1;
      while i <= l_count
      loop
         if g_dfit_rates_rec(i).def_income_tax_rate_id = l_last_rate_id then
            l_rows := l_rows + 1;
            g_dfit_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         else
            l_rows := 1;
            l_last_rate_id := g_dfit_rates_rec(i).def_income_tax_rate_id;
            g_dfit_rates_hash(to_char(l_last_rate_id))(l_rows).id := i;
         end if;
         i := i + 1;
      end loop;
      write_log(g_job_no, 1, 0, 'Dfit Rates Rows: ' || to_char(l_count));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no,
                   4,
                   code,
                   'Dfit Rates ' || sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;

   end get_dfit_rates;

   -- =============================================================================
   --  Function GET_DEF_TAX_BASIS
   -- =============================================================================
   function get_def_tax_basis(a_start_row out pls_integer) return pls_integer is
      l_count                        integer;
      code                           pls_integer;
      l_rows                         pls_integer;
      i                              pls_integer;
      k                              pls_integer;
      j                              pls_integer;
      l_save_count                   integer;
      l_current_record_id            pls_integer;
      l_current_tax_month            pls_integer;
      l_current_norm_id              pls_integer;
      l_year_count                   pls_integer;
      l_move_row_count               pls_integer;
      l_def_tax_basis_save_record_id pls_integer;
      l_def_tax_basis_save_tax_month pls_integer;
      l_def_tax_basis_save_norm_id   pls_integer;

   begin
      if not def_tax_basis_cur%isopen then
         open def_tax_basis_cur;
      end if;
      a_start_row := 1;
      fetch def_tax_basis_cur bulk collect
         into g_def_tax_basis_rec limit g_limit;

      i       := g_def_tax_basis_rec.first;
      l_count := g_def_tax_basis_rec.count;

      -- create an index to the data

      if l_count = 0 then
         close def_tax_basis_cur;
         return 0;
      else
         g_def_tax_basis_last_record_id := g_def_tax_basis_rec(l_count).tax_record_id;
         g_def_tax_basis_last_tax_month := g_def_tax_basis_rec(l_count).tax_month;
         g_def_tax_basis_last_norm_id   := g_def_tax_basis_rec(l_count).normalization_id;
      end if;

      -- save the last records that are not complete
      k := 0;
      g_def_tax_basis_save2_rec.delete;
      if l_count = g_limit then
         j := 1;
         for j in reverse 1 .. l_count
         loop
            if g_def_tax_basis_last_record_id = g_def_tax_basis_rec(j).tax_record_id and
               g_def_tax_basis_last_norm_id = g_def_tax_basis_rec(j).normalization_id and
               g_def_tax_basis_last_tax_month = g_def_tax_basis_rec(j).tax_month then
               g_def_tax_basis_save2_rec.extend;
               k := k + 1;
               g_def_tax_basis_save2_rec(k) := g_def_tax_basis_rec(j);
            else
               exit;
            end if;
         end loop;
         l_count := l_count - k;
      else
         g_def_tax_basis_last_record_id := 0;
         g_def_tax_basis_last_norm_id   := 0;
         g_def_tax_basis_last_tax_month := 0;
      end if;

      -- restore the save records
      j := 0;

      if g_def_tax_basis_save1_rec.count > 0 then
         l_save_count := g_def_tax_basis_save1_rec.count;
         k            := 0;
         for j in 1 .. l_save_count
         loop
            g_def_tax_basis_rec.extend;
            g_def_tax_basis_rec(l_count + j) := g_def_tax_basis_save1_rec(j);
            k := k + 1;
         end loop;

         -- we must reorganize the collection
         l_def_tax_basis_save_record_id := g_def_tax_basis_rec(1).tax_record_id;
         l_def_tax_basis_save_tax_month := g_def_tax_basis_rec(1).tax_month;
         l_def_tax_basis_save_norm_id   := g_def_tax_basis_rec(1).normalization_id;
         l_move_row_count               := 0;
         for j in 1 .. l_count
         loop
            if l_def_tax_basis_save_record_id = g_def_tax_basis_rec(j).tax_record_id and
               l_def_tax_basis_save_tax_month = g_def_tax_basis_rec(j).tax_month and
               l_def_tax_basis_save_norm_id = g_def_tax_basis_rec(j).normalization_id then
               l_move_row_count := l_move_row_count + 1;
            else
               exit;
            end if;
         end loop;
         l_count := l_count + l_save_count;
         for j in 1 .. l_move_row_count
         loop
            g_def_tax_basis_rec.extend;
            g_def_tax_basis_rec(l_count + j) := g_def_tax_basis_rec(j);
         end loop;
         l_count     := l_count + l_move_row_count;
         a_start_row := l_move_row_count + 1;
      end if;

      g_def_tax_basis_save1_rec := g_def_tax_basis_save2_rec;

      -- create an index to the data

      i                   := a_start_row;
      l_current_record_id := 0;
      l_current_tax_month := 0;
      l_current_norm_id   := 0;
      while i <= l_count
      loop
         if g_def_tax_basis_last_record_id = g_def_tax_basis_rec(i).tax_record_id and g_def_tax_basis_rec(i)
           .tax_month = g_def_tax_basis_last_tax_month and
            g_def_tax_basis_last_norm_id = g_def_tax_basis_rec(i).normalization_id then
            i := i + 1;
            continue;
         end if;

         g_def_tax_basis_hash(to_char(g_def_tax_basis_rec(i).tax_record_id) || g_sep || to_char(g_def_tax_basis_rec(i).tax_month) || g_sep || to_char(g_def_tax_basis_rec(i).normalization_id) || g_sep || to_char(g_def_tax_basis_rec(i).tax_year)) := i;
         if l_current_record_id = g_def_tax_basis_rec(i).tax_record_id and
            l_current_tax_month = g_def_tax_basis_rec(i).tax_month and
            l_current_norm_id = g_def_tax_basis_rec(i).normalization_id then
            l_year_count := l_year_count + 1;
            g_def_tax_basis_bals_hash(to_char(g_def_tax_basis_rec(i).tax_record_id) || g_sep || to_char(g_def_tax_basis_rec(i).tax_month) || g_sep || to_char(g_def_tax_basis_rec(i).normalization_id))(l_year_count).tax_year := g_def_tax_basis_rec(i)
                                                                                                                                                                                                                               .tax_year;
            g_def_tax_basis_bals_hash(to_char(g_def_tax_basis_rec(i).tax_record_id) || g_sep || to_char(g_def_tax_basis_rec(i).tax_month) || g_sep || to_char(g_def_tax_basis_rec(i).normalization_id))(l_year_count).id := i;

         else
            l_year_count        := 1;
            l_current_record_id := g_def_tax_basis_rec(i).tax_record_id;
            l_current_tax_month := g_def_tax_basis_rec(i).tax_month;
            l_current_norm_id   := g_def_tax_basis_rec(i).normalization_id;
            if l_current_record_id = 117152 then
               j := 0;
            end if;
            g_def_tax_basis_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_tax_month) || g_sep || to_char(l_current_norm_id))(l_year_count).tax_year := g_def_tax_basis_rec(i)
                                                                                                                                                                              .tax_year;
            g_def_tax_basis_bals_hash(to_char(l_current_record_id) || g_sep || to_char(l_current_tax_month) || g_sep || to_char(l_current_norm_id))(l_year_count).id := i;

         end if;
         i := i + 1;
      end loop;

      write_log(g_job_no, 1, 0, 'Def Tax Basis Rows: ' || to_char(l_count - a_start_row + 1));
      return l_count;
   exception
      when others then
         code := sqlcode;
         write_log(g_job_no,
                   4,
                   code,
                   'Def Tax Basis ' || sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;

   end get_def_tax_basis;

   -- =============================================================================
   --  Function FILTER_TAX_DETAIL_BALS
   -- =============================================================================
   function filter_tax_detail_bals(a_tax_record_id pls_integer,
                                   a_tax_month     pls_integer,
                                   a_norm_id       pls_integer,
                                   a_tax_year      number) return pls_integer is
      i                       pls_integer;
      l_code                  pls_integer;
      l_count                 pls_integer;
      l_row_count             pls_integer;
      l_low_year              pls_integer;
      l_tax_year              number;
      l_year_count            pls_integer;
      l_current_year          number;
      l_tax_detail_bals_index pls_integer;
      l_found                 boolean;

   begin
      l_tax_detail_bals_index := 0;
      l_current_year          := 0;
      begin
         l_found := g_tax_detail_bals_hash(to_char(a_tax_record_id) || g_sep || to_char(a_tax_month) || g_sep || to_char(a_norm_id))
                    .exists(1);
      exception
         when no_data_found then
            return 0;
         when others then
            l_code := sqlcode;
            write_log(g_job_no,
                      4,
                      l_code,
                      ' filter_tax_detail_bals record id =' || to_char(a_tax_record_id) || '' ||
                      sqlerrm(l_code) || ' ' || dbms_utility.format_error_backtrace);
            return 0;
      end;
      if g_tax_detail_bals_hash(to_char(a_tax_record_id) || g_sep || to_char(a_tax_month) || g_sep || to_char(a_norm_id))
       .exists(1) then
         l_year_count := g_tax_detail_bals_hash(to_char(a_tax_record_id) || g_sep || to_char(a_tax_month) || g_sep || to_char(a_norm_id))
                         .count;
         for k in 1 .. l_year_count
         loop
            l_tax_year := g_tax_detail_bals_hash(to_char(a_tax_record_id) || g_sep ||
                                                 to_char(a_tax_month) || g_sep ||
                                                 to_char(a_norm_id))(k).tax_year;
            if (l_tax_year > a_tax_year and (l_tax_year < l_current_year or l_current_year = 0)) then
               l_current_year          := l_tax_year;
               l_tax_detail_bals_index := g_tax_detail_bals_hash(to_char(a_tax_record_id) || g_sep ||
                                                                 to_char(a_tax_month) || g_sep ||
                                                                 to_char(a_norm_id))(k).id;
            end if;
         end loop;
      end if;
      i := 0;
      return l_tax_detail_bals_index;

   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_detail_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_detail_bals;

   -- =============================================================================
   --  Function FILTER_TAX_RATES
   -- =============================================================================
   function filter_tax_rates(a_tax_rates_sub_rec in out nocopy type_tax_rates_rec,
                             a_rate_id           pls_integer) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_index pls_integer;
      l_code  pls_integer;

   begin

      l_count := g_tax_rates_rec.count;
      if a_tax_rates_sub_rec.exists(1) then
         a_tax_rates_sub_rec.delete;
      end if;
      l_row := 0;

      begin
         l_count := g_tax_rates_hash(a_rate_id).count;
      exception
         when no_data_found then
            WRITE_LOG(G_JOB_NO, 1, 0, 'Error!  No tax rates found for tax rate id = '||a_rate_id||' ! Exiting...');
            return - 1;
      end;

      for i in 1 .. l_count
      loop
         l_index := g_tax_rates_hash(a_rate_id)(i).id;
         a_tax_rates_sub_rec.extend(1);
         a_tax_rates_sub_rec(i) := g_tax_rates_rec(l_index);
      end loop;
      return l_count;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_tax_rates ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_tax_rates;

   -- =============================================================================
   --  Function FILTER_DFIT_RATES
   -- =============================================================================
   function filter_dfit_rates(a_dfit_rates_sub_rec in out nocopy type_dfit_rates_rec,
                              a_rate_id            pls_integer) return pls_integer is
      i       pls_integer;
      l_count integer;
      l_row   integer;
      l_index pls_integer;
      l_code  pls_integer;

   begin

      l_count := g_dfit_rates_rec.count;
      if a_dfit_rates_sub_rec.exists(1) then
         a_dfit_rates_sub_rec.delete;
      end if;
      l_row := 0;
      begin
          l_count := g_dfit_rates_hash(to_char(a_rate_id)).count;
      exception
           when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Filter Dfit Rates Rate id = ' || to_char(a_rate_id) || '  ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
          return 0;
       end;
      for i in 1 .. l_count
      loop
         l_index := g_dfit_rates_hash(to_char(a_rate_id))(i).id;
         a_dfit_rates_sub_rec.extend(1);
         a_dfit_rates_sub_rec(i) := g_dfit_rates_rec(l_index);
      end loop;
      return l_count;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Filter Dfit Rates ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_dfit_rates;

   -- =============================================================================
   --  Function FILTER_DEF_TAX_BASIS_BALS
   -- =============================================================================
   function filter_def_tax_basis_bals(a_tax_record_id pls_integer,
                                      a_tax_month     pls_integer,
                                      a_norm_id       pls_integer,
                                      a_tax_year      number) return pls_integer is
      i                          pls_integer;
      l_code                     pls_integer;
      l_count                    pls_integer;
      l_row_count                pls_integer;
      l_low_year                 pls_integer;
      l_tax_year                 number;
      l_year_count               pls_integer;
      l_current_year             number;
      l_def_tax_basis_bals_index pls_integer;
      l_found                    boolean;

   begin
      l_def_tax_basis_bals_index := 0;
      l_current_year             := 0;
      begin
         l_found := g_def_tax_basis_bals_hash(to_char(a_tax_record_id) || g_sep || to_char(a_tax_month) || g_sep || to_char(a_norm_id))
                    .exists(1);
      exception
         when no_data_found then
            return 0;
         when others then
            l_code := sqlcode;
            write_log(g_job_no,
                      4,
                      l_code,
                      ' filter_def_tax_basis_bals record id =' || to_char(a_tax_record_id) || '' ||
                      sqlerrm(l_code) || ' ' || dbms_utility.format_error_backtrace);
            return 0;
      end;
      if l_found then
         l_year_count := g_def_tax_basis_bals_hash(to_char(a_tax_record_id) || g_sep || to_char(a_tax_month) || g_sep || to_char(a_norm_id))
                         .count;
         for k in 1 .. l_year_count
         loop
            l_tax_year := g_def_tax_basis_bals_hash(to_char(a_tax_record_id) || g_sep ||
                                                    to_char(a_tax_month) || g_sep ||
                                                    to_char(a_norm_id))(k).tax_year;
            if (l_tax_year > a_tax_year and (l_tax_year < l_current_year or l_current_year = 0)) then
               l_current_year             := l_tax_year;
               l_def_tax_basis_bals_index := g_def_tax_basis_bals_hash(to_char(a_tax_record_id) ||
                                                                       g_sep ||
                                                                       to_char(a_tax_month) ||
                                                                       g_sep || to_char(a_norm_id))(k).id;
            end if;
         end loop;
      end if;
      i := 0;
      return l_def_tax_basis_bals_index;

   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'filter_def_tax_basis_bals ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end filter_def_tax_basis_bals;

   -- =============================================================================
   --  Function CALC_DFIT
   -- =============================================================================
   function calc_dfit(a_job_no number) return integer is
      cursor tax_job_params_cur is
         select version, tax_year, vintage, tax_book_id, tax_class_id, company_id
           from tax_job_params
          where job_no = a_job_no;
      l_version            number(22, 0);
      l_tax_year           number(22, 2);
      l_vintage            number(22, 0);
      l_norm_id            number(22, 0);
      l_tax_month          pls_integer;
      l_tax_record_id      number(22, 0);
      l_tax_class_id       number(22, 0);
      l_company_id         number(22, 0);
      l_dfit_rates_rows    pls_integer;
      l_tax_rates_rows     pls_integer;
      l_tax_rates_sub_rows pls_integer;
      l_tax_detail_rows    pls_integer;
      l_def_tax_basis_rows pls_integer;
      type tax_years_type is table of number(22, 2) index by pls_integer;
      tax_years      tax_years_type;
      tax_year_count number(22, 2);
      l_current_year number;
      code           integer;
      rows           pls_integer;
      l_short_months pls_integer;
      l_next_year    number(22, 2);
      l_year         number(22, 2);
      l_year_count   pls_integer;
      k              pls_integer;

      l_code                  pls_integer;
      m                       pls_integer;
      l_tax_rates_sub_rec     type_tax_rates_rec;
      l_tax_detail_bals_index pls_integer;
      l_dfit_rates_sub_rec    type_dfit_rates_rec;
      l_dfit_rates_sub_rows   pls_integer;

      l_def_tax_basis_bals_index pls_integer;
      l_start_row                pls_integer;
      l_total_rows               pls_integer;
      rtn_code                   number(22,0);

   begin
      g_start_time := current_timestamp;
      --pp_plsql_debug.debug_procedure_on('fast_tax');

      g_job_no  := a_job_no;
      g_line_no := 1;
      dbms_output.enable(100000);
      write_log(g_job_no, 0, 0, 'Tax Deferred  Started Version=' || get_version());
      set_session_parameter();

      rtn_code := AUDIT_TABLE_PKG.setcontext('audit','no');

      l_tax_rates_sub_rec  := type_tax_rates_rec();
      l_dfit_rates_sub_rec := type_dfit_rates_rec();

      g_table_norm_ids       := table_list_id_type(-1);
      g_table_vintage_ids    := table_list_id_type();
      g_table_class_ids      := table_list_id_type(-1);
      g_table_company_ids    := table_list_id_type(-1);
      g_tax_detail_save1_rec := type_tax_detail_save_rec();
      g_tax_detail_save2_rec := type_tax_detail_save_rec();

      g_def_tax_basis_save1_rec := type_def_tax_basis_save_rec();
      g_def_tax_basis_save2_rec := type_def_tax_basis_save_rec();

      tax_year_count        := 0;
      g_table_norm_count    := 0;
      g_table_vintage_count := 0;
      g_table_class_count   := 0;
      g_table_company_count := 0;

      open tax_job_params_cur;
      loop
         fetch tax_job_params_cur
            into l_version, l_tax_year, l_vintage, l_norm_id, l_tax_class_id, l_company_id;
         if not l_version is null then
            g_version := l_version;
         end if;
         if not l_tax_year is null then
            tax_year_count := tax_year_count + 1;
            tax_years(tax_year_count) := l_tax_year;
         end if;
         if not l_vintage is null then
            g_table_vintage_count := 1 + g_table_vintage_count;
            g_table_vintage_ids.extend;
            g_table_vintage_ids(g_table_vintage_count) := l_vintage;
         end if;
         if not l_norm_id is null then
            g_table_norm_count := 1 + g_table_norm_count;
            g_table_norm_ids.extend;
            g_table_norm_ids(g_table_norm_count) := l_norm_id;
         end if;
         if not l_tax_class_id is null then
            g_table_class_count := 1 + g_table_class_count;
            g_table_class_ids.extend;
            g_table_class_ids(g_table_class_count) := l_tax_class_id;
         end if;
         if not l_company_id is null then
            g_table_company_count := 1 + g_table_company_count;
            g_table_company_ids.extend;
            g_table_company_ids(g_table_company_count) := l_company_id;
         end if;
         exit when tax_job_params_cur%notfound;
      end loop;

      g_start_year := tax_years(tax_years.first);
      g_end_year   := tax_years(tax_years.last);

      l_dfit_rates_rows := get_dfit_rates();
      l_tax_rates_rows  := get_tax_rates();
      l_total_rows      := 0;

      loop
         l_tax_detail_rows := get_tax_detail(l_start_row);
         exit when l_tax_detail_rows = 0;
         l_total_rows := l_total_rows + ( l_tax_detail_rows - l_start_row + 1);
         for l_tax_detail_index in l_start_row .. l_tax_detail_rows
         loop
            l_tax_record_id := g_tax_detail_rec(l_tax_detail_index).tax_record_id;
            l_tax_month     := g_tax_detail_rec(l_tax_detail_index).tax_month;
            l_norm_id       := g_tax_detail_rec(l_tax_detail_index).normalization_id;
            l_tax_year      := g_tax_detail_rec(l_tax_detail_index).tax_year;

            l_tax_detail_bals_index := filter_tax_detail_bals(l_tax_record_id,
                                                              l_tax_month,
                                                              l_norm_id,
                                                              l_tax_year);

            l_tax_rates_sub_rows := filter_tax_rates(l_tax_rates_sub_rec,
                                                     g_tax_detail_rec(l_tax_detail_index).tax_rate_id);

            ----filter tax rates function was enhanced to exit if there were no rates to use
            ----bad config, but force the exit if encountered, don't continue on.
            ----error message for this situation is in the filter tax rates function.
            if l_tax_rates_sub_rows < 0 then
               return -1;
            end if;

            l_dfit_rates_sub_rows := filter_dfit_rates(l_dfit_rates_sub_rec,
                                                       g_tax_detail_rec(l_tax_detail_index)
                                                       .def_income_tax_rate_id);

            l_code := calc(l_tax_detail_index,
                           l_tax_rates_sub_rec,
                           l_tax_rates_sub_rows,
                           l_tax_detail_bals_index,
                           l_dfit_rates_sub_rec,
                           l_dfit_rates_sub_rows);
         end loop;
         l_code := update_deferred_income_tax(l_start_row, l_tax_detail_rows);
      end loop;

      l_current_year := 0;

      loop
         l_def_tax_basis_rows := get_def_tax_basis(l_start_row);
         exit when l_def_tax_basis_rows = 0;
         l_total_rows := l_total_rows + ( l_def_tax_basis_rows - l_start_row + 1);
         --*  Start Calculating BASIS Amount Deferred Income Tax

         for l_def_tax_basis_index in l_start_row .. l_def_tax_basis_rows
         loop
            l_tax_record_id := g_def_tax_basis_rec(l_def_tax_basis_index).tax_record_id;
            l_tax_month     := g_def_tax_basis_rec(l_def_tax_basis_index).tax_month;
            l_norm_id       := g_def_tax_basis_rec(l_def_tax_basis_index).normalization_id;
            l_tax_year      := g_def_tax_basis_rec(l_def_tax_basis_index).tax_year;

            l_def_tax_basis_bals_index := filter_def_tax_basis_bals(l_tax_record_id,
                                                                    l_tax_month,
                                                                    l_norm_id,
                                                                    l_tax_year);

            l_dfit_rates_sub_rows := filter_dfit_rates(l_dfit_rates_sub_rec,
                                      g_def_tax_basis_rec(l_def_tax_basis_index).def_income_tax_rate_id);

            if (l_current_year != g_def_tax_basis_rec(l_def_tax_basis_index).tax_year) then
               l_current_year := g_def_tax_basis_rec(l_def_tax_basis_index).tax_year;
               select nvl(months, 0)
                 into l_short_months
                 from tax_year_version
                where version_id = g_version
                  and tax_year = l_current_year;
               if (l_short_months <= 0 or l_short_months >= 12) then
                  l_short_months := 12;
               end if;
            end if;
            l_code := basis_calc(l_def_tax_basis_index,
                                 l_dfit_rates_sub_rec,
                                 l_dfit_rates_sub_rows,
                                 l_def_tax_basis_bals_index,
                                 l_def_tax_basis_bals_index,
                                 l_short_months);

         end loop;
         l_code := update_def_tax_basis(l_start_row, l_def_tax_basis_rows);
      end loop;

      write_log(g_job_no, 0, 0, 'Total Calculation Rows=' || to_char(l_total_rows));
      commit;
      rtn_code := AUDIT_TABLE_PKG.setcontext('audit','yes');
      write_log(g_job_no, 0, 0, 'Finished');
      return 0;

   exception
      when others then
         code := sqlcode;
         write_log(g_job_no, 4, code, sqlerrm(code) || ' ' || dbms_utility.format_error_backtrace);
         return 0;
   end calc_dfit;

   -- =============================================================================
   --  Function UPDATE_DEFERRED_INCOME_TAX
   -- =============================================================================
   function update_deferred_income_tax(a_start_row pls_integer,
                                       a_num_rec   pls_integer) return number as
      i      pls_integer;
      l_code pls_integer;

   begin
      forall i in a_start_row .. a_num_rec
         update deferred_income_tax
            set def_income_tax_balance_end = g_tax_detail_rec(i).def_income_tax_balance_end,
                norm_diff_balance_end = g_tax_detail_rec(i).norm_diff_balance_end,
                def_income_tax_provision = g_tax_detail_rec(i).def_income_tax_provision,
                def_income_tax_reversal = g_tax_detail_rec(i).def_income_tax_reversal,
                def_income_tax_retire = g_tax_detail_rec(i).def_income_tax_retire,
                def_income_tax_gain_loss = g_tax_detail_rec(i).def_income_tax_gain_loss,
                aram_rate_end = g_tax_detail_rec(i).aram_rate_end,
                def_income_tax_balance_beg = g_tax_detail_rec(i).def_income_tax_balance_beg,
                norm_diff_balance_beg = g_tax_detail_rec(i).norm_diff_balance_beg,
                aram_rate = g_tax_detail_rec(i).aram_rate,
                def_income_tax_adjust = g_tax_detail_rec(i).def_income_tax_adjust
          where tax_record_id = g_tax_detail_rec(i).tax_record_id
            and tax_month = g_tax_detail_rec(i).tax_month
            and tax_year = g_tax_detail_rec(i).tax_year
            and normalization_id = g_tax_detail_rec(i).normalization_id;

      write_log(g_job_no, 3, 0, 'Update Deferred Income Tax Rows=' || to_char(a_num_rec - a_start_row + 1));
      return 0;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Update  Deferred Income Tax ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end update_deferred_income_tax;

   -- =============================================================================
   --  Function UPDATE_DEF_TAX_BASIS
   -- =============================================================================
   function update_def_tax_basis(a_start_row pls_integer,
                                 a_num_rec   pls_integer) return number as
      i      pls_integer;
      l_code pls_integer;
   begin
      forall i in a_start_row .. a_num_rec
         update deferred_income_tax
            set def_income_tax_balance_end = g_def_tax_basis_rec(i).def_income_tax_balance_end,
                norm_diff_balance_end = g_def_tax_basis_rec(i).norm_diff_balance_end,
                def_income_tax_provision = g_def_tax_basis_rec(i).def_income_tax_provision,
                def_income_tax_reversal = g_def_tax_basis_rec(i).def_income_tax_reversal,
                def_income_tax_retire = g_def_tax_basis_rec(i).def_income_tax_retire,
                def_income_tax_gain_loss = g_def_tax_basis_rec(i).def_income_tax_gain_loss,
                aram_rate_end = g_def_tax_basis_rec(i).aram_rate_end,
                def_income_tax_balance_beg = g_def_tax_basis_rec(i).def_income_tax_balance_beg,
                norm_diff_balance_beg = g_def_tax_basis_rec(i).norm_diff_balance_beg,
                aram_rate = g_def_tax_basis_rec(i).aram_rate,
                basis_diff_add_ret = g_def_tax_basis_rec(i).basis_diff_add_ret,
                def_income_tax_adjust = g_def_tax_basis_rec(i).def_income_tax_adjust
          where tax_record_id = g_def_tax_basis_rec(i).tax_record_id
            and tax_month = g_def_tax_basis_rec(i).tax_month
            and tax_year = g_def_tax_basis_rec(i).tax_year
            and normalization_id = g_def_tax_basis_rec(i).normalization_id;

      write_log(g_job_no, 3, 0, 'Update Def Tax Basis Rows=' || to_char(a_num_rec - a_start_row + 1));
      return 0;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Update  Deferred Income Tax ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return 0;
   end update_def_tax_basis;

   -- =============================================================================
   --  Function CALC
   -- =============================================================================
   function calc(a_tax_detail_index      pls_integer,
                 a_tax_rates_sub_rec     type_tax_rates_rec,
                 a_tax_rates_rows        pls_integer,
                 a_tax_detail_bals_index pls_integer,
                 a_dfit_rates            type_dfit_rates_rec,
                 a_dfit_rates_rows       pls_integer) return number is
      i            pls_integer;
      l_rate_index pls_integer;
      l_nyears     pls_integer;
      j            pls_integer;
      l_statutory_trueup            pls_integer;
      l_trueup_adjustment            pls_integer;

      l_tax_year binary_double;
      type num_ary100_type is table of binary_double index by binary_integer;
      l_def_tax_rates num_ary100_type;

      l_diff_current binary_double := 0;
      l_diff_ft      binary_double := 0;
      l_rem_rate     binary_double := 0;

      type int_ary100_type is table of pls_integer index by binary_integer;
      l_def_tax_dates int_ary100_type; -- dates
      l_tax_date      pls_integer; -- dates

      l_def_income_tax_balance_beg binary_double := 0;
      l_def_income_tax_balance_end binary_double := 0;
      l_norm_diff_balance_beg      binary_double := 0;
      l_norm_diff_balance_end      binary_double := 0;
      l_def_income_tax_provision   binary_double := 0;
      l_def_income_tax_reversal    binary_double := 0;
      l_def_income_tax_adjust      binary_double := 0;
      l_def_income_tax_retire      binary_double := 0;
      l_def_income_tax_gain_loss   binary_double := 0;
      l_aram_rate                  binary_double := 0;
      l_aram_rate_end              binary_double := 0;
      l_life                       binary_double := 0;
      l_depreciation               binary_double := 0;
      l_depreciation_2             binary_double := 0;
      l_gain_loss                  binary_double := 0;
      l_cor_expense                binary_double := 0;
      l_gain_loss_2                binary_double := 0;
      l_cor_expense_2              binary_double := 0;
      l_capitalized_depr           binary_double := 0;
      l_capitalized_depr_2         binary_double := 0;
      l_retirements                binary_double := 0;
      l_extraordinary_retires      binary_double := 0;
      l_cost_of_removal_ft         binary_double := 0;
      l_amount_to_amortize         binary_double := 0;
      l_tax_balance_from           binary_double := 0;
      l_accum_reserve_from         binary_double := 0;
      l_accum_reserve_adjust_a     binary_double := 0;
      l_accum_reserve_adjust_b     binary_double := 0;
      l_trueup_amount              binary_double := 0;

      l_cap_depr_ind  pls_integer;
      l_no_zero_check pls_integer;

      l_scoop          pls_integer := 0;
      l_more_from_depr boolean := false;

      l_gain_loss_dfit_bal binary_double := 0;

      l_dfit_rate_order int_ary100_type;
      l_count           pls_integer;
      l_def_tax_date    binary_double;
      l_code            pls_integer;

   begin

      --
      -- Move the items in the deferred tax table to variables
      --   Get rid of the gets we don't need
      --

      l_tax_year                   := g_tax_detail_rec(a_tax_detail_index).tax_year;
      l_def_income_tax_balance_beg := nvl(g_tax_detail_rec(a_tax_detail_index)
                                          .def_income_tax_balance_beg,
                                          0) + nvl(g_tax_detail_rec(a_tax_detail_index)
                                                   .def_income_tax_bal_transfer,
                                                   0);
      --def_income_tax_balance_end     = dw_def_tax.getitemnumber(r,'def_income_tax_balance_end ;
      l_norm_diff_balance_beg := nvl(g_tax_detail_rec(a_tax_detail_index).norm_diff_balance_beg, 0) +
                                 nvl(g_tax_detail_rec(a_tax_detail_index).norm_diff_balance_transfer,
                                     0);
      --norm_diff_balance_end          = dw_def_tax.getitemnumber(r,'norm_diff_balance_end;
      --def_income_tax_provision       = dw_def_tax.getitemnumber(r,'def_income_tax_provision;
      --def_income_tax_reversal        = dw_def_tax.getitemnumber(r,'def_income_tax_reversal;
      l_def_income_tax_adjust := nvl(g_tax_detail_rec(a_tax_detail_index).def_income_tax_adjust, 0);
      --def_income_tax_retire          = dw_def_tax.getitemnumber(r,'def_income_tax_retire;
      --def_income_tax_gain_loss       = dw_def_tax.getitemnumber(r,'def_income_tax_gain_loss;
      l_aram_rate := nvl(g_tax_detail_rec(a_tax_detail_index).aram_rate, 0);
      --aram_rate_end                  = dnvl(w_def_tax.getitemnumber(r,'aram_rate_end;
      l_life                   := nvl(g_tax_detail_rec(a_tax_detail_index).life, 0);
      l_depreciation           := nvl(g_tax_detail_rec(a_tax_detail_index).da_depreciation, 0);
      l_depreciation_2         := nvl(g_tax_detail_rec(a_tax_detail_index).db_depreciation, 0);
      l_gain_loss              := nvl(g_tax_detail_rec(a_tax_detail_index).da_gain_loss, 0);
      l_gain_loss_2            := nvl(g_tax_detail_rec(a_tax_detail_index).db_gain_loss, 0);
      l_cor_expense            := nvl(g_tax_detail_rec(a_tax_detail_index).da_cor_expense, 0);
      l_cor_expense_2          := nvl(g_tax_detail_rec(a_tax_detail_index).db_cor_expense, 0);
      l_retirements            := nvl(g_tax_detail_rec(a_tax_detail_index).da_retirements, 0);
      l_extraordinary_retires  := nvl(g_tax_detail_rec(a_tax_detail_index).da_extraordinary_retires,
                                      0);
      l_capitalized_depr       := nvl(g_tax_detail_rec(a_tax_detail_index).da_capitalized_depr, 0);
      l_capitalized_depr_2     := nvl(g_tax_detail_rec(a_tax_detail_index).db_capitalized_depr, 0);
      l_cost_of_removal_ft     := nvl(g_tax_detail_rec(a_tax_detail_index).cost_of_removal_ft, 0);
      l_amount_to_amortize     := nvl(g_tax_detail_rec(a_tax_detail_index).amount_to_amortize, 0);
      l_cap_depr_ind           := nvl(g_tax_detail_rec(a_tax_detail_index).cap_depr_ind, 0);
      l_tax_balance_from       := nvl(g_tax_detail_rec(a_tax_detail_index).tax_balance_from, 0);
      l_accum_reserve_from     := nvl(g_tax_detail_rec(a_tax_detail_index).accum_reserve_from, 0);
      l_no_zero_check          := nvl(g_tax_detail_rec(a_tax_detail_index).no_zero_check, 0);
      l_accum_reserve_adjust_a := nvl(g_tax_detail_rec(a_tax_detail_index).accum_reserve_adjust_a,
                                      0);
      l_accum_reserve_adjust_b := nvl(g_tax_detail_rec(a_tax_detail_index).accum_reserve_adjust_b,
                                      0);
      l_statutory_trueup := nvl(g_tax_detail_rec(a_tax_detail_index).statutory_trueup,
                                      0);
      l_trueup_adjustment := nvl(g_tax_detail_rec(a_tax_detail_index).trueup_adjustment,
                                      0);
      -- PSEG CONVERSION EQUATIONs:

      -- handled in the retrieve
      --if isnull(def_income_tax_adjust) then def_income_tax_adjust = 0

      --gain_loss_dfit_bal             = g_tax_detail_rec(a_tax_detail_index).gain_loss_def_tax_balance;

      -- handled in the retrieve
      --if isnull(gain_loss_dfit_bal) then gain_loss_dfit_bal = 0

      l_def_income_tax_balance_beg := l_def_income_tax_balance_beg + l_gain_loss_dfit_bal;

      --norm_diff_balance_beg          = g_tax_detail_rec(a_tax_detail_index).pseg_beg_norm;

      --/////////////////////////////////////////////////////////////////////////
      --  We may need Deferred Tax Rates
      --  Get the most recent effective deferred tax rate
      --  The datawindow comes in ordered by ascending effective date
      --/////////////////////////////////////////////////////////////////////////

      -- dw_def_tax_rates.setsort('effective_date a;
      -- w_def_tax_rates.sort()
      -- Sort Deferred Tax Rates
      for i in 1 .. a_dfit_rates_rows
      loop
         l_count := 1;
         for j in 1 .. a_dfit_rates_rows
         loop
            if (a_dfit_rates(i).effective_date > a_dfit_rates(j).effective_date) then
               l_count := 1 + l_count;
            elsif (a_dfit_rates(i).effective_date = a_dfit_rates(j).effective_date and i > j) then
               l_count := 1 + l_count;
            end if;
         end loop;
         l_dfit_rate_order(l_count) := i;
      end loop;

      for i in 1 .. a_dfit_rates_rows
      loop
         j := l_dfit_rate_order(i);
         l_def_tax_rates(i) := a_dfit_rates(j).annual_rate;
         l_def_tax_dates(i) := a_dfit_rates(j).effective_date;
      end loop;
      -- YYYYMMDD
      l_tax_date := (round(l_tax_year, 0) * 10000) + 100 + 1;

      l_rate_index := 0;

      for i in 1 .. a_dfit_rates_rows
      loop
         --PostMsg(5,tax_date,"tax_date","Rate Dates','Tax Rates");
         --PostMsg(5,def_tax_dates[i - 1],"def_tax_datese","Rate Dates",'Tax Rates");
         l_def_tax_date := l_def_tax_dates(i);
         if (l_tax_date >= l_def_tax_date) then
            l_rate_index := i;
         end if;
      end loop;

      --PostMsg(5,rate_index,"Calc","Rate index','Tax Rates");
      if (l_rate_index = 0) then
         --PostMsg(5,1L,"Calc","Warning','No Good Def Tax Rates");
         l_rate_index := 1;
      end if;

      --
      --
      --
      --
      --

      -- the 'ending aram rate' is the one we use this year

      l_aram_rate_end := 0; --this is a div by 0 check

      --PC-6129 when norm schema is set to true up, aram rate will be the def tax rate
      if (l_norm_diff_balance_beg != 0) and (l_statutory_trueup = 0) then
         l_aram_rate_end := l_def_income_tax_balance_beg / l_norm_diff_balance_beg;
      else
         l_aram_rate_end := l_def_tax_rates(l_rate_index);
      end if;

      --* temporary fix for jonathan 4.15.5
      --if( norm_diff_balance_beg == 0  depreciation == 0 )
      --    goto have_calced;

      --diff_current = (depreciation * (1 - (capitalized_depr * ( 1 - cap_depr_ind)))  - gain_loss + cor_expense )  -
      --               (depreciation_2  * (1 - (capitalized_depr_2 * (1 - cap_depr_ind))) - gain_loss_2 + cor_expense_2 );

      l_diff_current := (l_depreciation * (1 - (l_capitalized_depr * (1 - l_cap_depr_ind))) -
                        l_gain_loss + l_cor_expense + l_accum_reserve_adjust_a) -
                        (l_depreciation_2 * (1 - (l_capitalized_depr_2 * (1 - l_cap_depr_ind))) -
                        l_gain_loss_2 + l_cor_expense_2 + l_accum_reserve_adjust_b);

      -- for tax_only basis differnces
      l_diff_current := l_diff_current - l_amount_to_amortize;

      l_diff_ft := (l_cor_expense * l_cost_of_removal_ft) -
                   (l_cor_expense_2 * l_cost_of_removal_ft);

      --diff_current = (depreciation - gain_loss + cor_expense) - (depreciation_2 - gain_loss_2 + cor_expense_2);

      -- if both the current difference and the balance have the same sign
      -- then deferred taxes are building-up

      if ((l_diff_current * l_norm_diff_balance_beg) >= 0) then
         l_def_income_tax_provision := (l_diff_current - l_diff_ft) * l_def_tax_rates(l_rate_index);
         goto have_calced;
      end if;

      -- We are in a reversal, because the current difference and the balance
      -- do not have the same sign

      -- if if there is no zero-check necessity, then the calc is straightforward.

      if (abs(l_diff_current - l_diff_ft) <= abs(l_norm_diff_balance_beg)) then
         l_def_income_tax_reversal := (l_diff_current - l_diff_ft) * l_aram_rate_end;
         goto have_calced;
      end if;

      -- Need to do a zero check because the current turnaround difference is greater than the
      -- accumulated difference.

      l_def_income_tax_reversal := (-l_norm_diff_balance_beg) * l_aram_rate_end;

      -- Will need to know if there is more 'from' difference coming in the future to do
      -- the following check for finished with def tax, or just another cross-over point
      -- Determine if there is more depreciation out there

      l_more_from_depr := false;

      l_nyears := round(g_tax_detail_rec(a_tax_detail_index)
                        .tax_year - g_tax_detail_rec(a_tax_detail_index).vintage_year + 2);

      if (l_nyears <= a_tax_rates_rows) then
         l_rem_rate := 0;

         -- these are sorted in asc year order
         for j in l_nyears .. a_tax_rates_rows
         loop
            l_rem_rate := l_rem_rate + a_tax_rates_sub_rec(j).rate;
         end loop;
         if (l_rem_rate != 0) then
            l_more_from_depr := true;
         end if;
         -- nyears <= ...
      end if;
      if (l_no_zero_check = 1) then
         l_more_from_depr := true;
      end if;
      -- Now, if there are more future 'from' tax rates then this is
      -- a temporary cross-over point. Apply the remainder of the current difference as a build-up
      -- at the statutory rate

      if (l_tax_balance_from * l_accum_reserve_from > 0 and
         abs(l_tax_balance_from) > abs(l_accum_reserve_from)) then
         l_more_from_depr := true;
      end if;

      if (l_more_from_depr = true) then
         l_def_income_tax_provision := (l_diff_current + l_norm_diff_balance_beg) *
                                       l_def_tax_rates(l_rate_index);

         goto have_calced;
      else
         l_diff_current := (-l_norm_diff_balance_beg);
         goto have_calced;
      end if;

      <<have_calced>>

      --PC-6129 true up amount will be the timing difference * the rate less the current amount
      if (l_statutory_Trueup) = 1 and (l_trueup_Adjustment) = 0 then
         l_trueup_amount := (l_norm_diff_balance_beg * l_def_tax_rates(l_rate_index)) - l_def_income_tax_balance_beg;
      elsif (l_statutory_Trueup) = 1 and (l_trueup_Adjustment) = 1 then
         l_def_income_tax_adjust := (l_norm_diff_balance_beg * l_def_tax_rates(l_rate_index)) - l_def_income_tax_balance_beg;
         l_trueup_amount :=0;
      else
         l_trueup_amount :=0;
      end if;

      --Add true up to provision if we're further increasing/decreasing the DIT amount, else reversal
      if (((l_def_income_tax_balance_beg >0) and (l_trueup_amount>0)) or ((l_def_income_tax_balance_beg <=0) and (l_trueup_amount<0))) then
         l_def_income_tax_provision := l_def_income_tax_provision + l_trueup_amount;
      else
         l_def_income_tax_reversal := l_def_income_tax_reversal + l_trueup_amount;
      end if;

      l_def_income_tax_balance_end := l_def_income_tax_balance_beg + l_def_income_tax_provision +
                                      l_def_income_tax_reversal + l_def_income_tax_adjust;

      l_norm_diff_balance_end := l_norm_diff_balance_beg + l_diff_current;

      --//////////////////////////////////////////////////////////////////////////////////
      -- Put Everything back into the datawindows
      --//////////////////////////////////////////////////////////////////////////////////

      -- PSEG TEMP: fill in beginning difference balance

      --g_tax_detail_rec(a_tax_detail_index).norm_diff_balance_beg = norm_diff_balance_beg;

      g_tax_detail_rec(a_tax_detail_index).def_income_tax_balance_end := l_def_income_tax_balance_end;
      g_tax_detail_rec(a_tax_detail_index).norm_diff_balance_end := l_norm_diff_balance_end;
      g_tax_detail_rec(a_tax_detail_index).def_income_tax_provision := l_def_income_tax_provision;
      g_tax_detail_rec(a_tax_detail_index).def_income_tax_reversal := l_def_income_tax_reversal;
      g_tax_detail_rec(a_tax_detail_index).def_income_tax_retire := l_def_income_tax_retire;
      g_tax_detail_rec(a_tax_detail_index).def_income_tax_gain_loss := l_amount_to_amortize; --def_income_tax_gain_loss;
      if (abs(l_def_income_tax_adjust) < .0000001) then
         l_def_income_tax_adjust := 0;
      end if;
      g_tax_detail_rec(a_tax_detail_index).def_income_tax_adjust := l_def_income_tax_adjust;
      if (abs(l_aram_rate_end) < .0000001) then
         l_aram_rate_end := 0;
      end if;
      g_tax_detail_rec(a_tax_detail_index).aram_rate_end := l_aram_rate_end;

      if a_tax_detail_bals_index <> 0 then
         g_tax_detail_rec(a_tax_detail_bals_index).def_income_tax_balance_beg := l_def_income_tax_balance_end;
         g_tax_detail_rec(a_tax_detail_bals_index).norm_diff_balance_beg := l_norm_diff_balance_end;
         g_tax_detail_rec(a_tax_detail_bals_index).aram_rate := l_aram_rate_end;
      end if;
      return 0;
   exception
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   4,
                   l_code,
                   'Calc Record ID=' || to_char(g_tax_detail_rec(a_tax_detail_index).tax_record_id) || '  ' ||
                   sqlerrm(l_code) || ' ' || dbms_utility.format_error_backtrace);
         return - 1;
   end calc;

   -- =============================================================================
   --  Function BASIS_CALC
   -- =============================================================================
   function basis_calc(a_def_tax_index         pls_integer,
                       a_def_tax_rates_sub_rec type_dfit_rates_rec,
                       a_def_tax_rates_rows    pls_integer,
                       a_def_tax_bals_index    pls_integer,
                       a_def_tax_bals_rows     pls_integer,
                       a_short_months          pls_integer) return pls_integer is

      l_tax_year     binary_double;
      l_vintage_year pls_integer;
      i              pls_integer;
      l_rate_index   pls_integer;
      l_nyears       pls_integer;
      j              pls_integer;
      l_statutory_Trueup pls_integer;
      l_trueup_adjustment pls_integer;

      type num_ary200_type is table of binary_double index by binary_integer;
      l_def_tax_rates num_ary200_type;

      l_activity   binary_double;
      l_addition   binary_double;
      l_retirement binary_double;
      l_amort      binary_double;
      l_rem_rate   binary_double;
      l_rem_life   binary_double;
      l_ratio      binary_double;

      type int_ary200_type is table of pls_integer index by binary_integer;
      l_def_tax_dates int_ary200_type; --date
      l_tax_date      pls_integer; --date

      l_def_income_tax_balance_beg binary_double;
      l_def_income_tax_balance_end binary_double;
      l_norm_diff_balance_beg      binary_double;
      l_norm_diff_balance_end      binary_double;
      l_def_income_tax_provision   binary_double;
      l_def_income_tax_reversal    binary_double;
      l_def_income_tax_adjust      binary_double;
      l_def_income_tax_retire      binary_double;
      l_aram_rate                  binary_double;
      l_aram_rate_end              binary_double;
      l_life                       binary_double;
      l_basis_amount_beg           binary_double;
      l_basis_amount_end           binary_double;
      l_basis_amount_activity      binary_double;
      l_basis_amount_transfer      binary_double;
      l_number_months_beg          binary_double;
      l_input_amortization         binary_double;
      l_amount_to_amortize         binary_double;
      l_bonus_amount               binary_double;
      l_alloc_bonus_option         varchar2(35);
      l_tax_balance_end           binary_double;
      l_accum_reserve_end         binary_double;
      l_basis_diff_add_ret        binary_double;
      l_basis_diff_activity_split binary_double;
      l_trueup_amount             binary_double;

      l_scoop             pls_integer;
      l_more_from_depr    pls_integer := 0;
      l_allocation        boolean;
      l_retire_reversal   boolean;
      l_tax_depr_reversal boolean;

      --
      -- Move the items in the deferred tax table to variables
      --   Get rid of the gets we don't need
      --

   begin

      l_tax_year                   := g_def_tax_basis_rec(a_def_tax_index).tax_year;
      l_def_income_tax_balance_beg := nvl(g_def_tax_basis_rec(a_def_tax_index)
                                          .def_income_tax_balance_beg,
                                          0) + nvl(g_def_tax_basis_rec(a_def_tax_index)
                                                   .def_income_tax_bal_transfer,
                                                   0);
      --def_income_tax_balance_end     := dw_def_tax.getitemnumber(r,'def_income_tax_balance_end ')
      l_norm_diff_balance_beg := nvl(g_def_tax_basis_rec(a_def_tax_index).norm_diff_balance_beg, 0) +
                                 nvl(g_def_tax_basis_rec(a_def_tax_index).norm_diff_balance_transfer,
                                     0);
      l_basis_amount_beg      := nvl(g_def_tax_basis_rec(a_def_tax_index).basis_amount_beg, 0);
      l_basis_amount_end      := nvl(g_def_tax_basis_rec(a_def_tax_index).basis_amount_end, 0);
      l_basis_amount_activity := nvl(g_def_tax_basis_rec(a_def_tax_index).basis_amount_activity, 0);
      l_basis_amount_transfer := nvl(g_def_tax_basis_rec(a_def_tax_index).basis_amount_transfer, 0);

      --norm_diff_balance_end          := nvl(g_def_tax_basis_rec(a_def_tax_index ).norm_diff_balance_end;
      --def_income_tax_provision       := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_provision;
      --def_income_tax_reversal        := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_reversal;
      l_def_income_tax_adjust := nvl(g_def_tax_basis_rec(a_def_tax_index).def_income_tax_adjust, 0);
      --def_income_tax_retire          := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_retire;
      --def_income_tax_gain_loss       := nvl(g_def_tax_basis_rec(a_def_tax_index ).def_income_tax_gain_loss;
      l_aram_rate := nvl(g_def_tax_basis_rec(a_def_tax_index).aram_rate, 0);
      --aram_rate_end                  := nvl(g_def_tax_basis_rec(a_def_tax_index ).aram_rate_end;
      l_life               := nvl(g_def_tax_basis_rec(a_def_tax_index).life, 0);
      l_vintage_year       := nvl(g_def_tax_basis_rec(a_def_tax_index).vintage_year, 0);
      l_input_amortization := nvl(g_def_tax_basis_rec(a_def_tax_index).input_amortization, 0);
      l_number_months_beg  := nvl(g_def_tax_basis_rec(a_def_tax_index).number_months_beg, 0);
      --amount_to_amortize             := nvl(g_def_tax_basis_rec(a_def_tax_index ).amount_to_amortize;
      l_tax_balance_end           := nvl(g_def_tax_basis_rec(a_def_tax_index).tax_balance_end, 0);
      l_accum_reserve_end         := nvl(g_def_tax_basis_rec(a_def_tax_index).accum_reserve_end, 0);
      l_basis_diff_add_ret        := nvl(g_def_tax_basis_rec(a_def_tax_index).basis_diff_add_ret, 0);
      l_basis_diff_activity_split := nvl(g_def_tax_basis_rec(a_def_tax_index)
                                         .basis_diff_activity_split,
                                         0);
      l_bonus_amount             := nvl(g_def_tax_basis_rec(a_def_tax_index).bonus_amount, 0);
      l_alloc_bonus_option       := nvl(g_def_tax_basis_rec(a_def_tax_index).alloc_bonus_option, 'no');
      l_statutory_trueup             := nvl(g_def_tax_basis_rec(a_def_tax_index).statutory_trueup, 0);
      l_trueup_adjustment             := nvl(g_def_tax_basis_rec(a_def_tax_index).trueup_adjustment, 0);

      --env_temp = getenv("TEMP");
      --sprintf(filename,"%s\\tfitcalc.txt",env_temp);
      --unit = fopen(filename,"a+");
      --fprintf(unit ,"%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t\n",
      --    tax_year ,
      --def_income_tax_balance_beg ,
      --norm_diff_balance_beg ,
      --basis_amount_beg,
      --basis_amount_end,
      --basis_amount_activity,
      --basis_amount_transfer,
      --def_income_tax_adjust ,
      --aram_rate ,
      --life ,
      --vintage_year ,
      --input_amortization ,
      --number_months_beg,
      --amount_to_amortize ,
      --def_tax->book_depr_alloc_ind,
      --def_tax->basis_diff_retire_reversal,
      --def_tax->amortization_type_id);

      /*
       if(def_tax->tax_balance_end == 0)
          amount_to_amortize = - (def_tax->norm_diff_balance_beg -
                            def_tax->basis_amount_activity);
       else
          amount_to_amortize =  - (((def_tax->tax_balance_end  -
                                   def_tax->accum_reserve_end)
                                 / def_tax->tax_balance_end ) *
                             def_tax->basis_amount_end )
                                   -  (def_tax->norm_diff_balance_beg
                            -   def_tax->basis_amount_activity);
      */

      if (g_def_tax_basis_rec(a_def_tax_index).book_depr_alloc_ind >= 1) then
         l_allocation := true;
      else
         l_allocation := false;
      end if;

      if (g_def_tax_basis_rec(a_def_tax_index).basis_diff_retire_reversal = 1) then
         l_retire_reversal := false;
      else
         l_retire_reversal := true;
      end if;

      if (g_def_tax_basis_rec(a_def_tax_index).amortization_type_id = -1) then
         l_tax_depr_reversal := true;
         l_retire_reversal   := false; -- this combination is always true,so force it
      else
         l_tax_depr_reversal := false;
      end if;

      -- Calculate amount of tax-only defferred tax to amortize
      if (l_tax_balance_end = 0) then
         l_amount_to_amortize := - (l_norm_diff_balance_beg - l_basis_amount_activity);
      else

         if l_alloc_bonus_option = 'yes' then
            l_amount_to_amortize := - ((((l_tax_balance_end - l_bonus_amount) - (l_accum_reserve_end - l_bonus_amount)) / (l_tax_balance_end - l_bonus_amount)) *
                                       l_basis_amount_end) - l_norm_diff_balance_beg;
         else
            l_amount_to_amortize := - (((l_tax_balance_end - l_accum_reserve_end) / l_tax_balance_end) *
                                       l_basis_amount_end) - l_norm_diff_balance_beg;
         end if;

         if (l_basis_amount_activity != 0) then
            l_amount_to_amortize := l_amount_to_amortize - l_basis_diff_add_ret;
         end if;

      end if;

      --
      --  We may need Deferred Tax Rates
      --  Get the most recent effective deferred tax rate
      --  The datawindow comes in ordered by ascending effective date
      --

      --dw_def_tax_rates.setsort('effective_date a')
      --dw_def_tax_rates.sort()

      for i in 1 .. a_def_tax_rates_rows
      loop
         l_def_tax_rates(i) := a_def_tax_rates_sub_rec(i).annual_rate;
         l_def_tax_dates(i) := a_def_tax_rates_sub_rec(i).effective_date;
      end loop;

      -- YYYYMMDD
      l_tax_date := (round(l_tax_year) * 10000) + 100 + 1;
      --tax_date := date('1/1/' +string(tax_year))

      l_rate_index := 0;

      for i in 1 .. a_def_tax_rates_rows
      loop
         if (l_tax_date >= l_def_tax_dates(i)) then
            l_rate_index := i;
         end if;
      end loop;

      if (l_rate_index = 0) then
         --messagebox('Warning','No Good Def Tax Rates')
         l_rate_index := 1;
      end if;

      -- Remaining Life
      if (l_vintage_year = round(l_tax_year)) then
         l_rem_life := l_life;
      else
         l_rem_life := ((l_life * 12) - (l_number_months_beg - 6)) / 12;
      end if;
      -- rem_life := vintage_year + life - tax_year + .5;

      if (l_rem_life < 1) then
         l_rem_life := 1;
      end if;

      -- the 'ending aram rate' is the one we use this year

      l_aram_rate_end := 0; --this is a div by 0 check
      --PC-6129 when norm schema is set to true up, aram rate will be the def tax rate
      if (l_norm_diff_balance_beg != 0) and (l_statutory_trueup = 0) then
         l_aram_rate_end := l_def_income_tax_balance_beg / l_norm_diff_balance_beg;
      else
         l_aram_rate_end := l_def_tax_rates(l_rate_index);
      end if;

      -- Check for activity
      -- The signs are confusing here,  Book-Basis differences in tax_book_reconcile are
      -- Negative.  The natural sign for a retirement is going to be negative.

      l_activity := - (l_basis_amount_end - l_basis_amount_beg - l_basis_amount_transfer);

      -- if the activity has a different sign than the Diff_balance, then it is a retirement
      -- also, if norm_diff_balance_beg is zero then this is probably initial addition.

      if (l_basis_amount_activity = 0) then
         l_retirement := l_activity;
         l_addition   := 0;
      else
         --addition   := activity;
         --retirement := 0;
         if (l_basis_amount_transfer <> 0) then
            l_addition   := l_activity;
            l_retirement := 0;
         else
            --retirement := -(basis_amount_end - (basis_amount_beg + basis_amount_activity));
            --addition   := activity - retirement;
            if (l_basis_diff_activity_split = 1) then
               l_retirement := - (l_basis_amount_end -
                                  (l_basis_amount_beg + l_basis_amount_activity));
               l_addition   := l_activity - l_retirement;
            else
               l_addition   := l_activity;
               l_retirement := 0;
            end if;
         end if;
      end if;

      -- If it is a retirement, then ratio a retirement amount out of the
      -- unamortized balance

      if ((l_basis_amount_beg + l_basis_amount_transfer + l_addition) = 0) then
         --div by 0 check
         l_ratio := 0;
      else
         l_ratio := (-l_norm_diff_balance_beg + l_addition) /
                    (l_basis_amount_beg + l_basis_amount_transfer + l_addition);
      end if;

      l_retirement := l_retirement * l_ratio;

      if (l_retire_reversal = false) then
         l_retirement := 0;
      end if;
      -- Check for end-of-life; or just calc the amortization

      --if rem_life := 1 or
      if (round(l_norm_diff_balance_beg + l_addition, 1) *
         round(l_norm_diff_balance_beg + l_addition + l_retirement, 1) <= 0) then
         l_retirement := - (l_norm_diff_balance_beg + l_addition);
         l_amort      := 0;
      else
         if (l_tax_depr_reversal = true) then
            l_amort := l_amount_to_amortize;
         else
            l_amort := - (l_norm_diff_balance_beg + (l_addition * .5) + l_retirement) *
                        (1 / l_rem_life) * (a_short_months / 12);
         end if;
      end if;
      -- OVERRIDE with input, if entered

      if (l_allocation = true) then
         l_amort := l_input_amortization;
      end if;

      l_def_income_tax_retire    := l_retirement * l_aram_rate_end;
      l_def_income_tax_provision := l_addition * l_def_tax_rates(l_rate_index);
      l_def_income_tax_reversal  := (l_amort + l_retirement) * l_aram_rate_end;


      --PC-6129 true up amount will be the timing difference * the rate less the current amount
      if (l_statutory_Trueup) = 1 and (l_trueup_adjustment) = 0 then
         l_trueup_amount := (l_norm_diff_balance_beg * l_def_tax_rates(l_rate_index)) - l_def_income_tax_balance_beg;
      elsif (l_statutory_Trueup) = 1 and (l_trueup_adjustment) = 1 then
         l_def_income_tax_adjust := (l_norm_diff_balance_beg * l_def_tax_rates(l_rate_index)) - l_def_income_tax_balance_beg;
         l_trueup_amount :=0;
      else
         l_trueup_amount :=0;
      end if;

      --PC-6129 true up with be a provision when the DIT is positive and trueup is increasing the balance.  It is a reversal when the DIT is negative and trueup is decreasing the balance.
      --The true up amount is a reversal when the true up amount is causing the DIT balance to get closer to zero.
      if (((l_def_income_tax_balance_beg >0) and (l_trueup_amount>0)) or ((l_def_income_tax_balance_beg <=0) and (l_trueup_amount<0))) then
         l_def_income_tax_provision := l_def_income_tax_provision + l_trueup_amount;
      else
         l_def_income_tax_reversal := l_def_income_tax_reversal + l_trueup_amount;
      end if;

      l_def_income_tax_balance_end := l_def_income_tax_balance_beg + l_def_income_tax_provision +
                                      l_def_income_tax_reversal + l_def_income_tax_adjust;

      l_norm_diff_balance_end := l_norm_diff_balance_beg + l_addition + l_retirement + l_amort;

      --
      -- Put Everything back into the datawindows
      --

      if (abs(l_def_income_tax_balance_end) < .0000001) then
         l_def_income_tax_balance_end := 0;
      end if;
      g_def_tax_basis_rec(a_def_tax_index).def_income_tax_balance_end := l_def_income_tax_balance_end;

      if (abs(l_norm_diff_balance_end) < .0000001) then
         l_norm_diff_balance_end := 0;
      end if;
      g_def_tax_basis_rec(a_def_tax_index).norm_diff_balance_end := l_norm_diff_balance_end;

      if (abs(l_def_income_tax_provision) < .0000001) then
         l_def_income_tax_provision := 0;
      end if;
      g_def_tax_basis_rec(a_def_tax_index).def_income_tax_provision := l_def_income_tax_provision;

      if (abs(l_def_income_tax_reversal) < .0000001) then
         l_def_income_tax_reversal := 0;
      end if;
      g_def_tax_basis_rec(a_def_tax_index).def_income_tax_reversal := l_def_income_tax_reversal;

      if (abs(l_def_income_tax_retire) < .0000001) then
         l_def_income_tax_retire := 0;
      end if;
      g_def_tax_basis_rec(a_def_tax_index).def_income_tax_retire := l_def_income_tax_retire;

      if (abs(l_def_income_tax_adjust) < .0000001) then
         l_def_income_tax_adjust := 0;
      end if;
      g_def_tax_basis_rec(a_def_tax_index).def_income_tax_adjust := l_def_income_tax_adjust;

      if (abs(l_aram_rate_end) < .0000001) then
         l_aram_rate_end := 0;
      end if;
      g_def_tax_basis_rec(a_def_tax_index).aram_rate_end := l_aram_rate_end;

      if (abs(l_addition + l_retirement) < .0000001) then
         g_def_tax_basis_rec(a_def_tax_index).basis_diff_add_ret := 0;
      else
         g_def_tax_basis_rec(a_def_tax_index).basis_diff_add_ret := l_addition + l_retirement;
      end if;

      -- the following for display purposes only in Pat's window

      --dw_def_tax.setitem(r,'ratio',                      ratio)

      -- Next year's balances

      if (a_def_tax_bals_index <> 0) then
         if (abs(l_def_income_tax_balance_end) < .0000001) then
            l_def_income_tax_balance_end := 0;
         end if;
         g_def_tax_basis_rec(a_def_tax_bals_index).def_income_tax_balance_beg := l_def_income_tax_balance_end;
         if (abs(l_norm_diff_balance_end) < .0000001) then
            l_norm_diff_balance_end := 0;
         end if;
         g_def_tax_basis_rec(a_def_tax_bals_index).norm_diff_balance_beg := l_norm_diff_balance_end;
         if (abs(l_aram_rate_end) < .0000001) then
            l_aram_rate_end := 0;
         end if;
         g_def_tax_basis_rec(a_def_tax_bals_index).aram_rate := l_aram_rate_end;
      end if;

      return 1;
   end basis_calc;

   -- =============================================================================
   --  Procedure SET_SESSION_PARAMETER
   -- =============================================================================
   procedure set_session_parameter is
      cursor session_param_cur is
         select parameter, value, type
           from pp_session_parameters
          where lower(users) = 'all'
             or lower(users) = 'dfit';
      l_count pls_integer;
      i       pls_integer;
      type type_session_param_rec is table of session_param_cur%rowtype;
      l_session_param_rec type_session_param_rec;
      l_code              pls_integer;
      l_sql               varchar2(200);

   begin

      open session_param_cur;
      fetch session_param_cur bulk collect
         into l_session_param_rec;
      close session_param_cur;

      l_count := l_session_param_rec.count;
      for i in 1 .. l_count
      loop
         if l_session_param_rec(i).type = 'number' or l_session_param_rec(i).type = 'boolean' then
            l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
                    .value;
         elsif l_session_param_rec(i).type = 'string' then
            l_sql := 'alter session set ' || l_session_param_rec(i).parameter || ' = ''' || l_session_param_rec(i)
                    .value || '''';
         end if;
         execute immediate l_sql;
         write_log(g_job_no,
                   0,
                   0,
                   'Set Session Parameter ' || l_session_param_rec(i).parameter || ' = ' || l_session_param_rec(i)
                   .value);
      end loop;
      return;
   exception
      when no_data_found then
         return;
      when others then
         l_code := sqlcode;
         write_log(g_job_no,
                   5,
                   l_code,
                   'Set Session Parameter ' || sqlerrm(l_code) || ' ' ||
                   dbms_utility.format_error_backtrace);
         return;
   end set_session_parameter;

   -- =============================================================================
   --  Procedure WRITE_LOG
   -- =============================================================================
   procedure write_log(a_job_no     number,
                       a_error_type number,
                       a_code       number,
                       a_msg        varchar2) as
      pragma autonomous_transaction;
      l_code   pls_integer;
      l_msg    varchar(2000);
      l_cur_ts timestamp;

   begin
      l_cur_ts := current_timestamp;
      l_msg    := a_msg;
      dbms_output.put_line(l_msg);
      insert into tax_job_log
         (job_no, line_no, log_date, error_type, error_code, msg)
      values
         (a_job_no, g_line_no, sysdate, a_error_type, a_code, l_msg);
      commit;
      g_line_no := g_line_no + 1;
      return;
   exception
      when others then
         l_code := sqlcode;
         dbms_output.put_line('Write Log ' || sqlerrm(l_code) || ' ' ||
                              dbms_utility.format_error_backtrace);
         return;
   end write_log;

end TAX_DFIT_CALC;
/


